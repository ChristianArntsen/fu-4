<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
if (defined('ENVIRONMENT') && ENVIRONMENT != 'testing' && ENVIRONMENT != 'jenkins') {
	require BASEPATH . '../application/vendor/autoload.php';
}
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package        CodeIgniter
 * @author        ExpressionEngine Dev Team
 * @copyright    Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license        http://codeigniter.com/user_guide/license.html
 * @link        http://codeigniter.com
 * @since        Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Email Helpers
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 * @author        ExpressionEngine Dev Team
 * @link        http://codeigniter.com/user_guide/helpers/email_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Validate email address
 *
 * @access    public
 * @return    bool
 */
if (!function_exists('valid_email')) {
	function valid_email($address)
	{
		return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $address)) ? FALSE : TRUE;
	}
}

// ------------------------------------------------------------------------

/**
 * Send an email
 *
 * @access    public
 * @return    bool
 */
if (!function_exists('send_email')) {
	function send_email($recipient, $subject = 'Test email', $message = 'Hello World')
	{
		echo 'just sent mail';
		return mail($recipient, $subject, $message);
	}
}
if (!function_exists('url_encode')) {
	function url_encode($string)
	{
		//$string = str_replace(' ', '', $string);
		$replacements = array('%26', '%27', '%22', '%21', '%2A', '%28', '%29', '%3B', '%3A', '%40', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%23', '%5B', '%5D');
		$entities = array('&', "'", '"', '!', '*', '(', ')', ';', ':', '@', '=', '+', '$', ',', '/', '?', '#', '[', ']');
		return str_replace($entities, $replacements, ($string));
	}
}
if (!function_exists('send_sendgrid')) {
	function send_sendgrid($to, $subject, $message, $from = '', $fromName = '', $campaign_id = 0, $course_id = false, $invoice_id = null)
	{
		$sendgridConfig = config_item('sendgrid');
		$sendgrid = new SendGrid($sendgridConfig['username'], $sendgridConfig['password']);

		$success = true;
		$errors = 0;
		$from = ($from == '') ? 'no-reply@foreup.com' : $from;
        $setFrom = ($from == 'billing@foreup.com') ? $from : 'no-reply@foreup.com';
		$fromName = ($fromName == '') ? 'ForeUP' : $fromName;
		if(!$course_id){
			$CI = & get_instance();
			if($CI->session->userdata('course_id')){
				$course_id = $CI->session->userdata('course_id');
			}
		}

		$to_text = '';
		$bcc_text = '';
		if (!is_array($to) && $to == '') {
			send_sendgrid_email('to=jhopkins@foreup.com&', "Error - No recipients", url_encode("To: $to<br/><br/>From: $from<br/><br/>Subject: $subject<br/><br/>Message: $message<br/><br/>Errors: "), "", $from, 'ForeUP Error');
			$errors++;
			$success = false;
		} else if (is_array($to)) {
			$to = array_unique($to);
			$chunked_to = array_chunk($to, 800);

			log_message("debug", 'email_helper send_sendgrid campaign_id - ' . $campaign_id . ' recipient count ' . count($to));
			foreach ($chunked_to as $chunk) {
				$to_text = 'to=jhopkins@foreup.com&';
				$bcc_text = '';

				log_message("debug", 'email_helper send_sendgrid recipients - ' . json_encode($chunk));

				$email = new SendGrid\Email();
				$email->setSubject($subject)
					->setFrom($setFrom)
					->setFromName($fromName)
					->setReplyTo($from)
					->addUniqueArg("marketing_campaign_id", $campaign_id)
					->addUniqueArg("course_id", $course_id)
					->addUniqueArg("invoice_id", $invoice_id)
					->setHtml($message)
					->setSmtpapiTos($chunk);
				$result = ($sendgrid->send($email));
				log_message("debug", 'email_helper send_sendgrid response - ' . json_encode($result->body['message']));

				if ($result->body['message']== 'error') {
					$errors = '';
					foreach ($result->body->errors as $error)
						$errors .= $error;
					send_sendgrid_email('to=jhopkins@foreup.com&', "Error - Sendgrid reported error array", url_encode("To: " . implode(', ', $chunk) . "<br/><br/>From: $from<br/><br/>Subject: $subject<br/><br/>Errors: $errors<br/><br/>Message: $message"), "", $from, 'ForeUP Error');
					$errors++;
					$success = false;
				}
			}
		} else {
			$tos = array();
			$tos[] = $to;
			$email = new SendGrid\Email();
			$email->setSubject($subject)
				->setFrom($setFrom)
				->setFromName($fromName)
				->setReplyTo($from)
				->addUniqueArg("marketing_campaign_id", $campaign_id)
				->addUniqueArg("course_id", $course_id)
				->addUniqueArg("invoice_id", $invoice_id)
				->setHtml($message)
				->setSmtpapiTos($tos);
			$result = $sendgrid->send($email);

			if (!empty($result->body['message']) && $result->body['message'] == 'error') {
				$errors = '';
				foreach ($result->body['message'] as $error)
					$errors .= $error;
				send_sendgrid_email('to=jhopkins@foreup.com&', "Error - Sendgrid reported error individual", url_encode("To: $to<br/><br/>From: $from<br/><br/>Subject: $subject<br/><br/>Errors: $errors<br/><br/>Message: $message"), "", $from, 'ForeUP Error');
				$errors++;
				$success = false;
			}
		}
		$CI = &get_instance();
		$CI->load->model('Communication');
		$CI->Communication->record('Email', count($to), $from, $campaign_id, $course_id);

		return array('success' => $success, 'errors' => $errors);
	}
}

if (!function_exists('send_sendgrid_email')) {
	function send_sendgrid_email($to_text, $subject, $message, $bcc_text, $from, $fromName)
	{
		//echo "api_user=foreup&api_key=GolfCompete#17&{$to_text}{$bcc_text}subject=$subject&html=$message&from=$from&fromname=$fromName<br/>";
		$ch2 = curl_init('https://sendgrid.com/api/mail.send.json');
		curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch2, CURLOPT_POST, 1);
		//curl_setopt($ch2, CURLOPT_POSTFIELDS,"api_user=foreup&api_key=GolfCompete#17&{$to_text}{$bcc_text}subject=$subject&html=$message&replyto=$from&from=no-reply@foreupsoftware.com&fromname=$fromName");
		curl_setopt($ch2, CURLOPT_POSTFIELDS,"api_user=foreup&api_key=GolfCompete#17&{$to_text}{$bcc_text}subject=$subject&html=$message&replyto=$from&from=no-reply@foreupsoftware.com&fromname=$fromName");		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch2, CURLOPT_TIMEOUT, 300);
		$data2 = curl_exec($ch2);
		//print_r($data2);
		return $data2;
	}
}

/* End of file email_helper.php */
/* Location: ./system/helpers/email_helper.php */