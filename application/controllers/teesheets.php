<?php
require_once ("secure_area.php");
class Teesheets extends Secure_area
{
	function __construct()
	{
		parent::__construct('teesheets');
		$this->load->model('teesheet');
        $this->load->model('teetime');
        $this->load->model('standby');
        $this->load->model('customer');
        $this->load->model('sale');
		$this->load->model('course');
        $this->load->library('sale_lib');
        $this->load->helper('url');
	}
	
	function test_items(){
		$this->load->model('Item');
		$this->Item->create_teetimes();
	}
	
	function test_weather() {
		$this->load->model('weather');
		$this->weather->zip_code = '84663';
		$results = $this->weather->getLatest();
		print_r($results);
	}
	function index()
	{
        if ($this->config->item('stack_tee_sheets') && !$this->session->userdata('selected_stacked_course_id')) {
            $this->session->set_userdata('selected_stacked_course_id', $this->config->item('course_id'));
        }
        if ($this->config->item('tee_sheet_speed_up') == 1 && $this->input->get('in_iframe') != 1)
        {
            redirect(site_url('v2/home#tee_sheet'));
        }
		$this->load->model('note');
		$this->load->model('Squeeze');
        $this->load->model('Increment_adjustment');
		$dataArray = array();
		$tsMenu = '';
        if ($this->input->post('teesheetMenu'))
            $this->teesheet->switch_tee_sheet();
		$current_teesheet = $this->session->userdata('teesheet_id');
		//build a menu with the tee sheet id's
		$tsMenu = $this->teesheet->get_tee_sheet_menu($current_teesheet);
		//echo (date('Ymd', strtotime('-1 week'))-100).'0000';
        $JSONData = $this->teesheet->getJSONTeeTimes((date('Ymd', strtotime('-1 week'))-100).'0000', (date('Ymd', strtotime('+3 week'))-100).'0000');
		$dataArray = array(
            'tsMenu'=>$tsMenu,
            'teesheet_year'=>$this->input->post('teesheet_year'),
            'teesheet_month'=>$this->input->post('teesheet_month'),
            'teesheet_date'=>$this->input->post('teesheet_date'),
            'current_teesheet'=>$current_teesheet,
            'JSONData'=>$JSONData,
            'openhour'=> $this->session->userdata('openhour'),
            'teesheet_color'=>$this->session->userdata('teesheet_color'),
            'closehour'=> $this->session->userdata('closehour'),
			'book_sunrise_sunset' => $this->session->userdata('book_sunrise_sunset'),
			'sunrise_offset' => $this->session->userdata('sunrise_offset'),
			'sunset_offset' => $this->session->userdata('sunset_offset'),
            'increment'=> $this->session->userdata('increment'),
            'holes'=> $this->session->userdata('holes'),
            'fntime'=> $this->session->userdata('frontnine'),
            //'notes'=>$this->note->get_all()->result_array(),
            //'fdweather'=> $this->teesheet->make_5_day_weather(),
            'user_level'=> $this->session->userdata('user_level'),
            'associated_courses'=>$this->session->userdata('associated_courses'),
            'user_id'=> $this->session->userdata('user_id'),
            'purchase'=>$this->session->userdata('sales'),
            'price_colors' => $this->Green_fee->get_colors(),
			'controller_name'=>strtolower(get_class()),
			'squeezes' => $this->Squeeze->get_json_list(),
            'dailies' => $this->Increment_adjustment->get_json_list(true)
        );
        if ($this->session->userdata('selected_stacked_course_id') && $this->config->item('stack_tee_sheets')) {
            $dataArray['tee_sheets'] = $this->teesheet->get_all_from_course(10000, 0, $this->session->userdata('selected_stacked_course_id'))->result_array();
        }
        else {
            $dataArray['tee_sheets'] = $this->teesheet->get_all(10000, 0, true)->result_array();
        }
        $dataArray['json_tee_sheets'] = array();
        foreach ($dataArray['tee_sheets'] as $tee_sheet) {
            $tee_sheet_id = $tee_sheet['teesheet_id'];
            //$tee_sheet['title'] = str_replace("'", '', $tee_sheet['title']);
            $dataArray['json_tee_sheets'][$tee_sheet_id] = $tee_sheet;
            $dataArray['tee_times'][$tee_sheet_id] = json_decode($this->teesheet->getJSONTeeTimes((date('Ymd', strtotime('-1 week')) - 100) . '0000', (date('Ymd', strtotime('+3 week')) - 100) . '0000', '', false, false, $tee_sheet_id));
        }
        $dataArray['json_tee_sheets'] = str_replace("'", "\'", json_encode($dataArray['json_tee_sheets']));
        $dataArray['tee_times'] = json_encode($dataArray['tee_times']);

		$this->load->model('v2/Customer_model');
		$dataArray['field_settings'] = $this->Customer_model->get_field_settings();
        $this->load->view("teetimes/manage", $dataArray);
	}
	function facebook_page_select()
	{
		return $this->load->view('customers/facebook_page_select');
	}
	function clear_facebook_settings()
	{
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_page_id = '';
		$course_info->facebook_page_name = '';
		$course_info->facebook_extended_access_token = '';
		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_page_id', '');
			$this->session->set_userdata('facebook_page_name', '');
			$this->session->set_userdata('facebook_extended_access_token', '');
			$result = true;
		}
		else
		{
			$result = false;
		}
		echo json_encode(array('success'=>$result));
	}

	function update_facebook_page_id()
	{
		$page_id = $this->input->post('page_id');
		$page_name = $this->input->post('page_name');
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_page_id = $page_id;
		$course_info->facebook_page_name = $page_name;

		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_page_id', $page_id);
			$this->session->set_userdata('facebook_page_name', $page_name);
			$result = true;
		}
		else
		{
			$result = false;
		}
		echo json_encode(array('success'=>$result));

	}

	function update_facebook_access_token()
	{
		$access_token = $this->input->post('extended_access_token');
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_extended_access_token = $access_token;

		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_extended_access_token', $access_token);
			$result = true;
		}
		else
		{
			$result = false;
		}
		echo json_encode(array('success'=>$result,'new token'=>$access_token));

	}
	function past_weather($date)
	{
		session_write_close();
		$this->load->model("weather");
		$weather = new Weather();
		$historical = $weather->getWeatherInPast($date);

		echo json_encode(["data" => $historical]);
	}
	function make_5_day_weather()
	{
        session_write_close();
		echo json_encode($this->teesheet->make_5_day_weather());
	}
	function zero_teed_off($teetime_id, $tee_sheet_id = 0)
	{
		echo json_encode(array('teetimes'=> $this->teetime->zero_teed_off($teetime_id, $tee_sheet_id)));
	}
	function zero_turn($teetime_id, $tee_sheet_id = 0)
	{
		echo json_encode(array('teetimes'=>$this->teetime->zero_turn($teetime_id, $tee_sheet_id)));
	}
	function check_in($teetime_id, $count = 0, $tee_sheet_id = 0)
	{
        $message = '';
        $tee_times = $this->teetime->check_in($teetime_id, $count, $tee_sheet_id, $message);
		echo json_encode(array('success'=> count($tee_times) > 0,'teetimes'=>$tee_times, 'message'=>$message));
	}
    function get_stats()
    {
    	$view = $this->input->post('view');
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $day = $this->input->post('day');
        $dow = $this->input->post('dow');
        echo $this->teesheet->get_stats($view, $year, $month, $day, $dow);
    }
    function get_teetime_info($teetime_id)
    {
        $response = array();
        $response['tee_time_info'] = $this->teetime->get_info($teetime_id);
        // Return customer info
        $response['customer_info'] = array();
        if(!empty($response['tee_time_info']->person_id)){
            $this->load->model('v2/customer_model');
            $params['person_id'] = $response['tee_time_info']->person_id;
            $response['customer_info'] = $this->customer_model->get($params);
        }
        // Return time frame info
        $date = date("Y-m-d", strtotime($response['tee_time_info']->start + 1000000));
        $time = date("Hi", strtotime($response['tee_time_info']->start));
        $response['green_fee_timeframe'] = $this->Pricing->get_price_timeframe($response['tee_time_info']->teesheet_id, $response['tee_time_info']->price_class_1, $date, $time, false);
        $response['cart_fee_timeframe'] = $this->Pricing->get_price_timeframe($response['tee_time_info']->teesheet_id, $response['tee_time_info']->price_class_1, $date, $time, true);

        echo json_encode($response);
    }
	function get_teetime_hover_data()
	{
		session_write_close();
		$this->load->model('event');
		$teetime_id = substr($this->input->post('teetime_id'), 0, 20);
		//echo 'trying to get here';
		$data['teetime_info'] = $this->teetime->get_info($teetime_id);
		// Don't return shotgun
		if ($data['teetime_info']->type == 'shotgun')
		{
			echo '';
			return;
		}
		
		$sale_ids = $this->sale->get_sale_ids_by_teetime($teetime_id);
		$data['person_info'] = array();
		if ($data['teetime_info']->person_id != 0)
		{
			$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id);
			if ($data['teetime_info']->person_id_2 != 0)
			{
				$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_2);
				if ($data['teetime_info']->person_id_3 != 0)
				{
					$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_3);
					if ($data['teetime_info']->person_id_4 != 0)
					{
						$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_4);
						if ($data['teetime_info']->person_id_5 != 0)
							$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_5);
					}
				}
			}
		}
		$data['event_people'] = $this->event->get_people($teetime_id);

		//print_r($sale_ids);
		//echo $this->db->last_query();
		echo $this->load->view('teesheets/teetime_info', $data);
		foreach ($sale_ids->result_array() as $result)
		//while ($result = $sale_ids->fetch_assoc())
		{
			$sale_id = $result['sale_id'];
            $data['sale_number'] = $result['number'];
            $data['customer_info'] = array();
            if ($result['customer_id'] > 0)
                $data['customer_info'] = $this->Customer->get_info($result['customer_id']);
			// //echo $result['sale_id'].'<br/>';
			// $sale_info = $this->Sale->get_info($sale_id)->row_array();
			// $this->sale_lib->copy_entire_sale($sale_id);
			// $data['cart']=$this->sale_lib->get_cart();
			$data['cart'] = $this->sale->get_sale_items($sale_id, true)->result_array();
			$data['payments'] = $this->sale->get_sale_payments($sale_id)->result_array();
			//print_r($data);
			// $data['payments']=$this->sale_lib->get_payments();
			$this->load->view("teesheets/teetime_sales_info",$data);
			// $this->sale_lib->clear_all();
			////echo $this->sales->receipt($result['sale_id']);
		}
	}
	/*
	Inserts/updates a teesheet
	*/
	function save($teesheet_id=false)
	{
        $current_tee_sheet_data = $this->teesheet->get_info($teesheet_id);

        $teesheet_data = array(
			'teesheet_id' => $teesheet_id,
			'title' => $this->input->post('name'),
			'holes' => $this->input->post('holes'),
			'increment' => $this->input->post('increment'),
			'frontnine' => $this->input->post('frontnine'),
			'default' => $this->input->post('default'),
			'color' => $this->input->post('teesheet_color'),
			'book_sunrise_sunset' => $this->input->post('book_sunrise_sunset'),
			'sunrise_offset' => $this->input->post('sunrise_offset'),
			'sunset_offset' => $this->input->post('sunset_offset'),
			'online_booking' => $this->input->post('online_booking'),
			'require_credit_card' => $this->input->post('require_credit_card'),
			'send_thank_you' => $this->input->post('send_thank_you'),
			'thank_you_campaign_id' => $this->input->post('thank_you_campaign_id'),
			'days_out' => 0,//$this->input->post('days_out'), // NOT YET PUSHED OUT
			'days_in_booking_window' => $this->input->post('days_in_booking_window'),
			'minimum_players' => $this->input->post('minimum_players'),
			'limit_holes' => $this->input->post('limit_holes'),
			'booking_carts' => $this->input->post('booking_carts'),
            'hide_online_prices' => $this->input->post('hide_online_prices'),
			'online_open_time' => $this->input->post('online_open_time'),
			'online_close_time' => $this->input->post('online_close_time'),
			'online_rate_setting' => $this->input->post('online_rate_setting'),
			'show_on_aggregate' => $this->input->post('show_on_aggregate'),
			'pay_online' => (int) $this->input->post('pay_online'),
			'block_online_booking' => $this->input->post('block_online_booking'),
			'reservation_limit_per_day' => (int) $this->input->post('reservation_limit_per_day'),
			'notify_all_participants' => (int) $this->input->post('notify_all_participants'),
			'self_cancel_only' => (int) $this->input->post('self_cancel_only'),
			'online_booking_new_day_start_time' => $this->input->post('online_booking_new_day_start_time')
		);

        if (!$teesheet_id) {
            $teesheet_data['course_id'] = $this->session->userdata('course_id');
        }
		if ((int)$teesheet_data['increment'] < 5) {
			unset($teesheet_data['increment']);
		}
		
		$this->load->model('online_hours');
		$this->online_hours->save($teesheet_id, $this->input->post('hours'));

		if($this->teesheet->save($teesheet_data, $teesheet_id)){

			if ($this->input->post('increment') && $this->session->userdata('increment') != $this->input->post('increment'))
	            $this->teesheet->adjust_teetimes($teesheet_id);
        	if($teesheet_id==false)
				$teesheet_id = $teesheet_data['teesheet_id'];
			if ($teesheet_data['default'] == 1 && !$this->config->item('stack_tee_sheets'))
				$this->teesheet->switch_tee_sheet($teesheet_id);
			echo json_encode(array('success'=>true,'message'=>lang('teesheets_successful_updating').' '.
			$teesheet_data['title'],'teesheet_id'=>$teesheet_id));

            // Update tee times if needed
            if ((int) $current_tee_sheet_data->book_sunrise_sunset != (int) $teesheet_data['book_sunrise_sunset']) {
                $this->teesheet->realign_tee_times($teesheet_id);
            }
        }
		else
		{

			echo json_encode(array('success'=>false,'message'=>lang('teesheets_error_adding_updating').' '.
			$teesheet_data['teesheet_title'],'teesheet_id'=>-1,'sql'=>$this->db->last_query()));
		}
	}
	/*
	Inserts/updates a teetime
	*/
    function standby_to_teetime($date = false)
    {
        $this->load->model('Increment_adjustment');
        $date = !empty($date) ? $date : substr($this->input->post('date'), 0, 25);
        $standby_id = $this->input->post('id');
        $destination_date = $date;

        $side = $this->input->post('side');
        $destination_start_time = date('YmdHi', strtotime($destination_date))-1000000;
        $month = intval(substr($destination_start_time, 4, 2));

        $this->load->model('teesheet');
        $data = ($this->standby->get_by_id($standby_id));
        if(empty($data)){
	        $standby_list = $this->standby->get_list(date('Y-m-d', strtotime($destination_date)));
	        echo json_encode(array('success'=>false,'standby_change'=>$standby_list, 'message'=>lang('teesheets_teetime_saving_error')));
	        return false;
        }

        if ($this->input->post('tee_sheet_id')) {
            $data['teesheet_id'] = $this->input->post('tee_sheet_id');
        }
        $teesheet_info = $this->teesheet->get_info($data['teesheet_id']);

        $destination_end_time = $this->Increment_adjustment->get_slot_time($destination_start_time + 1000000, $data['teesheet_id'], 1, 1);

        $this->load->model('teetime');
        $this->load->model('item');
        $this->load->library('sale_lib');
        $go_to_register = false;

        $teetime_id = $this->teesheet->generateID('tt');
        $json_ready_events = array();
        $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;


        $existing_teetime_info = $this->teetime->get_info($teetime_id);
        $event_data = array(
            'TTID'=>$teetime_id,
            'teesheet_id'=>$data['teesheet_id'],
            'start'=>$destination_start_time,
            'end'=>$destination_end_time,
            'side'=>$this->input->post('side') ? $this->input->post('side') : 'front',
            'allDay'=>'false',
            'duration' => 1
        );
        $event_data['details'] = 'Standby: ' . $data['details'];
        $event_data['holes'] = !empty($data['holes']) ? $data['holes'] : $teesheet_info->holes;
        $event_data['player_count'] = $data['players'];
        $event_data['paid_player_count'] = 0;
        $event_data['title'] = $data['name'];
        $event_data['type'] = 'teetime';
        $event_data['booking_source'] = 'standby';
        $event_data['booker_id'] = $employee_id;
        $event_data['send_confirmation'] = 1;
        $event_data['person_id'] = $data['person_id'];
        $replaceables = array("'", '"', "\\", "/");
        $posted_name = str_replace($replaceables, '', $this->input->post('teetime_title'));
        $event_data['person_name'] = !empty($posted_name) ? $posted_name : $data['name'];
        $save_response = $this->teetime->save($event_data,$teetime_id, $json_ready_events);
        if($save_response['success'])
        {
                $this->standby->delete($standby_id);
                $standby_list = $this->standby->get_list(date('Y-m-d', strtotime($destination_date)));

                if(!$teetime_id)
				{
		                    echo json_encode(array('standby_change'=>$standby_list, 'success'=>true,'message'=>"", 'teetimes'=>$json_ready_events, 'go_to_register'=>$go_to_register, 'send_confirmation'=>$save_response['send_confirmation'], 'second_time_available'=>$save_response['second_time_available']));
				}
				else
				{
		                    echo json_encode(array('standby_change'=>$standby_list, 'success'=>true,'message'=>"", 'teetimes'=>$json_ready_events, 'go_to_register'=>$go_to_register, 'send_confirmation'=>$save_response['send_confirmation'], 'second_time_available'=>$save_response['second_time_available']));
				}
        }
	    else
	    {
			echo json_encode(array('success'=>false,'message'=>lang('teesheets_teetime_saving_error'), 'teetimes'=>$json_ready_events));
	    }
    }
	function save_teetime($existing_teetime_id=false)
	{
		//log_message('error', "$existing_teetime_id ".now()." Start Save");
				
		$title_array = $person_id_array = $person_name_array = $price_class_array = $teesheet_ids = array();
		$cart_id = 0;

		// GENERATE TEE TIME ID
		if (!$existing_teetime_id || $existing_teetime_id == 'blank_time') {
            $teetime_id = $this->teesheet->generateID('tt');
            $new_tee_time = 1;
        }
		else {
            $teetime_id = $existing_teetime_id;
            $new_tee_time = 0;
        }

		// LOAD LIBRARIES AND MODELS
		$this->load->library('name_parser');
		$this->load->model('item');
		$this->load->model('event');
        $this->load->model('Increment_adjustment');
        $this->load->library('sale_lib');

		$go_to_register = false;
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		$existing_teetime_info = $this->teetime->get_info($teetime_id);

		$type = $new_tee_time ? 'pending_reservation' : $this->input->post('event_type');
		$split_teetimes = $type == 'teetime' ? $this->input->post('split_teetimes') : 0;
		$replaceables = array("'", '"', "\\", "/");
        $tee_sheet_id = ($this->input->post('new_teesheet')?$this->input->post('new_teesheet'):($this->input->post('teetime_teesheet_id') ? $this->input->post('teetime_teesheet_id') : $this->session->userdata('teesheet_id')));
        $tee_sheet_changed = ($existing_teetime_info->teesheet_id != $tee_sheet_id);

		// CREATE THE PRIMARY EVENT (TEE TIME, EVENT, ETC)
		$event_data = array(
			'TTID'=>$teetime_id,
			'teesheet_id'=>$tee_sheet_id,
			'start'=>($this->input->post('start')<1000?$existing_teetime_info->start:$this->input->post('start')),
            'end'=>($this->input->post('end')<1000?$existing_teetime_info->end:$this->input->post('end')),
			'side'=>$this->input->post('side'),
			'allDay'=>'false',
            'new_tee_time'=>$new_tee_time
		);
        if ($this->input->post('new_date')) {
            $event_data['start'] = date('Ymd', strtotime($this->input->post('new_date'))) - 100 . substr($event_data['start'], 8);
            $event_data['end'] = date('Ymd', strtotime($this->input->post('new_date'))) - 100 . substr($event_data['end'], 8);
        }

        if ($this->input->post('duration')) {
            $event_data['duration'] = $this->input->post('duration');
		}
		
		$increment_multiple = 1;
		
		$start = strtotime($event_data['start']+1000000);
		$end = strtotime($event_data['end']+1000000);
		$minute_difference = round(abs($start - $end) / 60, 0);
		$teesheet_info = $this->teesheet->get_info($event_data['teesheet_id']);
        if ($teesheet_info->holes == 9) {
            $event_data['side'] = 'front';
        }
		if (((string)$type == 'shotgun' || $existing_teetime_info->type == 'shotgun') && $minute_difference < $teesheet_info->increment * 2) {
			$increment_multiple = $this->input->post('shotgun_holes') ? $this->input->post('shotgun_holes') : $existing_teetime_info->holes;
			$date = date('Y-m-d H:i:s', strtotime($event_data['start']+1000000));
			$event_data['end'] = date('YmdHi', strtotime($date." + ".floor($increment_multiple * $teesheet_info->increment)." minutes")) - 1000000;
		}
		else if ($existing_teetime_info->title == '' && ($this->input->post('event_players') > 5 || $this->input->post('players') > 5) && ($minute_difference < $teesheet_info->increment * 2 || $event_data['duration'] < 2)) {
			$players = $this->input->post('event_type') != 'teetime' ? $this->input->post('event_players') : $this->input->post('players');
			$increment_multiple = ceil($players/4);
			$date = date('Y-m-d H:i:s', strtotime($event_data['start']+1000000));
			$event_data['end'] = date('YmdHi', strtotime($date." + ".floor($increment_multiple * $teesheet_info->increment)." minutes")) - 1000000;
            $event_data['duration'] = $increment_multiple;
        }

		if (!$existing_teetime_id || $existing_teetime_id == -1 || $existing_teetime_id == 'blank_time')
			$event_data['booking_source'] = 'POS';
			
                //log_message('error', "Start time: " . $this->input->post('start'));
		if ($this->config->item('simulator') && $this->input->post('teetime_time'))
		{
			$event_data['end'] = $event_data['start'] + $this->input->post('teetime_time');
			if ($event_data['end'] % 100 > 59)
                        $event_data['end'] += 40;
		}
		if ($type)
			$event_data['type'] = $type;

		$teetime_customers = array();
        for($x = 1; $x <= 5; $x++){
			$key = 'person_id';
			if($x > 1){
				$key .= '_'.$x;
			}
			if($this->input->post($key)){
				$teetime_customers[$x-1] = array('person_id'=>(int) $this->input->post($key), 'position'=>$x);
			}
		}

		// ADD DATA FOR A BLOCK EVENT TYPE
		if ($type == 'closed') {
			$event_data['title'] = str_replace($replaceables, '', $this->input->post('closed_title'));
			$event_data['details'] = $this->input->post('closed_details');
			$event_data['holes'] = 9;
		}
		// ADD DATA FOR A TOURNAMENT, EVENT, OR LEAGUE EVENT TYPE
		else if ($type == 'tournament' || $type == 'league' || $type == 'event' || $type == 'shotgun') {
			if ($type == 'shotgun') {
				$event_data['type'] = 'shotgun';
				$event_data['title'] = $this->input->post('shotgun_title') ? str_replace($replaceables, '', $this->input->post('shotgun_title')) : $existing_teetime_info->title;
				$event_data['details'] = $this->input->post('shotgun_details') ? $this->input->post('shotgun_details') : $existing_teetime_info->details;
				$event_data['holes'] = $this->input->post('shotgun_holes') ? $this->input->post('shotgun_holes') : $existing_teetime_info->holes;
				$event_data['player_count'] = 0;// $this->input->post('shotgun_players');
				$event_data['carts'] = $this->input->post('shotgun_carts') ? $this->input->post('shotgun_carts') : $existing_teetime_info->carts;
				//$event_data['paid_carts'] = $this->input->post('event_carts') ? $this->input->post('event_carts') : $existing_teetime_info->paid_carts;
				$event_data['default_cart_fee'] = $this->input->post('sg_default_price_category') ? $this->input->post('sg_default_cart_fee') : $existing_teetime_info->default_cart_fee;
				$event_data['default_price_category'] = $this->input->post('sg_default_price_category') ? str_replace('price_category_', '', $this->input->post('sg_default_price_category')) : $existing_teetime_info->default_price_category;
                $event_data['use_event_rate'] = $this->input->post('sg_use_event_rate');
                $total_checkin = $this->input->post('checkin_txtbox');
                $event_data['status'] = 'checked in';
			}
			else {
				$event_data['title'] = str_replace($replaceables, '', $this->input->post('event_title'));
				$event_data['details'] = $this->input->post('event_details');
				$event_data['holes'] = $this->input->post('event_holes');
				$event_data['player_count'] = $this->input->post('event_players');
				$event_data['carts'] = $this->input->post('event_carts');
				$event_data['person_id'] = $this->input->post('event_people_0');
				$event_data['person_id_2'] = $this->input->post('event_people_1');
				$event_data['person_id_3'] = $this->input->post('event_people_2');
				$event_data['person_id_4'] = $this->input->post('event_people_3');
				$event_data['person_id_5'] = $this->input->post('event_people_4');
				$event_data['promo_id'] = $this->input->post('promo_id');
				//$event_data['paid_carts'] = $this->input->post('event_carts');
				$event_data['default_cart_fee'] = $this->input->post('default_cart_fee');
                $event_data['default_price_category'] = str_replace('price_category_', '', $this->input->post('default_price_category'));
                $event_data['use_event_rate'] = $this->input->post('use_event_rate');
                $event_data['status'] = 'checked in';
			}

			// AUTOMATICALLY ADD TEE TIMES TO SHOPPING CART
			if ($this->input->post('purchase_quantity') || isset($total_checkin)) {
				$go_to_register = true;
				//  THESE ARE PRICE CLASSES ASSIGNED ON THE TEE TIME
				$price_class_1 = $this->input->post('price_class_1') ? $this->input->post('price_class_1') : '';
				$price_class_2 = $this->input->post('price_class_2') ? $this->input->post('price_class_2') : '';
				$price_class_3 = $this->input->post('price_class_3') ? $this->input->post('price_class_3') : '';
				$price_class_4 = $this->input->post('price_class_4') ? $this->input->post('price_class_4') : '';
				$price_class_5 = $this->input->post('price_class_5') ? $this->input->post('price_class_5') : '';
				// THESE ARE THE PRICE CLASSES ASSIGNED TO EACH CUSTOMER IN THE TEE TIME
				$ppc_1 = (isset($person_info->price_class) && $person_info->price_class !== 0)?$person_info->price_class:'';
				$ppc_2 = (isset($person_info_2->price_class) && $person_info_2->price_class !== 0)?$person_info_2->price_class:'';
				$ppc_3 = (isset($person_info_3->price_class) && $person_info_3->price_class !== 0)?$person_info_3->price_class:'';
				$ppc_4 = (isset($person_info_4->price_class) && $person_info_4->price_class !== 0)?$person_info_4->price_class:'';
				$ppc_5 = (isset($person_info_5->price_class) && $person_info_5->price_class !== 0)?$person_info_5->price_class:'';
				// THE TEE TIME PRICE CLASSES TAKE PRECEDENCE
				$price_class_1 = $price_class_1 ? $price_class_1 : $ppc_1;
				$price_class_2 = $price_class_2 ? $price_class_2 : $ppc_2;
				$price_class_3 = $price_class_3 ? $price_class_3 : $ppc_3;
				$price_class_4 = $price_class_4 ? $price_class_4 : $ppc_4;
				$price_class_5 = $price_class_5 ? $price_class_5 : $ppc_5;
				if ($type == 'shotgun') {
					$person_price_class_array = array();
					$teetime_customers = array();
					$shotgun_players = $this->input->post('shotgun_players');

					$ind = 0;
					if($this->input->post('checkin_box',[])){
						foreach($this->input->post('checkin_box',[]) as $checkin_position) {
							$coords = explode('_', $checkin_position);
							if (!empty($shotgun_players[$coords[0]][$coords[1]]['person_id'])) {
								$person_info = $this->Customer->get_info($shotgun_players[$coords[0]][$coords[1]]['person_id'], false, true);
								$teetime_customers[$ind] = array('person_id'=>(int) $shotgun_players[$coords[0]][$coords[1]]['person_id']);
								$person_price_class_array[$ind] = $person_info->price_class;

								$ind++;
							}
						}
					}
					if($ind > $total_checkin){
						$total_checkin  = $ind;
					}

				}
				else {
					$person_price_class_array = array(
						0 => $price_class_1,
						1 => $price_class_2,
						2 => $price_class_3,
						3 => $price_class_4,
						4 => $price_class_5
					);
				}
                log_debug_message($this->config->item('course_id'), 'adding green fee to cart'.$teetime_id, $this->session->userdata('tracker_sale_id'));
				$this->session->set_userdata(
					array(
						'cart'=>array(),
						'basket'=>array(),
						'customer'=>!empty($event_data['person_id']) ? $event_data['person_id'] : '',
						'customer_quickbuttons'=>NULL,
						'purchase_quantity'=>$this->input->post('purchase_quantity'), 
						'shotgun_checkin_slots' => $type == 'shotgun' ? $this->input->post('checkin_box') : array(),
						'teetime'=>$teetime_id
					)
				);
				if(!empty($total_checkin)){
					$cart_id = $this->add_green_fees_to_cart($teetime_id, $person_price_class_array, $teetime_customers, $this->input->post('teetime_teesheet_id'),$event_data,$total_checkin);
				} else {
					$cart_id = $this->add_green_fees_to_cart($teetime_id, $person_price_class_array, $teetime_customers, $this->input->post('teetime_teesheet_id'),$event_data);
				}

				// If using new Backbone POS
				if($this->session->userdata('sales_v2') == '1'){
					
					$this->load->model('v2/Cart_model');
					foreach($teetime_customers as $c_ind => $customer) {
						if (!empty($customer['person_id']) && $customer['person_id'] > 0) {
							$this->Cart_model->save_customer($customer['person_id'], array('selected' => !$c_ind ? true : false));
						}
					}

				}else{
					if (!empty($event_data['person_id']) && $event_data['person_id'] > 0) {
						$this->sale_lib->set_customer_quickbutton($event_data['person_id'], $event_data['person_name']);
					}
					if (!empty($event_data['person_id_2']) && $event_data['person_id_2'] > 0) {
						$this->sale_lib->set_customer_quickbutton($event_data['person_id_2'], $event_data['person_name_2']);
					}
					if (!empty($event_data['person_id_3']) && $event_data['person_id_3'] > 0) {
						$this->sale_lib->set_customer_quickbutton($event_data['person_id_3'], $event_data['person_name_3']);
					}
					if (!empty($event_data['person_id_4']) && $event_data['person_id_4'] > 0) {
						$this->sale_lib->set_customer_quickbutton($event_data['person_id_4'], $event_data['person_name_4']);
					}
					if (!empty($event_data['person_id_5']) && $event_data['person_id_5'] > 0) {
						$this->sale_lib->set_customer_quickbutton($event_data['person_id_5'], $event_data['person_name_5']);
					}
				}
			}
			// IF WE'RE JUST CHECKING IN, THEN ADD CHECKIN DATA
			else if ($this->input->post('checkin')) {
				$this->teetime->check_in($teetime_id, $this->input->post('checkin'));
				$event_data['status'] = 'checked in';
			}
		}

		// ADD DATA FOR A TEE TIME EVENT TYPE
		else if ($type == 'teetime') {
			$person_data = $person_data_2 = $person_data_3 = $person_data_4 = $person_data_5 = array();
			// SAVE CUSTOMERS (UP TO 5)
			if ($this->input->post('save_customer_1') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data['first_name'] = $np->getFirstName();
					$person_data['last_name'] = $np->getLastName();
					$person_data['phone_number'] = $this->input->post('phone');
					$person_data['email'] = $this->input->post('email');
					$person_data['zip'] = $this->input->post('zip');
					$person_id = ($this->input->post('person_id') == '')?false:$this->input->post('person_id');
                    $customer_data = array('course_id'=>$this->session->userdata('course_id'));

                    $this->Customer->save($person_data, $customer_data, $person_id);
				}
			}
			if ($this->input->post('save_customer_2') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_2')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_2['first_name'] = $np->getFirstName();
					$person_data_2['last_name'] = $np->getLastName();
					$person_data_2['phone_number'] = $this->input->post('phone_2');
					$person_data_2['email'] = $this->input->post('email_2');
					$person_data_2['zip'] = $this->input->post('zip_2');
					$person_id = ($this->input->post('person_id_2') == '')?false:$this->input->post('person_id_2');
                    $customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_2, $customer_data, $person_id);
				}
			}
			if ($this->input->post('save_customer_3') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_3')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_3['first_name'] = $np->getFirstName();
					$person_data_3['last_name'] = $np->getLastName();
					$person_data_3['phone_number'] = $this->input->post('phone_3');
					$person_data_3['email'] = $this->input->post('email_3');
					$person_data_3['zip'] = $this->input->post('zip_3');
					$person_id = ($this->input->post('person_id_3') == '')?false:$this->input->post('person_id_3');
                    $customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_3, $customer_data, $person_id);
				}
			}
			if ($this->input->post('save_customer_4') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_4')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_4['first_name'] = $np->getFirstName();
					$person_data_4['last_name'] = $np->getLastName();
					$person_data_4['phone_number'] = $this->input->post('phone_4');
					$person_data_4['email'] = $this->input->post('email_4');
					$person_data_4['zip'] = $this->input->post('zip_4');
					$person_id = ($this->input->post('person_id_4') == '')?false:$this->input->post('person_id_4');
                    $customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_4, $customer_data, $person_id);
				}
			}
			if ($this->input->post('save_customer_5') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_5')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_5['first_name'] = $np->getFirstName();
					$person_data_5['last_name'] = $np->getLastName();
					$person_data_5['phone_number'] = $this->input->post('phone_5');
					$person_data_5['email'] = $this->input->post('email_5');
					$person_data_5['zip'] = $this->input->post('zip_5');
					$person_id = ($this->input->post('person_id_5') == '')?false:$this->input->post('person_id_5');
                    $customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_5, $customer_data, $person_id);
				}
			}
			// ADD ALL TEE TIME DATA
			$event_data['cart_num_1'] = $this->input->post('assigned_carts_1') ? preg_replace("/[^0-9a-zA-Z ]/", "", $this->input->post('assigned_carts_1')) : '';
			$event_data['cart_num_2'] = $this->input->post('assigned_carts_2') ? preg_replace("/[^0-9a-zA-Z ]/", "", $this->input->post('assigned_carts_2')) : '';
			$event_data['details'] = $this->input->post('teetime_details') ? $this->input->post('teetime_details') : '';
			$event_data['holes'] = $this->input->post('teetime_holes');
			$event_data['player_count'] = ($this->input->post('players')?$this->input->post('players'):$this->input->post('current_player_count'));
            $event_data['player_count'] = $event_data['player_count'] > 0 ? $event_data['player_count'] : 1;
			$event_data['carts'] = $this->input->post('carts');
			$event_data['person_id'] = (isset($person_data['person_id']))?$person_data['person_id']:$this->input->post('person_id');
			$event_data['price_class_1'] = str_replace('price_category_', '', $this->input->post('price_class_1'));
			$event_data['person_name'] = str_replace($replaceables, '', $this->input->post('teetime_title'));
			$event_data['person_id_2'] = (isset($person_data_2['person_id']))?$person_data_2['person_id']:$this->input->post('person_id_2');
			$event_data['price_class_2'] = str_replace('price_category_', '', $this->input->post('price_class_2'));
			$event_data['person_name_2'] = str_replace($replaceables, '', $this->input->post('teetime_title_2'));
			$event_data['person_id_3'] = (isset($person_data_3['person_id']))?$person_data_3['person_id']:$this->input->post('person_id_3');
			$event_data['price_class_3'] = str_replace('price_category_', '', $this->input->post('price_class_3'));
			$event_data['person_name_3'] = str_replace($replaceables, '', $this->input->post('teetime_title_3'));
			$event_data['person_id_4'] = (isset($person_data_4['person_id']))?$person_data_4['person_id']:$this->input->post('person_id_4');
			$event_data['price_class_4'] = str_replace('price_category_', '', $this->input->post('price_class_4'));
			$event_data['person_name_4'] = str_replace($replaceables, '', $this->input->post('teetime_title_4'));
			$event_data['person_id_5'] = (isset($person_data_5['person_id']))?$person_data_5['person_id']:$this->input->post('person_id_5');
			$event_data['price_class_5'] = str_replace('price_category_', '', $this->input->post('price_class_5'));
			$event_data['person_name_5'] = str_replace($replaceables, '', $this->input->post('teetime_title_5'));
			$event_data['teed_off_time'] = ($this->input->post('teed_off_time')?date('Y-m-d', strtotime($existing_teetime_info->teed_off_time)).' '.date('H:i:00', strtotime($this->input->post('teed_off_time'))):$existing_teetime_info->teed_off_time);
			$event_data['turn_time'] = ($this->input->post('turn_time')?date('Y-m-d', strtotime($existing_teetime_info->turn_time)).' '.date('H:i:00', strtotime($this->input->post('turn_time'))):$existing_teetime_info->turn_time);
			$event_data['send_confirmation'] = $this->input->post('send_confirmation_emails') ? '1' : '2';
			$teetime_title = str_replace($replaceables, '', $this->input->post('teetime_title'));
			$event_data['credit_card_id'] = $this->input->post('credit_card_id');
			$event_data['promo_id'] = $this->input->post('promo_id');

			// CREATE TITLE FOR TEE TIME FROM CUSTOMER NAMES
			if ($event_data['person_id'] != 0) {
				if ($event_data['credit_card_id'] != 0) {
					$this->load->model("Customer_credit_card");
					$card_data = array('customer_id' => $event_data['person_id']);
					$this->Customer_credit_card->save($card_data, $event_data['credit_card_id']);
				}
				$person_info = $this->Customer->get_info($event_data['person_id'], false, true);
				$title_array[1] = ($person_info->last_name != '') ? $person_info->last_name : $person_info->first_name;
				$person_id_array[1] = $event_data['person_id'];
				$person_name_array[1] = $event_data['person_name'];
				$price_class_array[1] = $event_data['price_class_1'];
			} else if ($event_data['person_name'] != '')
				$title_array[1] = $person_name_array[1] = $event_data['person_name'];
			if ($event_data['person_id_2'] != 0) {
				$person_info_2 = $this->Customer->get_info($event_data['person_id_2'], false, true);
				$title_array[2] = ($person_info_2->last_name != '') ? $person_info_2->last_name : $person_info_2->first_name;
				$person_id_array[2] = $event_data['person_id_2'];
				$person_name_array[2] = $event_data['person_name_2'];
				$price_class_array[2] = $event_data['price_class_2'];
			} else if ($event_data['person_name_2'] != '')
				$title_array[2] = $person_name_array[2] = $event_data['person_name_2'];
			if ($event_data['person_id_3'] != 0) {
				$person_info_3 = $this->Customer->get_info($event_data['person_id_3'], false, true);
				$title_array[3] = (($person_info_3->last_name != '') ? $person_info_3->last_name : $person_info_3->first_name);
				$person_id_array[3] = $event_data['person_id_3'];
				$person_name_array[3] = $event_data['person_name_3'];
				$price_class_array[3] = $event_data['price_class_3'];
			} else if ($event_data['person_name_3'] != '')
				$title_array[3] = $person_name_array[3] = $event_data['person_name_3'];
			if ($event_data['person_id_4'] != 0) {
				$person_info_4 = $this->Customer->get_info($event_data['person_id_4'], false, true);
				$title_array[4] = (($person_info_4->last_name != '') ? $person_info_4->last_name : $person_info_4->first_name);
				$person_id_array[4] = $event_data['person_id_4'];
				$person_name_array[4] = $event_data['person_name_4'];
				$price_class_array[4] = $event_data['price_class_4'];
			} else if ($event_data['person_name_4'] != '')
				$title_array[4] = $person_name_array[4] = $event_data['person_name_4'];
			if ($event_data['person_id_5'] != 0) {
				$person_info_5 = $this->Customer->get_info($event_data['person_id_5'], false, true);
				$title_array[5] = (($person_info_5->last_name != '') ? $person_info_5->last_name : $person_info_5->first_name);
				$person_id_array[5] = $event_data['person_id_5'];
				$person_name_array[5] = $event_data['person_name_5'];
				$price_class_array[5] = $event_data['price_class_5'];
			} else if ($event_data['person_name_5'] != '')
				$title_array[5] = $person_name_array[5] = $event_data['person_name_5'];

			$teetime_title = count($title_array) > 0 ? implode(' - ', $title_array):$teetime_title;
			$event_data['title'] = $teetime_title;
			// END MAKE TITLE FROM CUSTOMER NAMES

			$event_data['booker_id'] = $employee_id;

			// AUTOMATICALLY ADD TEE TIMES TO SHOPPING CART
			if ($this->input->post('purchase_quantity')) {
				$go_to_register = true;
				//  THESE ARE PRICE CLASSES ASSIGNED ON THE TEE TIME
				$price_class_1 = $this->input->post('price_class_1') ? $this->input->post('price_class_1') : '';
				$price_class_2 = $this->input->post('price_class_2') ? $this->input->post('price_class_2') : '';
				$price_class_3 = $this->input->post('price_class_3') ? $this->input->post('price_class_3') : '';
				$price_class_4 = $this->input->post('price_class_4') ? $this->input->post('price_class_4') : '';
				$price_class_5 = $this->input->post('price_class_5') ? $this->input->post('price_class_5') : '';
				// THESE ARE THE PRICE CLASSES ASSIGNED TO EACH CUSTOMER IN THE TEE TIME
				$ppc_1 = (isset($person_info->price_class) && $person_info->price_class !== 0)?$person_info->price_class:'';
				$ppc_2 = (isset($person_info_2->price_class) && $person_info_2->price_class !== 0)?$person_info_2->price_class:'';
				$ppc_3 = (isset($person_info_3->price_class) && $person_info_3->price_class !== 0)?$person_info_3->price_class:'';
				$ppc_4 = (isset($person_info_4->price_class) && $person_info_4->price_class !== 0)?$person_info_4->price_class:'';
				$ppc_5 = (isset($person_info_5->price_class) && $person_info_5->price_class !== 0)?$person_info_5->price_class:'';
				// THE TEE TIME PRICE CLASSES TAKE PRECEDENCE
				$price_class_1 = $price_class_1 ? $price_class_1 : $ppc_1;
				$price_class_2 = $price_class_2 ? $price_class_2 : $ppc_2;
				$price_class_3 = $price_class_3 ? $price_class_3 : $ppc_3;
				$price_class_4 = $price_class_4 ? $price_class_4 : $ppc_4;
				$price_class_5 = $price_class_5 ? $price_class_5 : $ppc_5;
				$person_price_class_array = array(
					0=>$price_class_1,
					1=>$price_class_2,
					2=>$price_class_3,
					3=>$price_class_4,
					4=>$price_class_5
				);
                $default_price_class = $person_price_class_array[0];
                $default_teetime_customer = 0;
                if (!empty($teetime_customers[0])) {
                    $default_teetime_customer = $teetime_customers[0];
                }
                if (!$this->input->post('tee_time_person_select') && $this->input->post('expand_players')) {
                    unset($person_price_class_array[0]);
                    unset($teetime_customers[0]);
                }
                if (!$this->input->post('tee_time_person_select_2')) {
                    unset($person_price_class_array[1]);
                    unset($teetime_customers[1]);
                }
                if (!$this->input->post('tee_time_person_select_3')) {
                    unset($person_price_class_array[2]);
                    unset($teetime_customers[2]);
                }
                if (!$this->input->post('tee_time_person_select_4')) {
                    unset($person_price_class_array[3]);
                    unset($teetime_customers[3]);
                }
                if (!$this->input->post('tee_time_person_select_5')) {
                    unset($person_price_class_array[4]);
                    unset($teetime_customers[4]);
                }
                $person_price_class_array = array_values($person_price_class_array);
                $teetime_customers = array_values($teetime_customers);
                $quantity = $this->input->post('purchase_quantity');
                
                // Apply person 1 pricing to each tee time in the cart that does not have a player tied to it 
                // (if course has setting enabled)
                if($this->config->item('teetime_default_to_player1') == 1){
					for($i = 0; $i < $quantity; $i++) {
						if (empty($person_price_class_array[$i])) {
							$person_price_class_array[$i] = $default_price_class;
						}
						if (empty($teetime_customers[$i])) {
							$teetime_customers[$i] = $default_teetime_customer;
						}
					}               	
                }

                log_debug_message($this->config->item('course_id'), 'adding green fee to cart'.$teetime_id, $this->session->userdata('tracker_sale_id'));
				$this->session->set_userdata(
					array(
						'cart'=>array(),
						'basket'=>array(),
						'customer'=>$event_data['person_id'],
						'customer_quickbuttons'=>NULL,
						'purchase_quantity'=>$this->input->post('purchase_quantity'), 
						'teetime'=>$teetime_id
					)
				);

	    		$cart_id = $this->add_green_fees_to_cart($teetime_id, $person_price_class_array, $teetime_customers, $this->input->post('teetime_teesheet_id'),$event_data);
				
				// If using new Backbone POS
				if($this->session->userdata('sales_v2') == '1'){
					
					$this->load->model('v2/Cart_model');
					
					$first = true;
					for($x = 1; $x <= 5; $x++){
						$person_key = 'person_id_'.$x;
						$checkin_key = 'tee_time_person_select_'.$x;
						if($x == 1){
							$person_key = 'person_id';
							$checkin_key = 'tee_time_person_select';
						}

						if(!empty($event_data[$person_key]) && $event_data[$person_key] > 0 && $this->input->post($checkin_key)){
							$this->Cart_model->save_customer($event_data[$person_key], array('selected' => $first));
							$first = false;
						}					
					}

				}else{
					if ($event_data['person_id'] > 0) {
						$this->sale_lib->set_customer_quickbutton($event_data['person_id'], $event_data['person_name']);
					}
					if ($event_data['person_id_2'] > 0) {
						$this->sale_lib->set_customer_quickbutton($event_data['person_id_2'], $event_data['person_name_2']);
					}
					if ($event_data['person_id_3'] > 0) {
						$this->sale_lib->set_customer_quickbutton($event_data['person_id_3'], $event_data['person_name_3']);
					}
					if ($event_data['person_id_4'] > 0) {
						$this->sale_lib->set_customer_quickbutton($event_data['person_id_4'], $event_data['person_name_4']);
					}
					if ($event_data['person_id_5'] > 0) {
						$this->sale_lib->set_customer_quickbutton($event_data['person_id_5'], $event_data['person_name_5']);
					}
				}		
			}
			// IF WE'RE JUST CHECKING IN, THEN ADD CHECKIN DATA
			else if ($this->input->post('checkin')) {

				$selected_people_to_checkin = [
					false,
					false,
					false,
					false,
					false
				];
				if ($this->input->post('tee_time_person_select') && $this->input->post('expand_players')) {
					$selected_people_to_checkin [0] = true;
				}
				if ($this->input->post('tee_time_person_select_2')) {
					$selected_people_to_checkin [1] = true;
				}
				if ($this->input->post('tee_time_person_select_3')) {
					$selected_people_to_checkin [2] = true;
				}
				if ($this->input->post('tee_time_person_select_4')) {
					$selected_people_to_checkin [3] = true;
				}
				if ($this->input->post('tee_time_person_select_5')) {
					$selected_people_to_checkin [4] = true;
				}

				$message = "";
				$this->teetime->check_in($teetime_id, $this->input->post('checkin'),false,$message,$selected_people_to_checkin);
				$event_data['status'] = 'checked in';

			}
		}
		$teetime_helper = new \fu\Teetime();
		$players = $teetime_helper->parseTeetimePlayers($event_data);
		if(empty($players) && ($existing_teetime_id != 'blank_time' && !empty($existing_teetime_id))){
			$acl = new \fu\acl\Acl();
			$acl->attempt(new \fu\acl\Permissions\Teetimes\Anonymous(["create"]));
		}
        $json_ready_events = array();

        $delete_teetime = $this->input->post('delete_teetime');
		if ($delete_teetime > 0){
            if($existing_teetime_info->paid_player_count > 0){
				$acl = new \fu\acl\Acl();
				$acl->attempt(new \fu\acl\Permissions\Teetimes\Checkedin(["delete"]));
			}

			$event_data['status'] = 'deleted';
            $event_data['canceller_id'] = $this->session->userdata('person_id');
            $event_data['date_cancelled'] = date('Y-m-d H:i:s');
            $event_data['type'] = $type.'-emp deleted';
            // Save a log of the tee time being deleted
			$this->event->delete_people($teetime_id);

            if ($delete_teetime == 2) {
                // Delete all future linked tee times
                $json_events = $this->teetime->delete_linked_events($existing_teetime_info->event_link_id, $existing_teetime_info->start);
            }
            else if ($delete_teetime == 3) {
                // Delete all linked tee tiems
                $json_events = $this->teetime->delete_linked_events($existing_teetime_info->event_link_id);
            }
            else {
                $json_events = array();
            }
            $json_ready_events = array_merge($json_ready_events, $json_events);
        }


		// SPLIT THE TEE TIME
        if ($split_teetimes && !$this->input->post('purchase_quantity') && $this->input->post('players') > 1)
		{
			$cur_teetime_info = (array)$this->teetime->get_info($teetime_id);
			$cur_teetime_info['status'] = 'deleted';
            $cur_teetime_info['canceller_id'] = $this->session->userdata('person_id');
            $cur_teetime_info['date_cancelled'] = date('Y-m-d H:i:s');
            $cur_teetime_info['type'] = $type.' - split';
            $save_response = $this->teetime->save($cur_teetime_info,$teetime_id, $json_events);
			$json_ready_events = array_merge($json_ready_events, $json_events);

			$json_events = array();
			$carts = $event_data['carts'];
			$paid = $cur_teetime_info['paid_player_count'];
			$paid_carts = $cur_teetime_info['paid_carts'];
			//foreach($title_array as $index => $title)
			$player_count = ($event_data['player_count'] > count($title_array))?$event_data['player_count']:count($title_array);
			for ($i = 1; $i <= $player_count; $i++)
			{
                $current_slot = floor(($i-1)/4);
                if ($cur_teetime_info['duration'] != 1 && $current_slot + 1 > $cur_teetime_info['duration'] || $player_count < 6) {
                    $current_slot = 0;
                }
                $event_data['start'] = $this->Increment_adjustment->get_slot_time($cur_teetime_info['start'] + 1000000, $event_data['teesheet_id'],  $current_slot);
                $event_data['end'] = $this->Increment_adjustment->get_slot_time($cur_teetime_info['start'] + 1000000, $event_data['teesheet_id'], $current_slot + 1);
                $event_data['duration'] = 1;
				$event_data['person_id_2'] = $event_data['person_id_3'] = $event_data['person_id_4'] = $event_data['person_id_5'] = 0;
				$event_data['person_name_2'] = $event_data['person_name_3'] = $event_data['person_name_4'] = $event_data['person_name_5'] = '';
				$event_data['price_class_2'] = $event_data['price_class_3'] = $event_data['price_class_4'] = $event_data['price_class_5'] = '';
				$event_data['person_id'] = isset($person_id_array[$i]) ? $person_id_array[$i] : 0;
				$event_data['person_name'] = isset($person_name_array[$i]) ? $person_name_array[$i] : '';
				$event_data['price_class_1'] = !isset($price_class_array[$i])  || $price_class_array[$i] == null ? '' : $price_class_array[$i];
				$event_data['title'] = (isset($title) && $title != '')? $title : $event_data['person_name'];
                if ($event_data['title'] == '')
				{
					$event_data['title'] = $person_name_array[1];
					$event_data['person_id'] = ($person_id_array[1])?$person_id_array[1]:0;
					$event_data['person_name'] = $person_name_array[1];
					$event_data['price_class_1'] = $price_class_array[1] == null ? '' : $price_class_array[1];
				}
				else if (!$event_data['person_id'])
					$event_data['person_id'] = 0;
                //$event_data['title'] = ($title_array[$index] != '') ? $title_array[$index]:($title_array[0]);
                $event_data['player_count'] = 1;
				$event_data['carts'] = $carts > 0 ? 1:0;
				$event_data['paid_player_count'] = $paid > 0 ? 1:0;
				$event_data['paid_carts'] = $paid_carts > 0 ? 1:0;
				$event_data['split'] = true;
				$teetime_id = $this->teesheet->generateID('tt');
				$event_data['TTID'] = $teetime_id;
                $save_response = $this->teetime->save($event_data,$teetime_id, $json_events);
				$teesheet_ids[] = $save_response['send_confirmation'];
				$sql_array[] = $this->db->last_query();
				$carts--;
				$paid--;
				$paid_carts--;
			}
			$json_ready_events = array_merge($json_ready_events, $json_events);
		}
		// SAVE TEE TIME WITHOUT SPLITTING
		else {
        	$save_response = $this->teetime->save($event_data,$teetime_id, $json_ready_events);
			if(!$new_tee_time){
				if($teesheet_info->notify_all_participants == 1 && $this->has_teetime_changed($existing_teetime_info,$event_data)){
					if(empty($save_response['send_confirmation'])){
						$this->teetime->send_change_email($teetime_id);
					}

				}
			}
		}


		foreach($json_ready_events as $key => $teetime){
			if(isset($teesheet_info->teesheet_id) && $teetime['teesheet_id'] != $existing_teetime_info->teesheet_id){
				$teetime['teesheet_id'] = $existing_teetime_info->teesheet_id;
				$teetime['status'] = "deleted";
				$json_ready_events[] = $teetime;
			}
		}





		$message = '';
		if ($this->session->userdata('teesheet_id') != $this->session->userdata('default_teesheet_id') && $existing_teetime_id)// && $this->session->userdata('teesheet_reminder_count') < 3)
		{
			$message = lang('teesheets_not_default_teesheet');
			$this->session->set_userdata('teesheet_reminder_count', $this->session->userdata('teesheet_reminder_count')+1);
		}
		if($save_response['success'])
		{
            foreach(array_keys($json_ready_events) as $keys)
            {
                foreach(array_keys($json_ready_events[$keys]) as $new_data)
                log_message('error', 'JSON DATA: ' . $new_data . ' ' . $json_ready_events[$keys][$new_data]);
            }
			if(!isset($cart_id)){
				$cart_id = "";
			}

                    //New teetime
			if(!$teetime_id)
			{
                $response = array('success'=>true,'message'=>$message, 'teetimes'=>$json_ready_events, 'go_to_register'=>$go_to_register, 'send_confirmation'=>$save_response['send_confirmation'], 'email_confirmation'=>$teesheet_ids, 'second_time_available'=>$save_response['second_time_available'], 'cart_id' => $cart_id, 'tee_time_id' => $teetime_id);
                if ($new_tee_time) {
                    return $response;
                }
                else {
                    echo json_encode($response);
                }
			}
			else //previous teetime
			{
                $response = array('success'=>true,'message'=>$message, 'teetimes'=>$json_ready_events, 'go_to_register'=>$go_to_register, 'send_confirmation'=>$save_response['send_confirmation'], 'email_confirmation'=>$teesheet_ids, 'second_time_available'=>$save_response['second_time_available'], 'cart_id' => $cart_id, 'tee_time_id' => $teetime_id);
                if ($new_tee_time) {
                    return $response;
                }
                else {
                    echo json_encode($response);
                }
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('teesheets_teetime_saving_error'), 'teetimes'=>$json_ready_events));
		}
	}
	function save_simple($teetime_id = false)
	{
		$this->load->library('name_parser');
		$this->load->model('item');
		$this->load->model('teesheet');

		$new_tee_time = 0;
		if (!$teetime_id) {
			$teetime_id = $this->teesheet->generateID('tt');
			$new_tee_time = 1;
		}


		//Start
		$start = $this->input->post('start');
		$start = \Carbon\Carbon::parse($start);
		$startTeetime = \fu\Teetime::convertDatetimeToTeetime($start);



		$event_data = array(
			'TTID' => $teetime_id,
			'teesheet_id' => $this->input->post('teesheet_id'),
			'start' => $startTeetime,
			'end' => $startTeetime,
			'side' => $this->input->post('side',"front"),
			'type' => $this->input->post('event_type',"teetime"),
			'details' => $this->input->post('teetime_details',"Details yo"),
			'holes' => $this->input->post('teetime_holes',18),
			'player_count' => $this->input->post('players',3),
			'carts' => $this->input->post('carts',3),
			'title' => $this->input->post('teetime_title',"Title"),
			'duration'=>1,
			'booking_source' => $this->input->post('booking_source',"api"),
			'allDay' => 'false'
		);

		$this->load->model('Increment_adjustment');
		$time = $this->Increment_adjustment->get_slot_time($start->toDateTimeString(), $event_data['teesheet_id'],  0, true);

		if($time!=$startTeetime){
			echo json_encode(array('success' => false,"msg"=>"Invalid timeslot"));
			return "";
		}

		if ($this->input->post('delete_teetime') == 1)
			$event_data['status'] = 'deleted';

		$json_ready_events = [];
		$results = $this->teetime->save($event_data, $teetime_id, $json_ready_events,true);
		if ($results && $results['success']) {
			echo json_encode(array('success' => true,"data"=>["teetime_id"=>$results['teetime_id']]));
		} else
		{
			echo json_encode(array('success' => false));
		}
	}
	function has_teetime_changed($existing_teetime,$new_teetime)
	{
		if(
		(isset($new_teetime['status']) && $existing_teetime->status != $new_teetime['status']) ||
		(isset($new_teetime['title']) && $existing_teetime->title != $new_teetime['title']) ||
		(isset($new_teetime['start']) && $existing_teetime->start != $new_teetime['start'])

		){
			if($existing_teetime->player_count > 0)
				return true;
		}

		return false;
	}

    function copy_event() {
        $event_id = $this->input->post('id');
        $this->session->set_userdata('copied_event_id', $event_id);
    }

    function paste_event() {
        $this->load->model('event');
        $this->load->model('Increment_adjustment');
        $event_id = $this->session->userdata('copied_event_id');
        if (!$event_id) {
            echo json_encode(array('success'=>false,'message'=>'No event has been copied'));
            return;
        }

        // Get time and side information
        $new_start = $this->input->post('start');
        $new_side = $this->input->post('side');
        $new_tee_sheet_id = $this->input->post('tee_sheet_id');
        // Fetch tee time information
        $event_data = $this->teetime->get_info($event_id);
        if ($event_data->teesheet_id != $new_tee_sheet_id) {
            echo json_encode(array('success' => false, 'message' => 'Error: You can only paste onto the tee sheet of origin'));
            return;
        }
        // Adjust end time
        if ($event_data->duration) {
            $new_end = $this->Increment_adjustment->get_slot_time((int)$new_start + 1000000, $event_data->teesheet_id, $event_data->duration, 1);
        }
        else {
            $start_min = strtotime($event_data->start);
            $end_min = strtotime($event_data->end);
            $min_diff = round(abs($end_min - $start_min) / 60, 0);
            $new_end = date('YmdHi', strtotime(($new_start + 1000000).' +'.$min_diff.' minutes')) - 1000000;
        }

        // Set new values
        $event_data->start = $new_start;
        $event_data->end = $new_end;
        $event_data->side = $new_side;
        $event_data->paid_player_count = 0;
        $event_data->person_paid_1 = 0;
        $event_data->person_paid_2 = 0;
        $event_data->person_paid_3 = 0;
        $event_data->person_paid_4 = 0;
        $event_data->person_paid_5 = 0;
        $event_data->date_booked = gmdate('Y-m-d H:i:s');
        $event_data->confirmation_emailed = 0;
        $event_data->thank_you_emailed = 0;
        $event_data->booking_source = 'POS';
        $event_data->booker_id = $this->session->userdata('person_id');
        $event_data->teed_off_time = '0000-00-00 00:00:00';
        $event_data->turn_time = '0000-00-00 00:00:00';
        $event_data->finish_time = '0000-00-00 00:00:00';
        $event_data->paste = true;

        // Save new tee time
        $json_ready_events = array();
        $event_data = (array)$event_data;
        $response = $this->teetime->save($event_data, false, $json_ready_events);
        $new_tee_time_id = $response['teetime_id'];

        // Save event people to the new event

        $event_people = $this->event->get_people($event_id);
        if (count($event_people) > 0) {
            $people_array = array();
            foreach($event_people as $person){
                $people_array[] = array('person_id' => $person['person_id'], 'label'=> $person['label']);
            }
            $this->event->save_people($new_tee_time_id, $people_array);
        }

        if($response['success']) {
            echo json_encode(array('success' => true, 'teetimes' => $json_ready_events));
        }
        else {
            echo json_encode(array('success' => false, 'message' => 'Paste failed'));
        }
    }

	function shotgun_person_checkin($teetime_id){
		$this->load->model('event');
		$hole = (int) $this->input->post('hole');
		$player_position = (int) $this->input->post('player_position');
		$checked_in = (int) $this->input->post('checked_in');
		$success = $this->event->shotgun_person_checkin($teetime_id, $hole, $player_position, $checked_in);

		echo json_encode(array('success'=>$success));
	}

	function shotgun_person_pay($teetime_id){
		$this->load->model('event');
		$hole = (int) $this->input->post('hole');
		$player_position = (int) $this->input->post('player_position');
		$paid = (int) $this->input->post('paid');
		$success = $this->event->shotgun_person_pay($teetime_id, $hole, $player_position, $paid);

		echo json_encode(array('success'=>$success));
	}

	function event_checkin($teetime_id){
		$this->load->model('event');
		$person_id = (int) $this->input->post('person_id');
		if(!$person_id){
			$person_id = (int) $this->input->post('event_person_id');
		}
        $message = '';
		$success = $this->event->person_checkin($teetime_id, $person_id, true, $message);

		echo json_encode(array('success'=>$success, 'message'=>$message));
	}

	function event_pay($teetime_id, &$message = ''){
        $person_id = (int) $this->input->post('person_id');
        $event_person_id = (int) $this->input->post('event_person_id');
        $person_name = $this->input->post('person_name');
		$price_class = $this->input->post('price_class');
        $use_event_rate = $this->input->post('use_event_rate');
		$customer_info = $this->Customer->get_info($person_id, false, true);
		if ($customer_info->price_class > 1 && !$use_event_rate) {
			$price_class = $customer_info->price_class;
		}
		$price_classes = array();
		if(!empty($price_class)){
			$price_classes[] = $price_class;
		}

		if(empty($person_id) && empty($event_person_id)){
			echo json_encode(array('success'=>false));
			return false;
		}

        if ($this->config->item('current_day_checkins_only')) {
            // Get tee time
            $tee_time_info = $this->teetime->get_info($teetime_id);
            $tt_date = date('Y-m-d', strtotime($tee_time_info->start + 1000000));
            $todays_date = date('Y-m-d');
            if ($tt_date != $todays_date) {
                // If it doesn't belong to today, then prevent action
                $message = "You can only check in players on today's date.";
                echo json_encode(array('success'=>false, 'message' => $message));
                return false;
            }
        }

		$this->session->set_userdata(
			array(
				'cart'=>array(),
				'basket'=>array(),
				'customer'=>$person_id,
                'event_person_id'=>$event_person_id,
				'customer_quickbuttons'=>NULL,
				'purchase_quantity'=>$this->input->post('purchase_quantity'), 
				'teetime'=>$teetime_id
			)
		);
		$cart_id = $this->add_green_fees_to_cart($teetime_id, $price_classes, [[
			"person_id"=>$person_id,
			"position"=>1
		]],$this->input->post('teetime_teesheet_id'));
		
		if($this->session->userdata('sales_v2') == '1'){
            if(!empty($person_id)) {
                $this->load->model('Cart_model');
                $this->Cart_model->save_customer($person_id, array('selected' => true));
            }
		}else{
			$this->sale_lib->set_customer_quickbutton($person_id, $person_name);
		}

		echo json_encode(array('success'=>true, 'cart_id' => $cart_id));
	}

	function save_event_person($teetime_id = null){
		$this->load->model('event');
		$person_id = (int) $this->input->post('person_id');
        $label = $this->input->post('label');
        $result = $this->event->save_person($teetime_id, $person_id, $label);
		echo json_encode(array('success'=>$result['success'],'event_person_id'=>$result['event_person_id']));
	}
	
	function save_shotgun_person($teetime_id = null){
		$this->load->model('event');
		$person_info = array();
		$person_info['teetime_id'] = $teetime_id;
		$person_info['person_id'] = (int) $this->input->post('person_id');
        $person_info['label'] = $this->input->post('label');
		$person_info['hole'] = $this->input->post('hole');
		$person_info['player_position'] = $this->input->post('player_position');
		
		$success = $this->event->save_shotgun_person($person_info);

		echo json_encode(array('success'=>$success));
	}

    function view_print_cart($teetime_id) {
        $this->load->model('event');
        // Collect all event data
        $data = array();
        $data['teetime_info']=$this->teetime->get_info($teetime_id);
        $data['preview'] = 1;
        $event_people = $this->event->get_people($teetime_id);
        foreach ($event_people as $event_person) {
            $data['event_people'][$event_person['hole']][$event_person['player_position']] = $event_person;
        }
        $data['a_b'] = 'both';

        $this->load->view('teesheets/cart_sign_form', $data);
    }

    function cart_sign_pdf ($teetime_id) {
        $show_course_name = $this->input->post('show_course_name');
        $show_event_name = $this->input->post('show_event_name');
        $show_a_b = $this->input->post('show_a_b');

        $this->load->model('event');
        // Collect all event data
        $data = array();
        $data['teetime_info']=$this->teetime->get_info($teetime_id);
        $event_people = $this->event->get_people($teetime_id);
        foreach ($event_people as $event_person) {
            $data['event_people'][$event_person['hole']][$event_person['player_position']] = $event_person;
        }
        $data['a_b'] = $show_a_b;
        $data['show_event_name'] = $show_event_name;
        $data['show_course_name'] = $show_course_name;

        $snappy = new  \Knp\Snappy\Pdf();
        $snappy->setBinary("application/bin/wkhtmltopdf");
        $snappy->setOption("viewport-size",'1366x1024');
        $snappy->setOption("zoom",'.8');

        $html = $this->load->view('teesheets/cart_sign_preview', $data, true);
        header('Content-Type: application/pdf');
        $cart_sign_name = 'Cart Signs for - '.$data['teetime_info']->title.' - '.date('Y-m-d g:ia', strtotime($data['teetime_info']->start + 1000000));
        header("Content-Disposition: attachment; filename='$cart_sign_name.pdf'");
        echo $snappy->getOutputFromHtml($html);
    }
	function save_customer($customer_id=-1){
		$person_data = array(
		'first_name'=>trim($this->input->post('first_name')),
		'last_name'=>trim($this->input->post('last_name')),
		'email'=>$this->input->post('email'),
		'phone_number'=>$this->input->post('phone_number'),
		'cell_phone_number'=>$this->input->post('cell_phone_number'),
		'birthday'=>$this->input->post('birthday'),
		'address_1'=>$this->input->post('address_1'),
		'address_2'=>$this->input->post('address_2'),
		'city'=>$this->input->post('city'),
		'state'=>$this->input->post('state'),
		'zip'=>$this->input->post('zip'),
		'country'=>$this->input->post('country'),
		'comments'=>$this->input->post('comments')
		);
		$customer_data=array();
		$giftcard_data=array();
		$groups_data=array();
		$passes_data=array();
		
		$new_customer = 0;
		if ($customer_id == -1) {
			$new_customer = 1;
			$customer_data['course_id']=$this->session->userdata('course_id');
		}
		if($this->Customer->save($person_data,$customer_data,$customer_id, $giftcard_data, $groups_data, $passes_data))
		{
			//New customer
			if($customer_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_adding').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_data['person_id']));
				$customer_id = $customer_data['person_id'];
			}
			else //previous customer
			{
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_updating').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_adding_updating').' '.
			$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));
		}
	}
	function delete_shotgun_person($teetime_id = null){
		$this->load->model('event');
		$person_info = array();
		$person_info['teetime_id'] = $teetime_id;
		$person_info['hole'] = $this->input->post('hole');
		$person_info['player_position'] = $this->input->post('player_position');
		
		$success = $this->event->delete_shotgun_person($person_info);

		echo json_encode(array('success'=>$success));
	}
	
	function delete_event_person($teetime_id = null){
		$this->load->model('event');
		$event_person_id = (int) $this->input->post('event_person_id');
		$success = $this->event->delete_person($teetime_id, $event_person_id);

		echo json_encode(array('success'=>$success));
	}


	function customer_search($type='', $add_option = false)
	{
		session_write_close();
        $term = $this->input->get('term');
		
        if($this->config->item('limit_fee_dropdown_by_customer') == 1){
        	$this->Customer->include_valid_price_classes = true;
        }
		$suggestions = $this->Customer->get_customer_search_suggestions($term,100,$type);
		
		if ($add_option && !is_numeric(substr($term, 0, 1)))
		{
			$replaceables = array("'", '"', "\\", "/");
			// Parse name
			$this->load->library('name_parser');
			$np = new Name_parser();
			$np->setFullName(str_replace($replaceables, '', $this->input->get('term')));
			$np->parse();
			if (!$np->notParseable()) {
				$first_name = $np->getFirstName();
				$last_name = $np->getLastName();
			}
			// Add option to add to database at the beginning
            $suggestions = array_merge(
				array(
					array('label' => "Add to Customers ($last_name, $first_name)", 'add_customer'=>1, 'first_name'=>$first_name, 'last_name'=>$last_name)
					//, array('label' => "Add to Customers ($last_name, $first_name)", 'save_label'=>1, 'first_name'=>$first_name, 'last_name'=>$last_name)
				), 
				$suggestions);
		}
		echo json_encode($suggestions);
	}
	function view_customer_quick_add($last_name = '', $first_name = '', $slot = '')
	{
		$data = array();
		$data['quick_add'] = true;
		$data['controller'] = strtolower(get_class());
		$data['slot'] = $slot;
		$data['person_info'] = (object) array();
		$data['person_info']->first_name = $first_name;
		$data['person_info']->last_name = $last_name;
		
		$this->load->view('customers/form', $data);
	}
	function customer_quick_add() 
	{
		$this->load->library('name_parser');
		$np = new Name_parser();
		$np->setFullName(str_replace($replaceables, '', $this->input->post('name')));
		$np->parse();
		if (!$np->notParseable()) {
			$person_data['first_name'] = $np->getFirstName();
			$person_data['last_name'] = $np->getLastName();
			$person_data['phone_number'] = $this->input->post('phone');
			$person_data['email'] = $this->input->post('email');
			$person_data['zip'] = $this->input->post('zip');
			$person_id = ($this->input->post('person_id') == '')?false:$this->input->post('person_id');
			$customer_data = array('course_id'=>$this->session->userdata('course_id'));
			$this->Customer->save($person_data, $customer_data, $person_id);
		}
	}
	
	function customer_and_group_search($type='')
	{
		$suggestions[]=array('value'=> 0, 'label' => 'Everyone ', 'is_group'=>1);

	    //$everyone_count = $this->Customer->count_all(true);
	    $groups = $this->Customer->get_group_info('', $this->input->get('term'));
		//$suggestions[]=array('value'=> 0, 'label' => $this->db->last_query());
		foreach ($groups as $group)
		{
	    	$suggestions[]=array('value'=> $group['group_id'], 'label' => $group['label'], 'is_group'=>1);
		}
		
		$suggestions = array_merge($suggestions, $this->Customer->get_customer_search_suggestions($this->input->get('term'), 100, $type));
		echo json_encode($suggestions);
	}
	function get_group_members($group_id)
	{
		$customers = $this->Customer->get_all(10000, 0, $group_id, true);
		$results = array();
		foreach($customers->result_array() as $customer)
		{
			$results[] = array('last_name'=>$customer['last_name'], 'name'=>$customer['last_name'].', '.$customer['first_name'], 'person_id'=>$customer['person_id']);
		}
		echo json_encode(array('customers'=>$results));
	}
	function teetime_search()
	{
		$customer_id = $this->input->post('customer_id');
		//echo json_encode(array('search_results'=>$customer_id));
		//return;
		$search_results = $this->teetime->search($customer_id);
		echo json_encode(array('search_results'=>$search_results));
	}
	function view_note()
	{
		$note_data = array();
		$this->load->view('employees/form_message', $note_data);
	}
	function save_note()
	{
		$note_data = array(
			'course_id'=>$this->session->userdata('course_id'),
			'author'=>$this->session->userdata('person_id'),
			'recipient'=>$this->input->post('recipient_id') ? $this->input->post('recipient_id') : null,
			'date'=>date('Y-m-d H:i:s'),
			'message'=>$this->input->post('message')
		);
		$this->load->model('note');
		$this->note->save($note_data);

		$notes = $this->note->get_all()->result_array();
		echo json_encode(array('note_html' => $this->load->view('employees/notes', array('notes'=>$notes), true)));
	}
	function get_notes($limit = 5)
	{
		session_write_close();
		$this->load->model('note');
		$notes = $this->note->get_all($limit)->result_array();
		//echo $this->db->last_query();
		echo json_encode(array('note_count'=>count($notes), 'note_html' => $this->load->view('employees/notes', array('notes'=>$notes), true)));
	}
	function delete_note($note_id)
	{
		$this->load->model('note');
		echo json_encode('success', $this->note->delete($note_id));
	}
	function view_standby()
	{
        $this->load->model('Increment_adjustment');
        $tee_sheet_id = $this->input->get('tee_sheet_id') ? $this->input->get('tee_sheet_id') : $this->session->userdata('teesheet_id');
		$data = array();
	    $data['no_delete'] = $this->input->get('id') ? FALSE : TRUE;
	    //$this->load->helper('sale_helper');
	    $data['standby_name'] = $this->input->get('name');
	    $data['standby_id'] = $this->input->get('id');
        $data['my_standby_time'] = $this->input->get('time');

	    //log_message('error', 'standby time: ' . $data['my_standby_time']);
	    $data['standby_holes'] = $this->input->get('holes');
	    $data['standby_players'] = $this->input->get('players');
	    $data['standby_details'] = $this->input->get('details');
	    //log_message('error', 'Details ' . $standby_details);
	    $data['standby_email'] = $this->input->get('email');
	    $data['standby_phone'] = $this->input->get('phone');
        $data['teesheet_info'] = $this->teesheet->get_info($tee_sheet_id);
        $tee_sheets = $this->teesheet->get_all(100, 0, 1)->result_array();
        $data['teesheets'] = array();
        foreach ($tee_sheets as $tee_sheet) {
            $data['teesheets'][$tee_sheet['teesheet_id']] = $tee_sheet['title'];
        }
        $data['teesheet_id'] = $tee_sheet_id;
        $data['person_id'] = $this->input->get('person_id');

        $date = date('Y-m-d');
        $teetimes = $this->Increment_adjustment->get_increment_array($date, $tee_sheet_id);

        $data['teetimes'] = $teetimes;
        $this->load->view('teetimes/standby', $data);
	}
        function teetime_standby_get_list($date = false)
        {
	        session_write_close();
            $date = $date ? $date : date('Y-m-d H:i:s');
            $standby_list = $this->standby->get_list($date);
            echo json_encode($standby_list);
        }
        function teetime_save_standby()
        {
            $time = $this->input->post('standby_time');
            $holes = $this->input->post('teetime_holes');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $name = $this->input->post('teetime_title');
            $num_players = $this->input->post('players');
            $date = $this->input->post('standby_date');
            $tee_sheet_date = $this->input->post('tee_sheet_date');
            $details = $this->input->post('standby_details');
            $person_id = $this->input->post('person_id');
            $tee_sheet_id = $this->input->post('teesheet_id');
            $person_data = array();
            if ($this->input->post('save_customer_1') == 'on') {
                $this->load->library('name_parser');
                $replaceables = array("'", '"', "\\", "/");
                $np = new Name_parser();
                $np->setFullName(str_replace($replaceables, '', $name));
                $np->parse();
                if (!$np->notParseable()) {
                    $person_data['first_name'] = $np->getFirstName();
                    $person_data['last_name'] = $np->getLastName();
                    $person_data['phone_number'] = $this->input->post('phone');
                    $person_data['email'] = $this->input->post('email');
                    $person_id = ($this->input->post('person_id') == '')?false:$this->input->post('person_id');
                    $customer_data = array('course_id'=>$this->session->userdata('course_id'));
                    $this->Customer->save($person_data, $customer_data, $person_id);
                    $person_id = $person_data['person_id'];
                }
            }
            $this->standby->save($name, $email, $phone, $holes, $num_players, $time, $details, $person_id, $date, $tee_sheet_id);
            $this->teetime_standby_get_list($tee_sheet_date);
        }
        function teetime_edit_standby($array = array())
        {
            $time = $this->input->post('standby_time');
            if ($time < 1000) {
                $time = '0'.$time;
            }
            $holes = $this->input->post('teetime_holes');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $name = $this->input->post('teetime_title');
            $num_players = $this->input->post('players');
            $date = $this->input->post('standby_date');
            $tee_sheet_date = $this->input->post('tee_sheet_date');
            $details = $this->input->post('standby_details');
            $id = $this->input->post('id');
            $delete = $this->input->post('delete');
            $person_id = $this->input->post('person_id');
            $tee_sheet_id = $this->input->post('teesheet_id');
            if ($delete === 'false')
            {
                ($this->standby->edit($name, $email, $phone, $holes, $num_players, $time, $details, $id, $person_id, $date, $tee_sheet_id));
                $add_standby_to_tee_sheet = (int)$this->input->post('add_standby_to_tee_sheet');
                if ($add_standby_to_tee_sheet) {
                    $standby_time = (int)$this->input->post('standby_time');
                    $h_z = $standby_time < 1000 ? '0' : '';
                    $date = date('Y-m-d H:i:s', strtotime($this->input->post('standby_date').' '.$h_z.$standby_time));
                    $this->standby_to_teetime($date);
                    return;
                }
                $this->teetime_standby_get_list($tee_sheet_date);

            }
            else
            {
                ($this->standby->delete($id));
                $this->teetime_standby_get_list($tee_sheet_date);
            }

        }

	function teesheet_updates($course_id, $teetimes = 'stu ff')
	{
		session_write_close();
		$refresh_rate =  (string) $this->config->item('teesheet_refresh_rate');
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

		function sendMsg($id, $msg, $refresh_rate) {
		  echo "id: $id" . PHP_EOL;
		  echo "retry: {$refresh_rate}000" . PHP_EOL;
		  echo "data: $msg" . PHP_EOL;
		  echo PHP_EOL;
		  ob_flush();
		  flush();
		}


		//if(isset($_GET['user']) && isset($_GET['msg'])){
		//	$fp = fopen($course_id."log.txt", 'a');
		//    fwrite($fp, "<div class='msgln'><b>stuff</b>: other stuff<br></div>");
		//    fclose($fp);
		//}

		//if(file_exists($course_id."log.txt") && filesize($course_id."log.txt") > 0){
		//    $handle = fopen($course_id."log.txt", "r");
		//    $contents = fread($handle, filesize($course_id."log.txt"));
		//    fclose($handle);

		//deleting file when it get bigger
		//	if(filesize($course_id."log.txt")>1100){
		//		@unlink($course_id."log.txt");
		//	}
		//}
		$contents = 'stuff2';
		$return_teetimes = array();
		$return_teetimes = $this->getJSONTeeTimes2();
		sendMsg(time(),$return_teetimes, $refresh_rate);
	}
	/*
	 * fetch_all_data is used by local_teesheet.js to grab all the data needed to populate the local database
	 */
	function fetch_all_data()
	{
		$teetimes = $this->teesheet->getTeeTimes((date('Ymd', strtotime('-1 week'))-100).'0000')->result_array();
		$teesheets = $this->teesheet->get_all()->result_array();

		echo json_encode(array('teetimes'=>$teetimes, 'teesheets'=>$teesheets));

	}
	function switch_teetime_sides($teetime_id)
	{
		$this->teetime->switch_sides($teetime_id);
	}
    function split_by_time($teetime_id)
    {
        $events = $this->teetime->split_by_time($teetime_id);
        echo json_encode(array('teetimes'=>$events));
    }
	function mark_teetime_teedoff($teetime_id, $tee_sheet_id = 0)
	{
		$events = $this->teetime->mark_as_teedoff($teetime_id, $tee_sheet_id);
		echo json_encode(array('teetimes'=>$events));
	}
	function mark_teetime_turned($teetime_id, $tee_sheet_id = 0)
	{
		$events = $this->teetime->mark_as_turned($teetime_id, $tee_sheet_id);
		echo json_encode(array('teetimes'=>$events));
	}
	function mark_teetime_finished($teetime_id, $tee_sheet_id = 0)
	{
		$events = $this->teetime->mark_as_finished($teetime_id, $tee_sheet_id);
		echo json_encode(array('teetimes'=>$events));
	}
	function view_move($teetime_id, $side)
	{
        $this->load->model('Increment_adjustment');
		$data = array('teetime_id'=>$teetime_id, 'side'=>$side);
		$data['teetime_info'] = $this->teetime->get_info($teetime_id);
		$teesheet_info = $this->teesheet->get_info($this->session->userdata('teesheet_id'));
        $this->teesheet->set_teesheet_info($teesheet_info);
        $date = date('Y-m-d', strtotime($data['teetime_info']->start + 1000000));
        $data['teetimes'] = $this->Increment_adjustment->get_increment_array($date, $teesheet_info->teesheet_id);
        $data['teesheets'] = $this->teesheet->get_teesheet_array();
        $data['difference'] = (strtotime($data['teetime_info']->end) - strtotime($data['teetime_info']->start))/60;
        $data['duration'] = $data['teetime_info']->duration;
        $data['frontnine'] = $teesheet_info->frontnine;

        $this->load->view('teetimes/move', $data);
	}
	function get_teetime_dropdown()
	{
        $this->load->model('Increment_adjustment');
		$teesheet_id = $this->input->post('teesheet_id');
        $standby = $this->input->post('standby');
		$teesheet_info = $this->teesheet->get_info($teesheet_id);

        $frontnine = $teesheet_info->frontnine;
		$dif = fmod((floor($frontnine/100)*60+$frontnine%100), $teesheet_info->increment);
		$frontnine += $teesheet_info->increment - $dif;

        $date = $this->input->post('new_date');
        $teetimes = $this->Increment_adjustment->get_increment_array($date, $teesheet_info->teesheet_id);

		echo json_encode(
			array(
				'teetime_dropdown' => form_dropdown($standby ? 'standby_time' : 'new_time', $teetimes, ''),
				'difference_input' => form_hidden('difference', $teesheet_info->increment),
				'frontnine_input' => form_hidden('frontnine', $frontnine)
			)
		);
	}
	function view_repeat($teetime_id)
	{
		$data = array('teetime_id'=>$teetime_id);
		$data['teetime_info'] = $this->teetime->get_info($teetime_id);
		$this->load->view('teetimes/repeat', $data);
	}
	function move($teetime_id, $player_count)
	{
		$new_date = $this->input->post('new_date');
		$new_time = $this->input->post('new_time');
		/*
		 * Added new_teesheet parameter, but still need to adjust the difference value...
		 * We'll use the same functionality that save uses to adjust the 'difference' value to normalize if for the new teesheet. Once we've adjusted the difference, we should be done.
		 */
		$teesheet_id = $this->input->post('new_teesheet');
		$difference = $this->input->post('difference');
		$frontnine = $this->input->post('frontnine');
		$time_1 = ($new_time+$difference < 1000)?'0'.($new_time+$difference):$new_time+$difference;
		$time_2 = ($new_time+$frontnine < 1000)?'0'.($new_time+$frontnine):$new_time+$frontnine;
		$time_3 = ($new_time+$frontnine+$difference < 1000)?'0'.($new_time+$frontnine+$difference):$new_time+$frontnine+$difference;
		$date_string = date('YmdHi', strtotime($new_date.' '.$new_time)) - 1000000;
		$end_date_string = date('YmdHi', strtotime($new_date.' '.$time_1)) - 1000000;
		$back_date_string = date('YmdHi', strtotime($new_date.' '.$time_2)) - 1000000;
		$back_end_date_string = date('YmdHi', strtotime($new_date.' '.$time_3)) - 1000000;
		$available = $this->teetime->check_availability(array('start'=>$date_string,'player_count'=>$player_count, 'teesheet_id'=>$teesheet_id));
		if (strtotime($new_date) === false)
			echo json_encode(array('success'=>false, 'message'=>'Not a valid date', 'date_strings'=>array($date_string,$end_date_string,$back_date_string,$back_end_date_string)));
		else if (!$available)
			echo json_encode(array('success'=>false, 'message'=>'Teetime', 'date_strings'=>array($date_string,$end_date_string,$back_date_string,$back_end_date_string), 'frontnine'=>$frontnine));
		else
		{
			$this->teetime->change_date($teetime_id, $date_string, $end_date_string, $back_date_string, $back_end_date_string, $teesheet_id);
			echo json_encode(array('success'=>true, 'message'=>'Successfully changed date', 'sql'=>$this->db->last_query(), 'date_strings'=>array($date_string,$end_date_string,$back_date_string,$back_end_date_string),'teesheet_id'=>$teesheet_id, 'available'=>$available));
		}
	}
	function repeat($teetime_id)
	{
		session_write_close();
        $this->load->model('Increment_adjustment');
		$this->load->model('event');
		$repeats = $this->input->post('repeats');
		$sunday = $this->input->post('sunday');
		$monday = $this->input->post('monday');
		$tuesday = $this->input->post('tuesday');
		$wednesday = $this->input->post('wednesday');
		$thursday = $this->input->post('thursday');
		$friday = $this->input->post('friday');
		$saturday = $this->input->post('saturday');
		$start_date = date('Ymd', strtotime($this->input->post('start_date')));
		$end_date = ($repeats != 'once')?date('Ymd', strtotime($this->input->post('end_date'))):date('Ymd', strtotime($this->input->post('start_date')));

		$teetime_info = $this->teetime->get_info($teetime_id);
		$event_data = (array)$teetime_info;
		$event_data['paid_player_count'] = 0;
		$event_data['paid_carts'] = 0;
		$event_data['status'] = '';
		$event_data['date_booked'] = gmdate('Y-m-d H:i:s');

        if (empty($teetime_info->event_link_id)) {
            $event_data['event_link_id'] = $teetime_id;
            $this->teetime->simple_save(array('event_link_id' => $teetime_id), array($teetime_id, $teetime_id.'b'));
        }
        else {
            $event_data['event_link_id'] = $teetime_info->event_link_id;
        }

		unset($event_data['TTID'], $event_data['tee_sheet_title']);
		$teetime_event_start = date('Ymd', strtotime($teetime_info->start+1000000));
		$start_time = date('Hi', strtotime($teetime_info->start));
		$end_time = date('Hi', strtotime($teetime_info->end));

        if ((int)$end_time < (int)$start_time) {
            echo json_encode(array('success'=>false, 'message'=>'Error repeating this event, start/end mismatch'));
            return;
        }

        $event_date = date('Ymd', strtotime(($teetime_info->start+1000000)." +1 day"));
		$event_date = ($start_date > $event_date)?$start_date:$event_date;
		$json_ready_events = array();

		$event_people = $this->event->get_people($teetime_id);

		while ($event_date <= $end_date)
		{
            $response = array('success' => false);
            $event_data['start'] = $this->Increment_adjustment->get_slot_time(($event_date.$start_time), $event_data['teesheet_id']);
            if ($event_data['duration']) {
                $event_data['end'] = $this->Increment_adjustment->get_slot_time(($event_date.$start_time), $event_data['teesheet_id'],  $event_data['duration'], 1);
            }
            else {
                $event_data['end'] = ($event_date.$end_time) - 1000000;
            }
			//$event_data['start'] = ($event_date.$start_time) - 1000000;
            if ($repeats == 'once' || $repeats == 'daily')
			{
				$response = $this->teetime->save($event_data, false, $json_ready_events);
			}
			else if ($repeats == 'weekly')
			{
				$dow = date('w',strtotime($event_date));
				if (($sunday && $dow == 0) ||
					($monday && $dow == 1) ||
					($tuesday && $dow == 2) ||
					($wednesday && $dow == 3) ||
					($thursday && $dow == 4) ||
					($friday && $dow == 5) ||
					($saturday && $dow == 6))
                        $response = $this->teetime->save($event_data,false, $json_ready_events);
			}
			else if ($repeats == 'bi_weekly')
			{
				$tt_time = strtotime($teetime_event_start);
				$event_date_time = strtotime($event_date);
				$diff = $event_date_time - $tt_time;
				if ($diff%(60*60*24*14) == 0)
                    $response = $this->teetime->save($event_data,false, $json_ready_events);
			}
            if($response['success'] && !empty($event_people)){
                $people_array = array();
                foreach($event_people as $person){
                    $people_array[] = array('person_id' => $person['person_id'], 'label'=> $person['label']);
                }
                $this->event->save_people($response['teetime_id'], $people_array);
            }
			$event_date = date('Ymd', strtotime("$event_date +1 day"));
		}

		echo json_encode(array('success'=>true, 'repeats'=>$repeats, 'sunday'=>$sunday, 'monday'=>$monday, 'end_date'=>$end_date, 'teetime_info'=>$teetime_info, 'teetimes'=>$json_ready_events));
	}

	public function create_cart_from_teetime($teetime_id)
	{
		$body = file_get_contents('php://input');
		$body = json_decode($body,true);
		if(!isset($body['positions'])){
			echo json_encode(array('success'=>false,'message'=>'No positions were specified'));
			return false;
		}

		$person_price_classes = [];
		$teesheet_id = [];
		$person_ids = [];
		$quantity = count($body['positions']);

		$this->load->model("Teetime");
		$tee_time_info = $this->teetime->get_info($teetime_id);

		$teetime_lib = new \fu\Teetime();
		$teetime_lib->setOnlyCustomers(false);
		$players = $teetime_lib->parseTeetimePlayers($tee_time_info);
		foreach($players as $position => $player)
		{
			if(!in_array($player->getPosition(),$body['positions']))
				continue;
			$person_price_classes[] = $player->getPriceClass();
			$person_ids[] = [
				"person_id"=>$player->getPersonId(),
				"position"=>$player->getPosition()
			];
		}


		$cart_id = $this->add_green_fee_seasonal_pricing($teetime_id,$person_price_classes,$person_ids,$quantity,$teesheet_id);

		print json_encode(["cart_id"=>$cart_id]);
		return true;
	}


	function add_green_fees_to_cart ($teetime_id, $person_price_class_array = array(), $person_id_array = array(), $teesheet_id = false,$event_info = null,$quantity = null)
	{
		$this->load->model('Pricing');
		$this->load->model('Price_class');
		$this->load->model('Special');
		$this->load->model('v2/Cart_model');
		$this->load->model('Pass');

		$holes = $this->input->post('teetime_holes');
		if(empty($quantity)){
			$quantity = $this->input->post('purchase_quantity');
		}

		$promo_id = $this->input->post('promo_id');
        $start_time = (int)substr($this->input->post('start'),8);
		$adjusted_start_time = $this->input->post('start') + 1000000;
        $dow = date('w',strtotime($adjusted_start_time));
		$carts = $this->input->post('carts') - $this->input->post('paid_carts');
		$event_type = $this->input->post('event_type');
		$cart_fee = ($event_type == 'shotgun' ? $this->input->post('sg_default_cart_fee') : $this->input->post('default_cart_fee'));

        $cart_id = false;

		if(($event_type == 'tournament' || $event_type == 'league' || $event_type == 'event') && ($cart_fee)){
			$carts = $quantity;
		}
		if(!$teesheet_id){
			$teesheet_id = $this->session->userdata('teesheet_id');
		}
		
		$course_id = $this->session->userdata('course_id');

        if($this->config->item('seasonal_pricing') == 1)
		{
			$cart_id = $this->add_green_fee_seasonal_pricing($teetime_id,$person_price_class_array,$person_id_array,$quantity,$event_info);
		}
		else if ($this->config->item('simulator'))
		{
			$cart_id = $this->process_simulator();
		}
		else
		{
			$cart_id = $this->process_old_pricing($person_price_class_array, $teesheet_id, $start_time, $dow, $holes, $quantity, $course_id, $carts);
		}

		// If a promo was attached to the tee time, add coupon to cart
		if(!empty($promo_id)){
			
			// Sales V2
			if($this->session->userdata('sales_v2') == 1){
				$this->Cart_model->save_payment([
					'type' => 'coupon',
					'description' => 'Coupon',
					'record_id' => $promo_id,
					'amount' => 0.00
				]);	
			
			// Sales V1
			}else{
				$cart_items = $this->sale_lib->get_cart();
				
				$this->load->model('promotion');
				$promo = (array) $this->promotion->get_coupon_data($promo_id);
				$promo_discount = $this->promotion->get_numeric_discount($cart_items, 'category', $promo);
				
				if(is_numeric($promo_discount)){
					$this->sale_lib->add_payment('Coupon', $promo_discount, $promo_discount, false, false, false, $promo_id);
				}	
			}		
		}

		return $cart_id;	
	}
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_teesheets_manage_table_data_rows($this->teesheet->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20),$this);
		echo $data_rows;
	}
	/*
	Gives search suggestions based on what is being searched for

	function suggest()
	{
		$suggestions = $this->teesheet->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	 */
	function get_row()
	{
		$teesheet_id = $this->input->post('row_id');
		$data_row=get_teesheet_data_row($this->teesheet->get_info($teesheet_id),$this);
		echo $data_row;
	}

	/*
	This deletes teetimes from the teetimes table
	*/
	function delete_teetime($teetime_to_delete = -1)
	{
		session_write_close();
		if ($teetime_to_delete == -1)
			$teetime_to_delete=$this->input->post('id');

        $tee_time_info = $this->teetime->get_info($teetime_to_delete);

		if(!($tee_time_info->person_id > 0) && $this->teetime->delete($teetime_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
		}
	}
	/*
	This deletes teesheet from the teesheet table
	*/
	function delete_teesheets()
	{
		$teesheets_to_delete=$this->input->post('ids');

		if($this->teesheet->delete_list($teesheets_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
		}
	}
	/*
	 Deletes the specified teesheet
	*/
	function delete_teesheet($teesheet_id = false)
	{
		if($this->teesheet->delete($teesheet_id))
		{
			echo json_encode(array('success'=>true,'message'=>"Teesheet successfully deleted."));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>'There was an error deleting the season.'));
		}
	}
	function getJSONTeeTimes($when = '', $side = '') {
		$end = ($when == 'today')? (date('Ymd')-100).'2399':'';
        echo $this->teesheet->getJSONTeeTimes((date('Ymd')-100).'0000',$end, $side, true, true);
    }
	function getJSONTeeTimes2($when = '', $side = '') {
		$end = ($when == 'today')? (date('Ymd')-100).'2399':'';
		$tee_sheet_id = '';
		if ($this->config->item('stack_tee_sheets')) {
            // Send all tee_sheet_ids as a string
            $tee_sheets = $this->teesheet->get_all(100,0,1)->result_array();

            $tee_sheet_ids = array();
            foreach($tee_sheets as $tee_sheet) {
                $tee_sheet_ids[] = $tee_sheet['teesheet_id'];
            }
			$tee_sheet_id = implode(',',$tee_sheet_ids);
		}
        return $this->teesheet->getJSONTeeTimes((date('Ymd')-100).'0000',$end, $side, true, true, $tee_sheet_id);
    }
    function get_newest_tee_times () {
        echo ($this->getJSONTeeTimes2());
    }
	function get_json_teetimes($start, $end) {
        $tee_sheet_id = '';
        if ($this->config->item('stack_tee_sheets')) {
            // Send all tee_sheet_ids as a string
            $tee_sheets = $this->teesheet->get_all(100,0,1)->result_array();

            $tee_sheet_ids = array();
            foreach($tee_sheets as $tee_sheet) {
                $tee_sheet_ids[] = $tee_sheet['teesheet_id'];
            }
            $tee_sheet_id = implode(',',$tee_sheet_ids);
        }
	    echo $this->teesheet->getJSONTeeTimes($start,$end, '', false, false, $tee_sheet_id);
    }
	function logout()
	{
        $this->Employee->logout();
	}

	/*
	Loads the teesheet edit form
	*/
	function view_teesheet($teesheet_id=-1)
	{
		$this->load->model('Booking_class');
		$data['teesheet_info']=$this->teesheet->get_info($teesheet_id);
		$data['thank_you_campaign'] = '';
		if ($data['teesheet_info']->thank_you_campaign_id)
		{
			$mc = $this->Marketing_campaign->get_info($data['teesheet_info']->thank_you_campaign_id);
			$data['thank_you_campaign'] = $mc->name;
		}
		$data['obcs'] = $this->Booking_class->get_all($teesheet_id);
		$this->load->view("teesheets/form",$data);
	}
	function view_booking_class($teesheet_id, $booking_class_id = -1)
	{
		$this->load->model('Booking_class');
		$this->load->model('v2/Item_model');
		$course_ids = array();
		$this->load->model('course');
		$this->course->get_linked_course_ids($course_ids, 'shared_customers', $this->session->userdata('course_id'));

		$data = array(
			'booking_class_info' => $this->Booking_class->get_info($booking_class_id, $teesheet_id),
			'price_classes' => ($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types($teesheet_id)),
			'groups' => $this->Customer->get_group_info(),
			'class_groups' => $this->Booking_class->get_groups($booking_class_id),
			'passes' => $this->Item_model->get(['item_type' => 'pass','course_ids'=>$course_ids]),
			'teesheet_id' => $teesheet_id,
			'booking_fee_item' => false
		);

		if(!empty($data['booking_class_info']->booking_fee_item_id)){
			$this->load->model('v2/Item_model');
			$items = $this->Item_model->get(['item_id' => $data['booking_class_info']->booking_fee_item_id]);
			
			if(!empty($items[0])){
				$data['booking_fee_item'] = $items[0];
			}
		}

		$this->load->view('teesheets/booking_class_form', $data);
	}
	
	function view_aggregate_settings($teesheet_id, $booking_class_id = -1)
	{
		$this->load->model('Booking_class');
		$data = array(
			'booking_class_info' => $this->Booking_class->get_aggregate_settings($teesheet_id),
			'teesheet_id' => $teesheet_id
		);
		$this->load->view('teesheets/booking_class_form', $data);
	}

    function view_increment_settings($teesheet_id)
    {
        $this->load->model('Increment_adjustment');
        $this->load->model('Course');

        $teesheet_info = $this->teesheet->get_info($teesheet_id);
        $data = array(
            'increment_adjustments' => $this->Increment_adjustment->get_all(false, false, $teesheet_id),
            'teesheet_id' => $teesheet_id,
            'future_booking_count' => $this->teesheet->count_tee_times($teesheet_id, date('YmdHi') - 1000000),
            'using_api' => $this->Course->is_using_api($teesheet_info->course_id)
        );
        $this->load->view('teesheets/increment_settings', $data);
    }

    function view_increment_quick_settings($teesheet_id, $date, $start)
    {
        $start = '0000-00-00 '.$start;
        $this->load->model('Increment_adjustment');
        $this->load->model('Course');
        $hour = date('H', strtotime($start));
        $minute = (date('i', strtotime($start))>30)?'30':'00';
        $end_hour = date('H', strtotime($start." +30 minutes"));
        $end_minute = (date('i', strtotime($start." +30 minutes"))>30)?'30':'00';

        $data = array(
            'teesheet_id' => $teesheet_id,
            'start' => "$hour:$minute",
            'end' => "$end_hour:$end_minute",
            'date' => date("Y-m-d", strtotime($date))
        );
        $this->load->view('teesheets/increment_quick_settings', $data);
    }

    function save_increment_adjustments($add_row)
    {
        $this->load->model('Increment_adjustment');
        $values = $this->input->post();

        $exclusions = $values['increment_adjustment_id'];
        // Remove any Deleted increments
        $this->Increment_adjustment->delete_all($exclusions, $values['teesheet_id']);
        // Update other increments
        foreach($values['increment_adjustment_id'] as $iai)
        {
            $data = array(
                'label' => $values['increment_label_'.$iai],
                'color' => $values['increment_color_'.$iai],
                'start_date' => date('0000-m-d', strtotime("{$values['start_date_'.$iai]} 2000")),
                'end_date' => date('0000-m-d', strtotime("{$values['end_date_'.$iai]} 2000")),
                'start_time' => date('Hi', strtotime($values['start_time_'.$iai])),
                'end_time' => date('Hi', strtotime($values['end_time_'.$iai])),
                'monday' => !empty($values['monday_'.$iai]) ? 1 : 0,
                'tuesday' => !empty($values['tuesday_'.$iai]) ? 1 : 0,
                'wednesday' => !empty($values['wednesday_'.$iai]) ? 1 : 0,
                'thursday' => !empty($values['thursday_'.$iai]) ? 1 : 0,
                'friday' => !empty($values['friday_'.$iai]) ? 1 : 0,
                'saturday' => !empty($values['saturday_'.$iai]) ? 1 : 0,
                'sunday' => !empty($values['sunday_'.$iai]) ? 1 : 0,
                'increment' => $values['increment_'.$iai]
            );
            $this->Increment_adjustment->save($data, $iai);
        }
        // Add new if needed
        if ($add_row)
        {
            $tee_sheet_info = $this->teesheet->get_info($values['teesheet_id']);
            $data = array(
                'course_id' => $this->config->item('course_id'),
                'tee_sheet_id' => $values['teesheet_id'],
                'start_date' => '0000-01-01',
                'end_date' => '0000-12-31',
                'start_time' => '500',
                'end_time' => '500',
                'monday' => '1',
                'tuesday' => '1',
                'wednesday' => '1',
                'thursday' => '1',
                'friday' => '1',
                'saturday' => '1',
                'sunday' => '1',
                'increment' => $tee_sheet_info->increment
            );
            $this->Increment_adjustment->save($data);
        }

        echo json_encode(array('success'=>true));
    }

    function save_quick_increment_adjustment()
    {
        $this->load->model('Increment_adjustment');
        $values = $this->input->post();
        $tee_sheet_info = $this->teesheet->get_info($values['teesheet_id']);

        $data = array(
            'course_id' => $this->config->item('course_id'),
            'tee_sheet_id' => $values['teesheet_id'],
            'label' => $values['increment_label'],
            'color' => $values['increment_color'],
            'start_date' => date('0000-m-d', strtotime("{$values['date']} 2000")),
            'end_date' => date('0000-m-d', strtotime("{$values['date']} 2000")),
            'start_time' => date('Hi', strtotime($values['start_time'])),
            'end_time' => date('Hi', strtotime($values['end_time'])),
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'increment' => $tee_sheet_info->increment
        );
        $this->Increment_adjustment->save($data);

        echo json_encode(array('success'=>true, 'dailies'=>json_decode($this->Increment_adjustment->get_json_list(true))));
    }

	function hide_back_nine($value)
	{
		$this->session->set_userdata('hide_back_nine', $value);
	}
	function save_booking_class($booking_class_id = false)
	{
		$this->load->model('Booking_class');
		$booking_class_data = array(
			'teesheet_id' => $this->input->post('teesheet_id'),
			'name' => $this->input->post('bc_name'),
			'active' => $this->input->post('active'),
			'price_class' => $this->input->post('bc_price_class'),
			'online_open_time' => $this->input->post('booking_class_open_time'),
			'online_close_time' => $this->input->post('booking_class_close_time'),
			'online_booking_protected' => $this->input->post('online_booking_protected'),
			'require_credit_card' => $this->input->post('require_credit_card'),
			'days_in_booking_window' => $this->input->post('bc_days_in_booking_window'),
			'minimum_players' => $this->input->post('bc_minimum_players'),
			'limit_holes' => $this->input->post('bc_limit_holes'),
            'show_full_details' => $this->input->post('show_full_details'),
            'hide_online_prices' => $this->input->post('bc_hide_online_prices'),
            'use_customer_pricing' => $this->input->post('bc_use_customer_pricing'),
			'booking_carts' => $this->input->post('bc_booking_carts'),
			'pay_online' => $this->input->post('bc_pay_online'),
			'allow_name_entry' => $this->input->post('bc_allow_name_entry'),
			'pass_item_ids' => $this->input->post('bc_pass_item_ids'),
			'booking_fee_enabled' => $this->input->post('booking_fee_enabled'),
			'booking_fee_item_id' => $this->input->post('booking_fee_item_id'),
			'booking_fee_terms' => $this->input->post('booking_fee_terms'),
			'booking_fee_per_person' => $this->input->post('booking_fee_per_person')
		);
		$group_ids = $this->input->post('bc_class_groups');
		
		if($booking_class_data['online_booking_protected'] == 0){
			$booking_class_data['show_full_details'] = 0;
			$booking_class_data['allow_name_entry'] = 0;
		}

		if(empty($booking_class_data['booking_fee_item_id'])){
			$booking_class_data['booking_fee_item_id'] = null;
		}

		if($booking_class_data['booking_fee_enabled'] == 1 && empty($booking_class_data['booking_fee_item_id'])){
			$booking_class_data['booking_fee_enabled'] = 0;
		}
	
		$this->Booking_class->save($booking_class_data,$group_ids, $booking_class_id);
		
		$data = array();
		$data['teesheet_info']=$this->teesheet->get_info($booking_class_data['teesheet_id']);
		$data['obcs'] = $this->Booking_class->get_all($booking_class_data['teesheet_id']);
		foreach ($data['obcs'] as $key => $value) {
			$get_groups_label = $this->Booking_class->get_group_label($value['booking_class_id']);
			$data['obcs'][$key]['group_id'] = !empty($get_groups_label[0]['group_id']) ? $get_groups_label[0]['group_id'] : '';
		}
		echo $this->load->view('teesheets/booking_classes', $data);
	}
	function delete_booking_class($teesheet_id, $booking_class_id)
	{
		$this->load->model('Booking_class');
		$success = $this->Booking_class->delete($teesheet_id, $booking_class_id);

		echo json_encode(array('success'=>$success, 'booking_class_id'=>$booking_class_id));
	}
	/*
	Loads the teesheet edit form for the seasonal editor
	*/
	function seasonal_teesheet($teesheet_id = 0)
	{
        $this->load->model('Booking_class');
		$this->load->model('online_hours');

		$data['teesheet_info']=$this->teesheet->get_info($teesheet_id);
		$data['hours'] = $this->online_hours->get($teesheet_id);
		$data['thank_you_campaign'] = '';
        $data['is_admin'] = $this->permissions->is_admin();
		if (isset($data['teesheet_info']->thank_you_campaign_id) && $data['teesheet_info']->thank_you_campaign_id)
		{
			$mc = $this->Marketing_campaign->get_info($data['teesheet_info']->thank_you_campaign_id);
			$data['thank_you_campaign'] = $mc->name;
		}
		$data['obcs'] = $this->Booking_class->get_all($teesheet_id);
		foreach ($data['obcs'] as $key => $value) {
			$get_groups_label = $this->Booking_class->get_group_label($value['booking_class_id']);
			$data['obcs'][$key]['group_id'] = isset($get_groups_label[0]) ? $get_groups_label[0]['group_id'] : 0;
		}
		$this->load->view("teesheets/seasonal_form", $data);
	}

	/*
	Loads the teetime edit form
	*/
	function view_teetime($teetime_id=-1)
	{
        session_write_close();
		$this->load->model('customer');
		$this->load->model('event');
		$this->load->model('Customer_credit_card');

        $new_time = false;
        if ($teetime_id == 'blank_time') {
            $save_response = $this->save_teetime();
            //var_dump($save_response);
            $teetime_id = $save_response['tee_time_id'];
            $new_time = true;
        }

        $teesheet_info = $this->teesheet->get_info($this->input->post('teetime_teesheet_id'));






		$data['teesheet_holes'] = $teesheet_info->holes;

		$data['teetime_info']=$this->teetime->get_info($teetime_id);
        if ($new_time) {
            $data['existing_events'] = $this->teetime->check_availability((array)$data['teetime_info'], true);
        }
		$data['event_people'] = $this->event->get_people($teetime_id);

		$data['customer_info'] = [];
		if ($data['teetime_info']->title == '') {
			$data['checkin_text'] = 'Walk In';
			$data['save_text'] = 'Reserve';
		}
		else {
			$data['checkin_text'] = 'Check In';
			$data['save_text'] = 'Update';
		}




		/*
		 * Load Customer Relationships
		 */
		// Pull exclusive list of price class IDs a customer has access to
		if($this->config->item('limit_fee_dropdown_by_customer') == 1){
			$this->Customer->include_valid_price_classes = true;
		}
		if ($data['teetime_info']->person_id != 0 && $data['teetime_info']->person_id != '')
			$data['customer_info'][$data['teetime_info']->person_id] = $this->Customer->get_info($data['teetime_info']->person_id, $this->session->userdata('course_id'));
        if ($data['teetime_info']->person_id_2 != 0 && $data['teetime_info']->person_id_2 != '')
			$data['customer_info'][$data['teetime_info']->person_id_2] = $this->Customer->get_info($data['teetime_info']->person_id_2, $this->session->userdata('course_id'));
		if ($data['teetime_info']->person_id_3 != 0 && $data['teetime_info']->person_id_3 != '')
			$data['customer_info'][$data['teetime_info']->person_id_3] = $this->Customer->get_info($data['teetime_info']->person_id_3, $this->session->userdata('course_id'));
		if ($data['teetime_info']->person_id_4 != 0 && $data['teetime_info']->person_id_4 != '')
			$data['customer_info'][$data['teetime_info']->person_id_4] = $this->Customer->get_info($data['teetime_info']->person_id_4, $this->session->userdata('course_id'));
		if ($data['teetime_info']->person_id_5 != 0 && $data['teetime_info']->person_id_5 != '')
			$data['customer_info'][$data['teetime_info']->person_id_5] = $this->Customer->get_info($data['teetime_info']->person_id_5, $this->session->userdata('course_id'));



		/*
		 * Get Customer status
		 */
		if (!isset($data['customer_info']) || $data['teetime_info']->person_id == 0 || !isset($data['customer_info'][$data['teetime_info']->person_id]) || $data['customer_info'][$data['teetime_info']->person_id]->status_flag == 0) {
			$data['image_status'] = '';
		} else if ($data['customer_info'][$data['teetime_info']->person_id]->status_flag == 1) {
			$data['image_status'] = 'status_red customer_status';
		} else if ($data['customer_info'][$data['teetime_info']->person_id]->status_flag == 2) {
			$data['image_status'] = 'status_yellow customer_status';
		} else if ($data['customer_info'][$data['teetime_info']->person_id]->status_flag == 3){
			$data['image_status'] = 'status_green customer_status';
		}





		/*
		 * Generate price class menus
		 */
        $price_class_holes = $data['teetime_info']->holes ? $data['teetime_info']->holes : $teesheet_info->holes;
        $data['price_classes'] = $this->get_price_class_menu($data['teetime_info']->start, $price_class_holes, $data['teetime_info']->carts);
		
		if($this->config->item('seasonal_pricing') == 0){
			$data['price_classes'] = array('0'=>'Default Price') + $data['price_classes'];
			
			// FIX THE PRICE CLASS IDs
			if ($data['teetime_info']->price_class_1 != 0)
				$data['teetime_info']->price_class_1 = 'price_category_'.$data['teetime_info']->price_class_1;
			if ($data['teetime_info']->price_class_2 != 0)
				$data['teetime_info']->price_class_2 = 'price_category_'.$data['teetime_info']->price_class_2;
			if ($data['teetime_info']->price_class_3 != 0)
				$data['teetime_info']->price_class_3 = 'price_category_'.$data['teetime_info']->price_class_3;
			if ($data['teetime_info']->price_class_4 != 0)
				$data['teetime_info']->price_class_4 = 'price_category_'.$data['teetime_info']->price_class_4;
			if ($data['teetime_info']->price_class_5 != 0)
				$data['teetime_info']->price_class_5 = 'price_category_'.$data['teetime_info']->price_class_5;
            if ($data['teetime_info']->default_price_category != 0)
                $data['teetime_info']->default_price_category = 'price_category_'.$data['teetime_info']->default_price_category;
            if ($data['teetime_info']->sg_default_price_category != 0)
                $data['teetime_info']->sg_default_price_category = 'price_category_'.$data['teetime_info']->default_price_category;
		}

		$default_price_class = $this->Price_class->get_default();
		for($player_position = 1; $player_position <= 5; $player_position++){
			
			$price_class_menu_key = 'pc_menu_'.$player_position;
			$price_class_key = 'price_class_'.$player_position;
			$person_key = 'person_id_'.$player_position;
			if($player_position == 1){
				$person_key = 'person_id';
			}
			$filtered_price_classes = $data['price_classes'];

			// If course is set to only allow price classes that the customer can use (via passes or their default)
			if(!empty($data['customer_info'][$data['teetime_info']->{$person_key}])){
				$customer_price_class = $data['customer_info'][$data['teetime_info']->{$person_key}]->price_class;
			} else {
				$customer_price_class = null;
			}
			if($this->config->item('limit_fee_dropdown_by_customer') == 1){
				
				if(
					!empty($data['customer_info'][$data['teetime_info']->{$person_key}]) &&
					property_exists($data['customer_info'][$data['teetime_info']->{$person_key}], 'valid_price_classes')
				){
					$valid_customer_price_classes = $data['customer_info'][$data['teetime_info']->{$person_key}]->valid_price_classes;
					$customer_price_class = $data['customer_info'][$data['teetime_info']->{$person_key}]->price_class;
					$filtered_price_classes = [];
					
					// Filter price classes according to customer
					foreach($data['price_classes'] as $price_class_id => $price_class_name){
						if(in_array($price_class_id, $valid_customer_price_classes)){
							$filtered_price_classes[$price_class_id] = $price_class_name;
						}
					}	
				}	
			}
			$ordered_price_class = [];
			foreach($filtered_price_classes as $id=>$name)
			{
				$ordered_price_class[] = [
					"id"=>$id,
					"value"=>$name
				];
			}
			usort($ordered_price_class, function($a, $b)
			{
				return strcmp($a['value'], $b['value']);
			});

			$options = "";
			foreach($ordered_price_class as $price_class){
				$sel = "";
				//Select the option if it's the default and no class is currently selected
				//It's the class selected
				//Or if a player is selected, not class is already selected, and the player's it's the same as the player's price class.
				$teetime_info = $data['teetime_info'];
				if( $this->is_selected_price_class(
						$price_class['id'],
						$teetime_info->{$price_class_key},
						$teetime_info->{$person_key},
						$default_price_class,
						$customer_price_class
					)
				)
					$sel = "selected";
				$options .= "<option value='{$price_class['id']}' $sel>{$price_class['value']}</option>";
			}
			$data['teetime_info']->{$price_class_menu_key} = "<select id='$price_class_key' name='$price_class_key'>{$options}</select>";
		}





        /*
         * Load Booker Relationship
         */
		$this->db->select('first_name, last_name');
		$this->db->from('people');
		$this->db->where('person_id', $data['teetime_info']->booker_id);
		$this->db->limit(1);
		$booker = $this->db->get()->row_array();
        $employee_name = isset($booker['first_name']) ? $booker['first_name'].' ' : '';
        $employee_name .= isset($booker['last_name']) ? $booker['last_name'] : '';
        $booked_info = '';
        if (trim($employee_name) != '') {
            $time = strtotime($data['teetime_info']->date_booked.' GMT');
            $booked_info = "Booked by {$employee_name} - ".date('n/j', $time)." @ ".date('g:ia', $time);
        }
		$data['teetime_info']->employee_name = $employee_name;
		$data['teetime_info']->booked_info = $booked_info;



		/*
		 * Shotgun specific
		 */
		if ((string) $data['teetime_info']->type == 'shotgun') {
			// Reorder event people
			$event_people = array();
			foreach($data['event_people'] as $ep) {
				$event_people[$ep['hole']][$ep['player_position']] = $ep;
			}
			$data['event_people'] = $event_people;
		}


		$data['teetime_info']->dpc_menu = form_dropdown('default_price_category', $data['price_classes'], $data['teetime_info']->default_price_category);
		$data['teetime_info']->sgdpc_menu = form_dropdown('sg_default_price_category', $data['price_classes'], $data['teetime_info']->default_price_category);
		if ($this->config->item('simulator'))
			$this->load->view("teetimes/form_simulator",$data);
		else {
            $data['teetime_info']->formatted_teed_off_time =  date('h:i a', strtotime($data['teetime_info']->teed_off_time));
            $data['teetime_info']->formatted_turn_time =  date('h:i a', strtotime($data['teetime_info']->turn_time));
            $data['credit_card_dropdown'] =  $this->Customer_credit_card->dropdown($data['teetime_info']->person_id, $data['teetime_info']->credit_card_id);
            echo json_encode($data);
        }


	}
	function get_price_classes($teetime_id)
	{
		session_write_close();
		$this->load->model("teetime");
		$teetime = $this->teetime->get_info($teetime_id);
		if(empty($teetime)){
			return false;
		}
		$date = \Carbon\Carbon::parse($teetime->start_datetime);
		$start = $date->format('Y-m-d');
		$time = $date->format('Gi');

		$include_tax = $this->input->get("include_tax",false);

		$price_class_list = $this->Green_fee->get_price_class_price_details($teetime->teesheet_id,$start,$time,$include_tax);
		echo json_encode($price_class_list);
	}

    function get_price_class_menu($start = null, $holes= null, $carts= null) {
	    session_write_close();
        $start = $this->input->post('start') ? $this->input->post('start') : $start;
        $holes = $this->input->post('holes') ? $this->input->post('holes') : $holes;
        $person_id = ($this->config->item('limit_fee_dropdown_by_customer') == 1) ? $this->input->post('person_id') : 0;
        $carts = $this->input->post('carts') ? $this->input->post('carts') : $carts;
        $tee_sheet_id = $this->input->post('tee_sheet_id') ? $this->input->post('tee_sheet_id') : $this->session->userdata('teesheet_id');

        $fee_params = array(
            'date'=>date("Y-m-d", strtotime($start + 1000000)),
            'start_time'=>date("Hi", strtotime($start + 1000000)),
            'holes'=>$holes,
            'carts'=>$carts
        );
        $valid_price_classes = array();
        if ($person_id) {
            $valid_price_classes = $this->Price_class->get_customer_price_class_ids($person_id,$tee_sheet_id);
        }

        if ($this->input->post('return_json')){
            $price_class_list = $this->permissions->course_has_module('reservations') ? $this->Fee->get_types() : $this->Green_fee->get_types($tee_sheet_id, false, false, $fee_params);
            if (empty($valid_price_classes)){
                foreach($price_class_list as $price_class_id => $price_class){
                    $valid_price_classes[] = $price_class_id;
                }
            }
            echo json_encode(array('price_class_list' => $price_class_list, 'valid_price_classes' => $valid_price_classes));
        }
        else {
            return ($this->permissions->course_has_module('reservations') ? $this->Fee->get_types() : $this->Green_fee->get_types($tee_sheet_id, false, false, $fee_params));
        }
    }
    function fetch_prices($date, $tee_sheet_id){
        $data = array();
        // Organize all prices for today by price class and then timeframe
        $green_fee_types = $this->Green_fee->get_types($tee_sheet_id);
        foreach ($green_fee_types as $gf_index => $gf_type) {
            echo $gf_type;
            $price_timeframes = $this->Pricing->get_price_timeframes($tee_sheet_id, $gf_index, $date);
            // Parse out the data into the correct format
            var_dump($price_timeframes);
        }

        echo json_encode($data);
    }
	function credit_card_dropdown($person_id, $credit_card_id)
	{
		session_write_close();
		$this->load->model('Customer_credit_card');
		echo $this->Customer_credit_card->dropdown($person_id, $credit_card_id);
	}
	function send_confirmation_email($teetime_id = ''){
		session_write_close();
		if ($teetime_id != '')
		{
			$person_array = array();
			$this->load->model('Course');



			$teetime_data = $this->teetime->get_info($teetime_id, true);
			if(!empty($teetime_data)){
				$this->load->model('Teesheet');
				$teesheet_data = $this->teesheet->get_info($teetime_data->teesheet_id);
				$course_info = $this->Course->get_info($teesheet_data->course_id);
			} else {
				$course_info = $this->Course->get_info($this->session->userdata('course_id'));
			}

			$person_array[] = $this->Customer->get_info($teetime_data->person_id);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_2);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_3);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_4);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_5);
            
            $tee_sheet_title = $teetime_data->tee_sheet_title;
            if(empty($tee_sheet_title)) {
                $tee_sheet_title = '';
            }
			// Send emails to each golfer
			foreach ($person_array as $person_info)
			{
				if ($person_info->email)
				{
					$has_online_booking = $this->teesheet->has_online_booking($this->session->userdata('course_id'), $teetime_data->teesheet_id);

					$email_data = array(
						'confirmation_sent'=>$teetime_data->confirmation_emailed,
						'person_id'=>isset($person_info->person_id)?$person_info->person_id:$teetime_data->person_id,
						'course_name'=>$this->session->userdata('course_name'),
						'course_phone'=>$course_info->phone,
						'course_id'=>$this->session->userdata('course_id'),
						'first_name'=>$person_info->first_name,
						'booked_date'=>date('n/j/y', strtotime($teetime_data->start+1000000)),
						'booked_time'=>date('g:ia', strtotime($teetime_data->start+1000000)),
						'booked_holes'=>$teetime_data->holes,
						'booked_players'=>$teetime_data->player_count,
						'reservation'=> $teetime_data->TTID,
                        'tee_sheet' => $tee_sheet_title,
                        'has_online_booking' => $has_online_booking,
						'new_booking'=>true
					);
					$subject = $teetime_data->confirmation_emailed ? 'Updated Reservation Details' : 'Tee Time Reservation Confirmation';
					$this->teetime->send_confirmation_email($person_info->email, $subject, $email_data, $course_info->email, $this->session->userdata('course_name'));
					// Mark as confirmed
					$teetime_id = substr($teetime_id, 0, 20);
					$this->db->query("UPDATE foreup_teetime SET confirmation_emailed = 1 WHERE (TTID = '$teetime_id' OR TTID = '{$teetime_id}b') LIMIT 2");
				}
			}
		}
		echo json_encode(array());
	}
    function view_print_tee_sheet_options($date = false, $tee_sheet_id = false) {
        $data = array();
        if ($date) {
            $data['date'] = date("Y-m-d", strtotime($date+100));
        }
        else {
            $data['date'] = date("Y-m-d");
        }
        $data['tee_sheet_ids'] = array();
        $tee_sheets = $this->teesheet->get_all(100, 0, true)->result();
        foreach ($tee_sheets as $tee_sheet) {
            $data['tee_sheet_ids'][$tee_sheet->teesheet_id] = $tee_sheet->title;
        }
        $data['selected_tee_sheet_id'] = $this->session->userdata('teesheet_id');

        $this->load->view('teesheets/print_tee_sheet_form', $data);
    }
    function pdf_tee_sheet($date, $tee_sheet_id, $print_back_nine, $print_notes, $week=0) {
        set_time_limit(0);
        ini_set('memory_limit', '500M');
        $this->load->model('Increment_adjustment');
        $available_teesheets = $this->teesheet->get_all(100, 0, true)->result_array();
        $permission = false;
        foreach ($available_teesheets as $av_ts) {
            if ($av_ts['teesheet_id'] == $tee_sheet_id) {
                $permission = true;
            }
        }
        if (!$permission) {
            echo 'You do not have permission to access this tee sheet.';
            return;
        }
        $data = array();
        $data['print_back_nine'] = $print_back_nine;
        $data['print_notes'] = $print_notes;
        $data['date'] = $date;
        // Fetch tee sheet information
        $data['tee_sheet_info'] = $this->teesheet->get_info($tee_sheet_id);
        // Fetch increments
        $data['slots'] = $this->Increment_adjustment->get_increment_array($date, $tee_sheet_id, 1);
        // Fetch tee times
        $data['tee_times'] = array();
        $this->db->order_by("reround, start");
        $data['tee_times'] = $this->teesheet->getTeeTimes(date("Ymd0000", strtotime($date))-1000000, date("Ymd2400", strtotime($date))-1000000, $tee_sheet_id)->result_array();
	    $tee_sheet_html = $this->load->view('teesheets/pdf_tee_sheet', $data, true);

	    $snappy = new  \Knp\Snappy\Pdf();
	    if($week ==1)
	    {
		    $weekhtml = "<div width='100%'>";
	    	for($i=0;$i<7;$i++)
		    {
			    $data['date'] = $date;
			    $data['tee_times'] = $this->teesheet->getTeeTimes(date("Ymd0000", strtotime($date))-1000000, date("Ymd2400", strtotime($date))-1000000, $tee_sheet_id)->result_array();
			    if($i > 0){
			    	$data['hide_times'] = 1;
			    }
			    $tee_sheet_html = $this->load->view('teesheets/pdf_tee_sheet_mini', $data, true);
			    $weekhtml.= "<div style='display: inline-block; width:12%; border:black solid 1px;margin:5px;padding:2px;'>$tee_sheet_html</div>";

			    $date = \Carbon\Carbon::parse($date);
			    $date = $date->addDay(1)->toDateString();
		    }
			$weekhtml .= "</div>";

	        $tee_sheet_html = $weekhtml;
		    $snappy->setOption("orientation",'landscape');
        }


        $snappy->setBinary("application/bin/wkhtmltopdf");
        $snappy->setOption("viewport-size",'1366x1024');
        $snappy->setOption("zoom",'.8');

        $html = "<page style'width:800px;'>".$tee_sheet_html.'</page>';
        header('Content-Type: application/pdf');
        $tee_sheet_name = 'Tee Sheet '.date('Y-m-d', strtotime($date));
        header("Content-Disposition: attachment; filename='{$tee_sheet_name}.pdf'");
        echo $snappy->getOutputFromHtml($html);
    }
    function print_teesheet($date, $tee_sheet_id = false)
	{
        $this->teesheet->print_teesheet($date, $tee_sheet_id);
	}
	function get_message()
	{
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache');

		echo "id: 12345".PHP_EOL;
		echo "retry: 20000" . PHP_EOL;
		echo "data: message3".PHP_EOL;
		echo PHP_EOL;
		ob_flush();
		flush();
	}
	function generate_stats()
	{
		session_write_close();
		$teesheet_id = null;
		if(isset($_GET['teesheet_id'])){
			$teesheet_id = $_GET['teesheet_id'];
		}
		$date = null;
		$end = null;
		if(isset($_GET['date'])){
			$date = $_GET['date'];
		} else if(isset($_GET['start']) && isset($_GET['end'])){
			$date = $_GET['start'];
			$end = $_GET['end'];
		} else if(isset($_POST['start'])){
			$date = $this->input->post('start') ? date('Y-m-d', strtotime($this->input->post('start'))) : date('Y-m-d');
			$end = $this->input->post('end') ? date('Y-m-d', strtotime($this->input->post('end'))) : date('Y-m-d');
			if($date == $end){
				$end = \Carbon\Carbon::parse($end)->endOfDay()->toIso8601String();
			}
		}
		$results = $this->Dash_data->fetch_tee_sheet_data($teesheet_id,$date,$end);
		echo json_encode($results);
	}

	/*
	 * BEGIN CREDIT CARD FUNCTIONS
	 */
	function open_add_credit_card_window($tee_time_id, $person_id) {
		$course_id = $this->session->userdata('course_id');

		// USING ETS FOR PAYMENT PROCESSING
		if ($this->config->item('ets_key'))
		{
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));

			$session = $payment->set('action', 'session')
			  		   ->set('isSave', 'true')
					   ->send();
			if ($session->id)
			{
				$user_message = $previous_card_declined!='false'?'Card declined, please try another.':'';
				$return_code = '';
				$this->session->set_userdata('ets_session_id', (string)$session->id);
				//$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
				$data = array(
					'user_message'=>$user_message,
					'return_code'=>$return_code,
					'session'=>$session,
					'url' => 'index.php/teesheets/card_captured/'.$tee_time_id.'/'.$person_id,
					'tee_time_card' => true);
				$this->load->view('sales/ets', $data);
			}
			else
			{
				$data = array('processor' => 'ETS');
				$this->load->view('sales/cant_load', $data);
			}
		}
		else if ($this->config->item('mercury_e2e_id') && !$manual_entry)
		{
			$this->load->library('Hosted_checkout_2');

			$HC = new Hosted_checkout_2();
			$HC->set_default_swipe('Manual');
			$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//foreUP's Credentials
			$HC->set_response_urls('teesheets/card_captured/'.$tee_time_id.'/'.$person_id, 'credit_cards/process_cancelled');

			$initialize_results = $HC->initialize_payment('1.00','0.00','PreAuth','eCOM','Recurring');
			//print_r($initialize_results);
			if ((int)$initialize_results->ResponseCode == 0)
			{
				//Set invoice number to save in the database
				$invoice = $this->sale->add_credit_card_payment(array('mercury_id'=>$this->config->item('mercury_id'),'mercury_password'=>$this->config->item('mercury_password'),'tran_type'=>'PreAuth','frequency'=>'Recurring'));
				$this->session->set_userdata('invoice', $invoice);

				$user_message = (string)$initialize_results->Message;
				$return_code = (int)$initialize_results->ResponseCode;
				$this->session->set_userdata('payment_id', (string)$initialize_results->PaymentID);
				$url = $HC->get_iframe_url('eCOM', (string)$initialize_results->PaymentID);
				$data = array('user_message'=>'$1.00 authorization in order to save the credit card', 'return_code'=>$return_code, 'url'=>$url);
				$this->load->view('sales/hc_pos_iframe.php', $data);
			}
			//$data = array('amount'=>$total_amount, 'type'=>'PreAuth', '');
			//$this->load->view('sales/m_e2e_window.php', $data);
		}
		// USING MERCURY FOR PAYMENT PROCESSING
		else if ($this->config->item('mercury_id'))
		{
            $this->load->model('Blackline_devices');
            $blackline_info = $this->Blackline_devices->get_terminal_info($this->session->userdata('terminal_id'));
            if ($this->config->item('use_mercury_emv') && $blackline_info) {
                $data = array();
                $trans_type = 'auth';
                $total = 1;
                $payment_data = array(
                    'course_id' => $this->config->item('course_id'),
                    'mercury_id' => $this->config->item('mercury_id'),
                    'mercury_password' => $this->config->item('mercury_password'),
                    'tran_type' => $trans_type,
                    'amount' => $total,
                    'frequency' => 'OneTime',
                    'terminal_name' => $this->session->userdata('terminal_id'),
                    'initiation_time' => gmdate('Y-m-d H:i:s')
                );
                $invoice_id = $this->sale->add_credit_card_payment($payment_data);
                $data['payment_url'] = $this->Blackline_devices->get_payment_url($trans_type, $total, $blackline_info, $invoice_id);
                $data['tee_time_id'] = $tee_time_id;
                $data['person_id'] = $person_id;
                $data['controller_name'] = strtolower(get_class());
                $this->load->view('v2/sales/mercury_emv', $data);
            }
            else {
                $this->load->library('Hosted_checkout_2');

                $HC = new Hosted_checkout_2();
                $HC->set_default_swipe('Manual');
                $HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));//foreUP's Credentials
                $HC->set_response_urls('teesheets/card_captured/' . $tee_time_id . '/' . $person_id, 'credit_cards/process_cancelled');

                $initialize_results = $HC->initialize_payment('1.00', '0.00', 'PreAuth', 'POS', 'Recurring');
                //print_r($initialize_results);
                if ((int)$initialize_results->ResponseCode == 0) {
                    //Set invoice number to save in the database
                    $invoice = $this->sale->add_credit_card_payment(array('mercury_id'=>$this->config->item('mercury_id'),'mercury_password'=>$this->config->item('mercury_password'),'tran_type' => 'PreAuth', 'frequency' => 'Recurring'));
                    $this->session->set_userdata('invoice', $invoice);

                    $user_message = (string)$initialize_results->Message;
                    $return_code = (int)$initialize_results->ResponseCode;
                    $this->session->set_userdata('payment_id', (string)$initialize_results->PaymentID);
                    $url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
                    $data = array('user_message' => '$1.00 authorization in order to save the credit card', 'return_code' => $return_code, 'url' => $url);
                    $this->load->view('sales/hc_pos_iframe.php', $data);
                }
            }
		}
	}

	function card_captured($tee_time_id, $person_id) {
		$this->load->model('teetime');
        $approved = false;
		$this->load->model('Customer_credit_card');

		// Capture card response from ETS or Mercury
		$cc_data = array('course_id'=>'','card_type'=>'','masked_account'=>'');
		$approved = $this->Customer_credit_card->capture_card($person_id, $cc_data);
		// UPDATE TEE TIME WITH CC INFO
		$this->teetime->update_credit_card($tee_time_id, $cc_data);
		$credit_cards = $this->Customer_credit_card->get($person_id, $cc_data['credit_card_id']);
		
		$data = array ('controller_name'=>'teesheets', 'card_captured'=>true, 'course_id'=>$this->config->item('course_id'), 'open_billing_box'=>'true', 'credit_card_id'=>$cc_data['credit_card_id'], 'credit_card_name'=>$cc_data['card_type'].' '.$cc_data['masked_account'], 'credit_cards'=>$credit_cards);
		$data['hide_jquery'] = true;
		if ($approved){
            if ($this->input->post('returnJSON')) {
                echo json_encode(array('success'=>true,'credit_card_id'=>$cc_data['credit_card_id']));
            }
            else {
                $this->load->view('customers/card_captured', $data);
            }
		}
		else {
			if ($this->input->post('returnJSON')) {
				echo json_encode(array('success'=>false));
			}
		}
	}

	function add_new_charge($cc_id) {
		$amount = $this->input->post('amount');
		$description = $this->input->post('description');
		$subscription = $this->input->post('subscription');
		$start_date = $this->input->post('start_date');
		$month = $this->input->post('month');
		$day = $this->input->post('day');

		$data = array (
			'credit_card_id'=>$cc_id,
			'amount'=>$amount,
			'description'=>$description,
			'subscription'=>$subscription,
			'start_data'=>date('Y-m-d', strtotime($start_date)),
			'month'=>$month,
			'day'=>$day
		);
		//echo 'about to save';
		$this->load->model('Customer_billing');
		if ($this->Customer_billing->save($data))
		echo $this->db->last_query();
			echo $this->Customer_billing->create_billing_row($data);

		echo false;
	}
	function view_charge_card($tee_time_id = '', $credit_card_id = '')
	{
        $this->load->model('Customer_credit_card');
        $data = array();
        $data['teetime_info'] = $this->teetime->get_info($tee_time_id);
        $data['credit_card'] = $this->Customer_credit_card->get_info($credit_card_id);
        // GET DATA CONCERNING PREVIOUS CHARGES
		$data['charges'] = $this->sale->get_balance_item_sales('tee_time_charge', $tee_time_id);
        $this->load->view('teesheets/charge_credit_card', $data);
	}
	function charge_card()
	{
		$error_message = array();
		$credit_card_id = $this->input->post('card_to_charge');
		$amount = $this->input->post('charge_amount');
		$tee_time_id = $this->input->post('tee_time_id');
		$person_id = $this->input->post('charge_person_id');
		$this->load->model('Customer_credit_card');
		$this->load->helper('general');
		//$this->load->library("sale_lib");
		// VALIDATE VALUES
		if (!$credit_card_id)
			$error_message[] = 'No credit card id received.';
		if (!$tee_time_id)
			$error_message[] = 'No tee time id received.';
		if (!$amount || $amount <= 0 || !is_numeric($amount))
			$error_message[] = 'Invalid amount, please enter a dollar amount to charge.';
		if (count($error_message) > 0)
		{
			echo json_encode(array('success'=>false,'error_array'=>$error_message));
			return;
		}
		// CHARGE THE CARD
		$amount = to_currency_no_money($amount);
		$charge_data = array(
			'credit_card_id'=>$credit_card_id,
			'course_id'=>$this->session->userdata('course_id'),
            'invoice_id'=>0
		);
		$charged = $this->Customer_credit_card->charge($charge_data, $amount);
		if ($charged)
		{
			// CLEAR OUT THE CART
			//$this->sale_lib->clear_all();
			// MAKE SURE CHARGE ITEM EXISTS
			$item_id = $this->sale->get_balance_item('tee_time_charge');
			// ADD ITEM TO SALE
			// if(!$this->sale_lib->add_item($item_id,1,0, $amount) || !$this->sale_lib->add_item_to_basket($item_id,1,0, $amount))
				// $error_message[] = lang('sales_unable_to_add_item');
	 		// // ADD CUSTOMER TO SALE
			// if ($person_id && !$this->Customer->exists($person_id, $this->session->userdata('course_id')))
				// $error_message[] = lang('sales_unable_to_add_customer').$this->db->last_query();
			// // ADD CUSTOMER
			// $this->sale_lib->set_customer($person_id);
			// // ADD TEE TIME TO SALE
			// $this->sale_lib->set_teetime($tee_time_id);
			// ADD PAYMENT TO SALE
			// if( !$this->sale_lib->add_payment( $charge_data['payment_type'], $amount, $amount, false, $charge_data['credit_card_payment_id']) )
				// $error_message[] = lang('sales_unable_to_add_payment');
			// COMPLETE SALE
			if (count($error_message) == 0)
			{
				$sale_items = array();
				$sale_items[] = array(
					'item_id' => $item_id,
					'line' => 1,
					'quantity' => 1,
					'discount' => 0,
					'price' => $amount,
					'subtotal' => $amount,
					'total' => $amount			
				);
				
				$sale_payments = array();
				$sale_payments[] = array(
					'payment_type' => $charge_data['payment_type'],
					'payment_amount' => abs($amount),
					'invoice_id' => $charge_data['credit_card_payment_id']
				);	
				// Create sale that pays No Show Fee
				$sale_id = $this->sale->save($sale_items, $person_id, $this->session->userdata('person_id'), 'No Show Charge', $sale_payments, false, $tee_time_id);
				if ($sale_id > 0)
				{
					echo json_encode(array('success'=>true,'sale_id'=>$sale_id));
					return;
				}
			}
		}
		else {
			$error_message[] = 'Was not able to charge the card';
		}

		// IF WE HAVE ERRORS, WE END UP HERE
		echo json_encode(array('success'=>false,'error_array'=>$error_message));
	}
	/*
	 * END CREDIT CARD FUNCTIONS
	 */

        function get_standby()
        {

            $id = $this->input->post('id');

            $this->load->model('teetime');
            $url['urlData'] = ($this->standby->get_by_id2($id));

            echo json_encode($url);


        }
     /*
	 * Display teesheet note
	 */
    function view_teesheet_note() {
	    session_write_close();
        $teesheet_id = $this->input->post('teesheet_id');
        $note_id = $this->input->post('note');
        $date = $this->input->post('date');
    	$teesheetnote = $this->teesheet->get_teesheet_note($note_id,$teesheet_id,$date);
    	echo json_encode($teesheetnote);
    }    
     /*
	 * Save teesheet note
	 */   
    function add_teesheet_note() {
    	ini_set('display_errors', 1);
        $this->load->model('teesheet');
        $teesheet_id = $this->input->post('teesheet_id');
        $note = $this->input->post('note');
        $date = $this->input->post('date');
        $note_id = $this->input->post('note_id');
        $note_data = array('teesheet_id'=>$teesheet_id,'note'=>$note,'date'=>$date);
      	$note_id = $this->teesheet->save_teesheet_note($note_data, $note_id);
     	echo json_encode(array('note_id'=>$note_id));  	 
    }

     /*
	 * Save person flag nad comments
	 */   
    function set_session($person_flag, $person_comments) {
	 	 //$this->session->set_userdata("person_flag", $person_flag);
	 	 //$this->session->set_userdata("person_comments", $person_comments);
    }
    /*
	Loads the customer edit form
	*/
	function view_customer($customer_id=-1, $tee_time_slot = 1)
	{
		$this->load->model('Image');
		$this->load->model('Customer_credit_card');
		$this->load->model('Household');
        $this->load->model('Pass');
        $data['person_info']=$this->Customer->get_info($customer_id, $this->session->userdata('course_id'));
		$groups = $this->Customer->get_group_info($customer_id);
		$data['groups']=$groups;
		if($this->session->userdata('sales_v2') == 1){
            $passes = $this->Pass->get(array('customer_id' => $customer_id));
        }else{
            $passes = $this->Customer->get_pass_info($customer_id);
        }
		$data['passes']=$passes;
		$data['open_billing'] = $open_billing;
		$data['image_thumb_url'] = $this->Image->get_thumb_url($data['person_info']->image_id, 'person');
		$data['price_classes']=($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types());
		$data['credit_cards']=$this->Customer_credit_card->get($customer_id);
		// LOAD HOUSEHOLD DATA
		$data['household_head'] = '';
		$data['household_members'] = '';
		if ($this->Household->is_head($customer_id))
			$data['household_members'] = $this->Household->get_members($customer_id);
		else if ($this->Household->is_member($customer_id))
			$data['household_head'] = $this->Household->get_head($customer_id);
		$data['controller'] = strtolower(get_class());
		$data['status_array'] = array(0 => 'N/A',1 => 'Red', 2 => 'Yellow', 3 => 'Green');
        $data['tee_time_slot'] = $tee_time_slot;
		//echo $this->db->last_query();
		$this->load->view("customers/form",$data);
	}


	public function getTodaysSummary()
	{
		session_write_close();
		$this->load->model("Teesheet");
		$teesheet_id = 	$this->input->get("teesheet_id");
        $course_id = $this->input->get('course_id');
        $teesheetIds = [];
        $returnObjects = [];
        if(empty($teesheet_id)){
            $teesheet_id =  $this->session->userdata('teesheet_id');
        }
        if(!empty($course_id)){
            $tee_sheets = $this->teesheet->get_all_from_course(1000,0,$course_id);
            foreach($tee_sheets->result_array() as $tee_sheet) {
                $teesheetIds[] = $tee_sheet['teesheet_id'];
            }
        }
        else {
            $teesheetIds[] = $teesheet_id;
        }
		$day 		= 	date("Y-m-d", strtotime($this->input->get("date",Carbon\Carbon::now()->toDateTimeString())));
        foreach($teesheetIds as $teesheetId) {
            $teesheet = $this->teesheet->get_info($teesheetId);
            $now = new \Carbon\Carbon($day);
            $newteesheet = new fu\Teesheet($teesheet, $now);
            $newteesheet->setFrontOrBack("front");
            $potentialTeetimes = $newteesheet->getPotentialTeetimes();
            $existingTeetimes = $newteesheet->getExistingTeetimes();

            foreach ($existingTeetimes as $existing) {
                $existingTimestamp = $existing['start_datetime']->copy();
                while ($existingTimestamp->diffInMinutes($existing['end_datetime'], false) > 0) {
                    if ($existing["type"] == "teetime") {
                        $potentialTeetimes[$existingTimestamp->format("Y-m-d H:i:00")] += $existing['player_count'];
                    } else {
                        $potentialTeetimes[$existingTimestamp->format("Y-m-d H:i:00")] = 4;
                    }

                    $existingTimestamp = $newteesheet->seekNextPotentialTeetime($existingTimestamp);
                }
            }


            $returnObject = [];
            foreach ($potentialTeetimes as $time => $count) {
                $returnObject[] = ["time" => $time, "count" => $count];
            }
            $returnObjects[] = $returnObject;
        }

		print json_encode($returnObjects);
	}

	public function no_show(){

		$this->load->model('Pass');

		$data = $this->input->post();
		$this->teetime->update_no_show($data['teetime_id'], $data['position'], $data['customer_id']);

		// If the customer has an active pass, mark a "no show" 
		// on that pass
		if(!empty($data['customer_id'])){
			$pass = $this->Pass->get_customer_default($data['customer_id']);
			if($pass){
				$this->Pass->add_history($pass['pass_id'], 'no_show', [
					'teetime_id' => $data['teetime_id']
				]);
			}
		}
	}

	public function add_squeeze() {
		$this->load->model('Squeeze');
		$data = array();

		$data['tee_sheet_id'] = $this->input->post('tee_sheet_id');
		$data['start'] = $this->input->post('start');
		$data['end'] = $this->input->post('end');
		$data['date'] = $this->input->post('date');

		$data['course_id'] = $this->config->item('course_id');

		$success = $this->Squeeze->save($data);

		echo json_encode(array('success'=>$success));
	}

	public function remove_squeeze() {
		$this->load->model('Squeeze');
		$data = array();

		$data['tee_sheet_id'] = $this->input->post('tee_sheet_id');
		$data['start'] = $this->input->post('start');
		$data['end'] = $this->input->post('end');
		$data['date'] = $this->input->post('date');

		$data['course_id'] = $this->config->item('course_id');

        $squeeze_has_times = $this->teetime->get_squeeze_times($data['tee_sheet_id'],$data['date'],$data['start'],$data['end'], true) > 0;

        if ($squeeze_has_times) { // Tee Sheet has squeeze times
            echo json_encode(array('success'=>false,'message'=>'Error: Please move squeeze times before removing the squeeze'));
            return;
        }
		$success = $this->Squeeze->remove($data);

		echo json_encode(array('success'=>$success));
	}

	public function confirm_event_delete($tee_time_id) {
        $linked_event_info = $this->teetime->get_linked_event_info($tee_time_id);

        $data = array(
            'linked_event_info'=>$linked_event_info
        );

        $this->load->view('teetimes/confirm_event_delete', $data);
    }

    public function view_template_manager() {
        $this->load->model('Tee_sheet_template');

        $data = array();
        $data['templates'] = $this->Tee_sheet_template->get_all();
        foreach ($data['templates'] as $index => $template) {
            $events = $this->Tee_sheet_template->get_events($template['template_id']);
            $event_description = array();
            foreach ($events as $event) {
                if ($event['reround'] == 0) {
                    $start = date('g:ia', strtotime($event['start']));
                    $end = date('g:ia', strtotime($event['end']));
                    $event_description[] = '<tr><td class="' . $event['type'] . '" width="10%" style="text-align:center; padding:5px">' . ucfirst($event['type']) . '</td><td width="70%" style="padding:0 0 0 5px;">' . $event['title'] . '</td><td width="20%">' . $start . ' - ' . $end . '</td>';
                }
            }
            $data['templates'][$index]['description'] = implode('',$event_description);
        }

        $this->load->view('teesheets/template_manager', $data);
    }

    public function confirm_template_save($tee_sheet_id, $date) {
        $data = array();
        $tee_sheets = $this->teesheet->get_all(100, 0, 1)->result_array();
        $data['teesheets'] = array();
        foreach ($tee_sheets as $tee_sheet) {
            $data['teesheets'][$tee_sheet['teesheet_id']] = $tee_sheet['title'];
        }
        $data['events'] = $this->teetime->get_template_events($tee_sheet_id, $date);
        $data['date'] = $date;
        $data['tee_sheet_id'] = $tee_sheet_id;

        $this->load->view('teesheets/save_template', $data);
    }

    public function get_template_events() {
        $tee_sheet_id = $this->input->post('tee_sheet_id');
        $date = $this->input->post('date');
        $data = array();
        $data['events'] = $this->teetime->get_template_events($tee_sheet_id, $date);

        $this->load->view('teesheets/template_events', $data);
    }

    public function save_template() {
        $this->load->model('Tee_sheet_template');
        $success = false;
        $data = array();
        $data['tee_sheet_id'] = $this->input->get('template_tee_sheet_id');
        $data['course_id'] = $this->config->item('course_id');
        $data['date'] = date('Y-m-d', strtotime($this->input->get('template_date')));
        $data['name'] = $this->input->get('template_name');
        $data['date_created'] = gmdate('Y-m-d');
        $data['employee_id'] = $this->session->userdata('person_id');
        $template_id = $this->Tee_sheet_template->save($data);

        if ($template_id) {
            $events = $this->teetime->get_template_events($data['tee_sheet_id'], $data['date']);
            $template_events = array();
            foreach ($events as $event) {
                $template_events[] = array(
                    'template_id' => $template_id,
                    'TTID' => $event['TTID'],
                    'type' => $event['type'],
                    'start' => $event['start'],
                    'duration' =>  $event['duration'],
                    'end' => $event['end'],
                    'holes' => $event['holes'],
                    'carts' => $event['carts'],
                    'title' => $event['title'],
                    'details' => $event['details'],
                    'side' => $event['side'],
                    'reround' => $event['reround']
                );
            }
            // Save all template events
            $success = $this->Tee_sheet_template->save_events($template_events);
        }

        echo json_encode(array('success'=>$success, 'template_id'=>$template_id));
    }

    public function confirm_delete_template($template_id) {
        $this->load->model('Tee_sheet_template');
        $data = array('template_id'=>$template_id);
        $this->load->view('teesheets/confirm_delete_template', $data);
    }

    public function delete_template() {
        $this->load->model('Tee_sheet_template');
        $template_id = $this->input->post('template_id');

        echo json_encode(array('success'=>$this->Tee_sheet_template->delete($template_id)));
    }

    public function confirm_apply_template($template_id, $tee_sheet_id, $date) {
        $this->load->model('Tee_sheet_template');
        $data = array();
        $tee_sheets = $this->teesheet->get_all(100, 0, 1)->result_array();
        $data['teesheets'] = array();
        foreach ($tee_sheets as $tee_sheet) {
            $data['teesheets'][$tee_sheet['teesheet_id']] = $tee_sheet['title'];
        }
        $data['template'] = $this->Tee_sheet_template->get($template_id);
        $data['events'] = $this->Tee_sheet_template->get_events($template_id);
        $data['date'] = $date;
        $data['tee_sheet_id'] = $tee_sheet_id;

        $this->load->view('teesheets/apply_template', $data);
    }

    public function apply_template() {
        $this->load->model('Increment_adjustment');
        $this->load->model('Tee_sheet_template');
        $template_id = $this->input->get('template_id');
        $tee_sheet_id = $this->input->get('template_tee_sheet_id');
        $date = date('Ymd', strtotime($this->input->get('template_date'))) - 100;

        $events = $this->Tee_sheet_template->get_events($template_id);//, $tee_sheet_id, $date);

        $json_ready_events = array();

        if (count($events) == 0) {
            echo json_encode(array('success'=>false));
            return;
        }

        $new_id_time = date('mdyHis');

        foreach($events as $event) {
            $new_start = $date.(substr($event['start'], 8));
            $new_end = $date.(substr($event['end'], 8));
            $new_id = 'TTID_'.$new_id_time.substr($event['TTID'], 17);
            $new_event_data = array(
                'TTID' => $new_id,
                'teesheet_id' => $tee_sheet_id,
                'status' => '',
                'start' => $this->Increment_adjustment->get_slot_time($new_start + 1000000, $tee_sheet_id, 0),
                'end' => $new_end,
                'side' => $event['side'],
                'allDay' => 'false',
                'duration' => $event['duration'],
                'details' => $event['details'],
                'holes' => $event['holes'],
                'title' => $event['title'],
                'type' => $event['type'],
                'carts' => $event['carts'],
                'booking_source' => 'template',
                'booker_id' => $this->session->userdata('person_id'),
                'player_count' => 0,
                'reround' => $event['reround']
            );
            $this->db->insert('teetime', $new_event_data);
            $json_ready_events[] = $this->teetime->add_teetime_styles($new_event_data);
        }

        echo json_encode(array('success'=>true, 'json_events'=>$json_ready_events));
    }

    function excel_import($type = 'shotgun')
    {
        if ($type == 'shotgun') {
            $this->load->view("teesheets/excel_import", null);
        }
        else {
            $this->load->view("teesheets/event_excel_import", null);
        }
    }

    function do_excel_import()
    {
        $this->db->trans_start();

        $msg = 'do_excel_import';
        $failCodes = array();
        $names = array();
        if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
        {
            $msg = lang('items_excel_import_failed');
            echo json_encode( array('success'=>false,'message'=>$msg) );
            return;
        }
        else
        {
            $path_info = pathinfo($_FILES['file_path']['name']);
            if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE && strtolower($path_info['extension']) == 'csv')
            {
                // first row is the set of raw keys
                $raw_keys = fgetcsv($handle);

                // iterate over the data rows
                if ($this->input->post('import_type') == 'event') {
                    $row = 1;
                    while (($data = fgetcsv($handle)) !== FALSE) {
                        foreach ($data as $i => $dat) {
                            if (!empty($dat)) {
                                if ($i > 0) {
                                    continue;
                                }
                                $names[] = $dat;
                            }
                        }
                        $row++;
                    }
                }
                else {
                    $row = 1;
                    while (($data = fgetcsv($handle)) !== FALSE) {
                        foreach ($data as $i => $dat) {
                            if (!empty($dat)) {
                                $index = $i;
                                if ($index == 0 || $index == 5 || $index > 9) {
                                    continue;
                                } else if ($index > 5) {
                                    $index--;
                                }
                                $names[$row][$index] = $dat;
                            }
                        }
                        $row++;
                    }
                }
            }
            else
            {
                echo json_encode( array('success'=>false,'message'=>lang('common_upload_file_not_supported_format')));
                return;
            }
            $this->db->trans_complete();
        }

        $success = true;
        if(count($failCodes) > 0)
        {
            $msg = lang('customers_most_imported_some_failed')." (" .count($failCodes) ."): ".implode(", ", $failCodes);
            $success = false;
        }
        else
        {
            $msg = lang('customers_import_successfull');
        }

        echo json_encode( array('success'=>$success,'message'=>$msg, 'names'=>$names) );
    }

    function excel()
    {
        //$data = file_get_contents("import_customers.csv");
        $data = ' ,Player 1, Player 2, Player 3, Player 4, , Player 5, Player 6, Player 7, Player 8,
1A, Mr. Smith, Ms. Jones, Mrs. Johnson, Mr. Tanner, 1B, Ms. Harris, Mr. Donaldson, Mr. Greer, Mrs. Keller,
2A, Ms. Taylor, Mr. Fields, , Mr.Jackson, 2B,
3A, , , , , 3B, , , , ,
4A, , , , , 4B, , , , ,
5A, , , , , 5B, , , , ,
6A, , , , , 6B, , , , ,
7A, , , , , 7B, , , , ,
8A, , , , , 8B, , , , ,
9A, , , , , 9B, , , , ,
10A, , , , , 10B, , , , ,
11A, , , , , 11B, , , , ,
12A, , , , , 12B, , , , ,
13A, , , , , 13B, , , , ,
14A, , , , , 14B, , , , ,
15A, , , , , 15B, , , , ,
16A, , , , , 16B, , , , ,
17A, , , , , 17B, , , , ,
18A, , , , , 18B, , , , ,';
        $name = 'import_players.csv';
        force_download($name, $data);
    }

    function event_excel()
    {
        //$data = file_get_contents("import_customers.csv");
        $data = ' Players,
Mr. Smith, 
Ms. Jones, 
Mrs. Johnson, 
Mr. Tanner, 
Ms. Harris, 
Mr. Donaldson, 
Mr. Greer, 
Mrs. Keller,
Ms. Taylor, 
Mr. Fields,
Mr.Jackson';
        $name = 'import_players.csv';
        force_download($name, $data);
    }

	/**
	 * @param $price_class
	 * @param $price_class_key
	 * @param $data
	 * @param $default_price_class
	 * @param $person_key
	 * @param $customer_price_class
	 * @return bool
	 */
	private function is_selected_price_class($price_class_id, $teetime_price_class_id, $person_id, $default_price_class, $customer_price_class)
	{
		return $price_class_id == $teetime_price_class_id ||
		($teetime_price_class_id == 0 && $price_class_id == $default_price_class['class_id'] && (empty($person_id))) ||
		($teetime_price_class_id == 0 && (!empty($person_id)) && $customer_price_class == $price_class_id) ||
		($teetime_price_class_id == 0 && ($customer_price_class == "" || $customer_price_class == 0) && $price_class_id == $default_price_class['class_id']);
	}

	/**
	 * @param $teesheet_id
	 * @param $person_id
	 * @param $fee_params
	 * @param $position
	 * @param $pass
	 * @param $applied_rule
	 * @param $holes
	 * @return mixed
	 */
	private function generate_greenfee_item($teesheet_id, $person_id, $fee_params, $position, $applied_rule, $holes)
	{
		$green_fees = $this->Pricing->get_fees($fee_params);
		$green_fee = $green_fees[0];

		if (!empty($person_id)) {
			$green_fee['params'] = array('customer_id' => $person_id, 'player_position' => $position);
			if ($this->config->item('limit_fee_dropdown_by_customer') == 1) {
				$green_fee['params']['price_class_ids'] = $this->Price_class->get_customer_price_class_ids($person_id,$teesheet_id);
			}
		}

		if (!empty($applied_rule)) {
			$green_fee['params']['pass_id'] = $applied_rule['pass_id'];
			$green_fee['params']['pass_rule_number'] = $applied_rule['rule_number'];
			$green_fee['params']['pass_rule_id'] = $applied_rule['uniq_id'];
		}
		$green_fee['params']['teesheet_id'] = $teesheet_id;
		$green_fee['params']['holes'] = $holes;
		$green_fee['params']['season_id'] = (int)$green_fee['season_id'];
		return $green_fee;
	}

	/**
	 * @param $teesheet_id
	 * @param $price_class_id
	 * @param $holes
	 * @param $date
	 * @param $start_time
	 * @param $special_id
	 * @param $timeframe_id
	 * @return array
	 */
	private function generate_feeparams($teesheet_id, $price_class_id, $holes, $date, $start_time, $special_id, $timeframe_id)
	{
		$fee_params = array(
			'teesheet_id' => $teesheet_id,
			'price_class_id' => $price_class_id,
			'holes' => $holes,
			'type' => 'green_fee',
			'date' => $date,
			'start_time' => $start_time
		);

		if ($special_id) {
			$fee_params['special_id'] = $special_id;
			return $fee_params;
		} else {
			$fee_params['timeframe_id'] = $timeframe_id;
			return $fee_params;
		}
	}

	/**
	 * @param $teesheet_id
	 * @param $adjusted_start_time
	 * @param $person_id
	 * @return array
	 */
	private function get_applied_pass_rule($teesheet_id, $adjusted_start_time, $person_id)
	{
		$this->load->model("Pass");
		$date_time = DateTime::createFromFormat('YmdHi', $adjusted_start_time);
		$pass = $this->Pass->get_customer_default($person_id, [
			'teesheet_id' => $teesheet_id,
			'date' => $date_time->format('Y-m-d H:i:00')
		]);
		$applied_rule = false;
		// If a valid pass was found, set the price class
		if (!empty($pass)) {
			foreach ($pass['rules'] as $rule) {
				if ($rule['is_valid']) {
					$applied_rule = $rule;
					$applied_rule['pass_id'] = $pass['pass_id'];
					break;
				}
			}
		}
		return $applied_rule;
	}

	/**
	 * @param $teesheet_id
	 * @param $person_id
	 * @param $fee_params
	 * @param $green_fee
	 * @param $applied_rule
	 * @param $holes
	 * @return mixed
	 */
	private function generate_cart_item($teesheet_id, $person_id, $fee_params, $green_fee, $applied_rule, $holes)
	{
		$cart_fees = $this->Pricing->get_fees($fee_params);
		$cart_fee = $cart_fees[0];

		if (!empty($person_id)) {
			$cart_fee['params'] = array('customer_id' => $person_id);

			if ($this->config->item('limit_fee_dropdown_by_customer') == 1) {
				$cart_fee['params']['price_class_ids'] = $green_fee['params']['price_class_ids'];
			}
		}

		if (!empty($applied_rule)) {
			$cart_fee['params']['pass_id'] = $applied_rule['pass_id'];
			$cart_fee['params']['pass_rule_number'] = $applied_rule['rule_number'];
			$cart_fee['params']['pass_rule_id'] = $applied_rule['uniq_id'];
		}
		$cart_fee['params']['teesheet_id'] = $teesheet_id;
		$cart_fee['params']['holes'] = $holes;
		$cart_fee['params']['season_id'] = (int)$green_fee['season_id'];
		return $cart_fee;
	}

	/**
	 * @param $teetime_id
	 * @param $teesheet_id
	 * @param $holes
	 * @param $special
	 * @param $date
	 * @param $start_time
	 * @param $price_class_id
	 * @param $include_cart
	 * @return mixed
	 */
	private function add_items_old_pos($teetime_id, $teesheet_id, $holes, $special, $date, $start_time, $price_class_id, $include_cart)
	{
		$item_num = '7';
		$cart_num = '5';
		if ($holes == 9) {
			$item_num = '2';
			$cart_num = '4';

		} else if ($holes == 18) {
			$item_num = '1';
			$cart_num = '3';
		}
		$green_fee_item_id = $this->Item->get_item_id($this->session->userdata('course_id') . '_seasonal_' . $item_num);
		$cart_fee_item_id = $this->Item->get_item_id($this->session->userdata('course_id') . '_seasonal_' . $cart_num);

		if (!empty($special)) {
			$special_id = $special['special_id'];
			$tee_time_info = $this->teetime->get_info($teetime_id);
			$special_green_fee = $this->Special->get_prices($teesheet_id, $date, $start_time, $holes, false, $tee_time_info->booking_class_id);
			$special_cart_fee = $this->Special->get_prices($teesheet_id, $date, $start_time, $holes, true, $tee_time_info->booking_class_id);
		} else {
			$special = false;
		}

		// Get price for teetime
		$price = $this->Pricing->get_price($teesheet_id, $price_class_id, $date, $start_time, $holes, false);
		if ($special !== false) {
			$price = $special_green_fee;
			$timeframe_id = 'special:' . $special_id;
		}

		$this->sale_lib->add_item($green_fee_item_id, 1, 0, $price, $date, $timeframe_id, $price_class_id, $item_num);
		$this->sale_lib->add_item_to_basket($green_fee_item_id, 1, 0, $price, $date, $timeframe_id, $price_class_id, $item_num);

		if ($include_cart) {

			// Get price for cart
			$price = $this->Pricing->get_price($teesheet_id, $price_class_id, $date, $start_time, $holes, true);
			if ($special !== false) {
				$price = $special_cart_fee;
				$timeframe_id = 'special:' . $special_id;
			}

			$this->sale_lib->add_item($cart_fee_item_id, 1, 0, $price, $date, $timeframe_id, $price_class_id, $cart_num);
			$this->sale_lib->add_item_to_basket($cart_fee_item_id, 1, 0, $price, $date, $timeframe_id, $price_class_id, $cart_num);
			return $special_id;
		}
		return $special_id;
	}

	/**
	 * @param $person_price_class_array
	 * @param $teesheet_id
	 * @param $start_time
	 * @param $dow
	 * @param $holes
	 * @param $quantity
	 * @param $course_id
	 * @param $carts
	 * @return mixed
	 */
	private function process_old_pricing($person_price_class_array, $teesheet_id, $start_time, $dow, $holes, $quantity, $course_id, $carts)
	{
		$item_num = '7';
		$cart_num = '5';
		$type = 'regular';
		$price_category_index = null;
		$not_holiday = true;
		if ($this->config->item('holidays') && $this->Appconfig->is_holiday())//TODO: build is_holiday function
		{
			$price_category_index = 7;
			$not_holiday = false;
		} else if ((int)$this->config->item('early_bird_hours_begin') <= $start_time && (int)$this->config->item('early_bird_hours_end') > $start_time)
			$price_category_index = 2;
		else if ((int)$this->config->item('morning_hours_begin') <= $start_time && (int)$this->config->item('morning_hours_end') > $start_time)
			$price_category_index = 3;
		else if ((int)$this->config->item('afternoon_hours_begin') <= $start_time && (int)$this->config->item('afternoon_hours_end') > $start_time)
			$price_category_index = 4;
		else if ((int)$start_time >= $this->config->item('super_twilight_hour'))
			$price_category_index = 6;
		else if ((int)$start_time >= $this->config->item('twilight_hour'))
			$price_category_index = 5;

		// Check against settings for if it is currently the weekend
		if (($dow == 5 && $this->config->item('weekend_fri')) || ($dow == 6 && $this->config->item('weekend_sat')) || ($dow == 0 && $this->config->item('weekend_sun')))
			$type = 'weekend';
		if ($holes == 9) {
			$cart_num = '1';
			$item_num = '3';
		} else if ($holes == 18) {
			$cart_num = '5';
			$item_num = '7';
		}
		if ($type == 'weekend') {
			$cart_num += 1;
			$item_num += 1;
		}

		$prices = $this->Green_fee->get_info();
		$item_id = $this->Item->get_item_id($this->session->userdata('course_id') . '_' . $item_num);
		$cart_id = $this->Item->get_item_id($this->session->userdata('course_id') . '_' . $cart_num);

		for ($i = 0; $i < $quantity; $i++) {
			//TODO: Handle person_price_class_array here... the prices don't trump the holiday, but they trump the time of day
			if (isset($person_price_class_array[$i]) && $person_price_class_array[$i] != '' && $not_holiday && $person_price_class_array[$i] != 'price_category_1') {
				$p_cat = $person_price_class_array[$i];
				$pci = str_replace('price_category_', '', $p_cat);
			} else {
				$pci = $price_category_index;
				$p_cat = 'price_category_' . $pci;
			}
			$this->sale_lib->add_item($item_id, 1, 0, $prices[$teesheet_id][$course_id . '_' . $item_num]->$p_cat, null, null, $pci, $item_num);
			$this->sale_lib->add_item_to_basket($item_id, 1, 0, $prices[$teesheet_id][$course_id . '_' . $item_num]->$p_cat, null, null, $pci, $item_num);
			if ($i < $carts) {
				$this->sale_lib->add_item($cart_id, 1, 0, $prices[$teesheet_id][$course_id . '_' . $cart_num]->$p_cat, null, null, $pci, $cart_num);
				$this->sale_lib->add_item_to_basket($cart_id, 1, 0, $prices[$teesheet_id][$course_id . '_' . $cart_num]->$p_cat, null, null, $pci, $cart_num);
			}
		}
		return $cart_id;
	}

	private function process_simulator()
	{
		$item_num = '7';
		$cart_num = '5';
		$play_time_length = $this->input->post('teetime_time');
		if ($play_time_length == '30')
			$item_num = '1';
		if ($play_time_length == '100')
			$item_num = '2';
		if ($play_time_length == '130')
			$item_num = '3';
		if ($play_time_length == '200')
			$item_num = '4';

		$item_id = $this->Item->get_item_id($this->session->userdata('course_id') . '_' . $item_num);

		$this->sale_lib->add_item($item_id);
		$this->sale_lib->add_item_to_basket($item_id);

		return false;
	}

	/**
	 * @param $teetime_id
	 * @param $person_price_class_array - This is a required list of which price class to use for which tee time
	 * @param $person_id_array - These are the ids of customers that you may want to link to a tee time, this can be empty
	 * @param $quantity
	 * @param $event_info
	 * @return mixed
	 */
	private function add_green_fee_seasonal_pricing($teetime_id, $person_price_class_array,$person_id_array = [],$quantity, $event_info = null)
	{
		$this->load->model("teetime");
		$teetime = $this->teetime->get_info($teetime_id);
		if(!empty($event_info)){
			foreach($event_info as $key => $value){
				$teetime->{$key} = $value;
			}
		}

		$teesheet_id = $teetime->teesheet_id;
		$event_type = $teetime->type;
		$start_time = $teetime->start;

		$start_hourminutes = (int)substr($start_time,8);
		$use_event_rate = $teetime->use_event_rate;
		$default_price_category = $teetime->default_price_category;
		$cart_fee = $teetime->default_cart_fee;
		$adjusted_start_time = $teetime->start + 1000000;
		$booking_source = $teetime->booking_source;
		$aggregate_group_id = $teetime->aggregate_group_id;
		$holes = $teetime->holes;
		$carts = $teetime->carts;
		$specials_enabled = $this->Special->teesheet_specials_enabled($teesheet_id);

		$cart_id = false;
		$date = DateTime::createFromFormat('Ymd', substr($adjusted_start_time, 0, 8));
		$date = $date->format('Y-m-d');

		// If course is using new Backbone POS
		if ($this->session->userdata('sales_v2') == '1') {

			$cart = $this->Cart_model->get_cart();

			// Clear current cart to prepare it for new sale
			if (!empty($cart['suspended_name'])) {
				$this->Cart_model->suspend($cart['suspended_name']);
			} else {
				$this->Cart_model->reset();
			}

			$this->Cart_model->save_cart(array('teetime_id' => $teetime_id));
		}

		// If reservation was booked online, check if a special was enabled
		$special = false;
		$special_id = false;
		if ($booking_source == 'online' && $specials_enabled) {
			$this->Special->aggregate = 0;
			if (!empty($aggregate_group_id)) {
				$this->Special->aggregate = 1;
			}
			$tee_time_info = $this->teetime->get_info($teetime_id);
			$special = $this->Special->get_special_by_date($teesheet_id, $date, $start_hourminutes, $tee_time_info->booking_class_id);
			$special_id = $special['special_id'];
		}

		if ($event_type != 'teetime' && $event_type !="shotgun" && $quantity > 5) {
			// Add all tee times together
			$person_id = false;
			if (!empty($person_id_array[0]['person_id'])) {
				$person_id = $person_id_array[0]['person_id'];
			}
			$position = !empty($person_id_array[0]['position']) ? $person_id_array[0]['position'] : 1;

			// If using event rate, all players will use that rate
			if ($use_event_rate == 1) {
				$price_class_id = $default_price_category;

				// If person price class is set, use it
			} else if (isset($person_price_class_array[0]) && $person_price_class_array[0] != '') {
				$price_class_id = $person_price_class_array[0];

				// If price class is not set, use default price class
			} else {
				$price_class = $this->Price_class->get_default();
				$price_class_id = $price_class['class_id'];
			}

			// If customer has a current valid pass, use it for the price class
			$applied_rule = null;
			if (empty($use_event_rate)) {
				$applied_rule = $this->get_applied_pass_rule($teesheet_id, $adjusted_start_time, $person_id);
				if (!empty($applied_rule['price_class_id'])) {
					$price_class_id = $applied_rule['price_class_id'];
				}
			}



			$include_cart = false;
			if (($event_type != 'teetime' && $cart_fee)) {
				$include_cart = true;
			}

			$timeframe = $this->Pricing->get_price_timeframe($teesheet_id, $price_class_id, $date, $start_hourminutes, false);

			// If course is using new Backbone POS
			if ($this->session->userdata('sales_v2') == '1') {

				$fee_params = $this->generate_feeparams($teesheet_id, $price_class_id, $holes, $date, $start_hourminutes, $special_id, $timeframe['timeframe_id']);
				$green_fee = $this->generate_greenfee_item($teesheet_id, $person_id, $fee_params, $position, $applied_rule, $holes);
				$green_fee['quantity'] = $quantity;
				$this->Cart_model->save_item(false, $green_fee);
				if ($include_cart) {
					$fee_params['type'] = 'cart_fee';

					$cart_fee_timeframe = $this->Pricing->get_price_timeframe($teesheet_id, $price_class_id, $date, $start_hourminutes, true);
					if ($cart_fee_timeframe['timeframe_id'] != $timeframe['timeframe_id']) {
						$fee_params['special_id'] = false;
						$fee_params['timeframe_id'] = $cart_fee_timeframe['timeframe_id'];
					}

					$cart_fee = $this->generate_cart_item($teesheet_id, $person_id, $fee_params, $green_fee, $applied_rule, $holes);

					$cartsToCheckin  = min([$carts,$quantity]);
					if($cartsToCheckin > 0){
						$cart_fee['quantity'] = $cartsToCheckin;
						$this->Cart_model->save_item(false, $cart_fee);
					}

				}
				$cart_id = $this->Cart_model->cart_id;
				return $cart_id;

				// If course is using old POS
			} else {
				$this->add_items_old_pos($teetime_id, $teesheet_id, $holes, $special, $date, $start_hourminutes, $price_class_id, $include_cart);
			}
		} else {
			// Loop through each tee time being added
			$this->db->trans_start();
			for ($i = 0; $i < $quantity; $i++) {

				$person_id = false;
				if (!empty($person_id_array[$i]['person_id'])) {
					$person_id = $person_id_array[$i]['person_id'];
				}
				$position = !empty($person_id_array[$i]['position']) ? $person_id_array[$i]['position'] : 1;

				// If using event rate, all players will use that rate
				if ($use_event_rate == 1) {
					$price_class_id = $default_price_category;

					// If person price class is set, use it
				} else if (isset($person_price_class_array[$i]) && $person_price_class_array[$i] != '') {
					$price_class_id = $person_price_class_array[$i];

					// If price class is not set, use default price class
				} else {
					$price_class = $this->Price_class->get_default();
					$price_class_id = $price_class['class_id'];
				}

				// If customer has a current valid pass, use it for the price class
				$applied_rule = null;
				if (empty($use_event_rate)) {
					$applied_rule = $this->get_applied_pass_rule($teesheet_id, $adjusted_start_time, $person_id);
					if (!empty($applied_rule['price_class_id'])) {
						$price_class_id = $applied_rule['price_class_id'];
					}
				}



				$include_cart = false;
				if ($i < $carts || ($event_type != 'teetime' && $cart_fee)) {
					$include_cart = true;
				}
				$start_hourminutes = (int)substr($start_time,8);
				$timeframe = $this->Pricing->get_price_timeframe($teesheet_id, $price_class_id, $date, $start_hourminutes, false);

				// If course is using new Backbone POS
				if ($this->session->userdata('sales_v2') == '1') {

					$fee_params = $this->generate_feeparams($teesheet_id, $price_class_id, $holes, $date, $start_hourminutes, $special_id, $timeframe['timeframe_id']);
					$green_fee = $this->generate_greenfee_item($teesheet_id, $person_id, $fee_params, $position, $applied_rule, $holes);
					$this->Cart_model->save_item(false, $green_fee);

					if ($include_cart && ($teetime->paid_carts < $carts || $event_type != 'teetime')) {
						$fee_params['type'] = 'cart_fee';

						$cart_fee_timeframe = $this->Pricing->get_price_timeframe($teesheet_id, $price_class_id, $date, $start_hourminutes, true);
						if ($cart_fee_timeframe['timeframe_id'] != $timeframe['timeframe_id']) {
							$fee_params['special_id'] = false;
							$fee_params['timeframe_id'] = $cart_fee_timeframe['timeframe_id'];
						}
						$cart_fee = $this->generate_cart_item($teesheet_id, $person_id, $fee_params, $green_fee, $applied_rule, $holes);
						$this->Cart_model->save_item(false, $cart_fee);
					}
					$cart_id = $this->Cart_model->cart_id;

					// If course is using old POS
				} else {
					$this->add_items_old_pos($teetime_id, $teesheet_id, $holes, $special, $date, $start_hourminutes, $price_class_id, $include_cart);
				}
			}
			$this->db->trans_complete();
		}

		return $cart_id;
	}
}
?>
