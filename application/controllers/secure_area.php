<?php

class Secure_area extends CI_Controller
{
	/*
	Controllers that are considered secure extend Secure_area, optionally a $module_id can
	be set to also check if a user can access a particular module in the system.
	*/
	function __construct($module_id = null)
	{
		parent::__construct();

		$this->load->model('Employee');

		$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

		if ($request->headers->get("x-authorization")){
			$passed_token = explode(" ",$request->headers->get("x-authorization"));
			$passed_token = $passed_token[1];
			$token = new \fu\auth\json_web_token();
			$token->createToken();
			$token->setToken($passed_token);
			if(!$token->validate()){
				redirect('no_access/' . $module_id);
			}

			$employee_info = $this->Employee->get_info($token->getClaim("uid"),true);
			$this->Employee->login($employee_info->username,$employee_info->password,true);

			if($token->hasClaim("priceClassId") != null){
				$this->session->set_userdata("price_class_id",$token->getClaim("priceClassId"));
			}
		}

		if (!$this->Employee->is_logged_in()) {
			// explicitly sets the mode to default {still for verification}
			$this->session->set_userdata('login_mode', false);
			$mobile = $this->input->get('mobile') ? $this->input->get('mobile') : $this->input->post('mobile');
			redirect("login?mobile=$mobile");
		}

		if (!$this->Employee->has_permission($module_id, $this->Employee->get_logged_in_employee_info()->person_id) || (!$this->permissions->is_super_admin() && !$this->permissions->course_has_module($module_id))) {
			redirect('no_access/' . $module_id);
		}

		//load up global data
		$logged_in_employee_info = $this->Employee->get_logged_in_employee_info();
		$data['allowed_modules'] = $this->Module->get_allowed_modules($logged_in_employee_info->person_id);
		$data['user_info'] = $logged_in_employee_info;
		$this->load->vars($data);
	}
}
