<?php
// New online booking controller
class Booking extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('teesheet');
        $this->load->model('customer');
        $this->load->model('course');
        $this->load->model('Booking_class');
        $this->load->model('invoice');
        $this->load->model('user');
        $this->load->model('Special');
        $this->load->model('course_group');
        $this->load->model('online_hours');

        $this->total_transactions = 0;
	}

	private function filter_course_data($course_data){
		$validParams = array(
			'course_id',
			'active',
			'area_id',
			'name',
			'address',
			'city',
			'state',
			'postal',
			'zip',
			'country',
			'website',
			'area_code',
			'timezone',
			'open_time',
			'close_time',
			'latitude_centroid',
			'longitude_centrod',
			'phone',
			'online_booking',
			'online_booking_protected',
			'online_giftcard_purchases',
			'booking_rules',
			'email',
			'no_show_policy',
			'include_tax_online_booking',
			'credit_card_provider',
			'foreup_discount_percent',
			'customer_credit_nickname',
			'member_balance_nickname',
			'online_booking_welcome_message'
		);

        if($course_data['apriva_username']){
            $course_data['credit_card_provider'] = 'apriva';
        }else if($course_data['ets_key']){
            $course_data['credit_card_provider'] = 'ets';
        }else if($course_data['mercury_id']){
			$course_data['credit_card_provider'] = 'mercury';
		}else if($course_data['element_account_id']){
			$course_data['credit_card_provider'] = 'element';
		}else{
			$course_data['credit_card_provider'] = false;
		}

		if(!empty($course_data['use_ets_giftcards'])){
			$course_data['online_giftcard_purchases'] = 0;
		}

		return elements($validParams, $course_data, '');
	}

	private function output($data, $from_iframe){
		
		$code = '';
		if($from_iframe){
			$code .= '<script>
				App = window.parent.App; 
				ga = window.parent.ga; 
				$ = window.parent.$;
			';
		}

		$code .= $data;
		if($from_iframe){
			$code .= '</script>';
		}
		
		echo $code;
	}

	function index($course_id, $passed_schedule_id = false){

		// Log employee out of foreup sofware to prevent employee from accessing other courses data
		$this->session->unset_userdata('person_id');

		$this->session->unset_userdata('group_id');
		$this->session->unset_userdata('group_course_ids');

		$course_info = (array) $this->course->get_info($course_id);
		if(empty($course_info['course_id'])){
			$this->load->view('booking/reservations');
			redirect(base_url()."/404.html");
			return [];
		}
		$filtered_course_info = $this->filter_course_data($course_info);
		
		$timezone = null;
		if($course_info['timezone']){
			date_default_timezone_set($course_info['timezone']);
			$timezone = new DateTimeZone($course_info['timezone']);
		}
		
		$this->session->set_userdata('course_id', $course_id);
		$default_schedule = array();
		$booking_class_exists = false;

		// Get schedules with online booking enabled
		$schedules = $this->teesheet->get_teesheets($course_id, array('online_only' => true));

		$data = array();
		$data['course'] = $filtered_course_info;
		$data['page_name'] = $filtered_course_info['name'];

		$user_data = array();
		$user_data['logged_in'] = false;

		// If user is logged in
		if($this->session->userdata('customer_id')){
			$this->load->model('user');
			$this->load->model('v2/Raincheck_model');

			$primary_customer_id = $this->session->userdata('customer_id');
			$user_data = (array) $this->customer->get_info($primary_customer_id, $course_id);

			unset($user_data['password']);

			$user_data['user_id'] = (int) $this->session->userdata('customer_id');
			$user_data['logged_in'] = true;
			$this->load->model('Customer_credit_card');
			$user_data['credit_cards'] = $this->Customer_credit_card->get($primary_customer_id, false, $course_id);

			$this->load->model('user');
			$user_data['reservations'] = $this->user->get_teetimes($primary_customer_id, 'upcoming', $course_id);

			$transactions = $this->user->get_account_transactions($primary_customer_id, $course_id);
			$user_data['account_transactions'] = $transactions;
			$user_data['account_transactions_total'] = $this->user->total_transactions;

			$invoices = $this->invoice->get_all($primary_customer_id);
			$user_data['invoices'] = $invoices->result_array();

			$user_data['gift_cards'] = $this->user->get_giftcards($primary_customer_id, $course_id);
			$user_data['rainchecks'] = $this->Raincheck_model->get([
				'person_id' => $primary_customer_id
			], $course_id);

			$user_data['minimum_charge_transactions'] = $this->user->get_minimum_charge_transactions($primary_customer_id, $course_id);
			$user_data['minimum_charge_totals'] = $this->user->get_minimum_charge_totals($primary_customer_id, $course_id);
			$user_data['booking_class_ids'] = $this->Booking_class->get_customer_booking_class_ids($primary_customer_id);
		}
		$data['user'] = $user_data;

		// If no schedules were found, display a welcome page in place of tee time page
		if(empty($schedules)){

			$data['filter'] = false;
			$data['schedules'] = false;
			$settings['online_booking_protected'] = 0;
			$settings['currency_symbol'] = $course_info['currency_symbol'];
			$settings['website'] = $course_info['website'];
			$data['settings'] = $settings;
			$this->load->view('booking/reservations', $data);
			return false;
		}

		if($passed_schedule_id){
			$schedule_id = (int) $passed_schedule_id;
			$this->session->set_userdata('booking_class_id', null);
			$this->session->set_userdata('booking_class_info', null);

		}else if($this->session->userdata('schedule_id')){
			$schedule_id = (int) $this->session->userdata('schedule_id');

		}else{
			$schedule_id = $schedules[0]['teesheet_id'];
		}

		// Loop through schedules and retrieve booking classes for each
		$selected_found = false;
		foreach($schedules as &$schedule){
			$schedule['booking_classes'] = $this->Booking_class->get_all($schedule['teesheet_id']);
			
			foreach($schedule['booking_classes'] as $key => $booking_class){
				if($booking_class['active'] == 0){
					unset($schedule['booking_classes'][$key]);
				}
				if($this->session->userdata('booking_class_id') && $booking_class['booking_class_id'] == $this->session->userdata('booking_class_id')){
					$this->session->set_userdata('booking_class_info', (object) $booking_class);
				}
			}
			$schedule['booking_classes'] = array_values($schedule['booking_classes']);

			if(!empty($schedule['booking_classes'])){
				$booking_class_exists = true;
			}

			if($schedule_id == $schedule['teesheet_id']){
				$default_schedule = $schedule;
				$schedule['selected'] = true;
				$selected_found = true;
			}
		}
		$data['schedules'] = $schedules;

		if(!$selected_found){
			$schedules[0]['selected'] = true;
			$this->session->unset_userdata('schedule_id');
			$schedule_id = (int) $schedules[0]['teesheet_id'];
			$default_schedule = $schedules[0];
		}
		$default_schedule['show_full_details'] = 0;

		if(!$booking_class_exists){
			$this->session->set_userdata('booking_class_id', null);
			$this->session->set_userdata('booking_class_info', null);
		}

		$date = new DateTime();
		if((int) $default_schedule['block_online_booking'] == 1 && !$this->online_hours->is_available($schedule_id, $date)){
			
			$hours = $this->online_hours->get($schedule_id);
			
			$day_of_week = substr(strtolower(date('l')), 0, 3);
			$open = DateTime::createFromFormat('Hi', str_pad($hours[$day_of_week.'_open'], 4, '0', STR_PAD_LEFT));
			$close = DateTime::createFromFormat('Hi', str_pad($hours[$day_of_week.'_close'], 4, '0', STR_PAD_LEFT));
			
			$data = array(
				'golf_course_name' => $course_info['name'], 
				'closed' => true,
				'open_time' => $open->format('g:ia'),
				'close_time' => $close->format('g:ia')
			);
			$this->load->view('booking/unavailable', $data);
			return false;			
		}
		
		$time = 'morning';
		if((int) date('H') >= 14){
			$time = 'evening';
		} else if((int) date('H') >= 10){
			$time = 'midday';
		}

		$default_date = date('m-d-Y');
		
		// Override tee sheet settings with booking class settings
		$close_time = (int) $default_schedule['online_close_time'];
		if($this->session->userdata('booking_class_info')){
			$close_time = (int) $this->session->userdata('booking_class_info')->online_close_time;
			$default_schedule['limit_holes'] = (int) $this->session->userdata('booking_class_info')->limit_holes;
			$default_schedule['minimum_players'] = (int) $this->session->userdata('booking_class_info')->minimum_players;
			$default_schedule['days_in_booking_window'] = (int) $this->session->userdata('booking_class_info')->days_in_booking_window;
			$default_schedule['booking_carts'] = (int) $this->session->userdata('booking_class_info')->booking_carts;
			$default_schedule['show_full_details'] = (int) $this->session->userdata('booking_class_info')->show_full_details;
			$default_schedule['hide_online_prices'] = (int) $this->session->userdata('booking_class_info')->hide_online_prices;
		}
		
		$holes = (int) $default_schedule['holes'];
		if($default_schedule['limit_holes'] != 0){
			$holes = min($default_schedule['limit_holes'], $holes);
		}

		// If tee sheet close time is greater than course close time,
		// force it to course time
		if($close_time > $course_info['close_time']){
			$close_time = (int) $course_info['close_time'];
		}
		
		// If it is past the close time, default date to tomorrow
        if(($close_time - 100) <= (int) date('H').'00'){
			$default_date = date('m-d-Y', strtotime('+1 day'));
		}
		
		$players = (int) $default_schedule['minimum_players'];
		if($default_schedule['show_full_details'] == 1){
			$players = 0;
		}
		
		$this->load->model('promotion');
		$has_promos = $this->promotion->has_online_promos($course_id);

		$filter['schedule_id'] = (int) $schedule_id;
		$filter['course_id'] = (int) $course_id;
		$filter['time'] = $time;
		$filter['date'] = $default_date;
		$filter['holes'] = $holes;
		$filter['players'] = $players;
		$filter['booking_class'] = $this->session->userdata('booking_class_id');
		$data['filter'] = $filter;

		$settings['minimum_players'] = (int) $default_schedule['minimum_players'];
		$settings['limit_holes'] = (int) $default_schedule['limit_holes'];
		$settings['days_in_booking_window'] = (int) $default_schedule['days_in_booking_window'];
		$settings['booking_carts'] = (int) $default_schedule['booking_carts'];
		$settings['show_full_details'] = ($default_schedule['show_full_details'] == 1);
		$settings['hide_online_prices'] = ($default_schedule['hide_online_prices'] == 1);
		$settings['online_booking_protected'] = 0;
		$settings['online_close_time'] = $close_time;
		$settings['has_promo_codes'] = $has_promos;
		$settings['currency_symbol'] = $course_info['currency_symbol'];
		$settings['enable_captcha_online'] = $course_info['enable_captcha_online'];
		$settings['reservation_limit_per_day'] = $course_info['reservation_limit_per_day'];
		if(!empty($filter['booking_class'])){
			$settings['online_booking_protected'] = (int) $this->session->userdata('booking_class_info')->online_booking_protected;
		}
		$data['settings'] = $settings;

		$this->load->view('booking/reservations', $data);
	}
	
	// Loads an aggregate tee time page
	function group($group_id = null){
		
		$data = array('page_name' => '');
		if(empty($group_id)){
			$this->load->view('booking/unavailable', $data);
			return false;
		}
		$courses = $this->course->get_group_courses($group_id);
		$course_group = $this->course_group->get($group_id);
		
		if(empty($courses)){
			$this->load->view('booking/unavailable', $data);

			return false;
		}
		$data['page_name'] = $course_group['label'];

		$group_course_ids = array();
		foreach($courses as &$course){
			$course = $this->filter_course_data($course);
			$group_course_ids[] = (int) $course['course_id'];
		}

		if(!empty($courses[0]['timezone'])){
			date_default_timezone_set($courses[0]['timezone']);
		}
		
		$this->session->set_userdata('group_id', $group_id);
		$this->session->set_userdata('group_course_ids', $group_course_ids);
		$this->session->unset_userdata('course_id');
		
		// Get all schedules in group with online booking enabled 
		// and are set to show on aggregate page
		$schedules = $this->teesheet->get_teesheets(null, array(
			'online_only' => true, 
			'aggregate_only' => true, 
			'group_id' => $group_id
		));
		
		// If no schedules were found show error page
		$page_data = array();
		if(empty($schedules)){
			$this->load->view('booking/unavailable', $page_data);
			return false;
		}
		$this->session->set_userdata('booking_class_id', null);
		$this->session->set_userdata('booking_class_info', null);

		$days_in_booking_window = 1;
		foreach($schedules as $schedule){
			if((int) $schedule['days_in_booking_window'] > $days_in_booking_window){
				$days_in_booking_window = (int) $schedule['days_in_booking_window'];
			}
		}

		$data['schedules'] = array();
		$data['schedules'][] = array(
			'teesheet_id' => 0,
			'course_id' => 0,
			'title' => '- All Courses -',
			'booking_carts' => 1,
			'minimum_players' => 1,
			'limit_holes' => 0,
			'holes' => 18,
			'days_in_booking_window' => $days_in_booking_window,
			'online_booking_protected' => 0,
			'selected' => true,
			'booking_classes' => [],
			'online_close_time' => '2399',
			'online_open_time' => '0000'
		);
		$data['schedules'] = array_merge($data['schedules'], $schedules);
		$data['courses'] = $courses;

		$user_data = array();
		$user_data['logged_in'] = false;

		// If user is logged in
		if($this->session->userdata('customer_id')){
			$this->load->model('user');
			$user_data = (array) $this->user->get_info($this->session->userdata('customer_id'));
			$username = $this->user->get_username($this->session->userdata('customer_id'));
			$user_data['username'] = $username;

			$user_data['user_id'] = (int) $this->session->userdata('customer_id');
			$user_data['logged_in'] = true;
			$user_data['reservations'] = $this->user->get_teetimes($user_data['user_id'], 'upcoming', $group_course_ids);
		}
		if(isset($user_data['password'])){
			unset($user_data['password']);
		}
		$data['user'] = $user_data;
		
		$time = 'morning';
		if((int) date('H') >= 14){
			$time = 'evening';
		} else if((int) date('H') >= 10){
			$time = 'midday';
		}

		$default_date = date('m-d-Y');
		$holes = 18;
		$players = 0;
		
		$filter['schedule_id'] = 0;
		$filter['course_id'] = 0;
		$filter['time'] = $time;
		$filter['date'] = $default_date;
		$filter['holes'] = $holes;
		$filter['players'] = $players;
		$filter['booking_class'] = false;
		$filter['group_id'] = (int) $group_id;

		$settings['minimum_players'] = 1;
		$settings['limit_holes'] = 0;
		$settings['days_in_booking_window'] = $days_in_booking_window;
		$settings['booking_carts'] = 1;
		$settings['show_full_details'] = false;
		$settings['hide_online_prices'] = false;
		$settings['online_booking_protected'] = 0;
		$settings['online_close_time'] = '';

		if(!empty($group_course_ids)){
			$course_info = $this->course->get_info( $group_course_ids[0]);
			$settings['currency_symbol'] = $course_info->currency_symbol;
		}
		$data['group_id'] = $group_id;
		$data['filter'] = $filter;
		$data['settings'] = $settings;

		$this->load->view('booking/reservations', $data);		
	}

	function credit_card_window($course_id, $action = false){

		$this->load->model('online_booking');
		$this->load->model('user');

		$players = $this->input->get_post('players');
		$holes = $this->input->get_post('holes');
		$carts = $this->input->get_post('carts');
		$time = $this->input->get_post('time');
		$schedule_id = $this->input->get_post('schedule_id');
		$promo_code = $this->input->get_post('promo_code');
		$customer_id = $this->session->userdata('customer_id');
		$recipient = $this->input->get_post('recipient');
		$teetime_id = $this->input->get_post('teetime_id');
		$booking_class_id = $this->input->get_post('booking_class_id');
		$available_spots = $this->input->get_post('available_spots');
        $player_list = $this->input->get_post('player_list');
        $pending_reservation_id = $this->input->get_post('pending_reservation_id');

		$this->session->unset_userdata('giftcard_data');
		$this->session->unset_userdata('reservation_data');

		if($carts === false || $carts === 'false' || $carts === '0'){
			$carts = false;
		}else{
			$carts = true;
		}

		if($booking_class_id === false || $booking_class_id === 'false' || $booking_class_id === '0'){
			$booking_class_id = false;
		}else{
			$booking_class_id = (int) $booking_class_id;
		}		
		$course = (array) $this->course->get_info($course_id);

		// Check if user is logged in
		if(empty($customer_id)){
			$code = "alert('You must be logged in');
				$('#modal').modal('hide');";
			$this->output($code, true);
			return false;
		}

		$this->session->set_userdata('course_id', $course['course_id']);	
		$price_class_id = false;
		$terminal_id = $this->online_booking->get_purchase_terminal($course['course_id']);

		// If golfer is purchasing tee time through ForeUp (tee time share),
		// Use ForeUp's mercury account and add in discount
		$foreup_discount = 0;
		$teetime_share = false;
		if($action == 'purchase' && $recipient == 'foreup'){
			$foreup_discount = $course['foreup_discount_percent'];
			$teetime_share = true;
		}

		// If purchasing tee time (either from the golf course or from ForeUp) 
		// OR purchasing a booking fee 
		if($action == 'purchase' || $action == 'purchase-fee'){

			// Determine which price class we should use
			$is_aggregate = false;
			if(!$this->session->userdata('group_id')){
				$price_class_id = $this->online_booking->get_price_class($booking_class_id, $customer_id);
			}else{
				$is_aggregate = true;
			}

			// Make sure we can book this tee time
			if(!$this->online_booking->validate_reservation(
				$schedule_id,
				$time,
				$holes,
				$players,
				$carts,
				true,
				$booking_class_id,
				$is_aggregate,
				$customer_id
			)){
				$code = "alert('{$this->online_booking->response['msg']}');
					$('#modal').modal('hide');";
				$this->output($code, true);
				return false;	
			}

			$cart_data = $this->online_booking->load_cart(
				$course_id, 
				$schedule_id, 
				$time, 
				$holes, 
				$players, 
				$carts, 
				$customer_id, 
				$price_class_id, 
				$promo_code, 
				$foreup_discount,
                $booking_class_id
			);
			
			$total = abs($cart_data['total_due']);
			$totals = $cart_data;
			$total_fee = 0;
			$promo_id = $cart_data['promo_id'];

			// Check if course requires a booking fee to book online
			$booking_fee = false;
			if($recipient != 'foreup'){
				$customer = $this->Customer->get_info($customer_id);
				$booking_fee = $this->online_booking->get_booking_fee($course_id, $schedule_id, $booking_class_id, ($customer->taxable == 1), $players);
			}

			// Create a new shopping cart with the booking fee (if paying for fee)
			if(!empty($booking_fee)){

				// If we are only purchasing the booking fee (not paying for the tee time)
				// Then reset the cart so it ONLY has the booking fee in it
				if($action == 'purchase-fee'){
					$this->Cart_model->course_id = $course_id;
					$this->Cart_model->reset();
				}

				// Add booking fee to the cart
				$this->Cart_model->save_item(null, [
					'selected' => true,
					'item_type' => 'item',
					'item_id' => $booking_fee['item_id'],
					'unit_price' => $booking_fee['unit_price'],
					'quantity' => $booking_fee['quantity'],
					'unit_price_includes_tax' => $booking_fee['unit_price_includes_tax']
				]);

				$this->Cart_model->save_customer($customer_id, ['selected' => true]);

				$cart_data = $this->online_booking->get_cart_totals();
				$total = abs($cart_data['total_due']);
				$total_fee = $booking_fee['total'];
			}

			$reservation_data = array(
				'time' => $time,
				'players' => $players,
				'holes' => $holes,
				'carts' => $carts,
				'schedule_id' => $schedule_id,
				'customer_id' => $customer_id,
				'customer' => $this->user->get_info($customer_id),
				'price_class' => false,
				'course_id' => $course_id,
				'course' => array(
                    'course_id'=>$course['course_id'],
                    'name'=>$course['name'],
                    'email'=>$course['email'],
                    'phone'=>$course['phone'],
					'reservation_email'=>$course['reservation_email']
				),
				'action' => $action,
				'promo_code' => $promo_code,
				'promo_id' => $promo_id,
				'group_id' => $this->session->userdata('group_id'),
				'recipient' => $recipient,
				'teetime_id' => $teetime_id,
				'price_class_id' => $price_class_id,
				'available_spots' => $available_spots,
				'player_list' => $player_list,
				'total_fee' => $total_fee,
				'totals' => $totals,
                'pending_reservation_id' => $pending_reservation_id
			);
			$this->session->set_userdata('reservation_data', $reservation_data);			
		
		// If purchasing a giftcard online
		}else if($action == 'purchase_giftcard'){

			$this->load->model('v2/Cart_model');
			$this->load->model('v2/Item_model');
			$this->load->model('v2/Giftcard_model');

			$total = (float) $this->input->get_post('total');

			if(empty($total) || $total < 0){
				echo json_encode(['msg' => 'Gift card amount is not valid'], 400);
				return false;
			}
			$total = (float) round($total, 2);

			$giftcard_number = $this->Giftcard_model->get_new_online_number($course_id, $customer_id);
			$giftcard_item = $this->Item_model->get_special_item('online_giftcard');

			$customer = $this->user->get_info($customer_id);
			
			$giftcard_data = array(
				'send' => $this->input->get_post('send'),
				'from' => $customer,
				'to' => $this->input->get_post('to'),
				'customer_id' => $customer_id,
				'amount' => $total,
				'organization' => [
					'name' => $course['name'],
					'email' => $course['email'],
					'phone' => $course['phone']
				],
				'course_id' => $course_id,
				'giftcard_number' => $giftcard_number
			);

			$this->session->set_userdata('giftcard_data', $giftcard_data);			

			$details = 'Purchased online '.date('m/d/Y g:ia').' by '.$customer['first_name'].' '.$customer['last_name'];
			if(!empty($giftcard_data['send'])){
				$details .= ' for '.$giftcard_data['to']['name'].' ('.$giftcard_data['to']['email'].')';
			}
			$giftcard_item['item_type'] = 'giftcard';
			$giftcard_item['quantity'] = 1;
			$giftcard_item['unit_price'] = $total;
			$giftcard_item['selected'] = true;
			$giftcard_item['params'] = [
				'customer' => [
					'person_id' => $customer_id
				],
				'action' => 'new',
				'details' => $details,
				'giftcard_number' => $giftcard_number
			];
			
			// Create new shopping cart with the gift card
			$this->Cart_model->reset();
			$this->Cart_model->save_item(null, $giftcard_item);
			$this->Cart_model->save_customer($customer_id, ['selected' => true]);

		// If saving credit card to account
		}else{
			$this->session->unset_userdata('reservation_data');
			$total = 0;
		}

		// Apriva payments
        if ($recipient != 'foreup' && !empty($course['apriva_username'])) {
            $invoice_id = $this->Sale->add_credit_card_payment(array('tran_type'=>'Sale', 'frequency'=>'OneTime'));

            $data = array();
            $processor = \fu\credit_cards\ProcessorFactory::create("apriva", $data);
            if($action != 'add_card'){
                $params = array(
                    'action' => $action, // purchase or purchase-fee
                    'command_type' => 'credit',
                    'transaction_type' => 'sale',
                    'trans_amount' => number_format($total,2),//$total,
                    'tax_amount' => '0',
                    'tip_amount' => '0',
                    'ref_no' => $invoice_id
                );

                $processor->initiatePayment($params, $terminal_id, false, 'online');
            }else {

            }
            $this->session->set_userdata('credit_card_payment', array(
                'merchant' => 'apriva',
                'amount' => $total,
                'invoice_id' => $invoice_id,
                'apriva_username' => '',
                'action' => $action
            ));
        }
        // ETS payments
        else if ($recipient != 'foreup' && !empty($course['ets_key'])){
			
			$api_keys = $this->online_booking->get_credit_card_api_keys($course['course_id'], 'ets', $terminal_id);

			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($api_keys['key']);

			$invoice_id = $this->Sale->add_credit_card_payment(array('ets_id' => $course['ets_key'], 'tran_type'=>'Sale', 'frequency'=>'OneTime'));

			$data = array(
				'total' => $total,
				'action' => $action,
				'processor' => 'ETS',
				'invoice_id' => $invoice_id
			);

			// If making a purchase
			if($action != 'add_card'){
				$session = $payment->set('action', 'session')
					->set('amount', $total)
					->set('store.Primary', $invoice_id)
					->set('PaymentMethods', "CreditCard")
					->send();					
			
			// If just saving a credit card
			}else{
				$session = $payment->set('action', 'session')
					->set('isSave', 'true')
					->send();
			}

			if($session->id){
				$return_code = '';
				$this->session->set_userdata('ets_session_id', (string) $session->id);
				$data['session'] = $session;

				$this->session->set_userdata('credit_card_payment', array(
					'merchant' => 'ets',
					'amount' => $total,
					'invoice_id' => $invoice_id,
					'ets_key' => $api_keys['key'],
					'action' => $action
				));

				$viewData = $this->load->view('booking/ets_capture_form', $data, true);

			}else{
				$viewData = $this->load->view('booking/capture_form_error', $data, true);
			}

		// Mercury payments
		}else if(!empty($course['mercury_id']) || $teetime_share){

			$api_keys = $this->online_booking->get_credit_card_api_keys($course['course_id'], 'mercury', $terminal_id);
			if($teetime_share){
				$api_keys['id'] = $this->config->item('foreup_mercury_id');
				$api_keys['password'] = $this->config->item('foreup_mercury_password');
			}

			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();

			$HC->set_merchant_credentials($api_keys['id'], $api_keys['password']);
			$HC->set_response_urls('booking/process_credit_card', 'booking/credit_card_cancel');

            if ($course['country'] == 'CA' && $teetime_share) {
                // Convert price from Canadian Dollars to US Dollars
                $result = json_decode(file_get_contents('http://api.fixer.io/latest?symbols=USD&base=CAD'));
                $rate = $result->rates->USD;
                if (!empty($rate) && is_numeric($rate)) {
                    $charge_total = round($total * $rate, 2);
                }
                else {
                    $charge_total = $total;
                }
                $foreign_currency_type = 'CAD';
                $foreign_currency_symbol = '$';
                $foreign_exchange_rate = $rate;
            } else {
                $charge_total = $total;
                $foreign_currency_type = '';
                $foreign_currency_symbol = '';
                $foreign_exchange_rate = 1;
            }
			if($action != 'add_card'){
                $HC->set_avs_fields('Both');
				$initialize_results = $HC->initialize_payment($charge_total, '0.00', 'Sale', 'POS', 'OneTime');
			}else{
				$initialize_results = $HC->initialize_payment('0.00', '0.00', 'ZeroAuth', 'POS', 'Recurring');
			}

			if((int) $initialize_results->ResponseCode == 0){
				$invoice_id = (int) $HC->invoice;
				$payment_id = (string) $initialize_results->PaymentID;

				$this->session->set_userdata('credit_card_payment', array(
				    'foreign_total'=>$total,
                    'foreign_currency_type'=>$foreign_currency_type,
                    'foreign_currency_symbol'=>$foreign_currency_symbol,
                    'foreign_exchange_rate'=>$foreign_exchange_rate,
					'amount' => $charge_total,
					'action' => $action,
					'merchant' => 'mercury',
					'invoice_id' => $invoice_id,
					'payment_id' => $payment_id,
					'mercury_id' => $api_keys['id'],
					'mercury_password' => $api_keys['password']
				));

				$url = $HC->base_url().'/mobile/mCheckout.aspx';
				$data = array(
					'url' => $url, 
					'payment_id' => $payment_id
				);

				$viewData = $this->load->view('booking/mercury_capture_form', $data, true);				
			}
		
		// Element payments
		}else if(!empty($course['element_account_id'])){
			
			$api_keys = $this->online_booking->get_credit_card_api_keys($course['course_id'], 'element', $terminal_id);
			$this->load->library('v2/Element_merchant');
			$billing_address = '';
			$billing_zipcode = '';
			
			// Add row to credit card payments table (to be updated in future)
			$this->load->model('v2/Sale_model');
			$cc_invoice_id = $this->Sale_model->save_credit_card_payment(null, array('element_account_id' => $api_keys['account_id']));
			
			$init_params = array(
				'account_id' => $api_keys['account_id'],
				'account_token' => $api_keys['account_token'],
				'application_id' => Element_merchant::APPLICATION_ID,
				'acceptor_id' => $api_keys['acceptor_id'],
				'terminal_id' => $api_keys['application_id']
			);
			
			// Initialize payment library
			$this->element_merchant->init(
				new GuzzleHttp\Client(),
				$init_params
			);

			$this->element_merchant->set_terminal(array(
				'CardholderPresentCode' => 		Element_merchant::CARDHOLDER_PRESENT_CODE_ECOMMERCE,
				'CardInputCode' => 				Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED,
				'CardPresentCode' => 			Element_merchant::CARD_PRESENT_CODE_NOT_PRESENT,
				'MotoECICode' => 				Element_merchant::MOTO_ECI_CODE_NON_AUTHENTICATED_SECURE_ECOMMERCE_TRANSACTION,
				'TerminalCapabilityCode' => 	Element_merchant::TERMINAL_CAPABILITY_CODE_KEY_ENTERED,
				'TerminalEnvironmentCode' => 	Element_merchant::TERMINAL_ENVIRONMENT_CODE_ECOMMERCE,
				'TerminalType' => 				Element_merchant::TERMINAL_TYPE_ECOMMERCE
			));
			
			$params = array(
				'return_url' => 						site_url('booking/process_credit_card'),
				'market_code' => 						Element_merchant::MARKET_CODE_ECOMMERCE,
				'reference_number' => 					$cc_invoice_id,
				'ticket_number' => 						$cc_invoice_id,
				'billing_address1' =>					$billing_address,
				'billing_zipcode' => 					$billing_zipcode
			);

			if($action != 'add_card'){
				$params['amount'] = $total;
				$params['transaction_setup_method'] = Element_merchant::TRANSACTION_SETUP_CREDIT_CARD_SALE;
			
			}else{
				$params['transaction_setup_method'] = Element_merchant::TRANSACTION_SETUP_PAYMENT_ACCOUNT_CREATE;
				$params['payment_account_reference_number'] = 0;
				$params['payment_account_type'] = Element_merchant::PAYMENT_ACCOUNT_CREDIT_CARD;
			}
			
			// Get URL for payment iframe
			$this->element_merchant->init_hosted_payment($params);
			$url = $this->element_merchant->hosted_payment_url();

			$this->session->set_userdata('credit_card_payment', array(
				'amount' => $total,
				'action' => $action,
				'invoice_id' => $cc_invoice_id,
				'merchant' => 'element',
				'element_account_id' => $api_keys['account_id'],
				'element_account_token' => $api_keys['account_token'],
				'element_acceptor_id' => $api_keys['acceptor_id'],
				'element_application_id' => $api_keys['application_id']				
			));	
			
			$viewData = $this->load->view('booking/element_capture_form', array('url' => $url), true);
		}

		echo $viewData;
	}

	// Captures a new credit card submitted from iframe
	private function capture_credit_card(){
		
		$cc_payment = $this->session->userdata('credit_card_payment');
		
		// If there was an error processing the card
		if($cc_payment['merchant'] == 'mercury' && $this->input->post('ReturnCode') != 0){
			$message = $this->input->post('ReturnMessage');
			echo "<script>window.parent.alert('{$message}'); window.parent.$('#modal').modal('hide');</script>";
			return false;
		
		}else if($cc_payment['merchant'] == 'element' && 
			(
				$this->input->get('HostedPaymentStatus') != 'Complete' || 
				$this->input->get('ExpressResponseCode') != 0
			)
		){
			$message = $this->input->post('ExpressResponseMessage');
			echo "<script>window.parent.parent.alert('{$message}'); window.parent.parent.$('#modal').modal('hide');</script>";
			return false;
		}

		$person_id = $this->session->userdata('customer_id');

		$this->load->model('Customer_credit_card');
		$return_data = array();
		$credit_card_id = (int) $this->Customer_credit_card->capture_card($person_id, $return_data);
		
		if(empty($credit_card_id)){
			echo "<script>
				window.parent.alert('There was an error saving your card, please try again'); 
				window.parent.$('#modal').modal('hide');
			</script>";
			return false;			
		}

		$credit_card = (array) $this->Customer_credit_card->get_info($credit_card_id);

		// Add new credit card to user's profile
		echo "<h2 style='text-align: center; color: #999; margin-top: 150px;'>Please wait...</h2>
		<script>
			var card = {};
			card.credit_card_id = {$credit_card_id};
			card.masked_account = '{$credit_card['masked_account']}';
			card.expiration = '{$credit_card['expiration']}';
			card.card_type = '{$credit_card['card_type']}';

			window.parent.App.data.user.get('credit_cards').add(card);
		</script>";
	}

    function record_apriva_transaction(){
        $course_id = $this->session->userdata('course_id');
        $AprivaTransaction = new \fu\credit_cards\Processors\Apriva\AprivaTransaction();
        $post_data = $this->input->post();
        $api_data = $post_data['api_data'];
        $api_data['course_id'] = $course_id;
        $api_data['date_time'] = gmdate('Y-m-d H:i:s');
        $api_data['response'] = json_encode($api_data['response']);

        $AprivaTransaction->record($api_data);

        $transaction_data = $post_data['transaction_data'];
        $transaction_data['course_id'] = $course_id;
        $transaction_data['terminal_id'] = $this->session->userdata('terminal_id');
        $transaction_data['operator_id'] = $this->session->userdata('person_id');
        $AprivaTransaction->update($transaction_data['invoice_id'], $transaction_data);

        echo json_encode(array('success' => true));
    }


    function process_credit_card()
    {

        $this->load->model('v2/Cart_model');
        $this->load->model('v2/Sale_model');
        $this->load->model('v2/Giftcard_model');
        $this->load->model('online_booking');
        $this->load->model('teetime');

        // Gather reservation data
        $reservation = $this->session->userdata('reservation_data');
        $cc_payment = $this->session->userdata('credit_card_payment');
        $merchant_data = array();

        // Apriva
        if ($cc_payment['merchant'] == 'apriva') {
            $merchant_data['description'] = $this->input->post('description');
            $merchant_data['invoice_id'] = $this->input->post('record_id');
            $merchant_data['record_id'] = $this->input->post('record_id');
            $merchant_data['auth_code'] = $this->input->post('auth_code');
            $merchant_data['amount'] = $this->input->post('auth_amount');
            $merchant_data['type'] = $this->input->post('type');

            // Override mercury API config
            $this->config->set_item('mercury_id', false);
            $this->config->set_item('mercury_password', false);
            $this->config->set_item('ets_key', false);
            $this->config->set_item('element_account_id', false);
            $this->config->set_item('element_account_token', false);
            $this->config->set_item('element_acceptor_id', false);
            $this->config->set_item('element_application_id', false);
            $this->config->set_item('use_mercury_emv', false);

        // Mercury
        } else if($cc_payment['merchant'] == 'mercury'){
            $merchant_data['payment_id'] = $this->input->post('PaymentID');

            // Override mercury API config
            $this->config->set_item('mercury_id', $cc_payment['mercury_id']);
            $this->config->set_item('mercury_password', $cc_payment['mercury_password']);
            $this->config->set_item('ets_key', false);
            $this->config->set_item('element_account_id', false);
            $this->config->set_item('element_account_token', false);
            $this->config->set_item('element_acceptor_id', false);
            $this->config->set_item('element_application_id', false);
            $this->config->set_item('use_mercury_emv', false);

            $this->session->set_userdata('payment_id', $merchant_data['payment_id']);

            // ETS
        }else if($cc_payment['merchant'] == 'ets'){
            // Override ETS API config
            $this->config->set_item('ets_key', $cc_payment['ets_key']);
            $this->config->set_item('mercury_id', false);
            $this->config->set_item('mercury_password', false);
            $this->config->set_item('element_account_id', false);
            $this->config->set_item('element_account_token', false);
            $this->config->set_item('element_acceptor_id', false);
            $this->config->set_item('element_application_id', false);

            $transaction = $this->input->post('transactions');

            $merchant_data['transaction_id'] = $transaction['id'];
            $merchant_data['session_id'] = $this->input->post('id');

            $this->session->set_userdata('session_id', $merchant_data['session_id']);
		
		// Element
		}else if($cc_payment['merchant'] == 'element'){
			$this->config->set_item('ets_key', false);
			$this->config->set_item('mercury_id', false);
			$this->config->set_item('mercury_password', false);			
			$this->config->set_item('element_account_id',  $cc_payment['element_account_id']);
			$this->config->set_item('element_account_token',  $cc_payment['element_account_token']);
			$this->config->set_item('element_acceptor_id',  $cc_payment['element_acceptor_id']);
			$this->config->set_item('element_application_id',  $cc_payment['element_application_id']);

			$merchant_data = $this->input->get();

			if(!empty($merchant_data['HostedPaymentStatus']) && $merchant_data['HostedPaymentStatus'] == 'Cancelled'){
				echo "<script>window.parent.$('#modal').modal('hide');</script>";
				return false;
			}
		}

		$this->session->set_userdata('invoice', $cc_payment['invoice_id']);

		// If golfer is just saving a credit card to their account
		if($cc_payment['action'] == 'add_card'){
			return $this->capture_credit_card();
		}


		// Add credit card payment to cart
		$payment = $this->Cart_model->save_payment(array(
			'merchant' =>$cc_payment['merchant'],
			'amount' => $cc_payment['amount'],
			'type' => 'credit_card',
			'record_id' => $cc_payment['invoice_id']
		), $merchant_data);

		// If credit card payment failed for some reason
		if(!$payment){
			if(empty($message)){
				$message = 'Error processing credit card. '.$this->Cart_model->msg;
			}
			echo "<script>window.parent.alert('{$message}'); window.parent.$('#modal').modal('hide');</script>";
			return false;	
		}

		if($payment['amount'] < $cc_payment['amount']) {
            // Partial auth... return payment and report as declined
            $this->Cart_model->delete_payment($payment['payment_id']);

            $message = 'There was an error processing the credit card. There were insufficient funds for this transaction.';

            echo "<script>window.parent.App.vent.trigger('notification', {msg: '{$message}', type: 'error', delay:5000}); window.parent.$('#modal').modal('hide'); </script>";
            return false;
        }

		// Book tee time
		if(!empty($reservation)){
			
			if(empty($reservation['teetime_id'])){
				
				$paid = true;
				$paid_booking_fee = false;
				if($reservation['action'] == 'purchase-fee'){
					$paid = false;
					$paid_booking_fee = true;
				}
				$saved_reservation = $this->online_booking->book_time($reservation, $paid, $paid_booking_fee);

			// If tee time was already booked (user is coming from call to action button on confirmation page)
			}else{
				$saved_reservation = $reservation;
				$saved_reservation['foreup_discount'] = false;
				$saved_reservation['player_count'] = $reservation['players'];

				unset($saved_reservation['course'], $saved_reservation['user']);
			}

			// If there was an error booking the reservation, refund the credit card payment
			if(empty($saved_reservation)){
				$this->Cart_model->delete_payment($payment['payment_id']);
				
				$message = "Sorry, that tee time is no longer available";
				echo "<script>window.parent.alert('{$message}'); window.parent.$('#modal').modal('hide');</script>";
				return false;	
			}
		}

		// Make sure customer is on the course's customer list
		$this->user->add_to_course($reservation['customer_id'], $reservation['course_id']);

		$giftcard = false;

		// If tee time is purchased through ForeUp we won't create a sale for the course
		// We just use the cart to get accurate totals for the receipt email
		if(!empty($reservation) && $reservation['recipient'] == 'foreup'){

			$this->Sale_model->mark_teetime_paid(
				$saved_reservation['teetime_id'], 
				$reservation['customer_id'], 
				$reservation['players'],
				$reservation['carts']
			);
			$sale_response = true;

			$this->teetime->save_bartered_teetime(
				$saved_reservation['teetime_id'], 
				$reservation['players'], 
				$reservation['carts'], 
				$cc_payment['invoice_id'],
				$this->online_booking->get_teetime_message($reservation['group_id'], true)
			);		

		// If purchasing a gift card
		}else if($this->session->userdata('giftcard_data')){
			
			// Finish sale and issue giftcard
			$this->Cart_model->terminal_id = $this->online_booking->get_purchase_terminal();
			$sale_response = $this->Cart_model->save_sale();
			$giftcard_data = $this->session->userdata('giftcard_data');

			if($sale_response){
				$this->Action->save('online_giftcard_sale', $sale_response['sale_id'], 'create', json_encode($giftcard_data));
				
				$giftcards = $this->Giftcard_model->get(['giftcard_number' => $giftcard_data['giftcard_number']]);

                if (!empty($giftcards[0])) {
                    $giftcard = $giftcards[0];
                } else {
                    $this->load->model('Giftcard');

                    $new_gift_card_data = array();
                    $new_gift_card_data['value'] = $giftcard_data['amount'];
                    $new_gift_card_data['customer_id'] = $giftcard_data['customer_id'];
                    $new_gift_card_data['giftcard_number'] = $giftcard_data['giftcard_number'];
                    $new_gift_card_data['details'] = "Purchased online ".date("m/d/Y g:ia")." *";
                    $new_gift_card_data['date_issued'] = date('Y-m-d');
                    $new_gift_card_data['expiration_date'] = '';// '0000-00-00';
                    $new_gift_card_data['department'] = '';
                    $new_gift_card_data['category'] = '';
                    $new_gift_card_data['course_id'] = $giftcard_data['course_id'];

                    $response = $this->Giftcard->save($new_gift_card_data);
                }
            }else{
				$this->Action->save('online_giftcard_sale', $payment['invoice_id'], 'fail', json_encode($giftcard_data));
			}

		// If tee time (or booking fee) is purchased through course, save a sale as normal
		}else{
			$this->Cart_model->terminal_id = $this->online_booking->get_purchase_terminal();
			$this->Cart_model->save_customer($reservation['customer_id'], array('selected' => true));

			// If pre-paying for the tee time, set tee time info in cart
			// Tee time will be marked as 'paid' on sale completion
			if($reservation['action'] == 'purchase'){
				$this->Cart_model->save_cart(array('teetime_id' => $saved_reservation['teetime_id']));
			}

			$sale_response = $this->Cart_model->save_sale();	
		}

		// If there was an error finishing the sale
		if(!$sale_response){
			// Refund CC payment
			$this->Cart_model->delete_payment($payment['payment_id']);
			
			$message = "Sorry, there was a problem completing the transaction";
			echo "<script>window.parent.alert('{$message}'); window.parent.$('#modal').modal('hide');</script>";
			return false;
		}

		if(!empty($reservation)){
			
			$saved_reservation['purchased'] = true;
			if($reservation['action'] == 'purchase-fee'){
				$saved_reservation['purchased'] = false;
			}

			$saved_reservation['purchase_total'] = $cc_payment['amount'];
			$saved_reservation['paid_player_count'] = $saved_reservation['player_count'];	
			$carts = ($reservation['carts'])? $reservation['players'] : 0;	

			// Send purchase receipt email to customer
			$reservation_time = DateTime::createFromFormat('Y-m-d H:i', $reservation['time']);
			$email_data = array(
				'course_name' => $reservation['course']['name'],
				'booked_date' => $reservation_time->format('n/j/y'),
				'booked_time' => $reservation_time->format('g:ia'),
				'booked_holes' =>  $reservation['holes'],
				'booked_players' => $reservation['players'],
				'booked_carts' => $carts,
				'customer_name' => $reservation['customer']['first_name'].' '.$reservation['customer']['last_name'],
				'customer_email' => $reservation['customer']['email'],
				'card_type' => $payment['description'],
				'current_date' => date('g:ia n/j/y T'),
				'confirmation_number' => $saved_reservation['teetime_id'],
				'subtotal' => $reservation['totals']['subtotal'],
				'discount' => $reservation['totals']['discount'],
				'total' => $reservation['totals']['total'],
				'total_fee' => $reservation['total_fee']
			);

			$from_name = 'ForeUP';
			$from_email = 'booking@foreup.com';

			if($reservation['recipient'] != 'foreup'){
				$from_name = $reservation['course']['name'];
				$from_email = $reservation['course']['email'];
			}

			send_sendgrid(
				$reservation['customer']['email'],
				'Tee Time Purchase Receipt',
				$this->load->view("email_templates/purchase_receipt", $email_data, true),
				$from_email,
				$from_name
			);
		}

		// Clear out cart we were working with
		$this->Cart_model->delete($this->session->userdata('pos_cart_id'), false);
		$this->session->unset_userdata('pos_cart_id');

		if(!empty($reservation)){
			// Add new reservation to customer's reservation list on front end
			// Close credit card window, and display reservation confirmation
			$code = "
				App.data.last_reservation.set(".json_encode($saved_reservation).");
				App.data.user.get('reservations').add(App.data.last_reservation.clone());
				
				App.data.times.refresh();
				App.router.navigate('confirmation/".$saved_reservation['teetime_id']."', {trigger: true});
				ga('send', 'event', 'Online Booking', 'Tee Time Purchase', window.parent.App.data.course.getCurrentSchedule());
				$('#modal').modal('hide');
			";

		}else if($giftcard_data){

			$sale_response['total'] = $giftcard_data['amount'];
			$sale_response['payment_method'] = $payment['description'];

			$template_data = [
				'from' => $giftcard_data['from'],
				'to' => false,
				'organization' => $giftcard_data['organization'],
				'gift_card' => $giftcard,
				'sale' => $sale_response
			];
			$subject = 'Your gift card';

			send_sendgrid(
				$giftcard_data['from']['email'],
				$subject,
				$this->load->view("email_templates/online_giftcard", $template_data, true),
				$giftcard_data['organization']['email'],
				$giftcard_data['organization']['name']
			);

			// Email giftcard (if set to do so)
			if(!empty($giftcard_data['send'])){

				$template_data = [
					'from' => $giftcard_data['from'],
					'to' => $giftcard_data['to'],
					'organization' => $giftcard_data['organization'],
					'gift_card' => $giftcard
				];
				$subject = $giftcard_data['from']['first_name'].' '.$giftcard_data['from']['last_name'].' has sent you a gift card';

				send_sendgrid(
					$giftcard_data['to']['email'],
					$subject,
					$this->load->view("email_templates/online_giftcard", $template_data, true),
					$giftcard_data['organization']['email'],
					$giftcard_data['organization']['name']
				);
			}

			$this->session->unset_userdata('giftcard_data');

			// Add new giftcard to user's page and display the details
			$code = "
				App.data.user.get('gift_cards').add(".json_encode($giftcard).");
				App.router.navigate('account/giftcards/".$giftcard['giftcard_id']."', {trigger: true});
				ga('send', 'event', 'Online Booking', 'Online Giftcard Purchase', window.parent.App.data.course.getCurrentSchedule());
				$('#modal').modal('hide');
			";
		}

		$in_iframe = ($cc_payment['merchant'] != 'ets' && $cc_payment['merchant'] != 'apriva');
		$this->output($code, $in_iframe);	
	}

	function credit_card_cancel(){
		echo "<script>window.parent.$('#modal').modal('hide');</script>";
	}

	// End point for email cancellation link
	function confirm_cancellation($teetime_id, $person_id){

		$this->load->model('teetime');
		$this->load->model('reservation');
		$this->load->model('course');
		$this->load->model('teesheet');

		$data = array (
			'teetime_id' => $teetime_id,
			'teetime_info' => array(),
			'person_id' => $person_id
		);

		// Retrieve tee time details
		$tee_time_info = (array) $this->teetime->get_info($teetime_id);
		$data['teetime_info'] = $tee_time_info;

		// Parse tee time start time/date
		$timestamp = (int) $tee_time_info['start'] + 1000000;
		$data['teetime_info']['start_date'] = date('F jS, Y', strtotime($timestamp));
		$data['teetime_info']['start_time'] = date('g:ia', strtotime($timestamp));

		// Get name of course and tee sheet
		$tee_sheet_info = (array) $this->teesheet->get_info((int) $tee_time_info['teesheet_id']);
		$data['teetime_info']['tee_sheet_title'] = $tee_sheet_info['title'];

		$course_info = (array) $this->course->get_info((int) $tee_sheet_info['course_id']);
		$data['teetime_info']['course_name'] = $course_info['name'];
		$data['course_id'] = (int) $course_info['course_id'];
		$data['course_phone'] = $course_info['phone'];

		$this->load->view('booking/confirm_cancellation', $data);
	}

	// Cancel tee time
	function cancel($teetime_id, $person_id){

		$this->load->model('teetime');
		$this->load->model('course');
		$this->load->model('teesheet');

		$data['success'] = false;
		$teetime_info = $this->teetime->get_info($teetime_id);

		$course_info = $this->Course->get_info_from_teesheet_id($teetime_info->teesheet_id);
		$customer_info = $this->Customer->get_info($person_id);
		$tee_sheet_info = $this->teesheet->get_info($teetime_info->teesheet_id);

		$data['course_id'] = $course_info->course_id;
		$data['course_name'] = $course_info->name;

		// If tee time hasn't already been cancelled
		if($teetime_info->status != 'deleted'){

			// Cancel tee time
			$data['success'] = $this->teetime->delete($teetime_id, $person_id);
			$this->db->trans_complete();

			// Send cancellation email if the cancellation succeeded
			if ($data['success']){

				$email_data = array(
					'cancelled' => true,
					'person_id' => $person_id,
					'course_name' => $course_info->name,
					'course_phone' => $course_info->phone,
					'first_name' => $customer_info->first_name,
					'booked_date' => date('n/j/y', strtotime($teetime_info->start + 1000000)),
					'booked_time' => date('g:ia', strtotime($teetime_info->start + 1000000)),
					'booked_holes' => $teetime_info->holes,
					'booked_players' => $teetime_info->player_count,
					'tee_sheet' => $tee_sheet_info->title
				);

				send_sendgrid(
					$customer_info->email,
					'Reservation Cancellation Details',
					$this->load->view("email_templates/reservation_made", $email_data, true),
					'booking@foreup.com',
					$course_info->name
				);
			}

		// Tee time already cancelled
		}else{
			$data['success'] = true;
		}

		$this->load->view('booking/cancelled', $data);
	}
	
	// Forgot password screen
	function password_reset($reset_key, $course_id = false){
		
		$data['valid_key'] = true;
		if(empty($reset_key)){
			$data['valid_key'] = false;
		}
		$this->load->library('encrypt');
		
		list($customer_id, $expire) = explode('|', $this->encrypt->decode(base64url_decode($reset_key)));
		
		if ($customer_id && $expire && (int) $expire > time()){
			$data['valid_key'] = true;
		}else{
			$data['valid_key'] = false;
		}
		
		$this->load->model('course');
		
		$data['reset_key'] = $reset_key;
		$data['success'] = false;
		$data['page_name'] = 'ForeUp';
        $data['booking_url'] = site_url('booking/index/'.$course_id);

		$this->load->view('booking/password_reset', $data);
	}

	function get_available_spots() {
        $this->load->model('teetime');

        $params = array();
        $params['start'] = date('YmdHi', strtotime($this->input->post('time'))) - 1000000;
        $params['teesheet_id'] = $this->input->post('tee_sheet_id');
        $params['holes'] = $this->input->post('holes');

        $available_spots = $this->teetime->get_available_spots($params);

        echo json_encode(array('available_spots' => $available_spots));
    }
}
