<?php
// Client Booking Engine
class Timeclock extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();	
	    $this->load->model('timeclock_entry');
		$this->load->model('Terminal');
    }
	function index($employee_id = false)
	{
		$employee_array= array();
		$employees = $this->Employee->get_all(10000,0,[],true)->result_array();
		//print_r($employees);
		foreach ($employees as $employee)
		{
			$employee_array[$employee['person_id']] = $employee['first_name'].' '.$employee['last_name'];
		}
		$data['employees'] = $employee_array;
		$data['employee_id'] = $employee_id ? $employee_id : $this->session->userdata('person_id');
		$terminals_array = array();
		$terminals = $this->Terminal->get_all()->result_array();
		foreach ($terminals as $terminal)
		{
			$terminal_array[$terminal['terminal_id']] = $terminal['label'];
		}
		$data['terminals'] = $terminal_array;
		$data['last_entries'] = $this->timeclock_entry->get_all(10)->result_array();
		$data['clocked_in'] = $this->timeclock_entry->is_clocked_in();
		$this->load->view('timeclock/manage', $data);		
	}
	function load_employee_data($employee_id = false)
	{
		if ($employee_id)
		{
			$data['last_entries'] = $this->timeclock_entry->get_all(10, $employee_id)->result_array();
			$data['clocked_in'] = $this->timeclock_entry->is_clocked_in($employee_id);
		
			echo $this->load->view('timeclock/employee_data', $data, true);	
		}
	}
	function clock_in(){
		$employee_id = $this->input->post('employee_id');
		$password = $this->input->post('password');
		$terminal_id = $this->input->post('terminal_id');
		// VALIDATE EMPLOYEE AND PASSWORD
		if ($this->Employee->is_valid_password($employee_id, $password))
		{
            $role_id = null;
            $hourly_rate = 0.00;

            $this->load->model('acl_roles');
            $roles = $this->acl_roles->get_acl_roles(['employee_id' => $employee_id]);
            if(!empty($roles[0])){
                $role_id = $roles[0]['role_id'];
                $hourly_rate = $roles[0]['hourly_rate'];
            }

		    $this->timeclock_entry->clock_in($employee_id, $terminal_id, $role_id, $hourly_rate);
			$data = $this->timeclock_entry->get_all(10, $employee_id)->result_array();
			echo json_encode($data);
		}
		else {
			echo json_encode(array('error'=>true,'message'=>'Incorrect password'));
		}
	}
	function clock_out(){
		$employee_id = $this->input->post('employee_id');
		$password = $this->input->post('password');
		$terminal_id = $this->input->post('terminal_id');
		// VALIDATE EMPLOYEE AND PASSWORD
		if ($this->Employee->is_valid_password($employee_id, $password))
		{
			$this->timeclock_entry->clock_out($employee_id, $terminal_id);
			$data = $this->timeclock_entry->get_all(10, $employee_id)->result_array();
			echo json_encode($data);
		}
		else {
			echo json_encode(array('error'=>true,'message'=>'Incorrect password'));
		}
	}
	function save($employee_id, $start_time)
	{
		if ($this->input->post('delete_entry'))
			$this->timeclock_entry->delete($employee_id, $start_time);
		else 
			$this->timeclock_entry->save($employee_id, $start_time);
		
		echo json_encode(array());
	}
	function view($employee_id, $start_time) {
		$data = array(
			'employee_info'=>$this->Employee->get_info($employee_id),
			'entry'=>$this->timeclock_entry->get_info($employee_id, $start_time)
		);
		$this->load->view('timeclock/form', $data);
	}
	
}
?>