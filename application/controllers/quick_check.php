<?php

/**
 * Created by PhpStorm.
 * User: brend
 * Date: 11/10/2016
 * Time: 10:24 AM
 */
class quick_check
{
	public function __construct()
	{
		require_once(BASEPATH.'database/DB.php');

		// Load the DB class
		$this->db =& DB("slave", null);
	}

	function is_cache_valid($teetime_id)
	{
		$query = $this->db->select("last_updated")
			->from("foreup_teetime")
			->where("TTID = '$teetime_id'")
			->get();
		$result = $query->row();
		if(empty($result)){
			return false;
		}
		echo json_encode(["last_updated"=>$result->last_updated]);
	}
}