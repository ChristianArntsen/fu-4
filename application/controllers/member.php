<?php
class Member extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('person');
		$this->load->model('user');
		$this->load->model('customer');
	}

	function save(){

		if(!$this->user->is_logged_in()){
			return false;
		}
		$personId = $this->session->userdata('customer_id');

		$person_data = array(
			'first_name' => trim($this->input->post('first_name')),
			'last_name' => trim($this->input->post('last_name')),
			'email' => $this->input->post('email'),
			'phone_number' => $this->input->post('phone_number'),
			'cell_phone_number' => $this->input->post('cell_phone_number'),
			'birthday' => $this->input->post('birthday'),
			'address_1' => $this->input->post('address_1'),
			'address_2' => $this->input->post('address_2'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'zip' => $this->input->post('zip'),
			'country' => $this->input->post('country')
		);

		$user_data = array(
			'email' => $this->input->post('username'),
			'password' => $this->input->post('password')
		);

		if(empty($person_data['first_name']) || empty($person_data['last_name'])){
			echo json_encode(array('success'=>false, 'message'=>'First and last name are required'));
			return false;
		}

		if(empty($user_data['email'])){
			echo json_encode(array('success'=>false, 'message'=>'Username is required'));
			return false;
		}

		if($this->input->post('password') != $this->input->post('password_confirm')){
			echo json_encode(array('success'=>false, 'message'=>'Passwords do not match'));
			return false;
		}

		if(!empty($user_data['password'])){
			$user_data['password'] = md5($password);
		}

		$success = $this->person->save_person($person_data, $personId);
		$this->user->save($personId, $user_data);

		if($success){
			echo json_encode(array('success'=>true, 'message'=>'Account information saved'));
		}else{
			echo json_encode(array('success'=>false, 'message'=>'Error saving account information'));
		}
	}

	function save_course_account($courseId = null){

		if(!$this->user->is_logged_in()){
			return false;
		}
		$personId = $this->session->userdata('customer_id');

		if(empty($courseId)){
			return false;
		}

		// TODO: Update whether they want to receive texts, email notification, etc. on course account
	}

	/*
	 * Retrieves account balances and giftcards for user
	 */
	function get_balances(){

		if(!$this->user->is_logged_in()){
			return false;
		}
		$personId = $this->session->userdata('customer_id');
		$courseId = $this->session->userdata('course_id');

		$data['giftcards'] = $this->user->get_giftcards($personId, $courseId);
		$data['accounts'] = $this->user->get_customer_accounts($personId, $courseId);

		$this->load->view('user/account_balances', $data);
	}

	/*
	 * Retrieves all purchases a user has made
	 */
	function get_purchases(){

		if(!$this->user->is_logged_in()){
			return false;
		}
		$personId = $this->session->userdata('customer_id');
		$courseId = $this->session->userdata('course_id');

		$data['purchases'] = $this->user->get_purchases($personId, $courseId);

		$this->load->view('user/purchases', $data);
	}

	/*
	 * Retrieves all credit cards on file for a person
	 */
	function get_credit_cards(){

		if(!$this->user->is_logged_in()){
			return false;
		}
		$this->load->model('Customer_credit_card');
		$personId = $this->session->userdata('customer_id');
		$courseId = $this->session->userdata('course_id');

		$data['customer_id'] = $personId;
		$data['credit_cards'] = $this->Customer_credit_card->get($personId);

		$this->load->view('user/credit_cards', $data);
	}

	function delete_credit_card(){
		if(!$this->user->is_logged_in()){
			return false;
		}
		$this->load->model('Customer_credit_card');
		$personId = $this->session->userdata('customer_id');
		$courseId = $this->session->userdata('course_id');
		$cardId = $this->input->post('credit_card_id');

		if(empty($cardId)){
			echo json_encode(array('success'=>false, 'message'=>'Error deleting credit card'));
			return false;
		}

		$this->Customer_credit_card->delete_card($personId, $cardId);

		echo json_encode(array('success'=>true, 'message'=>'Credit card deleted'));
		return true;
	}

	function add_credit_card($person_id){
		$this->load->model('Customer_credit_card');
		$credit_card_id = $this->Customer_credit_card->capture_card($person_id);

		if(!empty($credit_card_id)){
			echo "<script>
				window.parent.set_feedback('Credit card added', 'success_message');
				window.parent.$.get('".site_url('member/get_credit_cards')."', null, function(response){
					window.parent.$('#credit_cards').html(response);
					window.parent.$.colorbox.close();
				});
				</script>";
		}else{
			echo "<script>
				window.parent.set_feedback('Error adding credit card', 'error_message');
				</script>";
		}
	}
}
