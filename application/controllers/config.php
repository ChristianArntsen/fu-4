<?php
require_once ("secure_area.php");
class Config extends Secure_area
{
	function __construct()
	{
		parent::__construct('config');
		$this->load->model('Teesheet');
		$this->load->model('Schedule');
		$this->load->model('Green_fee');
		$this->load->model('Fee');
		$this->load->model('Terminal');
		$this->load->model('Customer_loyalty');
		$this->load->model('Price_class');
		$this->load->model('Custom_payments');
	}


	function index()
	{
		$data['controller_name']=strtolower(get_class());
		/*
		 * NEW RESERVATION SYSTEM
		 */
		if ($this->session->userdata('reservations'))
		{
	        $data['teetime_prices']=$this->Fee->get_info();//$this->Item->get_teetime_prices();
	        $data['teetime_type']=$this->Fee->get_type_info();
			$data['teesheet_table'] = '';//get_teesheets_manage_table($this->Schedule->get_all($this->config->item('per_page'), $this->uri->segment(3)),$this);
			$data['teesheets'] = $this->Schedule->get_all();
		}
		/*
		 * OLD TEESHEET SYSTEM
		 */
		else
		{
	        $data['teetime_prices']=$this->Green_fee->get_info();//$this->Item->get_teetime_prices();
	        $data['teetime_type']=$this->Green_fee->get_type_info();
			$data['teesheet_table'] = get_teesheets_manage_table($this->Teesheet->get_all($this->config->item('per_page'), $this->uri->segment(3)),$this);
			$data['teesheets'] = $this->Teesheet->get_all();
		}
		$data['teetime_department']=$this->Item->get_teetime_department();
        $data['teetime_tax_rate']=$this->Item->get_teetime_tax_rate();
		$data['cart_department']=$this->Item->get_cart_department();
        $data['cart_tax_rate']=$this->Item->get_cart_tax_rate();
		$data['course_id']=$this->session->userdata('course_id');
		$data['course_info'] = $this->Course->get_info($data['course_id']);
		
		$data['price_categories'] = $this->Green_fee->get_price_categories();
		$data['price_classes'] = $this->Price_class->get_all();
		$data['price_colors'] = $this->Green_fee->get_colors();

		// Get green fee/cart fee receipt data
		$fee_rows = $this->Pricing->get_fee_item_ids();
		$green_fee_item_id = false;
		$cart_fee_item_id = false;

		$data['green_fee_receipt_content_id'] = false;
		$data['cart_fee_receipt_content_id'] = false;

		if(!empty($fee_rows)){
			foreach($fee_rows as $row){
				if($row['item_number'] == 1 || $row['item_number'] == 2){
					$green_fee_item_id = $row['item_id'];
					$data['green_fee_tax_included'] = $row['unit_price_includes_tax'];
				}
				if($row['item_number'] == 3 || $row['item_number'] == 4){
					$cart_fee_item_id = $row['item_id'];
					$data['cart_fee_tax_included'] = $row['unit_price_includes_tax'];
				}		
			}

			$this->load->model('v2/Item_model');
			$items = $this->Item_model->get(['item_id' => [$green_fee_item_id, $cart_fee_item_id]]);

			foreach($items as $fee_item){
				
				if($fee_item['item_type'] == 'green_fee'){
					$key = 'green_fee_receipt_content_id';
				}else if($fee_item['item_type'] == 'cart_fee'){
					$key = 'cart_fee_receipt_content_id';
				}

				if($fee_item['receipt_content']){
					$data[$key] = $fee_item['receipt_content_id'];
				}
			}	
		}

		$this->load->model('v2/Item_receipt_content');
		$receipt_content_menu = ['' => '- Select Receipt Agreement -'];
		$item_receipt_content = $this->Item_receipt_content->get();
		foreach($item_receipt_content as $receipt_content){
			$receipt_content_menu[$receipt_content['receipt_content_id']] = $receipt_content['name'];
		}

		$data['receipt_content_menu'] = $receipt_content_menu;

		$departments = $this->Item->get_department_suggestions();
		$data['use_terminals']=($this->session->userdata('use_terminals')?1:0);
        $data['use_loyalty']=($this->config->item('use_loyalty')?1:0);
        $data['online_purchase_terminal_id'] = $this->config->item('online_purchase_terminal_id');
        $data['online_invoice_terminal_id'] = $this->config->item('online_invoice_terminal_id');
        $data['loyalty_auto_enroll']=($this->config->item('loyalty_auto_enroll')?1:0);
        $data['terminals']=$this->Terminal->get_all($data['course_id'])->result_object();
		$data['loyalty_rates']=$this->Customer_loyalty->get_rates()->result_object();
		$data['departments']['all'] = 'Department';
		$data['page'] = ($this->input->get('page') ? $this->input->get('page') : 1);
		foreach ($departments as $department) {
			$data['departments'][$department['label']] = $department['label'];
		}
		$categories = $this->Item->get_category_suggestions();
		$data['categories']['all'] = 'Category';
		foreach ($categories as $category) {
			$data['categories'][$category['label']] = $category['label'];
		}
		$data['no_weekend'] = (!$this->config->item('weekend_fri') && !$this->config->item('weekend_sat') && !$this->config->item('weekend_sun'));
		
		// Quickbooks, load user (if one exists)
		$this->load->model('quickbooks');
		$data['quickbooks'] = $this->quickbooks->get_user($this->session->userdata('course_id'));

		// Get list of Quickbooks accounts
		$data['quickbooks']['accounts'] = $this->quickbooks->get_accounts_menu();
		$data['quickbooks']['account_map'] = $this->quickbooks->get_account_map($this->session->userdata('course_id'));
		
		// Quickbooks (export feature)
		$quickbooks_settings = $this->config->item('quickbooks_export_settings');
		$quickbooks_settings = json_decode($quickbooks_settings, true);
		$data['quickbooks_export']['accounts'] = $quickbooks_settings;

		$categories = $this->Item->get_all_categories();
		$data['quickbooks_export']['categories'] = $categories;
		
		$taxes = $this->Item_taxes->get_all_taxes();
		$data['quickbooks_export']['taxes'] = $taxes;
		$data['quickbooks_export']['terminals']	= $data['terminals'];
		
		// WebPrint printer drop down menu
		$this->load->model('v2/Printer_model');
		$this->load->model('v2/Printer_group_model');
		
		$printers = $this->Printer_model->get();
		$data['printers'] = [];
		foreach($printers as $printer){
			$data['printers'][$printer['printer_id']] = $printer;
		}

		$data['printer_menu'] = array('' => '- Select Printer -');
		$data['printer_menu'] += $this->Printer_model->get_menu();
		$data['printer_groups'] = $this->Printer_group_model->get();

		$data['custom_payments'] = $this->Custom_payments->get_all();

		// Customer field settings
		$this->load->model('v2/Customer_model');
		$data['customer_field_settings'] = $this->Customer_model->get_field_settings();
		
		$this->load->view("config/settings", $data);
	}

	function template(){
		$this->load->view('email_templates/template_5');
	}
	function holidays(){
		$data['holidays']=$this->Appconfig->get_holiday_info();
		$this->load->view('holidays/form', $data);
	}
	function add_holiday()
	{
		$holiday_label = $this->input->post("holiday_label");
		$holiday_date = $this->input->post("holiday_date");
		echo json_encode(array('success'=>$this->Appconfig->add_holiday($holiday_label, $holiday_date)));
	}
	function save_holiday_name($holiday_date)
	{
		$holiday_label = $this->input->post('holiday_label');
		$new_date = $this->input->post('new_date');
		echo json_encode(array('success'=>$this->Appconfig->update_holiday($holiday_date, $holiday_label, $new_date)));
	}
	function delete_holiday($holiday_date)
	{
		echo json_encode(array('success'=>$this->Appconfig->delete_holiday($holiday_date)));
	}
	
	function view($settings_section, $id = false, $show = false)
    {
        $course_id = $this->session->userdata('course_id');
        /*if ($settings_section === 'teesheet')
           $this->load->view('config/teesheet');
        else*/ if ($settings_section === 'sales')
           $this->load->view('config/sales');
        else if ($settings_section === 'teesheets')
        {
			$data['teetime_department']=$this->Item->get_teetime_department();
			$data['teetime_tax_rate']=$this->Item->get_teetime_tax_rate();
			$data['cart_department']=$this->Item->get_cart_department();
			$data['cart_tax_rate']=$this->Item->get_cart_tax_rate();        	
        	$data['teesheets'] = $this->Teesheet->get_all();
        	$data['price_classes'] = $this->Price_class->get_all();
        	$this->load->view('config/seasonal_green_fees',$data);
        }
        else if ($settings_section === 'teesheet')
        {
        	$data['id'] = $id;
        	$data['teesheet'] = $this->Teesheet->get_info($id);
        	$data['seasons'] = $this->Season->get_all($id);
        	
        	// Get list of all seasons in other teesheets
        	$seasons = $this->Season->get_all();
        	$seasons_dropdown = array(0 => '- Select Existing Season - ');
        	foreach($seasons as $season){
				$seasons_dropdown[$season->season_id] = $season->teesheet_title.' - '.$season->season_name;
			}
			$data['seasons_dropdown'] = $seasons_dropdown;
        	
        	$this->load->helper('date');
        	$this->load->view('config/seasonal_teesheet',$data);
        }
        else if ($settings_section === 'season')
        {
        	$this->load->model('Price_class');
        	$data['season'] = $this->Season->get_info($id);
        	
        	$data['teesheet'] = $this->Teesheet->get_info($data['season']->teesheet_id);
        	$data['price_classes'] = $this->Season->get_price_classes($id);
        	$available_price_classes = $this->Price_class->get_available_from_season($id);
        	
        	$data['price_class_dropdown'] = array('0' => '- Select Price Class -');
        	foreach($available_price_classes as $price){
				$data['price_class_dropdown'][$price['class_id']] = $price['name'];
			}
        	
        	$data['show'] = $show;

        	$this->load->helper('date');
        	$this->load->view('config/season',$data);
        }
    }	
	function view_terminal_options($terminal_id)
	{
		if ($this->permissions->is_admin())
		{
			$data = array();
			$terminal_info = $this->Terminal->get_info($terminal_id);
			$data['terminal_info'] = $terminal_info[0];
			
			// WebPrint printer drop down menu
			$this->load->model('v2/Printer_model');
			$this->load->model('v2/Terminal_printers_model');
			
			$data['printer_menu'] = array(null => 'Default');
			$data['printer_menu'] += $this->Printer_model->get_menu();
			$data['printer_menu']['0'] = '* Do not use *';
			$data['printer_groups'] = $this->Terminal_printers_model->get(array('terminal_id' => $terminal_id));
			
			$this->load->view('config/form_terminals', $data);
		}
		else
			echo 'You do not have permissions to access these settings.';
	}
	function delete_terminal($terminal_id)
	{
		echo json_encode(array('success'=>$this->Terminal->delete($terminal_id)));
	}
	function save_terminal_options($terminal_id = -1)
	{
		$terminal_data = array(
			'label'=>$this->input->post('t_label'),
			'quickbutton_tab'=>$this->input->post('t_quickbutton_tab'),
			//'mercury_id'=>$this->input->post('t_mercury_id'),
			//'mercury_password'=>$this->input->post('t_mercury_password'),
			//'ets_key'=>$this->input->post('t_ets_key'),
			'auto_print_receipts'=>$this->input->post('t_auto_print_receipts') == 'null' ? null : $this->input->post('t_auto_print_receipts'),
			'webprnt'=>$this->input->post('t_webprnt') == 'null' ? null : $this->input->post('t_webprnt'),
			'https'=>$this->input->post('t_https') == 'null' ? null : $this->input->post('t_https'),
			'receipt_ip'=>$this->input->post('t_webprnt_ip'),
			'default_kitchen_printer'=> $this->input->post('t_default_kitchen_printer') == '' ? null: $this->input->post('t_default_kitchen_printer'),
			'hot_webprnt_ip'=>$this->input->post('t_webprnt_hot_ip'),
			'cold_webprnt_ip'=>$this->input->post('t_webprnt_cold_ip'),
            'use_register_log'=>$this->input->post('t_track_cash') == 'null' ? null : $this->input->post('t_track_cash'),
            'persistent_logs'=>$this->input->post('persistent_logs'),
			'cash_register'=>$this->input->post('t_cash_register') == 'null' ? null : $this->input->post('t_cash_register'),
			'print_tip_line'=>$this->input->post('t_print_tip_line') == 'null' ? null : $this->input->post('t_print_tip_line'),
			'signature_slip_count'=>$this->input->post('t_signature_slip_count') == 'null' ? null : $this->input->post('t_signature_slip_count'),
			'credit_card_receipt_count'=>$this->input->post('t_credit_card_receipt_count') == 'null' ? null : $this->input->post('t_credit_card_receipt_count'),
			'non_credit_card_receipt_count'=>$this->input->post('t_non_credit_card_receipt_count') == 'null' ? null : $this->input->post('t_non_credit_card_receipt_count'),
			'auto_email_receipt'=>$this->input->post('t_auto_email_receipt') == 'null' ? null : $this->input->post('t_auto_email_receipt'),
			'auto_email_no_print'=>$this->input->post('t_auto_email_no_print') == 'null' ? null : $this->input->post('t_auto_email_no_print'),
			'require_signature_member_payments'=>$this->input->post('t_require_signature_member_payments') == 'null' ? null : $this->input->post('t_require_signature_member_payments'),
			'after_sale_load'=>$this->input->post('after_sale_load') == 'null' ? null : $this->input->post('after_sale_load'),
			'return_policy' => $this->input->post('return_policy'),
			'multi_cash_drawers' => $this->input->post('multi_cash_drawers'),
			'cash_drawer_number' => (int) $this->input->post('cash_drawer_number')
		);
		
		$terminal_data['return_policy'] = trim($terminal_data['return_policy']);
		$success = $this->Terminal->save($terminal_data, $terminal_id);

		if($terminal_data['multi_cash_drawers'] == 1){
			$terminal_data['persistent_logs'] = 1;
		}

		// Save printer mapping
		$printer_groups = $this->input->post('printer_groups');
		if($printer_groups){
			if(empty($terminal_id) || $terminal_id < 0){
				$terminal_id = (int) $terminal_data['terminal_id'];
			}
			$this->load->model('v2/Terminal_printers_model');
			$this->Terminal_printers_model->save_terminal_printers($terminal_id, $printer_groups);
		}

		$data = array('terminals'=>$this->Terminal->get_all()->result());
		echo json_encode(array('success'=>$success, 'terminal_table'=>$this->load->view('config/terminal_table', $data, true)));
	}
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_dept_cat_item()
	{
		$suggestions = $this->Item->get_department_suggestions($this->input->get('term'), true);
		$suggestions = array_merge($suggestions, $this->Item->get_category_suggestions($this->input->get('term')));
		$suggestions = array_merge($suggestions, $this->Item->get_subcategory_suggestions($this->input->get('term')));
		$suggestions = array_merge($suggestions, $this->Item->get_item_search_suggestions($this->input->get('term'),100,true,true));
		$suggestions = array_merge($suggestions, $this->Item_kit->get_search_suggestions($this->input->get('term'),100));
		echo json_encode($suggestions);
	}
	function save($settings_section)
	{
		$course_id = $this->session->userdata('course_id');
		$batch_save_data = array();
		if ($settings_section == 'details')
		{
			$allowed_extensions = array('png', 'jpg', 'jpeg', 'gif');

			// If a company logo was uploaded
			if(!empty($_FILES["company_logo"]) &&
				$_FILES["company_logo"]["error"] == UPLOAD_ERR_OK &&
				($_SERVER['HTTP_HOST'] !='demo.phppointofsale.com' && $_SERVER['HTTP_HOST'] !='demo.phppointofsalestaging.com')
			){
				$extension = strtolower(end(explode('.', $_FILES["company_logo"]["name"])));

				if (in_array($extension, $allowed_extensions))
				{
					$config['image_library'] = 'gd2';
					$config['source_image']	= $_FILES["company_logo"]["tmp_name"];
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']	 = 340;
					$config['height']	= 120;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
					$company_logo = $this->Appfile->save($_FILES["company_logo"]["name"], file_get_contents($_FILES["company_logo"]["tmp_name"]), $this->config->item('company_logo'));
				}
			}
			elseif($this->input->post('delete_logo'))
			{
				$this->Appfile->delete($this->config->item('company_logo'));
			}

			// If a reservation email photo was uploaded
			if(!empty($_FILES["reservation_email_photo"]) &&
				$_FILES["reservation_email_photo"]["error"] == UPLOAD_ERR_OK &&
				($_SERVER['HTTP_HOST'] !='demo.phppointofsale.com' && $_SERVER['HTTP_HOST'] !='demo.phppointofsalestaging.com')
			){
				$extension = strtolower(end(explode('.', $_FILES["reservation_email_photo"]["name"])));

				if (in_array($extension, $allowed_extensions))
				{
					$config['image_library'] = 'gd2';
					$config['source_image']	= $_FILES["reservation_email_photo"]["tmp_name"];
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 159;
					$config['height'] = 111;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
					$reservation_email_photo = $this->Appfile->save($_FILES["reservation_email_photo"]["name"], file_get_contents($_FILES["reservation_email_photo"]["tmp_name"]), $this->config->item('reservation_email_photo'));
				}
			}
			elseif($this->input->post('delete_reservation_email_photo'))
			{
				$this->Appfile->delete($this->config->item('delete_reservation_email_photo'));
			}

			$this->load->helper('html_purify');
			
			// Clean HTML from WYSIWYG of any possible Javascript/disallowed tags
			$reservation_email_text = html_purify($this->input->post('reservation_email_text', false));

			$batch_save_data=array(
				'name'=>$this->input->post('name'),
				'address'=>$this->input->post('address'),
				'phone'=>$this->input->post('phone'),
				'email'=>$this->input->post('email'),
				'fax'=>$this->input->post('fax'),
				'city'=>$this->input->post('city'),
				'state'=>$this->input->post('state'),
				'zip'=>$this->input->post('zip'),
			    'website'=>$this->input->post('website'),
				'reservation_email_text'=>$reservation_email_text
			 );

	        $this->session->set_userdata('zip', $this->input->post('zip'));

			if (isset($company_logo))
			{
				$batch_save_data['company_logo'] = $company_logo;
			}
			elseif($this->input->post('delete_logo'))
			{
				$batch_save_data['company_logo'] = 0;
			}

			if (isset($reservation_email_photo))
			{
				$batch_save_data['reservation_email_photo'] = $reservation_email_photo;
			}
			elseif($this->input->post('delete_reservation_email_photo'))
			{
				$batch_save_data['reservation_email_photo'] = 0;
			}
		}
		else if ($settings_section == 'hours')
		{
			$this->load->helper('html_purify');
			
			// Clean HTML from WYSIWYG of any possible Javascript/disallowed tags
			$booking_rules = html_purify($this->input->post('booking_rules', false));
			$no_show_policy = html_purify($this->input->post('no_show_policy', false));
			$terms_and_conditions = html_purify($this->input->post('terms_and_conditions', false));
			$online_booking_welcome_message = html_purify($this->input->post('online_booking_welcome_message', false));
			
            $batch_save_data = array(
				'open_time'=>$this->input->post('open_time'),
				'close_time'=>$this->input->post('close_time'),
				'early_bird_hours_begin'=>$this->input->post('early_bird_hours_begin'),
		        'early_bird_hours_end'=>$this->input->post('early_bird_hours_end'),
		        'morning_hours_begin'=>$this->input->post('morning_hours_begin'),
		        'morning_hours_end'=>$this->input->post('morning_hours_end'),
		        'afternoon_hours_begin'=>$this->input->post('afternoon_hours_begin'),
		        'afternoon_hours_end'=>$this->input->post('afternoon_hours_end'),
		        'twilight_hour'=>$this->input->post('twilight_hour'),
		        'super_twilight_hour'=>$this->input->post('super_twilight_hour'),
		        'holidays'=>$this->input->post('holidays'),
		        'holes'=>$this->input->post('holes'),
		        'online_booking'=>$this->input->post('online_booking'),
		        'online_booking_protected'=>$this->input->post('online_booking_protected'),
		        'include_tax_online_booking'=>$this->input->post('include_tax_online_booking'),
		        'booking_rules'=>$booking_rules,
                'no_show_policy'=>$no_show_policy,
	            'enable_captcha_online' => (int) $this->input->post('enable_captcha_online'),
	            'reservation_limit_per_day' => (int) $this->input->post('reservation_limit_per_day'),
                'terms_and_conditions'=>$terms_and_conditions,
                'online_booking_welcome_message'=>$online_booking_welcome_message,
                'open_sun'=>($this->input->post('open_sun'))?1:0,
		        'open_mon'=>($this->input->post('open_mon'))?1:0,
		        'open_tue'=>($this->input->post('open_tue'))?1:0,
		        'open_wed'=>($this->input->post('open_wed'))?1:0,
		        'open_thu'=>($this->input->post('open_thu'))?1:0,
		        'open_fri'=>($this->input->post('open_fri'))?1:0,
		        'open_sat'=>($this->input->post('open_sat'))?1:0,
		        'weekend_fri'=>($this->input->post('weekend_fri'))?1:0,
		        'weekend_sat'=>($this->input->post('weekend_sat'))?1:0,
		        'weekend_sun'=>($this->input->post('weekend_sun'))?1:0
			);
			if ($batch_save_data['open_time'] == false) {
				unset($batch_save_data['open_time']);
			}
			else {
				$this->session->set_userdata('openhour', $this->input->post('open_time'));
			}
			if ($batch_save_data['close_time'] == false) {
				unset($batch_save_data['close_time']);
			}
			else {
		        $this->session->set_userdata('closehour', $this->input->post('close_time'));
			}
		}
		else if ($settings_section == 'green_fees')
		{
			if (!$this->input->post()) {

			}else {
				// Save teetime price colors
				$this->Green_fee->save_colors($this->input->post('price_colors'));

				// Save green fees
				$price_cartegory_arrays = array();
		        $teesheets = $this->session->userdata('reservations') ? $this->Schedule->get_all() : $this->Teesheet->get_all();
		        foreach($teesheets->result() as $teesheet)
				{
					if ($teesheet->teesheet_id == $this->input->post('price_grid_select'))
					{
						// Save headers
						$price_category_array= array(
							'course_id'=>$course_id,
							'teesheet_id'=>$teesheet->teesheet_id,
							'price_category_1'=>'Regular',
							'price_category_2'=>'Early Bird',
							'price_category_3'=>'Morning',
							'price_category_4'=>'Afternoon',
							'price_category_5'=>'Twilight',
							'price_category_6'=>'Super Twilight',
							'price_category_7'=>'Holiday',
							'price_category_8'=>$this->input->post("price_category_8_".$teesheet->teesheet_id),
							'price_category_9'=>$this->input->post("price_category_9_".$teesheet->teesheet_id),
							'price_category_10'=>$this->input->post("price_category_10_".$teesheet->teesheet_id),
							'price_category_11'=>$this->input->post("price_category_11_".$teesheet->teesheet_id),
							'price_category_12'=>$this->input->post("price_category_12_".$teesheet->teesheet_id),
							'price_category_13'=>$this->input->post("price_category_13_".$teesheet->teesheet_id),
							'price_category_14'=>$this->input->post("price_category_14_".$teesheet->teesheet_id),
							'price_category_15'=>$this->input->post("price_category_15_".$teesheet->teesheet_id),
							'price_category_16'=>$this->input->post("price_category_16_".$teesheet->teesheet_id),
							'price_category_17'=>$this->input->post("price_category_17_".$teesheet->teesheet_id),
							'price_category_18'=>$this->input->post("price_category_18_".$teesheet->teesheet_id),
							'price_category_19'=>$this->input->post("price_category_19_".$teesheet->teesheet_id),
							'price_category_20'=>$this->input->post("price_category_20_".$teesheet->teesheet_id),
							'price_category_21'=>$this->input->post("price_category_21_".$teesheet->teesheet_id),
							'price_category_22'=>$this->input->post("price_category_22_".$teesheet->teesheet_id),
							'price_category_23'=>$this->input->post("price_category_23_".$teesheet->teesheet_id),
							'price_category_24'=>$this->input->post("price_category_24_".$teesheet->teesheet_id),
							'price_category_25'=>$this->input->post("price_category_25_".$teesheet->teesheet_id),
							'price_category_26'=>$this->input->post("price_category_26_".$teesheet->teesheet_id),
							'price_category_27'=>$this->input->post("price_category_27_".$teesheet->teesheet_id),
							'price_category_28'=>$this->input->post("price_category_28_".$teesheet->teesheet_id),
							'price_category_29'=>$this->input->post("price_category_29_".$teesheet->teesheet_id),
							'price_category_30'=>$this->input->post("price_category_30_".$teesheet->teesheet_id),
							'price_category_31'=>$this->input->post("price_category_31_".$teesheet->teesheet_id),
							'price_category_32'=>$this->input->post("price_category_32_".$teesheet->teesheet_id),
							'price_category_33'=>$this->input->post("price_category_33_".$teesheet->teesheet_id),
							'price_category_34'=>$this->input->post("price_category_34_".$teesheet->teesheet_id),
							'price_category_35'=>$this->input->post("price_category_35_".$teesheet->teesheet_id),
							'price_category_36'=>$this->input->post("price_category_36_".$teesheet->teesheet_id),
							'price_category_37'=>$this->input->post("price_category_37_".$teesheet->teesheet_id),
							'price_category_38'=>$this->input->post("price_category_38_".$teesheet->teesheet_id),
							'price_category_39'=>$this->input->post("price_category_39_".$teesheet->teesheet_id),
							'price_category_40'=>$this->input->post("price_category_40_".$teesheet->teesheet_id),
							'price_category_41'=>$this->input->post("price_category_41_".$teesheet->teesheet_id),
							'price_category_42'=>$this->input->post("price_category_42_".$teesheet->teesheet_id),
							'price_category_43'=>$this->input->post("price_category_43_".$teesheet->teesheet_id),
							'price_category_44'=>$this->input->post("price_category_44_".$teesheet->teesheet_id),
							'price_category_45'=>$this->input->post("price_category_45_".$teesheet->teesheet_id),
							'price_category_46'=>$this->input->post("price_category_46_".$teesheet->teesheet_id),
							'price_category_47'=>$this->input->post("price_category_47_".$teesheet->teesheet_id),
							'price_category_48'=>$this->input->post("price_category_48_".$teesheet->teesheet_id),
							'price_category_49'=>$this->input->post("price_category_49_".$teesheet->teesheet_id),
							'price_category_50'=>$this->input->post("price_category_50_".$teesheet->teesheet_id)
						);
						$this->Green_fee->save_types($price_category_array);
						$default_price_class = $this->input->post('default_price_class_'.$teesheet->teesheet_id);
						$teesheet_data = array('default_price_class'=>$default_price_class);
						if ($this->session->userdata('reservations'))
						{
							$this->Schedule->save($teesheet_data, $teesheet->teesheet_id);
						}
						else
						{
							$this->Teesheet->save($teesheet_data, $teesheet->teesheet_id);
						}
						if ($this->session->userdata('teesheet_id') == $teesheet->teesheet_id)
							$this->session->set_userdata('default_price_class', $default_price_class);
						for ($i = 1; $i <=8; $i++)
						{
							$price_array= array(
								'item_number'=>$course_id.'_'.$i,
								'teesheet_id'=>$teesheet->teesheet_id,
								'price_category_1'=>$this->input->post("{$teesheet->teesheet_id}_1_{$i}"),
								'price_category_2'=>$this->input->post("{$teesheet->teesheet_id}_2_{$i}"),
								'price_category_3'=>$this->input->post("{$teesheet->teesheet_id}_3_{$i}"),
								'price_category_4'=>$this->input->post("{$teesheet->teesheet_id}_4_{$i}"),
								'price_category_5'=>$this->input->post("{$teesheet->teesheet_id}_5_{$i}"),
								'price_category_6'=>$this->input->post("{$teesheet->teesheet_id}_6_{$i}"),
								'price_category_7'=>$this->input->post("{$teesheet->teesheet_id}_7_{$i}"),
								'price_category_8'=>$this->input->post("{$teesheet->teesheet_id}_8_{$i}"),
								'price_category_9'=>$this->input->post("{$teesheet->teesheet_id}_9_{$i}"),
								'price_category_10'=>$this->input->post("{$teesheet->teesheet_id}_10_{$i}"),
								'price_category_11'=>$this->input->post("{$teesheet->teesheet_id}_11_{$i}"),
								'price_category_12'=>$this->input->post("{$teesheet->teesheet_id}_12_{$i}"),
								'price_category_13'=>$this->input->post("{$teesheet->teesheet_id}_13_{$i}"),
								'price_category_14'=>$this->input->post("{$teesheet->teesheet_id}_14_{$i}"),
								'price_category_15'=>$this->input->post("{$teesheet->teesheet_id}_15_{$i}"),
								'price_category_16'=>$this->input->post("{$teesheet->teesheet_id}_16_{$i}"),
								'price_category_17'=>$this->input->post("{$teesheet->teesheet_id}_17_{$i}"),
								'price_category_18'=>$this->input->post("{$teesheet->teesheet_id}_18_{$i}"),
								'price_category_19'=>$this->input->post("{$teesheet->teesheet_id}_19_{$i}"),
								'price_category_20'=>$this->input->post("{$teesheet->teesheet_id}_20_{$i}"),
								'price_category_21'=>$this->input->post("{$teesheet->teesheet_id}_21_{$i}"),
								'price_category_22'=>$this->input->post("{$teesheet->teesheet_id}_22_{$i}"),
								'price_category_23'=>$this->input->post("{$teesheet->teesheet_id}_23_{$i}"),
								'price_category_24'=>$this->input->post("{$teesheet->teesheet_id}_24_{$i}"),
								'price_category_25'=>$this->input->post("{$teesheet->teesheet_id}_25_{$i}"),
								'price_category_26'=>$this->input->post("{$teesheet->teesheet_id}_26_{$i}"),
								'price_category_27'=>$this->input->post("{$teesheet->teesheet_id}_27_{$i}"),
								'price_category_28'=>$this->input->post("{$teesheet->teesheet_id}_28_{$i}"),
								'price_category_29'=>$this->input->post("{$teesheet->teesheet_id}_29_{$i}"),
								'price_category_30'=>$this->input->post("{$teesheet->teesheet_id}_30_{$i}"),
								'price_category_31'=>$this->input->post("{$teesheet->teesheet_id}_31_{$i}"),
								'price_category_32'=>$this->input->post("{$teesheet->teesheet_id}_32_{$i}"),
								'price_category_33'=>$this->input->post("{$teesheet->teesheet_id}_33_{$i}"),
								'price_category_34'=>$this->input->post("{$teesheet->teesheet_id}_34_{$i}"),
								'price_category_35'=>$this->input->post("{$teesheet->teesheet_id}_35_{$i}"),
								'price_category_36'=>$this->input->post("{$teesheet->teesheet_id}_36_{$i}"),
								'price_category_37'=>$this->input->post("{$teesheet->teesheet_id}_37_{$i}"),
								'price_category_38'=>$this->input->post("{$teesheet->teesheet_id}_38_{$i}"),
								'price_category_39'=>$this->input->post("{$teesheet->teesheet_id}_39_{$i}"),
								'price_category_40'=>$this->input->post("{$teesheet->teesheet_id}_40_{$i}"),
								'price_category_41'=>$this->input->post("{$teesheet->teesheet_id}_41_{$i}"),
								'price_category_42'=>$this->input->post("{$teesheet->teesheet_id}_42_{$i}"),
								'price_category_43'=>$this->input->post("{$teesheet->teesheet_id}_43_{$i}"),
								'price_category_44'=>$this->input->post("{$teesheet->teesheet_id}_44_{$i}"),
								'price_category_45'=>$this->input->post("{$teesheet->teesheet_id}_45_{$i}"),
								'price_category_46'=>$this->input->post("{$teesheet->teesheet_id}_46_{$i}"),
								'price_category_47'=>$this->input->post("{$teesheet->teesheet_id}_47_{$i}"),
								'price_category_48'=>$this->input->post("{$teesheet->teesheet_id}_48_{$i}"),
								'price_category_49'=>$this->input->post("{$teesheet->teesheet_id}_49_{$i}"),
								'price_category_50'=>$this->input->post("{$teesheet->teesheet_id}_50_{$i}")
							);
							$this->Green_fee->save($price_array);
						}
					}
				}
				$price_sql = $this->db->last_query();

		        // Save taxes and departments
		        // Teetime Department
				$teetime_department = $this->input->post('teetime_department');
		        $teetime_tax_rate = $this->input->post('teetime_tax_rate');
		        $cart_department = $this->input->post('cart_department');
		        $cart_tax_rate = $this->input->post('cart_tax_rate');

		        for ($i = 1; $i <=8; $i++)
				{
					$item_id = $this->Item->get_item_id("{$course_id}_{$i}");
					$department = ($i==1 || $i==2 || $i==5 || $i==6)?$cart_department:$teetime_department;
					$tax_rate = ($i==1 || $i==2 || $i==5 || $i==6)?$cart_tax_rate:$teetime_tax_rate;

					$item_data = array('department'=>$department);
					$this->Item->save($item_data, $item_id);
					//echo $this->db->last_query();

					$items_taxes_data = array();
					$items_taxes_data[] = array('course_id'=>$course_id,'name'=>'Sales Tax', 'percent'=>$tax_rate, 'cumulative' => '0' );
					$this->Item_taxes->save($items_taxes_data, $item_id);
				}
			}
			$batch_save_data=array('course_id'=>$course_id);
		}
		else if ($settings_section == 'fees')
		{
			if (!$this->input->post()) {

			}
			else {
			// Save green fees
				$price_cartegory_arrays = array();
		        $schedules = $this->Schedule->get_all();
		        foreach($schedules->result() as $schedule)
				{
					// Save headers
					$price_category_array= array(
						'course_id'=>$course_id,
						'schedule_id'=>$schedule->schedule_id,
						'price_category_1'=>'Regular',
						'price_category_2'=>'Early Bird',
						'price_category_3'=>'Morning',
						'price_category_4'=>'Afternoon',
						'price_category_5'=>'Twilight',
						'price_category_6'=>'Super Twilight',
						'price_category_7'=>'Holiday',
						'price_category_8'=>$this->input->post("price_category_8_".$schedule->schedule_id),
						'price_category_9'=>$this->input->post("price_category_9_".$schedule->schedule_id),
						'price_category_10'=>$this->input->post("price_category_10_".$schedule->schedule_id),
						'price_category_11'=>$this->input->post("price_category_11_".$schedule->schedule_id),
						'price_category_12'=>$this->input->post("price_category_12_".$schedule->schedule_id),
						'price_category_13'=>$this->input->post("price_category_13_".$schedule->schedule_id),
						'price_category_14'=>$this->input->post("price_category_14_".$schedule->schedule_id),
						'price_category_15'=>$this->input->post("price_category_15_".$schedule->schedule_id),
						'price_category_16'=>$this->input->post("price_category_16_".$schedule->schedule_id),
						'price_category_17'=>$this->input->post("price_category_17_".$schedule->schedule_id),
						'price_category_18'=>$this->input->post("price_category_18_".$schedule->schedule_id),
						'price_category_19'=>$this->input->post("price_category_19_".$schedule->schedule_id),
						'price_category_20'=>$this->input->post("price_category_20_".$schedule->schedule_id),
						'price_category_21'=>$this->input->post("price_category_21_".$schedule->schedule_id),
						'price_category_22'=>$this->input->post("price_category_22_".$schedule->schedule_id),
						'price_category_23'=>$this->input->post("price_category_23_".$schedule->schedule_id),
						'price_category_24'=>$this->input->post("price_category_24_".$schedule->schedule_id),
						'price_category_25'=>$this->input->post("price_category_25_".$schedule->schedule_id),
						'price_category_26'=>$this->input->post("price_category_26_".$schedule->schedule_id),
						'price_category_27'=>$this->input->post("price_category_27_".$schedule->schedule_id),
						'price_category_28'=>$this->input->post("price_category_28_".$schedule->schedule_id),
						'price_category_29'=>$this->input->post("price_category_29_".$schedule->schedule_id),
						'price_category_30'=>$this->input->post("price_category_30_".$schedule->schedule_id),
						'price_category_31'=>$this->input->post("price_category_31_".$schedule->schedule_id),
						'price_category_32'=>$this->input->post("price_category_32_".$schedule->schedule_id),
						'price_category_33'=>$this->input->post("price_category_33_".$schedule->schedule_id),
						'price_category_34'=>$this->input->post("price_category_34_".$schedule->schedule_id),
						'price_category_35'=>$this->input->post("price_category_35_".$schedule->schedule_id),
						'price_category_36'=>$this->input->post("price_category_36_".$schedule->schedule_id),
						'price_category_37'=>$this->input->post("price_category_37_".$schedule->schedule_id),
						'price_category_38'=>$this->input->post("price_category_38_".$schedule->schedule_id),
						'price_category_39'=>$this->input->post("price_category_39_".$schedule->schedule_id),
						'price_category_40'=>$this->input->post("price_category_40_".$schedule->schedule_id),
						'price_category_41'=>$this->input->post("price_category_41_".$schedule->schedule_id),
						'price_category_42'=>$this->input->post("price_category_42_".$schedule->schedule_id),
						'price_category_43'=>$this->input->post("price_category_43_".$schedule->schedule_id),
						'price_category_44'=>$this->input->post("price_category_44_".$schedule->schedule_id),
						'price_category_45'=>$this->input->post("price_category_45_".$schedule->schedule_id),
						'price_category_46'=>$this->input->post("price_category_46_".$schedule->schedule_id),
						'price_category_47'=>$this->input->post("price_category_47_".$schedule->schedule_id),
						'price_category_48'=>$this->input->post("price_category_48_".$schedule->schedule_id),
						'price_category_49'=>$this->input->post("price_category_49_".$schedule->schedule_id),
						'price_category_50'=>$this->input->post("price_category_50_".$schedule->schedule_id)
					);
					$this->Fee->save_types($price_category_array);
					$default_price_class = $this->input->post('default_price_class_'.$schedule->schedule_id);
					$schedule_data = array('default_price_class'=>$default_price_class);
					$this->Schedule->save($schedule_data, $schedule->schedule_id);

					if ($this->session->userdata('schedule_id') == $schedule->schedule_id)
						$this->session->set_userdata('default_price_class', $default_price_class);
					for ($i = 1; $i <=8; $i++)
					{
						$price_array= array(
							'item_number'=>$course_id.'_'.$i,
							'schedule_id'=>$schedule->schedule_id,
							'price_category_1'=>$this->input->post("{$schedule->schedule_id}_1_{$i}"),
							'price_category_2'=>$this->input->post("{$schedule->schedule_id}_2_{$i}"),
							'price_category_3'=>$this->input->post("{$schedule->schedule_id}_3_{$i}"),
							'price_category_4'=>$this->input->post("{$schedule->schedule_id}_4_{$i}"),
							'price_category_5'=>$this->input->post("{$schedule->schedule_id}_5_{$i}"),
							'price_category_6'=>$this->input->post("{$schedule->schedule_id}_6_{$i}"),
							'price_category_7'=>$this->input->post("{$schedule->schedule_id}_7_{$i}"),
							'price_category_8'=>$this->input->post("{$schedule->schedule_id}_8_{$i}"),
							'price_category_9'=>$this->input->post("{$schedule->schedule_id}_9_{$i}"),
							'price_category_10'=>$this->input->post("{$schedule->schedule_id}_10_{$i}"),
							'price_category_11'=>$this->input->post("{$schedule->schedule_id}_11_{$i}"),
							'price_category_12'=>$this->input->post("{$schedule->schedule_id}_12_{$i}"),
							'price_category_13'=>$this->input->post("{$schedule->schedule_id}_13_{$i}"),
							'price_category_14'=>$this->input->post("{$schedule->schedule_id}_14_{$i}"),
							'price_category_15'=>$this->input->post("{$schedule->schedule_id}_15_{$i}"),
							'price_category_16'=>$this->input->post("{$schedule->schedule_id}_16_{$i}"),
							'price_category_17'=>$this->input->post("{$schedule->schedule_id}_17_{$i}"),
							'price_category_18'=>$this->input->post("{$schedule->schedule_id}_18_{$i}"),
							'price_category_19'=>$this->input->post("{$schedule->schedule_id}_19_{$i}"),
							'price_category_20'=>$this->input->post("{$schedule->schedule_id}_20_{$i}"),
							'price_category_21'=>$this->input->post("{$schedule->schedule_id}_21_{$i}"),
							'price_category_22'=>$this->input->post("{$schedule->schedule_id}_22_{$i}"),
							'price_category_23'=>$this->input->post("{$schedule->schedule_id}_23_{$i}"),
							'price_category_24'=>$this->input->post("{$schedule->schedule_id}_24_{$i}"),
							'price_category_25'=>$this->input->post("{$schedule->schedule_id}_25_{$i}"),
							'price_category_26'=>$this->input->post("{$schedule->schedule_id}_26_{$i}"),
							'price_category_27'=>$this->input->post("{$schedule->schedule_id}_27_{$i}"),
							'price_category_28'=>$this->input->post("{$schedule->schedule_id}_28_{$i}"),
							'price_category_29'=>$this->input->post("{$schedule->schedule_id}_29_{$i}"),
							'price_category_30'=>$this->input->post("{$schedule->schedule_id}_30_{$i}"),
							'price_category_31'=>$this->input->post("{$schedule->schedule_id}_31_{$i}"),
							'price_category_32'=>$this->input->post("{$schedule->schedule_id}_32_{$i}"),
							'price_category_33'=>$this->input->post("{$schedule->schedule_id}_33_{$i}"),
							'price_category_34'=>$this->input->post("{$schedule->schedule_id}_34_{$i}"),
							'price_category_35'=>$this->input->post("{$schedule->schedule_id}_35_{$i}"),
							'price_category_36'=>$this->input->post("{$schedule->schedule_id}_36_{$i}"),
							'price_category_37'=>$this->input->post("{$schedule->schedule_id}_37_{$i}"),
							'price_category_38'=>$this->input->post("{$schedule->schedule_id}_38_{$i}"),
							'price_category_39'=>$this->input->post("{$schedule->schedule_id}_39_{$i}"),
							'price_category_40'=>$this->input->post("{$schedule->schedule_id}_40_{$i}"),
							'price_category_41'=>$this->input->post("{$schedule->schedule_id}_41_{$i}"),
							'price_category_42'=>$this->input->post("{$schedule->schedule_id}_42_{$i}"),
							'price_category_43'=>$this->input->post("{$schedule->schedule_id}_43_{$i}"),
							'price_category_44'=>$this->input->post("{$schedule->schedule_id}_44_{$i}"),
							'price_category_45'=>$this->input->post("{$schedule->schedule_id}_45_{$i}"),
							'price_category_46'=>$this->input->post("{$schedule->schedule_id}_46_{$i}"),
							'price_category_47'=>$this->input->post("{$schedule->schedule_id}_47_{$i}"),
							'price_category_48'=>$this->input->post("{$schedule->schedule_id}_48_{$i}"),
							'price_category_49'=>$this->input->post("{$schedule->schedule_id}_49_{$i}"),
							'price_category_50'=>$this->input->post("{$schedule->schedule_id}_50_{$i}")
						);
						$this->Fee->save($price_array);
					}
				}
				$price_sql = $this->db->last_query();

		        // Save taxes and departments
		        // Teetime Department
                $simulator = $this->session->userdata('reservations');
				$reservation_department = $this->input->post('teetime_department');
		        $reservation_tax_rate = $this->input->post('teetime_tax_rate');
		        $cart_department = $this->input->post('cart_department');
		        $cart_tax_rate = $this->input->post('cart_tax_rate');

		        for ($i = 1; $i <=8; $i++)
				{
					$item_id = $this->Item->get_item_id("{$course_id}_{$i}");
					$department = (!$simulator && ($i==1 || $i==2 || $i==5 || $i==6))?$cart_department:$reservation_department;
					$tax_rate = (!$simulator && ($i==1 || $i==2 || $i==5 || $i==6))?$cart_tax_rate:$reservation_tax_rate;

					$item_data = array('department'=>$department);
                    if ($simulator) {
                        $item_data['category'] = 'Green Fees';
                    }
					$this->Item->save($item_data, $item_id);

					$items_taxes_data = array();
					$items_taxes_data[] = array('course_id'=>$course_id,'name'=>'Sales Tax', 'percent'=>$tax_rate, 'cumulative' => '0' );
					$this->Item_taxes->save($items_taxes_data, $item_id);
				}
			}
			$batch_save_data=array('course_id'=>$course_id);
		}
		else if ($settings_section == 'pos')
		{
			$batch_save_data=array(
				'default_tax_1_rate'=>$this->input->post('default_tax_1_rate'),
				'default_tax_1_name'=>$this->input->post('default_tax_1_name'),
				'default_tax_2_rate'=>$this->input->post('default_tax_2_rate'),
				'default_tax_2_name'=>$this->input->post('default_tax_2_name'),
				'default_tax_2_cumulative' => $this->input->post('default_tax_2_cumulative') ? 1 : 0,
				'auto_gratuity' => $this->input->post('auto_gratuity'),
				'auto_gratuity_threshold' => (int) $this->input->post('auto_gratuity_threshold'),
                'service_fee_active' => $this->input->post('service_fee_active') ? 1 : 0,
                'service_fee_tax_1_name' => $this->input->post('service_fee_tax_1_name'),
                'service_fee_tax_1_rate' => $this->input->post('service_fee_tax_1_rate'),
                'service_fee_tax_2_name' => $this->input->post('service_fee_tax_2_name'),
                'service_fee_tax_2_rate' => $this->input->post('service_fee_tax_2_rate'),
                'credit_card_fee' => $this->input->post('credit_card_fee'),
				'unit_price_includes_tax' => $this->input->post('unit_price_includes_tax') ? 1 : 0,
				'customer_credit_nickname'=>trim($this->input->post('customer_credit_nickname')),
				'member_balance_nickname'=>trim($this->input->post('member_balance_nickname')),
				'credit_department' => $this->input->post('credit_department_category') == 0 ? 1 : 0,
				'credit_department_name' => $this->input->post('department'),
				'credit_category' => $this->input->post('credit_department_category') == 1 ? 1 : 0,
				'credit_category_name' => $this->input->post('category'),
				'credit_subtotal_only' => $this->input->post('customer_credit_subtotal_only') == 1 ? 1 : 0,
				'enable_captcha_online' => $this->input->post('enable_captcha_online') == 1 ? 1 : 0,
				'reservation_limit_per_day' => $this->input->post('reservation_limit_per_day') == 1 ? 1 : 0,
				'nonfiltered_giftcard_barcodes' => $this->input->post('nonfiltered_giftcard_barcodes') == 1 ? 1 : 0,
				'return_policy'=>preg_replace("/\s+/", " ", $this->input->post('return_policy')),
				'print_after_sale'=>$this->input->post('print_after_sale'),
				'webprnt'=>$this->input->post('webprnt'),
				'webprnt_ip'=>$this->input->post('webprnt_ip'),
				'webprnt_hot_ip'=>$this->input->post('webprnt_hot_ip'),
				'webprnt_cold_ip'=>$this->input->post('webprnt_cold_ip'),
				'webprnt_label_ip'=>$this->input->post('webprnt_label_ip'),
                'use_kitchen_buzzers'=>$this->input->post('use_kitchen_buzzers'),
                'print_credit_card_receipt'=>$this->input->post('print_credit_card_receipt'),
				'print_sales_receipt'=>$this->input->post('print_sales_receipt'),
				'print_two_receipts'=>$this->input->post('print_two_receipts'),
				'print_two_signature_slips'=>$this->input->post('print_two_signature_slips'),
				'print_two_receipts_other'=>$this->input->post('print_two_receipts_other'),
				'print_tip_line'=>$this->input->post('print_tip_line'),
				'cash_drawer_on_cash'=>$this->input->post('cash_drawer_on_cash'),
				'cash_drawer_on_sale'=>$this->input->post('cash_drawer_on_sale'),
				'updated_printing'=>$this->input->post('updated_printing'),
				'after_sale_load'=>$this->input->post('after_sale_load'),
				'teesheet_updates_automatically'=>$this->input->post('teesheet_updates_automatically')?1:0,
				'send_reservation_confirmations'=>$this->input->post('send_reservation_confirmations')?1:0,
				'auto_split_teetimes'=>$this->input->post('auto_split_teetimes')?1:0,
				'fnb_login'=>$this->input->post('fnb_login'),
				'receipt_printer'=>$this->input->post('receipt_printer'),
				'track_cash' => $this->input->post('track_cash'),
				'blind_close' => $this->input->post('blind_close'),
				'require_employee_pin' => $this->input->post('require_employee_pin'),
				'minimum_food_spend' => $this->input->post('minimum_food_spend'),
				'separate_courses' => $this->input->post('separate_courses'),
				'default_kitchen_printer' => $this->input->post('default_kitchen_printer'),
				'hide_employee_last_name_receipt' => $this->input->post('hide_employee_last_name_receipt'),
				'require_guest_count' => $this->input->post('require_guest_count'),
				'use_course_firing' => $this->input->post('use_course_firing'),
				'course_firing_include_items' => $this->input->post('course_firing_include_items'),
				'hide_modifier_names_kitchen_receipts' => $this->input->post('hide_modifier_names_kitchen_receipts'),
				'food_bev_sort_by_seat' => $this->input->post('food_bev_sort_by_seat'),
				'default_register_log_open' => (float) $this->input->post('default_register_log_open'),
				'print_suggested_tip' => (int) $this->input->post('print_suggested_tip'),
				'food_bev_tip_line_first_receipt' => (int) $this->input->post('food_bev_tip_line_first_receipt'),
				'auto_email_receipt' => (int) $this->input->post('auto_email_receipt'),
				'auto_email_no_print' => (int) $this->input->post('auto_email_no_print'),
				'require_signature_member_payments' => (int) $this->input->post('require_signature_member_payments'),
                'hide_taxable' => (int) $this->input->post('hide_taxable'),
                'allow_employee_register_log_bypass' => (int) $this->input->post('allow_employee_register_log_bypass'),
                'teetime_default_to_player1' => (int) $this->input->post('teetime_default_to_player1'),
                'print_tee_time_details' => (int) $this->input->post('print_tee_time_details'),
                'receipt_print_account_balance' => (int) $this->input->post('receipt_print_account_balance'),
                'print_minimums_on_receipt' => (int) $this->input->post('print_minimums_on_receipt'),
                'limit_fee_dropdown_by_customer' => (int) $this->input->post('limit_fee_dropdown_by_customer'),
                'nonfiltered_giftcard_barcodes' => (int) $this->input->post('nonfiltered_giftcard_barcodes'),
                'require_customer_on_sale' => (int) $this->input->post('require_customer_on_sale')
			);

            if ($batch_save_data['service_fee_active']) {
                // Create our service fee item if it doesn't exist already
                $this->load->model('sale');
                $this->load->model('Item_taxes');
                $item_id = $this->sale->get_balance_item('service_fee');
                // Update service fee item to tax rates
                $item_taxes_data = array();
                if ($batch_save_data['service_fee_tax_1_rate'] > 0) {
                    $items_taxes_data[] = array(
                        'course_id' => $this->session->userdata('course_id'),
                        'item_id' => $item_id,
                        'name' => $batch_save_data['service_fee_tax_1_name'],
                        'percent' => $batch_save_data['service_fee_tax_1_rate'],
                        'cumulative' => 0
                    );
                }
                if ($batch_save_data['service_fee_tax_2_rate'] > 0) {
                    $items_taxes_data[] = array(
                        'course_id' => $this->session->userdata('course_id'),
                        'item_id' => $item_id,
                        'name' => $batch_save_data['service_fee_tax_2_name'],
                        'percent' => $batch_save_data['service_fee_tax_2_rate'],
                        'cumulative' => 0
                    );
                }
                $this->Item_taxes->save($items_taxes_data, $item_id);
            }
			$printer_groups = $this->input->post('printer_groups');
			if($printer_groups){
				$this->load->model('v2/Printer_group_model');
				$this->Printer_group_model->save_default_printers($printer_groups);
			}
		}
		else if ($settings_section == 'misc')
		{
	        $batch_save_data=array(
				'timezone'=>$this->input->post('timezone'),
				'use_new_permissions'=>$this->input->post('use_new_permissions'),
				'date_format'=>$this->input->post('date_format'),
				'time_format'=>$this->input->post('time_format'),
				'mailchimp_api_key'=>$this->input->post('mailchimp_api_key'),
                'billing_email'=>$this->input->post('billing_email'),
                'reservation_email'=>$this->input->post('reservation_email'),
                'number_of_items_per_page'=>$this->input->post('number_of_items_per_page'),
                'hide_back_nine'=>$this->input->post('hide_back_nine')?1:0,
                'stack_tee_sheets'=>$this->input->post('stack_tee_sheets')?1:0,
                'side_by_side_tee_sheets'=>$this->input->post('side_by_side_tee_sheets')?1:0,
                'always_list_all'=>$this->input->post('always_list_all')?1:0,
                'current_day_checkins_only'=>$this->input->post('current_day_checkins_only')?1:0,
                'use_ets_giftcards'=>$this->input->post('use_ets_giftcards')?1:0,
				'deduct_tips'=>$this->input->post('deduct_tips')?1:0,
				'online_giftcard_purchases'=>$this->input->post('online_giftcard_purchases')?1:0,
                'show_invoice_sale_items'=>$this->input->post('show_invoice_sale_items')?1:0,
                'teed_off_color'=>$this->input->post('teed_off_color'),
                'tee_time_completed_color'=>$this->input->post('tee_time_completed_color')
		    );

			if($this->input->post('use_new_permissions'))
			{
				$this->load->model("acl");
				$this->acl->activate_new_permission();
			}

	        $this->load->model('v2/Customer_model');
	        $customer_field_settings = $this->input->post('customer_field_settings');
	        $customer_field_settings['first_name']['required'] = 1;
	        $customer_field_settings['last_name']['required'] = 1;
	       	$this->Customer_model->save_field_settings($customer_field_settings);
	       	
			$this->session->set_userdata('hide_back_nine', $batch_save_data['hide_back_nine']);
		}
		else if ($settings_section == 'quickbooks_export')
		{
	        $quickbooks_settings = $this->input->post();
	        $quickbooks_settings = json_encode($quickbooks_settings);
			
			$batch_save_data = array('quickbooks_export_settings' => $quickbooks_settings, 'course_id' => $this->session->userdata('course_id'));
			$this->session->set_userdata('quickbooks_export_settings', $quickbooks_settings);
		}		
		else if ($settings_section == 'sales')
        {
            $batch_save_data=array(
                'print_after_sale'=>$this->input->post('print_after_sale'),
                'print_two_receipts'=>$this->input->post('print_two_receipts'),
                'print_two_receipts_other'=>$this->input->post('print_two_receipts_other'),
                'after_sale_load'=>$this->input->post('after_sale_load'),
        		'return_policy'=>$this->input->post('return_policy'),
		        //'receipt_printer'=>$this->input->post('receipt_printer')
                'track_cash' => $this->input->post('track_cash'),
            );
        }
		else if ($settings_section == 'teesheet')
        {
            $batch_save_data=array(
                'teesheet_updates_automatically'=>$this->input->post('teesheet_updates_automatically')?1:0,
                'send_reservation_confirmations'=>$this->input->post('teetime_confirmations')?1:0,
                'teetime_reminders'=>$this->input->post('teetime_reminders')?1:0
            );
        }
		else if ($settings_section == 'terminals')
		{
			$terminal_labels = $this->input->post('terminal_label');
			$terminal_tabs = $this->input->post('terminal_tab');
			$terminal_ids = $this->input->post('terminal_id');
			$delete_terminal_ids = $this->input->post('delete_terminal_id');
			$use_terminals = $this->input->post('use_terminals')?1:0;
			$online_purchase_terminal_id = $this->input->post('online_purchase_terminal_id');
			$online_invoice_terminal_id = $this->input->post('online_invoice_terminal_id');
            $this->session->set_userdata('use_terminals', $use_terminals);

			$batch_save_data = array(
				'course_id' => $course_id, 
				'use_terminals' => $use_terminals, 
				'online_purchase_terminal_id' => $online_purchase_terminal_id,
				'online_invoice_terminal_id' => $online_invoice_terminal_id
			);
		}
		else if ($settings_section == 'loyalty')
		{
			$loyalty_labels = $this->input->post('loyalty_label');
			$loyalty_rate_ids = $this->input->post('loyalty_rate_id');
			$delete_loyalty_rate_ids = $this->input->post('delete_loyalty_rate_id');
			$loyalty_filters = $this->input->post('loyalty_filter');
			$loyalty_types = $this->input->post('type');
			$loyalty_value = $this->input->post('value');
			$loyalty_tee_time_indexes = $this->input->post('tee_time_index');
			$loyalty_price_categories = $this->input->post('price_category');
			$limit_timeframe = $this->input->post('limit_timeframe');
			$limit_timeframe_hidden = $this->input->post('limit_timeframe_hidden');
			$timeframe_id = $this->input->post('timeframe_id');
			$points_per_dollar = $this->input->post('points_per_dollar');
			$dollars_per_point = $this->input->post('dollars_per_point');
            $use_loyalty = $this->input->post('use_loyalty')?1:0;
            $loyalty_auto_enroll = $this->input->post('loyalty_auto_enroll')?1:0;

            foreach($loyalty_labels as $index => $loyalty_label)
			{
				$loyalty_data = array(
					'label'				=>$loyalty_label,
					'course_id'			=> $this->session->userdata('course_id'),
					'type'				=>$loyalty_types[$index],
					'value'				=>$loyalty_value[$index],
					'value_label'		=>$loyalty_filters[$index],
					'tee_time_index'	=>$loyalty_tee_time_indexes[$index],
					'price_category'	=>$loyalty_price_categories[$index],
					'points_per_dollar'	=>$points_per_dollar[$index],
					'limit_timeframe'	=>$limit_timeframe_hidden[$index]==true?1:0,
					'timeframe_id'	=>$timeframe_id[$index],
					'dollars_per_point'	=>$dollars_per_point[$index]
				);
				if (isset($delete_loyalty_rate_ids[$index]) && $delete_loyalty_rate_ids[$index] == '1')
					$this->Customer_loyalty->delete($loyalty_rate_ids[$index]);
				else
					$this->Customer_loyalty->save($loyalty_data, $loyalty_rate_ids[$index]);
			}

			$batch_save_data = array('course_id' => $course_id, 'use_loyalty' => $use_loyalty, 'loyalty_auto_enroll' => $loyalty_auto_enroll);
		
		// Save tax rates/info for seasonal pricing
		}else if($settings_section == 'seasonal_pricing_taxes'){
			
			$teetime_department = $this->input->post('teetime_department');
			$teetime_tax_rate = $this->input->post('teetime_tax_rate');
			$cart_department = $this->input->post('cart_department');
			$cart_tax_rate = $this->input->post('cart_tax_rate');
			
			$cart_fee_tax_included = (int) $this->input->post('cart_fee_tax_included');
			$green_fee_tax_included = $this->input->post('green_fee_tax_included');

			for ($i = 1; $i <= 4; $i++)
			{
				$item_id = $this->Item->get_item_id("{$course_id}_seasonal_{$i}");
				$department = ($i >= 3)? $cart_department : $teetime_department;
				$tax_rate = ($i >= 3) ? $cart_tax_rate : $teetime_tax_rate;
				$tax_included = ($i >= 3) ? $cart_fee_tax_included : $green_fee_tax_included;

				$item_data = array(
					'department' => $department,
					'unit_price_includes_tax' => $tax_included
				);
				$this->Item->save($item_data, $item_id);
				
				$items_taxes_data = array();
				$items_taxes_data[] = array('course_id'=>$course_id, 'name'=>'Sales Tax', 'percent'=>$tax_rate, 'cumulative' => '0' );

				$this->Item_taxes->save($items_taxes_data, $item_id);
			}
			
			echo json_encode(array('success'=>true,'message'=>lang('config_saved_successfully')));
			return true;		
		}
		
		$open_time = $this->config->item('open_time');
		if($this->Appconfig->save_all($batch_save_data))
		{
			$this->Action->save('settings', 0, 'update '.$settings_section, json_encode($batch_save_data));
			if ($settings_section == 'hours' && $this->input->post('open_time') && $open_time != $this->input->post('open_time'))
				$this->Teesheet->adjust_teetimes();
			echo json_encode(array('success'=>true,
			'message'=>lang('config_saved_successfully')
			));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('config_saved_unsuccessfully')));
		}
	}

	function view_printers()
	{
		$this->load->model('v2/Printer_model');
		$this->load->model('v2/Printer_group_model');
		
		$data = array();
		$data['printers'] = $this->Printer_model->get();
		$data['printer_groups'] = $this->Printer_group_model->get();
		
		$this->load->view('config/printers', $data);
	}
	
	function save_printer($printer_id = null)
	{
		$this->load->model('v2/Printer_model');
		
		$printer_data = array(
			'label' => $this->input->post('label'), 
			'ip_address' => $this->input->post('ip_address'),
		);
		$printer_id = $this->Printer_model->save($printer_id, $printer_data);
		
		echo json_encode(array('printer_id' => $printer_id));
	}
	
	function delete_printer()
	{
		$this->load->model('v2/Printer_model');
		$printer_id = $this->input->post('printer_id');
		$response = $this->Printer_model->delete($printer_id);
		
		echo json_encode(array('printer_id'=>$printer_id, 'success' => (bool) $response));
	}
	
	function save_printer_group($printer_group_id = null)
	{
		$this->load->model('v2/Printer_group_model');
		
		$printer_group_data = array(
			'label' => $this->input->post('label'), 
		);
		$printer_group_id = $this->Printer_group_model->save($printer_group_id, $printer_group_data);
		
		echo json_encode(array('printer_group_id' => $printer_group_id));
	}
	
	function delete_printer_group()
	{
		$this->load->model('v2/Printer_group_model');
		$printer_group_id = $this->input->post('printer_group_id');
		$response = $this->Printer_group_model->delete($printer_group_id);
		
		echo json_encode(array('printer_group_id'=>$printer_group_id, 'success' => (bool) $response));
	}			

	function manage_reminders(){

		$this->load->model('Reminder');
		$reminder = new Reminder();
		$reminders = $reminder->get_many_by([
			"course_id"=>$this->session->userdata('course_id')
		]);
		$data = [
			"reminders"=>$reminders
		];
		$this->load->view('config/reminders', $data);
	}
	function delete_reminder($id){
		$this->load->model('Reminder');
		$reminder = new Reminder();
		$reminder->delete_by([
			"id"=>$id,
			"course_id"=>$this->session->userdata('course_id'),
		]);
		return json_encode([
			"success"=>true
		]);
	}
	function add_reminder(){
		$this->load->model('Reminder');
		$reminder = new Reminder();
		$data = [
			"type"=>"text",
			"course_id"=>$this->session->userdata('course_id')
		];
		$data['id'] = $reminder->insert($data);
		print json_encode($data);
		return true;
	}

    function manage_custom_payments() {
        $this->load->model('custom_payments');
        $data = array();
        $data['payment_types'] = $this->custom_payments->get_all();
        $this->load->view('config/custom_payments', $data);
    }

    function save_custom_payments() {
        $this->load->model('custom_payments');

        // Get posted data
        $cpt = $this->input->post('custom_payment_type');
        $cpt_array = array();
        foreach($cpt as $type) {
            if ($type != '') {
                $cpt_array[] = array(
                    'course_id' => $this->session->userdata('course_id'),
                    'label' => $type,
                    'custom_payment_type' => $this->custom_payments->clean($type)
                );
            }
        }

        echo json_encode(array('success'=>$this->custom_payments->save($cpt_array)));
    }

    function delete_custom_payment($type) {
        $this->load->model('custom_payments');

        echo json_encode(array('success'=>$this->custom_payments->delete($type)));
    }

	function backup()
	{
		$this->load->dbutil();
		$prefs = array(
			'format'      => 'txt',             // gzip, zip, txt
			'add_drop'    => FALSE,              // Whether to add DROP TABLE statements to backup file
			'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
			'newline'     => "\n"               // Newline character used in backup file
    	);
		$backup =&$this->dbutil->backup($prefs);
		$backup = 'SET FOREIGN_KEY_CHECKS = 0;'."\n".$backup."\n".'SET FOREIGN_KEY_CHECKS = 1;';
		force_download('foreup_database.sql', $backup);
	}
	
	function convert_to_multi_printers(){
		$this->Course->convert_to_multi_printers($this->session->userdata('course_id'));
		redirect(site_url('config'), 'refresh');
	}

    function manage_cc_devices() {
        $this->load->model('Blackline_devices');
        $data = array();
        $data['cc_devices'] = $this->Blackline_devices->get_all();
        $terminals = $this->Terminal->get_all()->result_array();
        $data['terminals'] = array();
        if ($this->config->item('use_terminals')) {
            foreach ($terminals as $terminal) {
                $data['terminals'][$terminal['terminal_id']] = $terminal['label'];
            }
        } else {
            $data['terminals'] = array(0=>'Terminals not active');
        }

        $this->load->view('config/manage_cc_devices', $data);
    }

    function add_cc_device() {
        $this->load->model('Blackline_devices');
        $data = array();
        $data['register_url'] = $this->Blackline_devices->get_register_url();

        $this->load->view('config/add_cc_device', $data);
    }

    function save_blackline_device() {
        $data = $this->input->post();
        $this->load->model('Blackline_devices');
        $response = $this->Blackline_devices->save($data);

        echo json_encode($response);
    }

    function delete_blackline_device() {
        $device_id = $this->input->post('device_id');
        $this->load->model('Blackline_devices');
        $response = $this->Blackline_devices->remove($device_id);

        echo json_encode($response);
    }

    function save_device_terminals() {
        $response = array();
        $data = $this->input->post();
        //var_dump($data);
        $this->load->model('Blackline_devices');
        foreach ($data['blackline_id'] as $index => $id) {
            $device_data = array();
            $device_data['terminal_id'] = $data['terminal_id'][$index];
            $device_data['label'] = $data['device_label'][$index];

            $response = $this->Blackline_devices->save($device_data, $id);
        }

        echo json_encode($response);
    }

    // Save receipt content for green fees and cart fees
    function fee_receipt_settings(){

    	$data = $this->input->post();
    	$this->load->model('Pricing');
    	$this->load->model('v2/Item_receipt_content');

    	$fee_ids = $this->Pricing->get_fee_item_ids();
    	if(empty($fee_ids)){
    		return false;
    	}

    	$green_fee_ids = [];
    	$cart_fee_ids = [];
    	foreach($fee_ids as $fee){

    		$item_number = 'price'.$fee['item_number'];
    		if($item_number == Pricing::GREEN_FEE_18 || $item_number == Pricing::GREEN_FEE_9){
    			$green_fee_ids[] = $fee['item_id'];
    		
    		}else if($item_number == Pricing::CART_FEE_18 || $item_number == Pricing::CART_FEE_9){
    			$cart_fee_ids[] = $fee['item_id'];
    		}
    	}

		$green_fee_receipt_content_id = $this->input->post('green_fee_receipt_content_id');
		$cart_fee_receipt_content_id = $this->input->post('cart_fee_receipt_content_id');

		$this->Item_receipt_content->save_item_receipt_content($green_fee_receipt_content_id, $green_fee_ids);
		$this->Item_receipt_content->save_item_receipt_content($cart_fee_receipt_content_id, $cart_fee_ids);

		echo json_encode([
			'success' => true,
			'message' => 'Receipt settings saved'
		]);
    }

    function manage_refund_reasons() {
        $this->load->model('Refund_reason');
        $data = array();
        $data['reasons'] = $this->Refund_reason->get_all();
        $this->load->view("config/manage_refund_reasons", $data);
    }

    function save_reason(){
        $this->load->model('Refund_reason');
        $label = $this->input->post('label');
        $data = array(
            'label' => $label
        );
        $result = $this->Refund_reason->save($data);

        echo json_encode($result);
    }

    function delete_reason($reason_id) {
        $this->load->model('Refund_reason');
        $result = $this->Refund_reason->remove($reason_id);

        echo json_encode($result);
    }

    function manage_apriva_devices() {
        $this->load->model('v2/Pax_device_model');
        $data = array();
        $data['cc_devices'] = $this->Pax_device_model->get_all();
        $terminals = $this->Terminal->get_all()->result_array();
        $data['terminals'] = array();
        if ($this->config->item('use_terminals')) {
            foreach ($terminals as $terminal) {
                $data['terminals'][$terminal['terminal_id']] = $terminal['label'];
            }
        } else {
            $data['terminals'] = array(0=>'Terminals not active');
        }

        $this->load->view('config/manage_apriva_devices', $data);
    }

    function add_apriva_device() {
        $this->load->model('v2/Pax_device_model');

        $data = array();
        $data['register_url'] = $this->Pax_device_model->get_register_url();

        $this->load->view('config/add_cc_device', $data);
    }

    function save_apriva_device() {
        $this->load->model('v2/Pax_device_model');

        $data = $this->input->post();

        $response = $this->Pax_device_model->save($data);

        echo json_encode($response);
    }

    function delete_apriva_device() {
        $this->load->model('v2/Pax_device_model');

        $device_id = $this->input->post('device_id');

        $response = $this->Pax_device_model->remove($device_id);

        echo json_encode($response);
    }

    function save_apriva_device_terminals() {
        $this->load->model('v2/Pax_device_model');

        $response = array();
        $data = $this->input->post();

        foreach ($data['apriva_id'] as $index => $id) {
            $device_data = array();
            $device_data['terminal_id'] = $data['terminal_id'][$index];
            $device_data['label'] = $data['device_label'][$index];
            $device_data['ip_address'] = $data['device_ip_address'][$index];

            $response = $this->Pax_device_model->save($device_data, $id);
        }

        echo json_encode($response);
    }
}
?>

