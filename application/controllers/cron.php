<?php
//This controller is for the purpose of running cron tasks in the system

class Cron extends CI_Controller//REST_Controller
{
	private $cron_key = 'f0r3upcr0n';
    function __construct()
    {
        parent::__construct('cron');
		$this->load->library('Marketing_recipients_lib');
    	$this->load->library('Twilio');
    	$this->config->load('twilio', TRUE);
		
		$this->load->model('Customer');
		$this->load->model('Teetime');
		$this->load->model('Communication');
		$this->load->model('Sendhub_account');
		$this->load->library('sendhub');
	}
    function index()
    {
        //echo 'adding item';
    }
	
	function test_joins()
	{
		$this->db->from('people');		
        $this->db->join('sendhub_accounts', 'sendhub_accounts.phone_number = people.phone_number', 'left');
		$this->db->join('customers', 'customers.person_id = people.person_id');
		$this->db->where('customers.deleted = 0');
		$rsult = $this->db->get()->result_array();
		
		echo "<pre>";
		print_r($rsult);
		echo "</pre>";
	}
	
	//for testing against sendhub API
	function delete_sendhub_accounts()
	{
		$id = '2668287';
		$this->sendhub->delete_contact($id);
	}

	function tee_time_reminders()
	{
		$this->db->select('teesheet_id, courses.course_id, send_text_reminder, send_email_reminder, courses.name, courses.phone');
		$this->db->from('teesheet');		
        $this->db->join('courses', 'courses.course_id = teesheet.course_id');
		$this->db->where('send_email_reminder = 1 OR send_text_reminder = 1');    
        $ids = $this->db->get()->result_array();				
		
		$teesheet_ids = array();
		$course_list = array();
		
		foreach ($ids as $id)
		{
			$course_list[$id['course_id']]['name'] = $id['name'];
			$course_list[$id['course_id']]['phone'] = $id['phone'];					
			$course_list[$id['course_id']]['teesheets'][] = $id['teesheet_id'];
			$course_list[$id['course_id']]['send_email_reminder'] = $id['send_email_reminder'];
			$course_list[$id['course_id']]['send_text_reminder'] = $id['send_text_reminder'];
		}		

		$start_date = date('YmdHi', strtotime('+1 day',strtotime(date('Y-m-d')))) - 1000000;
		$end_date = date('YmdHi', strtotime('+2 day',strtotime(date('Y-m-d')))) - 1000000;
		$booking_deadline = date('Y-m-d H:i', strtotime('-2 day',strtotime(date('Y-m-d H:i'))));				
		
		foreach ($course_list as $course_id=>$course)
		{
			$this->db->select('TTID, start, date_booked, person_id, person_id_2, person_id_3, person_id_4, person_id_5, player_count, start');
			$this->db->from('teetime');
			$this->db->join('teesheet', 'teesheet.teesheet_id = teetime.teesheet_id');
			$this->db->where('start >', $start_date);
			$this->db->where('start <', $end_date);
			$this->db->where('start <', $end_date);
			$this->db->where('status != "deleted"');
			$this->db->where("TTID NOT LIKE '____________________b'");	//exclude the back 9 tee time
			$this->db->where('date_booked <', $booking_deadline);		//exlude tee times booked in last 48 hours		
			$this->db->where_in('teesheet.teesheet_id', array_values($course['teesheets']));
			
			$teetimes = $this->db->get()->result_array();	
			// echo "<pre>";
			// print_r($teetimes);
			// echo "</pre>";
			$all_customer_ids = array();
			
			foreach ($teetimes as $tee_time) {
				$customer_ids = array(
					$tee_time['person_id'],$tee_time['person_id_2'],$tee_time['person_id_3'],$tee_time['person_id_4'],$tee_time['person_id_5']
				);
				$this->db->select('people.person_id, sendhub_id, people.first_name, people.last_name, email, people.phone_number, customers.opt_out_email, customers.opt_out_text');				
				$this->db->from('people');		
        		$this->db->join('sendhub_accounts', 'sendhub_accounts.phone_number = people.phone_number', 'left');
				$this->db->join('customers', 'customers.person_id = people.person_id');
				$this->db->where('customers.deleted = 0');
				$this->db->where('customers.course_id', $course_id);
				if ($course['send_email_reminder']) $this->db->where("customers.opt_out_email = 'N'");													
				if ($course['send_text_reminder']) $this->db->where("customers.opt_out_text = 'N'");				
				$this->db->where_in('customers.person_id',$customer_ids);		
				$players_info = $this->db->get()->result_array();	
				
				if ($course['send_text_reminder']) $this->send_text_reminder($players_info, $tee_time, $course, $course_id, $all_customer_ids);
				// if ($course['send_email_reminder']) $this->send_email_reminder($players_info, $tee_time, $course, $course_id);					
			}			
		}																										
	}
	
	function send_email_reminder($players_info, $tee_time, $course, $course_id)
	{			
		foreach ($players_info as $person_info)
		{
			$email_data = array(
				'person_id'=>$person_info['person_id'],
				'course_name'=>$course['name'],
				'course_phone'=>$course['phone'],
				'course_id'=>$course_id,
				'first_name'=>$person_info['first_name'],
				'booked_date'=>date('n/j/y', strtotime($teetime_data->start+1000000)),
				'booked_time'=>date('g:ia', strtotime($teetime_data->start+1000000)),
				'booked_holes'=>$teetime_data->holes,
				'booked_players'=>$teetime_data->player_count,
				'TTID'=> $tee_time['TTID']
			);			
			$this->Teetime->send_reminder_email($person_info['email'], 'Tee Time Reservation Reminder', $email_data, 'course email', $course['name']);			
		}
		
	}
	
	function send_text_reminder($players_info, $tee_time, $course, $course_id, $all_customer_ids)
	{		
		foreach ($players_info as $person_info)
		{
			$text_data = array(
				'person_id'=>$person_info['person_id'],
				'sendhub_id'=>$person_info['sendhub_id'],
				'first_name'=>$person_info['first_name'],
				'opt_out_text'=>$person_info['opt_out_text'],
				'person_phone'=>$person_info['phone_number'],				
				'course_name'=>$course['name'],
				'course_phone'=>$course['phone'],
				'course_id'=>$course_id,																				
				'teetime'=> $tee_time
			);	
					
			if ($text_data['opt_out_text'] == 'N' && $text_data['person_phone'] != '') {
				$this->Sendhub_account->send_reminder_text($text_data, $all_customer_ids);
				// $this->Communication->send_reminder_text($text_data, $all_customer_ids);				
			}			
		}	
	}
	
	function auto_bill_credit_cards($cron_key)
	{
		//echo 'getting inside auto_bill_credit_cards';
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			set_time_limit(0);
			$product_names = array('1'=>'Software','2'=>'Website','3'=>'Marketing');
			$this->load->model('Billing');
			$this->load->model('Credit_card');
			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			//echo 'getting into it<br/>';
			$todays_billings = $this->Billing->get_todays_billings();
			$todays_billed = $this->Billing->get_todays_billed();
			echo '<br/>'.$this->db->last_query().'<br/><br/>';
			
			$todays_billings = $todays_billings->result_array();
			
			print_r($todays_billings);
			//echo '<br/>'.$this->db->last_query().'<br/>';

			foreach ($todays_billings as $billing)
			{
				if (in_array($billing['billing_id'], $todays_billed))
				{
					echo "<br/>Duplicate billing {$billing['billing_id']}<br/>";
					return;
				}
				$tax_rates = array();//explode(',', $billing['tax_rates']);
				//$tax_names = explode(',', $billing['tax_names']);
				$contact_emails = array_unique(explode(',', $billing['contact_emails']));
				$products = explode(',', $billing['products']);
				
				$credit_card_charges = $product_list = array();
				$subtotal = 0;
				$product_list = $this->Billing->get_billing_products($billing['course_id'], $billing['credit_card_id']);
				echo '<br/><br/>'.$this->db->last_query();
				foreach ($product_list->result_array() as $product)
				{
					$payment_type = ($product['free'] ? 'Free' : ($product['monthly'] ? 'Monthly Payment' : ($product['annual'] ? 'Annual Payment' : '')));
					$payment_amount = ($product['free'] ? 0 : ($product['monthly'] ? $product['monthly_amount'] : ($product['annual'] ? $product['annual_amount'] : 0)));
					$subtotal += $payment_amount;
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>"ForeUP Services - {$product_names[$product['product']]} $payment_type", 'amount'=>$payment_amount);
					$tax_rates[$product['tax_name']] = array('tax_amount'=>$payment_amount * $product['tax_rate'] / 100, 'tax_rate' => $product['tax_rate']);
				}
				echo '<br/>Parsed product list<br/>';
/*				
				if ($billing['total_annual_amount'] != '0.00')
				{
				}
				if ($billing['total_monthly_amount'] != '0.00')
				{
					$subtotal += $billing['total_monthly_amount'];
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>'foreUP Services - Monthly Payment', 'amount'=>$billing['total_monthly_amount']);
				}
				foreach ($products as $product)
					$product_list[] = array('line_number'=>count($credit_card_charges)+1,'description'=>' - '.$product_names[$product], 'amount'=>'');
			*/	
				/*if ($billing['annual'] && $billing['annual_month'] == date('n') && $billing['annual_day'] == date('j'))
				{
					$subtotal += $billing['annual_amount'];
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>'foreUP '.$products[$billing['product']].' Services - Annual Payment', 'amount'=>$billing['annual_amount']);
				}
				if ($billing['monthly'] && $billing['period_start'] <= date('n') && $billing['period_end'] >= date('n') && $billing['monthly_day'] == date('j'))
				{
					$subtotal += $billing['monthly_amount'];
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>'foreUP '.$products[$billing['product']].' Services - Monthly Payment', 'amount'=>$billing['monthly_amount']);
				}*/
				$taxes = number_format($billing['annual_tax_amount'] + $billing['monthly_tax_amount'], 2);
				$total = number_format($subtotal + $taxes, 2);
				$totals = array('subtotal'=>$subtotal,'taxes'=>$taxes,'total'=>$total);
				//print_r($billing);
				//echo '<br/>';
				$course_info = $this->Course->get_info($billing['course_id']);
				//$invoice = $this->Sale->add_credit_card_payment(array('mercury_id'=>config_item('foreup_mercury_id'),'tran_type'=>'CreditSaleToken','frequency'=>'Recurring'));
				$invoice = $this->Sale->add_credit_card_payment(array('mercury_id'=>config_item('foreup_mercury_id'),'tran_type'=>'CreditSaleToken','frequency'=>'Recurring'));
				$employee = 'Auto Billing';
				echo '<br/>Added Credit Card Payment<br/>';
				$HC->set_frequency('Recurring');
				$HC->set_token($billing['token']);
				$HC->set_cardholder_name($billing['cardholder_name']);
				$HC->set_invoice($invoice);
				$HC->set_merchant_credentials(config_item('foreup_mercury_id'),config_item('foreup_mercury_password'));//Test Credentials
				echo '<br/>Set all credentials<br/>';
				//echo config_item('foreup_mercury_id').' here are the credentials - '.config_item('foreup_mercury_password');
				//echo 'and here === '.$this->config->item('mercury_id').' --- '.$this->mercury_id;
				//return;
				$transaction_results = $HC->token_transaction('Sale',$total, '0.00', $taxes);
				echo '<br/>Ran transaction<br/>';
				$payment_data = array(
					'acq_ref_data'=>(string)$transaction_results->AcqRefData,
					'auth_code'=>(string)$transaction_results->AuthCode,
					'auth_amount'=>(string)$transaction_results->AuthorizeAmount,
					'avs_result'=>(string)$transaction_results->AVSResult,
					'batch_no'=>(string)$transaction_results->BatchNo,
					'card_type'=>(string)$transaction_results->CardType,
					'cvv_result'=>(string)$transaction_results->CVVResult,
					'gratuity_amount'=>(string)$transaction_results->GratuityAmount,
					'masked_account'=>(string)$transaction_results->Account,
					'status_message'=>(string)$transaction_results->Message,
					'amount'=>(string)$transaction_results->PurchaseAmount,
					'ref_no'=>(string)$transaction_results->RefNo,
					'status'=>(string)$transaction_results->Status,
					'token'=>(string)$transaction_results->Token,
					'process_data'=>(string)$transaction_results->ProcessData
				);
				$this->Sale->update_credit_card_payment($invoice, $payment_data);
				
				$credit_card_id = $billing['credit_card_id'];
				$credit_card_data = array(
					'token'=>$payment_data['token'],
					'token_expiration'=>date('Y-m-d', strtotime('+2 years'))
				);
				//echo "<br/>Status {$payment_data['status']}<br/>";
				if ($payment_data['status'] == 'Approved')
				{
					echo "<br/>Approved<br/>";
					$this->Credit_card->save($credit_card_data, $credit_card_id);
					$this->Credit_card->record_charges($credit_card_id, $credit_card_charges, array('total'=>$total), $billing['billing_id']);
				}
				$data = array(
					'receipt_title'=>'Auto Billing Receipt',
					'transaction_time'=>date('Y-m-d'),
					'customer'=>$course_info->name,
					'contact_email'=>$billing['contact_emails'],
					'sale_id'=>$invoice,
					'employee'=>$employee,
					'items'=>$credit_card_charges,
					'product_list'=>$product_list,
					'tax_rates'=>$tax_rates,
					'totals'=>$totals,
					'payment_data'=>$payment_data
				);
				$subject = ($payment_data['status'] == 'Approved'?'':'Declined ');
				$data['status_message'] = ($payment_data['status'] == 'Approved'?'':$payment_data['status_message']);
				$subject .=  $course_info->name.' Auto Bill';
				$recipients = array('billing@foreup.com','jhopkins@foreup.com');
				if ($billing['contact_email'] != '' && $payment_data['status'] == 'Approved')
					foreach ($contact_emails as $contact_email)
						$recipients[] = $contact_email;
				send_sendgrid($recipients, $subject, $this->load->view("billing/receipt_email",$data, true), 'billing@foreup.com', 'billing@foreup.com');
			}
		//echo json_encode(array('success'=>true, 'billing_count' => count($todays_billings)));
		//echo json_encode(array('success'=>$payment_data['status']=='Approved'?true:false, 'message'=>"A charge of {$payment_data['amount']} was {$payment_data['status']} for card {$payment_data['masked_account']}", 'item_id'=>0));
		}
	}
	
	function auto_bill_customers($cron_key)
	{
		//echo 'getting inside auto_bill_credit_cards';
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			echo '<br/>Starting auto_bill_customers<br/>';
			set_time_limit(0);
			ini_set('memory_limit', '100M');
			$this->load->model('Invoice');
			$this->load->model('Customer_billing');
			$this->load->model('Customer_credit_card');
			$todays_billings = $this->Customer_billing->get_todays_billings();
			echo '<br/><br/>Billings '.$this->db->last_query();
//print_r($todays_billings->result_array());
//return;
			foreach ($todays_billings->result_array() as $billing)
			{
				// SAVE INVOICE
				$customer = $this->Customer->get_info($billing['person_id']);
				$customer_id = $customer->person_id;
				$invoice_data = array(
					'course_id'=>$billing['course_id'],
					'credit_card_id'=>$billing['credit_card_id'],
					'billing_id'=>$billing['billing_id'],
					'person_id'=>$billing['person_id'],
					'month_billed'=>date('Y-m-d')
				);
				$this->Invoice->save($invoice_data);	
				$invoice_id = $invoice_data['invoice_id'];
				$invoice_number = $invoice_data['invoice_number'];
				
				// SAVE INVOICE ITEMS
				$item_data = array();
				$calculated_total = 0;
				$items = $this->Customer_billing->get_items($billing['billing_id']);
				
				// Just saving to invoices
				foreach ($items as $index => $item)
				{
					$item_data[] = array(
						'invoice_id'=>$invoice_data['invoice_id'],
						'line_number'=>$index,
						'description'=>$item['description'],
						'quantity'=>$item['quantity'],
						'amount'=>$item['amount'],
						'tax'=>$item['tax'],
						'pay_account_balance'=>0,
						'pay_member_balance'=>0
					);
					$calculated_total += (int)$item['quantity'] * (float)$item['amount'] * (1 + (float)$item['tax']/100);	
				}
				// ADD CUSTOMER ACCOUNT BALANCE
				if ($billing['pay_account_balance'] && $customer->account_balance < 0)
				{
					$index++;
					$item_data[] = array(
						'invoice_id'=>$invoice_data['invoice_id'],
						'line_number'=>$index,
						'description'=>'Customer Credit Balance',
						'quantity'=>1,
						'amount'=>-$customer->account_balance,
						'tax'=>0,
						'pay_account_balance'=>1,
						'pay_member_balance'=>0
					);
					$calculated_total += -(float)$customer->account_balance;	
				}
				// ADD MEMBER ACCOUNT BALANCE
				if ($billing['pay_member_balance'] && $customer->member_account_balance < 0)
				{
					$index++;
					$item_data[] = array(
						'invoice_id'=>$invoice_data['invoice_id'],
						'line_number'=>$index,
						'description'=>'Member Account Balance',
						'quantity'=>1,
						'amount'=>-$customer->member_account_balance,
						'tax'=>0,
						'pay_account_balance'=>0,
						'pay_member_balance'=>1
					);
					$calculated_total += -(float)$customer->member_account_balance;	
				}
				$this->Invoice->save_items($item_data);
				// Charge credit card and update the invoice
				$calculated_total = floor($calculated_total*100)/100;
				
				$charged = $this->Customer_credit_card->charge($billing, $calculated_total);
				$invoice_data = array(
					'total'=>$calculated_total,
					'paid'=>($charged)?$calculated_total:'0.00',
					'credit_card_payment_id'=>$billing['credit_card_payment_id']
				);
				$this->Invoice->save($invoice_data, $invoice_id);	
				
				// SET INVOICE ITEMS FOR SALE
				foreach ($item_data as $key => $invoice_item) {
					$item_data[$key]['payment_amount'] = $invoice_item['amount'];
				}		
				if ($charged)
				{
					// INITIATE A FULL SALE
					$sales_items = array(
						array(
							'invoice_id'=>$invoice_id,
							'invoice_number'=>$invoice_number,
							'line'=>1,
							'name'=>'INV '+$invoice_number,			
							'is_serialized'=>TRUE,			
							'quantity'=>1,
				            'discount'=>0,                         
							'price'=>number_format($calculated_total,2), 
							'paid'=>$paid,
							'max_discount'=>0,
							'is_invoice'=>TRUE,	
							'invoice_items'=>$item_data							
						)
					);
					$sales_payments = array(
						$billing['payment_type']=>
							array(
								'payment_type'=>$billing['payment_type'],
								'payment_amount'=>$calculated_total,
								'invoice_id'=>$invoice_id,
								'customer_id'=>$customer_id
							)
					);
					$sale_id = $this->Sale->save($sales_items, $customer_id,$billing['employee_id'],'Auto Billed',$sales_payments,false,'',$billing['course_id']);
					echo '<br/><br/>sale_id '.$sale_id;
					// SAVE SALES INVOICE
					$sales_invoices_data = array(
						'sale_id'=>$sale_id,										
						'invoice_id'=>$item['invoice_id'],									
						'line' => $item['line'],
						'quantity_purchased'=>1,
						'invoice_cost_price'=>0,
						'discount_percent'=>0,
						'invoice_unit_price'=>$item['price']
					);
					
					// Email copy of invoice			
					if ($billing['email_invoice'])
						$this->Invoice->send_email($invoice_id);
				}				
			}
		}
	}
	
	function send_scheduled_marketing($cron_key)
	{
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			$info = $this->Marketing_campaign->get_all_unsent_campaigns();
		   	$mrl  = $this->marketing_recipients_lib;
			//return;
		    foreach($info as $i)
		    {
		      $course  = $this->Course->get_info($i->course_id);
			  $mrl->reset();
		      $rec = $i->recipients;
		      $rec = unserialize($rec);
		      $mrl->set_groups($rec['groups']);
		      $mrl->set_individuals($rec['individuals']);
		      $type = $i->type;
		      $recpnts = $mrl->get_recipients($type);
		      if(strtolower($type) == 'email')
		      {
		      	$contents = $this->get_contents($i->campaign_id);
				if ($contents != '')
			        $this->Marketing_campaign->send_mails($i->campaign_id, $recpnts, $contents, $course, $i->subject);
			  }
		      else
		      {
		        /**
		         * not yet tested, but already setup.
		         */
		        // $this->Marketing_campaign->send_text($i->campaign_id, $recpnts, $i->content);
		        $this->Marketing_campaign->send_sendhub_text($i->campaign_id, $recpnts, $i->content);
		      }
		    }
		}
	}
		
	// this function will generate the html mark up based on the template selected
  private function get_contents($campaign_id)
  {
    $info = $this->Marketing_campaign->get_info($campaign_id, true);
    //print_r($info);
    $tpl = $info->template;
    // get the course info
    $course  = $this->Course->get_info($info->course_id);

    $address = sprintf('%s, %s %s, %s',
      $course->address, $course->city, $course->state,
      $course->zip
     );

//    $open_time = $course->open_time . ' - '. $course->close_time;
    $name = $course->name;
   	
    $data['logo']   = '';
    $data['title']  = ($info->title);
    $data['name']   = $name;
    $data['address']    = $address;
//    $data['open_time']  = $open_time;
    $data['phone_number'] = $course->phone;
    $data['support_email'] =  $course->email;//$this->config->item('email');
    $data['website'] =  $course->website;//$this->config->item('website');
    $tpl = strtolower(trim($tpl));
    $data['tpl']= base_url()."application/views/email_templates/$tpl";
    $data['logo'] = base_url().urlencode($info->logo_path);
    $data['logo_text'] = '';
    $data['header'] = ''; // not needed yet.
//    $data['header'] = $info->header;
    
    $tmp_content = $info->content;

    $data_opts = array(
      'campaign_id' => $campaign_id,
      'customer_id' => '__customer_id__',
      'course_id'  => $info->course_id
    );

    $ops = http_build_query($data_opts);
    $ops = str_replace('amp;', '', $ops);
    $link = site_url('subscriptions/unsubscribe?') . $ops;
    

    $data['contents'] = $tmp_content;
    $tpl = 
    $contents = $this->load->view("email_templates/{$tpl}/mailer.html", $data, true);

    //$contents .= "<div><a href='{$link}' target='_blank'>Click to unsubscribe</a></div>";

    return $tpl != '' ? trim($contents) : '';
  }

	function send_auto_mailers($cron_key)
	{
		//echo 'getting inside auto_bill_credit_cards';
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			set_time_limit(0);
			$this->load->model('Auto_mailer');
			$this->load->model('Recipient');
			$this->load->model('Auto_mailer_campaign');
			$this->load->model('Marketing_campaign');
			
			$auto_mailers = $this->Auto_mailer->get_all(10000, 0, true)->result_array();
			foreach ($auto_mailers as $auto_mailer)
			{
				$campaigns = $this->Auto_mailer_campaign->get_all($auto_mailer['auto_mailer_id'])->result_array();;
				$course  = $this->Course->get_info($auto_mailer['course_id']);
				foreach($campaigns as $campaign)
				{
					//print_r($campaign);
					$recipients = array();
					$date = date('Y-m-d 00:00:00', strtotime('-'.$campaign['days_from_trigger'].' days'));
					$rcpts = $this->Recipient->get_all_recipients($auto_mailer['auto_mailer_id'], $date);
					$index  = $campaign['type'] == 'email' ? 'email' : 'phone_number';
					//echo $this->db->last_query().'<br/><br/>';
					foreach ($rcpts->result_array() as $recipient)
					{
						//if(empty($recipient[$index]) || ($recipient["opt_out_{$index}"] == 'Y')) continue;
				        // making the email as a key will make sure that no email will be sent twice
				        $recipients[strtolower($recipient[$index])] = array(
				            "{$index}" => $recipient[$index],
				            'customer_id' => $recipient['person_id']
				        );
					}
					
					// get the template ready
					$contents = $this->get_contents($campaign['campaign_id']);
		        	//$this->Marketing_campaign->send_mails($campaign['campaign_id'], $recipients, $contents, $course);
					
					//echo '$auto_mailer '.$auto_mailer['name'].'<br/><br/>';
					//echo '$campaign_id '.$campaign['campaign_id'].'<br/>';
					//print_r($rcpts);
					//print_r($recipients);
					//print_r($contents);
					//echo '<br/><br/>';
					if ($contents != '')
					{
						if($campaign['type'] == 'email')
					    {
					      	$this->Marketing_campaign->send_mails($campaign['campaign_id'], $recipients, $contents, $course, $campaign['subject']);
						}
					    else
					    {
					        $this->Marketing_campaign->send_text($campaign['campaign_id'], $recipients, $contents);
					    }
					}
				}
					
					
			}
			
		}
	}
}

?>