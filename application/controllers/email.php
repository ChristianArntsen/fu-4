<?php

class Email extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        session_write_close();
        $this->load->model('Marketing_campaign');
    }
    function hostedEmail()
    {
        $emailHash = $this->input->get('email');
        $email = $this->Marketing_campaign->getHtmlByHash($emailHash);
        if(isset($email[0]))
            echo $email[0]['rendered_html'];
        else{
            log_message('error', "Attempted to load email with hash: $emailHash");
            show_404('page');
        }
    }
}
?>
