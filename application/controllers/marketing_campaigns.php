<?php
/*************************************************
 * change   author    date          remarks
 * 0.1      MPogay   21-MAR-2012    check specific line changes related to the version for more info
 * 0.2      MPogay   11-APR-2012    updated send_campaigns function to make it more dynamic
 *                                  and added get_text_tpl function for the text_marketing template
 **************************************************/
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Marketing_campaigns extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('marketing_campaigns');
		require 'application/vendor/autoload.php';
	    $this->load->library('Marketing_recipients_lib');
	    $this->load->model('Customer');
        $this->load->model('Promotion');
		$this->load->library('sendhub');
        $this->load->model('Item');
        $this->load->model('Marketing_Emailtemplate');
        $this->load->model('Marketing_Emailtemplate_Defaults');
        $this->load->model('Marketing_Images');
        $this->load->model('Sendgrid');
        $this->load->model('Marketing_Texting');

              
		$this->load->model('Sendhub_account');
		$this->load->helper('email_marketing');
		$this->load->model('course');
	}

	function test_apns()
	{
		 $this->load->library('apn');
		 $this->apn->payloadMethod = 'enhance'; // turn on the this method for debugging
		 $this->apn->connectToPush();

		 // Adding own variables in notification
		 $this->apn->setData(array ('someKey' => true));
		 $device_token = '131bdfac067938f21b280f19be020f4f2e11c7b2f263d5a81a368242fa76b4a1';
		 $device_token = 'ab271d42c0f9565d5aeecc466c2b6a6ead941949d89e45874c390ddfd9074d14';
		 echo 'about to send message';
		 $send_result = $this->apn->sendMessage($device_token,'test notification # 1 (TIME:'. date ('H: i: s'). ')', /* badge */ 2, /* sound */ 'default');
		 if ($send_result)
		 {
		 	//log_message('debug', 'Posted successfully');
			echo 'Posted succcesfully';
		 }
		 else
		 {
		 	log_message('error', $this->apn->error);
			echo $this->apn->error;
		 }

		 $this->apn->disconnectPush();
	}
	function test_sendhub_api()
	{
		$this->load->library('Sendhub');
		//$ac_resp = json_decode($this->sendhub->add_contact('Joel Hopkins', '8014770208'));
		//echo 'Add Contact<br/>';
		//print_r($ac_resp);
		//echo '<br/><br/>';
		$con_id = 1235065;//$ac_resp->id;
		//$ec_resp = $this->sendhub->edit_contact($con_id, 'Jimbo Hotskins', '8014770208');
		//echo 'Edit Contact<br/>';
		//print_r($ec_resp,1);
		//echo '<br/><br/>';
		//$mess_resp = $this->sendhub->send_message('This is my message second for you', $con_id);
		//echo 'Send Message<br/>';
		//print_r($mess_resp);
		//echo '<br/><br/>';
		$mess_id = 2218412;//$mess_resp->message_id;//thread_id 161240
		echo 'mess_id '.$mess_id.'<br/><br/>';
		$rm_resp = $this->sendhub->read_message($mess_id);
		print_r($rm_resp);
	}

	function recipient_search($type='')
	{
        header('Content-Type: application/json; charset=UTF-8');
		$suggestions[]=array('value'=> 0, 'label' => 'Everyone ('.$this->Customer->count_all('email').'/'.$this->Customer->count_all('phone').')', 'is_group'=>1, 'text'=>$this->Customer->count_all('phone'),'email'=>$this->Customer->count_all('email'));

	    //$everyone_count = $this->Customer->count_all(true);
	    $groups = $this->Customer->get_group_info('', $this->input->get('term'));
	    //$suggestions[]=array('value'=> 0, 'label' => $this->db->last_query());
		foreach ($groups as $group)
		{
			$gr = $this->Customer->get_multiple_group_info_for_email(array($group['group_id']), 'email');
	    	$grp = $this->Customer->get_multiple_group_info_for_text(array($group['group_id']), 'phone');

	    	$suggestions[]=array('value'=> $group['group_id'], 'label' => $group['label'].' ('.$gr->num_rows().'/'.$grp->num_rows().')', 'is_group'=>1, 'text'=>$grp->num_rows(), 'email'=>$gr->num_rows());
		}
		
		$suggestions = array_merge($suggestions, $this->Customer->get_customer_search_suggestions($this->input->get('term'), 100, $type));
		echo json_encode($suggestions);
	}
        
	function index()
	{
		redirect('/marketing_campaigns/campaign_builder#/dashboard');
		ini_set('memory_limit', '100M');
		$limits = $this->Billing->get_monthly_limits($this->session->userdata('course_id'));
		$this->load->model('Communication');
		$com_stats = $this->Communication->get_stats($this->session->userdata('course_id'));
		$data['email_credits'] = $limits['email_limit'];
		$data['text_credits'] = $limits['text_limit'];
		$data['used_email_credits'] = $com_stats['emails_mk_this_month'];
		$data['used_text_credits'] = $com_stats['texts_mk_this_month'];
		$marketing_message = '';
		if ($data['text_credits'] == 0 && $data['email_credits'] == 0)
			$marketing_message = 'You have no email or text credits, please contact ForeUP to remedy the issue.';
		else if (($data['text_credits'] != 0 && $data['text_credits'] - $data['used_text_credits'] < 10) ||
			($data['email_credits'] != 0 && $data['email_credits'] - $data['used_email_credits'] < 10))
		$marketing_message = 'You are running low on email or text credits, please contact ForeUP to remedy the issue.';
		$data['marketing_message'] = $marketing_message;
		$config['base_url'] = site_url('marketing_campaigns/index');
		$config['total_rows'] = $this->Marketing_campaign->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 1;
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_marketing_campaigns_manage_table($this->Marketing_campaign->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$data['status'] = $this->Marketing_campaign->get_status_count();

    	$this->load->view('marketing_campaigns/manage',$data);
	}
    /* added for excel expert */
	function excel_export() {
		$data = $this->Marketing_campaign->get_all()->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array("Gift Card Number", "Value");
		$rows[] = $row;
		foreach ($data as $r) {
			$row = array(
				$r->campaign_number,
				$r->value
			);
			$rows[] = $row;
		}

		$content = array_to_csv($rows);
		force_download('marketing_campaigns_export' . '.csv', $content);
		exit;
	}


	function search($offset = 0)
	{
        header('Content-Type: application/json; charset=UTF-8');
		$search=$this->input->post('search');
		$data_rows=get_marketing_campaigns_manage_table_data_rows($this->Marketing_campaign->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
        $config['base_url'] = site_url('marketing_campaings/index');
        $config['total_rows'] = $this->Marketing_campaign->search($search, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
        header('Content-Type: application/json; charset=UTF-8');
		$suggestions = $this->Marketing_campaign->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	function get_row()
	{
		$campaign_id = $this->input->post('row_id');
		$data_row=get_marketing_campaigns_data_row($this->Marketing_campaign->get_info($campaign_id),$this);
		echo $data_row;
	}

	function view($campaign_id=-1)
	{
		ini_set('memory_limit', '1000M');
		$info = $this->Marketing_campaign->get_info($campaign_id);
		$data = array();
		$data['type'] = $this->input->get('type');
		$data['customers'] = array('' => 'No Customer');

	    $data['groups'][0] = 'Everyone';
	    $data['group_count'] = array();

	    $data['group_count'][0]['email'] = $this->Customer->count_all('email');//$everyone_count;
		$data['group_count'][0]['phone'] = $this->Customer->count_all('phone');//$everyone_count;
		// Clear out any temporary replacement images from past emails
		$this->session->set_userdata('marketing_email_images', null);

		//echo $this->db->last_query();
		if($campaign_id != -1)
	    {
	      $mrl = unserialize($info->recipients);
	      $info->groups = $mrl['groups'];

	      $res = $this->Customer->get_multiple_info($mrl['individuals']);

	      $ind = array();

	      foreach($res->result_array() as $in)
	      {
	        $ind[] = array(
	            'id' => $in['person_id'],
	            'name' => sprintf('%s, %s', $in['last_name'], $in['first_name'])
	        );
	      }

	      $info->individuals = json_encode($ind);
	    }
	    else
	    {
	      $info->groups = array();
	      $info->individuals = '[]';
	    }

	    $sd = strtotime($info->send_date);
	    if($info->status == 'saved' && $sd < strtotime(date('Y-m-d h:i a')))
	    {
	      $info->send_date = date('Y-m-d h:i a');
	    }

	    $data['campaign_info'] = $info;
	    // part of the v0.1 changes
	    // added additional data for templates

	    $tpl_selections = array();

	    // list of available templates
	    // 1-6 , edit as necessary, template standard naming is
	    // template_1.php <-- for easy tracking and management
	    $templates = array(
	    	'eco_letter',
	        'outdoorsy',/* 'worn',*/ 'air_mail', 'helvetica', //'holiday',
	        'cool', 'clouds', 'natural', 'misty_meadow', 'classic',
	        /*'blue_wave',*/ 'full_event_green', 'full_event_orange',
	        'green_special_offer', /*'lawn_newsletter',*/
	        'newsletter_autumn_header', 'green_black_single',
	        //'blue_black_single',
	        'special_discount_feature','blank'//, 'top_leaf_border_red'
	    );
		if ($this->permissions->is_super_admin())
		{
			$templates[] = 'foreup_intro';
			$templates[] = 'foreup_second';
			$templates[] = 'foreup_third';
			$templates[] = 'foreup_fourth';
			$templates[] = 'meadow_creek';
		}
		if ($this->session->userdata('course_id') == '7566')
		{
			$templates[] = 'basic';
			$templates[] = 'fancy';
			$templates[] = 'mobile';
			$templates[] = 'postcard';
			$templates[] = 'simple';
			$templates[] = 'transactional';
			$templates[] = 'two_column';
			$templates[] = 'modular';
		}
	    // this is so that there would be an empty first selection
	    $tpl_selections[] = 'Select Template';
	    foreach($templates as $tpl)
	    {
	      $tmp = $tpl;

	      $tpl = str_replace('_', ' ', $tpl);
	      // set the third param to true to defer browser display and returned it as string
	      $tpl_selections[$tmp] = ucwords($tpl);
	    }

	    $data['tpl_selections'] = $tpl_selections;
            
    	$data['course'] = $this->session->userdata('course_name');
		$result = $this->Module->get_allowed_modules($this->session->userdata('person_id'));
        foreach ($result->result() as $module)
        {
            $allowed_modules[$module->module_id] = 1 ;
        }
        $data['my_allowed_modules']= $allowed_modules;
		
        $coupon_data = array();
        if (property_exists($info,'promotion_id') && property_exists($info,'promotion') && $info->promotion_id != -1 && $info->promotion != 0)
        {                   
            $coupon_data = $this->Promotion->get_coupon_data($info->promotion_id);
            //$data = array_merge($data, $coupon_data); 
            $coupon_data['promotion_id'] = $info->promotion_id;
            $coupon_data['item_name'] = $this->Item->get_item_name($coupon_data['item_id']);
            $coupon_data['valid_from'] = $coupon_data['valid_between_from'];
            $coupon_data['valid_to'] = $coupon_data['valid_between_to'];
            $data['coupon'] = $coupon_data;
        }
        
        $this->load->view("marketing_campaigns/form",$data);
	}

	function save($campaign_id=-1)
	{
        header('Content-Type: application/json; charset=UTF-8');
		ini_set('memory_limit', '200M');
		$type = $this->input->post('campaign_type');
		$queued = $this->input->post('ready_to_send');
		$promotion_id = ($this->input->post('promotion_id')?$this->input->post('promotion_id'):'-1');
	    if($this->input->post('is_sent') == 1)
	    {
	    	return print json_encode(array('success'=>true,'message'=>lang('marketing_campaigns_no_changes'),'campaign_id'=>-1));
	    }
            $mrl = $this->marketing_recipients_lib;

	    // save groups
	    $group_ids = $this->input->post('campaign_group');
	    if(!$group_ids) $group_ids = array();
	    $mrl->set_groups($group_ids);
	    // save individuals
	    $individuals = $this->input->post('campaign_individuals');
	    /*if(in_array(0, $group_ids))
	    {
	    	// everyone is checked, now get all groups
	    	$customers = $this->Customer->get_all()->result_array();
	    	$new_individual_ids = array();
	    	foreach($customers as $customer)
	        	$new_individual_ids[] = $customer['person_id'];

	    	$individuals = $new_individual_ids;
	    }*/
	    if(is_array($individuals)) $mrl->set_individuals($individuals);

	    $mrl_arr = array(
	    	'individuals' => $individuals,
	    	'groups' => $group_ids
	    );
        $recipient_count = $mrl->count_recipients($type);

	    $send_date = $this->input->post('campaign_send_date').' '.$this->input->post('campaign_send_hour');
	    $send_now = $queued ? $this->input->post('send_now') : 0;
	    //validate send_date
	    if($send_now)
	    {
	    	if(strtotime($send_date) === FALSE)
	    	{
	    		return print json_encode(array('success'=>false,'message'=>lang('marketing_campaigns_invalid_date'),'campaign_id'=>-1));
	    	}
			//Only need to validate date if we're queueing the campaign
	    	else if ($queued)
	    	{
	      		$st = strtotime($send_date);

	        	$min = date('i', $st);
	        	// a 5 min tolerance
	        	if($min + 5 > date('i')){
	        		$send_date = date('Y-m-d h:i a');
	        	}

	        	if($st < strtotime(date('Y-m-d h:i a')))
	        	{
	        		return print json_encode(array('success'=>false,'message'=>lang('marketing_campaigns_lesser_date'),'campaign_id'=>-1));
	      		}
	    	}
	    }
	    else
	    {
	    	//$send_date = date('Y-m-d h:i a');
	    }
	    // part of the v0.1 changes
	    $title = $this->input->post('campaign_title', FALSE);

	    // handle logo uploads
		//    $path = $this->input->post('campaign_logo_path');
	    $path = ''; // just set to null cause its not needed yet
	    $target_path = '';
	    if(!empty($path))
	    {
		    // additional validations like file size, type
		    // will be implemented here
			//      $path_parts = pathinfo($logo['tmp_name']);
			//      $ext = strtolower($path_parts["extension"]);
	    	// handle image upload
	    	$upload_dir = APPPATH . 'uploads/';

	    	$tmp_title = strtolower(strip_tags($title));
	    	$tmp_title = explode(' ', $tmp_title);
	    	$tmp_title = implode('_', $tmp_title);

	    	$upload_dir .= $tmp_title.'/';
	    	if(!is_dir($upload_dir))
	      	{
	        	// create the directory recursively
	        	// and set its permissions to 777
	        	mkdir($upload_dir, 0777, true);
	      	}

	      	$pr = pathinfo($path);

	      	$target_path = $upload_dir . $pr['basename'];
	      	// save the file
	      	copy($path, $target_path);
	      	// and delete the temporary file
	      	unlink($path);
	      	// the file path should be saved to the database
	    }



    	$title = empty($title) ? '' : $title;
    	$tpl = $this->input->post('campaign_template');
    	$tpl = empty($tpl) ? '' : $tpl;
		//    if($campaign_id == -1)
		//    {
		if($queued && !$this->Marketing_campaign->has_sufficient_credits($type, $recipient_count, $campaign_id) && $type != "text")
    	{
    		$queued = 0;
			$send_now = 0;
      		$success = false;
      		$message = lang('marketing_campaigns_not_sufficient_credits_'.$type);
		}
		else {
			$message = '';
			$success = true;
		}

		$status = $send_now?'sending':($queued?'scheduled':'saved');

		$tz = new DateTimeZone('America/Chicago');
		$date = new DateTime($send_date);
		$date->setTimeZone($tz);
		$send_date_cst =  $date->format('Y-m-d H:i:s');
		$date->setTimeZone($this->config->item('timezone'));
		$content = $type=='text'?trim($this->input->post('text_contents')):trim(urldecode($this->input->post('campaign_content', FALSE)));
		$content_2 = trim(urldecode($this->input->post('campaign_content_2', FALSE)));
		$content_3 = trim(urldecode($this->input->post('campaign_content_3', FALSE)));
		$content_4 = trim(urldecode($this->input->post('campaign_content_4', FALSE)));
		$content_5 = trim(urldecode($this->input->post('campaign_content_5', FALSE)));
		$content_6 = trim(urldecode($this->input->post('campaign_content_6', FALSE)));
		$campaign_data = array(
          	'title' => $title == '' ? ' ' : $title,
          	'subject' => $this->input->post('campaign_subject'),
          	'type' => $type,
          	'template' => $tpl,
          	'send_date' => date('Y-m-d H:i:s', strtotime($send_date)),
          	'send_date_cst' => $send_date_cst,
          	'content' => $content == '' ? ' ' : $content,
          	'content_2' => $content_2 == '' ? ' ' : $content_2,
          	'content_3' => $content_3 == '' ? ' ' : $content_3,
          	'content_4' => $content_4 == '' ? ' ' : $content_4,
          	'content_5' => $content_5 == '' ? ' ' : $content_5,
          	'content_6' => $content_6 == '' ? ' ' : $content_6,
          	'name' => $this->input->post('campaign_name'),
          	'status' => $status,
          	'queued' => $queued,
          	'recipients' => utf8_encode(serialize($mrl_arr)),
          	'recipient_count'=>$recipient_count,
          	'course_id'         =>  $this->session->userdata('course_id'),
          	'campaign_id' => $this->input->post('campaign_id')=='' ? null:$this->input->post('campaign_id'),
                'promotion_id' => $promotion_id
      	);
	    if(empty($target_path)){unset($campaign_data['logo_path']);}

		if( $this->Marketing_campaign->save( $campaign_data, $campaign_id ) )
		{
			// Clear out temporary replacement marketing images
			$this->session->set_userdata('marketing_email_images', null);

      		// trigger the send now check box here if checked
      		$send_flag = 0;
      		if($send_now)
      		{
        		$send_flag = 1;
	      	}

			//New campaign
			if($campaign_id==-1)
			{
		        echo json_encode(array('success'=>$success,'message'=>lang('marketing_campaigns_successful_adding').' '.
						strip_tags($campaign_data['name']).' '.$message,'campaign_id'=>$campaign_data['campaign_id'], 'send_now'=> $send_flag));
						$campaign_id = $campaign_data['campaign_id'];
			}
			else //previous campaign
			{
				echo json_encode(array('success'=>$success,'message'=>lang('marketing_campaigns_successful_updating').' '.
				strip_tags($campaign_data['name']).' '.$message,'campaign_id'=>$campaign_id, 'send_now'=> $send_flag));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('marketing_campaigns_error_adding_updating').' '.
			$campaign_data['name'],'campaign_id'=>-1));
		}

	}

	function delete()
	{
        header('Content-Type: application/json; charset=UTF-8');
		$campaigns_to_delete=$this->input->post('ids');

		if($this->Marketing_campaign->delete_list($campaigns_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('marketing_campaigns_successful_deleted').' '.
			count($campaigns_to_delete).' '.lang('marketing_campaigns_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('marketing_campaigns_cannot_be_deleted')));
		}
	}

	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
    // part of changes for v0.1
    // changed from 550 to 1065,
    // to cater the template view at the right
		return 1065;
	}

  /**
   * @function get_tpl check if the template is existing or not
   * and return the contents as a string rather than rendering
   * it directly to the browser
   * @return String template contents
   */
  function get_tpl()
  {
    $tpl = $this->input->post('tpl');

    if(empty($tpl)) return print '';
    // get the course info
    $course  = $this->Course->get_info($this->session->userdata('course_id'));

    $address = sprintf('%s, %s %s, %s',
      $course->address, $course->city, $course->state,
      $course->zip
     );

//    $ot = date('m/d/y ') . $course->open_time;
//    $ot = date('m/d/y') . ' ' . substr($course->open_time, 0, 2) . ':00';
//    $otd = date('h:i a', strtotime($ot));
//
//    $ct = date('m/d/y ') . $course->close_time;
//    $ct = date('m/d/y') . ' ' . substr($course->close_time, 0, 2) . ':00';
//    $ctd = date('h:i a', strtotime($ct));
//
//    $open_time = $otd . ' - '. $ctd;

    $name = $course->name;

    $data['logo']       = '';
    $data['logo_text']  = '';
    $data['name']    = $name;
    $data['address']    = $address;
//    $data['open_time']  = $open_time;
    $data['phone_number'] = $course->phone;
    $data['header'] = '';
    $data['support_email'] =  $this->config->item('email');
    $data['website'] =  $this->config->item('website');
    $tpl = strtolower(trim($tpl));
    $data['tpl']= "application/views/email_templates/$tpl";
    $data['logo'] = '';
    $data['logo_text'] = '';
    $data['include_ads'] = false;
	$contents = $this->load->view("email_templates/toolbar.html", array(), true);
    $contents .= $this->load->view("email_templates/{$tpl}/mailer.html", $data, true);
    echo $contents;
  }
  /**
   * this utility function will save the images on a temporary directory
   * this is to cater a virtual automatic update on template logo
   */
  public function save_tmp_img()
  {
    $img = $_FILES['image'];
    $img_path = '';

    if(!empty($img))
    {
      // handle image upload
      $upload_dir = APPPATH . 'uploads/__tmp/';
	  $upload_dir = str_replace(array('&nbsp; &nbsp;',' &nbsp;&nbsp;','     ','    ','   ','  ',' ', '&nbsp;'), '_', $upload_dir);

      if(!is_dir($upload_dir))
      {
        // create the directory recursively
        // and set its permissions to 777
        mkdir($upload_dir, 0777, true);
      }
      $target_path = $upload_dir . basename($img['name']);
	  //clean out white space
	  $target_path = str_replace(array('&nbsp; &nbsp;',' &nbsp;&nbsp;','     ','    ','   ','  ',' ', '&nbsp;'), '_', $target_path);
	  // save the file
      move_uploaded_file($img['tmp_name'], $target_path);
      // the file path should be saved to the database
      $img_path = $upload_dir . basename( $img['name']);
	  //clean out white space
	  $img_path = str_replace(array('&nbsp; &nbsp;',' &nbsp;&nbsp;','     ','    ','   ','  ',' ', '&nbsp;'), '_', $img_path);
	}

    echo $img_path;
  }

	public function save_image($image_id){
        header('Content-Type: application/json; charset=UTF-8');
		$this->load->model('Image');
		$replaced_image = $this->input->post('replaced_image_url');

		if(!empty($replaced_image)){
			$image_info = $this->Image->get_file($image_id);

			$replaced_images = $this->session->userdata('marketing_email_images');
			$replaced_images[$replaced_image] = $image_info;
			$this->session->set_userdata('marketing_email_images', $replaced_images);
		}

		$image_info['last_update'] = strtotime($image_info['date_updated']);
		echo json_encode($image_info);
	}

  // this function will generate the html mark up based on the template selected
  private function get_contents($campaign_id)
  {
    $info = $this->Marketing_campaign->get_info($campaign_id);

    $tpl = $info->template;

    // get the course info
    $course  = $this->Course->get_info($info->course_id);

    $address = sprintf('%s, %s %s, %s',
      $course->address, $course->city, $course->state,
      $course->zip
     );

//    $open_time = $course->open_time . ' - '. $course->close_time;
    $name = $course->name;

    $data['logo']   = '';
    $data['title']  = ($info->title);
    $data['name']   = $name;
    $data['address']    = $address;
//    $data['open_time']  = $open_time;
    $data['phone_number'] = $course->phone;
    $data['support_email'] =  $this->config->item('email');
    $data['website'] =  $this->config->item('website');
    $tpl = strtolower(trim($tpl));
    $data['tpl']= base_url()."application/views/email_templates/$tpl";
    $data['logo'] = base_url().urlencode($info->logo_path);
    $data['logo_text'] = '';
    $data['header'] = ''; // not needed yet.
//    $data['header'] = $info->header;

    $data_opts = array(
      'campaign_id' => $campaign_id,
      'customer_id' => '__customer_id__',
      'course_id'  => $info->course_id
    );

    $ops = http_build_query($data_opts);
    $ops = str_replace('amp;', '', $ops);
    $link = site_url('subscriptions/unsubscribe?') . $ops;


    $data['contents'] = $info->content;
    $data['contents_2'] = $info->content_2;
    $data['contents_3'] = $info->content_3;
    $data['contents_4'] = $info->content_4;
    $data['contents_5'] = $info->content_5;
    $data['contents_6'] = $info->content_6;


    $contents = $this->load->view("email_templates/{$tpl}/mailer.html", $data, true);

	$link = base_url()."index.php/subscriptions/unsubscribe/{$info->course_id}";

    $contents .= "<div><u><a href='{$link}' target='_new'>Unsubscribe</a></u></div>";

    return trim($contents);
  }

  function testing_delete_sendhub_account($id)
  {
  	$this->Sendhub_account->delete($id);
  }

  function send_now()
  {
      return;
  	ini_set('memory_limit','512M');
	$id = $this->input->post('campaign_id');

    $mrl  = $this->marketing_recipients_lib;
    $course  = $this->Course->get_info($this->session->userdata('course_id'));

    $i = $this->Marketing_campaign->get_info($id);
	// echo "<pre>";
	// print_r($i);
	// echo "</pre>";
    $rec = $i->recipients;
    $rec = unserialize($rec);
	$mrl->set_groups($rec['groups']);

    $mrl->set_individuals($rec['individuals']);
    $type = $i->type;
    $recpnts = $mrl->get_recipients($type);
	if(strtolower($type) == 'email')
    {
      $contents = $this->get_contents($i->campaign_id);
      $this->Marketing_campaign->send_mails($i->campaign_id, $recpnts, $contents, $course, $i->subject);
    }
    else
    {
      // $this->Marketing_campaign->send_text($i->campaign_id, $recpnts, $i->content);
	  // $this->Marketing_campaign->send_sendhub_text($i->campaign_id, $recpnts, $i->content);
	  $this->Sendhub_account->send_group_text($i->campaign_id, $recpnts, $i->content);
    }


    $response = array(
        'success'=>true
    );
    echo json_encode($response);
  }

  function send_campaigns()
  {
      
    $info = $this->Marketing_campaign->get_all_unsent_campaigns();
    $mrl  = $this->marketing_recipients_lib;
    $course  = $this->Course->get_info($this->session->userdata('course_id'));

    foreach($info as $i)
    {
      $mrl->reset();
      $rec = $i->recipients;
      $rec = unserialize($rec);
      $mrl->set_groups($rec['groups']);
      $mrl->set_individuals($rec['individuals']);
      $type = $i->type;

      $recpnts = $mrl->get_recipients($type);
      if(strtolower($type) == 'email')
      {
        $contents = $this->get_contents($i->campaign_id);
        $this->Marketing_campaign->send_mails($i->campaign_id, $recpnts, $contents, $course, $i->subject);
      }
      else
      {
        /**
         * not yet tested, but already setup.
         */
         $this->Marketing_campaign->send_text($i->campaign_id, $recpnts, $i->content);
		// $this->Marketing_campaign->send_sendhub_text($i->campaign_id, $recpnts, $i->content);
		//$this->Sendhub_account->send_group_text($i->campaign_id, $recpnts, $i->content);
      }
    }
  }

  function get_text_tpl()
  {
    $course  = $this->Course->get_info($this->session->userdata('course_id'));

    $address = sprintf('%s, %s %s, %s, %s',
      $course->address, $course->city, $course->state,
      $course->zip, $course->phone
     );
	$contents = $this->input->post('contents');
    $open_time = $course->open_time . ' - '. $course->close_time;
    $name = $course->name;

    if(strlen($name) > 20)
    {
      $name = substr($name, 0, 17) . '...';
    }

    $data['logo']       = '';
    $data['logo_text']  = '';
    $data['name']    = $name;
    $data['address']    = $address;
    $data['open_time']  = $open_time;
	$data['contents'] = $contents;
    $data['support_email'] =  $this->config->item('email');
    $data['website'] =  $this->config->item('website');
    $data['course_name'] =  '';

    $data['title'] = 'Text template';

    $contents = $this->load->view("marketing_campaigns/text_marketing/text_template", $data, true);
    echo $contents;
  }

  function test_tpl()
  {
    $tpl = 'holiday';
    $data['tpl']= base_url()."application/views/email_templates/$tpl";
    $data['title']  = 'Template Title';
    $data['phone_number'] = '(801) 225-6677';
    $data['support_email'] =  $this->config->item('email');
    $data['website'] =  $this->config->item('website');
    $data['name']   = 'Cascade Golf Center';
    $data['address']    = '1313 E 800 N, Orem UT, 84097';
    $data['contents'] = "We've got a great new range of widgets this week. Don't miss out on these amazing deals. Our unique widgets only way to go. ";
    $this->load->view("email_templates/{$tpl}/mailer.html", $data);
  }
    function generate_stats()
    {
        header('Content-Type: application/json; charset=UTF-8');
        $results = $this->Dash_data->fetch_marketing_data();
        echo json_encode($results);

    }
    function facebook_page_select()
	{
		return $this->load->view('customers/facebook_page_select');
	}

    function clear_facebook_settings()
	{
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_page_id = '';
		$course_info->facebook_page_name = '';
		$course_info->facebook_extended_access_token = '';
		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_page_id', '');
			$this->session->set_userdata('facebook_page_name', '');
			$this->session->set_userdata('facebook_extended_access_token', '');
			$result = true;
		}
		else
		{
			$result = false;
		}
		echo json_encode(array('success'=>$result));
	}

	function update_facebook_page_id()
	{
        header('Content-Type: application/json; charset=UTF-8');
		$page_id = $this->input->post('page_id');
		$page_name = $this->input->post('page_name');
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_page_id = $page_id;
		$course_info->facebook_page_name = $page_name;

		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_page_id', $page_id);
			$this->session->set_userdata('facebook_page_name', $page_name);
			$result = true;
		}
		else
		{
			$result = false;
		}
		echo json_encode(array('success'=>$result));

	}

	function update_facebook_access_token()
	{
        header('Content-Type: application/json; charset=UTF-8');
		$access_token = $this->input->post('extended_access_token');
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_extended_access_token = $access_token;

		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_extended_access_token', $access_token);
			$result = true;
		}
		else
		{
			$result = false;
		}
		echo json_encode(array('success'=>$result,'new token'=>$access_token));

	}
	
	function tryit(){
		header('Content-Type: application/json; charset=UTF-8');
		return print json_encode('cou');
	}

	function campaign_builder()
	{
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 1;
		$course_info = $this->course->get_info($this->session->userdata('course_id'));

		$drafts = $this->Marketing_campaign->get_by_status('draft',100,0,2)->result_array();
		foreach($drafts as &$draft){
			$draft['recipients'] = unserialize($draft['recipients']);
			if(isset($draft['rendered_html']))
				unset($draft['rendered_html']);
		}
		$current = array_merge(
			$this->Marketing_campaign->get_by_status('scheduled',100,0,2)->result_array(),
			$this->Marketing_campaign->get_by_status('sending',100,0,2)->result_array()
		);
		foreach($current as &$draft){
			$draft['recipients'] = unserialize($draft['recipients']);
			if(isset($draft['rendered_html']))
				unset($draft['rendered_html']);
		}
		$finished = $this->Marketing_campaign->get_by_status('sent',100,0,2)->result_array();
		foreach($finished as &$draft){
			$draft['recipients'] = unserialize($draft['recipients']);

			if(isset($draft['rendered_html']))
				unset($draft['rendered_html']);
		}
		$return = array(
			"drafts"=>$drafts,
			"current"=>$current,
			"finished"=>$finished
		);



		$this->load->view('marketing_campaigns/campaign_builder/app',
			array(
				"draftCampaigns"=>$return['drafts'],
				"currentCampaigns"=>$return['current'],
				"finishedCampaigns"=>$return['finished'],
				"courseInfo"=>
					$course_info
			)
		);
	}


    function ajaxSaveTemplate(){
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();
        $input_data = json_decode(trim(file_get_contents('php://input')), true);
        $campaign_id = $input_data['campaign_id'];
        $campaign_name = isset($input_data['campaign_name'])? $input_data['campaign_name']:'Custom';
        $success = false;
        $response = [];
        if(isset($campaign_id)){
            $response = $this->Marketing_Emailtemplate->campaignSaveAs($campaign_id,$campaign_name);
            if($response){
                $success = true;
            }
        }
        echo json_encode(array('success'=>$success ));
    }

    function ajaxGetAllStats()
    {
        header('Content-Type: application/json; charset=UTF-8');
        session_write_close();
        $course = $this->input->get('course');
        if(!$course || $_SESSION['foreup']['user_level']*1!==5)
            $course = $this->session->userdata('course_id');

        $stats = $this->Marketing_campaign->getUsageStats($course);

        return print json_encode(array('success'=>true,'data'=>$stats));
    }

    function ajaxGetAllCampaigns()
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();
        $course = $this->input->get('course');
        if(!$course || $_SESSION['foreup']['user_level']*1!==5)
            $course = $this->session->userdata('course_id');
		$this->load->model("Reminder");
		$drafts = $this->Marketing_campaign->get_by_status('draft',100,0,2,$course)->result_array();
		foreach($drafts as &$draft){
			$draft['recipients'] = unserialize($draft['recipients']);
            $draft['loading'] = [];
			if(isset($draft['rendered_html']))
				unset($draft['rendered_html']);
		}
		$current = array_merge(
			$this->Marketing_campaign->get_by_status('scheduled',100,0,2,$course,true)->result_array(),
			$this->Marketing_campaign->get_by_status('sending',100,0,2,$course,true)->result_array()
		);
		foreach($current as &$draft){
			$draft['recipients'] = unserialize($draft['recipients']);
            $draft['loading'] = [];
			if(isset($draft['rendered_html']))
				unset($draft['rendered_html']);
		}

		$reminders = array_merge(
			$this->Marketing_campaign->get_by_status('reminder',100,0,2,$course)->result_array()
		);
		$reminder_model = new Reminder();
		foreach($reminders as &$reminder){
			$reminder['recipients'] = unserialize($reminder['recipients']);
			$reminder['loading'] = [];
			if(isset($reminder['rendered_html']))
				unset($reminder['rendered_html']);

			$reminder['settings'] = $reminder_model->get_by([
				"campaign_id"=>$reminder['campaign_id']
			]);
		}

		$finished = $this->Marketing_campaign->get_by_status('sent',100,0,2,$course)->result_array();
		foreach($finished as &$draft){
			$draft['recipients'] = unserialize($draft['recipients']);
            $draft['loading'] = [];
			if(isset($draft['rendered_html']))
				unset($draft['rendered_html']);
		}

		$recurring = array_merge($this->Marketing_campaign->get_by_status('recurring',100,0,2,$course)->result_array());

		$this->load->model("repeatable");
		$repeatable_repo = new repeatable();
		$recurring_emails = [];
		foreach($recurring as &$email){
			$email['recipients'] = unserialize($email['recipients']);
			$email['loading'] = [];
			if(isset($email['rendered_html']))
				unset($email['rendered_html']);
			$repeatable = $repeatable_repo->getRowModel([
				"resource_id"=>$email['campaign_id'],
				"type"=>"marketing_campaign"
			]);
			if(!empty($repeatable)){
				$next = $repeatable->getNextCalculatedOccurrence();
				if(isset($next)){
					$email['next_occurence'] = $repeatable->getNextCalculatedOccurrence()->format("Y-m-d H:i:s");
					$email['next_occurence_timestamp'] = strtotime($repeatable->getNextCalculatedOccurrence()->format("Y-m-d H:i:s"));
				}
				if(isset($email['next_occurence'])){
					$recurring_emails[] = $email;
				}

			}

		}

		$return = array(
			"drafts"=>$drafts,
			"current"=>$current,
			"finished"=>$finished,
			"reminders"=>$reminders,
			"recurring"=>$recurring_emails
		);



        $stats = $this->Marketing_campaign->getUsageStats($course);
		$reminders = $reminder_model->get_many_by([
			"course_id"=>$course
		]);

		return print json_encode(array('success'=>true,'data'=>$return,'stats'=>$stats,"reminders"=>$reminders));
	}

    function ajaxDeleteTemplate()
    {
		header('Content-Type: application/json; charset=UTF-8');
        $campaign_id = $this->input->get('id');
        $info = $this->Marketing_Emailtemplate->delete($campaign_id);

        echo json_encode(array('success'=>true,'data'=>$info));
    }
	function ajaxSaveCampaign()
	{
		header('Content-Type: application/json; charset=UTF-8');
		$input_data = json_decode(trim(file_get_contents('php://input')), true);

		if(isset($input_data['data']['finishcampaign']) &&
			$input_data['data']['finishcampaign']){
			try{
				unset($input_data['data']['finishcampaign']);
                $input_data['campaign_id'] = $input_data['data']['campaign_id'];

				$input_data = $this->checkCreditsAndDates($input_data);

			} catch(Exception  $e){
				return print json_encode(array('success'=>false,'message'=>$e->getMessage(),'campaign_id'=>-1));
			}
		}

		if(isset($input_data['data']['queued']) && $input_data['data']['queued']){

			$input_data['data']['rendered_html'] = $this->getPreview($input_data['data']['campaign_id']);
		}

		$input_data['data']['course_id'] = $this->session->userdata('course_id');
		$input_data['data']['type'] = isset($input_data['data']['type']) && $input_data['data']['type'] ? $input_data['data']['type'] :  "email";
		$input_data['data']['version'] = 2;
		$input_data['data']['name'] = isset($input_data['data']['title']) ? $input_data['data']['title']:'' ;
		$input_data['data']['recipients'] = isset($input_data['data']['recipients']) ? utf8_encode(serialize($input_data['data']['recipients'])):'';
		if(!isset($input_data['data']['status']))
			$input_data['data']['status'] = "draft";
		if(isset($input_data['data']['campaign_id'])){
			$campaign_id = $input_data['data']['campaign_id'];
			unset($input_data['data']['campaign_id']);
		} else {
			$campaign_id = -1;
			$input_data['data']['remote_hash'] = uniqid();
		}
		if(isset($input_data['data']['json_content']) && !is_string($input_data['data']['json_content']) )
			$input_data['data']['json_content'] = json_encode($input_data['data']['json_content']);
		unset($input_data['data']['send_now']);
		if(isset($input_data['data']['timebefore']) && isset($input_data['data']['timebefore_unit'])){
			$this->Marketing_campaign->save_reminder($input_data['data']['timebefore'],$input_data['data']['timebefore_unit'],$campaign_id,$input_data['data']['type']);
			unset($input_data['data']['timebefore']);
			unset($input_data['data']['timebefore_unit']);
		}
		if(isset($input_data['data']['report_id'])){
			$report_id = $input_data['data']['report_id'];
			unset($input_data['data']['report_id']);
		}
		if(isset($input_data['data']['rrule'])){
			//Save
			$repeatableOccurence = new models\Repeatable\Row();
			$repeatableOccurence->createFromString($input_data['data']['rrule']);
			$marketing_campaign = new Marketing_campaign();
			$campaign = $marketing_campaign->getRowModelById($campaign_id);
			$campaign->setRepeatableOccurence($repeatableOccurence);
			$campaign->persistOccurences();
			unset($input_data['data']['rrule']);
		}
		$results = $this->Marketing_campaign->save($input_data['data'],$campaign_id);
		if($results && isset($report_id)){
			$this->Marketing_campaign->add_report_to_campaign($results['campaign_id'],$report_id);
		}

		echo json_encode(array('success'=>true,'data'=>$results ));
	}

	function ajaxTextSubscriptionStats()
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();

		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		$groups = [];
		$individuals = [];
		if(isset($input_data['groups']))
			$groups = $input_data['groups'];
		if(isset($input_data['individuals']))
			$individuals = $input_data['individuals'];

		$subscriptionCounts = [];
		$stats = $this->Marketing_Texting->getSubscriptionStats($individuals,$groups, $this->session->userdata('course_id'));



		echo json_encode(array('success'=>true,'data'=>$stats));
	}

	function ajaxEnableTextMarketing()
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();
		$this->Marketing_Texting->enableTexting($this->session->userdata('course_id'));

		echo json_encode(array('success'=>true));
	}



	function ajaxAddReport($campaign_id,$report_id)
	{
		header('Content-Type: application/json; charset=UTF-8');
		$this->Marketing_campaign->add_report_to_campaign($campaign_id,$report_id);

		echo json_encode(array('success'=>true));
	}
	function ajaxRemoveReport($campaign_id,$report_id)
	{
		header('Content-Type: application/json; charset=UTF-8');
		$this->Marketing_campaign->from_report_from_campaign($campaign_id,$report_id);

		echo json_encode(array('success'=>true));
	}


	public function ajaxGetRecipientList($marketing_id)
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();

		$campaign = $this->Marketing_campaign->get_info($marketing_id);


		//Load Campaign
		if($this->Marketing_campaign->get_reports_for_campaign($campaign->campaign_id)){
			$recipientRetriever = new \fu\marketing\recipientRetriever\report();
		} else {
			$recipientRetriever = new \fu\marketing\recipientRetriever\unsent();
		}
		$recipientRetriever->setPossibleRecipients(unserialize($campaign->recipients));
		$recipientRetriever->setCampaign($campaign);
		$recipients = $recipientRetriever->getRecipients();
		$recipientsUnique = [];
		$uniqueEmails = [];
		foreach($recipients as $recipient)
		{
			if(!$recipient['email'] || isset($uniqueEmails[$recipient['email']])){
				continue;
			}
			$uniqueEmails[$recipient['email']] = 1;
			$recipientsUnique[] = $recipient;
		}
		print json_encode([
			"data"=>$recipientsUnique
		]);
	}



    function ajaxGetCampaignStats()
    {
        header('Content-Type: application/json; charset=UTF-8');
        session_write_close();
        $course = $this->input->get('course');
        if(!$course || $_SESSION['foreup']['user_level']*1!==5)
            $course = $this->session->userdata('course_id');

        $campaign_id = $this->input->get('campaign_id');
        //$stats = $this->Marketing_campaign->getUsageStats($course);

        //$grouping = $this->input->get('grouping',"hour,day");
        //$grouping = explode(",",$grouping);
        $stats = $this->Sendgrid->getMarketingCampaignStatistics($campaign_id);
        //$facets = $this->Sendgrid->getMarketingEventFacets($campaign_id,$grouping);
//            $mySummaries = $this->Sendgrid->getCompleteMarketingSummaries($info->course_id);
        $mySummaries = $this->Sendgrid->getCompleteMarketingSummaries($course);
        //$info->stats = $stats;
        //$info->facets = $facets;
        //$info->summary = $mySummaries;
        $usage = $this->Marketing_campaign->getUsageStats($course);

        return print json_encode(array('success'=>true,'data'=>array('campaign'=>$stats,'course'=>$mySummaries,'usage'=>$usage)));
    }

	function ajaxGetCampaign()
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();
		$mrl  = $this->marketing_recipients_lib;


		$campaign_id = $this->input->get('campaign_id');
		$info = $this->Marketing_campaign->get_info($campaign_id);
		if(!isset($info->remote_hash)){
			$info->remote_hash = uniqid();
			$infoArray = (array)$info;
			$this->Marketing_campaign->save($infoArray,$campaign_id);
		}
		if(!empty($info->recipients)){
			$rec = $info->recipients;
			$info->recipients =  unserialize($rec);
			$people = $this->Marketing_campaign->convertIdsToSearchResults($info->recipients['individuals']);
			$people = array_merge($people,$this->Marketing_campaign->convertIdsToSearchResults($info->recipients['groups'],"groups"));
			$info->recipients_pretty = $people;
		}
		if($info->json_content){
			$info->json_content = json_decode($info->json_content);
		}



		$template = $this->Marketing_Emailtemplate->get($info->marketing_template_id);
		$template = array(
			"style"=>$template['style'],
			"thumbnail"=>$template['thumbnail'],
			"name"=>$template['name'],
		);
		$info->template_details = $template;


        if($this->input->get('stats')){
            $grouping = $this->input->get('grouping',"hour,day");
            $grouping = explode(",",$grouping);
            $stats = $this->Sendgrid->getMarketingCampaignStatistics($campaign_id);
            $facets = $this->Sendgrid->getMarketingEventFacets($campaign_id,$grouping);
//            $mySummaries = $this->Sendgrid->getCompleteMarketingSummaries($info->course_id);
            $mySummaries = $this->Sendgrid->getCompleteMarketingSummaries($info->course_id);
            $info->stats = $stats;
            $info->facets = $facets;
            $info->summary = $mySummaries;
        }


		//Get all attached reports as well as well as the number of recipients included in it.
		$report = $this->Marketing_campaign->get_reports_for_campaign($campaign_id);
		if(!empty($report)){
			$info->recipient_reports = [[
				"report_id"=>$report->report_id,
				"report_title"=>$report->title
			]];
		}

		echo json_encode(array('success'=>true,'data'=>$info,'cid'=>$campaign_id));
	}

	function ajaxGetCampaignAnalytics($campaign_id,$event)
	{
		header('Content-Type: application/json; charset=UTF-8');
		$stats = $this->Sendgrid->getHistoryByCampaign($campaign_id,$event);

		echo json_encode(array('success'=>true,'data'=>$stats));
	}

	function ajaxDeleteCampaign()
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();
		$campaign_id = $this->input->get('campaign_id');
		$info = $this->Marketing_campaign->delete($campaign_id);

		$this->load->model("Reminder");
		$reminder_model = new Reminder();
		$reminder_model->delete_by([
			"campaign_id"=>$campaign_id,
			"course_id"=>$this->session->userdata('course_id')
		]);

		$this->load->model("repeatable");
		$repeatable_model = new repeatable();
		$repeatable_model->delete_by([
			"resource_id"=>$campaign_id,
			"type"=>"marketing_campaign"
		]);
		echo json_encode(array('success'=>true,'data'=>$info));
	}

	function ajaxGetTemplates()
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();
		$templates = $this->Marketing_Emailtemplate->getAllTemplates();
		echo json_encode(array('success'=>true,'data'=>$templates));
	}


	/*
	 * Save image to directory
	 * Save image to table for future reference, save url and name
	 *
	 */
	function ajaxImageUpload()
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();
		$CI =& get_instance();
		$ds          = DIRECTORY_SEPARATOR;
		$storeFolder = $CI->config->item('campaigns')['upload_directory']['abs'];

		if (!empty($_FILES)) {
			$tempFile = $_FILES['file']['tmp_name'];
			$targetPath = $storeFolder . $ds;
			$fileName = $_FILES['file']['name'];
			$ext = pathinfo($fileName, PATHINFO_EXTENSION);
			if(!in_array(strtolower($ext),["jpg","jpeg","png","gif","bmp"])){
				throw new exception("Invalid file type, requires: ".json_encode($ext));
			}
			$fileName = sha1($fileName . time());
			$fileName = $fileName.".".$ext;

			$targetFile =  $targetPath.$fileName;
			move_uploaded_file($tempFile,$targetFile);
			$s3 = new fu\aws\s3();
			$s3->uploadToMarketingBucket($targetFile,"images/".$fileName);
			$this->Marketing_Images->save($fileName);

			$relativePath = $CI->config->item('campaigns')['upload_directory']['rel'];
			echo json_encode(array('success'=>true,'url'=>$relativePath.$fileName));
		}
	}


	function ajaxImageList()
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();
		$myImages = array();
		$defaults = array();
		$CI =& get_instance();
		$relativePath = $CI->config->item('campaigns')['upload_directory']['rel'];

		foreach($this->Marketing_Images->getMyHistory() as $image){
			$myImages[] = array(
				'alt'=>"",
				'url'=>$relativePath.$image['path'],
			);
		}
		foreach($this->Marketing_Images->getSystemDefaults() as $image){
			$defaults[] = array(
				'alt'=>"",
				'url'=>$relativePath.$image['path'],
			);
		}

		echo json_encode(array('success'=>true,'data'=>array(
			"myHistory" => $myImages,
			"defaults" => $defaults,
		)));
	}

	function ajaxImageDelete()
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();
		$image_id = $this->input->get('image_id');
		$this->Marketing_Images->delete($image_id);
		echo json_encode(array('success'=>true));
	}


	function getPreview($campaign_id)
	{
		session_write_close();
		$template_info = $this->Marketing_Campaign->getTemplateInfo($campaign_id);

		$CI =& get_instance();
		$stylesheetPath = $CI->config->item('campaigns')['stylesheets']['abs'];

		if($template_info['style']){
			$style = $template_info['style'];
		} else {
			$style = "classy.css";
		}
		$cssFile = $stylesheetPath.$style;
		$preview = file_get_contents($cssFile);

		$rendered = $this->Marketing_Emailtemplate->render($campaign_id,$cssFile);
		$pos = strpos($rendered,"<head>");
		$newstr = substr_replace($rendered, "<style>$preview</style>", $pos+strlen("<head>"), 0);
		return $newstr;
	}
	function preview()
	{
		session_write_close();
		$campaign_id = $this->input->get('campaign_id');

		$preview = $this->getPreview($campaign_id);
		$campaignData = array(
			"rendered_html"=>$preview
		);
		$this->Marketing_campaign->save($campaignData,$campaign_id);
		echo $preview;
		return "";
	}

	function template()
	{
		header('Content-Type: application/json; charset=UTF-8');
		session_write_close();

		$template_id = $this->input->get('template_id');
		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		if($input_data){
			$this->Marketing_Emailtemplate->save($input_data['template'],$template_id);
		}
		$template = $this->Marketing_Emailtemplate->get($template_id);
		if(!$template){
			echo json_encode(array('success'=>false,'data'=>$template));
			return;
		}
		echo json_encode(array('success'=>true,'data'=>$template));

	}

	function saveEmailTemplate()
	{

	}

	/**
	 * @param $input_data
	 */
	private function checkCreditsAndDates($input_data)
	{
		session_write_close();
		$campaign_id = $input_data['campaign_id'];
		$queued = $input_data['data']['queued'];

		$send_date = $input_data['data']['send_date'] . ' ' . $input_data['data']['send_hour'];
		unset($input_data['data']['send_hour']);
		$send_now = $queued ? $input_data['data']['send_now'] : 0;
		$i = $this->Marketing_campaign->get_info($input_data['data']['campaign_id']);

		$type = $i->type;
		$mrl = $this->marketing_recipients_lib;
		$group_ids = $input_data['data']['recipients']['groups'];
		$individuals = $input_data['data']['recipients']['individuals'];
		if (!$group_ids) $group_ids = array();
		if (is_array($individuals)) $mrl->set_individuals($individuals);
		$mrl->set_groups($group_ids);
		$recipient_count = $mrl->count_recipients($type);
		if ($send_now) {
			if (strtotime($send_date) === FALSE) {
				throw new exception(lang('marketing_campaigns_invalid_date'));
			} //Only need to validate date if we're queueing the campaign
			else if ($queued) {
				$st = strtotime($send_date);
				$min = date('i', $st);

				if ($min + 5 > date('i')) {
					$send_date = date('Y-m-d h:i a');
				}
				if ($st < strtotime(date('Y-m-d h:i a'))) {
					throw new exception(lang('marketing_campaigns_lesser_date'));
				}
			}
		}
		if ($queued && !$this->Marketing_campaign->has_sufficient_credits($type, $recipient_count, $campaign_id) && $type != "text") {
			$queued = 0;
			$send_now = 0;
			$success = false;
			throw new exception(lang('marketing_campaigns_not_sufficient_credits_' . $type));
		} else {
			$queued = 1;
			$message = '';
			$success = true;
		}



		if(!isset($input_data['data']['send_date'])){
			$dayToSend = \Carbon\Carbon::now();
		} else {
			$dayToSend = new \Carbon\Carbon($input_data['data']['send_date'],"UTC");
		}


		$dayToSend->timezone('America/Chicago');
		$send_date_cst = $dayToSend->toDateTimeString();
		$dayToSend->timezone($this->config->item('timezone'));
		$send_date =  $dayToSend->toDateTimeString();

        $status = $send_now?'sending':($queued?'scheduled':'saved');
		$input_data['data']['send_date_cst'] = $send_date_cst;
		$input_data['data']['send_date'] = $send_date;
		$input_data['data']['status'] = $status;
		$input_data['data']['queued'] = $queued;
		return $input_data;
	}


	public function ajaxTest()
	{
		$this->load->model("Marketing_Campaign");
		$marketing_campaign = new Marketing_campaign();
		$repeatableOccurence = new models\Repeatable\Row();
		$campaign = $marketing_campaign->getRowModelById(481);
		$campaign->setRepeatableOccurence($repeatableOccurence);
		$this->saveCampaignObj($campaign);


	}
	public function saveCampaignObj(models\MarketingCampaigns\Row $row)
	{
		$this->db->where('campaign_id', $row->campaign_id);
		$this->db->update('marketing_campaigns', $row);
		$row->persistOccurences();

	}
}
?>
