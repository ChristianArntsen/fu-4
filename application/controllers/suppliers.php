<?php
require_once ("person_controller.php");
class Suppliers extends Person_controller
{
	function __construct()
	{
		parent::__construct('suppliers');
	}
	
	function index()
	{
		$config['base_url'] = site_url('suppliers/index');
		$config['total_rows'] = $this->Supplier->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);
		
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_supplier_manage_table($this->Supplier->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('suppliers/manage',$data);
	}
	
	function excel()
	{
		$data = file_get_contents("import_suppliers.csv");
		$name = 'import_suppliers.csv';
		force_download($name, $data);
	}
	
	function excel_import()
	{
		$this->load->view("suppliers/excel_import", null);
	}
    
	/* added for excel expert */
	function excel_export() {
		$data = $this->Supplier->get_all()->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array("Supplier ID", "Company Name", "First Name", "Last Name", "E-Mail", "Phone Number", "Address 1", "Address 2", "City", "State", "Zip", "Country", "Comments", "Account Number");
		$rows[] = $row;
		foreach ($data as $r) {
			$row = array(
				$r->person_id,
				$r->company_name,
				$r->first_name,
				$r->last_name,
				$r->email,
				$r->phone_number,
				$r->address_1,
				$r->address_2,
				$r->city,
				$r->state,
				$r->zip,
				$r->country,
				$r->comments,
				$r->account_number
			);
			$rows[] = $row;
		}
		
		$content = array_to_csv($rows);

		force_download('suppliers_export' . '.csv', $content);
		exit;
	}
	function do_excel_import()
	{
		$this->db->trans_start();
        
        $msg = 'do_excel_import';
		$failCodes = array();
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = lang('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg) );
			return;
		}
		else
		{
			$path_info = pathinfo($_FILES['file_path']['name']);
			if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE && strtolower($path_info['extension']) == 'csv')
			{
        		//Skip first row
				fgetcsv($handle);
				while (($data = fgetcsv($handle)) !== FALSE) 
				{
					$price_category = (isset($data[17]) && $data[17] != '')?$this->Customer->get_price_class_id($data[17]):'';
					$groups = explode(',', isset($data[18])?$data[18]:'');
					$passes = array();
					$person_data = array(
					'first_name'=>isset($data[1])?$data[1]:'',
					'last_name'=>isset($data[2])?$data[2]:'',
					'email'=>isset($data[3])?$data[3]:'',
					'phone_number'=>isset($data[4])?$data[4]:'',
					'cell_phone_number'=>isset($data[5])?$data[5]:'',
					'address_1'=>isset($data[8])?$data[8]:'',
					'city'=>isset($data[9])?$data[9]:'',
					'state'=>isset($data[10])?$data[10]:'',
					'zip'=>isset($data[11])?$data[11]:'',
					'country'=>isset($data[12])?$data[12]:'',
					'comments'=>isset($data[13])?$data[13]:''
					);
					
					$supplier_data=array(
					'course_id'=>$this->session->userdata('course_id'),
					'company_name'=>isset($data[0])?$data[0]:'',
					'fax_number'=>isset($data[6])?$data[6]:'',
					'account_number'=>isset($data[7]) && trim($data[7])!='' ?$data[7]:null
					);
					if (strpos($supplier_data['account_number'], 'E+'))
				    {
				   		echo json_encode(array('success'=>false, 'message'=>lang('suppliers_format_account_numbers')));
						return;
				    }
				    else if(!$this->Supplier->save($person_data,$supplier_data, false))
					{
						$failCodes[] = $data[0].' '.$data[1];
					}
				}
            }
			else 
			{
				echo json_encode( array('success'=>false,'message'=>lang('common_upload_file_not_supported_format')));
				return;
			}
            $this->db->trans_complete();
		}

		$success = true;
		if(count($failCodes) > 0)
		{
			$msg = lang('customers_most_imported_some_failed')." (" .count($failCodes) ."): ".implode(", ", $failCodes);
			$success = false;
		}
		else
		{
			$msg = lang('customers_import_successfull');
		}

		echo json_encode( array('success'=>$success,'message'=>$msg) );
	}
	/*
	Returns supplier table data rows. This will be called with AJAX.
	*/
	function search($offset = 0)
	{
		$search=$this->input->post('search');
		$data_rows=get_supplier_manage_table_data_rows($this->Supplier->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
        $config['base_url'] = site_url('suppliers/index');
        $config['total_rows'] = $this->Supplier->search($search, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);
	}
	
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Supplier->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	/*
	Loads the supplier edit form
	*/
	function view($supplier_id=-1)
	{
		$data['person_info']=$this->Supplier->get_info($supplier_id);
		$data['controller_name'] = strtolower(get_class());
		$data['field_settings'] = false;
		$this->load->view("suppliers/form",$data);
	}
	
	/*
	Inserts/updates a supplier
	*/
	function save($supplier_id=-1)
	{
		$person_data = array(
		'first_name'=>$this->input->post('first_name'),
		'last_name'=>$this->input->post('last_name'),
		'email'=>$this->input->post('email'),
		'phone_number'=>$this->input->post('phone_number'),
		'cell_phone_number'=>$this->input->post('cell_phone_number'),
		'address_1'=>$this->input->post('address_1'),
		'address_2'=>$this->input->post('address_2'),
		'city'=>$this->input->post('city'),
		'state'=>$this->input->post('state'),
		'zip'=>$this->input->post('zip'),
		'country'=>$this->input->post('country'),
		'comments'=>$this->input->post('comments')
		);
		$supplier_data=array(
		'company_name'=>$this->input->post('company_name'),
		'fax_number'=>$this->input->post('fax_number'),
		'account_number'=>$this->input->post('account_number')=='' ? null:$this->input->post('account_number'),
		'course_id'=>$this->session->userdata('course_id')
		);
		if($this->Supplier->save($person_data,$supplier_data,$supplier_id))
		{
			if ($this->config->item('mailchimp_api_key'))
			{
				$this->Person->update_mailchimp_subscriptions($this->input->post('email'), $this->input->post('first_name'), $this->input->post('last_name'), $this->input->post('mailing_lists'));
			}
			
			//New supplier
			if($supplier_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('suppliers_successful_adding').' '.
				$supplier_data['company_name'],'person_id'=>$supplier_data['person_id']));
			}
			else //previous supplier
			{
				echo json_encode(array('success'=>true,'message'=>lang('suppliers_successful_updating').' '.
				$supplier_data['company_name'],'person_id'=>$supplier_id));
			}
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>lang('suppliers_error_adding_updating').' '.
			$supplier_data['company_name'],'person_id'=>-1));
		}
	}
	
	/*
	This deletes suppliers from the suppliers table
	*/
	function delete()
	{
		$suppliers_to_delete=$this->input->post('ids');
		
		if($this->Supplier->delete_list($suppliers_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('suppliers_successful_deleted').' '.
			count($suppliers_to_delete).' '.lang('suppliers_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('suppliers_cannot_be_deleted')));
		}
	}
	
	/*
	Gets one row for a supplier manage table. This is called using AJAX to update one row.
	*/
	function get_row()
	{
		$person_id = $this->input->post('row_id');
		$data_row=get_supplier_data_row($this->Supplier->get_info($person_id),$this);
		echo $data_row;
	}
	
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{			
		return 550;
	}
}
?>