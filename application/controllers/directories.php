<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directories extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    // TO DO: to verify if this module should be secured or not
  }

  public function index()
  {
    $data['allowed_modules']=$this->Module->get_allowed_modules(0);

    $ui = new stdClass();
    $ui->first_name = 'John';
    $ui->last_name  = 'Doe';
    $data['user_info'] = $ui;
    $am = array('reservations', 'directories', 'calendar', 'account',
        'newsletter', 'settings', 'information');
    $data['modules'] = $am;
    $data['controller_name'] = 'Directory';
    $people = $this->Employee->get_all();
    $accounts = array();

    foreach($people->result() as $person)
    {
      $course_info = $this->Course->get_info($person->course_id);
      $accounts[] = array(
          'last_name' => $person->last_name,
          'first_name' => $person->first_name,
          'job_title' => $person->position,
          'golf_course' => $course_info->name,
          'email' => mailto($person->email,$person->email, array('class' => 'underline')),
          'phone_number' => $person->phone_number,
          'activated' => $person->activated
      );
    }

    $data['accounts'] = $accounts;
    
    $data['contents'] = $this->load->view('user/directories/manage', $data, true);
    $this->load->view("user/home", $data);
  }
}