<?php
require_once ("secure_area.php");
class Food_and_beverage extends Secure_area {
	
	function __construct(){
		parent::__construct('food_and_beverage');
		$this->load->library('sale_lib');
		$this->load->model('schedule');
		$this->load->model('teesheet');
		$this->load->model('green_fee');
		$this->load->model('fee');
		$this->load->model('Customer_loyalty');
		$this->load->model('table_receipt');
		$this->load->model('table_ticket');
		$this->load->model('Employee');
		$this->load->model('Customer');
	}

	function index(){
		
		$this->load->model('table_layout');
		$this->load->model('Sale');
		$employee_info = $this->Employee->get_logged_in_employee_info();
		$employee_id = $employee_info->person_id;
		$user_level = $employee_info->user_level;
		$table_number = $this->session->userdata('table_number');

		// Initialize data to send to view
		$data['employee_id'] = $employee_id;
		$data['employee_info'] = $employee_info;
		$data['employee_level'] = $user_level;
		$data['table_number'] = false;
		$data['name'] = '';
		$data['suspended_sale_id'] = false;
		$data['items'] = array();
		$data['food_items'] = array();
		$data['cart'] = array();
		$data['receipts'] = array();
		$data['customers'] = array();

		// If session contains a table, load the data for that table
		if(!empty($table_number)){
			$suspended_sale_id = $this->Table->get_id_by_table_number($table_number);

			if(!empty($suspended_sale_id)){
				$table_info = $this->Table->get_info($suspended_sale_id);
				$table_info = $table_info->row_array();
				$data['sale_time'] = $table_info['sale_time'];
				$data['name'] = $table_info['name'];
				$data['orders_made'] = $table_info['orders_made'];

				$data['table_number'] = $table_number;
				$data['suspended_sale_id'] = $suspended_sale_id;
				$data['cart'] = $this->Table->get_items($suspended_sale_id);
				$data['customers'] = $this->Table->get_customers($suspended_sale_id);
				$data['receipts'] = $this->table_receipt->get($suspended_sale_id);

			}else{
				$this->session->unset_userdata('table_number');
			}
		}
		$register_log = $this->Sale->getUnfinishedRegisterLog();
		$data['register_log'] = $register_log->row_array();

		// Actual Backbone JS item data that will be placed in page as JSON
		$data['food_items'] = $this->Item->get_all_food();
		$data['sides'] = $this->Item->get_all_food(1);
		
		$this->apply_sort_numbers($data['food_items']);
		
		// Loop through items and place them in sub arrays as categories
		$items = array();
		foreach($data['food_items'] as $item){

			if(empty($item['category'])){
				$category = '-Uncategorized-';
			}else{
				$category = $item['category'];
			}
			
			if(empty($items[$category])){
				$items[$category] = array('name' => $category, 'category' => '', 'sub_category' => '', 'items' => array());
			}	
			$array =& $items[$category]['items'];
			
			if(!empty($item['subcategory'])){
				$subcategory = $item['subcategory'];
				
				if(empty($items[$category]['items'][$subcategory])){
					$items[$category]['items'][$subcategory] = array('name' => $subcategory, 'category' => $category, 'sub_category' => '', 'items' => array());
				}			
				$array =& $items[$category]['items'][$subcategory]['items'];			
			
			}else{
				$array =& $items[$category]['items'];
			}
			
			$array[] = $item;
		}

		$this->sort_items($items);
		$data['items'] = $items;
		unset($items);
		
		// Get table layout data organize into array by sale_id
		$data['layouts'] = $this->table_layout->get();

		// Get recent transactions
		$data['recent_transactions'] = $this->Table->get_recent_transactions($employee_id, 500);

		// Get list of all employees with access to F&B
		$employees = $this->Employee->get_all_fnb();
		$data['employees'] = $employees;

		$data['print_sales_receipt'] = $this->config->item('print_sales_receipt');
		$data['print_tip_line'] = $this->config->item('print_sales_receipt');
		$data['print_credit_card_receipt'] = $this->config->item('print_credit_card_receipt');
		$data['receipt_printer_ip'] = $this->config->item('receipt_printer');

		$this->load->view("food_and_beverage/register", $data);
	}
	
	private function apply_sort_numbers(&$items){
		
		// Get item ordering
		$order = $this->Table->get_button_order();
		
		// Loop through all items and apply sort number to item
		foreach($items as &$item){
			$itemKey = $item['name'].'/'.$item['category'].'/'.$item['subcategory'];
			
			if(isset($order[$itemKey])){
				$item['sort_order'] = $order[$itemKey];	
			}	
		}
	}
	
	// Loop through all categories, sub-categorys and items and sort
	// them in the array to display properly on F&B page
	private function sort_items(&$items){
		
		$order = $this->Table->get_button_order();
		
		$sort = function(&$a, &$b) use ($order){
			$valA = 0;
			$valB = 0;
			
			$aKey = $a['name'].'/'.$a['category'].'/'.$a['subcategory'];
			$bKey = $b['name'].'/'.$b['category'].'/'.$b['subcategory'];
			
			if(isset($order[$aKey])){
				$valA = $order[$aKey];
			}
			if(isset($order[$bKey])){
				$valB = $order[$bKey];
			}
			$a['sort_order'] = $valA;
			$b['sort_order'] = $valB;

			return $valA > $valB;			
		};
		
		usort($items, $sort);
		
		// Loop through categories
		foreach($items as &$category){
			
			// Sub categories (or items without a sub category)
			if(!empty($category['items'])){
				usort($category['items'], $sort);
				
				// Items
				foreach($category['items'] as &$sub_cat){
					usort($sub_cat['items'], $sort);
				}
			}
		} 
	}
	
	function login(){
		
		$pin_or_card = $this->input->post('pin_or_card');
		$employee_info = $this->Employee->get_info_by_pin_or_card($pin_or_card);
		if ($employee_info->person_id != '')
		{
			$has_food_and_beverage_permission = false;
			$allowed_modules = $this->Module->get_allowed_modules($employee_info->person_id);
			foreach ($allowed_modules->result_array() as $allowed_module)
			{
				$has_food_and_beverage_permission = ($has_food_and_beverage_permission ? $has_food_and_beverage_permission : $allowed_module['module_id'] == 'food_and_beverage');
			}

			// If user doesn't have F&B permission, return error
			if (!$has_food_and_beverage_permission)
			{
				echo json_encode(array('success'=>false, 'message'=>lang('food_and_beverage_no_permission')));
				return;
			}

			$logged_in = $this->Employee->login($employee_info->username, $employee_info->password, true);

			if ($logged_in)
			{
				$this->session->set_userdata('fnb_logged_in', true);

				// Force the session to update database immediately
				// instead of waiting for page refresh as usual
				$this->session->sess_write();

				$register_log = $this->Sale->getUnfinishedRegisterLog();

				echo json_encode(
					array(
						'employee' => $employee_info,
						'recent_transactions' => $this->Table->get_recent_transactions($employee_info->person_id, 500),
						'success' => true,
						'register_log' => $register_log->row_array()
					)
				);

			}else{
				echo json_encode(array('success'=>false, 'message'=>lang('food_and_beverage_error_logging_in')));
			}
			return;
		}

		echo json_encode(array('success'=>false, 'message'=>lang('food_and_beverage_invalid_pin_or_card')));
	}
	
	function logout()
	{
		$this->session->unset_userdata('fnb_logged_in');
		//$this->session->set_userdata('person_id', '-1');
		$this->session->unset_userdata('user_level');
		$this->session->unset_userdata('auto_mailers');
        $this->session->unset_userdata('config');
        $this->session->unset_userdata('courses');
        $this->session->unset_userdata('customers');
        $this->session->unset_userdata('dashboard');
        $this->session->unset_userdata('employees');
        $this->session->unset_userdata('events');
        $this->session->unset_userdata('giftcards');
        $this->session->unset_userdata('item_kits');
        $this->session->unset_userdata('items');
        $this->session->unset_userdata('invoices');
        $this->session->unset_userdata('marketing_campaigns');
        $this->session->unset_userdata('promotions');
        $this->session->unset_userdata('receivings');
        $this->session->unset_userdata('reports');
        $this->session->unset_userdata('reservations');
        $this->session->unset_userdata('sales');
        $this->session->unset_userdata('schedules');
        $this->session->unset_userdata('suppliers');
        $this->session->unset_userdata('teesheets');
		$this->session->unset_userdata('tournaments');

		$this->session->unset_userdata('table_id');
		$this->session->unset_userdata('table_number');
		$this->session->unset_userdata('suspended_sale_id');

		// Force the session to update database immediately
		$this->session->sess_write();

		echo json_encode(array('success' => true));
	}

	function credit_card_window(){

		$total = $this->input->post('amount');
		$receipt_id = $this->input->post('receipt_id');
		$table_number = $this->session->userdata('table_number');
		$action = $this->input->post('action');
		$payment_type = $this->input->post('payment_type');
		
		if(empty($total)){
			echo 'Error: Transaction total is required';
			return false;
		}

		if(empty($receipt_id)){
			echo 'Error: Receipt ID is required';
			return false;
		}
		$tax = 0;
		$previous_declined = false;

		// USING ETS FOR PAYMENT PROCESSING
		if ($this->config->item('ets_key'))
		{
			$type = $this->input->post('ets_type');
			
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));
			$store_Primary = $this->Sale->add_credit_card_payment(array('ets_id'=>$this->config->item('ets_key'),'tran_type'=>'Sale', 'frequency'=>'OneTime'));
			
			if($type == 'credit card'){
				$session = $payment->set('action', 'session')
				   ->set('amount', $total)
				   ->set('store.Primary', $store_Primary)
				   ->send();
			
			}else if ($type == 'giftcard'){
				
				if($action == 'refund'){
					$session = $payment->set('action', 'session')
					   ->set('posAction', 'refund')
					   ->set('amount', $total)
					   ->set('store.Primary', $store_Primary)
					   ->set('PaymentMethods', "GiftCard")
					   ->send();					
				}else{
					$session = $payment->set('action', 'session')
					   ->set('amount', $total)
					   ->set('store.Primary', $store_Primary)
					   ->set('PaymentMethods', "GiftCard")
					   ->send();
				 }
			}

			if ($session->id)
			{
				$user_message = $previous_card_declined != 'false' ? 'Card declined, please try another.':'';
				$return_code = '';
				$this->session->set_userdata('ets_session_id', (string)$session->id);
				$data = array(
					'action' => $action,
					'user_message' => $user_message,
					'return_code' => $return_code,
					'session' => $session,
					'url' => site_url('sales/ets_payment_made'),
					'amount' => $total,
					'payment_type' => $payment_type,
					'receipt_id' => $receipt_id);

				$this->load->view('food_and_beverage/credit_card_ets', $data);
			}
			else
			{
				$data = array('processor' => 'ETS');
				$this->load->view('sales/cant_load', $data);
			}

		// USING MERCURY FOR PAYMENT PROCESSING
		}else if ($this->config->item('mercury_id')){

			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();

			$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
			$HC->set_response_urls('food_and_beverage/mercury_payment/'.$receipt_id, 'sales/process_cancelled');
			$initialize_results = $HC->initialize_payment($total, $tax, 'Sale', 'POS', 'OneTime');

			if ((int) $initialize_results->ResponseCode == 0){
				$user_message = '';
				$payment_id = (string) $initialize_results->PaymentID;
				$return_code = (int) $initialize_results->ResponseCode;
				$this->session->set_userdata('payment_id', $payment_id);
				$url = $HC->get_iframe_url('POS', $payment_id);
				$data = array('user_message' => $user_message, 'return_code' => $return_code, 'url' => $url, 'receipt_id' => $receipt_id);

				$this->load->view('food_and_beverage/credit_card_mercury', $data);
			}
		}
	}

	function mercury_payment($receipt_id){
		$data = $this->input->post();
		$data['receipt_id'] = $receipt_id;

		$this->load->view('food_and_beverage/mercury_payment', $data);
	}

	function layout(){
		$data = array();
		$this->load->model('table_layout');
		$data['layouts'] = $this->table_layout->get();
		$data['employee_id'] = (int) $this->session->userdata('person_id');

		$this->load->view('food_and_beverage/layout', $data);
	}

	function edit_layout(){
		$this->load->model('table_layout');
		$data['layouts'] = $this->table_layout->get();

		$this->load->view('food_and_beverage/layout_editor', $data);
	}

	function save_layout(){
		$name = $this->input->post('name');
		$layout_id = $this->input->post('layout_id');

		$this->load->model('table_layout');
		$layout_id = $this->table_layout->save($layout_id, $name);

		if($layout_id){
			echo json_encode(array('success'=>true, 'layout_id'=>$layout_id));
		}else{
			echo json_encode(array('success'=>false));
		}
	}

	function delete_layout(){
		$layout_id = $this->input->post('layout_id');
		$this->load->model('table_layout');
		$success = $this->table_layout->delete($layout_id);

		if($success){
			echo json_encode(array('success'=>true, 'layout_id'=>$layout_id));
		}else{
			echo json_encode(array('success'=>false));
		}
	}

	function save_layout_object(){
		$this->load->model('table_layout_object');
		$layout_object = $this->input->post();
		
		// If a sale is currently open with that table, don't allow the name to be changed
		if(!empty($layout_object['object_id']) && $this->table_layout_object->has_open_sale($layout_object['object_id'])){
			echo json_encode(array('success'=>false, 'msg'=>'Please close open sale on table before changing name'));
			return false;
		}		
		
		// If an object already exists with that name, return error
		if($this->table_layout_object->object_exists($layout_object['label'], $layout_object['object_id'])){
			echo json_encode(array('success'=>false, 'msg'=>'Error, that name is already in use'));
			return false;
		}
		$object_id = $this->table_layout_object->save($layout_object);

		if($object_id){
			echo json_encode(array('success'=>true, 'object_id'=>$object_id));
		}else{
			echo json_encode(array('success'=>false));
		}
	}

	function delete_layout_object(){
		$this->load->model('table_layout_object');
		$object_id = $this->input->post('object_id');

		$success = $this->table_layout_object->delete($object_id);

		if($success){
			echo json_encode(array('success'=>true, 'object_id'=>$object_id));
		}else{
			echo json_encode(array('success'=>false));
		}
	}
	
	function last_layout_viewed(){
		$layout_id = (int) $this->input->post('layout_id');
		
		if(empty($layout_id)){
			$this->session->unset_userdata('fnb_last_layout');
		}else{
			$this->session->set_userdata('fnb_last_layout', $layout_id);
		}
		
		echo json_encode(array('success'=>true));
	}

	function item_search()
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->get('term'),100);
		$suggestions = array_merge($suggestions, $this->Item_kit->get_item_kit_search_suggestions($this->input->get('term'),100));
		$suggestions = array_merge($suggestions, $this->Tournament->get_search_suggestions($this->input->get('term'),100));
		echo json_encode($suggestions);
	}

	function employee_search()
	{
		$suggestions = $this->Employee->get_search_suggestions($this->input->get('term'),100, true);
		echo json_encode($suggestions);
	}

	function customer_search($type='')
	{
		$term = $this->input->get('term');

		if($term == '' || $term == null){
			$customers = array();
		}else{
			$customers = $this->Customer->get_customer_search_suggestions($term, 25, 'ln_and_pn');
		}

		// Get extra data on customers found
		if(!empty($customers)){
			$customer_ids = array();

			foreach($customers as $customer){
				$customer_ids[] = (int) $customer['value'];
			}
			$customer_id_list = implode(',',$customer_ids);

			$this->db->select("p.person_id AS value, c.account_number, c.discount,
				CONCAT(p.first_name,' ',p.last_name) AS label,
				p.person_id, p.first_name, p.last_name, p.phone_number AS phone,
				p.email, c.image_id, c.member_account_balance, c.member_account_balance_allow_negative,
				c.account_balance, c.account_balance_allow_negative, c.taxable,
				IF(
					ISNULL(image.image_id),
					CONCAT('images/profiles/profile_picture.png'),
					CONCAT({$this->config->item("urls")["files"]}'/', c.course_id, '/thumbnails/', image.filename)
				) AS photo, c.status_flag, p.comments", false);
			$this->db->from('customers AS c');
			$this->db->join('people AS p', 'p.person_id = c.person_id', 'inner');
			$this->db->join('images AS image', 'image.image_id = c.image_id', 'left');
			$this->db->where_in('c.person_id', $customer_ids);
			$this->db->group_by('c.person_id');

			$customers = $this->db->get()->result_array();
		}

		echo json_encode($customers);
	}
}