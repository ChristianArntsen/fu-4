<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Item_kits extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('item_kits');
	}

	function index()
	{
		$config['base_url'] = site_url('item_kits/index');
		$config['total_rows'] = $this->Item_kit->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);

		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_item_kits_manage_table( $this->Item_kit->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('item_kits/manage',$data);
	}
    function get_multiple_info()
    {
        $items = $this->input->post('items');
        $items_info = array('success'=>true, 'items'=> array());
        foreach ($items as $item_id) {
            $item_info =  $this->Item_kit->get_info($item_id);
            $item_info->item_id = $item_info->item_kit_id;
            $barcode_number = $item_info->item_kit_number != NULL ? $item_info->item_kit_number : $item_info->item_kit_id;
            //$item_info->long_item_id = number_pad($barcode_number, 11);
            $item_info->long_item_id = str_pad($barcode_number, 11,"0",STR_PAD_LEFT);
            $item_info->barcode_number = number_pad($item_info->item_kit_id, 11);
            $items_info['items'][] = $item_info;
        }
        echo json_encode($items_info);
    }


    /* added for excel expert */
	function excel_export() {
		$data = $this->Item_kit->get_all()->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array("UPC/EAN/ISBN", "Item Kit Name", "Category", "Cost Price", "Unit Price", "Tax1 Name" , "Tax1 Value", "Tax2 Name", "Tax2 Value", "Cumulative?", "Description");
		$rows[] = $row;

		foreach ($data as $r) {
			$taxdata = $this->Item_kit_taxes->get_info($r->item_kit_id);
			if (sizeof($taxdata) >= 2) {
				$r->taxn = $taxdata[0]['name'];
				$r->taxp = $taxdata[0]['percent'];
				$r->taxn1 = $taxdata[1]['name'];
				$r->taxp1 = $taxdata[1]['percent'];
				$r->cumulative = $taxdata[1]['cumulative'] ? 'y' : '';
			} else if (sizeof($taxdata) == 1) {
				$r->taxn = $taxdata[0]['name'];
				$r->taxp = $taxdata[0]['percent'];
				$r->taxn1 = '';
				$r->taxp1 = '';
				$r->cumulative = '';
			} else {
				$r->taxn = '';
				$r->taxp = '';
				$r->taxn1 = '';
				$r->taxp1 = '';
				$r->cumulative = '';
			}

			$row = array(
				$r->item_kit_number,
				$r->name,
				$r->category,
				$r->cost_price,
				$r->unit_price,
				$r->taxn,
				$r->taxp,
				$r->taxn1,
				$r->taxp1,
				$r->cumulative,
				$r->description
			);

			$rows[] = $row;
		}

		$content = array_to_csv($rows);
		force_download('itemkits_export' . '.csv', $content);
		exit;
	}

	function search($offset = 0)
	{
		$search=$this->input->post('search');
		$data_rows=get_item_kits_manage_table_data_rows($this->Item_kit->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
		$config['base_url'] = site_url('item_kits/index');
        $config['total_rows'] = $this->Item_kit->search($search, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Item_kit->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	function get_row()
	{
		$item_kit_id = $this->input->post('row_id');
		$data_row=get_item_kit_data_row($this->Item_kit->get_info($item_kit_id),$this);
		echo $data_row;
	}

	function view($item_kit_id=-1)
	{
		$this->load->model('quickbooks');
		$data['item_kit_info']=$this->Item_kit->get_info($item_kit_id);
		$data['item_kit_tax_info']=$this->Item_kit_taxes->get_info($item_kit_id);
		$data['default_tax_1_rate']=($item_kit_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_kit_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		$data['default_tax_2_cumulative']=($item_kit_id==-1) ? $this->Appconfig->get('default_tax_2_cumulative') : '';
		$data['quickbooks_accounts'] = $this->quickbooks->get_accounts_menu();
		$data['quickbooks_accounts'][''] = '- Default -';
		$this->load->view("item_kits/form",$data);
	}

	function save($item_kit_id=-1)
	{
		$item_kit_data = array(
		'item_kit_number'=>$this->input->post('item_kit_number')=='' ? null:$this->input->post('item_kit_number'),
		'name'=>$this->input->post('name'),
		'department'=>$this->input->post('department'),
		'category'=>$this->input->post('category'),
		'subcategory'=>$this->input->post('subcategory'),
		'unit_price'=>$this->input->post('unit_price')=='' ? null:$this->input->post('unit_price'),
		'cost_price'=>$this->input->post('cost_price')=='' ? null:$this->input->post('cost_price'),
		'description'=>$this->input->post('description'),
		'is_punch_card'=>$this->input->post('is_punch_card') ? 1 : 0,
		'course_id'=>$this->session->userdata('course_id')
		);

		if($this->Item_kit->save($item_kit_data,$item_kit_id))
		{
			//New item kit
			if($item_kit_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('item_kits_successful_adding').' '.
				$item_kit_data['name'],'item_kit_id'=>$item_kit_data['item_kit_id']));
				$item_kit_id = $item_kit_data['item_kit_id'];
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('item_kits_successful_updating').' '.
				$item_kit_data['name'],'item_kit_id'=>$item_kit_id));
			}

			if ($this->input->post('item_kit_item'))
			{
				$item_kit_items = array();
				foreach($this->input->post('item_kit_item') as $item_id => $item)
				{
					$price_class_id = (int) isset($item['price_class_id']) ? $item['price_class_id'] : 0;
					$quantity = isset($item['quantity']) ? $item['quantity'] : 0;
					
					$item_kit_items[] = array(
						'item_id' => $item_id,
						'quantity' => $quantity,
						'price_class_id' => $price_class_id
					);
				}

				$this->Item_kit_items->save($item_kit_items, $item_kit_id);
			}

			$item_kits_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			$tax_cumulatives = $this->input->post('tax_cumulatives');
			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$item_kits_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k], 'cumulative' => isset($tax_cumulatives[$k]) ? $tax_cumulatives[$k] : '0' );
				}
			}
			$this->Item_kit_taxes->save($item_kits_taxes_data, $item_kit_id);
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('item_kits_error_adding_updating').' '.
			$item_kit_data['name'],'item_kit_id'=>-1));
		}

	}

	function delete()
	{
		$item_kits_to_delete=$this->input->post('ids');

		if($this->Item_kit->delete_list($item_kits_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('item_kits_successful_deleted').' '.
			count($item_kits_to_delete).' '.lang('item_kits_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('item_kits_cannot_be_deleted')));
		}
	}

//	function generate_barcodes($item_kit_ids)
//	{
//		$result = array();
//
//		$item_kit_ids = explode('~', $item_kit_ids);
//		foreach ($item_kit_ids as $item_kid_id)
//		{
//			$item_kit_info = $this->Item_kit->get_info($item_kid_id);
//
//			$result[] = array('name' =>$item_kit_info->name.': '.to_currency($item_kit_info->unit_price), 'id'=> 'KIT '.number_pad($item_kid_id, 7), 'number'=>$item_kit_info->item_kit_number);
//		}
//
//		$data['items'] = $result;
//		$data['scale'] = 2;
//		$this->load->view("barcode_sheet", $data);
//	}
    function generate_barcodes($item_ids, $sheet_style = '', $start_index = 1, $additional_price = false, $additional_price_percent = false, $top_margin = '')
    {
        $result = array();

        $data['items'] = $result;
        $data['sheet_style'] = $sheet_style;
        if ($sheet_style == 5267)
        {
            $data['scale'] = 1.5;
            $data['thickness'] = 20;
            $v_limit = 20;
            $h_limit = 4;
            $char_limit = 18;
        }
        else if ($sheet_style == 5160)
        {
            $data['scale'] = 1.5;
            $data['thickness'] = 20;
            $v_limit = 10;
            $h_limit = 3;
            $char_limit = 32;
        }
        else {
            $data['scale'] = 2;
            $v_limit = 20;
            $h_limit = 4;
        }
        $item_ids = explode('~', $item_ids);
        foreach ($item_ids as $item_id)
        {
            $item_info = $this->Item_kit->get_info($item_id);
            $price = to_currency($item_info->unit_price);
            $additional_price_string = '';
            if ($additional_price_percent) {
                $additional_price_string = urldecode($additional_price).' '.to_currency($item_info->unit_price * (100 + $additional_price_percent)/100);
            }
            $result[] = array('name' =>character_limiter($item_info->name,$char_limit-strlen($price), '...').' '.$price, 'unit_price'=>to_currency($item_info->unit_price), 'id'=> number_pad($item_id, 11), 'number'=>$item_info->item_kit_number, 'additional_price'=>$additional_price_string);
        }

        /*$this->load->view('barcode_sheet', $data);
        $this->load->library("Html2pdf");
        $html2pdf = new Html2pdf('P','A4','fr', true, 'UTF-8', array(0, 8, 0, 3));
        //$html2pdf->setModeDebug();
        $html2pdf->setDefaultFont('Arial');
        //$html = $this->load->view('items/form',$data, true);
        $html = $this->load->view("barcode_sheet", $data, true);//"<div style='height:300px; width:300px; background-color:blue'>Just testing it</div>";
        $html2pdf->writeHTML($html);
        $html2pdf->Output('example.pdf');
        */
        $this->load->library('fpdf');
        $pdf = new FPDF();
        $pdf->Open();
        $pdf->AddPage();
        $pdf->SetFont('Helvetica', 'B', 8);
        $pdf->SetMargins(0, 0);
        $pdf->SetAutoPageBreak(false);
        $x = $y = 0;

        while ($start_index > 1)
        {
            if ($sheet_style == 5267)
                $this->Item->Avery5267($x, $y, $pdf, '', '', '', $top_margin);
            else if ($sheet_style == 5160)
                $this->Item->Avery5160($x, $y, $pdf, '', '', '', $top_margin);

            $y++; // next row
            if($y == $v_limit) { // end of page wrap to next column
                $x++;
                $y = 0;
                if($x == $h_limit) { // end of page
                    $x = 0;
                    $y = 0;
                    $pdf->AddPage();
                }
            }
            $start_index--;
        }
        foreach($result as $item) {
            if ($sheet_style == 5267)
                $this->Item->Avery5267($x, $y, $pdf, urlencode($item['name']), $item['id'], $item['number'], $top_margin);
            else if ($sheet_style == 5160)
                $this->Item->Avery5160($x, $y, $pdf, $item['name'], $item['id'], $item['number'], $top_margin, $item['additional_price']);

            $y++; // next row
            if($y == $v_limit) { // end of page wrap to next column
                $x++;
                $y = 0;
                if($x == $h_limit) { // end of page
                    $x = 0;
                    $y = 0;
                    $pdf->AddPage();
                }
            }
        }
        $pdf->Output();
    }
    function barcode_details($item_ids, $sheet_style = '')
    {
        $result = array();

        $item_ids = explode('~', $item_ids);
        foreach ($item_ids as $item_id)
        {
            $item_info = $this->Item_kit->get_info($item_id);

            $result[] = array('name' =>$item_info->name, 'id'=> $item_id);
        }

        $data['items'] = $result;
        $data['sheet_style'] = $sheet_style;
        $data['controller_name']=strtolower(get_class());

        $this->load->view('items/form_barcode.php', $data);
    }

    function generate_barcode_labels($item_kit_ids)
	{
		$result = array();

		$item_kit_ids = explode('~', $item_kit_ids);
		foreach ($item_kit_ids as $item_kid_id)
		{
			$item_kit_info = $this->Item_kit->get_info($item_kid_id);

			$result[] = array('name' =>$item_kit_info->name.': '.to_currency($item_kit_info->unit_price), 'id'=> 'KIT '.number_pad($item_kid_id, 7), 'number'=>$item_kit_info->item_kit_number);
		}

		$data['items'] = $result;
		$data['scale'] = 1;
		$this->load->view("barcode_labels", $data);
	}


	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 550;
	}

	function migrate($password = false, $dry_run = 1){
		
		if($password != 'secretpassword'){
			echo 'Wrong password';
			return false;
		}

		$courses_to_migrate = $this->db->select('GROUP_CONCAT(course_id) AS course_id')
			->from('courses')
			->where('sales_v2', 1)
			->get()
			->row_array();

		$course_ids = explode(',', $courses_to_migrate['course_id']);

		$item_kits = $this->db->select("name, item_kit_id, unit_price, 
			0 AS new_price, cost_price", false)
			->from('item_kits')
			->where_in('course_id', $course_ids)
			->get()
			->result_array();

		echo "<table border='1' cellpadding='5' style='width: 800px'>
			<tr>
				<th>ID</th>
				<th>Item Kit Name</th>
				<th style='width: 100px'>Price</th>
				<th style='width: 100px'>New Price</th>
			</tr>";

		foreach($item_kits as $item_kit){
			
			$item_kit_price = (float) $item_kit['unit_price'];
			$item_kit_cost = 0.00;

			$kit_items = $this->db->select('kit_item.item_kit_id, kit_item.item_id, 
				kit_item.quantity, item.unit_price, item.cost_price, item.name')
				->from('item_kit_items AS kit_item')
				->join('items AS item', 'item.item_id = kit_item.item_id', 'left')
				->where('kit_item.item_kit_id', $item_kit['item_kit_id'])
				->get()->result_array();

			if(!empty($kit_items)){
				
				$total_quantity = 0;
				foreach($kit_items as $kit_item){
					$total_quantity += $kit_item['quantity'];
				}
				
				$single_qty_item = false;
				foreach($kit_items as $key => $kit_item){
					/*if(!$single_qty_item && $kit_item['quantity'] == 1){
						$single_qty_item = $key;
					}*/					
					$kit_items[$key]['new_price'] = round($item_kit_price / $total_quantity, 2);
					$item_kit['new_price'] += ($kit_items[$key]['new_price'] * $kit_item['quantity']);
				}
				/*
				$remainder = ($item_kit_price * 100) % $total_quantity;
				if($remainder != 0){
					
					if(!$single_qty_item){
						$item_kit['new_price'] -= round($remainder / 100, 2);
					}else{
						$kit_items[$single_qty_item]['new_price'] += round($remainder / 100, 2);
						$single_qty_item = false;						
					}
				} */

				$item_kit_cost += $kit_item['cost_price'];
			}

			$item_kit['cost_price'] = $item_kit_cost;

			echo "<tr>
				<td>{$item_kit['item_kit_id']}</td>
				<td>
					<strong>{$item_kit['name']}</strong>
					<ul>";

			if(!empty($kit_items)){
				foreach($kit_items as $kit_item){
					echo "<li>({$kit_item['quantity']}) {$kit_item['name']} @ ".to_currency($kit_item['new_price'])."</li>";
					
					// Update sub-item price
					if($dry_run == 0){
						$this->db->update('item_kit_items', [
							'unit_price' => $kit_item['new_price']
						], [
							'item_kit_id' => $kit_item['item_kit_id'],
							'item_id' => $kit_item['item_id']
						]);						
					}
				}

			}else{
				echo "<li><em>No items</em></li>";
			}

			$red = '';
			if($item_kit['unit_price'] != $item_kit['new_price']){
				$red = 'color: red';
			}			

			echo "</ul>
				</td>
				<td>{$item_kit['unit_price']}</td>
				<td style='{$red}'>{$item_kit['new_price']}</td>
			</tr>";

			// Update item kit prices
			if($dry_run == 0){
				$this->db->update('item_kits', [
					'unit_price' => $item_kit['new_price'],
					'cost_price' => $item_kit_cost
				], [
					'item_kit_id' => $item_kit['item_kit_id']
				]);
			}
		}

		echo '</table>';
	}
}
?>