<?php
class Chart_Data_Request extends MY_Controller {

    function __construct(){

	    if (extension_loaded('newrelic')) {
		    newrelic_set_appname("reporting");
	    }
        parent::__construct();
        session_write_close();
        $this->load->model('Report');
        $this->CI = & get_instance();
        $this->sale_id = false;
    }

    public function create($report_id){
        $json = file_get_contents('php://input');
        $testRun = $this->input->get('testRun');

        $input = json_decode($json,true);

        $report = $this->CI->Report->get_info($report_id);
        //If split, set order param then parse output
        $chartParameters = $this->CreateReportChartsParameters($input);
        $reportParameters = $chartParameters->createReportParameters($report);
        $reportObj = fu\reports\ReportFactory::create($report->base,$reportParameters);

        //Get report, call Reports factory based off base_Table
        try {
            if(count($reportParameters->getColumns())==0){
                throw new \fu\Exceptions\InvalidArgument("Unable to run summary without any columns");
            }
	        if(isset($testRun) && $testRun){
		        $reportObj->setTestRun(true);
	        }
            $data = $reportObj->getData();
            $chartData = new \fu\reports\ReportChartsData($chartParameters);
            $chartData->loadFromDBRows($data);
            $data = $chartData->render();
        } catch (Exception $e){
            $data = ["error"=>$e->getMessage()];
        }
        print json_encode($data);
    }


    /**
     * @param $report
     * @param $input
     * @return \fu\reports\ReportParameters
     * @throws \fu\Exceptions\InvalidArgument
     */
    private function createReportChartsParameters($input)
    {

        if(!isset($input['range']) || $input['range'] == ""){
            throw new \fu\Exceptions\InvalidArgument("Unable to run chart without a range");
        }

        $chart = new \fu\reports\ReportChartsParameters();
        $chart->setFilters($input['filters']);
        $chart->setRange($input['range']);
        $chart->setDateRange($input['date_range']);
        $chart->setCategory($input['category']);
        $chart->setVisualGrouping($input['visual_grouping']);
        $chart->setVisualWidth($input['visual_width']);
        isset($input['split_by'])?$chart->setSplitBy($input['split_by']):"";

        return $chart;
    }

}
