<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Inventory_audits extends REST_Controller {

    function __construct(){
        parent::__construct();
        $this->authenticate();

        $this->load->model('v2/item_model');
        $this->load->model('v2/inventory_audit_model');
    }

    // Get list of items filtered by a variety of parameters
    function index_get($method = null)
    {

        session_write_close();

        if ($method == 'audit_items') {
            $items = $this->audit_items_get();

            $this->response($items, 200);
        }

        $audits = $this->audits_get();

        $this->response($audits, 200);
    }

    function index_post($method = null, $record_id = null){

        $item_id = $this->item_model->save($method, $this->request->body);

        if($method == 'audit_items'){
            return $this->quickbuttons_post();
        }

        if(!$item_id){
            $msg = 'Error saving item';
            if(!empty($this->item_model->error)){
                $msg = $this->item_model->error;
            }
            $this->response(['msg' => $msg], 400);

        }else{
            $this->response(['item_id' => (int) $item_id], 200);
        }
    }

    function index_put($method = null, $record_id = null){

        session_write_close();

        if($method == 'audit_items'){
            return $this->audit_items_put($record_id);
        }
        $this->index_post($method, $record_id);
    }

    function index_delete($method = null, $record_id = null){

        if($method == 'audit_items'){
            return $this->audit_items_delete($record_id);
        }

        $this->item_model->delete($method);
        $this->response(['item_id' => (int) $method], 200);
    }

    function audits_get(){
        session_write_close();
        $params = $this->input->get();
        $audit = $this->inventory_audit_model->get($params);

        $this->response($audit, 200);
    }

    function audit_items_get(){
        session_write_close();
        $params = $this->input->get();
        $audit = $this->inventory_audit_model->get_items($params);

        $this->response($audit, 200);
    }
}