<?php
require_once(APPPATH . 'libraries/REST_Controller.php');

class contact_forms_public extends CI_Controller {
	protected $response = NULL;

	private $provided_key = null;
	private $provided_contact_form_id = null;

	public function __construct(){
		parent::__construct();

		// Instantiate DI Models.
		$this->load->model('contact_form');
		$this->load->model('contact_form_reply');
		$this->load->model('contact_form_key');

		$this->provided_key = $this->input->get('key');
		$this->provided_contact_form_id = (int)$this->input->get('form');
		$this->contact_form_key->key = $this->provided_key;
		$this->contact_form->id = $this->provided_contact_form_id;

		// Get the course_id for the key
		if($this->contact_form_key->retrieve_by_key()) {
			// Valid key, now check that the contact form and it's course match
			if(
				$this->contact_form->retrieve() !== true ||
				$this->contact_form->course_id != $this->contact_form_key->course_id
			){
				// Invalid Contact for or mismatched courses
				$this->response_not_authorized();
			}
		} else {
			// Invalid Key
			$this->response_not_authorized();
		}
	}

	/*
	 * Returns the current contact form view
	 */
	public function index_get(){
		session_write_close();
		header('Content-type: text/html');

		// Clean up the data to be passed.
		unset($data);

		$data['form_url'] = base_url()."/api/customer_contact_form?form=".$this->contact_form->id."&key=".$this->contact_form_key->key;

		// Clean up data. Only pass what we need to.
		$data['contact_form'] = array(
			'name' => $this->contact_form->name,
			'has_first_name' => $this->contact_form->has_first_name,
			'has_last_name' => $this->contact_form->has_last_name,
			'has_phone_number' => $this->contact_form->has_phone_number,
			'has_email' => $this->contact_form->has_email,
			'has_birthday' => $this->contact_form->has_birthday,
			'has_address' => $this->contact_form->has_address,
			'has_city' => $this->contact_form->has_city,
			'has_state' => $this->contact_form->has_state,
			'has_zip' => $this->contact_form->has_zip,
			'has_message' => $this->contact_form->has_message,
			'req_first_name' => ($this->contact_form->req_first_name === true ? 'required' : ''),
			'req_last_name' => ($this->contact_form->req_last_name === true ? 'required' : ''),
			'req_phone_number' => ($this->contact_form->req_phone_number === true ? 'required' : ''),
			'req_email' => ($this->contact_form->req_email === true ? 'required' : ''),
			'req_birthday' => ($this->contact_form->req_birthday === true ? 'required' : ''),
			'req_address' => ($this->contact_form->req_address === true ? 'required' : ''),
			'req_city' => ($this->contact_form->req_city === true ? 'required' : ''),
			'req_state' => ($this->contact_form->req_state === true ? 'required' : ''),
			'req_zip' => ($this->contact_form->req_zip === true ? 'required' : ''),
			'req_message' => ($this->contact_form->req_message === true ? 'required' : '')
		);

		// Load up the requested contact_form
		$this->load->view("contact_forms/form", $data);
	}

	/*
	 * Save contact form reply
	 */
	public function index_post(){
		session_write_close();

		$json_request_data = json_decode(file_get_contents('php://input'),true);
		$this->contact_form_reply->id = null;
		$this->contact_form_reply->contact_form_id = $this->contact_form->id;
		$this->contact_form_reply->customer_group_id = (int) $this->contact_form->customer_group_id;
		$this->contact_form_reply->person_id = null;
		$this->contact_form_reply->first_name = isset($json_request_data['first_name'])?$json_request_data['first_name']:"";
		$this->contact_form_reply->last_name = isset($json_request_data['last_name'])?$json_request_data['last_name']:"";
		$this->contact_form_reply->phone_number = isset($json_request_data['phone_number'])?$json_request_data['phone_number']:"";
		$this->contact_form_reply->email = isset($json_request_data['email'])?$json_request_data['email']:"";
		$this->contact_form_reply->birthday = isset($json_request_data['birthday'])?$json_request_data['birthday']:"";
		$this->contact_form_reply->address = isset($json_request_data['address'])?$json_request_data['address']:"";
		$this->contact_form_reply->city = isset($json_request_data['city'])?$json_request_data['city']:"";
		$this->contact_form_reply->state = isset($json_request_data['state'])?$json_request_data['state']:"";
		$this->contact_form_reply->zip = isset($json_request_data['zip'])?$json_request_data['zip']:"";
		$this->contact_form_reply->message = isset($json_request_data['message'])?$json_request_data['message']:"";

		if($this->contact_form_reply->create()){
			// Successfully Saved
			$this->response_successful();

		} else {
			// Could not create new reply
			$this->response_invalid_request();
		}
	}

	/*
	 * EVERYTHING BELOW THIS LINE IS TO **REIMPLEMENT** FUNCTIONALITY FROM THE REST_CONTROLLER CLASS :,(
	 */

	/*
	 * Invalid Request Response
	 */
	private function response_invalid_request(){
		$data = [
			'success'=>false,
			'message'=>'Invalid Request'
		];
		$http_code = 400;

		$this->json_response($http_code, $data);
	}

	/*
	 * Invalid Request Response
	 */
	private function response_not_authorized(){
		$data = [
			'success'=>false,
			'message'=>'Not Authorized'
		];
		$http_code = 401;

		$this->json_response($http_code, $data);
	}

	/*
	 * Invalid Request Response
	 */
	private function response_successful(){
		$data = [
			'success'=>true
		];
		$http_code = 200;

		$this->json_response($http_code, $data);
	}

	/*
	 * JSON Response
	 */
	private function json_response($http_code, $data = array()){
		// TODO: Probably should add some validation here
		$http_code = (int) $http_code;
		$data = json_encode($data);

		header('Content-type: application/json');
		header('HTTP/1.1: ' . $http_code);
		header('Status: ' . $http_code);
		header('Content-Length: ' . strlen($data));

		exit($data);
	}

	/*
	 * Detect request method
	 */
	protected function _detect_method()
	{
		$method = strtolower($this->input->server('REQUEST_METHOD'));

		if ($this->config->item('enable_emulate_request') && $this->input->post('_method')) {
			$method = $this->input->post('_method');
		}

		if (in_array($method, array('get', 'delete', 'post', 'put', 'patch'))) {
			return $method;
		}

		return 'get'; // Default return
	}

	/*
	 * Remap
	 *
	 * Requests are not made to methods directly The request will be for an "object".
	 * this simply maps the object and method to the correct Controller method.
	 */
	public function _remap($object_called, $arguments)
	{
		$pattern = '/^(.*)\.(' . implode('|', array_keys(array('json' => 'application/json', 'jsonp' => 'application/javascript'))) . ')$/';
		if (preg_match($pattern, $object_called, $matches)) {
			$object_called = $matches[1];
		}

		$controller_method = $object_called . '_' . $this->_detect_method();

		call_user_func_array(array($this, $controller_method), $arguments);
	}
}
