<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Personal_message extends REST_Controller
{

    public function __construct()
    {
	    parent::__construct();
	    session_write_close();
	    $this->load->model("Course");
    	$this->load->model("personal_messages");

	    if (extension_loaded('newrelic'))
		    newrelic_set_appname("polling");
    }

	public function get_new_messages()
	{

	}

	public function index_get()
	{
		$person = $this->get('person');
		$timestamp = $this->get('timestamp');
		if(empty($timestamp)){
			$history = $this->personal_messages->get_message_history($person);
			print json_encode($history);
			return;
		}
		for($i=0;$i<10;$i++){
			if($this->personal_messages->has_new_messages($timestamp,$person)){
				$history = $this->personal_messages->get_new_messages($timestamp,$person);
				print json_encode($history);
				return;
			}
			sleep(1);
		}

		print json_encode([]);

	}
	
	public function threads_get()
	{
		$timestamp = $this->get('timestamp');
		$virtual_number = $this->db->from("course_virtual_numbers")
			->where("course_id", $this->session->userdata('course_id'))
			->get()->row();
		if(empty($virtual_number)){
			print json_encode([]);
			return;
		}

		for($i=0;$i<4;$i++){
			$threads = $this->personal_messages->get_threads($timestamp);
			if(!empty($threads)){
				print json_encode($threads);
				return;
			}
			sleep(2);
		}
		$threads = $this->personal_messages->get_threads($timestamp);
		print json_encode($threads);
		return $threads;
	}
	
	public function threads_delete($person_id)
	{
		$data = $this->request->body;
		$this->personal_messages->ignore_threads($person_id);
		
		print json_encode($data);
		return $data;
	}
}

?>