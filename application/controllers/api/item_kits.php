<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Item_Kits extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();

		$this->load->model('v2/Item_kit_model');
	}

	function index_get($id = null){
		
		session_write_close();

		if($id == 'csv'){
			return $this->csv_get();
		}

		$response = false;
		$params = $this->input->get();

		if(!empty($params['search'])){
			$params['q'] = $params['search'];
		}
		$response = $this->Item_kit_model->get($params);
		
		$data = [
			'total' => $this->Item_kit_model->total_records(),
			'rows' => $response
		];

		$this->response($data, 200);
	}

	function csv_get(){
		$csv = $this->Item_kit_model->get_csv();
		force_download('itemkits_export' . '.csv', $csv);
		exit;
	}

	function index_post($id = null){
		$this->index_put($id);
	}

	function index_put($id = null){
		
		session_write_close();

		$response = false;
		$item_kit_id = $this->Item_kit_model->save($id, $this->request->body);
		
		if($item_kit_id){
			$response['item_kit_id'] = $item_kit_id;
			$this->response($response, 200);
		}else{
			$this->response(['success' => false], 400);
		}
	}

	function index_delete($id = null){
		
		session_write_close();

		$response = false;
		$response = $this->Item_kit_model->delete($id);
		
		$this->response($response, 200);
	}
}