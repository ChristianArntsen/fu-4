<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Passes extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();

		$this->load->model('Pass');
	}

	function index_get($id = null){
		
		session_write_close();

		$response = false;
		$response = $this->Pass->get($this->input->get());
		
		$data = [
			'total' => $this->Pass->total,
			'rows' => array_values($response)
		];

		$this->response($data, 200);
	}

	function index_post(){
		$this->index_put(null);
	}

	function index_put($id = null){
		
		session_write_close();

		$response = false;
		$response = $this->Pass->save($id, $this->request->body);
		
		$this->response(['pass_id' => $response], 200);
	}

	function index_delete($id = null){
		
		session_write_close();

		$response = false;
		$response = $this->Pass->delete($id);
		
		$this->response($response, 200);
	}
}