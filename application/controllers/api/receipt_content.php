<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Receipt_content extends REST_Controller {

    function __construct(){

        parent::__construct();
        $this->authenticate();
        $this->load->model('v2/Item_receipt_content');

        session_write_close();
    }

    function index_get($id = null){
        $data = $this->Item_receipt_content->get(['id' => $id]);
        if(!empty($id)){
            $data = $data[0];
        }
        return $this->response($data);
    }

    function index_put($receipt_content_id = null, $method = null){

        $data = $this->request->body;
        $id = $this->Item_receipt_content->save($receipt_content_id, $data);

        return $this->response(['receipt_content_id' => $id], 201);
    }

    function index_delete($id = null){

        $this->Item_receipt_content->delete($id);

        // Test response for finishing the front end
        return $this->response(['receipt_content_id' => $id], 200);
    }

    function index_post($id = null, $method = null){

        return $this->index_put($id, $method);
    }
}