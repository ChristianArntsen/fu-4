<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Customers extends REST_Controller {
	
	function __construct(){
		
		parent::__construct();
		$this->authenticate();
		
		session_write_close();
		$this->load->model('v2/customer_model');
        $this->load->model('Customer_merge');
	}
	
	// Get list of customers filtered by a variety of parameters
	function index_get($id = null){
		$params = $this->input->get();

		if(!empty($id)){
			$params['person_id'] = $id;
		}
		$items = $this->customer_model->get($params);

		$this->response($items, 200);
	}
	
	function index_put($customer_id = null, $method = null){
		$new_customer = !$customer_id || $customer_id == "new" ? true : false;
		$response = array();

		// Upload  photo
		if($method == 'photo'){
			$this->load->model('Image');
			$data = $this->input->post();

			if($customer_id == 'new'){
				$customer_id = false;
			}
			$image_data = $this->customer_model->save_photo($customer_id, $data['data'], $data['width'], $data['height'], $data['course_id']);
			
			if($image_data){
				$response = $image_data;
			}

            // Save customer data
        } else if ($method == 'credentials') {
            $data = $this->input->post();
            $credential_data = $this->customer_model->save_credentials($customer_id, $data['username'], $data['password'], $data['confirm_password']);

            if($credential_data){
                $response = $credential_data;
            }

            // Save customer merge
        } else if ($method == 'merge') {
            $data = $this->input->post();
            if($data['person_id_1'] == 0 || $data['person_id_2']==0){
            	return false;
            }
            $merge_data = $this->customer_model->save_merge($data);

            if($merge_data){
                $response = $merge_data;
            }

        } else {
			$data = $this->request->body;
			$customer_id = $this->customer_model->save($customer_id, $data);

			if($customer_id){
				$response['person_id'] = $customer_id;
			}
		}

		if(!empty($response)){

			$dispatchContext = new \Onoi\EventDispatcher\DispatchContext();
            $person_id = !empty($response['person_id']) ? $response['person_id'] : '';
			$dispatchContext->set("person_id",$person_id);
            if($method == 'merge'){
                $this->eventDispatcher->dispatch("customer.merged",$dispatchContext);
            } else if($new_customer){
                $this->eventDispatcher->dispatch("customer.created",$dispatchContext);
            } else {
				$this->eventDispatcher->dispatch("customer.updated",$dispatchContext);
			}


			$response['success'] = true;
			$this->response->format = "json";
			$this->response($response, 200);
		
		}else{
			$msg = $this->customer_model->error;
			$this->response(array('success' => false, 'msg' => $msg), 400);
		}
		return true;
	}	
	
	function index_post($customer_id = null, $method = null){
		return $this->index_put($customer_id, $method);
	}
}
