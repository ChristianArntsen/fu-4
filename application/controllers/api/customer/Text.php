<?php
class Text extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        session_write_close();
    }

    public function create($customer_id,$type)
    {
        $json = file_get_contents('php://input');
        $input = json_decode($json,true);
        $course_id = $this->session->userdata('course_id');
        $input['type'] = $type;
        $input['customer_id'] = $customer_id;

        $this->load->model("Marketing_Texting_Queue");
        $queue = new Marketing_Texting_Queue();
        $result = $queue->insert([
            "course_id"=>$course_id,
            "job_name"=>$type,
            "parameters"=>json_encode($input),
            "status"=>"queued",
        ]);


        if(!$result){
            print json_encode($queue->form_validation->get_all_errors());
            return;
        }

        print json_encode(["id"=>$result]);
    }
}