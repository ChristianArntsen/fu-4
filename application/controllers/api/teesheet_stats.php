<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Teesheet_stats extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();

	}

	function index_get(){
		
		$start = $_GET['start'] ?? \Carbon\Carbon::now()->toDateString();
		$teesheet_id = $_GET['tee_sheet_id'] ?? $teesheet_id = $this->session->userdata('teesheet_id');
		$results = $this->Dash_data->fetch_tee_sheet_data($teesheet_id,$start);
		if(empty($results['header'])){
			//Error
		}
		echo json_encode($results['header']);
	}

}