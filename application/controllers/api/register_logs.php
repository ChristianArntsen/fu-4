<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Register_logs extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();

		$this->load->model('v2/Register_log_model');
	}

	function index_get($register_log_id = null){
		
		$params = $this->request->body;
		$params['register_log_id'] = $register_log_id;
		$logs = $this->Register_log_model->get($params);
		$this->response($logs, 200);
	}

	function index_post($register_log_id = null){

		$data = $this->request->body;

		if(empty($data['shift_start'])){
			$data['shift_start'] = date('c');
		}
		$log_id = $this->Register_log_model->save($register_log_id, $data);

		if($log_id){
			$this->session->set_userdata('unfinished_register_log', 1);
			$this->response(array('register_log_id' => $log_id, 'shift_start' => $data['shift_start']));

		}else{
			$this->response(array('success'=>false, 'msg'=>'Error saving register log'), 500);
		}
	}

	function index_put($register_log_id = null){
		
		$this->load->model('v2/Sale_model');
		$data =  $this->request->body;

		if($data['status'] == 'close'){
			
			$shift_end = date('c');
			$log = $this->Register_log_model->get(array('register_log_id' => $register_log_id));
			$persist = ($log['persist'] == 1);
			
			$payments = $this->Sale_model->get_register_log_payment_totals($log['shift_start'], $shift_end, $log['employee_id'], $log['terminal_id'], $persist);

			$data['cash_sales_amount'] = (float) $payments['cash'];
			$data['check_sales_amount'] = (float) $payments['check'];
			$data['shift_end'] = $shift_end;
		
			$this->session->unset_userdata('skipped_register_log');
			$this->session->unset_userdata('cash_register');
			$this->session->set_userdata('unfinished_register_log', 1);

			$log_id = $this->Register_log_model->save($register_log_id, $data);
			$this->response(array('success'=>true, 'shift_end' => $shift_end, 'msg'=>'Register log updated'));
			return true;
		}
		
		if(isset($data['persist'])){
			$log_id = $this->Register_log_model->save($register_log_id, array('persist' => (int) $data['persist']));
		}
		
		$this->response(array('success'=>true), 200);
	}

	function index_delete($method = null){
		$this->response(array('success' => false, 'msg' => 'Method not allowed'), 405);
	}
	
	// Set session variable that the cash log was skipped
	function skip_post(){
		$this->session->set_userdata('skipped_register_log', 1);
		$this->session->set_userdata('unfinished_register_log', 1);
		$this->session->sess_write();
		$this->response(array('success' => true), 200);
	}
	
	// Set session variable that the cash log was skipped
	function continue_post(){
		$this->session->set_userdata('unfinished_register_log', 1);
		$this->session->sess_write();
		$this->response(array('success' => true), 200);
	}	
}
