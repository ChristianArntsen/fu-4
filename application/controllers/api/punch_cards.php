<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Punch_cards extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();

		$this->load->model('v2/Punch_card_model');
	}

	function index_get($id = null){
		
		$response = false;
		$number = $this->input->get('punch_card_number');
		$response = $this->Punch_card_model->get(array('punch_card_number' => $number));
		
		$this->response($response, 200);
	}
}
