<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH . 'libraries/REST_Controller.php');
use Symfony\Component\DomCrawler\Crawler;
class Sendgrid_hook extends REST_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function list_get()
	{

	}

	function proxy_post()
	{
		$html = $_POST['html'];
		$envelope = json_decode($_POST['envelope']);
		$to = $envelope->to;
		preg_match("/(\d+)@/",$to[0],$matches);
		if(count($matches)> 1){
			$to = $matches[1];
		} else {
			die("Can't find $to");
		}
		$subject = $_POST['subject'];
		$from = $envelope->from;

		$allowed_user = $this->db->from("api_user_courses")
			->select("people.email")
			->join("users","api_user_courses.actual_user_id = users.id","LEFT")
			->join("customers","api_user_courses.course_id = customers.course_id","LEFT")
			->join("people","customers.person_id = people.person_id","LEFT")
			->where("users.email",$from)
			->where("customers.person_id",$to);
		$allowed_user = $allowed_user->get()->row();

		if(empty($allowed_user)){
			die("not allowed");
		}
		$sendgridConfig = $this->config->item('sendgrid');
		$sendgrid = new SendGrid($sendgridConfig['username'], $sendgridConfig['password']);
		$email = new \SendGrid\Email();
		echo "Sending email from $from to ".$allowed_user->email." after requesting $to";
		$email->setSubject($subject)
			->setFrom($from)
			->setReplyTo($from)
			->setHtml($html)
			->setSmtpapiTos([$allowed_user->email]);

		$sendgrid->send($email);
		//Forward email to him
		die($allowed_user->email);
	}

	function create_post()
	{
		$this->load->model('Sendgrid');
		if(isset($_POST['html'])){

			$html = $_POST['html'];

			$to = $_POST['to'];
			$from = $_POST['from'];

			if(!preg_match("/(\w*)\@golfnow\.foreupsoftware\.com/",$to,$matches)){
				$this->db->insert("gn_emails", [
					"html"=>$html
				]);
				error_log("Unable to find a reg match to ".$to);
				return;
            }

			$credentials = $this->db->from("gn_credentials")->where("email",$matches[1]."@golfnow.foreupsoftware.com")->get()->result_array();
			if(empty($credentials)){
				error_log("Unable to find credentials for email".$matches[1]."@golfnow.foreupsoftware.com");
				return;
			}
			$credentials = $credentials[0];
			$teesheet_id = $credentials['teesheet_id'];
			$this->session->set_userdata("course_id",$credentials['course_id']);
			$this->session->set_userdata("person_id",-1);

			$savedInfo = $this->parseEmail($html);
			$this->db->insert("gn_emails", [
				"teetime_info" => json_encode($savedInfo),
				"html"=>$html
			]);
			if(empty($savedInfo['Confirmation #'])){
				error_log("Unable to parse confirmation number.");
				return;
			}
			if($savedInfo['cancel']){
				//Get teetime id from confirmation #
				$existingTime = $this->db->from("gn_emails")
					->where("confirmation_number",$savedInfo['Confirmation #'])
					->get()->row();
				//Delete tee time
				if(!empty($existingTime)){
					$teetime_id = $existingTime->teetime_id;
					$this->load->model("teetime");
					$this->teetime->delete($existingTime->teetime_id);
				} else {
					error_log("Unable to find a tee time with confirmation #: ".$savedInfo['Confirmation #']);
				}
			} else {
				$teetime_id = $this->createTeetime($teesheet_id, $savedInfo);
			}

			$this->db->insert("gn_emails", [
				"teetime_info" => json_encode($savedInfo),
				"html"=>$html,
				'teetime_id'=>$teetime_id,
				"confirmation_number"=>$savedInfo['Confirmation #']
			]);


			return;
		}



		session_write_close();
		$input = json_decode(file_get_contents("php://input"));
		$this->Sendgrid->batchSaveEvents($input);
	}

	function info_get()
	{

	}

	function remove_get()
	{

	}

	/**
	 * @return array
	 */
	private function extract_numbers($number)
	{
		preg_match_all('!\d+!', $number, $matches);
		return implode("",$matches[0]);
	}

	/**
	 * @param $html
	 * @param $savedInfo
	 * @return mixed
	 */
	private function parseEmail($html)
	{
		$savedInfo = [
			"Group Name"=>"",
			"Confirmation #"=>"",
			"Golf Course Confirmation #"=>"",
			"Date"=>"",
			"Tee Time"=>"",
			"Golf Course"=>"",
			"Number of Players"=>"",
			"Total Green Fees"=>0,
			"Grand Total"=>0,
			"Paid Online"=>0,
			"Due at Course"=>0,
			"TaxesAndFees"=>0,
			"trade"=>false,
			"cancel"=>false
		];

		if(preg_match('/cancellation request/',$html)){
			$savedInfo['cancel'] = true;
		}
		$crawler = new Crawler($html);
		$allTd = $crawler->filter('td');
		foreach ($allTd as $i => $node) {
			$value = trim($node->nodeValue);
			$value = rtrim($value, ":");
			if (isset($savedInfo[$value])) {
				$savedInfo[$value] = trim($allTd->getNode($i + 1)->nodeValue);
			} else if (preg_match('/Tee Time Notes:/', $value)) {
				$savedInfo["Notes"] = $value;
			} else if (preg_match('/Taxes/i', $value)) {
				$savedInfo['TaxesAndFees'] = $allTd->getNode($i + 1)->nodeValue;
			} else if (preg_match('/paid\s*in full online/i', $value)) {
				$savedInfo["trade"] = true;
			}
		}
		return $savedInfo;
	}

	/**
	 * @param $savedInfo
	 * @param $groupInfo
	 * @return array
	 */
	private function getCustomerFromReservation($savedInfo)
	{
		$groupInfo = explode(" - ",$savedInfo['Group Name']);
		preg_match("/^(.*?)\s(.*)\s-\s(.*)$/", trim($savedInfo['Group Name']), $groupInfo);
		if(empty($groupInfo[1])){
			preg_match("/^(.*?)\s(.*)$/", trim($savedInfo['Group Name']), $groupInfo);
		}
		$firstName = $groupInfo[1];
		$lastName = $groupInfo[2];
		if(isset($groupInfo[3]))
			$number = $groupInfo[3];
		else
			$number = "";


		$number = $this->extract_numbers($number);
		$this->load->model("customer");
		//$suggestions = $this->customer->get_search_suggestions($name);
		$suggestions = $this->customer->get_customer_search_suggestions($firstName . " " . $lastName);
		$selectedCustomer = null;
		$personId = null;
		foreach ($suggestions as $suggestion) {
			if ($this->extract_numbers($suggestion['phone_number']) == $number) {
				$personId = $suggestion['value'];
			}
		}

		if (empty($personId)) {
			//Create a customer
			$personData = [
				"first_name" => $firstName,
				"last_name" => $lastName,
				"phone_number" => $number,
			];
			$customerData = [
				"course_id" => $this->session->userdata('course_id')
			];
			$result = $this->customer->save($personData, $customerData);
			$personId = $customerData['person_id'];
			return array($firstName, $lastName, $personId);
		}
		return array($firstName, $lastName, $personId);
	}

	/**
	 * @param $savedInfo
	 * @return array
	 */
	private function createTeetime($teesheet_id,$savedInfo)
	{
		list($firstName, $lastName, $personId) = $this->getCustomerFromReservation($savedInfo);


		$this->load->model("Increment_adjustment");
		$this->load->model("teesheet");
		$this->load->model("teetime");

		$start_time = \Carbon\Carbon::parse($savedInfo['Date'] . " " . $savedInfo['Tee Time']);
		/**
		 * Save Tee time
		 */
		$destination_start_time = $start_time->format('YmdHi') - 1000000;
		$destination_end_time = $this->Increment_adjustment->get_slot_time($destination_start_time + 1000000, $teesheet_id, 1, 1);

		$teetime_id = $this->teesheet->generateID('tt');
		$event_data = array(
			'TTID' => $teetime_id,
			'teesheet_id' => $teesheet_id,
			'start' => $destination_start_time,
			'end' => $destination_end_time,
			'side' => 'front',
			'allDay' => 'false',
			'duration' => 1
		);
		if ($savedInfo['trade']) {
			$event_data['details'] = "Paid in Full - Golf Now Trade
{$savedInfo['Notes']}";
			$event_data['paid_player_count'] = $savedInfo['Number of Players'];
		} else {
			$event_data['details'] = "Due at Course: {$savedInfo['Due at Course']}
GolfNow Confirmation #: {$savedInfo['Confirmation #']}
Total Green Fees: {$savedInfo['Total Green Fees']}
Taxes: {$savedInfo['TaxesAndFees']}
Grand Total: {$savedInfo['Grand Total']}
Paid Online: {$savedInfo['Paid Online']}
{$savedInfo['Notes']}";
			$event_data['paid_player_count'] = 0;
		}


		$event_data['holes'] = 18;
		$event_data['player_count'] = $savedInfo['Number of Players'];
		$event_data['title'] = $lastName;
		$event_data['type'] = 'teetime';
		$event_data['booking_source'] = 'GolfNow';
		$event_data['send_confirmation'] = 1;
		$event_data['person_id'] = $personId;
		$event_data['person_name'] = $lastName . ", " . $firstName;
		if(!$this->teetime->save($event_data,$teetime_id)){
			return false;
		}

		return $teetime_id;
	}

}