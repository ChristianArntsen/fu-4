<?php
class Report_Columns extends CI_Controller {

    function __construct(){

        parent::__construct();
        $this->load->model('Report');
        $this->load->model('Report_column');
        $this->CI = & get_instance();
    }

    public function create(){
    }

    public function show($id){
    }

    public function index(){
        $report_type = $this->input->get('report_type', null);
        if(isset($report_type)){
            $reports = $this->Report_column->get_all($report_type);
        } else {
            $reports = [];
        }

        print json_encode($reports);
    }

    public function update($id){
        echo "update";
    }

    public function delete($id){
        echo "delete";
    }

}
