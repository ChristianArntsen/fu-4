<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Sales extends REST_Controller {
	
	function __construct(){
		parent::__construct();
		$this->authenticate();

		$this->load->model('v2/sale_model');
		$this->load->model('v2/item_model');
		$this->load->model('Employee');

		$this->sale_id = false;
	}
	
	// Get list of sales
	function index_get($sale_id = null, $param1 = null, $param2 = null){

		$params = $this->input->get();

		if($param1=='tip-share'){
			// should really belong in employee...
			$transaction = new \fu\tips\TipTransaction($this->session->userdata('course_id'));
			$result = $transaction->tip_share_transaction($params['date'],$params['tzo']);
			return $this->response($result,200);
		}
		if($param1=='tip-transaction'){
			$transaction = new \fu\tips\TipTransaction($this->session->userdata('course_id'));
			if($param2) {
				$transaction->set_transaction_id($param2);
				$result = $transaction->get($param2);
				// single result expected
				$result = $result[0];
				$result['journal_entries'] = $transaction->get_journal_entries();
			}
			else {
				$result = $transaction->get($params);
				if(isset($params['sale_id'])&&isset($params['notes'])){
					// single result expected
					$result = $result[0];
					$transaction->set_transaction_id($result['transaction_id']);
					$result['journal_entries'] = $transaction->get_journal_entries();
				}
			}
			return $this->response($result,200);
		}
		$sales = $this->sale_model->get($params);

		$this->response($sales, 200);
	}
	
	// Post to sale (create new sale, add items to sale, etc)
	function index_post($sale_id = null, $param1 = null, $param2 = null){
		
		if($param1 == 'email_receipt'){
			return $this->email_receipt($sale_id);
		
		}else if($param1 == 'payments'){
			return $this->payments_post($sale_id, $param2);
		}		
		
		$this->response(null, 405);
	}
	
	// Email a sales receipt
	private function email_receipt($sale_id){
		$data = $this->post();
		$email = $data['email'];

		$response = $this->sale_model->email_receipt($sale_id, $email);
		
		if($response){
			$this->response(array('success' => true), 200);
			return true;	
		}else{
			$this->response(array('success' => false), 500);
			return false;	
		}
	}
	
	// Edit a sale (comments, time, employe, etc)
	function index_put($sale_id = null, $param1 = null, $param2 = null){
		$course_id =$this->session->userdata('course_id');
		
		if($param1 == 'payments'){
			return $this->payments_post($sale_id, $param2);
		}elseif($param1=='tip-transaction' || $param1=='tip-share'){
			$transaction = new \fu\tips\TipTransaction($course_id);
			$params = $this->request->body;
			$transaction->process_edited_transaction($course_id,$params);
			$this->response(array('success' => true,'data'=>$params), 200);
			return;
		}
		
		$sale_data = $this->request->body;
		if(isset($sale_data['employee_id']) && empty($sale_data['employee_id'])){
			unset($sale_data['employee_id']);
		}
		$response = $this->sale_model->save($sale_id, $sale_data); 
		
		if($response){
			$this->response(array('success' => true), 200);
		}else{
			$this->response(array('success' => false), 500);
		}
	}
	
	// Delete a sale
	function index_delete($sale_id = null){
        if(!empty($_SERVER['HTTP_REFUND_COMMENT'])){
	        $params = array(
		        'refund_reason' => $_SERVER['HTTP_REFUND_REASON'],
		        'refund_comment' => $_SERVER['HTTP_REFUND_COMMENT']
	        );
        } else {
	        $headers = apache_request_headers();
	        $params = [
	            "refund_reason" => $headers['refund_reason'],
	            "refund_comment" => $headers['refund_comment']
	        ];
        }

		$response = $this->sale_model->delete($sale_id);
        $this->sale_model->save_refund_details($sale_id, $params);
		
		if($response){
			$this->response(array('success' => true), 200);
		}else{
			$this->response(array('success' => false), 500);
		}		
	}
	
	// Apply a payment to an existing sale (for partial refunds or tips)
	private function payments_post($sale_id, $cc_invoice_id = false){
        $params = $this->request->body;

		if(!empty($cc_invoice_id)){
			$response = $this->sale_model->refund_credit_card_payment($cc_invoice_id, $params['refund_amount']);

			if(!$response){
				$msg = 'Error refunding credit card';
				if(!empty($this->sale_model->msg)){
					$msg = $this->sale_model->msg;
				}
				$this->response(array('success' => false, 'msg' => $msg), 500);
				return false;
			}

		}else{
			$response = $this->sale_model->save_payment($sale_id, $params);
		}

        $this->sale_model->save_refund_details($sale_id, $params);

		if($response){
			$this->response($response, 200);
		}else{
			$this->response(array('success' => false,'error'=>$this->sale_model->get_last_error()?$this->sale_model->get_last_error():'api/sales->payment_post Payment not saved.'), 500);
		}
					
		return true;
	}

	function cash_sales_total_get(){
		
		$this->load->model('v2/Sale_model');
		$start = $this->input->get('start');
		$end = $this->input->get('end');
		$employee_id = $this->input->get('employee_id');
		$terminal_id = $this->input->get('terminal_id');
		$persist = ($this->input->get('persist') == 1);
		
		if(empty($start) || $start == '0000-00-00 00:00:00' || 
			empty($end) || $end == '0000-00-00 00:00:00'){
			$this->response(array('success' => false, 'msg' => "'start' and 'end' dates required"), 400);	
			return false;
		}
		
		$totals = $this->Sale_model->get_register_log_payment_totals($start, $end, $employee_id, $terminal_id, $persist);
		$this->response(array('cash_sales_amount' => (float) $totals['cash']), 200);
	}
}
