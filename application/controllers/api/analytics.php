<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Analytics extends REST_Controller {

    function __construct(){

        parent::__construct();
        $this->sale_id = false;
    }

    function index_get($id){
    }
    function list_get(){
    } 
    function index_post(){
    }
    function index_put($id){
    }

    function sales_post(){
        $json = file_get_contents('php://input');
        $input = json_decode($json,true);
        try{
            $report = new fu\reports\SalesReport($input);
            $data = $report->getData();

            if(isset($input['summary'])){
                $input['columns'] = $input['summary'];
                $input['group'] = [];
                $report = new fu\reports\SalesReport($input);
                $summary = $report->getData();
                print json_encode([
                    "data"=>$data,
                    "summary"=>$summary
                ]);
            }
        } catch(Exception $e){
            print json_encode(["success"=>false,"message"=>$e->getMessage()]);
        }
    }

    function run_post(){

    }

}
