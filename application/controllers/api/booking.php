<?php
// Booking API to provide REST API functionality for online booking,
// Used internally (but could be used externally) for Backbone JS
require_once(APPPATH . 'libraries/REST_Controller.php');

use Carbon\Carbon;

class Booking extends REST_Controller
{
	/** @var  Teesheet */
	public $teesheet;


	function __construct()
	{
		parent::__construct();
		$this->load->model('teesheet');
		$this->load->model('schedule');
		$this->load->model('teetime');
		$this->load->model('course');
		$this->load->model('user');
		$this->load->helper('array');

		header('Content-type: application/json');
	}

	private function _get_course($course_id)
	{

		if (empty($course_id)) {
			return false;
		}

		$this->course_id = (int)$course_id;
		$course_info = (array)$this->course->get_info($course_id);
		$course_info = $this->_filter_course_data($course_info);

		if (empty($course_id)) {
			$this->response(array('success' => false, 'msg' => '{course_id} is required'), 400);
			return false;
		}

		$this->course_info = $course_info;
		$this->session->set_userdata('course_id', $course_id);
		$this->session->set_userdata('course_name', $course_info['name']);
		$this->session->set_userdata('reservation_email', $course_info['reservation_email']);
		$this->session->set_userdata('course_phone', $course_info['phone']);
		$this->session->set_userdata('course_email', $course_info['email']);

		if ($course_info['timezone']) {
			date_default_timezone_set($course_info['timezone']);
		}

		return $course_info;
	}

	private function _get_schedule($schedule_id)
	{

		if (empty($schedule_id)) {
			$this->response(array('success' => false, 'msg' => 'Schedule ID is required'), 400);
			return false;
		}
		$this->schedule_id = (int)$schedule_id;

		$schedule_data = (array)$this->teesheet->get_info($schedule_id);
		$this->schedule = $schedule_data;

		// If selected schedule is deleted or does not have online booking enabled, return error
		if (empty($schedule_data) || $schedule_data['online_booking'] == 0 || $schedule_data['deleted'] == 1) {
			$this->response(array('success' => false, 'msg' => 'Selected schedule does not exist, or is not available for booking'), 400);
			die();
			return false;
		}

		$this->session->set_userdata('holes', $schedule_data['holes']);
		$this->session->set_userdata('teesheet_id', $schedule_data['teesheet_id']);
		$this->session->set_userdata('schedule_id', $schedule_data['teesheet_id']);
		$this->session->set_userdata('days_out', $schedule_data['days_out']);
		$this->session->set_userdata('online_open_time', $schedule_data['online_open_time']);
		$this->session->set_userdata('online_close_time', $schedule_data['online_close_time']);
		$this->session->set_userdata('days_in_booking_window', $schedule_data['days_in_booking_window']);
		$this->session->set_userdata('increment', $schedule_data['increment']);

		return $schedule_data;
	}

	private function _get_user($user_id)
	{

		// Gather up users data to be sent back in response
		$this->load->model('customer');
		$this->load->model('invoice');
		$this->load->model('Account_transactions');
		$course_id = $this->session->userdata('course_id');
		$this->load->model('Customer_merge');
		$primary_person_id = $this->Customer_merge->get_primary($user_id);

		if (!empty($course_id) && !$this->session->userdata('group_id')) {
			$user_data = (array)$this->customer->get_info($primary_person_id, $course_id);
			//$user_data['online_user_data'] = $user_id == $primary_person_id ? $user_data : $this->customer->get_info($user_id, $course_id);
			$course_id_filter = (int)$course_id;

        } else {
			$user_data = $this->user->get_info($user_id);
			$user_data['member'] = 0;
			$user_data['account_number'] = '';
			$user_data['account_balance'] = '';
			$user_data['account_limit'] = 0;
			$user_data['invoice_balance'] = 0;
			$user_data['use_loyalty'] = 0;
			$user_data['loyalty_points'] = 0;

			$course_id_filter = $this->session->userdata('group_course_ids');
		}

		if (isset($user_data['password'])) {
			unset($user_data['password']);
		}

		$user_data['user_id'] = (int)$primary_person_id;
		$user_data['logged_in'] = true;
		$user_data['credit_cards'] = [];
		$user_data['reservations'] = [];
		$user_data['account_transactions'] = [];
		$user_data['account_transactions_total'] = 0;
		$user_data['gift_cards'] = [];
		$user_data['rainchecks'] = [];
		$user_data['invoices'] = [];

		// Get user's current reservations
		$user_data['reservations'] = $this->user->get_teetimes($user_data['user_id'], 'upcoming', $course_id_filter);

		// Get course specific information
		if (!empty($course_id)) {
			$this->load->model('Customer_credit_card');
			$this->load->model('v2/Raincheck_model');
			$user_data['credit_cards'] = $this->Customer_credit_card->get($user_data['user_id'], false, $course_id);

			// Get user's account transactions
			$transactions = $this->user->get_account_transactions($user_data['user_id'], $course_id);
			$user_data['account_transactions'] = $transactions;
			$user_data['account_transactions_total'] = (int)$this->user->total_transactions;

			// Get user's gift cards
			$user_data['gift_cards'] = $this->user->get_giftcards($user_data['user_id'], $course_id);

			$invoices = $this->invoice->get_all($user_data['user_id']);
			$user_data['invoices'] = $invoices->result_array();

			$user_data['rainchecks'] = $this->Raincheck_model->get([
				'person_id' => $user_data['user_id']
			], $course_id);

			$user_data['minimum_charge_transactions'] = $this->user->get_minimum_charge_transactions($user_data['user_id'], $course_id);
			$user_data['minimum_charge_totals'] = $this->user->get_minimum_charge_totals($user_data['user_id'], $course_id);
			$this->load->model("Booking_class");
			$user_data['booking_class_ids'] = $this->Booking_class->get_customer_booking_class_ids($user_data['user_id']);
		}

		if (isset($user_data['password'])) {
			unset($user_data['password']);
		}
		return $user_data;
	}

	private function _filter_course_data($course_data)
	{
		$validParams = array(
			'course_id',
			'active',
			'area_id',
			'name',
			'address',
			'city',
			'state',
			'postal',
			'zip',
			'country',
			'area_code',
			'timezone',
			'latitude_centroid',
			'longitude_centrod',
			'phone',
			'online_booking',
			'online_booking_protected',
			'booking_rules',
			'email',
			'no_show_policy',
			'include_tax_online_booking',
            'unit_price_includes_tax',
			'foreup_discount_percent',
			'close_time',
			'open_time',
			'seasonal_pricing',
			'reservation_email'
		);

		return elements($validParams, $course_data, '');
	}

	private function _validate_booking_date($date)
	{

		if (empty($this->schedule)) {
			return true;
		}
		$booking_date = clone $date;
		$booking_date->setTime(0, 0, 0);

		$timezone = null;
		if (!empty($this->course_info['timezone'])) {
			$timezone = new DateTimeZone($this->course_info['timezone']);
		}

		$curdate = new DateTime(null, $timezone);
		$curdate->setTime(0, 0, 0);

		$days_out = (int)$this->schedule['days_in_booking_window'];
		$booking_class = $this->session->userdata('booking_class_info');
		if (!empty($booking_class)) {
			$days_out = (int)$booking_class->days_in_booking_window;
		}
		$days_out--;

		$max_date = new DateTime(null, $timezone);
		$max_date->setTime(0, 0, 0);
		$max_date->modify("+{$days_out} days");

		if ($booking_date > $max_date || $booking_date < $curdate) {
			return false;
		}
		return true;
	}

	function index_get()
	{
		$this->response(null, 403);
		return true;
	}

	function booking_class_post()
	{

		$set_booking_class_id = (int)$this->input->post('booking_class_id');

		if (empty($set_booking_class_id)) {
			$this->session->set_userdata('booking_class_id', false);
			$this->session->set_userdata('booking_class_info', false);

		} else {
			$booking_class_info = $this->Booking_class->get_info($set_booking_class_id, $this->schedule_id);

			if (empty($booking_class_info) || $booking_class_info->teesheet_id != $this->schedule_id) {
				$this->response(array('success' => false, 'msg' => 'Selected booking class is invalid'), 400);
				return false;
			}

			$this->session->set_userdata('booking_class_id', (int)$set_booking_class_id);
			$this->session->set_userdata('booking_class_info', $booking_class_info);
		}

		$this->response(array('success' => true));
	}

	// GET Information about a specific course
	function courses_get($course_id, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		if(!is_numeric($course_id)){
			$this->response(array('success' => false, 'msg' => 'Course Id not valid.'), 500);
		}
		$course_info = $this->_get_course($course_id);

		// Route schedules
		if ($method == 'schedules') {
			$this->schedules_get($recordId, $method2);
			return true;
		}

		// Route login
		if ($method == 'login') {
			$this->courses_login($recordId, $method2);
			return true;
		}

		if ($method == 'promo_codes') {
			$this->promo_codes_get();
			return true;
		}

		if ($method == 'users' && $recordId == 'account_transactions') {
			$this->users_account_transactions_get();
			return true;
		}

		if ($recordId == 'invoices') {
			$this->users_invoices_get($method2);
			return true;
		}

		$teesheets = $this->teesheet->get_teesheets($this->course_id, array('online_only' => 1));
		$course_info['schedules'] = $teesheets;

		$this->response($course_info);
	}

	// POST Log in to a specific course, or schedule a teetime on a course schedule
	function courses_post($course_id, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$course_info = $this->_get_course($course_id);

		// Route schedules
		if ($method == 'schedules') {
			$this->schedules_post($recordId, $method2);
			return true;
		}

		// Route login
		if ($method == 'login') {
			$this->courses_login();
			return true;
		}

		// Route user signup
		if ($method == 'users') {

			if ($recordId == 'logout') {
				$this->users_logout();
			}
			if ($recordId == 'login') {
				$this->users_login();
			}
			if ($recordId == 'send_password_reset') {
				return $this->users_send_password_reset();
			}
			if ($recordId == 'reset_password') {
				return $this->users_reset_password();
			}
			if ($recordId == 'credit_cards') {
				$this->users_credit_cards_post();
			}
			if ($recordId == 'reservations') {
				$this->users_reservations_post();
			}
			if ($recordId == 'invoices') {
				$this->users_invoices_post($method2, $recordId2);
				return true;
			}

			$this->users_post();
			return true;
		}

		$this->response($course_info);
	}

	function courses_delete($course_id, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$course_info = $this->_get_course($course_id);

		// Route user signup
		if ($method == 'users') {

			if ($recordId == 'reservations') {
				$this->users_reservations_delete($method2);
				return true;
			}
			if ($recordId == 'credit_cards') {
				$this->users_credit_cards_delete($method2);
				return true;
			}
		}

		$this->response(null, 405);
	}

	function courses_put($course_id, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$course_info = $this->_get_course($course_id);

		// Route user profile update
		if ($method == 'users') {
			$this->users_put($method2);
			return true;
		}

		$this->response(null, 405);
	}

	// Logs a user into a specific course, if the user is not yet on
	// the courses customer list, they will be added
	function courses_login()
	{

	}

	function promo_codes_get()
	{

		$this->load->model('online_booking');
		$this->load->model('user');

		$course_id = $this->course_id;
		$players = $this->input->get_post('players');
		$holes = $this->input->get_post('holes');
		$carts = $this->input->get_post('carts');
		$time = $this->input->get_post('time');
		$schedule_id = $this->input->get_post('schedule_id');
		$promo_code = $this->input->get_post('promo_code');
		$customer_id = $this->session->userdata('customer_id');
		$booking_class_id = $this->input->get_post('booking_class_id');

		if ($carts === false || $carts === 'false' || $carts === '0') {
			$carts = false;
		} else {
			$carts = true;
		}

		if ($booking_class_id === false || $booking_class_id === 'false' || $booking_class_id === '0') {
			$booking_class_id = false;
		} else {
			$booking_class_id = (int)$booking_class_id;
		}

		$this->load->model('online_booking');
		$price_class_id = $this->online_booking->get_price_class($booking_class_id, $customer_id);

		// Parse reservation time, if time failed to parse throw error
		$time = DateTime::createFromFormat('Y-m-d H:i', $time);
		$this->load->model('Promotion');
		$this->Promotion->course_id = $course_id;
		$this->Promotion->seasonal_pricing = ($this->course_info['seasonal_pricing'] == 1);
		$coupon = $this->Promotion->get_coupon_by_online_code($promo_code, $time, $course_id);

		if (empty($coupon)) {
			$this->response(array('success' => false, 'msg' => 'Promo code is invalid or does not apply', 'error_type' => 'promo_code'), 400);
			return false;
		}

		// Build a new shopping cart to determine amount of discount offered by promo code
		$this->online_booking->load_cart(
			$course_id, 
			$schedule_id, 
			$time->format('Y-m-d H:i'), 
			$holes, 
			$players, 
			$carts,
			$customer_id,
			$price_class_id, 
			$promo_code
		);

		$cart_data = $this->Cart_model->get_totals();
		$promo_discount = $cart_data['total'] - $cart_data['total_due'];

		$response = array('success' => true, 'promo_code' => $promo_code, 'promo_discount' => $promo_discount);
		$this->response($response, 200);
	}

	// GET List of teesheets from a specific course
	function schedules_get($teesheet_id = null, $method = null)
	{

		// Route times
		if (!empty($teesheet_id) && $method == 'times') {
			$this->_get_schedule($teesheet_id);
			$this->times_get();
			return true;
		}

		$teesheets = $this->teesheet->get_teesheets($this->course_id, array('online_only' => 1));
		$this->response($teesheets);
	}

	// POST Schedules, only allow reserving of teetimes, nothing else
	function schedules_post($teesheet_id = null, $method = null)
	{

		$this->_get_schedule($teesheet_id);

		// Route times
		if ($method == 'times') {
			$this->schedules_times_post();
			return true;
		}

		// If trying to POST to root schedules URL, throw error
		$this->response(null, 405);
	}

	//Get available teetimes with extra advanced options
	function times_advanced_get()
	{
		$date = $this->input->get('date');
		$holes = (int)$this->input->get('holes',18);
		$available_spots = $this->input->get('players');
		$schedule_id = $this->input->get('schedule_id');
		$price_class_id = $this->input->get('price_class_id');
		$start_time = $this->input->get('start_time',0000);
		$end_time = $this->input->get('end_time',2300);
		$customer_id = $this->input->get('customer_id');

		/** teesheet $this->teesheets */
		$teesheet = $this->teesheet->get_info($schedule_id);
		if (!empty($this->course_info) || $teesheet->course_id != $this->session->userdata("course_id")) {
			$this->response(array('success' => false, 'msg' => 'You must log in to view tee times for this course'), 401);
			return false;
		}


		$this->load->model('Booking_class');
		$this->load->model('Online_Booking');
		$this->Online_Booking->set_filters($holes, $available_spots, "all", false, $date);
		$this->Online_Booking->setAdminMode(true);

		$customer_price_class = null;
		if(!empty($customer_id)){
			$customer_info = (array)$this->Customer->get_info($customer_id, $this->session->userdata('course_id'));
			if (!empty($customer_info['price_class'])) {
				$customer_price_class = $customer_info['price_class'];
			}
		}
		if(!empty($price_class_id)){
			$customer_price_class = $price_class_id;
		}

		$times = $this->Online_Booking->get_times(array(
			'teesheet_id' => $schedule_id,
			'start_time'=>$start_time,
			'end_time'=>$end_time,
			'customer_price_class'=>$customer_price_class
		));

		if(empty($times) && $this->Online_Booking->message){
			header("X-Message: ".$this->Online_Booking->message);
		}

		$this->response($times, 200);
		return false;
	}

	// GET Available teetimes from a specific schedule/teesheet
	function times_get()
	{

		if (!empty($this->course_info) && $this->course_info['online_booking_protected'] == 1 && !$this->session->userdata('customer_id')) {
			$this->response(array('success' => false, 'msg' => 'You must log in to view tee times for this course'), 401);
			return false;
		}
		$this->load->model('Booking_class');
		$this->load->model('Online_Booking');

		$time_range = $this->input->get('time');
		$course_id = $this->input->get('course_id');
		$holes = (int)$this->input->get('holes');
		$available_spots = $this->input->get('players');
		$booking_class_id = $this->input->get('booking_class');
		$schedule_id = $this->input->get('schedule_id');
        $schedule_ids = $this->input->get('schedule_ids');
		$group_id = $this->input->get('group_id');
		$specials_only = $this->input->get('specials_only');

		if (empty($schedule_id)) {
			$schedule_id = false;
		}

		if (empty($specials_only) || $specials_only == 0) {
			$specials_only = false;
		}
		if (empty($schedule_id) && empty($group_id)) {
			$this->response(array('success' => false, 'msg' => 'Group ID or Schedule ID required to get tee times'), 400);
			return false;
		}

		if (!empty($group_id)) {
			$this->schedule = false;
			$this->course_id = false;
			$this->group_id = $group_id;
		}

		if ($booking_class_id === 'false' || $booking_class_id === false) {
			$booking_class_id = false;
		}

		$this->session->unset_userdata('booking_class_id');
		$this->session->unset_userdata('booking_class_info');
		$this->booking_class = false;

        $this->session->set_userdata('time_booking_class_id', $booking_class_id);

		if (!$this->Online_Booking->has_booking_class_permission($booking_class_id, $this->session->userdata('customer_id'))) {
			$this->response(array('success' => false, 'msg' => 'You do not have permissions to use this booking class. Please select another one to see available tee times.'), 401);
			return false;
		}

		// If customer is logged in, pass price class into tee time API to override tee time prices
		$customer_price_class = false;
		$customer_taxable = true;
		if ($this->session->userdata('customer_id') && $this->session->userdata('course_id')) {
			$customer_info = (array)$this->Customer->get_info($this->session->userdata('customer_id'), $this->session->userdata('course_id'));
			if (!empty($customer_info['price_class'])) {
				$customer_price_class = $customer_info['price_class'];
			}
			$customer_taxable = ($customer_info['taxable'] == 1);
		}
		
		// Subtract selected date from current date for a day number
		$date = $this->input->get('date');
		if (empty($date)) {
			$date = date('m-d-Y');
		}
		$date = DateTime::createFromFormat('m#d#Y', $date);

		$this->Online_Booking->set_filters($holes, $available_spots, $time_range, $specials_only, $date);
		$times = $this->Online_Booking->get_times(array(
			'group_id' => $group_id,
			'teesheet_id' => $schedule_id,
            'teesheet_ids' => $schedule_ids,
			'booking_class_id' => $booking_class_id,
			'customer_price_class' => $customer_price_class,
			'customer_taxable' => $customer_taxable
		));

		if(empty($times) && $this->Online_Booking->message){
			header("X-Message: ".$this->Online_Booking->message);
		}

		$this->response($times, 200);
		return false;
	}

	function schedules_times_post()
	{
		$this->response(null, 405);
	}

	function users_get($userId)
	{
		$this->response(null, 405);
	}

	// POST Register as a new user, or login as an existing user
	function users_post($method = null)
	{

		if ($method == 'login') {
			$this->users_login();
			return true;
		}

		if ($method == 'logout') {
			$this->users_logout();
			return true;
		}

		if ($method == 'credit_cards') {
			$this->users_credit_cards_post();
			return true;
		}

		if ($method == 'reservations') {
			$this->users_reservations_post();
			return true;
		}

		if ($method == 'send_password_reset') {
			$this->users_send_password_reset();
			return true;
		}

		if ($method == 'reset_password') {
			$this->users_reset_password();
			return true;
		}

		$this->load->model('person');

		$first_name = $this->request->body['first_name'];
		$last_name = $this->request->body['last_name'];
		$phone = $this->request->body['phone'];
		$email = $this->request->body['email'];
		$password = $this->request->body['password'];

		if (empty($first_name)) {
			$this->response(array('success' => false, 'msg' => 'First name is required'), 400);
			return false;
		}
		if (empty($last_name)) {
			$this->response(array('success' => false, 'msg' => 'Last name is required'), 400);
			return false;
		}
		if (empty($phone)) {
			$this->response(array('success' => false, 'msg' => 'Phone number is required'), 400);
			return false;
		}
		if (empty($email)) {
			$this->response(array('success' => false, 'msg' => 'Email is required'), 400);
			return false;
		}
		if (empty($password)) {
			$this->response(array('success' => false, 'msg' => 'Password is required'), 400);
			return false;
		}
		if (strlen($password) < 6) {
			$this->response(array('success' => false, 'msg' => 'Password must be at least 6 characters'), 400);
			return false;
		}

		if ($this->user->username_exists($email)) {
			$this->response(array('success' => false, 'msg' => lang('customers_error_registering')), 409);
			return false;
		}

		$person_data = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'phone_number' => $phone
		);

		$user_data = array(
			'email' => $email,
			'password' => md5($password)
		);

		$person_id = false;

		$this->person->save_person($person_data);
		$person_id = $person_data['person_id'];
		$saved = $this->user->save($person_id, $user_data);

		if ($saved) {
			$logged_in = $this->user->login($email, $password);
			$course_id = $this->session->userdata('course_id');

			// Add user as a customer to this course (if they are not already)
			$this->user->add_to_course($person_id, $course_id);

			// Get user data to send back with success message
			$this->load->model('Customer_merge');
			$primary_person_id = $this->Customer_merge->get_primary($person_id);
			$user_data = $this->_get_user($primary_person_id);
			$user_data['logged_in'] = (bool)$logged_in;
			$user_data['username'] = $email;

			$this->response($user_data);

		} else {
			$this->response(array('success' => false, 'msg' => 'Error completing registration'), 405);
		}

		return true;
	}

	// Updates a users profile
	function users_put($method = null)
	{

        if ($method == 'reservations') {
            $this->users_reservations_put();
            return true;
        }

		$this->load->model('person');
		$this->load->model('user');
		$this->load->model('customer');

		if (!$this->session->userdata('customer_id')) {
			$this->response(array('success' => false, 'msg' => 'Please login to edit your account information'), 401);
			return false;
		}
		$customer_id = $this->session->userdata('customer_id');
		$online_customer_id = $this->session->userdata('online_customer_id');
		$current_customer_info = (array)$this->customer->get_info($customer_id);
		$current_username = $this->user->get_username($online_customer_id);

		if (empty($this->request->body['username'])) {

			$first_name = $this->request->body['first_name'];
			$last_name = $this->request->body['last_name'];
			$phone = $this->request->body['phone'];
			$address = $this->request->body['address'];
			$city = $this->request->body['city'];
			$state = $this->request->body['state'];
			$zip = $this->request->body['zip'];
			$birthday = $this->request->body['birthday'];
			$email = $this->request->body['email'];

			$this->load->helper('email');

			if (empty($first_name)) {
				$this->response(array('success' => false, 'msg' => 'First name is required'), 400);
				return false;
			}
			if (empty($last_name)) {
				$this->response(array('success' => false, 'msg' => 'Last name is required'), 400);
				return false;
			}
			if (empty($phone)) {
				$this->response(array('success' => false, 'msg' => 'Phone number is required'), 400);
				return false;
			}
			if (empty($email)) {
				$this->response(array('success' => false, 'msg' => 'Email is required'), 400);
				return false;
			}
			if (!valid_email($email)) {
				$this->response(array('success' => false, 'msg' => 'Email is invalid'), 400);
				return false;
			}

			if (!empty($birthday)) {
				$birthday = date('Y-m-d', strtotime($birthday));
			} else {
				$birthday = '0000-00-00';
			}

			$person_data = array(
				'first_name' => $first_name,
				'last_name' => $last_name,
				'phone_number' => $phone,
				'address_1' => $address,
				'city' => $city,
				'state' => $state,
				'zip' => $zip,
				'birthday' => $birthday,
				'email' => $email
			);
			$saved = $this->person->save_person($person_data, $customer_id);
			if ($online_customer_id != $customer_id) {
				$this->person->save_person($person_data, $online_customer_id);
			}
		} else {

			$username = $this->request->body['username'];
			$password = $this->request->body['password'];
			$current_password = $this->request->body['current_password'];

			if (empty($username)) {
				$this->response(array('success' => false, 'msg' => 'Username is required'), 400);
				return false;
			}
			if ($username != $current_username && $this->user->username_exists($username)) {
				$this->response(array('success' => false, 'msg' => 'That username is taken, please use a different one'), 400);
				return false;
			}

			$user_data = array();
			$user_data['email'] = $username;

			// If user is resetting their password
			if (!empty($current_password)) {

				// If password is too short, return error
				if (strlen($password) < 6) {
					$this->response(array('success' => false, 'msg' => 'New password must be at least 6 characters'), 400);
					return false;
				}

				// If their current password does not match, return error
				if (!$this->user->login($current_username, $current_password)) {
					$this->response(array('success' => false, 'msg' => 'Current password does not match'), 400);
					return false;
				}

				$user_data['password'] = md5($password);
			}

			$saved = $this->user->save($online_customer_id, $user_data);
		}

		if ($saved) {
			$this->response(array('success' => true), 200);
		} else {
			$this->response(array('success' => false, 'msg' => 'Error saving profile'), 400);
		}

		return true;
	}

	// POST Handle user login
	function users_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$booking_class_id = $this->input->post('booking_class_id');

		$logged_in = $this->user->login($username, $password);

		// If login failed, return error
		if (!$logged_in) {
			$this->response(array('success' => false, 'msg' => 'Username or password is invalid'), 401);
			return false;
		}

		// If user is attempting to log in through booking class,
		// make sure they have permission
		$user = $this->user->get_by_username($username);
        $course_id = $this->session->userdata('course_id');


		$this->load->model('Online_Booking');
        if (!$this->Online_Booking->has_booking_class_permission($booking_class_id, $user->person_id)) {
            $this->session->unset_userdata('customer_id');
            $this->response(array('success' => false, 'msg' => "You do not have permission to use this booking class. Please change booking class to see available tee times."), 401);
			return false;
        }

        $customer_id = $this->session->userdata('customer_id');

        // Add user as a customer to this course (if they are not already)
        $this->user->add_to_course($customer_id, $course_id);

        // Gather up users data to be sent back in response
        $user_data = $this->_get_user($customer_id);
		$this->response($user_data);
	}

	function users_logout()
	{
		$this->session->unset_userdata('customer_id');
		$this->response(array('msg' => 'You have been logged out successfully', 'success' => true), 200);
	}

	// Send email to user to reset their password
	function users_send_password_reset()
	{

		$this->load->model('User');
		$this->load->library('encrypt');
		$username = $this->input->post('username');
        $course_id = $this->input->post('course_id');
		$response = array('success' => false);

		$data = array();
		if (!empty($this->course_info)) {
			$data['course'] = (object)$this->course_info;
		} else {
			$data['course'] = false;
		}

		if (!$username) {
			$response['msg'] = 'Username is required';
			$this->response($response, 400);
			return false;
		}
		$customer = $this->User->get_by_username($username);

		if (!$customer) {
			$response['msg'] = 'No account found with that username';
			$this->response($response, 400);
			return false;
		}

		$email = (filter_var($customer->username, FILTER_VALIDATE_EMAIL) !== false) ? $customer->username : $customer->email;

		if (!$email) {
			if ($data['course']) {
				$response['msg'] = 'No email found in your account, please contact ' . $data['course']->name . ' for assitance';
			} else {
				$response['msg'] = 'No email found in your account, please contact the golf course for assistance';
			}
			$this->response($response, 400);
			return false;
		}

		$data['customer'] = $customer;
		$data['reset_key'] = base64url_encode($this->encrypt->encode($customer->person_id . '|' . (time() + (2 * 24 * 60 * 60))));
        $data['course_id'] = $course_id;
		$reset_data['success'] = true;

		send_sendgrid(
			$email,
			lang('login_reset_password'),
			$this->load->view("customers/login/reset_password_email", $data, true),
			'support@foreup.com',
			'ForeUP'
		);

		$this->response(array('success' => true), 200);
	}

	// Reset a user's password (using password reset key)
	function users_reset_password()
	{

		$this->load->library('encrypt');
		$data = $this->input->post();
		$response['success'] = false;

		if (!$data['key']) {
			$response['msg'] = 'Password reset key is required';
			$this->response($response, 400);
			return false;
		}

		if (!$data['password']) {
			$response['msg'] = 'Password is required';
			$this->response($response, 400);
			return false;
		}

		list($customer_id, $expire) = explode('|', $this->encrypt->decode(base64url_decode($data['key'])));

		if ($customer_id && $expire && (int)$expire > time()) {
			$valid_key = true;
		} else {
			$valid_key = false;
		}

		if (!$valid_key) {
			$response['msg'] = 'Password reset key is invalid or has expired';
			$this->response($response, 400);
			return false;
		}

		// If password is too short, return error
		if (strlen($data['password']) < 6) {
			$this->response(array('success' => false, 'msg' => 'New password must be at least 6 characters'), 400);
			return false;
		}

		// Save password to customer's profile
		$user_data = array();
		$user_data['password'] = md5($data['password']);
		$saved = $this->user->save($customer_id, $user_data);

		if ($saved) {
			$this->response(array('success' => true), 200);
		} else {
			$this->response(array('success' => false, 'msg' => 'Error saving password, please try again'), 500);
		}
	}

	function users_delete($method, $recordId)
	{
		if ($method == 'reservations') {
			$this->users_reservations_delete($recordId);
			return true;
		}

		if ($method == 'credit_cards') {
			$this->users_credit_cards_delete($recordId);
			return true;
		}
		$this->response(null, 405);
	}

	function users_credit_cards_post()
	{

		$customer_id = $this->session->userdata('customer_id');

		if (empty($customer_id)) {
			$this->response(array('success' => false, 'msg' => 'Please log in to save credit card'), 401);
			return false;
		}

		$number = $this->request->body['number'];
		$expiration = $this->request->body['expiration'];
		$type = $this->request->body['type'];
		$ets_id = $this->request->body['ets_id'];
		$ets_session_id = $this->request->body['ets_session_id'];
		$ets_customer_id = $this->request->body['ets_customer_id'];

		// Verify that the ETS ID is valid
		$this->load->library('Hosted_payments');
		$payment = new Hosted_payments();
		$payment->initialize($this->config->item('ets_key'));
		$response = $payment->set('action', 'verify')
			->set('sessionId', $ets_session_id)
			->set('customerId', $ets_customer_id)
			->set('transactionId', $ets_id)
			->send();

		// If ETS failed to validate token, return error	   
		if (empty($response) || !$response->status || $response->status != 'success') {
			$this->response(array('success' => false, 'msg' => 'Error validating credit card, please try again'), 405);
			return false;
		}

		switch ($type) {
			case 'MasterCard':
				$card_type = 'M/C';
				break;
			case 'Visa':
				$card_type = 'VISA';
				break;
			case 'Discover':
				$card_type = 'DCVR';
				break;
			case 'American Express':
				$card_type = 'AMEX';
				break;
			case 'Diners':
				$card_type = 'DINERS';
				break;
			case 'JCB':
				$card_type = 'JCB';
				break;
			default:
				$card_type = $type;
				break;
		}
		$number = str_replace('*', '', (string)$number);
		$expiration = DateTime::createFromFormat('my', (string)$expiration);

		$this->load->model('Customer_credit_card');

		// Save credit card data to customers profile
		$credit_card_data = array(
			'course_id' => $this->session->userdata('course_id'),
			'card_type' => $card_type,
			'masked_account' => $number,
			'cardholder_name' => '',
			'customer_id' => $customer_id,
			'token' => (string)$ets_customer_id,
			'expiration' => $expiration->format('Y-m-01')
		);
		$credit_card_id = $this->Customer_credit_card->save($credit_card_data);
		$credit_card_data['credit_card_id'] = (int)$credit_card_id;

		$this->response($credit_card_data, 200);
	}

	function users_credit_cards_delete($creditCardId)
	{

		if (!$creditCardId) {
			$this->response(array('success' => false, 'msg' => 'Credit card ID is required'), 400);
			return false;
		}
		$this->load->model('Customer_credit_card');
		$this->Customer_credit_card->delete_card((int)$this->session->userdata('customer_id'), (int)$creditCardId);
		$success = $this->Customer_credit_card->db->affected_rows();

		if ($success) {
			$this->response(array('success' => true, 'msg' => 'Credit card deleted'), 200);
		} else {
			$this->response(array('success' => false, 'msg' => 'Error deleting credit card'), 500);
		}
	}

	// Make a new reservation
	function users_reservations_post(){

		$this->load->model('online_booking');
		$this->load->model('Customer');
		$validate_only = (!empty($this->request->body['validate_only']))? true : false;

		// User must be logged in to register a teetime
		if (!$this->user->is_logged_in()) {
			$this->response(array('success' => false, 'msg' => 'Login is required'), 401);
			return false;
		}

		if (!empty($this->request->body['teesheet_id'])) {
			$schedule_id = $this->request->body['teesheet_id'];
		} else {
			$schedule_id = $this->request->body['schedule_id'];
		}

		$course_id = $this->request->body['course_id'];
		$customer_id = $this->session->userdata('customer_id');

		// Make sure course ID is found
		if (empty($course_id)) {
			$this->response(array('success' => false, 'msg' => 'Course ID is required'), 400);
			return false;
		}

		// Make sure schedule ID was passed
		if (empty($schedule_id)) {
			$this->response(array('success' => false, 'msg' => 'Schedule ID is required'), 400);
			return false;
		}
		$this->_get_course($course_id);

		$promo_id = false;
		$time = $this->request->body['time'];
		$total = $this->request->body['total'] ?? null;
		$players = (int)$this->request->body['players'];
		$carts = $this->request->body['carts'];
		$holes = (int)$this->request->body['holes'];
		$group_id = $this->request->body['group_id'];
		$booking_class_id = $this->request->body['booking_class_id'];
		$available_spots = $this->request->body['available_spots'];
		$promo_code = $this->request->body['promo_code'];
        $pending_reservation_id = isset($this->request->body['pending_reservation_id'])?$this->request->body['pending_reservation_id']:null;

		$player_list = false;
		if (!empty($this->request->body['player_list'])) {
			$player_list = $this->request->body['player_list'];
		}

		if (!empty($promo_code)) {
			$this->load->model('Promotion');
			$res_start = DateTime::createFromFormat('Y-m-d H:i', $time);
			$coupon = $this->Promotion->get_coupon_by_online_code($promo_code, $res_start, $course_id);
			if (!empty($coupon)) {
				$promo_id = $coupon['id'];
			}
		}

		if (empty($group_id)) {
			$group_id = null;
			$is_aggregate = false;
		} else {
			$is_aggregate = true;
		}

		$credit_card_id = 0;
		if (isset($this->request->body['credit_card_id'])) {
			$credit_card_id = (int)$this->request->body['credit_card_id'];
		}

		if ($carts === false || $carts === 'false' || $carts === 0) {
			$carts = 0;
		} else {
			$carts = $players;
		}

		// If we are only validating the tee time, spoof the credit card ID for now since the 
		// user will be inputing that at a later stage for real
		if($validate_only){
			$credit_card_id = true;
		}

		// Make sure reservation is still valid
		if (!$this->online_booking->validate_reservation(
				$schedule_id,
				$time,
				$holes,
				$players,
				$carts,
				$credit_card_id,
				$booking_class_id,
				$is_aggregate,
				$customer_id,
                $pending_reservation_id
			)
		){
			$this->response($this->online_booking->response, $this->online_booking->response_code);
			return false;
		}

		// If only validating the tee time, return a response now. 
		// Do not actually book the tee time
		if($validate_only){
			$this->response(['valid' => true], 200);
			return true;
		}

		$customer_info = $this->Customer->get_info($customer_id);
		if(empty($customer_info->first_name) && empty($customer_info->last_name)){
			$this->response(array(
				'success' => false,
				'msg' => 'We we\'re unable to reserve your time. Try again. <a href="#" onclick="App.data.times.refresh(); $(\'.bootstrap-growl\').hide(); return false;">Refresh Data</a>',
				'time' => date('Y-m-d H:i', strtotime($time)),
				'schedule_id' => $schedule_id
			), 409);
			return false;
		}
		$reservation_data = array(
			'time' => $time,
			'players' => $players,
			'holes' => $holes,
			'carts' => $carts,
			'schedule_id' => $schedule_id,
			'customer_id' => $customer_id,
			'customer' => (array)$customer_info,
			'price_class' => false,
			'course_id' => $course_id,
			'course' => $this->course_info,
			'group_id' => $group_id,
			'promo_id' => $promo_id,
			'available_spots' => $available_spots,
			'credit_card_id' => $credit_card_id,
			'player_list' => $player_list,
            'duration' => 1,
            'pending_reservation_id' => $pending_reservation_id,
			'total'=>$total//,
//            'booking_class_id' => $booking_class_id
		);

		if($this->config->item('enable_captcha_online')){
			$secret = "6LeQqCIUAAAAAILRhL-2hjUoMHACJax7hSeQZNfs";
			if(!empty($this->request->body['captchaid'])){
				$captchaId = $this->request->body['captchaid'];
				$recaptcha = new \ReCaptcha\ReCaptcha($secret);
				$resp = $recaptcha->verify($captchaId);
				if($resp->isSuccess()){
					$saved_reservation = $this->online_booking->book_time($reservation_data);
				}
			}

		} else {
			$saved_reservation = $this->online_booking->book_time($reservation_data);
		}




		if (empty($saved_reservation)) {
			$this->response(array(
				'success' => false,
				'msg' => 'Sorry, the tee time at ' .
					date('g:ia', strtotime($time)) .
					' is no longer available <a href="#" onclick="App.data.times.refresh(); $(\'.bootstrap-growl\').hide(); return false;">Refresh Data</a>',
				'time' => date('Y-m-d H:i', strtotime($time)),
				'schedule_id' => $schedule_id
			), 409);
			return false;
		}

		// Make sure this individual is saved as a customer for this course
		$this->user->add_to_course($customer_id, $course_id);

		$this->response($saved_reservation);
	}

	// Cancel a user's reservation
	function users_reservations_delete($teetime_id)
	{

		// User must be logged in to cancel a teetime
		if (!$this->user->is_logged_in()) {
			$this->response(array('success' => false, 'msg' => 'Login is required'), 401);
			return false;
		}
		$user_id = $this->session->userdata('customer_id');

		// User must be logged in to cancel a teetime
		if (empty($teetime_id)) {
			$this->response(array('success' => false, 'msg' => 'Teetime ID is required'), 400);
			return false;
		}
		$this->load->model('teetime');

		// Get information about teetime and course
		$reservation_info = (array)$this->teetime->get_info($teetime_id);
		$teesheet_info = (array)$this->teesheet->get_info($reservation_info['teesheet_id']);
		$this->_get_course($teesheet_info['course_id']);

		$success = $this->teetime->delete($teetime_id, $user_id, 'teetime-online user');
		$this->db->trans_complete();

		$email_data = array(
			'cancelled' => true,
			'person_id' => $user_id,
			'course_name' => $this->course_info['name'],
			'course_phone' => $this->course_info['phone'],
			'first_name' => $this->session->userdata('first_name'),
			'booked_date' => date('n/j/y', strtotime($reservation_info['start'] + 1000000)),
			'booked_time' => date('g:ia', strtotime($reservation_info['start'] + 1000000)),
			'booked_holes' => $reservation_info['holes'],
			'booked_players' => $reservation_info['player_count'],
			'tee_sheet' => $teesheet_info['title']
		);

		if ($success) {

			send_sendgrid(
				$this->session->userdata('customer_email'),
				'Reservation Cancellation Details',
				$this->load->view("email_templates/reservation_made", $email_data, true),
				'booking@foreup.com',
				$this->course_info['name']
			);

			$this->response(array('success' => true, 'msg' => 'Reservation Cancelled'), 200);
		} else {
			$this->response(array('success ' => false, 'msg' => 'Error cancelling your reservation'), 400);
		}
	}

	function users_invoices_get($invoice_id, $method = null)
	{

		if ($method && $method == 'payments') {
			$this->users_invoices_payments_post($invoice_id);
			return true;
		}
		$this->load->model('invoice');

		// If user tries to enter a random invoice ID, make sure they have access to it
		if (!$this->invoice->has_access($invoice_id, $this->session->userdata('customer_id'))) {
			$this->response(null, 404);
			return false;
		}

		$invoice_data = $this->invoice->get_details($invoice_id);
		$this->response($invoice_data, 200);
	}

	function users_account_transactions_get()
	{

		$this->load->model('Account_transactions');
		$personId = (int)$this->session->userdata('customer_id');
		$courseId = $this->course_id;
		$params = $this->input->get();

		$filters = array(
			'hide_balance_transfers' => true,
		);

		if (!$params['per_page']) {
			$params['per_page'] = 100;
		}

		$filters['limit'] = $params['per_page'];

		if ($params['page']) {
			$filters['offset'] = (int)($params['page'] - 1) * $params['per_page'];
		}

		if (!empty($params['start_date'])) {
			$filters['date_start'] = Carbon::parse($params['start_date'])->setTime(0, 0, 0);
		}
		if (!empty($params['end_date'])) {
			$filters['date_end'] = Carbon::parse($params['end_date'])->setTime(23, 59, 59);
		}

		$transactions = $this->Account_transactions->get_transactions(
			'member',
			$personId,
			$filters,
			$courseId
		);

		header('X-Total: ' . (int)$this->Account_transactions->number_records());
		header('X-Page: ' . (int)$params['page']);
		header('X-Per-Page: ' . (int)$params['per_page']);

		$this->response($transactions, 200);
	}

	function users_invoices_post($invoice_id, $method)
	{

		if ($method == 'payments') {
			$this->users_invoices_payments_post($invoice_id);
			return true;
		}

		$this->response(null, 405);
	}

	function users_invoices_payments_post($invoice_id)
	{

		// Make sure user has permission to access invoice
		if (!$this->Invoice->has_access($invoice_id, $this->session->userdata('customer_id'))) {
			$this->response(null, 404);
			return false;
		}
		$data = $this->request->body;

		if (empty($data['amount'])) {
			$this->response(array('success ' => false, 'msg' => 'Amount is required'), 400);
			return false;
		}

		if (empty($data['credit_card_id'])) {
			$this->response(array('success ' => false, 'msg' => 'Existing credit card is required'), 400);
			return false;
		}

		$this->load->model('Online_booking');
		$this->load->model('Customer_credit_card');
		$this->load->model('Sale');

		$terminal_id = $this->Online_booking->get_invoice_terminal();

		// Charge credit card for specified amount
		$charge_success = $this->Invoice->charge_invoice($invoice_id, $this->course_id, (int)$data['credit_card_id'], (float)$data['amount'], $terminal_id);

		if ($charge_success) {

			// Return updated invoice details
			$invoice_details = $this->Invoice->get_details($invoice_id);
			$this->response($invoice_details, 200);

		} else {
			$this->response(array('success' => false, 'msg' => 'Error charging credit card. Use a different card, or try again later.'), 400);
		}
	}

	function members_get()
	{

		$customer_id = $this->session->userdata('customer_id');
		if (empty($customer_id)) {
			$this->response(array('success' => false, 'msg' => 'Please log in'), 401);
			return false;
		}

		$query = $this->input->get('q');
		$params = ['q' => $query];

		if (empty($query)) {
			$this->response([], 200);
			return true;
		}

		$params['member'] = 1;

		$this->load->model('v2/Customer_model');
		$customers = $this->Customer_model->get($params);

		foreach ($customers as &$customer) {
			$customer = elements(['first_name', 'last_name', 'email',
				'phone_number', 'cell_phone_number', 'person_id'], $customer);
		}

		$this->response($customers, 200);
	}

    function pending_reservation_post()
    {
        $response = array(
            'success' => true
        );

        $this->response($response, 200);
        // Return false until bugs are fixed

        $this->load->model('online_booking');
        $teesheet_id = $this->input->post('schedule_id');
        $start = $this->input->post('time');
        $person_id = $this->input->post('guest_id');
        $guests = $this->input->post('players');
        $holes = $this->input->post('holes');

        $carts = 0;

        $errors = array();
        if (!$teesheet_id || $teesheet_id == '')
            $errors[] = 'Missing parameter: schedule_id';
        if (!$start || $start == '')
            $errors[] = 'Missing parameter: time';
        if (!$guests || $guests == '')
            $errors[] = 'Missing parameter: guests';
        if (!$holes || $holes == '')
            $errors[] = 'Missing parameter: holes';
        if (count($errors)>0)
            $this->response(array('success'=>false,'error'=>implode(', ', $errors)));

        $course_id = $this->session->userdata('course_id');
        $course_info = (array)$this->course->get_info($course_id);

        $reservation_data = array(
            'time' => $start,
            'players' => $guests,
            'holes' => $holes,
            'carts' => $carts,
            'schedule_id' => $teesheet_id,
            'customer_id' => $person_id,
            'customer' => array(),
            'price_class' => false,
            'duration' => 1,
            'pending_reservation' => 1,
            'group_id' => '',
            'promo_id' => '',
            'pending_reservation_id' => '',
            'course_id' => $course_id,
			'course' => $course_info,
            'available_spots' => 4
        );

        $saved_reservation = $this->online_booking->book_time($reservation_data);

        if (!empty($saved_reservation)) {
            $response = array(
                'success'=>true,
                'booked_reservation'=>true,
                'reservation_id'=>$saved_reservation['teetime_id']);
            $this->response($response, 200);
        }
        else {
            $response = array(
                'success' => false
            );

            $this->response($response, 400);
        }

    }

    function pending_reservation_delete($reservation_id)
    {
        $this->load->model('online_booking');
        $response = $this->online_booking->delete_pending_reservation($reservation_id);

        $this->response($response, 200);
    }

    function users_reservations_get($reservation_id) {
        $this->load->model('online_booking');
        $reservation_info = $this->online_booking->get_reservation($reservation_id);

        $this->response(array('reservation_info'=>$reservation_info), 200);
    }

    function users_reservations_put() {
        $this->load->model('online_booking');

        $tee_time_id = $this->request->body['TTID'];
        $players = $this->request->body['players'];
        $start = $this->request->body['start'];
        if (date('Y-m-d H:i:s', strtotime($start + 1000000)) < date('Y-m-d H:i:s', strtotime('+24 hours'))) {
            $success = false;
        }
        else {
            $success = $this->online_booking->update_reservation($tee_time_id, array('player_count'=>$players));
        }
        $response = array('success'=>$success);
        if ($success) {
            $this->response($response, 200);
        }
        else {
            $this->response($response, 400);
        }
    }

}
