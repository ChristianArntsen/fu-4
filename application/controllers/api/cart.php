<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Cart extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();

		$this->load->model('v2/Cart_model');
		$this->load->model('v2/Employee_model');
		$this->load->model('Employee');
	}
	
	private function log_cart_error($cart_id, $client_data, $msg = ''){
		
		$timestamp = time();
		$log_text = '[TIMESTAMP '.$timestamp.'] V2 SALE SAVE ERROR - Error: '.$msg.
			' [COURSE_ID: '.$this->session->userdata('course_id').'] [CART_ID '.$cart_id.']'.
			' [MYSQL ERROR '.$this->db->_error_number().'- '.$this->db->_error_message();
		
		// Write log to PHP error log
		error_log($log_text);
		
		// Dump data to separate file (PHP error log can only handle so much before it truncates data)
		$data_dump = '/********** Database Cart ***********/'.PHP_EOL.
			var_export($this->Cart_model->all(), true).';'.PHP_EOL.
			var_export($this->Cart_model->get_totals(), true).';'.PHP_EOL.
			'/********** Client Cart [CART_ID: '.$cart_id.'] ***********/'.PHP_EOL.
			var_export($client_data, true).';';

		$data_dump_path = '/var/log/httpd/v2_sale_error_'.$timestamp.'.txt';
        if (base_url() != 'http://localhost:8080/') {
            file_put_contents($data_dump_path, $data_dump);
        }
	}

	private function cart_exists($cart_id){

		if(!$this->Cart_model->cart_exists($cart_id)){
			
			$timestamp = time();
			$log_text = '[TIMESTAMP '.$timestamp.'] MISSING CART - [COURSE_ID: '.$this->session->userdata('course_id').'] [CART_ID '.$cart_id.']';
			
			// Write log to PHP error log
			error_log($log_text);

			$this->response([
				'msg' => 'Your sale has timed out. Refreshing page...', 
				'error' => 'missing_cart'
			], 500);
			return false;
		}

		return true;
	}

	// Get cart data (items, customers, payments)
	function index_get($cart_id = null, $record_id = null){
		
		if($cart_id == 'search'){
			return $this->search_get($record_id);
		}
		
		$params = $this->input->get();
		if(!empty($cart_id)){
			$params['cart_id'] = (int) $cart_id;
			$this->Cart_model->cart_id = $cart_id;
		}

        $carts = $this->Cart_model->get($params);
        if(!empty($cart_id)){
        	$carts = $carts[0];
        }
        $this->response($carts, 200);
	}
	
	function orders_post(){
		
		session_write_close();
		$data = $this->request->body;

		$sale_id = (int) $data['sale_id'];
		$table_number = $data['table_number'];
		$employee_id = (int) $this->session->userdata('person_id');
		$items = $data['items'];
		$message = $data['message'];
		
		$this->load->model('table_ticket');
		$ticket_id = $this->table_ticket->save($sale_id, $table_number, $employee_id, $items, $message);
		
		if(empty($ticket_id)){
			$this->response(array('success' => false, 'msg' => 'Error sending F&B order'), 500);
			return false;
		}else{
			$this->response(array('success' => true, 'ticket_id' => (int) $ticket_id), 201);
		}	
	}
	
	function search_get($record_id = null){
		
		session_write_close();
		$this->load->model('v2/Item_model');
		$this->load->model('v2/Item_kit_model');
		$term = trim($this->input->get('q'));
		$data = array();
		
		// Invoice
		if(stripos($term, 'INV ') !== false) {
            $term_parts = explode(' ', $term);
            $this->load->model('v2/Invoice_model');
            $invoices = $this->Invoice_model->get(array('invoice_number' => $term_parts[1]));

            if ($invoices[0] && $invoices[0]['deleted'] == 0) {
                $item = array(
                    'total' => (float)$invoices[0]['due'],
                    'unit_price' => (float)$invoices[0]['due'],
                    'subtotal' => (float)$invoices[0]['due'],
                    'tax' => 0,
                    'quantity' => 1,
                    'item_type' => 'invoice',
                    'name' => 'Invoice #' . $invoices[0]['invoice_number'],
                    'params' => $invoices[0],
                    'invoice_id' => (int)$invoices[0]['invoice_id']
                );
                $data[] = $item;
            }

        // Statements (New member billing)
        }else if(stripos($term, 'STA ') !== false){
            $term_parts = explode(' ', $term);
            $this->load->model('v2/Statement_model');
            $statements = $this->Statement_model->get(array('number' => $term_parts[1]));
            $statement_item = $this->Item_model->get_special_item('statement_payment');

            if($statements[0]){
                $statement = $statements[0];
                $item = array(
                    'total' => (float) $statement['total_open'],
                    'unit_price' => (float) $statement['total_open'],
                    'subtotal' => (float) $statement['total_open'],
                    'tax' => 0,
                    'quantity' => 1,
                    'item_type' => 'statement',
                    'name' => 'Statement #'.$statement['number'],
                    'params' => $statement
                );

                $item = array_merge($statement_item, $item);
                $data[] = $item;
            }

		// Raincheck
		}else if(stripos($term, 'RID ') !== false){	
			$this->load->model('v2/Raincheck_model');	
			$data = $this->Raincheck_model->get(array('number' => $term));
	
		// Coupon
		}else if(stripos($term, 'CP') !== false){	
			$this->load->model('Promotion');
			$promo_id = substr($term, 2);
			$promo = (array) $this->Promotion->get_info($promo_id);
			
			if(!empty($promo)){
				$promo['promo_id'] = $promo['id'];
				$data[0] = $promo;
			}
			
		// Sale (for refunding)
		}else if(stripos($term, 'POS ') !== false){	
			$this->load->model('v2/Sale_model');
			$term_parts = explode(' ', $term, 2);
			$sale_number = (int) $term_parts[1];
			
			$sales = $this->Sale_model->get(array('sale_id' => $sale_number));
			
			if(!empty($sales[0])){
				$data[0] = $sales[0];
			}
			
		// Item kit barcode
		}else if(stripos($term, 'KIT ') !== false){
            $term = str_replace('KIT ', '', $term);
			$item_kit_id = $this->Item_kit_model->check_number($term);
			$data = $this->Item_kit_model->get(array('item_kit_id' => $item_kit_id));
		
		// Item barcode
		}else if($item_id = $this->Item_model->check_item_number($term)){
			$data = $this->Item_model->get(array('item_id' => $item_id));
		
		// Item kit UPC
		}else if($item_id = $this->Item_kit_model->check_number($term)){
			$item_kit_id = $this->Item_kit_model->check_number($term);
			$data = $this->Item_kit_model->get(array('item_kit_id' => $item_kit_id));

		// Default to search all items by name
		}else{
			$data = $this->Item_model->get(array('q' => $term));
		}

		$this->response($data, 200);
	}

	function index_delete($cart_id = null, $method = null, $record_id = null){
		
		if($method == 'customers'){
			if(!$this->cart_exists($cart_id)){
				return false;
			}			
			return $this->customers_delete($record_id);

		}else if($method == 'items'){
			if(!$this->cart_exists($cart_id)){
				return false;
			}			
			return $this->items_delete($record_id);

		}else if($method == 'payments'){
			if(!$this->cart_exists($cart_id)){
				return false;
			}			
			return $this->payments_delete($record_id);
		}
		
		$this->Cart_model->cart_id = $cart_id;
		$cart_details = $this->Cart_model->get_cart();
		
		// Remove all items, customers and payments from cart
		$success = $this->Cart_model->delete($cart_id);
		
		if($success){
			
			$response = array('success' => true);
			
			// If the cart being deleted was active, re-initialize the cart
			// so there is always an active cart in the database
			if($cart_details['status'] != 'suspended'){
				$new_cart_id = $this->Cart_model->initialize_cart();
				$this->session->set_userdata('pos_cart_id', (int) $new_cart_id);
				$this->session->sess_write();
				$response['cart_id'] = $new_cart_id;				
			}
			
			$this->response($response, 200);
		
		}else{
			$this->response(array('success' => false), 400);
		}
	}

	function index_post($cart_id = null, $method = null, $record_id = null){
		return $this->index_put($cart_id, $method, $record_id);
	}

	function index_put($cart_id = null, $method = null, $record_id = null){

		$this->cart_id = $cart_id;
		$this->Cart_model->cart_id = (int) $cart_id;

		if(!$this->cart_exists($cart_id)){
			return false;
		}

		if($method == 'customers'){
			return $this->customers_post($record_id);

		}else if($method == 'items'){
			return $this->items_post($record_id);

		}else if($method == 'payments'){
			return $this->payments_post($record_id);
		
		}else if($method == 'override'){
			return $this->override_post($cart_id);
		}else if($method == 'initialize'){
			return $this->initialize_post();
		}

		$data = $this->request->body;
		
		$customer_note = null;
		if(!empty($data['customer_note'])){
			$customer_note = $data['customer_note'];
		}

		// If completing sale
		if(!empty($data['status']) && $data['status'] == 'complete'){

			// Sync items from client cart
			if(isset($data['items']) && !empty($data['items'])){
				foreach($data['items'] as $item){
					if(empty($item) || empty($item['line'])){
						continue;
					}
					$this->Cart_model->save_item($item['line'], $item);
				}
			}

			if($this->Cart_model->is_paid()){

				if(!empty($data['employee_pin'])){
					$this->Cart_model->employee_pin = $data['employee_pin'];
				}
				$response = $this->Cart_model->save_sale($customer_note);
				
				if(!$response){
					$msg = 'Error completing sale. Please try again. If this issue continues, contact support';
					if(!empty($this->Cart_model->msg)){
						$msg = $this->Cart_model->msg;
					}
					
					$status = 400;
					if(!empty($this->Cart_model->status)){
						$status = $this->Cart_model->status;
					}
					
					$this->log_cart_error($cart_id, $data, 'Sale failed to save');
					$this->response(array('success' => false, 'msg' => $msg), $status);
					return false;					
				}
				
				$response['success'] = true;
				if(isset($response['sale_id'])){
					$dispatchContext = new \Onoi\EventDispatcher\DispatchContext();
					$dispatchContext->set("sale_id",$response['sale_id']);
					$this->eventDispatcher->dispatch("sale.created",$dispatchContext);
				}
				$this->response($response);

				return true;				
				
			}else{
				$this->log_cart_error($cart_id, $data, 'Total mismatch between front and back end');
				$this->response(array('success' => false, 'msg' => 'Sale completion failed, total due has not been met'), 400);
				return false;
			}
		
		// If resuming a suspended cart
		}else if(!empty($data['status']) && $data['status'] == 'active'){
			$success = $this->Cart_model->resume($cart_id);
			if(!$success){
				$this->response(array('msg' => $this->Cart_model->msg), 409);
				return false;
			}

		// If suspending cart
		}else if(!empty($data['status']) && $data['status'] == 'suspended'){
			$cart_id = $this->Cart_model->suspend($data['suspended_name']);
			if($cart_id){
				$this->response(array('success' => true, 'cart_id' => $cart_id));
			}else{
				$this->response(array('success' => false, 'msg' => $this->Cart_model->msg), 400);
			}
			return true;

		// If just updating cart (taxable, mode, etc)
		}else{
            if (!empty($data['return_sale_id']) && $this->sale_model->has_been_returned($data['return_sale_id'])) {
                $this->response(array('success' => false, 'msg' => 'This sale has already been returned'), 400);
            }
            else {
                $success = $this->Cart_model->save_cart($data);
            }
		}

		$this->response(array('success' => $success));
	}
	
	// Manager override authorization
	function override_post($cart_id){
		
		$person_id = (int) $this->input->post('person_id');
		$password = $this->input->post('password');
		
		if(empty($person_id)){
			$this->response(array('success' => false, 'msg' => 'Please select a manager'), 400);
			return false;
		}
		
		$this->load->model('v2/Employee_model');
		$is_authenticated = $this->Employee_model->authenticate($person_id, 2, $password);
		
		if($is_authenticated){
			$this->Cart_model->save_cart(array('override_authorization_id' => $person_id));
			$data = (array) $this->Employee_model->get(['person_id' => $person_id]);
			$this->response($data[0], 200);
		}else{
			$this->response(array('success' => false, 'msg' => 'Password is invalid'), 401);
		}
	}	

    /*******************************************************************
     * Customers - Handles customers attached to cart
	 ******************************************************************/
	function customers_get($customer_id){

		$itemData = $this->request->body;
		$this->response(array('msg'=>'TODO: Get customer'));
	}

	// Attach a customer to the sale
	function customers_post($customer_id = false){
		
		session_write_close();
		
		$customer = $this->request->body;

		if(empty($customer_id)){
			$customer_id = $customer['person_id'];
		}
		if(!isset($customer['selected'])){
			$customer['selected'] = true;
		}
		
		$success = $this->Cart_model->save_customer($customer_id, $customer);
		$this->response(array('success' => $success));
	}

	function customers_put($customer_id){
		return $this->customers_post($customer_id);
	}

	// Remove a customer from a sale
	function customers_delete($customer_id){
		
		session_write_close();

		$success = $this->Cart_model->delete_customer($customer_id);
		$this->response(array('success' => $success));
	}

    /*******************************************************************
     * Items
	 ******************************************************************/
	function items_get($item_id){

		$itemData = $this->request->body;
		$this->response(array('msg'=>'TODO: Get item'));
	}

	// Add a new item to cart
	function items_post($line = false){

		session_write_close();
		$item = $this->request->body;
		
		if(!isset($item['selected'])){
			$item['selected'] = true;
		}

		$success = $this->Cart_model->save_item($line, $item);
		$this->response(array('success' => $success));
	}

	function items_put($line){
		return $this->items_post($line);
	}

	// Delete an item from cart
	function items_delete($line){

		session_write_close();
		$success = $this->Cart_model->delete_item($line);
		$this->response(array('success' => $success));
	}

    /*******************************************************************
     * Payments
	 ******************************************************************/
	function payments_get($item_id){
		$itemData = $this->request->body;
		$this->response(array('msg'=>'TODO: Get payments'));
	}

	// Add a new payment to cart
	function payments_post(){

		$payment = $this->request->body;
		
		if(strlen($payment['amount']) > 9){
			$this->response(array('msg' => 'Please select "Credit Card" payment before swiping card'), 400);
			return false;
		}

		if(isset($payment['approved']) && $payment['approved'] === false){
			$this->response(array('msg' => 'Payment unsuccessful. Please try again.'), 400);
			return false;
		}
		
		if(empty($payment['merchant_data'])){
			$payment['merchant_data'] = false;
		}

        $this->Cart_model->save_refund_details($payment);
		$payment = $this->Cart_model->save_payment($payment, $payment['merchant_data']);

		if($payment){
			//Release the lock
			if(!empty($payment['merchant_data']) && !empty($payment['merchant_data']['payment_id'])){
				$payment_lock = new \models\payment_lock();
				$payment_lock->init_by_payment_id($payment['merchant_data']['payment_id']);
				$payment_lock->release_lock();				
			}
 			$response['success'] = true;
			$response += $payment;

		}else{
			$response['success'] = false;
		}

		if($payment){
			if ($this->Cart_model->status == 409) {
				error_log("MERCURY 409 PAYMENT ERROR - payments_post - ".json_encode($payment));
			}
			
			if($this->Cart_model->msg){
				if ($this->Cart_model->status == 409) {
					error_log("MERCURY 409 PAYMENT ERROR - payments_post - ".json_encode($payment));
				}
				$response['status'] = $this->Cart_model->status;
				$response['msg'] = $this->Cart_model->msg;
			}
			$this->response($response);
		
		}else{
			$response['msg'] = $this->Cart_model->msg;
			$status = 400;
			
			if(!empty($this->Cart_model->status)){
				$status = $this->Cart_model->status;
			}
			$this->response($response, $status);
		}
	}

	function payments_put($type){
		return $this->payments_post();
	}

	// Delete a payment from cart
	function payments_delete($type){
		
		session_write_close();
		$success = $this->Cart_model->delete_payment(urldecode($type));

		if($success){
			$this->response(array('success' => $success), 200);
		}else{
			$this->response(array('success' => $success), 500);
		}
	}

	function stats_get(){
		
		session_write_close();
		$this->load->model('Dash_data');
		$pos_totals = $this->Dash_data->fetch_pos_data();

		$data = array(
			'total_sales' => (float) clean_number($pos_totals['header']['Total']),
			'average_sale' => (float) clean_number($pos_totals['header']['Average_Sale'])
		);

		$this->response($data, 200);
	}

	function initialize_post(){
		$new_cart_id = $this->Cart_model->initialize_cart();
		$this->session->set_userdata('pos_cart_id', (int) $new_cart_id);
		$this->session->sess_write();
		$response['cart_id'] = $new_cart_id;

		$this->response($response, 200);
	}
}
