<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Courses extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();
		
		$this->load->model('v2/Course_model');
	}

	function index_patch($course_id = null){
		
		if($this->session->userdata('course_id') != $course_id){
			$this->response(array('msg' => "You don't have access to that course"), 403);
			return false;
		}
		
		$data = $this->request->body;
		$response = $this->Course_model->save($course_id, $data);

		$this->response(array('success' => true), 200);
	}

    function refund_reasons_get(){

        session_write_close();
        $this->load->model('Refund_reason');
        $refund_reasons = $this->Refund_reason->get_all();

        $data = array(
            '',
            'Defective/Broken Item',
            'Wrong Item/Size',
            'Unhappy Customer'
        );
        foreach ($refund_reasons as $refund_reason) {
            $data[] = $refund_reason['label'];
        }

        $this->response($data, 200);
    }
}
