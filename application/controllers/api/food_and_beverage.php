<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Food_and_beverage extends REST_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->authenticate();

		$this->load->model('table_receipt');
		$this->load->model('table_ticket');
		$this->load->library('sale_lib');
		$this->load->model('Employee');
		$this->load->library('v2/Cart_lib');

		$this->suspended_sale_id = false;
		$this->table_num = false;
	}

	// Retrieves suspended sale ID for table number and checks if user
	// can access table
    private function _get_table($table_num){

		$table_num = urldecode($table_num);
		$suspended_sale_id = $this->Table->get_id_by_table_number($table_num);
		$employee_id = (int) $this->session->userdata('person_id');

		// If table service (suspended sale) doesn't exist, create a new one
		if(empty($suspended_sale_id) && !empty($table_num)){
			$suspended_sale_id = $this->Table->save(array(), 0, $employee_id, '', array(), null, $table_num);
		}

		if(!$suspended_sale_id){
			$this->response(array('success'=>false, 'msg' => 'Table does not exist'), 404);
			return false;
		}
		$table_info = $this->Table->get_info($suspended_sale_id);
		$table_info = $table_info->row_array();

		$is_manager = $this->permissions->is_manager() || $this->permissions->is_admin() || $this->permissions->is_super_admin();

		// If employee isn't allowed to access table, return error
		if(!$is_manager && $table_info['employee_id'] != $this->session->userdata('person_id')){
			$this->response(array('success'=>false, 'msg' => 'You don\'t have permission to access that table'), 403);
			return false;
		}

		$this->suspended_sale_id = $suspended_sale_id;
		$this->table_num = $table_num;
		$this->name = isset($table_info['name']) ? $table_info['name'] : '';
		$this->employee_id = $employee_id;
		$this->orders_made = isset($table_info['orders_made']) ? $table_info['orders_made'] : 0;
		$this->manager_auth_id = isset($table_info['manager_auth_id']) ? $table_info['manager_auth_id'] : 0;
		$this->guest_count = isset($table_info['guest_count']) ? $table_info['guest_count'] : null;
		
		return true;
	}

    function index_get()
    {
        echo 'Food and beverage root';
    }

    // Used for manager authorization
    function authorize_post(){

		$pin_or_card = trim($this->input->post('pin'));
		$employee_info = $this->Employee->get_info_by_pin_or_card($pin_or_card);

		// If no employee was found, return error
		if(empty($employee_info)){
			$this->response(array('success' => false), 403);
			return false;
		}

		if((int) $employee_info->user_level >= 2){
			$this->Table->update_manager_auth($this->suspended_sale_id, $employee_info->person_id);
			$this->response(array('success' => true, 'employee_id' => (int) $employee_info->person_id), 200);
			return true;
		}

		// If employee does not have manager permissions, return error
		$this->response(array('success' => false), 403);
		return false;
	}

	// Used to login a new user in F&B
	function authenticate_post($method = null){

		if($method == 'logout'){
			$this->session->unset_userdata('fnb_logged_in');
			$this->session->unset_userdata('table_id');
			$this->session->unset_userdata('table_number');
			$this->session->unset_userdata('suspended_sale_id');

			// Force the session to update database immediately
			$this->session->sess_write();
			$this->response(array('success' => true), 200);
			return true;
		}

		$this->load->model('Module');
		$this->load->model('Sale');
		$this->load->model('v2/Employee_model');

		$pin_or_card = trim($this->input->post('pin'));
		$employee_id = $this->Employee_model->authenticate_pin($pin_or_card);

		if(empty($employee_id)){
			$this->response(array('success' => false), 403);
			return false;
		}

		$employee_info = $this->Employee->get_info($employee_id);
		$logged_in = $this->Employee->login($employee_info->username, $employee_info->password, true);

		$modules = $this->Module->get_allowed_modules($employee_info->person_id);
		$employee_info->modules = $modules->result_array();

		if(empty($logged_in)){
			$this->response(array('success' => false), 403);
			return false;
		}

		$food_bev_access = false;
		foreach($employee_info->modules as $module){
			if($module['module_id'] == 'food_and_beverage' || $module['module_id'] == 'food_and_beverage_v2'){
				$food_bev_access = true;
			}
		}

		if(empty($food_bev_access)){
			$this->response(array('success' => false, 'message' => lang('food_and_beverage_no_permission')), 403);
			return false;
		}

		// Force the session to update the database immediately
		// instead of waiting for page refresh as usual
		$this->session->set_userdata('fnb_logged_in', true);
		$this->session->sess_write();

		$register_log = $this->Sale->getUnfinishedRegisterLog();
		$employee_info->fnb_logged_in = true;
		$employee_info->skipped_register_log = false;

		$this->response(array(
			'success' => true,
			'employee' => $employee_info,
			'recent_transactions' => $this->Table->get_recent_transactions($employee_info->person_id, 500),
			'register_log' => $register_log->row_array()
		), 200);
	}

    /*******************************************************************
     * Service - Manages listing/opening/suspending sales on tables
	 ******************************************************************/
	function service_get($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		session_write_close();

		// If retrieving data for single table
		if(!empty($table_num)){

			$this->_get_table($table_num);

			// Route receipts
			if($method == 'receipts'){
				$this->receipts_get($recordId, $method2, $recordId2);
				return true;
			}

			// Route cart
			if($method == 'cart'){
				$this->cart_get($recordId);
				return true;
			}

			// Route kitchen orders
			if($method == 'orders'){
				$this->orders_get($recordId);
				return true;
			}

			// Route customers
			if($method == 'customers'){
				$this->customers_get($recordId);
				return true;
			}

			// Retrieve table data along with all items, receipts and
			// payments associated with it
			$table_info = $this->Table->get_info($this->suspended_sale_id);
			$table_info = $table_info->row_array();

			$data['fired_meal_courses'] = array();
			if($this->config->item('use_course_firing') == 1){
				$this->load->model('v2/Meal_course_model');
				$data['fired_meal_courses'] = $this->Meal_course_model->get_table_fired_courses(array('sale_id' => $this->suspended_sale_id));
			}

			$data['table_num'] = $this->table_num;
			$data['suspended_sale_id'] = (int) $this->suspended_sale_id;
			$data['employee_id'] = isset($table_info['employee_id']) ? $table_info['employee_id'] : 0;
			$data['cart'] = $this->Table->get_items($this->suspended_sale_id);
			$data['receipts'] = $this->table_receipt->get($this->suspended_sale_id);
			$data['customers'] = $this->Table->get_customers($this->suspended_sale_id);
			$data['sale_time'] = isset($table_info['sale_time']) ? $table_info['sale_time'] : '';
			$data['name'] = isset($table_info['name']) ? $table_info['name'] : '';
			$data['orders_made'] = (int) isset($table_info['orders_made']) ? $table_info['orders_made'] : 0;
			$data['guest_count'] = isset($table_info['guest_count']) ? $table_info['guest_count'] : null;
			$data['admin_auth_id'] = empty($table_info['manager_auth_id']) ? false : (int) $table_info['manager_auth_id'];

			$this->response($data);
			return true;
		}

		// If getting list of available tables
		$tables = $this->Table->get_all(true)->result_array();
		$this->response($tables);
	}

	function service_post($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$this->_get_table($table_num);

		// Route receipts
		if($method == 'receipts'){
			$this->receipts_post($recordId, $method2, $recordId2);
			return true;
		}

		// Route cart
		if($method == 'cart'){
			$this->cart_post($recordId);
			return true;
		}

		// Route kitchen orders
		if($method == 'orders'){
			$this->orders_post($recordId);
			return true;
		}

		// Route customers
		if($method == 'customers'){
			$this->customers_post($recordId);
			return true;
		}

		// Merge this table into another existing table
		if($method == 'merge'){
			$this->merge_post($recordId);
			return true;
		}

		$success = true;
		$employee_id = $this->post('employee_id');
		if(!empty($employee_id)){

			// Employees can only be switched by admins
			if($this->permissions->is_employee()){
				$this->response(array('success' => false,'msg' => 'You don\'t have permission to switch table employees'), 403);
				return false;
			}

			$success = $this->Table->update(array('employee_id'=>(int) $employee_id), $this->suspended_sale_id);
		}

		// If changing nickname of table
		if($this->input->post('name')){
			$success = $this->Table->update(array('name'=>$this->input->post('name')), $this->suspended_sale_id);
		}

		if($success){
			$this->response(array('success' => true,'msg' => 'Table updated'));
		}else{
			$this->response(array('success' => false,'msg' => 'Error updating table'), 500);
		}
	}

	function service_put($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$this->_get_table($table_num);

		// Route receipts
		if($method == 'receipts'){
			$this->receipts_put($recordId, $method2, $recordId2);
			return true;
		}

		// Route cart
		if($method == 'cart'){
			$this->cart_put($recordId);
			return true;
		}

		// Route kitchen orders
		if($method == 'orders'){
			$this->orders_put($recordId);
			return true;
		}

		// Route customers
		if($method == 'customers'){
			$this->customers_put($recordId);
			return true;
		}

		$requestData = $this->request->body;

		$success = true;
		if(isset($requestData['name']) || isset($requestData['guest_count'])){
			$success = $this->Table->update(array('name' => $requestData['name'], 'guest_count' => (int) $requestData['guest_count']), $this->suspended_sale_id);
		}

		$this->response(array('success' => $success));
	}

	function service_delete($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		session_write_close();
		$this->_get_table($table_num);

		// Get any payments made on sale
		$payments = $this->Table->get_payments($this->suspended_sale_id);
        //$payments = $this->sale_lib->get_payments($this->suspended_sale_id);

		// Route receipts
		if($method == 'receipts'){
			$this->receipts_delete($recordId, $method2, $recordId2);
			return true;
		}

		// Route cart
		if($method == 'cart'){
            $item_payments = $this->Table->item_payments($this->suspended_sale_id,$recordId);
			if(count($item_payments) > 0){
				$this->response(array('success' => false, 'msg' => 'Error please refund payments before removing any items from this sale.'), 400);
				return;
			}
			$this->cart_delete($recordId);
			return true;
		}

		// Route kitchen orders
		if($method == 'orders'){
			$this->orders_delete($recordId);
			return true;
		}

		// Route customers
		if($method == 'customers'){
			$this->customers_delete($recordId);
			return true;
		}

		// Check if some payments have been made, but not all receipts are paid off, return error
		if(count($payments) > 0 && !$this->table_receipt->is_paid($this->suspended_sale_id)){
			$this->response(array('success' => false, 'msg' => 'Error closing sale, finish paying receipts or refund payments to close sale'), 400);
			return false;
		}

        $success = $this->Table->delete($this->suspended_sale_id);

		if(empty($success)){
			$this->response(array('success' => $success, 'msg' => 'Error closing table service'), 400);
		}else{
			$this->response(array('success' => $success, 'msg' => 'Table service closed'));
		}
	}

    /*******************************************************************
     * Recent Transactions
	 ******************************************************************/
	function recent_transactions_get($method = null)
	{
		session_write_close();
		$this->load->model('Sale');

		// Route sales tips
		if($method == 'tips'){
			$this->recent_transactions_tips_get($sale_id);
			return true;
		}

		// If employee ID passed as parameter, filter by it
		$employee_id = $this->get('employee_id');
		$sales = $this->Table->get_recent_transactions($employee_id, 500);

		if(is_array($sales)){
			$this->response($sales, 200);
		}

		$this->response($sales);
	}

	function recent_transactions_post($sale_id = null, $method = null)
	{
		$this->load->model('Sale');

		// Route sales tips
		if($method == 'tips'){
			$this->recent_transactions_tips_post($sale_id);
			return true;
		}

		$this->response(array('success'=>false,'msg'=>'Method not allowed'), 405);
	}

	function recent_transactions_put($sale_id, $method = null, $record_id = null)
	{
		$this->load->model('Sale');

		// Route sales tips
		if($method == 'tips'){
			$this->recent_transactions_tips_put($sale_id, $record_id);
			return true;
		}

		$this->response(array('success'=>false,'msg'=>'Method not allowed'), 405);
	}

	function recent_transactions_delete($method = null)
	{
		$this->response(array('success'=>false,'msg'=>'Method not allowed'), 405);
	}

    /*******************************************************************
     * Sales Tips
	 ******************************************************************/
	function recent_transactions_tips_get($sale_id = null, $method = null)
	{
		session_write_close();
		$this->load->model('Sale');

		// Route sales tips
		if($method == 'tips'){
			$this->sales_tips_get($sale_id);
			return true;
		}

		// If employee ID passed as parameter, filter by it
		$employee_id = (int) $this->get('employee_id');
		$sales = $this->Sale->get_recent_transactions(500, $employee_id);

		if(is_array($sales)){
			$this->response($sales, 200);
		}

		$this->response($sales);
	}

	function recent_transactions_tips_put($sale_id = null){
		$this->recent_transactions_tips_post($sale_id);
	}

	function recent_transactions_tips_post($sale_id = null)
	{
		if(empty($sale_id)){
			$this->response(array('success'=>false, 'msg'=>'Sale ID is required'), 403);
			return false;
		}

		session_write_close();
		$this->load->model('payment');

		$tip = $this->request->body;
		$gratuity = $tip['amount'];
		$type = $tip['type'];
		$tip_recipient = (int) $tip['tip_recipient'];
		$cc_invoice_id = (int) $tip['invoice_id'];
		$giftcard_number = trim($tip['gift_card_number']);
		$member_account_name = lang('customers_member_account_balance');

		if($this->config->item('member_balance_nickname')){
			$member_account_name = $this->config->item('member_balance_nickname');
		}

		$customer_account_name = lang('customers_account_balance');
		if($this->config->item('customer_credit_nickname')){
			$customer_account_name = $this->config->item('customer_credit_nickname');
		}

		if($type == 'cash'){
			$payment_type = "Cash ".lang('sales_tip');
		}else if($type == 'check'){
			$payment_type = "Check ".lang('sales_tip');
		}else if($type == 'member_balance'){
			$payment_type = $type;
		}else if($type == 'customer_balance'){
			$payment_type = $type;
		}else if($type == 'gift_card'){
			$payment_type = "Gift Card:".$giftcard_number." Tip";
		}else{
			$payment_type = 'Credit Card Tip';
		}

		if($gratuity < 0){
			$this->response(array('success'=>false, 'msg'=>'Tip must be greater than 0.00'), 403);
			return false;
		}

		if(empty($tip_recipient)){
			$tip_recipient = (int) $this->session->userdata('person_id');
		}

		// If user is adding tip to a credit card payment
		if (!empty($cc_invoice_id)){

			$payment_info = $this->Sale->get_sale_payment($sale_id, $cc_invoice_id)->row_array();
			$credit_card_info = $this->Sale->get_credit_card_payment($cc_invoice_id);
			$credit_card_info = $credit_card_info[0];

			if(empty($payment_info)){
				$this->response(array('success'=>false, 'msg'=>'Credit card payment does not exist'), 404);
				return false;
			}
			$approved = false;

			// If account is using ETS payments
			if ($this->config->item('ets_key')){

				$this->load->library('Hosted_payments');
				$payment = new Hosted_payments();
				$payment->initialize($this->config->item('ets_key'));

				$session = $payment->set('action', 'addTip')
							->set('transactionID', $credit_card_info['payment_id'])
							->set('amount', $gratuity)
							->send();

				if($session->status == 'success'){
					$approved = true;
				}

				$auth_amount = $session->transactions->amount - $gratuity;
				$payment_data = array (
					'auth_amount' => (string) $auth_amount,
					'gratuity_amount' => (string) $gratuity,
				);

			// If account is using Mercury payments
			}else if($this->config->item('mercury_id')){

				$this->load->library('Hosted_checkout_2');
				$HC = new Hosted_checkout_2();
				$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
				$HC->set_invoice($credit_card_info['invoice']);
				$HC->set_token($credit_card_info['token']);
				$HC->set_ref_no($credit_card_info['ref_no']);
				$HC->set_frequency($credit_card_info['frequency']);
				$HC->set_memo($credit_card_info['memo']);
				$HC->set_auth_code($credit_card_info['auth_code']);
				$HC->set_cardholder_name($credit_card_info['cardholder_name']);
				$HC->set_customer_code($credit_card_info['customer_code']);

				$response = $HC->token_transaction('Adjust', $credit_card_info['amount'], $gratuity, $credit_card_info['tax_amount']);

				if ((string) $response->Status == 'Approved'){
					$approved = true;
				}

				$payment_data = array (
					'auth_amount'=> (string) $response->AuthorizeAmount,
					'gratuity_amount' => (string) $gratuity,
					'process_data' => (string) $response->ProcessData,
					'token' => (string) $response->Token
				);
			}

			// If the credit card gratuity transaction was approved
			if($approved){

				//Update credit card payment info
				$this->Sale->update_credit_card_payment($cc_invoice_id, $payment_data);

				//Update sale payment info
				$payment_type = "{$credit_card_info['card_type']} ".substr($credit_card_info['masked_account'], -8)." Tip";
				$sales_payments_data = array(
					'sale_id' => $sale_id,
					'payment_type' => $payment_type,
					'payment_amount' => $payment_data['gratuity_amount'],
					'invoice_id' => $cc_invoice_id,
					'tip_recipient' => $tip_recipient
				);

				$this->payment->add($sales_payments_data);

				// Respond with tip data
				$this->response(array(
					'amount' => $payment_data['gratuity_amount'],
					'type' => $type,
					'payment_type' => $payment_type,
					'invoice_id' => $cc_invoice_id,
					'tip_recipient' => $tip_recipient
				));
				return true;
			
			}else{

				$this->response(array('success'=>false, 'msg'=>'Credit card declined', 400));
				return false;			
			}

		}else if($giftcard_number){

			$this->load->model('Giftcard');

			// Check if a tip was already withdrawn from the account,
			// calculate difference between last tip amount set, and new tip amount
			$last_tip = $this->Table->get_payment($sale_id, $payment_type);

			$transfer_amount = (float) $gratuity;
			if(!empty($last_tip['payment_amount'])){
				$transfer_amount -= (float) $last_tip['payment_amount'];
			}

			// If user is adding tip to a gift card payment
			$giftcard_value = (float) $this->Giftcard->get_giftcard_value($giftcard_number);

			// If tip amount less than gift card amount
			if($transfer_amount > $giftcard_value){
				$this->response(array('success' => false, 'msg' => "Insufficient funds on gift card."), 400);
				return false;
			}

			// Update the gift card with the new tip amount
			$this->Giftcard->update_giftcard_value($giftcard_number, (float) ($giftcard_value - $transfer_amount), $sale_id);

			$sales_payments_data = array(
				'sale_id' => $sale_id,
				'payment_type' => $payment_type,
				'payment_amount' => $gratuity,
				'invoice_id' => 0,
				'tip_recipient' => $tip_recipient
			);

			// Add sale payment data to sale payments table
			if($this->payment->add($sales_payments_data)){
				$this->response(array(
					'amount' => $gratuity,
					'payment_type' => $payment_type,
					'type' => $type,
					'invoice_id' => 0,
					'tip_recipient' => $tip_recipient
				));

				return true;
			}

		// If user is adding other tip type
		}else{

			if($type == 'member_balance' || $type == 'customer_balance'){

				if(empty($tip['customer_id'])){
					$this->response(array('success'=>false, 'msg'=>'Customer ID is required for account balance tip'), 400);
					return false;
				}
				$customer_info = $this->Customer->get_info($tip['customer_id'], $this->session->userdata('course_id'));

				// Check if account allows negative balance
				$allow_negative = false;
				if($type == 'member_balance' && $customer_info->member_account_balance_allow_negative == '1'){
					$allow_negative = true;
				}else if($type == 'customer_balance' && $customer_info->account_balance_allow_negative == '1'){
					$allow_negative = true;
				}

				if($type == 'member_balance'){
					$cur_balance = (float) $customer_info->member_account_balance;
					$account_name = $member_account_name;
				}else{
					$cur_balance = (float) $customer_info->account_balance;
					$account_name = $customer_account_name;
				}
				$account_name .= ' Tip - '.$customer_info->last_name.', '.$customer_info->first_name;
				$payment_type = $account_name;

				// Check if a tip was already withdrawn from the account,
				// calculate difference between last tip amount set, and new tip amount
				$last_tip = $this->Table->get_payment($sale_id, $account_name);

				$transfer_amount = (float) $gratuity;
				if(!empty($last_tip['payment_amount'])){
					$transfer_amount -= (float) $last_tip['payment_amount'];
				}

				// If there is insufficient balance, return error
				if((float) $transfer_amount > $cur_balance && !$allow_negative){
					$this->response(array('success'=>false, 'msg'=>'Insufficient balance on account'), 400);
					return false;
				}

				// Transfer money out of member/customer account
				$this->load->model('Account_transactions');
				if($type == 'member_balance' && $transfer_amount != 0){
					$this->Account_transactions->save('member', $tip['customer_id'], 'POS '.$sale_id.' Tip', -$transfer_amount, 'Tip', $sale_id, 0, $this->employee_id);

				}else if($type == 'customer_balance' && $transfer_amount != 0){
					$this->Account_transactions->save('customer', $tip['customer_id'], 'POS '.$sale_id.' Tip', -$transfer_amount,'Tip', $sale_id, 0, $this->employee_id);
				}
			}

			//Update sale payment info
			$sales_payments_data = array(
				'sale_id' => $sale_id,
				'payment_type' => $payment_type,
				'payment_amount' => $gratuity,
				'invoice_id' => 0,
				'tip_recipient' => $tip_recipient
			);

			if($this->payment->add($sales_payments_data)){
				$this->response(array(
					'amount' => $gratuity,
					'payment_type' => $payment_type,
					'type' => $type,
					'invoice_id' => 0,
					'tip_recipient' => $tip_recipient
				));
				return true;
			}
		}

		$this->response(array('success' => false, 'msg' => "Unable to add tip at this time. Please try again later. Note: Tips must be added before end of day."), 500);
		return false;
	}

    /*******************************************************************
     * Table layouts
	 ******************************************************************/
	function table_layouts_get($layout_id = null, $method = null, $recordId = null){
		session_write_close();
		$this->load->model('table_layout');
		$layouts = $this->table_layout->get($layout_id);
		$this->response(array_values($layouts), 200);
	}

	function table_layouts_post($layout_id = null, $method = null, $recordId = null){
		session_write_close();
		$this->load->model('table_layout');

		if($method == 'objects'){
			return $this->layout_objects_post($layout_id, $layoutId);
		}
		$response = $this->table_layout->save($layout_id, $this->request->body['name']);

		if($response){
			$this->response(array('success' => true, 'layout_id' => $response), 200);
		}else{
			$this->response(array('success' => false), 500);
		}
	}

	function table_layouts_put($layout_id = null, $method = null, $recordId = null){
		session_write_close();
		$this->load->model('table_layout');

		if($method == 'objects'){
			return $this->layout_objects_put($layout_id, $recordId);
		}

		return $this->table_layouts_post($layout_id, $method, $recordId);
	}

	function table_layouts_delete($layout_id = null, $method = null, $recordId = null){
		session_write_close();
		$this->load->model('table_layout');

		if($method == 'objects'){
			return $this->layout_objects_delete($recordId);
		}

		$layout_open_sales = $this->table_layout->layout_has_open_sales($layout_id);
		if($layout_open_sales){
			$this->response(array('success' => false, 'msg' => 'Layout must not have any tables with open sales'), 400);
			return false;
		}

		$success = $this->table_layout->delete($layout_id);

		if($success){
			$this->response(array('success' => true), 200);
		}else{
			$this->response(array('success' => false), 500);
		}
	}

	function layout_objects_put($layout_id = null, $recordId = null){
		return $this->layout_objects_post($layout_id, $recordId);
	}

	function layout_objects_post($layout_id = null, $recordId = null){

		$this->load->model('table_layout_object');
		$layout_object = $this->request->body;

		if($layout_id){
			$layout_object['layout_id'] = $layout_id;
		}

		if(!empty($recordId) &&
			$this->table_layout_object->has_table_name_changed($recordId, $layout_object['label']) &&
			$this->table_layout_object->has_open_sale($recordId)
		){
			$this->response(array('success' => false, 'msg' => 'Open sale must be finished or cancelled before renaming table'), 400);
			return false;
		}

		// If an object already exists with that name, return error
		if($this->table_layout_object->object_exists($layout_object['label'], $layout_object['object_id'])){
			$this->response(array('success'=>false, 'msg'=>'Error, that name is already in use'), 400);
			return false;
		}

		$object_id = $this->table_layout_object->save($layout_object);

		if($object_id){
			$this->response(array('success'=>true, 'object_id' => $object_id), 200);
		}else{
			$this->response(array('success'=>false), 500);
		}
	}

	function layout_objects_delete($recordId = null){
		$this->load->model('table_layout_object');

		// If a sale is currently open with that table, don't allow it to be deleted
		if(!empty($recordId) && $this->table_layout_object->has_open_sale($recordId)){
			$this->response(array('success' => false, 'msg' => 'Open sale must be finished or cancelled before deleting table'), 400);
			return false;
		}

		$success = $this->table_layout_object->delete($recordId);

		if($success){
			$this->response(array('success' => $success), 200);
		}else{
			$this->response(array('success' => $success), 400);
		}
	}

    /*******************************************************************
     * Register Log
	 ******************************************************************/
	function register_log_get($sale_id = null)
	{
		$this->response($sales);
	}

	function register_log_post($register_log_id = null)
	{
		$this->load->model('Sale');
		$data = $this->request->body;

		$now = date('Y-m-d H:i:s');
		$cash_register = new stdClass();
		$cash_register->employee_id = $this->session->userdata('person_id');
		$cash_register->terminal_id = $this->session->userdata('terminal_id');
		$cash_register->open_amount = $data['open_amount'];
		$cash_register->close_amount = 0;
		$cash_register->shift_end = '0000-00-00 00:00:00';
		$cash_register->course_id = $this->session->userdata('course_id');

		$cash_register->shift_start = $now;
		$logId = $this->Sale->insertRegisterLog($cash_register);

		if($logId){
			$cash_register->register_log_id = (int) $logId;
			$this->response($cash_register);
			$this->session->set_userdata('unfinished_register_log', 1);
		}else{
			$this->response(array('success'=>false, 'msg'=>'Error saving register log'), 500);
		}
	}

	function register_log_put($register_log_id = null)
	{
		$this->load->model('Sale');
		$data = elements(array('close_amount', 'pennies', 'nickels', 'dimes', 'quarters', 'ones', 'fives',
			'tens', 'twenties', 'fifties', 'hundreds'), $this->request->body, 0);
		$now = date('Y-m-d H:i:s');

		if($data['close_amount'] <= 0){
			$this->response(array('success' => false,'msg' => 'Close amount is required'), 403);
			return false;
		}
		$cash_register = $this->Sale->getUnfinishedRegisterLog()->row();

		$cash_register->shift_end = $now;
		$cash_register->close_amount = $data['close_amount'];
		$counts = array(
			'register_log_id' => $cash_register->register_log_id,
			'pennies' => $data['pennies'],
			'nickels' => $data['nickels'],
			'dimes' => $data['dimes'],
			'quarters' => $data['quarters'],
			'ones' => $data['ones'],
			'fives' => $data['fives'],
			'tens' => $data['tens'],
			'twenties' => $data['twenties'],
			'fifties' => $data['fifties'],
			'hundreds' => $data['hundreds']
		);

		$cash_register->cash_sales_amount = $this->Sale->get_cash_sales_total_for_shift($cash_register->shift_start, $cash_register->shift_end);
		$cash_register->course_id = $this->session->userdata('course_id');

		$success = $this->Sale->updateRegisterLog($cash_register);
		$this->Sale->saveRegisterLogCounts($counts);

		$this->session->unset_userdata('skipped_register_log');
		$this->session->unset_userdata('cash_register');
		$this->session->unset_userdata('unfinished_register_log');

		$this->response(array('success'=>$success, 'msg'=>'Register log updated'));
	}

	function register_log_delete($method = null)
	{
		$this->response(array('success'=>false,'msg'=>'Method not allowed'), 405);
	}

    /*******************************************************************
     * Receipts
	 ******************************************************************/
	function receipts_get($receipt_id = false, $method = null, $record_id = null)
	{
		session_write_close();
		if($method == 'items'){
			$this->receipt_items_get($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_get($receipt_id, $record_id);
			return true;
		}

		if($method == 'has_payments') {
			$this->receipt_has_payments($receipt_id);
			return true;
		}

		if($method == 'payment_count') {
			$this->receipt_payment_count($receipt_id);
			return true;
		}

		if($method == 'payments_total') {
			$this->receipt_payments_total($receipt_id);
			return true;
		}

		if($method == 'delete_sale'){
			$this->delete_receipt_sale($receipt_id);
			return true;
		}

		$receipts = $this->table_receipt->get($this->suspended_sale_id, $receipt_id);
		$this->response($receipts);
	}

	function receipts_post($receipt_id = false, $method = null, $record_id = null)
	{
		if($method == 'items'){
			$this->receipt_items_post($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_post($receipt_id, $record_id);
			return true;
		}

		if($method == 'comps'){
			$this->receipt_comps_post($receipt_id);
			return true;
		}
		$success = $this->table_receipt->save($this->suspended_sale_id, null);
		$this->response(array('success'=>true,'msg'=>'All receipts for table'));
	}

	// Applies a comp to all items in a receipt
	function receipt_comps_post($receipt_id){
		session_write_close();

		$data = $this->input->post();
		$comp = $data['comp'];

		if(!$comp){
			$this->table_receipt->clear_comps($this->suspended_sale_id, $receipt_id);
		}else{
			$this->table_receipt->comp_items($this->suspended_sale_id, $receipt_id, $comp['type'], $comp['description'], $comp['amount'], $comp['employee_id']);
		}

		$this->response(array('success'=>true));
	}

	function receipts_put($receipt_id = false, $method = null, $record_id = null)
	{
		if($method == 'items'){
			$this->receipt_items_put($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_put($receipt_id, $record_id);
			return true;
		}

		$receiptData = $this->request->body;
		$success = $this->table_receipt->save(
			$this->suspended_sale_id, 
			$receipt_id, 
			null, 
			$receiptData['auto_gratuity'],
			$receiptData['auto_gratuity_type'],
			$receiptData['taxable'], 
			$receiptData['customer']
		);
		$this->response(array('success'=>($success?true:false),'msg'=>'Receipt updated'));
	}

	function receipts_delete($receipt_id = false, $method = null, $record_id = null)
	{
		session_write_close();
		if($method == 'items'){
			$this->receipt_items_delete($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_delete($receipt_id, $record_id);
			return true;
		}

		if($method == 'sale'){
			$this->delete_receipt_sale($receipt_id,$this->request->body);
			return true;
		}

		if($receipt_id === false){
			$this->response(array('msg'=>'Can\'t delete all receipts at once.'), 400);
			return false;
		}

		if((int) $receipt_id == 1){
			//$this->response(array('msg'=>'Can\'t delete receipt #1'), 400);
			//return false;
		}

		if(!$this->table_receipt->can_delete($this->suspended_sale_id, $receipt_id)){
			$this->response(array('msg'=>'Receipt must be empty to delete'), 400);
			return false;
		}

		$success = $this->table_receipt->delete($this->suspended_sale_id, $receipt_id);
		$this->response(array('success' => $success, 'msg'=>'Receipt deleted'));
	}

    /*******************************************************************
     * Receipt items
	 ******************************************************************/
	function receipt_items_get($receiptId, $line = null)
	{
		session_write_close();
		$items = $this->table_receipt->get_items($this->suspended_sale_id, $receiptId);
		if(is_array($items)){
			$this->response($items, 200);
		}
		$this->response($items);
	}

	function receipt_items_post($receiptId, $line = null)
	{
		session_write_close();
		$itemData = $this->post();

		$success = $this->table_receipt->add_item($itemData['item_id'], $itemData['line'], $this->suspended_sale_id, $receiptId);
		$this->response(array('success'=>$success,'msg'=>'Receipt item added'));
	}

	function receipt_items_put($receiptId, $line = null)
	{
		session_write_close();
		$itemData = $this->request->body;

		$success = $this->table_receipt->add_item($itemData['item_id'], $itemData['line'], $this->suspended_sale_id, $receiptId);
		$this->response(array('success'=>$success,'msg'=>'Receipt item updated'));
	}

	function receipt_items_delete($receiptId, $line = null)
	{
		session_write_close();
		$itemData = $this->request->body;

		if(!$this->table_receipt->can_delete_item($this->suspended_sale_id, $line)){
			$this->response(array('success'=>false, 'msg'=>'Item must belong to at least 1 receipt'), 400);
			return false;
		}

		$success = $this->table_receipt->delete_item(0, $line, $this->suspended_sale_id, $receiptId);
		$this->response(array('success'=>true, 'msg'=>'Receipt item deleted'));
	}

    /*******************************************************************
     * Receipt payments
	 ******************************************************************/
	function receipt_payments_get($receiptId, $line = null)
	{
		session_write_close();
		$items = $this->Table->get_payments($this->suspended_sale_id, $receiptId);
		if(is_array($items)){
			$this->response($items, 200);
		}
		$this->response($items);
	}

	function receipt_has_payments($receiptId)
	{
		session_write_close();
		$items = $this->Table->get_payments($this->suspended_sale_id, $receiptId);
		if(is_array($items) && count($items) > 0){
			$this->response('true', 200);
		}
		$this->response('false');
	}

	function receipt_payment_count($receiptId)
	{
		session_write_close();
		$items = $this->Table->get_payments($this->suspended_sale_id, $receiptId);
		if(is_array($items) && count($items) > 0){
			$this->response("".count($items), 200);
		}
		$this->response("0");
	}

	function receipt_payments_total($receiptId){
		session_write_close();
		if(!$this->table_receipt->receipt_exists($this->suspended_sale_id, $receiptId)){
			$this->response(array('error'=>'Receipt does not exist'),404);
			return;
		}

		$receiptData = $this->table_receipt->get($this->suspended_sale_id, $receiptId);
		if(!empty($receiptData['total_paid']))$this->response("".$receiptData['total_paid'],200);
		$this->response("0",200);
	}

	function validate_processor_payment($payment){
        if(!isset($payment['credit_card_id'])){
            return false;
        }elseif(!isset($payment['merchant_data']) || count($payment['merchant_data']) === 0){
            return false;
        }
        return true;
    }

	function receipt_payments_post($receiptId, $line = null)
	{
	    // TODO: chop into many smaller functions, and use this ONLY for dispatch...
		$this->load->model('Item_comp');
		$payment = $this->request->body;
		$msg = null;

		if(empty($payment)){
			$payment = $this->post();
		}

		// Handle Mercury credit card payments a bit differently
		if(!empty($payment['Payment_ID']) || !empty($payment['merchant_data']['payment_id']) || (!empty($payment['merchant']) && $payment['merchant'] == 'mercury')){
			$payment['type'] = 'creditcard-mercury';

		}else{
			// Make sure payment type is passed
			if(empty($payment['type'])){
				$this->response(array('success'=>false, 'msg'=>'Payment type is required'), 400);
				return false;
			}
			// Make sure payment amount is over 0
			if($payment['amount'] < 0){
				$this->response(array('success'=>false, 'msg'=>'Amount can not be less than 0'), 400);
				return false;
			}
			$payment['amount'] = (float) round($payment['amount'], 2);
		}

		// Process payment depending on type
        // TODO: give each thing its own dispatch method
        // once we know what we are doing, we should send it down a discreet path
		switch(strtolower($payment['type'])){
			case 'cash':
			case 'check':
				if($payment['card_number'] != '')
				$payment['type'] = $payment['type'].':'.$payment['card_number'];
			break;
			
			case 'credit card':
                if(empty($payment['credit_card_id']) && isset($payment['merchant_data']['credit_card_id'])){
                    $payment['credit_card_id'] = $payment['merchant_data']['credit_card_id'];
                }
                if(empty($payment['auth_code']) && isset($payment['merchant_data']['auth_code'])){
                    $payment['auth_code'] = $payment['merchant_data']['auth_code'];
                }
				
				// handle paying with saved credit card
                if(!$this->validate_processor_payment($payment)){
                    // do nothing, it is a shift-click credit card payment
                }
				elseif(isset($payment['credit_card_id']) && $payment['auth_code']>0){ // mercury saved

					$payment['auth_code'];
					// need to grab the Customer_credit_card model somehow...
					$this->load->model('Customer_credit_card');
					//$this->load->model('Hosted_checkout_2');
					$this->load->library('Hosted_checkout_2');
					//$HC = new Hosted_checkout_2();
					$HC = $this->hosted_checkout_2;
					$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));

					$credit_card = $this->Customer_credit_card->get_info($payment['credit_card_id']);

					$token = $credit_card->token;

                    $invoice_id = $this->sale->add_credit_card_payment([
                        'tran_type' => 'Sale',
                        'frequency' => 'OneTime',
                        'mercury_id' => $this->config->item('mercury_id'),
                        'mercury_password' => $this->config->item('mercury_password')
                    ]);

					$HC->set_frequency('OneTime');
					$HC->set_token($token);
                    $HC->set_invoice($invoice_id);

					$transaction_results = $HC->token_transaction('Sale', $payment['amount'], '0.00', '0.00');


					$card_type = $transaction_results->CardType;
                    $token = $transaction_results->Token;

                    switch(strtoupper($card_type)){
                        case 'MASTERCARD':
                            $card_type = 'M/C';
                            break;
                        case 'VISA':
                            $card_type = 'VISA';
                            break;
                        case 'DISCOVER':
                            $card_type = 'DCVR';
                            break;
                        case 'AMERICANEXPRESS':
                            $card_type = 'AMEX';
                            break;
                        case 'DINERS':
                            $card_type = 'DINERS';
                            break;
                        case 'JCB':
                            $card_type = 'JCB';
                            break;
                    }

                    $masked_account = $credit_card->masked_account;
                    $auth_code = (string) $payment['merchant_data']['auth_code'];
                    $payment_type = $payment['type'];
                    $tran_type = 'Sale';
                    $transaction_time = date('Y-m-d H:i:s');

                    $payment_data = array(
                        'course_id' 		=> $this->session->userdata('course_id'),
                        'mercury_id'        => $this->config->item('mercury_id'),
                        'mercury_password'  => $this->config->item('mercury_password'),
                        'cardholder_name'   => (string) $credit_card->cardholder_name,
                        'ref_no'            => (string) $transaction_results->RefNo,
                        'acq_ref_data'      => (string) $transaction_results->AcqRefData,
                        'memo'              => (string) 'ForeUP v.1.0',
                        'trans_post_time'   => gmdate('Y-m-d H:i:s'),
                        'masked_account' 	=> (string) $masked_account,
                        'tran_type' 		=> (string) $tran_type,
                        'amount' 			=> (string) $transaction_results->PurchaseAmount,
                        'auth_amount'		=> (string)	$transaction_results->AuthorizeAmount,
                        'auth_code'			=> (string) $transaction_results->AuthCode,
                        'card_type' 		=> (string) $card_type,
                        'frequency' 		=> 'OneTime',
                        'process_data'		=> (string) $transaction_results->ProcessData,
                        'status'			=> (string) $transaction_results->Status,
                        'status_message'	=> (string) $transaction_results->Message,
                        'display_message'	=> (string) $transaction_results->Message,
                        'operator_id'		=> (string) $this->session->userdata('person_id'),
                        'cart_id'			=> empty($this->cart_id)?0:$this->cart_id,
                        'token'             => (string) $transaction_results->Token,
                        'avs_result' => (string) $transaction_results->AVSResult,
                        'batch_no' => (string) $transaction_results->BatchNo,
                        'cvv_result' => (string) $transaction_results->CVVResult,
                        'gratuity_amount' => (string) $transaction_results->GratuityAmount,
                    );

                    $this->sale->update_credit_card_payment($invoice_id, $payment_data);

                    if ((string) $transaction_results->Status != 'Approved') {
                        $this->response(array('success' => false, 'msg' => 'Credit card declined. ' .  $transaction_results->StatusMessage), 400);
                        return false;
                    }

				}else{ // ets saved payment
                    if(!empty($payment['credit_card_id'])){
                        $this->load->model('Customer_credit_card');
                        $credit_card = $this->Customer_credit_card->get_info($payment['credit_card_id']);
                        $token = $credit_card->token;

                    }else if(empty($payment['session_id']) || empty($payment['transaction_id'])){
                        $this->response(array('success' => false, 'msg' => 'Invalid card data. ' .  $transaction_results->StatusMessage), 400);
                        return false;
                    }

                    $this->load->library('Hosted_payments');
                    $hosted_payment = new Hosted_payments();
                    $hosted_payment->initialize($this->config->item('ets_key'));

                    $hosted_payment->set("action", "payment")
                        ->set("amount", $payment['amount'])
                        ->set("accountId", $token);

                    $ets_response = (array) $hosted_payment->send();
                    $ets_response['store'] = (array) $ets_response['store'];
                    $transaction = (array) $ets_response['transactions'];

                    $transaction_time = date('Y-m-d H:i:s', strtotime($ets_response['created']));

                    $approved = false;
                    if($ets_response['status'] == 'success'){
                        $approved = true;
                    }

                    $ets_card_type = $transaction['cardType'];
                    $card_type = '';

                    switch(strtoupper($ets_card_type)){
                        case 'MASTERCARD':
                            $card_type = 'M/C';
                            break;
                        case 'VISA':
                            $card_type = 'VISA';
                            break;
                        case 'DISCOVER':
                            $card_type = 'DCVR';
                            break;
                        case 'AMERICANEXPRESS':
                            $card_type = 'AMEX';
                            break;
                        case 'DINERS':
                            $card_type = 'DINERS';
                            break;
                        case 'JCB':
                            $card_type = 'JCB';
                            break;
                    }

                    $masked_account = (string) $transaction['cardNumber'];
                    $auth_code = (string) $transaction['approvalCode'];
                    $payment_type = $payment['type'];
                    $tran_type = 'Sale';

                    $payment_data = array(
                        'course_id' 		=> $this->session->userdata('course_id'),
                        'masked_account' 	=> $masked_account,
                        'trans_post_time' 	=> (string) $transaction_time,
                        'ets_id' 			=> $this->config->item('ets_key'),
                        'tran_type' 		=> $tran_type,
                        'amount' 			=> (string) $transaction['amount'],
                        'auth_amount'		=> (string)	$transaction['amount'],
                        'auth_code'			=> $auth_code,
                        'card_type' 		=> $card_type,
                        'frequency' 		=> 'OneTime',
                        'payment_id' 		=> (string) $transaction['id'],
                        'process_data'		=> (string) $ets_response['id'],
                        'status'			=> (string) $transaction['status'],
                        'status_message'	=> (string) $transaction['message'],
                        'cardholder_name'	=> (string) $transaction['name'],
                        'display_message'	=> (string) $ets_response['message'],
                        'operator_id'		=> (string) $this->session->userdata('person_id'),
                        'cart_id'			=> empty($this->cart_id)?0:$this->cart_id
                    );

                    $invoice_id = (int) $this->sale->add_credit_card_payment($payment_data);

                    if (!$approved) {
                        $this->response(array('success' => false, 'msg' => 'Credit card declined. ' .  $transaction_results->StatusMessage), 400);
                        return false;
                    }
                }
			break;
			
			case 'loyalty_points':
				
				$receipt = $this->table_receipt->get($this->suspended_sale_id, $receiptId);
				$receipt_total = $receipt['total_loyalty_dollars'];
				$receipt_points_total = $receipt['total_loyalty_cost'];

				$cur_payment_dollars = 0;
				$cur_payment_points = 0;
				$max_dollars_allowed = 0;

				// Check if a loyalty payment already exists for this customer
				$result = $this->db->select('payment_amount, number')
					->from('table_payments')
					->where(array(
						'customer_id' => (int) $payment['customer_id'],
						'payment_type' => 'Loyalty',
						'sale_id' => $this->suspended_sale_id,
						'receipt_id' => $receiptId
					))->get();
					
				$existing_payment = $result->row_array();
				if($existing_payment){
					$cur_payment_dollars = (float) $existing_payment['payment_amount'];
					$cur_payment_points = (float) $existing_payment['number'];
				}

				// Get customer's current loyalty data
				$this->db->select('loyalty_points, use_loyalty');
				$this->db->from('customers');
				$this->db->where('person_id', (int) $payment['customer_id']);
				$customer = $this->db->get()->row_array();

				if(empty($customer) || $customer['use_loyalty'] == 0){
					return false;
				}
				$customer['loyalty_points'] = (int) $customer['loyalty_points'];

				// If customer has enough points to cover entire sale
				if($customer && $customer['loyalty_points'] > $receipt_points_total){
					$max_dollars_allowed = $receipt_total;

				// If customer doesn't have enough points, calculate what amount
				// of dollars the customer's points translate to
				}else{
					$ratio = round($customer['loyalty_points'] / $receipt_points_total, 5);
					$max_dollars_allowed = round($ratio * $receipt_total, 2);
				}
				$max_dollars_allowed -= $cur_payment_dollars;
				$receipt_points_total -= $cur_payment_points;

				$payment['amount'] = min($payment['amount'], $max_dollars_allowed);
				$payment['type'] = 'Loyalty';
				$payment['number'] = min($customer['loyalty_points'], $receipt_points_total);
			break;

			case 'gift card':
				$this->load->model('Giftcard');

				if(empty($payment['card_number'])){
					$this->response(array('success'=>false, 'msg'=>'Gift card number is required'), 400);
					return false;
				}
				$giftcard_id = $this->Giftcard->get_giftcard_id($payment['card_number']);
				$payment['type'] = 'Gift Card:'.$payment['card_number'];
				// If gift card doesn't exist, or is expired, return error
				if(empty($giftcard_id) || $this->Giftcard->is_expired($giftcard_id)){
					$this->response(array('success'=> false, 'msg'=>lang('sales_giftcard_does_not_exist')), 400);
					return false;
				}

				$giftcard_info = $this->Giftcard->get_info($giftcard_id);
				$payment_type = $payment['type'].':'.$payment['card_number'];
				$cur_giftcard_value =  $giftcard_info->value;


				/** Get pending payments for giftcards $result */
				$result = $this->db->select('payment_amount')
					->from('table_payments')
					->where(array(
						'number' => $payment['card_number'],
						'payment_type' => $payment['type'],
						'sale_id' => $this->suspended_sale_id
					))->get();
				$existing_payment = $result->row_array();
				$existing_amount = 0;
				if ($existing_payment) {
					$existing_amount = (float)$existing_payment['payment_amount'];
				}
				$cur_giftcard_value-=$existing_amount;
				/**  */


				if ($cur_giftcard_value <= 0 && $payment['amount'] > 0){
					$this->response(array('success'=>false, 'msg'=>lang('sales_giftcard_balance_is').' '.to_currency($giftcard_info->value)), 400);
					return false;
				}

				if (($cur_giftcard_value - $payment['amount']) > 0){
					$msg = lang('sales_giftcard_balance_is').' '.to_currency($cur_giftcard_value);
				}
				$payment['amount']=min( $payment['amount'], $cur_giftcard_value );
				$payment['number'] = $payment['card_number'];
				$payment['gift_card'] = $giftcard_info;

			break;
			
			case 'creditcard-ets':
			case 'giftcard-ets':
				if(empty($payment['id'])){
					$this->response(array('success'=>false, 'msg'=>'Credit card session ID is required'), 400);
					return false;
				}
				if(empty($payment['created'])){
					$this->response(array('success'=>false, 'msg'=>'Created date is required'), 400);
					return false;
				}

				$tran_type = 'Sale';
				if(empty($payment['transactions']) && !isset($payment['merchant_data'])){
					$this->response(array('success'=>false, 'msg'=>'Credit card transaction data required'), 400);
					return false;
				
				}elseif(empty($payment['transactions'])&&$payment['merchant_data']['type']==='credit_card_save'){
					$tran_type = 'PreAuth';
					$transaction_id = $payment['merchant_data']['customers']['id'];
					$ets_card_type = $payment['merchant_data']['customers']['cardType'];
					$card_type = $this->cart_lib->standardize_credit_card_type($ets_card_type);
					$masked_account = (string) $payment['merchant_data']['customers']['cardNumber'];
					//$auth_code = (string) $payment['merchant_data']['customers']['approvalCode'];

					//Update credit card payment data
					$trans_message = (string) $payment['merchant_data']['customers']['message'];
					$amount = "0";
					$auth_amount = "0";
					$payment_id = (string) $payment['merchant_data']['customers']['id'];
					$status = (string) $payment['merchant_data']['customers']['status'];

					// Save credit card to database for later charges
                    $token = $payment['merchant_data']['customers']['id'];
					$this->load->model('Customer_credit_card');

						$card_data = [
							'recurring' => 0,
							'customer_id' => 0,
							'course_id' => $this->session->userdata('course_id'),
							'token' => $payment['merchant_data']['customers']['id'],
							'card_type' => $card_type,
							'masked_account' => $masked_account,
                            'ets_key' => $this->config->item('ets_key')
						];
						$credit_card_id = $this->Customer_credit_card->save($card_data);

                    $payment['params']['credit_card_id'] = (int) $credit_card_id;
                    $payment['credit_card_id'] = (int) $credit_card_id;
					$invoice_id = $payment['record_id'];
					
				}else{
					$transaction_id = $payment['transactions']['id'];

					// Convert card type to match mercury card types
					if ((string) $payment['transactions']['type'] == 'credit card') {
						$ets_card_type = $payment['transactions']['cardType'];
						$card_type = $this->cart_lib->standardize_credit_card_type($ets_card_type);

						$masked_account = (string) $payment['transactions']['cardNumber'];
						$auth_code = (string) $payment['transactions']['approvalCode'];
					}
					else if((string) $payment['transactions']['type'] == 'gift card')
					{
						$card_type = 'Giftcard';
						$masked_account = (string) $payment['transactions']['cardNumber'];
						$auth_code = '';
					}
					else
					{
						$card_type = 'Bank Acct';
						$masked_account = (string) $payment['transactions']['accountNumber'];
						$auth_code = '';
					}

                    //record_id/store primary === invoice_id, transaction_id === payment_id
					//Update credit card payment data
					$trans_message = (string) $payment['transactions']['message'];
					$amount = (string) $payment['transactions']['amount'];
					$auth_amount = (string)	$payment['transactions']['amount'];
					$payment_id = (string) $payment['transactions']['id'];
					$status = (string) $payment['transactions']['status'];
					$invoice_id = $payment['record_id'];
				}

				$this->load->model('sale');

				// Verify ETS transaction
				$this->load->library('Hosted_payments');
				$hosted_payment = new Hosted_payments();
				$hosted_payment->initialize($this->config->item('ets_key'));
				$session_id = $payment['id'];

				$hosted_payment->set("action", "verify")
					->set("sessionID", $session_id)
					->set("transactionID", $transaction_id)
					->set("invoice",$invoice_id);


				$account_id = $payment['customers']['id'];
				if ($account_id){
					$hosted_payment->set('accountID', $account_id);
				}
				
				$verify = $hosted_payment->send();
				$transaction_time = date('Y-m-d H:i:s', strtotime($payment['created']));

				$credit_card_data = array(
					'course_id' => $this->session->userdata('selected_course'),
					'card_type' => $card_type,
					'masked_account' => $masked_account,
					'cardholder_name' => ''
				);

				$payment_data = array (
					'course_id' 		=> $this->session->userdata('course_id'),
					'masked_account' 	=> $masked_account,
					'trans_post_time' 	=> (string) $transaction_time,
					'ets_id' 			=> $this->config->item('ets_key'),
					'tran_type' 		=> $tran_type,
					'amount' 			=> $amount,
					'auth_amount'		=> $auth_amount,
					'auth_code'			=> isset($auth_code)?$auth_code:'',
					'card_type' 		=> $card_type,
					'frequency' 		=> 'OneTime',
					'payment_id' 		=> $payment_id,
					'process_data'		=> (string) $payment['id'],
					'status'			=> $status,
					'status_message'	=> (string) $trans_message,
					'display_message'	=> (string) $payment['message'],
					'operator_id'		=> (string) $this->session->userdata('person_id')
				);


				if(!empty($token)){
					$payment_data['token'] = $token;
				}

				if(!empty($payment['credit_card_id'])){
					$invoice_id = (int) $this->sale->add_credit_card_payment($payment_data);
				}else{
					$invoice_id = (int) $payment['record_id'];;
					$this->sale->update_credit_card_payment($invoice_id, $payment_data);
				}
				
				// If transaction was not successful
				if((string)$verify->status != 'success'){
					$this->response(array('success'=>false, 'msg'=>'Credit card declined'), 400);
					return false;
				}				
				
				$this->session->set_userdata('invoice_id', $invoice_id);
				$payment_data['payment_type'] = $payment_data['card_type'].' '.$payment_data['masked_account'];
				
				// Data to return
				$payment['cardholder_name'] = '';
				if(!empty($payment_data['cardholder_name'])){
					$payment['cardholder_name'] = $payment_data['cardholder_name'];
				}
				$payment['tran_type'] = $tran_type;
				$payment['type'] = $payment_data['payment_type'];
				$payment['card_type'] = $card_type;
				$payment['card_number'] = $masked_account;
				$payment['number'] = $masked_account;
				$payment['credit_card_invoice_id'] = $invoice_id;
				$payment['amount'] = $payment_data['auth_amount'];
				$payment['auth_code'] = isset($auth_code)?$auth_code:'';

				$payment['params']['cardholder_name'] = $payment['cardholder_name'];
				$payment['params']['masked_account'] = $payment_data['masked_account'];
				$payment['params']['card_type'] = $payment_data['card_type'];
				$payment['params']['amount_refunded'] = 0;
			break;

            case 'creditcard-apriva-not being-used yet':
                $rd = array();
                $payment_data = array(
                    'course_id' => $this->config->item('course_id'),
                    'mercury_id' => $this->config->item('mercury_id'),
                    'tran_type' => '',//$rd['TransType'],
                    'amount' => '',//$amount,
                    'auth_amount' => $rd['Amount'],
                    'card_type' => $rd['CardType'],
                    'frequency' => 'OneTime',
                    'masked_account' => $rd['Last4'],
                    'cardholder_name' =>  $rd['Name'],
                    'terminal_name' => $rd['TerminalID'],
                    'trans_post_time' => gmdate('Y-m-d H:i:s'),
                    'auth_code' => $rd['AuthCode'],
                    'acq_ref_data' => $rd['ProcessorExtraData1'],
                    'process_data' => $rd['ProcessorExtraData2'],
                    'ref_no' => $rd['ProcessorExtraData3'],
                    'token' => $rd['CardToken'],
                    'response_code' => $rd['ResultCode'],
                    'status' => $rd['ResultCode'],
                    'status_message' => $rd['ReturnMessage'],
                    'display_message' => $rd['Message']
                );

                $this->sale->update_credit_card_payment($invoice_id, $payment_data);
                $payment_data['payment_type'] = $payment_data['card_type'] . ' ' . $payment_data['masked_account'];

                $payment['type'] = $payment_data['payment_type'];
                $payment['description'] = $rd['CardType'] . ' ' . $rd['Last4'];
                $payment['amount'] = $amount;
                $payment['record_id'] = (int)$payment['record_id'];
                $payment['cardholder_name'] = $rd['Name'];
                $payment['credit_card_invoice_id'] = (int)$payment['record_id'];
                $payment['card_number'] = $rd['Last4'];
                $payment['card_type'] = $rd['CardType'];
                $payment['auth_code'] = $rd['AuthCode'];
                $payment['number'] = $rd['Last4'];

                $payment['params']['cardholder_name'] = $rd['Name'];
                $payment['params']['masked_account'] = $rd['Last4'];
                $payment['params']['card_type'] = $rd['CardType'];
                $payment['params']['amount_refunded'] = 0;
            break;
            case 'creditcard-element':

				$this->load->model('v2/Sale_model');
				
				// If card is swiped with encrypted swiper
				if(isset($payment['merchant_data']['encrypted_track'])){
					
					$this->load->library('v2/Element_merchant');

					// Initialize payment library
					$this->element_merchant->init(
						new GuzzleHttp\Client()
					);

					// Send swiped card data to Element API
					$this->element_merchant->credit_card_sale(array(
						'amount' => abs($payment['amount']),
						'encrypted_track' => $payment['merchant_data']['encrypted_track']
					));
					$tran_type = 'Sale';
					
					if(!$this->element_merchant->success()){
						$this->response(array('success'=>false, 'msg'=>'Error: '.$this->element_merchant->message()), 400);
						return false;
					}

					$transaction_id = (string) $this->element_merchant
						->response()->xml()
						->Response->Transaction->TransactionID;

					// Get more details on the credit card just swiped
					$this->element_merchant->transaction_query(array(
						'transaction_id' => $transaction_id
					));
				
					$transaction_details = (array) $this->element_merchant->response()->xml()
						->Response->ReportingData->Items->Item;
					
					if($transaction_details['ExpressResponseMessage'] != 'Approved'){
						$approved = false;
					}else{
						$approved = true;
					}

					$cc_invoice_data = array(
						'tran_type' => $tran_type,
						'element_account_id' => $this->config->item('element_account_id'),
						'amount' => $transaction_details['ApprovedAmount'],
						'auth_amount' => $transaction_details['ApprovedAmount'],
						'payment_id' => $transaction_details['TransactionID'],
						'status' => $transaction_details['TransactionStatus'],
						'status_message' => $transaction_details['ExpressResponseMessage'],
						'masked_account' => substr((string) $transaction_details['CardNumberMasked'], -4, 4),
						'card_type' => $this->cart_lib->standardize_credit_card_type($transaction_details['CardLogo']),
						'auth_code' => $transaction_details['ApprovalNumber'],
						'response_code' => $transaction_details['ExpressResponseCode']
					);
					
					$invoice = $this->Sale_model->save_credit_card_payment(null, $cc_invoice_data);
				
				// If card is entered manually via hosted payments iframe
				}else{	

					$element_data = $payment['merchant_data'];
					$cc_invoice_data = array(
						'tran_type' => 'Sale',
						'element_account_id' => $this->config->item('element_account_id'),
						'amount' => $element_data['ApprovedAmount'],
						'auth_amount' => $element_data['ApprovedAmount'],
						'payment_id' => $element_data['TransactionID'],
						'status' => $element_data['HostedPaymentStatus'],
						'status_message' => $element_data['ExpressResponseMessage'],
						'masked_account' => $element_data['LastFour'],
						'card_type' => $this->cart_lib->standardize_credit_card_type($element_data['CardLogo']),
						'auth_code' => $element_data['ApprovalNumber'],
						'response_code' => $element_data['ExpressResponseCode']
					);
					
					$invoice = $payment['invoice_id'];
					
					if($cc_invoice_data['status'] != 'Complete'){
						$approved = false;
					}else{
						$approved = true;
					}
					
					$this->Sale_model->save_credit_card_payment($invoice, $cc_invoice_data);	
				}
				
				if(!$approved){
					$this->response(array('success'=>false, 'msg'=>'Error: '.$cc_invoice_data['status_message']), 400);
					return false;
				}				
				
				$payment['type'] = $cc_invoice_data['card_type'].' '.$cc_invoice_data['masked_account'];
				$payment['card_type'] = $cc_invoice_data['card_type'];
				$payment['card_number'] = $cc_invoice_data['masked_account'];
				$payment['cardholder_name'] = '';
				$payment['credit_card_invoice_id'] = $invoice;
				$payment['amount'] = $cc_invoice_data['auth_amount'];
				$payment['auth_code'] = $cc_invoice_data['auth_code'];
				$payment['number'] = $cc_invoice_data['masked_account'];							
			break;
			
			case 'creditcard-mercury':
                $this->load->model('Blackline_devices');
                $blackline_info = $this->Blackline_devices->get_terminal_info($this->session->userdata('terminal_id'));

                if($this->config->item('use_mercury_emv') && $blackline_info){

                    $rd = $payment['merchant_data'];
                    if ((string)$rd['ResultCode'] != 'Approved') {
                        $this->response(array('success' => false, 'msg' => 'Credit card declined. ' . $rd['Message']), 400);
                        return false;
                    }
                    $amount = (float)$rd['Amount'];
                    if ($rd['TransType'] == 'return') {
                        $amount = (float)0 - $amount;
                    }

                    $invoice_id = $payment['record_id'];
                    if ($rd['CardType'] == 'Discover') {
                        $rd['CardType'] = "DCVR";
                    }
                    else if ($rd['CardType'] == 'Amex') {
                        $rd['CardType'] = "AMEX";
                    }
                    else if ($rd['CardType'] == 'MasterCard') {
                        $rd['CardType'] = "M/C";
                    }
                    else if ($rd['CardType'] == 'Visa') {
                        $rd['CardType'] = "VISA";
                    }
                    $payment_data = array(
                        'course_id' => $this->config->item('course_id'),
                        'mercury_id' => $this->config->item('mercury_id'),
                        'tran_type' => $rd['TransType'],
                        'amount' => $amount,
                        'auth_amount' => $rd['Amount'],
                        'card_type' => $rd['CardType'],
                        'frequency' => 'OneTime',
                        'masked_account' => $rd['Last4'],
                        'cardholder_name' =>  $rd['Name'],
                        'terminal_name' => $rd['TerminalID'],
                        'trans_post_time' => gmdate('Y-m-d H:i:s'),
                        'auth_code' => $rd['AuthCode'],
                        'acq_ref_data' => $rd['ProcessorExtraData1'],
                        'process_data' => $rd['ProcessorExtraData2'],
						'ref_no' => $rd['ProcessorExtraData3'],
						'token' => $rd['CardToken'],
                        'response_code' => $rd['ResultCode'],
                        'status' => $rd['ResultCode'],
                        'status_message' => $rd['ReturnMessage'],
                        'display_message' => $rd['Message']
                    );

                    $this->sale->update_credit_card_payment($invoice_id, $payment_data);
                    $payment_data['payment_type'] = $payment_data['card_type'] . ' ' . $payment_data['masked_account'];

                    $payment['type'] = $payment_data['payment_type'];
                    $payment['description'] = $rd['CardType'] . ' ' . $rd['Last4'];
                    $payment['amount'] = $amount;
                    $payment['record_id'] = (int)$payment['record_id'];
                    $payment['cardholder_name'] = $rd['Name'];
                    $payment['credit_card_invoice_id'] = (int)$payment['record_id'];
                    $payment['card_number'] = $rd['Last4'];
                    $payment['card_type'] = $rd['CardType'];
                    $payment['auth_code'] = $rd['AuthCode'];
                    $payment['number'] = $rd['Last4'];

                    $payment['params']['cardholder_name'] = $rd['Name'];
                    $payment['params']['masked_account'] = $rd['Last4'];
                    $payment['params']['card_type'] = $rd['CardType'];
                    $payment['params']['amount_refunded'] = 0;
                
                }else {
                    // If credit card is declined for some reason
                    if ($payment['approved'] != 'true') {
						$ReturnMessage = !empty($payment['merchant_data']['ReturnMessage']) ? $payment['merchant_data']['ReturnMessage'] : '';
                        $this->response(array('success' => false, 'msg' => 'Credit card declined. ' . $ReturnMessage), 400);
                        return false;
                    }

                    // Data to return
                    $payment['type'] = $payment['description'];
                    $payment['card_type'] = $payment['card_type'];
                    $payment['card_number'] = $payment['card_number'];
                    $payment['number'] = $payment['card_number'];
                    $payment['cardholder_name'] = $payment['cardholder_name'];
                    $payment['credit_card_invoice_id'] = $payment['invoice_id'];
					if($payment['tran_type'] !== 'PreAuth') {
						$payment['amount'] = $payment['auth_amount'];
					}else{
                        $tran_type = "PreAuth";
						$payment['credit_card_id'] = $payment['params']['credit_card_id'];
					}
                    $payment['auth_code'] = $payment['auth_code'];
                }
			break;

			case 'account_balance':
			case 'member_balance':

				if(empty($payment['customer_id'])){
					$this->response(array('success'=>false, 'msg'=>'Customer ID is required for member balance payment'), 400);
					return false;
				}
				$customer_info = $this->Customer->get_info($payment['customer_id'], $this->session->userdata('course_id'));
			    $payment['record_id'] = (int) $payment['customer_id'];
				$cur_balance = 0;
				if($payment['type'] == 'member_balance'){
					$account_name = lang('customers_member_account_balance');
					if($this->config->item('member_balance_nickname')){
						$account_name = $this->config->item('member_balance_nickname');
					}
					$cur_balance = (float) $customer_info->member_account_balance;

					if(!empty($payment['signature'])){
						$this->load->model('v2/Cart_model');
						
						$payment['signature'] = $this->cart_lib->save_signature(
							$payment['signature'], [
								'type' => $payment['type'],
								'record_id' => $payment['customer_id']
							], $this->suspended_sale_id
						);
					}

				}else{
					$account_name = lang('customers_account_balance');
					if($this->config->item('customer_credit_nickname')){
						$account_name = $this->config->item('customer_credit_nickname');
					}
					$cur_balance = (float) $customer_info->account_balance;
				}
				$account_name .= ' - '.$customer_info->last_name.', '.$customer_info->first_name;	

				// Retrieve any payments already made for this table on this account
				$cur_payment = $this->Table->get_payments($this->suspended_sale_id, $receiptId, $account_name);
				$cur_payment_amount = 0;
				if(!empty($cur_payment['payment_amount'])){
					$cur_payment_amount = (float) $cur_payment['payment_amount'];
				}
				$payment['amount'] += $cur_payment_amount;

				// If using account balance payment, check if customers are limited to certain items
				if($payment['type'] == 'account_balance'){
					
					$this->load->model('v2/Course_model');
					$restriction = $this->Course_model->get_customer_account_spending_restriction();

                    // If we're restricting customer credit to subtotal only. Calculate the subtotal
                    $subtotal_only = $this->config->item('credit_subtotal_only');

                    if (!$restriction && $subtotal_only) {
                        // Skip this if we have restrictions because the restrictions will take care of calculating the subtotal
                        $receipt = $this->table_receipt->get($this->suspended_sale_id, $receiptId);
                        $valid_total = 0;
                        foreach ($receipt['items'] as $item) {
                            $valid_total += $item['subtotal'];
                        }
                        $payment['amount'] = min($payment['amount'], $valid_total);
                    } elseif($restriction){
						$receipt = $this->table_receipt->get($this->suspended_sale_id, $receiptId);
                        $total_field = ($subtotal_only) ? 'subtotal' : 'total';
						$valid_total = $this->cart_lib->get_filtered_item_total($receipt['items'], $restriction['field'], $restriction['value'], $total_field);

						if($valid_total == 0){
							$this->response(array('success' => false, 'msg' => 'No valid items on receipt'), 400);
							return false;
						}
						$payment['amount'] = min($payment['amount'], $valid_total);
					}
				}
				
				// Check if account allows negative balance
				$allow_negative = false;
				$account_limit = 0;
				if($payment['type'] == 'member_balance' && $customer_info->member_account_balance_allow_negative == 1){
					$allow_negative = true;
					$account_limit = (float) $customer_info->member_account_limit;
				
				}else if($payment['type'] == 'account_balance' && $customer_info->account_balance_allow_negative == 1){
					$allow_negative = true;
					$account_limit = (float) $customer_info->account_limit;
				}

				// If there is no balance on account, return error
				if($payment['amount'] > 0 && (
					($cur_balance <= 0 && !$allow_negative) || ($account_limit != 0 && $cur_balance <= $account_limit)
				)){
					$this->response(array('success'=>false, 'msg'=>'Insufficient balance on account'), 400);
					return false;
				}

				// If there is some balance, but not enough, set the payment to the remaining balance
				if(
					(!$allow_negative && $payment['amount'] > $cur_balance) || 
					($account_limit != 0 && $payment['amount'] > ($cur_balance - $account_limit))
				){
					$msg = 'Insufficient account funds for entire payment. Maximum funds applied.';
					$payment['amount'] = $cur_balance - $account_limit;
				}

				// Subtract any existing payments from this account (because they are added together when inserting into the database)
				$payment['amount'] -= $cur_payment_amount;
				$payment['type'] = $account_name;
			break;

			default:
                $this->load->model('custom_payments');
                //Check if it's a custom payment
                $payment_type = $this->custom_payments->get_payment_type($payment['type']);
                if ($payment_type) {
                    $payment['type'] = $payment_type;
                }
                else {
                    $this->response(array('success' => false, 'msg' => 'Payment type is invalid'), 400);
                    return false;
                }
			break;
		}
		
		if(empty($payment['customer_id'])){
			$payment['customer_id'] = 0;
		}
		if(empty($payment['credit_card_invoice_id'])){
			if(isset($payment['merchant_data']) && !empty($payment['merchant_data']['credit_card_invoice_id']))
				$payment['credit_card_invoice_id'] = $payment['merchant_data']['credit_card_invoice_id'];
			else $payment['credit_card_invoice_id'] = 0;
		}
		if(empty($payment['auth_code'])){
			if(isset($payment['merchant_data']) && !empty($payment['merchant_data']['auth_code']))
				$payment['auth_code'] = $payment['merchant_data']['auth_code'];
			else $payment['auth_code'] = 0;
		}
		if(empty($payment['number'])){
			$payment['number'] = '';
		}
		if(empty($payment['trans_type'])){
			$payment['trans_type'] = null;
		}
		if(empty($payment['signature'])){
			$payment['signature'] = null;
		}
		if(empty($payment['customer_note'])){
			$payment['customer_note'] = null;
		}

		$customer_note = $payment['customer_note'];

		$this->session->set_userdata('taxable', true);
		$success = $this->Table->add_payment(
			$this->suspended_sale_id, 
			$receiptId, 
			$payment['type'], 
			$payment['amount'], 
			$payment['customer_id'], 
			$payment['credit_card_invoice_id'], 
			$payment['number'],
			$payment['signature'],
			isset($payment['tran_type'])?$payment['tran_type']:null,
			isset($payment['auth_code'])?$payment['auth_code']:null,
			isset($payment['credit_card_id'])?$payment['credit_card_id']:null
		);
		
		// If payment was successfully added
		if($success){
			if(!empty($payment['credit_card_invoice_id']))
			{
				//Release the lock
				$payment_lock = new \models\payment_lock();
				$payment_lock->init_by_invoice($payment['credit_card_invoice_id']);
				$payment_lock->release_lock();
			}

			$sale_id = false;
			$sale_number = false;
			$payment['type'] = $success;
			
			$newPayment = $this->Table->get_payments($this->suspended_sale_id, $receiptId, $payment['type']);
			$returnData = array(
				'type' => $payment['type'],
				'amount' => !empty($newPayment['payment_amount']) ? (float) $newPayment['payment_amount'] : 0,
				'auth_code' => $payment['auth_code']
			);

			if(!empty($payment['card_type'])){
				$returnData['card_type'] = $payment['card_type'];
			}
			if(!empty($payment['card_number'])){
				$returnData['card_number'] = $payment['card_number'];
			}
			if(!empty($payment['gift_card'])){
				$returnData['gift_card'] = $payment['gift_card'];
			}
			if(!empty($payment['credit_card_id'])){
                $credit_card_id = $payment['credit_card_id'];
                $returnData['credit_card_id'] = $payment['credit_card_id'];
            }

			// Retrieve all data for receipt (items, payments, etc)
			$receiptData = $this->table_receipt->get($this->suspended_sale_id, $receiptId);
			
			// If receipt is now fully paid, complete the sale
			if(isset($receiptData['total_due']) && $receiptData['total_due'] <= 0 && isset($receiptData['items']) && count($receiptData['items'])) {

				$this->load->model('v2/Sale_model');

				// If receipt is overpaid, add 'Change issued' payment
				if ($receiptData['total_due'] < 0) {
					
					$changePayment = array(
						'type' => 'Change issued',
						'amount' => $receiptData['total_due'],
						'credit_card_invoice_id' => 0
					);
					
					$this->Table->add_payment(
						$this->suspended_sale_id, 
						$receiptId, 
						$changePayment['type'], 
						$changePayment['amount'], 
						$changePayment['credit_card_invoice_id']
					);

					// Manually add payment to current receipt data
					$receiptData['payments'][] = $changePayment;
				}
				$this->load->model('Sale');
				$this->load->model('Customer_loyalty');
				$this->table_receipt->mark_paid($this->suspended_sale_id, $receiptId);

				$customer_id = -1;
				if (!empty($receiptData['customer']['person_id'])) {
					$customer_id = (int)$receiptData['customer']['person_id'];
				}

				// Loop through payments, if payment is associated with customer
				// associate them with the sale too
				foreach ($receiptData['payments'] as $payment) {
					if (!empty($payment['customer_id'])) {
						$customer_id = (int)$payment['customer_id'];
					}
				}

				// Gather up receipt data to complete the sale
				$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
				$comment = '';
				$payments = array();
				$items = array();
				$sideTypes = array('soups', 'salads', 'sides');
				$comps = array();

				if (!empty($receiptData['service_fee'])){	
					$receiptData['items'][] = $receiptData['service_fee'];
				}

				// Loop through items, add split prices as the default price
				// Split off side dishes as their own items
				$lineCount = 1;
				if (isset($receiptData['items']) && is_array($receiptData['items'])){
					foreach ($receiptData['items'] as $key => $itemData) {

						$item = $itemData;

						if (!empty($itemData['comp'])) {
							$comp = $itemData['comp'];
							$comp['line'] = (int)$lineCount;
							$comp['dollar_amount'] = (float)$itemData['split_comp_total'];
							$comp['item_id'] = (int)$itemData['item_id'];
							$comps[] = $comp;
						}

						// Overwrite regular price with split price on all items
						$item['total'] = $item['split_total'];
						$item['price'] = $item['split_price'];
						$item['subtotal'] = $item['split_subtotal'];
						$item['line'] = $lineCount;

						// Since side is being sold on its own, make sure we denote that
						// it was not sold as a side item
						$item['is_side'] = 0;

						// If receipt is not taxable, clear taxes
						if ($item['taxable']) {
							$item['tax'] = $item['split_tax'];
						} else {
							$item['tax'] = 0;
							unset($item['taxes']);
						}

						$items[] = $item;
						$lineCount++;

						// Loop through the various side dishes associated with each item
						foreach($sideTypes as $sideType){

							if(!empty($item[$sideType])){

								foreach($item[$sideType] as $side){
									// If side is 'none' skip it
									if ((int)$side['item_id'] == 0) {
										continue;
									}

									$side['line'] = $lineCount;
									$side['total'] = $side['split_total'];
									$side['price'] = $side['split_price'];
									$side['subtotal'] = $side['split_subtotal'];
									$side['unit_price_includes_tax'] = 0;

									// If receipt is not taxable, clear taxes
									if ($side['taxable']) {
										$side['tax'] = $side['split_tax'];
									} else {
										$side['tax'] = 0;
										unset($side['taxes']);
									}

									$items[] = $side;

									unset($items[$lineCount - 1][$sideType]);
									$lineCount++;
								}
							}
						}

						if(!empty($item['service_fees'])){
							foreach($item['service_fees'] as $service_fee){

								$service_fee['line'] = $lineCount;
								$service_fee['total'] = $service_fee['split_total'];
								$service_fee['price'] = $service_fee['split_price'];
								$service_fee['subtotal'] = $service_fee['split_subtotal'];
								$service_fee['unit_price_includes_tax'] = 0;

								// If receipt is not taxable, clear taxes
								if($service_fee['taxable']){
									$service_fee['tax'] = $service_fee['split_tax'];
								}else{
									$service_fee['tax'] = 0;
									unset($service_fee['taxes']);
								}
								
								$items[] = $service_fee;
								$lineCount++;
							}
						}
					}
				}

				// this is to make sure that $invoice id propagates from saved payments
                if(empty($invoice_id))
                    $invoice_id = isset($payment['credit_card_invoice_id'])?$payment['credit_card_invoice_id']:null;

                // Conform payments to play nice with sale->save method
                $new_payments = array();
				foreach($receiptData['payments'] as $key => $payment){
                    if(isset($payment['tran_type']) && $payment['tran_type']==="PreAuth")continue;
				    // make sure we show the type and masked number in reports,
                    // rather than 'Credit Card *'
                    if ($this->validate_processor_payment($payment) && strpos($payment['type'],'Credit Card')!==false) {
                        $payment['type'] = $credit_card->card_type . ' ' . $credit_card->masked_account;
                    }
					
					// Get proper payment type key used for storing in database
					$payments[$key]['type'] = $this->Sale_model->generate_payment_type($payment['type']);
					
                    $payments[$key]['payment_type'] = ucfirst($payment['type']);
                    $payments[$key]['payment_amount'] = $payment['amount'];
                    $payments[$key]['invoice_id'] = 0;

                    // this is to make sure that invoice id propagates from saved payments
                    // so that tips can be added
                    $payments[$key]['invoice_id'] =
                        isset($payment['credit_card_invoice_id']) &&
                        $payment['credit_card_invoice_id'] ?
                            $payment['credit_card_invoice_id'] :
                            $invoice_id;

					$customer_account = 'Customer Credit';
					$member_account = 'Member Balance';

					if($this->config->item('customer_credit_nickname')){
						$customer_account = $this->config->item('customer_credit_nickname');
					}
					if($this->config->item('member_balance_nickname')){
						$member_account = $this->config->item('member_balance_nickname');
					}

					if((stripos($payment['type'], $member_account) !== false || stripos($payment['type'], $customer_account) !== false ||
							$payment['type'] == 'member_account' || $payment['type'] == 'customer_account' ||
							$payment['type'] == 'member_account_tip' || $payment['type'] == 'customer_account_tip') &&
						empty($payment['record_id'])){
						$payment['record_id'] = (int) $payment['customer_id'];
					}else {
						$payments[$key]['record_id'] = $payments[$key]['invoice_id'];
					}

					if($payments[$key]['type'] == 'credit_card'){
                        $payments[$key]['number'] = isset($credit_card)?$credit_card->masked_account:explode(' ',$payment['type'])[1];
                    }

                    if(isset($payment['customer_id'])) {
                        $payments[$key]['customer_id'] = $payment['customer_id'];
                    }
                    if(!empty($payment['signature'])) {
                        $payments[$key]['signature'] = $payment['signature'];
                    }

                    if($payment['type'] == 'Loyalty' && (int) $payment['number'] > 0) {
                        $payments[$key]['loyalty_point_value'] = (int) $payment['number'];
                    }
                    $new_payments[] = $payments[$key];
				}
				$payments = $new_payments;

				$this->session->set_userdata('purchase_override', (int) $this->manager_auth_id);

				// Complete sale (places all data in sales table)
				$this->Sale->guest_count = $this->guest_count;

				if(!empty($customer_note)){
					$this->Sale->customer_note = $customer_note;
				}

				// Always force the receipt to taxable so the Sale->save method doesn't remove our taxes
				// This is required to accomodate force tax for individual items
				// Talk to Jordan if you are confused by this. If Jordan is no longer employed at foreUP
				// then may God have mercy on your soul.
				$receiptData['taxable'] = 1;

				$sale_id = $this->Sale->save($items, $customer_id, $employee_id, $comment, 
					                         $payments, false, false, false, null, false, 
					                         $this->table_num, false, 
					                         $receiptData['auto_gratuity_amount'], 
					                         $receiptData['taxable'], 0, false);
				if($sale_id === -1)$sale_id = false;
				else{
					$this->table_receipt->setCompletedSale($this->suspended_sale_id,$receiptData['receipt_id'], $sale_id);
				}

				$sale_number = false;
				if(isset($this->Sale->sale_number))
				    $sale_number = $this->Sale->sale_number;

				$receipt_data = array();
				if(isset($this->Sale->additional_receipt_data))
				    $receipt_data = $this->Sale->additional_receipt_data;
				if(!empty($receipt_data['loyalty'])){
					$returnData['loyalty'] = $receipt_data['loyalty'];
				}
				
				// Update inventory deductions with new sale ID
				$this->Inventory->update_sale_id($this->suspended_sale_id, $sale_id);

				// Save any comps attached to items over to sale table
				$this->Item_comp->save_to_sale($sale_id, $comps);
				$this->session->unset_userdata('purchase_override');
			}

			$returnData['sale_id'] = $sale_id;
			$returnData['number'] = $sale_number;

			// Format list of items from receipt as array
			$items = array();
			if (isset($receiptData['items']) && is_array($receiptData['items'])) {
				foreach ($receiptData['items'] as $item) {
					if (empty($item['line'])) {
						continue;
					}
					$items[] = (int)$item['line'];
				}
			}

			if(!empty($msg)){
				$returnData['msg'] = $msg;
			}
			$returnData['credit_card_id'] = empty($credit_card_id)?null:$credit_card_id;

			// Mark all items in cart (that are in receipt) as paid
            if(!isset($payment['tran_type']) || $payment['tran_type']!=="PreAuth")
			    $this->Table->mark_items_paid($this->suspended_sale_id, $items);
			$this->response($returnData);

		}else{
			$this->response(array('success'=>$success));
		}
	}

	function delete_receipt_sale($receiptId, $reason=''){
		$reason = $this->_delete_args['reason'];
		$receiptData = $this->table_receipt->get($this->suspended_sale_id, $receiptId);
		if(isset($receiptData['completed_sale_id'])){
			$this->db->update('sales', array('deleted' => 1,'refund_reason'=>$reason), array('sale_id' => (int) $receiptData['completed_sale_id']));

		}
	}

	function receipt_payments_put($receiptId, $paymentType = null)
	{
		return $this->receipt_payments_post($receiptId);
	}

	function receipt_payments_delete($receiptId, $paymentType = null)
	{
		$itemData = $this->request->body;
		$paymentType = urldecode($paymentType);
		$paymentType = str_replace('-', '/', $paymentType);
		
		// get our plain-Jane parens back after all the fiddling 
		$paymentType = str_replace('&#40;', '(', $paymentType);
		$paymentType = str_replace('&#41;', ')', $paymentType);

		// Get payment data
		$paymentData = $this->Table->get_payments($this->suspended_sale_id, $receiptId, $paymentType);
        $refundSuccess = true;

        // If payment being deleted is from credit card, refund the payment back to card
		if(!empty($paymentData['credit_card_invoice_id']) && stripos($paymentType, 'Gift') === false && $paymentData['tran_type']!== 'PreAuth'){

            $returnable = $this->Table->too_late_to_return($this->suspended_sale_id);
            if (!$returnable) {
                $this->response(
                    array(
                        'success'=>false,
                        'msg'=>'This table has an incomplete payment on it that is older than 24 hours. In order to avoid an incorrect credit to the card, please call foreUP Support for further assistance closing this table.'
                    ));
                return false;
            }

            $refundSuccess = false;
			$errorMessage = 'Error refunding credit card payment';

			$this->load->model('sale');
			$cc_payment = $this->sale->get_credit_card_payment($paymentData['credit_card_invoice_id']);
			$cc_payment = $cc_payment[0];
            // Removed gratuity amount because it is included in the auth amount of the payment
			$amount = $cc_payment['auth_amount'];// + $cc_payment['gratuity_amount'];

            $this->load->model('Payment_return');
            $return_log_data = array(
                'table_id' => $this->suspended_sale_id,
                'note' => 'food_and_beverage->receipt_payments_delete',
                'invoice_id' => $paymentData['credit_card_invoice_id'],
                'employee_id' => $this->session->userdata('person_id')
            );
            $this->Payment_return->save($return_log_data);

			// If course is using ETS for payments
			if($this->config->item('ets_key')){

				// If payment hasn't been refunded already
				if ($cc_payment['amount_refunded'] <= 0){

					if($cc_payment['process_data'] != ''){
						$this->load->library('Hosted_payments');
						$hosted_payment = new Hosted_payments();
						$hosted_payment->initialize($this->config->item('ets_key'));

						if($cc_payment['card_type'] == 'Giftcard'){
							$ets_response = $hosted_payment->set('posAction', 'refund')
									->set('amount', $amount)
									->set('sessionID', $cc_payment['process_data'])
									->send();
							$errorMessage = 'Error refunding gift card payment';
						}else{
							$ets_response = $hosted_payment->set('action', 'void')
									->set('transactionID', $cc_payment['payment_id'])
									->set('sessionID', $cc_payment['process_data'])
									->send();
						}

						if($ets_response->status == 'success'){
							$refundSuccess = true;
							$sql = $this->sale->add_ets_refund_payment($paymentData['credit_card_invoice_id'], $amount);

						}else{
							if(!empty($ets_response->message)){
								$messages = '';
								foreach($ets_response->message as $message){
									$messages .= implode(', ', $message);
								}
								$errorMessage .= ' - '.$messages;
							}
						}
					}
				}else{
					$refundSuccess = true;
				}

			// If course is using Mercury for payments
			}else if($this->config->item('mercury_id')){

				$this->load->library('Hosted_checkout_2');
				$HC = new Hosted_checkout_2();
				$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
				$HC->set_invoice($paymentData['credit_card_invoice_id']);
				$HC->set_cardholder_name($cc_payment['cardholder_name']);
				$HC->set_token($cc_payment['token']);
				$response = $HC->issue_refund($paymentData['credit_card_invoice_id'], $amount);

				if ((int) $response->ResponseCode === 0 && $response->Status == 'Approved'){
					$refundSuccess = true;
					$sql = $this->Sale->add_refund_payment($paymentData['credit_card_invoice_id'], $amount, $response);
				}
			
			}else if($this->config->item('element_account_id')){
				$this->load->model('v2/Cart_model');
				$refundSuccess = $this->Cart_model->refund_credit_card($paymentData['credit_card_invoice_id']);
			}
			
		}else if (empty($paymentData['credit_card_invoice_id'])){
			$paymentType = str_replace('/', '-', $paymentType);
		}

		if(!$refundSuccess){
			$this->response(array('success'=>false, 'msg'=>$errorMessage), 500);
			return false;
		}

		$success = $this->Table->delete_payment($this->suspended_sale_id, $receiptId, $paymentType);

		if($success){
			$receiptData = $this->table_receipt->get($this->suspended_sale_id, $receiptId);

			// If all payments were deleted from receipt, mark items in cart as UNPAID (that belong to receipt)
			if(isset($receiptData['items']) && count($receiptData['items']) &&
                    (empty($receiptData['amount_paid']) || $receiptData['amount_paid'] <= 0)){
				$items = array();
				foreach($receiptData['items'] as $item){
					$items[] = (int) $item['line'];
				}
				// Mark all items in cart (that are in receipt) as unpaid
                $this->Table->mark_items_unpaid($this->suspended_sale_id, $items);
			}
		}

		$this->response(array('success'=>true, 'msg'=>'Payment deleted'));
	}

    /*******************************************************************
     * Cart
	 ******************************************************************/
	function cart_get($line)
	{
		session_write_close();

		// If getting all items in cart
		if(empty($line)){
			$result = $this->Table->get_sale_items($this->suspended_sale_id);
			$rows = $result->result_array();
			$items = array();

			// Loop through rows and conform them to proper item structure
			foreach($rows as $row){
				$item = array(
					'item_id' => $row['item_id'],
					'line' => $row['line'],
					'description' => $row['description'],
					'serialnumber' => $row['serialnumber'],
					'seat' => $row['seat'],
					'quantity' => $row['quantity_purchased'],
					'cost' => $row['item_cost_price'],
					'price' => $row['item_unit_price'],
					'discount' => $row['discount_percent']
				);

				$items[] = $item;
			}
			$this->response($items);

		// If getting just one item
		}else{
			$this->response(array('TODO: data here'));
		}
	}

	function cart_post($line)
	{
		$itemData = $this->post();
		$result = $this->Table->save_item($this->suspended_sale_id, $itemData);
		$this->response(array('msg'=>'Post cart'));
	}

	function cart_put($line)
	{
		$msg = 'Item added to cart';
		$itemData = $this->request->body;

		$item = $this->Item->get_info((int) $itemData['item_id']);
		$saleItem = $this->Table->get_item($line, $this->suspended_sale_id);

		$minPrice = $this->sale_lib->calculate_subtotal($item->unit_price, 1, $item->max_discount);
		$setPrice = $this->sale_lib->calculate_subtotal($itemData['price'], 1, $itemData['discount']);
		
		if(empty($saleItem['quantity'])){
			$saleItem['quantity'] = 0;
		}
		
		$inventoryChange = (int) $itemData['quantity'] - (int) $saleItem['quantity'];
		$adjustedInventory = $item->quantity - $inventoryChange;

		// Validate the item price
		if($setPrice < $minPrice){
			$this->response(array('success'=>false, 'msg'=>'Error saving item. Maximum discount is ' . $item->maxDiscount. '% or '. '$'.$minPrice), 400);
			return false;
		}

		// If there is insufficient inventory, return error
		if($adjustedInventory < 0 && $item->is_unlimited == 0){
			$this->response(array('success'=>false, 'msg'=>'Error saving "'.$item->name.'", insufficient inventory available.'), 400);
			return false;
		}

		// If inventory is low, warn the user
		$warning = false;
		if($item->is_unlimited == 0){
			if($adjustedInventory <= $item->reorder_level){
				$warning = 'Warning, '.($item->quantity - $inventoryChange).' "'.$item->name.'" remain in inventory';
			}
		}

		// Adjust inventory for item
		$this->Item->adjust_inventory($itemData['item_id'], -$inventoryChange);

		$inv_data = array(
			'trans_date' => date('Y-m-d H:i:s'),
			'trans_items' => $itemData['item_id'],
			'trans_user' => $this->employee_id,
			'trans_comment' => 'F&B item order, table #'.$this->table_num,
			'trans_inventory' => -$inventoryChange,
            'course_id'=>$this->session->userdata('course_id'),
            'suspended_sale_id'=>$this->suspended_sale_id
		);
		$this->Inventory->insert($inv_data);

		// Save item to table cart
		$success = $this->Table->save_item($this->suspended_sale_id, $line, $itemData);

		// If item is new and not currently on a receipt
		// Automatically place item on next available receipt
		if(!$this->table_receipt->is_on_receipt($this->suspended_sale_id, $line)){
			$receipt_id = $this->table_receipt->get_available_receipt($this->suspended_sale_id);
			$this->table_receipt->add_item($itemData['item_id'], $itemData['line'], $this->suspended_sale_id, $receipt_id);
		}

		$response = array('success' => $success, 'msg' => $msg);
		if($warning){
			$response['warning'] = $warning;
		}

		$this->response($response);
	}

	function cart_delete($line)
	{
		session_write_close();
		$saleItem = $this->Table->get_item($line, $this->suspended_sale_id);

		$item_id = 0;
		if(isset($saleItem['item_id']))
		    $item_id = (int) $saleItem['item_id'];

		// Credit inventory back for item
		if(isset($saleItem['quantity']))
		  $this->Item->adjust_inventory($item_id, $saleItem['quantity']);

		$inv_data = array(
			'trans_date' => date('Y-m-d H:i:s'),
			'trans_items' => $item_id,
			'trans_user' => $this->employee_id,
			'trans_comment' => 'F&B item order, table #'.$this->table_num,
			'trans_inventory' => isset($saleItem['quantity'])?$saleItem['quantity']:0,
            'course_id'=>$this->session->userdata('course_id'),
            'suspended_sale_id'=>$this->suspended_sale_id
		);
		$this->Inventory->insert($inv_data);

		$success = $this->Table->delete_item($this->suspended_sale_id, $line);
		$this->response(array('success'=>$success, 'msg'=>'Item removed from cart'));
	}

    /*******************************************************************
     * Kitchen Order - Post item line numbers to have them sent to kitchen
	 ******************************************************************/
	function orders_get($order_id)
	{
		$this->response(array('msg'=>'TODO: get order data'));
	}

	function orders_post($order_id)
	{
		session_write_close();
		$orderData = $this->request->body;
		if(empty($orderData['items'])){
			$this->response(array('msg'=>'No items selected'), 400);
			return false;
		}
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		
		$ticket_id = $this->table_ticket->save(
			$this->suspended_sale_id, 
			$this->table_num, 
			$employee_id, 
			$orderData['items'],
			$orderData['message'],
			(int) $orderData['meal_course_id']
		);

		if(empty($ticket_id)){
			$this->response(array('success'=>false, 'msg'=>'Error placing order'), 500);
			return false;
		}

		$lines = array();
		foreach($orderData['items'] as $item){
			$lines[] = $item['line'];
		}
		$this->Table->mark_items_ordered($this->suspended_sale_id, $lines);

		// Get newly created ticket data to respond with
		$ticket = $this->table_ticket->get($this->suspended_sale_id, $ticket_id);
		unset($ticket['items']);

		$this->response($ticket);
	}

	function orders_put($order_id)
	{
		$itemData = $this->request->body;
		$this->response(array('msg'=>'TODO: edit order'));
	}

	function orders_delete($order_id)
	{
		session_write_close();
		$success = $this->Table->delete_item($this->suspended_sale_id, $line);
		$this->response(array('success'=>$success, 'msg'=>'Item removed from cart'));
	}

    /*******************************************************************
     * Table Customers - Handles customers attached to a F&B sale
	 ******************************************************************/
	function customers_get($customer_id)
	{
		$itemData = $this->request->body;
		$this->response(array('msg'=>'TODO: Get customer'));
	}

	// Saves a customer to the table
	function customers_post($customer_id)
	{
		session_write_close();
		$success = $this->Table->save_customer($this->suspended_sale_id, $customer_id);
		$this->response(array('success' => $success));
	}

	function customers_put($customer_id){
		$this->customers_post($customer_id);
	}

	// Remove a customer from a table
	function customers_delete($customer_id)
	{
		session_write_close();
		$success = $this->Table->delete_customer($this->suspended_sale_id, $customer_id);
		$this->response(array('success' => $success));
	}
	
    /*******************************************************************
     * Reservations - Handles restaurant reservation CRUD
	 ******************************************************************/
	function reservations_get(){
		session_write_close();
		$params = $this->input->get();
		$this->load->model('v2/Restaurant_reservation_model', 'Reservations');
		$reservations = $this->Reservations->get($params);
		
		$this->response($reservations, 200);
	}

	// Saves a new reservation
	function reservations_post(){
		session_write_close();
		$data = $this->request->body;
		$this->load->model('v2/Restaurant_reservation_model', 'Reservations');
		$success = $this->Reservations->save(null, $data);
		
		if(!$success){
			$this->response(array('msg' => 'Error saving reservation'), 500);
		}else{
			$this->response(array('reservation_id' => $success), 201);
		}
	}
	
	// Update an existing reservation
	function reservations_put($reservation_id){
		session_write_close();
		$this->load->model('v2/Restaurant_reservation_model', 'Reservations');
		$data = $this->request->body;
		$success = $this->Reservations->save($reservation_id, $data);
		
		if(!$success){
			$this->response(array('msg' => 'Error saving reservation'), 500);
		}else{
			$this->response(array('reservation_id' => $success), 200);
		}
	}

	// Mark reservation as deleted
	function reservations_delete($reservation_id){
		session_write_close();
		$this->load->model('v2/Restaurant_reservation_model', 'Reservations');
		$success = $this->Reservations->delete($reservation_id);
		
		if(!$success){
			$this->response(array('msg' => 'Error deleting reservation'), 500);
		}else{
			$this->response(array('reservation_id' => $success), 200);
		}
	}

	/*******************************************************************
    * Merge Table - Merge an existing table into another table (destination)
	******************************************************************/	
	function merge_post($recordId = null){

		$destination = $this->input->post('destination');

		if($this->table_num == $destination){
			if(!$success){
				$this->response(array('msg' => 'Please select a different table than the source table'), 400);
				return false;
			}			
		}

		$success = $this->Table->merge($this->table_num, $destination);

		if(!$success){
			$this->response(array('msg' => 'Error merging tables'), 500);
			return false;
		}

		$this->response(['success' => true, 'destination' => $destination], 200);
	}
}