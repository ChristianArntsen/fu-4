<?php
class Report_Tags extends CI_Controller {

    function __construct(){

        parent::__construct();
        $this->load->model('Report_tag');
        $this->CI = & get_instance();
    }

    public function create(){
        $json = file_get_contents('php://input');
        $id = $this->Report_tag->create($json);

        print json_encode(["id"=>$id]);
    }

    public function index(){
        $reports = $this->Report_tag->get_all();
        print json_encode($reports);
    }


}
