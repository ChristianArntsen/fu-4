<?php
// Booking API to provide REST API functionality for online booking,
// Used internally (but could be used externally) for Backbone JS
require_once(APPPATH.'libraries/REST_Controller.php');

class Emailcampaign extends REST_Controller
{
	function __construct()
	{
		parent::__construct();
		require 'application/vendor/autoload.php';
		$this->load->model('Marketing_Campaign');

		$this->load->model('course');
	}


	/*
	 * An endpoint so phantomjs can access a campaign without being the real user.
	 * Only users from 127.0.0.1 can call the phantomJSGet
	 */
	function phantomJS_get()
	{
		$campaign_id = $this->input->get('campaign_id');
		$campaign = $this->Marketing_Campaign->phantomJSGet($campaign_id);
		if($campaign)
			$campaign = $campaign[0];
		$course_info = $this->course->get_info($campaign['course_id']);

		$template = $campaign['json_content'];
		$this->load->view('marketing_campaigns/campaign_builder/app',array("prefilledTemplate"=>$template,"logo"=>$campaign['logo'],"logo_align"=>$campaign['logo_align'],"emailHash"=>$campaign['remote_hash'],'courseInfo'=>$course_info));
	}


	function list_get ()
	{

	}


	function create_post()
	{

	}

	function info_get()
	{

	}

	function remove_get()
	{

	}
}
