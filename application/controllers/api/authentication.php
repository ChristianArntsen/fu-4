<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Authentication extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('v2/Employee_model');
		$this->load->model('Employee');
		$this->load->model('Module');
		$this->load->model('Sale');
	}
	
	function index_post(){
		
		$pin = $this->input->post('pin');
		$response = $this->Employee_model->authenticate_pin($pin);
		
		if(empty($response)){
			$this->response(array('success' => false, 'msg' => 'Invalid credentials'), 403);
			return false;
		}
		$info = $this->Employee_model->get(array('person_id'=>$response));
		$this->response(array(
			'success' => true,
			'employee_id' => $response,
			'pin' => $pin,
			'user_level' => $info[0]['user_level']
		), 200);				
	}
	
	function login_post(){

		$employee_id = (int) $this->input->post('employee_id');
		$username = $this->input->post('username');
		$password = trim($this->input->post('password'));
		$cart_id = $this->input->post('cart_id');
		$terminal_id = $this->input->post('terminal_id');

		if($employee_id){
			$employee_info = $this->Employee->get_info($employee_id);
			if(empty($employee_info)){
				$this->response(array('success' => false, 'msg' => 'User not found'), 403);
				return false;
			}
			$username = $employee_info->username;
		}

		$logged_in = $this->Employee->login($username, $password);

		if(empty($logged_in)){
			$this->response(array('success' => false, 'msg' => 'Invalid login credentials'), 403);
			return false;
		}

		// Carry over terminal and cart data so it doesn't get lost
		if(!empty($cart_id)){
			$this->session->set_userdata('pos_cart_id', (int) $cart_id);
		}
		if(!empty($terminal_id)){
			$this->session->set_userdata('terminal_id', (int) $terminal_id);
		}

		// Force the session to update the database immediately
		// instead of waiting for page refresh as usual
		$this->session->sess_write();
		
		$employee_id = $this->session->userdata('person_id');
		if(empty($employee_info)){
			$employee_info = $this->Employee->get_info($employee_id);
		}

		$modules = $this->Module->get_allowed_modules($employee_id);
		$employee_info->modules = $modules->result_array();				
		$employee_info->sales_stats = $this->session->userdata('sales_stats');

		$this->response(array(
			'success' => true,
			'employee' => $employee_info
		), 200);
	}
	
	function logout_post(){
		$this->Employee->logout();
		$this->response(array(
			'success' => true
		), 200);		
	}
}
