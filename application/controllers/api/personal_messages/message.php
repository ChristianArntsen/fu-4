<?php
class Message extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        session_write_close();
    }

    public function create($customer_id)
    {
        $json = file_get_contents('php://input');
        $input = json_decode($json,true);
        $course_id = $this->session->userdata('course_id');
        if(empty($input['message'])){
        	$input['message']="";
        }

        $this->load->model("Course");
        $course_info = $this->Course->get_info($course_id);

	    $course_ids = array();
	    $this->Course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);

	    $marketing_texting_row = $this->db->from('marketing_texting')
		    ->where('customers.person_id',$customer_id)
		    ->where("customers.deleted",0)
		    ->where_in('marketing_texting.course_id',$course_ids)
		    ->join("customers","marketing_texting.person_id = customers.person_id AND foreup_marketing_texting.course_id = foreup_customers.course_id")
		    ->get()->row();

	    if(empty($marketing_texting_row)){
	    	$this->load->model("Customer");
	    	$customer = $this->Customer->get_info($customer_id);
	    	$phone_number = !empty($customer->cell_phone_number)?$customer->cell_phone_number:$customer->phone_number;
	    	if(empty($phone_number)){
			    print json_encode(["success"=>false,"msg"=>"The customer doesn't have a phone number."]);
			    return true;
		    }
		    $msisdnMaker = new \fu\marketing\texting\MsisdnMaker();
		    $msisdnMaker->set($phone_number);
		
		    $isValid = $msisdnMaker->isValid();
		    $msisdn = $msisdnMaker->get();
		    $marketing_texting_row = (object)[
		        "msisdn"=>$msisdn,
			    'valid_phone'=>$isValid ? 1 : 0
		    ];
		    $this->db->insert("marketing_texting",[
			    "person_id"=>$customer_id,
			    "msisdn"=>$msisdn,
			    "course_id"=>$customer->course_id,
			    "valid_phone"=>$isValid,
			    "texting_status"=>"not-invited"
		    ]);
	    }
	    $person_id = $this->session->userdata('person_id');
	    
	    
	    
	    
	    if($marketing_texting_row->valid_phone == 0){
		    print json_encode(["success"=>false,"msg"=>"The number is invalid. $msisdn"]);
		    return true;
	    }

	    $nexmo = new \fu\nexmo\Nexmo($course_info,$this->db,$this->config->item('api_key', 'nexmo'),$this->config->item('api_secret', 'nexmo'));

	    $this->load->model("Personal_messages");

	    if(!$this->Personal_messages->has_been_messaged($customer_id)){
	    	$welcome_message = "You've recieved a message from ".$course_info->name.".  Respond to communicate with the golf course.  Let them know you got the message.";
		    $this->Personal_messages->save_message($welcome_message,$marketing_texting_row->msisdn,0,$person_id);
		    $nexmo->send_message($marketing_texting_row->msisdn,$welcome_message);
		    sleep(1);
	    }
	    $this->Personal_messages->save_message($input['message'],$marketing_texting_row->msisdn,0,$person_id);
	    $result = $nexmo->send_message($marketing_texting_row->msisdn,$input['message']);
	    
	    print json_encode($result);
    }
}