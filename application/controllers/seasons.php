<?php
require_once ("secure_area.php");
class Seasons extends Secure_area
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('teesheet');
		$this->load->model('Pricing');
		$this->load->model('Season');
		$this->load->model('Price_class');
		$this->load->model('Special');
        $this->load->model('Booking_class');
	}
	
	function index()
	{
		redirect();
	}

	/*
	Loads the season edit form
	*/
	function view_season($season_id=-1,$teesheet_id=0)
	{
		$data['season_info'] = $this->Season->get_info($season_id);
		if ($teesheet_id == 0){
			$teesheet_id = $data['season_info']->teesheet_id;
		}
		$data['seasons_array'] = $this->Season->get_all($teesheet_id, true);
		$data['teesheet_info'] = $this->teesheet->get_info($teesheet_id);

		$this->load->helper("date");
		$this->load->view("seasons/form", $data);
	}
	
	/*
	Loads the timeframe edit window for a price class
	*/
	function view_price($class_id, $season_id)
	{
		$season = $this->Season->get_info($season_id);
		$data['season_info'] = $season;
		$data['teesheet_info'] = $season->teesheet_id;
		$data['timeframes'] = $this->Pricing->get_timeframes($season_id, $class_id);
		$data['price_info'] = $this->Price_class->get($class_id);

		$this->load->helper('date');
		$this->load->view('config/seasonal_timeframe_modal',$data);
	}
	
	/*
	Loads the season edit form
	*/
	function delete_price($season_id, $class_id)
	{
		$price_info = $this->Price_class->get($class_id);
		if($price_info['default'] == 1){
			echo json_encode(array('success'=>false, 'message'=>'Default price class can not be deleted'));
			return false;	
		}
		
		$response = $this->Season->delete_price_class($season_id, $class_id);
		
		if(!$response){
			echo json_encode(array('success'=>false, 'message'=>'Error removing price class'));			
		}else{
			echo json_encode(array('success'=>true, 'message'=>'Price class removed'));
		}
	}	
	
	/*
	Adds a new price class to a season (with default timeframe)
	*/
	function add_price($season_id)
	{
		$class_id = (int) $this->input->post('class_id');
		$response = $this->Season->add_price_class($season_id, $class_id);
		
		if(!$response){
			echo json_encode(array('success'=>false, 'message'=>'Error adding price class to season'));			
		}else{
			echo json_encode(array('success'=>true, 'message'=>'Price class added'));
		}
	}	
	
	/*
	 * Adds a new timeframe to a price class within a season
	 */
	function add_timeframe($season_id, $class_id){
		
		$name = $this->input->post('name');
		if(empty($name)){
			$name = '';
		}
		
		$timeframe_data = array(
				'class_id'			=> $class_id,
				'season_id'			=> $season_id,
				'timeframe_name' 	=> $name,
				'monday'			=> 1,
				'tuesday'			=> 1,
				'wednesday'			=> 1,
				'thursday'			=> 1,
				'friday'			=> 1,
				'saturday'			=> 1,
				'sunday'			=> 1,
				'start_time'		=> 0,
				'end_time' 			=> 2400
		);
        $price_info = $this->Price_class->get($class_id);


		if($timeframe_id = $this->Pricing->add_timeframe($timeframe_data))
		{
			$timeframe = $this->Pricing->get_timeframe($timeframe_id);
			$this->load->helper('date');
			$this->load->view('pricing/timeframe',array('timeframe'=>$timeframe, 'price_class' => $price_info));
		}
		else
		{
			echo json_encode(array('success'=>false, 'message'=>'Error creating time frame'));
		}	
	}
	
	/*
	 * Deletes a timeframe from a price class associated with a season
	 */ 
	function delete_timeframe($timeframe_id)
	{
		if($this->Pricing->delete_timeframe($timeframe_id))
		{
			echo json_encode(array('success'=>true,'message'=>"Timeframe was successfully deleted."));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>'Error deleting timeframe'));
		}
	}
	
	function save_timeframes($season_id, $class_id){
	
		// Pull all of the variables in from the post so that we can use their array keys
		$season_id = $this->input->post('season_id');
		$timeframe_name = $this->input->post('timeframe_name');
		$monday = $this->input->post('monday');
		$tuesday = $this->input->post('tuesday');
		$wednesday = $this->input->post('wednesday');
		$thursday = $this->input->post('thursday');
		$friday = $this->input->post('friday');
		$saturday = $this->input->post('saturday');
		$sunday = $this->input->post('sunday');
		$start_time = $this->input->post('start_time');
		$end_time = $this->input->post('end_time');
		$price1 = $this->input->post('price1');
		$price2 = $this->input->post('price2');
		$price3 = $this->input->post('price3');
		$price4 = $this->input->post('price4');
		$price5 = $this->input->post('price5');
		$price6 = $this->input->post('price6');
		$active = $this->input->post('active');
		
		// Iterate through each timeframe that was open
		foreach($this->input->post('timeframe') as $timeframe_id)
		{
			if(empty($timeframe_name[$timeframe_id])){
				echo json_encode(array('success'=>true, 'message' => 'Timeframe name is required'));
				return false;
			}
			
			// If timeframe is the default, only allow change of prices
			if($this->Season->is_default_timeframe($timeframe_id)){
				$timeframe_data = array(			
					'price1'			=> $price1[$timeframe_id],
					'price2'			=> $price2[$timeframe_id],
					'price3'			=> $price3[$timeframe_id],
					'price4'			=> $price4[$timeframe_id],
					'price5'			=> $price5 && !empty($price5[$timeframe_id]) ? $price5[$timeframe_id] : 0,
					'price6'			=> $price6 && !empty($price6[$timeframe_id]) ? $price6[$timeframe_id] : 0					
				);
								
			}else{
	
				// Get the info from the arrays for the specified timeframe
				if($active[$timeframe_id] == 1){
					
					$timeframe_data = array(
						'timeframe_name'	=> $timeframe_name[$timeframe_id],
						'monday'			=> $monday[$timeframe_id] ? 1 : 0,
						'tuesday'			=> $tuesday[$timeframe_id] ? 1 : 0,
						'wednesday'			=> $wednesday[$timeframe_id] ? 1 : 0,
						'thursday'			=> $thursday[$timeframe_id] ? 1 : 0,
						'friday'			=> $friday[$timeframe_id] ? 1 : 0,
						'saturday'			=> $saturday[$timeframe_id] ? 1 : 0,
						'sunday'			=> $sunday[$timeframe_id] ? 1 : 0,
						'start_time'		=> $start_time[$timeframe_id],
						'end_time'			=> $end_time[$timeframe_id],
						'price1'			=> $price1[$timeframe_id],
						'price2'			=> $price2[$timeframe_id],
						'price3'			=> $price3[$timeframe_id],
						'price4'			=> $price4[$timeframe_id],
						'price5'			=> $price5 && !empty($price5[$timeframe_id]) ? $price5[$timeframe_id] : 0,
						'price6'			=> $price6 && !empty($price6[$timeframe_id]) ? $price6[$timeframe_id] : 0,
						'active'			=> $active[$timeframe_id]
					);
				
				}else{
					$timeframe_data = array(
						'active'			=> $active[$timeframe_id]	
					);				
				}
			}
			
			//Save the info for that timeframe
			if (!$this->Pricing->save_timeframe($timeframe_id, $timeframe_data)){
				$success = false;
			}
		}
		
		echo json_encode(array('success'=>true, 'message' => 'Timeframe settings saved'));			
	}
	
	/*
	Inserts/updates a season
	*/
	function save($season_id = false)
	{
		if($this->input->post('duplicate_from')){
			$response = $this->Season->duplicate($this->input->post('duplicate_from'), $this->input->post('teesheet_id'));
		
		}else{
			$teesheet_id = $this->input->post('teesheet_id');
			$season_name = $this->input->post('season_name');
			$holiday = $this->input->post('holiday');
			
			if(empty($season_name)){
				echo json_encode(array('success'=>false, 'message' => 'Season name is required'));
				return false;
			}
			
			$season_data = array(
				'season_id' => $season_id,
				'teesheet_id' => $teesheet_id,
				'season_name' => $season_name,
				'holiday' => $holiday
			);
			
			$start_date = $this->input->post('start_date');
			if($start_date){
				$season_data['start_date'] = '0000-' . $start_date;
			}

			$end_date = $this->input->post('end_date');
			if($end_date){
				$season_data['end_date'] = '0000-' . $end_date;
			}		
			
			$response = $this->Season->save($season_data, $season_id);
		}
		
		if(!empty($response)){
			if($season_id){
				$response = $season_id;
			}
			echo json_encode(array('success' => true, 'message' => 'Season successfully saved', 'season_id' => $response));
		}else{       
			echo json_encode(array('success' => false, 'message' => 'There was an error saving the season'));
		}
	}

	function delete_season($season_id = false)
	{
		if($this->Season->delete_season($season_id))
		{
			echo json_encode(array('success'=>true,'message'=>"Season $season_id was successfully deleted."));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>'There was an error deleting the season.'));
		}
	}
	
	function get_price($time = false, $date = false)
	{
		$this->load->model('Pricing');
		var_dump($this->Pricing->get_price(83, 99, $time, $date));
	}
	
	function get_price_classes($season_id, $class_id = null){
		
	}
	/*
	Loads the Pricing graphic view window for teesheet
	*/
	function pricing_graphics_view($teesheet_id){
		$data = array();
		$date_array = array();
		$start_date = $this->input->post('startDate');
		if ($start_date != '') {
			$default_date = $start_date;
		}else{
			$default_date = date("Y-m-d",strtotime('monday this week'));
		}
		for ($i=0; $i<7;$i++){
	      	$currentDate =date("Y-m-d", strtotime($default_date . " + $i day"))."<br />";
	   		$date_array[] = $currentDate;
   		}
   		$data['week_days'] = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
   		$data['date_array'] = $date_array;
		$data['teesheet_info'] = $this->teesheet->get_info($teesheet_id);
		$teetimes = array('0700'=>'7:00',
                        '0730'=>'7:30',
                        '0800'=>'8:00',
                        '0830'=>'8:30',
                        '0900'=>'9:00',
                        '0930'=>'9:30',
                        '1000'=>'10:00',
                        '1030'=>'10:30',
                        '1100'=>'11:00',
                        '1130'=>'11:30',
                        '1200'=>'12:00',
                        '1230'=>'12:30',
                        '1300'=>'13:00',
                        '1330'=>'13:30',
                        '1400'=>'14:00',
                        '1430'=>'14:30',
                        '1500'=>'15:00',
                        '1530'=>'15:30',
                        '1600'=>'16:00',
                        '1630'=>'16:30',
                        '1700'=>'17:00',
                        '1730'=>'17:30',
						'1800'=>'18:00',
						'1830'=>'18:30',
						'1900'=>'19:00',
						'1930'=>'19:30');
		$data['teetimes'] = $teetimes;
		$data['teesheet_id'] = $teesheet_id;
		$data['price_class'] = $price_class = $this->Price_class->get_all();
		foreach ($price_class as $key => $value) {
			$data['price_classes'][2][$key] = $value['color'];		
			$price_array[$value['class_id']] = $value['name'];
		}
		$grid_pricing = array();
		foreach ($date_array as $key2 => $date) { 
			$incr = 0;
			$day = array();
			foreach ($price_array as $class_key => $price_class_name) { 
				$prclass = array();
				foreach ($teetimes as $key => $time) {	
					$prclass[$time] = $this->Pricing->get_price_timeframe($data['teesheet_id'], $class_key, $date, $key);
					if ($class_key != $prclass[$time]['price_class_id']) {
						$prclass[$time]['price1'] = $prclass[$time]['price2'] = $prclass[$time]['price3'] = $prclass[$time]['price4'] = '';		
					}
					$prclass['color'] = $data['price_classes'][2][$incr];
				}
				$day[$price_class_name] = $prclass;
				$incr++;
			}		
			$grid_pricing[$date] = $day;
		}
		$data['grid_pricing'] = $grid_pricing;
		$this->load->view('config/pricing_graphics_view',$data);
	}

	/*
	Loads the daily specials
	*/
	function view_specials($teesheet_id){
		$data = array();
		$data['is_aggregate'] = $this->input->get('aggregate');
		$data['specials'] = $this->Special->get_all($teesheet_id, false, (int) $data['is_aggregate']);
		$data['teesheet_id'] = $teesheet_id;
		$specials_enabled = $this->Special->get_enabled_specials($teesheet_id);
		foreach ($specials_enabled as $key => $value) {
			$data['specials_enabled'] =	$value['specials_enabled'];
		}
		$this->load->view('config/daily_specials', $data);
	}
	/*
	Delete the daily specials and times 
	*/
	function delete_specials($special_id){
		$response = $this->Special->delete($special_id);
		echo json_encode($response['status']);die();	
	}
	/*
	Load the Add daily specials form 
	*/
	function add_daily_specials($teesheet_id, $id = false){

		$data['teesheet_info'] = $this->teesheet->get_info($teesheet_id);
		$data['is_aggregate'] = $this->input->get('aggregate');
        $booking_classes = $this->Booking_class->get_all($teesheet_id);
        $booking_class_array = array('0' => 'All');
        foreach ($booking_classes as $booking_class) {
            $booking_class_array[$booking_class['booking_class_id']] = $booking_class['name'];
        }
        $data['booking_classes'] = $booking_class_array;
		$teetimes = $this->Special->get_teetime_special_array(
			$this->config->item('open_time'), 
			$this->config->item('close_time'), 
			$data['teesheet_info']->increment, 
			$teesheet_id
		);
		$special = $this->Special->get_info($teesheet_id, $id);

		if ($id) {
			$data['specials'] = $special[0];
			$data['is_aggregate'] = ($special[0]['is_aggregate'] == 1);
		}else{
			$data['specials'] = array(
				'active' => 1,
				'special_id' => null,
                'booking_class_id' => 0,
				'name' => null,
				'date' => null,
				'monday' => null,
				'tuesday' => null,
				'wednesday' => null,
				'thursday' => null,
				'friday' => null,
				'saturday' => null,
				'sunday' => null,
				'special_times' => '',
				'price_1' => null,
				'price_2' => null,
				'price_3' => null,
				'price_4' => null,
				'price_5' => null,
				'price_6' => null
			);
		}
		$data['teetimes'] = $teetimes;
		$data['teesheet_id'] = $teesheet_id;
		$this->load->view('config/form_specials',$data);
	}
	/*
	Save daily specials and times
	*/
	function save_special_options($special_id = false){
		
		$specialtime = $this->input->post('specialtime');
		$special_data = array(
			'date' => $this->input->post('special_date'),
			'teesheet_id' => $this->input->post('teesheet_id'),
			'name' => $this->input->post('special_name'),
            'booking_class_id' => $this->input->post('booking_class_id'),
			'price_1' => $this->input->post('18_hole_green_fee'),
			'price_2' => $this->input->post('9_hole_green_fee'),
			'price_3' => $this->input->post('18_hole_cart'),
			'price_4' => $this->input->post('9_hole_cart'),
			'monday' => (int) $this->input->post('monday'),
			'tuesday' => (int) $this->input->post('tuesday'),
			'wednesday' => (int) $this->input->post('wednesday'),
			'thursday' => (int) $this->input->post('thursday'),
			'friday' => (int) $this->input->post('friday'),
			'saturday' => (int) $this->input->post('saturday'),
			'sunday' => (int) $this->input->post('sunday'),
			'is_recurring' => (int) $this->input->post('is_recurring'),
			'is_aggregate' => (int) $this->input->post('is_aggregate'),
			'active' => (int) $this->input->post('active')
		);

		if($this->input->post('when') == 'date'){
			$special_data['monday'] = 0;
			$special_data['tuesday'] = 0;
			$special_data['wednesday'] = 0;
			$special_data['thursday'] = 0;
			$special_data['friday'] = 0;
			$special_data['saturday'] = 0;
			$special_data['sunday'] = 0;
			$special_data['is_recurring'] = 0;
		
		}else{
			$special_data['date'] = '0000-00-00';
			$special_data['is_recurring'] = 1;
		}
		
		$success = $this->Special->save($special_data, $specialtime, $special_id);
		echo json_encode(array('success'=>$success));	
	}

	/*
	Get special enable 
	*/
	function specials_enabled($special_enabled,$teesheet_id){
		$specials_enabled = $this->Special->enabled_specials($special_enabled,$teesheet_id);
	}
}
