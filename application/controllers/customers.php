<?php
//require_once(APPPATH.'libraries/spreadsheet_excel_reader.php');
require_once ("person_controller.php");
class Customers extends Person_controller
{
	function __construct()
	{
		parent::__construct('customers');
		$this->load->model('Customer_loyalty');
        $this->load->model('Employee_Audit_Log');
                //$this->load->library('spreadsheet_excel_reader');
	}

	function index()
	{
		if ($this->config->item('sales_v2') == 1 && $this->input->get('in_iframe') != 1){
            redirect(site_url('v2/home#customers'));
            return false;
        }

		$config['base_url'] = site_url('customers/index');
		$config['total_rows'] = $this->Customer->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);

        $data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
        $groups = $this->Customer->get_group_info();
		//print_r($groups);
		$data['groups'] = array('all'=>"Everyone");
	    foreach($groups as $group)
		{
			$data['groups'][$group['group_id']] = $group['label'].' ('.$group['member_count'].')';
		}
		if(!$this->session->userdata('sales_v2')){
			$passes = $this->Customer->get_pass_info();
			$data['passes'] = array('all'=>"Everyone");
			foreach($passes as $pass)
			{
				$data['passes'][$pass['pass_id']] = $pass['label'].' ('.$pass['member_count'].')';
			}
			$data['old_passes_on'] = true;
		}

        //print_r($passes);
        $data['manage_table']=get_people_manage_table($this->Customer->get_all($config['per_page'], $this->uri->segment(3)),$this);
        $this->load->view('people/manage',$data);
    }
	function manage_credit_cards($person_id, $tee_sheet = false)
	{
		$this->load->model('Customer_credit_card');
		$data = array();
        $data['tee_sheet'] = $tee_sheet;
		$data['credit_cards'] = $this->Customer_credit_card->get($person_id);
		$data['customer_id'] = $person_id;
		$data['customer'] = $this->Customer->get_info($person_id,$this->session->userdata('course_id'));
	 	$this->load->view('customers/manage_credit_cards', $data);
	}
	function delete_credit_card($customer_id, $credit_card_id)
	{
		$this->load->model('Customer_credit_card');
		$this->Customer_credit_card->remove_from_billings($credit_card_id);
		echo json_encode(array('success'=> $this->Customer_credit_card->delete_card($customer_id, $credit_card_id)));
	}
 	function open_add_credit_card_window($person_id, $billing_id = -1, $in_tee_sheet = 0) {
		$course_id = $this->session->userdata('course_id');

		// USING ETS FOR PAYMENT PROCESSING
		if ($this->config->item('ets_key'))
		{
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));

			$session = $payment->set('action', 'session')
			  		   ->set('isSave', 'true')
					   ->send();

			if ($session->id)
			{
				if(!isset($previous_card_declined)){
					$previous_card_declined = false;
				}
				$user_message = $previous_card_declined!='false'?'Card declined, please try another.':'';
				$return_code = '';
				$this->session->set_userdata('ets_session_id', (string)$session->id);
				//$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
				$data = array('user_message'=>$user_message, 'return_code'=>$return_code, 'session'=>$session, 'url' => site_url('customers/card_captured').'/'.$person_id.'/'.$billing_id);
				$this->load->view('sales/ets', $data);
			}
			else
			{
				$data = array('processor' => 'ETS');
				$this->load->view('sales/cant_load', $data);
			}
		}
		else if ($this->config->item('mercury_e2e_id') && !$manual_entry)
		{
			$this->load->library('Hosted_checkout_2');

			$HC = new Hosted_checkout_2();
			$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//foreUP's Credentials
			$HC->set_response_urls('customers/card_captured/'.$person_id.'/'.$billing_id, 'credit_cards/process_cancelled');

			$initialize_results = $HC->initialize_payment('1.00','0.00','PreAuth','eCOM','Recurring');
			//print_r($initialize_results);
			if ((int)$initialize_results->ResponseCode == 0)
			{
				//Set invoice number to save in the database
				$invoice = $this->sale->add_credit_card_payment(array('mercury_id'=>$this->config->item('mercury_id'),'mercury_password'=>$this->config->item('mercury_password'),'tran_type'=>'PreAuth','frequency'=>'Recurring'));
				$this->session->set_userdata('invoice', $invoice);

				$user_message = (string)$initialize_results->Message;
				$return_code = (int)$initialize_results->ResponseCode;
				$this->session->set_userdata('payment_id', (string)$initialize_results->PaymentID);
				$url = $HC->get_iframe_url('eCOM', (string)$initialize_results->PaymentID);
				$data = array('user_message'=>'$1.00 authorization in order to save the credit card', 'return_code'=>$return_code, 'url'=>$url);
				$this->load->view('sales/hc_pos_iframe.php', $data);
			}
		}
		// USING MERCURY FOR PAYMENT PROCESSING
		else if ($this->config->item('mercury_id'))
		{
            $this->load->model('Blackline_devices');
            $blackline_info = $this->Blackline_devices->get_terminal_info($this->session->userdata('terminal_id'));
            if ($this->config->item('use_mercury_emv') && $blackline_info) {
                $data = array();
                $trans_type = 'auth';
                $total = 1;
                $payment_data = array(
                    'course_id' => $this->config->item('course_id'),
                    'mercury_id' => $this->config->item('mercury_id'),
                    'mercury_password' => $this->config->item('mercury_password'),
                    'tran_type' => $trans_type,
                    'amount' => $total,
                    'frequency' => 'OneTime',
                    'terminal_name' => $this->session->userdata('terminal_id'),
                    'initiation_time' => gmdate('Y-m-d H:i:s')
                );
                $invoice_id = $this->sale->add_credit_card_payment($payment_data);
                $data['payment_url'] = $this->Blackline_devices->get_payment_url($trans_type, $total, $blackline_info, $invoice_id);
                $data['tee_time_id'] = !empty($tee_time_id) ? $tee_time_id : '';
                $data['person_id'] = $person_id;
                $data['billing_id'] = $billing_id;
                $data['controller_name'] = strtolower(get_class());
				$data['in_tee_sheet'] = $in_tee_sheet;
                $this->load->view('v2/sales/mercury_emv', $data);
            }
            else {
                $this->load->library('Hosted_checkout_2');

                $HC = new Hosted_checkout_2();
                $HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));//foreUP's Credentials
                $HC->set_response_urls('customers/card_captured/' . $person_id . '/' . $billing_id, 'credit_cards/process_cancelled');

                $initialize_results = $HC->initialize_payment('1.00', '0.00', 'PreAuth', 'POS', 'Recurring');
                //print_r($initialize_results);
                if ((int)$initialize_results->ResponseCode == 0) {
                    //Set invoice number to save in the database
                    $invoice = $this->sale->add_credit_card_payment(array('mercury_id'=>$this->config->item('mercury_id'), 'mercury_password'=>$this->config->item('mercury_password'),'tran_type' => 'PreAuth', 'frequency' => 'Recurring'));
                    $this->session->set_userdata('invoice', $invoice);

                    $user_message = (string)$initialize_results->Message;
                    $return_code = (int)$initialize_results->ResponseCode;
                    $this->session->set_userdata('payment_id', (string)$initialize_results->PaymentID);
                    $url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
                    $data = array('user_message' => '$1.00 authorization in order to save the credit card', 'return_code' => $return_code, 'url' => $url);
                    $this->load->view('sales/hc_pos_iframe.php', $data);
                }
            }
		}
		// Using Element Express merchant
		else if($this->config->item('element_account_id'))
		{
			$this->load->view('customers/add_credit_card', array('person_id' => $person_id));
		}
	}

	function card_captured($person_id, $billing_id) {

		$credit_card_data = array('course_id'=>'', 'credit_card_id'=>'');
		$approved = false;
		$this->load->model('Customer_credit_card');
		$this->load->model('sale');
		$this->load->library('v2/Cart_lib');
        $credit_card_id = '';

		if($this->config->item('ets_key')){

			$response = $this->input->post('response');
			$ets_response = json_decode($response);
			$transaction_time = date('Y-m-d H:i:s', strtotime($ets_response->created));
			// VERIFYING A POSTed TRANSACTION
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));
			$session_id = $payment->get("session_id");

			//$transaction_id = $ets_response->transactions->id;

			$account_id = $ets_response->customers->id;
			$payment->set("action", "verify")
				->set("sessionID", $ets_response->id)
				//->set("transactionID", $transaction_id)
				->set('accountID', $account_id);

			$verify = $payment->send();
			// Convert card type to match mercury card types
			if ((string)$ets_response->customers->cardType != 'UNKNOWN') {
				$ets_card_type = $ets_response->customers->cardType;
				$card_type = '';
				switch($ets_card_type){
					case 'MasterCard':
						$card_type = 'M/C';
					break;
					case 'Visa':
						$card_type = 'VISA';
					break;
					case 'Discover':
						$card_type = 'DCVR';
					break;
					case 'American Express':
						$card_type = 'AMEX';
					break;
					case 'Diners':
						$card_type = 'DINERS';
					break;
					case 'JCB':
						$card_type = 'JCB';
					break;
					default:
						$card_type = $ets_card_type;
					break;
				}
				$masked_account = str_replace('*', '', (string) $verify->customers->cardNumber);
				$expiration = DateTime::createFromFormat('my', $ets_response->customers->cardExpiration);
			}
			else {
				$card_type = 'Bank Acct';
				$masked_account = str_replace('*', '', (string) $verify->customers->accountNumber);
				$expiration = DateTime::createFromFormat('my', date('my', strtotime('+2 years')));
			}

			if((string)$ets_response->status == 'success'){
				$credit_card_data = array(
					'course_id' 		=> $this->session->userdata('course_id'),
					'card_type' 		=> $card_type,
					'masked_account' 	=> $masked_account,
					'cardholder_name' 	=> '',
					'customer_id'		=> $person_id,
					'token' 			=> (string) $ets_response->customers->id,
					'expiration' 		=> $expiration->format('Y-m-01')
				);
				$credit_card_id = $this->Customer_credit_card->save($credit_card_data);

				$approved = true;
			}
			$hide_jquery = 1;

		}else if($this->config->item('mercury_id')){
            $this->load->model('Blackline_devices');
            $blackline_info = $this->Blackline_devices->get_terminal_info($this->session->userdata('terminal_id'));
            if ($this->config->item('use_mercury_emv') && $blackline_info) {


                $response = $this->input->post();
                $rd = $response;
                if ((string)$rd['ResultCode'] != 'Approved') {
                    //$this->response(array('success' => false, 'msg' => 'Credit card declined. ' . $rd['Message']), 400);
                    $approved = false;
                }
                else {
                    $amount = (float)$rd['Amount'];
					if ($rd['CardType'] == 'MasterCard') {
						$rd['CardType'] = 'M/C';
					} else if ($rd['CardType'] == 'Visa') {
						$rd['CardType'] = 'VISA';
					} else if ($rd['CardType'] == 'Amex') {
						$rd['CardType'] = 'AMEX';
					} else if ($rd['CardType'] == 'Discover') {
						$rd['CardType'] = 'DCVR';
					} else if ($rd['CardType'] == 'DinersClub') {
						$rd['CardType'] = 'DINERS';
					}
                    //Add card to billing_credit_cards
                    $credit_card_data = array(
                        'course_id' => $this->config->item('course_id'),
                        'customer_id' => $person_id == NULL ? '' : $person_id,
                        'token' => $rd['CardToken'],
                        'token_expiration' => date('Y-m-d', strtotime('+2 years')),
                        'card_type' => $rd['CardType'],
                        'masked_account' => $rd['Last4'],
                        'cardholder_name' => $rd['Name']
                    );
                    $credit_card_id = $this->Customer_credit_card->save($credit_card_data);

                    $invoice_id = $response['RefID'];
                    $payment_data = array(
                        'course_id' => $this->config->item('course_id'),
                        'mercury_id' => $this->config->item('mercury_id'),
                        'tran_type' => $rd['TransType'],
                        'amount' => $amount,
                        'auth_amount' => $rd['Amount'],
                        'card_type' => $rd['CardType'],
                        'frequency' => 'OneTime',
                        'masked_account' => $rd['Last4'],
                        'cardholder_name' => $rd['Name'],
                        'terminal_name' => $rd['TerminalID'],
                        'trans_post_time' => gmdate('Y-m-d H:i:s'),
                        'auth_code' => $rd['AuthCode'],
                        'acq_ref_data' => $rd['ProcessorExtraData1'],
                        'process_data' => $rd['ProcessorExtraData2'],
						'ref_no' => $rd['ProcessorExtraData3'],
                        'token' => $rd['CardToken'],
                        'response_code' => $rd['ResultCode'],
                        'status' => $rd['ResultCode'],
                        'status_message' => $rd['Message'],
                        'display_message' => $rd['Message']
                    );

                    $this->sale->update_credit_card_payment($invoice_id, $payment_data);

                    $approved = true;
                }
            }
            else {
                $this->load->library('Hosted_checkout_2');
                $HC = new Hosted_checkout_2();
                $HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));//foreUP's Credentials
                //$data = $HC->payment_made();
                $payment_id = $this->session->userdata('payment_id');
                $this->session->unset_userdata('payment_id');
                $HC->set_payment_id($payment_id);
                $verify_results = $HC->verify_payment();
                //echo 'about to complete<br/>';
                $HC->complete_payment();
                //echo 'completed <br/>';
                $invoice = $this->session->userdata('invoice');
                $this->session->unset_userdata('invoice');

                //Add card to billing_credit_cards
                $credit_card_data = array(
                    'course_id' => $this->session->userdata('course_id'),
                    'customer_id' => $person_id,
                    'token' => (string)$verify_results->Token,
                    'token_expiration' => date('Y-m-d', strtotime('+2 years')),
                    'card_type' => (string)$verify_results->CardType,
                    'masked_account' => (string)$verify_results->MaskedAccount,
                    'cardholder_name' => (string)$verify_results->CardholderName
                );
                $this->Customer_credit_card->save($credit_card_data);
                //echo 'saved credit card data';
                //Update credit card payment data
                $payment_data = array(
                    'course_id' => $this->session->userdata('course_id'),
                    'mercury_id' => $this->config->item('mercury_id'),
                    'tran_type' => (string)$verify_results->TranType,
                    'amount' => (string)$verify_results->Amount,
                    'auth_amount' => (string)$verify_results->AuthAmount,
                    'card_type' => (string)$verify_results->CardType,
                    'frequency' => 'Recurring',
                    'masked_account' => (string)$verify_results->MaskedAccount,
                    'cardholder_name' => (string)$verify_results->CardholderName,
                    'ref_no' => (string)$verify_results->RefNo,
                    'operator_id' => (string)$verify_results->OperatorID,
                    'terminal_name' => (string)$verify_results->TerminalName,
                    'trans_post_time' => (string)$verify_results->TransPostTime,
                    'auth_code' => (string)$verify_results->AuthCode,
                    'voice_auth_code' => (string)$verify_results->VoiceAuthCode,
                    'payment_id' => $payment_id,
                    'acq_ref_data' => (string)$verify_results->AcqRefData,
                    'process_data' => (string)$verify_results->ProcessData,
                    'token' => (string)$verify_results->Token,
                    'response_code' => (int)$verify_results->ResponseCode,
                    'status' => (string)$verify_results->Status,
                    'status_message' => (string)$verify_results->StatusMessage,
                    'display_message' => (string)$verify_results->DisplayMessage,
                    'avs_result' => (string)$verify_results->AvsResult,
                    'cvv_result' => (string)$verify_results->CvvResult,
                    'tax_amount' => (string)$verify_results->TaxAmount,
                    'avs_address' => (string)$verify_results->AVSAddress,
                    'avs_zip' => (string)$verify_results->AVSZip,
                    'payment_id_expired' => (string)$verify_results->PaymentIDExpired,
                    'customer_code' => (string)$verify_results->CustomerCode,
                    'memo' => (string)$verify_results->Memo
                );
                $this->sale->update_credit_card_payment($invoice, $payment_data);
                //echo 'saved credit card payment data';

                if ($payment_data['response_code'] === 0 && $payment_data['status'] === "Approved") {
                    $approved = true;
                }
            }
            $hide_jquery = 0;
		}else if($this->config->item('element_account_id')){

			$track_data = $this->input->post('track_data');
			$this->load->library('v2/Element_merchant');

			// Initialize payment library
			$this->element_merchant->init(
				new GuzzleHttp\Client()
			);

			$this->element_merchant->set_terminal(array(
				'CardholderPresentCode' => 		Element_merchant::CARDHOLDER_PRESENT_CODE_PRESENT,
				'CardInputCode' => 				Element_merchant::CARD_INPUT_CODE_MAGSTRIPE_READ,
				'CardPresentCode' => 			Element_merchant::CARD_PRESENT_CODE_PRESENT,
				'MotoECICode' => 				Element_merchant::MOTO_ECI_CODE_SINGLE,
				'TerminalCapabilityCode' => 	Element_merchant::TERMINAL_CAPABILITY_CODE_MAGSTRIPE_READER,
				'TerminalEnvironmentCode' => 	Element_merchant::TERMINAL_ENVIRONMENT_CODE_LOCAL_ATTENDED,
				'TerminalType' => 				Element_merchant::TERMINAL_TYPE_POINT_OF_SALE
			));

			// First create a row in the database to store card details
			$credit_card_data = array(
				'course_id' => $this->session->userdata('course_id'),
				'customer_id' => $person_id,
				'token' => '',
				'token_expiration' => '',
				'card_type' => '',
				'masked_account' => '',
				'cardholder_name' => ''
			);
			$credit_card_id = $this->Customer_credit_card->save($credit_card_data);

			// Save card details
			$this->element_merchant->payment_account_create(array(
				'encrypted_track' => $track_data,
				'payment_account_reference_number' => $credit_card_id
			));

			if(!$this->element_merchant->success()){
				echo json_encode(array(
					'msg' => 'Error: '.$this->element_merchant->message(),
					'success' => false
				));
				return false;
			}

			$payment_account_id = (string) $this->element_merchant->response()->xml()
				->Response->PaymentAccount->PaymentAccountID;

			// Save was successful. Retrieve details on the card
			$this->element_merchant->payment_account_query(array(
				'payment_account_id' => $payment_account_id
			));

			$card_details = (array) $this->element_merchant->response()->xml()
				->Response->QueryData->Items->Item;

			$exp_date = DateTime::createFromFormat('y-m-d', (string) $card_details['ExpirationYear'].'-'.(string) $card_details['ExpirationMonth'].'-01');

			$card_data = array(
				'course_id' => $this->session->userdata('course_id'),
				'customer_id' => $person_id,
				'token' => (string) $payment_account_id,
				'token_expiration' => '',
				'card_type' => $this->cart_lib->standardize_credit_card_type((string) $card_details['PaymentBrand']),
				'masked_account' => substr((string) $card_details['TruncatedCardNumber'], -4, 4),
				'expiration' => $exp_date->format('Y-m-d')
			);

			// Update database with card data from Express API
			$this->Customer_credit_card->save($card_data, $credit_card_id);
			$approved = true;
		}

		$credit_cards =$this->Customer_credit_card->get($person_id);
		$person_info = $person_id ? $this->Customer->get_info($person_id,$this->session->userdata('course_id')) : $this->Customer->get_info($data['person_id'],$this->session->userdata('course_id'));

		$data = array ('card_captured'=>true, 'course_id'=>$credit_card_data['course_id'], 'billing_id'=>$billing_id, 'open_billing_box'=>'true', 'customer_id'=>$person_id, 'credit_card_id'=>$credit_card_data['credit_card_id'], 'credit_cards'=>$credit_cards,'person_info'=>$person_info, 'hide_jquery'=>$hide_jquery);
		if ($approved){
            $cc_checkboxes = "";
            $cc_dropdown = "<select id='cc_dropdown' name='cc_dropdown'>";
            $cc_dropdown .= "<option value='invoice'>Only Generate Invoice</option>";

            foreach($credit_cards as $credit_card)
            {
                $cc_checkboxes .= "<div id='checkbox_holder_".$credit_card['credit_card_id']."'class='field_row clearfix ".(($credit_card['customer_id'] != '' && $credit_card['customer_id'] == $person_info->person_id)?'':'hidden')."'>".
                    "<div class='form_field'>".
                    form_checkbox('groups[]', $credit_card['credit_card_id'], ($credit_card['customer_id'] != '' && $credit_card['customer_id'] == $person_info->person_id) ? true:FALSE, "id='group_checkbox_{$credit_card['credit_card_id']}'")
                    .' '.$credit_card['card_type'].' '.$credit_card['masked_account'].
                    " - <span class='' onclick='customer.add_billing_row(\'".$credit_card['credit_card_id']."\')'>Add Billing</span>".
                    "</div>
			</div>";
                $selected = '';
                if ($credit_card_id == $credit_card['credit_card_id'])
                {
                    $selected = 'selected';
                }
                $cc_dropdown .= "<option value='{$credit_card['credit_card_id']}' $selected>Charge to {$credit_card['card_type']} {$credit_card['masked_account']}</option>";

            }
            $cc_dropdown .= '</select>';
            if ($this->input->post('returnJSON')) {
                echo json_encode(array('success'=>$approved,'credit_card_id'=>$credit_card_data['credit_card_id'],'cc_dropdown'=>$cc_dropdown));
            }
            else {
                $this->load->view('customers/card_captured', $data);
            }
		}
		else {
			if ($this->input->post('returnJSON')) {
				echo json_encode(array('success'=>$approved));
			}
		}
	}
	function add_new_charge($cc_id) {
		$amount = $this->input->post('amount');
		$description = $this->input->post('description');
		$subscription = $this->input->post('subscription');
		$start_date = $this->input->post('start_date');
		$month = $this->input->post('month');
		$day = $this->input->post('day');

		$data = array (
			'credit_card_id'=>$cc_id,
			'amount'=>$amount,
			'description'=>$description,
			'subscription'=>$subscription,
			'start_data'=>date('Y-m-d', strtotime($start_date)),
			'month'=>$month,
			'day'=>$day
		);
		//echo 'about to save';
		$this->load->model('Customer_billing');
		if ($this->Customer_billing->save($data))
		echo $this->db->last_query();
			echo $this->Customer_billing->create_billing_row($data);

		echo false;
	}
	function find_item_info()
	{
		$item_number=$this->input->post('scan_item_number');
		echo json_encode($this->Billing->find_item_info($item_number));
	}
	function get_item_info($type = null, $item_id = null){

		if(empty($type) || empty($item_id)){
			echo 'null';
			return false;
		}

		$data = array();
		if($type == 'item'){
			$this->load->model('Item');
			$this->load->model('Item_taxes');

			$data = (array) $this->Item->get_info($item_id);
			$data['taxes'] = $this->Item_taxes->get_info($item_id);

		}else if($type == 'item_kit'){
			$this->load->model('Item_kit');
			$this->load->model('Item_kit_taxes');

			$data = (array) $this->Item_kit->get_info($item_id);
			$data['taxes'] = $this->Item_kit_taxes->get_info($item_id);
		}

		echo json_encode($data);
		return false;
	}

	function save_invoice(){

		$email_success = false;
		$charge_success = false;
		$this->load->model('Customer_credit_card');

		$customer_list = array();
		$customer_id = $this->input->post('customer_id');
		if ($customer_id != '') {
        	$customer_list[$customer_id] = 1;
        }
        $credit_card_id = (int) $this->input->post('credit_card_id');
		$email_invoice = $this->input->post('email_invoice');
		$show_account_transactions = $this->input->post('show_account_transactions');
		$items = $this->input->post('items');
		$bill_start = date('Y-m-d H:i:s');
		$bill_end = $bill_start;
        $invoice_date = date('Y-m-d', strtotime($this->input->post('invoice_date')));
        $due_days = (int) $this->input->post('invoice_due_days');
		$due_date = date('Y-m-d', strtotime($invoice_date.' +'.$due_days.' days'));

		$customer_groups = $this->input->post('campaign_group');
        $customer_individuals = $this->input->post('campaign_individuals');
		if (!empty($customer_groups)) {
			foreach ($customer_groups as $group) {
				// ADD GROUP
				$group_members = $this->Customer->get_group_members($group);
				foreach ($group_members as $member) {
					$customer_list[$member['person_id']] = 1;
				}
			}
		}
		if (!empty($customer_individuals)) {
			foreach ($customer_individuals as $individual) {
				// ADD EACH INDIVIDUAL
				$customer_list[$individual] = 1;
			}
		}


		$pay_member_account = 0;
		if($this->input->post('pay_member_balance') == 'on'){
			$pay_member_account = 1;
		}
		$pay_customer_account = 0;
		if($this->input->post('pay_customer_balance') == 'on'){
			$pay_customer_account = 1;
		}

		foreach ($customer_list as $person_id => $one) {
			$invoice_data = array(
				'course_id' => $this->session->userdata('course_id'),
				'bill_start' => $bill_start,
				'bill_end' => $bill_end,
				'billing_id' => 0,
				'pay_member_account' => $pay_member_account,
				'pay_customer_account' => $pay_customer_account,
				'person_id' => $person_id,
				'employee_id' => $this->session->userdata('person_id'),
				'show_account_transactions' => $show_account_transactions,
				'items' => $items,
				'email_invoice' => $email_invoice,
				'credit_card_id' => $credit_card_id,
				'due_date' => $due_date,
                'date' => $invoice_date
			);

			$invoice_response = $this->Invoice->save($invoice_data);
			$invoice_id = $invoice_response['invoice_id'];

			// If selected, charge credit card and update the invoice
			if((int) $credit_card_id != 0){

				$due = (float) $invoice_response['total'];

				// Charge the card, charge_info is passed by reference and
				// will be updated with credit_card_payment_id
				$charge_success = $this->Invoice->charge_invoice($invoice_id, $this->session->userdata('course_id'), $credit_card_id, $due);
			}

			// If selected, email copy of invoice to customer
			if ($email_invoice){
				$email_success = $this->Invoice->send_email($invoice_id);
			}
		}

		echo json_encode(array('success' => $invoice_response['invoice_id'], 'email_status' => $email_success, 'charge_status' => $charge_success));
	}

	function edit_invoice_totals($invoice_id)
	{
		$data = array();
		$data['invoice_info'] = $this->Invoice->get_info($invoice_id);
		$this->load->view('invoices/edit', $data);
	}

	function save_invoice_totals($invoice_id)
	{
		$paid = $this->input->post('paid');
		$overdue = $this->input->post('overdue');
		$notes = $this->input->post('notes');
        $new_date = date('Y-m-d 00:00:00', strtotime($this->input->post('change_date')));
        $due_date = date('Y-m-d 00:00:00', strtotime($this->input->post('due_date')));

        $original_invoice = $this->Invoice->get_info($invoice_id);
		$existing_date = date('Y-m-d 00:00:00', strtotime($original_invoice[0]['date']));
		$manual_change = $invoice_data = array(
			'paid'=>$paid,
			'overdue'=>$overdue,
			'notes'=>$notes
		);
        $invoice_data['due_date'] = $due_date;
        $manual_change['new_date'] = $manual_change['previous_date'] = $existing_date;
        if ($existing_date != $new_date) {
            // Change invoice date, and change date of all transactions associated with this invoice
            $invoice_data['date'] = $manual_change['new_date'] = $new_date;
            $this->Invoice->update_associated_transaction_dates($invoice_id, $new_date);
        }
		$manual_change['previous_paid'] = $original_invoice[0]['paid'];
		$manual_change['previous_overdue'] = $original_invoice[0]['overdue'];
		$manual_change['invoice_id'] = $invoice_id;
		$manual_change['course_id'] = $this->session->userdata('course_id');
		$manual_change['person_id'] = $this->session->userdata('person_id');
		$this->Invoice->record_manual_change($manual_change);
		$invoice_response = $this->Invoice->save($invoice_data, $invoice_id);
		echo json_encode(array('success' => true, 'billing_id' => $invoice_response));
	}

	function save_recurring_invoice($billing_id = null){

		if($billing_id == -1){
			$billing_id = null;
		}
		$customer_list = array();
		$title = $this->input->post('billing_title');
		$customer_id = $this->input->post('customer_id');
		if ($customer_id != '') {
			$customer_list[$customer_id] = 1;
		}
		$items = $this->input->post('items');
		$never_end = $this->input->post('end_never');
		$end_date = $this->input->post('end_date');
		$due_days = $this->input->post('due_days');
		$auto_bill_delay = $this->input->post('auto_bill_delay');
		$customer_groups = $this->input->post('campaign_group');
		$customer_individuals = $this->input->post('campaign_individuals');
		foreach ($customer_groups as $group) {
			// ADD GROUP
			$group_members = $this->Customer->get_group_members($group);
			foreach ($group_members as $member) {
				$customer_list[$member['person_id']] = 1;
			}
		}
		foreach ($customer_individuals as $individual) {
			// ADD EACH INDIVIDUAL
			$customer_list[$individual] = 1;
		}
		if($never_end == 1){
			$end_date = '0000-00-00';
		}

		if($due_days === false || $due_days == ''){
			$due_days = 30;
		}
		$batch_id = 0;
		if (count($customer_list) > 1)
			$batch_id = $this->Customer_billing->get_next_batch_id();

        $frequency = $this->input->post('frequency');

		foreach ($customer_list as $person_id => $one) {
			$billing_data = array(
				'title' => $title,
				'batch_id'=>$batch_id,
				'course_id' => $this->session->userdata('course_id'),
				'employee_id' => $this->session->userdata('person_id'),
				'show_account_transactions' => (int) $this->input->post('show_account_transactions'),
				'email_invoice' => (int) $this->input->post('email_invoice'),
				'frequency' => $frequency > 0 ? $frequency : 1,
				'frequency_period' => $this->input->post('frequency_period'),
				'frequency_on' => $this->input->post('frequency_on'),
				'frequency_on_date' => $this->input->post('frequency_on_date'),
				'start_date' => $this->input->post('start_date'),
				'end_date' => $end_date,
				'due_days' => (int) $due_days,
				'auto_bill_delay' => $auto_bill_delay,
				'auto_pay_overdue' => $this->input->post('auto_pay_overdue') ? '1' : '0',
				'attempt_limit' => $this->input->post('attempt_limit'),
				'credit_card_id' => (int) $this->input->post('credit_card_id') ? (int) $this->input->post('credit_card_id') : $this->input->post('cc_dropdown'),
				'person_id' => $person_id,
				'items' => $items
			);

			$response = $this->Customer_billing->save($billing_data, $billing_id);
		}
		echo json_encode(array('success' => true, 'billing_id' => $response));
	}

	/*
	Returns customer table data rows. This will be called with AJAX.
	*/
	function search($offset = 0, $group_id = 'all', $pass_id = 'all')
	{
        if ($pass_id == 'undefined') {
            $pass_id = 'all';
        }

		$data = array();
		$search=$this->input->post('search');
		$this->show_deleted_customers = $this->input->post('show_deleted_customers');
		$data_rows=get_people_manage_table_data_rows($this->Customer->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $group_id, $pass_id, $offset, $this->show_deleted_customers),$this);
		//echo $this->db->last_query();
		$config['base_url'] = site_url('customers/index');
		$config['total_rows'] = $this->Customer->search($search, 0, $group_id, $pass_id, null, $this->show_deleted_customers);
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['data_rows'] = $data_rows;
		echo json_encode($data);
	}

	function customer_search($type='')
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->get('term'),100,$type);
		echo json_encode($suggestions);
	}

	function get_all($limit=100000, $offset=0)
	{
		set_time_limit(0);
		ini_set('memory_limit', '500M');
		$customers = $this->Customer->get_all($limit, $offset, false, true);
		echo json_encode(array('status'=>'success','customers'=>$customers->result_array()));
	}

	function get_info($customer_id = false, $course_id = false)
	{
		if (!$customer_id)
		{
			echo json_encode(array('status'=>'failed','message'=>'Missing customer_id','customer_info'=>array()));
		}
		else
		{
			$this->load->model('Image');
			// GET GROUP AND PASS INFO TOO AND IMAGE LINK
			$customer_info = $this->Customer->get_info($customer_id, $course_id);
			$customer_info->groups = $this->Customer->get_customer_groups($customer_id);
			$customer_info->passes = $this->Customer->get_customer_passes($customer_id);
			$customer_info->image = 'http://foreupsoftware.com/'.$this->Image->get_thumb_url($customer_info->image_id, 'person');
			echo json_encode(array('status'=>'success','customer_info'=>$customer_info));
		}
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Customer->get_search_suggestions($this->input->get('term'),100, $this->input->get('show_deleted_customers'));
		echo json_encode($suggestions);
	}

	/*
	Loads the customer edit form
	*/
	function view($customer_id=-1)
	{
		$this->load->model('Image');
		$this->load->model('User');
		$this->load->model('Customer_credit_card');
		$this->load->model('Household');
		$this->load->model('Minimum_charge');
		$this->load->model('v2/Customer_model');
		$this->load->library('Sendgrid');
		$this->load->model('Pass');

		$data['person_info']=$this->Customer->get_info($customer_id, $this->session->userdata('course_id'));
		$bounced = '';
		if ($data['person_info']->email != '')
			$bounced = $this->sendgrid->check_bounce($data['person_info']->email);
		$data['bounced'] = $bounced;
		$groups = $this->Customer->get_group_info($customer_id);
		$data['groups']=$groups;
		
		if($this->session->userdata('sales_v2') == 1){
			$passes = $this->Pass->get(array('customer_id' => $customer_id));	
		}else{
			$passes = $this->Customer->get_pass_info($customer_id);
		}
		$data['passes']=$passes;
		
		$data['open_billing'] = isset($open_billing)?$open_billing:'';
		$data['image_thumb_url'] = $this->Image->get_thumb_url($data['person_info']->image_id, 'person');
		$data['price_classes']=($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types());
		$data['credit_cards']=$this->Customer_credit_card->get($customer_id);
		
		$data['minimum_charges'][''] = '- Assign New Charge -';
		$minimum_charges = $this->Minimum_charge->get();
		foreach($minimum_charges as $charge){
			$label = '';
			if($charge['is_active'] == 0){
				$label .= '(DEACTIVATED) ';
			}
			$label .= $charge['name'].' '.to_currency($charge['minimum_amount']);
			$data['minimum_charges'][$charge['minimum_charge_id']] = $label;
		}

		$data['customer_minimum_charges'] = array();
		if(!empty($customer_id) && $customer_id > 0){
			$data['customer_minimum_charges'] = $this->Minimum_charge->get(array('customer_id' => $customer_id));
		}
		
		// LOAD HOUSEHOLD DATA
		$data['household_head'] = '';
		$data['household_members'] = '';
		if ($this->Household->is_head($customer_id))
			$data['household_members'] = $this->Household->get_members($customer_id);
		else if ($this->Household->is_member($customer_id))
			$data['household_head'] = $this->Household->get_head($customer_id);
		$data['controller'] = strtolower(get_class());
		$data['status_array'] = array(0 => 'N/A',1 => 'Red', 2 => 'Yellow', 3 => 'Green');
		$user_info = $this->User->get_info($customer_id);
		$data['person_info']->username = isset($user_info['username']) ? $user_info['username'] : '';
		$data['field_settings'] = $this->Customer_model->get_field_settings();


		$course_model = new \Course();
		$course_info = $course_model->get_info($this->session->userdata('course_id'));
		$data['can_edit_text_subscription'] = $course_info->send_alert_sms_marketing;

		//echo $this->db->last_query();
		$this->load->view("customers/form",$data);
	}

	function unblock_email() {
		$email = $this->input->get('email');
		$this->load->library('Sendgrid');
		echo json_encode(array('success'=>$this->sendgrid->delete_bounce($email)));
	}

	function history($customer_id=-1)
	{
		$this->load->model('Customer_credit_card');
		$data['person_info']=$this->Customer->get_info($customer_id,$this->session->userdata('course_id'));
		$data['course_info']=$this->Course->get_info($this->session->userdata('course_id'));
//		print_r($data['person_info']);
		$data['stats'] = $this->Customer->get_stats($data['person_info']->person_id);
		//print_r($data['stats']);
		$this->load->view("customers/history",$data);
	}

	function billing($customer_id=-1)
	{
		$this->load->model('Customer_credit_card');
		$this->load->model('Customer_billing');
		$this->load->model('Household');
		$this->load->model('Invoice');
		$data['person_info']=$this->Customer->get_info($customer_id,$this->session->userdata('course_id'));
		$data['course_info']=$this->Course->get_info($this->session->userdata('course_id'));
		$data['credit_cards']=$this->Customer_credit_card->get($customer_id);
		$data['billings']=$this->Customer_billing->get_all($customer_id);
		$data['invoices']=$this->Invoice->get_all($customer_id);
		$data['head_of_household'] = $this->Household->is_head($customer_id);

		$this->load->view("customers/form_billing",$data);
	}

	function load_billing($billing_id, $person_id = false)
	{
		$this->load->model('Customer_credit_card');
		$this->load->model('Customer_billing');
		$this->load->library('Sale_lib');

		$data = $this->Customer_billing->get_details($billing_id);
		if($billing_id == -1){
			$data['person_info'] = $this->Customer->get_info($person_id,$this->session->userdata('course_id'));
		}

		$data['is_recurring_billing'] = true;
		$data['course_info'] = $this->Course->get_info($this->session->userdata('course_id'));
		$data['credit_cards'] = $this->Customer_credit_card->get($data['person_id']);
		$data['popup'] = 1;
		$data['editing'] = 1;
		$data['html'] = $this->load->view('customers/invoice', $data, true);

		echo json_encode($data);
	}

	function delete_billing($billing_id)
	{
		$this->load->model('Customer_credit_card');
		$this->load->model('Customer_billing');
		$success = $this->Customer_billing->delete($billing_id);
		echo json_encode(array('deleted_billing'=>$success));
	}

	function load_sales_report_invoice($invoice_id, $pdf = false)
	{
		$this->load->model("Customer_credit_card");
		$invoice_data = $this->Invoice->get_info($invoice_id);
		$data = $invoice_data[0];
		$data['items']=$this->Invoice->get_items($invoice_id);
		$data['person_info']= $this->Customer->get_info($data['person_id'],$this->session->userdata('course_id'));
		$data['course_info']= $this->Course->get_info($data['course_id']);
		$data['credit_cards']= $this->Customer_credit_card->get($data['person_id']);
		$data['popup'] = 1;
		$data['is_invoice'] = true;
		$data['sent'] = true;
		if ($pdf)
		{
			$data['pdf'] = true;
			// $data['emailing_invoice'] = false;
			$invoice_html = $this->load->view('customers/invoice', $data, true);

			$this->load->library('Html2pdf');
			$html2pdf = new Html2pdf('P','A4','en');
			//$html2pdf->setModeDebug();
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->setDefaultFont('Arial');
			$html2pdf->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
			$html2pdf->Output('invoice.pdf');
		}
		else
		{
			return $this->load->view('customers/invoice', $data, false);
		}
	}

	function add_invoice_queue($member_balance, $customer_credit,$start_date, $end_date)
	{
		$this->load->model('Queue_invoice');
		$queue_invoice_data = array(
			'course_id'=>$this->session->userdata('course_id'),
			'member_balance'=>$member_balance,
			'customer_credit'=>$customer_credit,
			'start_date'=>date('Y-m-d', strtotime($start_date)),
			'end_date'=>date('Y-m-d', strtotime($end_date))
		);
		if (!$this->Queue_invoice->exists($queue_invoice_data)) {
			$this->Queue_invoice->save($queue_invoice_data, FALSE);
		}
	}

	function load_invoice($invoice_id = -1, $person_id = false, $pdf = false, $batch_type = false, $member_balance = false, $customer_credit = false, $recurring_billings = false, $start_on = false, $end_on = false, $limit = 10000, $offset = 0)
	{
		$this->load->model('Customer');
		$this->load->model('Customer_credit_card');
		$this->load->model('Course');

		$data = array();

		// If the invoice is being created
		if($invoice_id == -1){
			$data['sent'] = false;
			$course_info = $this->Course->get_info($this->session->userdata('course_id'));
			$data['course_info'] = $course_info;

			if(!empty($person_id)){
				$data['person_info'] = $this->Customer->get_info($person_id,$this->session->userdata('course_id'));
				$data['credit_cards']= $this->Customer_credit_card->get($person_id);
			}

		// If viewing a previously generated invoice
		}else{
			$data = $this->Invoice->get_details($invoice_id);
			$data['sent'] = true;
		}

		if ($pdf)
		{
			$data['pdf'] = true;
			$invoice_html = $this->load->view('customers/invoice', $data, true);

			$snappy = new  \Knp\Snappy\Pdf();
            $snappy->setBinary("application/bin/wkhtmltopdf");
            $snappy->setOption("viewport-size",'1366x1024');
            $snappy->setOption("zoom",'.8');

            $html = "<page style'width:800px;'>".$invoice_html.'</page>';
            header('Content-Type: application/pdf');
            $invoice_name = 'Invoice'.$data['invoice_number'].'_'.$data['person_info']->first_name.'_'.$data['person_info']->last_name;
            header("Content-Disposition: attachment; filename='$invoice_name.pdf'");
            echo $snappy->getOutputFromHtml($html);
        }
		else
		{
			$invoice_html = $this->load->view('customers/invoice', $data, true);
			echo json_encode(array('html' => $invoice_html, 'customer' => isset($data['person_info'])?$data['person_info']:null));
		}
	}

	function email_invoice($invoice_id)
	{
		if(empty((int)$invoice_id)){
			echo json_encode(array('emailed_invoice'=>false));
			return false;
		}
		$this->load->model('Invoice');
		$success = $this->Invoice->send_email($invoice_id);
		echo json_encode(array('emailed_invoice'=>$success));
	}

	function delete_invoice($invoice_id)
	{
		$this->load->model('Customer_credit_card');
		$this->load->model('Invoice');
		$success = $this->Invoice->delete($invoice_id);
		echo json_encode(array('deleted_invoice'=>$success));
	}

	function bulk_edit()
	{
		$data = array();
		/*$suppliers = array('' => lang('items_do_nothing'), '-1' => lang('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name']. ' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['suppliers'] = $suppliers;
		$data['allow_alt_desciption_choices'] = array(
			''=>lang('items_do_nothing'),
			1 =>lang('items_change_all_to_allow_alt_desc'),
			0 =>lang('items_change_all_to_not_allow_allow_desc'));

		$data['serialization_choices'] = array(
			''=>lang('items_do_nothing'),
			1 =>lang('items_change_all_to_serialized'),
			0 =>lang('items_change_all_to_unserialized'));*/
		$data['groups']=$this->Customer->get_group_info($customer_id);
		$data['price_classes']=($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types());

		$this->load->view("customers/form_bulk", $data);
	}

	function bulk_update()
	{
		$customers_to_update=$this->input->post('customer_ids');
		$customer_data = array();

		$customer_data=array(
			'taxable'=>$this->input->post('taxable')=='' ? 0:1,
			'member'=>$this->input->post('member')==''?0:1,
			'price_class'=>$this->input->post('price_class')
		);
		$groups_data = $this->input->post("groups")!=false ? $this->input->post("groups"):array();
		$remove_groups_data = $this->input->post("remove_groups")!=false ? $this->input->post("remove_groups"):array();

		if($this->input->post('activate_online_booking') && $this->input->post("online_booking_password")){
			$errors = $this->Customer->activate_online_booking($customers_to_update,$this->input->post("online_booking_password"));

		}

		if($this->Customer->update_multiple($customer_data,$customers_to_update, $groups_data, $remove_groups_data))
		{
			echo json_encode(array('success'=>true,'errors'=>$errors,'message'=>lang('customers_successful_bulk_edit')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_updating_multiple')));
		}
	}

	// GROUPS FUNCTIONALITY
	function manage_groups()
	{
		$data['groups']=$this->Customer->get_group_info();
		//$data['last_query']=$this->db->last_query();
		$this->load->view('customers/manage_groups',$data);
	}

	function add_group()
	{
		$group_label = $this->input->post("group_label");
		echo json_encode(array('success'=>true,'group_id'=>$this->Customer->add_group($group_label)));
	}

	function save_group_name($group_id)
	{
		$group_label = $this->input->post('group_label');
		$item_cost_plus_percent_discount = $this->input->post('item_cost_plus_percent_discount');

		echo json_encode(array('success'=>$this->Customer->save_group_name($group_id, $group_label, $item_cost_plus_percent_discount)));
	}

	function delete_group($group_id)
	{
		$this->Customer->delete_group($group_id);
		echo json_encode(array('success'=>true));
	}

	// PASSES FUNCTIONALITY
	function manage_passes()
	{
		$data['passes']=$this->Customer->get_pass_info();
		//$data['last_query']=$this->db->last_query();
		$this->load->view('customers/manage_passes',$data);
	}

	function add_pass()
	{
		$pass_label = $this->input->post("pass_label");
		echo json_encode(array('success'=>true,'pass_id'=>$this->Customer->add_pass($pass_label)));
	}

	function save_pass_name($pass_id)
	{
		$pass_label = $this->input->post('pass_label');
		echo json_encode(array('success'=>$this->Customer->save_pass_name($pass_id, $pass_label)));
	}

	function delete_pass($pass_id)
	{
		$this->Customer->delete_pass($pass_id);
		echo json_encode(array('success'=>true));
	}

	function save_image($person_id = null){
		$this->load->model('Image');
		$image_id = $this->input->post('image_id', null);
		if($image_id === null){
			$success = true;
		}else{
			$success = $this->Customer->save_image($person_id, $image_id);
		}
		$url = $this->Image->get_thumb_url($image_id, 'person');

		echo json_encode(array('success'=>true, 'image_id'=>$image_id, 'thumb_url'=>$url));
	}

	function add_email($customer_id, $type)
	{
		$data['customer_id'] = $customer_id;
		$data['type'] = $type;
		$this->load->view("customers/add_email", $data);
	}

	//Account Balance Tracking
	function loyalty_adjustment($person_id=-1)
	{
		$this->load->model('Household');
		$household_head = $this->Household->get_head($person_id);
		if (!empty($household_head['household_id'])) {

			$this->load->view('customers/under_household', array('household_head'=>$household_head['first_name'].' '.$household_head['last_name']));
		}
		else {
			$data['customer_info']=$this->Customer->get_info($person_id,$this->session->userdata('course_id'));
			$this->load->view("customers/loyalty_adjustment",$data);
		}
	}

	function loyalty_details($person_id=-1)
	{
		$data['customer_info']=$this->Customer->get_info($person_id,$this->session->userdata('course_id'));

		$this->load->view("customers/loyalty_details",$data);
	}

	function save_loyalty_transaction($person_id=-1)
	{
		if($this->Customer_loyalty->save_transaction($person_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_transaction'),'person_id'=>$person_id));
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_transaction'),'person_id'=>-1));
		}
	}
	//Account Balance Tracking
	function account_adjustment($person_id=-1)
	{
		$this->load->model('Household');
		$household_head = $this->Household->get_head($person_id);
		if (!empty($household_head['household_id'])) {

			$this->load->view('customers/under_household', array('household_head'=>$household_head['first_name'].' '.$household_head['last_name']));
		}
		else {
			$data['customer_info']=$this->Customer->get_info($person_id,$this->config->item('course_id'));
			$this->load->view("customers/account_adjustment",$data);
		}
	}

	function account_details($person_id=-1)
	{
		$data['customer_info']=$this->Customer->get_info($person_id,$this->session->userdata('course_id'));

		$this->load->view("customers/account_details",$data);
	}

	function save_account_transaction($person_id=-1)
	{
		$customer_info = $this->Customer->get_info($person_id,$this->session->userdata('course_id'));
        $add_subtract = $this->input->post('add_subtract');
        $remaining_balance = $customer_info->account_limit - $customer_info->account_balance;
        $remaining_balance = '$'.number_format($remaining_balance < 0 ? $remaining_balance : 0,2);

        if($customer_info->account_limit != 0 && (float)$add_subtract < 0 && (float)$add_subtract + $customer_info->account_balance < $customer_info->account_limit)
        {
                echo json_encode(array('success'=>false,'message'=>lang('customers_transaction_error_account_limit').' '.lang('customers_remaining_account_credit_available').': '.$remaining_balance,'person_id'=>$person_id));
        }
        else if($this->Account_transactions->save('customer', $person_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_transaction'),'person_id'=>$person_id));
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_transaction'),'person_id'=>-1));
		}
	}
	//Account Balance Tracking
	function member_account_adjustment($person_id=-1)
	{
		$this->load->model('Household');
		$household_head = $this->Household->get_head($person_id);
		if ($household_head['household_id']) {

			$this->load->view('customers/under_household', array('household_head'=>$household_head['first_name'].' '.$household_head['last_name']));
		}
		else {
			$data['customer_info']=$this->Customer->get_info($person_id,$this->session->userdata('course_id'));
			$this->load->view("customers/member_account_adjustment",$data);
		}
	}

	function member_account_details($person_id=-1)
	{
		$data['customer_info']=$this->Customer->get_info($person_id,$this->session->userdata('course_id'));

		$this->load->view("customers/member_account_details",$data);
	}

	function save_member_account_transaction($person_id=-1)
	{
		$customer_info = $this->Customer->get_info($person_id,$this->session->userdata('course_id'));
        $add_subtract = $this->input->post('add_subtract');
        $remaining_balance = $customer_info->member_account_limit - $customer_info->member_account_balance;
        $remaining_balance = '$'.number_format($remaining_balance < 0 ? $remaining_balance : 0,2);

        if($customer_info->member_account_limit != 0 && (float)$add_subtract < 0 && (float)$add_subtract + $customer_info->member_account_balance < $customer_info->member_account_limit)
        {
                echo json_encode(array('success'=>false,'message'=>lang('customers_transaction_error_account_limit').' '.lang('customers_remaining_account_credit_available').': '.$remaining_balance,'person_id'=>$person_id));
        }
        else if($this->Account_transactions->save('member', $person_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_transaction'),'person_id'=>$person_id));
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_transaction'),'person_id'=>-1));
		}
	}
	//Account Balance Tracking
	function billing_account_adjustment($person_id=-1)
	{
		$this->load->model('Household');
		$household_head = $this->Household->get_head($person_id);
		if ($household_head['household_id']) {

			$this->load->view('customers/under_household', array('household_head'=>$household_head['first_name'].' '.$household_head['last_name']));
		}
		else {
			$data['customer_info']=$this->Customer->get_info($person_id,$this->session->userdata('course_id'));
			$this->load->view("customers/billing_account_adjustment",$data);
		}
	}

	function billing_account_details($person_id=-1)
	{
		$data['customer_info']=$this->Customer->get_info($person_id,$this->session->userdata('course_id'));

		$this->load->view("customers/billing_account_details",$data);
	}

	function save_billing_account_transaction($person_id=-1)
	{
		if($this->Account_transactions->save('invoice', $person_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_transaction'),'person_id'=>$person_id));
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_transaction'),'person_id'=>-1));
		}
	}
	//------------------------------------------- Account Balance
	function save_email($customer_id, $type)
	{
		$person_data = array(
			'email'=>$this->input->post('email')
		);
		$customer_data = array();
		$response = array(
			'person_id'=>$customer_id,
			'type'=>$type,
			'email'=>$this->input->post('email'),
			'saved_email'=>FALSE
		);
		if ($this->Customer->save($person_data, $customer_data, $customer_id)) {
			$response['saved_email'] = TRUE;
		}
		echo json_encode($response);
	}
	/*
	Validate user name
	*/
	function validate_user_name($customer_id, $username)
	{
		$is_available = true;
		if(!empty($username)){
			$is_available = $this->Customer->username_is_available($customer_id, $username);
		}

		echo json_encode($is_available);
	}
	/*
	Inserts/updates a customer
	*/
	function save($customer_id=-1)
	{
        // Validate username
        if(!empty($this->input->post('username'))) {
            if (strlen($this->input->post('username')) < 5 ) {
                echo json_encode(array('success'=>false,'message'=>lang('employees_username_minlength')));
                return;
            }
            $is_available = $this->Customer->username_is_available($customer_id, $this->input->post('username'));
            if (!$is_available) {
                echo json_encode(array('success'=>false,'message'=>lang('username_in_use')));
                return;
            }
        }
        if (!empty($this->input->post('account_number'))) {
            $account_customer = $this->Customer->get_id_from_account_number($this->input->post('account_number'));
            if (!empty($account_customer) && $customer_id != $account_customer) {
                echo json_encode(array('success' => false, 'message' => lang('customers_account_number_in_use')));
                return;
            }
        }


        $from_customer_edit_page = $this->input->post('from_customer_edit_page');
		$person_data = array(
		'first_name'=>trim($this->input->post('first_name')),
		'last_name'=>trim($this->input->post('last_name')),
		'email'=>trim($this->input->post('email')),
		'phone_number'=>$this->input->post('phone_number'),
		'cell_phone_number'=>$this->input->post('cell_phone_number'),
		'birthday'=>$this->input->post('birthday'),
		'address_1'=>$this->input->post('address_1'),
		'address_2'=>$this->input->post('address_2'),
		'city'=>$this->input->post('city'),
		'state'=>$this->input->post('state'),
		'zip'=>$this->input->post('zip'),
		'country'=>$this->input->post('country'),
		'comments'=>$this->input->post('comments')
		);
		$taxable = $this->input->post('taxable');
		$customer_data=array(
		'company_name' => $this->input->post('company_name'),
		'account_number'=>$this->input->post('account_number')=='' ? null:$this->input->post('account_number'),
		'account_balance'=>$this->input->post('account_balance'),
		'account_limit'=>$this->input->post('account_limit'),
		'invoice_balance'=>$this->input->post('invoice_balance'),
		'invoice_email'=>$this->input->post('invoice_email'),
		'member_account_balance'=>$this->input->post('member_account_balance'),
		'member_account_limit'=>$this->input->post('member_account_limit'),
		'account_balance_allow_negative'=>$this->input->post('account_balance_allow_negative')==''?0:1,
		'member_account_balance_allow_negative'=>$this->input->post('member_account_balance_allow_negative')==''?0:1,
		'discount'=>$this->input->post('discount'),
		'taxable'=> empty($taxable) ? 1:0,
		'member'=>$this->input->post('member')==''?0:1,
		'use_loyalty'=>$this->input->post('use_loyalty')==''?0:1,
		'loyalty_points'=>$this->input->post('loyalty_points'),
		'price_class'=>$this->input->post('price_class')?$this->input->post('price_class'):'price_category_1',
		'opt_out_email'=>$this->input->post('email_unsubscribed')==''?0:1,
		'opt_out_text'=>$this->input->post('text_unsubscribed')==''?0:1,
        'status_flag' => $this->input->post('cust_status')
		);
		if (!$this->permissions->is_employee() && $this->input->post('username'))
		{
			$customer_data['username'] = $this->input->post('username');
		}

		//Password has been changed OR first time password set
		if($this->input->post('password')!='' && (!$this->permissions->is_employee())){
			$password = $this->input->post('password');
			$customer_data['password'] = md5($password);
			$customer_data['unencrypted_password'] = $password;
            if ($password != $this->input->post('repeat_password')) {
                echo json_encode(array('success'=>false,'message'=>lang('employees_password_must_match')));
                return;
            }
            if (strlen($password) < 8 ) {
                echo json_encode(array('success'=>false,'message'=>lang('employees_password_minlength')));
                return;
            }
		}


		$new_customer = 0;
		if ($customer_id == -1) {
			$new_customer = 1;
			$customer_data['course_id']=$this->session->userdata('course_id');
		}
		$groups_data = $this->input->post("groups")!=false ? $this->input->post("groups"):'delete';
		$passes_data = $this->input->post("passes")!=false ? $this->input->post("passes"):'delete';
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		$cur_customer_info = $this->Customer->get_info($customer_id, $this->config->item('course_id'));
		$giftcard_data = array();

		$text_subscribed = $this->input->post('text_subscribed')? "subscribed" : "unsubscribed";
		$this->load->model("marketing_texting");
		$marketing_texting = new \Marketing_Texting();
		$marketing_texting->updateSubscriptionById($customer_id,$this->config->item('course_id'),$text_subscribed);


		if($this->Customer->save($person_data,$customer_data,$customer_id, $giftcard_data, $groups_data, $passes_data))
		{
			//send username and password via email
			if (!empty($customer_data['password'])) {
				$this->Customer->send_username_password_email($person_data, $customer_data, $password);
			}

			if ($this->config->item('mailchimp_api_key'))
			{
				$this->Person->update_mailchimp_subscriptions($this->input->post('email'), $this->input->post('first_name'), $this->input->post('last_name'), $this->input->post('mailing_lists'));
			}

            // logging parameters
            $act_type = 1; // we don't know yet
            $emp_id = $_SESSION['foreup']['emp_id']*1;
            $comments = '';
            $pers_alt = $customer_id*1;
            //TODO: Need a way to resolve which tables were actually updated
            $tbls = 'foreup_customers';

			//New customer
			if($customer_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_adding').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_data['person_id']));
				$customer_id = $customer_data['person_id'];

                // logging parameters
                $act_type = 3; // insert
                $pers_alt = $customer_id*1;
                $comments = 'Customer created from customers.php controller.';
			}
			else //previous customer
			{
				// UPDATE ACCOUNT BALANCE IF A DIFFERENT AMOUNT IS ENTERED IN
				if ($cur_customer_info->account_balance != $customer_data['account_balance'])
				{
					$account_transaction_data=array(
						'course_id' =>$this->session->userdata('course_id'),
						'trans_customer'=>$customer_id,
						'trans_date'=>date('Y-m-d H:i:s'),
						'trans_user'=>$employee_id,
						'trans_comment'=>lang('items_manually_editing_of_quantity'),
						'trans_description'=>'Customer Edit',
						'trans_amount'=>$cur_customer_info ? (float)$this->input->post('account_balance') - (float)$cur_customer_info->account_balance : $this->input->post('account_balance'),
						'running_balance'=>$customer_data['account_balance']
					);
					$this->Account_transactions->insert('customer', $account_transaction_data);
				}
				// UPDATE MEMBER ACCOUNT BALANCE IF A DIFFERENT AMOUNT IS ENTERED IN
				if ($cur_customer_info->member_account_balance != $customer_data['member_account_balance'])
				{
					$m_account_transaction_data=array(
						'course_id' =>$this->session->userdata('course_id'),
						'trans_customer'=>$customer_id,
						'trans_date'=>date('Y-m-d H:i:s'),
						'trans_user'=>$employee_id,
						'trans_comment'=>lang('items_manually_editing_of_quantity'),
						'trans_description'=>'Customer Edit',
						'trans_amount'=>$cur_customer_info ? (float)$this->input->post('member_account_balance') - (float)$cur_customer_info->member_account_balance : $this->input->post('member_account_balance'),
						'running_balance'=>$customer_data['member_account_balance']
					);
					$this->Account_transactions->insert('member', $m_account_transaction_data);
				}

				if($cur_customer_info->opt_out_email != $customer_data['opt_out_email'] && $customer_data['opt_out_email'] == 0)
				{

					$this->load->library('Sendgrid');
					//Delete the suppression record in sendgrid
					$this->sendgrid->delete_unsubscribe($person_data['email']);
				}

				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_updating').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_id));

                // logging parameters
                $act_type = 4; // update
                $comments = 'Customer updated from customers.php controller.';
			}


			// UPDATE INVOICE BALANCE IF A DIFFERENT AMOUNT IS ENTERED IN
			if ($cur_customer_info->invoice_balance != $customer_data['invoice_balance'])
			{
				$i_account_transaction_data = array(
					'course_id' =>$this->session->userdata('course_id'),
					'trans_customer'=>$customer_id,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_comment'=>lang('items_manually_editing_of_quantity'),
					'trans_description'=>'Customer Edit',
					'trans_amount'=>$cur_customer_info ? (float)$this->input->post('invoice_balance') - (float)$cur_customer_info->invoice_balance : $this->input->post('invoice_balance'),
					'running_balance'=>$customer_data['invoice_balance']
				);

				$this->Account_transactions->insert('invoice', $i_account_transaction_data);
			}

			// UPDATE LOYALTY BALANCE IF A DIFFERENT AMOUNT IS ENTERED IN
			if ($cur_customer_info->loyalty_points != $customer_data['loyalty_points'])
			{
				$loyalty_transaction_data=array(
					'trans_customer'=>$customer_id,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_comment'=>lang('items_manually_editing_of_quantity'),
					'trans_description'=>'Customer Edit',
					'trans_amount'=>$cur_customer_info ? (float)$this->input->post('loyalty_points') - (float)$cur_customer_info->loyalty_points : $this->input->post('loyalty_points')
				);
				$this->Customer_loyalty->insert($loyalty_transaction_data);
			}
			// SAVE HOUSEHOLD INFORMATION IF PRESENT
			$household_members = $this->input->post('household_member_id');
			if ($from_customer_edit_page || ($household_members && count($household_members) > 0))
			{
                if (empty($household_members) || count($household_members) == 0) {
                    $household_members = array();
                }
				$this->load->model("Household");
				$this->Household->save($household_members, $customer_id, $from_customer_edit_page);
			}

			$this->load->model('Minimum_charge');
			$this->Minimum_charge->save_customer_charges($customer_id, $this->input->post('minimum_charges'));
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_adding_updating').' '.
			$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));
		}
	}

	// Should this be moved to customer_billing.php?
	function save_billing($billing_id = -1, $customer_id = -1)
	{
		$this->load->model('Invoice');
		$this->load->model('Customer_billing');
		$this->load->library('Sale_lib');
		$this->load->model('Item_taxes');
		$this->load->model('Item_kit_taxes');

		$frequency = $this->input->post('frequency');

		// Invoice/billing items
		$title = $this->input->post('billing_title');
		$descriptions = $this->input->post('description');
		$item_ids = $this->input->post('item_id');
		$prices = $this->input->post('price');
		$quantities = $this->input->post('quantity');
		$item_types = $this->input->post('type');
		$taxes = $this->input->post('tax');

		$bill_day = $this->input->post('bill_day');
		$bill_month = $this->input->post('bill_month');
		$cc_dropdown = $this->input->post('cc_dropdown');
		$email_invoice = $this->input->post('email_invoice');
		$end_month = $this->input->post('end_month');
		$pay_account_balance = $this->input->post('pay_account_balance');
		$pay_member_balance = $this->input->post('pay_member_balance');
		$include_itemized_sales = $this->input->post('include_itemized_sales');
		$show_overdue_items = $this->input->post('show_overdue_items');
		$start_date = $this->input->post('start_date');
		$start_month = $this->input->post('start_month');
		$generate_days_before = $this->input->post('generate_days_before');

		if ($frequency == 'one_time')
		{
			$member_nickname = ($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname'));
			$customer_credit_nickname = ($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname'));
			$invoice_data = array(
				'course_id'=>$this->session->userdata('course_id'),
				'month_billed'=>date('Y-m-d'),
				'credit_card_id'=>$cc_dropdown,
				'billing_id'=>'',
				'person_id'=>$customer_id,
				'include_itemized_sales'=>$include_itemized_sales=='on'?1:0,
				'show_overdue_items'=>$show_overdue_items=='on'?1:0
			);
			$this->Invoice->save($invoice_data);

			// Loop through invoice items and calculate totals
			$item_data = array();
			$calculated_total = 0;
			foreach ($descriptions as $index => $description)
			{
				if(empty($description)){
					continue;
				}

				if(!empty($taxes[$index])){
					$tax_percent = $taxes[$index];
				}else{
					$tax_percent = 0;
				}
				$price = $prices[$index];
				$qty = $quantities[$index];
				$item_id = $item_ids[$index];
				$item_type = $item_types[$index];

				$item_data[] = array(
					'invoice_id' => $invoice_data['invoice_id'],
					'item_id' => $item_ids[$index],
					'item_type' => $item_type,
					'line_number' => $index,
					'description' => $description,
					'quantity' => $qty,
					'amount' => $price,
					'tax' => $tax_percent,
					'pay_account_balance'=> ($customer_credit_nickname === $description ? ($pay_account_balance=='on'?1:0) : 0),
					'pay_member_balance'=>($member_nickname === $description ? ($pay_member_balance=='on'?1:0) : 0)
				);
				$item_subtotal = round((int) $qty * (float) $price, 2);

				// If invoice line is inventory item, retrieve taxes associated
				if(!empty($item_id) && $item_type == 'item'){
					$item_tax = $this->sale_lib->calculate_item_tax($item_subtotal, $item_id, $item_type);

				}else if(!empty($item_id) && $item_type == 'item_kit'){
					$item_taxes = $this->Item_kit_taxes->get_info($item_id);
					$item_tax = $this->sale_lib->calculate_item_tax($item_subtotal, $item_id, $item_type);

				}else{
					$item_tax = round($item_subtotal * ($tax_percent / 100), 2);
				}

				$calculated_total += $item_subtotal + $item_tax;
			}
			$this->Invoice->save_items($item_data);

			// Charge credit card and update the invoice
			$invoice_id = $invoice_data['invoice_id'];
			$invoice_data = array(
				'total' => $calculated_total
				// 'paid'=>(true || $this->Customer->charge())?$calculated_total:'0.00'
			);
			$this->Invoice->save($invoice_data, $invoice_id);

			// Email copy of invoice
			if ($email_invoice){
				$this->Invoice->send_email($invoice_id);
			}

		}else{

			$billing_data = array(
				'title'=>$title,
				'course_id'=>$this->session->userdata('course_id'),
				'employee_id'=>$this->session->userdata('person_id'),
				'start_date'=>$start_date,
				'start_month'=>$start_month,
				'end_month'=>$end_month,
				'pay_account_balance'=>$pay_account_balance=='on'?1:0,
				'pay_member_balance'=>$pay_member_balance=='on'?1:0,
				'include_itemized_sales'=>$include_itemized_sales=='on'?1:0,
				'show_overdue_items'=>$show_overdue_items=='on'?1:0,
				'email_invoice'=>$email_invoice=='on'?1:0,
				'description'=>'',
				'frequency'=>$frequency,
				'month'=>$bill_month,
				'day'=>$bill_day,
				'generate_days_before'=>$generate_days_before,
				'credit_card_id'=>$cc_dropdown,
				'person_id'=>$customer_id
			);

			$this->Customer_billing->save($billing_data, $billing_id);

			$item_data = array();
			$calculated_total = 0;

			foreach ($descriptions as $index => $description)
			{
				if(empty($description)){
					continue;
				}

				if(!empty($taxes[$index])){
					$tax_percent = $taxes[$index];
				}else{
					$tax_percent = 0;
				}
				$price = $prices[$index];
				$qty = $quantities[$index];
				$item_id = $item_ids[$index];

				$item_data[] = array(
					'billing_id' => $billing_data['billing_id'],
					'item_id' => $item_id,
					'line_number' => $index,
					'description' => $description,
					'quantity' => $qty,
					'amount' => $price,
					'tax' => $tax_percent
				);
				$item_subtotal = round((int) $qty * (float) $price, 2);

				// If invoice line is inventory item, retrieve taxes associated
				if(!empty($item_id)){
					$item_tax = $this->sale_lib->calculate_item_tax($item_subtotal, $item_id,'item');
				}else{
					$item_tax = round($item_subtotal * ($tax_percent / 100), 2);
				}

				$calculated_total += $item_subtotal + $item_tax;
			}
			$this->Customer_billing->save_items($item_data, $billing_data['billing_id']);

			// Charge credit card and update the invoice
			$billing_id = $billing_data['billing_id'];
			$billing_data = array(
				'total' => $calculated_total
			);
			$this->Customer_billing->save($billing_data, $billing_id);
		}

		echo json_encode(array('sql'=>$this->db->last_query(), 'tf'=>(!$invoice_data['invoice_id'] or !$this->Invoice->exists($invoice_data['invoice_id'])), 'invoice_id'=>$id_were_passing_in, 'exists'=>!$this->Invoice->exists($invoice_data['invoice_id']), 'true_id'=>!$invoice_data['invoice_id']));
	}
	/*
	This deletes customers from the customers table
	*/
	function delete($confirmed = false)
	{
		if ($confirmed)
		{
			$customers_to_delete=$this->input->post('customer_ids');

			if($this->Customer->delete_list($customers_to_delete))
			{
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted').' '.
				count($customers_to_delete).' '.lang('customers_one_or_multiple')));
			}
			else
			{
				echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
			}

		}
		else {
			$customers_to_delete=$this->input->post('ids');
			//print_r($items_to_delete);
			$result = array();
			foreach ($customers_to_delete as $customer_id)
			{
				$customer_info = $this->Customer->get_info($customer_id);

				$result[] = array('name' =>$customer_info->last_name.', '.$customer_info->first_name, 'id'=> $customer_id);
			}
			$data['customers'] = $result;

			$this->load->view('customers/confirm_delete', $data);
		}

	}

	function undelete($person_id){
		$response['success'] = $this->Customer->undelete($person_id);
		echo json_encode($response);
	}

	function excel()
	{
		//$data = file_get_contents("import_customers.csv");
		$data = 'First Name,Last Name,E-Mail,Phone Number,Birthday,Address 1,Address2,City,State,Zip,Country,Comments,Account Number,Customer Credits,Allow Negative Customer Credits,Member Account Balance,Allow Negative Member Account,Non-Taxable,Company Name,Gift Card Number,Gift Card Balance,Price Category,Member,Groups,Unsubscribed Email,Loyalty Points
Customer,One,c1@a.com,(999) 999-9999,1/1/2001,9999 Street 1,,Rockford,MI,49341,,"comment1",,,,-13666.03,y,,,,,,y,3,,';
		$name = 'import_customers.csv';
		force_download($name, $data);
	}

	function excel_import()
	{
		$this->load->view("customers/excel_import", null);
	}
    /* added for excel expert */
	function excel_export($group_id = '') {
		set_time_limit(0);
		ini_set('memory_limit', '500M');
		$data = $this->Customer->get_all_for_download(100000,0,$group_id)->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array('First Name', 'Last Name', 'E-Mail', 'Phone Number', 'Address 1', 'Address 2', 'City', 'State', 'Zip', 'Country', 'Comments', 'Account Number', 'Taxable', 'Company Name', 'Unsubscribe Email','Birthday', 'Groups', 'Phone Number 2', 'Customer Credit', 'Member Account','Open Invoices', 'Loyalty');
		$rows[] = $row;
		foreach ($data as $r) {
			if ($r->birthday == '0000-00-00' || $r->birthday == '1969-12-31') // too bad if they really were born on the Unix Epoch...
				$r->birthday = '';
			else
				$r->birthday = date('Y-m-d', strtotime($r->birthday));
			$row = array(
				$r->first_name,
				$r->last_name,
				$r->email,
				$r->phone_number,
				$r->address_1,
				$r->address_2,
				$r->city,
				$r->state,
				$r->zip,
				$r->country,
				$r->comments,
				$r->account_number,
				$r->taxable ? 'y' : '',
				$r->company_name,
				$r->opt_out_email,
				$r->birthday,
				$r->groups,
				$r->cell_phone_number,
				$r->account_balance,
				$r->member_account_balance,
				$r->invoice_balance,
				$r->loyalty_points
			);
			$rows[] = $row;
		}

		$content = array_to_csv($rows);
		force_download('customers_export' . '.csv', $content);
		exit;
	}


	function do_excel_import()
	{
		//echo json_encode(array('success'=>true,'message'=>lang('customers_import_successfull')));
		//return;
		$this->db->trans_start();

        $msg = 'do_excel_import';
		$failCodes = array();
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = lang('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg) );
			return;
		}
		else
		{
			$path_info = pathinfo($_FILES['file_path']['name']);
			if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE && strtolower($path_info['extension']) == 'csv')
			{
                // from import_customers.csv on the feature/import branch and the excel_export function above
                // also accepts data already using our database naming convention
                $key_namespace_remap = array(
                    'First Name'=>'first_name', 'first_name'=>'first_name',
                    'Last Name'=>'last_name', 'last_name'=>'last_name',
                    'Primary E-Mail'=>'email', 'email'=>'email', 'E-Mail'=>'email',
                    'Invoice E-Mail'=>'invoice_email', 'invoice_email'=>'invoice_email',// not used?
                    'Phone Number'=>'phone_number', 'phone_number'=>'phone_number',
                    'Cell Phone Number'=>'cell_phone_number', 'Phone Number 2'=>'cell_phone_number',
                        'cell_phone_number'=>'cell_phone_number',
                    'Birthday'=>'birthday', 'birthday'=>'birthday',
                    'Address 1'=>'address_1', 'Address1'=>'address_1', 'address_1'=>'address_1',
                    'Address 2'=>'address_2', 'Address2'=>'address_2', 'address_2'=>'address_2',
                    'City'=>'city', 'city'=>'city',
                    'State'=>'state', 'state'=>'state',
                    'Zip'=>'zip', 'zip'=>'zip',
                    'Country'=>'country', 'country'=>'country',
                    'Email Unsubscribed'=>'opt_out_email', 'Unsubscribe Email'=>'opt_out_email', 'Unsubscribed Email'=>'opt_out_email',
                        'opt_out_email'=>'opt_out_email',
                    'Text Unsubscribed'=>'opt_out_text','opt_out_text'=>'opt_out_text',// not used?
                    'Comments'=>'comments', 'comments'=>'comments',
                    'Status'=>'status', 'status'=>'status',// not used?
                    'Account Number'=>'account_number', 'account_number'=>'account_number',
                    'Account Balance'=>'account_balance', 'account_balance'=>'account_balance',
                    'Allow Negative Account Balance'=>'account_balance_allow_negative',
                        'account_balance_allow_negative'=>'account_balance_allow_negative',
                    'Company Name'=>'company_name', 'company_name'=>'company_name',
                    'Price Class'=>'price_class', 'Price Category'=>'price_class', 'price_class'=>'price_class',
                    'Discount'=>'discount', 'discount'=>'discount',// not used?
                    'Non-Taxable'=>'taxable', 'taxable'=>'taxable', // how is this interpreted in the software?
                    'Member'=>'member', 'member'=>'member',
                    'Member Balance'=>'member_account_balance', 'Member Account Balance'=>'member_account_balance', 'member_account_balance'=>'member_account_balance',
                    'Allow Negative Member Balance'=>'member_account_balance_allow_negative',
                        'member_account_balance_allow_negative'=>'member_account_balance_allow_negative',
                        'Allow Negative Member Account'=>'member_account_balance_allow_negative',
                    'Member Balance Limit'=>'member_balance_limit',
                        'member_balance_limit'=>'member_balance_limit',
                    'Customer Credit'=>'account_balance', 'Customer Credits'=>'account_balance', 'customer_credit'=>'account_balance',
                    'Allow Negative Customer Credit'=>'account_balance_allow_negative','Allow Negative Customer Credits'=>'account_balance_allow_negative',
                        'customer_credit_allow_negative'=>'account_balance_allow_negative',
                    'Customer Credit Limit'=>'account_balance_limit',
                        'customer_credit_limit'=>'account_balance_limit',
                    'Invoice Email'=>'invoice_email', 'invoice_email'=>'invoice_email',// not used?
                    'Allow Loyalty'=>'use_loyalty', 'use_loyalty'=>'use_loyalty',// not used?
                    'Loyalty Points'=>'loyalty_points', 'Loyalty'=>'loyalty_points', 'loyalty_points'=>'loyalty_points',
                    'Require Food Minimum'=>'food_min', 'food_min'=>'food_min',// not used?
                    'Username'=>'username', 'username'=>'username',// not used?
                    'Password'=>'password', 'password'=>'password',// not used?
                    'Groups'=>'groups', 'groups'=>'groups',
                    'Giftcard Number'=>'giftcard_number', 'Gift Card Number'=>'giftcard_number', 'giftcard_number'=>'giftcard_number',
                    'Giftcard Value'=>'giftcard_value', 'Gift Card Value'=>'giftcard_value', 'Giftcard Balance'=>'giftcard_value', 'Gift Card Balance'=>'giftcard_value', 'giftcard_value'=>'giftcard_value'
                );

   				// first row is the set of raw keys
				$raw_keys = fgetcsv($handle);

                // Remap keys, preserving order
                $keys = array();
                foreach($raw_keys as $key){
                    if(!isset($key_namespace_remap[$key])) continue; // don't have it...
                    if($key_namespace_remap[$key]==='') continue; // don't use it yet...
                    $keys[] = $key_namespace_remap[$key];
                }

                /*
                * array_intersect gives us the proper subset of keys for each.
                * Importantly, it keeps the order of the first argument array.
                * array_combine will then let us automagically create a hash from the arrays
                */
                $person_default = array(
                    'first_name'=>'',
                    'last_name'=>'',
                    'email'=>'',
                    'phone_number'=>'',
                    'cell_phone_number'=>'',
                    'birthday'=>'',
                    'address_1'=>'',
                    'address_2'=>'',
                    'city'=>'',
                    'state'=>'',
                    'zip'=>'',
                    'country'=>'',
                    'comments'=>''
                );
                $customer_default = array(
                    'account_number'=>null,
                    'account_balance'=>0,
                    'account_balance_allow_negative'=>0,
                    'member_account_balance'=>0,
                    'member_account_balance_allow_negative'=>0,
                    'loyalty_points'=>0,
                    //Taxable has been relabeled non-taxable, so it is a little confusing here
                    'taxable'=>1,
                    'company_name' => '',
                    'course_id' => $this->session->userdata('course_id'),
                    'price_class'=>'price_category_1',
                    'member'=>0,
                    'opt_out_email'=>0,
                    //'groups'=>'',
                    //'giftcard_number'=>null,
                    //'giftcard_value'=>0,
                    //'customer_id'=>'',
                    'course_id' => $this->session->userdata('course_id'),
                    'invoice_email'=>''
                );

                // We are performing an import, so we need to call interface for audit stuff
                $act_type = 11; // mass upsert
                $emp_id =$this->session->userdata('emp_id');
                $comments = 'Excel import of multiple customers.';
                //TODO: Need a way to resolve which tables were actually updated
                $tbls = 'foreup_customers';
                $gmt = date('Y-m-d H:i:s');

                $log_entry = array(
                    'editor_id'=>$emp_id,
                    'gmt_logged'=>$gmt,
                    'action_type' => $act_type, // mass_upsert
                    'comments'=> $comments,
                    'tables_updated'=> $tbls
                );

                //TODO: Separate these actions into their own controller
                // insert into employee audit log, returning id
                $this->db->query("INSERT INTO foreup_employee_audit_log (editor_id,action_type_id,comments,gmt_logged) VALUES ('$emp_id',$act_type,'$comments','$gmt')");
                $eal_id = $this->db->query("SELECT LAST_INSERT_ID() as id")->result_array()[0]['id'];
                $log_entry['employee_audit_log_id']=$eal_id;

                // iterate over the data rows
				while (($data = fgetcsv($handle)) !== FALSE)
				{
                    $price_category = 'price_category_1'; // set later
					$groups = array(); // set later
					$passes = array();
                    $all_data = array_combine($keys,$data);

                    // ignore non-person data
                    $person_intermediate = array_intersect_key($all_data,$person_default);
                    // apply default values
                    $person_data = array_merge($person_default,$person_intermediate);
                    // BEGIN post processing
                    if($person_data['birthday']!=='') {
                        $person_data['birthday'] = date('Y-m-d',strtotime($person_data['birthday']));
                    }
                    // END post processing

                    /*
					$person_data = array(
					'first_name'=>isset($data[0])?trim($data[0]):'',
					'last_name'=>isset($data[1])?trim($data[1]):'',
					'email'=>isset($data[2])?$data[2]:'',
					'phone_number'=>isset($data[3])?$data[3]:'',
					'birthday'=>isset($data[4])?date('Y-m-d', strtotime($data[4])):'',
					'address_1'=>isset($data[5])?$data[5]:'',
					'address_2'=>isset($data[6])?$data[6]:'',
					'city'=>isset($data[7])?$data[7]:'',
					'state'=>isset($data[8])?$data[8]:'',
					'zip'=>isset($data[9])?$data[9]:'',
					'country'=>isset($data[10])?$data[10]:'',
					'comments'=>isset($data[11])?$data[11]:''
					);
                    */

                    // ignore non-person data
                    $customer_intermediate = array_intersect_key($all_data,$customer_default);
                    // apply default values
                    $customer_data = array_merge($customer_default,$customer_intermediate);
                    // BEGIN post processing
                    if($customer_data['account_number'] == '') {
                        $customer_data['account_number'] = null;
                    }
                    if($customer_data['account_balance'] !== $customer_default['account_balance']) {
                        $customer_data['account_balance'] = (float) str_replace(array('$',','), '', $customer_data['account_balance']);
                    }

                    $temp = $customer_data['account_balance_allow_negative'];
                    $customer_data['account_balance_allow_negative'] = ($temp === 'y' || $temp == 1)?1:0;


                    if($customer_data['member_account_balance'] !== $customer_default['member_account_balance']) {
                        $customer_data['member_account_balance'] = (float) str_replace(array('$',','), '', $customer_data['member_account_balance']);
                    }

                    $temp = $customer_data['member_account_balance_allow_negative'];
                    $customer_data['member_account_balance_allow_negative'] = $temp === 'y' || $temp == 1?1:0;



                    if($customer_data['loyalty_points'] !== $customer_default['loyalty_points']) {
                        $customer_data['loyalty_points'] = (int) str_replace(array('$',','), '', $customer_data['loyalty_points']);
                    }

                    $temp = $customer_data['taxable'];
                    $customer_data['taxable'] = ($temp == 'y' || $temp == 1)?0:1;

                    $price_category = ($customer_data['price_class'] != '')?$this->Customer->get_price_class_id($customer_data['price_class']):'';
                    if($price_category){
                        $customer_data['price_class'] = $price_category;
                    }

                    $temp = $customer_data['member'];
                    ($customer_data['member'] = $temp == 'y' || $temp == 1)?1:0;

                    $temp = $customer_data['opt_out_email'];
                    $customer_data['opt_out_email'] = ($temp == 'y' || $temp == 1)?1:0;

                    $groups = array();
                    if(isset($all_data['groups'])&&is_string($all_data['groups'])) {
                        $groups = explode(',', $all_data['groups']);
                    }
                    // END post processing

                    /*
					$customer_data=array(
					'account_number'=>(isset($data[12]) && $data[12]=='') ? null:$data[12],
					'account_balance'=>isset($data[13])?(float) str_replace(array('$',','), '', $data[13]):0,
					'account_balance_allow_negative'=>(isset($data[14]) && (strtolower($data[14]) != 'y' && $data[14] != 1))?0:1,
					'member_account_balance'=>isset($data[15])?(float) str_replace(array('$',','), '', $data[15]):0,
					'member_account_balance_allow_negative'=>(isset($data[16]) && (strtolower($data[16]) != 'y' && $data[16] != 1))?0:1,
					'loyalty_points'=>isset($data[25])?(int) str_replace(array('$',','), '', $data[25]):0,
					//Taxable has been relabeled non-taxable, so it is a little confusing here
					'taxable'=>(isset($data[17]) && (strtolower($data[17]) == 'y' || $data[17] == 1)) ? 0:1,
					'company_name' => isset($data[18])?$data[18]:'',
                    'course_id' => $this->session->userdata('course_id'),
					'price_class'=>($price_category)?$price_category:'price_category_1',
					'member'=>(isset($data[22]) && (strtolower($data[22]) != 'y' && $data[22] != 1))?0:1,
					'opt_out_email'=>(isset($data[24]) && (strtolower($data[24]) != 'y' && $data[24] != 1))?0:1
					);
                    */
					$giftcard_data = array();
					if (isset($all_data['giftcard_number']) && is_numeric($all_data['giftcard_number']))
						$giftcard_data = array(
							'giftcard_number'=>$data['giftcard_number'],
							'value'=>(float) str_replace(array('$',','), '', $all_data['giftcard_value']),
							'customer_id'=>'',
							'course_id' => $this->session->userdata('course_id')
						);

					if (isset($giftcard_data['giftcard_number']) && strpos($giftcard_data['giftcard_number'], 'E+'))
				    {
				   		echo json_encode(array('success'=>false, 'message'=>lang('giftcards_format_giftcard_numbers')));
						return;
				    }
				    else if (isset($customer_data['account_number']) &&strpos($customer_data['account_number'], 'E+'))
				    {
				   		echo json_encode(array('success'=>false, 'message'=>lang('customers_format_account_numbers')));
						return;
				    }
				    else if(!$this->Customer->save($person_data,$customer_data, false, $giftcard_data, $groups, $passes, true, $log_entry))
					{
						$failCodes[] = $person_data['first_name'].' '.$person_data['last_name'];
   					}
				}
            }
			else
			{
				echo json_encode( array('success'=>false,'message'=>lang('common_upload_file_not_supported_format')));
				return;
			}
            $this->db->trans_complete();
   		}

		$success = true;
		if(count($failCodes) > 0)
		{
			$msg = lang('customers_most_imported_some_failed')." (" .count($failCodes) ."): ".implode(", ", $failCodes);
			$success = false;
		}
		else
		{
			$msg = lang('customers_import_successfull');
		}

		echo json_encode( array('success'=>$success,'message'=>$msg) );
	}
	function cleanup()
	{
		$this->Customer->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('customers_cleanup_sucessful')));
	}
	/*
	Gets one row for a customer manage table. This is called using AJAX to update one row.
	*/
	function get_row($allow_deleted = false)
	{
		$person_id = $this->input->post('row_id');
		$this->show_deleted_customers = $this->input->post('show_deleted');
		$customer = $this->Customer->get_info($person_id, $this->session->userdata('course_id'), $allow_deleted);
		//get course name if customer belongs to a shared course
		$name = get_course_name($customer);
		if($name !== null) {
			$customer->course_name = $name;
		}

		$data_row=get_person_data_row($customer,$this);
		echo $data_row;
	}

	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 1100;
	}
	function generate_stats()
	{
		$results = $this->Dash_data->fetch_customer_data();
		echo json_encode($results);
	}
	function view_merge() {
		$this->load->view('customers/merge');
	}
	function merge(){

		$person_id_1 = $this->input->post('person_id_1');
		$person_id_2 = $this->input->post('person_id_2');

		$this->load->model("Customer");
		$person_1 = $this->Customer->get_info($person_id_1,$this->session->userdata('course_id'));
		$person_2 = $this->Customer->get_info($person_id_2,$this->session->userdata('course_id'));
		if($person_1->course_id != $person_2->course_id){
			$response = array('success'=>false, 'message'=>"Unable to merge customers from different courses.");
			echo json_encode($response);
			return;
		}

		// MERGE CUSTOMER DATA
		$this->load->model('Customer_merge');
		$message = '';
		if (!$this->Customer->validate_merge($message,$person_id_2)) {
			$response = array('success'=>false, 'message'=>$message);
			echo json_encode($response);
			return;
		}
		$this->db->trans_start();

		$this->Customer->merge($person_id_1,$person_id_2);

		
		
		
		$this->db->trans_complete();
	}
    function add_invoice_to_cart()
    {
        $this->load->model('v2/Cart_model');
        $this->load->model('v2/Invoice_model');

        $invoice_id = (int)$this->input->post('invoice_id');

        if (empty($invoice_id)) {
            echo json_encode(array('success' => false));
            return false;
        }

        $this->Cart_model->reset();
        $invoices = $this->Invoice_model->get(array('invoice_id' => $invoice_id));

        if (empty($invoices[0]['invoice_id'])) {
            echo json_encode(array('success' => false));
            return false;
        }
        $invoice = $invoices[0];

        $item = array(
            'total' => (float)$invoice['due'],
            'unit_price' => (float)$invoice['due'],
            'subtotal' => (float)$invoice['due'],
            'tax' => 0,
            'quantity' => 1,
            'item_type' => 'invoice',
            'name' => $invoice['name'],
            'params' => $invoice,
            'invoice_id' => (int)$invoice['invoice_id'],
            'selected' => true
        );

        $this->Cart_model->save_item(false, $item);
        $this->Cart_model->save_customer($invoice['customer']['person_id'], array('selected' => true));

        echo json_encode(array('success' => true));
    }

	public function send_text_invite()
	{
		$customer_id = $this->input->get('customer_id');
		$course_id = $this->input->get('course_id');

		$this->load->model("Marketing_Texting");
		$this->Marketing_Texting->reInviteUser($customer_id,$course_id);


		echo json_encode(array('success' => true));
	}

}
?>
