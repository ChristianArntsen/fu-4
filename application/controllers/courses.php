<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Courses extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('courses');
		$this->load->model('Course_message');
	}

	function index()
	{
            //if (!$this->permissions->is_super_admin())
              //  redirect('home', 'location');
		$config['base_url'] = site_url('courses/index');
		$config['total_rows'] = $this->Course->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);

		$data['controller_name']=strtolower(get_class());
		$data['stats'] = $this->Course->get_stats();
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_courses_manage_table($this->Course->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('courses/manage',$data);
	}

	function find_item_info()
	{
		$item_number=$this->input->post('scan_item_number');
		echo json_encode($this->Course->find_item_info($item_number));
	}

	function edit_terminal_processing($course_id) {
		$data = array();

		$this->load->model('Terminal');
		$data['terminals'] = $this->Terminal->get_all($course_id)->result_object();
		$data['course_id'] = $course_id;

		$this->load->view('courses/terminal_processing', $data);
	}

	function search($offset = 0)
	{
		$search=$this->input->post('search');
		$data_rows=get_courses_manage_table_data_rows($this->Course->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
		$config['base_url'] = site_url('courses/index');
        $config['total_rows'] = $this->Course->search($search, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);

	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Course->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	function course_search()
	{
            	$suggestions = $this->Course->get_course_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_category()
	{
		$suggestions = $this->Course->get_category_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_teesheet()
	{
		$suggestions = $this->Course->get_teesheet_search_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}

	function get_row()
	{
		$item_id = $this->input->post('row_id');
		$data_row=get_course_data_row($this->Course->get_info($item_id),$this);
		echo $data_row;
	}

	function get_info($item_id=-1)
	{
		echo json_encode($this->Course->get_info($item_id));
	}

	function view($item_id=-1)
	{

		$this->load->model('Course_information');
		$this->load->model('Course_information_setup');
		$this->load->model('Course_information_website');
		$data['item_info'] = $this->Course->get_info($item_id);
		$data['groups']=$this->Course->get_group_info('', $item_id);
		$data['managed_groups']=$this->Course->get_managed_group_info('', $item_id);
		if ($item_id != -1) {
			$data['course_information'] = $this->Course_information->get($item_id);
			$data['course_setup_information'] = $this->Course_information_setup->get($item_id);
			$data['course_website_information'] = $this->Course_information_website->get($item_id);
		}
		//print_r($data);
		$this->load->view("courses/form",$data);
	}

	function mobile_app($course_id=-1)
	{
		$this->load->model('Mobile_app_modules');
		$this->load->model('Mobile_app_module_permissions');

		$data['course_info'] = $this->Course->get_info($course_id);
		$mobile_app_module_permissions = $this->Mobile_app_module_permissions->get($course_id);
		$data['mobile_app_modules'] = $this->Mobile_app_modules->get_all();

		$data['mobile_app_module_permissions'] = array();
		foreach ($mobile_app_module_permissions as $mobile_app_module_permission) {
			$data['mobile_app_module_permissions'][] = $mobile_app_module_permission['mobile_app_module_id'];
		}

		$this->load->view("courses/mobile_app",$data);
	}

	function save_mobile_app_info($course_id) {
		$this->load->model('Mobile_app_module_permissions');

		$data = array(
			'mobile_app_active' => $this->input->post('mobile_app_active'),
			'base_color' => $this->input->post('base_color'),
			'mobile_app_icon_url' => $this->input->post('mobile_app_icon_url'),
			'mobile_test_flight_email' => $this->input->post('mobile_test_flight_email'),
			'mobile_app_short_title' => $this->input->post('mobile_app_short_title'),
			'course_summary' => $this->input->post('course_summary'),
			'mobile_app_summary' => $this->input->post('mobile_app_summary')
		);

		$this->Course->update($data, $course_id);

		// Delete all permissions
		$this->Mobile_app_module_permissions->delete_all($course_id);
		// Add permissions
		$mobile_app_module_permissions = $this->input->post('mobile_app_module_permissions');

		if (!empty($mobile_app_module_permissions)) {
			foreach ($mobile_app_module_permissions as $mobile_app_module_permission) {
				$permission_data = array(
					'course_id' => $course_id,
					'mobile_app_module_id' => $mobile_app_module_permission
				);

				$this->Mobile_app_module_permissions->save($permission_data);
			}
		}

		echo json_encode(array('success'=>true));
	}

	function view_tee_sheet_holes($course_id) {
		$this->load->model('Teesheet');
		$this->load->model('Teesheet_holes');
		$data = array();

		$data['course_info'] = $this->Course->get_info($course_id);
		$data['tee_sheets'] = $this->Teesheet->get_teesheets($course_id);
		$data['tee_sheet_list'] = array();
		foreach ($data['tee_sheets'] as $tee_sheet) {
			$data['tee_sheet_list'][$tee_sheet['teesheet_id']] = $tee_sheet['title'];
		}
		$data['tee_sheet_holes'] = array();
//		foreach($data['tee_sheets'] as $tee_sheet) {
//			$data['tee_sheet_holes'][$tee_sheet['teesheet_id']] = $this->Teesheet_holes->get_all($tee_sheet['teesheet_id']);
//		}

		$this->load->view('courses/tee_sheet_holes', $data);
	}

	function get_tee_sheet_holes($tee_sheet_id) {
		$this->load->model('Teesheet_holes');
		$tee_sheet_holes = $this->Teesheet_holes->get_all($tee_sheet_id);
		echo json_encode(array('success'=>true, 'tee_sheet_holes'=>$tee_sheet_holes));
	}

	function save_tee_sheet_holes($tee_sheet_id) {
		$this->load->model('Teesheet_holes');
		$post_vars = $this->input->post();

		// Delete all tee sheet holes
		$this->Teesheet_holes->delete_all($tee_sheet_id);

		// Save new tee sheet holes
		foreach ($post_vars['par'] as $index => $par) {
			$tee_sheet_hole = [
				'tee_sheet_id' => $tee_sheet_id,
				'hole_number' => $index+1,
				'par' => $par,
				'handicap' => $post_vars['handicap'][$index],
				'pro_tip' => $post_vars['pro_tip'][$index],
				'tee_box_long' => $post_vars['tee_box_long'][$index],
				'tee_box_lat' => $post_vars['tee_box_lat'][$index],
				'mid_point_long' => $post_vars['mid_point_long'][$index],
				'mid_point_lat' => $post_vars['mid_point_lat'][$index],
				'hole_long' => $post_vars['hole_long'][$index],
				'hole_lat' => $post_vars['hole_lat'][$index],
				'green_front_long' => $post_vars['green_front_long'][$index],
				'green_front_lat' => $post_vars['green_front_lat'][$index],
				'green_back_long' => $post_vars['green_back_long'][$index],
				'green_back_lat' => $post_vars['green_back_lat'][$index]
			];

			$this->Teesheet_holes->save($tee_sheet_hole);
		}
		echo json_encode(array('success'=>true));
	}

	function view_message($message_id=-1)
	{
        $data['message_info'] = $this->Course_message->get_info($message_id);
		//$data['groups']=$this->Course->get_group_info('', $item_id);
		//print_r($data);
		$this->load->view("courses/form_message",$data);
	}

	function new_push_notification()
	{
		$this->load->view("courses/form_push_notification");
	}

	function send_push_notification()
	{
		if (!$this->permissions->is_super_admin()) return;

		// New PushNotification object
		$push = new \fu\PushNotifications\PushNotifications();

		// TODO: Better validation and error handling
		if(!$this->input->post('push_message') || !$this->input->post('push_url')){
			echo json_encode(array('success'=>false, 'message'=>'Must include a message and a URL.'));
			return;
		}
		$push_message = $this->input->post('push_message');
		$push_url = $this->input->post('push_url');

		if ($this->input->post('push_all_courses')) {
			// Sending to all
		} else if ($this->input->post('push_course_id')) {
			// Sending to segment
			$push->setSegments(array('course_'.$this->input->post('push_course_id')));
		} else {
			// Sending to nobody...
			echo json_encode(array('success'=>false, 'message'=>'Must include at least one course if not sending to all.'));
			return;
		}

		// Send the notification
		if($push->sendMessage($push_message, $push_url) === true){
			echo json_encode(array('success'=>true, 'message'=>'Push notification sent successfully.'));
			return;
		} else {
			echo json_encode(array('success'=>false, 'message'=>'Push notification failed to send.'));
			return;
		}

		/*
		 * Example curl request:
		 *     curl -X POST -u "APIKEY:APISECRET" -H "Content-Type: application/json" --data '{"alert":"Testing CURL Call to Roost API", "url":"http://google.com", "segments" :["6270"]}' https://api.goroost.com/api/push
		*
		*/
	}

	function manage_groups()
	{
		$data['groups']=$this->Course->get_group_info();
		//$data['last_query']=$this->db->last_query();
		$this->load->view('courses/manage_groups',$data);
	}

	function add_group()
	{
		$group_label = $this->input->post("group_label");
		$group_type = $this->input->post("group_type");
		echo json_encode(array('success'=>true,'group_id'=>$this->Course->add_group($group_label, $group_type)));
	}

	function delete_group($group_id)
	{
		$this->Course->delete_group($group_id);
		echo json_encode(array('success'=>true));
	}

	//Ramel Inventory Tracking
	function inventory($item_id=-1)
	{
		$data['item_info']=$this->Course->get_info($item_id);
		$this->load->view("courses/inventory",$data);
	}

	function count_details($item_id=-1)
	{
		$data['item_info']=$this->Course->get_info($item_id);
		$this->load->view("courses/count_details",$data);
	} //------------------------------------------- Ramel

	function save_message($message_id)
	{
		$message_data = array(
			'message' 	=> $this->input->post('message'),
			'read' 		=> 0
		);
		
		if ($this->input->post('all_courses'))
		{
			$original_message = $message_data;
			$this->db->from('courses');
			$this->db->where('sales', 1);
			$courses = $this->db->get()->result_array();
			foreach ($courses as $course)
			{
				$message_data = $original_message;
				$message_data['course_id'] = $course['course_id'];
				$this->Course_message->save($message_data);
			}
			echo json_encode(array('success'=>true));
			return;
		}
		else if ($this->input->post('employee_id'))
		{
			$message_data['person_id'] = $this->input->post('employee_id');
			$message_data['course_id'] = $this->input->post('employee_course_id');
			$this->Course_message->save($message_data);
			echo json_encode(array('success'=>true));
			return;
		}
		else if ($this->input->post('course_id'))
		{
			$message_data['course_id'] = $this->input->post('course_id');
			$this->Course_message->save($message_data);
			echo json_encode(array('success'=>true));
			return;
		}
			echo json_encode(array('success'=>false));
	}

	function save($item_id=-1)
	{
		$item_data = array(
            'name'=>$this->input->post('name'),
            'simulator'=>$this->input->post('simulator'),
            'teesheets'=>$this->input->post('teesheets'),
            'reservations'=>$this->input->post('reservations'),
            'schedules'=>$this->input->post('schedules'),
            'sales'=>$this->input->post('sales'),
            'food_and_beverage'=>$this->input->post('food_and_beverage'),
            'sales_v2'=>$this->input->post('sales_v2'),
            'food_and_beverage_v2'=>$this->input->post('food_and_beverage_v2'),
            'items'=>$this->input->post('items'),
            'item_kits'=>$this->input->post('item_kits'),
            'employees'=>$this->input->post('employees'),
            'customers'=>$this->input->post('customers'),
            'invoices'=>$this->input->post('invoices'),
            'reports'=>$this->input->post('reports'),
            'suppliers'=>$this->input->post('suppliers'),
            'receivings'=>$this->input->post('receivings'),
            'config'=>$this->input->post('config'),
            'giftcards'=>$this->input->post('giftcards'),
            'passes'=>$this->input->post('passes'),
            'marketing_campaigns'=>$this->input->post('marketing_campaigns'),
            'tournaments'=>$this->input->post('tournaments'),
            'promotions'=>$this->input->post('promotions'),
            'quickbooks'=>$this->input->post('quickbooks'),
            'address'=>$this->input->post('address'),
            'city'=>$this->input->post('city'),
            'state'=>$this->input->post('state'),
            'zip'=>$this->input->post('zip'),
            'phone'=>$this->input->post('phone'),
            'active_course'=>$this->input->post('active_course'),
            'demo'=>$this->input->post('demo'),
            'clean_out_nightly'=>$this->input->post('clean_out_nightly'),
            //'fax'=>$this->input->post('fax'),
            'email'=>$this->input->post('email'),
            'website'=>$this->input->post('website'),
            'foreup_discount_percent'=>$this->input->post('foreup_discount_percent') == '' ? 25 : $this->input->post('foreup_discount_percent'),
            //'open_time'=>$this->input->post('open_time'),
            //'close_time'=>$this->input->post('close_time'),
            //'increment'=>$this->input->post('increment'),
            //'twilight_hour'=>$this->input->post('twilight_hour'),
            //'holes'=>$this->input->post('holes'),
            'mercury_id'=>$this->input->post('mercury_id'),
            'mercury_password'=>$this->input->post('mercury_password'),
            'use_mercury_emv'=>$this->input->post('use_mercury_emv'),
            'ets_key'=>$this->input->post('ets_key'),
            'seasonal_pricing'=>$this->input->post('seasonal_pricing'),
            'erange_id'=>$this->input->post('erange_id'),
            'erange_password'=>$this->input->post('erange_password'),
            'element_account_id' => $this->input->post('element_account_id'),
            'element_account_token' => $this->input->post('element_account_token'),
            'element_application_id' => $this->input->post('element_application_id'),
            'element_acceptor_id' => $this->input->post('element_acceptor_id'),
            'apriva_username' => $this->input->post('apriva_username'),
            'apriva_password' => $this->input->post('apriva_password'),
            'apriva_product' => $this->input->post('apriva_product'),
            'apriva_key' => $this->input->post('apriva_key'),
            'require_guest_count' => $this->input->post('require_guest_count'),
            'latitude_centroid' => $this->input->post('latitude_centroid'),
            'longitude_centroid' => $this->input->post('longitude_centroid')
		);
		$this->Fee->rename_green_fees($item_id, $this->input->post('simulator'));
		$groups_data = $this->input->post("groups")!=false ? $this->input->post("groups"):array();
		$managed_groups = $this->input->post("managed_groups")!=false ? $this->input->post("managed_groups"):array();

        if($this->Course->save($item_data,$item_id, $groups_data, $managed_groups))
		{
			//New item
			if($item_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_adding').' '.
				$item_data['name'],'course_id'=>$item_data['course_id']));
				$item_id = $item_data['course_id'];
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_updating').' '.
				$item_data['name'],'course_id'=>$item_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_error_adding_updating').' '.
			$item_data['name'],'course_id'=>-1));
		}


		$this->load->model('Course_information');
		$this->load->model('Course_information_setup');
		$this->load->model('Course_information_website');
		$postedInformation = $this->input->post("information");
		$courseSetupInformation = $this->input->post("course_setup_information");
		$courseWebsiteInformation = $this->input->post("course_website_information");
		$postedInformation['sales_rep'] = isset($postedInformation['sales_rep']) && $postedInformation['sales_rep'] !=''?$postedInformation['sales_rep']:null;
		$postedInformation['mobile_rep'] = isset($postedInformation['mobile_rep']) && $postedInformation['mobile_rep'] !=''?$postedInformation['mobile_rep']:null;
		$courseSetupInformation['setup_rep'] = isset($courseSetupInformation['setup_rep']) && $courseSetupInformation['setup_rep'] !=''?$courseSetupInformation['setup_rep']:null;
		$courseWebsiteInformation['website_rep'] = isset($courseWebsiteInformation['website_rep']) && $courseWebsiteInformation['website_rep'] !=''?$courseWebsiteInformation['website_rep']:null;
		$this->Course_information->save($item_id,$postedInformation);
		$this->Course_information_setup->save($item_id,$courseSetupInformation);
		$this->Course_information_website->save($item_id,$courseWebsiteInformation);

	}

	function save_terminal_processing() {
		$this->load->model('Terminal');
		$course_id = $this->input->post('course_id');
		$terminal_ids = $this->input->post('terminal_id');
		$mercury_ids = $this->input->post('mercury_id');
		$mercury_passwords = $this->input->post('mercury_password');
		$ets_keys = $this->input->post('ets_key');
		$element_account_ids = $this->input->post('element_account_id');
		$element_account_tokens = $this->input->post('element_account_token');
		$element_application_ids = $this->input->post('element_application_id');
        $element_acceptor_ids = $this->input->post('element_acceptor_id');
        $apriva_usernames = $this->input->post('apriva_username');
        $apriva_passwords = $this->input->post('apriva_password');
        $apriva_products = $this->input->post('apriva_product');
        $apriva_keys = $this->input->post('apriva_key');

        $this->db->trans_start();

		foreach ($terminal_ids as $index => $terminal_id) {
			$terminal_data = array(
				'mercury_id' => $mercury_ids[$index],
				'mercury_password' => $mercury_passwords[$index],
				'ets_key' => $ets_keys[$index],
				'element_account_id' => $element_account_ids[$index],
				'element_account_token' => $element_account_tokens[$index],
				'element_application_id' => $element_application_ids[$index],
				'element_acceptor_id' => $element_acceptor_ids[$index],
                'apriva_username' => $apriva_usernames[$index],
                'apriva_password' => $apriva_passwords[$index],
                'apriva_product' => $apriva_products[$index],
                'apriva_key' => $apriva_keys[$index]
			);
			$this->db->where('course_id', $course_id);
			$this->db->where('terminal_id', $terminal_id);
			$this->db->update('terminals', $terminal_data);
		}

		$status = $this->db->trans_status();
		$this->db->trans_complete();

		echo json_encode(array('success'=>$status));
	}


	function delete()
	{
		$items_to_delete=$this->input->post('ids');

		if($this->Course->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('items_successful_deleted').' '.
			count($items_to_delete).' '.lang('items_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_cannot_be_deleted')));
		}
	}

	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 550;
	}
}
?>
