<?php
require_once ("secure_area.php");
class meal_courses extends Secure_area {
	
    function __construct(){
        parent::__construct();
		$this->load->model('v2/Meal_course_model');
    }

    function index(){
		$data['meal_courses'] = $this->Meal_course_model->get();
		$this->load->view('meal_courses/manage', $data);
    }
    
	function delete($meal_course_id = null){
		$success = $this->Meal_course_model->delete($meal_course_id);
		echo json_encode(array('success' => $success));
	}

	function save($meal_course_id = null){
		
		$data = $this->input->post();
		$meal_course_id = $this->Meal_course_model->save($meal_course_id, $data);
		
		if(!empty($meal_course_id)){
			echo json_encode(array('success' => true, 'meal_course_id' => $meal_course_id));
		}else{
			echo json_encode(array('success' => false));
		}
	}
}
?>
