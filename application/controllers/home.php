<?php
require_once("secure_area.php");

class Home extends Secure_area
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Course_message');
		$this->load->model('Article');
	}

	function index($ajax = 0)
	{
		$data = array();
		$data['article_1'] = $this->Article->get_newest_article(1);
		$data['article_2'] = $this->Article->get_newest_article(2);
		$data['article_3'] = $this->Article->get_newest_article(3);
		$data['ajax'] = $ajax;

		$email = "";
		$this->load->model("employee");
		$info = $this->employee->get_info($this->session->userdata('person_id'));
		$this->load->model("course");
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		if(empty($info->email)){
			if(!empty($course_info->email)){
				$email = $course_info->email;
			}
		} else {
			$email = $info->email;
		}
		$info->email  = null;
		$this->session->set_userdata("email",$email);

		$data['user'] = [
			"user_level"=>$this->session->userdata('user_level'),
			"name"=>$this->session->userdata('first_name'). " ".$this->session->userdata('last_name'),
			"user_id"=>$this->session->userdata('person_id'),
			"course_id"=>$this->session->userdata('course_id'),
			"course_name"=>$this->session->userdata('course_name'),
			"email"=>$this->session->userdata('email'),
			'course_info'=>$course_info
		];
		$this->load->view("home", $data);
	}

	function activate_social()
	{
		$this->db->where("course_id",$this->session->userdata('course_id'));
		$this->db->update("courses",[
			"social_management"=>1
		]);
	}

	function logout($bypass = false)
	{
		$this->load->model('Sale');
		$cash_register = $this->Sale->getUnfinishedRegisterLog()->row();

		if ($this->session->userdata('unfinished_register_log') && is_object($cash_register) && $bypass != 'bypass')// && !$this->session->userdata('logout_immediately'))
		{
			//echo 'unfinished_register_log '.site_url('sales/closeregister?continue=logout');
			//return;
			$this->session->set_userdata('logout_immediately', true);
			$person_id = $this->session->userdata('person_id');
			$has_sales_permission = false;
			if ($person_id) {
				$allowed_modules = $this->Module->get_allowed_modules($employee_info->person_id);
				foreach ($allowed_modules->result_array() as $allowed_module) {
					$has_sales_permission = ($has_sales_permission ? $has_sales_permission : $allowed_module['module_id'] == 'sales');
				}
			}
			if ($this->session->userdata('mobile')) {
				echo json_encode(array('message' => 'cash register log active, closing amount needed', 'status' => 'hold', 'required' => array('closing_amount')));
			} else if ($has_sales_permission)//Has to have sales permission
				redirect(site_url('sales/closeregister?continue=logout'));
			else {
				$this->Employee->logout();
			}
		} else {
			$this->Employee->logout();
		}
	}

	function switch_user()
	{
		$this->session->unset_userdata('fnb_logged_in');
		//$this->session->set_userdata('person_id', '-1');
		$this->session->unset_userdata('user_level');
		$this->session->unset_userdata('auto_mailers');
		$this->session->unset_userdata('config');
		$this->session->unset_userdata('courses');
		$this->session->unset_userdata('customers');
		$this->session->unset_userdata('dashboard');
		$this->session->unset_userdata('employees');
		$this->session->unset_userdata('events');
		$this->session->unset_userdata('giftcards');
		$this->session->unset_userdata('item_kits');
		$this->session->unset_userdata('items');
		$this->session->unset_userdata('invoices');
		$this->session->unset_userdata('marketing_campaigns');
		$this->session->unset_userdata('promotions');
		$this->session->unset_userdata('receivings');
		$this->session->unset_userdata('reports');
		$this->session->unset_userdata('reservations');
		$this->session->unset_userdata('sales');
		$this->session->unset_userdata('schedules');
		$this->session->unset_userdata('suppliers');
		$this->session->unset_userdata('teesheets');
		$this->session->unset_userdata('tournaments');

		$this->session->unset_userdata('table_id');
		$this->session->unset_userdata('table_number');
		$this->session->unset_userdata('suspended_sale_id');

		// Force the session to update database immediately
		$this->session->sess_write();

		//echo json_encode(array('success' => true));
		$this->load->view('login/login');
	}

	function login()
	{
		$pin_or_card = $this->input->post('pin_or_card');
		$employee_info = $this->Employee->get_info_by_pin_or_card($pin_or_card);
		if ($employee_info->person_id != '') {
			$has_food_and_beverage_permission = false;
			$allowed_modules = $this->Module->get_allowed_modules($employee_info->person_id);
			foreach ($allowed_modules->result_array() as $allowed_module) {
				$has_food_and_beverage_permission = ($has_food_and_beverage_permission ? $has_food_and_beverage_permission : $allowed_module['module_id'] == 'food_and_beverage');
			}

			// If user doesn't have F&B permission, return error
			if (!$has_food_and_beverage_permission) {
				echo json_encode(array('success' => false, 'message' => lang('food_and_beverage_no_permission')));
				return;
			}

			$logged_in = $this->Employee->login($employee_info->username, $employee_info->password, true);

			if ($logged_in) {
				$this->session->set_userdata('fnb_logged_in', true);

				// Force the session to update database immediately
				// instead of waiting for page refresh as usual
				$this->session->sess_write();

				$register_log = $this->Sale->getUnfinishedRegisterLog();

				echo json_encode(
					array(
						'employee' => $employee_info,
						'recent_transactions' => $this->Table->get_recent_transactions($employee_info->person_id, 500),
						'success' => true,
						'register_log' => $register_log->row_array()
					)
				);

			} else {
				echo json_encode(array('success' => false, 'message' => lang('food_and_beverage_error_logging_in')));
			}
			return;
		}

		echo json_encode(array('success' => false, 'message' => lang('food_and_beverage_invalid_pin_or_card')));
	}

	function terminal_window()
	{
		$this->load->model('Terminal');
		$this->load->model('v2/Register_log_model');
		$this->load->model('v2/Employee_model');
		$data['terminals'] = $this->Terminal->get_all()->result_object();

		foreach($data['terminals'] as &$terminal){

			$terminal->active_log = false;
			$terminal->active_log_employee = false;

			if($terminal->multi_cash_drawers == 0){
				continue;
			}
			$terminal->active_log = $this->Register_log_model->get_active($terminal->terminal_id, false);

			if($terminal->active_log){
				$employees = $this->Employee_model->get(['person_id' => $terminal->active_log['employee_id']]);
				$terminal->active_log_employee = $employees[0];
			}	
		}

		$this->load->view('terminals/select', $data);
	}

	function logout_options()
	{
		// DETERMINE IF THERE IS A OPEN REGISTER LOG
		$open_log = $this->Sale->getUnfinishedRegisterLog()->row();
		$data = array();
		echo json_encode(array('open_log' => is_object($open_log), 'html' => $this->load->view('login/logout', $data, true)));
	}

	function get_course_messages()
	{
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

		function sendMsg($id, $msg)
		{
			echo "id: $id" . PHP_EOL;
			echo "retry: 7200000" . PHP_EOL;
			echo "data: $msg" . PHP_EOL;
			echo PHP_EOL;
			ob_flush();
			flush();
		}

		$messages = $this->Course_message->get_all();
		sendMsg(time(), json_encode($messages));
	}

	function view_print_queue()
	{
		$this->load->view('help/print_queue');
	}

	function minify_files()
	{
		$this->load->driver('minify');
		// PRIMARY JAVASCRIPT FILE
		$files = array(
			'js/jquery-1.3.2.min.js',
			'js/jquery-ui.min.js',
			'js/jquery.color.js',
			'js/jquery.form.js',
			'js/jquery.tablesorter.min.js',
			'js/jquery.validate.min.js',
			'js/jquery.maskedinput.js',
			//'js/jquery.mask.min.js', 
			//'js/jquery.contactable.min.js', 
			//'js/jquery.qtip.min2.js', 
			//'js/jquery.customSelect.js', 
			'js/thickbox.js',
			'js/jquery.colorbox.js',
			'js/jquery.colorbox2.js',
			'js/ui.expandable.js',
			'js/common.js',
			'js/manage_tables.js',
			'js/date.js',
			'js/datepicker.js',
			'js/hoverIntent.js',
			'js/superfish.js',
			'js/jquery.loadmask.min.js',
			'js/jHtmlArea-0.7.0.js',
			'js/jquery.wysiwyg.js',
			'js/jquery.checkbox.js',
			'js/jquery.contextMenu.js',
			'js/jquery.dd.js',
			'js/jquery.timepicker.js',
			'js/jquery.sth.js'
		);
		$contents = $this->minify->combine_files($files);
		$contents = $this->minify->js->min($contents);
		var_dump($this->minify->save_file($contents, 'js/all_js.js'));

		// TEE SHEET JAVASCRIPT FILE
		$files = array(
			'js/jquery-1.5.2.min.js',
			'js/jquery-ui.min.js',
			'js/jquery.color.js',
			'js/jquery.form.js',
			'js/jquery.tablesorter.min.js',
			'js/jquery.validate.min.js',
			'js/jquery.maskedinput.js',
			//'js/jquery.mask.min.js', 
			//'js/jquery.contactable.min.js', 
			//'js/jquery.qtip.min2.js', 
			//'js/jquery.customSelect.js', 
			'js/thickbox.js',
			'js/jquery.colorbox.js',
			'js/jquery.colorbox2.js',
			'js/ui.expandable.js',
			'js/common.js',
			'js/manage_tables.js',
			'js/date.js',
			'js/datepicker.js',
			'js/hoverIntent.js',
			'js/superfish.js',
			'js/jquery.loadmask.min.js',
			'js/jHtmlArea-0.7.0.js',
			'js/jquery.wysiwyg.js',
			'js/jquery.checkbox.js',
			'js/jquery.contextMenu.js',
			'js/jquery.dd.js',
			'js/jquery.timepicker.js',
			'js/jquery.sth.js'
		);
		$contents = $this->minify->combine_files($files);
		$contents = $this->minify->js->min($contents);
		echo 'ats_js ' . $this->minify->save_file($contents, 'js/all_tee_sheet_js.js');

		// TEE SHEET 2 JAVASCRIPT FILE
		$files = array(
			'js/jquery.xml2json.pack.js',
			'js/jquery.syncscroll.js',
			//'js/javascript.js',
			//'js/fullcalendar.js'
		);
		$contents = $this->minify->combine_files($files);
		$contents = $this->minify->js->min($contents);
		echo '<br/>ats2_js ' . $this->minify->save_file($contents, 'js/all_tee_sheet_2_js.js');

		// RESERVATIONS 2 JAVASCRIPT FILE
		$files = array(
			'js/jquery.xml2json.pack.js',
			'js/jquery.syncscroll.js',
			'js/javascript_res.js',
			'js/fullcalendar.js'
		);
		$contents = $this->minify->combine_files($files);
		$contents = $this->minify->js->min($contents);
		echo '<br/>ars2_js ' . $this->minify->save_file($contents, 'js/all_reservations_2_js.js');

		// PRIMARY CSS FILE
		$files = array(
			'css/phppos.css',
			'css/menubar.css',
			'css/general.css',
			'css/popupbox.css',
			'css/register.css',
			'css/receipt.css',
			'css/reports.css',
			'css/tables.css',
			'css/thickbox.css',
			'css/colorbox.css',
			'css/colorbox2.css',
			'css/datepicker.css',
			'css/editsale.css',
			'css/footer.css',
			'css/contactable.css',
			'css/css3.css',
			'css/ui-lightness/jquery-ui-1.8.14.custom.css',
			'css/jquery.loadmask.css',
			'css/redmond.css',
			'css/superfish.css',
			'css/superfish-vertical.css',
			'css/jquery.qtip.min2.css',
			'css/jHtmlArea.css',
			'css/jquery.wysiwyg.css',
			'css/jquery.checkbox.css',
			'css/jquery.sth.css',
			'css/new_general.css',
			'css/dd.css',
			'css/flags.css',
			'css/skin2.css',
			'css/sprite.css',
			'css/style.css'
		);
		//$contents = $this->minify->combine_files($files);
		//$contents = $this->minify->css->min($contents);
		//echo '<br/>ac_css '.$this->minify->save_file($contents, 'css/all_css.css');

		// TEESHEET CSS FILE
		$files = array(
			'css/phppos.css',
			'css/menubar.css',
			'css/general.css',
			'css/popupbox.css',
			'css/register.css',
			'css/receipt.css',
			'css/reports.css',
			'css/tables.css',
			'css/thickbox.css',
			'css/colorbox.css',
			'css/colorbox2.css',
			'css/datepicker.css',
			'css/editsale.css',
			'css/footer.css',
			'css/contactable.css',
			'css/css3.css',
			'css/ui-lightness/jquery-ui-1.8.14.custom.css',
			'css/jquery.loadmask.css',
			'css/redmond.css',
			'css/superfish.css',
			'css/superfish-vertical.css',
			'css/jquery.qtip.min2.css',
			'css/jHtmlArea.css',
			'css/jquery.wysiwyg.css',
			'css/jquery.checkbox.css',
			'css/jquery.sth.css',
			'css/new_general.css',
			'css/dd.css',
			'css/flags.css',
			'css/skin2.css',
			'css/sprite.css',
			'css/style.css',
			'css/fullcalendar.css',
			'css/styles.css'
		);
		//$contents = $this->minify->combine_files($files);
		//$contents = $this->minify->css->min($contents);
		//echo '<br/>atsc_css '.$this->minify->save_file($contents, 'css/all_tee_sheet_css.css');
	}

	function api_unit_test()
	{
		if (!$this->permissions->is_super_admin())
			return;
		$this->load->library('unit_test');
		$api_key = '95c9cf1a-5815-43e5-8787-f67d89c7109b';
		$extended_api_key = 'c57cc45b-34c7-4b7a-8a6c-fda8fa38b9c0';
		$base_url = 'https://api.foreupsoftware.com/index.php/api/';
		$guest_id = 0;

		$response = file_get_contents($base_url . 'golf_course/areas?api_key=' . $api_key);
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'golf_course/areas status');
		$this->unit->run(count($obj->areas) > 0, true, 'golf_course/areas returning areas');


		$response = file_get_contents($base_url . 'golf_course/list?api_key=' . $api_key . '&area_id=3');
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'golf_course/list status');
		$this->unit->run(count($obj->golf_courses) > 0, true, 'golf_course/list returning area list');

		$response = file_get_contents($base_url . 'golf_course/info?api_key=' . $api_key . '&golf_course_id=7566');
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'golf_course/info status');
		$this->unit->run(count($obj->golf_course_info) > 0, true, 'golf_course/info returning course info');

		$response = file_get_contents($base_url . 'golf_course/links?api_key=' . $api_key . '&golf_course_id=7566');
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'golf_course/links status');
		$this->unit->run(count($obj->golf_course_links) > 0, true, 'golf_course/links returning course links');

		$response = file_get_contents($base_url . 'guest/create?api_key=' . $api_key . '&golf_course_id=7566&first_name=John&last_name=Doe&email=jonnydoe@foreup.com' . '&phone_number=555-555-5555&zip=84604');
		$obj = json_decode($response);
		$this->unit->run($obj->status, '201', 'guest/create status');
		$this->unit->run(is_numeric($obj->guest_id), true, 'guest/create returning guest_create');
		$guest_id = $obj->guest_id;

		$response = file_get_contents($base_url . 'guest/info?api_key=' . $api_key . '&golf_course_id=7566&guest_id=' . $guest_id);
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'guest/info status');
		$this->unit->run(count($obj->guest_info) > 0, true, 'guest/info returning guest_info');

		$response = file_get_contents($base_url . 'guest/list?api_key=' . $api_key . '&golf_course_id=7566');
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'guest/list status');
		$this->unit->run(count($obj->guests) > 0, true, 'guest/list returning guest_list');

		$response = file_get_contents($base_url . 'guest/update?api_key=' . $api_key . '&golf_course_id=7566&guest_id=' . $guest_id);
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'guest/update status');
		$this->unit->run(is_numeric($obj->guest_id) > 0, true, 'guest/update returning guest_update');

		$response = file_get_contents($base_url . 'guest/reservations?api_key=' . $api_key . '&golf_course_id=7566&schedule_id=52&guest_id=' . $guest_id);
		$obj = json_decode($response);
		$this->unit->run($obj->status !== '200', true, 'guest/reservations status');
		$this->unit->run(count($obj->reservations) == 0, true, 'guest/reservations returning guest_reservations');

		$response = file_get_contents($base_url . 'guest/search?api_key=' . $api_key . "&golf_course_id=7566&guest_id=$guest_id&type=phone&value=555-555-5555");
		$obj = json_decode($response);
		$this->unit->run($obj->status !== '200', true, 'guest/search status');
		$this->unit->run(count($obj->guests) == 0, true, 'guest/search returning guest_search');

		//This test could use some more in-depth testing if there is a problem
		$response = file_get_contents($base_url . 'reservation/available_times?api_key=' . $api_key . '&schedule_id=52&date=2016-01-01&time_range=morning');
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'reservation/available_times status');
		$this->unit->run(count($obj->available_reservation_times) == 0, true, 'reservation/available_times returning available_times');

		$response = file_get_contents($base_url . 'reservation/is_available?api_key=' . $api_key . '&schedule_id=52&golf_course_id=7566&date=2013-10-01T=9:10&guests=1');
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'reservation/is_available status');
		$this->unit->run(($obj->reservation_is_available), true, 'reservation/is_available returning is_available');

		$response = file_get_contents($base_url . 'reservation/book?api_key=' . $api_key . "&schedule_id=52&time=2013-10-10T1:00&guest_id=$guest_id&guests=1&holes=9");
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'reservation/book status');
		$this->unit->run(($obj->booked_reservation), true, 'reservation/book returning book');

		$booked_id = $obj->reservation_id;

		$response = file_get_contents($base_url . 'reservation/info?api_key=' . $api_key . "&reservation_id=$booked_id&schedule_id=52");
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'reservation/info status');
		$this->unit->run(count($obj->reservation_info) > 0, true, 'reservation/info returning id');

		$response = file_get_contents($base_url . 'reservation/list?api_key=' . $api_key . "&schedule_id=52");
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'reservation/list status');
		$this->unit->run(count($obj->booked_reservations) > 0, true, 'reservation/list returning list'); //WHY is it false?


		$response = file_get_contents($base_url . 'reservation/update?api_key=' . $api_key . "&reservation_id=$booked_id&schedule_id=52");
		$obj = json_decode($response);
		$this->unit->run($obj->status !== '200', true, 'reservation/update status');
		$this->unit->run(strlen($obj->reservation_id) > 0, false, 'reservation/update returning id');

		$response = file_get_contents($base_url . 'guest/reservations?api_key=' . $extended_api_key . "&guest_id=$guest_id&schedule_id=52");
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'guest/reservations extended_permission status');
		$this->unit->run(count($obj->reservations) > 0, true, 'guest/reservation returning id');


		$response = file_get_contents($base_url . 'guest/search?api_key=' . $extended_api_key . "&golf_course_id=7566&type=phone&value=555-555-5555");
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'guest/reservations extended_permission status'); // RETURNS NO STATUS CODE
		$this->unit->run(count($obj->guests) > 0, true, 'guest_search returning id');

		$response = file_get_contents($base_url . 'reservation/update?api_key=' . $extended_api_key . "&reservation_id=$booked_id&schedule_id=52");
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'reservation/update status');
		$this->unit->run(strlen($obj->reservation_id) > 0, true, 'reservation/update returning id');

		$response = file_get_contents($base_url . 'reservation/cancel?api_key=' . $api_key . "&reservation_id=$booked_id");
		$obj = json_decode($response);
		$this->unit->run($obj->status, '200', 'reservation/cancel status');
		$this->unit->run($obj->success, true, 'reservation/cancel returning cancel'); // Why is this false?
		$this->unit->run($obj->reservation_cancelled, true, 'reservation/cancel returning cancel');


		//$this->unit->run($obj->status, '200', 'golf_course/list status');
		//$this->unit->run(count($obj->golf_courses) > 0, true, 'golf_course/list returning area list');

		echo $this->unit->report();
	}

	function unit_test_pos_api()
	{
		if (!$this->permissions->is_super_admin())
			return;
		$this->load->library('unit_test');
		$base_url = 'https://mobile.foreupsoftware.com/index.php/';
		$ch2 = curl_init('https://mobile.foreupsoftware.com/index.php/login?mobile=1');

		curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch2, CURLOPT_POST, 1);
		curl_setopt($ch2, CURLOPT_POSTFIELDS, "username=demo2&password=password86");
		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);

		$data2 = (curl_exec($ch2));

		$data2 = json_decode($data2);
		$session_data = $data2->session;

		$ch = curl_init($base_url . 'sales');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data&opening_amount=100.00");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = json_decode(curl_exec($ch));
		$this->unit->run($response->status !== 'hold', true, 'test setup');

		$ch = curl_init($base_url . 'sales/add/27418');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = curl_exec($ch);

		$response = json_decode(substr($response, 3));
		$this->unit->run(is_numeric($response->register_box_info->cart->{'1'}->item_id), true, 'ADD item');

		$ch = curl_init($base_url . 'sales/delete_item/0');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$delete_data = (curl_exec($ch));
		$delete_data = substr($delete_data, 3);  //The delete_item api needs to be fixed

		$data = json_decode($delete_data);
		$this->unit->run(is_numeric($data->register_box_info->basket->{'1'}->item_id), true, 'sales/delete successful');

		$ch = curl_init($base_url . 'sales/add/27418');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = curl_exec($ch);

		$ch = curl_init($base_url . 'sales/get_recent_transactions/5');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = json_decode(substr(curl_exec($ch), 3));

		$this->unit->run(is_numeric($response[0]->sale_id), true, 'sales/get recent transactions successful');


		$ch = curl_init($base_url . 'sales/add_payment');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data&amount_tendered=160.06&payment_type=cash");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = json_decode(substr(curl_exec($ch), 3));
		//log_message('error', 'add payment response: ' . var_dump($response));
		$this->unit->run($response->payments_cover_total, true, 'sales/payment made in full is successful');

		$ch = curl_init($base_url . 'sales/delete_payment');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data&payment_id=cash");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = json_decode(substr(curl_exec($ch), 3));
		$this->unit->run($response->amount_due === '160.06', true, 'sales/delete payment is successful');


		$ch = curl_init($base_url . 'sales/add_payment');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data&amount_tendered=160.06&payment_type=cash");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = json_decode(substr(curl_exec($ch), 3));
		//log_message('error', 'add payment response: ' . var_dump($response));
		$this->unit->run($response->payments_cover_total, true, 'sales/payment made in full is successful');

		$ch = curl_init($base_url . 'sales/suspend/6');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = curl_exec($ch);
		//log_message('error', "SUSPEND: $response");

		$ch = curl_init($base_url . 'sales/cancel_sale');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = json_decode(substr(curl_exec($ch), 3));

		$this->unit->run(count($response->cart) == 0, true, 'Cancel Sale Successful');

		$ch = curl_init($base_url . 'sales/add/27418');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = curl_exec($ch);

		$response = json_decode(substr($response, 3));
		$this->unit->run(is_numeric($response->register_box_info->cart->{'1'}->item_id), true, 'ADD item');

		$ch = curl_init($base_url . 'sales/add_payment');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data&amount_tendered=160.06&payment_type=Cash");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = json_decode(substr(curl_exec($ch), 3));
		//log_message('error', 'add payment response: ' . var_dump($response));
		$this->unit->run($response->payments_cover_total, true, 'sales/payment made in full is successful');

		$ch = curl_init($base_url . 'sales/complete');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = json_decode(substr(curl_exec($ch), 3));
		$sale_id = $response->receipt_data->sale_id;
		$sale_id = str_replace('POS ', '', $sale_id);
		$this->unit->run(strlen($sale_id) > 0, true, 'Sale Completetion Successful');
		//$this->unit->run(count($response->cart) == 0, true, 'Sale Successful');


		$ch = curl_init($base_url . "sales/email_receipt/$sale_id/" . urlencode('junkmail188@gmail.com'));//signorehopkins@gmail.com'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = json_decode(substr(curl_exec($ch), 3));

		$this->unit->run($response->success, true, 'EMAIL SENT SUCCESSFULLY');


		$ch = curl_init($base_url . 'sales/add/27418');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = curl_exec($ch);

		$response = json_decode(substr($response, 3));
		$this->unit->run(is_numeric($response->register_box_info->cart->{'1'}->item_id), true, 'ADD item');

		$ch = curl_init($base_url . "sales/change_mode/sale");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = json_decode(substr(curl_exec($ch), 3));

		$this->unit->run($response->mode === 'return', true, 'SALE SUCCESSFULLY SWITCHED TO RETURN MODE');


		$ch = curl_init($base_url . "sales/change_mode/return");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = json_decode(substr(curl_exec($ch), 3));

		$this->unit->run($response->mode === 'sale', true, 'RETURN MODE SUCCESSFULLY SWITCHED TO SALE MODE');


		$ch = curl_init($base_url . "sales/update_cart");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data&line=0&checked=true&quantity=2");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = json_decode(substr(curl_exec($ch), 3));

		$this->unit->run(strlen($response->basket_info->subtotal) > 0, true, 'CART SUCCESSFULLY UPDATED');


		$ch = curl_init($base_url . "sales/item_search?term=all");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = json_decode(substr(curl_exec($ch), 3));

		$this->unit->run(strlen($response[0]->value) > 0, true, 'ITEM FOUND!');

		$ch = curl_init($base_url . "sales/closeregister");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$session_data&closing_amount=180.03");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = (substr(curl_exec($ch), 3));
		log_message('error', "CLOSE REGISTER: $response");
		//$this->unit->run($response->success, true, 'REGISTER CLOSED!');


		$ch = curl_init($base_url . "login");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "mobile=1&username=demo2&password=password86");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = json_decode(curl_exec($ch));
		$terminal_ids = $response->terminals;
		$second_session = $response->session;
		$this->unit->run(count($terminal_ids) > 0, true, "LOGIN SUCCESSFUL");


		$ch = curl_init($base_url . "home/set_terminal/" . $terminal_ids[0]);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "session=$second_session");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


		$response = json_decode(curl_exec($ch));
		//var_dump($response);
		$this->unit->run($response->success, true, 'Terminal Successfully Changed');

		echo $this->unit->report();

		//echo $response;
	}

	function mark_message_as_read($message_id)
	{
		$this->Course_message->mark_as_read($message_id);
	}

	function set_terminal($terminal_id)
	{
		$this->load->model('Terminal');
		$this->session->set_userdata('terminal_id', $terminal_id);
		if ($terminal_id != 0) {
			$terminal_info = $this->Terminal->get_info($terminal_id);
			$this->session->set_userdata('quickbutton_tab', $terminal_info[0]['quickbutton_tab']);
			$this->session->set_userdata('all_persistent_logs', $terminal_info['0']['persistent_logs']);
			$this->session->set_userdata('terminal_https',$terminal_info['0']['https']);
			echo json_encode(array('success' => true, 'terminal' => $terminal_info[0]));
		} else {
			echo json_encode(array('success' => true, 'terminal' => array('terminal_id' => 0, 'label' => 'Other')));
		}
	}

	function view_giftcard($giftcard_id = -1, $cart_line = 0, $view_only = false)
	{
		$this->load->library('Sale_lib');
		$data = array();
		$data['customers'] = array('' => 'No Customer');
		/*foreach ($this->Customer->get_all()->result() as $customer)
		{
			$data['customers'][$customer->person_id] = $customer->first_name . ' '. $customer->last_name;
		}*/
		$data['view_only'] = $view_only;
		$data['cart_line'] = $cart_line;
		$data['giftcard_info'] = $this->Giftcard->get_info($giftcard_id);
		if ($cart_line) {
			$giftcard_data = $this->sale_lib->get_giftcard_details($cart_line);
			$data['giftcard_info']->customer_id = $giftcard_data['giftcard_data']['customer_id'];
			$data['giftcard_info']->giftcard_number = $giftcard_data['giftcard_data']['giftcard_number'];
			$data['giftcard_info']->customer_name = $giftcard_data['giftcard_data']['customer_name'];
			$data['giftcard_info']->value = $giftcard_data['value'];
			$data['giftcard_info']->details = $giftcard_data['giftcard_data']['details'];
			$data['giftcard_info']->expiration_date = $giftcard_data['giftcard_data']['expiration_date'];
		}
		$customer_info = $this->Customer->get_info($data['giftcard_info']->customer_id);

		if ($customer_info->last_name . $customer_info->first_name != '') {
			$data['customer'] = $customer_info->last_name . ', ' . $customer_info->first_name;
		} else {
			$data['customer'] = 'No Customer';
		}
		$this->load->view("giftcards/form", $data);
	}

	function view_punch_card($punch_card_id = -1, $cart_line = 0, $view_only = false)
	{
		$this->load->library('Sale_lib');
		$data = array();
		$data['customers'] = array('' => 'No Customer');

		$data['view_only'] = $view_only;
		$data['cart_line'] = $cart_line;
		$data['punch_card_info'] = $this->Punch_card->get_info($punch_card_id);
		$items = $this->Punch_card->get_items($punch_card_id, false);

		if ($cart_line) {
			$punch_card_data = $this->sale_lib->get_punch_card_details($cart_line);
			$data['punch_card_info']->customer_id = $punch_card_data['punch_card_data']['customer_id'];
			$data['punch_card_info']->punch_card_number = $punch_card_data['punch_card_data']['punch_card_number'];
			$data['punch_card_info']->customer_name = $punch_card_data['punch_card_data']['customer_name'];
			$data['punch_card_info']->value = $this->Punch_card->get_punch_card_value($punch_card_id, false);
			$data['punch_card_info']->details = $punch_card_data['punch_card_data']['details'];
			$data['punch_card_info']->expiration_date = $punch_card_data['punch_card_data']['expiration_date'];
		} else {
			$data['punch_card_info']->value = $items;
		}
		$customer_info = $this->Customer->get_info($data['punch_card_info']->customer_id);

		if ($customer_info->last_name . $customer_info->first_name != '') {
			$data['customer'] = $customer_info->last_name . ', ' . $customer_info->first_name;
		} else {
			$data['customer'] = 'No Customer';
		}

		$this->load->view("punch_cards/form", $data);
	}

	function view_ets_giftcard()
	{
		$ets_response = json_decode($this->input->post('response'), true);

		$data['date_created'] = $ets_response['created'];
		$data['amount'] = '$' . $ets_response['giftCards']['amount'];

		$this->load->view("giftcards/ets_balance", $data);
	}

	function view_giftcard_lookup()
	{
		$data = array();

		// If course is using ETS giftcards
		if ($this->config->item('use_ets_giftcards') == 1) {
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));

			// Open ETS session
			$session = $payment->set("action", "session")
				->set("posAction", "balance")
				->send();

			// If session opened successfully
			if ($session->id) {
				$this->session->set_userdata('ets_session_id', (string)$session->id);
				$data = array(
					'session' => $session,
					'url' => site_url('home/view_ets_giftcard')
				);

				$this->load->view('sales/ets_giftcard_balance.php', $data);
			} // If session failed to open
			else {
				$data = array('processor' => 'ETS');
				$this->load->view('sales/cant_load', $data);
			}

			// If course is using ForeUp giftcards
		} else {
			$this->load->view("giftcards/lookup", $data);
		}
	}

	function view_key_guide()
	{
		$data = array();
		$this->load->view('help/quick_key_guide');
	}

	function giftcard_lookup()
	{
		$type = $this->input->post('type');
		if (!$type) {
			$type = 'giftcard';
		}
		$number = $this->input->post('giftcard_number');

		if ($type == 'giftcard') {
			$info = $this->Giftcard->get_info($this->Giftcard->get_giftcard_id($number));
		} else {
			$info = $this->Punch_card->get_info((int)$this->Punch_card->get_punch_card_id($number));
		}
		echo json_encode(array($info));
	}

	function add_article()
	{
		if (!$this->permissions->is_super_admin())
			return;
		//echo 'stuff';
		$this->load->view('articles/form');
	}

	function save_article()
	{
		$error = '';
		$data = '';
		$config['upload_path'] = './images/home';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '100';
		$config['max_width'] = '360';
		$config['max_height'] = '260';
		//$config['file_name'] = '';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('img'))
			$error = $this->upload->display_errors();
		else
			$data = $this->upload->data();

		//print_r($data);
		//return;
		$article_data = array(
			'title' => $this->input->post('title'),
			'text' => $this->input->post('text'),
			'img' => $data['file_name'],
			'column' => $this->input->post('column')
		);
		$this->Article->save($article_data);
		$this->index();
		//echo json_encode(array('home_page'=>$this->index(1), 'error'=>$error, 'data'=>$data));
	}
}