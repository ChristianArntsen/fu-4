<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

	private $upload_path;
	private $upload_path_thumb;
	private $upload_url;
	private $upload_url_thumb;
	private $image_sizes;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('image');
		$this->load->helper(array('form', 'url'));
		$this->load->library('image_lib');
		ini_set('memory_limit', '256M');

		$this->images_path = 'archives/images';
		$this->course_path = 'archives/images/'.$this->session->userdata('course_id');
		$this->upload_path = 'archives/images/'.$this->session->userdata('course_id').'/default';
		$this->upload_path_thumb = 'archives/images/'.$this->session->userdata('course_id').'/thumbnails';
		$this->upload_path_preview = 'archives/images/'.$this->session->userdata('course_id').'/previews';
// echo $this->images_path;
// echo $this->course_path;
// echo $this->upload_path;
		if(!is_dir($this->images_path)){
			mkdir($this->images_path, 0755, true);
		}
		if(!is_dir($this->course_path)){
			mkdir($this->course_path, 0755, true);
		}	
		if(!is_dir($this->upload_path)){
			mkdir($this->upload_path, 0755, true);
		}

		if(!is_dir($this->upload_path_thumb)){
			mkdir($this->upload_path_thumb, 0755, true);
		}

		if(!is_dir($this->upload_path_preview)){
			mkdir($this->upload_path_preview, 0755, true);
		}

		$this->upload_url = $this->image->upload_url($this->session->userdata('course_id'));
		$this->upload_url_thumb = $this->image->upload_url_thumb($this->session->userdata('course_id'));
		$this->upload_url_preview = $this->image->upload_url_preview($this->session->userdata('course_id'));
	}

	public function index($module = '')
	{


		$data['preview_url'] = $this->image->upload_url_preview($this->session->userdata('course_id'));
		$data['thumb_url'] = $this->image->upload_url_thumb($this->session->userdata('course_id'));
		$data['url'] = $this->image->upload_url($this->session->userdata('course_id'));

		$data['preview_path'] = $data['preview_url'];
		$data['thumb_path'] = $data['thumb_url'] ;
		$data['path'] = $data['url'];


		$data['uploading'] = false;
		$data['editing'] = false;

		$data['crop_ratio'] = $this->input->get('crop_ratio', 1);
		$data['width'] = $this->input->get('width');
		$data['height'] = $this->input->get('height');
		$data['module'] = $module;
		$data['image_id'] = $this->input->get('image_id');
		$data['files'] = $this->image->get_files($data['module']);
		$this->load->view('upload/manage', $data);
	}

	public function view($image_id)
	{
		$data = $this->image->get_file($image_id);
		$editing = $this->input->get('editing');
		$uploading = $this->input->get('uploading');

		$data['preview_url'] = $this->image->upload_url_preview($data['course_id']);
		$data['thumb_url'] = $this->image->upload_url_thumb($data['course_id']);
		$data['url'] = $this->image->upload_url($data['course_id']);
		$data['editing'] = $editing;
		$data['uploading'] = $uploading;
		$data['crop_ratio'] = $this->input->get('crop_ratio', 1);
		$this->load->view('upload/manage_image', $data);
	}

	public function view_list($module = '')
	{
		$data['files'] = $this->image->get_files($module);
		$editing = $this->input->get('editing');

		$data['preview_url'] = $this->image->upload_url_preview($data['course_id']);
		$data['thumb_url'] = $this->image->upload_url_thumb($data['course_id']);
		$data['url'] = $this->image->upload_url($data['course_id']);
		$data['editing'] = $editing;
		$data['crop_ratio'] = $this->input->get('crop_ratio', 1);
		$this->load->view('upload/image_list', $data);
	}

	public function get_info($image_id){
		$image = $this->image->get_file($image_id);
		$image['last_update'] = strtotime($image['date_updated']);
		echo json_encode($image);
	}

	public function search()
	{
		$term = $this->input->get('q');
		$module = $this->input->get('module');
		$data['files'] = $this->image->search($term, $module);

		$data['preview_url'] = $this->image->upload_url_preview($this->session->userdata('course_id'));
		$data['thumb_url'] = $this->image->upload_url_thumb($this->session->userdata('course_id'));
		$data['url'] = $this->image->upload_url($this->session->userdata('course_id'));

		$this->load->view('upload/image_list', $data);
	}

	public function upload_file()
	{
		$status = "";
		$msg = "";
		$file_id = '';
		$file_element_name = 'userfile';

		$crop_ratio = $this->input->post('crop_ratio');
		$module = $this->input->post('module');
		$width = $this->input->post('width');
		$height = $this->input->post('height');

		/*
		if (empty($_POST['title']))
		{
			$status = "error";
			$msg = "Please enter a title";
		}*/

		if ($status != "error")
		{
			$config['upload_path'] = $this->upload_path;
			$config['allowed_types'] = 'gif|jpg|png|doc|txt';
			$config['max_size']  = 1024 * 8;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload($file_element_name))
			{
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			}
			else
			{
				$data = $this->upload->data();

				$image_data = array(
					'filename' => $data['file_name'],
					'label' => '',
					'width' => $data['image_width'],
					'height' => $data['image_height'],
					'filesize' => $data['file_size'],
					'module' => $module,
					'course_id'	=> $this->session->userdata('course_id'),
					'date_created' => date('Y-m-d h:i:s')
				);

				$file_id = $this->image->save_file(null, $image_data);
				if($file_id)
				{
					$this->standardize($file_id, $data['file_name']);
					$this->make_thumbnail($file_id, $data['file_name']);
					$this->make_preview($file_id, $data['file_name']);
					$this->transfer_to_s3($data['file_name']);
					$status = "success";
					$msg = "File successfully uploaded";
				}
				else
				{
					unlink($data['full_path']);
					$status = "error";
					$msg = "Error saving file, please try again.";
				}
			}
			@unlink($_FILES[$file_element_name]);
		}

		echo json_encode(array('status' => $status, 'msg' => $msg, 'image_id' => $file_id, 'crop_ratio'=>$crop_ratio));
	}

	private function transfer_to_s3($fileName)
	{
		$s3 = new fu\aws\s3();
		$s3->uploadToFileBucket($this->upload_path."/".$fileName,$this->session->userdata('course_id').'/default/'.$fileName);
		$s3->uploadToFileBucket($this->upload_path_preview."/".$fileName,$this->session->userdata('course_id').'/previews/'.$fileName);
		$s3->uploadToFileBucket($this->upload_path_thumb."/".$fileName,$this->session->userdata('course_id').'/thumbnails/'.$fileName);
	}

	private function get_image_info($filename)
	{
		// Get dimensions of image
		$image_info = getimagesize($this->upload_path.'/'.$filename, $image_info);

		// Get filesize of image in kilobytes
		$filesize = round(filesize($this->upload_path.'/'.$filename) / 1024, 2);

		$image_data = array(
			'width' => $image_info[0],
			'height' => $image_info[1],
			'filesize' => $filesize
		);
		return $image_data;
	}

	private function standardize($image_id, $filename)
	{
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 600;
		$config['height'] = 600;
		$config['image_library'] = 'ImageMagick';
		$config['library_path']='/usr/bin';
		$config['source_image']	= $this->upload_path.'/'.$filename;

		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		$image_info = $this->get_image_info($filename);
		$this->image->save_file($image_id, $image_info);
	}

	private function make_thumbnail($image_id, $filename)
	{
		$this->image_lib->clear();
		$config['image_library'] = 'gd2';
		$config['source_image']	= $this->upload_path.'/'.$filename;
		$config['new_image'] = $this->upload_path_thumb.'/'.$filename;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 75;
		$config['height'] = 75;

		$this->image_lib->initialize($config);
		$this->image_lib->resize();
	}

	private function make_preview($image_id, $filename)
	{
		$this->image_lib->clear();
		$config['image_library'] = 'gd2';
		$config['source_image']	= $this->upload_path.'/'.$filename;
		$config['new_image'] = $this->upload_path_preview.'/'.$filename;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 600;
		$config['height'] = 450;

		$this->image_lib->initialize($config);
		$this->image_lib->resize();
	}

	public function save_image($image_id)
	{
		$this->image_lib->clear();

		$label = $this->input->post('label');
		if(empty($label)){
			echo json_encode(array('msg'=>'Image label is required', 'status'=>'error'));
			return false;
		}

		$x = $this->input->post('x');
		$y = $this->input->post('y');
		$w = $this->input->post('w');
		$h = $this->input->post('h');

		$image_info = $this->image->get_file($image_id);
		if($image_info['course_id'] != $this->session->userdata('course_id')){
			return false;
		}

		// If no crop was made, do not process image
		if($x != 0 || $y != 0 || $w != 0 || $h != 0){
			$config['image_library'] = 'gd2';
			$config['source_image'] = $this->upload_path.'/'.$image_info['filename'];
			$config['maintain_ratio'] = FALSE;
			$config['x_axis'] = $x;
			$config['y_axis'] = $y;
			$config['width'] = $w;
			$config['height'] = $h;
			$this->image_lib->initialize($config);
			$cropped = $this->image_lib->crop();

			$this->standardize($image_id, $image_info['filename']);
			$this->make_preview($image_id, $image_info['filename']);
			$this->make_thumbnail($image_id, $image_info['filename']);
		}else{
			$cropped = true;
		}

		// Save new image info
		if($cropped){
			$data = $this->get_image_info($image_info['filename']);
			$data['label'] = $label;
			$data['saved'] = 1;
			$this->image->save_file($image_id, $data);
		}

		$data = $this->image->get_file($image_id);
		$data['preview_url'] = $this->upload_url_preview;
		$data['thumb_url'] = $this->upload_url_thumb;
		$data['url'] = $this->upload_url;
		$data['editing'] = false;
		$data['uploading'] = false;

		echo json_encode(array('msg'=>'Image saved', 'status'=>'success'));
	}

	public function save_cropped_image($filename)
	{
		$this->image_lib->clear();
		$x = $this->input->post('x');
		$y = $this->input->post('y');
		$w = $this->input->post('w');
		$h = $this->input->post('h');
		echo $x.' - '.$y.' - '.$w.' - '.$h;
		$config['image_library'] = 'gd2';
		$config['source_image'] = './uploads/'.$filename;
		$config['maintain_ratio'] = FALSE;
		$config['x_axis'] = $x;
		$config['y_axis'] = $y;
		$config['width'] = $w;
		$config['height'] = $h;
		echo 'about to load library';
		$this->image_lib->initialize($config);
		$this->image_lib->crop();

		unset($config); // clear $config
		//echo $this->load->library('image_lib', $config);
		$this->image_lib->clear();
		$config['image_library'] = 'gd2';
		$config['source_image'] = './uploads/'.$filename;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 640;
		$config['height'] = 640;

		$this->image_lib->initialize($config);

		//$size = _get_size($tempFile);

		if ( ! $this->image_lib->resize())
		{
			echo $this->image_lib->display_errors();
		}
		echo '<br/>About to make thumb';
		$this->make_thumbnail($filename);
	}

	public function files()
	{
		$files = $this->image->get_files();
		$this->load->view('upload/files', array('files' => $files));
	}

	public function crop($image_id)
	{
	   $file = $this->image->get_file($image_id);
	   $this->load->view('upload/crop_file', array('file' => $file));
	}

	public function crop_image($image_id)
	{
		$file = $this->image->get_file($image_id);
		print_r($file);
		$this->save_cropped_image($file->filename);
	}

	public function delete_file($file_id)
	{
		if ($this->image->delete_file($file_id))
		{
			$status = 'success';
			$msg = 'File successfully deleted';
		}
		else
		{
			$status = 'error';
			$msg = 'Error deleteing file, please try again';
		}
		echo json_encode(array('status' => $status, 'msg' => $msg, 'file_id'=>$file_id));
	}

	public function do_upload()
	{

		$upload_path_url = 'archives/images/'.$this->session->userdata('course_id').'/default';
		$config['upload_path'] = 'archives/images/'.$this->session->userdata('course_id').'/default';
		$config['allowed_types'] = 'jpg';
		$config['max_size'] = '200000';

	  	$this->load->library('upload', $config);

	  	if ( ! $this->upload->do_upload())
	  	{
	  		$error = array('error' => $this->upload->display_errors());
	  		$this->load->view('upload/manage', $error);
	  	}
	  	else
	  	{
		   $data = $this->upload->data();
		/*
                  // to re-size for thumbnail images un-comment and set path here and in json array
		   $config = array(
			'source_image' => $data['full_path'],
			'new_image' => $this->$upload_path_url '/thumbs',
			'maintain_ration' => true,
			'width' => 80,
			'height' => 80
		  );

		$this->load->library('image_lib', $config);
		$this->image_lib->resize();
		*/
		//set the data for the json array
		$info->name = $data['file_name'];
		$info->size = $data['file_size'];
		$info->type = $data['file_type'];
		$info->url = $upload_path_url .$data['file_name'];
		$info->thumbnail_url = $upload_path_url .$data['file_name'];//I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$data['file_name']
		$info->delete_url = base_url().'upload/deleteImage/'.$data['file_name'];
		$info->delete_type = 'DELETE';

		if (IS_AJAX) {   //this is why we put this in the constants to pass only json data
		           echo json_encode(array($info));
	                    //this has to be the only the only data returned or you will get an error.
	                    //if you don't give this a json array it will give you a Empty file upload result error
	                    //it you set this without the if(IS_AJAX)...else... you get ERROR:TRUE (my experience anyway)
	                      }
		else {   // so that this will still work if javascript is not enabled
			  	$file_data['upload_data'] = $this->upload->data();
			  	$this->load->view('upload/upload_success', $file_data);
			}
		}
	}

	public function delete_image($image_id)
	{
		$success =unlink(FCPATH.'uploads/' .$file);

		$info->sucess =$success;
		$info->path =base_url().'uploads/' .$file;
		$info->file =is_file(FCPATH.'uploads/' .$file);

		if (IS_AJAX) {//I don't think it matters if this is set but good for error checking in the console/firebug
		    echo json_encode(array($info));}

		else {     //here you will need to decide what you want to show for a successful delete
			  	$file_data['delete_data'] = $file;
			  	$this->load->view('admin/delete_success', $file_data);
		}
	}
}