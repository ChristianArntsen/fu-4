<?php

class Subscriptions extends CI_Controller
{	
    function __construct()
    {
        parent::__construct('unsubscribe');
		$this->load->model('Person');
		$this->load->model('Customer');
		$this->load->model('Course');
		$this->load->model('Subscription');
	}

    function unsubscribe()
    {
    	$course_id = $this->input->get('course_id');
    	$this->session->set_userdata('course_id', $course_id);		    
    	$course_info = $this->Course->get_info($course_id);	
		$this->session->set_userdata('course_name', $course_info->name);
		$course_info = (array)$course_info;
        $this->load->view("subscriptions/form", $course_info);		
    }
	
	function submit()
	{
		$email = $this->input->post('email');
		$course_id = $this->input->post('course_id');
		// $unsubscribe_all = $this->input->post('all');
		// $course_news_announcements = $this->input->post('news');
		// $teetime_reminders = $this->input->post('reminders');
		// $foreup_news_announcements = $this->input->post('foreup_news');
		$comments = $this->input->post('comments');	
		
		$customer_info = array();					
		
		$unsubscribe_data = array(
			'email'=>$email,
			'date'=>date('Y-m-d H:i:s',time()),
			// 'opt_out_email'=>'Y',
			'course_news_announcements'=>1,
			// 'teetime_reminders'=>$teetime_reminders,			
			'comments'=>$comments
		);		
		
		if ($this->Subscription->unsubscribe_email($unsubscribe_data, $customer_info, $course_id)) {								
			// $this->load->view('subscriptions/confirm', $customer_info);	
			$results = array(
				'success'=>1,
				'message'=>"Subscription preferences have been updated successfully for {$email}"
			);
			echo json_encode($results);		
		}
		else 
		{
			echo "Error. Could not unsubscribe email provided";
		}					
	}
	
	
}
?>