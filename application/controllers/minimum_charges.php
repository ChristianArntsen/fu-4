<?php
require_once(APPPATH.'controllers/secure_area.php');

class Minimum_charges extends Secure_area {

	function __construct() {
		parent::__construct();
		$this->load->model('Minimum_charge');
	}

    function index(){
		$charges = $this->Minimum_charge->get();
		$data['minimum_charges'] = $charges;
		$this->load->view('minimum_charges/table', $data);
    }

    function view_charge($charge_id = null){
		
		$this->load->model('v2/Item_model');
		$charge = array(
			'name' => '',
			'is_active' => 1,
			'start_date' => '0000-01-01',
			'end_date' => '0000-12-31',
			'minimum_amount' => 0.00,
			'frequency' => 1,
			'frequency_period' => 'month',
			'frequency_on' => '',
			'frequency_on_date' => '',
			'item_filters' => array('departments' => array(), 'categories' => array(), 'sub_categories' => array())
		);

		if(!empty($charge_id)){
			$charge = $this->Minimum_charge->get(array('charge_id' => $charge_id));
			$charge = $charge[0];
		}

		$data['charge'] = $charge;
		$data['departments'] = $this->Item_model->get_departments();
		$data['categories'] = $this->Item_model->get_categories();
		$data['sub_categories'] = $this->Item_model->get_sub_categories();

		$this->load->view('minimum_charges/form', $data);
    }

    function save_charge($charge_id = null){
    	$data = $this->input->post();
    	
    	$data['start_date'] = date('Y-m-d', strtotime('0000-'.$data['start_date_month'].'-'.$data['start_date_day']));
    	$data['end_date'] = date('Y-m-d', strtotime('0000-'.$data['end_date_month'].'-'.$data['end_date_day']));

    	if($data['frequency_on'] == 'date'){
			if($data['frequency_period'] == 'month'){
				$data['frequency_on_date'] = '1900-01-'.sprintf('%1$02d', $data['frequency_on_day']);	
			}else if($data['frequency_period'] == 'year'){
				$data['frequency_on_date'] = '1900-'.sprintf('%1$02d', $data['frequency_on_month']).'-'.sprintf('%1$02d', $data['frequency_on_day']);				
			}
    	}else{
    		$data['frequency_on_date'] = null;
    	}

    	$charge_id = $this->Minimum_charge->save($data, $charge_id);
    	echo json_encode(array('charge_id' => $charge_id));
    }

	function delete($charge_id = null){
		$success = $this->Minimum_charge->delete($charge_id);
		echo json_encode(array('success'=>$success));
	}
}
?>