<?php
$lang['invoices_new']='New Invoice';
$lang['invoices_new_invoice_or_billing']='New Invoice/Billing';
$lang['invoices_batch_pdfs']='Batch PDFs';
$lang['invoices_templates']='Templates';
$lang['invoices_settings']='Billing Settings';
$lang['invoices_new_recurring_billing']='New Recurring Billing';
$lang['invoice_details']='Invoice Details';
$lang['invoices_basic_information'] ='Invoice Details';
$lang['invoices_invoice_number']='Invoice Number';
$lang['invoices_invoice_date_billed']='Date Billed';
$lang['invoices_invoice_date_created']='Date Created';
$lang['invoices_invoice_customer_name']='Customer Name';
$lang['invoices_invoice_total']='Total';
$lang['invoices_invoice_paid']='Paid';
$lang['invoices_invoice_amount_due']='Due';
$lang['invoices_no_batch_files'] = 'There are no batched invoice files available';
$lang['invoices_generate_on_message'] = 'If set, all invoices will be generated on this day of the month';
$lang['invoices_generate_on'] = 'Generate on';
$lang['invoices_paid'] = 'Paid';
$lang['invoices_overdue'] = 'Overdue';
$lang['invoices_confirm_delete'] = 'You are about to delete some Invoices or Recurring Billings. This action cannot be undone.\n\nPaid invoices will not be deleted. Please refund associated payments before deleting invoices.\n\n Would you like to proceed?';
$lang['invoices_update'] = 'Update Invoice/Billing';
$lang['invoices_pay_options'] = 'Payment Options';
$lang['invoies_projected_bills'] = 'Projected Bills';
$lang['invoices_notes'] = 'Note';
$lang['invoices_payment'] = 'Payment';
$lang['invoices_charge_attempts'] = 'Charge Attempts';
$lang['invoices_change_date'] = 'Change Date';
$lang['invoices_due_date'] = 'Due Date';
?>