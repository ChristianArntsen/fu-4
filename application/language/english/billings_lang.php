<?php
$lang['billings_new']='New Billing';
$lang['billings_no_billings_to_display']='No Billings to display';
$lang['billings_no_invoices_to_display']='No Invoices to display';
$lang['billings_annual_charge'] = 'Annual Charge';
$lang['billings_monthly_charge'] = 'Monthly Charge';
$lang['billings_email'] = 'Billing Email';
$lang['billings_website_charge'] = 'Website Charge';
$lang['billings_email_marketing_charge'] = 'Email Marketing Charge';
$lang['billings_email_marketing_limit'] = 'Email Marketing Limit';
$lang['billings_text_marketing_charge'] = 'Text Marketing Charge';
$lang['billings_text_marketing_limit'] = 'Text Marketing Limit';
$lang['billings_new_credit_card'] = 'Manage Credit Cards';
$lang['billings_saved_cards'] = 'Saved Cards';
$lang['billings_add_card'] = "Add Credit Card";

$lang['billings_required_course'] = 'Course required';
$lang['billings_required_email_format'] = 'Billing email not valid';
$lang['billings_required_start_date'] = 'Start date required';
$lang['billings_required_payment_method_selected'] = 'At least one payment method required';
$lang['billings_required_annual_amount'] = 'Annual amount required';
$lang['billings_required_monthly_amount'] = 'Monthly amount required';
$lang['billings_required_teetimes_amount'] = 'Daily or weekly teetime quantity required';
$lang['billings_required_teesheet_id'] = 'Tee sheet required';
$lang['billings_required_credit_card'] = 'Credit card required for billing';

$lang['billings_product'] = 'Product';
$lang['billings_annual'] = 'Annual';
$lang['billings_monthly'] = 'Monthly';
$lang['billings_teetimes'] = 'Teetimes';
$lang['billings_start_date'] = 'Start Date';
$lang['billings_add_charges'] = 'Add Charges';
$lang['billings_charge_card'] = 'Charge Card Now';
$lang['billings_credit_card'] = 'Credit Card';
$lang['billings_totals'] = 'Totals';
$lang['billings_tax_name'] = 'Tax Name';
$lang['billings_free'] = 'Free';

// CUSTOMER BILLING 
$lang['billings_golf_course'] = 'Golf Course';
$lang['billings_customer'] = 'Customer';
$lang['billings_billing_title'] = 'Billing Title';
$lang['billings_amount_charged'] = 'Amount Charged';
$lang['billings_charge_successful'] = 'Charge Successful?*';
$lang['billings_email_sent'] = 'Email Sent?*';
$lang['billings_charge_successful_note'] = '* Charge Successful? If "Yes" then card was processed and the full amount was collected. If "No" then card declined and no payment was collected.';
$lang['billings_email_sent_note'] = '** Email Sent? If "Yes" then the customer received a copy of their successful payment. If "No" then customer\'s email was invalid or the payment did not process. If "Not Active" then the recurring billing profile set up for this customer does not invlude an email address or was not set to send. Therefore no email was sent to the customer.';
$lang['billings_stage'] = 'Stage';
