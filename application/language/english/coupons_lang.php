<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * table headers
 */
$lang['coupons_title'] = 'Title';
$lang['coupons_number_available'] = 'No. Available';
$lang['coupons_amount_type'] = 'Amount Type';
$lang['coupons_amount'] = 'Amount';
$lang['coupons_rules'] = 'Rules';
$lang['coupons_expiration_date'] = 'Expiration';
$lang['coupons_no_data_to_display'] = 'No coupons to display.';

$lang['coupons_new'] = 'New Coupon';
$lang['coupons_update'] = 'Update Coupon';
$lang['coupons_confirm_delete']='Are you sure you want to delete the selected coupons?';
$lang['coupons_none_selected']='You have not selected any coupons to edit.';