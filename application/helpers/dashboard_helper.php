<?php
function get_graph($widget_id, $graph_type, $graph_data)
{
	$CI = &get_instance();

	if($graph_type == 'linechart'){

		$html = "
		chart = new Highcharts.Chart({
			chart: {
				renderTo: $('#widget_{$widget_id} > div.chart')[0],
				type: 'line',
				borderRadius: 0,
				spacingTop: 20
			},
			title: null,
			subtitle: {
				text: '',
				x: -20
			},
			xAxis: {
				categories: response.categories
			},
			yAxis: {
				title: null,
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			},
			tooltip: {
				formatter: function() {
						return '<b>'+ this.series.name +'</b><br/>'+
						this.x +': $'+ (this.y).toFixed(2);
				}
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'top',
				x: -10,
				y: 100,
				borderWidth: 0
			},
			series: response.data
		});";
	}
}
?>