<?php

class Marketing_Texting_Queue extends MY_Model
{
    protected $_table = "marketing_texting_queue";

    protected $validate = array(
        [
            'field' => 'course_id',
            'label' => 'course_id',
            'rules' => 'required|integer'
        ],
        [
            'field' => 'job_name',
            'label' => 'job_name',
            'rules' => 'required|enum[enable_texting, teeoff, teetime_reminder]',
            'errors' => [
                'enum' => '{field} must be one of the following: {param}',
            ],
        ],
        [
            'field' => 'status',
            'label' => 'status',
            'rules' => ''
        ],
        [
            'field' => 'parameters',
            'label' => 'parameters',
            'rules' => ''
        ]
    );

	function __construct()
	{
		parent::__construct();
	}

}
