<?php
class Booking_class extends CI_Model
{
	/*
	 * Gets avaialble booking classes by teesheet id
	 */
	function get_all($teesheet_id)
	{
		$this->db->select('booking_class.*, IF(IFNULL(price_class.cart, 1) = 1, booking_class.booking_carts, 0) AS booking_carts', false);
		$this->db->from('booking_classes AS booking_class');
		$this->db->join('price_classes AS price_class', 'price_class.class_id = booking_class.price_class', 'left');
		$this->db->where("booking_class.teesheet_id = {$teesheet_id}");
		$this->db->where('booking_class.deleted', 0);
		$this->db->where('booking_class.is_aggregate', 0);
		$this->db->group_by('booking_class.booking_class_id');
        $result = $this->db->get()->result_array();
		
		foreach($result as &$row){
			$row['pass_item_ids'] = explode(',',$row['pass_item_ids']);
			if(empty($row['pass_item_ids'])){
				$row['pass_item_ids'] = [];
			}

			if($row['booking_fee_enabled'] == 1 && !empty($row['booking_fee_item_id'])){
				$this->load->model('Online_booking');
				$fee = $this->Online_booking->get_booking_fee($this->session->userdata('course_id'), $row['teesheet_id'], $row['booking_class_id']);
				if(!empty($fee)){
					$row['booking_fee_item'] = $fee;
				}
			}
		}

		return $result;
	}
	
	/*
	 * Gets info on a single booking class
	 */
	function get_info($booking_class_id, $teesheet_id = false)
	{
		$this->db->select('booking_class.*, IF(IFNULL(price_class.cart, 1) = 1, booking_class.booking_carts, 0) AS booking_carts', false);
		$this->db->from('booking_classes AS booking_class');
		$this->db->join('price_classes AS price_class', 'price_class.class_id = booking_class.price_class', 'left');
		$this->db->where('booking_class.booking_class_id',$booking_class_id);
		if(!empty($teesheet_id)){
			$this->db->where('booking_class.teesheet_id', $teesheet_id);
		}
		$this->db->group_by('booking_class.booking_class_id');
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			$data = $query->row();
			$data->pass_item_ids = explode(',',$data->pass_item_ids);
			if(empty($data->pass_item_ids)){
				$data->pass_item_ids = [];
			}

			$data->booking_fee_item = false;
			if($data->booking_fee_enabled == 1 && !empty($data->booking_fee_item_id)){
				$this->load->model('v2/Item_model');
				$this->load->model('v2/Cart_model');

				$items = $this->Item_model->get([
					'item_id' => $data->booking_fee_item_id,
					'include_inactive' => true,
					'include_deleted' => true
				]);
				
				if(!empty($items[0])){
					$item = $items[0];
					$item['quantity'] = 1;
					$this->Cart_model->calculate_item_totals($item, true);
					$data->booking_fee_item = $item;
				}
			}
			return $query->row();
		}
		else
		{
			//Get all the fields from booking_classes table
			$fields = $this->db->list_fields('booking_classes');

			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$booking_class_obj->$field='';
			}

			return $booking_class_obj;
		}
	}

	// Get the aggregate setting row, if no aggregate setting exists, create it
	function get_aggregate_settings($teesheet_id){
		
		// Get aggregate setting row
		$this->db->select('booking_class_id');
		$this->db->from('booking_classes');
		$this->db->where('teesheet_id', (int) $teesheet_id);
		$this->db->where('is_aggregate', 1);
		$row = $this->db->get()->row_array();

		// If now aggregate setting row is found, create one
		if(empty($row['booking_class_id'])){
			$this->db->insert('booking_classes', array(
				'teesheet_id' => (int) $teesheet_id,
				'name' => 'Aggregate Settings',
				'is_aggregate' => 1,
				'online_open_time' => '0600',
				'online_close_time' => '1800',
				'days_in_booking_window' => 7,
				'minimum_players' => 1,
				'limit_holes' => 0,
				'active' => 1
			));
			$booking_class_id = $this->db->insert_id();
		
		}else{
			$booking_class_id = $row['booking_class_id'];
		}
		
		return $this->get_info($booking_class_id, $teesheet_id);
	}

	function load_info($booking_class_id, $teesheet_id)
	{
		$booking_info = $this->get_info($booking_class_id, $teesheet_id);
		if ($booking_info->booking_class_id != '')
		{
			$this->session->set_userdata('booking_class_id', $booking_info->booking_class_id);
			$this->session->set_userdata('booking_class_info', $booking_info);
			
			return true;
		}
		else {
			return false;
		}
	}
	/*
	Save booking class and group
	*/
	function save($data,$group_ids = false, $booking_class_id = false)
	{
		
		if(!empty($data['pass_item_ids']) && is_array($data['pass_item_ids'])){
			$data['pass_item_ids'] = implode(',',$data['pass_item_ids']);
		}else{
			$data['pass_item_ids'] = null;
		}

		if ($booking_class_id)
		{
			// UPDATE BOOKING CLASS AND GROUP
			$this->db->where('booking_class_id', $booking_class_id);
			$this->db->update("booking_classes", $data);
		}
		else 
		{
			// INSERT BOOKING CLASS AND GROUP
			$this->db->insert("booking_classes", $data);
			$booking_class_id = $this->db->insert_id();
		}
		
		// DELETE old booking class groups	
		$this->db->where('booking_class_id', $booking_class_id);
		$this->db->delete('booking_class_groups');
		
		if(!empty($group_ids)){
			// CREATE NEW booking class groups
			foreach ($group_ids as $group_id) {
				$booking_class_group_data = array(
					'booking_class_id'=>$booking_class_id,
					'group_id'=>$group_id
				);
				$this->db->insert('booking_class_groups', $booking_class_group_data);
			}
		}
	}
	function validate_user($booking_class_id, $type, $person_id)
	{
		$this->db->from('booking_class_validators');
		$this->db->join('customers', 'booking_class_validators.value = customers.price_class');
		$this->db->where('person_id', $person_id);
		$this->db->where('booking_class_id', $booking_class_id);
		$this->db->where('type', $type);
		$this->db->limit(1);
		
		$result = $this->db->get();
		return $result->num_rows() == 1;
	}
	function save_validators($booking_class_id, $validators)
	{
		$this->delete_validators($booking_class_id);
		$this->db->insert_batch($validators);
	}
	function delete_validators($booking_class_id)
	{
		$this->db->where('booking_class_id', $booking_class_id);
		$this->db->delete('booking_class_validators');
	}
	function delete($teesheet_id, $booking_class_id)
	{
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->where('booking_class_id', $booking_class_id);
		return $this->db->update('booking_classes', array('deleted'=>1));
	}

	function get_groups($booking_class_id){
		$this->db->from('booking_class_groups');
		$this->db->where('booking_class_id',$booking_class_id);
		$result = $this->db->get();
		$class_groups = array();
		foreach ($result->result_array() as $class_group) {
			$class_groups[] = $class_group['group_id'];
		}
		return $class_groups;
	
	}
	function get_group_label($booking_class_id){
		$this->db->select("group_id");
		$this->db->from('booking_class_groups');
		$this->db->where('booking_class_id',$booking_class_id);
		$result = $this->db->get()->result_array();

		return $result;
	}

	function get_customer_pass_booking_class_ids($customer_id){
		$this->load->model("Pass");
		$passes = $this->pass->get([
			"customer_id"=>$customer_id
		]);


	}

	function get_customer_booking_class_ids($customer_id){
		$this->load->model("Teesheet");
		$teesheets = $this->teesheet->get_all()->result_array();
		$booking_class_ids = [];
		$this->load->model("User");
		foreach($teesheets as $teesheet){
			foreach($this->get_all($teesheet['teesheet_id']) as $bookingClass){
				if(isset($booking_class_ids[$bookingClass['booking_class_id']]))
					continue;
				$booking_class_ids[$bookingClass['booking_class_id']] = $this->User->has_booking_class_access($customer_id,$bookingClass['booking_class_id']);
			}
		}
		$to_return = [];
		foreach($booking_class_ids as $id => $active){
			if($active)
				$to_return[] = $id;
		}
		return $to_return;

		/**
		 * Fix DEV-3069
		 */
//		$rows = $this->db->select('booking_class_id')
//			->from('customer_group_members AS customer_group')
//			->where('customer_group.person_id', (int) $customer_id)
//			->join('booking_class_groups AS booking_class_group', 'booking_class_group.group_id = customer_group.group_id', 'left')
//			->get()
//			->result_array();
//
//		if(empty($rows)){
//			return [];
//		}
//
//		$booking_class_ids = [];
//		foreach($rows as $row){
//			$booking_class_ids[] = (int) $row['booking_class_id'];
//		}
//
//		return $booking_class_ids;
	}
}