<?php
class Green_fee extends CI_Model
{
	/*
	Determines if a given teesheet has greenfees
	*/
	function exists( $item_number, $teesheet_id = '' )
	{
		$teesheet_id = ($teesheet_id != '')?$teesheet_id:$this->session->userdata('teesheet_id');

		$this->db->from('green_fees');
		$this->db->where('item_number',$item_number);
		$this->db->where('teesheet_id',$teesheet_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	/*
	Determines if a given course has green_fee_types
	*/
	function types_exist( $course_id, $teesheet_id = '' )
	{
		$this->db->from('green_fee_types');
		$this->db->where('course_id',$course_id);
		$this->db->where('teesheet_id',$teesheet_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	function get_price_category($time)
	{
		$course_info = $this->course->get_info($this->session->userdata('course_id'));

		if ((int)$time >= (int)$course_info->early_bird_hours_begin && (int)$time < (int)$course_info->early_bird_hours_end) {
            $rate = 'early bird';
			//echo $i.' - '.$this->course_info->early_bird_hours_begin.' - '.$this->course_info->early_bird_hours_end.'<br/>';
            $col = 2;
        }
		else if ((int)$time >= (int)$course_info->morning_hours_begin && (int)$time < (int)$course_info->morning_hours_end) {
            $rate = 'morning';
            $col = 3;
        }
		else if ((int)$time >= (int)$course_info->afternoon_hours_begin && (int)$time < (int)$course_info->afternoon_hours_end) {
            $rate = 'afternoon';
            $col = 4;
        }
		else if ((int)$time >= (int)$course_info->super_twilight_hour) {
            $rate = 'super twilight';
            $col = 6;
        }
        else if ((int)$time >= (int)$course_info->twilight_hour) {
            $rate = 'twilight';
            $col = 5;
        }
        else {
            $rate = 'regular';
            $col = 1;
        }
        /*if (($day_of_week == 'Fri' && $course_info->weekend_fri) ||
                ($day_of_week == 'Sat' && $course_info->weekend_sat) ||
                ($day_of_week == 'Sun' && $course_info->weekend_sun)) {
            $day_rate = 'weekend';
			$cart_row += 1;
            $row += 3;
        }
        else {
            $day_rate = 'weekday';
			$cart_row += 2;
            $row += 4;
        }
        $time = $hour.':'.$min_leading_zero.$min.$am_pm;
        $sd_z = '';
        if ($this->selected_day < 10)
            $sd_z = '0';
        $i_z = '';
        if ($i < 1000)
            $i_z = '0';
        $time_string = $i_z.(int)$i;*/
		return "price_category_{$col}";
	}
	function get_price_categories() {
		$price_categories = $this->get_type_info();
		$price_categories = isset($price_categories[$this->session->userdata('teesheet_id')]) ? (array)($price_categories[$this->session->userdata('teesheet_id')]) : array();
		$price_categories = array_filter($price_categories);
		unset($price_categories['course_id']);
		unset($price_categories['teesheet_id']);

		return $price_categories;
	}
	/*
	Gets information about a particular item's green_fees
	*/
	function get_info($item_number = '', $teesheet_id = '', $price_category = '', $course_id = false)
	{
		$course_id = $course_id ? $course_id : $this->session->userdata('course_id');
			
		if ($price_category != ''){
			$this->db->select("teesheet_id, item_number, $price_category AS price");
		}
		$this->db->from('green_fees');
		
		if ($item_number != ''){
			$this->db->where('item_number',$item_number);
		}else{
			$this->db->like('item_number', "{$course_id}_");
		}
		
		if ($teesheet_id != ''){
			$this->db->where('teesheet_id',$teesheet_id);
		}
	
		$this->db->order_by('teesheet_id');
		$this->db->order_by('item_number');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			$price_array = array();
			foreach ($query->result() as $teetime_price)
				$price_array[$teetime_price->teesheet_id][$teetime_price->item_number] = $teetime_price;
			return $price_array;
		
		}else{
			//Get empty base parent object, as $giftcard_id is NOT an giftcard
			$green_fee_obj=new stdClass();

			//Get all the fields from giftcards table
			$fields = $this->db->list_fields('green_fees');

			foreach ($fields as $field)
			{
				$green_fee_obj->$field='';
			}

			return array(0=>$green_fee_obj);
		}
	}
	function rename_green_fees($course_id, $simulator)
	{
		if ($simulator) {
			$names = array(
				1=>'30 Min',
				2=>'1 Hour',
				3=>'1 Hour 30 Min',
				4=>'2 Hours',
				5=>'2 Hours 30 Min',
				6=>'3 Hours',
				7=>'3 Hours 30 Min',
				8=>'4 Hours'
			);
		}
		else
			$names = array(
				1=>'9 - Hole Weekday Cart',
				2=>'9 - Hole Weekend Cart',
				3=>'9 - Hole Weekday',
				4=>'9 - Hole Weekend',
				5=>'18 - Hole Weekday Cart',
				6=>'18 - Hole Weekend Cart',
				7=>'18 - Hole Weekday',
				8=>'18 - Hole Weekend'
			);
		for ($i=1; $i <= 8; $i++)
		{
			$this->db->where('item_number', $course_id.'_'.$i);
			$this->db->update('items', array('name'=> $names[$i]));
		}
	}
	/*
	Gets information about a particular course's green_fee_types
	*/
	function get_type_info($course_id='', $teesheet_id = '')
	{
		$course_id = ($course_id != '' && $course_id)?$course_id:$this->session->userdata('course_id');
		//$teesheet_id = ($teesheet_id != '')?$teesheet_id:$this->session->userdata('teesheet_id');

        $this->db->from('green_fee_types');
		$this->db->where('course_id',$course_id);
		if ($teesheet_id != '')
		{
			$this->db->where('teesheet_id',$teesheet_id);
			$this->db->limit(1);
		}
		$this->db->group_by('teesheet_id');
		$query = $this->db->get();
		//echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			$return_array = array();
			foreach($query->result() as $teetime_type)
			{
				$return_array[$teetime_type->teesheet_id] = $teetime_type;
			}
			return $return_array;
		}
		else
		{
			//Get empty base parent object, as $giftcard_id is NOT an giftcard
			$green_fee_type_obj=new stdClass();

			//Get all the fields from giftcards table
			$fields = $this->db->list_fields('green_fee_types');

			foreach ($fields as $field)
			{
				$green_fee_type_obj->$field='';
			}

			return array(0=>$green_fee_type_obj);
		}
	}



	function get_types($teesheet_id = false, $course_id = false, $get_old = false, $price_params = array())
	{
		$this->permissions->course_has_module('reservations') ? $this->load->model('schedule'):$this->load->model('Teesheet');
		$teesheet_id = $teesheet_id ? $teesheet_id : ($this->permissions->course_has_module('reservations') ? $this->schedule->get_default() : $this->Teesheet->get_default());
		$type_array = array();
		
		// If course is using new seasonal pricing
		if($this->config->item('seasonal_pricing') == 1 && !$get_old){
			$type_array = $this->get_price_classes_w_payments($teesheet_id, $price_params);
		
		// If course is using old pricing
		}else{
			$type_info = $this->get_type_info($course_id, $teesheet_id);
			
			foreach($type_info[$teesheet_id] as $index => $type)
			{
				if(substr($index, 0, 14) == 'price_category' && $type != '')
					$type_array[$index] = $type;
			}
		}
		return $type_array;
	}

	/*
	Inserts or updates a green_fees
	*/
	function save(&$green_fees_data)
	{
		if (!$this->exists($green_fees_data['item_number'], $green_fees_data['teesheet_id']))
		{
			if($this->db->insert('green_fees',$green_fees_data))
				return true;
			return false;
		}

		$this->db->where('item_number', $green_fees_data['item_number']);
		$this->db->where('teesheet_id', $green_fees_data['teesheet_id']);
		return $this->db->update('green_fees',$green_fees_data);
	}
	/*
	Inserts or updates a green_fee_types
	*/
	function save_types(&$green_fee_types_data)
	{
		if (!$this->types_exist($green_fee_types_data['course_id'], $green_fee_types_data['teesheet_id']))
		{
			if($this->db->insert('green_fee_types',$green_fee_types_data))
				return true;
			return false;
		}

		$this->db->where('course_id', $green_fee_types_data['course_id']);
		$this->db->where('teesheet_id', $green_fee_types_data['teesheet_id']);
		return $this->db->update('green_fee_types',$green_fee_types_data);
	}

	/*
	 Fetches prices for all of courses teesheets
	 */
	function get_prices($teesheets)
	{
		$price_array = array();
		foreach($teesheets->result() as $teesheet)
		{
			$price_array = array_merge($price_array, $this->get_teesheet_prices($teesheet->teesheet_id));
		}
		return $price_array;
	}
	function get_teesheet_prices($teesheet_id, $course_id = false)
	{
		$course_id = $course_id ? $course_id : $this->session->userdata('course_id');
		$price_array = array();
		for ($i = 1; $i <=8; $i++)
			 $price_array[$teesheet_id][$course_id."_".$i] = $this->get_info($course_id.'_'.$i, $teesheet_id, '', $course_id);
		return $price_array;
	}
	function get_teetime_types($teetime_index, $teesheet_id, $both = true, $all_rates = false)
    {
    	$no_weekend = (!$this->config->item('weekend_fri') && !$this->config->item('weekend_sat') && !$this->config->item('weekend_sun'));

    	if ($this->config->item('simulator'))
		{
			$ttime_array = array('1'=>"30 Min");
			if ($teetime_index == 2)
		    	$ttime_array = array('2'=>"1 Hour");
			else if ($teetime_index == 3)
				$ttime_array = array('3'=>'1 Hour 30 Min');
			else if ($teetime_index == 4)
				$ttime_array = array('4'=>'2 Hours');
			else if ($teetime_index == 5)
		    	$ttime_array = array('5'=>"2 Hours 30 Min");
			else if ($teetime_index == 6)
				$ttime_array = array('6'=>'3 Hours');
			else if ($teetime_index == 7)
				$ttime_array = array('7'=>'3 Hours 30 Min');
			else if ($teetime_index == 8)
				$ttime_array = array('8'=>'4 Hours');
		}
		else
		{
			if ($no_weekend)
			{
				if ($all_rates)
				{
					$ttime_array = array('7'=>"18 - ", '3'=>'9 - ');
					if ($teetime_index == 3)
				    	$ttime_array = array('3'=>"9 - ", '7'=>'18 - ');
				}
				else if ($both)
				{
					$ttime_array = array('7'=>"18 - ", '3'=>'9 - ');
					if ($teetime_index == 3)
				    	$ttime_array = array('3'=>"9 - ", '7'=>'18 - ');
				}
				else
				{
					$ttime_array = array('3'=>'9 - ');
					if ($teetime_index == 7)
						$ttime_array = array('7'=>'18 - ');
				}
			}
			else
			{
				if ($all_rates)
				{
					$ttime_array = array('7'=>"18 - Weekday", '3'=>'9 - Weekday','8'=>'18 - Weekend', '4'=>'9 - Weekend');
					if ($teetime_index == 3)
				    	$ttime_array = array('3'=>"9 - Weekday", '7'=>'18 - Weekday','4'=>'9 - Weekend', '8'=>'18 - Weekend');
					else if ($teetime_index == 8)
						$ttime_array = array('8'=>'18 - Weekend', '4'=>'9 - Weekend','7'=>"18 - Weekday", '3'=>'9 - Weekday');
					else if ($teetime_index == 4)
						$ttime_array = array('4'=>'9 - Weekend', '8'=>'18 - Weekend','3'=>"9 - Weekday", '7'=>'18 - Weekday');
				}
				else if ($both)
				{
					$ttime_array = array('7'=>"18 - Weekday", '3'=>'9 - Weekday');
					if ($teetime_index == 3)
				    	$ttime_array = array('3'=>"9 - Weekday", '7'=>'18 - Weekday');
					else if ($teetime_index == 8)
						$ttime_array = array('8'=>'18 - Weekend', '4'=>'9 - Weekend');
					else if ($teetime_index == 4)
						$ttime_array = array('4'=>'9 - Weekend', '8'=>'18 - Weekend');
				}
				else
				{
					$ttime_array = array('3'=>'9 - Weekday');
					if ($teetime_index == 4)
				    	$ttime_array = array('4'=>'9 - Weekend');
					else if ($teetime_index == 7)
						$ttime_array = array('7'=>'18 - Weekday');
					else if ($teetime_index == 8)
						$ttime_array = array('8'=>'18 - Weekend');
				}

			}
		}
    	$price_category_labels = $this->get_type_info('',$teesheet_id);
        $type_array = array();
		foreach ($ttime_array as $index => $teetime_label) {
			for ($i = 1; $i <=50; $i++) {
				$price_category = 'price_category_'.$i;
			    if ($i == 1)
				    $type_array['1_'.$index] = "$teetime_label Regular";
				else if ($i == 2) {
					if ($this->config->item('early_bird_hours_begin') != '2399')
			            $type_array['2_'.$index] = "$teetime_label Early Bird";
				}
				else if ($i == 3) {
			        if ($this->config->item('morning_hours_begin') != '2399')
			            $type_array['3_'.$index] = "$teetime_label Morning";
				}
				else if ($i == 4) {
					if ($this->config->item('afternoon_hours_begin') != '2399')
			            $type_array['4_'.$index] = "$teetime_label Afternoon";
				}
				else if ($i == 5) {
			        if ($this->config->item('twilight_hour') != '2399')
			            $type_array['5_'.$index] = "$teetime_label Twilight";
				}
				else if ($i == 6) {
					if ($this->config->item('super_twilight_hour') != '2399')
			            $type_array['6_'.$index] = "$teetime_label Super Twilight";
				}
				else if ($i == 7) {
			        if ($this->config->item('holidays'))
			            $type_array['7_'.$index] = "$teetime_label Holiday";
				}
				else
				    if ($price_category_labels[$this->session->userdata('teesheet_id')]->$price_category != '')
		                $type_array[$i.'_'.$index] = $teetime_label.' '.$price_category_labels[$this->session->userdata('teesheet_id')]->$price_category;
			}
		}
        return $type_array;
    }
	function get_cart_types($teetime_index, $teesheet_id, $both = true, $all_rates = false)
    {
    	$no_weekend = (!$this->config->item('weekend_fri') && !$this->config->item('weekend_sat') && !$this->config->item('weekend_sun'));

    	if ($this->config->item('simulator'))
		{
			/*$ttime_array = array('1'=>"30 Min");
			if ($teetime_index == 2)
		    	$ttime_array = array('2'=>"1 Hour");
			else if ($teetime_index == 3)
				$ttime_array = array('3'=>'1 Hour 30 Min');
			else if ($teetime_index == 4)
				$ttime_array = array('4'=>'2 Hours');
			else if ($teetime_index == 5)
		    	$ttime_array = array('5'=>"2 Hours 30 Min");
			else if ($teetime_index == 6)
				$ttime_array = array('6'=>'3 Hours');
			else if ($teetime_index == 7)
				$ttime_array = array('7'=>'3 Hours 30 Min');
			else if ($teetime_index == 8)
				$ttime_array = array('8'=>'4 Hours');*/
		}
		else
		{
			if ($no_weekend)
		    {
				if ($all_rates)
				{
			    	$ttime_array = array('5'=>"18 - Cart", '1'=>'9 - Cart','6'=>'18 - Cart', '2'=>'9 - Cart');
					if ($teetime_index == 1)
				    	$ttime_array = array('1'=>"9 - Cart", '5'=>'18 - Cart','2'=>'9 - Cart', '6'=>'18 - Cart');
			    }
				else if ($both)
				{
			    	$ttime_array = array('5'=>"18 - Cart", '1'=>'9 - Cart');
					if ($teetime_index == 1)
				    	$ttime_array = array('1'=>"9 - Cart", '5'=>'18 - Cart');
			    }
				else
				{
					$ttime_array = array('1'=>'9 - Cart');
					if ($teetime_index == 5)
				    	$ttime_array = array('5'=>'18 - Cart');
				}
			}
			else
			{
				if ($all_rates)
				{
			    	$ttime_array = array('5'=>"18 - Weekday Cart", '1'=>'9 - Weekday Cart','6'=>'18 - Weekend Cart', '2'=>'9 - Weekend Cart');
					if ($teetime_index == 1)
				    	$ttime_array = array('1'=>"9 - Weekday Cart", '5'=>'18 - Weekday Cart','2'=>'9 - Weekend Cart', '6'=>'18 - Weekend Cart');
			    	if ($teetime_index == 6)
						$ttime_array = array('6'=>'18 - Weekend Cart', '2'=>'9 - Weekend Cart','5'=>"18 - Weekday Cart", '1'=>'9 - Weekday Cart');
			    	if ($teetime_index == 2)
						$ttime_array = array('2'=>'9 - Weekend Cart', '6'=>'18 - Weekend Cart','1'=>"9 - Weekday Cart", '5'=>'18 - Weekday Cart');
				}
				else if ($both)
				{
			    	$ttime_array = array('5'=>"18 - Weekday Cart", '1'=>'9 - Weekday Cart');
					if ($teetime_index == 1)
				    	$ttime_array = array('1'=>"9 - Weekday Cart", '5'=>'18 - Weekday Cart');
			    	if ($teetime_index == 6)
						$ttime_array = array('6'=>'18 - Weekend Cart', '2'=>'9 - Weekend Cart');
			    	if ($teetime_index == 2)
						$ttime_array = array('2'=>'9 - Weekend Cart', '6'=>'18 - Weekend Cart');
				}
				else
				{
					$ttime_array = array('1'=>'9 - Weekday Cart');
					if ($teetime_index == 2)
				    	$ttime_array = array('2'=>'9 - Weekend Cart');
					if ($teetime_index == 5)
				    	$ttime_array = array('5'=>'18 - Weekday Cart');
					if ($teetime_index == 6)
				    	$ttime_array = array('6'=>'18 - Weekend Cart');
				}

			}
			$price_category_labels = $this->get_type_info('', $teesheet_id);
			$price_category_prices = $this->get_info($this->session->userdata('course_id').'_'.$teetime_index, $teesheet_id);
			//print_r($price_category_prices);
	        $type_array = array();
			foreach ($ttime_array as $index => $teetime_label) {
				for ($i = 1; $i <=50; $i++) {
					$price_category = 'price_category_'.$i;
					//if ((float)$price_category_prices[$teesheet_id][$this->session->userdata('course_id').'_'.$teetime_index]->$price_category > 0)
					{
					    if ($i == 1)
						    $type_array['1_'.$index] = "$teetime_label Regular";
						else if ($i == 2) {
							if ($this->config->item('early_bird_hours_begin') != '2399')
					            $type_array['2_'.$index] = "$teetime_label Early Bird";
						}
						else if ($i == 3) {
					        if ($this->config->item('morning_hours_begin') != '2399')
					            $type_array['3_'.$index] = "$teetime_label Morning";
						}
						else if ($i == 4) {
							if ($this->config->item('afternoon_hours_begin') != '2399')
					            $type_array['4_'.$index] = "$teetime_label Afternoon";
						}
						else if ($i == 5) {
					        if ($this->config->item('twilight_hour') != '2399')
					            $type_array['5_'.$index] = "$teetime_label Twilight";
						}
						else if ($i == 6) {
							if ($this->config->item('super_twilight_hour') != '2399')
					            $type_array['6_'.$index] = "$teetime_label Super Twilight";
						}
						else if ($i == 7) {
					        if ($this->config->item('holidays'))
					            $type_array['7_'.$index] = "$teetime_label Holiday";
						}
						else
						    if ($price_category_labels[$this->session->userdata('teesheet_id')]->$price_category != '')
				                $type_array[$i.'_'.$index] = $teetime_label.' '.$price_category_labels[$this->session->userdata('teesheet_id')]->$price_category;
				    }
				}
			}
		}
        return $type_array;
    }

    function save_colors($colors = null){
		if(empty($colors) || !is_array($colors)){
			return false;
		}

		$sql = "INSERT INTO foreup_price_colors (course_id, price_category, color) VALUES ";
		foreach($colors as $price_category_id => $color){
			$sql .= "(".(int) $this->session->userdata('course_id').",".(int) $price_category_id.",".$this->db->escape($color)."),";
		}
		$sql = trim($sql,',');
		$sql .= " ON DUPLICATE KEY
			UPDATE color = VALUES(color)";

		return $this->db->query($sql);
	}

    function get_colors(){
        $course_ids = array();
        $this->load->model('course');
        $this->course->get_linked_course_ids($course_ids, 'shared_price_classes', $this->session->userdata('course_id'));

        if($this->config->item('seasonal_pricing') == 1){
			$this->db->select('color, class_id AS price_category');
			$this->db->from('price_classes');
			$this->db->where_in('course_id', $course_ids);
		}else{
			$this->db->select('color, price_category');
			$this->db->from('price_colors');
            $this->db->where_in('course_id', $course_ids);
        }
		$query = $this->db->get();
		$rows = $query->result_array();

		$data = array();
		if(!empty($rows)){
			foreach($rows as $row){
				if(empty($row['color'])){ continue; }
				$data[$row['price_category']] = $row['color'];
			}
		}

		return $data;
	}

	/**
	 * @param $teesheet_id
	 * @param $price_params
	 * @param $type_array
	 * @return mixed
	 */
	private function get_price_classes_w_payments($teesheet_id, $price_params)
	{
		$this->load->model('Price_class');
		$type_array = [];

		if(!isset($price_params['date'])){
			$price_params['date'] = "";
		}
		if(!isset($price_params['start_time'])){
			$price_params['start_time'] = "";
		}
		if(!isset($price_params['holes'])){
			$price_params['holes'] = 18;
		}
		if(!isset($price_params['carts'])){
			$price_params['carts'] = 0;
		}

		$classes = $this->get_price_class_price_details($teesheet_id,$price_params['date'], $price_params['start_time']);

		// Organize price classes into array for dropdown menu
		foreach ($classes as $class) {
			$price_info = '';
			if (!empty($price_params)) {

				if ($price_params['holes'] == 18) {
					$price = $class['pricing']['18_hole_price'];
					if ($price_params['carts']) {
						$price +=  $class['pricing']['18_cart_price'];
					}
					$price_info = ' - $' . number_format($price, 2);
				} else {
					$price = $class['pricing']['9_hole_price'];
					if ($price_params['carts']) {
						$price += $class['pricing']['9_cart_price'];
					}
					$price_info = ' - $' . number_format($price, 2);
				}
			}
			$type_array[$class['class_id']] = $class['name'] . $price_info;
		}
		return $type_array;
	}

	public function get_price_class_price_details($teesheet_id,$date,$start_time,$include_taxes = false)
	{
		$this->load->model('Price_class');
		$this->load->model('Online_Booking');
		$classes = $this->Price_class->get_all();


		if($include_taxes ){
			$teesheet = $this->teesheet->get_info($teesheet_id);



			$fee_rows = $this->Pricing->get_fee_item_ids();
			$green_fee_tax_included = false;
			$cart_fee_tax_included = false;
			if(!empty($fee_rows)) {
				foreach ($fee_rows as $row) {
					if ($row['item_number'] == 1 || $row['item_number'] == 2) {
						$green_fee_tax_included = $row['unit_price_includes_tax'];
					}
					if ($row['item_number'] == 3 || $row['item_number'] == 4) {
						$cart_fee_tax_included = $row['unit_price_includes_tax'];
					}
				}
			}
		}



		foreach ($classes as &$class) {
			$timeframe = $this->Pricing->get_price_timeframe($teesheet_id, $class['class_id'],$date, $start_time);
			if(!empty($timeframe)){
				$class['pricing'] = [
					"18_hole_price"=>$timeframe['price1'],
					"18_cart_price"=>$timeframe['price3'],
					"9_hole_price"=>$timeframe['price2'],
					"9_cart_price"=>$timeframe['price4']
				];


				if($include_taxes){
					$green_taxes = [
						[
							'percent' => $this->Online_Booking->get_tax_rate($teesheet->course_id, 'green_fee')
						]
					];
					$cart_taxes = [
						[
							'percent' => $this->Online_Booking->get_tax_rate($teesheet->course_id, 'cart_fee')
						]
					];
					$class['pricing']['cart_fee_tax_included'] = $cart_fee_tax_included == 1;
					$class['pricing']['green_fee_tax_included'] = $green_fee_tax_included == 1;
					$class['pricing']['18_hole_tax'] = $this->cart_lib->calculate_tax($class['pricing']['18_hole_price'], $green_taxes);
					$class['pricing']['18_cart_tax'] = $this->cart_lib->calculate_tax($class['pricing']['18_cart_price'], $cart_taxes);
					$class['pricing']['9_hole_tax'] = $this->cart_lib->calculate_tax($class['pricing']['9_hole_price'], $green_taxes);
					$class['pricing']['9_cart_tax'] = $this->cart_lib->calculate_tax($class['pricing']['9_cart_price'], $cart_taxes);
				}
			}
		}
		return $classes;
	}
}
?>
