<?php
class Terminal extends CI_Model
{
	function get_info($terminal_id)
	{
		$this->db->from('terminals');
		$this->db->where('terminal_id', $terminal_id);
		$this->db->limit(1);
		
		return $this->db->get()->result_array();
	}
	function get_all($course_id = false) {
		$course_id = $course_id ? $course_id : $this->session->userdata('course_id');

		$this->db->select('terminal_id, label, quickbutton_tab, auto_print_receipts, 
			receipt_ip, webprnt, receipt_ip, hot_webprnt_ip, cold_webprnt_ip, 
			persistent_logs, use_register_log, cash_register, print_tip_line, 
			signature_slip_count, credit_card_receipt_count, 
			non_credit_card_receipt_count,
			mercury_id, mercury_password,
			ets_key,
			element_account_id, element_account_token, element_application_id, element_acceptor_id,
			apriva_username, apriva_password, apriva_product, apriva_key,
			e2e_account_id, e2e_account_key,
			multi_cash_drawers,
			after_sale_load, return_policy, auto_email_receipt, auto_email_no_print,
			require_signature_member_payments, cash_drawer_number');
		$this->db->from('terminals');
		$this->db->where('course_id', $course_id);
		return $this->db->get();
	}
	function save(&$terminal_info, $terminal_id = false) {
		if (!$terminal_id || $terminal_id == -1)
		{
			$terminal_info['course_id'] = $this->session->userdata('course_id');
			$success = $this->db->insert('terminals', $terminal_info);
			$terminal_info['terminal_id'] = $this->db->insert_id();
			return $success;
		}
		else 
		{
			$this->db->where('terminal_id', $terminal_id);
			return $this->db->update('terminals', $terminal_info);
		}
	}
	function delete($terminal_id) {
		$this->db->where('terminal_id', $terminal_id);
		return $this->db->delete('terminals');
	}
}
?>
