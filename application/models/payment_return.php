<?php
class Payment_return extends CI_Model
{
    function save($data)
    {
        $data['date'] = gmdate('Y-m-d H:i:s');
        $data['course_id'] = $this->config->item('course_id');
        $this->db->insert('payment_return_log', $data);
    }

}