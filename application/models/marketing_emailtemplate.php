<?php
/*
 * A template is just a json object that represents an email
 */
class Marketing_Emailtemplate extends CI_Model
{
	public function __construct()
	{
		parent::__construct();


		$this->load->model('course');
		$this->load->model('Marketing_Campaign');
	}

	/*
	 * Take a stylesheet and render it with a saved template
	 *
	 * $stylesheet
	 *
	 */
	public function render($campaign_id,$stylesheet)
	{
		if(!$this->Marketing_Campaign->get_info($campaign_id))
			return false;

		$client = JonnyW\PhantomJs\Client::getInstance();
		$client->setBinDir('application/bin');
		$client->addOption('--load-images=true');
		$client->addOption('--ignore-ssl-errors=true');

		$request  = $client->getMessageFactory()->createRequest();
		$request->setMethod('GET');
		$request->setTimeout(2500);
		$serverName = base_url();

		$CI =& get_instance();
		$access_key= $CI->config->item('campaigns')['phantomJS_apikey'];
		$previewUrl = $serverName.'index.php/api/emailcampaign/phantomJS?campaign_id='.$campaign_id.'&api_key='.$access_key.'#/template_builder_compiled';
		$request->setUrl($previewUrl);

		$response = $client->getMessageFactory()->createResponse();
		$client->send($request, $response);

		$count = 0;
		while($response->getStatus() == 408){
			$count++;
			$client->send($request, $response);
			if($count>=5){
				die("Preview request timed out, try again.");
			}
		}
		//Retry if the domain name didn't work, this may not work if the website is not hosted on 127.0.0.1
		if($response->getStatus() !== 200){
			log_message("info","Request to $previewUrl failed.  Make sure the server has access to this url. ".  $response->getStatus()." ".$response->getContent());
			$previewUrl = 'http://127.0.0.1/index.php/api/emailcampaign/phantomJS?campaign_id='.$campaign_id.'&api_key=QtiKViKZK7apPI9X97d1#/template_builder_compiled';
			$request->setUrl($previewUrl);
			$client->send($request, $response);
		}

		if($response->getStatus() === 200) {


			$content = $response->getContent();
			log_message("debug","Content recieved by PhantomJS - ".$content);
			//The phantomjs page has much more than we actually need.  The actual content of the email has the id 'getme'
			$html = $this->getHtmlById($content, "getme");

			log_message("debug","render() - Selected DIV - ".$html);
			$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head></head>'.$html.'</html>';

			libxml_use_internal_errors(true);
			$dom = new DOMDocument;
			$dom->loadHTML('<?xml encoding="utf-8">'.$html);
			log_message("debug","render() - Dom loaded - ".$dom->saveHTML());
			libxml_clear_errors();


			$cleanHtml = $this->cleanRenderedHtml($dom);
			//$mustacheHtml = $this->fillMustacheTags($cleanHtml,$campaign_id);
			$htmlWithStyle = $this->inlineCSSHtml($cleanHtml,$stylesheet);

			log_message("debug","render() - Returned element - ".$htmlWithStyle);
			return $htmlWithStyle;

		}
		log_message("error","Attempted to recover by queering $previewUrl.  Returned ".$response->getStatus()." ".$response->getContent());
		return false;
	}


	public function fillMustacheTags($cleanHtml,$campaign_id)
	{
		$m = new Mustache_Engine;
		return $m->render($cleanHtml, [
			"customer"=>[
				"first_name"=>"BRENDON BEEBE"
			]
		]);
	}
	/*
	 * Loads the page that phantomjs will render.  This page contains javascript and other script tags
	 * which can't be used in an email.  This page won't be used anywhere else except for by phantomjs
	 */
	public function getTemplate($template_id)
	{
		$this->db->select('*');
		$this->db->from('marketing_templates');
		$this->db->where('id', $template_id);
		$result = $this->db->get();
		return $result->result_array();
	}

	/*
	 * Save the template to the database
	 */
	public function save($template,$id)
	{
		$course_info = $this->course->get_info($this->session->userdata('course_id'));

		$data = array("template"=>json_encode($template));
		$this->db->where('id', $id);
		$this->db->where('course_id', $course_info->course_id);
		return $this->db->update('marketing_templates',$data);
	}
    public function saveEntry($template,$id = null)
    {
        if(!isset($id))
        {
            if($this->db->insert('marketing_templates',$template))
            {
                $template['id']=$this->db->insert_id();
                //An insert should return the new id if possible, is there a better way?
                return $template;
            }
        }


        return false;
    }

    public function campaignSaveAs($campaign_id, $name = "Custom")
    {
        $campaign = $this->Marketing_Campaign->get_info($campaign_id);
        $campaign_template = $this->getTemplate($campaign->marketing_template_id)[0];
        $template = [];
        $template['course_id'] = $campaign->course_id;
        $template['template']= $campaign->json_content;
        $template['style']= $campaign_template['style'];

        $template['name']= $name;
        $template['thumbnail']= $this->getScreenShot($campaign->remote_hash);
        $template['category']= "saved";

        return $this->saveEntry($template);
    }
    private function getScreenShot($hash)
    {

        $client = JonnyW\PhantomJs\Client::getInstance();
        $client->setBinDir('application/bin');
        $client->addOption('--load-images=true');
        $client->addOption('--ignore-ssl-errors=true');


        $serverName = base_url();


        $previewUrl = $serverName.'index.php/email/hostedEmail?email='.$hash;
        $request  = $client->getMessageFactory()->createCaptureRequest($previewUrl);
        $response = $client->getMessageFactory()->createResponse();

        $uploadDir = $this->config->item('campaigns')['template_screenshots']['abs'];
        $fileName = sha1($hash . time()).".png";
        $file = $uploadDir."/".$fileName;

        $request->setCaptureFile($file);
        $top    = 0;
        $left   = 200;
        $width  = 600;
        $height = 600;
        $request->setCaptureDimensions($width, $height, $top, $left);
        $request->setViewportSize(1024, 768);
        $client->send($request, $response);
        if($response->getStatus() == 200){
			$s3 = new fu\aws\s3();
			$s3->uploadToMarketingBucket($file,"templateScreenshots/".$fileName);
            return $fileName;
        }

        throw new \Guzzle\Http\Exception\ServerErrorResponseException();
    }

	/*
	 *
	 */
	public function get($id)
	{
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		//If this is coming from localhost, it shouldn't add the extra security since we trust it
		if(!$this->session->userdata('ip_address') == "127.0.0.1"){
			//$this->db->where('course_id', $course_info->course_id);
		}
		$this->db->select('*');
		$this->db->from('marketing_templates');
		$this->db->where('id', $id);
		$result = $this->db->get()->result_array();
        if(is_array($result) && count($result) > 0){
            $result = $result[0];
        } else {
            return false;
        }
		$result['template']= json_decode($result['template']);
		return $result;
	}

	function delete($template_id)
    {
        $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("id = '$template_id' $course_id");
        return $this->db->delete('marketing_templates');
    }

	public function getAllTemplates()
	{
		$templates = array();

		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$this->db->select('*');
		$this->db->from('marketing_templates');
		$this->db->where('course_id',"-1");
		$result = $this->db->get();
		$templates['default']= $result->result_array();
        foreach($templates['default'] as &$template){
            $template['thumbnail'] = "/frontend/campaign-builder/resources/img/template-images/".$template['thumbnail'];
        }


		$this->db->from('marketing_templates');
		$this->db->where('course_id',$course_info->course_id);
		$result = $this->db->get();
		$templates['saved']= $result->result_array();
        foreach($templates['saved'] as &$template){
			$CI =& get_instance();
			$relativePath = $CI->config->item('campaigns')['template_screenshots']['rel'];
            $template['thumbnail'] = $relativePath."/".$template['thumbnail'];
        }
        ///frontend/campaign-builder/resources/img/template-images/
        ///uploads/templateScreenshots

		if(!$templates)
			return false;
		return $templates;
	}

	/**
	 * The rendered html contains a bunch of extra attributes and elements that aren't useful when viewed in an email.
	 * This method helps prevent any problems that these extra methods could cause.
	 *
	 * @param $dom
	 *
	 * @return string
	 */
	private function cleanRenderedHtml($dom)
	{
		log_message("debug","Cleaning html - ".json_encode($dom));
		$serverName = base_url();
		$this->removeAngularAttributes($dom);
		$xpath = new DOMXpath($dom);
		foreach ($xpath->query('//img[@src]') as $a) {
			$src = $a->getAttribute('src');
			if(preg_match("/http|www/",$src)){
				//$a->setAttribute('src', 'http://www.site.com'.$src);
			} else {
				$serverName = rtrim($serverName,"/");
				$a->setAttribute('src', $serverName.$src);
			}
		}

		foreach ($xpath->query('//a[@href]') as $a) {
			$href = $a->getAttribute('href');
			if(preg_match("/http|www/",$href)){
				//$a->setAttribute('src', 'http://www.site.com'.$src);
			} else if(preg_match("/com/",$href)){
				$a->setAttribute('href', 'http://'.$href);
			} else {
				$serverName = rtrim($serverName,"/");
				$a->setAttribute('href', $serverName.$href);
			}
		}

		log_message("debug","Cleaned html - ".json_encode($dom));
		$cleanedHtml = $this->removeMiscElements($dom);
		return $cleanedHtml;
	}
	private function inlineCSSHtml($html,$cssFile)
	{
		$cssToInlineStyles = new TijsVerkoyen\CssToInlineStyles\CssToInlineStyles();
		$cssToInlineStyles->setHTML($html);
		log_message("debug","inlineCSSHtml() - HTML used used - ".$html);
		$css = file_get_contents($cssFile);
		log_message("debug","inlineCSSHtml() - CSS used - ".$css);
		$cssToInlineStyles->setCSS($css);

		$inlinedHtml = $cssToInlineStyles->convert();
		log_message("debug","inlineCSSHtml() - Returned element - ".$inlinedHtml);
		return $inlinedHtml;
	}
	private function getHtmlById($html, $elem_id)
	{
		$doc = new DOMDocument;

		libxml_use_internal_errors(true); //Don't report html errors to the php log
		$doc->loadHTML($html);
		libxml_clear_errors();//Clear all the php syntax warnings


		$html5 = Sunra\PhpSimple\HtmlDomParser::str_get_html($html);
		$element = $html5->find("*[@id='getme']", 0);


		return $element->outertext;
	}

	/**
	 * @param $dom
	 */
	private function removeAngularAttributes($dom)
	{
		$xpath = new DOMXPath($dom);

		$nodes = $xpath->query('//*[@*]');
		foreach ($nodes as $node) {
			$node->removeAttribute('ng-click');
			$node->removeAttribute('ng-repeat');
			$node->removeAttribute('ng-scope');
			$node->removeAttribute('ng-valid');
			$node->removeAttribute('ng-class');
			$node->removeAttribute('ng-show');
			$node->removeAttribute('ng-if');
			$node->removeAttribute('ng-model');
			$node->removeAttribute('ng-style');
			$node->removeAttribute('ng-href');
			$node->removeAttribute('ng-src');
            $node->removeAttribute('sv-root');
            $node->removeAttribute('sv-part');
            $node->removeAttribute('sv-element');
		}
	}

	/**
	 * @param $dom
	 * @return mixed
	 */
	private function removeMiscElements($dom)
	{
		$html5 = Sunra\PhpSimple\HtmlDomParser::str_get_html($dom->saveHTML());
		$elementsToRemove = array(
			'div[sv-helper]',
			'div[sv-placeholder]',
			'.cb-medium-placeholder',
			'.cb-section-placeholder',
			'.cb-spacer-handle',
            'component-controls',
            '.border-leg',
			'.image-dropzone'
		);
		$string = implode(',', $elementsToRemove);
		$elements = $html5->find($string);
		foreach ($elements as $element) {
			$element->outertext = '';
		}

		$elements = $html5->find("component-text");
		foreach ($elements as $element) {
			$inner = $element->innertext;
			$element->outertext = $inner;
		}

        $elements = $html5->find("component-image");
        foreach ($elements as $element) {
            $inner = $element->innertext;
            $element->outertext = $inner;
        }

        $elements = $html5->find("component-logo");
        foreach ($elements as $element) {
            $inner = $element->innertext;
            $element->outertext = $inner;
        }

        $elements = $html5->find("component-divider");
        foreach ($elements as $element) {
            $inner = $element->innertext;
            $element->outertext = $inner;
        }

        $elements = $html5->find("component-spacer");
        foreach ($elements as $element) {
            $inner = $element->innertext;
            $element->outertext = $inner;
        }

        $elements = $html5->find("component-button");
        foreach ($elements as $element) {
            $inner = $element->innertext;
            $element->outertext = $inner;
        }

        $elements = $html5->find(".cb-element");
        foreach ($elements as $element) {
            $inner = $element->innertext;
            $element->outertext = $inner;
        }

		$cleanedHtml = $html5->save();
		return $cleanedHtml;
	}
}