<?php

class Marketing_Texting extends CI_Model
{
	function __construct()
	{
		parent::__construct();

        $this->load->model('Course');
        $this->load->model('Customer');
	}

    /**
     * Definition of texting_status:
     *
     *   'new' -> Customers who have not been invited to text marketing.
     *   'invited_not_subscribed' -> Invited, but haven't subscribed.
     *   'subscribed' -> Subscribed to text marketing.
     *   'unsubscribed' -> Texted STOP or were unsubscribed by the course.
     */

    /**
     * Returns a list of the course's customers with the given subscription status.
     *
     * @param int $course_id                Required.
     * @param string $texting_status   Required.
     * @return mixed
     */
    public function getAllBySubscriptionStatus($course_id, $texting_status)
    {
        $query = $this->db->get_where (
            'foreup_marketing_texting',
            array (
                'course_id' => $course_id,
                'texting_status' => $texting_status
            )
        );
        return $query->result_array();
    }

    /**
     * Returns all of the rows that have 'valid_phone' = 0.
     *
     * @param $course_id
     * @return mixed
     */
    public function getInvalidPhones($course_id)
    {
        $query = $this->db->get_where (
            'foreup_marketing_texting',
             array (
                 'course_id' => $course_id,
                 'valid_phone' => 0
             )
        );
        return $query->result_array();
    }

    /**
     * For updating subscription status when customers text a keyword to the marketing shortcode.
     * The script that handles subscription requests forwarded from Nexmo should call this
     * function to update subscription status.
     *
     * @param string $msisdn
     * @param string $keyword
     * @param string $texting_status
     */
    public function updateSubscriptionByMsisdn($msisdn, $keyword, $texting_status,$course_id = null)
    {
        $subscribe = array (
            'texting_status' => $texting_status
        );
        $this->db->where('msisdn', $msisdn);
        $this->db->where('keyword', $keyword);
        if(isset($course_id)){
            $this->db->where('course_id', $course_id);
        }
        $this->db->update('foreup_marketing_texting', $subscribe);
    }

    /**
     * For updating subscription status in the database from somewhere within the foreUP system
     * where it's easier to specify a row by person_id and course_id.
     *
     * @param int $person_id
     * @param int $course_id
     * @param string $texting_status
     */
    public function updateSubscriptionById($person_id, $course_id, $texting_status)
    {
        $unsubscribe = array (
            'texting_status' => $texting_status
        );

		$course_ids = [];
	    $this->course->get_linked_course_ids($course_ids, 'shared_customers');
        $this->db->where_in('course_id', $course_ids);
	    $this->db->where('person_id', $person_id);
        $this->db->update('foreup_marketing_texting', $unsubscribe);
    }

    public function updateMsisdn($person_id, $course_id,$msisdn)
    {
	    $msisdnMaker = new \fu\marketing\texting\MsisdnMaker();
	    $msisdnMaker->set($msisdn);

	    $isValid = $msisdnMaker->isValid();

	    $unsubscribe = array (
		    'valid_phone' => $isValid,
		    'msisdn' => $msisdnMaker->get()
	    );

	    $this->db->where('person_id', $person_id);
	    $this->db->where('course_id', $course_id);
	    $results = $this->db->from("marketing_texting")->get()->row();
	    if(empty($results)){
	    	$this->db->insert("marketing_texting",[
	    	    "person_id"=>$person_id,
	    	    "msisdn"=>$unsubscribe['msisdn'],
	    	    "course_id"=>$course_id,
	    	    "valid_phone"=>$unsubscribe['valid_phone'],
			    "texting_status"=>"new",
	    	    "keyword"=>"GOLF"
		    ]);
	    }

	    $this->db->where('person_id', $person_id);
	    $rows = $this->db->update('foreup_marketing_texting', $unsubscribe);
    }

    /**
     * Inserts a single customer's info into the texting table.
     *
     * @param $customer_data
     */
    public function insertCustomer($customer_data)
    {
        $columns = array_keys($customer_data);
        $values = array_values($customer_data);
        $updateString = [];
        foreach($customer_data as $key => $update){
            $updateString[] = "$key = \"$update\"";
        }
        $updateString = implode(",",$updateString);

        $this->db->query("INSERT INTO foreup_marketing_texting (".implode(',',$columns).")
        VALUES (\"".implode('", "',$values)."\")
        ON DUPLICATE KEY
          UPDATE $updateString
        ");
    }

    /**
     * Returns a list of all the rows with the given MSISDN. For checking to see if a customer is
     * subscribed to multiple courses' text marketing campaigns.
     *
     * @param string $msisdn
     */
    public function getRowsByMsisdn($msisdn)
    {
        $query = $this->db->get_where('foreup_marketing_texting', array('msisdn' => $msisdn));
        return $query->result_array();
    }


    public function getSubscriptionStats($individuals,$groups,$course_id)
    {
        //This has to be above everything else because in CI you can't have multiple sql calls being created.
        if (array_search(0, $groups) !== false) {
            $course_ids = array();
            $this->Course->get_linked_course_ids($course_ids, 'shared_customers',$course_id );
        }


        $select = $this->db
            ->select(array("count(distinct(foreup_people.person_id)) as count","marketing_texting.texting_status"))
            ->from('people');
        if (array_search(0, $groups) !== false || empty($groups)) {
            $course_ids[] = $course_id;
            $select->join('customers','people.person_id = customers.person_id');
            $select->where_in('customers.course_id', $course_ids);
        } else if ($groups) {
            $select->join('customer_group_members', 'customer_group_members.person_id = people.person_id', 'left');
            $select->or_where_in('customer_group_members.group_id',$groups);
        }
        if(isset($individuals) && count($individuals) > 0){
            $select->or_where_in('people.person_id',$individuals);
        }
        $this->db->join("marketing_texting","people.person_id = marketing_texting.person_id","right");
        $this->db->group_by("marketing_texting.texting_status");
        $results = $this->db->get()->result_array();
        $return = [];
        foreach($results as $result){
            $return[$result["texting_status"]] = $result['count'];
        }
        return $return;

    }

    public function enableTexting($course_id)
    {
        $data = array (
            'marketing_texting' => 1
        );
        $this->db->where('course_id', $course_id);
        $this->db->update('foreup_courses', $data);

        $this->db->insert("marketing_texting_queue",[
            "course_id"=>$course_id,
            "job_name"=>"enable_texting"
        ]);

        return true;
    }

    public function inviteNewCustomers()
    {
        //I want all customers who have no msisdn number
        //WHERE course has enabled texting
        $select = $this->readOnlyDB->select(["customers.person_id","customers.course_id","cell_phone_number","phone_number"])
            ->from("customers")
            ->join("courses","foreup_courses.course_id = customers.course_id")
            ->join("marketing_texting","foreup_marketing_texting.course_id = foreup_courses.course_id AND foreup_marketing_texting.person_id = foreup_customers.person_id","left")
            ->join("foreup_people","foreup_people.person_id = foreup_customers.person_id")
            ->where("foreup_courses.marketing_texting",1)
            ->where("foreup_marketing_texting.texting_status IS NULL")
            ->where("(foreup_people.phone_number != '' OR foreup_people.cell_phone_number != '')");
        $customers = $select->get()->result_array();

        $msisdnMaker = new \fu\marketing\texting\MsisdnMaker();
        $msisdnMaker->makeMsisdns($customers,"new");

        //For every new msisdn, send invite
        $this->readOnlyDB->select('customers.person_id, foreup_customers.course_id, msisdn, texting_status, keyword,foreup_courses.name as course_name');
        $this->readOnlyDB->from('customers');
        $this->readOnlyDB->join("courses","foreup_courses.course_id = customers.course_id","left");
        $this->readOnlyDB->join('marketing_texting','customers.person_id = marketing_texting.person_id AND foreup_customers.course_id = foreup_marketing_texting.course_id');
        $this->readOnlyDB->group_by("msisdn");
        $this->readOnlyDB->where('marketing_texting.texting_status',"new");
        $customers = $this->readOnlyDB->get()->result_array();

        $this->load->model("Course");
        foreach($customers as $customer){
        	$course_info = $this->Course->get_info($customer['course_id']);
	        $texting_library  = new \fu\marketing\texting\Texting($course_info->course_id);
	        $texting_library->setRecipients([$customer]);
	        $texting_library->sendTextInvite(null,"GOLF");
        }

    }

	private function getCourseInfo($course_id)
	{
		if(!isset($this->courses_info[$course_id])){
			$course_info = $this->CI->Course->get_info($course_id);
			$this->courses_info[$course_id] = $course_info;
		}

		return $this->courses_info[$course_id];
	}

    public function runQueuedJobs()
    {
        $select = $this->db->select("*")
            ->from("marketing_texting_queue")
            ->where("status = 'queued'");

        $this->load->model("Marketing_Texting_Queue");
        $marketing_texting_queue = new Marketing_Texting_Queue();

        $tasks = $select->get()->result_array();
        if(isset($tasks)){
            /*
             * TODO: If number of texting jobs increases this should be refactored
             */
            foreach($tasks as $task){
                $this->db->where("id",$task['id']);
                $this->db->update("marketing_texting_queue",["status"=>"finished"]);
                if($task['job_name']=="enable_texting"){

                    //Create MSISDN for everyone
                    //Get all customers
                    $this->db->select('customers.person_id, phone_number, cell_phone_number, course_id, course_id');
                    $this->db->from('customers');
                    $this->db->join('people','customers.person_id = people.person_id');
                    $this->db->where('course_id',$task['course_id']);
                    $customers = $this->db->get()->result_array();

                    $msisdnMaker = new \fu\marketing\texting\MsisdnMaker();
                    $msisdnMaker->makeMsisdns($customers,"not-invited");

                } elseif ($task['job_name'] == "teeoff" ){


                    $task['parameters'] = json_decode($task['parameters']);
                    if(!isset($task['parameters']->customer_id)){
                        $marketing_texting_queue->update($task['id'],[
                            "log"=>"Missing required fields"
                        ],true);
                        continue;
                    }
                    $texting = new \fu\marketing\texting\Texting($task['course_id']);
                    $loaded = $texting->loadRecipient($task['parameters']->customer_id);
                    $errors = $texting->sendAlert("Your teetime is ready please go to the first hole.");
                } elseif ($task['job_name'] == "teetime_reminder" ){


                    $task['parameters'] = json_decode($task['parameters']);
                    if(!isset($task['parameters']->customer_id) || !isset($task['parameters']->teetime_id)){
                        return $marketing_texting_queue->update($task['id'],[
                            "log"=>"Missing required fields"
                        ],true);
                        continue;
                    }

                    $teetime = new Teetime();
                    $teetime_info = $teetime->get_info($task['parameters']->teetime_id);
                    $teetime_start = new \Carbon\Carbon($teetime_info->start_datetime);
                    $diff = $teetime_start->diffForHumans();

                    $course = new Course();
                    $course_info = $course->get_info($task['course_id']);

                    if(isset($task['parameters']->campaign_id) AND !empty($task['parameters']->campaign_id)){
                        $campaign_model = new Marketing_campaign();
                        $campaign = $campaign_model->get_info($task['parameters']->campaign_id,true);
                        $content = $campaign->content;
                        if(empty($content)){
                            error_log("Attempting to send text reminder but unable to retrieve any content.  Campaign_id = {$task['parameters']->campaign_id}");
                            return false;
                        }
                    } else {
                        $content = "Your {$course_info->name} teetime is about $diff on {$teetime_start->toDayDateTimeString()}";

                    }

                    $content .= " rply STOP to unsubscribe.";

                    $texting = new \fu\marketing\texting\Texting($task['course_id']);
                    if($texting->loadRecipient($task['parameters']->customer_id)){
                        $texting->sendAlert($content);

                    }
                }
            }
        }
    }


    public function reInviteUser($customer_id,$course_id)
    {
        //If current status is not subscribe than remove marketing record so invites will resend
        $subscribe = array (
            'texting_status' => "new"
        );
        $this->db->where('person_id', $customer_id);
        $this->db->where('course_id', $course_id);
        $this->db->update('foreup_marketing_texting', $subscribe);
    }

    public function logEvent($course_id,$number,$type)
    {
        $data = [
            "course_id"=>$course_id,
            "to"=>$number,
            "type"=>$type
        ];

        $this->db_datastore = $this->load->database('datastore', TRUE);

        $this->db_datastore
            ->insert("sms_logs",$data);
    }
}
