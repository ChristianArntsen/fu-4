<?php
class Increment_adjustment extends CI_Model
{
    public $slot_times = array();
    private $slot_time_date = '';
    private $slot_tee_sheet_id = '';
    private $include_squeezes = '';

    function get_all($course_ids = false, $sort_by_time = false, $tee_sheet_id = false) {
        $this->db->from('increment_adjustments');
        if ($course_ids) {
            $this->db->where_in('course_id', $course_ids);
        }
        if ($tee_sheet_id) {
            $this->db->where('tee_sheet_id', $tee_sheet_id);
        }
        if ($sort_by_time) {
            $this->db->order_by('start_time, start_date, increment_adjustment_id');
        } else {
            $this->db->order_by('start_date, increment_adjustment_id');
        }
        $results = $this->db->get()->result_array();

        return $results;
    }

    function get_slot_time_info($tee_sheet_id){
        $this->db->select('
            t.increment,
            CASE WHEN t.book_sunrise_sunset = 1 THEN \'0000\' ELSE c.open_time END as open_time,
            CASE WHEN t.book_sunrise_sunset = 1 THEN \'2400\' ELSE c.close_time END as close_time
        ', false);
        $this->db->from('teesheet as t');
        $this->db->join('courses as c', 'c.course_id = t.course_id', 'left');
        $this->db->where('teesheet_id', $tee_sheet_id);
        $result = $this->db->get()->row_array();


        return $result;
    }
    function generate_slot_times($date, $tee_sheet_id, $include_squeezes = 0, $limit_close = false) {
        $this->load->model('teetime');
        $this->slot_time_date = $date;
        $this->slot_tee_sheet_id = $tee_sheet_id;
        $this->slot_times = array();
        $this->include_squeezes = $include_squeezes;

        $increments = $this->get($tee_sheet_id, $date);
        $squeezes = [];
        if ($include_squeezes) {
            $this->load->model('Squeeze');
            $squeezes = $this->Squeeze->get_json_list(true);
        }
        $course_sheet_info = $this->get_slot_time_info($tee_sheet_id);
        $current_increment = $default_increment = $course_sheet_info['increment'];
        $unrounded_current_time = $current_time = $start_of_day = $course_sheet_info['open_time'];
        $current_end_time = 2400;
        $end_of_day = $course_sheet_info['close_time'];
        if (!$limit_close) {
            $end_of_day = $end_of_day + 300;
        }
        $seven_eight_adjuster = 0;

        while ($current_time < $end_of_day) {
            $this->slot_times[] = (int)$current_time;

            // Get increment info
            if (!empty($increments[0]) && (int)$increments[0]['start_time'] <= (int)$current_time) {
                $current_increment = $increments[0]['increment'];
                $current_end_time = $increments[0]['end_time'];
                array_shift($increments);
            }
            else if ((int)$current_end_time <= (int)$current_time) {
                $current_increment = $default_increment;
                $current_end_time = 2400;
            }

            if (!empty($squeezes[$this->slot_tee_sheet_id]) &&
                !empty($squeezes[$this->slot_tee_sheet_id][$this->slot_time_date]) &&
                !empty($squeezes[$this->slot_tee_sheet_id][$this->slot_time_date][(int)$current_time])) {
                // Clean up time (In case of 7.5 split)
                if ($current_increment == '7.5' && $seven_eight_adjuster == 0) {
                    // This enables the 7.5 to alternate between 7 and 8 minute increments
                    $seven_eight_adjuster = 15 - (($current_time % 100) % 15);
                }
                else if ($current_increment != '7.5') {
                    $seven_eight_adjuster = 0;
                }
                $squeeze_unrounded_current_time = $unrounded_current_time + $current_increment / 2;
                // Add and adjust to current time
                $squeeze_time = floor($squeeze_unrounded_current_time);
                if ($squeeze_time%100 >= 60) {
                    $squeeze_time += 40;
                }
                $this->slot_times[] = (int)$squeeze_time;
            }

            // Clean up time (In case of 7.5 split)
            if ($current_increment == '7.5' && $seven_eight_adjuster == 0) {
                // This enables the 7.5 to alternate between 7 and 8 minute increments
                $seven_eight_adjuster = 15 - (($current_time % 100) % 15);
            }
            else if ($current_increment != '7.5') {
                $seven_eight_adjuster = 0;
            }
            $unrounded_current_time += $current_increment;
            // Add and adjust to current time
            $current_time = floor($unrounded_current_time);
            if ($current_time%100 >= 60) {
                $current_time += 40;
            }
            if ($unrounded_current_time%100 >= 60) {
                $unrounded_current_time += 40;
            }
            if (empty($current_increment)) {
                return;
            }
        }
    }

    function get_slot_time($slot_date_time, $tee_sheet_id, $slots_after = 0, $include_squeezes = 0){
        $timestamp = strtotime($slot_date_time);
        $date = date('Y-m-d', $timestamp);
        $slot_time = date("Hi", $timestamp);
        if ($this->slot_time_date != $date || $this->slot_tee_sheet_id != $tee_sheet_id || $this->include_squeezes != $include_squeezes){
            $this->generate_slot_times($date, $tee_sheet_id, $include_squeezes);
        }
        foreach ($this->slot_times as $slot_index => $time) {
            if ((int)$slot_time <= (int)$time && !empty($this->slot_times[$slot_index + $slots_after])) {
                $hz = $this->slot_times[$slot_index + $slots_after] < 1000 ? '0' : '';
                return (str_replace('-', '', $date) - 100).$hz.$this->slot_times[$slot_index + $slots_after];
            }
        }

        return $slot_date_time - 1000000;
    }

    function get($tee_sheet_id, $date) {
        $formatted_date = date('0000-m-d', strtotime($date));
        $day_of_week = strtolower(date('l', strtotime($date)));

        $results = $this->db->query("SELECT * FROM foreup_increment_adjustments
            WHERE tee_sheet_id = {$tee_sheet_id}
            AND ((start_date <= '$formatted_date' AND end_date >= '$formatted_date' OR
                (end_date < start_date AND (start_date <= '$formatted_date' OR end_date >= '$formatted_date'))))
            AND $day_of_week = 1
            ORDER BY start_time
        ")->result_array();

        return $results;
    }

    function get_increment_array($date, $tee_sheet_id, $include_squeezes = 0) {
        $open_time = $this->config->item('open_time');
        $close_time = $this->config->item('close_time');
        $this->generate_slot_times($date, $tee_sheet_id, $include_squeezes);
        $final_increment_array = array();
        foreach ($this->slot_times as $slot) {
            if ($slot >= $open_time && $slot < $close_time) {
                $hz = $slot < 1000 ? '0' : '';
                $final_increment_array[$slot] = date('g:ia', strtotime('20120101' . $hz . $slot));
            }
        }

        return $final_increment_array;
    }

    public function get_json_list($tee_sheet_id = false) {
        $this->load->model('course');
        $course_ids = array();
        $this->course->get_linked_course_ids($course_ids, 'shared_tee_sheet', $this->config->item('course_id'));

        $increments = $this->get_all($course_ids, true);
        $result = array();

        foreach ($increments as $increment) {
            $result[$increment['tee_sheet_id']][] = $increment;
        }

        return json_encode($result);
    }

    function save($data, $increment_adjustment_id = false) {
        if ($increment_adjustment_id) {
            $this->db->where('increment_adjustment_id', $increment_adjustment_id);
            $this->db->where('course_id', $this->config->item('course_id'));
            $success = $this->db->update('increment_adjustments', $data);
        }
        else {
            $success = $this->db->insert('increment_adjustments', $data);
            $increment_adjustment_id = $this->db->insert_id();
        }
        $data['increment_adjustment_id'] = $increment_adjustment_id;

        return array('success' => $success, 'data' => $data);
    }

    function delete($increment_adjustment_id) {
        $this->db->delete('increment_adjustments', array('course_id' => $this->config->item('course_id'), 'increment_adjustment_id' => $increment_adjustment_id));
    }

    function delete_all($exclusions, $tee_sheet_id) {
        $this->db->where_not_in('increment_adjustment_id', $exclusions);
        $this->db->where('tee_sheet_id', $tee_sheet_id);
        $this->db->delete('increment_adjustments', array('course_id' => $this->config->item('course_id')));
    }

}