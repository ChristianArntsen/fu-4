<?php
class Pass extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->library('fu/passes/Pass');
		$this->total = 0;
		$this->teetime_data = false;
	}

	function get($params = array()){

		$params = elements(array('customer_id', 'search', 'limit', 'offset', 'sort', 'order', 'pass_id', 'multi_customer', 'course_id'), $params, null);

        if(!empty($params['course_id'])){
            $course_id = (int) $params['course_id'];
        }else{
            $course_id = $this->session->userdata('course_id');
        }

        if(empty($course_id)){
			return [];
		}

        if ($params['customer_id'] == -1) {
            return [];
        }

        if($params['multi_customer'] !== null){
        	$params['multi_customer'] = (int) (bool) $params['multi_customer'];
       	}

		if(empty($params['limit']) || $params['limit'] > 100){
			$params['limit'] = 100;
		}

		if(empty($params['offset'])){
			$params['offset'] = 0;
		}

		if($params['order'] != 'asc' && $params['order'] != 'desc'){
			$params['order'] = 'DESC';
		}

		if(!in_array($params['sort'], ['start_date', 'end_date', 'name', 'customer', 'date_created', 'price_class_name'])){
			$params['sort'] = null;
		}
		if($params['sort'] == 'customer'){
			$params['sort'] = 'person.first_name';
		}

		$expired_query = "";
		$days_to_show_expired =  $this->config->item('days_to_show_expired_pass');
		if(!empty($days_to_show_expired)){
			$expired_query = " OR DATE_ADD(pass.end_date, INTERVAL $days_to_show_expired DAY) > NOW()";
		}

		$this->load->model('course');
		$course_ids = array();
		$this->course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);
		if(empty($course_ids)){
			return [];
		}
		$this->db->select('SQL_CALC_FOUND_ROWS pass.*, item.name, definition.*, price.name AS price_class_name, 
			GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				person.person_id,
				person.first_name,
				person.last_name
			) SEPARATOR 0x1E) AS customers,
			definition.price_class_id AS default_price_class_id', false)
			->from('passes AS pass')
			->join('items AS item', 'item.item_id = pass.pass_item_id', 'left')
			->join('pass_definitions AS definition', 'pass.pass_item_id = definition.item_id', 'left')
			->join('pass_customers AS pass_customer', 'pass_customer.pass_id = pass.pass_id', 'left')
			->join('customers AS customer', 'customer.person_id = pass_customer.person_id AND customer.course_id IN('.implode(',',$course_ids).')', 'left')
			->join('people AS person', 'person.person_id = customer.person_id', 'left')
			->join('price_classes AS price', 'price.class_id = definition.price_class_id', 'left')
			->where_in('pass.course_id', $course_ids)
			->where('pass.deleted', 0)
			->where("((pass.end_date = '0000-00-00 00:00:00') $expired_query)")
			->group_by('pass.pass_id');

		if(!empty($params['search'])){
			$query_escaped = $this->db->escape_like_str($params['search']);
			$this->db->where("(
				item.name LIKE '%".$query_escaped."%' OR
				(
					(
						person.first_name LIKE '%".$query_escaped."%'
						OR person.last_name LIKE '%".$query_escaped."%'
					)
					AND person.person_id = pass.customer_id
				)
			)");
		}

		if($params['multi_customer'] !== null){
			$this->db->where('definition.multi_customer', $params['multi_customer']);
		}

		if(!empty($params['customer_id'])){
			$this->db->where('customer.person_id', (int) $params['customer_id']);
			$this->db->where_in('customer.course_id', $course_ids);
		}

		if(!empty($params['sort'])){
			$this->db->order_by($params['sort'], strtoupper($params['order']));
		}

		if(!empty($params['pass_id'])){
			$ids = [];
			if(is_array($params['pass_id'])){
				foreach($params['pass_id'] as $person_id){
					$ids[] = (int) $person_id;
				}
				$this->db->where_in('pass.pass_id', array_unique($ids));

			}else{
				$this->db->where('pass.pass_id', (int) $params['pass_id']);
				$ids = [(int) $params['pass_id']];
			}
		}

		$this->db->limit($params['limit'], $params['offset']);
		$rows = $this->db->get()->result_array();

		$this->db->select('FOUND_ROWS() AS total', false);
		$row = $this->db->get()->row_array();
		$this->total = (int) $row['total'];

		$pass_ids = [];
		foreach($rows as $row){
			$pass_ids[] = (int) $row['pass_id'];
		}
		
		$history = $this->get_history($pass_ids);

		$passes = [];
		foreach($rows as $key => $row){
			
			unset($row[0]);
			$row['restrictions'] = json_decode($row['restrictions'], true);
			$row['message'] = null;
			$row['uses'] = [];

			// Make sure restrictions are sorted by rule number ascending
			if(!empty($row['restrictions'])){
				usort($row['restrictions'], function($a, $b){
					if($a['rule_number'] == $b['rule_number']){ 
						return 0; 
					}
					if($a['rule_number'] > $b['rule_number']){
						return 1;
					}
					return -1;			
				});				
			}


			$row['times_used'] = 0;
			if(!empty($history[$row['pass_id']])){
				$row['uses'] = $history[$row['pass_id']];
				foreach($history[$row['pass_id']] as $use) {
					$row['times_used'] += $use['count'];
				}
			}



			if(!empty($row['customers'])){
				$row['customers'] = $this->get_customer_array($row['customers'], $row['customer_id']);

				foreach($row['customers'] as $key => $customer){
					if($customer['person_id'] == $row['customer_id']){
						$row['customer'] = $customer;
						unset($row['customers'][$key]);
					}
				}
				$row['customers'] = array_values($row['customers']);
			}
			$this->apply_pass_details($row);

			if(!empty($history[$row['pass_id']])){
				$rule_number_map = [];
				foreach($row['rules'] as $key=>$rule){
					$rule_number_map[$rule['rule_number']]=$key;
				}
				foreach($history[$row['pass_id']] as $use) {

					if(!isset($use['rule_number']))
						continue;
					$rule_number = $use['rule_number'];
					if($rule_number == 0 && count($row['rules']) > 0){
						$rule_number = count($row['rules'])-1;
					}

					if(!isset($rule_number_map[$rule_number])){
						continue;
					}
					$rule_key = $rule_number_map[$rule_number];
					if(!isset($row['rules'][$rule_key]))
						continue;
					if(!isset($row['rules'][$rule_key]['times_used'])){
						$row['rules'][$rule_key]['times_used'] = 0;
					}

					$row['rules'][$rule_key]['times_used']++;


					//If date_created < release date, note as innaccurate
					if(\Carbon\Carbon::parse("2017-01-25") > \Carbon\Carbon::parse($use['date_created']) ){
						$row['rules'][$rule_key]['accurate'] = false;
					}
				}
			}



			unset($row['restrictions']);

			$passes[$row['pass_id']] = $row;
			unset($rows[$key]);
		}
		
		return $passes;
	}

	private function get_customer_array($customer_field = null){
		
		$customer_list = [];
		if(empty($customer_field)){
			return $customer_list;
		}
		$customers = explode("\x1E", $customer_field);

		$customer_list = [];
		foreach($customers as $customer){
			
			$customer_array = explode("\x1F", $customer);
			if(empty($customer_array[0])){
				continue;
			}
			$customer_list[] = [
				'person_id' => (int) $customer_array[0],
				'first_name' => $customer_array[1],
				'last_name' => $customer_array[2]
			];
		}

		return $customer_list;		
	}

	function apply_pass_details(&$row){
		
		// Check pass rule validity
		$pass = new \fu\passes\Pass($row);
		$pass->setCurrentUse($this->teetime_data);

		$row['rules'] = $pass->getRuleValidity();
		$row['is_valid'] = $pass->isValid();
	}

	// Customer may have several active passes, return the first valid
	// pass attached to customer
	function get_customer_default($customer_id, $teetime_data = false){

		if(empty($customer_id)){
			return false;
		}
		$this->teetime_data = $teetime_data;
		$passes = $this->get(array('customer_id' => $customer_id));

		foreach($passes as $pass){
			if($pass['is_valid']){
				return $pass;
			}
		}

		return false;
	}

	function save($pass_id = null, $data){

		$response = false;
		$data = elements(array('customer_id', 'start_date', 'end_date', 
			'is_valid', 'pass_item_id', 'course_id', 'customers'), $data, null);

		if(empty($data['course_id'])){
			$data['course_id'] = (int) $this->session->userdata('course_id');
		}

		$data['start_date'] = date('Y-m-d H:i:s', strtotime($data['start_date']));
		$data['end_date'] = date('Y-m-d H:i:s', strtotime($data['end_date']));
		$customers = $data['customers'];
		unset($data['customers']);

		$customer_ids = [];
		if(!empty($customers)){
			foreach($customers as $customer){
				$customer_ids[] = (int) $customer['person_id'];
			}		
		}

		if(empty($data['customer_id'])){
			return false;
		}
		$customer_ids[] = (int) $data['customer_id'];

		foreach($data as $key => $val){
			if($val === null){
				unset($data[$key]);
			}
		}

		if(empty($pass_id)){

			if(empty($data['customer_id']) || empty($data['pass_item_id'])){
				return false;
			}

			$pass_definition = $this->db->from("pass_definitions")
				->where("item_id",$data['pass_item_id'])
				->get()->row();

			if($pass_definition->enroll_loyalty)
			{
				$this->load->model("customer");
				$this->customer->enable_loyalty($data['customer_id']);
			}


			$data['customer_id'] = (int) $data['customer_id'];
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$existing_pass_customers = $this->delete_existing_pass($data['customer_id'], $data['pass_item_id']);
			
			$this->db->insert('passes', $data);
			$pass_id = (int) $this->db->insert_id();
		
		}else{
			$this->db->update('passes', $data, array('pass_id' => (int) $pass_id));
			$response = true;
		}

		$this->save_customers($pass_id, $customer_ids);
		return $pass_id;
	}

	function save_customers($pass_id, $customers = []){

		$this->db->where_not_in('person_id', $customers);
		$this->db->delete('pass_customers', ['pass_id' => $pass_id]);

		if(!empty($customers)){
			foreach($customers as $person_id){
				if(empty($person_id)){
					continue;
				}
				$this->db->query("INSERT IGNORE INTO foreup_pass_customers (person_id, pass_id, date_added)
					VALUES (".(int) $person_id.", ".(int) $pass_id.", CURDATE())");
			}
		}

		return true;
	}

	// If customer purchases a new pass (renew), make sure existing passes of 
	// the same type are deleted
	function delete_existing_pass($customer_id, $pass_item_id){
		
		$row = $this->db->select('passes.pass_id, GROUP_CONCAT(`foreup_pass_customers`.person_id) AS customers')
			->from('passes')
			->join('pass_customers', 'pass_customers.pass_id = passes.pass_id', 'left')
			->where('passes.customer_id', $customer_id)
			->where('passes.pass_item_id', $pass_item_id)
			->where('passes.deleted', 0)
			->get()->row_array();

		if(empty($row)){
			return false;
		}

		$customers = [];
		if(!empty($row['customers'])){
			$customers = explode(',', $row['customers']);
			array_unique($customers);
		}

		$this->db->update('passes', array('deleted' => 1), array('pass_id' => $row['pass_id']));
		return $customers;
	}
	
	function delete($pass_id){
		
		$this->db->update('passes', array('deleted' => 1), array('pass_id' => $pass_id));
		return $this->db->affected_rows();	
	}

	function add_history($pass_id, $type, $data){

		$data = elements(array('teetime_id', 'sale_id', 'count', 'price_class_id','rule_number','rule_id'), $data, null);

		if(empty($pass_id) || empty($type)){
			return false;
		}

		if(empty($data['count'])){
			$data['count'] = 1;
		}

		$data['date_created'] = date('Y-m-d H:i:s');
		$data['pass_id'] = (int) $pass_id;
		$data['type'] = $type;

		$this->db->insert('pass_history', $data);
		$history_id = $this->db->insert_id();
		
		return $history_id;
	}

	function delete_history($pass_id, $type, $params){

		$params = elements(array('teetime_id', 'sale_id'), $params, null);
		if(empty($pass_id) || empty($type)){
			return false;
		}

		if(empty($params['teetime_id']) && empty($params['sale_id'])){
			return false;
		}

		$params['pass_id'] = $pass_id;
		$params['type'] = $type;

		$this->db->delete('pass_history', $params);
		return $this->db->affected_rows();
	}

	function get_history($pass_id){

		if(empty($pass_id)){
			return [];
		}

		if(!is_array($pass_id)){
			$pass_id = [(int) $pass_id];
		}

		$rows = $this->db->select('history.*, teetime.teesheet_id, history.date_created AS date')
			->from('pass_history AS history')
			->join('teetime AS teetime', 'teetime.TTID = history.teetime_id', 'left')
			->where_in('history.pass_id', $pass_id)
			->get()->result_array();

		if(empty($rows)){
			return [];
		}

		$rowsByPass = [];
		foreach($rows as $key => $row){
			$rowsByPass[$row['pass_id']][] = $row;
			unset($rows[$key]);
		}

		return $rowsByPass;
	}
}