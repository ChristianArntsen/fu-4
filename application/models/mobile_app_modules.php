<?php
class Mobile_app_modules extends CI_Model
{
    public function get_all() {
        $this->db->from('mobile_app_modules');
        return $this->db->get()->result_array();
    }
}