<?php
class Invoice extends CI_Model
{
	function __construct(){
		$this->load->model('Account_transactions');
		$this->load->model('sale');
		$this->load->model('Customer_billing');
		$this->load->model('Payment');		
		$this->load->library('sale_lib');
		$this->load->library('zip');
		$this->ServiceFee = new \fu\service_fees\ServiceFee();
	}

	/*
	Determines if a given item_id is an item
	*/
	function exists($invoice_id)
	{
		$this->db->from('invoices');
		$this->db->where("invoice_id", $invoice_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the items
	*/
	function get_all($customer_id = false, $limit=10000, $offset=0, $join = false)
	{
		$this->db->from('invoices');
		//$this->db->order_by("name", "asc");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('invoices.deleted !=', 1);
		if ($join)
			$this->db->join('people', 'people.person_id = invoices.person_id');
		if ($customer_id)
			$this->db->where('invoices.person_id', $customer_id);
		$this->db->offset($offset);
		$this->db->limit($limit);
		$this->db->order_by('date DESC');
		$this->db->order_by('invoice_id DESC');
		$all = $this->db->get();

		return $all;
	}
	function get_one($invoice_id)
	{
		$this->db->from('invoices');
		//$this->db->order_by("name", "asc");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('invoices.deleted !=', 1);
		$this->db->join('people', 'people.person_id = invoices.person_id');
		$this->db->where('invoices.invoice_id', $invoice_id);
		$this->db->limit(1);
	
		return $this->db->get()->row();
	}
	function get_specific_invoices($course_id, $start, $end, $beginning_number, $ending_number)
	{
		$this->db->from('invoices');
		$this->db->where('course_id', $course_id);
		$this->db->where('invoices.deleted !=', 1);
		if ($start)
			$this->db->where("(date >= '$start 00:00:00' AND date <= '$end 23:59:59')");
		if ($beginning_number)
			$this->db->where("(invoice_number >= $beginning_number AND invoice_number <= $ending_number)");
		// if ($join)
			// $this->db->join('people', 'people.person_id = invoices.person_id');
		// if ($customer_id)
			// $this->db->where('invoices.person_id', $customer_id);
		$this->db->order_by('invoice_number ASC');		
		$all = $this->db->get();
		//echo $this->db->last_query();
		return $all;
	}
	function count_all()
	{
		$this->db->from('invoices');
        $this->db->where('course_id', $this->config->item('course_id'));
        $this->db->where('deleted', 0);
		return $this->db->count_all_results();
	}
	function get_course_payments($course_id)
	{
		$this->db->from('invoice_items');
		$this->db->join('invoices', 'invoice_items.charge_id = invoices.charge_id');
		$this->db->join('customer_credit_cards', 'customer_credit_cards.credit_card_id = invoices.credit_card_id');
		$this->db->where('customer_credit_cards.course_id', $course_id);
		$this->db->order_by('date DESC, invoice_items.charge_id');
		//$result = $this->db->get();
		//echo $this->db->last_query();
		return $this->db->get()->result_array();
	}

	// Apply payment to an invoice (called from Sale model), if payment
	// is over invoice total, go back and pay overdue invoices with excess
	function pay_invoice($sale_id, $invoice_id, $employee_id = false, $payment = 0, $pay_oldest_first = false)
	{
        if($payment == 0){
			return false;
		}

        $this->db->select('total, paid, person_id, employee_id');
        $this->db->from('invoices');
        $this->db->where('invoice_id', $invoice_id);
        $row = $this->db->get()->row_array();

        $amount_due = (float)($row['total'] - $row['paid']);
        $customer_id = $row['person_id'];
        $course_id = (int)$this->session->userdata('course_id');

        if (empty($employee_id)) {
            $employee_id = (int)$row['employee_id'];
        }

        $excess_payment = 0;
        if (!$pay_oldest_first) {
            if ($payment > $amount_due) {
                $excess_payment = (float)($payment - $amount_due);
            }

            if ($excess_payment > 0) {
                $payment_applied = $amount_due;
            } else {
                $payment_applied = $payment;
            }

            // Apply total invoice payment to general "invoice" account
            // REDUNDANT... REMOVED 3/26/14 by JOEL HOPKINS
            //$this->Account_transactions->save('invoice', $customer_id, 'Invoice Payment', $payment, lang('sales_point_of_sale').' '.$sale_id, $sale_id, $invoice_id, $employee_id);

            // Update invoice with new paid amount
            $this->save(array('paid' => $row['paid'] + $payment_applied), $invoice_id);
        }
		else {
            $excess_payment = $payment;
        }
		// If payment applied is greater than invoice amount due,
		// retrieve overdue invoices to apply excess payment to
		if($excess_payment > 0){

			// Retrieve overdue invoices from oldest to newest
			$this->db->select('invoice_id, total, paid');
			$this->db->from('invoices');
			$this->db->where('total - paid > 0');
			$this->db->where('deleted', 0);
			$this->db->where('person_id', $customer_id);
			$this->db->where('course_id', $course_id);
			$this->db->order_by('date ASC');
			$overdue_invoices = $this->db->get()->result_array();

			// Loop through each overdue invoice and apply payment to as many as possible
			foreach($overdue_invoices as $overdue_invoice){
				$due = (float) ($overdue_invoice['total'] - $overdue_invoice['paid']);
				$overdue_invoice_id = (int) $overdue_invoice['invoice_id'];

				if($excess_payment >= $due){
					$overdue_payment = $due;
				}else{
					$overdue_payment = $excess_payment;
				}

				// Update overdue invoice with new paid amount
				$this->db->update('invoices', array('paid' => $overdue_invoice['paid'] + $overdue_payment), array('invoice_id' => $overdue_invoice_id));
				$excess_payment -= $overdue_payment;

				// If we are out of excess payment to apply, exit the loop
				if($excess_payment <= 0){
					break;
				}
			}
		}

		return true;
	}

	/*
	Gets invoice by course_id and invoice number
	*/
	function get_by_invoice_number($invoice_number)
	{
		$this->db->select('invoice_id');
		$this->db->from('invoices');
		$this->db->where('invoice_number', $invoice_number);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		$row = $this->db->get()->row_array();
		$invoice_id = $row['invoice_id'];

		return $this->get_details($invoice_id);
	}
	/*
	Gets information about a particular item
	*/
	function get_info($invoice_id)
	{
		$this->db->from('invoices');
		$this->db->where("invoice_id", $invoice_id);
		$this->db->limit(1);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->result_array();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('invoices');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return (array) $item_obj;
		}
	}

	function has_access($invoice_id, $person_id){
		
		$this->db->select('invoice_id');
		$this->db->from('invoices');
		$this->db->where(array('invoice_id' => (int) $invoice_id, 'person_id' => (int) $person_id));
		$row = $this->db->get()->row_array();
		
		if(!empty($row['invoice_id'])){
			return true;
		}else{
			return false;
		}
	}

	function get_details($invoice_id){
		if(empty((int)$invoice_id)){
			return false;
		}
		$invoice_data = array();

		$this->db->select('invoice.*');
		$this->db->from('invoices AS invoice');
		$this->db->where('invoice.invoice_id', (int) $invoice_id);
		$invoice_data = $this->db->get();
//        echo $this->db->last_query();
//        echo '---------';
//        echo $this->db->error_message();
//        echo '---------';
        $invoice_data = $invoice_data->row_array();

		$date = $invoice_data['date'];
		$bill_start = $invoice_data['bill_start'];
		$bill_end = $invoice_data['bill_end'];

		// Retrieve customer information
		if(!empty($invoice_data['person_id'])){
			$invoice_data['person_info'] = $this->Customer->get_info($invoice_data['person_id']);
		}

		// Get course information
		$this->db->select('name, address, city, state, state_name, postal,
			woeid, zip, country, phone, email, company_logo, member_balance_nickname, customer_credit_nickname');
		$this->db->from('courses');
		$this->db->where('course_id', $invoice_data['course_id']);
		$invoice_data['course_info'] = $this->db->get()->row();

		$total_due = $invoice_data['total'];
		$subtotal = 0.0;
		$tax = 0.0;
		$current_total = 0.0;

		$this->db->select('taxable, member_account_balance, account_balance');
		$this->db->from('customers');
		$this->db->where('person_id', (int) $invoice_data['person_id']);
		$this->db->where('deleted', 0);
		$member_info = $this->db->get()->row_array();

		$invoice_data['member_nickname'] = ($invoice_data['course_info']->member_balance_nickname == '' ? lang('customers_member_account_balance'):$invoice_data['course_info']->member_balance_nickname);
		$invoice_data['customer_credit_nickname'] = ($invoice_data['course_info']->customer_credit_nickname == '' ? lang('customers_account_balance'):$invoice_data['course_info']->customer_credit_nickname);
		// Loop through invoice items to seperate out taxes
		$invoice_data['current_items'] = $this->get_items($invoice_id);
		$CI =& get_instance();
		$CI->load->model('Item');
		$ItemTax = new fu\items\ItemTax();
        foreach($invoice_data['current_items'] as $item){
            if(empty($item['item_id'])) {
                $item_tax = $item['tax'];
            }
            else {
                $item_info = $CI->Item->get_info($item['item_id']);
                $item_tax = $ItemTax->calculate_item_tax($item_info->unit_price_includes_tax?$item['total']:$item['subtotal'],$item['item_id'],'item', $invoice_data['course_id']);
            }
			$subtotal += (float) $item['subtotal'];
            if((isset($item_info->force_tax) && $item_info->force_tax==='1')||($item_info->force_tax !==0 && $invoice_data['person_info']->taxable==='1'))
			   $tax += (float) $item_tax;
			if(isset($item['service_fees'])) {
				foreach ($item['service_fees'] as $line => $invoice_item) {
					$sf_item = $this->process_invoice_item($invoice_data['course_id'], $invoice_id, $line, $invoice_item, $member_info, $invoice_data['person_info']->taxable, false);
					//$sale_total += $item['total'];
					$subtotal += $sf_item['subtotal'];
					$tax += $sf_item['tax'];
					$sf_item['item_type'] = 'service_fee';
					$sale_items[] = $sf_item;
				}
			}
		}
		$current_total = $subtotal + $tax;

		$invoice_data['current_total'] = $current_total;
		$invoice_data['current_subtotal'] = $subtotal;
		$invoice_data['current_tax'] = $tax;

		// Format dates
		$invoice_data['date'] = date('m/d/Y', strtotime($invoice_data['date']));
		if(empty($invoice_data['due_date']) || $invoice_data['due_date'] == '0000-00-00'){
			$invoice_data['due_date'] = 'n/a';
		}else{
			$invoice_data['due_date'] = date('m/d/Y', strtotime($invoice_data['due_date']));
		}
		
		$invoice_data['account_transactions'] = array('customer'=>array(),'member'=>array(),'invoice'=>array());
		
		// Retrieve account transactions (if they should be displayed on invoice)
		if($invoice_data['show_account_transactions'] == 1){

			$account_filter = array();
			$account_filter[] = 'invoice';
			if($invoice_data['pay_customer_account'] == 1){
				$account_filter[] = 'customer';
			}
			if($invoice_data['pay_member_account'] == 1){
				$account_filter[] = 'member';
			}

			// Retrieve transactions for customer,member and invoice accounts
			if(!empty($account_filter)){
				
				$filters = array(
					'date_start' => $bill_start, 
					'date_end' => $bill_end, 
					'exclude_invoice_id' => $invoice_id, 
					'hide_positive' => false, 
					'hide_balance_transfers' => true,
					'only_sales' => true
				);		
				
				// If invoice was manually created, and should show account transactions, show them from begining of time
				if($invoice_data['billing_id'] == 0){
					unset($filters['date_start']);
				}
				
				$transactions = $this->Account_transactions->get_transactions(
					$account_filter,
					$invoice_data['person_id'],
					$filters,
					$invoice_data['course_id']
				);
                $sale_ids = [];
				foreach ($transactions as $transaction) {
                    if (!empty($transaction['sale_id'])) {
                        $sale_ids[] = $transaction['sale_id'];
                    }
					$invoice_data['account_transactions'][$transaction['account_type']][] = $transaction;
				}
                if ($this->config->item('show_invoice_sale_items') && !empty($sale_ids)) {
                    $invoice_sales_items = $this->Sale->get_invoice_sale_items($sale_ids);
                    $invoice_data['sales_items'] = $this->format_sales_items($invoice_sales_items);

                }
            }
		}
		$invoice_data['customer_current_charges'] = $current_total;
		$invoice_data['customer_overdue_charges'] = abs($invoice_data['overdue']);
		$invoice_data['customer_total_due'] = $current_total + $invoice_data['customer_overdue_charges'] - $invoice_data['paid'];

		return $invoice_data;
	}

    function format_sales_items($sales_items) {
        $sales_item_array = [];
        foreach($sales_items as $sale_item) {
            if (!isset($sales_item_array[$sale_item['sale_id']])) {
                $sales_item_array[$sale_item['sale_id']] = [];
            }
            $sales_item_array[$sale_item['sale_id']][] = $sale_item;
        }
        return $sales_item_array;
    }

	/*
		Inserts new or updates existing invoice
	*/
	function save($invoice_data, $invoice_id = false,$test = false)
	{
		// I don't like the current_time parameter after the test parameter
		// TODO: find where this is invoked, and make test the last parameter
		$success = false;
		$invoice_response = false;

		// If creating a new invoice
		if (!$invoice_id or !$this->exists($invoice_id))
		{
			if(empty($invoice_data['items'])){
				return false;
			}
			$invoice_items = $invoice_data['items'];
			unset($invoice_data['items']);
			
			if(empty($invoice_data['date'])){
				$invoice_data['date'] = date('Y-m-d H:i:s');
			}
			
			if(empty($invoice_data['bill_start'])){
				$invoice_data['bill_start'] = date('Y-m-d 00:00:00', strtotime($invoice_data['date']));
			}

			// If no bill end date is selected, use the date being created as bill end date
			if(empty($invoice_data['bill_end']) || $invoice_data['bill_end'] == '0000-00-00 00:00:00'){
				$invoice_data['bill_end'] = date('Y-m-d 23:59:59', strtotime($invoice_data['date']));
			}

			// Retrieve outstanding invoice total (for overdue amount)
			$invoice_data['overdue'] = $this->Account_transactions->get_total('invoice', $invoice_data['person_id'], array(
				'date_end' => $invoice_data['bill_end']
			), $invoice_data['course_id']);
			
			// If invoice account is actually positive, reset it to 0 (no amount due)
			if($invoice_data['overdue'] > 0){
				$invoice_data['overdue'] = 0;
			}		

			// Get/set the next invoice_number
			$this->db->select_max('invoice_number');
			$this->db->from('invoices');
			$this->db->where('course_id', $invoice_data['course_id']);
			$result = $this->db->get()->row_array();
			$invoice_data['invoice_number'] = (int) $result['invoice_number'] + 1;

			$this->db->trans_start($test);

			// Insert new invoice into database
			if($this->db->insert('invoices', $invoice_data)){
				$invoice_id = $this->db->insert_id();
				$success = $invoice_id;
			}
			
			// Create a new sale with invoice items
			$sale = $this->save_items($invoice_items, $invoice_id, $invoice_data['person_id'], $invoice_data['employee_id'], $invoice_data['course_id'], $invoice_data['date'],$test);
			$invoice_total = $sale['total'];
			$sale_id = (int) $sale['sale_id'];

            $sale_number = (int) $sale['sale_number'];
			if (!$sale_id OR $sale_id == -1) {
				$failure_data = array(
					'invoice_items'=>$invoice_items,
					'invoice_id'=>$invoice_id,
					'invoice_data'=>$invoice_data
				);
				send_sendgrid('jhopkins@foreup.com', 'Failed Invoice Generation', $this->load->view("invoices/failed",$failure_data, true), 'error@foreup.com', 'ForeUP Invoice Failure');
			}

			$paid = 0;
			$partial_payment = 0;
			$total_due = $invoice_total;
			// If there is a positive balance on the customer balance or member account and we have those marked to pay off, then we'll use those funds to pay down the invoice
			// Check invoice account first (This isn't actually paying off with the invoice account, it's checking to see if it already paid off the invoice)
			$notes = '';
			$course_info = $this->Course->get_info($invoice_data['course_id']);
			if ($total_due > 0) {
				$customer = $this->Customer->get_info($invoice_data['person_id'], $invoice_data['course_id']);
				if ($customer->invoice_balance > -$total_due) {
					$partial_payment = $total_due + $customer->invoice_balance;
					if ($partial_payment > $total_due) {
						$partial_payment = $total_due;
					}
					$notes .= ("Invoice Balance: ".$customer->invoice_balance);
				}
				$total_due = $total_due - $partial_payment;
				$paid = $paid + $partial_payment;
			}
			// Then check Member Account
			$partial_payment = 0;
			if ($total_due > 0) {
				if (isset($invoice_data['pay_customer_account']) && $invoice_data['pay_member_account'] === 1 && $customer->member_account_balance > 0)	{
					if ($customer->member_account_balance > $total_due) {
						$partial_payment = $total_due;
					}
					else {
						$partial_payment = $customer->member_account_balance;
					} 
					// Deduct paid amount from account
					$member_nickname = ($course_info->member_balance_nickname == '' ? lang('customers_member_account_balance'):$course_info->member_balance_nickname);
					$this->Account_transactions->save('member', $invoice_data['person_id'], 'Invoiced Balance Transfer', -$partial_payment, lang('sales_point_of_sale').' hi '.$sale_number, $sale_id, $invoice_id, $invoice_data['employee_id'], $invoice_data['course_id'], false, false, 1);
					$this->Account_transactions->save('invoice', $invoice_data['person_id'], 'Invoice Payment - '.$member_nickname, $partial_payment, lang('sales_point_of_sale').' hi '.$sale_number, $sale_id, $invoice_id, $invoice_data['employee_id'], $invoice_data['course_id'], false, false, 1);
					
					if ($notes != '') {$notes .= '<br/>';};
					$notes .= "$member_nickname Balance: ".($customer->member_account_balance - $partial_payment);
				}
				$total_due = $total_due - $partial_payment;
				$paid = $paid + $partial_payment;
			}			
			// Then check Customer Credit
			$partial_payment = 0;
			if ($total_due > 0) {
				if (isset($invoice_data['pay_customer_account']) && $invoice_data['pay_customer_account'] === 1 && $customer->account_balance > 0)	{
					if ($customer->account_balance > $total_due) {
						$partial_payment = $total_due;
					}
					else {
						$partial_payment = $customer->account_balance;
					} 
					// Deduct paid amount from account
					$customer_credit_nickname = ($course_info->customer_credit_nickname == '' ? lang('customers_account_balance'):$course_info->customer_credit_nickname);
					$this->Account_transactions->save('customer', $invoice_data['person_id'], 'Invoiced Balance Transfer', -$partial_payment, lang('sales_point_of_sale').' hi '.$sale_number, $sale_id, $invoice_id, $invoice_data['employee_id'], $invoice_data['course_id'], false, false, 1);
					$this->Account_transactions->save('invoice', $invoice_data['person_id'], 'Invoice Payment - '.$customer_credit_nickname, $partial_payment, lang('sales_point_of_sale').' hi '.$sale_number, $sale_id, $invoice_id, $invoice_data['employee_id'], $invoice_data['course_id'], false, false, 1);

					if ($notes != '') {$notes .= '<br/>';};
					$notes .= "$customer_credit_nickname Balance: ".($customer->account_balance - $partial_payment);
				}
				$total_due = $total_due - $partial_payment;
				$paid = $paid + $partial_payment;
			}			
						
			// Update invoice with sale total
			$this->db->where('invoice_id', $invoice_id);
			$this->db->update('invoices', array(
				'total' => $invoice_total,
				'paid'	=> $paid,
				'sale_id' => $sale_id,
				'notes' => $notes
			));
			
			$invoice_response['sale_id'] = $sale_id;
			$invoice_response['invoice_id'] = $invoice_id;
			$invoice_response['total'] = $invoice_total;

		// If saving an existing invoice
		}else{
			$this->db->where('invoice_id', $invoice_id);
			$success = $this->db->update('invoices', $invoice_data);

			$invoice_response = array(
				'invoice_id' => $invoice_id
			);
		}

		if($test){
				$this->db->where('invoice_id', $invoice_id);
				$this->db->delete('invoices');
		}
		$this->db->trans_complete();

		return $invoice_response;
	}

	function record_manual_change($manual_change_data)
	{
		return $this->db->insert('invoice_changes', $manual_change_data);
	}

    function update_associated_transaction_dates($invoice_id, $date) {
        $this->db->from('sales_items');
        $this->db->where('invoice_id', $invoice_id);
        $this->db->limit(1);
        $invoice_item = $this->db->get()->row_array();
        $sale_id = $invoice_item['sale_id'];

        // Update account transactions
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('sale_id', $sale_id);
        $this->db->update('account_transactions', array('trans_date'=>$date));

        // Update sale transactions
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('sale_id', $sale_id);
        $this->db->update('sales', array('sale_time'=>$date));

        return true;
    }

	// Creates a new sale with the items
	function save_items($items, $invoice_id, $customer_id, $employee_id, $course_id, $invoice_date = false,$test = false)
	{
		if(empty($items)){
			return false;
		}

		$sale_total = 0;
		$subtotal = 0;
		$tax = 0;
		$is_taxable = true;
		$sale_items = $this->process_invoice_items($items, $invoice_id, $customer_id, $employee_id, $course_id, $sale_total,$subtotal,$tax,$is_taxable,true);

		// Invoice is automatically charged against the "invoice" account
		$payments = array();
		$payments[0] = array(
			'payment_type' => 'Invoice Charge',
			'payment_amount' => $sale_total,
			'invoice_id' => 0,
			'customer_id' => $customer_id
		);
		
		// Create new sale with the charged invoice items
        $generating_invoice = 1;
		$sale_id = $this->sale->save($sale_items, $customer_id, $employee_id, '', $payments, false, -1, $course_id, NULL, $invoice_date, '', false, 0, $is_taxable, $generating_invoice,true,$test);

		$custom_line_taxes = [];
		foreach($sale_items as $item)
		{
			// Save any custom line item tax rates
			if((int) $sale_id > 0 && !empty($item['custom_line_taxes'])){
				foreach($item['custom_line_taxes'] as $idx => $tax){
					$item['custom_line_taxes'][$idx]['sale_id'] = (int) $sale_id;
				}
				$this->db->insert_batch('sales_items_taxes', $item['custom_line_taxes']);
			}
		}

		
		return array(
			'sale_id' => $sale_id,
            'sale_number' => $this->sale->sale_number,
			'total' => $sale_total
		);
	}

	function process_invoice_items($items, $invoice_id, $customer_id, $employee_id, $course_id, &$sale_total=0, &$subtotal = 0 , &$tax = 0 ,&$is_taxable=1, $supress_service_fees = false){
		$sale_items = array();
		$tax_included = $this->config->item('unit_price_includes_tax');

		if(empty($employee_id)){
			$employee_id = 0;
		}

		// Find out if the customer is taxable
		$this->db->select('taxable, member_account_balance, account_balance');
		$this->db->from('customers');
		$this->db->where('person_id', (int) $customer_id);
		$this->db->where('deleted', 0);
		$member_info = $this->db->get()->row_array();

		$is_taxable = true;
		if(isset($member_info['taxable']) && (int) $member_info['taxable'] == 0){
			$is_taxable = false;
		}

		// Reset order of lines so there are no gaps in line numbers
		$items = array_values($items);
		$custom_line_taxes = array();

		// Loop through invoice items and build array to be logged as sale
		$service_fee_array = array();
		foreach($items as $line => $invoice_item){

			$item = $this->process_invoice_item($course_id,$invoice_id,$line,$invoice_item,$member_info,$is_taxable,$tax_included);
			if($invoice_item['item_type']==='service_fee' && $supress_service_fees)continue;
			$sale_total += $item['total'];
			$subtotal += $item['subtotal'];
			$tax += $item['tax'];
			$service_fee_array = array_merge($service_fee_array,$item['service_fees']);
			$sale_items[] = $item;
		}
		foreach($service_fee_array as $line => $invoice_item) {
			$line = (int) $line + count($items)+1;
			$item = $invoice_item; //$this->process_invoice_item($course_id, $invoice_id, $line, $invoice_item, $member_info, $is_taxable, $tax_included);
			$sale_total += $item['total'];
			$subtotal += $item['subtotal'];
			$tax += $item['tax'];
			//if($supress_service_fees)continue;
			$item['item_type'] = 'service_fee';
			$item['invoice_id'] = $invoice_id;
			$item['type'] = 'service_fee';
			$item['line'] = $line;
			$sale_items[] = $item;
		}
		return $sale_items;
	}

	function process_invoice_item($course_id,$invoice_id,$line,$invoice_item,$row,$is_taxable,$tax_included){
		$line = $line + 1;
        if(isset($invoice_item['tax_included'])){
            $tax_included = $invoice_item['tax_included'];
        }

		// If line items are member or customer account balances
		if($invoice_item['item_type'] == 'member_balance' || $invoice_item['item_type'] == 'account_balance'){
			$item_id = $this->Sale->get_balance_item($invoice_item['item_type'], $course_id);
			if($invoice_item['item_type'] == 'member_balance'){
				$serial_number = 'MemberBalance';
			}else{
				$serial_number = 'CustomerBalance';
			}
			$invoice_item['price'] = $invoice_item['item_type'] == 'member_balance' ? -$row['member_account_balance'] : -$row['account_balance'];

			$calculated_subtotal = $this->sale_lib->calculate_subtotal($invoice_item['price'], 1, 0);

			$item = array(
				'item_id' => $item_id,
				'item_number' => $invoice_item['item_type'],
				'item_kit_id' => 0,
				'line' => $line,
				'serialnumber' => $serial_number,
				'modifiers' => null,
				'modifier_total' => 0,
				'quantity' => 1,
				'invoice_id' => $invoice_id,
				'description' => $invoice_item['description'],
				'price' => $invoice_item['price'],
				'base_price' => $invoice_item['price'],
				'subtotal' => $invoice_item['price'],
				'tax' => 0,
				'discount' => 0,
				'total' => $invoice_item['price'],
				'receipt_only' => 2
			);

		}else if(!empty($invoice_item['item_id']) && $invoice_item['item_type'] == 'item'){
			$this->load->model('Item');
			$item_info = (array) $this->Item->get_info($invoice_item['item_id']);
			$calculated_subtotal = $this->sale_lib->calculate_subtotal($invoice_item['price'], $invoice_item['quantity'], 0);
			$tax = 0;

			if(
				(isset($item_info['force_tax'])&&$item_info['force_tax']==='1') ||
				(
					(
						!isset($item_info['force_tax']) ||
						$item_info['force_tax']!=='0'
					) &&
					$is_taxable
				)
			){
				$tax = $this->sale_lib->calculate_item_tax($calculated_subtotal, $invoice_item['item_id'], $invoice_item['item_type']);
			}

			if ($tax_included) {
				// we need taxes for the case where user is not taxable
				$tmp_item = $item_info;
				$tmp_item['force_tax'] = 1;
				$tax2 = $this->sale_lib->calculate_item_tax($calculated_subtotal, $tmp_item, $invoice_item['item_type']);

				$total = $calculated_subtotal;
				$subtotal = $total - $tax2;

				// we need to remove taxes from total for the case where user is not taxable
				if(!$is_taxable && $item_info['force_tax']!=1){
					$total = $subtotal;
				}
			}
			else {
				$subtotal = $calculated_subtotal;
				$total = $tax + $subtotal;
			}
			$service_fees = $this->ServiceFee->getServiceFeesForItem($invoice_item['item_id']);
			$serialized_fees = array();
			foreach($service_fees as $fee){
				$res = $fee->calculatePrice($is_taxable,1,$invoice_item['quantity']);
				$fee = $fee->getAsArray();
				// then set price, subtotal, and tax
				$fee = array_merge($fee,$res);
				$fee['price'] = $fee['subtotal'];
				$fee['item_type'] = 'item';
				$fee['quantity'] = (float) $invoice_item['quantity'];
				$fee['is_service_fee'] = 1;
				$serialized_fees[] = $fee;
			}

			$item = array(
				'item_id' => $invoice_item['item_id'],
				'name' => isset($invoice_item['name'])&&$invoice_item['name']?$invoice_item['name']:$invoice_item['description'],
				'invoice_id' => $invoice_id,
				'modifiers' => null,
				'modifier_total' => 0,
				'line' => (int) $line,
				'quantity' => (float) $invoice_item['quantity'],
				'discount' => 0,
				'department' => $item_info['department'],
				'category' => $item_info['category'],
				'subcategory' => $item_info['subcategory'],
				'service_fees' => $serialized_fees,
				'price' => $invoice_item['price'],
				'base_price' => $invoice_item['price'],
				'subtotal' => $subtotal,
				'tax' => $tax,
				'total' => $total,
				'is_service_fee' => isset($invoice_item['is_service_fee'])?$invoice_item['is_service_fee']:0
			);

			// If line is an item kit
		}else if(!empty($invoice_item['item_id']) && $invoice_item['item_type'] == 'item_kit'){

			$item_taxes = $this->Item_kit_taxes->get_info($invoice_item['item_kit_id']);
			$subtotal = $this->sale_lib->calculate_subtotal($invoice_item['price'], $invoice_item['quantity'], 0);
			$tax = 0;
			if($is_taxable){
				$tax = $this->sale_lib->calculate_item_tax($subtotal, $invoice_item['item_id'],$invoice_item['item_type']);
			}
			$total = $tax + $subtotal;

			$item = array(
				'item_kit_id' => $invoice_item['item_id'],
				'invoice_id' => $invoice_id,
				'modifiers' => null,
				'modifier_total' => 0,
				'line' => (int) $line,
				'quantity' => (float) $invoice_item['quantity'],
				'discount' => 0,
				'price' => $invoice_item['price'],
				'base_price' => $invoice_item['price'],
				'subtotal' => $subtotal,
				'tax' => $tax,
				'total' => $total
			);

			// If line is a manually entered item
		}else{
			$calculated_subtotal = $this->sale_lib->calculate_subtotal($invoice_item['price'], $invoice_item['quantity'], 0);
			$tax = 0;
			if($is_taxable){

				if((float) $invoice_item['tax_percentage'] > 0){
					$custom_line_taxes[] = array(
						'line' => (int) $line,
						'item_id' => 0,
						'name' => 'Tax',
						'percent' => (float) $invoice_item['tax_percentage'],
						'cumulative' => 0,
						'amount'=>$invoice_item['tax_percentage']*$invoice_item['price']/100
					);
				}
				if ($tax_included) {
					$actual_price = round(($calculated_subtotal / (1 + $invoice_item['tax_percentage'] /100)), 2);
					$tax = ($calculated_subtotal - $actual_price);
				}
				else {
					$tax = round($calculated_subtotal * ($invoice_item['tax_percentage'] / 100), 2);
				}
			}
			if ($tax_included) {
				$total = $calculated_subtotal;
				$subtotal = $total - $tax;
			}
			else {
				$subtotal = $calculated_subtotal;
				$total = $tax + $subtotal;
			}

			$item = array(
				'item_id' => 0,
				'item_kit_id' => 0,
				'description' => $invoice_item['description'],
				'invoice_id' => $invoice_id,
				'modifiers' => null,
				'modifier_total' => 0,
				'line' => (int) $line,
				'quantity' => (float) $invoice_item['quantity'],
				'discount' => 0,
				'price' => $invoice_item['price'],
				'base_price' => $invoice_item['price'],
				'subtotal' => $subtotal,
				'tax' => $tax,
				'total' => $total,
				'custom_line_taxes'=>$custom_line_taxes
			);
		}
		if(isset($tax_included)){
			$item['unit_price_includes_tax']=$tax_included;
		}
		return $item;
	}

	function get_items($invoice_id)
	{
		$invoice_id = (int) $invoice_id;
		if(empty($invoice_id)){
			return [];
		}
		$query = $this->db->query("SELECT *
			FROM (
				SELECT 'item' AS type, si.item_id, si.line, si.serialnumber, IF(si.item_id = 0, si.description, i.name) AS name,
					si.description, si.quantity_purchased AS quantity,
					si.item_unit_price AS price, si.discount_percent AS discount, si.subtotal, si.tax, si.total
				FROM foreup_sales_items	AS si
				LEFT JOIN foreup_items AS i
					ON i.item_id = si.item_id
				WHERE invoice_id = {$invoice_id}
				-- and si.type != 'service_fee'
				GROUP BY si.line
				UNION ALL
				SELECT 'item_kit' AS type, si.item_kit_id, si.line, '', kit.name AS name,
					si.description, si.quantity_purchased AS quantity,
					si.item_kit_unit_price AS price, si.discount_percent AS discount, si.subtotal, si.tax, si.total
				FROM foreup_sales_item_kits AS si
				LEFT JOIN foreup_item_kits AS kit
					ON kit.item_kit_id = si.item_kit_id
				WHERE invoice_id = {$invoice_id}
				GROUP BY si.line
			) AS invoice_items
			GROUP BY invoice_items.line
			ORDER BY invoice_items.line ASC");
		$rows = $query->result_array();

		return $rows;
	}

	// Generates a new invoice from a recurring bill
	function generate_from_billing($billing_id, $date = null, $do_not_save = false){
        $this->load->model('Customer');

		if(empty($billing_id)){
			return false;
		}
		if(empty($date)){
			$date = date('Y-m-d H:i:s');
		}
		
		// Retrieve recurring bill data
		$bill_data = $this->Customer_billing->get_details($billing_id, false);
		$course_ids = array();
        $this->Customer->get_linked_course_ids($course_ids, $bill_data['course_id']);

		// When this bill was last ran, start date of this bill is the day after the last bill
        if (empty(strtotime($bill_data['last_invoice_bill_end']))) {
            $bill_start_timestamp = strtotime("-{$bill_data['frequency']} {$bill_data['frequency_period']}");
        }
        else {
            $bill_start_timestamp = strtotime($bill_data['last_invoice_bill_end']) + 86400;
        }
		$bill_start = date('Y-m-d 00:00:00', $bill_start_timestamp);
		$bill_end_timestamp = strtotime($date) - 86400;
		$bill_end = date('Y-m-d 23:59:59', $bill_end_timestamp);

		$bill_start_date = date('m/d/Y', $bill_start_timestamp);
		$bill_end_date = date('m/d/Y', $bill_end_timestamp);
		
		// Loop through billing items, if the item is to pay off balance,
		// get the total balance of account for the billing period
		foreach($bill_data['current_items'] as $key => $bill_item){
			
			if($bill_item['item_type'] != 'member_balance' && $bill_item['item_type'] != 'account_balance'){
				continue;
			}
			$current_balance_due = (float) 0;

			if($bill_item['item_type'] == 'member_balance'){
				
				// Get balance of member account as of the bill end date
				$bill_end_balance = (float) $this->Account_transactions->get_total('member', $bill_data['person_id'], array(
					'date_end' => $bill_end
				), $bill_data['course_id']);
				
				// Get current balance of member account
				$balance = $this->db->select('member_account_balance')->from('customers')->where('person_id', $bill_data['person_id'])->where_in('course_id', $course_ids)->get()->row_array();
				$current_balance = (float) $balance['member_account_balance'];
			}else if($bill_item['item_type'] == 'account_balance'){
				
				// Get balance of customer account as of the bill end date
				$bill_end_balance = (float) $this->Account_transactions->get_total('customer', $bill_data['person_id'], array(
					'date_end' => $bill_end
				), $bill_data['course_id']);
				
				// Get current balance of  customer account
				$balance = $this->db->select('account_balance')->from('customers')->where('person_id', $bill_data['person_id'])->where_in('course_id', $course_ids)->get()->row_array();
				$current_balance = (float) $balance['account_balance'];
			}
			$bill_end_credit = '';
			
			// If any balances are positive, set them to 0 (we don't need
			// to charge anything, just display the positive balance)
			if($bill_end_balance > 0){
				$bill_end_credit = '- Credit: '.to_currency(abs($bill_end_balance));
				$bill_end_balance = 0;
			}
			$bill_end_balance = abs($bill_end_balance);
			
			if($current_balance > 0){
				$current_balance = 0;
			}
			$current_balance = abs($current_balance);			
			
			// Restrict the bill end balance to be no more than 
			// what is actually owed on account as of now (in case bill 
			// is generated for past date)
			if($bill_end_balance > $current_balance){
				$bill_end_balance = $current_balance;
			}
			
			// Set price of line to reflect current balance of account
			$bill_data['current_items'][$key]['description'] = $bill_item['description']. ' '.$bill_end_credit.' (As of '.$bill_end_date.')';
			$bill_data['current_items'][$key]['price'] = $bill_end_balance;
		}

		if(empty($bill_data['due_days']) && $bill_data['due_days'] !== 0){
			$bill_data['due_days'] = 30;
		}
		$due_date = date('Y-m-d', strtotime($bill_end.' +'.($bill_data['due_days'] + 1).' days'));
		$auto_bill_date = date('Y-m-d', strtotime($bill_end.' +'.($bill_data['auto_bill_delay'] + 1).' days'));
		// Create new invoice structure
		$invoice_data = array(
			'course_id' => (int) $bill_data['course_id'],
			'bill_start' => $bill_start,
			'bill_end' => $bill_end,
			'billing_id' => $billing_id,
			'person_id' => $bill_data['person_id'],
			'employee_id' => $bill_data['employee_id'],
			'credit_card_id' => $bill_data['credit_card_id'],
			'email_invoice' => (int) $bill_data['email_invoice'],
			'show_account_transactions' => (int) $bill_data['show_account_transactions'],
			'pay_member_account' => (int) $bill_data['pay_member_balance'],
			'pay_customer_account' => (int) $bill_data['pay_account_balance'],
			'items' => $bill_data['current_items'],
			'attempt_limit' => $bill_data['attempt_limit'],
			'due_date' => $due_date,
			'date' => $date,
			'auto_bill_date' => $auto_bill_date
		);

		if ($do_not_save) {
			return $invoice_data;
		}
		else {
			$response = $this->save($invoice_data);
			return $response;
		}
	}

	/*
		Deletes one invoice
	*/
	function delete($invoice_id)
	{
		$invoice_id = (int) $invoice_id;
		if(empty($invoice_id)){
			return false;
		}
		$invoice_data = $this->db->select('paid, sale_id')
			->from('invoices')
			->where('invoice_id', $invoice_id)
			->where('course_id', $this->session->userdata('course_id'))
			->get()
			->row_array();
		$sale_id = (int) $invoice_data['sale_id'];
		if(empty($sale_id)){
			return false;
		}
		if ($invoice_data['paid'] > 0) {
            return false;
        }
		// Delete sale associated with invoice
        if ($this->Sale->is_deleted($sale_id)) {
            return true;
        }
        else {
            return $this->Sale->delete($sale_id);
        }
	}

    function process_invoice_reversal($invoice_id)
    {
        //Mark the invoice as deleted
        $this->db->where('invoice_id',$invoice_id);
        $this->db->where('course_id', $this->session->userdata('course_id'));
        return $this->db->update('invoices', array('deleted' => 1));
    }
	/*
		Deletes a list of invoices
	*/
	function delete_list($invoice_ids)
	{
        foreach ($invoice_ids as $invoice_id) {
            $this->delete($invoice_id);
        }
        return true;
 	}

	/*
		Email invoice to customer
 	*/
	function send_email($invoice_id)
	{
		$data = $this->get_details($invoice_id);

		$data['is_invoice'] = true;
		$data['sent'] = true;
		$data['popup'] = 1;
		$data['emailing_invoice'] = true;
		$email = array($data['person_info']->email, $data['person_info']->invoice_email);
		
		$success = send_sendgrid(
			$email,
			"Invoice From {$data['course_info']->name}",
			$this->load->view('customers/invoice', $data, true),
			$data['course_info']->email,
		 	$data['course_info']->name,
            0,
            false,
            $invoice_id
		);

		if ($success)
		{
			$data = array('emailed'=> 1, 'send_date'=>date('Y-m-d'));
			$this->save($data, $invoice_id);
		}

		return ($success ? true : false);
	}

	// Retrieves invoices that need emails sent out, optional filter by day
	function get_invoices_to_email($date = null, $course_id = false){

		$dateFilter = '';
		$course_sql = '';
		if(!empty($date)){
			$date = date('Y-m-d', strtotime($date));
			$dateFilter = "AND DATE(date) = ".$this->db->escape($date);
		}
		if (!empty($course_id)){
			$course_sql = "AND course_id = ".$this->db->escape($course_id);
		}

		$query = $this->db->query("SELECT course_id, invoice_id
			FROM foreup_invoices
			WHERE email_invoice = 1
				AND deleted = 0
				{$course_sql}
				AND emailed = 0");
//				{$dateFilter}");

		return $query->result_array();
	}

	// Get which courses had recurring bills that ran on a specific date
	function get_billed_courses($date, $course_id = false){

		// Default to today
		if(empty($date)){
			$date = date('Y-m-d');
		}else{
			$date = date('Y-m-d', strtotime($date));
		}
        if (!$course_id)
        {
            $course_sql = "";
        }else{
            $course_sql = "AND i.course_id = $course_id";
        }

		$date = $this->db->escape($date);

		$query = $this->db->query("SELECT i.course_id, c.name, c.email,
				c.billing_email
			FROM foreup_invoices AS i
			INNER JOIN foreup_courses AS c
				ON c.course_id = i.course_id
			WHERE (DATE(date) = {$date} OR DATE(auto_bill_date) = {$date})
				AND billing_id > 0
				$course_sql
			GROUP BY course_id");

		return $query->result_array();
	}

	function clean_filename($string) {
		$string = str_replace(' ', '_', $string); // Replaces all spaces with underscores
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars
		return preg_replace('/-+/', '-', $string); // Replaces multiple underscores with single one
	}

	// Generate PDFs from invoices stored in database
	function generate_pdfs($course_id, $filters = array()){
        echo "Memory limit ".ini_get('memory_limit').PHP_EOL;
		$course_id = (int) $course_id;
		$filter_sql = '';

		// Filter rows by specific date
		$date = date('m-d-Y');
		if(!empty($filters['date'])){
			$date = date('m-d-Y', strtotime($filters['date']));
			$sql_date = date('Y-m-d', strtotime($filters['date']));
			$filter_sql .= " AND DATE(date) = ".$this->db->escape($sql_date);
		}

		if(!empty($filters['recurring'])){
			$filter_sql .= " AND billing_id > 0";
		}

		// Filter rows by billing ID
		if(!empty($filters['billing_id'])){
			$ids = array();
			if(is_array($filters['billing_id'])){
				foreach($filters['billing_id'] as $billing_id){
					$ids[] = (int) $billing_id;
				}
			}else{
				$ids[] = (int) $filters['billing_id'];
			}
			$filter_sql .= " AND billing_id IN(".implode(',', $ids).")";
		}

		// Filter rows by Invoice ID
		if(!empty($filters['invoice_id'])){
			$ids = array();
			if(is_array($filters['invoice_id'])){
				foreach($filters['invoice_id'] as $invoice_id){
					$ids[] = (int) $invoice_id;
				}
			}else{
				$ids[] = (int) $filters['invoice_id'];
			}
			$filter_sql .= " AND invoice_id IN(".implode(',', $ids).")";
		}

		// Retrieve invoice_ids
		$query = $this->db->query("SELECT invoice_id
			FROM foreup_invoices
			WHERE deleted = 0 
				AND course_id = {$course_id}
			{$filter_sql}");
		$invoices = $query->result_array();

		if(empty($invoices)){
			return false;
		}
        echo "Invocies to run<br/>";
        // Initialize combined PDF and zip class
		$this->zip->clear_data();
        $all_pdf = null;
        $all_pdf = new \Knp\Snappy\Pdf();
        $all_pdf->setBinary("application/bin/wkhtmltopdf");
        $all_pdf->setOption("viewport-size",'1366x1024');
        $all_pdf->setOption("zoom",'.8');
        $all_pdf_html = '';

        // Loop through invoices to generate PDFs
		foreach($invoices as $invoice){
            echo 'Generating invoice PDF '.$invoice['invoice_id']."PEAK MEMORY USED: ".round(memory_get_peak_usage(true) / 1000000, 2).' Mbs'.PHP_EOL;
                echo $this->db->error_message();
			$invoice_data = $this->get_details($invoice['invoice_id']);
			$invoice_data['pdf'] = true;

			$invoice_data['popup'] = 1;
			$invoice_data['is_invoice'] = true;
			$invoice_data['sent'] = true;
			$customer = $invoice_data['person_info'];

			// Get HTML to generate PDF
			$invoice_html = $this->load->view('customers/invoice', $invoice_data, true);

			// Generate individual PDF
            $pdf = new \Knp\Snappy\Pdf();
            $pdf->setBinary("application/bin/wkhtmltopdf");
            $pdf->setOption("viewport-size",'1366x1024');
            $pdf->setOption("zoom",'.8');
            echo 'Finished PDF <br/>';
			// Add individual PDF to combiniation PDF
			$all_pdf_html .= "<page style'width:800px;'>".$invoice_html."</page>";
            echo 'Added to big PDF<br/><br/>';
			// Replace spaces with underscores and filter invalid filename characters
			$first_name = $this->clean_filename($customer->first_name);
			$last_name = $this->clean_filename($customer->last_name);

			$cc_id = '';
			if($invoice_data['credit_card_id'] > 0){
				$cc_id = '_'.$invoice_data['credit_card_id'];
			}

			// Build filename for individual PDF
			$pdf_filename = 'Invoice'.$invoice_data['invoice_number'].'_'.$first_name.'_'.$last_name . $cc_id .'_'. $date . '.pdf';

			// Add newly generated PDF to zip file
			$this->zip->add_data($pdf_filename, $pdf->getOutputFromHtml("<page style'width:800px;'>".$invoice_html."</page>"));

            $this->db->close();
			unset($invoice_html, $invoice_data, $pdf, $customer);
		}

		// Add combined PDF to zip file
		$all_pdf_filename = $date."_Combined_Invoices".".pdf";
		$this->zip->add_data($all_pdf_filename, $all_pdf->getOutputFromHtml($all_pdf_html));

		$folder_name = "archives/invoices/{$course_id}";

		// Create necessary folders to store zip file of invoices
		if (!is_dir('archives')){
			mkdir('archives', 0777);
		}
		if (!is_dir('archives/invoices')){
			mkdir('archives/invoices', 0777);
		}
		if (!is_dir($folder_name)){
			mkdir($folder_name, 0777);
		}
		$path = "{$folder_name}/invoices_".$date.".zip";

		// SAVE ZIPPED PDF INVOICES TO THE GOLF COURSE
        echo 'Saved zip<br/>';
		$this->zip->archive($path);

		echo 'Saved to AWS S3<br/>';
		$s3 = new fu\aws\s3();
        //$s3->set_is_stream(true);
		$s3->uploadToInvoiceBucket($path, $path);

		return $path;
	}

	// Retrieves invoices that have credit cards attached that need charged
	function get_invoices_to_charge($date = null, $course_id = false){

		$dateFilter = '';
		$course_sql = '';
		if(!empty($date)){
			$date = date('Y-m-d', strtotime($date));
			$dateFilter = "AND DATE(date) = ".$this->db->escape($date);
		}
		if(!empty($course_id)){
			$course_sql = "AND i.course_id = ".$this->db->escape($course_id);
		}
		$now = date('Y-m-d');

		$query = $this->db->query("SELECT invoice_id, sale_id,
				credit_card_id, i.course_id, total, paid, (total - paid) AS due
			FROM foreup_invoices AS i
			LEFT JOIN foreup_courses_information AS ci
                ON ci.course_id = i.course_id
	        WHERE deleted = 0
				AND charged = 0
				AND ((started = 0 AND last_billing_attempt = '0000-00-00')
				  OR (started < attempt_limit AND last_billing_attempt < '$now'))
				AND credit_card_id > 0
				AND credit_card_payment_id = 0
				{$dateFilter}
				{$course_sql}
				AND auto_bill_date <= '$now'
				AND total - paid > 0
				AND ci.stage IN ('live','demo')
				");

		return $query->result_array();
	}

	// Retrieves any automatic invoice credit card charges that failed
	// on a specific day for a specific course
	function get_charge_report($course_id = null, $date = null){

		if(empty($date)){
			$date = date('Y-m-d');
		}

		$dateFilter = '';
		if(!empty($date)){
			$date = date('Y-m-d', strtotime($date));
			$dateFilter = "AND DATE(i.date) = ".$this->db->escape($date);
		}

		$courseFilter = '';
		if(!empty($course_id)){
			$courseFilter = "AND i.course_id = ".(int) $course_id;
		}

		$query = $this->db->query("SELECT
			(
				SELECT COUNT(i.invoice_id)
				FROM foreup_invoices AS i
				WHERE i.started = 1
					AND i.charged = 1
					AND i.credit_card_payment_id > 0
					{$dateFilter}
					{$courseFilter}
			) AS num_success,
			(
				SELECT COUNT(i.invoice_id)
				FROM foreup_invoices AS i
				WHERE i.started = 1
					AND i.charged = 0
					AND i.total - i.paid > 0
					{$dateFilter}
					{$courseFilter}
			) AS num_failed");
		$report = $query->row_array();

		if($report['num_success'] == 0 && $report['num_failed'] == 0){
			return $report;
		}
		
		// Get list of failed credit cards/customers
		$query = $this->db->query("SELECT i.invoice_id, i.invoice_number,
				i.credit_card_id, i.course_id, i.total, i.paid,
				(i.total - i.paid) AS due, cc_payment.masked_account,
				 cc_payment.card_type, cc_payment.amount, i.person_id,
				 person.first_name, person.last_name
			FROM foreup_invoices AS i
			INNER JOIN foreup_sales_payments_credit_cards AS cc_payment
				ON cc_payment.invoice = i.credit_card_payment_id
			LEFT JOIN foreup_people AS person
				ON person.person_id = i.person_id
			WHERE i.deleted = 0
				AND (cc_payment.status = 'Declined' OR cc_payment.status = 'error')
				AND i.started = 1
				AND i.charged = 0
				AND i.last_billing_attempt != '0000-00-00'
				AND i.credit_card_id > 0
				AND i.credit_card_payment_id > 0
				{$dateFilter}
				{$courseFilter}");
				
		$report['failed_charges'] = $query->result_array();
		return $report;
	}
	function search($search, $limit=20, $month = 'all', $paid_status = 'all', $offset = 0, $customer_id = false) {
		$course_id = '';
        //if (!$this->permissions->is_super_admin())
        {
        	//$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
        	$course_ids = array();
			$this->Customer->get_linked_course_ids($course_ids);
	    	$course_ids = join(',',$course_ids);
			$course_id = "AND foreup_customers.course_id IN ({$course_ids})";
        }
       
        $this->db->from('invoices');
		//$this->db->order_by("name", "asc");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('invoices.deleted !=', 1);
		$this->db->join('people', 'people.person_id = invoices.person_id');
		if ($customer_id)
			$this->db->where('invoices.person_id', $customer_id);
		if ($paid_status == 'paid')
			$this->db->where('total - paid = 0');
		if ($paid_status == 'unpaid')
			$this->db->where('total - paid > 0');
		if ($month != 'all' && $month != '') {
			$this->db->where('date >=', date('Y-m-01 00:00:00', strtotime($month)));
			$this->db->where('date <=', date('Y-m-t 23:59:59', strtotime($month)));
		}
		$this->db->offset($offset);
		$this->db->limit($limit);
		$this->db->order_by('date DESC');
		$all = $this->db->get();
//echo $this->db->last_query();
		return $all;
	}
	function get_overdue_invoices($billing_id, $invoice_id) {
		$overdue_invoices = $this->db->from('invoices')
			->where('billing_id', $billing_id)
			->where('invoice_id !=', $invoice_id) // Make sure we don't count the primary invoice twice for any reason
			->where('deleted', 0)
			->where('total - paid > 0')
			//->where('started', 1)
			->get();
			echo $this->db->last_query();
			
		return $overdue_invoices->result_array();
	}
	function charge_invoice($invoice_id, $course_id, $credit_card_id, $amount, $terminal_id = false, $allow_pay_overdue = false){
        $lock = new \fu\locks\invoice_charge([
            "invoice_id"=>$invoice_id
        ]);
        $lock->get_lock();

        $charge_info = array(
			'course_id' => (int) $course_id,
			'credit_card_id' => (int) $credit_card_id,
			'invoice_id' => (int) $invoice_id
		);
		
		$invoice_data = $this->db->select('person_id, employee_id, course_id, billing_id, overdue')
			->from('invoices')
			->where('invoice_id', $invoice_id)
			->limit(1)
			->get()
			->row_array();

        if ($invoice_data['started'] > 0) {
            return false;
        }

        // Mark that we have started charge on invoice
        $this->db->query("UPDATE foreup_invoices 
			SET started = started + 1 
			WHERE invoice_id = $invoice_id 
				AND course_id = $course_id 
			LIMIT 1");

        $billing = $this->db->select('auto_pay_overdue')
			->from('customer_billing')
			->where('billing_id', $invoice_data['billing_id'])
			->limit(1)
			->get()
			->row_array();
		
		$overdue_invoice_total = 0;
		$overdue_invoices = array();
		if (isset($billing['auto_pay_overdue']) && $billing['auto_pay_overdue'] && $allow_pay_overdue){
			// Get all overdue invoices for this billing
			$overdue_invoices = $this->get_overdue_invoices($invoice_data['billing_id'], $invoice_id);
			foreach($overdue_invoices as $overdue_invoice) {
				$overdue_invoice_total += $overdue_invoice['total'] - $overdue_invoice['paid'];
			}
		}
		
		$due = (float) ($amount + $overdue_invoice_total);
		
		$customer_id = $invoice_data['person_id'];
		$employee_id = $invoice_data['employee_id'];
		$course_id = $invoice_data['course_id'];
		
		// Charge the card, charge_info is passed by reference and
		// will be updated with credit_card_payment_id
		$success = $this->Customer_credit_card->charge($charge_info, $due);

		// If card was charged successfully, update invoice with paid amount
		if($success){
			
			$updated_invoice_data = array(
				'credit_card_payment_id' => $charge_info['credit_card_payment_id'],
				'charged' => 1,
				'emailed' => 0,
				'last_billing_attempt' => date('Y-m-d'),
				'notes' => 'Paid with: '.$charge_info['payment_type'].' on '.date('Y-m-d')
			);
			
			// Create a new sale to log that the invoice was paid for
			$sale_items = array();

			$sale_items[] = array(
				'invoice_id' => $invoice_id,
				'line' => 1,
				'price' => (float) $amount			
			);
			// Add overdue invoices if present
			if (count($overdue_invoices) > 0) {
				$updated_invoice_data['notes'] .= "<br/>Overdue Invoices Paid:";
				$updated_invoice_data['overdue'] = $invoice_data['overdue'] + $overdue_invoice_total;
			}
			
			foreach ($overdue_invoices as $index => $overdue_invoice) {
				$sale_items[] = array(
					'invoice_id' => $overdue_invoice['invoice_id'],
					'line' => $index + 2,
					'price' => (float) $overdue_invoice['total'] - $overdue_invoice['paid']			
				);
				$updated_invoice_data['notes'] .= "<br/>Inv #{$overdue_invoice['invoice_number']}: $".(round($overdue_invoice['total'] - $overdue_invoice['paid'],2));
			}
			$sale_payments = array();
			$sale_payments[] = array(
				'payment_type' => $charge_info['payment_type'],
				'payment_amount' => abs($due),
				'invoice_id' => $charge_info['credit_card_payment_id']
			);	
			// Create sale that pays off invoice
			$this->Sale->save($sale_items, $customer_id, $employee_id, 'Auto-billed invoice', $sale_payments, false, -1, $course_id, null, false, '', $terminal_id);

		// If charge failed, update invoice that an attempt was made
		}else{
			$updated_invoice_data = array(
				'charged' => 0,
				'last_billing_attempt' => date('Y-m-d'),
				'notes' => 'Declined payment: '.$charge_info['payment_type']
			);
		}
		// Apply charge update to invoice
		$this->save($updated_invoice_data, $invoice_id);
        // Closing connection after each charge
        $this->db->close();

		return $success;
	}


	function mark_as_charged($invoice_id)
	{
		$data = array(
			'charged'=>1
		);
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('foreup_invoices', $data);
	}

	function mark_as_emailed($invoice_id)
	{
		$data = array(
			'emailed'=>1
		);
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('foreup_invoices', $data);
	}
}
?>
