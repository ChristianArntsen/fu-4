<?php
namespace models\AclRolePermissions;

class Row
{
    public $role_id;
    public $type;
    public $action;
    public $resource_type;
    public $resource_id;
}