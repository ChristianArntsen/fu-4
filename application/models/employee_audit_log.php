<?php
class Employee_Audit_Log extends CI_MODEL {
    public function save($log_data)
    {
        //
        /*   foreup_employee_audit_log
         *   editor_id INT(10) UNSIGNED NOT NULL,
         *   gmt_logged DATETIME DEFAULT CURRENT_TIMESTAMP,
         *   action_type_id INT(10) UNSIGNED COMMENT 'Try to avoid duplicates so we can add to primary key if needed for performance',
         *   comments VARCHAR(256)
         *   PRIMARY KEY (`editor_id`,`gmt_logged`)
         * */
        if($this->db->insert('employee_audit_log',$log_data))
        {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        return false;
    }
}
?>