<?php
class Tee_time_log extends CI_Model
{

    function save($tee_time_id, $sql, $employee_id = false) {
        if (!$employee_id) {
            $employee_id = $this->session->userdata('person_id');
        }

        $data = array(
            'tee_time_id' => $tee_time_id,
            'employee_id' => $employee_id,
            'log_date' => gmdate('Y-m-d H:i:s'),
            'sql' => $sql
        );
        $this->db->insert('teetime_log', $data);
    }
}