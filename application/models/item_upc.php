<?php
class Item_upc extends CI_Model
{
	function __construct(){
		parent::__construct();
	}

	/*
	Determines if a given upc exists for our course
	*/
	function exists($upc)
	{
		$this->db->from('items_upcs');
		$this->db->where("upc", $upc);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Determines if a given upc exists for our course
	*/
	function get_item_id($upc)
	{
		$this->db->from('items_upcs');
		$this->db->where("upc", $upc);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows()==1) {
			$result = $query->row_array();
			return $result['item_id'];
		}
		else {
			return 0;
		}
	}

	/*
	Returns all the item's upc codes
	*/
	function get_all($item_id)
	{
		$this->db->from('items_upcs');
		$this->db->where("item_id", $item_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$query = $this->db->get();

		return $query->result_array();
	}

	/*
	Inserts or updates an upc
	*/
	function save(&$upc_data,$upc_id=false)
	{
		if ($this->exists($upc_data['upc']))
		{
			$upc_data['error'] = 'UPC # is already in use. Please select a different one.';
			return false;
		}
		else if (!$upc_id)
		{
        	if($this->db->insert('items_upcs',$upc_data))
			{
				$upc_data['upc_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('upc_id', $upc_id);
		return $this->db->update('items_upcs',$upc_data);
	}


	/*
	Deletes one upc
	*/
	function delete($upc_id)
	{
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('upc_id', $upc_id);
        return $this->db->delete('items_upcs');
	}
}
?>
