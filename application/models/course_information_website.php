<?php
class Course_information_website extends CI_Model {
	
	function __construct(){
		parent::__construct();	
	}

	public function get($course_id)
	{
		$this->db->select("courses_information_website.*,concat(first_name,\" \",last_name) as website_rep_name",false);
		$this->db->from('courses_information_website');
		$this->db->where("course_id = '$course_id'");
		$this->db->join("people","courses_information_website.website_rep = people.person_id","left");
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows()==1){
			return $query->row();
		} else {
			$this->db->insert("courses_information_website",["course_id"=>$course_id]);
			return $this->get($course_id);
		}
	}

	public function save($course_id,$data)
	{
		$this->db->where("course_id = '$course_id'");
		$data['course_id']=$course_id;
		$this->db->insert_on_duplicate_update_batch('courses_information_website',$data);
	}

}