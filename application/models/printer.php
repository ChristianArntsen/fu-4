<?php
class Printer extends CI_Model
{
	// ADD/UPDATE NEW PRINTER
	public function save($printer_data, $printer_id = false)
	{
		if ($printer_id)
		{
			$this->db->where('printer_id', $printer_id);
			$this->db->update('printers', $printer_data);
			return $printer_id;
		}
		else {
			$printer_data['course_id'] = (int) $this->session->userdata('course_id');
			$this->db->insert('printers', $printer_data);
			return $this->db->insert_id();
		}
	}
	
	// DELETE PRINTER
	public function delete($printer_id)
	{
		$printer_id = (int) $printer_id;
		$course_id = (int) $this->session->userdata('course_id');
		
		$this->db->query("DELETE printer.*, item_printer.*
			FROM foreup_printers AS printer
			LEFT JOIN foreup_item_printers AS item_printer
				ON item_printer.printer_id = printer.printer_id
			WHERE printer.printer_id = {$printer_id}
				AND printer.course_id = {$course_id}");
				
		return $this->db->affected_rows();
	}
	
	public function get_default_kitchen_printer(){
		
		$printer_id = $this->config->item('default_kitchen_printer');
		if(empty($printer_id)){
			return false;
		}
		
		$this->db->select('p.printer_id, p.label, p.ip_address');
		$this->db->from('printers AS p');
		$this->db->where('p.course_id', (int) $this->session->userdata('course_id'));
		$this->db->where('p.printer_id', (int) $printer_id);
		$row = $this->db->get()->row_array();
		
		return $row; 
	}
	
	// GET PRINTERS
	public function get_all()
	{
		$this->db->from('printers');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->get()->result_array();
	}
	
	public function get($printer_id)
	{
		$this->db->from('printers');
		$this->db->where('printer_id', (int) $printer_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->get()->row_array();
	}	
	
	// Returns printer data in an array for a drop down menu
	public function get_menu(){
		$printers = $this->get_all();
		$menu = array();
		
		if(empty($printers)){
			return $menu;
		}
		
		foreach($printers as $printer){
			$menu[$printer['printer_id']] = $printer['label'];
		}
		return $menu;		
	}
	
	// DELETE ITEM PRINTERS
	public function delete_item_printers($item_id)
	{
		$this->db->where('item_id', $item_id);
		$this->db->delete('item_printers');
	}
	
	// SAVE ITEM PRINTERS
	public function save_item_printers($item_id, $printers)
	{
		if(empty($item_id)){
			return false;
		}
		$this->db->delete('item_printers', array('item_id' => (int) $item_id));
		
		$batch_data = array();
		foreach($printers as $printer_id){
			
			if(empty($printer_id)){
				continue;
			}
			
			$batch_data[] = array(
				'item_id' => (int) $item_id,
				'printer_id' => (int) $printer_id
			);
		}
		
		$rows = 0;
		if(!empty($batch_data)){
			$this->db->insert_batch('item_printers', $batch_data);
			$rows = $this->db->affected_rows();
		}
		
		return $rows;
	}
	
	// GET ITEM PRINTERS
	public function get_all_item_printers($item_id)
	{
		$this->db->select('printers.printer_id, printers.label, printers.ip_address');
		$this->db->from('item_printers');
		$this->db->join('printers', 'printers.printer_id = item_printers.printer_id', 'inner');
		$this->db->where('item_printers.item_id', $item_id);
		return $this->db->get()->result_array();
	}
	
	public function get_ip_address($printer_id){
		$row = $this->db->select('ip_address')
			->from('printers')
			->where('printer_id', (int) $printer_id)
			->where('course_id', (int) $this->session->userdata('course_id'))
			->get()->row_array();
			
		if(empty($row['ip_address'])){
			return '';
		}
		
		return $row['ip_address'];
	}
}
