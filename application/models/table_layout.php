<?php
class Table_Layout extends CI_Model
{
	function __construct(){
		parent::__construct();
	}

	// Save a new layout
	function save($layout_id, $name, $order = 0){

		if(empty($layout_id)){
			$this->db->insert('table_layouts', array(
				'name' => $name,
				'order' => $order,
				'course_id' => $this->session->userdata('course_id')
			));
			$layout_id = (int) $this->db->insert_id();

		}else{
			$this->db->update('table_layouts', array(
				'name' => $name,
				'order' => $order
			), array('layout_id' => $layout_id));
		}

		return $layout_id;
	}

	// Retrieve layout data
	function get($layout_id = null)
	{
		$layouts = array();

		// Select all layouts and objects belonging to those layouts
		$this->db->select('layout.layout_id, layout.name AS layout_name,
			layout.order AS layout_order, layout.course_id,

			open_table.employee_id, open_table.sale_time AS time_opened,
			employee.first_name AS employee_first_name,
			employee.last_name AS employee_last_name, open_table.name AS table_name,

			object.label AS object_label, object.type AS object_type,
			object.pos_x AS object_pos_x, object.pos_y AS object_pos_y,
			object.width AS object_width, object.height AS object_height,
			object.rotation AS object_rotation, object.object_id', false);
		$this->db->from('table_layouts AS layout');
		$this->db->join('table_layout_objects AS object', 'object.layout_id = layout.layout_id', 'left');
		$this->db->join('tables AS open_table', 'open_table.table_id = object.label AND open_table.course_id = '.(int) $this->session->userdata('course_id'), 'left');
		$this->db->join('people AS employee', 'employee.person_id = open_table.employee_id', 'left');

		if(!empty($layout_id)){
			$this->db->where('layout.layout_id', (int) $layout_id);
		}
		$this->db->where('layout.course_id', $this->session->userdata('course_id'));
		$result = $this->db->get();
		$rows = $result->result_array();

		// Loop through layouts and objects. Organize into multi-dimensional array
		// according to layout
		foreach($rows as $key => $row){

			$layout =& $layouts[$row['layout_id']];
			$layout['layout_id'] = $row['layout_id'];
			$layout['name'] = $row['layout_name'];
			$layout['order'] = $row['layout_order'];

			$layout['aspect_ratio'] = $this->layout_aspect_ratio($row['layout_id']);

			if(!empty($row['object_id'])){
				$layout['objects'][] = array(
					'object_id' => $row['object_id'],
					'label' => $row['object_label'],
					'type' => $row['object_type'],
					'pos_x' => $row['object_pos_x'],
					'pos_y' => $row['object_pos_y'],
					'rotation' => $row['object_rotation'],
					'width' => $row['object_width'],
					'height' => $row['object_height'],
					'employee_id' => $row['employee_id'],
					'table_name' => $row['table_name'],
					'employee_first_name' => $row['employee_first_name'],
					'employee_last_name' => $row['employee_last_name']
				);
			}
		}

		return $layouts;
	}

	function layout_aspect_ratio($layout_id){
		$layout_id = (int) $layout_id;
		$query =$this->db->query("select  
			max(pos_y + ifNull(height,80)) as lowest,
			max(pos_x + ifNull(width,80)) as rightest,
			max(pos_y + ifNull(height,80))/max(pos_x + ifNull(width,80)) as ratio
			from foreup_table_layout_objects where layout_id = {$layout_id};");

	    return $query->result_array()[0];
	}
	
	function layout_has_open_sales($layout_id){
		
		$this->db->select('object.object_id');
		$this->db->from('table_layouts AS layout');
		$this->db->join('table_layout_objects AS object', 'object.layout_id = layout.layout_id', 'left');
		$this->db->join('tables AS tables', 'tables.table_id = object.label AND tables.course_id = layout.course_id', 'inner');
		$this->db->where('layout.course_id', (int) $this->session->userdata('course_id'));
		$this->db->where('layout.layout_id', (int) $layout_id);
		$rows = $this->db->get()->num_rows();
		
		if($rows > 0){
			return true;
		}else{
			return false;
		}	
	}
	
	// Delete layout
	function delete($layout_id){
		if(empty($layout_id)){
			return false;
		}

		$layout_delete = $this->db->delete('table_layouts', array('layout_id'=>$layout_id, 'course_id'=>$this->session->userdata('course_id')));
		if(!$layout_delete){
			return false;
		}
		$success = $this->db->delete('table_layout_objects', array('layout_id'=>$layout_id));

		return $success;
	}
}
?>
