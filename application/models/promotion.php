<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promotion extends CI_Model
{
  public function get_all($limit=10000, $offset=0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
    $this->db->from('promotion_definitions');
		$this->db->where("deleted = 0 $course_id");
		$this->db->order_by("expiration_date", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}

	public function count_all()
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->from('promotion_definitions');
        $this->db->where("deleted = 0 $course_id");
		return $this->db->count_all_results();
	}
  public function get_last_coupon_id()
  {
      $course_id = $this->session->userdata('course_id');
      $result = $this->db->query('SELECT MAX(id) AS id FROM foreup_promotion_definitions WHERE course_id=\'' . $course_id . '\'');
      $id = $result->result_array();
      
      return $id[0]['id'];
  }      
  public function get_coupon_data($coupon_type_id)
  {
      $result = $this->db->query('SELECT * FROM foreup_promotion_definitions WHERE id='.'\''.$coupon_type_id.'\'');
      $return_array = $result->result_array();
      return $return_array[0];
  }
  public function search($search, $limit=20, $offset = 0)
	{
		$course_id = '';
	    if (!$this->permissions->is_super_admin())
	    {
	      $pr = $this->db->dbprefix('promotion_definitions');
	      $course_id = "AND {$pr}.course_id = '{$this->session->userdata('course_id')}'";
	    }
	    $this->db->from('promotion_definitions');
	    $this->db->like('lower(name)', strtolower($search));
		$this->db->where("deleted = 0 $course_id");
	    $this->db->order_by("id", "asc");
		// Just return a count of all search results
        if ($limit == 0)
            return $this->db->get()->num_rows();
        // Return results
        $this->db->offset($offset);
		$this->db->limit($limit);
	    return $this->db->get();
	}

  public function get_info($promotion_id)
  {
	$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->from('promotion_definitions');
	$this->db->where('id',$promotion_id);
	$this->db->where("deleted = 0 $course_id");

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $campaign_id is NOT an campaign
			$campaign_obj=new stdClass();

			//Get all the fields from campaigns table
			$fields = $this->db->list_fields('promotion_definitions');

			foreach ($fields as $field)
			{
				$campaign_obj->$field='';
			}

			return $campaign_obj;
		}
	}

  function get_search_suggestions($search,$limit=25)
  {
		       
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $suggestions = array();

		$this->db->from('promotion_definitions');
		$this->db->like('lower(title)', strtolower($search));
                $this->db->where("deleted = 0 $course_id");
		$this->db->order_by("id", "asc");
		$by_number = $this->db->get();
		foreach($by_number->result() as $row)
		{
			$suggestions[]=array('label' => $row->title);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
                
    }
    function get_coupon_name_suggestions($search, $limit=25)
    {
        $course_id = "AND course_id='{$this->session->userdata('course_id')}'";
        
        $result = $this->db->query("SELECT *, name AS label, id AS value FROM foreup_promotion_definitions WHERE name LIKE '%$search%' $course_id AND deleted='0' ORDER BY name ASC");
        $suggestions = $result->result_array();
        
        if (count($suggestions) > $limit)
        {
            $suggestions = array_slice($suggestions, 0, $limit);
        }
        return $suggestions;
    }
  /*
   * Determines if a given promotion_id is an promotion
	*/
	function exists( $campaign_id )
	{
		$this->db->from('promotion_definitions');
		$this->db->where('id',$campaign_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	
	function is_valid_day_of_week($promotion_id)
	{
		$today = date('l', time());
		
		//log_message('error', "TIME TODAY: $today");
		$result = $this->db->query("SELECT * FROM foreup_promotion_definitions WHERE id='$promotion_id'");
		$row = $result->result_array();  
		
		if ($row[0][$today] == 1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	function get_coupon_by_online_code($online_code, $time, $course_id){
		
		if($online_code == ''){
			return false;
		}
		
		$this->db->select('id, name, amount_type, amount, valid_between_from,
			valid_between_to, Sunday, Monday, Tuesday, Wednesday, Thursday,
			Friday, Saturday');
		$this->db->from('promotion_definitions');
		$this->db->where('online_code', $online_code);
		$this->db->where('deleted', 0);
		$this->db->where('expiration_date >=', $time->format('Y-m-d'));
		$this->db->where('course_id', (int) $course_id);
		$row = $this->db->get()->row_array();
		
		if(!$row){
			return false;
		}
		
		if($row[$time->format('l')] != 1){
			return false;
		}
		
		if($row['valid_between_from'] == 'anytime'){
			$start_time = 0;
		}else{
			$start_time = (int) date('H', strtotime($row['valid_between_from']));
		}
		
		if($row['valid_between_to'] == 'anytime'){
			$end_time = 2400;
		}else{
			$end_time = (int) date('H', strtotime($row['valid_between_to']));
		}		
		
		if($start_time > (int) $time->format('H') || $end_time <= (int) $time->format('H')){
			return false;
		}
		
		return $row;
	}
	
	function calculate_bogo_discount($base_price, $quantity, $get_quantity, $buy_quantity){		
		$discount = 0;
		$offer_quantity = $get_quantity + $buy_quantity;
   		
   		if($quantity >= $offer_quantity){
   			$quantity -= $offer_quantity;
   			$discount += $base_price * $get_quantity;
   			if($quantity >= $offer_quantity){
   				$discount += $this->calculate_bogo_discount($base_price, $quantity, $get_quantity, $buy_quantity);
			}
   		}
   		
   		return $discount;
	}	
	
    function get_numeric_discount($basket_items, $type, $coupon_data){
       
		$numeric_discount = 0;
        $item_count = 0;        
        $coupon_is_valid = false;
        $highest_bogo_price = 0; 
        $basket_total = 0;
        $basket_items = array_values($basket_items);    
        $cart_items_details = array();
		
        foreach($basket_items as $basket_item){
			
			$basket_total += $basket_item['subtotal'];
			
            if($basket_item['subcategory'] != $coupon_data['subcategory_type'] &&
            $basket_item['category'] != $coupon_data['category_type'] &&
            $basket_item['department'] != $coupon_data['department_type'] &&
            $basket_item['item_id'] != $coupon_data['item_id']){
				continue;
			}
			$item_count += $basket_item['quantity'];
			
			if($coupon_data['bogo'] === 'discount'){                    
				if($coupon_data['amount_type'] === '$' && $basket_item['subtotal'] >= $coupon_data['amount']){
					$numeric_discount += $coupon_data['amount'];
					$coupon_is_valid = true;
				
				}else if($coupon_data['amount_type'] === '%'){                                              
					$numeric_discount += (float) bcmul($basket_item['subtotal'], $coupon_data['amount'] / 100, 2);
					$coupon_is_valid = true;

				}else{
					$error_message = 'There is an internal error';
				}		
			
			}else if ($coupon_data['bogo'] === 'bogo') {
				$cart_items_details[] = array('base_price'=>$basket_item['base_price'],'quantity'=>$basket_item['quantity']);                   
			}
        }
        
        if ($coupon_data['bogo'] === 'bogo' && $item_count >= (intval($coupon_data['get_quantity']) + intval($coupon_data['buy_quantity']))){

        	foreach($cart_items_details as $key => $row){
				$quantity[$key]  = $row['quantity'];
				$base_price[$key] = $row['base_price'];
			}
			
			$offer_quantity = $coupon_data['get_quantity'] + $coupon_data['buy_quantity'];
            array_multisort($quantity, SORT_DESC, $base_price, SORT_ASC, $cart_items_details);            
           	$rem_quantity = 0;
           
           	foreach ($cart_items_details as $key => $cart_item_value){
           		$rem_quantity = $cart_item_value['quantity'] % $offer_quantity;
           		if($cart_item_value['quantity'] >= $offer_quantity){
           			$item_count -= $cart_item_value['quantity'];
           			$numeric_discount += $this->calculate_bogo_discount($cart_item_value['base_price'], $cart_item_value['quantity'], $coupon_data['get_quantity'], $coupon_data['buy_quantity']);	           		           		
           		}
			}
			
			if($item_count >= $offer_quantity){
				foreach ($cart_items_details as $key => $cart_item_value) {					
					if($item_count >= $offer_quantity){
						$item_count -= ($cart_item_value['quantity'] % $offer_quantity);
						$numeric_discount = (min($base_price) * $coupon_data['get_quantity']);	
		           	}
				}
			}
			
            if($coupon_data['amount_type'] !== 'FREE') {                                                                           
                $numeric_discount = bcmul(strval($numeric_discount), $coupon_data['amount'] / 100, 2);                                                
            }  
            
            $coupon_is_valid = true;
        }        
        
        if($coupon_data['bogo'] === 'discount' && $basket_total < (float) $coupon_data['min_purchase']){
			return 'Minimum purchase of $'.$coupon_data['min_purchase'].' required';
		}
        
        if (!$coupon_is_valid && $error_message === '') {                                      
            return $error_message = "This coupon requires the purchase of {$coupon_data['valid_for']}";                                  
        }
        
        if(empty($quantity)){
        	$quantity = array();
        }
        $item_count = array_sum($quantity);
        if ($coupon_data['bogo'] === 'bogo' && $item_count < (intval($coupon_data['get_quantity']) + intval($coupon_data['buy_quantity']))){            
            return $error_message = "More {$coupon_data['valid_for']} items need to be placed in the basket for this coupon to take effect";
       
        }else if (!$coupon_is_valid){
            return $error_message;
        }
       
        return $numeric_discount;
    }
    
    function calculate_online_booking_discount($promo_id, $teesheet_id, $price_class_id, $time, $holes, $players, $carts){
		
		$this->load->model('Item');
		$coupon_data = $this->get_coupon_data($promo_id);	
		$items = array();
		$course_id = (int) $this->course_id;
		
		if(isset($this->seasonal_pricing)){
			$seasonal_pricing = $this->seasonal_pricing;
		}else{
			$seasonal_pricing = ($this->config->item('seasonal_pricing') == 1);
		}
			
		$start_date = $time->format('Y-m-d');
		$start_time = $time->format('Hi');
		$start_day_of_week = $time->format('D');				
		
		// Get price of selected teetime
		// If course is using new seasonal pricing
		if($seasonal_pricing){
			$this->load->model('pricing');
			$this->pricing->course_id = $course_id;
			$green_fee = $this->pricing->get_price($teesheet_id, $price_class_id, $start_date, $start_time, $holes, false);
			$cart_fee = $this->pricing->get_price($teesheet_id, $price_class_id, $start_date, $start_time, $holes, true);
			
		// If course is using old standard pricing
		}else{
			$prices = $this->Green_fee->get_info('', '', '', $course_id);
			$price_indexes = $this->teesheet->determine_price_indexes($start_time, $holes, $start_day_of_week);
			
			if(empty($price_class_id)){
				$price_class_id = $price_indexes['price_category'];
			}

			$green_fee = (float) $prices[$teesheet_id][$course_id.'_'.$price_indexes['green_fee_index']]->$price_class_id;
			$cart_fee = (float) $prices[$teesheet_id][$course_id.'_'.$price_indexes['cart_price_index']]->$price_class_id;
		}
		
		$green_fee_department = $this->Item->get_teetime_department($course_id);
		$green_fee_department = $green_fee_department[0]['department'];	
		$cart_fee_department = $this->Item->get_cart_department($course_id);
		$cart_fee_department = $cart_fee_department[0]['department'];
		
		// Built a mock "sales cart" to calculate what total discount will be
		// when the tee times are actually purchased
		if($players > 0){
			
			for($x = 0; $x < $players; $x++){
				$items[] = array(
					'item_id' => false,
					'category' => 'Green Fees',
					'subcategory' => 'Green Fees',
					'department' => $green_fee_department,
					'quantity' => 1,
					'subtotal' => (float) $green_fee,
					'base_price' => (float) $green_fee
				);
			}
		}
		
		if($carts > 0){
			for($x = 0; $x < $carts; $x++){
				$items[] = array(
					'item_id' => false,
					'category' => 'Cart Fees',
					'subcategory' => 'Cart Fees',
					'department' => $cart_fee_department,
					'quantity' => 1,
					'subtotal' => (float) $cart_fee,
					'base_price' => (float) $cart_fee
				);
			}
		}
		
		$discount = $this->get_numeric_discount($items, false, $coupon_data);
		
		// If returned value is a string, there was an error
		if(is_numeric($discount)){
			return (float) $discount;
		}else{
			return false;
		}
	}
	
  /*
   * inserts a new record
   */
  function save(&$promotion_data,$promotion_id=false)
  {
      //log_message('error', 'PROMOTION: ' . $promotion_data['expiration_date']);
		if (!$promotion_id or !$this->exists($promotion_id))
		{
			if($this->db->insert('promotion_definitions',$promotion_data))
			{
				$promotion_data['id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('id', $promotion_id);
		return $this->db->update('promotion_definitions',$promotion_data);
	}

  /*
	Deletes one promotion
	*/
	function delete($promotion_id)
	{
    /*
     * TO DO: validation checking
     */
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("id = '$promotion_id' $course_id");
		return $this->db->update('promotion_definitions', array('deleted' => 1));
	}

	/*
	Deletes a list of promotions
	*/
	function delete_list($promotion_ids)
	{
    /*
     * TO DO: validation checking
     */
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where_in('id',$promotion_ids);
		return $this->db->update('promotion_definitions', array('deleted' => 1));
 	}
	
	function is_coupon_redeemed($coupon_id, $coupon_type)
	{
		$result = $this->db->query('SELECT redeemed FROM foreup_promotions WHERE id=\''. $coupon_id . '\' AND coupon_definition_id=\'' . $coupon_type . '\'');
		$array = $result->result_array();          
		if ($array[0]['redeemed'] !== 'N')
		{
			return true;              
		}
		else
		{
			return false;
		}         
	}
	
	function has_online_promos($course_id){
		
		$num_rows = $this->db->select('id')
			->from('promotion_definitions')
			->where('course_id', (int) $course_id)
			->where("online_code != '' OR online_code IS NOT NULL")
			->limit(1)->get()->num_rows();
		
		if($num_rows > 0){
			return true;
		}
		
		return false;
	}
}
