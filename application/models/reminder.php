<?php

class Reminder extends MY_Model
{
    protected $_table = "reminders";

    public $before_create = ['timestamps'];

	function __construct()
	{
		parent::__construct();
	}

    protected function timestamps($data)
    {
        $data['last_ran'] = date('Y-m-d H:i:00');
        return $data;
    }

}
