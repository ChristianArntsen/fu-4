<?php

class Customer extends Person
{
	// Option for get_info method call to include all price class IDs a customer has access to
	public $include_valid_price_classes = false;

	/*
	Determines if a given person_id is a customer
	*/
	function exists($person_id)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids);

		$this->db->select('course_id');
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.person_id', $person_id);
		$this->db->limit(1);
		if ($this->session->userdata('course_id'))
			$this->db->where_in('customers.course_id', array_values($course_ids));
		$query = $this->db->get();
		//echo $this->db->last_query();
		return ($query->num_rows() == 1);
	}

	function get_id_from_account_number($account_number)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids);

		$this->db->where_in('customers.course_id', array_values($course_ids));

		$this->db->select('person_id');
		$this->db->from('customers');
		$this->db->where('account_number', $account_number);
		//$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		$customer_info = $this->db->get()->result_array();
		//echo $this->db->last_query();
		//print_r($customer_info);
		return $customer_info[0]['person_id'];
	}

	function get_by_username($username)
	{
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where("(username = '$username')");
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->row();
		}

		return false;
	}

	function save_image($person_id, $image_id)
	{
		return $this->db->update('customers',
			array('image_id' => (int)$image_id),
			"person_id = " . (int)$person_id . "
				AND course_id = " . (int)$this->session->userdata('course_id')
		);
	}

	function update_password($customer_id, $password)
	{
		$customer_data = array('password' => $password);
		$this->db->where('person_id', $customer_id);
		$success = $this->db->update('customers', $customer_data);

		return $success;
	}

	/*
	Determines if a customer username already exists
	*/
	function username_exists($username)
	{
		$this->load->model('user');
		return $this->user->username_exists($username);
	}

	/*
	Returns all the customers
	*/
	function get_all($limit = 10000, $offset = 0, $group_id = false, $limited_data = false)
	{

		$course_ids = array();
		$this->get_linked_course_ids($course_ids);
		$this->db->where_in('customers.course_id', array_values($course_ids));
		if ($limited_data) {
			$this->db->select('customers.person_id, first_name, last_name, email');
		}
		$this->db->from('customers');
		$this->db->join('people', 'customers.person_id = people.person_id');
		if ($group_id && $group_id != 'all') {
			$this->db->join('customer_group_members', 'people.person_id = customer_group_members.person_id');
			$this->db->where('customer_group_members.group_id', $group_id);
		}

		$this->db->where("deleted = 0 ");
		$this->db->where('hide_from_search', 0);
		$this->db->order_by("last_name asc, first_name asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$customers = $this->db->get();
		$this->benchmark->mark('query_end');

		return $customers;
	}

	function get_all_for_download($limit = 10000, $offset = 0, $group_id = false, $limited_data = false)
	{
		$course_id = '';

		$course_ids = array();
		$this->get_linked_course_ids($course_ids);

		$group_sql = '';
		if ($group_id && $group_id != 'all') {
			$group_sql = " AND gm.group_id = $group_id";
		}


		$customers = $this->db->query("
			SELECT c.person_id, first_name, last_name, email, phone_number, cell_phone_number, address_1, address_2, city, state, zip, country, comments, account_number, taxable, company_name, opt_out_email, birthday, GROUP_CONCAT(label) groups, account_balance, member_account_balance, invoice_balance, loyalty_points 
			FROM foreup_customers AS c
			JOIN foreup_people AS p ON p.person_id = c.person_id 
			LEFT JOIN foreup_customer_group_members AS gm ON p.person_id = gm.person_id
			LEFT JOIN foreup_customer_groups AS cg ON gm.group_id = cg.group_id 
			WHERE c.deleted = 0 AND c.hide_from_search = 0
				AND c.course_id IN (" . implode(',', array_values($course_ids)) . ")
				$group_sql
			GROUP BY c.person_id
			ORDER BY last_name asc, first_name asc
		");

		//$customers = $this->db->get();


		return $customers;
	}

	function get_negative_balances($balance_type = false)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids);

		$this->db->where_in('customers.course_id', array_values($course_ids));

		if ($balance_type == 'member')
			$this->db->where('member_account_balance <', 0);
		else if ($balance_type == 'customer')
			$this->db->where('account_balance <', 0);
		else if ($balance_type == 'both')
			$this->db->where('(member_account_balance < 0 OR account_balance < 0)');
		else
			return;
		$this->db->from('customers');
		$this->db->join('people', 'customers.person_id=people.person_id');
		$this->db->where('deleted', 0);
		$this->db->where('hide_from_search', 0);
		//$this->db->where('credit_card_id !=', 0);
		//$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->order_by('last_name asc, first_name asc');
		$customers = $this->db->get();

		return $customers->result_object();
	}

	function count_all($filter = '', $person_ids = false, $group_ids = false)
	{
		$course_id = '';
		//if (!$this->permissions->is_super_admin())
		{
			//$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
			$this->db->where_in('customers.course_id', array_values($course_ids));
		}
		$this->db->from('customers');
		if ($filter == 'email') {
			$this->db->select('COUNT(distinct(email)) AS count');
			$this->db->join('people', 'people.person_id = customers.person_id');
			$this->db->where('email !=', '');
			//$this->db->group_by('email');
		} else if ($filter == 'phone') {
			$this->db->select('COUNT(distinct(phone_number)) AS count');
			$this->db->join('people', 'people.person_id = customers.person_id');
			$this->db->join('marketing_texting', 'foreup_marketing_texting.person_id = foreup_customers.person_id', 'right');
			$this->db->where('phone_number !=', '');
			//$this->db->group_by('phone_number');
		} else {
			$this->db->select('COUNT(*) AS count');
		}
		if ($person_ids !== false) {
			if (count($person_ids) == 0)
				$this->db->where('customers.person_id', 0);
			else
				$this->db->where_in('customers.person_id', $person_ids);
		}
		if ($group_ids !== false && !in_array(0, $group_ids)) {
			if (count($group_ids) == 0)
				$this->db->where('customers.person_id', 0);
			else {
				$this->db->join('customer_group_members', 'people.person_id = customer_group_members.person_id');
				$this->db->where_in('customer_group_members.group_id', $group_ids);
			}
		}
		$this->db->where("deleted = 0 $course_id");
		$this->db->where('hide_from_search', 0);
		//echo $query->
		$start = time();
		$this->benchmark->mark('query_start');
		$query = $this->db->get();
		//$result = $query->num_rows();
		//$result = $this->db->count_all_results();
		//print_r($result);
		$end = time();
		$this->benchmark->mark('query_end');
		//echo "time ".($end - $start).'<br/>';
		//echo $this->db->last_query();
		//echo '<br/>Benchmark '.$this->benchmark->elapsed_time('query_start','query_end');
		$query = $query->row_array();
		return $query['count'];//$results;
	}

	/*
	Determines if a employee is logged in
	*/
	function is_logged_in()
	{
		return ($this->session->userdata('customer_id') != false);// && $this->session->userdata('type') == 'customer');
	}

	/*
	Gets information about the currently logged in employee.
	*/
	function get_logged_in_customer_info()
	{
		if ($this->is_logged_in()) {
			return $this->get_info($this->session->userdata('customer_id'));
		}

		return false;
	}

	/*
	Gets information about a particular customer
	*/
	function get_info($customer_id, $course_id = false, $allow_deleted = false)
	{
		if ($course_id && $course_id != '') {
			$course_ids = array();
			$this->get_linked_course_ids($course_ids, $course_id);
			$this->db->where_in('customers.course_id', array_values($course_ids));
		}
		$this->db->from('customers');
		$this->db->select("customers.*,people.*,marketing_texting.texting_status,marketing_texting.msisdn,marketing_texting.keyword");
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->join('marketing_texting', 'foreup_people.person_id = foreup_marketing_texting.person_id AND foreup_customers.course_id = foreup_marketing_texting.course_id', "left");
		if (!$allow_deleted)
			$this->db->where('deleted', 0);
		$this->db->where('customers.person_id', $customer_id);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			
			$row = $query->row();

			if($this->include_valid_price_classes){
				$this->load->model('Price_class');
				$row->valid_price_classes = $this->Price_class->get_customer_price_class_ids($row->person_id);				
			}
			return $row;
		
		} else {
			//Get empty base parent object, as $customer_id is NOT an customer
			$person_obj = parent::get_info(-1);

			//Get all the fields from customer table
			$fields = $this->db->list_fields('customers');

			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field) {
				$person_obj->$field = '';
			}

			return $person_obj;
		}
	}

	/*
	Gets information about a particular customer by email
	*/
	function get_info_by_email($email, $course_id)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids, $course_id);

		$this->db->where_in('customers.course_id', array_values($course_ids));

		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('people.email', $email);
		$this->db->where('deleted', 0);
		//$this->db->where('customers.course_id', $course_id);
		$result = $this->db->get()->result_array();
		// echo $this->db->last_query();
		return $result;
	}

	/*
	Gets information about multiple customers
	*/
	function get_multiple_info($customer_ids, $type = 'email')
	{
		//if ($type == 'email')
		//	$this->db->select('customers.person_id, customers.course_id, customers.username, customers.opt_out_email, customers.opt_out_text, people.first_name,people.email, people.last_name, people.phone_number');
		//else
		//	$this->db->select('customers.person_id, customers.course_id, customers.username, customers.opt_out_email, customers.opt_out_text, people.first_name,people.email, people.last_name, people.phone_number, sendhub_accounts.phone_number AS sendhub_phone_number, sendhub_accounts.sendhub_id, sendhub_accounts.text_reminder_unsubscribe ');
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		//if ($type != 'email')
		//	$this->db->join('sendhub_accounts', 'sendhub_accounts.phone_number = people.phone_number', 'left');
		$this->db->where_in('customers.person_id', $customer_ids);
		$this->db->order_by("last_name asc, first_name asc");
		$result = $this->db->get();
		//echo $this->db->last_query();
		return $result;
	}

	/*
	Gets information about multiple customers
	*/
	function get_multiple_info_for_email($customer_ids)
	{
		$this->db->select('customers.person_id, customers.course_id, customers.opt_out_email, customers.opt_out_text, people.first_name,people.email, people.last_name, people.phone_number');
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where_in('customers.person_id', $customer_ids);
		$this->db->where('opt_out_email', 0);
		$this->db->order_by("last_name asc, first_name asc");
		$result = $this->db->get();
		//echo $this->db->last_query();
		return $result;
	}

	/*
	Gets information about multiple customers
	*/
	function get_multiple_info_for_text($customer_ids)
	{
		$c_id_array = array();
		foreach ($customer_ids as $customer_id)
			$c_id_array[] = array('customer_id' => $customer_id);

		$this->db->query('CREATE TEMPORARY TABLE foreup_campaign_customer_ids (
			customer_id INT,
			PRIMARY KEY `customer_id` (`customer_id`))
			ENGINE=Innodb');
		$this->db->insert_batch('campaign_customer_ids', $c_id_array);

		$this->db->select('customers.person_id, customers.course_id, customers.opt_out_email, customers.opt_out_text, people.first_name,people.email, people.last_name, people.phone_number, sendhub_accounts.phone_number AS sendhub_phone_number, sendhub_accounts.sendhub_id, sendhub_accounts.text_reminder_unsubscribe ');
		$this->db->from('campaign_customer_ids');
		$this->db->join('customers', 'customers.person_id = campaign_customer_ids.customer_id', 'left');
		$this->db->join('people', 'people.person_id = campaign_customer_ids.customer_id', 'left');
		$this->db->join('sendhub_accounts', 'sendhub_accounts.phone_number = people.phone_number', 'left');
		//$this->db->where_in('customers.person_id',$customer_ids);
		$this->db->where('opt_out_text', 0);
		//$this->db->order_by("last_name asc, first_name asc");
		$result = $this->db->get();
		//echo '<br/>Last Query<br/>';
		//echo $this->db->last_query();
		return $result;
	}

	function get_price_classes()
	{
		$price_class_array = array('0' => 'None');
		for ($i = 4; $i <= 30; $i++)
			if ($this->config->item('price_category_' . $i) != '')
				$price_class_array[$i] = $this->config->item('price_category_' . $i);

		return $price_class_array;
	}

	function get_price_class_id($label)
	{
		$label = trim($label);
		$this->db->from('green_fee_types');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('price_category_1', $label);
		$this->db->or_where('price_category_2', $label);
		$this->db->or_where('price_category_3', $label);
		$this->db->or_where('price_category_4', $label);
		$this->db->or_where('price_category_5', $label);
		$this->db->or_where('price_category_6', $label);
		$this->db->or_where('price_category_7', $label);
		$this->db->or_where('price_category_8', $label);
		$this->db->or_where('price_category_9', $label);
		$this->db->or_where('price_category_10', $label);
		$this->db->or_where('price_category_11', $label);
		$this->db->or_where('price_category_12', $label);
		$this->db->or_where('price_category_13', $label);
		$this->db->or_where('price_category_14', $label);
		$this->db->or_where('price_category_15', $label);
		$this->db->or_where('price_category_16', $label);
		$this->db->or_where('price_category_17', $label);
		$this->db->or_where('price_category_18', $label);
		$this->db->or_where('price_category_19', $label);
		$this->db->or_where('price_category_20', $label);
		$this->db->or_where('price_category_21', $label);
		$this->db->or_where('price_category_22', $label);
		$this->db->or_where('price_category_23', $label);
		$this->db->or_where('price_category_24', $label);
		$this->db->or_where('price_category_25', $label);
		$this->db->or_where('price_category_26', $label);
		$this->db->or_where('price_category_27', $label);
		$this->db->or_where('price_category_28', $label);
		$this->db->or_where('price_category_29', $label);
		$this->db->or_where('price_category_30', $label);
		$this->db->or_where('price_category_31', $label);
		$this->db->or_where('price_category_32', $label);
		$this->db->or_where('price_category_33', $label);
		$this->db->or_where('price_category_34', $label);
		$this->db->or_where('price_category_35', $label);
		$this->db->or_where('price_category_36', $label);
		$this->db->or_where('price_category_37', $label);
		$this->db->or_where('price_category_38', $label);
		$this->db->or_where('price_category_39', $label);
		$this->db->or_where('price_category_40', $label);
		$this->db->or_where('price_category_41', $label);
		$this->db->or_where('price_category_42', $label);
		$this->db->or_where('price_category_43', $label);
		$this->db->or_where('price_category_44', $label);
		$this->db->or_where('price_category_45', $label);
		$this->db->or_where('price_category_46', $label);
		$this->db->or_where('price_category_47', $label);
		$this->db->or_where('price_category_48', $label);
		$this->db->or_where('price_category_49', $label);
		$this->db->or_where('price_category_50', $label);

		$this->db->limit(1);

		$price_types = $this->db->get()->result_array();
		//echo json_encode($price_types[0]);
		//return;
		foreach ($price_types[0] as $index => $value) {
			if ($label == $value)
				return $index;
		}
		return '';
	}

	/*
	Updates multiple items at once
	*/
	function update_multiple($customer_data, $customer_ids, $groups_data, $remove_groups_data)
	{
		foreach ($customer_ids as $customer_id) {
			$this->save_group_memberships($groups_data, $customer_id, true, $remove_groups_data);
		}
		$this->db->where_in('person_id', $customer_ids);
		return $this->db->update('customers', $customer_data);
	}

	// GROUP Functionality
	function get_customer_groups($customer_id)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids, $this->session->userdata('course_id'));

		$this->db->where_in('course_id', array_values($course_ids));

		$this->db->from('customer_groups');
		$this->db->join('customer_group_members', "customer_groups.group_id = customer_group_members.group_id", 'LEFT');
		//$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('person_id', $customer_id);
		return $this->db->get()->result_array();
	}

	// PASS Functionality
	function get_customer_passes($customer_id)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids, $this->session->userdata('course_id'));

		$this->db->where_in('course_id', array_values($course_ids));

		$this->db->from('customer_passes');
		$this->db->join('customer_pass_members', "customer_passes.pass_id = customer_pass_members.pass_id", 'LEFT');
		//$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('person_id', $customer_id);
		return $this->db->get()->result_array();
	}

	// GROUP Functionality
	function get_group_info($customer_id = '', $search = false)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids, $this->session->userdata('course_id'));
		$course_id_string = "('" . implode("','", $course_ids) . "')";

		$search_string = '';
		if ($search) {
			$search_string = " AND label LIKE '%{$this->db->escape_like_str($search)}%'";
		}

		$query = $this->db->query("SELECT 
				`foreup_customer_groups`.`group_id` AS group_id, 
				`label`, 
				item_cost_plus_percent_discount,
				SUM(CASE WHEN foreup_customer_group_members.person_id = '$customer_id' THEN 1 ELSE 0 END) AS is_member, 
				SUM(CASE WHEN foreup_customers.deleted = 0 AND foreup_customers.hide_from_search = 0 AND foreup_customers.course_id IN $course_id_string THEN 1 ELSE 0 END) AS member_count
			FROM (`foreup_customer_groups`)
			LEFT JOIN `foreup_customer_group_members` 
				ON `foreup_customer_groups`.`group_id` = `foreup_customer_group_members`.`group_id`
			LEFT JOIN `foreup_customers` 
				ON `foreup_customers`.`person_id` = `foreup_customer_group_members`.`person_id`
			WHERE 
				`foreup_customer_groups`.`course_id` IN $course_id_string 
				$search_string
			GROUP BY `group_id`");

		return $query->result_array();
	}

	function get_single_group_info($group_id)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids, $this->session->userdata('course_id'));

		$this->db->where_in('course_id', array_values($course_ids));

		$this->db->from('customer_groups');
		$this->db->where('group_id', $group_id);
		//$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);

		$group_id = $this->db->get()->result_array();
	}

	function get_group_members($group_id)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids, $this->session->userdata('course_id'));

		$this->db->where_in('course_id', array_values($course_ids));

		$this->db->from('customer_group_members');
		$this->db->join('customers', 'customers.person_id = customer_group_members.person_id', 'left');
		$this->db->where('group_id', $group_id);
		$this->db->where('deleted', 0);
		$this->db->where('hide_from_search', 0);

		return $this->db->get()->result_array();
	}

	function get_multiple_group_info(array $group_ids, $filter = 'none', $is_group_message = false, $course_id = false)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids, $course_id);
		$this->db->where_in('customers.course_id', array_values($course_ids));

		if ($filter == 'email')
			$this->db->select('customers.person_id, customers.course_id, customers.username, customers.opt_out_email, customers.opt_out_text, people.person_id, people.first_name, people.phone_number, people.email');
		else if ($is_group_message)
			$this->db->select('customers.person_id, customers.course_id, customers.username, customers.opt_out_email, customers.opt_out_text, people.person_id, sendhub_accounts.sendhub_id, people.first_name, people.phone_number, people.email, sendhub_accounts.phone_number AS sendhub_phone_number');
		if (!in_array(0, $group_ids)) {
			$this->db->from('customer_group_members');
			$this->db->join('people', 'people.person_id = customer_group_members.person_id');
			$this->db->join('customers', 'customers.person_id = people.person_id');
			if (($filter == 'phone' || $filter == 'phone_number') && $is_group_message) {
				$this->db->join('sendhub_accounts', 'sendhub_accounts.phone_number = people.phone_number', 'left');
			}
			$this->db->where_in('customer_group_members.group_id', $group_ids);
		} else {
			$this->db->from('customers');
			$this->db->join('people', 'people.person_id = customers.person_id');
		}

		//$this->db->where('course_id', ($course_id ? $course_id : $this->session->userdata('course_id')));
		$this->db->where('deleted', 0);
		$this->db->where('hide_from_search', 0);

		if ($filter == 'email') {
			$this->db->where('email !=', '');
			$this->db->group_by('email');
		} else if ($filter == 'phone' || $filter == 'phone_number') {
			$this->db->where('phone_number !=', '');
			$this->db->group_by('phone_number');
		}
		$result = $this->db->get();
		//echo $this->db->last_query();
		return $result;
	}

	function get_multiple_group_info_for_email(array $group_ids, $filter = 'none', $is_group_message = false, $course_id = false)
	{
		// $course_ids = array();
		// $this->get_linked_course_ids($course_ids, $course_id);
		// $this->db->where_in('customers.course_id', array_values($course_ids));

		$this->db->select('customers.person_id AS person_id, email, first_name, opt_out_email');
		if (!in_array(0, $group_ids)) {
			$this->db->from('customer_group_members');
			$this->db->join('people', 'people.person_id = customer_group_members.person_id');
			$this->db->join('customers', 'customers.person_id = people.person_id');
			$this->db->where_in('customer_group_members.group_id', $group_ids);
		} else {
			$this->db->from('customers');
			$this->db->join('people', 'people.person_id = customers.person_id');
		}

		$this->db->where('course_id', $course_id ? $course_id : $this->session->userdata('course_id'));
		$this->db->where('deleted', 0);
		$this->db->where('hide_from_search', 0);
		$this->db->where('opt_out_email', 0);
		$this->db->where('email !=', '');
		$this->db->group_by('email');
		$result = $this->db->get();
		//echo $this->db->last_query();
		return $result;
	}

	function get_multiple_group_info_for_text(array $group_ids, $filter = 'none', $is_group_message = false, $course_id = false)
	{
		// $course_ids = array();
		// $this->get_linked_course_ids($course_ids, $course_id);
		// $this->db->where_in('customers.course_id', array_values($course_ids));

		$this->db->select('customers.person_id, customers.course_id, customers.opt_out_email, customers.opt_out_text, people.person_id, people.first_name, people.phone_number, people.email');
		if (!in_array(0, $group_ids)) {
			$this->db->from('customer_group_members');
			$this->db->join('people', 'people.person_id = customer_group_members.person_id');
			$this->db->join('customers', 'customers.person_id = people.person_id');
			$this->db->where_in('customer_group_members.group_id', $group_ids);
		} else {
			$this->db->from('customers');
			$this->db->join('people', 'people.person_id = customers.person_id');
		}
		$this->db->join('marketing_texting', 'marketing_texting.person_id = people.person_id', 'RIGHT');

		$this->db->where('customers.course_id', $course_id ? $course_id : $this->session->userdata('course_id'));
		$this->db->where('deleted', 0);
		$this->db->where('hide_from_search', 0);
		$this->db->where('opt_out_text', 0);
		$this->db->where('people.phone_number !=', '');
		$this->db->group_by('people.phone_number');
		$result = $this->db->get();
		//echo '<br/>Last Query<br/>';
		//echo $this->db->last_query();
		return $result;
	}

	function save_group_memberships($groups_data, $person_id, $positives_only = false, $remove_groups_data = false)
	{
		if (!$positives_only)
			$this->db->delete('customer_group_members', array('person_id' => $person_id));
		foreach ($groups_data as $group_id) {
            $this->db->query("INSERT IGNORE INTO foreup_customer_group_members (person_id, group_id) VALUES ('{$person_id}','{$group_id}')");
        }

		//Remove each customer from all the groups
		if ($remove_groups_data) {
			foreach ($remove_groups_data as $group_id) {
				$this->db->delete('customer_group_members', array('person_id' => $person_id, 'group_id' => $group_id));
			}
		}
	}

	function get_group_id($group_label)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids, $this->session->userdata('course_id'));

		$this->db->where_in('course_id', array_values($course_ids));

		$this->db->from('customer_groups');
		$this->db->where('label', trim($group_label));
		//$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);

		$group_id = $this->db->get()->result_array();
		//$group_id[] = $group_label;
		//$group_id[] = $this->db->last_query();

		if (isset($group_id[0]) && isset($group_id[0]['group_id']) && $group_id[0]['group_id'] != '')
			return $group_id[0]['group_id'];
		else if ($group_label != '')
			return $this->add_group($group_label);
		else return '';
	}

	function add_group($group_label)
	{
		$this->db->insert('customer_groups', array('label' => trim($group_label), 'course_id' => $this->session->userdata('course_id')));
		return $this->db->insert_id();
	}

	function save_group_name($group_id, $group_label, $item_cost_plus_percent_discount = null)
	{
		if($item_cost_plus_percent_discount === '' || 
			$item_cost_plus_percent_discount === null || 
			$item_cost_plus_percent_discount === false
		){
			$item_cost_plus_percent_discount = null;
		}else{
			$item_cost_plus_percent_discount = round($item_cost_plus_percent_discount, 2);
		}

		$this->db->where('group_id', $group_id);
		return $this->db->update('customer_groups', array(
			'label' => $group_label, 
			'item_cost_plus_percent_discount' => $item_cost_plus_percent_discount
		));
	}

	function delete_group($group_id)
	{
		$this->db->delete('customer_groups', array('group_id' => $group_id));
		$this->db->delete('customer_group_members', array('group_id' => $group_id));
	}

	// PASS Functionality
	function get_pass_info($customer_id = '')
	{
        if ($customer_id == -1) {
            return [];
        }

		$course_ids = array();
		$this->get_linked_course_ids($course_ids, $this->session->userdata('course_id'));

		// $this->db->where_in('foreup_customers.course_id', array_values($course_ids));
// 	    
		// $this->db->select('foreup_customer_passes.pass_id AS pass_id, start_date, expiration, label, foreup_customers.person_id, count(foreup_customers.person_id) AS member_count');
		// $this->db->from('customer_passes');
		// if ($customer_id == '')
		// $this->db->join('customer_pass_members', "customer_passes.pass_id = customer_pass_members.pass_id", 'LEFT');
		// else
		// $this->db->join('customer_pass_members', "customer_passes.pass_id = customer_pass_members.pass_id AND foreup_customer_pass_members.person_id = '$customer_id'", 'LEFT');
		// $this->db->join('customers', "customers.person_id = customer_pass_members.person_id");
		//$this->db->where('course_id', $this->session->userdata('course_id'));
		//$this->db->where('deleted', 0);
		//$this->db->group_by('pass_id');

		//return $this->db->get()->result_array();


		$course_id_string = "('" . implode("','", $course_ids) . "')";

		$query = $this->db->query(
			"SELECT 
				`foreup_customer_passes`.`pass_id` AS pass_id, 
				GROUP_CONCAT(IF(foreup_customer_pass_members.person_id = '$customer_id', start_date, '')) AS start_date, 
				GROUP_CONCAT(IF(foreup_customer_pass_members.person_id = '$customer_id', expiration, '')) AS expiration, 
				`label`, 
				SUM(CASE WHEN foreup_customer_pass_members.person_id = '$customer_id' THEN 1 ELSE 0 END) AS is_member, 
				SUM(CASE WHEN foreup_customers.deleted = 0 AND foreup_customers.course_id IN $course_id_string THEN 1 ELSE 0 END) AS member_count
			FROM (`foreup_customer_passes`)
			LEFT JOIN `foreup_customer_pass_members` 
				ON `foreup_customer_passes`.`pass_id` = `foreup_customer_pass_members`.`pass_id`
			LEFT JOIN `foreup_customers` 
				ON `foreup_customers`.`person_id` = `foreup_customer_pass_members`.`person_id`
			WHERE 
				`foreup_customer_passes`.`course_id` IN $course_id_string 
			GROUP BY `pass_id`
			ORDER BY `is_member` DESC"
		);

		//echo $this->db->last_query();
		return $query->result_array();
	}

	function get_multiple_pass_info(array $pass_ids, $filter = 'none')
	{
		$this->db->from('customer_pass_members');
		$this->db->join('people', 'people.person_id = customer_pass_members.person_id');
		$this->db->where_in('customer_pass_members.pass_id', $pass_ids);
		if ($filter == 'email') {
			$this->db->where('email !=', '');
			$this->db->group_by('email');
		} else if ($filter == 'phone') {
			$this->db->where('phone_number !=', '');
			$this->db->group_by('phone_number');
		}
		return $this->db->get();
	}

	function save_pass_memberships($passes_data, $person_id, $positives_only = false)
	{
		if (!$positives_only)
			$this->db->delete('customer_pass_members', array('person_id' => $person_id));
		foreach ($passes_data as $pass_id) {
			$this->db->insert('customer_pass_members', array('person_id' => $person_id, 'pass_id' => $pass_id, 'start_date' => $this->input->post('start_date_' . $pass_id), 'expiration' => $this->input->post('expiration_' . $pass_id)));
		}
	}

	function get_pass_id($pass_label)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids, $this->session->userdata('course_id'));

		$this->db->where_in('course_id', array_values($course_ids));

		$this->db->from('customer_passes');
		$this->db->where('label', trim($pass_label));
		//$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);

		$pass_id = $this->db->get()->result_array();
		//$group_id[] = $group_label;
		//$group_id[] = $this->db->last_query();

		if (isset($pass_id[0]) && isset($pass_id[0]['pass_id']) && $pass_id[0]['pass_id'] != '')
			return $pass_id[0]['pass_id'];
		else if ($pass_label != '')
			return $this->add_pass($pass_label);
		else return '';
	}

	function add_pass($pass_label)
	{
		$this->db->insert('customer_passes', array('label' => trim($pass_label), 'course_id' => $this->session->userdata('course_id')));
		return $this->db->insert_id();
	}

	function save_pass_name($pass_id, $pass_label)
	{
		$this->db->where('pass_id', $pass_id);
		return $this->db->update('customer_passes', array('label' => $pass_label));
	}

	function delete_pass($pass_id)
	{
		$this->db->delete('customer_passes', array('pass_id' => $pass_id));
		$this->db->delete('customer_pass_members', array('pass_id' => $pass_id));
	}

	/*
	Checks to see if Username is already in use
	*/
	function username_is_available($customer_id, $username)
	{
		if (empty($username) || empty($customer_id)) {
			return false;
		}
		$this->db->from('users');
		if (empty($username)) {
			return false;
		}
		$this->db->where('email', $username);
		if (!empty($customer_id)) {
			$this->db->where("person_id != {$customer_id}");
		}
		$this->db->limit(1);
		$results = $this->db->get();

		return ($results->num_rows == 0);
	}

	/*
	Send customer updated username and password
	*/
	function send_username_password_email($person_data, $customer_data, $password)
	{
		$data['person_data'] = $person_data;
		$data['customer_data'] = $customer_data;
		$data['password'] = $password;
		$course_info = $this->Course->get_info($this->session->userdata('course_id'));
		$data['course_info'] = $course_info;
		$data['customer_data']['username'] = $customer_data['email'];
		$view = $this->load->view('customers/username_password_updated', $data, true);


		send_sendgrid(
			$person_data['email'],
			$course_info->name,
			$this->load->view('customers/username_password_updated', $data, true),
			$course_info->email,
			$course_info->name
		);
	}

	function activate_online_booking($customers,$password)
	{
		$errors = [];
		$this->load->model("user");
		$this->load->model("customer");
		foreach($customers as $customer_id)
		{
			$customer_info = $this->customer->get_info($customer_id);
			try{
				if(!empty($customer_info->email)){
					$this->user->save($customer_id,[
						"email"=>$customer_info->email,
						"password"=>md5($password)
					]);
				}
			} catch(Exception $e){
				$errors[] = $customer_info->first_name." ".$customer_info->last_name." unable to create a online booking account.";
				http_response_code (200);
			}

		}
		return $errors;
	}

	function send_online_booking_email($username, $password, $email=false, $course_id = false)
	{
		if(empty($course_id)){
		    $course_id = $this->session->userdata('course_id');
        }
	    $course_info = $this->Course->get_info($course_id);

		$data = [
			"username"=>$username,
			"password"=>$password,
			"course_name"=>$course_info->name,
			"course_id"=>$course_info->course_id,
			"course_phone"=>$course_info->phone

		];

		send_sendgrid(
			$email?$email:$username,
			$course_info->name,
			$this->load->view('email_templates/new_account', $data,true),
			$course_info->email,
			$course_info->name
		);
		
	}

	/*
	Inserts or updates a customer
	*/
	function save(&$person_data, &$customer_data, $customer_id = false, &$giftcard_data = array(), &$groups_data = array(), &$passes_data = array(), $import = false, $log_entry = false)
	{
		// do we know a person_id?
		// if so, ensure it is consistent across the two variables that store it.
		if(!$customer_id && isset($person_data['person_id'])){
			$customer_id = $person_data['person_id'];
			if($customer_id == 0)return false;
		}
		elseif($customer_id && $customer_id > 0 && !isset($person_data['person_id'])){
			$person_data['person_id'] = $customer_id;
			$customer_data['person_id'] = $customer_id;
		}
		elseif($customer_id && $customer_id > 0 && isset($person_data['person_id']) &&
			$customer_id !== isset($person_data['person_id'])){
			// TODO: throw exception here due to ambiguous person_id
			return false;
		}

		$success = false;
		$this->load->model('user');

		$user_data = array();
		if (!empty($customer_data['username'])) {
			$user_data['email'] = $customer_data['username'];
			unset($customer_data['username']);
		}
		if (!empty($customer_data['password'])) {
			$user_data['password'] = $customer_data['password'];
			unset($customer_data['password']);
		}
		$unencrypted_password = isset($customer_data['unencrypted_password']) ? $customer_data['unencrypted_password'] : '';
		unset($customer_data['unencrypted_password']);

        $mass = false;
        if(!is_array($log_entry)) {

			if(isset($_SESSION['foreup']['emp_id']) && $_SESSION['foreup']['emp_id']*1>0) {
				// not mass upsert
				$act_type = 7; // upsert
				$emp_id = $_SESSION['foreup']['emp_id'] * 1; //$this->session->userdata('emp_id')
				$comments = 'Customer saved.';
				//TODO: Need a way to resolve which tables were actually updated
				$tbls = 'foreup_customers';
				$gmt = date('Y-m-d H:i:s');

				$log_entry = array(
					'editor_id' => $emp_id,
					'gmt_logged' => $gmt,
					'action_type' => $act_type,
					'comments' => $comments,
					'tables_updated' => $tbls
				);

				/* TODO: Please fix this query, causing a duplicate key error when making a 
					sale that has split payments and one of the payments being loyalty
				
				$this->db->query("INSERT INTO foreup_employee_audit_log (editor_id,action_type_id,comments,gmt_logged) VALUES ('$emp_id',$act_type,'$comments','$gmt')");
				$eal_id = $this->db->query("SELECT LAST_INSERT_ID() as id")->result_array()[0]['id'];
				$log_entry['employee_audit_log_id'] = $eal_id; */
			}
        }
        else {
            $mass = true;
            // pull into their variables
            $act_type = $log_entry['action_type'];
            $emp_id = $log_entry['editor_id'];
            $comments = $log_entry['comments'];
            //TODO: Need a way to resolve which tables were actually updated
            $tbls = $log_entry['tables_updated'];
            $gmt = $log_entry['gmt_logged'];
            $eal_id = $log_entry['employee_audit_log_id'];
        }

		if(isset($person_data['birthday'])&&is_string($person_data['birthday'])){
			$person_data['birthday']=date('Y-m-d', strtotime($person_data['birthday']));
		}

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		if (parent::save_person($person_data, $customer_id,$log_entry)) {
			if (isset($customer_data['price_class'])) {
				$customer_data['price_class'] = ($customer_data['price_class'] === 0 || $customer_data['price_class'] === null) ? '' : $customer_data['price_class'];
			}

			if (isset($customer_data['username']) && $this->username_exists($customer_data['username']) && !isset($customer_data['member'])) {
				$success = false;
			} else if (!$customer_id or !$this->exists($customer_id)) {
				$customer_id = $giftcard_data['customer_id'] = $customer_data['person_id'] = $person_data['person_id'];
                if(!$mass) {
                  $act_type = 3;
                }
				if (!$customer_data['course_id'] > 0)
					return false;

                // We need the customer to be created before we do the loyalty so we can pass the foreign key constraints
                $success = $this->db->insert('customers', $customer_data);

				// Get course info to set use_loyalty setting
				if (!isset($customer_data['use_loyalty'])) {
					$course_info = $this->Course->get_info($customer_data['course_id']);

					if (!empty($course_info->loyalty_auto_enroll) ? $course_info->loyalty_auto_enroll : false) {
					    // We need to auto enroll them.
                        $this->load->model('Loyalty_package');
                        $defaults = $this->Loyalty_package->get_defaults()->result_object();

                        if (count($defaults) > 0) {
                            // If there are default packages uses them
                            $customer_data['use_loyalty'] = 0;

                            $this->load->model('Customers_loyalty_package');
                            foreach ($defaults as $default) {
                                $package = [
                                    'course_id' => $customer_data['course_id'],
                                    'loyalty_package_id' => $default->id,
									'customer_id' => $customer_id
                                ];
                                $this->Customers_loyalty_package->save($package);
                            }
                            // foo_break_here();
                        } else {
                            // Otherwise just set the customer as use_loyalty
                            $customer_data['use_loyalty'] = 1;
                        }
                    } else {
                        $customer_data['use_loyalty'] = 0;
                    }

                    // Now we are actually updating the 'use_loyalty' field
                    $this->db->where('person_id', $customer_id);
                    $this->db->where('course_id', $customer_data['course_id']);
                    $this->db->update('customers', $customer_data);
				}
				$user_saved = $this->user->save($customer_id, $user_data);
				if ($unencrypted_password != '' && $user_saved) {
					// EMAIL USERNAME AND PASSWORD TO CUSTOMER
					$c_data = array_merge($customer_data, $user_data);
					$this->send_username_password_email($person_data, $c_data, $unencrypted_password);
				}

				if (isset($giftcard_data['giftcard_number']) && isset($giftcard_data['value']))
					$this->Giftcard->save($giftcard_data);

				if (isset($customer_data['account_balance']) && $customer_data['account_balance'] != 0) {
					$account_transaction_data = array(
						'course_id' => $this->session->userdata('course_id'),
						'trans_customer' => $customer_id,
						'trans_date' => date('Y-m-d H:i:s'),
						'trans_user' => $this->session->userdata('person_id'),
						'trans_comment' => 'Original Balance - Imported',
						'trans_description' => 'Customer Import',
						'trans_amount' => $customer_data['account_balance'],
						'running_balance' => $customer_data['account_balance']
					);
					$this->Account_transactions->insert('customer', $account_transaction_data);
				}
				if (isset($customer_data['member_account_balance']) && $customer_data['member_account_balance'] != 0) {
					$account_transaction_data = array(
						'course_id' => $this->session->userdata('course_id'),
						'trans_customer' => $customer_id,
						'trans_date' => date('Y-m-d H:i:s'),
						'trans_user' => $this->session->userdata('person_id'),
						'trans_comment' => 'Original Balance - Imported',
						'trans_description' => 'Customer Import',
						'trans_amount' => $customer_data['member_account_balance'],
						'running_balance' => $customer_data['member_account_balance']
					);
					$this->Account_transactions->insert('member', $account_transaction_data);
				}


			} else {
                if(!$mass) {
                    $act_type = 4;
                    $log_entry['action_type']=$act_type;
                }
				$course_ids = array();
				$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $this->config->item('course_id'));

				$this->db->where('person_id', $customer_id);
				$this->db->where_in('course_id', $course_ids);
				$success = $this->db->update('customers', $customer_data);

				if($this->config->item('marketing_texting')){
					$this->load->model("marketing_texting");
					$marketing_texting = new \Marketing_Texting();
					if(isset($person_data['cell_phone_number']) && !empty($person_data['cell_phone_number'])){
						$marketing_texting->updateMsisdn($customer_id,$this->config->item('course_id'),$person_data['cell_phone_number']);
					} else if (isset($person_data['phone_number']) && !empty($person_data['phone_number'])){
						$marketing_texting->updateMsisdn($customer_id,$this->config->item('course_id'),$person_data['phone_number']);
					} else {
						$marketing_texting->updateMsisdn($customer_id,$this->config->item('course_id'),$person_data['phone_number']);
					}
				}

				$user_saved = $this->user->save($customer_id, $user_data);
				if ($unencrypted_password != '' && $user_saved) {
					// EMAIL USERNAME AND PASSWORD TO CUSTOMER
					$c_data = array_merge($customer_data, $user_data);
					$this->send_username_password_email($person_data, $c_data, $unencrypted_password);
				}
			}

			if ($import) {
				$group_ids = array();
				foreach ($groups_data as $label) {
					$gid = $this->get_group_id($label);
					if ($gid != '')
						$group_ids[$gid] = $gid;
				}
				$groups_data = $group_ids;
			}

			if (count($groups_data) != 0) {
				// BECAUSE THE CUSTOMER SAVE HAPPENS IN SO MANY PLACES WE WANT TO SPECIFICALLY TELL THE MODEL TO CLEAR OUT USER GROUPS BEFORE WE DO
				$groups_data = ($groups_data == 'delete') ? array() : $groups_data;
				$this->save_group_memberships($groups_data, $customer_id);
			}

			if (count($passes_data) != 0) {
				// BECAUSE THE CUSTOMER SAVE HAPPENS IN SO MANY PLACES WE WANT TO SPECIFICALLY TELL THE MODEL TO CLEAR OUT USER PASSES BEFORE WE DO
				$passes_data = ($passes_data == 'delete') ? array() : $passes_data;
				$this->save_pass_memberships($passes_data, $customer_id);
			}

		}

        // successfully saved our customer!
        $pers_alt = $person_data['person_id']*1;

        // insert into person altered
        //TODO: FiX THIS
		//$this->db->query("INSERT INTO foreup_person_altered (employee_audit_log_id,person_edited,tables_updated) VALUES ($eal_id,$pers_alt,'$tbls')");

		$this->db->trans_complete();

        return $this->db->trans_status();

	}

	function validate_merge(&$message,$person_id_2)
	{
		$secondaries = $this->Customer_merge->get_secondary_accounts($person_id_2);

		if (count($secondaries) > 0) {
			$message = 'This duplicate account you are attempting to merge has been merged into previously and cannot be merged into another account';
			return false;
		}

		return true;
	}

	/*
	Merge customer
	 */
	function merge($person_id_1,$person_id_2)
	{
		// Merge person data
		$this->merge_account_info($person_id_1,$person_id_2);
		parent::merge($person_id_1,$person_id_2);
        $this->merge_households($person_id_1,$person_id_2);
        $this->merge_groups($person_id_1,$person_id_2);
		$this->merge_passes($person_id_1,$person_id_2);
		$this->merge_miscellaneous($person_id_1,$person_id_2);
		$success = $this->db->trans_status();
		$message = '';
		if (!$success) {
			$message = 'There was an error with the merge.';
		}

		//If secondary account doesn't belong to multiple courses then delete the user credentials
		$count = $this->db->from("customers")
			->where("person_id =",$person_id_2)
			->count_all_results();
		if($count == 1){
			$this->db->from("users")
				->where("person_id =",$person_id_2)
				->delete();
		}

		$response = array('success' => $success, 'message' => $message);
		echo json_encode($response);
	}

	/*
	Merge account info
	 */
	function merge_account_info($person_id_1,$person_id_2)
	{

		$course_id = $this->config->item('course_id');
		$course_ids = array();
		$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);

		$original_person_info = $this->get_info($person_id_1, $course_id);
		$merge_person_info = $this->get_info($person_id_2, $course_id);
		// Instead of deleting the individual, we're just going to mark them to not show in customer searches and remove their account number
		$this->db->where('person_id', $person_id_2);
		$this->db->where('course_id', $course_id);
		$this->db->update('customers', array('hide_from_search' => 1, 'account_number' => null));

		// Save the merge action to the database
		$merge_data = array(
			'datetime' => '',
			'employee_id' => $this->session->userdata('person_id'),
			'customer_id' => $person_id_1,
			'duplicate_customer_id' => $person_id_2,
			'original_customer_data' => json_encode($original_person_info),
			'course_id' => $course_id
		);
		$this->Customer_merge->save($merge_data);
		$this->merge_id = $merge_data['merge_id'];

		$updated_info = array();

		$updated_info['opt_out_email'] = $original_person_info->opt_out_email || $merge_person_info->opt_out_email ? 1 : 0;
		$updated_info['unsubscribe_all'] = $original_person_info->unsubscribe_all || $merge_person_info->unsubscribe_all ? 1 : 0;
		$updated_info['status_flag'] = $original_person_info->status_flag;
		if ($original_person_info->status_flag == 0) {
			$updated_info['status_flag'] = $merge_person_info->status_flag;
		} else if ($merge_person_info->status_flag == 0) {

		} else if ($original_person_info->status_flag > $merge_person_info->status_flag) {
			$updated_info['status_flag'] = $merge_person_info->status_flag;
		}

		if ((int)$this->input->post('account_number_box') == 2) {
			$updated_info['account_number'] = $merge_person_info->account_number;
			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'account_number',
				'original_value' => $original_person_info->account_number,
				'new_value' => $merge_person_info->account_number
			);
			$this->Customer_merge->record_change($merge_change_data);
		}

		if ((int)$this->input->post('price_class_box') == 2) {
			$updated_info['price_class'] = $merge_person_info->price_class;
			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'price_class',
				'original_value' => $original_person_info->price_class,
				'new_value' => $merge_person_info->price_class
			);
			$this->Customer_merge->record_change($merge_change_data);
		}

		if ($this->input->post('discount_box') == 2) {
			$updated_info['discount'] = $merge_person_info->discount;
			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'discount',
				'original_value' => $original_person_info->discount,
				'new_value' => $merge_person_info->discount
			);
			$this->Customer_merge->record_change($merge_change_data);
		}

		if ($this->input->post('non_taxable_box') == 2) {
			$updated_info['taxable'] = $merge_person_info->taxable;
			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'taxable',
				'original_value' => $original_person_info->taxable,
				'new_value' => $merge_person_info->taxable
			);
			$this->Customer_merge->record_change($merge_change_data);
		}

		if ($this->input->post('member_box') == 2) {
			$updated_info['member'] = $merge_person_info->member;
			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'member',
				'original_value' => $original_person_info->member,
				'new_value' => $merge_person_info->member
			);
			$this->Customer_merge->record_change($merge_change_data);
		}

		if ($this->input->post('food_minimum_box') == 2) {
			// Switch all food minimums
			$this->db->select('minimum_charge_id');
			$this->db->from('customer_minimum_charges');
			$this->db->where('person_id', $person_id_2);
			$minimums = $this->db->get()->result_array();

			$this->db->select('minimum_charge_id');
			$this->db->from('customer_minimum_charges');
			$this->db->where('person_id', $person_id_1);
			$original_minimums = $this->db->get()->result_array();

			if (COUNT($minimums) > 0) {
				$merge_change_data = array(
					'merge_id' => $this->merge_id,
					'type' => 'minimum',
					'original_value' => json_encode($original_minimums),
					'new_value' => json_encode($minimums)
				);
				$this->Customer_merge->record_change($merge_change_data);

				foreach ($minimums as $minimum) {
					// Delete existing minimums and insert them for the primary customer because we're not deleting the duplicate customer, just hiding them
					// This whill have
					$this->db->query("DELETE FROM foreup_customer_minimum_charges WHERE person_id = $person_id_2 AND minimum_charge_id = {$minimum['minimum_charge_id']} LIMIT 1 ");
					$this->db->query("INSERT IGNORE INTO foreup_customer_minimum_charges (person_id, minimum_charge_id, date_added) VALUES ($person_id_1, {$minimum['minimum_charge_id']}, '" . date('Y-m-d') . "'})");
				}
			}

		}

		// This is required... always transfer member account
		if (true) { //$this->input->post('member_account_balance_box_2')) {
			// Transfer member account balance
			$original_values = $update_values = array();
			$original_values['member_account_balance'] = $original_person_info->member_account_balance;
			$update_values['member_account_balance'] = $merge_person_info->member_account_balance;
			$updated_info['member_account_balance'] = $original_person_info->member_account_balance + $merge_person_info->member_account_balance;

			// Update settings
			$original_values['member_account_balance_allow_negative'] = $original_person_info->member_account_balance_allow_negative;
			$update_values['member_account_balance_allow_negative'] = $updated_info['member_account_balance_allow_negative'] = ($merge_person_info->member_account_balance_allow_negative || $original_person_info->member_account_balance_allow_negative) ? 1 : 0;
			$original_values['member_account_limit'] = $original_person_info->member_account_limit;
			$update_values['member_account_limit'] = $updated_info['member_account_limit'] = ($merge_person_info->member_account_limit < $original_person_info->member_account_limit) ? $merge_person_info->member_account_limit : $original_person_info->member_account_limit;


			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'member_account_info',
				'original_value' => json_encode(array()),
				'new_value' => json_encode($update_values)
			);
			$this->Customer_merge->record_change($merge_change_data);

			// Update all transactions
			$this->db->select('trans_id');
			$this->db->from('account_transactions');
			$this->db->where('account_type', 'member');
			$this->db->where('trans_customer', $person_id_2);
			$this->db->where('course_id', $course_id);
			$new_transactions = $this->db->get()->result_array();

			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'member_account_transactions',
				'original_value' => json_encode(array()),
				'new_value' => json_encode($new_transactions)
			);
			$this->Customer_merge->record_change($merge_change_data);

			$this->db->where('account_type', 'member');
			$this->db->where('trans_customer', $person_id_2);
			$this->db->where('course_id', $course_id);
			$this->db->update('account_transactions', array('trans_customer' => $person_id_1));
		}

		// This is required... always transfer customer account
		if (true) { //$this->input->post('customer_account_balance_box_2')) {
			// Transfer customer account balance
			$original_values = $update_values = array();
			$original_values['account_balance'] = $original_person_info->account_balance;
			$update_values['account_balance'] = $merge_person_info->account_balance;
			$updated_info['account_balance'] = $original_person_info->account_balance + $merge_person_info->account_balance;

			// Update settings
			$original_values['account_balance_allow_negative'] = $original_person_info->account_balance_allow_negative;
			$update_values['account_balance_allow_negative'] = $updated_info['account_balance_allow_negative'] = ($merge_person_info->account_balance_allow_negative || $original_person_info->account_balance_allow_negative) ? 1 : 0;
			$original_values['account_limit'] = $original_person_info->account_limit;
			$update_values['account_limit'] = $updated_info['account_limit'] = ($merge_person_info->account_limit < $original_person_info->account_limit) ? $merge_person_info->account_limit : $original_person_info->account_limit;

			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'customer_account_info',
				'original_value' => json_encode(array()),
				'new_value' => json_encode($update_values)
			);
			$this->Customer_merge->record_change($merge_change_data);

			// Update all transactions
			$this->db->select('trans_id');
			$this->db->from('account_transactions');
			$this->db->where('account_type', 'customer');
			$this->db->where('course_id', $course_id);
			$this->db->where('trans_customer', $person_id_2);
			$new_transactions = $this->db->get()->result_array();

			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'customer_account_transactions',
				'original_value' => json_encode(array()),
				'new_value' => json_encode($new_transactions)
			);
			$this->Customer_merge->record_change($merge_change_data);

			$this->db->where('account_type', 'customer');
			$this->db->where('trans_customer', $person_id_2);
			$this->db->where('course_id', $course_id);
			$this->db->update('account_transactions', array('trans_customer' => $person_id_1));
		}
		// This is required... always transfer invoice balance
		if (true) {
			// Transfer invoice account balance
			$original_values = $update_values = array();
			$original_values['invoice_balance'] = $original_person_info->invoice_balance;
			$update_values['invoice_balance'] = $merge_person_info->invoice_balance;
			$updated_info['invoice_balance'] = $original_person_info->invoice_balance + $merge_person_info->invoice_balance;
		}
		// This is required... always transfer loyalty
		if (true) {
			// Transfer loyalty balance
			$original_values = $update_values = array();
			$original_values['loyalty_points'] = $original_person_info->loyalty_points;
			$update_values['loyalty_points'] = $merge_person_info->loyalty_points;
			$updated_info['loyalty_points'] = $original_person_info->loyalty_points + $merge_person_info->loyalty_points;
		}
		$this->db->where('person_id', $person_id_1);
		$this->db->where('course_id', $course_id);
		return $this->db->update('customers', $updated_info);
	}

	/*
	Merge households
	 */
	function merge_households($person_id_1,$person_id_2)
	{
        $this->load->model('Household');
        $original_members = $this->Household->get_members($person_id_1);
        $new_members = $this->Household->get_members($person_id_2);
        $household_to_delete = $this->Household->get_id($person_id_2);
        $all_members = [];
        $original_member_ids = [];
        foreach ($original_members as $omem){
            $all_members[] = $omem['household_member_id'];
            $original_member_ids[] = $omem['household_member_id'];
        }
        foreach ($new_members as $nmem){
            if ($nmem['household_member_id'] != $person_id_1) {
                $all_members[] = $nmem['household_member_id'];
            }
        }
        if (count($new_members) > 0) {
            $this->Household->delete($household_to_delete);
            $this->Household->save($all_members, $person_id_1);
        }

		$merge_change_data = array(
			'merge_id' => $this->merge_id,
			'type' => 'households',
			'original_value' => json_encode($original_member_ids),
			'new_value' => json_encode($all_members)
		);
		$this->Customer_merge->record_change($merge_change_data);

	}

    /*
    Merge groups
     */
    function merge_groups($person_id_1,$person_id_2)
    {

        $original_groups = $this->db->query("SELECT gm.group_id FROM foreup_customer_group_members AS gm LEFT JOIN foreup_customer_groups AS cg ON cg.group_id = gm.group_id WHERE person_id = $person_id_1 AND course_id = {$this->config->item('course_id')}")->result_array();
        $groups = $this->get_customer_groups($person_id_2);
        $group_id_array = array();

        if ($this->input->post('groups_box_2')) {
            // Add person 2 groups

            foreach ($groups as $group) {
                $this->db->query("
					INSERT IGNORE INTO foreup_customer_group_members (group_id, person_id) VALUES ('{$group['group_id']}', '$person_id_1');
				");
                $group_id_array[] = $group['group_id'];
            }
        }

        $merge_change_data = array(
            'merge_id' => $this->merge_id,
            'type' => 'groups',
            'original_value' => json_encode($original_groups),
            'new_value' => json_encode($group_id_array)
        );
        $this->Customer_merge->record_change($merge_change_data);

    }

	/*
	Merge passes
	 */
	function merge_passes($person_id_1,$person_id_2)
	{

		if ($this->input->post('passes_box_2')) {
			// Add person 2 passes
			$this->db->select('pass_id');
			$this->db->from("passes");
			$this->db->where('customer_id', $person_id_2);
			$this->db->where('course_id', $this->config->item('course_id'));
			$passes = $this->db->get()->result_array();

			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'passes',
				'original_value' => json_encode(array()),
				'new_value' => json_encode($passes)
			);
			$this->Customer_merge->record_change($merge_change_data);

			$this->db->where('customer_id', $person_id_2);
			$this->db->where('course_id', $this->config->item('course_id'));
			$this->db->update("passes", array('customer_id' => $person_id_1));

			foreach($passes as $pass){
				$this->db->where('person_id', $person_id_2);
				$this->db->where('pass_id', $pass['pass_id']);
				$this->db->update("pass_customers", array('person_id' => $person_id_1));
			}

		}
	}

	/*
	Merge Miscellaneous
	 */
	function merge_miscellaneous($person_id_1,$person_id_2)
	{
		// All required
		$this->update_old_sales($person_id_1,$person_id_2);
		$this->update_tee_times($person_id_1,$person_id_2);
		$this->transfer_gift_cards($person_id_1,$person_id_2);
		$this->transfer_punch_cards($person_id_1,$person_id_2);
		$this->transfer_credit_cards($person_id_1,$person_id_2);
		$this->transfer_invoices($person_id_1,$person_id_2);
	}

	/*
	Update old sales
	*/
	function update_old_sales($person_id_1,$person_id_2)
	{
		// Required
		if (true)//$this->input->post('update_sales_box'))
		{

			$this->db->select('sale_id');
			$this->db->from('sales');
			$this->db->where('customer_id', $person_id_2);
			$this->db->where('course_id', $this->config->item('course_id'));
			$sale_ids = $this->db->get()->result_array();

			if (COUNT($sale_ids) > 0) {
				$merge_change_data = array(
					'merge_id' => $this->merge_id,
					'type' => 'sales',
					'original_value' => '',
					'new_value' => json_encode($sale_ids)
				);
				$this->Customer_merge->record_change($merge_change_data);


				$this->db->where('customer_id', $person_id_2);
				$this->db->where('course_id', $this->config->item('course_id'));
				$this->db->update('sales', array('customer_id' => $person_id_1));
			}
		}
	}

	/*
	Update tee times
	*/
	function update_tee_times($person_id_1,$person_id_2)
	{
		// Required
		if (true) //$this->input->post('update_tee_times_box'))
		{
			$this->load->model('Teesheet');
			$tee_sheets = $this->Teesheet->get_teesheets($this->config->item('course_id'));
			$tee_sheet_id_array = array();
			foreach ($tee_sheets as $tee_sheet) {
				$tee_sheet_id_array[] = $tee_sheet['teesheet_id'];
			}

			$tee_time_ids = array();
			if (COUNT($tee_sheet_id_array) > 0) {
				$this->db->select('TTID');
				$this->db->from('teetime');
				$this->db->where("(person_id = $person_id_2 OR person_id_2 = $person_id_2 OR person_id_3 = $person_id_2 OR person_id_4 = $person_id_2 OR person_id_5 = $person_id_2)");
				$this->db->where_in('teesheet_id', $tee_sheet_id_array);
				$tee_time_ids = $this->db->get()->result_array();
			}

			if (COUNT($tee_time_ids) > 0) {
				$merge_change_data = array(
					'merge_id' => $this->merge_id,
					'type' => 'tee times',
					'original_value' => '',
					'new_value' => json_encode($tee_time_ids)
				);
				$this->Customer_merge->record_change($merge_change_data);

				$this->db->where('person_id', $person_id_2);
				$this->db->where_in('teesheet_id', $tee_sheet_id_array);
				$this->db->update('teetime', array('person_id' => $person_id_1));

				$this->db->where('person_id_2', $person_id_2);
				$this->db->where_in('teesheet_id', $tee_sheet_id_array);
				$this->db->update('teetime', array('person_id_2' => $person_id_1));

				$this->db->where('person_id_3', $person_id_2);
				$this->db->where_in('teesheet_id', $tee_sheet_id_array);
				$this->db->update('teetime', array('person_id_3' => $person_id_1));

				$this->db->where('person_id_4', $person_id_2);
				$this->db->where_in('teesheet_id', $tee_sheet_id_array);
				$this->db->update('teetime', array('person_id_4' => $person_id_1));

				$this->db->where('person_id_5', $person_id_2);
				$this->db->where_in('teesheet_id', $tee_sheet_id_array);
				$this->db->update('teetime', array('person_id_5' => $person_id_1));
			}
		}
	}

	/*
	Transfer gift cards
	*/
	function transfer_gift_cards($person_id_1,$person_id_2)
	{
		// Required
		if (true)//$this->input->post('transfer_gift_cards_box'))
		{

			$this->db->select('giftcard_id');
			$this->db->from('giftcards');
			$this->db->where('customer_id', $person_id_2);
			$this->db->where('course_id', $this->config->item('course_id'));
			$giftcard_ids = $this->db->get()->result_array();
			if (COUNT($giftcard_ids) > 0) {
				$merge_change_data = array(
					'merge_id' => $this->merge_id,
					'type' => 'gift_cards',
					'original_value' => '',
					'new_value' => json_encode($giftcard_ids)
				);
				$this->Customer_merge->record_change($merge_change_data);

				$this->db->update('giftcards', array('customer_id' => $person_id_1), array('customer_id' => $person_id_2, 'course_id' => $this->config->item('course_id')));
			}
		}

	}

	/*
	Transfer punch cards
	*/
	function transfer_punch_cards($person_id_1,$person_id_2)
	{
		// Required
		if (true)//$this->input->post('transfer_punch_cards_box'))
		{
			$this->db->select('punch_card_id');
			$this->db->from('punch_cards');
			$this->db->where('customer_id', $person_id_2);
			$this->db->where('course_id', $this->config->item('course_id'));
			$punch_card_ids = $this->db->get()->result_array();

			if (COUNT($punch_card_ids) > 0) {
				$merge_change_data = array(
					'merge_id' => $this->merge_id,
					'type' => 'punch_cards',
					'original_value' => '',
					'new_value' => json_encode($punch_card_ids)
				);
				$this->Customer_merge->record_change($merge_change_data);

				$this->db->where('customer_id', $person_id_2);
				$this->db->where('course_id', $this->config->item('course_id'));
				$this->db->update('punch_cards', array('customer_id' => $person_id_1));
			}
		}
	}

	/*
	Transfer credit cards
	*/
	function transfer_credit_cards($person_id_1,$person_id_2)
	{
		// Required
		if (true)//$this->input->post('transfer_credit_cards_box'))
		{
			$this->db->select("credit_card_id");
			$this->db->from('customer_credit_cards');
			$this->db->where('customer_id', $person_id_2);
			$this->db->where('course_id', $this->config->item('course_id'));
			$credit_cards_ids = $this->db->get()->result_array();

			if (COUNT($credit_cards_ids) > 0) {
				$merge_change_data = array(
					'merge_id' => $this->merge_id,
					'type' => 'credit_cards',
					'original_value' => '',
					'new_value' => json_encode($credit_cards_ids)
				);
				$this->Customer_merge->record_change($merge_change_data);

				$this->db->where('customer_id', $person_id_2);
				$this->db->where('course_id', $this->config->item('course_id'));
				$this->db->update('customer_credit_cards', array('customer_id' => $person_id_1));
			}
		}
	}

	/*
	Transfer invoices
	*/
	function transfer_invoices($person_id_1,$person_id_2)
	{
		// Required
		if (true) //$this->input->post('transfer_invoices_box'))
		{
			$this->db->select('invoice_id');
			$this->db->from('invoices');
			$this->db->where('person_id', $person_id_2);
			$this->db->where('course_id', $this->config->item('course_id'));
			$invoice_ids = $this->db->get()->result_array();

			if (COUNT($invoice_ids) > 0) {
				$merge_change_data = array(
					'merge_id' => $this->merge_id,
					'type' => 'invoices',
					'original_value' => '',
					'new_value' => json_encode($invoice_ids)
				);
				$this->Customer_merge->record_change($merge_change_data);

				// Invoices
				$this->db->where('person_id', $person_id_2);
				$this->db->where('course_id', $this->config->item('course_id'));
				$this->db->update('invoices', array('person_id' => $person_id_1));
			}

			$this->db->select('billing_id');
			$this->db->from('customer_billing');
			$this->db->where('person_id', $person_id_2);
			$this->db->where('course_id', $this->config->item('course_id'));
			$billing_ids = $this->db->get()->result_array();

			if (COUNT($billing_ids) > 0) {
				$merge_change_data = array(
					'merge_id' => $this->merge_id,
					'type' => 'billings',
					'original_value' => '',
					'new_value' => json_encode($billing_ids)
				);
				$this->Customer_merge->record_change($merge_change_data);
				// Recurring billings
				$this->db->where('person_id', $person_id_2);
				$this->db->where('course_id', $this->config->item('course_id'));
				$this->db->update('customer_billing', array('person_id' => $person_id_1));
			}
		}

	}

	function get_linked_course_ids(&$course_ids, $course_id = false)
	{
		$this->load->model('course');
		return $this->course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);
	}

	/*
	Deletes one customer
	*/
	function delete($customer_id)
	{
		$this->db->delete('customer_group_members', array('person_id' => $customer_id));
		//$course_id = '';
		// if (!$this->permissions->is_super_admin())
		$course_ids = array();
		$this->get_linked_course_ids($course_ids);
		$this->db->where_in('customers.course_id', array_values($course_ids));

		$this->db->where("person_id", $customer_id);
		return $this->db->update('customers', array('deleted' => 1, 'account_number' => null));//,'username'=>null));
	}

	/*
	Deletes a list of customers
	*/
	function delete_list($customer_ids)
	{
		$this->db->where_in('person_id', $customer_ids);
		$this->db->delete('customer_group_members');

		$course_ids = array();
		$this->get_linked_course_ids($course_ids);

		$this->db->where_in('person_id', $customer_ids);
		$this->db->update('customer_billing', array('deleted' => 1));

		$this->db->where_in('person_id', $customer_ids);
		$this->db->update('invoices', array('deleted' => 1));

		//When customer belongs to only a single course, delete online credentials
		$query = $this->db->from("users")
			->join("customers","customers.person_id = users.person_id")
			->group_by("users.person_id")
			->select("COUNT(DISTINCT(course_id)) as count,users.person_id")
			->where_in('users.person_id', $customer_ids)
			->get();
		$users_to_delete = [];
		foreach($query->result() as $row){
			if($row->count == 1){
				$users_to_delete[] = $row->person_id;
			}
		}
		if(count($users_to_delete) > 0){
			$this->db->where_in('person_id', $users_to_delete);
			$this->db->delete('users');
		}


		$this->db->where_in('customers.course_id', array_values($course_ids));
		$this->db->where_in('person_id', $customer_ids);


		return $this->db->update('customers', array('deleted' => 1, 'account_number' => null, 'invoice_balance' => null));//,'username'=>null));
	}

	function undelete($person_id){
		return $this->db->query("UPDATE foreup_customers SET deleted = 0 WHERE person_id = $person_id");
	}

	/*
	Get search suggestions to find customers
	*/
	function get_search_suggestions($search, $limit = 25, $show_deleted = 0)
	{
		$course_id = '';
		if (!$this->permissions->is_super_admin()) {
			//$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
			$course_ids = join(',', $course_ids);
			$course_id = "foreup_customers.course_id IN ({$course_ids})";
		}
		$suggestions = array();

		$this->db->select('first_name, last_name');
		$this->db->from('customers');
		$this->db->join('people', 'customers.person_id=people.person_id');
		$this->db->where("(first_name LIKE '" . $this->db->escape_like_str($search) . "%' or
		last_name LIKE '" . $this->db->escape_like_str($search) . "%' or
		CONCAT(`first_name`,' ',`last_name`) LIKE '%" . $this->db->escape_like_str($search) . "%') ");
		if(!$show_deleted)
			$this->db->where('deleted',0);
		$this->db->where('hide_from_search', 0);
		if (!$this->permissions->is_super_admin())
			$this->db->where($course_id);
		$this->db->order_by("last_name asc, first_name asc");
		$by_name = $this->db->get();
		foreach ($by_name->result() as $row) {
			$suggestions[] = array('label' => $row->first_name . ' ' . $row->last_name);
		}

		if (count($suggestions) < $limit) {
			$this->db->select('email');
			$this->db->from('customers');
			$this->db->join('people', 'customers.person_id=people.person_id');
			if(!$show_deleted)
				$this->db->where('deleted',0);
			$this->db->where('hide_from_search', 0);
			if (!$this->permissions->is_super_admin())
				$this->db->where($course_id);
			$this->db->like("email", $search, 'after');
			$this->db->order_by("email", "asc");
			$by_email = $this->db->get();
			foreach ($by_email->result() as $row) {
				$suggestions[] = array('label' => $row->email);
			}
		}
        if (count($suggestions) < $limit) {
            $this->db->select('phone_number');
            $this->db->from('customers');
            $this->db->join('people', 'customers.person_id=people.person_id');
            if(!$show_deleted)
                $this->db->where('deleted',0);
            $this->db->where('hide_from_search', 0);
            if (!$this->permissions->is_super_admin())
                $this->db->where($course_id);
            $this->db->like("phone_number", $search, 'after');
            $this->db->order_by("phone_number", "asc");
            $by_phone = $this->db->get();
            foreach ($by_phone->result() as $row) {
                $suggestions[] = array('label' => $row->phone_number);
            }
        }
        if (count($suggestions) < $limit) {
            $this->db->select('cell_phone_number');
            $this->db->from('customers');
            $this->db->join('people', 'customers.person_id=people.person_id');
            if(!$show_deleted)
                $this->db->where('deleted',0);
            $this->db->where('hide_from_search', 0);
            if (!$this->permissions->is_super_admin())
                $this->db->where($course_id);
            $this->db->like("cell_phone_number", $search, 'after');
            $this->db->order_by("cell_phone_number", "asc");
            $by_phone = $this->db->get();
            foreach ($by_phone->result() as $row) {
                $suggestions[] = array('label' => $row->cell_phone_number);
            }
        }
        if (count($suggestions) < $limit) {
			$this->db->select('account_number');
			$this->db->from('customers');
			$this->db->join('people', 'customers.person_id=people.person_id');
			if(!$show_deleted)
				$this->db->where('deleted',0);
			$this->db->where('hide_from_search', 0);
			if (!$this->permissions->is_super_admin())
				$this->db->where($course_id);
			$this->db->like("account_number", $search, 'after');
			$this->db->order_by("account_number", "asc");
			$by_account_number = $this->db->get();
			foreach ($by_account_number->result() as $row) {
				$suggestions[] = array('label' => $row->account_number);
			}
		}

		// $this->db->from('customers');
		// $this->db->join('people','customers.person_id=people.person_id');
		// $this->db->where("deleted = 0 $course_id");
		// $this->db->like("company_name",$search);
		// $this->db->order_by("company_name", "asc");
		// $by_company_name = $this->db->get();
		// foreach($by_company_name->result() as $row)
		// {
		// $suggestions[]=array('label'=> $row->company_name);
		// }

		//only return $limit suggestions
		if (count($suggestions > $limit)) {
			$suggestions = array_slice($suggestions, 0, $limit);
		}
		return $suggestions;

	}

	// GET CUSTOMER TEE TIME STATS ETC
	function get_stats($customer_id, $start = false, $end = false)
	{


		$now = date('YmdHi') - 1000000;
		$this_month = date('Ym010000') - 1000000;
		$this_year = date('Y00010000');
		$start = $end = '';
		if ($start)
			$start = " AND start > " . (date('Ymd0000', strtodate($start)) - 1000000);
		if ($end)
			$end = " AND start < " . date('Ymd2399', strtodate($end)) - 1000000;

		$this->load->model("Teesheet");
		$tee_sheets = $this->Teesheet->get_teesheets($this->config->item('course_id'));
		$teesheet_ids = array_map(function($o) { return $o['teesheet_id']; }, $tee_sheets);
		$teesheet_ids = implode(",", $teesheet_ids);
		// GET MOST RECENT RESERVATION

		$recent_reservation = $this->db->query("SELECT MAX(start) as start
			FROM  (SELECT * FROM foreup_teetime WHERE person_id = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_2 = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_3 = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_4 = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_5 = $customer_id) AS t
            WHERE status != 'deleted'
            AND teesheet_id IN ($teesheet_ids)
			AND TTID LIKE '____________________'
			")->row_array();
		// GET MOST RECENT NO SHOW

		$recent_no_show = $this->db->query("SELECT MAX(start) as start
			FROM (SELECT * FROM foreup_teetime WHERE person_id = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_2 = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_3 = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_4 = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_5 = $customer_id) AS t
			WHERE status != 'deleted'
			AND TTID LIKE '____________________'
			AND teesheet_id IN ($teesheet_ids)
			AND paid_player_count = 0
			AND start < $now
			")->row_array();


		// GET MONTH TO DATE RESERVATIONS
		// GET MONTH TO DATE PEOPLE
		// GET YEAR TO DATE RESERVATIONS
		// GET YEAR TO DATE NO SHOWS

		$query = $this->db->query("SELECT 
			COUNT(DISTINCT start) AS ytd_booked, COUNT(DISTINCT CASE WHEN paid_player_count = 0 THEN start END) AS ytd_no_shows, SUM(player_count) AS ytd_players_booked, SUM(IF(paid_player_count = 0, player_count, 0 )) AS no_shows_players,
			COUNT(DISTINCT CASE WHEN start > $this_month THEN start END) AS m_booked, COUNT(DISTINCT CASE WHEN paid_player_count = 0 AND start > $this_month THEN start END) AS m_no_shows, SUM(IF(start>$this_month, player_count, 0)) AS m_players_booked
			FROM (SELECT * FROM foreup_teetime WHERE person_id = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_2 = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_3 = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_4 = $customer_id
						UNION SELECT * FROM foreup_teetime WHERE person_id_5 = $customer_id) AS t
			WHERE status != 'deleted'
			AND teesheet_id IN ($teesheet_ids)
			AND start > $this_year
			AND TTID LIKE '____________________'");
		//echo $this->db->last_query();
		$results = $query->row_array();

		$results['recent_reservation'] = $recent_reservation['start'];
		$results['recent_no_show'] = $recent_no_show['start'];

		return $results;
	}

	function format_phone($number)
    {
        if($this->config->item("currency_symbol") == "$"){
            return "(" . substr($number, 0, 3) . ") " . substr($number, 3, 3) . "-" . substr($number, 6);
        }
        return $number;
    }

	/*
	Get search suggestions to find customers
	*/
	function get_customer_search_suggestions($search, $limit = 25, $type = '')
	{
		$split_name = explode(',', $search);
		$added_search_sql = '';
		if (count($split_name) > 1) {
			$first_name = trim($split_name[1]);
			$last_name = trim($split_name[0]);
			$added_search_sql = " or (first_name LIKE '{$this->db->escape_like_str($first_name)}%' AND last_name LIKE '{$this->db->escape_like_str($last_name)}%')";
		} else {
			$split_name = explode(' ', $search);
			if (count($split_name) > 1) {
				$first_name = trim($split_name[0]);
				$last_name = trim($split_name[1]);
				$added_search_sql = " or (first_name LIKE '{$this->db->escape_like_str($first_name)}%' AND last_name LIKE '{$this->db->escape_like_str($last_name)}%')";
			}
		}
		//Clean search term
		$original_search_term = $search;
		$search = str_replace(array('(', ')', '-', '_', ' ',','), '',$search);

		$course_ids = array();
		$this->get_linked_course_ids($course_ids);
		$course_id_list = join(',', $course_ids);
		$course_id = "AND foreup_customers.course_id IN ({$course_id_list})";

		$suggestions = array();

		if ($type == 'last_name' || $type == '' || $type == 'ln_and_pn') {
			$this->db->select('foreup_people.person_id as person_id, last_name, first_name, phone_number, email, zip, price_class, status_flag, people.comments');
			$this->db->from('customers');
			$this->db->join('people', 'customers.person_id=people.person_id');
			$this->db->where("(first_name LIKE '" . $this->db->escape_like_str($original_search_term) . "%' or
			last_name LIKE '" . $this->db->escape_like_str($original_search_term) . "%' $added_search_sql) and deleted=0 $course_id");
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('hide_from_search', 0);
			$this->db->order_by("last_name asc, first_name asc");
			$this->db->limit($limit);
			$by_name = $this->db->get();
			foreach ($by_name->result() as $row) {
				$suggestions[] = array('value' => $row->person_id, 'label' => $row->last_name . ', ' . $row->first_name, 'name' => $row->last_name . ', ' . $row->first_name, 'last_name' => $row->last_name, 'phone_number' => $this->format_phone($row->phone_number), 'email' => $row->email, 'zip' => $row->zip, 'price_class' => $row->price_class, 'status_flag' => $row->status_flag, 'comments' => $row->comments);
			}
		}

		if (($type == 'account_number' || $type == '' || $type == 'ln_and_pn') && count($suggestions) < $limit) {
			$this->db->select('foreup_people.person_id as person_id, last_name, first_name, phone_number, email, zip, price_class, status_flag, account_number, people.comments');
			$this->db->from('customers');
			$this->db->join('people', 'customers.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('hide_from_search', 0);
			$this->db->like("account_number", $search, 'after');
			$this->db->order_by("account_number", "asc");
			$this->db->limit($limit);
			$by_account_number = $this->db->get();
			foreach ($by_account_number->result() as $row) {
				$suggestions[] = array('value' => $row->person_id, 'label' => $row->last_name . ', ' . $row->first_name . ' (' . $row->account_number . ')', 'name' => $row->last_name . ', ' . $row->first_name, 'last_name' => $row->last_name, 'phone_number' => $this->format_phone($row->phone_number), 'email' => $row->email, 'zip' => $row->zip, 'price_class' => $row->price_class, 'status_flag' => $row->status_flag, 'comments' => $row->comments);
				//$suggestions[]=array('value'=> $row->person_id, 'label' => $row->account_number);
			}
		}


		if (($type == 'phone_number' || $type == 'ln_and_pn') && count($suggestions) < $limit) {
			$this->db->select('foreup_people.person_id as person_id, last_name, first_name, phone_number, email, price_class, status_flag, people.comments');
			$this->db->from('customers');
			$this->db->join('people', 'customers.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('hide_from_search', 0);
			$this->db->where('phone_number != ""');
			$this->db->like("phone_number", $search);
			$this->db->order_by("phone_number", "asc");
			$this->db->limit($limit);
			$by_account_number = $this->db->get();
			//$suggestions[] = array('sql'=>$this->db->last_query());
			foreach ($by_account_number->result() as $row) {
				$suggestions[] = array('value' => $row->person_id, 'label' => $this->format_phone($row->phone_number), 'name' => $row->last_name . ', ' . $row->first_name, 'last_name' => $row->last_name, 'email' => $row->email, 'phone_number' => $this->format_phone($row->phone_number), 'price_class' => $row->price_class, 'status_flag' => $row->status_flag, 'comments' => $row->comments);
			}
		}

		if ($type == 'email' && count($suggestions) < $limit) {
			$this->db->select('foreup_people.person_id as person_id, email, last_name, first_name, phone_number, price_class, status_flag, people.comments');
			$this->db->from('customers');
			$this->db->join('people', 'customers.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('hide_from_search', 0);
			$this->db->where('email != ""');
			$this->db->like("email", $search, 'after');
			$this->db->order_by("email", "asc");
			$this->db->limit($limit);
			$by_account_number = $this->db->get();
			//$suggestions[] = array('sql'=>$this->db->last_query());
			foreach ($by_account_number->result() as $row) {
				$suggestions[] = array('value' => $row->person_id, 'label' => $row->email, 'name' => $row->last_name . ', ' . $row->first_name, 'phone_number' => $this->format_phone($row->phone_number), 'price_class' => $row->price_class, 'status_flag' => $row->status_flag, 'comments' => $row->comments);
			}
		}

		//only return $limit suggestions
		if (count($suggestions > $limit)) {
			$suggestions = array_slice($suggestions, 0, $limit);
		}

		if($this->include_valid_price_classes){
			$this->load->model('Price_class');
			foreach($suggestions as &$suggestion){
				$suggestion['valid_price_classes'] = $this->Price_class->get_customer_price_class_ids($suggestion['value']);
			}			
		}

		return $suggestions;
	}

	/*
	Preform a search on customers
	*/
	function search($search, $limit = 20, $group_id = 'all', $pass_id = 'all', $offset = 0, $show_deleted_customers = 0)
	{
		$course_id = '';
		//if (!$this->permissions->is_super_admin())
		{
			//$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
			$course_ids = join(',', $course_ids);
			$course_id = "AND foreup_customers.course_id IN ({$course_ids})";
		}
		$this->db->select("customers.person_id AS person_id, course_id, member, price_class, account_number, account_balance, member_account_balance, invoice_balance, loyalty_points, first_name, last_name, email, phone_number, cell_phone_number, deleted");
		$this->db->from('customers');
		$this->db->join('people', 'customers.person_id=people.person_id');
		if ($group_id == 'no_group') {
			$this->db->join('customer_group_members', 'customers.person_id=customer_group_members.person_id', 'left');
			$this->db->where('group_id', null);
		} else if ($group_id != 'all') {
			$this->db->join('customer_group_members', 'customers.person_id=customer_group_members.person_id');
			$this->db->where('group_id', $group_id);
		}
		if ($pass_id == 'no_pass') {
			$this->db->join('customer_pass_members', 'customers.person_id=customer_pass_members.person_id', 'left');
			$this->db->where('pass_id', null);
		} else if ($pass_id != 'all') {
			$this->db->join('customer_pass_members', 'customers.person_id=customer_pass_members.person_id');
			$this->db->where('pass_id', $pass_id);
		}

		$where_clause = "(first_name LIKE '" . $this->db->escape_like_str($search) . "%' or
		last_name LIKE '" . $this->db->escape_like_str($search) . "%' or
		email LIKE '" . $this->db->escape_like_str($search) . "%' or
		phone_number LIKE '%" . $this->db->escape_like_str($search) . "%' or
		cell_phone_number LIKE '%" . $this->db->escape_like_str($search) . "%' or
		account_number LIKE '" . $this->db->escape_like_str($search) . "%' or
		CONCAT(`first_name`,' ',`last_name`) LIKE '%" . $this->db->escape_like_str($search) . "%')";

		if($show_deleted_customers) {
			$where_clause .= " AND hide_from_search = 0 $course_id";
		}else {
			$where_clause .= " and deleted=0 AND hide_from_search = 0 $course_id";
		}

		$this->db->where($where_clause);
		$this->db->order_by("last_name asc, first_name asc");
		// Just return a count of all search results
		if ($limit == 0)
			return $this->db->count_all_results();
		// Return results
		$this->db->offset($offset);
		$this->db->limit($limit);
		return $this->db->get();
	}

	function cleanup()
	{
		$customer_data = array('account_number' => null);
		$this->db->where('deleted', 1);
		return $this->db->update('customers', $customer_data);
	}

	function getAllCustomers()
	{

	}


	function enable_loyalty($person_id)
	{
		$course_ids = array();
		$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $this->config->item('course_id'));
		$this->db
			->where_in("course_id",$course_ids)
			->update('customers',[
				"use_loyalty"=>1
			],[
					"person_id"=>$person_id
				]
			);
	}

	function disable_loyalty($person_id)
	{
		$course_ids = array();
		$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $this->config->item('course_id'));
		$this->db
			->where_in("course_id",$course_ids)
			->update('customers',[
				"use_loyalty"=>0
			],[
					"person_id"=>$person_id
				]
			);
	}

}
