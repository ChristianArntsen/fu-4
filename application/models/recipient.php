<?php
class Recipient extends Person
{	
	/*
	Determines if a given person_id is a customer
	*/
	function exists($person_id)
	{
		$course_ids = array();
		
		$this->db->from('auto_mailer_recipients');	
		$this->db->join('people', 'people.person_id = auto_mailer_recipients.person_id');
		$this->db->where('auto_mailer_recipients.person_id',$person_id);
		$this->db->where('auto_mailer_recipients.course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
        $query = $this->db->get();
		//echo $this->db->last_query();
		return ($query->num_rows()==1);
	}
	
	/*
	Returns all the customers
	*/
	function get_all_recipients($auto_mailer_id, $date = false)
	{
		$this->db->from('auto_mailer_recipients');
		$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');			
		$this->db->order_by("last_name", "asc");
		if ($date)
		{
			$this->db->where('start_date', $date);
		}
		$this->db->where("deleted = 0 $course_id");
		$this->db->where('auto_mailer_id', $auto_mailer_id);
		$customers = $this->db->get();
		//echo $this->db->last_query();
        return $customers;
	}
	
	function count_all($filter = '')
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
		    $this->db->where('auto_mailer_recipients.course_id', $this->session->userdata('course_id'));
        }
        $this->db->from('auto_mailer_recipients');
		if ($filter == 'email')
		{
			$this->db->join('people','people.person_id = auto_mailer_recipients.person_id');
			$this->db->where('email !=', '');
			//$this->db->group_by('email');
		}
		else if ($filter == 'phone')
		{
			$this->db->join('people','people.person_id = auto_mailer_recipients.person_id');
			$this->db->where('phone_number !=', '');
			//$this->db->group_by('email');
		}
		$this->db->where("deleted = 0 $course_id");
		//$this->db->get();
		
		return $this->db->count_all_results();
	}
	
	/*
	Gets information about a particular customer
	*/
	function get_info($recipient_id)
	{
		$this->db->from('auto_mailer_recipients');	
		$this->db->join('people', 'people.person_id = auto_mailer_recipients.person_id');
		$this->db->where('auto_mailer_recipients.person_id',$recipient_id);
		$query = $this->db->get();
		
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $customer_id is NOT an customer
			$person_obj=parent::get_info(-1);
			
			//Get all the fields from customer table
			$fields = $this->db->list_fields('auto_mailer_recipients');
			
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			
			return $person_obj;
		}
	}
	
	/*
	Gets information about multiple customers
	*/
	function get_multiple_info($recipient_ids)
	{
		$this->db->from('auto_mailer_recipients');
		$this->db->join('people', 'people.person_id = auto_mailer_recipients.person_id');		
		$this->db->where_in('auto_mailer_recipients.person_id',$recipient_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();		
	}
	
	
	/*
	Inserts or updates a recipient
	*/
	function save(&$recipient_data,$recipient_id=false)
	{
		$success=false;
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		
		if (!$recipient_id or !$this->exists($recipient_id))
		{
			$recipient_id = $recipient_data['person_id'];
			$success = $this->db->insert('auto_mailer_recipients',$recipient_data);
		}
		else
		{
			$this->db->where('person_id', $recipient_id);
			$success = $this->db->update('auto_mailer_recipients',$recipient_data);
		}
		$this->db->trans_complete();	
		return $this->db->trans_status();
	}
	/*
	Deletes one recipient
	*/
	function delete($recipient_id)
	{
		$course_id = '';
        $this->db->where('auto_mailer_recipients.course_id', $this->session->userdata('course_id'));
        $this->db->where("person_id", $recipient_id);
		$this->db->limit(1);
		return $this->db->update('auto_mailer_recipients', array('deleted' => 1));
	}
	
	/*
	Deletes a list of recipients
	*/
	function delete_list($recipient_ids)
	{
		$this->db->where('auto_mailer_recipients.course_id', $this->session->userdata('course_id'));
        $this->db->where_in('person_id',$recipient_ids);
		return $this->db->update('auto_mailer_recipients', array('deleted' => 1));
 	}
 	
 	/*
	Get search suggestions to find recipients
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
            $this->db->where('auto_mailer_recipients.course_id', $this->session->userdata('course_id'));
        }
        $suggestions = array();
		
		$this->db->from('auto_mailer_recipients');
		$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');	
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);		
		}
		
		$this->db->from('auto_mailer_recipients');
		$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');	
		$this->db->where("deleted = 0 $course_id");
		$this->db->like("email",$search);
		$this->db->order_by("email", "asc");		
		$by_email = $this->db->get();
		foreach($by_email->result() as $row)
		{
			$suggestions[]=array('label'=> $row->email);		
		}

		$this->db->from('auto_mailer_recipients');
		$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');	
		$this->db->where("deleted = 0 $course_id");		
		$this->db->like("phone_number",$search);
		$this->db->order_by("phone_number", "asc");		
		$by_phone = $this->db->get();
		foreach($by_phone->result() as $row)
		{
			$suggestions[]=array('label'=> $row->phone_number);		
		}
		
		$this->db->from('auto_mailer_recipients');
		$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');	
		$this->db->where("deleted = 0 $course_id");		
		$this->db->like("account_number",$search);
		$this->db->order_by("account_number", "asc");		
		$by_account_number = $this->db->get();
		foreach($by_account_number->result() as $row)
		{
			$suggestions[]=array('label'=> $row->account_number);		
		}
		
		$this->db->from('auto_mailer_recipients');
		$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');	
		$this->db->where("deleted = 0 $course_id");		
		$this->db->like("company_name",$search);
		$this->db->order_by("company_name", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->company_name);		
		}
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	
	}
	
	/*
	Get search suggestions to find customers
	*/
	function get_recipient_search_suggestions($search,$limit=25,$type='')
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
            $this->db->where('auto_mailer_recipients.course_id', $this->session->userdata('course_id'));
        }
        $suggestions = array();
		
		if ($type == 'last_name' || $type == '' || $type == 'ln_and_pn') {
			$this->db->from('auto_mailer_recipients');
			$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');	
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");
			$this->db->order_by("last_name", "asc");		
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=array('value'=> $row->person_id, 'label' => $row->last_name.', '.$row->first_name, 'phone_number'=>$row->phone_number, 'email'=>$row->email);		
			}
		}
		
		if ($type == 'account_number' || $type == '') {
			$this->db->from('auto_mailer_recipients');
			$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');	
			$this->db->where("deleted = 0 $course_id");		
			$this->db->like("account_number",$search);
			$this->db->order_by("account_number", "asc");		
			$by_account_number = $this->db->get();
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('value'=> $row->person_id, 'label' => $row->account_number);		
			}
		}

			
		if ($type == 'phone_number' || $type == 'ln_and_pn') {
			$this->db->from('auto_mailer_recipients');
			$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');	
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('phone_number != ""');		
			$this->db->like("phone_number",$search);
			$this->db->order_by("phone_number", "asc");		
			$by_account_number = $this->db->get();
			//$suggestions[] = array('sql'=>$this->db->last_query());
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('value'=> $row->person_id, 'label' => $row->phone_number, 'name'=>$row->last_name.', '.$row->first_name, 'email'=>$row->email);		
			}
		}

		if ($type == 'email') {
			$this->db->from('auto_mailer_recipients');
			$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');	
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('email != ""');		
			$this->db->like("email",$search);
			$this->db->order_by("email", "asc");		
			$by_account_number = $this->db->get();
			//$suggestions[] = array('sql'=>$this->db->last_query());
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('value'=> $row->person_id, 'label' => $row->email, 'name'=>$row->last_name.', '.$row->first_name, 'phone_number'=>$row->phone_number);		
			}
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	/*
	Preform a search on recipients
	*/
	function search($search, $limit=20, $group_id = 'all')
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
            $this->db->where('auto_mailer_recipients.course_id', $this->session->userdata('course_id'));
        }
        $this->db->from('auto_mailer_recipients');
		$this->db->join('people','auto_mailer_recipients.person_id=people.person_id');		
		
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		email LIKE '%".$this->db->escape_like_str($search)."%' or 
		phone_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		account_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		company_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");		
		$this->db->order_by("last_name", "asc");
		
		$this->db->limit($limit);
		return $this->db->get();
	}
}
?>
