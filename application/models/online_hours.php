<?php
class Online_hours extends CI_Model {
	
	public function get($teesheet_id){
		return $this->db->from('online_hours')
			->where('teesheet_id', (int) $teesheet_id)
			->get()->row_array();
	}

	// Save teesheet hours settings
	public function save($teesheet_id, $data = array()){
		
		if(empty($teesheet_id)){
			return false;
		}

		$num_rows = $this->db->select('teesheet_id')
			->from('online_hours')
			->where('teesheet_id', $teesheet_id)
			->get()->num_rows();

		if($num_rows > 0){
			$this->db->update('online_hours', $data, array('teesheet_id' => $teesheet_id));
		}else{
			$data['teesheet_id'] = $teesheet_id;
			$this->db->insert('online_hours', $data);
		}

		return $this->db->affected_rows();
	}

	// Check if a teesheet is currently available for a given time range
	public function is_available($teesheet_id, $date){

		$day_of_week = substr(strtolower($date->format('l')), 0, 3);
		$time = (int) $date->format('Hi');
		
		$this->db->select("teesheet_id");
		$this->db->from('online_hours');
		$this->db->where($day_of_week.'_open <= '.$time.' AND '.$day_of_week.'_close > '.$time);
		$this->db->where('teesheet_id', (int) $teesheet_id);
		$num_rows = $this->db->get()->num_rows();

		if($num_rows > 0){
			return true;
		}

		return false;
	}
}
