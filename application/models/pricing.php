<?php
class Pricing extends CI_Model
{
    const GREEN_FEE_9 = 'price2';
    const GREEN_FEE_18 = 'price1';
    const CART_FEE_9 = 'price4';
    const CART_FEE_18 = 'price3';

    private $prices_cache;
    public $course_id = false;
    public $include_receipt_content = false;

    function __construct(){
		$this->load->model('Price_class');
		$this->load->model('Special');
		$this->prices_cache = array();
	}

    /*
	Returns all the timeframes for a given price category
	*/
	function get_timeframes($season_id, $class_id, $limit = 10000, $offset = 0)
	{
		$course_id = '';
        $this->db->from('seasonal_timeframes')
			->where('class_id', $class_id)
			->where('season_id', $season_id)
        	->order_by("default", "ASC")
        	->order_by("start_time", "ASC")
        	->order_by("timeframe_name", "DESC")
			->limit($limit)
			->offset($offset);

		$results = $this->db->get()->result();
		return $results;
	}

	/*
	Adds a new timeframe for a given price category
	*/
	function add_timeframe($timeframe_data)
	{
		// If the timeframe is a default, first check if default already exists
		if(isset($timeframe_data['default']) && $timeframe_data['default'] == 1){
			$query = $this->db->get_where('seasonal_timeframes', array('season_id' => $timeframe_data['season_id'], 'default' => 1));

			if($query->num_rows() > 0){
				return false;
			}
		}

		if ($this->db->insert('seasonal_timeframes', $timeframe_data))
		{
			$timeframe_id = $this->db->insert_id();
			return $timeframe_id;
		}
		return false;
	}

	/*
	 Gets a timeframe with a given timeframe ID
	*/
	function get_timeframe($timeframe_id)
	{
		if (!$timeframe_id || $timeframe_id < 1) return false;

		$this->db->from('seasonal_timeframes');
        $this->db->where("timeframe_id", $timeframe_id);

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else return false;
	}
	/*
	Deletes a timeframe
	*/
	function delete_timeframe($timeframe_id)
	{
		if (!$timeframe_id){
			return false;
		}
		return $this->db->delete('seasonal_timeframes', array('timeframe_id' => (int) $timeframe_id));
	}
	/*
	Inserts or updates a price category
	*/
	function save(&$pricing_data, $price_id=false)
	{
		if (!$price_id || !$this->exists($price_id))
		{
			if($this->db->insert('seasonal_prices',$pricing_data))
			{
				$price_id = $this->db->insert_id();
				if (is_array($pricing_data))
					$pricing_data['price_id'] = $price_id;
				elseif (is_object($pricing_data))
					$pricing_data->price_id = $price_id;
				$this->create_default_timeframe($price_id,$pricing_data['season_id']);
				return true;
			}
			return false;
		}
		$this->db->where('price_id', $price_id);
		return $this->db->update('seasonal_prices',$pricing_data);
	}

	function get_overall_default()
	{
		$this->load->model('Teesheet');
		$teesheet_id = $this->Teesheet->get_default();
		$season_id = $this->Season->get_default($teesheet_id);
		$price_id = $this->get_default($season_id);
		$timeframe = $this->get_default_timeframe($price_id);
		if ($timeframe !== false)
			return timeframe;
		else return false;
	}

	/*
	Updates a timeframe
	*/
	function save_timeframe($timeframe_id, $timeframe_data)
	{
		$this->db->where('timeframe_id', $timeframe_id);
		return $this->db->update('seasonal_timeframes', $timeframe_data);
	}

	/*
	 Determines if a given price_id is a price category
	*/
	function exists($price_id)
	{
		$this->db->from('seasonal_prices');
		$this->db->where('price_id',$price_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	 * Get list of prices for the teesheet and day specified
	 */
	function get_price_timeframes($teesheet_id = false, $price_class = false, $date = false, $filter_days = true, $timeframe_id = false, $cart = false, $start_time = false)
	{
        $date_filter_sql = '';
		if($date){
			$day_of_week = strtolower(date('l', strtotime($date)));
			$date = substr($date, 5);
			$season_date = $this->db->escape("0000-" . $date);

			$date_filter_sql = "AND (
				(season.start_date <= {$season_date} AND season.end_date >= {$season_date}) OR
				(season.start_date > season.end_date AND (season.start_date <= {$season_date} OR season.end_date >= {$season_date}))
			)";
		}else{
			$day_of_week = strtolower(date('l'));
		}

		$default_price = $this->Price_class->get_default($this->course_id);

        // super admin does not need pricing, or default_class_id
		$default_class_id = !$this->permissions->is_super_admin() && !empty($default_price['class_id']) ?
		    (int) $default_price['class_id']:false;

		if(empty($this->course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}else{
			$course_id = $this->course_id;
		}

		$teesheet_sql = '';
		if($teesheet_id){
			$teesheet_sql = 'AND season.teesheet_id = '.(int) $teesheet_id;
		}

		$timeframe_sql = '';
		if($timeframe_id){
			$timeframe_sql = 'AND timeframe.timeframe_id = '.(int) $timeframe_id;
		}

		// Order by the selected price class first, then by the default price class
		$default_class_id = "";
		$price_class_sql = null;
		if($price_class && $default_class_id !== false){
			$price_class_id = (int) $price_class;
			$price_class_sql = "AND (timeframe.class_id = {$price_class_id} ";

			if(isset($default_price['class_id'])){
				$default_class_id = (int) $default_price['class_id'];
				$price_class_sql .= " OR timeframe.class_id = {$default_class_id} ";
			}
			$price_class_sql .= " )";

			if($price_class_id == $default_class_id){
				$price_class_sql = " AND timeframe.class_id = {$default_class_id} ";
			}
		}

		$order_sql = 'season.season_name, price_class.name, timeframe.start_time';

		$filter_days_sql = '';
		if($filter_days){
			$filter_days_sql = "AND timeframe.{$day_of_week} = 1";
		}else {
			$order_sql = "IF(timeframe.{$day_of_week} = 1, 1, 0) DESC, " . $order_sql;
		}

		$query = $this->db->query("SELECT timeframe.timeframe_id, timeframe.price1,
				timeframe.price2, timeframe.price3, timeframe.price4,
				timeframe.price5, timeframe.price6, timeframe.start_time,
				timeframe.end_time, timeframe.default AS default_timeframe, timeframe.season_id,
				season.start_date, season.end_date, season.default AS default_season,
				season.holiday AS season_holiday, timeframe.timeframe_name, season.season_name,
				price_class.name AS price_class_name, price_class.class_id AS price_class_id,
				price_class.cart AS price_class_allow_carts, teesheet.title AS teesheet_name,
				season.teesheet_id,
				IF(timeframe.{$day_of_week} = 1, 1, 0) AS is_today
			FROM foreup_seasonal_timeframes AS timeframe
			INNER JOIN foreup_seasons AS season
				ON season.season_id = timeframe.season_id
			LEFT JOIN foreup_price_classes AS price_class
				ON price_class.class_id = timeframe.class_id
			LEFT JOIN foreup_teesheet AS teesheet
				ON teesheet.teesheet_id = season.teesheet_id
			WHERE season.course_id = {$course_id}
                {$timeframe_sql}
				{$teesheet_sql}
				{$date_filter_sql}
				{$price_class_sql}
				{$filter_days_sql}
				AND timeframe.active = 1
				AND teesheet.deleted = 0
			GROUP BY timeframe.timeframe_id
			ORDER BY {$order_sql}");

		$rows = $query->result_array();
		return $rows;
	}

	// Get a price by price class, date, and time
	function get_price($teesheet_id, $price_class = false, $date = false, $time = false, $holes = '9', $cart = false){
		$timeframe = $this->get_price_timeframe($teesheet_id, $price_class, $date, $time, $cart);
		$price = 0.00;
		if($holes == '9' && empty($cart)){
			$price = $timeframe['price2'];

		}else if($holes == '9' && !empty($cart)){
			$price = $timeframe['price4'];

		}else if($holes == '18' && empty($cart)){
			$price = $timeframe['price1'];

		}else if($holes == '18' && !empty($cart)){
			$price = $timeframe['price3'];
		}

		return (float) $price;
	}

	// Get a price by specific timeframe_id
	function get_price_by_timeframe($timeframe_id, $holes = '9', $cart = false){

		$this->db->select('price1, price2, price3, price4, price5, price6');
		$this->db->from('seasonal_timeframes');
		$this->db->where('timeframe_id', (int) $timeframe_id);
		$timeframe = $this->db->get()->row_array();

		$price = 0.00;
		if($holes == '9' && empty($cart)){
			$price = $timeframe['price2'];

		}else if($holes == '9' && !empty($cart)){
			$price = $timeframe['price4'];

		}else if($holes == '18' && empty($cart)){
			$price = $timeframe['price1'];

		}else if($holes == '18' && !empty($cart)){
			$price = $timeframe['price3'];
		}

		return (float) $price;
	}

	// Get timeframe data for a specific price class, date and time
	function get_price_timeframe($teesheet_id, $price_class = false, $date = false, $time = false, $cart = false)
	{
		if ($time == false){
			$time = date('Hi');
		}
		$time = sprintf('%04d', $time);
		$time_hours = substr($time, 0, 2);
		$time_min = substr($time, 2, 2);
		if((int) $time_min < 30){
			$time_min = '00';
		}else{
			$time_min = '30';
		}
		$time = $time_hours.$time_min;

		// If date is empty, default to today
		if ($date == false){
			$date = date('Y-m-d');
		}

		if(!empty($price_class) && $cart){
			$price_class_data = $this->Price_class->get($price_class);
			if($price_class_data['cart'] == 0){
				$price_class = false;
			}
		}

		// If price class is empty, use default price class
		if(empty($price_class)){
			$default_price = $this->Price_class->get_default($this->course_id);

            // super admin does not need pricing or price_class
			$price_class = !$this->permissions->is_super_admin()?(int) $default_price['class_id']:false;
		}

		// Get possible timeframes for this particular price class, date, and teesheet
		$cache_key = md5($teesheet_id . $price_class . $date);

		if(isset($this->prices_cache[$cache_key])){
			$price_array = $this->prices_cache[$cache_key];
		}else{
			$price_array = $this->get_price_timeframes($teesheet_id, $price_class, $date);
			$this->prices_cache[$cache_key] = $price_array;
		}

		$ordered_prices = array();
		foreach($price_array as $timeframe){

			if((int) $timeframe['start_time'] <= (int) $time && (int) $timeframe['end_time'] > (int) $time){

				// Create a list of possible prices based on distance from reservation time
				$time_difference = abs((int) ((int) $timeframe['start_time'] - (int) $time));
				$time_difference = sprintf('%04d', $time_difference);

				$start_date = str_replace('0000', date('Y'), $timeframe['start_date']);
				$start_date = DateTime::createFromFormat('Y-m-d', $start_date);
				$date_selected = DateTime::createFromFormat('Y-m-d', $date);

				$days_difference = $start_date->diff($date_selected);
				if ($days_difference != '') {
					$days_difference = abs((int) $days_difference->format('%a'));
				}

				if(!empty($timeframe['holiday']) && $timeframe['holiday'] == 1){ $holiday_sort = 0; }else{ $holiday_sort = 1; }
				if(!empty($timeframe['price_class_id']) && $timeframe['price_class_id'] == (int) $price_class){ $class_sort = 0; }else{ $class_sort = 1; }
				if(!empty($timeframe['default_season']) && $timeframe['default_season'] == 1){ $default_season_sort = 1000; }else{ $default_season_sort = 0; }

				// Build key to sort pricing
				$key = $holiday_sort.$default_season_sort.$class_sort.$days_difference.$timeframe['default_timeframe'].$time_difference;
				$ordered_prices[$key] = $timeframe;
			}
		}

		// Sort array by key from lowest to highest
		ksort($ordered_prices);

		// First timeframe in list is the correct one
		$timeframe = array_shift($ordered_prices);
		return $timeframe;
	}

	// Gets a list of prices by time for a particular day (used for online booking)
	function get_prices($teesheet_id, $price_class = false, $date = false){

		$this->load->helper('date');
		$times = time_array();
		$time_prices = array();
		$price_array = $this->get_price_timeframes($teesheet_id, $price_class, $date);

		// Loop through list of times throughout the daty
		foreach($times as $time => $time_data){

			// Loop through possible prices for each time
			foreach($price_array as $key => $timeframe){

				if((int) $timeframe['start_time'] <= (int) $time && (int) $timeframe['end_time'] > (int) $time){
					$time_prices[(int) $time] = $timeframe;
					break 1;
				}
			}
		}

		return $time_prices;
	}

	// Returns all price classes for 9 holes, and all price classes
	// for 18 holes. Used in select menu dropdown for sales page
	function get_teetime_price_classes($teesheet_id = false, $date = false, $include_timeframes = true){

		$filter_days = true;
		if($date == false){
			$filter_days = false;
		}

		$timeframes = $this->get_price_timeframes($teesheet_id, false, $date, $filter_days);
		$num_price_classes = count($timeframes);

		$specials_prices = $this->Special->get_all($teesheet_id);
		$num_special_price_classes = count($specials_prices);

		$rows_9 = array();
		$rows_18 = array();
		$rows_9_today = array();
		$rows_18_today = array();
		$special_9_today = array();
		$special_18_today = array();

		// Load list of 9 hole price classes
		for($x = 0; $x < $num_price_classes; $x++){
			$t = $timeframes[$x];

			$timeframe_name = $t['timeframe_name'] != 'Default' ? $t['timeframe_name'] : '';

			$season_name = '';
			if($t['season_holiday'] == 1){
				$season_name = '['.$t['season_name'].']';
			}

			$key_9 = $t['price_class_id'].'_'.$t['timeframe_id'].'_2';
			$key_18 = $t['price_class_id'].'_'.$t['timeframe_id'].'_1';


			if (!$include_timeframes) {
				$rows_9[$key_9] = '9 - '.$season_name.' '.$t['price_class_name'];
				$rows_18[$key_18] = '18 - '.$season_name.' '.$t['price_class_name'];
			} else if($t['is_today'] == 1){
				$rows_9_today[$key_9] = '9 - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
				$rows_18_today[$key_18] = '18 - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
			}else{
				$rows_9[$key_9] = '9 - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
				$rows_18[$key_18] = '18 - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
			}
		}

		for($x = 0; $x < $num_special_price_classes; $x++){
			$t = $specials_prices[$x];

			$key_special_9 = 'special:'.$t['special_id'].'_2';
			$key_special_18 = 'special:'.$t['special_id'].'_1';

			$special_9_today[$key_special_9] = '9 SPECIAL -'.$t['name'];
			$special_18_today[$key_special_18] = '18 SPECIAL -'.$t['name'];
		}

		return array_merge($rows_9_today, $rows_18_today, $rows_9, $rows_18, $special_9_today, $special_18_today);
	}

	// Returns all price classes for 9 hole cart, and all price classes
	// for 18 hole cart. Used in select menu dropdown for sales page
	function get_cart_price_classes($teesheet_id = false, $date = false, $include_timeframes = true){

		$filter_days = true;
		if($date == false){
			$filter_days = false;
		}
		if(empty($teesheet_id)){
			$teesheet_id = (int) $this->session->userdata('teesheet_id');
		}

		$timeframes = $this->get_price_timeframes($teesheet_id, false, $date, $filter_days);
		$num_price_classes = count($timeframes);

		$specials_prices = $this->Special->get_all($teesheet_id);
		$num_special_price_classes = count($specials_prices);

		$rows_9 = array();
		$rows_18 = array();
		$rows_9_today = array();
		$rows_18_today = array();
		$special_9_today = array();
		$special_18_today = array();

		// Load list of 9 hole price classes
		for($x = 0; $x < $num_price_classes; $x++){
			
			$t = $timeframes[$x];
			$timeframe_name = $t['timeframe_name'] != 'Default' ? $t['timeframe_name'] : '';

			$season_name = '';
			if($t['season_holiday'] == 1){
				$season_name = '['.$t['season_name'].']';
			}

			$key_9 = $t['price_class_id'].'_'.$t['timeframe_id'].'_4';
			$key_18 = $t['price_class_id'].'_'.$t['timeframe_id'].'_3';

			if (!$include_timeframes) {
				$rows_9[$key_9] = '9 Cart - '.$season_name.' '.$t['price_class_name'];
				$rows_18[$key_18] = '18 Cart - '.$season_name.' '.$t['price_class_name'];
			}else if($t['is_today'] == 1){
				$rows_9_today[$key_9] = '9 Cart - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
				$rows_18_today[$key_18] = '18 Cart - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
			}else{
				$rows_9[$key_9] = '9 Cart - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
				$rows_18[$key_18] = '18 Cart - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
			}
		}

		for($x = 0; $x < $num_special_price_classes; $x++){
			$t = $specials_prices[$x];

			$key_special_9 = 'special:'.$t['special_id'].'_4';
			$key_special_18 = 'special:'.$t['special_id'].'_3';

			$special_9_today[$key_special_9] = '9 Cart SPECIAL -'.$t['name'];
			$special_18_today[$key_special_18] = '18 Cart SPECIAL -'.$t['name'];
		}

		return array_merge($rows_9_today, $rows_18_today, $rows_9, $rows_18, $special_9_today, $special_18_today);
	}

	function get_fees($params = array()){
		$teesheet_id = false;
		$date = false;
		$timeframe_id = false;
		$filter_days = false;
		$filter_holes = false;
		$filter_type = false;
		$filter_price_class = false;
		$search = false;
		$special_id = false;
        $fees = array();
        $cart = false;

		if(!empty($params['teesheet_id'])){
			$teesheet_id = (int) $params['teesheet_id'];
		}
		if(!empty($params['date'])){
			$date = $params['date'];
		}
		if(isset($params['filter_days'])){
			$filter_days = (bool) $params['filter_days'];
		}
		if(!empty($params['holes'])){
			$filter_holes = (int) $params['holes'];
		}
		if(!empty($params['type'])){
			$filter_type = $params['type'];
            if ($params['type'] == 'cart_fee') {
                $cart = true;
            }
		}
		if(!empty($params['price_class_id'])){
			$filter_price_class = (int) $params['price_class_id'];
		}
		if(!empty($params['timeframe_id'])){
			$timeframe_id = (int) $params['timeframe_id'];
		}
		if(!empty($params['special_id'])){
			$special_id = (int) $params['special_id'];
		}
		if(!empty($params['search'])){
			$search = trim($params['search']);
		}

		$course_id = (int) $this->session->userdata('course_id');
		if(!empty($this->course_id)){
			$course_id = $this->course_id;
		}

		$filter_days = true;
		if($date == false){
			$filter_days = false;
		}

		$num_price_classes = 0;
		if($special_id === false){
			$timeframes = $this->get_price_timeframes($teesheet_id, $filter_price_class, $date, $filter_days, $timeframe_id, $cart, isset($params['start_time'])?$params['start_time']:false);
			$num_price_classes = count($timeframes);
		}
		
		$num_special_price_classes = 0;
		if($timeframe_id === false){

			$aggregate = false;
			if(!empty($params['aggregate_specials'])){
				$aggregate = (int) $params['aggregate_specials'];
			}
			$this->Special->course_id = $this->course_id;
			$specials_prices = $this->Special->get_all($teesheet_id, $special_id, $aggregate);

			$num_special_price_classes = count($specials_prices);
		}

		// Get the special item IDs that correlate with green/cart fees

        $item_ids = array();
        $item_rows = $this->get_fee_item_ids();

        foreach($item_rows as &$row){

			if(!empty($row['taxes'])){
				$row['taxes'] = explode("\x1E", $row['taxes']);

				// Loop through each pass, break apart fields
				foreach($row['taxes'] as &$tax_row){
					$tax = explode("\x1F", $tax_row);
					$tax = array(
						'name' => $tax[0],
						'percent' => (float) $tax[1],
						'cumulative' => (int) $tax[2]
					);
					$tax_row = $tax;
				}
			}

			$item_ids[(int) $row['item_number']] = $row;
		}

		$cart_fees = array();
		$green_fees = array();

		// Loop through list of available prices and create fees
		for($x = 0; $x < $num_price_classes; $x++){

			$t = $timeframes[$x];

			if(!$filter_type || $filter_type == 'green_fee'){
				if(!$filter_holes || $filter_holes == 9){
					$fees[] = $this->get_fee_row($t, $item_ids, 9, 'green_fee', false);
				}
				if(!$filter_holes || $filter_holes == 18){
					$fees[] = $this->get_fee_row($t, $item_ids, 18, 'green_fee', false);
				}
			}

			if((!$filter_type || $filter_type == 'cart_fee') && $t['price_class_allow_carts']){
				if(!$filter_holes || $filter_holes == 9){
					$fees[] = $this->get_fee_row($t, $item_ids, 9, 'cart_fee', false);
				}
				if(!$filter_holes || $filter_holes == 18){
					$fees[] = $this->get_fee_row($t, $item_ids, 18, 'cart_fee', false);
				}
			}
		}

		// Loop through specials and create fees
		for($x = 0; $x < $num_special_price_classes; $x++){
			$t = $specials_prices[$x];

			for($y = 1; $y <= 4; $y++){
				$t["price{$y}"] = $t["price_{$y}"];
			}

			if(!$filter_type || $filter_type == 'green_fee'){
				if(!$filter_holes || $filter_holes == 9){
					$fees[] = $this->get_fee_row($t, $item_ids, 9, 'green_fee', (int) $t['special_id']);
				}
				if(!$filter_holes || $filter_holes == 18){
					$fees[] = $this->get_fee_row($t, $item_ids, 18, 'green_fee', (int) $t['special_id']);
				}
			}

			if(!$filter_type || $filter_type == 'cart_fee'){
				if(!$filter_holes || $filter_holes == 9){
					$fees[] = $this->get_fee_row($t, $item_ids, 9, 'cart_fee', (int) $t['special_id']);
				}
				if(!$filter_holes || $filter_holes == 18){
					$fees[] = $this->get_fee_row($t, $item_ids, 18, 'cart_fee', (int) $t['special_id']);
				}
			}
		}

		// Sort fees by 9 holes, then 18 holes, with specials always being
		// at the bottom
        usort($fees, function ($a, $b) {

            // Build sort key
            $key_a = '1';
            $key_b = '1';

            if ($a['holes'] == 9) {
                $key_a .= '0';
            } else {
                $key_a .= '1';
            }

            if ($a['special_id']) {
                $key_a .= '1';
            } else {
                $key_a .= '0';
            }

            if ($b['holes'] == 9) {
                $key_b .= '0';
            } else {
                $key_b .= '1';
            }

            if ($b['special_id']) {
                $key_b .= '1';
            } else {
                $key_b .= '0';
            }

            if ((int)$key_a < (int)$key_b) {
                return -1;
            } else if ((int)$key_a > (int)$key_b) {
                return 1;
            }
            return 0;
        });

		// If search string is passed, filter fees by which match that string
		if($search){
			foreach($fees as $key => $fee){
				if(stripos($fee['name'], trim($search)) === false){
					unset($fees[$key]);
				}
			}
			$fees = array_values($fees);
		}

		usort($fees, function($a, $b){
			return strcmp($a['name'], $b['name']);
		});

		return $fees;
	}

	private function get_fee_row($timeframe, $item_ids, $holes, $type, $special_id){

        // check if it is a super admin that does not need pricing
        if($this->permissions->is_super_admin()){
            return false;
        } // throw an exception if it is a user that needs pricing, but does not have item_ids
        elseif(!isset($item_ids) || !count($item_ids)){
            throw new Exception('get_fee_row expects item_ids to be a populated array. This might be because there are no default Green Fees or Cart Fees');
        }

        if(empty($timeframe['timeframe_name'])){
			$timeframe_name = '';
		}else{
			$timeframe_name = trim($timeframe['timeframe_name']);
		}

		if($timeframe_name == 'Default'){
			$timeframe_name = '';
		}

		if(empty($timeframe['is_today'])){
			$timeframe['is_today'] = false;
		}
		if(empty($timeframe['season_id'])){
			$timeframe['season_id'] = 0;
		}
		if(empty($timeframe['season_name'])){
			$timeframe['season_name'] = '';
		}
		if(empty($timeframe['teesheet_name'])){
			$timeframe['teesheet_name'] = '';
		}
		if(empty($timeframe['price_class_name'])){
			$timeframe['price_class_name'] = '';
		}
		if(empty($timeframe['price_class_id'])){
			$timeframe['price_class_id'] = 0;
		}
		if(empty($timeframe['timeframe_id'])){
			$timeframe['timeframe_id'] = 0;
		}
		if(empty($timeframe['season_holiday'])){
			$timeframe['season_holiday'] = 0;
		}

		$season_name = '';
		if($timeframe['season_holiday'] == 1){
			$season_name = '['.trim($timeframe['season_name']).']';
		}
		$price = 0;
		$taxes = array();
		if($holes == 9 && $type == 'cart_fee'){
			$price = (float) $timeframe[self::CART_FEE_9];
			$name = '9 Cart -';
			$item_number = 4;

		}else if($holes == 9 && $type == 'green_fee'){
			$price = (float) $timeframe[self::GREEN_FEE_9];
			$name = '9 -';
			$item_number = 2;

		}else if($holes == 18 && $type == 'cart_fee'){
			$price = (float) $timeframe[self::CART_FEE_18];
			$name = '18 Cart -';
			$item_number = 3;

		}else if($holes == 18 && $type == 'green_fee'){
			$price = (float) $timeframe[self::GREEN_FEE_18];
			$name = '18 -';
			$item_number = 1;
		}

		
		$receipt_content = [];
		$item_id = -1;
		$tax_included = 0;
		if(isset($item_ids[$item_number])){

			$item_id = $item_ids[$item_number]['item_id'];
			$taxes = $item_ids[$item_number]['taxes'];
			$tax_included = $item_ids[$item_number]['unit_price_includes_tax'];
			$receipt_content['content'] = $item_ids[$item_number]['receipt_content'];
			$receipt_content['content_id'] = (int) $item_ids[$item_number]['receipt_content_id'];
			$receipt_content['signature_line'] = ($item_ids[$item_number]['receipt_content_signature_line'] == 1);
			$receipt_content['separate_receipt'] = ($item_ids[$item_number]['receipt_content_separate_receipt'] == 1);
		}

		if(!$special_id){
			$name .= $season_name.' '.$timeframe['price_class_name'].' '.$timeframe_name;
		}else{
			$name .= ' SPECIAL - '.$timeframe['name'];
		}

		return array(
			'price_class_id' => (int) $timeframe['price_class_id'],
			'price_class_name' => $timeframe['price_class_name'],
			'timeframe_id' => (int) $timeframe['timeframe_id'],
			'teesheet_id' => (int) $timeframe['teesheet_id'],
			'teesheet_name' => $timeframe['teesheet_name'],
			'season_id' => (int) $timeframe['season_id'],
			'season_name' => $timeframe['season_name'],
			'special_id' => $special_id,
			'is_today' => (bool) $timeframe['is_today'],
			'item_type' => $type,
			'holes' => $holes,
			'unit_price' => $price,
			'base_price' => $price,
			'name' => $name,
			'item_id' => $item_id,
			'item_number' => $item_number,
			'taxes' => $taxes,
			'quantity' => 1,
			'selected' => true,
			'unit_price_includes_tax' => (int) $tax_included,
			'max_discount' => 0,
			'receipt_content' => $receipt_content
		);
	}

	function get_fee_item_ids(){
        
        $course_id = $this->session->userdata('course_id');

        $this->db->select("SUBSTRING_INDEX(item.item_number, '_', -1) AS item_number, item.item_id,
			item.unit_price_includes_tax, GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				tax.name,
				tax.percent,
				tax.cumulative
			) SEPARATOR 0x1E) AS taxes,
			receipt_content.receipt_content_id, receipt_content.content AS receipt_content,
			receipt_content.signature_line AS receipt_content_signature_line, 
			receipt_content.separate_receipt AS receipt_content_separate_receipt", false);
        $this->db->from('items AS item');
        $this->db->join('item_receipt_content AS receipt_content', 'receipt_content.receipt_content_id = item.receipt_content_id', 'left');
        $this->db->join('items_taxes AS tax', 'tax.item_id = item.item_id', 'left');
        $this->db->where('item.course_id', $course_id);
        $this->db->like('item.item_number', "{$course_id}_seasonal_", 'after');
        $this->db->group_by('item.item_id');

        $item_rows = $this->db->get()->result_array();

        return $item_rows;		
	}

	/*
	 Determines if a price class has already been applied to a season
	*/
	function used($class_id, $season_id)
	{
		$query = $this->db
			->from('price_classes')
			->join('seasonal_timeframes', 'seasonal_timeframes.class_id = price_classes.class_id')
			->where('seasonal_timeframes.class_id',$class_id)
			->where('seasonal_timeframes.season_id',$season_id)
			->limit(1)
			->get();

		return ($query->num_rows()==1);
	}
}
?>
