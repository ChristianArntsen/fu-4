<?php
class Table_Receipt extends CI_Model
{
	function __construct(){
		parent::__construct();
		$this->load->model('v2/item_model');
		$this->load->library('v2/cart_lib');
	}

	// Check if current user has permission to access a sale
	function has_sale_access($sale_id)
	{
		$course_id = $this->session->userdata('course_id');
		$this->db->select('table_id');
		$result = $this->db->get_where('foreup_tables', array('sale_id'=>(int) $sale_id, 'course_id'=>$course_id));

		if($result->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function receipt_exists($sale_id, $receipt_id){
		$this->db->select('receipt_id');
		$query = $this->db->get_where("table_receipts", array('receipt_id'=>$receipt_id, 'sale_id'=>$sale_id));

		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function is_on_receipt($sale_id, $line){
		$this->db->select('receipt_id');
		$query = $this->db->get_where("table_receipt_items", array('line'=>$line, 'sale_id'=>$sale_id));

		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function update_gratuity($sale_id, $receipt_id, $gratuity, $gratuity_type = 'percentage'){
		$gratuity = (float) $gratuity;
		$gratuity_type = $this->db->escape($gratuity_type);

		$query = $this->db->query("UPDATE foreup_table_receipts
			SET auto_gratuity = {$gratuity}, auto_gratuity_type = {$gratuity_type}
			WHERE sale_id = ".(int) $sale_id."
				AND receipt_id = ". (int) $receipt_id);
		return true;
	}

	function mark_paid($sale_id, $receipt_id){
		$query = $this->db->query("UPDATE foreup_table_receipts
			SET date_paid = NOW(),
				status = 'complete'
			WHERE sale_id = ".(int) $sale_id."
				AND receipt_id = ". (int) $receipt_id);
		return true;
	}

	function mark_unpaid($sale_id, $receipt_id){
		$query = $this->db->query("UPDATE foreup_table_receipts
			SET date_paid = NULL,
				status = 'pending'
			WHERE sale_id = ".(int) $sale_id."
				AND receipt_id = ". (int) $receipt_id);
		return true;
	}

	function complete($sale_id, $receipt_id){
		$success = $this->db->update('table_receipts',
			array('status' => 'complete', 'date_paid' => date('Y-m-d h:i:s')),
			array('sale_id' => $sale_id, 'receipt_id' => $receipt_id)
		);

		return $success;
	}

	function can_delete($sale_id, $receipt_id){

		if(empty($sale_id) || empty($receipt_id)){
			return false;
		}

		$query = $this->db->query("SELECT COUNT(receipt_items.line) AS count, receipt_items.line
			FROM ".$this->db->dbprefix('table_receipt_items')." AS receipt_items
			INNER JOIN (SELECT ri.line
				FROM ".$this->db->dbprefix('table_receipt_items')." AS ri
				WHERE ri.sale_id = ".(int) $sale_id."
				AND ri.receipt_id = ".(int) $receipt_id.") AS item_filter
				ON item_filter.line = receipt_items.line
			WHERE receipt_items.sale_id = ".(int) $sale_id."
			GROUP BY receipt_items.line");
		$rows = $query->result_array();

		// If receipt is empty, its ok to delete
		if($query->num_rows == 0){
			return true;
		}

		// If receipt contains items that do NOT belong on other receipts
		// disallow deletion
		foreach($rows as $row){
			if($row['count'] <= 1){
				return false;
			}
		}

		return true;
	}

	// Check if there is at least 1 open receipt to add new items to.
	// If there are no receipts avaialable, create new one
	function get_available_receipt($sale_id){
		$this->db->select('receipt_id');
		$this->db->where('status', 'pending');
		$this->db->where('sale_id', $sale_id);
		$this->db->from('table_receipts');
		$query = $this->db->get();

		if($query->num_rows == 0){
			$receipt_id = $this->save($sale_id);
		}else{
			$row = $query->row_array();
			$receipt_id = (int) $row['receipt_id'];
		}

		return $receipt_id;
	}

	function get_next_receipt_num($sale_id){
		$query = $this->db->query("SELECT MAX(receipt_id) AS receipt_id
			FROM ".$this->db->dbprefix('table_receipts')."
			WHERE sale_id = ".(int) $sale_id."
			GROUP BY sale_id");
		$row = $query->row_array();
		
		if(empty($row['receipt_id'])){
			$row['receipt_id'] = 0;
		}
		
		return (int) $row['receipt_id'] + 1;
	}
	
	function comp_items($sale_id, $receipt_id, $type = '', $description = '', $amount = 0, $employee_id = false){
		
		$this->load->model('Item_comp');
		
		$this->db->select('sale_id, item_id, line');
		$this->db->from('table_receipt_items');
		$this->db->where('sale_id', (int) $sale_id);
		$this->db->where('receipt_id', (int) $receipt_id);
		$rows = $this->db->get()->result_array();
		
		if(!empty($rows)){
			foreach($rows as $row){
				$this->Item_comp->save($sale_id, $row['line'], $row['item_id'], $type, $description, $amount, $employee_id);
			}
		}
		
		return true;
	}
	
	function clear_comps($sale_id, $receipt_id){
		
		$receipt_id = (int) $receipt_id;
		$sale_id = (int) $sale_id;
		
		$this->db->query("DELETE comp.* 
			FROM foreup_table_comps AS comp
			INNER JOIN foreup_table_receipt_items AS item
				ON item.line = comp.line
			WHERE item.receipt_id = {$receipt_id}
				AND item.sale_id = {$sale_id}");
		
		return $this->db->affected_rows();
	}

	function new_receipt($sale_id)
    {
        $this->db->query("
			INSERT INTO foreup_table_receipts (sale_id,receipt_id)
			(SELECT {$sale_id}, if( max(receipt_id) is not null, max(receipt_id)+ 1, 1) FROM foreup_table_receipts
			WHERE sale_id = '{$sale_id}');
		");

        if ($this->db->affected_rows()) {
            $id = $this->db->insert_id();
            $this->db->select('r.receipt_id');
            $this->db->from('table_receipts AS r');
            $this->db->where('r.id', $id);
            $query = $this->db->get();
            $rows = $query->result_array();
            return $rows[0]['receipt_id'];
        }
    }
	
	// Saves a receipt
	function save($sale_id, $receipt_id = null, $items = null, $gratuity = false, $gratuity_type = 'percentage', $taxable = null, $customer = null){
		if(!$this->has_sale_access($sale_id)){
			return false;
		}

		if(empty($receipt_id)){
			$receipt_id = $this->new_receipt($sale_id);
		}

		// TODO: sometimes get an fkey error because an existing receipt gets in
		// Prolly a race condition, but what triggers it?
		if(!$this->receipt_exists($sale_id, $receipt_id)){
			$receipt_id = $this->new_receipt($sale_id);
			if($taxable === null){
				$taxable = 1;
			}
			
			$table = $this->db->select('guest_count')
				->from('foreup_tables')
				->where('sale_id', $sale_id)
				->get()->row_array();
			$guest_count = (int) $table['guest_count'];

			$auto_gratuity = 0;
			if($guest_count >= $this->config->item('auto_gratuity_threshold')){
				$auto_gratuity = (float) $this->config->item('auto_gratuity');
				$gratuity_type = 'percentage';
			}

			$this->db->update('table_receipts',
				array(
					'auto_gratuity' => $auto_gratuity,
					'auto_gratuity_type' => $gratuity_type,
					'taxable' => (int) $taxable,
					'customer_id' => null
				),
				array('receipt_id' => $receipt_id, 'sale_id' => $sale_id)
			);
		}

		if ($gratuity !== false){
			$this->update_gratuity($this->suspended_sale_id, $receipt_id, $gratuity, $gratuity_type);
		}
		
		if($taxable !== null){
			$this->db->update('table_receipts', 
				array('taxable' => (int) $taxable), 
				array('receipt_id' => $receipt_id, 'sale_id' => $sale_id)
			);
		}
		
		if($customer !== null){
			if($customer['person_id']){
				$customer_id = (int) $customer['person_id'];
			}else{
				$customer_id = null;
			}
			
			$this->db->update('table_receipts', 
				array('customer_id' => $customer_id), 
				array('receipt_id' => $receipt_id, 'sale_id' => $sale_id)
			);
		}		
		
		// If items were passed as well, insert those into database
		if(!empty($items)){
			$item_batch = array();

			// Conform the item array for DB insertion
			foreach($items as $item){
				$item_batch[] = array('receipt_id'=>$receipt_id, 'sale_id'=>$sale_id, 'item_id'=>$item['item_id'], 'line'=>$item['line']);
			}
			$this->db->insert_batch('table_receipt_items', $item_batch);
		}

		return $receipt_id;
	}

	function setCompletedSale($sale_id,$receipt_id,$completed_sale_id){
		$success = $this->db->update('table_receipts',
			array('completed_sale_id'=>$completed_sale_id),
			array('sale_id' => $sale_id, 'receipt_id' => $receipt_id)
		);

		return $success;
	}

	// Retrieves all receipts and all items attached to those receipts
	function get($sale_id, $receipt_filter_id = false){

		$this->load->model('Table');
		$side_types = array('soups', 'salads', 'sides');

		// Retrieve all items from table (sale_id)
		$items = $this->Table->get_items($sale_id);
		$cart_items = array();
		
		// Organize table items by line
		foreach($items as $item){
			$cart_items[$item['line']] = $item;
		}
		unset($items);
		
		// Retrieve all payments made for this table
		$payment_rows = $this->Table->get_payments($sale_id, $receipt_filter_id);
		$payments = array();
		foreach($payment_rows as $row){
			$receipt_id = $row['receipt_id'];
			unset($row['sale_id'], $row['receipt_id']);
			$payments[$receipt_id][$row['payment_type']] = array(
				'type' => $row['payment_type'],
				'amount' => $row['payment_amount'],
				'customer_id' => $row['customer_id'],
				'number' => $row['number'],
				'signature' => $row['signature'],
				'tran_type' => isset($row['tran_type'])?$row['tran_type']:null,
				'auth_code' => isset($row['auth_code'])?$row['auth_code']:null,
				'credit_card_invoice_id' => isset($row['credit_card_invoice_id'])?$row['credit_card_invoice_id']:null,
				'credit_card_id' => isset($row['credit_card_id'])?$row['credit_card_id']:null
			);
		}
		unset($payment_rows);

		// Retrieve all receipts with items associated
		// Even if only retrieving single receipt, it is necessary to
		// retrieve all receipts to calculate which items are split
		$this->db->select('r.receipt_id, r.taxable, r.status, r.date_created AS receipt_date,
			r.date_paid, r.auto_gratuity, r.auto_gratuity_type, r.sale_id, ri.line AS item_line,
			p.first_name, p.last_name, c.member_account_balance, c.member_account_balance_allow_negative,
			c.account_balance, c.account_balance_allow_negative, c.taxable AS customer_taxable, 
			c.discount AS customer_discount, c.member, c.use_loyalty,
			r.customer_id,r.completed_sale_id', false);
		$this->db->from('table_receipts AS r');
		$this->db->join('people AS p', 'p.person_id = r.customer_id', 'left');
		$this->db->join('customers AS c', 'c.person_id = r.customer_id', 'left');
		$this->db->join('table_receipt_items AS ri', 'ri.receipt_id = r.receipt_id AND ri.sale_id = r.sale_id', 'left');
		$this->db->where('r.sale_id', $sale_id);
		$this->db->group_by('r.receipt_id, ri.line');
		$this->db->order_by('r.receipt_id ASC, ri.line ASC');
		$query = $this->db->get();

		$rows = $query->result_array();
		$receipts = array();
		$item_counts = array();

		// Loop through receipt items, create multi-dimensional array by receipt
		foreach($rows as $item){
			$receipt_id = $item['receipt_id'];
			$receipts[$receipt_id]['date_created'] = $item['receipt_date'];
			$receipts[$receipt_id]['sale_id'] = $item['sale_id'];
			$receipts[$receipt_id]['receipt_id'] = $receipt_id;
			$receipts[$receipt_id]['auto_gratuity'] = $item['auto_gratuity'];
			$receipts[$receipt_id]['auto_gratuity_type'] = $item['auto_gratuity_type'];
			$receipts[$receipt_id]['date_paid'] = $item['date_paid'];
			$receipts[$receipt_id]['status'] = $item['status'];
			$receipts[$receipt_id]['taxable'] = (int) $item['taxable'];
			$receipts[$receipt_id]['completed_sale_id'] = (int) $item['completed_sale_id'];
			
			if(!empty($item['customer_id'])){
				$receipts[$receipt_id]['customer']['person_id'] = (int) $item['customer_id'];
				$receipts[$receipt_id]['customer']['first_name'] = $item['first_name'];
				$receipts[$receipt_id]['customer']['last_name'] = $item['last_name'];
				$receipts[$receipt_id]['customer']['member_account_balance'] = $item['member_account_balance'];
				$receipts[$receipt_id]['customer']['member_account_balance_allow_negative'] = $item['member_account_balance_allow_negative'];
				$receipts[$receipt_id]['customer']['account_balance'] = $item['account_balance'];
				$receipts[$receipt_id]['customer']['account_balance_allow_negative'] = $item['account_balance_allow_negative'];
				$receipts[$receipt_id]['customer']['taxable'] = (int) $item['customer_taxable'];
				$receipts[$receipt_id]['customer']['discount'] = (float) $item['customer_discount'];
				$receipts[$receipt_id]['customer']['member'] = (int) $item['member'];
				$receipts[$receipt_id]['customer']['use_loyalty'] = (int) $item['use_loyalty'];
			}

			if(isset($payments[$receipt_id])){
				$receipts[$receipt_id]['payments'] = array_values($payments[$receipt_id]);
			}

			if(!isset($receipts[$receipt_id]['total'])){
				$receipts[$receipt_id]['total'] = 0;
			}
			$receipts[$receipt_id]['total_due'] = 0;
			$receipts[$receipt_id]['tax'] = 0;
			$receipts[$receipt_id]['subtotal'] = 0;
			$receipts[$receipt_id]['comp_total'] = 0;
			$receipts[$receipt_id]['total_loyalty_cost'] = 0;
			$receipts[$receipt_id]['total_loyalty_reward'] = 0;
			$receipts[$receipt_id]['total_loyalty_dollars'] = 0;

			// If row is an item, add the item to the proper receipt
			if(!empty($item['item_line'])){
				unset($item['receipt_id'], $item['receipt_date'], $item['sale_id']);

				if(empty($cart_items[$item['item_line']])){
					continue;
				}			
							
				$cart_item = $cart_items[$item['item_line']];
				$item = $cart_item;
				$receipts[$receipt_id]['items'][] = $item;

				if(!isset($item_counts[$item['line']])){
					$item_counts[$item['line']] = 1;
				}else{
					$item_counts[$item['line']]++;
				}
			}
		}

		// Loop through each receipt and it's items, calculate actual prices
		// based on how receipt is split up
        $ItemPrice = new \fu\items\ItemPrice();
		foreach($receipts as $receipt_key => &$receipt){
			$items_subtotal = 0;
			if(!empty($receipt['items'])){
				
				foreach($receipt['items'] as $item_key => &$item){

					$divisor = $item_counts[$item['line']];
					$result = $ItemPrice->calculatePrice($item, $receipt['taxable'], $divisor);
					
					// Add item + sides total to final receipt total
                    $receipt['subtotal'] += $result['subtotal'];
                    $receipt['tax'] += $result['tax'];
                    $receipt['comp_total'] += $result['comp_total'];
                    $receipt['total'] += $result['total'];
                    $receipt['total_loyalty_reward'] += $result['total_loyalty_reward'];
                    $receipt['total_loyalty_cost'] += $result['total_loyalty_cost'];
                    $receipt['total_loyalty_dollars'] += $result['total_loyalty_dollars'];
				}
			}
			
			// Round receipt totals for good measure
            $receipt['subtotal'] = round($receipt['subtotal'], 2);
            $receipt['tax'] = round($receipt['tax'], 2);
            $receipt['comp_total'] = round($receipts[$receipt_key]['comp_total'], 2);
            $receipt['total'] = round($receipt['subtotal'] + $receipt['tax'], 2);

			// Get auto gratuity
			if($this->config->item('service_fee_active') == 1){
				$service_fee = $this->get_service_fee(
                    $receipt['subtotal'],
                    $receipt['auto_gratuity'],
                    $receipt['auto_gratuity_type'],
                    $receipt['taxable']
				);
				$receipts[$receipt_key]['auto_gratuity_amount'] = 0;
				
				if($service_fee){
                    $receipt['service_fee'] = $service_fee;
                    $receipt['tax'] += $service_fee['tax'];
                    $receipt['total'] += $service_fee['total'];
				}else{
                    $receipt['service_fee'] = false;
				}

			}else{
			    // calculates based on receipt subtotal
                $receipt['auto_gratuity_amount'] = $this->calculate_auto_gratuity(
                    $receipt['subtotal'],
                    $receipt['auto_gratuity'],
                    $receipt['auto_gratuity_type']
				);
                $receipt['service_fee'] = false;
                $receipt['total'] += $receipt['auto_gratuity_amount'];
			}

			// Calculate the amount due on the receipt
            $receipt['total_paid'] = 0;
			if(!empty($payments[$receipt_key])){
				foreach($payments[$receipt_key] as $payment){
                    $receipt['total_paid'] += (float) round($payment['amount'], 2);
				}
			}
            $receipt['total_due'] = round($receipt['total'] - $receipt['total_paid'], 2);
		}

		if(!empty($receipt_filter_id)){
			if(!empty($receipts[$receipt_filter_id]))
			    return $receipts[$receipt_filter_id];
			else
				return array();
		}
		
		return array_values($receipts);
	}

	function has_payments($sale_id,$receipt_id){
		$payment_rows = $this->Table->get_payments($sale_id, $receipt_id);
		return $payment_rows;
	}

	function calculate_auto_gratuity($total, $gratuity, $type = 'percentage'){
		
		if($type == 'dollar'){
			return (float) $gratuity;
		}
		$percent = (float) $gratuity / 100;
		return (float) round($total * $percent, 2);
	}

	function get_service_fee($total, $gratuity, $type = 'percentage', $taxable = 1){
		
		$gratuity_amount = $this->calculate_auto_gratuity($total, $gratuity, $type);
		
		if(empty($gratuity) || empty($gratuity_amount)){
			return false;
		}

		$fee_item = $this->item_model->get_special_item('service_fee');
		$fee_item['unit_price'] = $fee_item['split_price'] = $fee_item['split_subtotal'] = $fee_item['price'] = $fee_item['subtotal'] = $gratuity_amount;
		$fee_item['quantity'] = 1;
		$fee_item['discount'] = $fee_item['discount_percent'] = 0;

		if($taxable == 1 || (isset($fee_item['force_tax']) && $fee_item['force_tax'] == 1)){
			$fee_item['tax'] = $fee_item['split_tax'] = $this->cart_lib->calculate_tax($fee_item['subtotal'], $fee_item['taxes']);
			$fee_item['taxable'] = true;
		}else{
			$fee_item['tax'] = 0;
			$fee_item['taxable'] = false;
		}
		
		$fee_item['total'] = $fee_item['split_total'] = $fee_item['tax'] + $fee_item['subtotal'];
		
		return $fee_item;
	}

	function get_items($sale_id, $receipt_id = false){

		if(empty($receipt_id)){
			return false;
		}
		$this->load->model('Table');

		$this->db->select('line', false);
		$this->db->from('table_receipt_items');
		$this->db->where('sale_id', $sale_id);
		$this->db->where('receipt_id', $receipt_id);
		$query = $this->db->get();

		$rows = $query->result_array();
		$lines = array();

		if(empty($rows)){
			return array();
		}

		foreach($rows as $row){
			$lines[] = $row['line'];
		}

		// Retrieve specific items from cart
		$items = $this->Table->get_items($sale_id, $lines);
		return $items;
	}

	function can_delete_item($sale_id, $line){
		$this->db->select('line', false);
		$this->db->from('table_receipt_items');
		$this->db->where('sale_id', $sale_id);
		$this->db->where('line', $line);
		$query = $this->db->get();

		if($query->num_rows() > 1){
			return true;
		}else{
			return false;
		}
	}

	// Adds a new item to a receipt
	function add_item($item_id, $line, $sale_id, $receipt_id){
		
		if(!$this->receipt_exists($sale_id, $receipt_id)){
			$this->save($sale_id, $receipt_id);
		}
		
		$this->db->query("INSERT IGNORE INTO foreup_table_receipt_items (sale_id, receipt_id, item_id, line)
			VALUES (".(int) $sale_id.",".(int) $receipt_id.",".(int) $item_id.",".(int) $line.")");
		return true;
	}

	// Removes an item from a receipt
	function delete_item($item_id, $line, $sale_id, $receipt_id = null){

		if(!$this->has_sale_access($sale_id)){
			return false;
		}
		$where['line'] = $line;
		$where['sale_id'] = $sale_id;

		if(!empty($receipt_id)){
			$where['receipt_id'] = $receipt_id;
		}
		$results = $this->db->delete('table_receipt_items', $where);
		
		return $results;
	}

	// Delete entire receipt including receipt items associated with it
	function delete($sale_id, $receipt_id = false){

		if(!$this->has_sale_access($sale_id)){
			return false;
		}
		if(empty($sale_id) || $receipt_id === false){
			return false;
		}

		$result = $this->db->query("DELETE r.*, ri.*
			FROM foreup_table_receipts AS r
			LEFT JOIN foreup_table_receipt_items AS ri
				ON ri.sale_id = r.sale_id
				AND ri.receipt_id = r.receipt_id
			WHERE r.sale_id = ".(int) $sale_id."
				AND r.receipt_id = ".(int) $receipt_id);

		return $result;
	}

	function is_paid($sale_id, $receipt_id = false){
		if(empty($sale_id)){
			return false;
		}

		$where = "r.status = 'pending' AND r.sale_id = ".(int) $sale_id;
		if(!empty($receipt_id)){
			$where .= " AND r.receipt_id = ".(int) $receipt_id;
		}

		// Select any/all receipts that have not been paid
		// Ignore receipts with no items
		$query = $this->db->query("SELECT r.receipt_id
			FROM foreup_table_receipts AS r
			INNER JOIN foreup_table_receipt_items AS i
				ON i.receipt_id = r.receipt_id
				AND i.sale_id = r.sale_id
			WHERE {$where}
			GROUP BY r.receipt_id");
        $res = $query->result_array();

        if(empty($res))return true;
        /*
        $receipts = $this->get($sale_id,$receipt_id);

        $all_paid = true;
        foreach($receipts as $receipt){
            $receipt;
        }
*/
        return false;
	}
}
?>