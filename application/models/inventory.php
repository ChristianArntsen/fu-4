<?php
class Inventory extends CI_Model
{
	function insert($inventory_data)
	{
		if(empty($inventory_data['trans_items'])){
			return false;
		}
		if(isset($inventory_data['trans_user']) && ($inventory_data['trans_user'] === false || $inventory_data['trans_user'] === 0) ){
			$inventory_data['trans_user'] = null;
		}
	    $response = $this->db->insert('inventory',$inventory_data);
		return $response;
	}
	function update_time($sale_id, $date)
	{
        $this->db->where('sale_id', $sale_id);
		$this->db->update('inventory', array('trans_date'=>date('Y-m-d H:i:s', strtotime($date))));
	}
    function update_sale_id($suspended_sale_id, $sale_id)
    {
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('suspended_sale_id', $suspended_sale_id);
        $this->db->update('inventory', array('sale_id'=>$sale_id));
    }
	function get_inventory_data_for_item($item_id)
	{
		$result =$this->db->query("SELECT *, DATE_FORMAT(trans_date, '%c-%e-%y %l:%i %p') AS date FROM (`foreup_inventory`) WHERE `trans_items` = '$item_id' ORDER BY `trans_date` desc");
		return $result->result_array();
	}
}

?>