<?php

/*************************************************
 * change   author    date          remarks
 * 0.1      MBPogay   21-MAR-2012   check specific line changes related to the version for more info
 **************************************************/
class Employee extends Person
{
    function __construct()
    {

        $this->load->model('Employee_Audit_Log');
	    $this->load->model('employee_permissions');
    }
	/*
	Determines if a given person_id is an employee
	*/
	function exists($person_id)
	{
		$this->db->from('employees');
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->where('employees.person_id', $person_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows() == 1);
	}

	function username_exists($username, $employee_id)
	{
		$this->db->from('employees');
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->where('employees.username', $username);
		$this->db->where('employees.person_id !=', $employee_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows() == 1);
	}

	function course_pin_exists($course_id, $pin, $employee_id)
	{
		$this->db->from('employees');
		$this->db->where('course_id', $course_id);
		$this->db->where('pin', $pin);
		$this->db->where('person_id !=', $employee_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows() == 1);
	}

	/*
	Returns all the employees
	*/
	function get_all($limit = 10000, $offset = 0, $types = array(),$limit_to_foreup = false)
	{
		$course_id = '';
		if (!$this->permissions->is_super_admin())
			$course_id = " AND foreup_employees.user_level != 5 AND foreup_employees.course_id = '{$this->session->userdata('course_id')}'";
		if($limit_to_foreup){
			$course_id = " AND foreup_employees.course_id = '{$this->session->userdata('course_id')}'";
		}

		$this->db->from('employees');
		$this->db->where("foreup_employees.deleted = 0 $course_id");
		$this->db->join('people', 'employees.person_id=people.person_id');
		$this->db->order_by("activated asc, last_name asc");
		//$this->db->order_by("last_name", "asc");
		if (is_array($types) && count($types) > 0)
			$this->db->where_in('employees.user_level', $types);
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}

	function get_all_fnb($limit = 10000, $offset = 0, $types = array())
	{
		$course_id = '';
		if (!$this->permissions->is_super_admin()) {
			$course_id = "AND foreup_employees.course_id = '{$this->session->userdata('course_id')}'";
		}
		$this->db->from('employees');
		$this->db->where("foreup_employees.deleted = 0 $course_id");
		$this->db->join('people', 'employees.person_id = people.person_id');
		$this->db->join('permissions', "permissions.person_id = employees.person_id AND foreup_permissions.module_id = 'food_and_beverage'", 'inner');
		$this->db->order_by("activated asc, last_name asc");
		$this->db->order_by("last_name", "asc");
		if (is_array($types) && count($types) > 0) {
			$this->db->where_in('employees.user_level', $types);
		}
		$this->db->limit($limit);
		$this->db->offset($offset);
		$query = $this->db->get();
		return $query->result_array();
	}

	function count_all()
	{
		$course_id = '';
		if (!$this->permissions->is_super_admin())
			$course_id = " AND foreup_employees.user_level != 5 AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('employees');
		$this->db->where("deleted = 0 $course_id");
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular employee
	*/
	function get_info($employee_id, $reset = false)
	{
		$course_id = '';
		if (!$this->permissions->is_super_admin() && !$reset)
			$course_id = " AND foreup_employees.course_id  = '{$this->session->userdata('course_id')}'";
		$this->db->from('employees');
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->where("foreup_employees.person_id = '$employee_id' $course_id");
		$this->db->limit(1);
		$query = $this->db->get();

		$this->load->model("acl");
        $this->load->model("Acl_roles");

		if ($query->num_rows() == 1) {
			$data = $query->row();
			$data->permissions = $this->acl->get_user_permissions_array();
            $data->roles = $this->Acl_roles->get_acl_roles(['employee_id' => $data->person_id]);

			return $data;
		} else {
			//Get empty base parent object, as $employee_id is NOT an employee
			$person_obj = parent::get_info(-1);

			//Get all the fields from employee table
			$fields = $this->db->list_fields('employees');

			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field) {
				$person_obj->$field = '';
			}

			return $person_obj;
		}
	}

	/*
	Gets information about a particular employee by referencing their pin or card #
	*/
	function get_info_by_pin_or_card($pin_or_card = false)
	{
		if ($pin_or_card) {
			$this->db->from('employees');
			$this->db->join('people', 'people.person_id = employees.person_id', 'left');
			$this->db->where("course_id", $this->session->userdata('course_id'));
			$this->db->where("deleted", 0);
			$this->db->where("(card = '$pin_or_card' OR pin = '$pin_or_card')");
			$this->db->limit(1);
			$query = $this->db->get();
		}
		if ($pin_or_card && $query->num_rows() == 1) {
			return $query->row();
		} else {
			//Get empty base parent object, as $employee_id is NOT an employee
			$person_obj = parent::get_info(-1);

			//Get all the fields from employee table
			$fields = $this->db->list_fields('employees');

			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field) {
				$person_obj->$field = '';
			}

			return $person_obj;
		}
	}

	/*
	Gets information about multiple employees
	*/
	function get_multiple_info($employee_ids)
	{
		$this->db->from('employees');
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->where_in('employees.person_id', $employee_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates an employee
	*/
	function save(&$person_data, &$employee_data, &$permission_data, $employee_id = false, $detailed_permissions = array(),$log_entry = false)
	{

		// do we know a person_id?
		// if so, ensure it is consistent across the two variables that store it.
		if(!$employee_id && isset($person_data['person_id'])){
			$employee_id = $person_data['person_id'];
		}
		elseif($employee_id && $employee_id > 0 && !isset($person_data['person_id'])){
			$person_data['person_id'] = $employee_id;
			$employee_data['person_id'] = $employee_id;
		}
		elseif($employee_id && $employee_id > 0 && isset($person_data['person_id']) &&
			$employee_id !== isset($person_data['person_id'])){
			// TODO: throw exception here due to ambiguous person_id
			return false;
		}

		$success = false;

		//Run these queries as a transaction, we want to make sure we do all or nothing
		//$this->db->trans_start();
		//	echo '0ts --- ';
		//      if ($this->db->trans_status()===false) echo 'false';else echo 'true';
        $mass = false;
        if(!is_array($log_entry)) {

			if(isset($_SESSION['foreup']['emp_id']) && $_SESSION['foreup']['emp_id']*1>0) {
				// not mass upsert
				$act_type = 7; // upsert
				$emp_id = $_SESSION['foreup']['emp_id'] * 1; //$this->session->userdata('emp_id')
				$comments = 'Employee saved.';
				//TODO: Need a way to resolve which tables were actually updated
				$tbls = 'foreup_employees';
				$gmt = date('Y-m-d H:i:s');

				$log_entry = array(
					'editor_id' => $emp_id,
					'gmt_logged' => $gmt,
					'action_type' => $act_type, // mass_upsert
					'comments' => $comments,
					'tables_updated' => $tbls
				);

				$this->db->query("INSERT INTO foreup_employee_audit_log (editor_id,action_type_id,comments,gmt_logged) VALUES ('$emp_id',$act_type,'$comments','$gmt')");
				$eal_id = $this->db->query("SELECT LAST_INSERT_ID() as id")->result_array()[0]['id'];
				$log_entry['employee_audit_log_id'] = $eal_id;
			}
        }
        else {
            // pull into their variables
            $mass = true;
            $act_type = $log_entry['action_type'];
            $emp_id = $log_entry['editor_id'];
            $comments = $log_entry['comments'];
            //TODO: Need a way to resolve which tables were actually updated
            $tbls = $log_entry['tables_updated'];
            $gmt = $log_entry['gmt_logged'];
            $eal_id = $log_entry['employee_audit_log_id'];
        }

        $hourly_rate = null;
        if(isset($person_data['hourly_rate'])){
            $hourly_rate = $person_data['hourly_rate'];
        }
        unset($person_data['hourly_rate']);

		if (parent::save_person($person_data, $employee_id, $log_entry)) {
			//	echo '1ts --- ';
			//    if ($this->db->trans_status()===false) echo 'false';else echo 'true';
			if (isset($employee_data['username']) && $this->username_exists($employee_data['username'], $employee_id)) {
				$employee_data['error'] = 'Username is already in use. Please select another one.';
				$success = false;
			} else if ($this->course_pin_exists($employee_data['course_id'], $employee_data['pin'], $employee_id) && $employee_data['pin'] != '') {
				$employee_data['error'] = 'Pin is already in use.';
				$success = false;
			} else if (!$employee_id or !$this->exists($employee_id)) {
				$employee_data['person_id'] = $employee_id = $person_data['person_id'];
				$success = $this->db->insert('employees', $employee_data);
			} else {
				$this->db->where('person_id', $employee_id);
				$this->db->limit(1);
				$success = $this->db->update('employees', $employee_data);
                //TODO: In the case where this person is an employee at two courses, would it potentially update the wrong course?
			}

			$this->load->model("acl_roles");
			$role_id = $this->acl_roles->set_role_from_userlevel($employee_data['user_level'],$employee_data['person_id']);
            $this->acl_roles->save_employee_acl_role($employee_data['person_id'], $role_id, ['hourly_rate' => $hourly_rate]);

			//We have either inserted or updated a new employee, now lets set permissions.
			if ($success && !$this->permissions->is_employee()) {
				//First lets clear out any permissions the employee currently has.
				$success = $this->db->delete('permissions', array('person_id' => $employee_id));

				//Now insert the new permissions
				if ($success) {
					foreach ($permission_data as $allowed_module) {
						$success = $this->db->insert('permissions',
							array(
								'module_id' => $allowed_module,
								'person_id' => $employee_id));
					}
					// UPDATE EMPLOYEE SPECIFIC PERMISSIONS
					$this->employee_permissions->save($detailed_permissions, $employee_id);
				}
			}
		}
		//      echo '3ts --- ';
		//      if ($this->db->trans_status()===false) echo 'false';else echo 'true';

		//    $this->db->trans_complete();
		//echo $this->db->last_query();
        // successfully saved our employee!
        $pers_alt = $person_data['person_id']*1;

        // TODO: Errors out sometimes, can't have this break anything
        //$this->db->query("INSERT INTO foreup_person_altered (employee_audit_log_id,person_edited,tables_updated) VALUES ($eal_id,$pers_alt,'$tbls')");
		return $success;
	}
	
	
	

	function save_image($person_id, $image_id)
	{
		if (empty($person_id)) {
			return false;
		}
		return $this->db->update('employees', array('image_id' => (int)$image_id), "person_id = " . (int)$person_id);
	}

	/*
	Deletes one employee
	*/
	function delete($employee_id)
	{
		$course_id = '';
		if (!$this->permissions->is_super_admin())
			$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$success = false;

		//Don't let employee delete their self
		if ($employee_id == $this->get_logged_in_employee_info()->person_id)
			return false;

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		//Delete permissions
		if ($this->db->delete('permissions', array('person_id' => $employee_id))) {
			$this->db->where("person_id = '$employee_id' $course_id");
			$this->db->limit(1);
			$success = $this->db->update('employees', array('deleted' => 1, 'username' => null, 'pin' => null, 'card' => null));
		}
		$this->db->trans_complete();
		return $success;
	}

	/*
	Deletes a list of employees
	*/
	function delete_list($employee_ids)
	{
		$success = false;

		//Don't let employee delete their self
		if (in_array($this->get_logged_in_employee_info()->person_id, $employee_ids))
			return false;

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		$this->db->where_in('person_id', $employee_ids);
		//Delete permissions
		if ($this->db->delete('permissions')) {
			//delete from employee table
			if (!$this->permissions->is_super_admin())
				$this->db->where('course_id', $this->session->userdata('course_id'));
			$this->db->where_in('person_id', $employee_ids);
			$success = $this->db->update('employees', array('deleted' => 1, 'username' => null));
		}
		$this->db->trans_complete();
		return $success;
	}

	/*
	Get search suggestions to find employees
	*/
	function get_search_suggestions($search, $limit = 5, $name_only = false)
	{
		$course_id = '';
		if (!$this->permissions->is_super_admin())
			$course_id = " AND foreup_employees.user_level != 5 AND foreup_employees.course_id = '{$this->session->userdata('course_id')}'";
		$suggestions = array();

		$this->db->from('employees');
		$this->db->join('people', 'employees.person_id=people.person_id');
		$this->db->where("(first_name LIKE '%" . $this->db->escape_like_str($search) . "%' or
		last_name LIKE '%" . $this->db->escape_like_str($search) . "%' or
		CONCAT(`first_name`,' ',`last_name`) LIKE '%" . $this->db->escape_like_str($search) . "%') and deleted=0 $course_id");
		$this->db->order_by("last_name", "asc");
		$by_name = $this->db->get();
		foreach ($by_name->result() as $row) {
			$suggestions[] = array('label' => $row->first_name . ' ' . $row->last_name, 'value' => $row->person_id, 'course_id' => $row->course_id);
		}
		if (!$name_only) {
			$this->db->from('employees');
			$this->db->join('people', 'employees.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->like("email", $search);
			$this->db->order_by("email", "asc");
			$by_email = $this->db->get();
			foreach ($by_email->result() as $row) {
				$suggestions[] = array('label' => $row->email, 'value' => $row->person_id, 'course_id' => $row->course_id);
			}

			$this->db->from('employees');
			$this->db->join('people', 'employees.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->like("username", $search);
			$this->db->order_by("username", "asc");
			$by_username = $this->db->get();
			foreach ($by_username->result() as $row) {
				$suggestions[] = array('label' => $row->username, 'value' => $row->person_id, 'course_id' => $row->course_id);
			}


			$this->db->from('employees');
			$this->db->join('people', 'employees.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->like("phone_number", $search);
			$this->db->order_by("phone_number", "asc");
			$by_phone = $this->db->get();
			foreach ($by_phone->result() as $row) {
				$suggestions[] = array('label' => $row->phone_number, 'value' => $row->person_id, 'course_id' => $row->course_id);
			}
		}


		//only return $limit suggestions
		if (count($suggestions > $limit)) {
			$suggestions = array_slice($suggestions, 0, $limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on employees
	*/
	function search($search, $limit = 20, $course_id = '', $offset = 0)
	{
		$course_sql = '';
		if (!$this->permissions->is_super_admin())
			$course_sql = "  AND foreup_employees.user_level != 5 AND foreup_employees.course_id = '{$this->session->userdata('course_id')}'";
		else if ($course_id != '')
			$course_sql = " AND foreup_employees.course_id = '$course_id'";

		$this->db->from('employees');
		$this->db->join('people', 'employees.person_id=people.person_id');
		$this->db->where("(first_name LIKE '%" . $this->db->escape_like_str($search) . "%' or
		last_name LIKE '%" . $this->db->escape_like_str($search) . "%' or
		email LIKE '%" . $this->db->escape_like_str($search) . "%' or
		phone_number LIKE '%" . $this->db->escape_like_str($search) . "%' or
		username LIKE '%" . $this->db->escape_like_str($search) . "%' or
		CONCAT(`first_name`,' ',`last_name`) LIKE '%" . $this->db->escape_like_str($search) . "%') and deleted=0 $course_sql");
		$this->db->order_by("last_name", "asc");
		// Just return a count of all search results
		if ($limit == 0)
			return $this->db->get()->num_rows();
		// Return results
		$this->db->offset($offset);
		$this->db->limit($limit);
		return $this->db->get();
	}

	function can_override($person_id, $password)
	{
		$password = md5($password);
		$this->db->from('employees');
		$this->db->where("person_id = '$person_id' AND password = '" . $password . "' AND deleted = 0");
		$this->db->where_in("user_level", array(2, 3));
		$this->db->limit(1);
		$query = $this->db->get();
		//echo $this->db->last_query();
		if ($query->num_rows() == 1)
			return true;
		return false;
	}

	function is_valid_password($person_id = false, $password = false)
	{
		if (!$person_id || !$password)
			return false;
		$password = md5($password);
		$this->db->from('employees');
		$this->db->where("person_id = '$person_id' AND password = '" . $password . "' AND deleted = 0");
        // is it possible that a person can be an employee of more than one enterprise?
        // perhaps an accountant who is granted access for multiple courses in a town...
		$this->db->limit(1);
		$query = $this->db->get();
		//echo $this->db->last_query();
		if ($query->num_rows() == 1)
			return true;
		return false;
	}

	function login($username, $password, $override = false)
	{
		$this->load->model('employee_permissions');
		if ($this->permissions->is_super_admin() || $override)
			$user_password = $password;
		else {
			$user_password = md5($password);
		}

		$this->db->select('employees.person_id AS person_id,
							employees.user_level AS user_level,
							employees.course_id AS course_id,
							employees.activated AS activated,
							employees.id AS emp_id,
							courses.allow_employee_register_log_bypass,
							courses.name AS course_name,
							courses.timezone AS timezone,
							courses.open_time AS open_time,
							courses.close_time AS close_time,
							courses.early_bird_hours_begin AS early_bird_hours_begin,
							courses.early_bird_hours_end AS early_bird_hours_end,
							courses.morning_hours_begin AS morning_hours_begin,
							courses.morning_hours_end AS morning_hours_end,
							courses.afternoon_hours_begin AS afternoon_hours_begin,
							courses.afternoon_hours_end AS afternoon_hours_end,
							courses.twilight_hour AS twilight_hour,
							courses.super_twilight_hour AS super_twilight_hour,
							courses.holidays AS holidays,
							courses.zip AS zip,
							courses.auto_mailers AS auto_mailers,
							courses.config AS config,
							courses.courses AS courses,
							courses.customers AS customers,
							courses.dashboards AS dashboards,
							courses.employees AS employees,
							courses.events AS events,
							courses.food_and_beverage AS food_and_beverage,
							courses.food_and_beverage_v2 AS food_and_beverage_v2,
							courses.giftcards AS giftcards,
							courses.passes,
							courses.use_new_permissions,
							courses.invoices AS invoices,
							courses.item_kits AS item_kits,
							courses.items AS items,
							courses.marketing_campaigns AS marketing_campaigns,
							courses.promotions AS promotions,
							courses.quickbooks AS quickbooks,
							courses.receivings AS receivings,
							courses.reports AS reports,
							courses.reservations AS reservations,
							courses.sales AS sales,
							courses.sales_v2 AS sales_v2,
							courses.tee_sheet_speed_up AS tee_sheet_speed_up,
							courses.schedules AS schedules,
							courses.suppliers AS suppliers,
							courses.teesheets AS teesheets,
							courses.tournaments AS tournaments,
							courses.use_terminals AS use_terminals,
							courses.hide_back_nine AS hide_back_nine,
							courses.at_login AS at_login,
							courses.at_password AS at_password,
							courses.at_test AS at_test,
							courses.simulator AS simulator,
							courses.facebook_page_id AS facebook_page_id,
							courses.facebook_page_name AS facebook_page_name,
							courses.facebook_extended_access_token AS facebook_extended_access_token,
							courses.seasonal_pricing AS seasonal_pricing,
							courses.multiple_printers AS multiple_printers,
							courses.hide_modifier_names_kitchen_receipts,
							courses.food_bev_sort_by_seat,
							courses.default_register_log_open,
							courses.hide_taxable');
		$this->db->from('employees');
		$this->db->join('courses', 'employees.course_id=courses.course_id');
		$this->db->where("username = ".$this->db->escape($username)." AND password = " . $this->db->escape($user_password) . " AND deleted = 0");
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			$row = $query->row();
			$this->session->set_userdata('course_id', $row->course_id);
			if ($row->activated != 1)
				return false;




			//print_r($teesheet_info);
            $this->session->set_userdata('person_id', $row->person_id);
            $this->session->set_userdata('emp_id', $row->person_id);
			$this->session->set_userdata('user_level', $row->user_level);
			$this->session->set_userdata('course_name', $row->course_name);
			$this->session->set_userdata('openhour', $row->open_time);
			$this->session->set_userdata('closehour', $row->close_time);
			$this->session->set_userdata('zip', $row->zip);
			// 0.1 - change early_bird_hours_start to early_bird_hours_begin
			$this->session->set_userdata('early_bird_hours_begin', $row->early_bird_hours_begin);
			$this->session->set_userdata('early_bird_hours_end', $row->early_bird_hours_end);
			$this->session->set_userdata('morning_hours_begin', $row->morning_hours_begin);
			$this->session->set_userdata('morning_hours_end', $row->morning_hours_end);
			$this->session->set_userdata('afternoon_hours_begin', $row->afternoon_hours_begin);
			$this->session->set_userdata('afternoon_hours_end', $row->afternoon_hours_end);
			$this->session->set_userdata('twilight_hour', $row->twilight_hour);
			$this->session->set_userdata('super_twilight_hour', $row->super_twilight_hour);
			$this->session->set_userdata('auto_mailers', $row->auto_mailers);
			$this->session->set_userdata('config', $row->config);
			$this->session->set_userdata('courses', $row->courses);
			$this->session->set_userdata('customers', $row->customers);
			$this->session->set_userdata('dashboards', $row->dashboards);
			$this->session->set_userdata('employees', $row->employees);
			$this->session->set_userdata('events', $row->events);
			$this->session->set_userdata('food_and_beverage', $row->food_and_beverage);
			$this->session->set_userdata('food_and_beverage_v2', $row->food_and_beverage_v2);
			$this->session->set_userdata('use_new_permissions', $row->use_new_permissions);
			$this->session->set_userdata('giftcards', $row->giftcards);
			$this->session->set_userdata('passes', $row->passes);
			$this->session->set_userdata('item_kits', $row->item_kits);
			$this->session->set_userdata('items', $row->items);
			$this->session->set_userdata('invoices', $row->invoices);
			$this->session->set_userdata('marketing_campaigns', $row->marketing_campaigns);
			$this->session->set_userdata('promotions', $row->promotions);
			$this->session->set_userdata('quickbooks', $row->quickbooks);
			$this->session->set_userdata('receivings', $row->receivings);
			$this->session->set_userdata('reports', $row->reports);
			$this->session->set_userdata('reservations', $row->reservations);
			$this->session->set_userdata('sales', $row->sales);
			$this->session->set_userdata('sales_v2', $row->sales_v2);
			$this->session->set_userdata('tee_sheet_speed_up', $row->tee_sheet_speed_up);
			$this->session->set_userdata('schedules', $row->schedules);
			$this->session->set_userdata('suppliers', $row->suppliers);
			$this->session->set_userdata('teesheets', $row->teesheets);
			$this->session->set_userdata('tournaments', $row->tournaments);
			$this->session->set_userdata('is_simulator', $row->simulator);
			$this->session->set_userdata('use_terminals', $row->use_terminals);
			$this->session->set_userdata('hide_back_nine', $row->hide_back_nine);
			$this->session->set_userdata('facebook_page_id', $row->facebook_page_id);
			$this->session->set_userdata('facebook_page_name', $row->facebook_page_name);
			$this->session->set_userdata('facebook_extended_access_token', $row->facebook_extended_access_token);
			$this->session->set_userdata('seasonal_pricing', $row->seasonal_pricing);
			$this->session->set_userdata('multiple_printers', $row->multiple_printers);
			$this->session->set_userdata('hide_modifier_names_kitchen_receipts', $row->hide_modifier_names_kitchen_receipts);
			$this->session->set_userdata('food_bev_sort_by_seat', $row->food_bev_sort_by_seat);
			$this->session->set_userdata('default_register_log_open', $row->default_register_log_open);
            $this->session->set_userdata('hide_taxable', $row->hide_taxable);
            $this->session->set_userdata('allow_employee_register_log_bypass', $row->allow_employee_register_log_bypass);

//            echo 'almost there';
			/*
			 * Load up session variables for SCHEDULES
			 */
			if ($row->reservations) {
				//echo 'inside schedule section';
				$this->load->model('Schedule');
				$default_schedule = $this->Schedule->get_all(1)->result();
				//print_r($default_schedule);
				$default_schedule = $default_schedule[0];
				$schedule_info = $this->Schedule->get_info((string)$default_schedule->schedule_id);
				$schedule_id = $schedule_info->schedule_id;
				$this->session->set_userdata('schedule_id', $schedule_id);
				$this->session->set_userdata('schedule_type', $schedule_info->type);
				$this->session->set_userdata('teesheet_id', $schedule_id);
				$this->session->set_userdata('increment', $schedule_info->increment);
				$this->session->set_userdata('frontnine', $schedule_info->frontnine);
				$this->session->set_userdata('holes', $schedule_info->holes);
				$this->session->set_userdata('default_price_class', $schedule_info->default_price_class);
				$this->session->set_userdata('default_teesheet_id', $schedule_id);
				$this->session->set_userdata('default_schedule_id', $schedule_id);

			} else {
				$this->load->model('Teesheet');
				$default_teesheet = $this->Teesheet->get_all(1)->result();
				$default_teesheet = $default_teesheet[0];
				$teesheet_info = $this->Teesheet->get_info((string)$default_teesheet->teesheet_id);
				$teesheet_id = $teesheet_info->teesheet_id;
				$this->session->set_userdata('sunrise_offset', $teesheet_info->sunrise_offset);
				$this->session->set_userdata('sunset_offset', $teesheet_info->sunset_offset);
				$this->session->set_userdata('teesheet_id', $teesheet_id);
				$this->session->set_userdata('increment', $teesheet_info->increment);
				$this->session->set_userdata('frontnine', $teesheet_info->frontnine);
				$this->session->set_userdata('teesheet_color', $teesheet_info->color);
				$this->session->set_userdata('holes', $teesheet_info->holes);
				$this->session->set_userdata('default_price_class', $teesheet_info->default_price_class);
				$this->session->set_userdata('default_teesheet_id', $teesheet_id);
			}
			// SET DETAILED EMPLOYEE PERMISSIONS
			$permissions = $this->employee_permissions->get_all($row->person_id);
			// AUTO TURN ON PERMISSIONS IF ADMIN OR MANAGER
			$sales_stats = $permissions ? $permissions['sales_stats'] : ($row->user_level > 1 ? 1 : 0);
			$this->session->set_userdata('sales_stats', $sales_stats);

			$this->session->set_userdata('teesheet_reminder_count', 0);
			$this->session->set_userdata('taxable', 'true');
			$this->session->set_userdata('associated_courses', '');

			date_default_timezone_set($row->timezone);
			$this->db->where('person_id', $row->person_id);
			$this->db->update('employees', array('last_login' => date('Y-m-d h:i:s')));



			// Set JWT Token
			$jwt = new \fu\auth\json_web_token();

			// Set Private Claims
			$jwt->addPrivateClaim('uid', $this->session->userdata('person_id'));
			$jwt->addPrivateClaim('level', $this->session->userdata('user_level'));
			$jwt->addPrivateClaim('cid', $this->session->userdata('course_id'));
			$jwt->addPrivateClaim('emp_id', $this->session->userdata('emp_id'));
			$jwt->addPrivateClaim('employee', true);

			// Create the token
			$jwt->createToken();

			// Save the token to cookie.
			$jwt->setCookie(); // Returns false if cookie could not be set.


			return true;
		}
		return false;
	}


	/*
	Logs out a user by destorying all session data and redirect to login
	*/
	function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

	/*
	Determins if a employee is logged in
	*/
	function is_logged_in()
	{
		return $this->session->userdata('person_id') != false;
	}

	/*
	Gets information about the currently logged in employee.
	*/
	function get_logged_in_employee_info()
	{
		if ($this->is_logged_in()) {
			return $this->get_info($this->session->userdata('person_id'));
		}

		return false;
	}

	/*
	Determins whether the employee specified employee has access the specific module.
	*/
	function has_permission($module_id, $person_id)
	{
		//if no module_id is null, allow access
		if ($module_id == null) {
			return true;
		}
		$query = $this->db->get_where('permissions', array('person_id' => $person_id, 'module_id' => $module_id), 1);

		return $query->num_rows() == 1;
	}

	function get_employee_by_username_or_email($username_or_email)
	{
		$this->db->select('employees.*, peoples.email as email');
		$this->db->from('employees');
		$this->db->join('people as peoples', 'employees.person_id = peoples.person_id');
		$this->db->where("(username = '$username_or_email')");
		$this->db->where('deleted', 0);
		$this->db->where('activated', 1);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 1) {
			return $query->row();
		}
		return false;
	}

	function update_employee_password($employee_id, $password)
	{
		$employee_data = array('password' => $password);
		$this->db->where('person_id', $employee_id);
		$this->db->limit(1);
		$success = $this->db->update('employees', $employee_data);

		return $success;
	}

	function build_permissions()
    {
        $acl = new \fu\acl\Acl();

	    $this->load->model("acl");
	    $allPermissions = $this->acl->get_all_permissions();
	    $permissionsToReturn = [];

	    foreach($allPermissions as $permission){
	    	$permissionName = "\\".$permission;
		    $initializedClass = new $permissionName([]);
		    $actions = $initializedClass->getAvailableActions();
		    foreach($actions as $action){
			    $initializedClass->setActions([$action]);
			    $permissionsToReturn[str_replace('\\', '/', $permission).":".$action] = $acl->can($initializedClass);
		    }
	    }

        return $permissionsToReturn;
    }

	function get_logged_in_employee_name_by_id($employee_id)
	{
		$this->load->model("acl");

		$this->db->select('concat(first_name, " ",last_name) as employee_name', false);
		$this->db->from('people');
		$this->db->where("(person_id = '$employee_id')");
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			$data = $query->result_array();
			$data['permissions'] = $this->acl->get_user_permissions_array();
			return $data;
		}
		return false;
	}
}
