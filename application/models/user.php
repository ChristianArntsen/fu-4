<?php
use fu\auth;

class User extends Person
{
	/*
		Determines if a user exists
	*/
	function exists($person_id)
	{
		$this->db->from('users');
		$this->db->where('person_id', $person_id);
		$this->db->where('deleted != 1');
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows() == 1);
	}

	/*
		Gets information about a user base on username
	*/
	function get_by_username($username)
	{
		$this->db->select('p.person_id, p.first_name, p.last_name, p.email, u.email AS username');
		$this->db->from('users AS u');
		$this->db->join('people AS p', 'p.person_id = u.person_id');
		$this->db->where('u.email', $username);
		$this->db->where('u.deleted', 0);

		return $this->db->get()->row();
	}

	/*
		Returns all Users
	*/
	function get_all($limit = 10000, $offset = 0)
	{
//
		// $this->db->from('marketing_sendhub_accounts');
		// $this->db->where("deleted = 0 $course_id");
		// $this->db->order_by("send_date", "desc");
		// $this->db->limit($limit);
		// $this->db->offset($offset);
		// return $this->db->get();
	}

	function count_all()
	{
		// $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		// $this->db->from('marketing_sendhub_accounts');
		// $this->db->where("deleted = 0 $course_id");
		// return $this->db->count_all_results();
	}

	/*
		Gets information about a particular user
	*/
	function get_info($person_id)
	{
		$this->db->select('p.*, u.email AS username, u.password');
		$this->db->from('people AS p');
		$this->db->join('users AS u', 'p.person_id = u.person_id');
		$this->db->where('p.person_id', $person_id);
		$result = $this->db->get();

		$person_info = $result->row_array();

		if (isset($person_info['birthday']) && $person_info['birthday'] != '0000-00-00') {
			$person_info['birthday'] = date('m/d/Y', strtotime($person_info['birthday']));
		}

		return $person_info;
	}

	/*
	Gets information about multiple sendhub_accounts
	*/
	function get_multiple_info($sendhub_account_ids)
	{
		// $this->db->from('marketing_sendhub_accounts');
		// $this->db->where_in('sendhub_account_id',$sendhub_account_ids);
		// $this->db->where('deleted',0);
		// $this->db->order_by("send_date", "desc");
		// return $this->db->get();
	}

	function login($username, $password)
	{
		if ($this->permissions->is_super_admin()) {
			// how does this work?
			$user_password = $password;
		} else {
			$user_password = md5($password);
		}

		$this->db->select('p.person_id, p.first_name, p.last_name, p.phone_number, p.email');
		$this->db->from('users AS u');
		$this->db->join('people AS p', 'u.person_id = p.person_id');
		$this->db->where('u.email', $username);
		$this->db->where('u.password', $user_password);
		$this->db->limit(1);
		$query = $this->db->get();

		// If credentials were valid
		if ($query->num_rows() == 1) {
			$this->load->model('customer_merge');
			// Load user info into session
			$row = $query->row();
			$primary_customer_id = $this->customer_merge->get_primary($row->person_id);
			$this->session->set_userdata('customer_id', $primary_customer_id);
			$this->session->set_userdata('online_customer_id', $row->person_id);
			$this->session->set_userdata('first_name', $row->first_name);
			$this->session->set_userdata('last_name', $row->last_name);
			$this->session->set_userdata('phone_number', $row->phone_number);
			$this->session->set_userdata('customer_email', $row->email);
			$this->session->set_userdata('type', 'customer');

			// Set JWT Token
			$jwt = new auth\json_web_token();

			// Set Private Claims
			$jwt->addPrivateClaim('uid', $primary_customer_id);
			$jwt->addPrivateClaim('level', 0);
			$jwt->addPrivateClaim('cid', $this->session->userdata('course_id'));
			$jwt->addPrivateClaim('employee', false);

			// Create the token
			$jwt->createToken();

			// Save the token to cookie.
			$jwt->setCookie(); // Returns false if cookie could not be set.

			return true;
		}

		return false;
	}

	/*
		Determines if a given username exists in database
	*/
	function username_exists($username)
	{
		$this->db->from('users');
		$this->db->where('email', $username);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows() == 1);
	}

	function save($person_id, $user_data)
	{

		if(!$person_id && isset($user_data['person_id'])){
			$person_id = $user_data['person_id'];
		}
		elseif($person_id && $person_id > 0 && !isset($user_data['person_id'])){
			$user_data['person_id'] = $person_id;
		}
		elseif($person_id && $person_id > 0 && isset($user_data['person_id']) &&
			$person_id !== isset($user_data['person_id'])){
			// TODO: throw exception here due to ambiguous person_id
			return false;
		}

		$success = false;
		$this->load->helper('array');

		if (empty($user_data)) {
			return false;
		}

		$user_data = elements(array('email', 'password'), $user_data);

		if (empty($user_data['email'])) {
			unset($user_data['email']);
		}
		if (empty($user_data['password'])) {
			unset($user_data['password']);
		}

		if (empty($user_data)) {
			return false;
		}

		// If user already exists, update record
		if ($this->exists($person_id)) {
			$this->db->where('person_id', $person_id);
			$success = $this->db->update('users', $user_data);

			// If no user yet, insert record
		} else {
			$user_data['person_id'] = $person_id;
			$success = $this->db->insert('users', $user_data);
		}

		return $success;
	}

	function add_to_course($personId, $courseId)
	{

		$this->load->model('customer');

		if (empty($courseId) || empty($personId)) {
			return false;
		}

		// Check if user is already a customer of course
		if (!$this->customer->exists($personId)) {
            // Need to set loyalty setting according to course's settings
            $this->load->model('Course');
            $course_info = $this->Course->get_info($courseId);
            $auto_enroll_loyalty = !empty($course_info->loyalty_auto_enroll) ? $course_info->loyalty_auto_enroll : false;
			$this->db->insert('customers', array('person_id' => $personId, 'course_id' => $courseId, 'use_loyalty' => $auto_enroll_loyalty));
		} else {
			$this->db->where('person_id', $personId);
			$this->db->where('course_id', $courseId);
			$this->db->update('customers', array('deleted' => 0));
		}

		return true;
	}

	function update_password($person_id, $password)
	{
		$user_data = array('password' => $password);
		$this->db->where('person_id', $person_id);
		$success = $this->db->update('users', $user_data);

		return $success;
	}

	/*
	Updates multiple sendhub_accounts at once
	*/
	function update_multiple($sendhub_account_data, $sendhub_account_ids)
	{
		// $this->db->where_in('sendhub_account_id',$sendhub_account_ids);
		// return $this->db->update('marketing_sendhub_accounts',$sendhub_account_data);
	}

	/*
	Deletes one sendhub_account
	*/
	function delete($phone_number)
	{
		// $this->db->where("phone_number = $phone_number");
		// return $this->db->update('sendhub_accounts', array('deleted' => 1));
	}

	/*
	Deletes a list of sendhub_accounts
	*/
	function delete_list($sendhub_account_ids)
	{
		// $this->db->where('course_id', $this->session->userdata('course_id'));
		// $this->db->where_in('sendhub_account_id',$sendhub_account_ids);
		// return $this->db->update('marketing_sendhub_accounts', array('deleted' => 1));
	}

	/*
   Get search suggestions to find sendhub_accounts
   */
	function get_search_suggestions($search, $limit = 25)
	{
	}

	/*
	Preform a search on sendhub_accounts
	*/
	function search($search, $limit = 20, $offset = 0)
	{
	}

	/*
		Check if a user is logged in
	*/
	function is_logged_in()
	{
		return ($this->session->userdata('customer_id') != false);// && $this->session->userdata('type') == 'customer');
	}

	/*
		Logs out a user by destorying all session data and redirect to login
	*/
	function logout()
	{
		$course_id = $this->session->userdata('course_id');
		$this->session->sess_destroy();
		redirect("be/reservations/$course_id");
	}

	/*
		Retrieves all course customer accounts that are linked to the user
	*/
	function get_customer_accounts($personId, $courseId = null)
	{

		if (empty($personId)) {
			return false;
		}

		$this->db->select('course.name AS course_name, course.course_id, course.address AS course_address, course.city AS course_city,
			course.state AS course_state, course.postal AS course_postal, cust.account_number, cust.account_balance,
			cust.member_account_balance, cust.loyalty_points, cust.opt_out_email, cust.opt_out_text,
			cust.course_news_announcements, cust.teetime_reminders');
		$this->db->from('customers AS cust');
		$this->db->join('courses AS course', 'course.course_id = cust.course_id', 'inner');
		$this->db->join('people AS p', 'p.person_id = cust.person_id', 'inner');
		$this->db->where('cust.person_id', $personId);
		$this->db->where('cust.deleted', 0);
		$this->db->where('course.active', 1);

		if (!empty($courseId)) {
			$this->db->where('cust.course_id', $courseId);
		}

		$this->db->order_by("course.name ASC");
		$customer_accounts = $this->db->get();

		return $customer_accounts->result_array();
	}

	/*
		Retrieves all teetimes the user has made
	*/
	function get_teetimes($personId, $type = 'upcoming', $courseId = null)
	{

		if (empty($personId)) {
			return false;
		}
		$personId = (int)$personId;
		$courses_string = '';
		$courses_string_2 = "";
		if (!empty($courseId)) {
			if (is_array($courseId)) {
				$courses_string = " ts.course_id IN (" . implode(',', $courseId) . ") AND";
				$courses_string_2 = " course_id IN (" . implode(',', $courseId) . ") AND";
			} else {
				$courses_string = " ts.course_id IN (" . $courseId . ") AND";
				$courses_string_2 = " course_id IN (" . $courseId . ") AND";
			}
		}

		$timestamp = date('YmdHs') - 1000000;
		$time_sql = '';
		if ($type == 'upcoming') {
			$time_sql = " AND start > " . $timestamp . " AND date_cancelled = '0000-00-00 00:00:00'";
		}

		$result = $this->db->query("SELECT time.TTID, time.TTID AS teetime_id, time.type, time.start, time.end,
			time.allDay, time.holes, time.carts, time.player_count AS player_count, time.paid_player_count,
			time.paid_carts, time.clubs, time.title, time.cplayer_count, time.choles,
			time.ccarts, time.cpayment, time.phone, time.email, time.details, time.side,
			time.person_id, time.person_name,
			time.person_id_2, time.person_name_2,
			time.person_id_3, time.person_name_3,
			time.person_id_4, time.person_name_4,
			time.person_id_5, time.person_name_5,
			time.last_updated, time.date_booked, time.date_cancelled,
			time.booking_source, time.booker_id, time.canceller_id, time.teesheet_id,
			time.teed_off_time, time.turn_time, time.finish_time, time.booking_class_id,
			sheet.name AS course_name, sheet.course_id AS course_id, sheet.title AS teesheet_title
        FROM (SELECT * FROM `foreup_teetime` WHERE person_id = $personId $time_sql
            UNION SELECT * FROM `foreup_teetime` WHERE person_id_2 = $personId $time_sql
            UNION SELECT * FROM `foreup_teetime` WHERE person_id_3 = $personId $time_sql
            UNION SELECT * FROM `foreup_teetime` WHERE person_id_4 = $personId $time_sql
            UNION SELECT * FROM `foreup_teetime` WHERE person_id_5 = $personId $time_sql) AS time
        LEFT JOIN (SELECT course.name, course.course_id, ts.teesheet_id, ts.title
            FROM `foreup_teesheet` AS ts
	        LEFT JOIN `foreup_courses` AS course
	        ON `course`.`course_id` = `ts`.`course_id`
            WHERE {$courses_string} deleted = 0) AS sheet ON sheet.teesheet_id = time.teesheet_id
		WHERE {$courses_string_2} time.status != 'deleted'
        ORDER BY time.start ASC");

		$teetimes = $result->result_array();

		$lastId = '';
		// Loop through tee times
		foreach ($teetimes as $key => $teetime) {
			$players = array();

			// Filter out the back nine reservation when the tee time is for 18 holes
			if ($teetime['holes'] == '18' && substr($teetime['TTID'], 20) == 'b') {
				unset($teetimes[$key]);
				continue;
			}

			// Organize people into a sub array on each tee time
			if (!empty($teetime['person_id'])) {
				$players[] = array('person_id' => $teetime['person_id'], 'person_name' => $teetime['person_name']);
			}
			if (!empty($teetime['person_id_2'])) {
				$players[] = array('person_id' => $teetime['person_id_2'], 'person_name' => $teetime['person_name_2']);
			}
			if (!empty($teetime['person_id_3'])) {
				$players[] = array('person_id' => $teetime['person_id_3'], 'person_name' => $teetime['person_name_3']);
			}
			if (!empty($teetime['person_id_4'])) {
				$players[] = array('person_id' => $teetime['person_id_4'], 'person_name' => $teetime['person_name_4']);
			}
			if (!empty($teetime['person_id_5'])) {
				$players[] = array('person_id' => $teetime['person_id_5'], 'person_name' => $teetime['person_name_5']);
			}
			$teetimes[$key]['player_list'] = $players;

			// Make unix timestamp from teetime start and end fields
			$start = $teetime['start'] + 1000000;
			$start = new DateTime($start);
			$teetimes[$key]['time'] = $start->format('Y-m-d H:i');

			$lastId = $teetime['TTID'];

			$teetimes[$key]['purchased'] = false;
			if($teetime['paid_player_count'] > 0){
				$teetimes[$key]['purchased'] = true;
			}
		}

		return array_values($teetimes);
	}

	/*
		Retrieves all customer billing for user
	*/
	function get_customer_billing($personId, $courseId = null)
	{

		if (empty($personId)) {
			return false;
		}

		$this->db->select('course.name AS course_name, course.course_id, bill.billing_id, bill.title,
			bill.start_date, bill.pay_account_balance, bill.pay_member_balance, bill.email_invoice,
			bill.total, bill.description, bill.frequency');
		$this->db->from('customer_billing AS bill');
		$this->db->join('courses AS course', 'course.course_id = bill.course_id', 'inner');
		$this->db->where('course.active', 1);

		if (!empty($courseId)) {
			$this->db->where('bill.course_id', $courseId);
		}

		$this->db->order_by("course.name ASC");
		$customer_accounts = $this->db->get();

		return $customer_accounts->result_array();
	}

	/*
		Retrieves all user's giftcards
	*/
	function get_giftcards($personId, $courseId = null)
	{

		if (empty($personId)) {
			return false;
		}

		$this->db->select('gc.giftcard_id, gc.course_id, gc.giftcard_number, gc.value,
			gc.details, gc.expiration_date');
		$this->db->from('foreup_giftcards AS gc');
		$this->db->where('gc.deleted', 0);
		$this->db->where('gc.customer_id', $personId);

		if (!empty($courseId)) {
			$this->db->where('gc.course_id', $courseId);
		}

		$this->db->order_by("gc.expiration_date ASC");
		$giftcards = $this->db->get();

		return $giftcards->result_array();
	}

	/*
		Retrieves all purchases made by user
	*/
	function get_purchases($personId, $courseId = null)
	{
		if (empty($personId)) {
			return false;
		}

		$this->load->model('sale');
		$this->load->model('reports/detailed_sales');

		$this->sale->create_sales_items_temp_table(array('customer_id' => $personId));
		$this->detailed_sales->params = array('department' => 'all', 'terminal' => 'all', 'hide_invoice_sales' => true);
		$sales = $this->detailed_sales->getData();

		return $sales;
	}

	/*
	 * Retrives a customer's member account history
	 */
	function get_account_transactions($personId, $courseId = null)
	{
		if (empty($personId)) {
			return false;
		}

		$this->load->model('Account_transactions');
		$transactions = $this->Account_transactions->get_transactions(
			'member',
			$personId,
			array(
				'hide_balance_transfers' => true,
				'limit' => 25),
			$courseId
		);

		$this->total_transactions = $this->Account_transactions->number_records();

		return $transactions;
	}

	function get_minimum_charge_transactions($person_id, $course_id){
		$course_ids = [];
		$this->load->model('course');
		$this->course->get_linked_course_ids($course_ids);
		$minimum_charges = $this->db->select('minimum_charge_id, date_added')
			->from('customers')
			->join('customer_minimum_charges', 'customer_minimum_charges.person_id = customers.person_id', 'inner')
			->where('customers.person_id', $person_id)
			->where_in('customers.course_id', $course_ids)
			->get()->result_array();

		if(empty($minimum_charges)){
			return false;
		}

		$this->load->model('Minimum_charge');
		$transactions = [];
		foreach($minimum_charges as $charge){
			$items = $this->Minimum_charge->get_items_purchased($person_id, $charge['minimum_charge_id'], $course_id, $charge['date_added']);
			if(!empty($items)){
				$transactions += $items;
			}
		}
		
		return $transactions;
	}

	function get_minimum_charge_totals($person_id, $course_id){
		$course_ids = [];
		$this->load->model('course');
		$this->course->get_linked_course_ids($course_ids);
		$minimum_charges = $this->db->select('minimum_charge_id')
			->from('customers')
			->join('customer_minimum_charges', 'customer_minimum_charges.person_id = customers.person_id', 'inner')
			->where('customers.person_id', $person_id)
			->where_in('customers.course_id', $course_ids)
			->get()->result_array();

		if(empty($minimum_charges)){
			return false;
		}

		$charge_ids = [];
		foreach($minimum_charges as $charge){
			$charge_ids[] = (int) $charge['minimum_charge_id'];
		}

		$this->load->model('v2/Customer_model');
		$spending_totals = $this->Customer_model->get_minimum_spending([$person_id], $charge_ids, $course_id);

		if(empty($spending_totals[$person_id])){
			return [];
		}
		return $spending_totals[$person_id];
	}

	function get_username($personId)
	{

		$this->db->select('email AS username');
		$this->db->from('users');
		$this->db->where('person_id', $personId);
		$row = $this->db->get()->row_array();

		if ($row) {
			return $row['username'];
		} else {
			return false;
		}
	}

	function has_booking_class_access($person_id, $booking_class_id)
	{
		$pass_item_ids = [];
		$groups = [];

		// Get pass item IDs asscoiated with booking class
		$this->db->select('booking_class_id, pass_item_ids');
		$this->db->from('booking_classes');
		$this->db->where('booking_class_id', $booking_class_id);
		$rows = $this->db->get()->result_array();

		foreach($rows as $row){
			if(!empty($row['pass_item_ids'])){
				$pass_item_ids += explode(',', $row['pass_item_ids']);
			}
		}
		$pass_item_ids = array_unique($pass_item_ids);

		// First check that the booking class has at least one group or pass item associated
		// If no groups or pass items associated, allow anyone to book
		$this->db->select('booking_class_id');
		$this->db->from('booking_class_groups');
		$this->db->where('booking_class_id', $booking_class_id);
		$groups = $this->db->get()->result_array();
		
		if(empty($groups) && empty($pass_item_ids)){
			return true;
		}

		// Check if the person belongs to a valid group
		$this->db->from('customer_group_members');
		$this->db->join('booking_class_groups', "foreup_customer_group_members.group_id = foreup_booking_class_groups.group_id");
		$this->db->where('booking_class_id', $booking_class_id);
		$this->db->where('customer_group_members.person_id', $person_id);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1){
			return true;
		}

		$this->load->model('Pass');
		$passes = $this->Pass->get(['customer_id' => $person_id]);

		if(empty($passes)){
			return false;
		}

		// Check if the person has a valid pass
		foreach($passes as $pass){
			if(in_array($pass['item_id'], $pass_item_ids) && $pass['is_valid']){
				return true;
			}
		}

		return false;
	}
}