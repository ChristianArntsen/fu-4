<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Model
{
	/**
	 * checks if a user has logged in
	 * @return Boolean
	 */
	public function is_logged_in()
	{
		return $this->session->userdata('user_id') != false;
	}

	public function login($username, $password)
	{
		$user_password = md5($password);

		$this->db->from('users');
		$this->db->where("email = '$username' AND password = '" . $user_password . "' AND deleted = 0");

		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			// found record
			// set user data
			$row = $query->row();
			$this->session->set_userdata('user_id', $row->id);

			return true;
		} else {
			// no data found
			return false;
		}
	}

	public function logout()
	{
		$this->session->set_userdata('user_id', false);
		$this->session->set_userdata('login_mode', false);
		redirect('login');
	}
}