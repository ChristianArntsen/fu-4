<?php
class Table extends CI_Model
{
	function __construct(){
		parent::__construct();
		$this->db->query("SET SESSION group_concat_max_len = 512000");
		$this->load->library('v2/cart_lib');
	}

	function get_all($order_by_table_id = false)
	{
		$this->db->from('tables');
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		
		if ($order_by_table_id)
			$this->db->order_by('table_id');
		else
			$this->db->order_by('sale_id');
		return $this->db->get();
	}

	public function get_info($sale_id)
	{
		$this->db->select('tables.*, COUNT(foreup_table_tickets.ticket_id) AS orders_made');
		$this->db->from('tables');
		$this->db->join('table_tickets', 'table_tickets.sale_id = tables.sale_id', 'left');
		$this->db->where('tables.sale_id', $sale_id);
		$this->db->where('tables.course_id', $this->session->userdata('course_id'));
		$this->db->group_by('tables.sale_id');
		
		return $this->db->get();
	}

	public function get_customers($sale_id, $customer_id = null){
		
		if(empty($sale_id)){
			return false;
		}

		if(empty($customer_id)){
			$this->db->select('customer_id')
				->from('table_customers')
				->where('sale_id', (int) $sale_id);

			$rows = $this->db->get()->result_array();
			$customer_id = [];
			foreach($rows as $row){
				$customer_id[] = (int) $row['customer_id'];
			}
		}

		if(!is_array($customer_id)){
			$customer_id = [$customer_id];
		}

		$this->load->model('v2/Customer_model');
		
		if(empty($customer_id)){
			return [];
		}
		$customers = $this->Customer_model->get(['person_id' => $customer_id]);
		return $customers;
	}

	// remove customer from a sale
	public function delete_customer($sale_id, $customer_id = false){
		if(empty($sale_id)){
			return false;
		}
		$params['sale_id'] = (int) $sale_id;

		if(!empty($customer_id)){
			$params['customer_id'] = (int) $customer_id;
		}
		return $this->db->delete('table_customers', $params);
	}

	// add customer to a sale
	public function save_customer($sale_id, $customer_id){

		if(empty($sale_id) || empty($customer_id)){
			return false;
		}
		$sale_id = (int) $sale_id;
		$customer_id = (int) $customer_id;

		return $this->db->query("INSERT IGNORE INTO foreup_table_customers
			(sale_id, customer_id, date_added)
			 VALUES ({$sale_id}, {$customer_id}, NOW())");
	}

	function get_payment($sale_id, $type){

		if(empty($sale_id) || empty($type)){
			return false;
		}

		$this->db->select('*');
		$this->db->from('sales_payments');
		$this->db->where('sale_id', (int) $sale_id);
		$this->db->where('payment_type', $type);
		$query = $this->db->get();

		return $query->row_array();
	}

	function get_recent_transactions($employee_id = null, $limit = 100) {
		$ul = $this->session->userdata('user_level')*1;
		$pid = $this->session->userdata('person_id')*1;
		$member_account_name = lang('customers_member_account_balance');
		if($this->config->item('member_balance_nickname')){
			$member_account_name = $this->config->item('member_balance_nickname');
		}

		$customer_account_name = lang('customers_account_balance');
		if($this->config->item('customer_credit_nickname')){
			$customer_account_name = $this->config->item('customer_credit_nickname');
		}
		
		$this->db->select("sales.sale_id AS sale_id, sales.customer_id,
			CONCAT(customer.first_name,' ',customer.last_name) AS customer_name,
			customer.first_name, customer.last_name, sales.table_id AS table_number, 
			sales.sale_time, sales.employee_id,
			CONCAT(employee.first_name,' ',employee.last_name) AS employee_name,
			GROUP_CONCAT(
				DISTINCT
				CONCAT(payment.payment_type,':::',
					payment.payment_amount,':::',
					payment.invoice_id,':::',
					payment.tip_recipient
				)
			SEPARATOR '||') AS payments", false);
		$this->db->from('sales');
		$this->db->join('sales_payments AS payment', 'payment.sale_id = sales.sale_id', 'left');
		$this->db->join('people AS customer', 'customer.person_id = sales.customer_id', 'left');
		$this->db->join('people AS employee', 'employee.person_id = sales.employee_id', 'left');
		$this->db->where('sales.course_id', $this->session->userdata('course_id'));
		$this->db->where('sales.sale_time >',  date('Y-m-d 00:00:00'));
		if($ul < 3)$this->db->where('employee.person_id',$pid);
		$this->db->where("sales.table_id != '0'");
		$this->db->where("sales.table_id != ''");
		if(!empty($employee_id)){
			$this->db->where('sales.employee_id', (int) $employee_id);
		}
		$this->db->limit($limit);
		$this->db->group_by('sales.sale_id');
		$this->db->order_by('sales.sale_time DESC');
		$rows = $this->db->get()->result_array();
		
		$sales = array();

		foreach($rows as $row){
			$sale = array();
			$payments = $row['payments'];
			unset($row['payments']);

			$sale = $row;
			$sale['tips'] = array();
			$sale['payments'] = array();

			if(!empty($payments)){
				$payments = explode('||', $payments);
				
				foreach($payments as $payment){
					$paymentArray = explode(':::', $payment);
					
					// Get payment type key
					$paymentType = trim($paymentArray[0]);
					if(stripos($paymentType, 'cash') !== false){
						$type = 'cash';
					}else if(stripos($paymentType, 'gift') !== false){
						$type = 'gift_card';
					}else if(stripos($paymentType, 'check') !== false){
						$type = 'check';
					}else if(stripos($paymentType, $member_account_name) !== false){
						$type = 'member_balance';
					}else if(stripos($paymentType, $customer_account_name) !== false){
						$type = 'customer_balance';
					}else{
						$type = 'credit_card';
					}	
									
					$pay = array(
						'type' => $type,
						'payment_type' => $paymentType,
						'amount' => (float) $paymentArray[1],
						'invoice_id' => (int) $paymentArray[2],
						'tip_recipient' => (int) $paymentArray[3],
						'tip_recipient_name' => ''
					);
					
					// If payment is a tip with a recipient, get recipient details
					if($pay['tip_recipient'] != 0){
						$this->db->select("CONCAT(first_name,' ',last_name) AS name", false);
						$query = $this->db->get_where('people', array('person_id' => $pay['tip_recipient']));
						$name_result = $query->row_array();
						$pay['tip_recipient_name'] = $name_result['name'];
					}
					
					// If payment is a tip
					if(stripos($pay['payment_type'], 'tip') !== false){
						if(stripos($pay['type'], 'member') !== false || stripos($pay['type'], 'customer') !== false){
							$pay['customer_id'] = $sale['customer_id'];
						}
						$sale['tips'][] = $pay;
					}else{
						$sale['payments'][] = $pay;
					}
					$sale['taxes'] = array();
				}
			}

			$sale['subtotal'] = 0;
			$sale['total'] = 0;
			$sale['tax'] = 0;
			$sale['quantity'] = 0;

			// Get items associated with sale
			$this->db->select('si.line, si.item_id, si.item_unit_price AS price, si.is_side,
				si.discount_percent AS discount, si.subtotal, si.tax, si.total,
				si.quantity_purchased AS quantity, i.name, si.num_splits');
			$this->db->from('sales_items AS si');
			$this->db->join('items AS i', 'i.item_id = si.item_id');
			$this->db->where('sale_id', $sale['sale_id']);
			$this->db->order_by('si.line');
			$items = $this->db->get()->result_array();

			foreach($items as $item){
				$sale['subtotal'] += $item['subtotal'];
				$sale['tax'] += $item['tax'];
				$sale['total'] += $item['total'];
				$sale['quantity'] += $item['quantity'];
				$item['taxes'] = array();
			}

			$sale['items'] = $items;
			$sales[] = $sale;
		}

		return $sales;
	}

	function exists($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id',$sale_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows() == 1);
	}

	function get_id_by_table_number($table_number)
	{
		$this->db->from('tables');
		$this->db->where('table_id', $table_number);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		//$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->row_array();

		if ($query->num_rows()==1)
			return $result['sale_id'];
		else
			return false;
	}

	function get_item($line, $sale_id){
		$line = (int) $line;

		$this->db->select('ti.item_id, ti.sale_id, ti.comments, ti.line, ti.seat,
			ti.quantity_purchased AS quantity, ti.item_unit_price AS price,
			ti.is_ordered, ti.is_paid, ti.total_splits, i.force_tax');
		$this->db->from('table_items as ti');
        $this->db->join('items AS i', 'ti.item_id = i.item_id', 'left');
		$this->db->where('sale_id', (int) $sale_id);
		$this->db->where('line', (int) $line);

		return $this->db->get()->row_array();
	}

	function update($sale_data, $sale_id)
	{
		$this->db->where('sale_id', $sale_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$success = $this->db->update('tables',$sale_data);

		return $success;
	}

	function save($items, $customer_id, $employee_id, $comment, $payments, $sale_id = false, $table_id = false)
	{
		if (!$sale_id || $sale_id == -1)
		{
			$sale_id = $this->get_id_by_table_number($table_id);
		}

		$payment_types='';
		foreach($payments as $payment_id=>$payment)
		{
			$payment_types=$payment_types.$payment['payment_type'].': '.to_currency($payment['payment_amount']).'<br />';
		}

		$sales_data = array(
			'sale_time' => date('Y-m-d H:i:s'),
			'customer_id'=> $this->Customer->exists($customer_id) ? $customer_id : null,
			'employee_id'=>$employee_id,
			'manager_auth_id' => 0,
			'payment_type'=>$payment_types,
			'comment'=>$comment,
			'deleted' => 0,
			'course_id'=>$this->session->userdata('course_id'),
			'table_id'=>$table_id
		);

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		if ($sale_id)
		{
			$this->db->delete('table_payments', array('sale_id' => $sale_id));
			//$this->db->delete('table_items_taxes', array('sale_id' => $sale_id)); // does not exist
			$this->db->delete('table_items', array('sale_id' => $sale_id));
			$this->db->delete('table_item_kits_taxes', array('sale_id' => $sale_id));
			$this->db->delete('table_item_kits', array('sale_id' => $sale_id));
			$this->db->delete('table_items_modifiers', array('sale_id' => $sale_id));
			$this->db->delete('table_customers', array('sale_id' => $sale_id));
			$this->db->delete('table_comps', array('sale_id' => $sale_id));

			$this->db->where('sale_id', $sale_id);
			$this->db->update('tables', $sales_data);
		}
		else
		{
			$this->db->insert('tables', $sales_data);
			$sale_id = $this->db->insert_id();
		}
		//echo $this->db->last_query();
		foreach($payments as $payment_id=>$payment)
		{
			$sales_payments_data = array
			(
				'sale_id'=>$sale_id,
				'payment_type'=>$payment['payment_type'],
				'payment_amount'=>$payment['payment_amount']
			);
			$this->db->insert('table_payments',$sales_payments_data);
		}

		foreach($items as $line=>$item)
		{
			if (isset($item['item_id']))
			{
				$cur_item_info = $this->Item->get_info($item['item_id']);

				$sales_items_data = array
				(
					'sale_id'=>$sale_id,
					'item_id'=>$item['item_id'],
					'line'=>$item['line'],
					'description'=>$item['description'],
					'serialnumber'=>$item['serialnumber'],
					'quantity_purchased'=>$item['quantity'],
					'discount_percent'=>$item['discount'],
					'item_cost_price' => $cur_item_info->cost_price,
					'item_unit_price'=>$item['price'] + $item['modifier_total']
					);

				$this->db->insert('table_items',$sales_items_data);

				// Insert any modifiers
				if(!empty($item['modifiers'])){
					foreach($item['modifiers'] as $key => $modifier){
						$modifierData = array();
						$modifierData['sale_id'] = $sale_id;
						$modifierData['item_id'] = $item['item_id'];
						$modifierData['line'] = $item['line'];
						$modifierData['modifier_id'] = $modifier['modifier_id'];
						$modifierData['selected_option'] = $modifier['selected_option'];

						if(empty($modifier['selected_price'])){
							$modifier['selected_price'] = '0.00';
						}

						$modifierData['selected_price'] = $modifier['selected_price'];

						$this->db->insert('table_items_modifiers', $modifierData);
					}
				}
			}
			else
			{
				$cur_item_kit_info = $this->Item_kit->get_info($item['item_kit_id']);

				$sales_item_kits_data = array
				(
					'sale_id'=>$sale_id,
					'item_kit_id'=>$item['item_kit_id'],
					'line'=>$item['line'],
					'description'=>$item['description'],
					'quantity_purchased'=>$item['quantity'],
					'discount_percent'=>$item['discount'],
					'item_kit_cost_price' => $cur_item_kit_info->cost_price,
					'item_kit_unit_price'=>$item['price']
				);

				$this->db->insert('table_item_kits',$sales_item_kits_data);
			}

			$customer = $this->Customer->get_info($customer_id);
 			if ($customer_id == -1 or $customer->taxable)
 			{
				if (isset($item['item_id']))
				{
					foreach($this->Item_taxes->get_info($item['item_id']) as $row)
					{
						$this->db->insert('table_items_taxes', array(
							'sale_id' 	=>$sale_id,
							'item_id' 	=>$item['item_id'],
							'line'      =>$item['line'],
							'name'		=>$row['name'],
							'percent' 	=>$row['percent'],
							'cumulative'=>$row['cumulative']
						));
					}
				}
				else
				{
					foreach($this->Item_kit_taxes->get_info($item['item_kit_id']) as $row)
					{
						$this->db->insert('table_item_kits_taxes', array(
							'sale_id' 		=>$sale_id,
							'item_kit_id'	=>$item['item_kit_id'],
							'line'      	=>$item['line'],
							'name'			=>$row['name'],
							'percent' 		=>$row['percent'],
							'cumulative'	=>$row['cumulative']
						));
					}
				}
			}
		}
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return -1;
		}

		return $sale_id;
	}
	
	function update_manager_auth($sale_id, $manager_id){
		$this->db->update('tables', array('manager_auth_id' => $manager_id), array('sale_id'=>$sale_id));
		return $this->db->affected_rows();
	}
	
	function delete($sale_id)
	{
        $ticket_id = array();
		$this->db->from('table_tickets');
		$this->db->where('sale_id', $sale_id);
		//$this->db->limit(1);
		$query = $this->db->get();
		//$result = $query->row_array();


		for($i = 0;$i<$query->num_rows();$i++) {
			$result = $query->row_array($i);
			$ticket_id[] = $result['ticket_id'];
		}

		// Run these queries as a transaction, we want to make sure we do all or nothing

		$this->db->trans_start();

		$this->db->delete('table_payments', array('sale_id' => $sale_id));
		$this->db->delete('table_items_modifiers', array('sale_id' => $sale_id));
		$this->db->delete('table_item_side_modifiers', array('sale_id' => $sale_id));
		$this->db->delete('table_item_sides', array('sale_id' => $sale_id));
		$this->db->delete('table_item_kits_taxes', array('sale_id' => $sale_id));
		$this->db->delete('table_item_kits', array('sale_id' => $sale_id));
		$this->db->delete('table_items', array('sale_id' => $sale_id));

		foreach($ticket_id as $id) {
			$this->db->delete('table_ticket_item_kits', array('ticket_id' => $id));
			$this->db->delete('table_ticket_items', array('ticket_id' => $id));
			$this->db->delete('table_tickets', array('ticket_id' => $id));
		}


		$this->db->delete('table_customers', array('sale_id' => $sale_id));
		$this->db->delete('table_comps', array('sale_id' => $sale_id));
		$this->db->delete('table_receipt_items', array('sale_id' => $sale_id));
		$this->db->delete('table_receipts', array('sale_id' => $sale_id));
		$this->db->delete('tables', array('sale_id' => $sale_id));

		$this->db->trans_complete();
		return $this->db->trans_status();
	}

	function get_sale_items($sale_id)
	{
		$this->db->from('table_items');
		$this->db->where('sale_id', $sale_id);
		return $this->db->get();
	}

	function get_sale_item_modifiers($sale_id){

		if(empty($sale_id)){
			return false;
		}

		$this->db->select('m.modifier_id, m.name, s.selected_price, m.default_price AS price,
			m.options, s.line, s.item_id, s.selected_option AS selected_option', false);
		$this->db->from('modifiers AS m');
		$this->db->join('table_items_modifiers AS s', 's.modifier_id = m.modifier_id', 'inner');
		$this->db->where(array('s.sale_id' => $sale_id));
		$this->db->order_by('s.line');

		$query = $this->db->get();
		$modifiers = $query->result_array();
		$rows = array();

		// Loop through rows and decode JSON options into array
		foreach($modifiers as $key => $row){

			if(!empty($row['options'])){
				$row['options'] = json_decode($row['options'], true);
				$row['options'][] = 'no';
			}
			// If no options set, just default to 'yes' or 'no'
			if(empty($row['options']) || empty($row['options'][0])){
				$row['options'] = array('yes','no');
			}

			$rows[$row['line']][$row['modifier_id']] = $row;
		}

		return $rows;
	}

	function get_sale_total($sale_id)
	{
		$items = $this->get_sale_items($sale_id)->result_array();
		$item_kits = $this->get_sale_item_kits($sale_id)->result_array();

		$total = 0;
		foreach ($items as $item)
			$total += $item['quantity_purchased'] * $item['item_unit_price'] * (100 - $item['discount_percent']) / 100;
		foreach ($item_kits as $item_kit)
			$total += $item_kit['quantity_purchased'] * $item_kit['item_kit_unit_price'] * (100 - $item_kit['discount_percent']) / 100;
		return $total;
	}

	function get_sale_item_kits($sale_id)
	{
		$this->db->from('table_item_kits');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_sale_payments($sale_id)
	{
		$this->db->from('table_payments');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_customer($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id',$sale_id);
		return $this->Customer->get_info($this->db->get()->row()->customer_id);
	}

	function get_comment($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get()->row()->comment;
	}

	function apply_side_type(&$sides, $type){
		if(!isset($sides) || empty($sides)){
			return array();
		}

		$sidesWithType = array();
		foreach($sides as $key => $side){
			$side['type'] = $type;
			$sidesWithType[] = $side;
		}

		return $sidesWithType;
	}

	function has_sufficient_inventory($item_id){

		// Check if item has sufficient inventory
		$this->db->select("quantity, is_unlimited, reorder_level");
		$this->db->from('items');
		$this->db->where('item_id', (int) $item_id);
		$data = $this->db->get()->row_array();

		if($data['quantity'] <= 0 && $data['is_unlimited'] == 0){
			return false;
		}

		return array('quantity' => (int) $data['quantity'], 'reorder_level' => (int) $data['reorder_level']);
	}

	// Insert an item part of table sale
	function save_item($sale_id, $line, $item){

		// Delete the item record
		$this->db->trans_start();
		$deleteData = array('sale_id'=>(int) $sale_id, 'line'=>$line);
		$this->db->query("SET foreign_key_checks = 0");
		$this->db->delete('table_items_modifiers', $deleteData);
		$this->delete_item_side($sale_id, $line);
		$this->db->delete('table_items', $deleteData);
		$this->db->delete('table_comps', $deleteData);
		$this->db->query("SET foreign_key_checks = 1");

		// Gather up all side types and place them into a single array
		// with type of side defined as a column
		$sides = array_merge(
			$this->apply_side_type($item['soups'], 'soups'),
			$this->apply_side_type($item['salads'], 'salads'),
			$this->apply_side_type($item['sides'], 'sides')
		);

		$sales_items_data = array(
			'sale_id' =>$sale_id,
			'item_id' => $item['item_id'],
			'line' => $item['line'],
			'seat' => $item['seat'],
			'comments' => $item['comments'],
			'is_ordered' => $item['is_ordered'],
			'quantity_purchased' => $item['quantity'],
			'discount_percent' => $item['discount'],
			'item_unit_price' => $item['price'],
			'meal_course_id' => (int) $item['meal_course_id']
		);

		// Insert item into table
		$success = $this->db->insert('table_items', $sales_items_data);
		$this->db->trans_complete();
		
		// Insert any modifiers
		if(!empty($item['modifiers'])){
			
			$modifierInsertSql = "INSERT IGNORE INTO foreup_table_items_modifiers (`sale_id`, `line`, `modifier_id`, `option`) VALUES ";
			
			foreach($item['modifiers'] as $key => $modifier){
				
				if(is_array($modifier['selected_option'])){
					$modifier['selected_option'] = json_encode($modifier['selected_option']);
				}			
				
				$modifierData = array();
				$modifierData['sale_id'] = (int) $sale_id;
				$modifierData['line'] = (int) $item['line'];
				$modifierData['modifier_id'] = (int) $modifier['modifier_id'];
				
				if(empty($modifier['selected_option'])){
					$modifierData['option'] = 'NULL';
				}else{
					$modifierData['option'] = $this->db->escape($modifier['selected_option']);
				}
				
				$modifierInsertSql .= "({$modifierData['sale_id']}, {$modifierData['line']}, {$modifierData['modifier_id']}, {$modifierData['option']}),";
			}
			$modifierInsertSql = trim($modifierInsertSql, ',');
			$this->db->query($modifierInsertSql);
		}
		$this->db->trans_complete();
		
		if(!empty($item['comp']) && isset($item['comp']['amount'])){	
			$this->load->model('Item_comp');
			$this->Item_comp->save(
				$sale_id, 
				$line, 
				$item['item_id'],
				$item['comp']['type'],
				$item['comp']['description'],
				$item['comp']['amount'],
				$item['comp']['employee_id']
			);
		}		
		
		// Attach any sides to item
		if(!empty($sides)){
			foreach($sides as $side){
				$this->save_item_side($sale_id, $item['line'], $side);
			}
		}

		return $success;
	}

	// Adds a side to a line item
	function save_item_side($sale_id, $line, $side){

		$this->delete_item_side((int) $sale_id, (int) $line, $side['type'], (int) $side['position']);
		$side_data = array(
			'sale_id' => (int) $sale_id,
			'item_id' => (int) $side['item_id'],
			'cart_line' => $line,
			'position' => (int) $side['position'],
			'type' => $side['type'],
			'date_created' => date('Y-m-d h:i:s')
		);

		// Insert item into table
		$success = $this->db->insert('table_item_sides', $side_data);

		// Insert any side modifiers
		if(!empty($side['modifiers'])){

			foreach($side['modifiers'] as $key => $modifier){
				
				if(is_array($modifier['selected_option'])){
					$modifier['selected_option'] = json_encode($modifier['selected_option']);
				}

				if(empty($modifier['selected_option'])){
					$modififier['selected_option'] = null;
				}
								
				$modifierData = array();
				$modifierData['sale_id'] = $sale_id;
				$modifierData['line'] = $line;
				$modifierData['modifier_id'] = $modifier['modifier_id'];
				$modifierData['option'] = $modifier['selected_option'];
				$modifierData['side_position'] = (int) $side['position'];
				$modifierData['side_type'] = $side['type'];

				$this->db->insert('table_item_side_modifiers', $modifierData);
			}
		}

		return $success;
	}

	// Removes a line item from cart and any receipts, including
	// associated sides and modifiers
	function delete_item($sale_id, $line = false){

		$keyData = array('sale_id' => (int) $sale_id, 'line' => (int) $line);

		$this->db->query("SET foreign_key_checks = 0");
		$this->db->delete('table_items_modifiers', $keyData);
		$this->db->delete('table_receipt_items', $keyData);
		$this->delete_item_side($sale_id, $line);
		$success = $this->db->delete('table_items', $keyData);
		$this->db->query("SET foreign_key_checks = 1");

		return $success;
	}

	// Removes a side from an item, including modifiers of that side
	// If 'type' or 'position' is not supplied, it will delete ALL sides
	// for that specific item
	function delete_item_side($sale_id, $line, $type = null, $position = null){

		$keyData = array(
			'sale_id' => (int) $sale_id,
			'cart_line' => (int) $line
		);
		$modifierKeyData = array(
			'sale_id' => (int) $sale_id,
			'line' => (int) $line
		);

		if($type !== null){
			$keyData['type'] = $type;
			$modifierKeyData['side_type'] = $type;
		}
		if($position !== null){
			$keyData['position'] = (int) $position;
			$modifierKeyData['side_position'] = (int) $position;
		}

		$this->db->query("SET foreign_key_checks = 0");
		$this->db->delete('table_item_side_modifiers', $modifierKeyData);
		$success = $this->db->delete('table_item_sides', $keyData);
		$this->db->query("SET foreign_key_checks = 1");

		return $success;
	}

	// Retrieve all items associated with a specific table
	function get_items($sale_id = false, $lines = null){

		$this->load->model('modifier');
		$this->load->model('Printer');
		$this->load->model('v2/Printer_group_model');
		$this->load->model('v2/Item_model');
		$cart = array();

		if(empty($sale_id)){
			return $cart;
		}
		
		// Get list of item taxes for this table
		$this->db->select('tax.item_id, tax.percent, tax.cumulative, tax.name');
		$this->db->from('items_taxes AS tax');
		$this->db->join('table_items AS table_item', 'table_item.item_id = tax.item_id', 'inner');
		$this->db->where('table_item.sale_id', (int) $sale_id);
		$this->db->order_by('tax.item_id, tax.cumulative ASC');
		$this->db->group_by('tax.item_id, tax.name, tax.percent');
		$result = $this->db->get();
		$rows = $result->result_array();
		$taxes = array();

		// Organize array of taxes by line number
		foreach($rows as $row){
			$taxes[$row['item_id']][] = array(
				'percent' => $row['percent'],
				'name' => $row['name'],
				'cumulative' => $row['cumulative']
			);
		}
		unset($rows);

		// Get list of items that have been sent to kitchen (ordered)
		$this->db->select('ticket_item.line');
		$this->db->from('table_tickets AS ticket');
		$this->db->join('table_ticket_items AS ticket_item', 'ticket.ticket_id = ticket_item.ticket_id', 'inner');
		$this->db->where('ticket.sale_id', $sale_id);
		$this->db->group_by('ticket_item.line');
		$this->db->order_by('ticket_item.line ASC');
		$result = $this->db->get();
		$rows = $result->result_array();
		$items_ordered = array();

		// Organize array of items ordered by line number
		foreach($rows as $row){
			$items_ordered[(int) $row['line']]= (int) $row['line'];
		}
		unset($rows);

		if(!empty($lines) && !is_array($lines)){
			$lines = array($lines);
		}

		// Select all food and beverage items with associated modifiers
		// and sides
		$this->db->select("cart_item.line, cart_item.item_id, i.course_id,
			i.name, i.department, i.category, i.subcategory, i.supplier_id,
			i.item_number, i.max_discount, i.number_of_sides AS number_sides,
			0 AS number_salads, 0 AS number_soups, i.print_priority, i.do_not_print,
			i.quantity AS inventory_level, i.is_unlimited AS inventory_unlimited,
			i.kitchen_printer AS printer_ip, i.unit_price AS base_price, i.prompt_meal_course,
			i.do_not_print_customer_receipt, i.cost_price, i.unit_price_includes_tax,
			i.force_tax,
			
			cart_item.discount_percent AS discount, cart_item.comments,
			cart_item.is_paid, cart_item.is_ordered,
			cart_item.item_unit_price AS price, cart_item.seat,
			cart_item.quantity_purchased AS quantity, i.is_side,
			cart_item.meal_course_id,

			modifier.modifier_id,
			modifier.name AS modifier_name,
			modifier.default_price AS modifier_default_price,
			modifier.options AS modifier_options,
			modifier.category_id AS modifier_category_id,
			modifier.required AS modifier_required,
			item_modifier.option AS modifier_selected_option,
			item_default_modifier.default AS modifier_default,
			modifier.multi_select AS modifier_multi_select,
			item_default_modifier.order AS modifier_order,

			GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				customer_group.group_id,
				customer_group.label,
				customer_group.item_cost_plus_percent_discount
			) SEPARATOR 0x1E) AS customer_groups,
			
			GROUP_CONCAT(DISTINCT item_printer_group.printer_group_id) AS printer_groups,
			
			comp.amount AS comp_amount, comp.type AS comp_type, comp.description AS comp_description, comp.employee_id AS comp_employee,

			GROUP_CONCAT(DISTINCT CONCAT(side.side_id,'^:^',side.not_available,'^:^',side.upgrade_price)) AS side_data", false);
		$this->db->from('table_items AS cart_item');
		$this->db->join('items AS i', 'i.item_id = cart_item.item_id', 'left');
		$this->db->join('item_sides AS side', 'side.item_id = i.item_id', 'left');
		$this->db->join('item_printer_groups AS item_printer_group', 'item_printer_group.item_id = i.item_id', 'left');		
		$this->db->join('table_items_modifiers AS item_modifier',
			'cart_item.line = item_modifier.line AND item_modifier.sale_id = cart_item.sale_id', 'left');
		$this->db->join('modifiers AS modifier', "modifier.modifier_id = item_modifier.modifier_id AND modifier.deleted = '0'", 'left');
		$this->db->join('item_modifiers AS item_default_modifier', 
			"modifier.modifier_id = item_default_modifier.modifier_id AND item_default_modifier.item_id = i.item_id", 'left');
		$this->db->join('item_customer_groups AS item_customer_group', 'item_customer_group.item_id = i.item_id', 'left');
		$this->db->join('customer_groups AS customer_group', 'customer_group.group_id = item_customer_group.customer_group_id', 'left');
		$this->db->join('table_comps AS comp', 'comp.line = cart_item.line AND comp.sale_id = cart_item.sale_id', 'left'); 
		$this->db->where('cart_item.sale_id', $sale_id);
		$this->db->group_by('cart_item.line, modifier.modifier_id');
		$this->db->order_by('cart_item.line ASC');

		if(!empty($lines)){
			$this->db->where_in('cart_item.line', $lines);
		}

		$result = $this->db->get();
		$rows = $result->result_array();

		// Organize items with modifiers as sub array
		foreach($rows as $key => $row){
			$line = (int) $row['line'];
			$item_id = (int) $row['item_id'];
			$food_item =& $cart[$line];

			if(!isset($cart[$line])){
				$sides = array();
				$food_item = $row;
				$food_item['modifiers'] = array();
				$food_item['customer_groups'] = $this->Item_model->get_customer_group_array($food_item['customer_groups']);

				// Add taxes to item as array list
				$food_item['taxes'] = array();
				if(!empty($taxes[$item_id])){
					$food_item['taxes'] = $taxes[$item_id];
				}

				// Break apart side data into arrays
				$sides = array();
				$sidesArray = explode(',', $food_item['side_data']);
				foreach($sidesArray as $side){
					$sideArray = explode('^:^', $side);
					
					if(!isset($sideArray[0]) || !isset($sideArray[1])){
						continue;
					} 
					
					$sideItemId =  $sideArray[0];
					$sideHidden = $sideArray[1];
					$sidePrice = (float) $sideArray[2];

					if($sideHidden){
						$food_item['hidden_sides'][$sideItemId] = 1;
					}
					$food_item['side_prices'][$sideItemId] = $sidePrice;
				}
				unset($food_item['side_data']);

				$food_item['number_sides'] = (int) $food_item['number_sides'];
				$food_item['number_salads'] = (int) $food_item['number_salads'];
				$food_item['number_soups'] = (int) $food_item['number_soups'];
				$food_item['is_ordered'] = (bool) (int) $food_item['is_ordered'];
				$food_item['is_paid'] = (bool) (int) $food_item['is_paid'];
				$food_item['modifier_total'] = 0;
				$food_item['showed_edit'] = 1; // If set to 0, view will automatically show the edit window for item
				$food_item['print_priority'] = (int) $food_item['print_priority'];
				$food_item['inventory_level'] = (int) $food_item['inventory_level'];
				$food_item['inventory_unlimited'] = (int) $food_item['inventory_unlimited'];

				if($this->session->userdata('multiple_printers') == 1){
					
					$printer_group_ids = explode(',', $food_item['printer_groups']);
					$food_item['printers'] = $this->Printer_group_model->get_item_printers(
						$printer_group_ids, 
						$this->session->userdata('terminal_id'),
						(bool) $food_item['do_not_print']
					);
					$food_item['printer_ip'] = '';						
					
				}else{
					if($food_item['printer_ip'] == '1'){
						$food_item['printer_ip'] = '_hot_'.$this->config->item('webprnt_hot_ip');
					}else if($food_item['printer_ip'] == '2'){
						$food_item['printer_ip'] = '_cold_'.$this->config->item('webprnt_cold_ip');	
					}else{
						$food_item['printer_ip'] = '';
					}	
				}
				
				$comp = false;
				if($food_item['comp_amount'] !== null){
					$comp = array();
					$comp['amount'] = (float) $food_item['comp_amount'];
					$comp['type'] = $food_item['comp_type'];
					$comp['description'] = $food_item['comp_description'];
					$comp['employee_id'] = $food_item['comp_employee'];
				}
				$food_item['comp'] = $comp;	
				
				// Get any sides associated with item
				$sides = $this->get_item_sides($sale_id, $line, null, null, array(
					'quantity' => $food_item['quantity'],
					'discount' => $food_item['discount'],
					'comp' => $food_item['comp']
				));
				$food_item += $sides;			

				unset($food_item['modifier_name'], $food_item['modifier_id'],
					$food_item['modifier_default_price'], $food_item['modifier_options'],
					$food_item['modifier_selected_option'], $food_item['modifier_selected_price'],
					$food_item['comp_amount'], $food_item['comp_type'], $food_item['comp_description'],
					$food_item['comp_employee']);
			}

			// Add modifier(s) to item
			if(!empty($row['modifier_id'])){
				$modifier = array(
					'modifier_id' => $row['modifier_id'],
					'name' => $row['modifier_name'],
					'price' => $row['modifier_default_price'],
					'options' => $row['modifier_options'],
					'selected_option' => $row['modifier_selected_option'],
					'required' => (bool) $row['modifier_required'],
					'category_id' => (int) $row['modifier_category_id'],
					'default' => $row['modifier_default'],
					'multi_select' => $row['modifier_multi_select'],
					'order' => (int) $row['modifier_order']
				);

				$structured_modifier = $this->modifier->structure_options($modifier);
				$cart[$line]['modifiers'][] = $structured_modifier;

				// Add price of modifiers to subtotal of item
				$food_item['modifier_total'] += (float) $structured_modifier['selected_price'];
			}
			
			if(empty($food_item['sides_comp_total'])){
				$food_item['sides_comp_total'] = 0;
			}

			// Calculate tax and final total of item
			$food_item['subtotal'] = $this->cart_lib->calculate_subtotal($food_item['price'] + $food_item['modifier_total'], $food_item['quantity'], $food_item['discount']);
			$food_item['subtotal_no_discount'] = $this->cart_lib->calculate_subtotal($food_item['price'] + $food_item['modifier_total'], $food_item['quantity'], 0);
			$food_item['tax'] = $this->cart_lib->calculate_item_tax($food_item['subtotal'], $food_item['item_id'], 'item');

			if($food_item['unit_price_includes_tax'] == 1){
				$food_item['subtotal'] -= $food_item['tax'];
				$food_item['subtotal_no_discount'] -= $food_item['tax'];
			}

			$food_item['discount_amount'] = $food_item['subtotal_no_discount'] - $food_item['subtotal'];
			$food_item['comp_total'] = $this->get_comp_discount($food_item['comp'], $food_item['unit_price_includes_tax'] == 1?$food_item['subtotal'] + $food_item['tax']:$food_item['subtotal']);
			$food_item['subtotal'] -= $food_item['comp_total'];
			$food_item['comp_total'] += $food_item['sides_comp_total'];

			// Add in total cost of sides
			$food_item['total'] = $food_item['subtotal'] + $food_item['tax'];

            $food_item['service_fees'] = array();
            
            $ServiceFee = new \fu\service_fees\ServiceFee();
            $fees = $ServiceFee->getServiceFeesForItem($item_id);
            foreach ($fees as $fee) {
                $food_item['service_fees'][] = $fee->getAsArray();
            }

			$this->calculate_item_loyalty($food_item);
		}
		
		unset($rows);
		return array_values($cart);
	}
	
	function calculate_item_loyalty(&$item){
		
		$this->Item_model->get_loyalty_points($item);
		$item['loyalty_cost'] = 0;
		$item['loyalty_reward'] = 0;

		if($this->config->item('use_loyalty')){
			if(!empty($item['loyalty_dollars_per_point']) != 0){
				$item['loyalty_cost'] = (float) round($item['total'] / $item['loyalty_dollars_per_point'], 2);		
			}else{
				$item['loyalty_cost'] = 0.0;
			}
			
			if(!empty($item['loyalty_points_per_dollar'])){
				$item['loyalty_reward'] = (float) round($item['subtotal'] * $item['loyalty_points_per_dollar'], 2);
			}else{
				$item['loyalty_reward'] = 0;
			}	
		}

		$item['total_loyalty_points'] = 0;
		$item['total_loyalty_dollars'] = 0;
		if(!empty($item['loyalty_cost'])){
			$item['total_loyalty_points'] = $item['loyalty_cost'];
			$item['total_loyalty_dollars'] = $item['total'];
		}
	}

	function get_comp_discount($comp, $subtotal){
		if(empty($comp) || empty($comp['amount'])){
			return 0;
		}
		
		$amount = $comp['amount'];
		if($comp['type'] == 'percentage'){
			$amount = round($subtotal * ($amount / 100), 2);
		}
		return $amount;
	}	

	// Retrieves all sides for a cart line item
	// Sides include (type): soups, salads, sides
	function get_item_sides($sale_id, $line, $type = null, $positions = null, $inherit = null){
		
		$this->load->model('modifier');
		$this->load->model('Printer');
		$this->load->model('v2/Printer_group_model');
		$sides = array();
		$sides['sides_subtotal'] = 0;
		$sides['sides_tax'] = 0;
		$sides['sides_total'] = 0;

		if(empty($sale_id)){
			return $cart;
		}

		// Get list of item taxes for sides
		$this->db->select('tax.item_id, tax.percent, tax.cumulative, tax.name');
		$this->db->from('items_taxes AS tax');
		$this->db->join('table_item_sides AS side', 'side.item_id = tax.item_id', 'inner');
		$this->db->where('side.sale_id', (int) $sale_id);
		$this->db->where('side.cart_line', (int) $line);
		$this->db->order_by('tax.item_id, tax.cumulative ASC');
		$this->db->group_by('tax.item_id, tax.name, tax.percent');
		$result = $this->db->get();
		$rows = $result->result_array();
		$taxes = array();

		// Organize array of taxes by item id
		foreach($rows as $row){
			$taxes[$row['item_id']][] = array(
				'percent' => $row['percent'],
				'name' => $row['name'],
				'cumulative' => $row['cumulative']
			);
		}
		unset($rows);

		if(!empty($positions) && !is_array($positions)){
			$positions = array($positions);
		}

		// Select all item sides with associated modifiers
		$this->db->select("side.position, side.type, i.item_id,
			i.name, i.department, i.category, i.subcategory,
			i.item_number, i.max_discount, i.is_unlimited,
			IF(ISNULL(item_side.upgrade_price), i.add_on_price,
			item_side.upgrade_price) AS price, i.is_side,
			i.kitchen_printer AS printer_ip, i.print_priority,
			i.do_not_print, i.meal_course_id, i.unit_price_includes_tax,
			i.force_tax,
			
			GROUP_CONCAT(DISTINCT item_printer_group.printer_group_id) AS printer_groups,
			
			GROUP_CONCAT(DISTINCT CONCAT(modifier.modifier_id,'^:^',
				modifier.name,'^:^',
				modifier.default_price,'^:^',
				IFNULL(modifier.options, ''),'^:^',
				side_modifier.option,'^:^',
				modifier.required,'^:^',
				modifier.category_id,'^:^',
				modifier.multi_select
			) SEPARATOR '|') AS modifiers", false);
		$this->db->from('table_item_sides AS side');
		$this->db->join('table_items AS parent_item', 'parent_item.sale_id = side.sale_id AND parent_item.line = side.cart_line', 'inner');
		$this->db->join('item_sides AS item_side', 'item_side.item_id = parent_item.item_id AND item_side.side_id = side.item_id', 'left');
		$this->db->join('items AS i', 'i.item_id = side.item_id', 'left');
		$this->db->join('table_item_side_modifiers AS side_modifier',
			'side_modifier.sale_id = side.sale_id AND side_modifier.line = side.cart_line AND side_modifier.side_type = side.type AND side_modifier.side_position = side.position', 'left');
		$this->db->join('modifiers AS modifier', "modifier.modifier_id = side_modifier.modifier_id", 'left');
		$this->db->join('item_printer_groups AS item_printer_group', 'item_printer_group.item_id = i.item_id', 'left');			
		$this->db->where('side.sale_id', (int) $sale_id);
		$this->db->where('side.cart_line', (int) $line);
		if(!empty($type)){
			$this->db->where('side.type', $type);
		}
		$this->db->group_by('parent_item.line, side.position, side.type');
		$this->db->order_by('side.type, side.position ASC');
		if(!empty($positions)){
			$this->db->where_in('side.position', $positions);
		}

		$result = $this->db->get();
		$rows = $result->result_array();

		// Organize sides with modifiers and taxes in multi-dimensional array
		$keys = array('soups' => 0, 'salads' => 0, 'sides' => 0);
		foreach($rows as $key => $row){
			$position = (int) $row['position'];
			$side_type = $row['type'];
			$item_id = (int) $row['item_id'];
			$sideKey = $keys[$side_type];
			$food_item =& $sides[$side_type][$sideKey];
			$food_item['print_priority'] = (int) $food_item['print_priority'];

			if(!isset($sides[$side_type][$position])){
				$food_item = $row;

				if(!empty($inherit)){
					$food_item = array_merge($inherit, $food_item);
				}
				
				// If course using new multi printers
				if($this->session->userdata('multiple_printers') == 1){
					
					$printer_group_ids = explode(',', $food_item['printer_groups']);
					$row['printers'] = $this->Printer_group_model->get_item_printers(
						$printer_group_ids, 
						$this->session->userdata('terminal_id'),
						(bool) $food_item['do_not_print']
					);
					$food_item['printer_ip'] = '';		
					
				}else{
					if($food_item['printer_ip'] == '1'){
						$food_item['printer_ip'] = '_hot_'.$this->config->item('webprnt_hot_ip');
					}else if($food_item['printer_ip'] == '2'){
						$food_item['printer_ip'] = '_cold_'.$this->config->item('webprnt_cold_ip');	
					}else{
						$food_item['printer_ip'] = '';
					}				
				}

				if($item_id == 0){
					$food_item['name'] = 'None';
					$food_item['item_id'] = 0;
					$food_item['price'] = 0;
					$food_item['tax'] = 0;
					$food_item['total'] = 0;
					$food_item['discount'] = 0;
					$food_item['quantity'] = 0;
				}

				// Add taxes to side as array list
				$food_item['taxes'] = array();
				if(!empty($taxes[$item_id])){
					$food_item['taxes'] = $taxes[$item_id];
				}
				$food_item['modifier_total'] = 0;

				// Break apart modifier data into array
				$modifiers = array();
				$modifiersArray = explode('|', $food_item['modifiers']);
				$food_item['modifiers'] = array();

				if(!empty($modifiersArray[0])){
					foreach($modifiersArray as $modifier){
						$modifierArray = explode('^:^', $modifier);

						$mod = array(
							'modifier_id' =>  $modifierArray[0],
							'name' => $modifierArray[1],
							'price' => $modifierArray[2],
							'options' => $modifierArray[3],
							'selected_option' => $modifierArray[4],
							'required' => $modifierArray[5],
							'category_id' => $modifierArray[6],
							'multi_select' => $modifierArray[7]
						);

						$structured_modifier = $this->modifier->structure_options($mod);
						$food_item['modifiers'][] = $structured_modifier;

						// Add price of modifiers to subtotal of side
						$food_item['modifier_total'] += (float) $structured_modifier['selected_price'];
					}
				}

				unset($food_item['modifier_name'], $food_item['modifier_id'],
					$food_item['modifier_default_price'], $food_item['modifier_options'],
					$food_item['modifier_selected_option'], $food_item['modifier_selected_price']);
				$keys[$side_type]++;
			}
			
			if(empty($sides['sides_comp_total'])){
				$sides['sides_comp_total'] = 0;
			}			
			
			// Calculate tax and final total of side
			$food_item['subtotal'] = $this->cart_lib->calculate_subtotal($food_item['price'] + $food_item['modifier_total'], $food_item['quantity'], $food_item['discount']);
			$food_item['subtotal_no_discount'] = $this->cart_lib->calculate_subtotal($food_item['price'] + $food_item['modifier_total'], $food_item['quantity'], 0);
			$food_item['tax'] = $this->cart_lib->calculate_item_tax($food_item['subtotal'], $food_item['item_id'], 'item');

			if($food_item['unit_price_includes_tax'] == 1){
				$food_item['subtotal'] -= $food_item['tax'];
				$food_item['subtotal_no_discount'] -= $food_item['tax'];
			}

			$food_item['base_subtotal'] = $food_item['subtotal_no_discount'];
			$food_item['discount_amount'] = $food_item['subtotal_no_discount'] - $food_item['subtotal'];			
			$food_item['comp_total'] = $this->get_comp_discount($food_item['comp'], $food_item['subtotal']);
			$food_item['subtotal'] -= $food_item['comp_total'];
			$food_item['total'] = $food_item['subtotal'] + $food_item['tax'];
			
			$sides['sides_subtotal'] += $food_item['subtotal'];	
			$sides['sides_tax'] += $food_item['tax'];	
			$sides['sides_total'] += $food_item['total'];
			$sides['sides_comp_total'] += $food_item['comp_total'];	
		}
		unset($rows);
		unset($taxes);

		if(!empty($type)){
			return $sides[$type];
		}else{
			return $sides;
		}
	}

	function item_payments($sale_id,$line) {
	    $sale_id;
        $line;
        $this->db->select();
        $this->db->from('table_payments as p');
        $this->db->join('table_receipt_items as ri', 'p.sale_id = ri.sale_id AND p.receipt_id = ri.receipt_id', 'left');
        $this->db->where('ri.sale_id', (int) $sale_id);
        $this->db->where('ri.line', (int) $line);
        $query = $this->db->get();

        $rows = $query->result_array();
        return $rows;
    }

    function too_late_to_return($sale_id) {
        $this->db->from('tables');
        $this->db->where('sale_id', (int) $sale_id);

        $table_info = $this->db->get()->result_array();

        if (date('Y-m-d H:i:s', strtotime($table_info['sale_time'].' + 24 hours')) < date('Y-m-d H:i:s') // Twenty four hours later
            || date('Y-m-d', strtotime($table_info['sale_time'])) == date('Y-m-d', '-1 day') && date('H') > 3) { // Or after 4 am the next day
            return true;
        } else {
            return false;
        }
    }

	function get_payments($sale_id, $receipt_id = null, $payment_type = null){
		if(empty($sale_id)){
			return false;
		}

		$this->db->select('sale_id, receipt_id, payment_type, payment_type as type, payment_amount,
			customer_id, credit_card_invoice_id, number, signature, tran_type, 
			credit_card_invoice_id, auth_code, credit_card_id');
		$this->db->from('table_payments');
		$this->db->where('sale_id', (int) $sale_id);
		if(!empty($receipt_id)){
			$this->db->where('receipt_id', (int) $receipt_id);
		}
		if(!empty($payment_type)){
			$this->db->where('payment_type', strtolower($payment_type));
		}
		$this->db->order_by('receipt_id');
		$query = $this->db->get();

		$rows = $query->result_array();

		if(!empty($payment_type) && !empty($rows[0])){
			return $rows[0];
		}
		foreach($rows as &$row){
			if(preg_match("/Gift Card/",$row['type'])){
				$giftcard_id = $this->Giftcard->get_giftcard_id($row['number']);
				$giftcard_info = $this->Giftcard->get_info($giftcard_id);
				$row['gift_card'] = $giftcard_info;
			}
		}

		return $rows;
	}

	function add_payment($sale_id, $receipt_id, $type, $amount, $customer_id = 0, 
						 $credit_card_invoice_id = 0, $number = null, $signature_url = null, 
						 $tran_type = null, $auth_code = null, $credit_card_id = null){
		if(empty($sale_id) || empty($receipt_id) || empty($type)){
			return false;
		}
		$amount = (float) round($amount, 2);
		$number = $this->db->escape($number);

		$existing_cc_payments = 0;
		if(!empty($credit_card_id) || !empty($credit_card_invoice_id)){
			$existing_cc_payments = $this->db->select('payment_type')
				->from('table_payments')
				->like('payment_type', $type)
                ->where('sale_id', $sale_id)
				->get()->num_rows();

			if($existing_cc_payments > 0){
				$type .= ' ('.($existing_cc_payments + 1).')';
			}
		}

		if(!empty($signature_url)){
			$signature_url = $this->db->escape($signature_url);
		}else{
			$signature_url = 'NULL';
		}

		$success = $this->db->query("INSERT INTO foreup_table_payments
			(sale_id, receipt_id, payment_type, payment_amount, credit_card_invoice_id, customer_id, number, signature, tran_type, auth_code,credit_card_id)
			VALUES (".(int) $sale_id.",".(int) $receipt_id.",".$this->db->escape($type).",".$amount.",".(int) $credit_card_invoice_id.",".(int) $customer_id.",".$number.", ".$signature_url.",'".$tran_type."','".$auth_code."','".$credit_card_id."')
				ON DUPLICATE KEY UPDATE
					payment_amount = payment_amount + VALUES(payment_amount)");

		return $type;
	}

	function delete_payment($sale_id, $receipt_id, $type){
		if(empty($sale_id) || empty($receipt_id) || empty($type)){
			return false;
		}

		$success = $this->db->delete('table_payments', array('sale_id'=>(int) $sale_id, 'receipt_id'=>(int) $receipt_id, 'payment_type'=>$type));
		return $success;
	}

	function mark_items_paid($sale_id, $items){

		if(empty($items)){
			return false;
		}
		if(!is_array($items)){
			$items = array($items);
		}

		$this->db->where_in('line', $items);
		$this->db->where('sale_id', $sale_id);
		return $this->db->update('table_items', array('is_paid' => 1));
	}

	function mark_items_unpaid($sale_id, $items){

		if(empty($items)){
			return false;
		}
		if(!is_array($items)){
			$items = array($items);
		}

		$this->db->where_in('line', $items);
		$this->db->where('sale_id', $sale_id);
		return $this->db->update('table_items', array('is_paid' => 0));
	}

	function mark_items_ordered($sale_id, $items){

		if(empty($items)){
			return false;
		}
		if(!is_array($items)){
			$items = array($items);
		}

		$this->db->where_in('line', $items);
		$this->db->where('sale_id', $sale_id);
		return $this->db->update('table_items', array('is_ordered' => 1));
	}
	
	function get_button_order(){
		
		$this->db->select('name, category, sub_category, order');
		$this->db->from('table_buttons');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$rows = $this->db->get()->result_array();
		
		$data = array();
		foreach($rows as $row){
			$key = $row['name'].'/'.$row['category'].'/'.$row['sub_category'];
			$data[$key] = (int) $row['order'];
		}
		unset($rows);
		
		return $data;
	}

	function merge($sourceNumber = false, $destinationNumber = false){
		
		if($sourceNumber === false || $destinationNumber === false){
			return false;
		}

		if($sourceNumber == $destinationNumber){
			return false;
		}

		$source = $this->db->select('sale_id')
			->from('tables')
			->where('table_id', $sourceNumber)
			->where('course_id', $this->session->userdata('course_id'))
			->get()->row_array();
		$sourceSaleId = $source['sale_id'];

		$destination = $this->db->select('sale_id')
			->from('tables')
			->where('table_id', $destinationNumber)
			->where('course_id', $this->session->userdata('course_id'))
			->get()->row_array();

		if(empty($destination['sale_id'])){
			$this->db->update('tables', 
				[
					'table_id' => $destinationNumber,
					'sale_time' => date('Y-m-d H:i:s')
				], 
				[
					'sale_id' => $sourceSaleId
				]
			);
			return true;
		}else{
			$destinationSaleId = $destination['sale_id'];
		}

		$cart = $this->db->select('MAX(line) AS last_line')
			->from('table_items')
			->where('sale_id', $destinationSaleId)
			->get()->row_array();
		$lastLine = (int) $cart['last_line'];

		$receipt = $this->db->select('MAX(receipt_id) AS last_receipt_id')
			->from('table_receipts')
			->where('sale_id', $destinationSaleId)
			->get()->row_array();
		$lastReceiptId = (int) $receipt['last_receipt_id'];

		// Begin merging source table into destination table
		$this->db->trans_start();

		$this->db->query("UPDATE foreup_table_comps
			SET line = line + {$lastLine}, sale_id = {$destinationSaleId}
			WHERE sale_id = {$sourceSaleId}");

		$this->db->update('table_customers', ['sale_id' => $destinationSaleId], ['sale_id' => $sourceSaleId]);

		$this->db->query("UPDATE foreup_table_items
			SET line = line + {$lastLine}, sale_id = {$destinationSaleId}
			WHERE sale_id = {$sourceSaleId}");

		$this->db->query("UPDATE foreup_table_items_modifiers
			SET line = line + {$lastLine}, sale_id = {$destinationSaleId}
			WHERE sale_id = {$sourceSaleId}");

		$this->db->query("UPDATE foreup_table_item_sides
			SET cart_line = cart_line + {$lastLine}, sale_id = {$destinationSaleId}
			WHERE sale_id = {$sourceSaleId}");

		$this->db->query("UPDATE foreup_table_item_side_modifiers
			SET line = line + {$lastLine}, sale_id = {$destinationSaleId}
			WHERE sale_id = {$sourceSaleId}");

		$this->db->query("UPDATE foreup_table_payments
			SET receipt_id = receipt_id + {$lastReceiptId}, sale_id = {$destinationSaleId}
			WHERE sale_id = {$sourceSaleId}");

		$this->db->query("UPDATE foreup_table_receipts
			SET receipt_id = receipt_id + {$lastReceiptId}, sale_id = {$destinationSaleId}
			WHERE sale_id = {$sourceSaleId}");

		$this->db->query("UPDATE foreup_table_receipt_items
			SET receipt_id = receipt_id + {$lastReceiptId},
				line = line + {$lastLine},
				sale_id = {$destinationSaleId}
			WHERE sale_id = {$sourceSaleId}");

		$this->db->update('table_tickets', ['sale_id' => $destinationSaleId], ['sale_id' => $sourceSaleId]);
		$this->db->delete('tables', ['sale_id' => $sourceSaleId]);

		$this->db->trans_complete();

		if($this->db->trans_status() === false){
			return false;
		}

		return true;
	}
}