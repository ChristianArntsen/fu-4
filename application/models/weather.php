<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Weather extends CI_Model {
	private $timestamp;
	private $forecast;

	function getLatest(){
		$this->loadFromDatabase();
		if($this->timeToGetNewWeather() == true){
			return $this->fetchAndStoreWeather();
		} else {
			return $this->forecast;
		}

	}

	public function getWeatherInPast($date){
		$date = \Carbon\Carbon::parse($date);
		$query = $this->db->from("weather")
			->where("date >= '" .$date->startOfDay()."'")
			->where("date <= '".$date->endOfDay()."'")
			->where("course_id = ".$this->session->userdata('course_id'))
			->select(["ROUND(MAX(temperature)) as max","ROUND(MIN(temperature)) as min","icon","day"])
			;
		$historical = $query->get()->row();
		return $historical;
	}


	private function timeToGetNewWeather(){
		//echo 'here'.strtotime($this->timestamp." +5 minutes")." ".time().'<br/>';
		if(strtotime(($this->timestamp != '' ? $this->timestamp : '1/1/1970') ." +5 minutes") < time() || empty($this->forecast)){
			return true;
		} else {
			return false;
		}
	}

	private function loadFromDatabase(){
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$forecast = $this->db->get('courses_weather_forecast')->row();
		if(empty($forecast)){
			return "";
		}
		$this->timestamp = $forecast->timestamp;
		$this->forecast = json_decode($forecast->forecast,true);
	}

	private function fetchAndStoreWeather(){
		//If Lat/Long doesn't exist
		//If Zip exists Get Lat/Long and store it
		//With lat/long get weather


		$this->db->from("courses");
		$this->db->select(["latitude_centroid","longitude_centroid"]);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$course_info = $this->db->get()->row_array();


		$url = $this->config->item('forecastio_api_url').'/'.$this->config->item('forecastio_api_key').'/'.$course_info['latitude_centroid'].','.$course_info['longitude_centroid'];
		$historical_weather = json_decode(file_get_contents($url));
		// Fetch Forecast.io Information
		$historical_weather = json_decode(file_get_contents($url));
		$historical_weather->daily->data[0]->temperatureMin;
		$historical_weather->daily->data[0]->icon;
		$historical_weather->daily->data[0]->time;

		$historical_weather->currently->icon;
		$historical_weather->currently->temperature;


		$weather_array = [];
		$weather_array['today'] = [
			"image"=>$historical_weather->currently->icon,
			"temp"=>$this->convert_temp($historical_weather->currently->temperature),
			"city"=>"",
		];
		$weather_array['forecast'] = [];
		foreach($historical_weather->daily->data as $key => $day){
			$date = \Carbon\Carbon::createFromTimestamp($day->time);
			$weather_array['forecast'][] = array(
				'day'=>$date->format("D"),
				'low_temp'=>(int)$this->convert_temp($day->temperatureMin),
				'high_temp'=>(int)$this->convert_temp($day->temperatureMax),
				'image'=>$day->icon,
				'date'=>$date->format("Y-m-d")
			);
		}

		$this->storeForecast($weather_array);
		return $weather_array;
	}

	private function convert_temp($temp)
	{
		if($this->config->item("currency_symbol") != "$"){
			return  ($temp - 32) * 5/9;
		}

		return $temp;
	}


	private function storeForecast($forecast)
	{
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$current_forecast = $this->db->get('courses_weather_forecast')->row();
		$data = [
			"forecast"=>json_encode($forecast),
			"course_id"=>$this->session->userdata('course_id')
		];

		if(empty($current_forecast)){
			$results = $this->db->insert("courses_weather_forecast",$data);
		} else {
			$results = $this->db
				->where("course_id",$this->session->userdata('course_id'))
				->update("courses_weather_forecast",$data);
		}

		return $results;
	}

}
?>
