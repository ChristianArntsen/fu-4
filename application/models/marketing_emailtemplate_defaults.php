<?php
/*
 * A template is just a json object that represents an email
 */
class Marketing_Emailtemplate_Defaults extends CI_Model
{
	public function __construct()
	{
		parent::__construct();


		$this->load->model('course');
	}



	/*
	 *
	 */
	public function get_list()
	{
		$templates = array();

		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$this->db->select('*');
		$this->db->from('marketing_templates_defaults');
		$result = $this->db->get();
		$templates['default']= $result->result_array();


		$this->db->from('marketing_templates');
		$this->db->where('course_id',$course_info->course_id);
		$result = $this->db->get();
		$templates['saved']= $result->result_array();

		if(!$templates)
			return false;
		return $templates;
	}

}