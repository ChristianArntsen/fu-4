<?php

use fu\credit_cards\ProcessorFactory;

class Credit_card_payments extends CI_Model {

    var $cart_id = 0;
    var $msg = null;

    /*
     *
     *  Adding this section to unify all payment processing into a single class
     *  This should handle each processor in one place
     *  The purpose is to cut down the many places the payment code is referenced
     *
     */

    function credit_card_window() {
        $total = $this->input->post('amount');
        $action = $this->input->post('action');
        $payment_type = $this->input->post('type');
        $cart_id = $this->input->post('cart_id');
        $transaction_type = $this->input->post('transaction_type');
        $sale_id = $this->input->post('sale_id');
        $source = $this->input->post('source');
        $receipt_id = $this->input->post('receipt_id');
        $table_number = $this->input->post('table_number');
        $giftcard_void = $this->input->post('giftcard_void');
        $bypass_fee = $this->input->post('bypass_fee');
        $billing_address = $this->input->post('address');
        $billing_zip = $this->input->post('zip');
        $source = $this->input->post('source');
        $customer_note = $this->input->post('customer_note');
        $tax = 0;
        $window = 'payment';

        if(empty($source)){
            $source = 'sales';
        }

        if(empty($giftcard_void)){
            $giftcard_void = false;
        }

        if(empty($action)){
            $action = 'sale';
        }

        // If course charges a credit card fee, add the fee into the charge amount
        // Do not charge fees on return. Fees only implemented in POS for now
        // (not F&B)
        $fee_amount = 0;
        if($payment_type != 'credit_card_save' && $action == 'sale' && !$bypass_fee && $this->config->item('credit_card_fee') > 0 && $payment_type != 'gift_card'){
            $fee_percentage = round($this->config->item('credit_card_fee') / 100, 5);
            $fee_amount = round($total * $fee_percentage, 2);
        }
        $total += $fee_amount;

        if(empty($total) && $payment_type != 'credit_card_save'){
            echo 'Error: Transaction amount can not be 0.00';
            echo "<script>setTimeout(function(){ $('div.modal-footer #cancel-credit-card-payment').show();}, 200);</script>";
            return false;
        }
        $total = abs($total);

        // USING APRIVA FOR PAYMENT PROCESSING
        if ($this->config->item('apriva_username')) {
            $data = array();
            $processor = \fu\credit_cards\ProcessorFactory::create("apriva", $data);
            $trans_type = $action == 'refund' ? 'return' : 'sale';

            $payment_lock = new \models\payment_lock();
            if(!empty($table_number)){
                $payment_lock->init_by_cart_amount($table_number."-".$receipt_id,$total);
            } else if(!empty($cart_id)){
                $payment_lock->init_by_cart_amount("Cart"."-".$cart_id,$total);
            }

            $lock = $payment_lock->check_payment_lock();
            // If there is a payment lock in place
            if( $lock && !empty($lock->invoice_id) ) {

                $payment_data = $this->get_info($lock->invoice_id);
                // Reuse invoice id if possible
                if($this->can_use_payment($payment_data)){

                    $invoice_id = $lock->invoice_id;
                    $this->session->set_userdata('invoice_id', $lock->invoice_id);

                // Otherwise restart
                } else {

                    $payment_lock->release_lock();
                    $this->credit_card_window();
                    return ;
                }
            // If there is no payment lock, create one
            } else {
                $invoice_id = $this->Sale->add_credit_card_payment(array('tran_type'=>$action, 'frequency'=>'OneTime'));
                $payment_lock->create_lock($invoice_id,null);
            }

            $params = array(
                'command_type' => 'credit',
                'transaction_type' => $trans_type,
                'trans_amount' => number_format($total,2),//$total,
                'tax_amount' => '0',
                'tip_amount' => '0',
                'ref_no' => $invoice_id
            );

            $terminal_id = $this->session->userdata('terminal_id');
            $processor->initiatePayment($params, $terminal_id);
        }
        // USING ETS FOR PAYMENT PROCESSING
        else if ($this->config->item('ets_key')){
//            // NEW ETS INTEGRATION
//            $data = array(
//                'amount' => $total,
//                'type' => $payment_type,
//                'action' => $action,
//                'cart_id' => $cart_id
//            );
//            if ($this->input->get('inside_iframe')) {
//                $this->load->view('v2/sales/ets_v3_credit_card', $data);
//            } else {
//                $this->load->view('v2/sales/ets_v3_iframe', $data);
//            }
//return;
//            // OLD ETS INTEGRATION
            $this->load->library('Hosted_payments');
            $payment = new Hosted_payments();
            $payment->initialize($this->config->item('ets_key'));

            $invoice_id = $this->Sale->add_credit_card_payment(array('tran_type'=>'Sale', 'frequency'=>'OneTime'));

            if($payment_type == 'credit_card_save'){
                $session = $payment->set('action', 'session')
                    ->set('isSave', 'true')
                    ->set('PaymentMethods', "CreditCard")
                    ->set('store.Primary', $invoice_id)
                    ->send();

            }else if($payment_type == 'credit_card' || $payment_type == 'credit_card_partial_refund'){
                if($action == 'refund'){
                    $session = $payment->set('action', 'session')
                        ->set('posAction', 'refund')
                        ->set('amount', $total)
                        ->set('store.Primary', $invoice_id)
                        ->set('PaymentMethods', "CreditCard")
                        ->send();

                }else{
                    $session = $payment->set('action', 'session')
                        ->set('amount', $total)
                        ->set('store.Primary', $invoice_id)
                        ->set('PaymentMethods', "CreditCard")
                        ->send();
                }
           
            }else if ($payment_type == 'gift_card'){
                if($action == 'refund'){
                    $session = $payment->set('action', 'session')
                        ->set('posAction', 'refund')
                        ->set('amount', $total)
                        ->set('store.Primary', $invoice_id)
                        ->set('PaymentMethods', "GiftCard")
                        ->send();
                    $window = 'gift_card_refund';

                }else{
                    $session = $payment->set('action', 'session')
                        ->set('amount', $total)
                        ->set('store.Primary', $invoice_id)
                        ->set('PaymentMethods', "GiftCard")
                        ->send();
                }
            }

            if($session->id){
                $return_code = '';
                $this->session->set_userdata('ets_session_id', (string) $session->id);

                $data = array(
                    'action' => $action,
                    'return_code' => $return_code,
                    'session' => $session,
                    'amount' => $total,
                    'payment_type' => $payment_type,
                    'invoice_id' => $invoice_id,
                    'source' => $source,
                    'receipt_id' => $receipt_id,
                    'window' => $window,
                    'type' => $payment_type,
                    'giftcard_void' => $giftcard_void,
                    'fee' => $fee_amount,
                    'customer_note' => $customer_note
                );

                $this->load->view('v2/sales/ets_credit_card', $data);

            }else{
                $data = array('processor' => 'ETS', 'session' => $session);
                $this->load->view('v2/sales/credit_card_error', $data);
            }

            // USING MERCURY FOR PAYMENT PROCESSING
        }else if($this->config->item('mercury_id')){
            $this->load->model('Blackline_devices');
            $blackline_info = $this->Blackline_devices->get_terminal_info($this->session->userdata('terminal_id'));

            if($this->config->item('use_mercury_emv') && $blackline_info){
                $data = array();
                $trans_type = $action == 'refund' ? 'Refund' : 'Sale';
                if ($payment_type == 'credit_card_save') {
                    $trans_type = 'auth';
                    $total = 1;
                }
                $payment_data = array(
                    'course_id' => $this->config->item('course_id'),
                    'mercury_id' => $this->config->item('mercury_id'),
                    'mercury_password' => $this->config->item('mercury_password'),
                    'tran_type' => $trans_type,
                    'amount' => $total,
                    'frequency' => 'OneTime',
                    'terminal_name' => $blackline_info['blackline_terminal_id'],
                    'initiation_time' => gmdate('Y-m-d H:i:s'),
                    'emv_payment' => 1,
                    'customer_note' => $customer_note
                );
                $data['invoice_id'] = $invoice_id = $this->Sale->add_credit_card_payment($payment_data);
                $this->session->set_userdata('mercury_emv', array(
                    'amount' => $total,
                    'invoice_id' => $invoice_id,
                    'type' => $payment_type,
                    'action' => $action,
                    'sale_id' => $sale_id,
                    'source' => $source,
                    'receipt_id' => $receipt_id,
                    'fee' => $fee_amount,
                    'customer_note' => $customer_note
                ));
                $data['payment_url'] = $this->Blackline_devices->get_payment_url($trans_type, $total, $blackline_info, $invoice_id);
                $this->load->view('v2/sales/mercury_emv', $data);
            
            }else{
                $this->load->library('Hosted_checkout_2');
                $HC = new Hosted_checkout_2();

                $HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
                $HC->set_response_urls('v2/home/mercury_payment', 'v2/home/mercury_error');


                $payment_lock = new \models\payment_lock();
                if(!empty($table_number)){
                    $payment_lock->init_by_cart_amount($table_number."-".$receipt_id,$total);
                } else if(!empty($cart_id)){
                    $payment_lock->init_by_cart_amount("Cart"."-".$cart_id,$total);
                }

	            $lock = $payment_lock->check_payment_lock();
                if( $lock && !empty($lock->payment_id) ) {

                    $payment_data = $this->get_info(false, $lock->payment_id);
                    $transaction_results = $this->check($payment_data);
                    $payment_data = $this->get_info(false, $lock->payment_id);
                    if($this->can_use_payment($payment_data)){
                        $HC->set_invoice($lock->invoice_id);
                        $this->session->set_userdata('invoice_id', $lock->invoice_id);
                        $initialize_results = (object)[
                            "PaymentID"=>$lock->payment_id,
                            "ResponseCode"=>0
                        ];

                    } else {
                        if($this->is_payment_floating($lock->invoice_id)){
                            $this->mark_to_be_voided($lock->invoice_id);
                        }
                        $payment_lock->release_lock();
                        $this->credit_card_window();
                        return ;
                    }
                } else {
                    if($payment_type == 'credit_card_save'){
                        $initialize_results = $HC->initialize_payment('1.00', '0.00', 'PreAuth', 'POS', 'OneTime');
                    }else if($action == 'refund'){
                        $initialize_results = $HC->initialize_payment($total, 0, 'Return', 'POS', 'OneTime');
                    }else{
                        $initialize_results = $HC->initialize_payment($total, 0, 'Sale', 'POS', 'OneTime');
                    }

                    $payment_lock->create_lock($HC->invoice,(string)$initialize_results->PaymentID);
                }




                $invoice_id = (int)$HC->invoice;
                $this->session->set_userdata('mercury_payment', array(
                    'amount' => $total,
                    'invoice_id' => $invoice_id,
                    'type' => $payment_type,
                    'action' => $action,
                    'sale_id' => $sale_id,
                    'source' => $source,
                    'receipt_id' => $receipt_id,
                    'table_number' => $table_number,
                    'fee' => $fee_amount,
                    'customer_note' => $customer_note
                ));

                if((int)$initialize_results->ResponseCode === 100){
                    // provide useful feedback when mercury authentication fails
                    $return_code = (int)$initialize_results->ResponseCode;
                    $message = $initialize_results->Message;
                    $data = array(
                        'return_code' => $return_code,
                        'error' => $message,
                        'url' => '/401.html'
                    );
                    $this->load->view('v2/sales/mercury_credit_card', $data);
                }elseif((int)$initialize_results->ResponseCode === 0){
                    $payment_id = (string)$initialize_results->PaymentID;
                    $return_code = (int)$initialize_results->ResponseCode;
                    $this->session->set_userdata('payment_id', $payment_id);
                    $url = $HC->get_iframe_url('POS', $payment_id);
                   
                    $data = array(
                        'return_code' => $return_code,
                        'url' => $url
                    );

                    $payment_data = array(
                        'course_id' => $this->session->userdata('course_id'),
                        'mercury_id' => $this->config->item('mercury_id'),
                        'mercury_password' => $this->config->item('mercury_password'),
                        'tran_type' => $action == 'refund' ? 'Return' : 'Sale',
                        'amount' => $total,
                        'frequency' => 'OneTime',
                        'terminal_name' => $this->session->userdata('terminal_id'),
                        'initiation_time' => gmdate('Y-m-d H:i:s'),
                        'payment_id' => (string) $initialize_results->PaymentID,
                        'cart_id' => $cart_id
                    );
                    $this->update_credit_card_payment($invoice_id, $payment_data);

                    $this->load->view('v2/sales/mercury_credit_card', $data);
                }
            }

        }else if($this->config->item('element_account_id')){

            $this->load->library('v2/Element_merchant');

            // Initialize payment library
            $this->element_merchant->init(
                new GuzzleHttp\Client()
            );

            $market_code = Element_merchant::MARKET_CODE_RETAIL;
            if($source == 'f&b'){
                $market_code = Element_merchant::MARKET_CODE_FOOD_RESTAURANT;
            }

            // Add row to credit card payments table (to be updated in future)
            $this->load->model('v2/Sale_model');
            $cc_invoice_id = $this->Sale_model->save_credit_card_payment(null, array());

            $params = [
                'amount' => $total,
                'return_url' => site_url('v2/home/element_payment'),
                'market_code' => $market_code,
                'transaction_setup_method' => Element_merchant::TRANSACTION_SETUP_CREDIT_CARD_SALE,
                'billing_address1' => $billing_address,
                'billing_zipcode' => $billing_zip,
                'reference_number' => $cc_invoice_id,
                'ticket_number' => $cc_invoice_id
            ];

            if($payment_type == 'credit_card_save'){
                $params['transaction_setup_method'] = Element_merchant::TRANSACTION_SETUP_PAYMENT_ACCOUNT_CREATE;
                $params['payment_account_reference_number'] = 0;
                $params['payment_account_type'] = Element_merchant::PAYMENT_ACCOUNT_CREDIT_CARD;                

                unset($params['amount']);
            }

            // Get URL for payment iframe
            $url = $this->element_merchant
                ->set_terminal(array(
                    'CardInputCode' => Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED_MAGSTRIPE_FAILURE
                ))->init_hosted_payment(
                    $params
                )->hosted_payment_url();

            $this->session->set_userdata('element_payment', array(
                'amount' => $total,
                'invoice_id' => $cc_invoice_id,
                'action' => $action,
                'type' => $payment_type,
                'sale_id' => $sale_id,
                'source' => $source,
                'receipt_id' => $receipt_id,
                'fee' => $fee_amount
            ));

            $this->load->view('v2/sales/element_credit_card', array('url' => $url));
        }
    }

    function get_info($invoice = false, $payment_id = false) {
        if (empty($invoice) && empty($payment_id)) {
            return array();
        }

        $this->db->from('sales_payments_credit_cards');
        if (!empty($invoice)) {
            $this->db->where('invoice', $invoice);
        }
        if (!empty($payment_id)) {
            $this->db->where('payment_id', $payment_id);
        }
        $this->db->limit(1);

        return $this->db->get()->row_array();
    }

    function cancel_credit_card_payment()
    {

    }

    function complete_payment($payment, $data){
        
        if ($this->config->item('mercury_id')) {
            return $this->mercury_payment($payment, $data);
        }

        return false;
    }

    function mercury_payment($payment, $mercury_data){

        $this->load->model('Blackline_devices');
        $blackline_info = $this->Blackline_devices->get_terminal_info($this->session->userdata('terminal_id'));
        if(empty($mercury_data['payment_id']) && empty($mercury_data['credit_card_id']) && !($this->config->item('use_mercury_emv') && $blackline_info)){
            return false;
        }
        if($this->config->item('use_mercury_emv') && $blackline_info && empty($mercury_data['credit_card_id'])){
            $this->update_credit_card_payment($payment['record_id'], array('payment_id'=>$mercury_data['payment_id']));
            $rd = $payment['merchant_data'];

            $invoice_id = $payment['record_id'];
            if ($rd['CardType'] == 'Discover') {
                $rd['CardType'] = "DCVR";
            }
            else if ($rd['CardType'] == 'Amex') {
                $rd['CardType'] = "AMEX";
            }
            else if ($rd['CardType'] == 'MasterCard') {
                $rd['CardType'] = "M/C";
            }
            else if ($rd['CardType'] == 'Visa') {
                $rd['CardType'] = "VISA";
            }
            $payment_data = array(
                'course_id' => $this->session->userdata('course_id'),
                'mercury_id' => $this->config->item('mercury_id'),
                'tran_type' => $rd['TransType'],
                'amount' => $rd['Amount'],
                'auth_amount' => $rd['Amount'],
                'card_type' => $rd['CardType'],
                'frequency' => 'OneTime',
                'masked_account' => $rd['Last4'],
                'cardholder_name' =>  $rd['Name'],
                'terminal_name' => $rd['TerminalID'],
                'trans_post_time' => gmdate('Y-m-d H:i:s'),
                'auth_code' => $rd['AuthCode'],
                'acq_ref_data' => $rd['ProcessorExtraData1'],
                'process_data' => $rd['ProcessorExtraData2'],
                'ref_no' => $rd['ProcessorExtraData3'],
                'memo' => 'ForeUP v.1.0',
                'token' => $rd['CardToken'],
                'response_code' => $rd['ResultCode'],
                'status' => $rd['ResultCode'],
                'status_message' => $rd['ReturnMessage'],
                'display_message' => $rd['Message'],
                'signature_data' => $rd['SignatureData'],
                'cvv_result' => $rd['EntryMethod'],
                'avs_address' => $rd['EMV_AID'],
                'avs_result' => $rd['EMV_TVR'],
                'avs_zip' => $rd['EMV_TSI']
            );
            // If saving credit card, store CC details in database
            if($payment['type'] == 'credit_card_save'){

                $this->load->model('Customer_credit_card');
                $card_data = [
                    'recurring' => 0,
                    'customer_id' => 0,
                    'course_id' => $this->session->userdata('course_id'),
                    'token' => $payment_data['token'],
                    'card_type' => $payment_data['card_type'],
                    'masked_account' => $payment_data['masked_account']
                ];
                $credit_card_id = $this->Customer_credit_card->save($card_data);

                $payment['params']['credit_card_id'] = (int) $credit_card_id;
            }

            $this->update_credit_card_payment($invoice_id, $payment_data);
            if ((string) $rd['ResultCode'] != 'Approved') {
                $this->msg = (string) $rd['Message'];
                return false;
            }
            $amount = (float) $rd['Amount'];
            if ($rd['TransType'] == 'return') {
                $amount = (float) 0 - $amount;
            }

            $payment['type'] = 'credit_card';
            $payment['description'] = $rd['CardType'] . ' ' . $rd['Last4'];
            $payment['amount'] = $amount;
            $payment['record_id'] = (int)$payment['record_id'];
            $payment['cardholder_name'] = $rd['Name'];
            $payment['card_number'] = $rd['Last4'];
            $payment['card_type'] = $rd['CardType'];
            $payment['auth_code'] = $rd['AuthCode'];
            $payment['number'] = $rd['Last4'];
            $payment['tran_type'] = $rd['TransType'];

            $payment['params']['cardholder_name'] = $rd['Name'];
            $payment['params']['masked_account'] = $rd['Last4'];
            $payment['params']['card_type'] = $rd['CardType'];
            $payment['params']['amount_refunded'] = 0;
        
        }else{
            $this->load->model('sale');
            $this->load->library('Hosted_checkout_2');      
            
            $HC = new Hosted_checkout_2();
            $HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));

            // If charging a saved (tokenized) card
            $token = false;
            if(!empty($mercury_data['credit_card_id'])){
                
                $credit_card = $this->Customer_credit_card->get_info($mercury_data['credit_card_id']);
                $token = $credit_card->token;

                $memo = 'ForeUP v.1.0';
                // TODO: we can search by he token to get the memo...
                // but it means changing the database, which is dangerous at this time
                /*
                $this->db->from('sales_payments_credit_cards');
                $this->db->where("token",$token);

                $getit = $this->db->get()->result_array();
                if(count($getit)){
                    $memo = $getit[0]['memo'];
                }
                */


                $HC->set_frequency('OneTime');
                $HC->set_token($token);
                $transaction_results = $HC->token_transaction('Sale', $payment['amount'], '0.00', '0.00');

                $invoice_id = $this->sale->add_credit_card_payment([
                    'tran_type' => 'Sale',
                    'frequency' => 'OneTime',
                    'mercury_id' => $this->config->item('mercury_id'),
                    'mercury_password' => $this->config->item('mercury_password')
                ]);

                $payment_id = (string) $transaction_results->PaymentID;
            
                $transaction_time = date('Y-m-d H:i:s');

                $payment_data = array(
                    'course_id' => $this->session->userdata('course_id'),
                    'mercury_id' => $this->config->item('mercury_id'),
                    'tran_type' => 'Sale',
                    'cardholder_name' => $credit_card->cardholder_name,
                    'frequency' => 'OneTime',
                    'acq_ref_data' => (string) $transaction_results->AcqRefData,
                    'auth_code' => (string) $transaction_results->AuthCode,
                    'auth_amount' => (string) $transaction_results->AuthorizeAmount,
                    'avs_result' => (string) $transaction_results->AVSResult,
                    'batch_no' => (string) $transaction_results->BatchNo,
                    'card_type' => (string) $transaction_results->CardType,
                    'cvv_result' => (string) $transaction_results->CVVResult,
                    'gratuity_amount' => (string) $transaction_results->GratuityAmount,
                    'masked_account' => (string) $credit_card->masked_account,
                    'status_message' => (string) $transaction_results->Message,
                    'amount' => (string) $transaction_results->PurchaseAmount,
                    'ref_no' => (string) $transaction_results->RefNo,
                    'status' => (string) $transaction_results->Status,
                    'token' => (string) $transaction_results->Token,
                    'process_data' => (string) $transaction_results->ProcessData,
                    'trans_post_time' => (string) $transaction_time,
                    'cart_id' => $this->cart_id,
                    'memo' => (string) $memo
                );     

            // Verify a charge already made
            }else{
                $payment_id = $mercury_data['payment_id'];
                $invoice_id = $payment['record_id'];

                $HC->set_payment_id($payment_id);
                $transaction_results = $HC->verify_payment();



                $payment_data = array(
                    'course_id' => $this->session->userdata('course_id'),
                    'mercury_id' => $this->config->item('mercury_id'),
                    'tran_type' => (string) $transaction_results->TranType,
                    'amount' => (string) $transaction_results->Amount,
                    'auth_amount' => (string) $transaction_results->AuthAmount,
                    'card_type' => (string) $transaction_results->CardType,
                    'frequency' => 'OneTime',
                    'masked_account' => (string) $transaction_results->MaskedAccount,
                    'cardholder_name' => (string) $transaction_results->CardholderName,
                    'ref_no' => (string) $transaction_results->RefNo,
                    'operator_id' => (string) $transaction_results->OperatorID,
                    'terminal_name' => (string) $transaction_results->TerminalName,
                    'trans_post_time' => (string) $transaction_results->TransPostTime,
                    'auth_code' => (string) $transaction_results->AuthCode,
                    'voice_auth_code' => (string) $transaction_results->VoiceAuthCode,
                    'payment_id' => $payment_id,
                    'acq_ref_data' => (string) $transaction_results->AcqRefData,
                    'process_data' => (string) $transaction_results->ProcessData,
                    'token' => (string) $transaction_results->Token,
                    'response_code' => (int) $transaction_results->ResponseCode,
                    'status' => (string) $transaction_results->Status,
                    'status_message' => (string) $transaction_results->StatusMessage,
                    'display_message' => (string) $transaction_results->DisplayMessage,
                    'avs_result' => (string) $transaction_results->AvsResult,
                    'cvv_result' => (string) $transaction_results->CvvResult,
                    'tax_amount' => (string) $transaction_results->TaxAmount,
                    'avs_address' => (string) $transaction_results->AVSAddress,
                    'avs_zip' => (string) $transaction_results->AVSZip,
                    'payment_id_expired' => (string) $transaction_results->PaymendIDExpired,
                    'customer_code' => (string) $transaction_results->CustomerCode,
                    'memo' => (string) $transaction_results->Memo
                );
            }
    
            // If saving credit card, store CC details in database
            if($payment['type'] == 'credit_card_save'){
                
                $this->load->model('Customer_credit_card');  
                $card_data = [
                    'recurring' => 0,
                    'customer_id' => 0,
                    'course_id' => $this->session->userdata('course_id'),
                    'token' => $payment_data['token'],
                    'card_type' => $payment_data['card_type'],
                    'masked_account' => $payment_data['masked_account']
                ];
                $credit_card_id = $this->Customer_credit_card->save($card_data);

                $payment['params']['credit_card_id'] = (int) $credit_card_id;
            }

            $this->update_credit_card_payment($invoice_id, $payment_data);
            $tran_type = (string) $transaction_results->TranType;

            if ((string) $transaction_results->Status != 'Approved') {
                $this->msg = (string) $payment_data['status_message'];
                return false;
            }

            $amount = (float) $payment_data['auth_amount'];
            if($tran_type == 'Return'){
                $amount = (float) 0 - $amount;
           
            }else if($tran_type == 'PreAuth'){
                $amount = 0;
            }

            $payment['description'] = $payment_data['card_type'] . ' ' . $payment_data['masked_account'];
            $payment['amount'] = $amount;
            $payment['record_id'] = (int) $invoice_id;
            $payment['cardholder_name'] = $payment_data['cardholder_name'];
            $payment['card_number'] = $payment_data['masked_account'];
            $payment['card_type'] = $payment_data['card_type'];
            $payment['auth_code'] = $payment_data['auth_code'];
            $payment['number'] = $payment_data['masked_account'];
            $payment['auth_amount'] = $payment_data['auth_amount'];
            $payment['tran_type'] = $payment_data['tran_type'];

            $payment['params']['cardholder_name'] = $payment_data['cardholder_name'];
            $payment['params']['masked_account'] = $payment_data['masked_account'];
            $payment['params']['card_type'] = $payment_data['card_type'];
            $payment['params']['amount_refunded'] = 0;
        }
        $payment['verified'] = 1;


        return $payment;
    }

    function blackline_payment()
    {

    }

    function element_payment()
    {

    }

    function mercury_error()
    {

    }

    function ets_giftcard_window()
    {

    }

    /*
     *  End combining existing credit card functionality
     */

    function get_incomplete_payments($processor = 'all', $threshold = 3, $initiation_time = false) {
        if (!$initiation_time) {
            $initiation_time = gmdate('Y-m-d H:i:s', strtotime(' -30 minutes '));
        } else {
            $initiation_time = gmdate('Y-m-d H:i:s', strtotime(urldecode($initiation_time)));
        }
        $five_minutes_ago = gmdate('Y-m-d H:i:s', strtotime(' -5 minutes '));

        $this->db->from('sales_payments_credit_cards');
        if ($processor == 'mercury' || $processor == 'all') {
            $this->db->where("mercury_id != ''");
        }
        if ($processor == 'ets' || $processor == 'all') {
            $this->db->where("ets_id != ''");
        }
        if ($processor == 'element' || $processor == 'all') {
            $this->db->where('');
        }
        $this->db->where('status', '');
        $this->db->where("success_check_count < $threshold");
        $this->db->where("initiation_time > '$initiation_time'");
        $this->db->where("initiation_time < '$five_minutes_ago'");

        return $this->db->get()->result_array();
    }

    function get_early_cancelled_payments($processor = 'all', $cancel_time = false) {
        if (!$cancel_time) {
            $cancel_time = gmdate('Y-m-d H:i:s', strtotime(' -30 minutes '));
        } else {
            $cancel_time = gmdate('Y-m-d H:i:s', strtotime(urldecode($cancel_time)));
        }

        $this->db->from('sales_payments_credit_cards');
        if ($processor == 'mercury' || $processor == 'all') {
            $this->db->where("mercury_id != ''");
        }
        if ($processor == 'ets' || $processor == 'all') {
            $this->db->where("ets_id != ''");
        }
        if ($processor == 'element' || $processor == 'all') {
            $this->db->where('');
        }
        $this->db->where("cancel_time > '$cancel_time'");
        $this->db->where("trans_post_time != '0000-00-00 00:00:00'");

        return $this->db->get()->result_array();
    }

    function check($payment_data) {
        $this->mark_checked($payment_data['invoice']);
        if ((int)$payment_data['emv_payment']) {
            $command = $payment_data['tran_type'];
            $terminal_id = $payment_data['terminal_name'];
            $ref_id = $payment_data['invoice'];
            $date = date('mdY', strtotime($payment_data['initiation_time']));

            // Fetch blackline info
            $this->load->model('Blackline_devices');
            $blackline_info = $this->Blackline_devices->get_terminal_info(false, $payment_data['terminal_name']);

            $key = $blackline_info['blackline_api_key'];
            $password = $blackline_info['blackline_api_password'];

            // Get payment response

            $url = "https://econduitapp.com/services/api.asmx/checkStatus?command={$command}&key={$key}&password={$password}&terminalId={$terminal_id}&refID={$ref_id}&date={$date}";

            $result = json_decode(file_get_contents($url));

            $new_payment_data = array(
                'tran_type' => $command,
                'amount' => $result->Amount,
                'auth_amount' => (string)$result->Amount,
                'card_type' => (string)$result->CardType,
                'masked_account' => (string)$result->Last4,
                'cardholder_name' => (string)$result->Name,
                'ref_no' => (string)$result->ProcessorExtraData3,
                'terminal_name' => (string)$terminal_id,
                'trans_post_time' => gmdate('Y-m-d H:i:s'),
                'auth_code' => (string)$result->AuthCode,
                'acq_ref_data' => (string)$result->ProcessorExtraData1,
                'process_data' => (string)$result->ProcessorExtraData2,
                'token' => (string)$result->CardToken,
                'response_code' => (int)$result->ResultCode,
                'status' => (string)$result->ResultCode,
                'status_message' => (string)$result->Message,
                'display_message' => (string)$result->Message,
                'signature_data' => (string)$result->SignatureData,
                'memo' => 'ForeUP v.1.0'
            );
            // Email these ones to me since they're our last problem payments.

            $html = '';
            $html .= '<p>--------------------------------------- Original Data -------------------------------------</p>';
            foreach($payment_data as $index => $data) {
                $html .= "<p>{$index} => {$data} </p>";
            }
            $html .= '<p>-------------------------------------- Updated Data ----------------------------------------</p>';
            foreach($result as $index => $data) {
                $html .= "<p>{$index} => {$data} </p>";
            }

            send_sendgrid(
                'jhopkins@foreup.com',
                'Bad EMV Payment',
                $html,
                'jhopkins@foreup.com',//$this->session->userdata('course_email'),
                'ForeUP EMV'
            );

            return $this->update_credit_card_payment($payment_data['invoice'], $new_payment_data);
        }
        else {
            $verify_results = $this->verify($payment_data);

            if ($verify_results && (string)$verify_results->Status != '') {
                //Update credit card payment data
                $new_payment_data = array(
                    'tran_type' => (string)$verify_results->TranType,
                    'amount' => (string)$verify_results->Amount,
                    'auth_amount' => (string)$verify_results->AuthAmount,
                    'card_type' => (string)$verify_results->CardType,
                    'masked_account' => (string)$verify_results->MaskedAccount,
                    'cardholder_name' => (string)$verify_results->CardholderName,
                    'ref_no' => (string)$verify_results->RefNo,
                    'operator_id' => (string)$verify_results->OperatorID,
                    'terminal_name' => (string)$verify_results->TerminalName,
                    'trans_post_time' => (string)$verify_results->TransPostTime,
                    'auth_code' => (string)$verify_results->AuthCode,
                    'voice_auth_code' => (string)$verify_results->VoiceAuthCode,
                    'acq_ref_data' => (string)$verify_results->AcqRefData,
                    'process_data' => (string)$verify_results->ProcessData,
                    'token' => (string)$verify_results->Token,
                    'response_code' => (int)$verify_results->ResponseCode,
                    'status' => (string)$verify_results->Status,
                    'status_message' => (string)$verify_results->StatusMessage,
                    'display_message' => (string)$verify_results->DisplayMessage,
                    'avs_result' => (string)$verify_results->AvsResult,
                    'cvv_result' => (string)$verify_results->CvvResult,
                    'tax_amount' => (string)$verify_results->TaxAmount,
                    'avs_address' => (string)$verify_results->AVSAddress,
                    'avs_zip' => (string)$verify_results->AVSZip,
                    'payment_id_expired' => (string)$verify_results->PaymendIDExpired,
                    'customer_code' => (string)$verify_results->CustomerCode,
                    'memo' => (string)$verify_results->Memo
                );

                return $this->update_credit_card_payment($payment_data['invoice'], $new_payment_data);
            }
        }

        return false;
    }

    function mark_checked($invoice) {
        $this->db->query("UPDATE foreup_sales_payments_credit_cards SET success_check_count = success_check_count + 1 WHERE invoice = $invoice");
    }

    function get_cancelled_mercury_payments() {
        $this->db->from('mercury_cancelled_payments');
        $this->db->where('cancel_failed', 0);
        $this->db->where('cancel_time', '0000-00-00 00:00:00');
        $this->db->where('set_to_cancel_time >', gmdate('Y-m-d H:i:s', strtotime(' -35 minutes')));
        $this->db->where('set_to_cancel_time <', gmdate('Y-m-d H:i:s', strtotime(' -10 minutes')));

        return $this->db->get()->result_array();
    }

    function set_to_cancel($payment_id, $already_cancelled = false) {
        $data = array(
            'payment_id'=>$payment_id,
            'set_to_cancel_time'=>gmdate("Y-m-d H:i:s"),
            'mercury_id'=>$this->config->item('mercury_id'),
            'mercury_password'=>$this->config->item('mercury_password')
        );

        if (!empty($already_cancelled)) {
            $data['cancel_time'] = gmdate('Y-m-d H:i:s');
        }

        $this->db->insert('mercury_cancelled_payments', $data);
    }

    function get_payments_without_sale() {
        $this->db->from('sales_payments_credit_cards as pcc');
        $this->db->join('sales_payments as pc', 'pcc.invoice = pc.invoice_id', 'left');
        $this->db->where('initiation_time >', gmdate('Y-m-d H:i:s', strtotime('-35 minutes')));
        $this->db->where('initiation_time <', gmdate('Y-m-d H:i:s', strtotime('-5 minutes')));
        $this->db->where("amount_refunded = 0");
        $this->db->where('mercury_id !=', '');
        $this->db->where('status', 'Approved');
        $this->db->where('tran_type', 'Sale');
        $this->db->where('sale_id IS NULL', null, false);

        return $this->db->get()->result_array();
    }

    function verify($payment_data) {

        if ($payment_data['mercury_id'] != '') {

            $this->load->library('Hosted_checkout_2');
            $HC = new Hosted_checkout_2();
            $HC->set_merchant_credentials($payment_data['mercury_id'], $payment_data['mercury_password']);
            $HC->set_payment_id($payment_data['payment_id']);
            $HC->set_invoice($payment_data['invoice']);

            return $HC->verify_payment();
        }

        return false;
    }

    function void($payment_id) {
        $this->load->library('Hosted_checkout_2');

        $payment_data = $this->get_info(false, $payment_id);

        if (empty($payment_data)) {
            return false;
        }

        if (empty($payment_data['token'])) {
            // Verify the payment if it hasn't been verified yet.
            $this->check($payment_data);
        }

        // Try to cancel the payment
        $HC = new Hosted_checkout_2();
        $success = $HC->cancel_payment($payment_data, false);

        if ($success) {
            // Update the payments table to reflect that we've already voided the transaction
            $returned_info = array(
                'token_used' => 1,
                'amount_refunded' => $payment_data['auth_amount']
            );

            $this->update_credit_card_payment($payment_data['invoice'], $returned_info);
        }

        return $success;
    }

    function update_credit_card_payment($invoice, $payment_info){
        $this->db->where('invoice', $invoice);
        return $this->db->update('sales_payments_credit_cards', $payment_info);
    }

    public function is_payment_floating($invoice)
    {
        $result = $this->db->query("
            SELECT
            COUNT(*) as count
            FROM `foreup_sales_payments_credit_cards`
            
            LEFT JOIN foreup_sales_payments ON `foreup_sales_payments_credit_cards`.invoice = foreup_sales_payments.invoice_id
            LEFT JOIN foreup_table_payments ON foreup_table_payments.credit_card_invoice_id = foreup_sales_payments_credit_cards.invoice
            LEFT JOIN foreup_courses ON foreup_sales_payments_credit_cards.course_id = foreup_courses.course_id
            LEFT JOIN foreup_pos_cart ON foreup_sales_payments_credit_cards.cart_id = foreup_pos_cart.cart_id
            WHERE
            invoice = $invoice AND foreup_courses.course_id = {$this->session->userdata('course_id')} AND
            (foreup_sales_payments_credit_cards.`status` = 'Approved' OR foreup_sales_payments_credit_cards.`status` ='success') AND
            `amount_refunded` = 0 AND
            foreup_sales_payments.sale_id IS NULL AND
            auth_amount > 0 AND
            foreup_sales_payments_credit_cards.tran_type = 'Sale' AND
            (foreup_sales_payments_credit_cards.status = 'Approved' OR foreup_sales_payments_credit_cards.status = 'success' ) AND
            cancel_time = '0000-00-00 00:00:00' AND
            token_used = 0 AND
            foreup_sales_payments_credit_cards.mercury_id != 88430119384 AND
            ((foreup_sales_payments_credit_cards.mercury_id != '' AND  foreup_sales_payments_credit_cards.mercury_password != '')) AND
            foreup_table_payments.sale_id IS NULL 
        ")->row();
        return $result->count != 0;
    }

    public function mark_to_be_voided($invoice)
    {
        return $this->db->where("invoice",$invoice)
            ->update("sales_payments_credit_cards",[
                "to_be_voided"=>1
            ]);
    }

    /**
     * @param $payment_data
     * @return bool
     */
    private function can_use_payment($payment_data)
    {
        return empty($payment_data['status']) && $payment_data['cancel_time'] == "0000-00-00 00:00:00";
    }
}