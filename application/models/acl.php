<?php
class Acl extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->model("employee");
		$this->load->model("acl_roles");

	}

	public function activate_new_permission($all_courses = false)
	{
		if($all_courses){
		    $this->db->select('person_id, course_id, user_level')
                ->from('employees');
            $employees = $this->db->get();
        }else{
            $employees = $this->Employee->get_all();
        }

		foreach($employees->result_array() as $employee){
			$this->acl_roles->set_acl_role($employee['person_id'],$employee['user_level']);
		}
	}

	public function get_all_permissions()
	{
		$namespace = 'fu\acl\Permissions';

		// Relative namespace path
		$namespaceRelativePath = str_replace('\\', DIRECTORY_SEPARATOR, $namespace);

		// Include paths
		$includePathStr = get_include_path();
		$classArr = array();
		$includePath = APPPATH."libraries";
		$path = $includePath . DIRECTORY_SEPARATOR . $namespaceRelativePath;
		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
		$directoryPath = preg_quote($includePath.'/'.$namespaceRelativePath,'/');
		foreach($objects as $name => $object){
			if (preg_match('/^'.$directoryPath.'(?<class>[^.].+)\.php$/', $name, $matches)) {
				$className = $namespace . str_replace("/","\\",$matches['class']);
				$classArr[] = $className;
			}
		}

		return $classArr;
	}

    public function get_user_permissions_array($person_id = false){
       
		$acl = new \fu\acl\Acl($person_id);

        $this->load->model("acl");
        $allPermissions = $this->get_all_permissions();
        $permissionsToReturn = [];

        foreach($allPermissions as $permission){
            $permissionName = "\\".$permission;
            $initializedClass = new $permissionName([]);
            $actions = $initializedClass->getAvailableActions();
            foreach($actions as $action){
                $initializedClass->setActions([$action]);
                $permissionsToReturn[str_replace('\\', '/', $permission).":".$action] = $acl->can($initializedClass);
            }
        }

        return $permissionsToReturn;       
    }
}