<?php
class Charge_attempt extends CI_Model
{
	function save($credit_card_id, $invoice_id, $amount, $date, $success, $status_message, $credit_card_charge_id, $employee_id = 0) {
		$data = array(
			'credit_card_id' =>$credit_card_id,
			'invoice_id'=>$invoice_id,
			'amount'=>$amount,
			'date'=>$date,
			'success'=>$success,
			'status_message'=>$status_message,
			'credit_card_charge_id'=> $credit_card_charge_id,
			'employee_id' => $employee_id
			);
			
		if ($this->db->insert('invoice_charge_attempts', $data)) {
			return true;
		}
		else {
			return false;
		}
		
	}

	function get_all($invoice_id = false, $credit_card_id = false) {
		$where_sql = ' WHERE 1';
		if ($invoice_id)
			$where_sql = " WHERE invoice_id = $invoice_id";
		if ($credit_card_id)
			$where_sql = " WHERE ca.credit_card_id = $credit_card_id";
		
		$query = $this->db->query("SELECT * FROM foreup_invoice_charge_attempts AS ca 
			LEFT JOIN foreup_customer_credit_cards AS cc ON ca.credit_card_id = cc. credit_card_id
			$where_sql
			");
		return $query->result_array();
	}
	
	function get_report_data($month) {
		$where_sql = ' WHERE 1';
		
		$start = date("Y-m-01 00:00:00", strtotime($month));
		$end = date("Y-m-01 00:00:00", strtotime($start .' + 31 days'));
		$where_sql .= " AND (ca.date > '$start' AND ca.date < '$end') ";
		$where_sql .= " AND cc.course_id = ".$this->session->userdata('course_id');
		
		$query = $this->db->query("SELECT c.first_name AS customer_first_name, c.last_name AS customer_last_name, e.first_name AS employee_first_name, 
			e.last_name AS employee_last_name, invoice_number, cc.*, ca.* FROM foreup_invoice_charge_attempts AS ca 
			LEFT JOIN foreup_customer_credit_cards AS cc ON ca.credit_card_id = cc. credit_card_id
			LEFT JOIN foreup_invoices AS i ON i.invoice_id = ca.invoice_id
			LEFT JOIN foreup_people AS c ON c.person_id = cc.customer_id
			LEFT JOIN foreup_people AS e ON e.person_id = ca.employee_id
			$where_sql
			ORDER BY ca.`date`
			");
			//echo $this->db->last_query();
		$results = $query->result_array();
									
		foreach ($results as $result) {
			$return_html .= "
				<tr>
					<td class='center_alignment'>".date('Y-m-d', strtotime($result['date']))."</td>
					<td>{$result['customer_first_name']} {$result['customer_last_name']}</td>
					<td class='center_alignment'>{$result['invoice_number']}</td>
					<td class='center_alignment'>{$result['card_type']} ".str_replace(array('x', '*'),'', $result['masked_account'])."</td>
					<td class='currency_alignment'>$".$result['amount']."</td>
					<td class='center_alignment'>{$result['success']}</td>
					<td class='center_alignment'>{$result['status_message']}</td>
					<td>{$result['employee_first_name']} {$result['employee_last_name']}</td>
				</tr>
				";
		}
		echo json_encode(array('return_html'=>$return_html));
	}
}
	