<?php
class Mobile_app_module_permissions extends CI_Model
{
    public function get($course_id) {
        $this->db->from('mobile_app_module_permissions');
        $this->db->where('course_id', $course_id);
        return $this->db->get()->result_array();
    }

    public function delete_all($course_id) {
        $this->db->delete('mobile_app_module_permissions', array('course_id'=>$course_id));
    }

    public function save($data) {
        $this->db->insert('mobile_app_module_permissions', $data);
    }

}