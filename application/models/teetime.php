<?php
class Teetime extends CI_Model
{
	public $teetime_price_classes = array();
    public $is_holiday = -1;

	/*
	Determines if a given teetime_id is an teetime
	*/
	function exists($teetime_id, $teesheet_id = false)
	{
		$this->db->from('teetime');
		$this->db->where("TTID", $teetime_id);
		$this->db->limit(1);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}

	/*
	Returns all the teetimes
	*/
	function get_all($limit=10000, $offset=0)
	{
		$course_id = '';
		if (!$this->permissions->is_super_admin())
			$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('teetime');
		$this->db->where("deleted = 0 $course_id");
		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}

    function get_squeeze_times($tee_sheet_id, $date, $start, $end, $count_only = false) {
        $sz = $start < 1000 ? '0' : '';
        $ez = $end < 1000 ? '0' :'';
        $this->db->from('teetime');
        $this->db->where('status !=', 'deleted');
        $this->db->where('teesheet_id', $tee_sheet_id);
        $this->db->where('start >', date('YmdHi', strtotime($date.' '.$sz.$start)) - 1000000);
        $this->db->where('start <', date('YmdHi', strtotime($date.' '.$ez.$end)) - 1000000);

        if (!$count_only) {
            return $this->db->get()->result_array();
        }
        else {
            return $this->db->get()->num_rows();
        }
    }
	function get_teetime_stats()
	{
		$results = $this->db->query("SELECT count(TTID) AS total, COUNT(DISTINCT foreup_teesheet.course_id) AS booking_courses, SUM(IF(details LIKE '%online booking%', 1, 0)) AS online FROM foreup_teetime JOIN foreup_teesheet ON foreup_teesheet.teesheet_id = foreup_teetime.teesheet_id WHERE TTID LIKE '____________________' AND status != 'deleted'");
		return $results->result_array();
	}
	function get_teetime_course_stats()
	{
		$return_array = array();
		$this->load->model('Communication');
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-'.$this->Communication->days_in_month(date('m')));
		$pm_start_date = date('Y-m-01', strtotime($start_date.' -1 month'));
		$pm_end_date = date('Y-m-'.$this->Communication->days_in_month(date('m', strtotime($start_date.' -1 month'))), strtotime($start_date.' -1 month'));
		$results = $this->db->query("SELECT foreup_courses.name as name, foreup_courses.course_id AS course_id, count(TTID) AS total, SUM(IF(details LIKE '%online booking%', 1, 0)) AS online,
			sum(IF (date_booked >= '$start_date' AND date_booked <= '$end_date' AND details LIKE '%online booking%', 1, 0 )) AS online_this_month,
			sum(IF (date_booked >= '$pm_start_date' AND date_booked <= '$pm_end_date' AND details LIKE '%online booking%', 1, 0 )) AS online_last_month,
			sum(IF (date_booked >= '$start_date' AND date_booked <= '$end_date', 1, 0 )) AS reserved_this_month,
			sum(IF (date_booked >= '$pm_start_date' AND date_booked <= '$pm_end_date', 1, 0 )) AS reserved_last_month
			FROM foreup_teetime
			JOIN foreup_teesheet ON foreup_teesheet.teesheet_id = foreup_teetime.teesheet_id
			JOIN foreup_courses ON foreup_courses.course_id = foreup_teesheet.course_id
			WHERE TTID LIKE '____________________' AND status != 'deleted'
			GROUP BY foreup_courses.course_id ORDER BY foreup_courses.name");

		foreach($results->result_array() as $result)
			$return_array[$result['course_id']] = $result;

		return $return_array;
	}
	function get_play_length_data($start_date, $end_date, $teesheet_id)
	{
		$results = $this->db->query("SELECT teed_off_time, turn_time, finish_time, TIMESTAMPDIFF(MINUTE, teed_off_time, turn_time) AS half_time, TIMESTAMPDIFF(MINUTE, teed_off_time, finish_time) AS total_time FROM foreup_teetime
			WHERE teed_off_time >= '$start_date 00:00:00' AND teed_off_time <= '$end_date 23:59:59' AND TTID LIKE '____________________' AND status != 'deleted'
			AND teesheet_id = $teesheet_id AND (turn_time != '0000-00-00 00:00:00' OR finish_time != '0000-00-00 00:00:00')
			ORDER BY teed_off_time");

		return $results->result_array();
	}
	function count_all($last_week=false)
	{/*
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('teetime');
		$this->db->where("deleted = 0 $course_id");
        return $this->db->count_all_results();*/
		$this->db->from('teetime');
		$this->db->join('teesheet', 'teetime.teesheet_id=teesheet.teesheet_id');
		if (!$this->permissions->is_super_admin())
			$this->db->where('course_id', $this->session->userdata('course_id'));
		if($last_week === true)
			$this->db->where('YEARWEEK(date_booked) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)');
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular teetime
	*/
	function get_info($teetime_id, $include_title = false)
	{
		//Get Tee Sheet title also
		if ($include_title)
			$this->db->select('teetime.*, teesheet.title AS tee_sheet_title');
		$this->db->from('teetime');
		if ($include_title)
			$this->db->join('teesheet', 'teetime.teesheet_id = teesheet.teesheet_id', 'LEFT');
		$this->db->where("TTID = '$teetime_id'");
		$this->db->limit(1);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $teetime_id is NOT a teetime
			$teetime_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('teetime');

			foreach ($fields as $field)
			{
				$teetime_obj->$field='';
			}

			return $teetime_obj;
		}
	}
	function switch_sides($teetime_id)
	{
		$main_id = substr($teetime_id, 0, 20);
		$this->db->query("UPDATE foreup_teetime SET side = CASE WHEN side = 'front' THEN 'back' ELSE 'front' END WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') LIMIT 2");
		//echo $this->db->last_query();
		echo json_encode(array());
	}
	function split_by_time($teetime_id)
	{
		// FIND HOW MANY TIME SLOTS THIS SPANS, THEN SAVE A DUPLICATE TEE TIME FOR EACH SLOT
		$main_id = substr($teetime_id, 0, 20);
		$tti = $tee_time_info = $this->get_info($main_id);
        $tee_sheet_info = $this->teesheet->get_info($tti->teesheet_id);
        $increment = $tee_sheet_info->increment;//$this->session->userdata('increment');
		$rtti = $reround_tee_time_info = $this->get_info($main_id.'b');

        $this->load->model('Increment_adjustment');

		// CALCULATE HOW MANY TIME SLOTS
		$reservation_minutes = (strtotime($tti->end + 1000000) - strtotime($tti->start + 1000000)) / 60;
		$tee_time_count = ceil($reservation_minutes / $increment);
		if ($tee_time_count > 1)
		{
			$new_reservation_array = array();
			// COPY DATA FROM OLD RESERVATIONS
			$new_tee_time_data = (array) $tti;
			$new_reround_tee_time_data = (array) $rtti;
            $num_of_players = $tti->player_count;
            $duration = $tti->duration;
            $used_player_count = 0;
            $optimum_count = $duration * 4;
            $starting_player_count = floor($num_of_players / $duration);
			for ($i = 0; $i < $tti->duration; $i++)
			{
                if ($num_of_players >= $optimum_count) {
                    if ($i == 0) {
                        // First time slot gets any extra if there are too many players
                        $player_count = $num_of_players - (($duration - 1) * 4);
                    }
                    else {
                        $player_count = 4;
                    }
                }
                else {
                    if ($num_of_players - ($duration * $starting_player_count) - $used_player_count > 0) {
                        $player_count = $starting_player_count + 1;
                        $used_player_count++;
                    }
                    else {
                        $player_count = $starting_player_count ? $starting_player_count : 1;
                    }
                }
				// GENERATE NEW TEE TIME ID
				$tee_time_id = $this->generate_id();
				// GENERATE NEW START AND END TIMES
                $new_start = $this->Increment_adjustment->get_slot_time($tti->start + 1000000, $tti->teesheet_id,  $i);
                $r_new_start = $this->Increment_adjustment->get_slot_time($rtti->start + 1000000, $rtti->teesheet_id, $i);
                $new_end = $this->Increment_adjustment->get_slot_time($tti->start + 1000000, $tti->teesheet_id, $i + 1);
                $r_new_end = $this->Increment_adjustment->get_slot_time($rtti->start + 1000000, $rtti->teesheet_id, $i + 1);
				// ASSIGN IDs AND TIMES
				$new_tee_time_data['TTID'] = $tee_time_id;
				$new_reround_tee_time_data['TTID'] = $tee_time_id.'b';
				$new_tee_time_data['start'] = $new_start;
				$new_tee_time_data['end'] = $new_end;
				$new_reround_tee_time_data['start'] = $r_new_start;
				$new_reround_tee_time_data['end'] = $r_new_end;

				$new_tee_time_data['start_datetime'] = \fu\Teetime::convertTeetimeToDatetime($new_start);
				$new_tee_time_data['end_datetime'] = \fu\Teetime::convertTeetimeToDatetime($new_end);
                $new_tee_time_data['duration'] = 1;
                $new_tee_time_data['player_count'] = $player_count;
                $new_tee_time_data['last_updated'] = gmdate("Y-m-d H:i:s");
				$new_reround_tee_time_data['start_datetime'] = \fu\Teetime::convertTeetimeToDatetime($r_new_start);
				$new_reround_tee_time_data['end_datetime'] = \fu\Teetime::convertTeetimeToDatetime($r_new_end);
                $new_reround_tee_time_data['duration'] =1;
                $new_reround_tee_time_data['player_count'] = $player_count;
                $new_reround_tee_time_data['last_updated'] = gmdate("Y-m-d H:i:s");
				// INSERT NEW TEE TIMES INTO DATABASE
				$this->db->insert('teetime', $new_tee_time_data);
				$this->db->insert('teetime', $new_reround_tee_time_data);
				// PREP RESERVATIONS FOR RETURN
				$new_reservation_array[] = $this->add_teetime_styles($new_tee_time_data);
				$new_reservation_array[] = $this->add_teetime_styles($new_reround_tee_time_data);
			}
			// DELETE ORIGINAL TEE TIME
			$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
			$this->db->limit(2);
			$this->db->update('teetime', array('status'=>'deleted'));

			$tti = (array) $tti;
			$rtti = (array) $rtti;
			$tti['status'] = 'deleted';
			$rtti['status'] = 'deleted';
			$new_reservation_array[] = $this->add_teetime_styles($tti);
			$new_reservation_array[] = $this->add_teetime_styles($rtti);

			return $new_reservation_array;
		}
		else
			return array();
	}

	function mark_as_teedoff($teetime_id, $tee_sheet_id = false)
	{
		$main_id = substr($teetime_id, 0, 20);
		$date = date('Y-m-d H:i:s');
		$teesheet_id = $tee_sheet_id ? $tee_sheet_id : $this->session->userdata('teesheet_id');

		$this->db->query("UPDATE foreup_teetime
			SET status = CASE
				WHEN `status` = 'deleted' THEN 'deleted'
				ELSE 'teed off'
			END,
			teed_off_time = ".$this->db->escape($date)."
			WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");
		//echo $this->db->last_query();

		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(2);
		$result = $this->db->get();
		$teetimes = $result->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	function mark_as_turned($teetime_id, $tee_sheet_id = false)
	{
		$main_id = substr($teetime_id, 0, 20);
		$date = date('Y-m-d H:i:s');
		$teesheet_id = $tee_sheet_id ? $tee_sheet_id : $this->session->userdata('teesheet_id');
		$this->db->query("UPDATE foreup_teetime SET turn_time = ".$this->db->escape($date)." WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");
		//echo $this->db->last_query();
		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(2);
		$teetimes = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	function mark_as_finished($teetime_id, $tee_sheet_id = false)
	{
		$main_id = substr($teetime_id, 0, 20);
		$date = date('Y-m-d H:i:s');
		$teesheet_id = $tee_sheet_id ? $tee_sheet_id : $this->session->userdata('teesheet_id');

		$this->db->query("UPDATE foreup_teetime SET finish_time = ".$this->db->escape($date)." WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");
		//echo $this->db->last_query();
		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(2);
		$teetimes = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	function zero_teed_off($teetime_id, $tee_sheet_id = false)
	{
		$main_id = substr($teetime_id, 0, 20);
		$teesheet_id = $tee_sheet_id ? $tee_sheet_id : $this->session->userdata('teesheet_id');
		$this->db->query("UPDATE foreup_teetime SET teed_off_time = '0000-00-00 00:00:00' WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");

		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(2);
		$teetimes = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	function zero_turn($teetime_id, $tee_sheet_id = false)
	{
		$main_id = substr($teetime_id, 0, 20);
		$teesheet_id = $tee_sheet_id ? $tee_sheet_id : $this->session->userdata('teesheet_id');
		$this->db->query("UPDATE foreup_teetime SET turn_time = '0000-00-00 00:00:00' WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");

		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('status !=', 'deleted');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(2);
		$teetimes = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	function update_credit_card($teetime_id, $cc_data)
	{
		$main_id = substr($teetime_id, 0, 20);
		$teesheet_id = $this->session->userdata('teesheet_id');
		return $this->db->query("UPDATE foreup_teetime SET credit_card_id = '{$cc_data['credit_card_id']}' WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");
	}
	function check_in($teetime_id, $count, $tee_sheet_id = false, &$message = '',$selected_people_to_checkin = [])
	{
		$main_id = substr($teetime_id, 0, 20);
		$teesheet_id = $tee_sheet_id ? $tee_sheet_id : $this->session->userdata('teesheet_id');

        if ($this->config->item('current_day_checkins_only')) {
            // Get tee time
            $tee_time_info = $this->get_info($teetime_id);
            $tt_date = date('Y-m-d', strtotime($tee_time_info->start + 1000000));
            $todays_date = date('Y-m-d');
            if ($tt_date != $todays_date) {
                // If it doesn't belong to today, then prevent action
                $message = "You can only check in players on today's date.";
                return array();
            }
        }

		$this->db->query("UPDATE foreup_teetime SET paid_player_count = paid_player_count + $count WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND teesheet_id = {$teesheet_id} LIMIT 2");

		$this->db->from('foreup_teetime');
		$this->db->where("(TTID = '{$main_id}' OR TTID = '{$main_id}b')");
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->where('status !=', 'deleted');
		$this->db->limit(2);
		$teetimes = $this->db->get()->result_array();

		if(!$this->config->item('sales'))
		{
			$teetime = $this->mark_players_as_paid($teetime_id, $count,$selected_people_to_checkin);
		}


		$json_ready_events = array();
		foreach ($teetimes as $teetime)
			$json_ready_events[] = $this->add_teetime_styles($teetime);

		return $json_ready_events;
	}
	/*
	Checks whether an existing time is still available before saving
	*/
	function check_availability($teetime_data, $return_events = false)
	{
		if(empty($teetime_data['start']))
			return false;
		if (isset($teetime_data['status']) && $teetime_data['status'] == 'deleted')
			return true;
		//added this to allow the API to use this function as well as online booking
		if (!$teetime_data['teesheet_id']) {
			$teetime_data['teesheet_id'] = $this->session->userdata('teesheet_id');
		}

		//Check if teetimes spots are available
		$this->db->select('player_count, status, side, TTID, type, title');
		$this->db->from('teetime');
		$this->db->where('teesheet_id', $teetime_data['teesheet_id']);
		$this->db->where("start <= {$teetime_data['start']} AND end > {$teetime_data['start']}");
        if (!empty($teetime_data['pending_reservation_id'])) {
            $this->db->where('TTID != ', $teetime_data['pending_reservation_id']);
            $this->db->where('TTID != ', $teetime_data['pending_reservation_id'].'b');
        }
		$result = $this->db->get();
		//echo $this->db->last_query();
		$result = $result->result_array();

		$total_players = 0;
		$event_blocking = false;
        $blocking_events = array();
		foreach ($result as $event) {
			if ($event['status'] == 'deleted' ||
				(isset($teetime_data['TTID']) && $event['TTID'] == $teetime_data['TTID']) ||
				$event['side'] != $teetime_data['side']) {

			}
			else if ($event['type'] != 'teetime') {
				$event_blocking = true;
                $blocking_events[] = $event;
			}
			else {
				$total_players += $event['player_count'];
                $blocking_events[] = $event;
			}
		}
		if ($return_events) {
            return $blocking_events;
        }
		else if ($event_blocking OR ($total_players > 0 && $total_players + $teetime_data['player_count'] > 4))
		{
			return false;
		}
		else
		{
			return true;
		}

	}
	function update_purchased_teetime()
	{
		$teetime_id = $this->session->userdata('teetime_to_purchase');
		$teetime_info = $this->get_info($teetime_id);
		$players = $this->session->userdata('players_to_purchase');
		$carts = $this->session->userdata('carts_to_purchase');
		$invoice_id = $this->session->userdata('invoice_id');
		$json = array();
		$cart_count = ($carts == 1)?$players:0;
		$data = array(
			'teetime_id'=>$teetime_id,
			'teesheet_id'=>$teetime_info->teesheet_id,
			'start'=>$teetime_info->start,
			'start_datetime'=>$teetime_info->start_datetime,
			'end'=>$teetime_info->end,
			'holes'=>$teetime_info->holes,
			'date_booked'=>$teetime_info->date_booked,
			'booker_id'=>$teetime_info->booker_id,
			'player_count'=>$players,
			'paid_player_count'=>$players,
			'carts'=>$cart_count,
			'paid_carts'=>$cart_count,
			'booking_source'=>'online',
			'details'=>'Reserved and Paid For using online booking @ '.date('g:ia n/j T')
		);

		$this->save($data, $teetime_id, $json);
		//echo $this->db->last_query();
		$data['invoice_id'] = $invoice_id;
		unset($data['paid_player_count']);
		unset($data['paid_carts']);
		unset($data['booking_source']);
		unset($data['details']);
		unset($data['TTID']);
		//Save all original data in our teetimes_bartered table
		return $this->db->insert('teetimes_bartered', $data);
		//echo $this->db->last_query();
	}

	function save_bartered_teetime($teetime_id, $purchased_players, $purchased_carts, $invoice_id, $details = false){

		$teetime_info = $this->get_info($teetime_id);

		$carts = 0;
		if($purchased_carts){
			$carts = $purchased_players;
		}

		// Update tee time with number of paid players/carts
		$carts = 0;
		if($purchased_carts){
			$carts = $purchased_players;
		}

		// Update tee time with paid information
		$updated_teetime = array(
			'player_count' => $purchased_players,
			'carts' => $carts,
			'paid_player_count' => $purchased_players,
			'paid_carts' => $carts
		);

		if(!empty($details)){
			$updated_teetime['details'] = $details;
		}

		$this->db->update('teetime', $updated_teetime, array('TTID' => $teetime_id));
		$this->db->update('teetime', $updated_teetime, array('TTID' => $teetime_id.'b'));

		$bartered_teetime = array(
			'teetime_id' => $teetime_id,
			'teesheet_id' => $teetime_info->teesheet_id,
			'start' => $teetime_info->start,
			'start_datetime' => $teetime_info->start_datetime,
			'end' => $teetime_info->end,
			'player_count' => $purchased_players,
			'holes' => $teetime_info->holes,
			'carts' => $carts,
			'date_booked' => $teetime_info->date_booked,
			'booker_id' => $teetime_info->booker_id,
			'invoice_id' => $invoice_id
		);

		// Save bartered tee time data
		$this->db->insert('teetimes_bartered', $bartered_teetime);
		return $this->db->affected_rows();
	}

	/*
	Adjust for the 7/8 split
	*/
	function clean_time(&$time, $base_time = 0, $increment = false)
	{
		if(!$increment){
			$increment = $this->session->userdata('increment');
		}

		if ($increment == 7.5)
		{
			$minutes = floor(ceil($time + $base_time)/100)*60+ceil($time + $base_time)%100;
			if (fmod($time, 1) == 0) {
				if (($minutes + 1)%15 == 0) {
					$time = $time + 1;
				}
				else if ($minutes%15 == 8) {
					$time = $time - 1;
				}
			}
			else if ($minutes%15 != 0)
				$time = floor($time);
			else
				$time = ceil($time);

            if ($time%100 >59)
                $time += 40;
		}
	}

	function adjust_7_8_time($time){
		if (fmod($time, 15) != 0){
			$time = floor($time);
		}else{
			$time = ceil($time);
		}
		return $time;
	}

	/*
	Inserts or updates a teetime
	*/
	function save(&$teetime_data,$teetime_id=false, &$json_ready_events=array(), $online_booking = false, $course_id = false)
	{
        $this->load->model('Tee_time_log');

        $split = isset($teetime_data['split']) ? $teetime_data['split'] : false;
        $paste = isset($teetime_data['paste']) ? $teetime_data['paste'] : false;
        $template = $this->input->get('template_tee_sheet_id');
        $new_tee_time = !empty($teetime_data['new_tee_time']);
        if ($new_tee_time) {
            $teetime_data['player_count'] = 4;
        }
        if($teetime_data['type'] == "closed"){
	        $teetime_data['player_count'] = 0;
        }
        unset($teetime_data['split']);
        unset($teetime_data['paste']);
        unset($teetime_data['new_tee_time']);
        // CHECK AVAILABILITY IF WE'RE COMING FROM ONLINE BOOKING
		if ($online_booking)
		{
			$lock = new \fu\locks\teetime_slot([
				"teetime_start"=>$teetime_data['start'],
				"teesheet_id"=>$teetime_data['teesheet_id']
			]);
			$lock->get_lock();
			if (!$this->check_availability($teetime_data))
				return array('success'=>false, 'message'=>'error', 'send_confirmation'=>'');
		}
		// IF WE DON'T ALREADY HAVE AN ID, ASSIGN ONE
		if ($teetime_id) {
            $cur_teetime_data = $this->get_info($teetime_id);
            if ($cur_teetime_data->type == 'pending_reservation' && (!isset($teetime_data['status']) || (isset($teetime_data['status']) && $teetime_data['status']!="deleted"))) {
                $teetime_data['status'] = '';
            }
        }
		else
			$teetime_id = $this->generate_id('tt');

		$teetime_data['TTID'] = $teetime_id;

		// ASSIGN ID TO SECOND TEE TIME
		$second_id = $teetime_id.'b';
		if (strlen($teetime_id) > 20)
			$second_id = substr($teetime_id, 0, 20);


		// FROM ONLINE BOOKING OR API, WE PASS IN COURSE ID, OTHERWISE GET INFO FROM SESSION
        if (empty($teetime_data['teesheet_id'])) {
            $teetime_data['teetime_id'] = $this->session->userdata('teesheet_id');
        }
        $teesheet_data = $this->teesheet->get_info($teetime_data['teesheet_id']);
		$teesheet_holes = 18;

        $frontnine = $teesheet_data->frontnine;

        if ($course_id) {
            $is_simulator = 0;
            if(!empty($teesheet_data->is_simulator)){
                $is_simulator = $teesheet_data->is_simulator;
            }
            $teesheet_holes = $teesheet_data->holes;
        }
        else
        {
            $is_simulator = $this->session->userdata('is_simulator');
        }



        if ((isset($teetime_data['booking_source']) && ($teetime_data['booking_source'] == 'online' || $teetime_data['booking_source'] == 'api')) || $split == true) //Not yet adjusting for simulators
		{
			$second_data = $teetime_data;
			$teetime_data['TTID'] = $teetime_id;
			$second_data['TTID'] = $second_id;
			$second_data['side'] = $second_data['side'] == 'front' ? 'back' : 'front';
			if ($teesheet_data->holes == 9 || $teesheet_holes == 9)
				$second_data['side'] = 'front';
			$second_data['start'] += $frontnine;
			$second_data['end'] += $frontnine;
			if ($second_data['start']%100 >59)
				$second_data['start'] += 40;
			if ($second_data['end']%100 >59)
				$second_data['end'] += 40;
            if ($is_simulator || $teetime_data['holes'] == 9)
                $second_data['status'] = 'deleted';
            else {
	            if (isset($teetime_data['status']))
		            $second_data['status'] = $teetime_data['status'];
	            else
	                $second_data['status'] = '';

            }

    	} else {

			$second_data = $teetime_data;
			$second_data['TTID'] = $second_id;
			if (!isset($teetime_data['status']) && isset($teetime_data['holes']) && $teetime_data['holes'] == 18)
				$second_data['status'] = $cur_teetime_data->status;


			// Erasing second tee time if there is one since we're only playing 9
			if (isset($teetime_data['holes']) && $teetime_data['holes'] == 9) {
				if (strlen($teetime_id) > 20)
					$teetime_data['status'] = 'deleted';
				else
					$second_data['status'] = 'deleted';
			}


			//Identify the second side
			$second_side = 'back';
            if ((isset($cur_teetime_data->side) && $cur_teetime_data->side =='back') ||
				(isset($teetime_data['side']) && $teetime_data['side'] == 'back') ||
	            $teesheet_data->holes == 9)
				$second_side = 'front';

            /*
             * Calculate the reround time
             */
			if (isset($second_data['type']) && $second_data['type'] == 'shotgun') {
				$second_data['start'] = $teetime_data['start'];
				$second_data['end'] = $teetime_data['end'];
			} else {
				$start = $end = '';
				if (isset($teetime_data['start']))
					$start = $teetime_data['start'];
				else if (isset($cur_teetime_data['start']))
					$start = $cur_teetime_data['start'];
				if (isset($teetime_data['end']))
					$end = $teetime_data['end'];
				else if (isset($cur_teetime_data['end']))
					$end = $cur_teetime_data['end'];
				$second_data['start'] = $start + $frontnine;
				$second_data['end'] = $end + $frontnine;

				if ((($start % 100) + ($frontnine % 100)) > 59 )//if ($second_data['start']%100 >59)
					$second_data['start'] += 40;
                if ((($end % 100) + ($frontnine % 100)) > 59 )//if ($second_data['end']%100 >59)
					$second_data['end'] += 40;
            }
			$second_data['side'] = $second_side;
		}







		/*
		 * Calculate start and end times
		 */
        $this->load->model('Increment_adjustment');
        $duration = empty($teetime_data['duration']) ? (!empty($cur_teetime_data) ? $cur_teetime_data->duration : 0) : $teetime_data['duration'];
        $second_data['start'] = $this->Increment_adjustment->get_slot_time($second_data['start'] + 1000000, $teesheet_data->teesheet_id,  0, (!$online_booking));
        if ($duration) {
            $teetime_data['end'] = $this->Increment_adjustment->get_slot_time($teetime_data['start'] + 1000000, $teesheet_data->teesheet_id, $duration, (!$online_booking));
            $second_data['end'] = $this->Increment_adjustment->get_slot_time($second_data['start'] + 1000000, $teesheet_data->teesheet_id, $duration, (!$online_booking));
        }
        else if (isset($second_data['end'])) {
	        $this->clean_time($teetime_data['start'], 0, $teesheet_data->increment);
            $this->clean_time($teetime_data['end'], 0, $teesheet_data->increment);
            $this->clean_time($second_data['end'], 0, $teesheet_data->increment);
        }
        if (strlen($teetime_id) > 20) {
            unset($second_data['start']);
            unset($second_data['end']);
            unset($second_data['duration']);
        }



        /*
         * Save the second teetime
         */
        $second_time_available = true;
		if (strlen($teetime_id) == 20 && (empty($second_data['status']) || $second_data['status'] != 'deleted') &&
			(isset($teetime_data['holes']) && $teetime_data['holes'] == 18))
			$second_time_available = $this->check_availability($second_data);
		if ($online_booking)
		{
			if (!$second_time_available)
				return array('success'=>false, 'message'=>'reround_error', 'send_confirmation'=>'');
		}
		if(isset($second_data['start']) && $second_data['start'] != "")
			$second_data['start_datetime']=\fu\Teetime::convertTeetimeToDatetime($second_data['start']);
		if(isset($second_data['end']) && $second_data['end'] != "")
			$second_data['end_datetime']=\fu\Teetime::convertTeetimeToDatetime($second_data['end']);
        unset($teetime_data['pending_reservation_id']);
        unset($second_data['pending_reservation_id']);
        if (!$second_id or !$this->exists($second_id))
		{
			if (strlen($second_id) > 20) {
				$second_data['reround'] = 1;
			}
			$second_data['date_booked'] = gmdate('Y-m-d H:i:s');

			if($this->db->insert('teetime',$second_data))
				$success = true;
			else
				$success = false;
            $this->Tee_time_log->save($second_id, $this->db->last_query());
            if ((isset($teetime_data['booking_source']) && $teetime_data['booking_source'] == 'standby') || $split || $paste || $template)
				$json_ready_events[] = $this->add_teetime_styles(array_merge($second_data));
		}
		else
		{
			$this->db->where('TTID', $second_id);
			$this->db->limit(1);
			$success = $this->db->update('teetime',$second_data);
            $this->Tee_time_log->save($second_id, $this->db->last_query());
			$json_ready_events[] = $this->add_teetime_styles(array_merge($second_data, (array)$this->get_info($second_data['TTID'])));
		}



        /*
         * Shotgun logic
         */
		if (isset($teetime_data['type']) && $teetime_data['type'] == 'shotgun')
		{
			$players_array = array();
			$cur_teetime_data->display_html = '';
			// Fetch player data
			$this->load->model('event');
			$players = $this->event->get_people($teetime_data['TTID']);
			// Organize player data
			foreach ($players as $player)
			{
				$players_array[(int)$player['hole']][(int)$player['player_position']] = $player;
			}
			ksort($players_array);
			// Create display_html
			$cur_teetime_data->display_html .= '<table><tbody>';
			foreach ($players_array as $hole => $holes)
			{

				$cur_teetime_data->display_html .= "<tr>";
				$cur_teetime_data->display_html .= "<td>{$hole}A</td>";
				$cur_teetime_data->display_html .= "<td>".str_replace("'", '', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)!empty($holes['1']['label']) ? $holes['1']['label'] : ''), "\0..\37'\\")))."</td>";
				$cur_teetime_data->display_html .= "<td>".str_replace("'", '', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)!empty($holes['2']['label']) ? $holes['2']['label'] : ''), "\0..\37'\\")))."</td>";
				$cur_teetime_data->display_html .= "<td>".str_replace("'", '', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)!empty($holes['3']['label']) ? $holes['3']['label'] : ''), "\0..\37'\\")))."</td>";
				$cur_teetime_data->display_html .= "<td>".str_replace("'", '', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)!empty($holes['4']['label']) ? $holes['4']['label'] : ''), "\0..\37'\\")))."</td>";
				$cur_teetime_data->display_html .= "</tr>";
				if (!empty($holes['5']['label']) || !empty($holes['6']['label']) || !empty($holes['7']['label']) || !empty($holes['8']['label']))
				{
					$cur_teetime_data->display_html .= "<tr>";
					$cur_teetime_data->display_html .= "<td>{$hole}B</td>";
					$cur_teetime_data->display_html .= "<td>".str_replace("'", '', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)!empty($holes['5']['label']) ? $holes['5']['label'] : ''), "\0..\37'\\")))."</td>";
					$cur_teetime_data->display_html .= "<td>".str_replace("'", '', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)!empty($holes['6']['label']) ? $holes['6']['label'] : ''), "\0..\37'\\")))."</td>";
					$cur_teetime_data->display_html .= "<td>".str_replace("'", '', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)!empty($holes['7']['label']) ? $holes['7']['label'] : ''), "\0..\37'\\")))."</td>";
					$cur_teetime_data->display_html .= "<td>".str_replace("'", '', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)!empty($holes['8']['label']) ? $holes['8']['label'] : ''), "\0..\37'\\")))."</td>";
					$cur_teetime_data->display_html .= "</tr>";
				}
			}
			$cur_teetime_data->display_html .= "</tbody></table>";
		}





		/*
		 * Save the initial teetime
		 */
		if(isset($teetime_data['start']))
			$teetime_data['start_datetime']=\fu\Teetime::convertTeetimeToDatetime($teetime_data['start']);
		if(isset($teetime_data['end']))
			$teetime_data['end_datetime']=\fu\Teetime::convertTeetimeToDatetime($teetime_data['end']);
        if ($success && !$this->exists($teetime_id))
		{
			if (strlen($teetime_id) > 20) {
				$teetime_data['reround'] = 1;
			}
			$teetime_data['date_booked'] = gmdate('Y-m-d H:i:s');
			if($this->db->insert('teetime',$teetime_data))
				$saved = true;
			else
				$saved = false;
            $this->Tee_time_log->save($teetime_id, $this->db->last_query());
        }
		else {
			$this->db->where('TTID', $teetime_id);
			$this->db->limit(1);
			$saved = $this->db->update('teetime',$teetime_data);
            $this->Tee_time_log->save($teetime_id, $this->db->last_query());

			$this->eventDispatcher = \fu\Events\EventDispatcher::getDispatcher();
			$dispatchContext = new \Onoi\EventDispatcher\DispatchContext();
			$dispatchContext->set("tt_id",$teetime_id);
			$dispatchContext->set("teesheet_id",$teesheet_data->teesheet_id);
			if(isset($teetime_data['status']) && $teetime_data['status'] == "deleted"){
				$this->eventDispatcher->dispatch("teetime.deleted",$dispatchContext);
			} else {
				$this->eventDispatcher->dispatch("teetime.updated",$dispatchContext);
			}
        }


        /*
         * Prepare JSON Ready Events
         */
		empty($cur_teetime_data)?$cur_teetime_data = []:'';
		$json_ready_events[] = $this->add_teetime_styles(array_merge((array)$cur_teetime_data, $teetime_data));

		// Send a confirmation email if we haven't done it yet.
		$send_confirmation = '';


		if ($saved && isset($teetime_data['person_id']) && $teetime_data['person_id'] != 0)
		{
			if(
				(!isset($teetime_data) || (!isset($teetime_data['confirmation_emailed']) || !$teetime_data['confirmation_emailed']) &&
				((!isset($cur_teetime_data) || !isset($cur_teetime_data->confirmation_emailed) || !$cur_teetime_data->confirmation_emailed)) && $this->input->post('send_confirmation_emails'))){
				$send_confirmation =  $teetime_id;
			} 
		}


		log_message('error', 'Last database: ' . $this->db->last_query());
        return array('success'=>$saved, 'send_confirmation'=>$send_confirmation, 'second_time_available'=>$second_time_available, 'teetime_id'=>$teetime_id);
	}
	function search($customer_id='')
	{
        $this->load->model('teesheet');
		$teetime_html = '';
		if ($customer_id == '')
			$teetime_html = "<div class='teetime_result'>No results</div>";
		else {
            $tee_sheet_names = array();
			if ($this->session->userdata('selected_stacked_course_id') && $this->config->item('stack_tee_sheets')) {
                $teesheet_id_list = $this->teesheet->get_all_from_course(10000, 0, $this->session->userdata('selected_stacked_course_id'))->result_array();
            }
            else if ($this->config->item('stack_tee_sheets')){
                $teesheet_id_list = $this->teesheet->get_all(10000, 0, true)->result_array();
            } else {
                $teesheet_id_list = array();
                $teesheet_ids[] = $this->session->userdata('teesheet_id');
            }
            foreach($teesheet_id_list as $teesheet) {
                $teesheet_ids[] = $teesheet['teesheet_id'];
                $tee_sheet_names[$teesheet['teesheet_id']] = $teesheet['title'];
            }
            $this->db->from("teetime");
            $current_day = (date('Ymd')-100).'0000';

            $teesheet_sql = "(".implode(',',$teesheet_ids).")";
			$this->db->where("TTID LIKE '____________________' AND status != 'deleted' AND teesheet_id IN $teesheet_sql AND start >= $current_day AND (person_id = $customer_id OR person_id_2 = $customer_id OR person_id_3 = $customer_id OR person_id_4 = $customer_id OR person_id_5 = $customer_id)");
			$this->db->order_by("start");
			$teetimes = $this->db->get()->result_array();
			foreach ($teetimes as $teetime)
			{
				$date = date("F j, Y, g:i a", strtotime($teetime['start']+1000000));
                $title = $tee_sheet_names[$teetime['teesheet_id']];
				$teetime_html .= "<div class='teetime_result'><div>{$date}</div><div>Players: {$teetime['player_count']}</div>";
                if (!empty($tee_sheet_names[$teetime['teesheet_id']])) {
                    $teetime_html .= "<div>Tee Sheet: {$tee_sheet_names[$teetime['teesheet_id']]}</div>";
                }
                $teetime_html .= '</div>';
			}
		}
		return $teetime_html;
	}

	function send_reminder_email($email, $subject, $data, $from_email, $from_name)
	{
		$view = $this->load->view("email_templates/reservation_reminder",$data, true);
		send_sendgrid(
			$email,
			$subject,
			$view,//$this->load->view("email_templates/reservation_reminder",$data, true),
			$from_email,
			$from_name
		);
	}

	function send_change_email($teetime_id)
	{
		$teetime = $this->get_info($teetime_id,true);
		$this->load->model("Teetime");
		$teetimeService = new \fu\Teetime();
		$players = $teetimeService->parseTeetimePlayers($teetime);

		$ids=[];
		foreach($players as $player){
			if($player->getPersonId()!=0)
				$ids[] = $player->getPersonId();
		}
		if (!empty($ids)) {
            $playerInfo = $this->db->query("SELECT u.email AS username, p.email, p.first_name, p.last_name, p.person_id 
                FROM foreup_people AS p
                LEFT JOIN foreup_users AS u ON p.person_id = u.person_id
                WHERE p.person_id IN (".implode(',',$ids).")")->result_array();

            $emails = array_column($playerInfo, "email");
            $usernames = array_column($playerInfo, "username");
            foreach($usernames as $username) {
                if (filter_var($username, FILTER_VALIDATE_EMAIL) !== false) {
                    $emails[] = $username;
                }
            }
            $tee_sheet_title = $teetime->tee_sheet_title;
            if (empty($tee_sheet_title)) {
                $tee_sheet_title = '';
            }

            $this->load->model("Course");
            $course_model = new Course();
            $course_info = $course_model->get_info($this->session->userdata('course_id'));

            $email_data = array(
                'course_name' => $this->session->userdata('course_name'),
                'course_phone' => $course_info->phone,
                'course_id' => $this->session->userdata('course_id'),
                'booked_date' => date('n/j/y', strtotime($teetime->start + 1000000)),
                'booked_time' => date('g:ia', strtotime($teetime->start + 1000000)),
                'booked_holes' => $teetime->holes,
                'booked_players' => $teetime->player_count,
                'reservation' => $teetime->TTID,
                'tee_sheet' => $tee_sheet_title,
                'players' => $players
            );


			if($teetime->status == "deleted"){
				$html = $this->load->view("email_templates/teetime_cancelled", $email_data, true);
				$subject = "Reservation was cancelled";
			} else {
				$html = $this->load->view("email_templates/teetime_change", $email_data, true);
				$subject = "Reservation Update.";
			}
            foreach ($playerInfo as $recipient) {
                $substitutions["{{customer.first_name}}"][] = ucfirst($recipient['first_name']);
                $substitutions["{{customer.last_name}}"][] = ucfirst($recipient['last_name']);
                $substitutions["{{customer.person_id}}"][] = $recipient['person_id'];
            }

            $email = new \SendGrid\Email();
            $email->setSubject($subject)
                ->setFrom("no-reply@foreupsoftware.com")
                ->setFromName($this->session->userdata('course_name'))
                ->setHtml($html)
                ->setSubstitutions($substitutions)
                ->setSmtpapiTos($emails);


            $sendgridConfig = $this->config->item('sendgrid');
            $sendgrid = new \SendGrid($sendgridConfig['username'], $sendgridConfig['password']);

            $result = $sendgrid->send($email);

            return $result;
        }

        return false;
	}

	function send_confirmation_email($email, $subject, $data, $from_email, $from_name, $teesheet_id = false)
	{
		send_sendgrid(
			$email,
			$subject,
			$this->load->view("email_templates/reservation_made",$data, true),
			$from_email,//$this->session->userdata('course_email'),
			$from_name
		);
		if(empty($data['TTID']) && !empty($data['reservation'])){
			$TTID = substr($data['reservation'], 0, 20);
		} else {
			$TTID = substr($data['TTID'], 0, 20);
		}

		if(empty($teesheet_id)){
			$teesheet_id = $this->session->userdata('teesheet_id');
		}

		$this->db->query("UPDATE foreup_teetime SET confirmation_emailed = 1 WHERE (TTID = '{$TTID}' OR TTID = '{$TTID}b') AND teesheet_id = {$teesheet_id} LIMIT 2");
		//echo $this->db->last_query();
	}

	function get_price_class($teetime_price_class = 1, $customer_price_class = 1, $start_time, $teetime_id){
		$start_time = (int) substr($start_time, 8);
        if ($this->is_holiday === -1) {
            $this->is_holiday = $this->Appconfig->is_holiday();
        }
		$not_holiday = true;

		$price_category_index = 1;

		if(!empty($teetime_price_class)){
			$price_category_index = $teetime_price_class;

		}else if($this->config->item('holidays') && $this->is_holiday){
			$price_category_index = 7;
			$not_holiday = false;

		}else if(!empty($customer_price_class)){
			$price_category_index = $customer_price_class;

		}else if ((int) $this->config->item('early_bird_hours_begin') <= $start_time && (int)$this->config->item('early_bird_hours_end') > $start_time){
			$price_category_index = 2;

		}else if ((int) $this->config->item('morning_hours_begin') <= $start_time && (int)$this->config->item('morning_hours_end') > $start_time){
			$price_category_index = 3;

		}else if ((int) $this->config->item('afternoon_hours_begin') <= $start_time && (int)$this->config->item('afternoon_hours_end') > $start_time){
			$price_category_index = 4;

		}else if ((int) $start_time >= $this->config->item('super_twilight_hour')){
			$price_category_index = 6;

		}else if ((int) $start_time >= $this->config->item('twilight_hour')){
			$price_category_index = 5;
		}
		$this->teetime_price_classes[$teetime_id] = $price_category_index;

		return $price_category_index;
	}

	function add_teetime_styles($teetime_data) {

		if(empty($teetime_data['status'])){
			$teetime_data['status'] = '';
		}
		if(empty($teetime_data['turn_time'])){
			$teetime_data['turn_time'] = '0000-00-00 00:00:00';
		}
		if(empty($teetime_data['teed_off_time'])){
			$teetime_data['teed_off_time'] = '0000-00-00 00:00:00';
		}
		if(empty($teetime_data['paid_player_count'])){
			$teetime_data['paid_player_count'] = 0;
		}

		$return_data = array();
		$return_data['id'] = $teetime_data['TTID'];
		$return_data['type'] = $teetime_data['type'];
		$return_data['name'] = $this->teesheet->clean_for_json($teetime_data['title']);
		$return_data['status'] = $teetime_data['status'];
		$return_data['title'] = $this->teesheet->clean_for_json($teetime_data['title']);
		$return_data['teesheet_id'] = $teetime_data['teesheet_id'];
		$return_data['allDay'] = '';
		$return_data['stimestamp'] = $teetime_data['start'];
        $return_data['duration'] = empty($teetime_data['duration']) ? 0 : $teetime_data['duration'];
		$return_data['etimestamp'] = $teetime_data['end'];
        $carts = array();
        if (isset($teetime_data['cart_num_1']) && $teetime_data['cart_num_1'] != '') {
            $carts[] = $teetime_data['cart_num_1'];
        }
        if (isset($teetime_data['cart_num_2']) && $teetime_data['cart_num_2'] != '') {
            $carts[] = $teetime_data['cart_num_2'];
        }
        $return_data['cart_numbers'] = implode(',',$carts);
		$return_data['start'] = $teetime_data['start'];
		$return_data['end'] = $teetime_data['end'];
		$return_data['side'] = $teetime_data['side'];
		$return_data['player_count'] = $teetime_data['player_count'];
		if(isset($teetime_data['carts']))
			$return_data['carts'] = $teetime_data['carts'];
		$return_data['display_html'] = isset($teetime_data['display_html'])?$teetime_data['display_html']:'';
		$return_data['backgroundColor']= '';
		if ($teetime_data['holes'] == 18)
			$return_data['borderColor']= '';
		else
			$return_data['borderColor']= '';
		//Add classes for styling
		$booked_class = $cart_class = $checkedin_class = $teed_off_class = $raincheck_class = $online_class = $details_class = '';
		if (isset($teetime_data['booking_source']) && $teetime_data['booking_source'])
			$booked_class = 'booked_online';
		if (isset($teetime_data['carts']) && $teetime_data['carts'] > 0)
			$cart_class = 'carts';
		if (($teetime_data['status'] == 'checked in' || $teetime_data['status'] == 'walked in') && !$this->config->item('sales'))
			$checkedin_class = 'checked_in';
		if ($teetime_data['teed_off_time'] > '0000-00-00 00:00:00')
			$teed_off_class = 'teed_off';
		if ($teetime_data['turn_time'] > '0000-00-00 00:00:00')
			$teed_off_class .= ' mark_turn';
		if (!empty($teetime_data['finish_time']) && $teetime_data['finish_time'] != '0000-00-00 00:00:00')
			$teed_off_class .= ' mark_finished';
		if (isset($teetime_data['raincheck_players_issued']) && $teetime_data['raincheck_players_issued'] > 0)
			$raincheck_class .= ' rainchecks_issued';
		if ($teetime_data['details'] != '')
			$details_class = 'details';
		if (isset($teetime_data['booking_source']) && ($teetime_data['booking_source'] == 'online' || $teetime_data['booking_source'] == 'ForeUP App' || $teetime_data['booking_source'] == 'api'))
			$online_class = ' online_booking';

		$pc = ($teetime_data['player_count']>5)?5:$teetime_data['player_count'];
		$ppc = ($teetime_data['paid_player_count']>$pc)?$pc:$teetime_data['paid_player_count'];

		if ($ppc == 0 || $teetime_data['type'] != 'teetime')
			$paid_class = '';
		else if ($this->config->item('simulator'))
			$paid_class =  "paid_4_4";
		else
			$paid_class = "paid_{$ppc}_{$pc}";
		$return_data['className'] = $teetime_data['type'].' holes_'.$teetime_data['holes'].' players_'.$teetime_data['player_count'].' '.$cart_class.' '.$checkedin_class.' '.$paid_class.' '.$teed_off_class.' '.$raincheck_class.' '.$online_class.' '.$details_class;
		$return_data['className'] .= (strlen($teetime_data['TTID']) > 20 ? ' reround':'');
		$return_data['className'] .= (strlen($teetime_data['TTID']) == 20 ? ' first_nine':'');

		// If teetime is a re-round, use the previous 9 price class
		if(strlen($teetime_data['TTID']) > 20 && !empty($this->teetime_price_classes[substr($teetime_data['TTID'], 0, 20)])){
			$price_class_id = $this->teetime_price_classes[substr($teetime_data['TTID'], 0, 20)];
		}else{
			$price_class_1 = isset($teetime_data['price_class_1']) ? $teetime_data['price_class_1'] : "";
			$price_class_id = $this->get_price_class((isset($teetime_data['default_price_category']) ? $teetime_data['default_price_category'] : 1), $price_class_1, $teetime_data['start'], $teetime_data['TTID']);
		}
		$return_data['className'] .= ' price_color_'.$price_class_id;

		return $return_data;

//                $return_data['start'] = $this->format_time_string($teetime->start);
//                $return_data['end'] = $this->format_time_string($teetime->end);

		/*if ($teetime_data->type == 'teetime' && ($teetime_data->status == 'checked in' || $teetime_data->status == 'walked in')) {
            $return_data['backgroundColor'] = '#5c9ccc';
            $return_data['borderColor'] = '#4297d7';
        }
        else if ($teetime_data->type == 'tournament') {
            $return_data['backgroundColor'] = '#5f9b5f';
            $return_data['borderColor'] = '#336133';
        }
        else if ($teetime_data->holes == 18) {
            $return_data['borderColor'] = '#333';
            $return_data['backgroundColor'] = '#445651';
        }*/



	}

	function change_date($teetime_id, $new_date, $new_end_date, $back_new_date, $back_new_end_date, $teesheet_id = '')
	{
		$teesheet_id = ($teesheet_id != '') ? $teesheet_id : $this->session->userdata('teesheet_id');
		//$teetime_info = $this->get_info($teetime_id)->result_array();
		$teetime_id = substr($teetime_id, 0, 20);
		$new_date = date('Ymd', strtotime($new_date.' -1 month'));
		//$json_ready_events = array();
		//$teetime_info[''];
		$primary_teetime_id = substr($teetime_id, 0, 20);
		$secondary_teetime_id = $primary_teetime_id.'b';

		$new_datetime = \fu\Teetime::convertTeetimeToDatetime($new_date);
		$new_end_datetime = \fu\Teetime::convertTeetimeToDatetime($new_end_date);
		$new_back_datetime = \fu\Teetime::convertTeetimeToDatetime($back_new_date);
		$new_back_end_datetime = \fu\Teetime::convertTeetimeToDatetime($back_new_end_date);

		//$this->db->query("UPDATE foreup_teetime SET start = CONCAT('{$new_date}', SUBSTRING(start, 9)), end = CONCAT('{$new_date}', SUBSTRING(end, 9)) WHERE TTID = '{$primary_teetime_id}%' LIMIT 1");
		$this->db->query("UPDATE foreup_teetime SET start = '{$new_date}', end = '{$new_end_date}', teesheet_id = '{$teesheet_id}',start_datetime='{$new_datetime}',end_datetime={$new_end_datetime} WHERE TTID = '{$primary_teetime_id}' LIMIT 1");
		return $this->db->query("UPDATE foreup_teetime SET start = '{$back_new_date}', end = '{$back_new_end_date}', teesheet_id = '{$teesheet_id}',start_datetime='{$new_back_datetime}',end_datetime={$new_back_end_datetime} WHERE TTID = '{$secondary_teetime_id}' LIMIT 1");
	}

	function remove_player($teetime_id,$person_id)
	{
		$this->load->model("teesheet");
		$teetime = $this->get_info($teetime_id);
		$teesheet = $this->teesheet->get_info($teetime->teesheet_id);
		if($teesheet->self_cancel_only == 0){
			return false;
		}
		if($teetime->player_count <= 1){
			return false;
		}

		$teetimeLib = new \fu\Teetime();
		$list = $teetimeLib->parseTeetimePlayers($teetime);
		$playersLeft = 0;
		foreach($list as $key=>$player)
		{
			if($player->getPersonId() == $person_id){
				array_splice($list,$key,1);
			} else if ($player->getPersonId() > 0) {
				$playersLeft++;
			}
		}
		$teetime = $teetimeLib->parseListToTeetime($list,$teetime);

		if($playersLeft == 0){
			return false;
		}


		$teetime->player_count = $teetime->player_count -1;
		$teetime = (array)$teetime;
		unset($teetime['reround']);
		$this->save( $teetime,$teetime_id);

		return true;
	}


	/*
	Deletes one teetime
	*/
	function delete($teetime_id, $person_id = '', $type = 'teetime-auto deleted')
	{
        $this->load->model('Tee_time_log');
		if ($person_id != '')
		{
			if($this->remove_player($teetime_id,$person_id)){
				$this->send_change_email($teetime_id);
				return true;
			}
			//If the teetime is being deleted by a golfer online
			$this->db->where('paid_player_count', 0);
		}
		else
			//If the teetime is being deleted from an employee using the system
			$person_id = $this->session->userdata('person_id');

		if ($person_id != '')
		{


			$this->db->where("TTID", substr($teetime_id,0,20));
			$this->db->or_where("TTID", substr($teetime_id,0,20).'b');
			$this->db->limit(2);

			$this->eventDispatcher = \fu\Events\EventDispatcher::getDispatcher();
			$dispatchContext = new \Onoi\EventDispatcher\DispatchContext();
			$dispatchContext->set("tt_id",substr($teetime_id,0,20));
			$dispatchContext->set("teesheet_id",$this->session->userdata("teesheet_id"));
			$this->eventDispatcher->dispatch("teetime.deleted",$dispatchContext);

			$this->db->update('teetime', array('status' => 'deleted', 'type' => $type, 'canceller_id' => $person_id, 'date_cancelled' => date('Y-m-d H:i:s')));
            $this->Tee_time_log->save($teetime_id, $this->db->last_query());
            return $this->db->affected_rows() > 0;
		}
		else
			return false;
	}

    function get_template_events($tee_sheet_id, $date) {
        $start = date('Ymd0000', strtotime($date)) - 1000000;
        $end = date('Ymd2359', strtotime($date)) - 1000000;
        $this->db->from('teetime');
        $this->db->where('status !=', 'deleted');
        $this->db->where('teesheet_id', $tee_sheet_id);
        $this->db->where('type !=', 'teetime');
        $this->db->where('start >', $start);
        $this->db->where('end <', $end);

        return $this->db->get()->result_array();
    }

	function generate_id($type = 'tt') {
		if ($type == 'tt')
			$prefix = 'TTID_'.date('mdHis');
		else if ($type == 'ts')
			$prefix = 'teesheet_id_';
		else
			$prefix = '';

		$length = 20 - strlen($prefix);
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$string = $prefix;
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters)-1)];
		}
		return $string;
	}
	function get_stats($view, $year, $month, $day, $dow) {
		$brand = $this->session->userdata("teesheet_id");
		$statsArray = array();
		$eday = $day+1;
		if ($month < 10)
			$month = "0".$month;
		if ($day < 10)
			$day = "0".$day;
		if ($eday < 10)
			$eday = "0".$eday;
		if ($view == 'agendaDay') {
			//get one day stats

			$dayString = $year.$month.$day.'0000';
			$edayString = $year.$month.$eday.'0000';
			if ($this->config->item('sales'))
				$this->db->select("choles,
	                count(*) AS teetimes,
	                sum(case WHEN `status` = 'checked in' THEN 1 ELSE 0 END) AS tcheckin,
	                sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
	                sum(`player_count`) AS players,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `paid_player_count` ELSE 0 END) AS cplayers,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `paid_carts` ELSE 0 END) AS ccarts,
	                sum(`carts`) AS carts");
			else
				$this->db->select("choles,
	                count(*) AS teetimes,
	                sum(case WHEN `status` = 'checked in' THEN 1 ELSE 0 END) AS tcheckin,
	                sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
	                sum(`player_count`) AS players,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `player_count` ELSE 0 END) AS cplayers,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `carts` ELSE 0 END) AS ccarts,
	                sum(`carts`) AS carts");
			$this->db->from('teetime');
			$this->db->where("teesheet_id = '$brand'
                    AND `start` > '$dayString'
                    AND `end` < '$edayString'
                    AND `status` != 'deleted'
                    AND `TTID` NOT LIKE '____________________b'");
			$this->db->group_by('choles');

			$results = $this->db->get();
			foreach($results->result() as $result) {
				if ($result->choles == '9')
					$statsArray['nine'] = $result;
				else if ($result->choles == '18')
					$statsArray['eighteen'] = $result;
			}
		}
		else if ($view == 'agendaWeek') {
			//get full week stats
		}
		return json_encode($statsArray);
	}

	/**
	 * get all online bookings associated with each course id (for non superadmin)
	 */
	public function get_online_bookings_count($last_week = false)
	{
		$course_id = '';
		if (!$this->permissions->is_super_admin())
			$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";

		$this->db->from('teetime');
		$this->db->join('teesheet', 'teetime.teesheet_id=teesheet.teesheet_id');
		$this->db->where("lower(booking_source) = 'online' {$course_id}");

		if($last_week === true)
			$this->db->where('YEARWEEK(date_booked) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)');

		return $this->db->count_all_results();
	}

	/**
	 *
	 */
	public function get_bookings($limit = false)
	{
		$this->db->select('teetime.date_booked, teetime.start, teetime.end, teetime.player_count, teetime.holes');
		$this->db->from('teetime');
		$this->db->join('teesheet', 'teetime.teesheet_id=teesheet.teesheet_id');
		$this->db->where('status != \'delete\'');
		if($limit !== false) $this->db->limit($limit);
		$this->db->order_by('date_booked', 'desc');

		$query = $this->db->get();

		if($query->num_rows() > 0) return $query->result_array();
		else return array();
	}

	public function get_thank_you_list($teesheet_id)
	{
		// Fetch all tee times and person_ids that qualify for thank yous.
		$nine_hours_ago = date('YmdHi', strtotime('-9 hours'))-1000000;
		$seven_hours_ago = date('YmdHi', strtotime('-7 hours'))-1000000;
		//$seven_hours_ago = date('Ymd2359')-1000000;
		$result = $this->db->query("
			SELECT CONCAT(`person_id`,',', `person_id_2`,',', `person_id_3`,',', `person_id_4`,',', `person_id_5`) AS people_ids
			FROM (`foreup_teetime`)
			WHERE `teesheet_id` = '$teesheet_id'
			AND `type` = 'teetime'
			AND `start` > '$nine_hours_ago'
			AND `start` < '$seven_hours_ago'
			AND `paid_player_count` > 0
			AND `status` != 'deleted'
			AND `thank_you_emailed` = 0
			AND LENGTH(TTID) < 21
			")->result_array();

		// Put all person_ids into array
		$customer_id_list = array();
		print_r($result);
		foreach($result AS $customer_ids)
		{
			print_r($customer_ids);
			$customer_ids = explode(',', $customer_ids['people_ids']);
			$customer_id_list = array_merge($customer_id_list, $customer_ids);
		}
		$customer_id_list =  array_unique($customer_id_list);

		// Get all customer email addresses for thank yous
		$email_list = array();
		if (count($customer_id_list) > 0)
		{
			$this->db->select('email');
			$this->db->from('people');
			$this->db->where_in('person_id', $customer_id_list);
			$this->db->limit(count($customer_id_list));
			$emails = $this->db->get()->result_array();

			foreach($emails as $email)
			{
				if ($email['email'] && $email['email'] != '')
					$email_list[] = array('email'=>$email['email']);
			}
		}

		// Mark all tee times we gathered as 'thanked'
		$this->db->query("
			UPDATE (`foreup_teetime`)
			SET `thank_you_emailed` = 1
			WHERE `teesheet_id` = '$teesheet_id'
			AND `type` = 'teetime'
			AND `start` > '$nine_hours_ago'
			AND `start` < '$seven_hours_ago'
			AND `paid_player_count` > 0
			AND `status` != 'deleted'
			AND LENGTH(TTID) < 21
			");

		// Return the email list
		return $email_list;
	}

	public function getTeetimesNeedingReminder($teesheet_id,$type = "day",$timeBefore = 1,$now=null)
	{
		if(!isset($now)){
			$now = \Carbon\Carbon::now();
		} else {
			$now = \Carbon\Carbon::parse($now,"UTC");
		}
		if($type == "day"){
			$date = new \fu\reports\MovingRanges\SingleDay($timeBefore,$now);
		} else if($type =="minute"){
			$date = new \fu\reports\MovingRanges\SingleMinute($timeBefore,$now);
			$this->load->model("Course");
			$course_model = new Course();
			$course_info = $course_model->get_info_from_teesheet_id($teesheet_id);
			$date->setTimezone($course_info->timezone);
		} else {
			throw new Exception("Invalid reminder type, expecting day or minute.  Recieved {$type}");
		}

		print "Searching between {$date->getStartTimeStamp()} and {$date->getEndTimeStamp()} <br/>";
		$result = $this->db->query("
			SELECT CONCAT(`person_id`,',', `person_id_2`,',', `person_id_3`,',', `person_id_4`,',', `person_id_5`) AS people_ids, TTID
			FROM (`foreup_teetime`)
			WHERE `teesheet_id` = '$teesheet_id'
			AND `type` = 'teetime'
			AND `start_datetime` >= '{$date->getStartTimeStamp()}'
			AND `start_datetime` < '{$date->getEndTimeStamp()}'
			AND `status` != 'deleted'
			AND LENGTH(TTID) < 21
			")->result_array();

		$customer_id_list = [];
		foreach($result AS $row)
		{
			$customer_ids = explode(',', $row['people_ids']);
			foreach($customer_ids as $id){
				$customer_id_list[$id] = $row['TTID'];
			}
		}

		return $customer_id_list;


	}

	public function getTomorrowsTeetimes($teesheet_id)
	{
		$tomorrow = new \fu\reports\MovingRanges\Tomorrow();
		$result = $this->db->query("
			SELECT CONCAT(`person_id`,',', `person_id_2`,',', `person_id_3`,',', `person_id_4`,',', `person_id_5`) AS people_ids, TTID
			FROM (`foreup_teetime`)
			WHERE `teesheet_id` = '$teesheet_id'
			AND `type` = 'teetime'
			AND `start_datetime` > '{$tomorrow->getStartTimeStamp()}'
			AND `start_datetime` < '{$tomorrow->getEndTimeStamp()}'
			AND `status` != 'deleted'
			AND LENGTH(TTID) < 21
			")->result_array();

		$customer_id_list = [];
		foreach($result AS $row)
		{
			$customer_ids = explode(',', $row['people_ids']);
			foreach($customer_ids as $id){
				$customer_id_list[$id] = $row['TTID'];
			}
		}

		return $customer_id_list;
	}

	public function update_no_show($teetime_id, $position, $customer_id = false, $no_show = true){

		if(empty($position) || empty($teetime_id)){
			return false;
		}

		if(!empty($no_show)){
			$no_show = 1;
		}else{
			$no_show = 0;
		}

		$teetime = $this->db->select('start_datetime,
			person_no_show_1, person_no_show_2, person_no_show_3,
			person_no_show_4, person_no_show_5, no_show_count')
			->from('teetime')
			->where('TTID', $teetime_id)
			->get()->row_array();

		$col = 'person_no_show_'.(int) $position;

		if($teetime[$col] == 1){
			return true;
		}

		$this->db->update('teetime',
			[
				$col => $no_show,
				'no_show_count' => $teetime['no_show_count'] + 1
			],
			['TTID' => $teetime_id]
		);

		$no_show_data = [
			'teetime_id' => $teetime_id,
			'player_position' => $position,
			'date_created' => date('Y-m-d H:i:s'),
			'teetime_date' => $teetime['start_datetime'],
			'course_id' => $this->session->userdata('course_id'),
			'employee_id' => $this->session->userdata('person_id')
		];

		if(!empty($customer_id)){
			$no_show_data['customer_id'] = (int) $customer_id;
		}
		$this->db->insert('teetime_no_shows', $no_show_data);

		return $this->db->affected_rows();
	}

	function remove_old_pending_reservations() {
	    $this->db->where('date_booked <', gmdate('Y-m-d H:i:s', strtotime('-2 minutes')));
        $this->db->where('type', 'pending_reservation');
        $this->db->where('start >', date('Ymd0000')-1000000);
        return $this->db->update('teetime', array('status'=>'deleted', 'last_updated' => gmdate('Y-m-d H:i:s')));
    }

    function get_available_spots($params) {
        $this->db->from('teetime');
        $this->db->where('start <=', $params['start']);
        $this->db->where('end >', $params['start']);
        $this->db->where('status !=', 'deleted');
        if (!empty($params['holes']) && $params['holes'] == '18') {
            $this->db->where('reround', 0);
        }
        $this->db->where('teesheet_id', $params['teesheet_id']);

        $reservations = $this->db->get()->result_array();
        $players = 0;
        $event = false;

        foreach($reservations as $reservation) {
            if ($reservation['type'] != 'teetime') {
                $event = true;
            }
            else {
                $players += $reservation['player_count'];
            }

        }

        return $event ? 0 : 4 - $players;
    }

    function simple_save($params, $tee_time_ids) {
        $this->db->where_in('TTID', $tee_time_ids);
        return $this->db->update('teetime', $params);
    }

    function delete_linked_events($event_link_id, $start = false) {
        $json_ready_events = array();

        $this->db->from('teetime');
        if ($start) {
            $this->db->where('start >=', $start);
        }
        $this->db->where('status !=', 'deleted');
        $this->db->where('event_link_id', $event_link_id);
        $linked_events_to_delete = $this->db->get()->result_array();

        foreach($linked_events_to_delete as $event) {
            $event_data = array();
            $event_data['status'] = 'deleted';
            $event_data['canceller_id'] = $this->session->userdata('person_id');
            $event_data['date_cancelled'] = date('Y-m-d H:i:s');
            $event_data['type'] = $event['type'].'-emp deleted';

            $this->db->where('TTID', $event['TTID']);
            $this->db->update('teetime', $event_data);

            $event['status'] = 'deleted';
            $json_ready_events[] = $this->add_teetime_styles($event);
        }

        return $json_ready_events;
    }

    function get_linked_event_info($event_id) {
        $event_info = $this->get_info($event_id);
        $events_after = array();
        $events_after_count = 0;
        $all_events_count = 0;

        if (empty($event_info->event_link_id)) {
            $all_events = array();
        }
        else {
            $this->db->from('teetime');
            $this->db->where('status !=', 'deleted');
            $this->db->where('event_link_id', $event_info->event_link_id);
            $this->db->order_by('start');

            $all_events = $this->db->get()->result_array();
        }

        foreach ($all_events as $event) {
            if ($event['start'] >= $event_info->start) {
                $events_after[] = $event;
                if ($event['reround'] == 0) {
                    $events_after_count++;
                }
            }
            if ($event['reround'] == 0) {
                $all_events_count++;
            }
        }

        return array(
            'target_event' => $event_info,
            'future_events' => array(
                'count' => $events_after_count,
                'events' => $events_after
            ),
            'all_events' => array(
                'count' => $all_events_count,
                'events' => $all_events
            )
        );
    }

	/**
	 * @param $teetime_id
	 * @param $count
	 * @return stdClass
	 */
	private function mark_players_as_paid($teetime_id, $count,$selected_people_to_checkin)
	{
//Fill plaidPlayer Buckets
		$teetime = $this->get_info($teetime_id);

		//If 3 then fill 3 slots up to 5
		$teetime->player_count;
		$applied_paids = 0;
		$players_are_paid = [
			$teetime->person_paid_1,
			$teetime->person_paid_2,
			$teetime->person_paid_3,
			$teetime->person_paid_4,
			$teetime->person_paid_5
		];
		foreach($selected_people_to_checkin as $pos => $is_paid){
			if ($is_paid) {
				$teetime->{"person_paid_" . ($pos + 1)} = true;
				$applied_paids++;
			}
		}
		if ($applied_paids >= $count) {
			foreach ($players_are_paid as $pos => $is_paid) {
				if (!$is_paid) {
					$teetime->{"person_paid_" . ($pos + 1)} = true;
					$applied_paids++;
				}

				if ($applied_paids >= $count) {
					break;
				}
			}
		}

		$this->save(json_decode(json_encode($teetime), true), $teetime_id);
		return $teetime;
	}
}
?>
