<?php
class Pass_item extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get($item_id){

		$this->db->select("*");
		$this->db->from('pass_definitions');
		$this->db->where('item_id', (int) $item_id);

		$row = $this->db->get()->row();

		if(!empty($row)){
			$row->restrictions = json_decode($row->restrictions, true);
		}

		return $row;
	}

	function save($item_id, $data){

		$response = false;
		if(empty($item_id)){
			return false;
		}
		$data_to_update = [];

		if(!empty($data['days_to_expiration'])){
			$data_to_update['days_to_expiration'] = (int) $data['days_to_expiration'];
		}
		if(!empty($data['price_class_id'])){
			$data_to_update['price_class_id'] = (int) $data['price_class_id'];
		}
		if(!empty($data['restrictions'])){
			$data_to_update['restrictions'] = json_encode($data['restrictions']);
		}
		$data_to_update['item_id'] = $item_id;

		$result = $this->db->from("pass_definitions")
			->where("item_id",$item_id)
			->get();

		if($result->num_rows() == 1){
			$pass = $result->row();

			$this->db->where("item_id",$item_id);
			$this->db->update('pass_definitions', $data_to_update);
			if($this->db->affected_rows() > 0){
				$response = true;
			}

		} else {
			$this->db->insert('pass_definitions', $data_to_update);
			if($this->db->affected_rows() > 0){
				$response = true;
			}
		}


		
		return $response;
	}
	
	function delete($item_id){
		$this->db->delete('pass_definitions', array('item_id' => $item_id));
		return $this->db->affected_rows();	
	}
}