<?php
require_once("report.php");
class Summary_giftcards extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('giftcards_giftcard_number'), 'align'=>'left'),array('data'=>lang('giftcards_original_balance'), 'align'=>'left'), array('data'=>lang('giftcards_card_value'), 'align'=> 'left'), array('data'=>lang('giftcards_customer_name'), 'align'=> 'left'), array('data'=>lang('giftcards_issue_date'), 'align'=> 'left'));
	}
	public function getDataTransactionColumns()
	{
		return array('summary' => array( array('data'=>lang('reports_date'), 'align'=>'left'), array('data'=>lang('reports_customer'), 'align'=> 'left'), array('data'=>lang('reports_employee'), 'align'=> 'left'), array('data'=>lang('reports_Amount'), 'align'=> 'left'), array('data'=>lang('reports_comments'), 'align'=> 'left')) );
	}
	
	public function getData()
	{
		$course_id = $this->session->userdata('course_id');

		$result = $this->db->query("SELECT g.giftcard_number, g.value,
				initial_transaction.trans_amount, 
				CONCAT(c.first_name, ' ', c.last_name) AS customer_name, 
				g.date_issued
			FROM foreup_giftcards AS g
			LEFT JOIN foreup_people AS c
				ON c.person_id = g.customer_id
			LEFT JOIN (
				SELECT MIN(trans_date) AS trans_date, trans_amount, trans_giftcard
				FROM foreup_giftcard_transactions
				INNER JOIN foreup_giftcards
					ON foreup_giftcards.giftcard_id = foreup_giftcard_transactions.trans_giftcard
				WHERE foreup_giftcards.course_id = {$course_id}
				GROUP BY trans_giftcard
			) AS initial_transaction
				ON initial_transaction.trans_giftcard = g.giftcard_id
			WHERE g.course_id = {$course_id}
				AND g.deleted = 0
			GROUP BY g.giftcard_id");

		return $result->result_array();
	}
	
	public function getSummaryData()
	{
		$this->db->select('SUM(value) as outstanding_giftcard_value', false);
		$this->db->from('giftcards');
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
        //$this->db->order_by('giftcard_number');

		return $this->db->get()->row_array();
	}

	public function getTransactions($giftcard_id)
	{
		$this->db->select('trans_date, CONCAT(customer.first_name," ",customer.last_name) as customer_name, CONCAT(employee.first_name," ",employee.last_name) as employee_name, trans_amount, CONCAT(trans_description," ",trans_comment) AS trans_comment', false);
		$this->db->from('giftcard_transactions');
		$this->db->join('people as customer', 'giftcard_transactions.trans_customer = customer.person_id', 'left');
		$this->db->join('people as employee', 'giftcard_transactions.trans_user = employee.person_id');
		$this->db->where('giftcard_transactions.trans_giftcard', $giftcard_id);
		$this->db->order_by('giftcard_transactions.trans_date', 'DESC');
		$this->db->group_by('giftcard_transactions.trans_id');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		return $data;
	}
}
?>