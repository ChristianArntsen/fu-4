<?php
require_once("report.php");
class Summary_taxes extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_department'), 'align'=>'left'), array('data'=>lang('reports_tax_percent'), 'align'=>'left'), array('data'=>lang('reports_tax'), 'align'=>'right'));
	}
	
	public function getData($department_split = true)
	{
		$this->db->select('si.sale_id, si.item_id, si.item_kit_id, si.line, si.department,percent,amount');
		$this->db->from('sales_items_temp si');
		
		if (isset($this->params['sale_type']) && $this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif (isset($this->params['sale_type']) && $this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
        if ($this->params['department'] != 'all' && $this->params['department'] != '') {
            $this->db->where('department = "'. $this->params['department'] . '"');
        }
        if (!empty($this->params['cat']) && $this->params['cat'] != 'all') {
            $this->db->where('category = "'. $this->params['cat'] . '"');
        }
        if (!empty($this->params['sub_cat']) && $this->params['sub_cat'] != 'all') {
            $this->db->where('subcategory = "'. $this->params['sub_cat'] . '"');
        }
        if (isset($this->params['employee_id']) && $this->params['employee_id'] != '' && $this->params['employee_id'] != 0) {
            $this->db->where('employee_id = '. $this->params['employee_id']);
        }
		$this->db->where('si.deleted', 0);
		$this->db->join("sales_items_taxes sit","si.sale_id = sit.sale_id AND si.item_id = sit.item_id AND si.line = sit.line","LEFT");
		$taxes_data = array();
		$taxes = $this->db->get();


		foreach($taxes->result_array() as $row)
		{
			if ($row['item_id'])
			{
				$this->getTaxesForItems($taxes_data, $row['department'], $department_split,$row);
			}
			else if ($row['item_kit_id'])
			{
				$this->getTaxesForItemKits($row['sale_id'], $row['item_kit_id'], $row['line'], $taxes_data, $row['department'], $department_split);			
			}
			else if($row['item_id'] == 0)
			{
				$this->getTaxesForItems($taxes_data, $row['department'], $department_split,$row);
			}
		}


		return $taxes_data;
	}


	function getTaxesForItems(&$taxes_data, $department, $department_split = true,$row)
	{
		if($row['amount'] == 0)
			return;
		if ($department_split)
		{
			if (empty($taxes_data[$department]))
			{
				$taxes_data[$department] = array();
			}
			if (empty($taxes_data[$department][$row['percent']]))
			{
				$taxes_data[$department][$row['percent']] = array('percent' => $row['percent'] . ' %', 'tax' => 0);
			}
			$taxes_data[$department][$row['percent']]['tax'] += $row['amount'];
		}
		else
		{
			if (empty($taxes_data[$row['percent']]))
			{
				$taxes_data[$row['percent']] = array('percent' => $row['percent'] . ' %', 'tax' => 0);
			}
			$taxes_data[$row['percent']]['tax'] += $row['amount'];
		}
	}
	
	function getTaxesForItemKits($sale_id, $item_kit_id, $line, &$taxes_data, $department, $department_split = true)
	{
		$query = $this->db->query("SELECT percent, cumulative, item_kit_unit_price, subtotal, total,
			item_kit_cost_price, quantity_purchased, discount_percent FROM ".$this->db->dbprefix('sales_item_kits_taxes').'
		JOIN '.$this->db->dbprefix('sales_item_kits'). ' USING(sale_id, item_kit_id, line) WHERE '.
		$this->db->dbprefix('sales_item_kits_taxes').'.sale_id = '.$sale_id.' and '.
		$this->db->dbprefix('sales_item_kits_taxes').'.item_kit_id = '.$item_kit_id.' and '.
		$this->db->dbprefix('sales_item_kits_taxes').'.line = '.$line. ' ORDER BY cumulative');

		//echo '<br/><br/>'.$this->db->last_query();

		$tax_result = $query->result_array();
		for($k=0;$k<count($tax_result);$k++)
		{
			$tax_included = $this->config->item('unit_price_includes_tax');
			$row = $tax_result[$k];
			$p = $row['item_kit_unit_price'];
			$q = $row['quantity_purchased'];
			$d = $row['discount_percent'];
			$t = $row['percent'];
			$subtotal = $row['subtotal'];
			$total = $row['total'];
			
			if ((float)$row['percent'] > 0)
			{
				if ($tax_included)
				{
					$discounted_price = $total;
				    $actual_price = to_currency_no_money(($discounted_price / (1 + $t /100)));
                    $tax = $discounted_price - $actual_price;
                }
				else if ($row['cumulative'])
				{
					$previous_tax = $tax;
					$tax = to_currency_no_money(($subtotal + $tax) * ($t / 100));
				}
				else
				{
					$tax = to_currency_no_money($subtotal * ($t / 100));
				}
				
				if ($department_split)
				{
					if (empty($taxes_data[$department]))
					{
						$taxes_data[$department] = array();
					}
					if (empty($taxes_data[$department][$row['percent']]))
					{
						$taxes_data[$department][$row['percent']] = array('percent' => $t . ' %', 'tax' => 0);
					}
					
					//echo '<div>Sale_id '.$sale_id.' - Item KIT ID: '.$item_kit_id.' - Line: '.$line.' - Tax: '.$tax.' - Percent: '.$t.'</div>';
					$taxes_data[$department][$row['percent']]['tax'] += $tax;
				}
				else
				{
					if (empty($taxes_data[$row['percent']]))
					{
						$taxes_data[$row['percent']] = array('percent' => $t . ' %', 'tax' => 0);
					}
					
					//echo '<div>Sale_id '.$sale_id.' - Item KIT ID: '.$item_kit_id.' - Line: '.$line.' - Tax: '.$tax.' - Percent: '.$t.'</div>';
					$taxes_data[$row['percent']]['tax'] += $tax;
				}
			}
		}
	}

	
	public function getSummaryData()
	{
		$this->db->select('sum(tax) as tax');
		$this->db->from('sales_items_temp');
		
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
        if ($this->params['department'] != 'all' && $this->params['department'] != '') {
            $this->db->where('department = "'. $this->params['department'] . '"');
        }
        if ($this->params['cat'] != 'all' && $this->params['cat'] != '') {
            $this->db->where('category = "'. $this->params['cat'] . '"');
        }
        if ($this->params['sub_cat'] != 'all' && $this->params['sub_cat'] != '') {
            $this->db->where('subcategory = "'. $this->params['sub_cat'] . '"');
        }

		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		return $this->db->get()->row_array();
	}
}
?>
