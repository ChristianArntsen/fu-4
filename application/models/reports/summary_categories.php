<?php
require_once("report.php");
class Summary_categories extends Report
{
	function __construct()
	{
		parent::__construct();
	}

	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_category'), 'align'=> 'left'), array('data'=>lang('reports_subtotal'), 'align'=> 'right'), array('data'=>lang('reports_tax'), 'align'=> 'right'), array('data'=>lang('reports_total'), 'align'=> 'right'), array('data'=>lang('reports_profit'), 'align'=> 'right'));
	}

	public function getData()
	{
		
		//$this->db->select("category, sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit, sum(quantity_purchased) as count, IF ( item_number = 'invoice_balance' OR item_number = 'account_balance' OR item_number = 'member_balance' , 1 , 0 ) AS accounts_receivable");
		//$this->db->from('sales_items_temp');
		$sales_or_returns = $where = '';
        if (!empty($this->params['sale_type'])) {
            if ($this->params['sale_type'] == 'sales') {
                $sales_or_returns = ' AND quantity_purchased > 0';
                //$this->db->where('quantity_purchased > 0');
            } elseif ($this->params['sale_type'] == 'returns') {
                $sales_or_returns = ' AND quantity_purchased < 0';
                //$this->db->where('quantity_purchased < 0');
            }
        }

		if ($this->params['department'] != 'all')
			$where .= " AND department = '{$this->params['department']}'";
            
        if ($this->params['terminal'] != 'all') {
            $where .= " AND terminal_id = '{$this->params['terminal']}'";
        }

        if (isset($this->params['employee_id']) && $this->params['employee_id'] != '' && $this->params['employee_id'] != 0) {
            $where .= " AND employee_id = '{$this->params['employee_id']}'";
        }
			//$this->db->where('department = "'.$this->params['department'].'"');
		//$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		//$this->db->group_by('category');
		//$this->db->order_by('category');

		//$result = $this->db->get()->result_array();

		$result = $this->db->query("
			SELECT actual_category, `category`, sum(subtotal) as subtotal, 
				sum(total) as total, sum(tax) as tax, sum(profit) as profit, 
				sum(quantity_purchased) as count, SUM(total_cost) AS cost,
				SUM(IF ( item_number = 'invoice_balance' OR item_number = 'account_balance' OR item_number = 'member_balance', 1, 0 )) AS accounts_receivable
			FROM (`foreup_sales_items_temp`)
			WHERE `foreup_sales_items_temp`.`deleted` =  0
				$sales_or_returns
				$where
			GROUP BY `category`
			ORDER BY `accounts_receivable`, `category`	
		")->result_array();
		
		//print_r($result);
		return $result;
	}
}
?>
