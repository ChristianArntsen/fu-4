<?php
require_once("report.php");
class Detailed_receivings extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array('summary' => array(array('data'=>lang('reports_receiving_id'), 'align'=>'left'), array('data'=>lang('reports_date'), 'align'=>'left'), array('data'=>lang('reports_items_received'), 'align'=>'right'), array('data'=>lang('reports_received_by'), 'align'=>'left'), array('data'=>lang('reports_supplied_by'), 'align'=>'left'), array('data'=>lang('reports_total'), 'align'=>'right'), array('data'=>lang('reports_payment_type'), 'align'=>'left'), array('data'=>lang('reports_comments'), 'align'=>'left')),
					'details' => array(array('data'=>lang('reports_name'), 'align'=>'left'), array('data'=>lang('reports_category'), 'align'=>'left'), array('data'=>lang('reports_quantity_purchased'), 'align'=>'right'), array('data'=>lang('reports_total'), 'align'=>'right'), array('data'=>lang('reports_discount'), 'align'=>'right'))
		);		
	}
	
	public function getData()
	{
		$this->db->select('receiving_id, receiving_date, sum(quantity_purchased) as items_purchased, CONCAT(employee.first_name," ",employee.last_name) as employee_name,  CONCAT(supplier.company_name," (",supplier_person.first_name," ",supplier_person.last_name,")") as supplier_name, sum(total) as total, sum(profit) as profit, payment_type, comment', false);
		$this->db->from('receivings_items_temp');
		$this->db->join('people as employee', 'receivings_items_temp.employee_id = employee.person_id');
		$this->db->join('suppliers as supplier', 'receivings_items_temp.supplier_id = supplier.person_id', 'left');
		$this->db->join('people as supplier_person', 'receivings_items_temp.supplier_id = supplier_person.person_id', 'left');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		$this->db->where('receivings_items_temp.deleted', 0);
		$this->db->group_by('receiving_id');
		$this->db->order_by('receiving_date');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		$data['details'] = array();
		
		foreach($data['summary'] as $key=>$value)
		{
			$this->db->select('name, items.category, quantity_purchased, serialnumber,total, discount_percent');
			$this->db->from('receivings_items_temp');
			$this->db->join('items', 'receivings_items_temp.item_id = items.item_id');
			$this->db->where('receiving_id = '.$value['receiving_id']);
			$result = $this->db->get();
			$data['details'][$key] = $result->result_array();
		}
		
		return $data;
	}
	
	public function getSummaryData()
	{
		$this->db->select('sum(total) as total');
		$this->db->from('receivings_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		$this->db->where('deleted', 0);
		
		if($this->params['by_category']){
			$this->db->select('sum(total) as total, category, payment_type');
			$this->db->group_by('category');
			return $this->db->get()->result_array();
		}
		
		return $this->db->get()->row_array();
	}
	
	public function getDataBySupplier()
	{
		$this->db->select("SUM(r.total) as total, r.category, r.payment_type,
			IF(first_name != '', 
				CONCAT(company_name, ' - ', first_name, ' ',last_name),
				company_name
			) as supplier, r.supplier_id, r.invoice_number", false);
		$this->db->from('receivings_items_temp AS r');
		$this->db->join('suppliers', 'suppliers.person_id = r.supplier_id');
		$this->db->join('people', 'suppliers.person_id = people.person_id');
		$this->db->group_by('r.supplier_id, category');
		$this->db->where('r.deleted', 0);
		
		return $this->db->get()->result_array();
	}		
}
?>
