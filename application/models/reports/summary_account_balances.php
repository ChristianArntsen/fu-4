<?php
require_once("report.php");
class Summary_account_balances extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		$customer_account_balance = ($this->config->item('customer_credit_nickname') ? $this->config->item('customer_credit_nickname'):lang('customers_account_balance'));
		$member_account_balance = ($this->config->item('member_balance_nickname') ? $this->config->item('member_balance_nickname'):lang('customers_member_account_balance'));
		return array(
			array('data'=>lang('giftcards_customer_name'), 'align'=>'left', 'width' => '300px'),
			array('data'=>"Course", 'align'=>'left', 'width' => '160px'),
			array('data'=>$customer_account_balance, 'align'=> 'right', 'width' => '100px'),
			array('data'=>$member_account_balance, 'align'=> 'right', 'width' => '100px'),
			array('data'=>'Open Invoices', 'align'=> 'right', 'width' => '100px'));
	}
	
	public function getData()
	{
		$course_model = new \Course();
		$course_ids = array();
        // SHOW ONLY ONE COURSE'S INFO INSTEAD OF WHOLE GROUP FOR NEW ERA 12/30/16
        $course_model->get_linked_course_ids($course_ids, 'shared_customers');
        $group_course_ids_string = implode(',', $course_ids);
        $course_ids = array($this->config->item('course_id'));
		$course_ids_string = implode(',', $course_ids);

		$result = $this->db->query("
			SELECT
				CONCAT(last_name, ', ', first_name) as customer_name,
				account_type,
				CASE WHEN trans_household != trans_customer AND trans_household > 0 THEN trans_household ELSE trans_customer END as customer_id,
				SUM(trans_amount),
				`foreup_courses`.`name`
			FROM
				(`foreup_account_transactions`)
			LEFT JOIN
				`foreup_people` ON `foreup_people`.`person_id` = CASE WHEN trans_household != trans_customer AND trans_household > 0 THEN trans_household ELSE trans_customer END
			LEFT JOIN
				`foreup_customers` ON `foreup_customers`.`person_id` = `foreup_people`.`person_id`
			LEFT JOIN
				`foreup_courses` on `foreup_courses`.`course_id` = `foreup_account_transactions`.`course_id`
			WHERE
				trans_date < '{$this->params['trans_date']}' AND
				`deleted` = 0 AND
				`foreup_customers`.`course_id` IN ({$group_course_ids_string}) AND
				`foreup_account_transactions`.`course_id` IN ({$course_ids_string})
			GROUP BY
				`customer_id`, `account_type`
		");
        $result = $result->result_array();

        if(!empty($result)){
			foreach($result as $data){
				if($data['account_type'] == 'customer') {
					$data['account_type'] = 'account_balance';
				}
				if($data['account_type'] == 'invoice') {
					$data['account_type'] = 'invoice_balance';
				}
				if($data['account_type'] == 'member') {
					$data['account_type'] = 'member_balance';
				}
				$arr_customer_data[$data['customer_id']]['customer_name'] = $data['customer_name'];
				$arr_customer_data[$data['customer_id']]['course_name'] = $data['name'];
				$arr_customer_data[$data['customer_id']][$data['account_type']] = $data['SUM(trans_amount)'];
			}
		}
        unset($result);

		foreach ($arr_customer_data as $key => $value) {
			$arr_result[$key]['customer_name'] =isset($value['customer_name'] ) ? $value['customer_name'] : 0.00;
			$arr_result[$key]['account_balance'] =isset($value['account_balance'] ) ? $value['account_balance'] : 0.00;
			$arr_result[$key]['invoice_balance'] =isset($value['invoice_balance'] ) ? $value['invoice_balance'] : 0.00;
			$arr_result[$key]['member_balance'] =isset($value['member_balance'] ) ? $value['member_balance'] : 0.00;
			$arr_result[$key]['course_name'] = isset($value['course_name'] ) ? $value['course_name'] : "-";
		}
        unset($arr_customer_data);

		return $arr_result;		
	}

	public function getTotalsByCourse()
	{
		$course_model = new \Course();
		$course_ids = array();
        // SHOW ONLY ONE COURSE'S INFO INSTEAD OF WHOLE GROUP FOR NEW ERA 12/30/16
		$course_model->get_linked_course_ids($course_ids, 'shared_customers');
        $group_course_ids_string = implode(',', $course_ids);
        $course_ids = array($this->config->item('course_id'));
        $course_ids_string = implode(',', $course_ids);

		$result = $this->db->query("
			SELECT
				`foreup_courses`.`name`,
				SUM(CASE WHEN foreup_account_transactions.account_type = \"member\" THEN foreup_account_transactions.trans_amount ELSE 0 END) AS 'member',
				SUM(CASE WHEN foreup_account_transactions.account_type = \"customer\" THEN foreup_account_transactions.trans_amount ELSE 0 END) AS 'customer',
				SUM(CASE WHEN foreup_account_transactions.account_type = \"invoice\" THEN foreup_account_transactions.trans_amount ELSE 0 END) AS 'invoice'
			FROM
				(`foreup_account_transactions`)
			LEFT JOIN
				`foreup_people` ON `foreup_people`.`person_id` = CASE WHEN trans_household != trans_customer AND trans_household > 0 THEN trans_household ELSE trans_customer END
			LEFT JOIN
				`foreup_customers` ON `foreup_customers`.`person_id` = `foreup_people`.`person_id`
			LEFT JOIN 
				`foreup_courses` ON `foreup_courses`.`course_id` = `foreup_account_transactions`.`course_id`
			WHERE
				trans_date < '{$this->params['trans_date']}' AND
				`deleted` = 0 AND
				`foreup_customers`.`course_id` IN ({$group_course_ids_string}) AND
				`foreup_account_transactions`.`course_id` IN ({$course_ids_string})
			GROUP BY
				`foreup_account_transactions`.`course_id`
		");
		$result = $result->result_array();
		return $result;
	}
}
?>
