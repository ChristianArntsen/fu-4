<?php
require_once("report.php");
class Summary_punch_cards extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns() {
		return array(
            array('data'=>lang('giftcards_punch_card_number'), 'align'=>'right'),
            array('data'=>lang('giftcards_punch_card_total_value'), 'align'=> 'left'),
            array('data'=>lang('giftcards_punch_card_redeemed'), 'align'=> 'left'),
            array('data'=>lang('giftcards_punch_card_remaining_value'), 'align'=> 'left'),
            array('data'=>lang('giftcards_punch_card_customer'), 'align'=> 'left'),
            array('data'=>lang('giftcards_punch_card_issue_date'), 'align'=> 'left'),
            array('data'=>lang('giftcards_punch_card_expiration_date'), 'align'=> 'left'),
            array('data'=>lang('giftcards_punch_card_total_punches'), 'align'=> 'left'),
            array('data'=>lang('giftcards_punch_card_punches_used'), 'align'=> 'left'));
	}
	
	public function getData() {
        $i = 0;
        $data = array();
		$this->db->select('foreup_punch_cards.punch_card_id, punch_card_number, SUM(foreup_punch_card_items.punches * foreup_items.unit_price) AS total_value, CONCAT(first_name, " ",last_name) AS customer_name, IF(expiration_date = "0000-00-00", "No Exp.", expiration_date) AS expiration_date, IF(issued_date = "0000-00-00", "No Issue Date", issued_date) AS issued_date, SUM(punches) AS punches, SUM(used) AS used', false);
		$this->db->from('punch_cards');
        $this->db->join('punch_card_items', 'punch_cards.punch_card_id = punch_card_items.punch_card_id');
        $this->db->join('items', 'punch_card_items.item_id = items.item_id');
		$this->db->join('people', 'punch_cards.customer_id = people.person_id', 'left');
		$this->db->where('foreup_punch_cards.deleted', 0);
		$this->db->where('foreup_punch_cards.course_id', $this->session->userdata('course_id'));
        $this->db->group_by('punch_cards.punch_card_id');
		$this->db->order_by('punch_card_number');
        $result = $this->db->get()->result_array();

        foreach($result as $record) {
            $data[$i] = $record;

            $data[$i]['sale_time'] = $data[$i]['issued_date'];//$result_sale[0]['sale_time'];
            
            $this->db->select('SUM(foreup_items.unit_price) AS redeemed', false);
            $this->db->from('punch_card_items');
            $this->db->join('items', 'punch_card_items.item_id = items.item_id');
            $this->db->join('sales_items', 'items.item_id = sales_items.item_id', 'left');
            $this->db->join('sales', 'sales_items.sale_id = sales.sale_id', 'left');
            $this->db->where('punch_card_items.punch_card_id', $record['punch_card_id']);
            $this->db->where('sales.deleted', 0);
            $this->db->like('payment_type', lang('sales_punch_card'));
            $this->db->like('payment_type', $record['punch_card_number']);
            $result_used = $this->db->get()->result_array();
            $data[$i]['redeemed'] = $result_used[0]['redeemed'];
            $i++;
        }
		return $data;
	}
}
?>