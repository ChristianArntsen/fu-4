<?php
require_once("report.php");
class Detailed_punch_cards extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(
			'summary' => array(
				array('data'=>lang('reports_sale_id'), 'align'=> 'left'), 
				array('data'=>lang('reports_date'), 'align'=> 'left'),
                array('data'=>lang('reports_payment_type'), 'align'=> 'right'),
                array('data'=>lang('reports_Amount'), 'align'=> 'right')
				),
			'details' => array(
				array('data'=>lang('reports_name'), 'align'=> 'left'), 
				array('data'=>lang('reports_category'), 'align'=> 'left'), 
				array('data'=>lang('reports_serial_number'), 'align'=> 'left'), 
				array('data'=>lang('reports_description'), 'align'=> 'left'), 
				array('data'=>lang('reports_qty'), 'align'=> 'left'), 
				array('data'=>lang('reports_subtotal'), 'align'=> 'right'), 
				array('data'=>lang('reports_total'), 'align'=> 'right'), 
				array('data'=>lang('reports_tax'), 'align'=> 'right'), 
				array('data'=>lang('reports_profit'), 'align'=> 'right'),
				array('data'=>lang('reports_discount'), 'align'=> 'right')
				)
			);		
	}
	
	public function getData()
	{
		$this->db->select('sp.sale_id, s.number, sale_time as sale_date, sp.payment_type, sp.payment_amount, comment', false);
		$this->db->from('sales_payments as sp');
        $this->db->join('sales as s', 's.sale_id = sp.sale_id', 'left');
        //$this->db->join('people as employee', 'sales_items_temp.employee_id = employee.person_id');
		//$this->db->join('people as customer', 'sales_items_temp.customer_id = customer.person_id', 'left');

		$this->db->where('deleted', 0);
		//$this->db->where('customer_id', $this->params['customer_id']);
		//$this->db->like('payment_type', lang('sales_punch_card'));
        //$this->db->like('payment_type', $this->params['punch_card_number']);
        $this->db->where('sp.payment_type', lang('sales_punch_card').':'.$this->params['punch_card_number']);
		$this->db->where('s.course_id', $this->config->item('course_id'));
        //$this->db->group_by('sale_id');
		$this->db->order_by('sale_date');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		//print_r($data['summary']);
		$data['details'] = array();
//		foreach($data['summary'] as $key=>$value)
//		{
//			$this->db->select('items.item_number as item_number, item_kits.item_kit_number as item_kit_number, items.name as item_name, item_kits.name as item_kit_name, sales_items_temp.category, quantity_purchased, serialnumber, sales_items_temp.description, subtotal,total, tax, profit, discount_percent');
//			$this->db->from('sales_items_temp');
//			$this->db->join('items', 'sales_items_temp.item_id = items.item_id', 'left');
//			$this->db->join('item_kits', 'sales_items_temp.item_kit_id = item_kits.item_kit_id', 'left');
//			$this->db->where('sale_id = '.$value['sale_id']);
//			$detail_results = $this->db->get();
//			$data['details'][$key] = $detail_results->result_array();
//		}
		return $data;
	}
	
	public function getSummaryData()
	{
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		$this->db->where('deleted', 0);
		//$this->db->where('customer_id', $this->params['customer_id']);
		$this->db->like('payment_type', lang('sales_punch_card'));
		$this->db->like('payment_type', $this->params['punch_card_number']);
		return $this->db->get()->row_array();
	}
}
?>