<?php
require_once("report.php");
class Summary_food_minimum extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns() {
		return array(array('data'=>lang('reports_customer_name'), 'align'=>'left'), array('data'=>lang('reports_total_spent'), 'align'=> 'left'), array('data'=>lang('reports_total_charged'), 'align'=> 'left'), array('data'=>lang('reports_minimum'), 'align'=> 'left'));
	}
	
	public function getData() {
		$i = 0;
        $data = array();   
		$start_date = date('Y-m-d', strtotime($this->params['start_date']));
		$end_date = date('Y-m-d', strtotime($this->params['end_date']));
    	$month = date('m', strtotime($this->params['start_date']));
    	$year =  date('Y', strtotime($this->params['start_date']));
		$start_of_last_month = date('Y-m-d H:i:s', strtotime("$start_date 00:00:00"));
		$start_of_this_month = date('Y-m-d H:i:s', strtotime("$end_date 00:00:00"));
		$where = '';
		if ($this->params['customer_id'] != 0) {
            $where .= " AND c.person_id = '{$this->params['customer_id']}'";
        }
		$customer_query = $this->db->query("
			SELECT c.person_id,c.course_id, CONCAT(first_name, ' ',last_name) as customer, course.minimum_food_spend, s.spent 
			FROM foreup_customers AS c 
			LEFT JOIN foreup_people AS p 
			ON c.person_id = p.person_id
			LEFT JOIN foreup_courses AS course 
			ON c.course_id = course.course_id
			LEFT JOIN (SELECT distinct(person_id), sum(spent) as spent FROM foreup_customer_food_spending WHERE month = {$month} AND year = {$year} group by `person_id`) AS s 
			ON s.person_id = c.person_id 
			WHERE c.deleted = 0 
			AND c.require_food_minimum = 1 
			AND course.minimum_food_spend > 0
			AND c.course_id = {$this->session->userdata('course_id')}
		 	$where 
		 	group by c.person_id
		 	HAVING s.spent IS NOT NULL
		");
        $result = $customer_query->result_array();
		foreach ($result as $record) {
			$data[$i] = $record;
			$this->db->select('SUM(foreup_sales_items.subtotal) AS total', false);
			$this->db->from('sales');
			$this->db->join('sales_items', 'sales_items.sale_id = sales.sale_id', 'left');
	        $this->db->join('items', 'items.item_id = sales_items.item_id', 'left');
			$this->db->where('sales.course_id', $record['course_id']);
			$this->db->where('sales.deleted', 0);
			$this->db->where('sales.customer_id', $record['person_id']);
			$this->db->where('sales.sale_time >=', $start_of_last_month);
			$this->db->where('sales.sale_time <', $start_of_this_month);
			$this->db->where('items.food_and_beverage', 1);
			$result_sale_items = $this->db->get();
            $result_sale_items = $result_sale_items->result_array();
			$data[$i]['total_charged'] = $result_sale_items[0]['total'];
			$i++;
		}
		return $data;
	}
}
?>