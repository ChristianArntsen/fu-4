<?php
require_once("report.php");
class Inventory_summary extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(
			'department' => array(
				array('data'=>lang('reports_dept').'/'.lang('reports_cat').'/'.lang('reports_subcat'), 'align'=>'left'),
				array('data'=>lang('reports_item_number'), 'align' => 'left'), 
				array('data'=>lang('reports_description'), 'align' => 'left'), 
				array('data'=>lang('reports_count'), 'align' => 'right'), 
				array('data' => lang('items_cost_price'), 'align' => 'right'),
				array('data' => lang('items_total_cost'), 'align' => 'right'),
				array('data' => lang('items_unit_price'), 'align' => 'right')
            ),
            'category' => array(
                array('data'=>lang('reports_category'), 'align'=> 'left'), 
				array('data'=>lang('reports_item_number'), 'align' => 'left'), 
				array('data'=>lang('reports_description'), 'align' => 'left'), 
				array('data'=>lang('reports_count'), 'align' => 'right'), 
				array('data' => lang('items_cost_price'), 'align' => 'right'),
				array('data' => lang('items_total_cost'), 'align' => 'right'),
				array('data' => lang('items_unit_price'), 'align' => 'right')
            ),
            'subcategory' => array(
                array('data'=>lang('reports_subcategory'), 'align'=> 'left'), 
				array('data'=>lang('reports_item_number'), 'align' => 'left'), 
				array('data'=>lang('reports_description'), 'align' => 'left'), 
				array('data'=>lang('reports_count'), 'align' => 'right'), 
				array('data' => lang('items_cost_price'), 'align' => 'right'),
				array('data' => lang('items_total_cost'), 'align' => 'right'),
				array('data' => lang('items_unit_price'), 'align' => 'right')
            )
        );		
	}
	
	public function getData($unknown_parameter='', $minus_teetimes=true)
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->select('name, item_number, quantity, cost_price, unit_price, reorder_level, description');
		$this->db->from('items');
		$this->db->where('deleted', 0);
        if ($minus_teetimes) {
            $this->db->where('category !=', 'Green Fees');
            $this->db->where('category !=', 'Carts');
        }
		if ($this->params['value'] != 'all')
		{
			$this->db->where($this->params['filter'], urldecode(urldecode($this->params['value'])));
		}
		$this->db->where('is_unlimited', 0);
		$this->db->order_by('name');
		$result = $this->db->get();
		
		return $result->result_array();

	}
	public function getData2($unknown_parameter='', $minus_teetimes=true)
	{
		//print_r($this->params);
		if ($this->params['filter'] == 'department' && $this->params['value'] != 'all')
			$department = ' AND department = "'.urldecode(urldecode($this->params['value'])).'" ';
		else if ($this->params['filter'] == 'category' && $this->params['value'] != 'all')
			$department = ' AND category = "'.urldecode(urldecode($this->params['value'])).'" ';
		else if ($this->params['filter'] == 'subcategory' && $this->params['value'] != 'all')
			$department = ' AND subcategory = "'.urldecode(urldecode($this->params['value'])).'" ';
		if ($this->params['supplier_id'])
			$supplier = ' AND supplier_id = '.$this->params['supplier_id'].' ';
		
		$minus = ($minus_teetimes) ? " AND category != 'Green Fees' AND category != 'Carts'" : '';
		//echo $this->db->last_query();
		//echo 'ready for reaching ';
		$dq = $this->db->query("SELECT department, name, item_number, quantity, cost_price, unit_price, reorder_level, description, SUM(cost_price*quantity) AS inventory_value,  SUM(unit_price * quantity) AS potential_value
							FROM ".$this->db->dbprefix('items')."
							WHERE ".$this->db->dbprefix('items').".deleted = 0
							$department
							$minus
							$supplier
							AND is_unlimited = 0
							AND course_id = ".$this->session->userdata('course_id')."
							GROUP BY department
							ORDER BY department",
							FALSE,
							TRUE,
							TRUE);
		//echo $this->db->last_query();
		$data = array();
		$data['dept_summary'] = $dq->result_array();
		$data['details'] = array();
		//echo 'get data';
		//print_r($data);
		foreach($data['dept_summary'] as $dept_key=>$dept_value)
		{
			$cq = $this->db->query("SELECT department, category, subcategory, name, item_number, quantity, cost_price, unit_price, reorder_level, description, SUM(cost_price*quantity) AS inventory_value,  SUM(unit_price * quantity) AS potential_value
							FROM ".$this->db->dbprefix('items')."
							WHERE ".$this->db->dbprefix('items').".deleted = 0
							AND department = '".addslashes($dept_value['department'])."'
							$department
							$supplier
							$minus
							AND is_unlimited = 0
							AND course_id = ".$this->session->userdata('course_id')."
							GROUP BY department, category
							ORDER BY department, category",
							FALSE,
							TRUE,
							TRUE);
			//echo $this->db->last_query();
			$data['summary'][$dept_key] = $cq->result_array();
			$data['details'][$dept_key] = array();
			foreach($data['summary'][$dept_key] as $key=>$value)
			{
				$scq = $this->db->query("SELECT subcategory, name, item_number, quantity, cost_price, unit_price, reorder_level, description, SUM(cost_price*quantity) AS inventory_value,  SUM(unit_price * quantity) AS potential_value
							FROM ".$this->db->dbprefix('items')."
							WHERE ".$this->db->dbprefix('items').".deleted = 0
							$supplier
							$minus
							$department
							AND department = '".addslashes($dept_value['department'])."'
							AND category = '".addslashes($value['category'])."'
							AND is_unlimited = 0
							AND course_id = ".$this->session->userdata('course_id')."
							GROUP BY department, category, subcategory
							ORDER BY department, category, subcategory",
							FALSE,
							TRUE,
							TRUE);
				$data['details'][$dept_key][$key] = $scq->result_array();
				$data['items'][$dept_key][$key] = array();
				
				foreach($data['details'][$dept_key][$key] as $dkey=>$dvalue) 
				{
					$iq = $this->db->query("SELECT name, item_number, quantity, cost_price, unit_price, reorder_level, description, SUM(cost_price*quantity) AS total_cost_price
							FROM ".$this->db->dbprefix('items')."
							WHERE ".$this->db->dbprefix('items').".deleted = 0
							$supplier
							$minus
							$department
							AND department = '".addslashes($dept_value['department'])."'
							AND category = '".addslashes($value['category'])."'
							AND subcategory = '".addslashes($dvalue['subcategory'])."'
							AND is_unlimited = 0
							AND course_id = ".$this->session->userdata('course_id')."
							GROUP BY department, category, subcategory, item_id
							ORDER BY department, category, subcategory",
							FALSE,
							TRUE,
							TRUE);
									
		 			$data['items'][$dept_key][$key][$dkey] = $iq->result_array();
				}
			}
		}
		return $data;
	}
	public function getSummaryData()
	{
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->select('sum(cost_price * quantity) as inventory_total,sum(unit_price * quantity) as inventory_sale_total');
		$this->db->from('items');
		if ($this->params['value'] != 'all')
		{
			$this->db->where($this->params['filter'], urldecode(urldecode($this->params['value'])));
		}
		if ($this->params['supplier_id'])
		{
			$this->db->where('supplier_id', $this->params['supplier_id']);
		}
		$this->db->where('category !=', 'Green Fees');
		$this->db->where('category !=', 'Carts');
		$this->db->where('deleted', 0);
		$this->db->where('is_unlimited', 0);
		$result = $this->db->get();
		//echo '<br/><br/>'.$this->db->last_query();
		return $result->row_array();
	}
}
?>