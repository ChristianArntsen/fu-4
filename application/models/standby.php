<?php
class Standby extends CI_Model
{
    function get_list($date = false)
    {
        if (!$date) {
            $date = date('Y-m-d');
        }
        else {
            $date = date('Y-m-d', strtotime($date));
        }

        if ($this->config->item('stack_tee_sheets')) {
            $this->load->model('teesheet');
            $course_id = $this->session->userdata('selected_stacked_course_id') ? $this->session->userdata('selected_stacked_course_id') : $this->session->userdata('course_id');
            $teesheets = $this->teesheet->get_all(100, 0, 0, $course_id)->result_array();
            foreach ($teesheets as $teesheet) {
                $teesheet_ids[] = $teesheet['teesheet_id'];
            }
            $this->db->where_in('teetime_standbys.teesheet_id', $teesheet_ids);
        }
        else {
            $teesheet_id = $this->session->userdata('teesheet_id');
            $this->db->where('teetime_standbys.teesheet_id', $teesheet_id);
        }
        $this->db->select('teesheet.title as tee_sheet_title, teetime_standbys.*');
        $this->db->from('teetime_standbys');
        $this->db->join('teesheet', 'teesheet.teesheet_id = teetime_standbys.teesheet_id', 'left');
        $this->db->where("time >= '$date 00:00:00' AND time < '$date 23:59:59' ORDER BY time ASC" );
		$standbys = $this->db->get();
        $standbys = $standbys->result_array();
        $standby_list = array();
        $count = 0;
        foreach ($standbys as $standby)
        {
            $standby_list[$count] = $standby;
            $standby_list[$count]['time'] = date("g:i a", strtotime($standby['time']));
            $count++;
        }
        return $standby_list;
    }
    function get_by_id($id)
    {
        
        if ($id === '')
            return '';
        
        $this->db->from('teetime_standbys');
        $this->db->where("standby_id = '$id'");
        $standby_entries = $this->db->get()->result_array();
        if(empty($standby_entries)){
            return null;
        }
        $standby_entry = $standby_entries[0];
        //log_message('error', 'Teetime details ' . urlencode($standby_entry['details']));
        //log_message('error', $standby_entry['name'] . " {$standby_entry['time']} {$standby_entry['players']} 
          //  {$standby_entry['holes']} {$standby_entry['details']} {$standby_entry['email']} {$standby_entry['phone']}");
        
        return $standby_entry;
      
    }
    function get_by_id2($id)
    {
        
        if ($id === '')
            return '';
        
        $this->db->from('teetime_standbys');
        $this->db->where("standby_id = '$id'");
        $standby_entries = $this->db->get()->result_array();
        $standby_entry = $standby_entries[0];
        //log_message('error', 'Teetime details ' . urlencode($standby_entry['details']));
        //log_message('error', $standby_entry['name'] . " {$standby_entry['time']} {$standby_entry['players']} 
          //  {$standby_entry['holes']} {$standby_entry['details']} {$standby_entry['email']} {$standby_entry['phone']}");
        
        return "?name=".urlencode($standby_entry['name']) ."&time=". urlencode($standby_entry['time']) . "&players=" .urlencode($standby_entry['players'])."&holes=".url_encode($standby_entry['holes']) . 
                "&details=" . urlencode($standby_entry['details']) . "&email=" . urlencode($standby_entry['email']) . "&phone=".urlencode($standby_entry['phone']) . "&id=" . urlencode($id) .
                "&tee_sheet_id=" . urlencode($standby_entry['teesheet_id']) . "&person_id=" . urlencode($standby_entry['person_id']);
        
      
    }
    function save($name, $email, $phone, $holes, $num_players, $time, $details, $person_id, $date = false, $tee_sheet_id = false)
    {
        if (!$date) {
            $date = date("Y-m-d");
        }
    	$teesheet_id = empty($tee_sheet_id) ? $this->session->userdata('teesheet_id') : $tee_sheet_id;
        //log_message('error', 'time is: ' . date('Y-m-d G:i', strtotime($time)));
        $h_z = '';
        if ($time < 1000) {
            $h_z = '0';
        }
        $this->db->query("INSERT INTO foreup_teetime_standbys (teesheet_id, person_id, name, email, phone, holes, players, time, details)
                                VALUES ('$teesheet_id', '$person_id', '$name', '$email', '$phone', '$holes', '$num_players', '". date('Y-m-d G:i', strtotime($date.' '.$h_z.$time))."', '$details')");
        
    }
    function edit($name, $email, $phone, $holes, $num_players, $time, $details, $id, $person_id, $date = false, $tee_sheet_id = false)
    {
        if (!$date) {
            $date = date("Y-m-d");
        }
        if (empty($tee_sheet_id)) {
            $tee_sheet_id = $this->session->userdata('teesheet_id');
        }

        $this->db->query("UPDATE foreup_teetime_standbys SET person_id='$person_id', name='$name', email='$email', phone='$phone', holes='$holes', players='$num_players', time='" . date('Y-m-d G:i', strtotime($date.' '.$time)) . "', details='$details', teesheet_id = '$tee_sheet_id' WHERE standby_id='$id'");
                                
        
    }
    function delete($id)
    {
        $this->db->query("DELETE FROM foreup_teetime_standbys WHERE standby_id='$id'");
    }
}