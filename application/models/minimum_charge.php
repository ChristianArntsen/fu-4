<?php
class Minimum_charge extends CI_Model {
	private $dryRun;

	public function __construct(){
		parent::__construct();
		$this->load->helper('array');
        $this->load->model('Household');
        $this->charge_count = 0;
	}

	public function setDryRun($dryrun){
		$this->dryRun = $dryrun;
	}

	public function get($params = array()){

		$params = elements(array('charge_id', 'course_id', 'is_active', 'customer_id'), $params, null);

		if(empty($params['course_id'])){
			$course_id = (int) $this->session->userdata('course_id');
		}

		if(!empty($params['charge_id'])){
			$this->db->where('minimum_charge_id', $params['charge_id']);
		}

		if(!empty($params['customer_id'])){
			$this->db->join('customer_minimum_charges', 'customer_minimum_charges.minimum_charge_id = minimum_charges.minimum_charge_id', 'inner');
			$this->db->where('customer_minimum_charges.person_id', (int) $params['customer_id']);
		}

		$this->db->select('*');
		$this->db->from('minimum_charges');
		$this->db->where('course_id', $course_id);
		$this->db->where('deleted', 0);

		if($params['is_active'] !== null){
			$this->db->where('is_active', (int)(bool) $params['is_active']);
		}

		$rows = $this->db->get()->result_array();

		foreach($rows as &$row){
			$item_filters = array();
			$row['item_filters'] = json_decode($row['item_filters'], true);
			
			foreach($row['item_filters'] as $type => $filters){
				foreach($filters as $filter){
					$item_filters[$type][] = $filter;
				}
			}
			$row['item_filters'] = $item_filters;
		}

		return $rows;
	}

	public function save($data, $charge_id = null){

		if(empty($data)){
			return false;
		}

		// Filter fields passed
		$data = elements(array('name', 'is_active', 'frequency', 'frequency_on', 
			'frequency_period', 'frequency_on_date', 'minimum_amount', 'departments', 
			'categories', 'sub_categories', 'start_date', 'end_date'), $data, null);

		$data['course_id'] = $this->session->userdata('course_id');
		$data['date_created'] = date('Y-m-d H:i:s');

		foreach($data as $field => $val){
			if($val === null){
				unset($data[$field]);
			}
		}

		$item_filters = array();
		$item_option_types = array('categories', 'sub_categories', 'departments');
		foreach($item_option_types as $type){
			if(empty($data[$type])){
				continue;
			}
			foreach($data[$type] as $filter){
				$item_filters[$type][] = base64_decode($filter);
			}
		}
		$data['item_filters'] = json_encode($item_filters);
		unset($data['departments'], $data['categories'], $data['sub_categories']);

		if(!empty($charge_id)){
			$this->db->update('minimum_charges', $data, array('minimum_charge_id' => $charge_id));
		
		}else{
			$this->db->insert('minimum_charges', $data);
			$charge_id = $this->db->insert_id();
		}

		return $charge_id;
	}

	public function save_customer_charges($customer_id, $minimum_charges = array()){

		if(empty($customer_id)){
			return false;
		}
		
		// Delete all previous charges before resaving the new ones
		$this->db->where('person_id', $customer_id);
		$this->db->delete('customer_minimum_charges');

		$charge_rows = array();
		if(!empty($minimum_charges)){
			
			foreach($minimum_charges as $charge){
				$charge['date_added'] = !isset($charge['date_added'])||empty($charge['date_added'])?date('Y-m-d'):$charge['date_added'];
				$charge_rows[] = array(
					'person_id' => $customer_id, 
					'minimum_charge_id' => $charge['minimum_charge_id'],
					'date_added' => $charge['date_added']
				);
			}
			
			if(!empty($charge_rows)){
				$this->db->insert_batch('customer_minimum_charges', $charge_rows);
			}
		}

		return true;
	}

	public function delete($charge_id){
		if(empty($charge_id)){
			return false;
		}
		return $this->db->update('minimum_charges', array('deleted' => 1), array('minimum_charge_id' => $charge_id));
	}

	public function charge_customers($course_id = false, $date = false,$manual_start = null){

		$this->charge_count = 0;

		// Default to today if no date passed
		if(empty($date)){
			$date = date('Y-m-d');
		}else{
			$date = date('Y-m-d', strtotime($date));
		}
		
		// Get charges that need ran on date
		$charges = $this->get_pending_minimum_charges($course_id, $date);
		if(empty($charges)){
			return false;
		}

		$charge_ids = array();
		foreach($charges as $charge){
			$charge_ids[] = (int) $charge['minimum_charge_id'];
		}

		$allCustomers = $this->get_pending_charge_customers($charge_ids);
		if(empty($allCustomers)){
			return false;
		}
		
		foreach($charges as $charge){
			
			if(empty($allCustomers[$charge['minimum_charge_id']])){
				continue;
			}

			// Customers needing charged
			$customers = $allCustomers[$charge['minimum_charge_id']];
			$filters = json_decode($charge['item_filters'], true);

			$charge_start = DateTime::createFromFormat('Y-m-d', $charge['start_date'])->setTime(0,0,0);
			$charge_start->setDate(date('Y'), $charge_start->format('m'), $charge_start->format('d'));

			$charge_end = new DateTime($date);
			$charge_end->setTime(23,59,59);
			$charge_end->setDate(date('Y'), $charge_end->format('m'), $charge_end->format('d'))->modify('-1 day');

			if(empty($manual_start)){
				if(!empty($charge['last_ran']) && $charge['last_ran'] != '0000-00-00 00:00:00'){
					$charge_start = DateTime::createFromFormat('Y-m-d H:i:s', $charge['last_ran'])->modify('+1 day')->setTime(0,0,0);

				}else if($charge_start->getTimestamp() > $charge_end->getTimestamp()){
					$charge_start = $charge_start->modify('-1 year');
				}
			} else {
				$charge_start = DateTime::createFromFormat('Y-m-d',$manual_start)->setTime(0,0,0);
			}

			$charge_start = $charge_start->format('Y-m-d H:i:s');
			$charge_end = $charge_end->format('Y-m-d H:i:s');

			// Get how much each customer has spent between charge start and end date
			$customer_spending = $this->get_total_spending($customers, $charge['course_id'], $filters, $charge_start, $charge_end);

			// Apply charges to each customer account
			$charges = $this->charge_customer_accounts($customer_spending, $charge['name'], $charge['minimum_charge_id'], $charge['minimum_amount']);

			if(!isset($this->dryRun) || !$this->dryRun){
				$this->db->update('minimum_charges', array(
					'last_ran' => $charge_end
				), array(
					'minimum_charge_id' => $charge['minimum_charge_id']
				));
			} else {
				return $charges;
			}
		}
	}

	// Apply charges to each customer's member accounts
	private function charge_customer_accounts($customers, $charge_name, $charge_id, $minimum_amount){
					
		if(empty($customers)){
			return false;
		}
		$this->load->model('Account_transactions');
		$charges = [];
		foreach($customers as $customer){

			// If customer spent a sufficient amount, do not charge them
			if((float) $minimum_amount <= 0 || (float) $customer['total'] >= (float) $minimum_amount ){
				if(!isset($this->dryRun) || !$this->dryRun)
					continue;
			}

			// Calculate how much they under spent
			$difference = $minimum_amount - $customer['total'];
            // Make sure we don't charge more than the minimum
            $difference = $difference > $minimum_amount ? $minimum_amount : $difference;

			$trans_details = $charge_name;
			$trans_description = date('m/d/Y', strtotime($customer['start_date'])).
				' to '.date('m/d/Y', strtotime($customer['end_date'])).
				' - Spent: '.to_currency($customer['total']).
				', Minimum: '. to_currency($minimum_amount);	

			$this->Account_transactions->minimum_charge_id = (int) $charge_id;

			if(!isset($this->dryRun) || !$this->dryRun){
				$transaction_id = $this->Account_transactions->save(
					'member',
					(int) $customer['customer_id'],
					$trans_details,
					-$difference,
					$trans_description,
					0,
					0,
					0,
					(int) $customer['course_id'],
					true,
					$customer['end_date']
				);
			
			}else{
				$charges[] = [
					"type"=>"member",
					"customer"=>$customer,
					"trans_details"=>$trans_details,
					"difference"=>-$difference,
					"transaction_date"=>$customer['end_date']
				];
			}

			$this->charge_count++;
		}
        $this->Account_transactions->minimum_charge_id = false;
		return $charges;
	}

	private function generate_item_filter_sql($column, $filter_list = false){
		$filter_sql = '';
		if(empty($filter_list)){
			return $filter_sql;
		}

		foreach($filter_list as &$filter){
			$filter = $this->db->escape($filter);
		}
		
		$filter_sql .= " {$column} IN (".implode(",", $filter_list).")";
		return $filter_sql;
	}

	public function get_current_spending($customers, $course_id, $charge_id)
	{
		$charge = $this->get([
			"charge_id" => $charge_id,
			"is_active" => 1
		]);
		if(empty($charge[0])){
			return false;
		}
		$charge = $charge[0];

		$charge_end = \Carbon\Carbon::now();
		$charge_end->addDay();
		$charge_end->endOfDay();

		if(!empty($charge['last_ran']) && $charge['last_ran'] != '0000-00-00 00:00:00'){
			$charge_start = \Carbon\Carbon::parse($charge['last_ran'])->endOfDay();
		} else {
			$charge_start = \Carbon\Carbon::parse($charge['start_date'])->year(\Carbon\Carbon::now()->year);
			if($charge_start->gt($charge_end)){
				$charge_start = $charge_start->modify('-1 year');
			}
		}

		if(!empty($charge['item_filters'])){
			return $this->minimum_charge->get_total_spending(
				$customers,
				$course_id,
				$charge['item_filters'],
				$charge_start->toDateTimeString(),
				$charge_end->toDateTimeString()
			);
		};
		return [];
	}

	public function get_items_purchased($person_id, $charge_id, $course_id, $date_added = null){

		if(empty($person_id) || empty($charge_id) || empty($course_id)){
			return false;
		}

		$charge = $this->get([
			"charge_id" => $charge_id,
			"is_active" => 1
		]);
		if(empty($charge[0])){
			return false;
		}
		$charge = $charge[0];

		$person_id = (int) $person_id;

		$charge_end = \Carbon\Carbon::now();
		$charge_end->addDay();
		$charge_end->endOfDay();

		if(!empty($charge['last_ran']) && $charge['last_ran'] != '0000-00-00 00:00:00'){
			$charge_start = \Carbon\Carbon::parse($charge['last_ran'])->endOfDay();
		} else {
			$charge_start = \Carbon\Carbon::parse($charge['start_date'])->year(\Carbon\Carbon::now()->year);
			if($charge_start->gt($charge_end)){
				$charge_start = $charge_start->modify('-1 year');
			}
		}

		if(!empty($date_added)){
			$date_added = Carbon\Carbon::parse($date_added);
			if($charge_start->diffInSeconds($date_added, false) > 0){
				//If date_Added > start_date
				$charge_start = $date_added;
			}
		}

		$item_filters = $charge['item_filters'];
        $item_filter_sql = 'AND (';
		$itemFilters = [];

		if(!empty($item_filters['departments'])){
			$itemFilters[] = $this->generate_item_filter_sql('i.department', $item_filters['departments']);
		}
		if(!empty($item_filters['categories'])){
			$itemFilters[] = $this->generate_item_filter_sql('i.category', $item_filters['categories']);
		}
		if(!empty($item_filters['sub_categories'])){
			$itemFilters[] = $this->generate_item_filter_sql('i.subcategory', $item_filters['sub_categories']);
		}
		$item_filter_sql .= implode(" OR ", $itemFilters);
		$item_filter_sql .= ')';

		$query = $this->db->query("SELECT i.item_id, i.name, i.category, i.department,
				si.quantity_purchased AS quantity, si.item_unit_price AS unit_price, si.subtotal,
				si.tax, si.total, s.sale_id, s.number AS sale_number, s.sale_time
			FROM foreup_sales AS s
			LEFT JOIN foreup_sales_items AS si
				ON si.sale_id = s.sale_id 
				AND si.invoice_id = 0
			LEFT JOIN foreup_items AS i
				ON i.item_id = si.item_id 
				{$item_filter_sql}
			WHERE i.item_id IS NOT NULL 
				AND (
					s.customer_id = {$person_id} OR
					s.customer_id IN (
						SELECT household_member_id
						FROM foreup_household_members as hm
						LEFT JOIN foreup_households AS h 
							ON h.household_id = hm.household_id
						WHERE household_head_id = {$person_id}
					)
				)
				AND s.course_id = {$course_id}
				AND s.deleted = 0
				AND s.sale_time >= '{$charge_start->toDateTimeString()}'
				AND s.sale_time < '{$charge_end->toDateTimeString()}'	
			GROUP BY si.sale_id, si.line
			ORDER BY s.sale_time DESC
			LIMIT 100");

		return $query->result_array();
	}

	// Retrieves total spent for each customer filtered by selected departments, categories or sub categories
	public function get_total_spending($customers, $course_id, $item_filters, $charge_start_date, $charge_end_date){
        $item_filter_sql = 'AND (';
		$itemFilters = [];

		if(!empty($item_filters['departments'])){
			$itemFilters[] = $this->generate_item_filter_sql('i.department', $item_filters['departments']);
		}
		if(!empty($item_filters['categories'])){
			$itemFilters[] = $this->generate_item_filter_sql('i.category', $item_filters['categories']);
		}
		if(!empty($item_filters['sub_categories'])){
			$itemFilters[] = $this->generate_item_filter_sql('i.subcategory', $item_filters['sub_categories']);
		}
		$item_filter_sql .= implode(" OR ",$itemFilters);
		$item_filter_sql .= ')';

		$customer_spending = array();

		// Total up spending by customer
		foreach($customers as $customer){
            $customer_id = (int) $customer['person_id'];
            $customer_array = [];
            $customer_array[] = $customer_id;

            // This should be used when getting a full list of all totals for a specific minimum_charge_id NOTE: Other changes should be made to accommodate this
            //$customer_string = "(SELECT person_id FROm foreup_customer_minimum_charges WHERE minimum_charge_id = {$minimum_charge_id}";
            $customer_string = "(".implode(',',$customer_array).")";

			$start_date = \Carbon\Carbon::parse($charge_start_date);
			$date_added = Carbon\Carbon::parse($customer['date_added']);
			$end_date = \Carbon\Carbon::parse($charge_end_date);

			if($start_date->diffInSeconds($date_added,false) > 0){
				//If date_Added for the customer (customer start date)> start_date for the charge
				$start_date = $date_added;
			}
			if($start_date->diffInSeconds($end_date,false) < 0){
				//Hasn't started yet
				continue;
			}

			// Added back the customers table in order to total households correctly
            // Left out the c.course_id in order to accommodate shared customer databases
            // In order to make the below query pull all customers from a specific minimum, replace customer_string with (SELECT person_id FROM foreup_customer_minimum_charges WHERE minimum_charge_id = 51)
            $query = $this->db->query("SELECT c.person_id as customer_id,
                    SUM(CASE WHEN i.item_id IS NULL THEN 0 ELSE IFNULL(si.subtotal,0) END) AS total,
                    '{$start_date->toDateTimeString()}' AS start_date, '{$end_date->toDateTimeString()}' AS end_date,
                    s.course_id,
                    people.last_name, people.first_name
                FROM foreup_customers AS c
                LEFT JOIN foreup_sales AS s
                    ON (s.customer_id IN
	                  (SELECT household_member_id FROM foreup_household_members as hm
		                LEFT JOIN foreup_households AS h ON h.household_id = hm.household_id
		                WHERE household_head_id = c.person_id
	                  ) OR s.customer_id = c.person_id
	                )
	                AND s.course_id = {$course_id}
	                AND s.deleted = 0
	                AND s.sale_time >= '{$start_date->toDateTimeString()}'
	                AND s.sale_time < '{$end_date->toDateTimeString()}'
	            LEFT JOIN foreup_sales_items AS si
	                ON si.sale_id = s.sale_id
	                AND si.invoice_id = 0
	            LEFT JOIN foreup_people AS people
                	ON c.person_id = people.person_id
                LEFT JOIN foreup_items AS i
	                ON i.item_id = si.item_id
	                {$item_filter_sql}
	            WHERE c.person_id IN
    	            {$customer_string}
    	            AND c.course_id = {$course_id}
                GROUP BY c.person_id
            ");

			$row = $query->row_array();

			if(!empty($row)){
				$row['course_id'] = $course_id;
				$customer_spending[] = $row;
			
			}else{
				$customer_spending[] = [
					"customer_id" => $customer_id,
					"total" => 0,
					"start_date" => $start_date,
					"end_date" => $end_date,
					"course_id" => $course_id
				];
			}
		}
		
		return $customer_spending;
	}

	// Get list of customers that need charged for each minimum charge plan
	public function get_pending_charge_customers($charge_ids,$customer_ids = false,$limit_by_date = true){
		
		if(empty($charge_ids)){
			return false;
		}

		$query = $this->db->select('minimum_charge_id, person_id, date_added')
			->from('customer_minimum_charges')
			->where_in('minimum_charge_id', $charge_ids);
		if($limit_by_date){
			$this->db->where("date_added <=",date('Y-m-d H:i:s'));
		}


		if($customer_ids)
		{
			$this->db->where_in("person_id",$customer_ids);
		}

		$rows = $query->get()->result_array();
		$customers = array();
		foreach($rows as $row){
			$customers[(int) $row['minimum_charge_id']][] = array(
				'person_id' => (int) $row['person_id'],
				'date_added' => $row['date_added']
			);
		}

		return $customers;
	}

	// Get list of minimum charges that need ran
	public function get_pending_minimum_charges($course_id = false, $date = false){

		// Default to today if no date passed
		if(empty($date)){
			$date = date('Y-m-d');
		}else{
			$date = date('Y-m-d', strtotime($date));
		}

		$dayOfWeek = (int) date('N', strtotime($date));
		$dayOfMonth = (int) date('j', strtotime($date));
		$monthDays = (int) date('t', strtotime($date));
		$month = (int) date('n', strtotime($date));

		$filters = array();

		// Always pull daily charges
		$filters[] = "(charge.frequency_period = 'day')";

		// If charge set for this day of week
		$filters[] = "(charge.frequency_period = 'week' AND charge.frequency_on = {$dayOfWeek})";

		// If charge set for this date of month
		$filters[] = "(charge.frequency_period = 'month'
			AND charge.frequency_on = 'date' AND DAY(charge.frequency_on_date) = {$dayOfMonth})";

		// If charge set for this date of the year
		$filters[] = "(charge.frequency_period = 'year' AND charge.frequency_on = 'date'
			AND DAY(charge.frequency_on_date) = {$dayOfMonth} AND MONTH(charge.frequency_on_date) = {$month})";

		// If end of month
		if($dayOfMonth == $monthDays){
			$filters[] = "(charge.frequency_period = 'month' AND charge.frequency_on = 'end')";
		}
		// If beginning of month
		if($dayOfMonth == 1){
			$filters[] = "(charge.frequency_period = 'month' AND charge.frequency_on = 'begin')";
		}
		// If beginning of year
		if($dayOfMonth == 1 && $month == 1){
			$filters[] = "(charge.frequency_period = 'year' AND charge.frequency_on = 'begin')";
		}
		// If end of year
		if($dayOfMonth == $monthDays && $month == 12){
			$filters[] = "(charge.frequency_period = 'year' AND charge.frequency_on = 'end')";
		}
		$filterSql = implode(' OR ', $filters);
		$date_clean = $this->db->escape($date);

		$date_parts = explode('-', $date);
		$month_day = '0000-'.$date_parts[1].'-'.$date_parts[2];

		$course_sql = '';
		if ($course_id){
			$course_sql = " AND charge.course_id = {$course_id}";
		}

        $query = $this->db->query("SELECT charge.minimum_charge_id, 
        		charge.name,
        		charge.course_id, charge.last_ran,
				charge.frequency, charge.frequency_period, 
				charge.frequency_on,
				charge.frequency_on_date,
				charge.minimum_amount,
				charge.item_filters,
				charge.start_date,
				charge.end_date
		    FROM foreup_minimum_charges AS charge
	        WHERE charge.deleted = 0
				AND charge.is_active = 1
				AND (
						(charge.start_date <= '{$month_day}' AND charge.end_date >= '{$month_day}') OR
						(charge.start_date > charge.end_date AND 
							(charge.start_date <= '{$month_day}' OR charge.end_date >= '{$month_day}')
						)
					)
				AND ({$filterSql})
				{$course_sql}
    	    GROUP BY charge.minimum_charge_id");
		
		$rows = $query->result_array();

		if($this->dryRun){
			return $rows;
		}
        // Loop through the rows and filter out charges that haven't
		// passed enough time to since last time it was ran
		foreach($rows as $key => $row){

			$frequency = (int) $row['frequency'];
			$period = $row['frequency_period'];

			// If set to every period, or charge hasn't been ran
			// before, keep it in the list
			if(empty($row['last_ran']) || $row['last_ran'] == '0000-00-00'){
				$course_array[$row['course_id']] = 1;
				continue;
			}

			// Figure out how many days have passed since last run
			$last_generation = DateTime::createFromFormat('Y#m#d H#i#s', $row['last_ran']);
			$curdate = DateTime::createFromFormat('Y#m#d', $date);
			$days_since_last = (int) abs($curdate->diff($last_generation, true)->format('%a'));

			$days_between = 0;
			if($period == 'week'){
				$days_between = $frequency * 7;
			}else if($period == 'month'){
				$days_between = $frequency * 28;
			}else if($period == 'year'){
				$days_between = $frequency * 365;
			}else{
				$days_between = $frequency;
			}

			// Remove charge from list if it hasn't had enough time
			// since it was last ran
			if($days_since_last < $days_between){
				unset($rows[$key]);
			}
		}

		return $rows;		
	}
}
?>