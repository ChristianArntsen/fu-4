<?php
namespace models;
class payment_lock extends \CI_Model
{
    private $cart_id,$amount,$identifier,$course_id;



    public function __construct()
    {
        $this->course_id = $this->session->userdata("course_id");
    }

    public function init_by_cart_amount($cart_id,$amount)
    {
        $this->cart_id = $cart_id;
        $this->amount = $amount;
    }

    public function init_by_invoice($invoice)
    {
        $query = $this->db->from("payment_locks")
            ->where("course_id",$this->course_id)
            ->where("invoice_id",$invoice)->get();
        $results = $query->result("models\\PaymentLock\\Row");
        if(count($results) ==0){
            return false;
        }

        $this->identifier = $results[0]->identifier;
        return $this;
    }

    public function init_by_payment_id($payment_id)
    {
        $query = $this->db->from("payment_locks")
            ->where("course_id",$this->course_id)
            ->where("payment_id",$payment_id)->get();
        $results = $query->result("models\\PaymentLock\\Row");
        if(count($results) ==0){
            return false;
        }

        $this->identifier = $results[0]->identifier;
        return $this;
    }


    public function check_payment_lock()
    {
        if($this->get_identifier() == ""){
            return false;
        }

        $identifier = $this->get_identifier();
        $query = $this->db->from("payment_locks")
            ->where("course_id",$this->course_id)
            ->where("identifier",$identifier)
            ->where("time_close",null)->get();
        $results = $query->result("models\\PaymentLock\\Row");

        if(count($results) ==0){
            return false;
        }
        return $results[0];
    }

    public function create_lock($invoice_id,$payment_id)
    {
        $lock = new \models\PaymentLock\Row();
        $lock->identifier =$this->get_identifier();
        $lock->invoice_id = $invoice_id;
        $lock->payment_id = $payment_id;
        $lock->course_id = $this->course_id;
        $lock->time_open = \Carbon\Carbon::now()->toDateTimeString();
        return $this->db->insert('payment_locks',$lock);
    }



    public function release_lock()
    {
        $data = [
            "time_close"=>\Carbon\Carbon::now()->toDateTimeString()
        ];
        return $this->db
            ->where("identifier",$this->get_identifier())
            ->update("payment_locks",$data);
    }

    private function get_identifier()
    {
        if(empty($this->identifier))
            return "{$this->cart_id}{$this->amount}";
        return $this->identifier;
    }

}