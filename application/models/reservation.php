<?php
class Reservation extends CI_Model
{
	/*
	Determines if a given reservation_id is an teetime
	*/
	function exists($reservation_id)
	{
		$this->db->from('reservations');
		$this->db->where("reservation_id", $reservation_id);
		// Should maybe be a schedule id
		//$this->db->where('track_id', $this->input->post('teesheet_id'));
		$this->db->limit(1);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}

	/*
	Returns all the teetimes
	*/
	function get_all($limit=10000, $offset=0)
	{
        $course_id = '';
		$this->db->from('reservations');
        $this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where("deleted", 0);
        $this->db->order_by("start", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	function get_teetime_stats()
	{
		// TODO: Rewrite this function for reservations
		$results = $this->db->query("SELECT count(reservation_id) AS total, COUNT(DISTINCT foreup_teesheet.course_id) AS booking_courses, SUM(IF(details LIKE '%online booking%', 1, 0)) AS online FROM foreup_reservations JOIN foreup_teesheet ON foreup_teesheet.teesheet_id = foreup_reservations.teesheet_id WHERE reservation_id LIKE '____________________' AND status != 'deleted'");
		return $results->result_array();
	}
	function get_teetime_course_stats()
	{
		// TODO: Rewrite this function for reservations
		$return_array = array();
		$this->load->model('Communication');
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-'.$this->Communication->days_in_month(date('m')));
		$pm_start_date = date('Y-m-01', strtotime($start_date.' -1 month'));
		$pm_end_date = date('Y-m-'.$this->Communication->days_in_month(date('m', strtotime($start_date.' -1 month'))), strtotime($start_date.' -1 month'));
		$results = $this->db->query("SELECT foreup_courses.name as name, foreup_courses.course_id AS course_id, count(reservation_id) AS total, SUM(IF(details LIKE '%online booking%', 1, 0)) AS online, 
			sum(IF (date_booked >= '$start_date' AND date_booked <= '$end_date' AND details LIKE '%online booking%', 1, 0 )) AS online_this_month, 
			sum(IF (date_booked >= '$pm_start_date' AND date_booked <= '$pm_end_date' AND details LIKE '%online booking%', 1, 0 )) AS online_last_month,
			sum(IF (date_booked >= '$start_date' AND date_booked <= '$end_date', 1, 0 )) AS reserved_this_month, 
			sum(IF (date_booked >= '$pm_start_date' AND date_booked <= '$pm_end_date', 1, 0 )) AS reserved_last_month 
			FROM foreup_reservations 
			JOIN foreup_teesheet ON foreup_teesheet.teesheet_id = foreup_reservations.teesheet_id 
			JOIN foreup_courses ON foreup_courses.course_id = foreup_teesheet.course_id 
			WHERE reservation_id LIKE '____________________' AND status != 'deleted' 
			GROUP BY foreup_courses.course_id ORDER BY foreup_courses.name");
		//echo $this->db->last_query();
		//while ($result = $results->fetch_assoc())
		foreach($results->result_array() as $result)
			$return_array[$result['course_id']] = $result;
//echo "<br/>Teetime<br/>";
//print_r($return_array);
		return $return_array;
	}
	function get_play_length_data($start_date, $end_date, $teesheet_id)
	{
		// TODO: Rewrite this function for reservations
		$results = $this->db->query("SELECT teed_off_time, turn_time, finish_time, TIMESTAMPDIFF(MINUTE, teed_off_time, turn_time) AS half_time, TIMESTAMPDIFF(MINUTE, teed_off_time, finish_time) AS total_time FROM foreup_reservations
			WHERE teed_off_time >= '$start_date 00:00:00' AND teed_off_time <= '$end_date 23:59:59' AND reservation_id LIKE '____________________' AND status != 'deleted'
			AND teesheet_id = $teesheet_id AND (turn_time != '0000-00-00 00:00:00' OR finish_time != '0000-00-00 00:00:00')
			ORDER BY teed_off_time");
			
		return $results->result_array();
	}
	function count_all($last_week=false)
	{
		// TODO: Rewrite this function for reservations
		$this->db->from('reservations');
	    $this->db->join('tracks', 'reservations.track_id=tracks.track_id');
	    $this->db->where('course_id', $this->session->userdata('course_id'));
	    if($last_week === true)
	        $this->db->where('YEARWEEK(date_booked) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)');
	    return $this->db->count_all_results();
	}
	
	/*
	Gets information about a particular teetime
	*/
	function get_info($reservation_id)
	{
		$this->db->from('reservations');
		$this->db->where("reservation_id", $reservation_id);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
			return $query->row();
		else
		{
			//Get empty base parent object, as $reservation_id is NOT a teetime
			$teetime_obj=new stdClass();
			//Get all the fields from items table
			$fields = $this->db->list_fields('reservations');
			foreach ($fields as $field)
				$teetime_obj->$field='';

			return $teetime_obj;
		}
	}
	/*
	Gets information about a particular teetime
	*/
	function get_detailed_info($reservation_id)
	{
		$this->db->from('reservations');
		$this->db->join('tracks', "tracks.track_id = reservations.track_id");
		$this->db->where("reservation_id", $reservation_id);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
			return $query->row();
		else
		{
			//Get empty base parent object, as $reservation_id is NOT a teetime
			$teetime_obj=new stdClass();
			//Get all the fields from items table
			$fields = $this->db->list_fields('reservations');
			foreach ($fields as $field)
				$teetime_obj->$field='';

			return $teetime_obj;
		}
	}
	function switch_sides($reservation_id)
	{
		// TODO: Rewrite this function for reservations
		$main_id = substr($reservation_id, 0, 20);
		$this->db->query("UPDATE foreup_reservations SET side = CASE WHEN side = 'front' THEN 'back' ELSE 'front' END WHERE reservation_id like '{$main_id}%'");
		//echo $this->db->last_query();
		echo json_encode(array());
	}
	function mark_as_teedoff($reservation_id)
	{
		$main_id = substr($reservation_id, 0, 20);
		$date = date('Y-m-d H:i:s');
		$this->db->query("UPDATE foreup_reservations SET status = CASE WHEN `status` = 'deleted' THEN 'deleted' ELSE 'teed off' END, teed_off_time = '$date' WHERE reservation_id like '{$main_id}%'");
		//echo $this->db->last_query();
		$this->db->from('foreup_reservations');
		$this->db->like('reservation_id', "{$main_id}", 'after');
		$this->db->where('status !=', 'deleted');
		$this->db->limit(2);
		$result = $this->db->get();
		$reservations = $result->result_array();
		$json_ready_events = array();
		foreach ($reservations as $reservation)
			$json_ready_events[] = $this->add_teetime_styles($reservation);
		
		return $json_ready_events;
	}
	function mark_as_turned($reservation_id)
	{
		$main_id = substr($reservation_id, 0, 20);
		$date = date('Y-m-d H:i:s');
		$this->db->query("UPDATE foreup_reservations SET turn_time = '$date' WHERE reservation_id like '{$main_id}%'");
		//echo $this->db->last_query();
		$this->db->from('foreup_reservations');
		$this->db->like('reservation_id', "{$main_id}", 'after');
		$this->db->where('status !=', 'deleted');
		$this->db->limit(2);
		$reservations = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($reservations as $reservation)
			$json_ready_events[] = $this->add_teetime_styles($reservation);
		
		return $json_ready_events;
	}
	function mark_as_finished($reservation_id)
	{
		$main_id = substr($reservation_id, 0, 20);
		$date = date('Y-m-d H:i:s');
		$this->db->query("UPDATE foreup_reservations SET finish_time = '$date' WHERE reservation_id like '{$main_id}%'");
		//echo $this->db->last_query();
		$this->db->from('foreup_reservations');
		$this->db->like('reservation_id', "{$main_id}", 'after');
		$this->db->where('status !=', 'deleted');
		$this->db->limit(2);
		$reservations = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($reservations as $reservation)
			$json_ready_events[] = $this->add_teetime_styles($reservation);
		
		return $json_ready_events;
	}
	function check_in($reservation_id, $count)
	{
		$main_id = substr($reservation_id, 0, 20);
		$this->db->query("UPDATE foreup_reservations SET paid_player_count = paid_player_count + $count WHERE reservation_id like '{$main_id}%' LIMIT 2");
		
		$this->db->from('foreup_reservations');
		$this->db->like('reservation_id', "{$main_id}", 'after');
		$this->db->where('status !=', 'deleted');
		$this->db->limit(2);
		$reservations = $this->db->get()->result_array();
		$json_ready_events = array();
		foreach ($reservations as $reservation)
			$json_ready_events[] = $this->add_teetime_styles($reservation);
		
		return $json_ready_events;
	}
	/*
	Checks whether an existing time is still available before saving 
	*/
	function check_availability($reservation_data)
	{
		if ($this->session->userdata('reservations'))
		{
			//Check if closed or an event
			$this->db->from('reservations');
			$this->db->where('status !=', 'deleted');
			$this->db->where('track_id', $reservation_data['track_id']);
			$this->db->where("start <=", $reservation_data['start']);
			$this->db->where('end >', $reservation_data['start']);
			$this->db->limit(1);
			$event_result = $this->db->get();	
			//echo $this->db->last_query();
			if ($event_result->num_rows() > 0)
				return false;
			else 
				return true;
		}	
		else 
		{
			//added this to allow the API to use this function as well as online booking
			if (!$reservation_data['track_id']) {
				$reservation_data['track_id'] = $this->input->post('track_id');
			}
			//Check if closed or an event
			$this->db->from('reservations');
			$this->db->where('status !=', 'deleted');
			$this->db->where('track_id', $reservation_data['track_id']);
			$this->db->where('side', 'front');
			$this->db->where("type !=", 'reservation');
			$this->db->where("start <=", $reservation_data['start']);
			$this->db->where('end >', $reservation_data['start']);
			$this->db->limit(1);
			$event_result = $this->db->get();	
			
			//Check if teetimes spots are available
			$this->db->select('sum(player_count) as total_players');
			$this->db->from('reservations');
			$this->db->where('status !=', 'deleted');
			$this->db->where('track_id', $reservation_data['track_id']);
			$this->db->where('side', 'front');
			$this->db->where("(start = {$reservation_data['start']} OR (start < {$reservation_data['start']} AND end > {$reservation_data['start']}))");
			$this->db->group_by('type');
			$result = $this->db->get()->result_array();		
			
			if ($event_result->num_rows() > 0 OR (isset($result[0]) && ($result[0]['total_players'] + $reservation_data['player_count'] > 4)))
				return false;
			else 
				return true;
		}
	}
	function update_purchased_teetime()
	{
		// TODO: UPDATE this function for reservations
		$reservation_id = $this->session->userdata('teetime_to_purchase');
		$reservation_info = $this->get_info($reservation_id);
		$players = $this->session->userdata('players_to_purchase');
		$carts = $this->session->userdata('carts_to_purchase');
		$invoice_id = $this->session->userdata('invoice_id');
		$json = array();
		$cart_count = ($carts == 1)?$players:0;
		$data = array(
			'reservation_id'=>$reservation_id,
			'track_id'=>$teetime_info->track_id,
			'start'=>$reservation_info->start,
			'end'=>$reservation_info->end,
			'holes'=>$reservation_info->holes,
			'date_booked'=>$reservation_info->date_booked,
			'booker_id'=>$reservation_info->booker_id,
			'player_count'=>$players, 
			'paid_player_count'=>$players, 
			'carts'=>$cart_count, 
			'paid_carts'=>$cart_count,
			'booking_source'=>'online',
			'details'=>'Reserved and Paid For using online booking @ '.date('g:ia n/j T')
		);

		$this->save($data, $reservation_id, $json);
		//echo $this->db->last_query();
		$data['invoice_id'] = $invoice_id;
		unset($data['paid_player_count']);
		unset($data['paid_carts']);
		unset($data['booking_source']);
		unset($data['details']);
		unset($data['reservation_id']);
		//Save all original data in our teetimes_bartered table
		return $this->db->insert('teetimes_bartered', $data);
		//echo $this->db->last_query();
	}
	/*
	Inserts or updates a teetime
	*/
	function save(&$reservation_data,$reservation_id=false, &$json_ready_events=array(), $online_booking = false)
	{
		$this->load->model('track');
		$track_info = $this->track->get_info($reservation_data['track_id']);
		if ($online_booking)
		{
			if (!$this->check_availability($reservation_data))
				return false;
			//TODO: Check if already booked maximum per individual
		}
		if ($reservation_id)
			$cur_reservation_data = $this->get_info($reservation_id);
		else 
			$reservation_id = $this->generate_id('tt');
		$second_id = $reservation_id.'b';
	    if (strlen($reservation_id) > 20) 
            $second_id = substr($reservation_id, 0, 20);
		$second_data = array();
			
		$frontnine = $this->session->userdata('frontnine');
		$dif = fmod((floor($frontnine/100)*60+$frontnine%100), $this->session->userdata('increment'));
		$frontnine += ($dif > $this->session->userdata('increment')/2)?($this->session->userdata('increment') - $dif):-$dif;
			
		if (!empty($reservation_data['booking_source']) && $reservation_data['booking_source'] == 'online') //Not yet adjusting for simulators
		{
			$second_data = $reservation_data;
			$reservation_data['reservation_id'] = $reservation_id;
			$second_data['reservation_id'] = $second_id;
			$second_data['side'] = 'back';
			$second_data['start'] += $frontnine;
        	$second_data['end'] += $frontnine;
        
			if ($second_data['start']%100 >59)
	            $second_data['start'] += 40;
	        if ($second_data['end']%100 >59)
	            $second_data['end'] += 40;
				
			if (!$track_info->reround_track_id || $this->session->userdata('is_simulator') || $reservation_data['holes'] == 9)
				$second_data['status'] = 'deleted';
			
		}
		else {
			// Set the id of the two tee times
	        $course_holes = $this->session->userdata('holes');
	        $start = $end = '';
			if (isset($reservation_data['start']))
				$start = $reservation_data['start'];
			else if (isset($cur_reservation_data['start']))
				$start = $cur_reservation_data['start'];
	        if (isset($reservation_data['end']))
				$end = $reservation_data['end'];
			else if (isset($cur_reservation_data['end']))
				$end = $cur_reservation_data['end'];
	        
	        $second_data = $reservation_data;
			$second_data['reservation_id'] = $second_id;
			if (!isset($reservation_data['status']) && isset($reservation_data['holes']) && $reservation_data['holes'] == 18)
				$second_data['status'] = $cur_reservation_data->status;
			$success = true;
			// Erasing second tee time if there is one since we're only playing 9
	        if (isset($reservation_data['holes']) && $reservation_data['holes'] == 9) {
	            if (strlen($reservation_id) > 20)
					$reservation_data['status'] = 'deleted';
				else 
					$second_data['status'] = 'deleted';
			}
	        // Updating secondary tee time
	        $second_side = 'back';
	        if ((isset($cur_reservation_data->side) && $cur_reservation_data->side =='back') || 
		        	(isset($reservation_data['side']) && $reservation_data['side'] == 'back') || 
		        	$course_holes == 9) 
	            $second_side = 'front';
	        
	        if (strlen($reservation_id) > 20) {
	            unset($second_data['start']);
			    unset($second_data['end']);
			}
			else {
				$second_data['start'] = $start + $frontnine;
	        	$second_data['end'] = $end + $frontnine;
	        
				if ($second_data['start']%100 >59)
		            $second_data['start'] += 40;
		        if ($second_data['end']%100 >59)
		            $second_data['end'] += 40;
			}
	    	$second_data['side'] = $second_side;
		}
        //print_r($reservation_data);
		/*
		 * SAVE REROUND RESERVATION
		 */
		if ($track_info->reround_track_id && $reservation_data['side'] == 'front')
		{
			$second_data['track_id'] = $track_info->reround_track_id;
			if (!$second_id or !$this->exists($second_id))
			{
				$second_data['date_booked'] = gmdate('Y-m-d H:i:s');
	        	if($this->db->insert('reservations',$second_data))
	        		$success = true;
				else
		        	$success = false;
			}
			else 
			{
				$this->db->where('reservation_id', $second_id);
				$success = $this->db->update('reservations',$second_data);
				$json_ready_events[] = $this->add_teetime_styles(array_merge($second_data, (array)$this->get_info($second_data['reservation_id'])));
			}
		}
		/*
		 * END SAVE REROUND RESERVATION
		 */
		/*
		 * SAVE PRIMARY RESERVATION
		 */
		$reservation_data['reservation_id'] = $reservation_id;
		$json_ready_events[] = $this->add_teetime_styles(array_merge((array)$cur_reservation_data, $reservation_data));
        if (!$this->exists($reservation_id))
		{
			$reservation_data['date_booked'] = gmdate('Y-m-d H:i:s');
			//print_r($reservation_data);
			if(isset($reservation_data['start']))
				$reservation_data['start_datetime'] = \fu\Teetime::convertTeetimeToDatetime($reservation_data['start']);
			if(isset($reservation_data['end']))
				$reservation_data['end_datetime'] = \fu\Teetime::convertTeetimeToDatetime($reservation_data['end']);

			if($this->db->insert('reservations',$reservation_data))
        		$saved = true;
			else
				$saved = false;
		}
		else {
			if(isset($reservation_data['start']))
				$reservation_data['start_datetime'] = \fu\Teetime::convertTeetimeToDatetime($reservation_data['start']);
			if(isset($reservation_data['end']))
				$reservation_data['end_datetime'] = \fu\Teetime::convertTeetimeToDatetime($reservation_data['end']);

			$this->db->where('reservation_id', $reservation_id);
			$saved = $this->db->update('reservations',$reservation_data);
		}

		/*
		 * END SAVE PRIMARY RESERVATION
		 */
		$send_confirmation = '';
		if ($saved && !empty($reservation_data['person_id']) &&
			(!isset($reservation_data) || (!isset($reservation_data['confirmation_emailed']) || !$reservation_data['confirmation_emailed']) && 
			(!isset($cur_reservation_data) || !isset($cur_reservation_data->confirmation_emailed) || !$cur_reservation_data->confirmation_emailed))
			 && $this->input->post('send_confirmation_emails'))
		{
			$send_confirmation =  $reservation_id;
		}

		return array('success'=>$saved, 'send_confirmation'=>$send_confirmation);
	}
	function search($customer_id='')
	{
		$teetime_html = '';
		if ($customer_id == '')
			$teetime_html = "<div class='teetime_result'>No results</div>";
		else {
			$this->db->from("reservations as r");
            $this->db->join('tracks as t','t.track_id = r.track_id', 'left');
			$current_day = (date('Ymd')-100).'0000';
			$teesheet_id = $this->session->userdata('schedule_id');
			$this->db->where("reservation_id LIKE '____________________' AND status != 'deleted' AND t.schedule_id = $teesheet_id AND start >= $current_day AND (person_id = $customer_id OR person_id_2 = $customer_id OR person_id_3 = $customer_id OR person_id_4 = $customer_id OR person_id_5 = $customer_id)");
			$teetimes = $this->db->get()->result_array();
			foreach ($teetimes as $teetime)
			{
				$date = date("F j, Y, g:i a", strtotime($teetime['start']+1000000));
				$teetime_html .= "<div class='teetime_result'><div>{$date}</div>";//<div>Players: {$teetime['player_count']}</div></div>";
			}
		}	
		return $teetime_html;
	}
	function send_confirmation_email($email, $subject, $data, $from_email, $from_name)
	{
		send_sendgrid(
			$email,
			$subject,
			$this->load->view("email_templates/reservation_made",$data, true),
			$from_email,
			$from_name
		);
		$reservation_id = substr($data['reservation_id'], 0, 20);
		$this->db->query("UPDATE foreup_reservations SET confirmation_emailed = 1 WHERE reservation_id LIKE '$reservation_id%' LIMIT 2");
	}

	function add_teetime_styles($reservation_data) {
		//echo 'adding teetime styles';
		//return $reservation_data;
		$return_data = array();
		$return_data['id'] = $reservation_data['reservation_id'];
		$return_data['track_id'] = $reservation_data['track_id'];
		$return_data['type'] = $reservation_data['type'];
		$return_data['name'] = $this->schedule->clean_for_json($reservation_data['title']);
		$return_data['status'] = $reservation_data['status'];
		$return_data['title'] = $this->schedule->clean_for_json($reservation_data['title']);
		$return_data['allDay'] = '';
		$return_data['stimestamp'] = $reservation_data['start'];
		$return_data['etimestamp'] = $reservation_data['end'];
        $return_data['start'] = $reservation_data['start'];
		$return_data['end'] = $reservation_data['end'];
        $return_data['side'] = $reservation_data['side'];
		$return_data['player_count'] = $reservation_data['player_count'];
		$return_data['carts'] = $reservation_data['carts'];
		$return_data['backgroundColor']= '';
		if ($reservation_data['holes'] == 18)
			$return_data['borderColor']= '';
		else
			$return_data['borderColor']= '';
		//Add classes for styling
		$booked_class = $cart_class = $checkedin_class = $teed_off_class = '';
		if (!empty($reservation_data['booking_source']))
			$booked_class = 'booked_online';
        if (!empty($reservation_data['carts']))
            $cart_class = 'carts';
        if (($reservation_data['status'] == 'checked in' || $reservation_data['status'] == 'walked in') && !$this->config->item('sales'))
            $checkedin_class = 'checked_in';
		if($reservation_data['status'] == 'teed off')
			$teed_off_class = 'teed_off';
		if ($reservation_data['turn_time'] != '0000-00-00 00:00:00')
			$teed_off_class .= ' mark_turn';
		if ($reservation_data['finish_time'] != '0000-00-00 00:00:00')
			$teed_off_class .= ' mark_finished';
		
		$pc = ($reservation_data['player_count']>5)?5:$reservation_data['player_count'];
		$ppc = ($reservation_data['paid_player_count']>$pc)?$pc:$reservation_data['paid_player_count'];
		
	    if ($ppc == 0 || ($reservation_data['type'] != 'teetime' && $reservation_data['type'] != 'reservation'))
            $paid_class = '';
		else if ($this->config->item('simulator'))
			$paid_class =  "paid_4_4";
	    else 
            $paid_class = "paid_{$ppc}_{$pc}";
		$return_data['className'] = $reservation_data['type'].' holes_'.$reservation_data['holes'].' players_'.$reservation_data['player_count'].' '.$cart_class.' '.$checkedin_class.' '.$paid_class.' '.$teed_off_class;

		return $return_data;
		
//                $return_data['start'] = $this->format_time_string($teetime->start);
//                $return_data['end'] = $this->format_time_string($teetime->end);
            
        /*if ($reservation_data->type == 'teetime' && ($reservation_data->status == 'checked in' || $reservation_data->status == 'walked in')) {
            $return_data['backgroundColor'] = '#5c9ccc';
            $return_data['borderColor'] = '#4297d7';
        }
        else if ($reservation_data->type == 'tournament') {
            $return_data['backgroundColor'] = '#5f9b5f';
            $return_data['borderColor'] = '#336133';
        }
        else if ($reservation_data->holes == 18) {
            $return_data['borderColor'] = '#333';
            $return_data['backgroundColor'] = '#445651';
        }*/
        
		
        
	}
	function change_date($reservation_id, $new_date, $new_end_date, $back_new_date, $back_new_end_date, $teesheet_id = '')
	{
		$teesheet_id = ($teesheet_id != '') ? $teesheet_id : $this->session->userdata('schedule_id');
		//$reservation_info = $this->get_info($reservation_id)->result_array();
		$reservation_id = substr($reservation_id, 0, 20);
		$new_date = date('Ymd', strtotime($new_date.' -1 month'));
		//$json_ready_events = array();
		//$reservation_info[''];
		$primary_reservation_id = substr($reservation_id, 0, 20);
		$secondary_reservation_id = $primary_reservation_id.'b';
		//$this->db->query("UPDATE foreup_reservations SET start = CONCAT('{$new_date}', SUBSTRING(start, 9)), end = CONCAT('{$new_date}', SUBSTRING(end, 9)) WHERE TTID = '{$primary_reservation_id}%' LIMIT 1");
		$this->db->query("UPDATE foreup_reservations SET start = '{$new_date}', end = '{$new_end_date}', teesheet_id = '{$teesheet_id}' WHERE reservation_id = '{$primary_reservation_id}' LIMIT 1");
		return $this->db->query("UPDATE foreup_teetime SET start = '{$back_new_date}', end = '{$back_new_end_date}', teesheet_id = '{$teesheet_id}' WHERE reservation_id = '{$secondary_reservation_id}' LIMIT 1");
	}
	/*
	Deletes one teetime
	*/
	function delete($reservation_id, $person_id = '')
	{
		if ($person_id != '')
		{
			//If the teetime is being deleted by a golfer online
			//$this->db->where('booker_id', $person_id);
			$this->db->where('paid_player_count', 0);
		}
		else 
			//If the teetime is being deleted from an employee using the system
			$person_id = $this->session->userdata('person_id');

		if ($person_id != '')
	    {
	    	$this->db->where("reservation_id", substr($reservation_id,0,20));
			$this->db->or_where("reservation_id", substr($reservation_id,0,20).'b');
			$this->db->limit(2);
		
		    $this->db->update('reservations', array('status' => 'deleted', 'canceller_id' => $person_id, 'date_cancelled' => date('Y-m-d H:i:s')));
			return $this->db->affected_rows() > 0;
		}
		else 
			return false;
    }
	function generate_id($type = 'tt') {
        if ($type == 'tt')
            $prefix = date('mdHis');
        else if ($type == 'ts')
            $prefix = 'teesheet_id_';
        else
            $prefix = '';
       
        $length = 20 - strlen($prefix);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $string = $prefix;
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters)-1)];
        }
        return $string;
    }
	function get_stats($view, $year, $month, $day, $dow) {
        $brand = $this->session->userdata("teesheet_id");
        $statsArray = array();
        $eday = $day+1;
        if ($month < 10)
            $month = "0".$month;
        if ($day < 10)
            $day = "0".$day;
        if ($eday < 10)
            $eday = "0".$eday;
        if ($view == 'agendaDay') {
            //get one day stats
            
            $dayString = $year.$month.$day.'0000';
            $edayString = $year.$month.$eday.'0000';
			if ($this->config->item('sales'))
	            $this->db->select("choles,
	                count(*) AS teetimes,
	                sum(case WHEN `status` = 'checked in' THEN 1 ELSE 0 END) AS tcheckin,
	                sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
	                sum(`player_count`) AS players,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `paid_player_count` ELSE 0 END) AS cplayers,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `paid_carts` ELSE 0 END) AS ccarts,
	                sum(`carts`) AS carts");
			else
	            $this->db->select("choles,
	                count(*) AS teetimes,
	                sum(case WHEN `status` = 'checked in' THEN 1 ELSE 0 END) AS tcheckin,
	                sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
	                sum(`player_count`) AS players,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `player_count` ELSE 0 END) AS cplayers,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `carts` ELSE 0 END) AS ccarts,
	                sum(`carts`) AS carts");
            $this->db->from('teetime');
            $this->db->where("teesheet_id = '$brand'
                    AND `start` > '$dayString' 
                    AND `end` < '$edayString'
                    AND `status` != 'deleted'
                    AND `reservation_id` NOT LIKE '____________________b'");
            $this->db->group_by('choles');
            
            $results = $this->db->get();
            foreach($results->result() as $result) {
                if ($result->choles == '9')
                    $statsArray['nine'] = $result;
                else if ($result->choles == '18')
                    $statsArray['eighteen'] = $result;
            }
        }
        else if ($view == 'agendaWeek') {
            //get full week stats
        }
        return json_encode($statsArray);
    }

    /**
     * get all online bookings associated with each course id (for non superadmin)
     */
    public function get_online_bookings_count($last_week = false)
    {
      $course_id = '';
      if (!$this->permissions->is_super_admin())
          $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";

      $this->db->from('teetime');
      $this->db->join('teesheet', 'teetime.teesheet_id=teesheet.teesheet_id');
      $this->db->where("lower(booking_source) = 'online' {$course_id}");

      if($last_week === true)
        $this->db->where('YEARWEEK(date_booked) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)');
      
      return $this->db->count_all_results();
    }

    /**
     *
     */
    public function get_bookings($limit = false)
    {
      $this->db->select('teetime.date_booked, teetime.start, teetime.end, teetime.player_count, teetime.holes');
      $this->db->from('teetime');
      $this->db->join('teesheet', 'teetime.teesheet_id=teesheet.teesheet_id');
      $this->db->where('status != \'delete\'');
      if($limit !== false) $this->db->limit($limit);
      $this->db->order_by('date_booked', 'desc');

      $query = $this->db->get();

      if($query->num_rows() > 0) return $query->result_array();
      else return array();
    }
}
?>
