<?php

use fu\credit_cards\Processors\Apriva\PaxInterpreter;

class Pax_device_model extends CI_Model {
    protected $devicePort = 10009;
    protected $deviceIP;
    protected $certURL = 'localpax.apriva.com';
    //protected $certURL = 'econduitapps.com';

    function __construct()
    {
        parent::__construct();

    }

    public function get_by_terminal($terminal_id) {
        if (empty($terminal_id)) {
            return false;
        }

        $this->db->from('apriva_devices');
        $this->db->where('terminal_id', $terminal_id);
        $this->db->where('deleted', 0);
        $this->db->limit(1);

        return $this->db->get()->row_array();
    }

    public function getDevicePort() {
        return $this->devicePort;
    }

    function save($device_data, $device_id = false) {
        $response = array('success'=>false);

        if (!empty($device_data) && $device_id) {
            $this->db->where('apriva_device_id', $device_id);
            $this->db->where('course_id', $this->config->item('course_id'));
            $response['success'] = $this->db->update('apriva_devices', $device_data);
        } else {
            $data = 'INSERT IGNORE INTO foreup_apriva_devices (course_id) VALUES';
            $data .= ' (' . $this->config->item('course_id') . ')';

            $response['success'] = $this->db->query($data);
        }

        return $response;
    }

    function get_all() {
        $this->db->from('apriva_devices');
        $this->db->where('course_id', $this->config->item('course_id'));
        $this->db->where('deleted', 0);

        return $this->db->get()->result_array();
    }

    function openSwipeWindow($payment_data, $terminal_id) {
        $PaxInterpreter = new PaxInterpreter();
        // Clean up amount

        $data['payment_data'] = $payment_data;
        $data['deviceIP'] = ''; //$this->getDeviceIP($terminal_id);//"10.86.183.118";//$this->getDeviceIP($params);
        // Currently uses cert URL to make all calls to card terminal... IP Address not used
        $data['certURL'] = $this->certURL;
        $data['devicePort'] = $this->getDevicePort();
        $data['terminal_id'] = $terminal_id;
        $data['action'] = 'sale';

        // Initiate Swipe Payment
        $payment_data['trans_amount'] = $this->formatAmount($payment_data['trans_amount']);
        $data['command_array'] = json_encode($PaxInterpreter->generateCommandArray($payment_data));

        // Generate data to check original transaction
        $trans_data = array(
            'command_type' => 'get_transaction',
            'edc_type' => 'all',
            'transaction_type' => $payment_data['transaction_type'],
            'ref_no' => $payment_data['ref_no']
        );
        $data['check_command_array'] = json_encode($PaxInterpreter->generateCommandArray($trans_data));

        // Generate data to void original transaction
        $trans_data = array(
            'command_type' => 'credit',
            'transaction_type' => 'void',
            'ref_no' => $payment_data['ref_no'],
            'transaction_number' => 'XX'
        );
        $data['void_command_array'] = json_encode($PaxInterpreter->generateCommandArray($trans_data));


        $this->load->view('v2/credit_cards/PaxInitialize', $data);

    }
    
    function adjustTip($payment_data, $terminal_id) {
        $PaxInterpreter = new PaxInterpreter();
        // Clean up amount

        $data['payment_data'] = $payment_data;
        $data['certURL'] = $this->certURL;
        $data['devicePort'] = $this->getDevicePort();
        $data['terminal_id'] = $terminal_id;
        $data['action'] = 'sale';

        // Initiate Swipe Payment
        $payment_data['trans_amount'] = $this->formatAmount($payment_data['trans_amount']);
        $data['command_array'] = json_encode($PaxInterpreter->generateCommandArray($payment_data));
var_dump($data);
        $this->load->view('v2/credit_cards/PaxInitialize', $data);

    }

    function voidTransaction($payment_data, $terminal_id) {
        $PaxInterpreter = new PaxInterpreter();
        // Clean up amount

        $data['payment_data'] = $payment_data;
        $data['certURL'] = $this->certURL;
        $data['devicePort'] = $this->getDevicePort();
        $data['terminal_id'] = $terminal_id;
        $data['action'] = 'sale';

        // Initiate Swipe Payment
        $payment_data['trans_amount'] = $this->formatAmount($payment_data['trans_amount']);
        $data['command_array'] = json_encode($PaxInterpreter->generateCommandArray($payment_data));
        var_dump($data);
        $this->load->view('v2/credit_cards/PaxInitialize', $data);

    }


    function formatAmount($amount) {
        return str_replace(array('$',',','.'), array('','',''), (string) $amount);
    }

}