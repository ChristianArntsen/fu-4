<?php
use Carbon\Carbon;

class Customer_model extends MY_Model {

	function __construct(){

		parent::__construct();
		$this->course_ids = array($this->session->userdata('course_id'));
        $this->course_id = false;

		$this->fields = array(
			'first_name' => 		array('name' => 'First Name', 'required' => true,"teesheet_required"=>false,"online_booking_required"=>false),
			'last_name' => 			array('name' => 'Last Name', 'required' => true,"teesheet_required"=>false,"online_booking_required"=>false),
			'email' => 				array('name' => 'Email', 'required' => false,"teesheet_required"=>false,"online_booking_required"=>false),
			'cell_phone_number' => 	array('name' => 'Cell Phone', 'required' => false),
			'phone_number' => 		array('name' => 'Phone', 'required' => false,"teesheet_required"=>false,"online_booking_required"=>false),
			'birthday' => 			array('name' => 'Birthday', 'required' => false),
			'address_1' => 			array('name' => 'Address', 'required' => false),
			'city' => 				array('name' => 'City', 'required' => false),
			'state' =>				array('name' => 'State', 'required' => false),
			'zip' => 				array('name' => 'Zip', 'required' => false,"teesheet_required"=>false,"online_booking_required"=>false),
			'country' => 			array('name' => 'Country', 'required' => false),
			'account_number' =>		array('name' => 'Account Number', 'required' => false)
		);

		$this->teetime_data = false;
        $this->send_booking_credentials = true;
	}

	// Search item database and return matched customer IDs
	private function search_customers($search,$force_fuzzy_search = false){
		$original_search = $search;
		$search = str_replace(array('(', ')', '-', '_',','), '',$search);
		// If search is an account number
		if(strlen($search) > 8 && is_numeric($search) && !$force_fuzzy_search){
			$this->readOnlyDB->or_where('account_number', $search);
			$this->readOnlyDB->or_where('account_number', $original_search);
		// If just a standard search
		}else{
	        $this->readOnlyDB->where(
				"(first_name LIKE '%".$this->db->escape_like_str($original_search)."%' OR last_name LIKE '%".$this->db->escape_like_str($original_search)."%' ".
				"OR CONCAT(first_name,' ',last_name) LIKE '%".$this->db->escape_like_str($original_search)."%' ".
				"OR phone_number LIKE '%".$this->db->escape_like_str($search)."%' OR email LIKE '%".$this->db->escape_like_str($original_search)."%'".
				"OR cell_phone_number LIKE '".$this->db->escape_like_str(str_replace(array('(', ')', '-', '_',',',' '), '',$search))."%'".
				" OR account_number LIKE '%".$this->db->escape_like_str($search)."%'".
				" OR account_number LIKE '%".$this->db->escape_like_str($original_search)."%')"
	        );
		}

		$this->readOnlyDB->select('people.person_id');
		$this->readOnlyDB->from('people');
		$this->readOnlyDB->join('customers', 'customers.person_id = people.person_id', 'inner');
		$this->readOnlyDB->where("customers.deleted", 0);
		$this->readOnlyDB->where("customers.hide_from_search", 0);
		$this->readOnlyDB->where_in('customers.course_id', $this->course_ids);
		$this->readOnlyDB->order_by("last_name, first_name", "ASC");
		$this->readOnlyDB->limit(30);
		$rows = $this->readOnlyDB->get()->result_array();

		if(count($rows) == 0 && !$force_fuzzy_search){
			return $this->search_customers($search,true);
		}

		$ids = array();
		foreach($rows as $row){
			if(empty($row['person_id'])){
				continue;
			}
			$ids[] = (int) $row['person_id'];
		}

		return $ids;
	}

	private function get_recent_sale_ids($customer_id = false){

		if(empty($customer_id)){
			return false;
		}
		
		if(!is_array($customer_id)){
			$customer_id = [$customer_id];
		}
		$customer_id = array_unique($customer_id);

		$course_id = (int) $this->session->userdata('course_id');
		$sale_ids = [];

		foreach($customer_id as $cust_id){
			
			if(empty($cust_id)){
				continue;
			}

			$rows = $this->db->select('sale_id')
				->from('foreup_sales')
				->where('customer_id', (int) $cust_id)
				->where('course_id', (int) $course_id)
				->order_by('sale_time', 'DESC')
				->limit(3)
				->get()->result_array();

			if(empty($rows)){
				continue;
			}

			foreach($rows as $row){
				$sale_ids[] = (int) $row['sale_id'];
			}			
		}

		return $sale_ids;
	}

	private function get_gift_card_ids($customer_id = false){
		
		if(empty($customer_id)){
			return false;
		}

		if(!is_array($customer_id)){
			$customer_id = [$customer_id];
		}

		$course_ids = array();
		$this->load->model('course');
		$this->course->get_linked_course_ids($course_ids, 'shared_giftcards', $this->session->userdata('course_id'));
		$gift_card_ids = [];

		$rows = $this->db->select('giftcard_id')
			->from('foreup_giftcards')
			->where_in('customer_id', $customer_id)
			->where_in('course_id', $course_ids)
			->get()->result_array();

		if(empty($rows)){
			return false;
		}

		foreach($rows as $row){
			$gift_card_ids[] = (int) $row['giftcard_id'];
		}

		return $gift_card_ids;
	}


	public function get($params){

	    if($this->course_id){
            $course_id = $this->course_id;
        }else{
            $course_id = $this->session->userdata('course_id');
        }

		$this->load->model('Course');
		$this->load->model('Pass');
		
		$course_ids = array();
		$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);		
		$this->course_ids = $course_ids;
		$course_id_list = implode(',', $this->course_ids);

		if(!empty($params['q'])){
			$ids = $this->search_customers($params['q']);

			if(empty($ids)){
				return array();
			}
		}

		// Select list of customers with photos, groups, and passes
		$this->db->select("c.person_id, c.taxable, c.discount, p.first_name, p.last_name, p.email, p.handicap_account_number, p.handicap_score,
			p.phone_number, p.cell_phone_number, p.birthday,c.date_created, p.address_1, p.address_2,
			p.city, p.state, p.zip, p.country, p.comments, c.image_id, c.account_number,
			c.account_balance, c.account_balance_allow_negative, c.member_account_balance,
			c.account_limit, c.member_account_limit, c.invoice_email,
			c.member_account_balance_allow_negative, c.invoice_balance, c.status_flag,
			c.invoice_balance_allow_negative, c.use_loyalty, c.loyalty_points, c.price_class,
			c.member, c.require_food_minimum, user.email AS username, c.opt_out_text,
			c.opt_out_email, c.unsubscribe_all, marketing_texting.texting_status,c.course_id,
			image.image_id,
			IF(
				ISNULL(image.image_id),
				'',
				CONCAT('".$this->config->item("urls")["files"]."/', c.course_id, '/thumbnails/', image.filename)
			) AS photo,

			GROUP_CONCAT(DISTINCT pass.pass_id) AS pass_ids,
			GROUP_CONCAT(DISTINCT(minimum_charges.minimum_charge_id)) AS minimum_charge_ids,

			GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				customer_group.group_id,
				customer_group.label
			) SEPARATOR 0x1E) AS groups
			", false);
		$this->db->from('people AS p');
		$this->db->join('customers AS c', 'c.person_id = p.person_id', 'inner');
		$this->db->join('images AS image', 'image.image_id = c.image_id', 'left');
		$this->db->join('pass_customers AS pass_customer', "pass_customer.person_id = p.person_id", 'left');
		$this->db->join('passes AS pass', "pass.pass_id = pass_customer.pass_id AND pass.course_id IN ({$course_id_list})", 'left');
		$this->db->join('customer_group_members AS group_member', 'group_member.person_id = c.person_id', 'left');
		$this->db->join('customer_groups AS customer_group', "customer_group.group_id = group_member.group_id AND customer_group.course_id IN ({$course_id_list})", 'left');
		$this->db->join('foreup_users AS user', 'user.person_id = c.person_id', 'left');
		$this->db->join('marketing_texting', 'p.person_id = foreup_marketing_texting.person_id AND c.course_id = foreup_marketing_texting.course_id', 'left');
		$this->db->join('customer_minimum_charges AS minimum_charges', "minimum_charges.person_id =  p.person_id", 'left');
		$this->db->where('c.deleted', 0);
		$this->db->where('c.hide_from_search', 0);
		$this->db->where_in('c.course_id', $this->course_ids);
		$this->db->group_by('c.person_id');

		if(!empty($params['person_id'])){
			$ids = [];
			if(is_array($params['person_id'])){
				foreach($params['person_id'] as $person_id){
					$ids[] = (int) $person_id;
				}
				$this->db->where_in('c.person_id', array_unique($ids));

			}else{
				$this->db->where('c.person_id', (int) $params['person_id']);
				$ids = [(int) $params['person_id']];
			}
		}
		$ids = array_unique($ids);

		if(!empty($params['q'])){
			$this->db->where_in("c.person_id", $ids);
		}

		if(isset($params['member'])){
			$this->db->where('member', (int) $params['member']);
		}
		if(isset($params['limit'])){
			$this->db->limit(1);
		}

		$rows = $this->db->get()->result_array();
        $recent_transactions = [];

        if(!empty($params['include_recent_transactions']) && !empty($ids)){
			
			$this->load->model('v2/Sale_model');
			$sale_ids = $this->get_recent_sale_ids(array_unique($ids));
			if(!empty($sale_ids)){
				$sales = $this->Sale_model->get([
					'sale_id' => $sale_ids
				]);

				if(!empty($sales)){
					foreach($sales as $sale){
						$recent_transactions[(int) $sale['customer_id']][] = $sale;
					}
				}
				unset($sales);
			}
		}

		$gift_cards = [];
		if(!empty($params['include_gift_cards']) && !empty($ids)){
			
			$this->load->model('v2/Giftcard_model');
			$gift_card_ids = $this->get_gift_card_ids(array_unique($ids));

			$gift_card_data = $this->Giftcard_model->get([
				'giftcard_id' => $gift_card_ids
			]);
			
			if(!empty($gift_card_data)){
				foreach($gift_card_data as $gift_card){
					$gift_cards[(int) $gift_card['customer_id']][] = $gift_card;
				}
			}
			unset($gift_card_data);			
		}

        $rainchecks = [];
        if(!empty($params['include_rainchecks']) && !empty($ids)){

            $this->load->model('v2/Raincheck_model');
            $raincheck_data = $this->Raincheck_model->get([
                'person_id' => array_unique($ids)
            ]);

            if(!empty($raincheck_data)){
                foreach($raincheck_data as $raincheck){
                    $rainchecks[(int) $raincheck['customer_id']][] = $raincheck;
                }
            }
            unset($raincheck_data);
        }

        $billings = [];
        $invoices = [];
        if(!empty($params['include_billing']) && !empty($ids)){

            $this->load->model('Customer_billing');
            $this->load->model('Invoice');

            if($ids){
                foreach($ids as $id){
                    $billings[(int) $id] = $this->Customer_billing->get_all($id)->result_array();
                    $invoices[(int) $id] = $this->Invoice->get_all($id)->result_array();
                    $invoices[(int) $id] = $this->Invoice->get_all($id)->result_array();
                }
            }
        }

        $account_transactions = [];
        if(!empty($params['include_account_transactions']) && !empty($ids)){

            $this->load->model('Account_transactions');

            if($ids){
                foreach($ids as $id){
                    $account_transactions[(int) $id]['customer'] = $transactions = $this->Account_transactions->get_transactions(
                        'customer',
                        $id,
                        array('limit'=>10)
                    );
                    $account_transactions[(int) $id]['member'] = $transactions = $this->Account_transactions->get_transactions(
                        'member',
                        $id,
                        array('limit'=>10)
                    );
                }
            }
        }

        $last_visits = [];
        if(!empty($params['include_last_visit']) && !empty($ids)){

            $this->load->model('v2/Sale_model');

            if($ids){
                foreach($ids as $key => $id){
                    $sale_params = array(
                        'customer_id' => $id,
                        'limit' => 1
                    );
                    $last_sale = $this->Sale_model->get($sale_params);
                    if (!empty($last_sale[0])) {
                        $last_visits[(int)$id] = $last_sale[0]['sale_time'];
                    } else {
                        $last_visits[(int)$id] = '';
                    }


					if($key != 0){
						continue;
					}
	                //Last Tee Time
	                $last_teetime = $this->db->query("
	                	SELECT MAX(start_datetime) as time FROM foreup_teetime
			                WHERE
			                status != 'deleted' AND
		                	(person_id = $id OR person_id_2 = $id OR person_id_3 = $id OR person_id_4 = $id OR person_id_5 = $id) 
		                	AND 
		                	start_datetime < '".Carbon::now()->toDateString()."'
		                	"
		                )->row();
	                //Next Tee Time
	                $next_teetime = $this->db->query("
	                	SELECT MIN(start_datetime) as time FROM foreup_teetime
			                WHERE
			                status != 'deleted' AND
		                	(person_id = $id OR person_id_2 = $id OR person_id_3 = $id OR person_id_4 = $id OR person_id_5 = $id) 
		                	AND 
		                	start_datetime > '".Carbon::now()->addDay(1)->toDateString()."'
		                	"
	                    )->row();
	                //Last Purchased Item
	                $last_purchase = $this->db->from("sales")
		                ->select("items.name, sales.sale_time")
		                ->where("customer_id",$id)
		                ->join("sales_items","sales.sale_id = sales_items.sale_id","left")
		                ->join("items","items.item_id = sales_items.item_id","left")
		                ->where("sales_items.type = 'item'")
	                    ->order_by("sale_time")->get()->row();
	                //Most Purchased
	                $most_purchase = $this->db->from("sales")
		                ->select("count(*) as total,items.name")
		                ->where("customer_id",$id)
		                ->join("sales_items","sales.sale_id = sales_items.sale_id","left")
		                ->join("items","items.item_id = sales_items.item_id","left")
		                ->where("sales_items.type = 'item'")
		                ->order_by("total","DESC")
		                ->group_by("sales_items.item_id")->get()->row();
	                //Current Tee Time
	                $todays_teetimes = $this->db->query("
	                	SELECT * FROM foreup_teetime
			                WHERE
			                status != 'deleted' AND
		                	(person_id = $id OR person_id_2 = $id OR person_id_3 = $id OR person_id_4 = $id OR person_id_5 = $id) 
		                	AND 
		                	start_datetime > '".Carbon::now()->startOfDay()->toDateTimeString()."'
		                	AND
		                	start_datetime < '".Carbon::now()->endOfDay()->toDateTimeString()."'
		                	"
	                )->result();
	                foreach($todays_teetimes as $teetime_today){
	                	if($teetime_today->reround == 1){
	                		$current_reround = $teetime_today;
		                } else {
	                		$current_teetime = $teetime_today;
		                }
	                }

	                $customer_details = [
	                    "prev_teetime"=>!empty($last_teetime->time)?Carbon::parse($last_teetime->time)->format("M j, Y h:i a"):"None",
	                    "next_teetime"=>!empty($next_teetime->time)?Carbon::parse($next_teetime->time)->format("M j, Y h:i a"):"None",
	                    "most_purchased"=>!empty($most_purchase->name)?$most_purchase->name . " ".$most_purchase->total."x":"None",
	                    "last_purchased"=>!empty($last_purchase->name)?$last_purchase->name." ".Carbon::parse($last_purchase->sale_time)->format("M j"):"None",
		                "teetime_today"=>false
	                ];
	                if(!empty($current_teetime)){
	                	$customer_details = array_merge($customer_details,[
	                		"teetime_today"=>true,
			                "todays_teetime"=>Carbon::parse($current_teetime->start_datetime)->format("M j, Y h:i a"),
			                "todays_reround"=>!empty($current_reround) ? Carbon::parse($current_reround->start_datetime)->format("M j, Y h:i a") : "No Reround",
			                "cart_numbers"=>$current_teetime->carts > 0 ? !empty($current_teetime->cart_num_1)? $current_teetime->cart_num_1." - ".$current_teetime->cart_num_2: "Not Recorded"  : "Walking",
		                ]);
	                }


                }
            }


        }

        $marketing_campaigns = [];
		if(!empty($params['include_marketing']) && !empty($ids)){
            $this->load->model('marketing_campaign');
            $emails = [];
            foreach ($rows as $person) {
                $emails[] = $person['email'];

            }
            $marketing_data = $this->marketing_campaign->get_customer_stats($emails);
            if(!empty($marketing_data)) {
                foreach($rows as $person) {
                    $marketing_campaigns[(int) $person['person_id']]['recent_campaigns'] =
                        empty($marketing_data['recent_campaigns'][$person['email']]) ? array() : $marketing_data['recent_campaigns'][$person['email']];
                    $marketing_campaigns[(int) $person['person_id']]['last_open_campaign'] =
                        empty($marketing_data['last_open_campaigns'][$person['email']]) ? array() : $marketing_data['last_open_campaigns'][$person['email']];
                }
            }
        }

        $credit_cards = [];
        if(!empty($params['include_credit_cards']) && !empty($ids)){
            $this->load->model('Customer_credit_card');
            $credit_card_data = $this->Customer_credit_card->get_multiple(array_unique($ids));
            if(!empty($credit_card_data)) {
                foreach($credit_card_data as $credit_card) {
                    $credit_card['card_type_simple'] = $this->Customer_credit_card->get_simple_type($credit_card);
                    $credit_cards[(int) $credit_card['customer_id']][] = $credit_card;
                }
            }
        }

        $recurring_charges = [];
        if(!empty($ids)){
            $recurring_charges = $this->get_recurring_charges(array_unique($ids));
        }

		$households = $this->get_household_members($ids);
        $household_heads = $this->get_household_heads($ids);
        $household_memberships = $this->is_household_member($ids);

		// Gather up each customer's pass and minimum IDs
		$pass_ids = [];
		$minimum_ids = [];
		foreach($rows as &$row){
			
			if(!empty($row['pass_ids'])){
				$customer_pass_ids = explode(',', $row['pass_ids']);
				$pass_ids = array_merge($pass_ids,$customer_pass_ids);
				$row['pass_ids'] = $customer_pass_ids;
			}

			if(!empty($row['minimum_charge_ids'])){
				$row['minimum_charge_ids'] = explode(",", $row['minimum_charge_ids']);



				// Loop through each minimum id, break apart fields
				foreach($row['minimum_charge_ids'] as $minimum_charge_id){
					if(empty($minimum_charge_id)){
						continue;
					}
					$minimum_ids[] = (int) $minimum_charge_id;
				}
			}
            if(empty($row['photo'])){
                $row['photo'] = 'https://www.gravatar.com/avatar/'.md5(strtolower(trim($row['email']))).'?r=pg&d='. urlencode(base_url()."images/profiles/Customer_photo_icon.png");
            }
		}
		$minimum_ids = array_unique($minimum_ids);
		$this->load->model('minimum_charge');
		foreach($minimum_ids as $key=>$id){
			$charge = $this->minimum_charge->get([
				"charge_id"=>$id
			]);
			if(empty($charge)){
				unset($minimum_ids[$key]);
			}
		}
		$minimum_ids = array_values($minimum_ids);
		$row['minimum_charge_ids'] = $minimum_ids;

		// Get minimum spending details
		$minimum_spending = $this->get_minimum_spending($ids, $minimum_ids, $course_id);

		// Get customer pass details
		$customer_passes = [];
		if(!empty($pass_ids)){
			$this->Pass->teetime_data = $this->teetime_data;
			$customer_passes = $this->Pass->get(['pass_id' => $pass_ids, 'course_id' => $course_id]);
		}

		// Loop through customers and break apart groups/passes into arrays
		foreach($rows as $key => &$row){

			$row['passes'] = [];
			$row['minimum_charges'] = [];
			$row['household_members'] = [];
            $row['household_id'] = '';
            $row['household_head'] = [];
            $row['recurring_charges'] = [];

			if(!empty($row['pass_ids'])){
				foreach($row['pass_ids'] as $pass_id){
					if(!empty($customer_passes[$pass_id])){
						$row['passes'][] = $customer_passes[$pass_id];
					}
				}
			}

			if(!empty($recurring_charges[$row['person_id']])){
			    $row['recurring_charges'] = $recurring_charges[$row['person_id']];
            }

			// Parse customer groups
            $customer_groups = [];
			if(!empty($row['groups'])){
				$row['groups'] = explode("\x1E", $row['groups']);
				// Loop through each pass, break apart fields
				foreach($row['groups'] as &$group_row){

					if(empty($group_row)){
						continue;
					}
					$group = explode("\x1F", $group_row);
                    $group = array(
						'group_id' => (int) $group[0],
						'label' => !empty($group[1]) ? $group[1] : ''
					);
					$customer_groups[] = $group;
				}
			}
            $row['groups'] = $customer_groups;

            if(!empty($households) && !empty($households[(int) $row['person_id']])){
                $row['household_members'] = $households[(int) $row['person_id']];
            }

            if(!empty($household_heads) && !empty($household_heads[(int) $row['person_id']])){
                $row['household_head'] = $household_heads[(int) $row['person_id']][0];
            }

            if(!empty($household_memberships) && !empty($household_memberships[(int) $row['person_id']])){
                $row['household_id'] = $household_memberships[(int) $row['person_id']];
            }
			if(!empty($params['include_recent_transactions'])){
				$row['recent_transactions'] = [];
			}

			if(!empty($params['include_gift_cards'])){
				$row['gift_cards'] = [];
			}

			if(!empty($gift_cards) && !empty($gift_cards[(int) $row['person_id']])){
				$row['gift_cards'] = $gift_cards[(int) $row['person_id']];
			}

            if(!empty($rainchecks) && !empty($rainchecks[(int) $row['person_id']])){
                $row['rainchecks'] = $rainchecks[(int) $row['person_id']];
            }

            if(!empty($billings) && !empty($billings[(int) $row['person_id']])) {
                $row['billings'] = $billings[(int) $row['person_id']];
            }

            if(!empty($invoices) && !empty($invoices[(int) $row['person_id']])) {
                $row['invoices'] = $invoices[(int) $row['person_id']];
            }

            if(!empty($account_transactions) && !empty($account_transactions[(int) $row['person_id']])) {
                $row['account_transactions'] = $account_transactions[(int) $row['person_id']];
            }

            if(!empty($last_visits) && !empty($last_visits[(int) $row['person_id']])) {
                $row['last_visit'] = $last_visits[(int) $row['person_id']];
            }

            if(!empty($credit_cards) && !empty($credit_cards[(int) $row['person_id']])){
                $row['credit_cards'] = $credit_cards[(int) $row['person_id']];
            }

            if(!empty($customer_details) && $key == 0){
            	$row['marketing_customer_info'] = $customer_details;
            }

            if(!empty($marketing_campaigns) && !empty($marketing_campaigns[(int) $row['person_id']])){
                $row['recent_campaigns'] = $marketing_campaigns[(int) $row['person_id']]['recent_campaigns'];
                $row['last_open_campaign'] = $marketing_campaigns[(int) $row['person_id']]['last_open_campaign'];
            }

            if(!empty($recent_transactions) && !empty($recent_transactions[(int) $row['person_id']])){
				$row['recent_transactions'] = $recent_transactions[(int) $row['person_id']];
			}

			if(!empty($minimum_spending) && !empty($minimum_spending[(int) $row['person_id']])){
				foreach($minimum_spending as $person_id => $spending){
					$row['minimum_charges'] = $spending;
				}
			}

			$row['person_id'] = (int) $row['person_id'];
			$row['taxable'] = (bool) $row['taxable'];
		}

		return $rows;
	}

	public function get_minimum_spending($person_ids, $minimum_ids, $course_id){

		$this->load->model('minimum_charge');

		$rows = [];
		if(empty($minimum_ids) || empty($person_ids)){
			return false;
		}

		$toReturn = [];
		// Foreach minimum, pass customer list
		foreach($minimum_ids as $minimum_id){

			$charge = $this->minimum_charge->get([
				"charge_id"=>$minimum_id
			]);
			$charge = $charge[0];

			$customers = $this->minimum_charge->get_pending_charge_customers($minimum_id, $person_ids,false);
			$spending = $this->minimum_charge->get_current_spending($customers[$minimum_id], $course_id, $minimum_id);

			if(empty($spending) && !empty($customers)){
				$rows[$customers[$minimum_id][0]["person_id"]][] = [
					"minimum_charge_id"=>$minimum_id,
					"total"=>0,
					"date_added" => $customers[$minimum_id][0]['date_added'],
					"name"=>$charge['name'],
					"minimum_amount"=>$charge['minimum_amount']
				];

			} else if(!empty($spending)){
				foreach($spending as $spend){
					$rows[$spend['customer_id']][] = [
						"minimum_charge_id"=>$minimum_id,
						"total"=>$spend['total'],
						"start_date"=>$spend['start_date'],
						"end_date"=>$spend['end_date'],
						"date_added" => $customers[$minimum_id][0]['date_added'],
						"name"=>$charge['name'],
						"minimum_amount"=>$charge['minimum_amount']
					];
				}				
			}
		}

		return $rows;
	}

	public function get_household_members($person_ids){

		if(empty($person_ids)){
			return [];
		}

		$rows = $this->db->select('households.household_head_id, household_members.household_member_id AS person_id, 
			people.first_name, people.last_name')
			->from('households')
			->join('household_members', 'household_members.household_id = households.household_id', 'inner')
			->join('people', 'people.person_id = household_members.household_member_id', 'left')
			->where_in('households.household_head_id', $person_ids)
			->get()
			->result_array();

		$households = [];
		foreach($rows as $row){
			$households[(int) $row['household_head_id']][] = [
				'person_id' => (int) $row['person_id'],
				'first_name' => $row['first_name'],
				'last_name' => $row['last_name']
			];
		}

		return $households;
	}

    public function get_household_heads($person_ids)
    {

        if(empty($person_ids)){
            return [];
        }

        $rows = $this->db->select('households.household_head_id, household_members.household_member_id AS person_id, 
			people.first_name, people.last_name')
            ->from('households')
            ->join('household_members', 'household_members.household_id = households.household_id', 'inner')
            ->join('people', 'people.person_id = households.household_head_id', 'left')
            ->where_in('household_members.household_member_id', $person_ids)
            ->get()
            ->result_array();

        $household_heads = [];
        foreach($rows as $row){
            $household_heads[(int) $row['person_id']][] = [
                'household_head_id' => (int) $row['household_head_id'],
                'first_name' => $row['first_name'],
                'last_name' => $row['last_name']
            ];
        }

        return $household_heads;
    }

    public function get_recurring_charges($person_ids){

        if(empty($person_ids)){
            return [];
        }

        $rows = $this->db->select('charge_customer.recurring_charge_id AS id, charge_customer.person_id, charge_customer.date_added')
            ->from('account_recurring_charge_customers AS charge_customer')
            ->where_in('charge_customer.person_id', $person_ids)
            ->group_by('charge_customer.recurring_charge_id, charge_customer.person_id')
            ->get()
            ->result_array();

        $recurring_charges = [];
        foreach($rows as $row){
            $recurring_charges[$row['person_id']][] = $row;
        }

        return $recurring_charges;
    }

    public function is_household_member($person_ids){

        if(empty($person_ids)){
            return [];
        }

        $rows = $this->db->from('household_members')
            ->where_in('household_member_id', $person_ids)
            ->get()
            ->result_array();

        $households = [];
        foreach($rows as $row){
            $households[(int) $row['household_member_id']] = (int) $row['household_id'];
        }

        return $households;
    }

	public function save_photo($customer_id, $base64Image, $width, $height, $course_id = false){
		
		$this->load->model('image');
		if(empty($base64Image)){
			return false;
		}

		if(empty($customer_id)){
			$customer_id = 'new_'.md5(microtime());
		}

		$image = base64_decode(
			substr(
				$base64Image, 
				strpos($base64Image, ',')
			)
		);

		if(!$course_id){
			$course_id = $this->session->userdata('course_id');
		}

		$size = round(strlen($image) / 1024, 2);

		$filename = 'person_'.$customer_id.'.jpg';
		$image_id = $this->image->upload($filename, $image, $course_id);

		$image_id = $this->image->save_file(null, array(
			'label' => $filename,
			'filename' => $filename,
			'width' => $width,
			'height' => $height,
			'date_created' => date('Y-m-d H:i:s'),
			'saved' => 1,
			'filesize' => $size,
			'module' => 'customers',
			'course_id' => $course_id
		));

		return array(
			'url' => $this->image->upload_url_thumb($course_id).'/'.$filename,
			'image_id' => $image_id
		);
	}

	public function save_credentials($customer_id, $username, $password, $confirm_password) {
        if(isset($data['username']) && $data['username'] != '' && $this->username_taken($person_id, $data['username'])){
            $this->error = 'Username is already taken';
            return false;
        }

        if(!empty($data['password']) && $data['password'] != $data['confirm_password']){
            $this->error = 'Passwords do not match';
            return false;
        }

        if(!empty($data['password']) && strlen($data['password']) < 6){
            $this->error = 'Password must be at least 6 characters';
            return false;
        }

        // If a username or password is sent, create a user account
        if(!empty($data['username']) || !empty($data['password'])){
            if(empty($data['username'])){
                $this->error = 'Username is required';
                return false;
            }

            if(empty($data['password'])){
                $this->error = 'Password is required';
                return false;
            }

            $params = array(
                'email' => $data['username'],
                'password' => md5($data['password']),
                'person_id' => (int) $person_id
            );

            if($this->send_booking_credentials){
                $this->Customer->send_online_booking_email($data['username'],$data['password']);
            }
            $this->db->insert('users', $params);
        }

        // Update user login credentials
        if(!empty($data['username'])){
            $params = array(
                'email' => $data['username']
            );

            if(!empty($data['password'])){
                $params['password'] = md5($data['password']);
                $this->Customer->send_online_booking_email($data['username'],$data['password']);
            }

            // Check if customer has user profile already
            $user_profile = $this->db->select('person_id')->from('users')->where('person_id', (int) $person_id)->get()->num_rows();

            if($user_profile > 0){
                $this->db->update('users', $params, array('person_id' => (int) $person_id));
            }else{
                $params['person_id'] = (int) $person_id;
                $this->db->insert('users', $params, array('person_id' => (int) $person_id));
            }
        }
    }

	public function get_field_settings($course_id = null){

		$fields = $this->fields;
        if(empty($course_id)){
            $course_id = $this->session->userdata('course_id');
        }

		$row = $this->db->select('customer_field_settings')
			->from('courses')
			->where('course_id', $course_id)
			->get()->row_array();

		if(empty($row['customer_field_settings'])){
			return $fields;
		}

		$required_fields = json_decode($row['customer_field_settings'], true);

		foreach($required_fields as $key => $required_field){
			$fields[$key]['required'] = (int) $required_field['required'];
			if(isset($required_field['teesheet_required'])){
				$fields[$key]['teesheet_required'] = (int) $required_field['teesheet_required'];
			}
			if(isset($required_field['online_booking_required'])){
				$fields[$key]['online_booking_required'] = (int) $required_field['online_booking_required'];
			}
		}
		ksort($fields);

		return $fields;
	}

	public function save_field_settings($field_settings){

		$field_keys = array_keys($this->fields);
		$field_settings = elements($field_keys, $field_settings);

		foreach($field_settings as &$field){
			$field['required'] = (int) $field['required'];
			if(isset($field['teesheet_required'])){
				$field['teesheet_required'] = (int) $field['teesheet_required'];
			}
			if(isset($field['online_booking_required'])){
				$field['online_booking_required'] = (int) $field['online_booking_required'];
			}
		}

		$this->db->update('courses', 
			array('customer_field_settings' => json_encode($field_settings)),
			array('course_id' => $this->session->userdata('course_id'))
		);

		return $this->db->affected_rows();
	}

	public function fields_valid($fields, $course_id = null){
		
		$field_settings = $this->get_field_settings($course_id);
		foreach($fields as $field => $value){
			
			if(empty($field_settings[$field])){
				continue;
			}
			$setting =& $field_settings[$field];

			if(!empty($setting['required']) && empty($value)){
				return $setting['name'].' is required';
			}
		}

		return true;
	}

	private function account_number_taken($person_id, $account_number){

		$this->db->select('account_number');
		$this->db->from('customers');
        $this->db->where('person_id !=', (int) $person_id);
		$this->db->where('account_number', trim($account_number));
		$this->db->where('deleted', 0);
		$this->db->where_in('course_id', $this->course_ids);
		$rows = $this->db->get()->num_rows();

		if($rows > 0){
			return true;
		}
		return false;
	}

	private function username_taken($person_id, $username){

		$this->db->select('email AS username');
		$this->db->from('users');
		$this->db->where('person_id !=', (int) $person_id);
		$this->db->where('email', trim($username));
		$rows = $this->db->get()->num_rows();
		
		if($rows > 0){
			return true;
		}
		return false;
	}

	public function save($person_id = null, $data){

	    $this->load->model('Customer');
		$this->load->model('Account_transactions');
		$this->load->model('Customer_loyalty');
        $this->error = false;

        $course_id = (int) $this->session->userdata('course_id');
		if(!empty($data['course_id'])){
		    $course_id = $data['course_id'];
        }
        $employee_id = (int) $this->session->userdata('person_id');

		$course_ids = array();
		$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);		
		$this->course_ids = $course_ids;

		// Fields for person table
		$person = elements(array('first_name', 'last_name', 'phone_number',
			'cell_phone_number', 'email', 'birthday', 'address_1', 'address_2',
			'city', 'state', 'zip', 'country', 'comments','handicap_account_number','handicap_score'), $data, null);
		foreach($person as &$field){
			if(isset($field)){
				$field = trim($field);
			}
		}

		// Fields for customer table
		$customer = elements(array('status_flag', 'date_created', 'price_class', 'account_number', 'discount',
			'account_balance', 'account_balance_allow_negative', 'member_account_balance', 'invoice_email',
			'member_account_balance_allow_negative', 'invoice_balance', 'require_food_minimum',
			'loyalty_points', 'use_loyalty', 'member', 'taxable', 'opt_out_text', 'opt_out_email',
			'account_limit', 'member_account_limit', 'image_id', 'course_id'), $data, null);

		$groups = false;
		$passes = false;
		$household_members = false;
		$minimum_charges = false;
        $recurring_charges = false;

		if(isset($data['groups'])){
			$groups = $data['groups'];
		}
		if(isset($data['passes'])){
			$passes = $data['passes'];
		}
		if(isset($data['household_members'])){
			$household_members = $data['household_members'];
		}
		if(isset($data['minimum_charges'])){
			$minimum_charges = $data['minimum_charges'];
		}
        if(isset($data['recurring_charges'])){
            $recurring_charges = $data['recurring_charges'];
        }

		if(!empty($customer['course_id'])){
			
			if(in_array($customer['course_id'], $this->course_ids)){
				$course_id = (int) $customer['course_id'];
			}
			unset($customer['course_id']);
		}

		$person = $this->filter_empty($person);
		$customer = $this->filter_empty($customer);
		if(isset($customer['account_balance'])){
			$customer['account_balance'] = (float) clean_number($customer['account_balance']);
		}
		if(isset($customer['member_account_balance'])){
			$customer['member_account_balance'] = (float) clean_number($customer['member_account_balance']);
		}
		if(isset($customer['invoice_balance'])){
			$customer['invoice_balance'] = (float) clean_number($customer['invoice_balance']);
		}
		if(isset($customer['account_limit'])){
			$customer['account_limit'] = (float) clean_number($customer['account_limit']);
		}
		if(isset($customer['member_account_limit'])){
			$customer['member_account_limit'] = (float) clean_number($customer['member_account_limit']);
		}
		if(isset($customer['loyalty_points'])){
			$customer['loyalty_points'] = (int) round(clean_number($customer['loyalty_points']));
		}
		
		if(isset($customer['discount']) && $customer['discount'] > 100){
			$customer['discount'] = 100;
		}
		if(isset($customer['use_loyalty'])){
			$customer['use_loyalty'] = (int) (bool) $customer['use_loyalty'];
		}
		if(isset($customer['opt_out_text'])){
			$customer['opt_out_text'] = (int) (bool) $customer['opt_out_text'];
		}
		if(isset($customer['opt_out_email'])){
			$customer['opt_out_email'] = (int) (bool) $customer['opt_out_email'];
		}
		if(isset($customer['account_balance_allow_negative'])){
			$customer['account_balance_allow_negative'] = (int) (bool) $customer['account_balance_allow_negative'];
		}
		if(isset($customer['member_account_balance_allow_negative'])){
			$customer['member_account_balance_allow_negative'] = (int) (bool) $customer['member_account_balance_allow_negative'];
		}
		if(isset($customer['member'])){
			$customer['member'] = (int) (bool) $customer['member'];
		}
		if(isset($customer['taxable'])){
			$customer['taxable'] = (int) (bool) $customer['taxable'];
		}

		if(isset($customer['account_number']) && $customer['account_number'] == ''){
			$customer['account_number'] = null;
		}
        if(isset($customer['account_number']) && $customer['account_number'] !== null){
			if($this->account_number_taken($person_id, $customer['account_number'])){
				$this->error = 'That account number is already taken';
				return false;
			}
		}

		// Make sure required fields are fill out
		// If not, return error
		$is_valid = $this->fields_valid($data, $course_id);
		if($is_valid !== true){
			$this->error = $is_valid;
			return false;
		}	

		if(isset($data['username']) && $data['username'] != '' && $this->username_taken($person_id, $data['username'])){
			$this->error = 'Username is already taken';
			return false;
		}

		if(!empty($data['password']) && $data['password'] != $data['confirm_password']){
			$this->error = 'Passwords do not match';
			return false;
		}

		if(!empty($data['password']) && strlen($data['password']) < 6){
			$this->error = 'Password must be at least 6 characters';
			return false;
		}

		if(!empty($person['birthday'])){
			$person['birthday'] = date("Y-m-d", strtotime($person['birthday']));
		}
		if(empty($customer['date_created'])){
			$customer['date_created'] = null;
		} else {
			$customer['date_created'] = date("Y-m-d", strtotime($customer['date_created']));
		}

		// Turn off caching historically queries. This eats up memory when doing a customer import
		$this->db->save_queries = FALSE;

		// If a person ID is not passed in, create a new customer
		if(!$person_id || !$this->Customer->exists($person_id)){

		    if(empty($person)){
		        $this->error = 'No data found to create person';
                return false;
		    }

			$this->db->insert('people', $person);
			$person_id = (int) $this->db->insert_id();

			// If a username or password is sent, create a user account
			if(!empty($data['username']) || !empty($data['password'])){
				if(empty($data['username'])){
					$this->error = 'Username is required';
					return false;
				}

				if(empty($data['password'])){
					$this->error = 'Password is required';
					return false;
				}

				$params = array(
					'email' => $data['username'],
					'password' => md5($data['password']),
					'person_id' => (int) $person_id
				);

                if($this->send_booking_credentials){
                    $this->Customer->send_online_booking_email($data['username'],$data['password'],$person['email'], $course_id);
                }
				$this->db->insert('users', $params);
			}

			$customer['course_id'] = $course_id;
			$customer['person_id'] = $person_id;

            if(empty($customer['date_created'])){
                $customer['date_created'] = Carbon::now()->toDateTimeString();
            }
			$this->db->insert('customers', $customer);

			// If a starting account balance was entered
			if (isset($customer['customer_balance']) && $customer['customer_balance'] != 0){
				$transaction = array(
					'course_id' => $course_id,
					'trans_customer'=> $person_id,
					'trans_date' => date('Y-m-d H:i:s'),
					'trans_user' => $employee_id,
					'trans_comment' => 'Original Balance - Imported',
					'trans_description' => 'Customer Import',
					'trans_amount' => $customer['customer_balance'],
					'running_balance' => $customer['customer_balance']
				);
				$this->Account_transactions->insert('customer', $transaction);
			}

			// If a starting member balance was entered
			if (isset($customer['member_account_balance']) && $customer['member_account_balance'] != 0){
				$transaction = array(
					'course_id' => $course_id,
					'trans_customer' => $person_id,
					'trans_date' => date('Y-m-d H:i:s'),
					'trans_user' => $employee_id,
					'trans_comment' => 'Original Balance - Imported',
					'trans_description' => 'Customer Import',
					'trans_amount' => $customer['member_account_balance'],
					'running_balance' => $customer['member_account_balance']
				);
				$this->Account_transactions->insert('member', $transaction);
			}
			$response = $person_id;

		// Update existing customer
		}else{

			$current_customer = $this->db->select('customers.account_balance, customers.account_balance_allow_negative,
				customers.member_account_balance, customers.member_account_balance_allow_negative, customers.invoice_balance, 
				customers.opt_out_email, customers.loyalty_points, people.email')
				->from('customers')
                ->join('people', 'people.person_id = customers.person_id', 'left')
				->where('customers.person_id', $person_id)
				->where_in('customers.course_id', $course_ids)
				->get()->row_array();

			// Update people table
			if(!empty($person)){
				$this->db->update('people', $person, array('person_id' => (int) $person_id));
			}



			// Update user login credentials
			if(!empty($data['username'])){
				$params = array(
					'email' => $data['username']
				);

				if(!empty($data['password'])){
					$params['password'] = md5($data['password']);
                    $this->Customer->send_online_booking_email($data['username'], $data['password'], $current_customer['email'], $course_id);
				}
				
				// Check if customer has user profile already
				$user_profile = $this->db->select('person_id')->from('users')->where('person_id', (int) $person_id)->get()->num_rows();
				
				if($user_profile > 0){
					$this->db->update('users', $params, array('person_id' => (int) $person_id));
				}else{
					$params['person_id'] = (int) $person_id;
					$this->db->insert('users', $params, array('person_id' => (int) $person_id));
				}
			}

			$customerBalanceComment = 'Customer Edit';





			if(isset($data['account_balance_option'])){
				if(!empty($data['account_balance_change_reason'])){
					$customerBalanceComment = $data['account_balance_change_reason'];
				}
				if($data['account_balance_option'] == "increase"){
					$difference = (float) ($customer['account_balance']);
					$customer['account_balance'] = $current_customer['account_balance'] + $difference;
					$comment = "Manual Increase";
				} else if ($data['account_balance_option'] == "decrease"){
					$difference = (float) -1 * ($customer['account_balance']);
					$customer['account_balance'] = $current_customer['account_balance'] + $difference;
					$comment = "Manual Decrease";
				} else {
					$difference = (float) ($customer['account_balance'] - (float) $current_customer['account_balance']);
					$comment = "Manual Edit";
				}

				$transaction = array(
					'course_id' => $course_id,
					'trans_customer' => $person_id,
					'trans_household' => $person_id,
					'trans_date' => date('Y-m-d H:i:s'),
					'trans_user' => $employee_id,
					'trans_comment' => $comment,
					'trans_description' => $customerBalanceComment,
					'trans_amount' => $difference,
					'running_balance' => $customer['account_balance'],
				);


				if($difference != 0){
					$this->Account_transactions->insert('customer', $transaction);
				}
			} else if (isset($customer['account_balance']) && $customer['account_balance'] != (float) $current_customer['account_balance']){

				if(!empty($data['account_balance_change_reason'])){
					$customerBalanceComment = $data['account_balance_change_reason'];
				}
				$difference = (float) ($customer['account_balance'] - (float) $current_customer['account_balance']);
				$transaction = array(
					'course_id' => $course_id,
					'trans_customer' => $person_id,
					'trans_household' => $person_id,
					'trans_date' => date('Y-m-d H:i:s'),
					'trans_user' => $employee_id,
					'trans_comment' => lang('items_manually_editing_of_quantity'),
					'trans_description' => $customerBalanceComment,
					'trans_amount' => $difference,
					'running_balance' => $customer['account_balance'],
				);
				$this->Account_transactions->insert('customer', $transaction);
			}




			// Add account balance transaction if changed
            $memberBalanceComment = 'Customer Edit';
			// Add member balance transaction if changed
			if(isset($data['member_account_option'])){
				if(!empty($data['member_balance_change_reason'])){
					$customerBalanceComment = $data['member_balance_change_reason'];
				}
				if($data['member_account_option'] == "increase"){
					$difference = (float) ($customer['member_account_balance']);
					$customer['member_account_balance'] = $current_customer['member_account_balance'] + $difference;
					$comment = "Manual Increase";
				} else if ($data['member_account_option'] == "decrease"){
					$difference = (float) -1 * ($customer['member_account_balance']);
					$customer['member_account_balance'] = $current_customer['member_account_balance'] + $difference;
					$comment = "Manual Decrease";
				} else {
					$difference = (float) ($customer['member_account_balance'] - (float) $current_customer['member_account_balance']);
					$comment = "Manual Edit";
				}

				$transaction = array(
					'course_id' => $course_id,
					'trans_customer' => $person_id,
					'trans_household' => $person_id,
					'trans_date' => date('Y-m-d H:i:s'),
					'trans_user' => $employee_id,
					'trans_comment' => $comment,
					'trans_description' => $customerBalanceComment,
					'trans_amount' => $difference,
					'running_balance' => $customer['member_account_balance'],
				);


				if($difference != 0){
					$this->Account_transactions->insert('member', $transaction);
				}
			} else if (isset($customer['member_account_balance']) && $customer['member_account_balance'] != (float) $current_customer['member_account_balance']){

                if(!empty($data['member_balance_change_reason'])){
                    $memberBalanceComment = $data['member_balance_change_reason'];
                }

				$difference = (float) ($customer['member_account_balance'] - (float) $current_customer['member_account_balance']);
				$transaction = array(
					'course_id' => $course_id,
					'trans_customer' => $person_id,
                    'trans_household' => $person_id,
                    'trans_date' => date('Y-m-d H:i:s'),
					'trans_user' => $employee_id,
					'trans_comment' => lang('items_manually_editing_of_quantity'),
					'trans_description' => $memberBalanceComment,
					'trans_amount' => $difference,
					'running_balance' => $customer['member_account_balance']
				);
				$this->Account_transactions->insert('member', $transaction);
			}




			// Update customer table
			if(!empty($customer)){
				$this->db->where_in('course_id', $this->course_ids);
				$this->db->update('customers', $customer, array('person_id' => (int) $person_id));
			}





			// Add invoice transaction if changed
			if (isset($customer['invoice_balance']) && $customer['invoice_balance'] != (float) $current_customer['invoice_balance']){

				$difference = (float) ($customer['invoice_balance'] - (float) $current_customer['invoice_balance']);
				$transaction = array(
					'course_id' => $course_id,
					'trans_customer' => $person_id,
                    'trans_household' => $person_id,
                    'trans_date' => date('Y-m-d H:i:s'),
					'trans_user' => $employee_id,
					'trans_comment' => lang('items_manually_editing_of_quantity'),
					'trans_description' => 'Customer Edit',
					'trans_amount' => $difference,
					'running_balance' => $customer['invoice_balance']
				);
				$this->Account_transactions->insert('invoice', $transaction);
			}

            $loyaltyComment = 'Customer Edit';

            // Add loyalty transaction if loyalty is changed
			if (isset($customer['loyalty_points']) && $customer['loyalty_points'] != $current_customer['loyalty_points']){

                if(!empty($data['loyalty_points_change_reason'])){
                    $loyaltyComment = $data['loyalty_points_change_reason'];
                }
				$difference = (int) ($customer['loyalty_points'] - $current_customer['loyalty_points']);
				$transaction = array(
					'trans_customer' => $person_id,
					'trans_date' => date('Y-m-d H:i:s'),
					'trans_user' => $employee_id,
					'trans_comment' => lang('items_manually_editing_of_quantity'),
					'trans_description' => $loyaltyComment,
					'trans_amount' => $difference
				);
				$this->Customer_loyalty->insert($transaction);
			}

			if(isset($customer['opt_out_email']) && $current_customer['opt_out_email'] != $customer['opt_out_email'] && $customer['opt_out_email'] == 0){
				if(!class_exists("Sendgrid")){
					$this->load->library('Sendgrid');
				}
				//Delete the suppression record in sendgrid
				$this->sendgrid->delete_unsubscribe($data['email']);
			}

			//Update texting number
			if($this->config->item('marketing_texting')){
				$this->load->model("marketing_texting");
				$marketing_texting = new \Marketing_Texting();
				if(isset($person['cell_phone_number']) && !empty($person['cell_phone_number'])){
					$marketing_texting->updateMsisdn($person_id,$course_id,$person['cell_phone_number']);
				} else if (isset($person['phone_number']) && !empty($person['phone_number'])){
					$marketing_texting->updateMsisdn($person_id,$course_id,$person['phone_number']);
				} else {
					$marketing_texting->updateMsisdn($person_id,$course_id,'');
				}
			}

			$response = $person_id;
		}

		$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);		
		$this->course_ids = $course_ids;
		$course_id_list = implode(',', $this->course_ids);

		if($groups !== false){
			$this->save_groups($person_id, $course_id_list, $groups);
		}

		if($passes !== false){
            $this->save_passes($person_id, $course_id_list, $passes);
        }

        if($recurring_charges !== false){
            $this->save_recurring_charges($person_id, $course_id, $course_id_list, $recurring_charges);
        }

		if($household_members !== false){
			$this->load->model('Household');

			$household_member_ids = [];
			foreach($household_members as $member){
				$household_member_ids[] = $member['person_id'];
			}
			$this->Household->save($household_member_ids, $person_id, true);
		}

		if($minimum_charges !== false){
			$this->load->model('Minimum_charge');
			$this->Minimum_charge->save_customer_charges($person_id, $minimum_charges);
		}

		return $response;
	}

	function save_recurring_charges($person_id, $course_id, $course_ids = '', $recurring_charges = []){

	    $person_id = (int) $person_id;

	    $this->db->query("DELETE charge_customer.*
	        FROM foreup_account_recurring_charge_customers AS charge_customer
	        LEFT JOIN foreup_account_recurring_charges AS charge
	            ON charge.id = charge_customer.recurring_charge_id
	        WHERE charge_customer.person_id = {$person_id}
	            AND charge.organization_id IN ({$course_ids})");

        if(!empty($recurring_charges)){
            $rows = [];
            foreach($recurring_charges as $charge){

                $row = [];
                $row['person_id'] = $person_id;
                $row['recurring_charge_id'] = (int) $charge['id'];
                $row['date_added'] = date('Y-m-d H:i:s');
                $row['course_id'] = (int) $course_id;

                if(!empty($charge['date_added'])){
                    $row['date_added'] = $charge['date_added'];
                }
                $rows[] = $row;
            }

            $this->db->insert_batch('account_recurring_charge_customers', $rows);
        }

        return true;
    }

	function save_groups($person_id, $course_id_list = [], $groups = array()){

		// Clear out current groups
		$this->db->query("DELETE member.*
			FROM foreup_customer_group_members AS member
			LEFT JOIN foreup_customer_groups AS g
				ON member.group_id = g.group_id
			WHERE (g.course_id IN({$course_id_list}) OR g.course_id IS NULL)
				AND member.person_id = {$person_id}");

		$rows = array();
		if(!empty($groups)){
			foreach($groups as $group){
				$rows[] = array('group_id' => (int) $group['group_id'], 'person_id' => (int) $person_id);
			}
			$this->db->insert_batch('customer_group_members', $rows);
		}

		return true;
	}

    function get_group_id($group_label, $course_id = false){

        if(empty($group_label)){
            return false;
        }
        $group_label = trim($group_label);

        $course_ids = array();
        if(empty($course_id)){
            $course_id = $this->session->userdata('course_id');
        }
        $this->Course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);

        $this->db->select('group_id');
        $this->db->from('customer_groups');
        $this->db->where_in('course_id', array_values($course_ids));
        $this->db->where('label', $group_label);
        $this->db->limit(1);

        $group = $this->db->get()->row_array();
        if(!empty($group['group_id'])){
            return (int) $group['group_id'];
        }

        $this->db->insert('customer_groups', ['label' => $group_label, 'course_id' => $course_id]);
        return (int) $this->db->insert_id();
    }

	function save_passes($person_id, $course_id_list = [], $passes = array()){

		// Remove customer from any passes. Customer can not be removed
		// from passes they own
		$this->db->query("DELETE pass_customer.*
			FROM foreup_pass_customers AS pass_customer
			INNER JOIN foreup_passes AS pass
				ON pass.pass_id = pass_customer.pass_id
			WHERE pass.course_id IN({$course_id_list})
				AND pass.customer_id != {$person_id}
				AND pass_customer.person_id = {$person_id}");

		if(empty($passes)){
			return true;
		}
		
		$rows = array();
		foreach($passes as $pass){
			$date = date('Y-m-d H:i:s');
			$row = array(
				'pass_id' => (int) $pass['pass_id'], 
				'person_id' => (int) $person_id,
				'date_added' => $date
			);

			$this->db->query("INSERT IGNORE INTO foreup_pass_customers (person_id, pass_id, date_added)
				VALUES ({$row['person_id']}, {$row['pass_id']}, '{$row['date_added']}')");
		}

		return true;
	}

	private function filter_empty($fields, $empty_value = null){
		foreach($fields as $key => $val){
			if($val === $empty_value){
				unset($fields[$key]);
			}
		}
		return $fields;
	}

    /*
    Merge customer
     */
    function save_merge($data)
    {
        $this->course_id = $this->config->item('course_id');
        $course_ids = array();
        $this->Course->get_linked_course_ids($course_ids, 'shared_customers', $this->course_id);
        $this->course_ids = $course_ids;

        // Merge person data
        $this->merge_account_info($data);
        $this->merge_person($data);
        $this->merge_households($data);
        $this->merge_groups($data);
        $this->merge_passes($data);
        $this->merge_miscellaneous($data);
        $success = $this->db->trans_status();
        $message = '';
        if (!$success) {
            $message = 'There was an error with the merge.';
        }

        //If secondary account doesn't belong to multiple courses then delete the user credentials
        $count = $this->db->from("customers")
            ->where("person_id =",$data['person_id_2'])
            ->count_all_results();
        if($count == 1){
            $this->db->from("users")
                ->where("person_id =",$data['person_id_2'])
                ->delete();
        }

        $response = array('success' => $success, 'message' => $message);
        return $response;
    }

    /*
    Merge person info
    */
    function merge_person($data)
    {
        $this->load->model('Person');
        $original_person_info = $this->Person->get_info($data['person_id_1']);
        $merge_person_info = $this->Person->get_info($data['person_id_2'], false, true);
        $updated_info = array();

        if ((int)$data['name_checkbox'] == 2) {
            $updated_info['first_name'] = $merge_person_info->first_name;
            $updated_info['last_name'] = $merge_person_info->last_name;
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'name',
                'original_value' => json_encode(array($original_person_info->first_name, $original_person_info->last_name)),
                'new_value' => json_encode(array($merge_person_info->first_name, $merge_person_info->last_name))
            );
            $this->Customer_merge->record_change($merge_change_data);
        }

        if ((int)$data['email_checkbox'] == 2) {
            $updated_info['email'] = $merge_person_info->email;
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'email',
                'original_value' => $original_person_info->email,
                'new_value' => $merge_person_info->email
            );
            $this->Customer_merge->record_change($merge_change_data);
        }

        if ((int)$data['phone_number_checkbox'] == 2) {
            $updated_info['phone_number'] = $merge_person_info->phone_number;
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'phone_number',
                'original_value' => $original_person_info->phone_number,
                'new_value' => $merge_person_info->phone_number
            );
            $this->Customer_merge->record_change($merge_change_data);
        }

        if ((int)$data['cell_phone_number_checkbox'] == 2) {
            $updated_info['address_1'] = $merge_person_info->address_1;
            $updated_info['city'] = $merge_person_info->city;
            $updated_info['state'] = $merge_person_info->state;
            $updated_info['zip'] = $merge_person_info->zip;
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'address',
                'original_value' => json_encode(array($original_person_info->address_1, $original_person_info->city, $original_person_info->state, $original_person_info->zip)),
                'new_value' => json_encode(array($merge_person_info->address_1, $merge_person_info->city, $merge_person_info->state, $merge_person_info->zip))
            );
            $this->Customer_merge->record_change($merge_change_data);
        }

        if ((int)$data['comments_checkbox'] == 2) {
            $updated_info['comments'] = $merge_person_info->comments;
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'comments',
                'original_value' => $original_person_info->comments,
                'new_value' => $merge_person_info->comments
            );
            $this->Customer_merge->record_change($merge_change_data);
        }

        if (count($updated_info) > 0) {
            $this->db->update('people', $updated_info, array('person_id' => $data['person_id_1']));
        }

        return ;
    }

    /*
    Merge account info
     */
    function merge_account_info($data)
    {
        $original_person_info = $this->Customer->get_info($data['person_id_1'], $this->course_id);
        $merge_person_info = $this->Customer->get_info($data['person_id_2'], $this->course_id);
        // Instead of deleting the individual, we're just going to mark them to not show in customer searches and remove their account number
        $this->db->where('person_id', $data['person_id_2']);
        $this->db->where('course_id', $this->course_id);
        $this->db->update('customers', array('hide_from_search' => 1, 'account_number' => null));

        // Save the merge action to the database
        $merge_data = array(
            'datetime' => '',
            'employee_id' => $this->session->userdata('person_id'),
            'customer_id' => $data['person_id_1'],
            'duplicate_customer_id' => $data['person_id_2'],
            'original_customer_data' => json_encode($original_person_info),
            'course_id' => $this->course_id
        );
        $this->Customer_merge->save($merge_data);
        $this->merge_id = $merge_data['merge_id'];

        $updated_info = array();

        $updated_info['opt_out_email'] = $original_person_info->opt_out_email || $merge_person_info->opt_out_email ? 1 : 0;
        $updated_info['unsubscribe_all'] = $original_person_info->unsubscribe_all || $merge_person_info->unsubscribe_all ? 1 : 0;
        $updated_info['status_flag'] = $original_person_info->status_flag;
        if ($original_person_info->status_flag == 0) {
            $updated_info['status_flag'] = $merge_person_info->status_flag;
        } else if ($merge_person_info->status_flag == 0) {

        } else if ($original_person_info->status_flag > $merge_person_info->status_flag) {
            $updated_info['status_flag'] = $merge_person_info->status_flag;
        }

        if ((int)$data['account_number_checkbox'] == 2) {
            $updated_info['account_number'] = $merge_person_info->account_number;
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'account_number',
                'original_value' => $original_person_info->account_number,
                'new_value' => $merge_person_info->account_number
            );
            $this->Customer_merge->record_change($merge_change_data);
        }

        if ((int)$data['price_class_checkbox'] == 2) {
            $updated_info['price_class'] = $merge_person_info->price_class;
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'price_class',
                'original_value' => $original_person_info->price_class,
                'new_value' => $merge_person_info->price_class
            );
            $this->Customer_merge->record_change($merge_change_data);
        }

        if ($data['discount_checkbox'] == 2) {
            $updated_info['discount'] = $merge_person_info->discount;
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'discount',
                'original_value' => $original_person_info->discount,
                'new_value' => $merge_person_info->discount
            );
            $this->Customer_merge->record_change($merge_change_data);
        }

        if ($data['taxable_checkbox'] == 2) {
            $updated_info['taxable'] = $merge_person_info->taxable;
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'taxable',
                'original_value' => $original_person_info->taxable,
                'new_value' => $merge_person_info->taxable
            );
            $this->Customer_merge->record_change($merge_change_data);
        }

        if ($data['member_checkbox'] == 2) {
            $updated_info['member'] = $merge_person_info->member;
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'member',
                'original_value' => $original_person_info->member,
                'new_value' => $merge_person_info->member
            );
            $this->Customer_merge->record_change($merge_change_data);
        }

        if ($data['minimums_checkbox'] == 2) {
            // Switch all food minimums
            $this->db->select('minimum_charge_id');
            $this->db->from('customer_minimum_charges');
            $this->db->where('person_id', $data['person_id_2']);
            $minimums = $this->db->get()->result_array();

            $this->db->select('minimum_charge_id');
            $this->db->from('customer_minimum_charges');
            $this->db->where('person_id', $data['person_id_1']);
            $original_minimums = $this->db->get()->result_array();

            if (COUNT($minimums) > 0) {
                $merge_change_data = array(
                    'merge_id' => $this->merge_id,
                    'type' => 'minimum',
                    'original_value' => json_encode($original_minimums),
                    'new_value' => json_encode($minimums)
                );
                $this->Customer_merge->record_change($merge_change_data);

                foreach ($minimums as $minimum) {
                    // Delete existing minimums and insert them for the primary customer because we're not deleting the duplicate customer, just hiding them
                    // This whill have
                    $this->db->query("DELETE FROM foreup_customer_minimum_charges WHERE person_id = {$data['person_id_2']} AND minimum_charge_id = {$minimum['minimum_charge_id']} LIMIT 1 ");
                    $this->db->query("INSERT IGNORE INTO foreup_customer_minimum_charges (person_id, minimum_charge_id, date_added) VALUES ({$data['person_id_1']}, {$minimum['minimum_charge_id']}, '" . date('Y-m-d') . "'})");
                }
            }

        }

        // Transfer member account balance
        $original_values = $update_values = array();
        $original_values['member_account_balance'] = $original_person_info->member_account_balance;
        $update_values['member_account_balance'] = $merge_person_info->member_account_balance;
        $updated_info['member_account_balance'] = $original_person_info->member_account_balance + $merge_person_info->member_account_balance;

        // Update settings
        $original_values['member_account_balance_allow_negative'] = $original_person_info->member_account_balance_allow_negative;
        $update_values['member_account_balance_allow_negative'] = $updated_info['member_account_balance_allow_negative'] = ($merge_person_info->member_account_balance_allow_negative || $original_person_info->member_account_balance_allow_negative) ? 1 : 0;
        $original_values['member_account_limit'] = $original_person_info->member_account_limit;
        $update_values['member_account_limit'] = $updated_info['member_account_limit'] = ($merge_person_info->member_account_limit < $original_person_info->member_account_limit) ? $merge_person_info->member_account_limit : $original_person_info->member_account_limit;


        $merge_change_data = array(
            'merge_id' => $this->merge_id,
            'type' => 'member_account_info',
            'original_value' => json_encode(array()),
            'new_value' => json_encode($update_values)
        );
        $this->Customer_merge->record_change($merge_change_data);

        // Update all transactions
        $this->db->select('trans_id');
        $this->db->from('account_transactions');
        $this->db->where('account_type', 'member');
        $this->db->where('trans_customer', $data['person_id_2']);
        $this->db->where('course_id', $this->course_id);
        $new_transactions = $this->db->get()->result_array();

        $merge_change_data = array(
            'merge_id' => $this->merge_id,
            'type' => 'member_account_transactions',
            'original_value' => json_encode(array()),
            'new_value' => json_encode($new_transactions)
        );
        $this->Customer_merge->record_change($merge_change_data);

        $this->db->where('account_type', 'member');
        $this->db->where('trans_customer', $data['person_id_2']);
        $this->db->where('course_id', $this->course_id);
        $this->db->update('account_transactions', array('trans_customer' => $data['person_id_1']));

        // Transfer customer account balance
        $original_values = $update_values = array();
        $original_values['account_balance'] = $original_person_info->account_balance;
        $update_values['account_balance'] = $merge_person_info->account_balance;
        $updated_info['account_balance'] = $original_person_info->account_balance + $merge_person_info->account_balance;

        // Update settings
        $original_values['account_balance_allow_negative'] = $original_person_info->account_balance_allow_negative;
        $update_values['account_balance_allow_negative'] = $updated_info['account_balance_allow_negative'] = ($merge_person_info->account_balance_allow_negative || $original_person_info->account_balance_allow_negative) ? 1 : 0;
        $original_values['account_limit'] = $original_person_info->account_limit;
        $update_values['account_limit'] = $updated_info['account_limit'] = ($merge_person_info->account_limit < $original_person_info->account_limit) ? $merge_person_info->account_limit : $original_person_info->account_limit;

        $merge_change_data = array(
            'merge_id' => $this->merge_id,
            'type' => 'customer_account_info',
            'original_value' => json_encode(array()),
            'new_value' => json_encode($update_values)
        );
        $this->Customer_merge->record_change($merge_change_data);

        // Update all transactions
        $this->db->select('trans_id');
        $this->db->from('account_transactions');
        $this->db->where('account_type', 'customer');
        $this->db->where('course_id', $this->course_id);
        $this->db->where('trans_customer', $data['person_id_2']);
        $new_transactions = $this->db->get()->result_array();

        $merge_change_data = array(
            'merge_id' => $this->merge_id,
            'type' => 'customer_account_transactions',
            'original_value' => json_encode(array()),
            'new_value' => json_encode($new_transactions)
        );
        $this->Customer_merge->record_change($merge_change_data);

        $this->db->where('account_type', 'customer');
        $this->db->where('trans_customer', $data['person_id_2']);
        $this->db->where('course_id', $this->course_id);
        $this->db->update('account_transactions', array('trans_customer' => $data['person_id_1']));

        // Transfer invoice account balance
        $original_values = $update_values = array();
        $original_values['invoice_balance'] = $original_person_info->invoice_balance;
        $update_values['invoice_balance'] = $merge_person_info->invoice_balance;
        $updated_info['invoice_balance'] = $original_person_info->invoice_balance + $merge_person_info->invoice_balance;

        // Transfer loyalty balance
        $original_values = $update_values = array();
        $original_values['loyalty_points'] = $original_person_info->loyalty_points;
        $update_values['loyalty_points'] = $merge_person_info->loyalty_points;
        $updated_info['loyalty_points'] = $original_person_info->loyalty_points + $merge_person_info->loyalty_points;

        $this->db->where('person_id', $data['person_id_1']);
        $this->db->where('course_id', $this->course_id);
        return $this->db->update('customers', $updated_info);
    }

    /*
    Merge households
     */
    function merge_households($data)
    {
        $this->load->model('Household');
        $original_members = $this->Household->get_members($data['person_id_1']);
        $new_members = $this->Household->get_members($data['person_id_2']);
        $household_to_delete = $this->Household->get_id($data['person_id_2']);
        $all_members = [];
        $original_member_ids = [];
        foreach ($original_members as $omem){
            $all_members[] = $omem['household_member_id'];
            $original_member_ids[] = $omem['household_member_id'];
        }
        foreach ($new_members as $nmem){
            if ($nmem['household_member_id'] != $data['person_id_1']) {
                $all_members[] = $nmem['household_member_id'];
            }
        }
        if (count($new_members) > 0) {
            $this->Household->delete($household_to_delete);
            $this->Household->save($all_members, $data['person_id_1']);
        }

        $merge_change_data = array(
            'merge_id' => $this->merge_id,
            'type' => 'households',
            'original_value' => json_encode($original_member_ids),
            'new_value' => json_encode($all_members)
        );
        $this->Customer_merge->record_change($merge_change_data);

    }

    /*
    Merge groups
     */
    function merge_groups($data)
    {

        $original_groups = $this->db->query("SELECT gm.group_id FROM foreup_customer_group_members AS gm LEFT JOIN foreup_customer_groups AS cg ON cg.group_id = gm.group_id WHERE person_id = {$data['person_id_1']} AND course_id = {$this->config->item('course_id')}")->result_array();
        $groups = $this->Customer->get_customer_groups($data['person_id_2']);
        $group_id_array = array();

        if (isset($data['groups_checkbox']) && $data['groups_checkbox']) {
            // Add person 2 groups

            foreach ($groups as $group) {
                $this->db->query("
					INSERT IGNORE INTO foreup_customer_group_members (group_id, person_id) VALUES ('{$group['group_id']}', '{$data['person_id_1']}');
				");
                $group_id_array[] = $group['group_id'];
            }
        }

        $merge_change_data = array(
            'merge_id' => $this->merge_id,
            'type' => 'groups',
            'original_value' => json_encode($original_groups),
            'new_value' => json_encode($group_id_array)
        );
        $this->Customer_merge->record_change($merge_change_data);

    }

    /*
    Merge passes
     */
    function merge_passes($data)
    {

        if (isset($data['passes_checkbox']) && $data['passes_checkbox']) {
            // Add person 2 passes
            $this->db->select('pass_id');
            $this->db->from("passes");
            $this->db->where('customer_id', $data['person_id_2']);
            $this->db->where('course_id', $this->course_id);
            $passes = $this->db->get()->result_array();

            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'passes',
                'original_value' => json_encode(array()),
                'new_value' => json_encode($passes)
            );
            $this->Customer_merge->record_change($merge_change_data);

            $this->db->where('customer_id', $data['person_id_2']);
            $this->db->where('course_id', $this->course_id);
            $this->db->update("passes", array('customer_id' => $data['person_id_1']));

            foreach($passes as $pass){
                $this->db->where('person_id', $data['person_id_2']);
                $this->db->where('pass_id', $pass['pass_id']);
                $this->db->update("pass_customers", array('person_id' => $data['person_id_1']));
            }

        }
    }

    /*
    Merge Miscellaneous
     */
    function merge_miscellaneous($data)
    {
        // All required
        $this->update_old_sales($data);
        $this->update_tee_times($data);
        $this->transfer_gift_cards($data);
        $this->transfer_punch_cards($data);
        $this->transfer_credit_cards($data);
        $this->transfer_invoices($data);
    }

    /*
    Update old sales
    */
    function update_old_sales($data)
    {
        // Required
        $this->db->select('sale_id');
        $this->db->from('sales');
        $this->db->where('customer_id', $data['person_id_2']);
        $this->db->where('course_id', $this->course_id);
        $sale_ids = $this->db->get()->result_array();

        if (COUNT($sale_ids) > 0) {
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'sales',
                'original_value' => '',
                'new_value' => json_encode($sale_ids)
            );
            $this->Customer_merge->record_change($merge_change_data);


            $this->db->where('customer_id', $data['person_id_2']);
            $this->db->where('course_id', $this->course_id);
            $this->db->update('sales', array('customer_id' => $data['person_id_1']));
        }
    }

    /*
    Update tee times
    */
    function update_tee_times($data)
    {
        // Required
        $this->load->model('Teesheet');
        $tee_sheets = $this->Teesheet->get_teesheets($this->course_id);
        $tee_sheet_id_array = array();
        foreach ($tee_sheets as $tee_sheet) {
            $tee_sheet_id_array[] = $tee_sheet['teesheet_id'];
        }

        $tee_time_ids = array();
        if (COUNT($tee_sheet_id_array) > 0) {
            $this->db->select('TTID');
            $this->db->from('teetime');
            $this->db->where("(person_id = {$data['person_id_2']} OR person_id_2 = {$data['person_id_2']} OR person_id_3 = {$data['person_id_2']} OR person_id_4 = {$data['person_id_2']} OR person_id_5 = {$data['person_id_2']})");
            $this->db->where_in('teesheet_id', $tee_sheet_id_array);
            $tee_time_ids = $this->db->get()->result_array();
        }

        if (COUNT($tee_time_ids) > 0) {
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'tee times',
                'original_value' => '',
                'new_value' => json_encode($tee_time_ids)
            );
            $this->Customer_merge->record_change($merge_change_data);

            $this->db->where('person_id', $data['person_id_2']);
            $this->db->where_in('teesheet_id', $tee_sheet_id_array);
            $this->db->update('teetime', array('person_id' => $data['person_id_1']));

            $this->db->where('person_id_2', $data['person_id_2']);
            $this->db->where_in('teesheet_id', $tee_sheet_id_array);
            $this->db->update('teetime', array('person_id_2' => $data['person_id_1']));

            $this->db->where('person_id_3', $data['person_id_2']);
            $this->db->where_in('teesheet_id', $tee_sheet_id_array);
            $this->db->update('teetime', array('person_id_3' => $data['person_id_1']));

            $this->db->where('person_id_4', $data['person_id_2']);
            $this->db->where_in('teesheet_id', $tee_sheet_id_array);
            $this->db->update('teetime', array('person_id_4' => $data['person_id_1']));

            $this->db->where('person_id_5', $data['person_id_2']);
            $this->db->where_in('teesheet_id', $tee_sheet_id_array);
            $this->db->update('teetime', array('person_id_5' => $data['person_id_1']));
        }
    }

    /*
    Transfer gift cards
    */
    function transfer_gift_cards($data)
    {
        // Required
        $this->db->select('giftcard_id');
        $this->db->from('giftcards');
        $this->db->where('customer_id', $data['person_id_2']);
        $this->db->where('course_id', $this->course_id);
        $giftcard_ids = $this->db->get()->result_array();
        if (COUNT($giftcard_ids) > 0) {
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'gift_cards',
                'original_value' => '',
                'new_value' => json_encode($giftcard_ids)
            );
            $this->Customer_merge->record_change($merge_change_data);

            $this->db->update('giftcards', array('customer_id' => $data['person_id_1']), array('customer_id' => $data['person_id_2'], 'course_id' => $this->course_id));
        }
    }

    /*
    Transfer punch cards
    */
    function transfer_punch_cards($data)
    {
        // Required
        $this->db->select('punch_card_id');
        $this->db->from('punch_cards');
        $this->db->where('customer_id', $data['person_id_2']);
        $this->db->where('course_id', $this->course_id);
        $punch_card_ids = $this->db->get()->result_array();

        if (COUNT($punch_card_ids) > 0) {
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'punch_cards',
                'original_value' => '',
                'new_value' => json_encode($punch_card_ids)
            );
            $this->Customer_merge->record_change($merge_change_data);

            $this->db->where('customer_id', $data['person_id_2']);
            $this->db->where('course_id', $this->course_id);
            $this->db->update('punch_cards', array('customer_id' => $data['person_id_1']));
        }
    }

    /*
    Transfer credit cards
    */
    function transfer_credit_cards($data)
    {
        // Required
        $this->db->select("credit_card_id");
        $this->db->from('customer_credit_cards');
        $this->db->where('customer_id', $data['person_id_2']);
        $this->db->where('course_id', $this->course_id);
        $credit_cards_ids = $this->db->get()->result_array();

        if (COUNT($credit_cards_ids) > 0) {
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'credit_cards',
                'original_value' => '',
                'new_value' => json_encode($credit_cards_ids)
            );
            $this->Customer_merge->record_change($merge_change_data);

            $this->db->where('customer_id', $data['person_id_2']);
            $this->db->where('course_id', $this->course_id);
            $this->db->update('customer_credit_cards', array('customer_id' => $data['person_id_1']));
        }
    }

    /*
    Transfer invoices
    */
    function transfer_invoices($data)
    {
        // Required
        $this->db->select('invoice_id');
        $this->db->from('invoices');
        $this->db->where('person_id', $data['person_id_2']);
        $this->db->where('course_id', $this->course_id);
        $invoice_ids = $this->db->get()->result_array();

        if (COUNT($invoice_ids) > 0) {
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'invoices',
                'original_value' => '',
                'new_value' => json_encode($invoice_ids)
            );
            $this->Customer_merge->record_change($merge_change_data);

            // Invoices
            $this->db->where('person_id', $data['person_id_2']);
            $this->db->where('course_id', $this->course_id);
            $this->db->update('invoices', array('person_id' => $data['person_id_1']));
        }

        $this->db->select('billing_id');
        $this->db->from('customer_billing');
        $this->db->where('person_id', $data['person_id_2']);
        $this->db->where('course_id', $this->course_id);
        $billing_ids = $this->db->get()->result_array();

        if (COUNT($billing_ids) > 0) {
            $merge_change_data = array(
                'merge_id' => $this->merge_id,
                'type' => 'billings',
                'original_value' => '',
                'new_value' => json_encode($billing_ids)
            );
            $this->Customer_merge->record_change($merge_change_data);
            // Recurring billings
            $this->db->where('person_id', $data['person_id_2']);
            $this->db->where('course_id', $this->course_id);
            $this->db->update('customer_billing', array('person_id' => $data['person_id_1']));
        }
    }
}
