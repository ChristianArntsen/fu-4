<?php
class Giftcard_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get($params){
		
		$this->load->model('course');
		$course_ids = array();
		$this->course->get_linked_course_ids($course_ids, 'shared_giftcards', $this->session->userdata('course_id'));
		
		$this->db->select('g.*, person.first_name AS customer_first_name, 
			person.last_name AS customer_last_name')
			->from('giftcards AS g')
			->join('people AS person', 'person.person_id = g.customer_id', 'left')
			->where_in('g.course_id', $course_ids)
			->where('g.deleted', 0);	
			
		if(isset($params['giftcard_number'])){
			$this->db->where('g.giftcard_number', $params['giftcard_number']);
		}
		
		if(isset($params['giftcard_id'])){
			if(!empty($params['giftcard_id']) && is_array($params['giftcard_id'])){
				$this->db->where_in('g.giftcard_id', $params['giftcard_id']);
			}else{
				$this->db->where('g.giftcard_id', (int) $params['giftcard_id']);	
			}
		}		
		
		$giftcards = $this->db->get()->result_array();

		return $giftcards;
	}
	
	public function get_new_online_number($course_id, $number_start = 1){
		
		$prefix = 'EGIFT';

		$last_issued_card = $this->db->select("giftcard_number, 
			CAST(
				REPLACE(giftcard_number, '{$prefix}', '') 
				AS UNSIGNED
			) AS digits", false)
			->from('giftcards')
			->where("giftcard_number LIKE '".$prefix.$number_start."%'")
			->where('deleted', 0)
			->where('course_id', $course_id)
			->order_by('digits', 'DESC')
			->limit(1)
			->get()
			->row_array();

		if(empty($last_issued_card['giftcard_number'])){
			return $prefix.$number_start.'1';
		}	

		$number = (int) str_replace($prefix.$number_start, '', $last_issued_card['giftcard_number']);
		$number += 1;

		$new_number = $prefix.$number_start.$number;
		
		return $new_number;
	}

	public function delete($params){
		
		if(empty($params['giftcard_id']) && empty($params['giftcard_number'])){
			return false;
		}
		
		$giftcard = $this->get($params);
		$giftcard = $giftcard[0];
		
		if(empty($giftcard)){
			return false;
		}
		
		$transaction = array(
			'course_id' => $giftcard['course_id'],
			'trans_date' => date('Y-m-d H:i:s'),
			'trans_giftcard' => (int) $giftcard['giftcard_id'],
			'trans_customer' => $this->session->userdata('customer'),
			'trans_user' => $this->session->userdata('person_id'),
			'trans_comment' => 'DELETED',
			'trans_description' => 'Card #'.$giftcard['giftcard_number'],
			'trans_amount' => 0.00
		);
		
		$this->db->insert('giftcard_transactions', $transaction);

		$this->load->model('course');
		$course_ids = array();
		$this->course->get_linked_course_ids($course_ids, 'shared_giftcards', $this->session->userdata('course_id'));
		
        $this->db->where_in('course_id', array_values($course_ids));
		$this->db->where('giftcard_id', (int) $giftcard['giftcard_id']);
		$this->db->update('giftcards', array('deleted' => 1, 'giftcard_number' => null));
		
		return $this->db->affected_rows();
	}
}
