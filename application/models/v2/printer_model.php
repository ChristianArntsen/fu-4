<?php
class Printer_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	function get($params = null){
		
		$params = elements(array('printer_id'), $params, null);
		
		if(isset($params['printer_id'])){
			$this->db->where('printer_id', (int) $params['printer_id']);
		}
		
		$this->db->select('*');
		$this->db->from('printers');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->order_by('label DESC');
		$printers = $this->db->get()->result_array();
		
		if(!empty($params['printer_id']) && count($printers) == 1){
			return $printers[0];
		}
		
		return $printers;
	}
	
	// Returns printer data in an array for a drop down menu
	public function get_menu(){
		$printers = $this->get();
		$menu = array();
		
		if(empty($printers)){
			return $menu;
		}
		
		foreach($printers as $printer){
			$menu[$printer['printer_id']] = $printer['label'] .' ('.$printer['ip_address'].')';
		}
		return $menu;		
	}	
	
	function save($printer_id, $data){
		
		$data = elements(array('label', 'ip_address'), $data);
		$data['course_id'] = (int) $this->session->userdata('course_id');
		
		if(!empty($printer_id)){
			$this->db->where('printer_id', (int) $printer_id);
			$this->db->update('printers', $data);
		
		}else{
			$this->db->insert('printers', $data);
			$printer_id = (int) $this->db->insert_id();
		}
		
		return (int) $printer_id;
	}
	
	function delete($printer_id){
		
		$printer_id = (int) $printer_id;
		$course_id = (int) $this->session->userdata('course_id');
		
		$this->db->delete('printers', array('course_id' => $course_id, 'printer_id' => $printer_id));
		
		return $this->db->affected_rows();
	}
}
