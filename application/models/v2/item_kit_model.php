<?php
class Item_kit_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->total = 0;
		$this->loyalty_rates = null;
	}

	private function search($search){

		$this->db->select('item_kit_id AS item_id');
		$this->db->from('item_kits');
		$this->db->where("deleted", 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where("name LIKE '%".$this->db->escape_like_str($search)."%'");
		$this->db->limit(100);
		$rows = $this->db->get()->result_array();

		$ids = array();
		foreach($rows as $row){
			$ids[] = (int) $row['item_id'];
		}

		return $ids;
	}
	
	public function check_number($number){
		
		$result = $this->db->query("SELECT item_kit_id 
			FROM foreup_item_kits
			WHERE (item_kit_number = ".$this->db->escape($number)." OR item_kit_id = ".(int) $number.")
				AND deleted = 0
				AND course_id = ".(int) $this->session->userdata('course_id'));
		$row = $result->row_array();
		
		if(empty($row['item_kit_id'])){
			return false;
		}
		
		return (int) $row['item_kit_id'];
	}

	private function where($params = []){
		
		$course_id = $this->session->userdata('course_id');
		
		if(!empty($params['item_kit_id'])){
			if(is_array($params['item_kit_id'])){
				$ids = array();
				foreach($params['item_kit_id'] as $item_id){
					$ids[] = (int) $item_id;
				}
				$this->db->where_in('i.item_kit_id', $ids);

			}else{
				$this->db->where('i.item_kit_id', (int) $params['item_kit_id']);
			}
		}
		
		$this->db->where('i.deleted', 0);
		$this->db->where('i.course_id', $course_id);	
	}

	public function get($params = null){

		$this->load->model('v2/Item_model');
		$params = elements(array('item_kit_id', 'q', 'limit', 'offset', 'sort', 'order'), $params, null);

		if(empty($params['limit'])){
			$params['limit'] = 100;
		}

		if(empty($params['offset'])){
			$params['offset'] = 0;
		}

		if($params['order'] != 'asc' && $params['order'] != 'desc'){
			$params['order'] = 'DESC';
		}

		if(!in_array($params['sort'], ['name', 'item_number', 'description', 'unit_price'])){
			$params['sort'] = null;
		}		

		if(!empty($params['q'])){
			$params['item_kit_id'] = $this->search($params['q']);

			if(empty($params['item_kit_id'])){
				return [];
			}
		}
		
		// Select list of item kits with attached items
		$this->db->select("i.item_kit_id, 
			i.item_kit_id AS item_id, i.course_id, i.name, i.department,
			i.category, i.subcategory, i.item_kit_number AS item_number,
			i.description, i.unit_price, i.unit_price AS base_price, i.max_discount,
			0 AS inventory_level, 1 AS inventory_unlimited, i.cost_price, i.is_punch_card,
			0 AS is_serialized,

			GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				kit_item_details.item_id,
				kit_item_details.name,
				kit_item_details.department,
				kit_item_details.category,
				kit_item_details.subcategory,
				kit_item_details.unit_price,
				kit_item_details.cost_price,
				kit_item.quantity,
				kit_item.unit_price,
				IFNULL(kit_item.price_class_id, ''),
				IFNULL(price_class.name, ''),
				IFNULL(kit_item.timeframe_id, ''),
				IFNULL(timeframe.timeframe_name, '')
			) SEPARATOR 0x1E) AS items,", false);
		$this->db->from('item_kits AS i');
		$this->db->join('item_kit_items AS kit_item', 'kit_item.item_kit_id = i.item_kit_id', 'left');
		$this->db->join('price_classes AS price_class', 'kit_item.price_class_id = price_class.class_id', 'left');
		$this->db->join('seasonal_timeframes AS timeframe', 'timeframe.timeframe_id = kit_item.timeframe_id', 'left');
		$this->db->join('items AS kit_item_details', 'kit_item_details.item_id = kit_item.item_id AND kit_item_details.deleted = 0', 'left');
		$this->db->group_by('i.item_kit_id');

		$this->where($params);

		if(!empty($params['sort'])){
			$this->db->order_by($params['sort'], $params['order']);
		}
		$this->db->limit($params['limit'], $params['offset']);

		$rows = $this->db->get()->result_array();

		// Get total possible rows
		$this->db->select('COUNT(i.item_kit_id) AS total', false);
		$this->db->from('item_kits AS i');
		$this->where($params);
		$totalRows = $this->db->get()->row_array();
		$this->total = (int) $totalRows['total'];

		$subItemIds = array();

		// Organize items with taxes as sub-array
		foreach($rows as $key => &$row){

			unset($row[0]);
			$item_kit_id = (int) $row['item_kit_id'];
			$row['item_type'] = 'item_kit';

			if($row['is_punch_card'] == 1){
				$row['item_type'] = 'punch_card';
				$row['params'] = array(
					'punch_card_number' => '',
					'customer' => '',
					'expiration_date' => '',
					'details' => ''
				);
				unset($row['is_punch_card']);
			}

			// Break apart items into array
			$row['items'] = $this->parse_sub_items($row['items']);
			foreach($row['items'] as $subItem){
				$subItemIds[] = (int) $subItem['item_id'];
			}				
			
			// Attach amount of loyalty points item is worth
			if($this->config->item('use_loyalty') == 1){
				$this->Item_model->get_loyalty_points($row);
			}

			$this->get_loyalty_points($row);
		}

		$this->apply_sub_item_details($rows, $subItemIds);

		return array_values($rows);
	}

	private function apply_sub_item_details(&$rows, $subItemIds = []){
		
		if(empty($subItemIds)){
			return false;
		}

		$this->load->model('v2/Item_model');
		$items = $this->Item_model->get(['item_id' => $subItemIds]);
		
		foreach($items as $key => $item){
			$items[$item['item_id']] = $item;
			unset($items[$key]);
		}

		foreach($rows as &$row){
			if(empty($row['items'])){
				continue;
			}
			foreach($row['items'] as &$subItem){
				if(!isset($items[$subItem['item_id']]))
					continue;
				$itemDetails = $items[$subItem['item_id']];
				$subItem = array_merge($itemDetails, $subItem);

				// Sub items of a punch card being sold should be non-taxable (tax is paid when punch card is redeemed)
				if($row['item_type'] == 'punch_card'){
					$subItem['taxes'] = [];
				}

				if(!empty($subItem['price_class_name'])){
					$subItem['name'] .= ' - '.$subItem['price_class_name'];
				}
				if(!empty($subItem['timeframe_name'])){
					$subItem['name'] .= ' '.$subItem['timeframe_name'];
				}				
			}
		}
	}

	private function parse_sub_items($items_raw){
		
		$items_raw = explode("\x1E", $items_raw);
		$items = [];

		// Loop through each item row, break apart fields
		foreach($items_raw as $item_row){
			
			$item = explode("\x1F", $item_row);
			if(empty($item[0])){
				continue;
			}
			
			$item = array_pad($item, 12, null);
			$item = array(
				'item_id' => (int) $item[0],
				'name' => $item[1],
				'department' => $item[2],
				'category' => $item[3],
				'subcategory' => $item[4],
				'base_price' => (float) $item[5],
				'cost_price' => (float) $item[6],
				'quantity' => (float) $item[7],
				'unit_price' => (float) $item[8],
				'price_class_id' => isset($item[9]) ? $item[9] : 0,
				'price_class_name' => isset($item[10]) ? $item[10] : '',
				'timeframe_id' => isset($item[11]) ? $item[11] : '',
				'timeframe_name' => isset($item[12]) ? $item[12] : ''
			);

			$item['base_quantity'] = $item['quantity'];
			$item['total'] = round($item['quantity'] * $item['unit_price'], 2);
			$item['total_cost'] = round($item['quantity'] * $item['cost_price'], 2);

			$items[] = $item;
		}

		return $items;
	}

	public function save($item_kit_id = false, $data = null){
		
		$data = elements(['item_kit_number', 'name', 'department', 'category', 'subcategory', 
			'unit_price', 'cost_price', 'is_punch_card', 'description', 'items','max_discount'], $data, '');

		if(empty($data['items'])){
			return false;
		}

		if(empty($data['name'])){
			return false;
		}

		if(empty($data['department'])){
			return false;
		}

		$data['unit_price'] = clean_number($data['unit_price']);
		$data['cost_price'] = clean_number($data['cost_price']);
		$data['is_punch_card'] = (int) $data['is_punch_card'];
		$data['course_id'] = (int) $this->session->userdata('course_id');
		$items = $data['items'];

		unset($data['items']);

		if(empty($data['item_kit_number'])){
			$data['item_kit_number'] = NULL;
		}

		if(empty($item_kit_id)){
			$this->db->insert('item_kits', $data);
			$item_kit_id = $this->db->insert_id();
		
		}else{
			$this->db->update('item_kits', $data, ['item_kit_id' => $item_kit_id]);
		}
		$this->save_sub_items($item_kit_id, $items);

		return $item_kit_id;
	}

	private function save_sub_items($item_kit_id, $items = []){
		
		if(empty($items) || empty($item_kit_id)){
			return false;
		}

		$this->db->delete('item_kit_items', ['item_kit_id' => $item_kit_id]);
		
		foreach($items as $item){
			
			if(empty($item['item_id'])){
				continue;
			}

			$item = elements(['item_id', 'quantity', 'unit_price', 'price_class_id', 'timeframe_id'], $item);
			$item['item_kit_id'] = $item_kit_id;
			$item['unit_price'] = clean_number($item['unit_price']);
			
			if(empty($item['price_class_id'])){
				$item['price_class_id'] = null;
			}
			if(empty($item['timeframe_id'])){
				$item['timeframe_id'] = null;
			}

			$this->db->insert('item_kit_items', $item);
		}

		return true;
	}

	public function get_loyalty_points(&$item_kit){

		if($this->loyalty_rates === null){
			$this->loyalty_rates = array();
			$this->load->model('Customer_loyalty');
			$loyalty = $this->Customer_loyalty->get_rates();

			foreach($loyalty->result_array() as $loyalty_row){
				$this->loyalty_rates[$loyalty_row['type']][$loyalty_row['value']] = $loyalty_row;
			}
		}
		
		$cost = false;
		$reward = false;
		if(isset($this->loyalty_rates['item_kit'][$item_kit['item_kit_id']])){
			$reward = (float) $this->loyalty_rates['item_kit'][$item_kit['item_kit_id']]['points_per_dollar'];
			$cost = (float) $this->loyalty_rates['item_kit'][$item_kit['item_kit_id']]['dollars_per_point'];

		}else if(isset($this->loyalty_rates['department'][$item_kit['department']])){
			$reward = (float) $this->loyalty_rates['department'][$item_kit['department']]['points_per_dollar'];
			$cost = (float) $this->loyalty_rates['department'][$item_kit['department']]['dollars_per_point'];

		}else if(isset($this->loyalty_rates['category'][$item_kit['category']])){
			$reward = (float) $this->loyalty_rates['category'][$item_kit['category']]['points_per_dollar'];
			$cost = (float) $this->loyalty_rates['category'][$item_kit['category']]['dollars_per_point'];

		}else if(isset($this->loyalty_rates['subcategory'][$item_kit['subcategory']])){
			$reward = (float) $this->loyalty_rates['subcategory'][$item_kit['subcategory']]['points_per_dollar'];
			$cost = (float) $this->loyalty_rates['subcategory'][$item_kit['subcategory']]['dollars_per_point'];
		}

		$item_kit['loyalty_points_per_dollar'] = $reward;
		$item_kit['loyalty_dollars_per_point'] = round($cost / 100, 7);
		return true;
	}

	public function total_records(){
		return $this->total;	
	}

	public function get_csv(){

		$this->load->helper('report');
		$data = $this->get();
		$rows = array();
		$row = array(
			"UPC/EAN/ISBN", "Name", "Department", "Category", "Sub-Category", "Cost", "Unit Price", "Description"
		);
		$rows[] = $row;

		foreach ($data as $r) {

			$row = array(
				$r['item_number'],
				$r['name'],
				$r['department'],
				$r['category'],
				$r['subcategory'],
				$r['cost_price'],
				$r['unit_price'],
				$r['description']
			);

			$rows[] = $row;
		}

		$content = array_to_csv($rows);

		return $content;
	}

	function delete($item_kit_id){

		if(empty($item_kit_id)){
			return false;
		}

		$this->db->where('course_id', (int) $this->session->userdata('course_id'));
		$this->db->where('item_kit_id', $item_kit_id);
		$this->db->update('item_kits', ['deleted' => 1, 'item_kit_number' => null]);

		return $this->db->affected_rows();
	}	
}