<?php
class Meal_course_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	function get($params = null){
		
		$params = elements(array('meal_course_id'), $params, null);
		
		if(!empty($params['meal_course_id'])){
			$this->db->where('meal_course_id', (int) $params['meal_course_id']);
		}
		
		$this->db->select('*');
		$this->db->from('meal_courses');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->order_by('order ASC');
		$meal_courses = $this->db->get()->result_array();
		
		if(!empty($params['meal_course_id']) && count($meal_courses) == 1){
			return $meal_courses[0];
		}
		
		return $meal_courses;
	}
	
	function get_table_fired_courses($params = null){
		
		$params = elements(array('sale_id'), $params, null);
		
		if(empty($params['sale_id'])){
			return false;
		}		
		
		$this->db->select("meal_course.*, UNIX_TIMESTAMP(MAX(ticket.date_created)) AS date_fired, 
			ticket.employee_id AS fired_employee_id, 
			CONCAT(employee.first_name,' ',employee.last_name) AS fired_employee_name", false);
		$this->db->from('meal_courses AS meal_course');
		$this->db->join('table_tickets AS ticket', 'ticket.meal_course_id = meal_course.meal_course_id AND ticket.sale_id ='.(int) $params['sale_id'], 'left');
		$this->db->join('people AS employee', 'employee.person_id = ticket.employee_id', 'left');
		$this->db->where('meal_course.course_id', $this->session->userdata('course_id'));
		$this->db->order_by('meal_course.order ASC');
		$this->db->group_by('meal_course.meal_course_id');

		$meal_courses = $this->db->get()->result_array();
		
		foreach($meal_courses as &$meal_course){
			if(!empty($meal_course['date_fired'])){
				$meal_course['date_fired'] = date('Y-m-d H:i:s', $meal_course['date_fired']);
			}
		}
		
		return $meal_courses;
	}	
	
	// Returns meal course data in an array for a drop down menu
	public function get_menu(){
		$meal_courses = $this->get();
		$menu = array();
		
		if(empty($meal_courses)){
			return $menu;
		}
		
		foreach($meal_courses as $meal_course){
			$menu[$meal_course['meal_course_id']] = '('.$meal_course['order'].') '.$meal_course['name'];
		}
		return $menu;		
	}	
	
	function save($meal_course_id, $data){
		
		$data = elements(array('name', 'order'), $data);
		$data['course_id'] = (int) $this->session->userdata('course_id');
		$data['order'] = (int) $data['order'];
		
		if(!empty($meal_course_id)){
			$this->db->where('meal_course_id', (int) $meal_course_id);
			$this->db->update('meal_courses', $data);
		
		}else{
			$this->db->insert('meal_courses', $data);
			$meal_course_id = (int) $this->db->insert_id();
		}
		
		return (int) $meal_course_id;
	}
	
	function delete($meal_course_id){
		
		$meal_course_id = (int) $meal_course_id;
		$course_id = (int) $this->session->userdata('course_id');
		
		$this->db->delete('meal_courses', array('course_id' => $course_id, 'meal_course_id' => $meal_course_id));
		
		return $this->db->affected_rows();
	}
}
