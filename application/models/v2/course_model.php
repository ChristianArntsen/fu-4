<?php
class Course_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}		

	public function get($course_id){
		return $this->db->get_where('courses', array('course_id' => (int) $course_id))->row_array();
	}
	
	public function save($course_id = null, $data){
		
		if(empty($course_id)){
			return false;
		}
		
		$data = elements(array('after_sale_load', 'area_code',
			'auto_gratuity', 'blind_close', 'booking_rules', 'cash_drawer_on_cash',
			'cash_register', 'city', 'close_time', 'country', 'county',
			'credit_category', 'credit_category_name', 'credit_department', 'credit_subtotal_only',
			'credit_department_name', 'currency_format', 'currency_symbol',
			'currency_symbol_placement', 'customer_credit_nickname',
			'date_format', 'default_register_log_open', 'fax', 'hide_back_nine', 
			'language', 'minimum_food_spend',
			'name', 'no_show_policy', 'phone', 'postal', 'print_after_sale', 
			'print_credit_card_receipt', 'print_sales_receipt', 'print_tip_line',
			'print_two_receipts', 'print_two_receipts_other', 'print_two_signature_slips',
			'return_policy', 'state', 'timezone', 'tip_line', 'track_cash', 
			'use_loyalty', 'use_terminals', 'webprnt', 'webprnt_cold_ip',
			'webprnt_hot_ip', 'webprnt_ip', 'webprnt_label_ip', 'website', 
			'zip'), $data, null);
		
		// Filter out fields that were not passed in	
		foreach($data as $key => $val){
			if($val === null){
				unset($data[$key]);
			}
		}
		
		$this->db->update('courses', $data, array('course_id' => (int) $course_id));
		return $this->db->affected_rows();
	}

	public function get_customer_account_spending_restriction(){
		
		// If payment is from customer account, and course has limits 
		// on what type of items can be purchased with that acccount
		$limit_category = $this->config->item('credit_category_name');
		$limit_department = $this->config->item('credit_department_name');

		if(
			($this->config->item('credit_category') == 1 && !empty($limit_category) && $limit_category != 'all') ||
			($this->config->item('credit_department') == 1 && !empty($limit_department) && $limit_department != 'all')
		){
			if($this->config->item('credit_category') == 1){
				$filter_by = 'category';
				$filter = $limit_category;
			}else{
				$filter_by = 'department';
				$filter = $limit_department;
			}
			
			return ['field' => $filter_by, 'value' => $filter];
    	}		

    	return false;
	}
}
