<?php
use Carbon\Carbon;

class Customer_model extends MY_Model
{

    function __construct()
    {

        parent::__construct();
        $this->course_ids = array($this->session->userdata('course_id'));

        $this->fields = array(
            'type' => array('name' => 'First Name', 'required' => true, "teesheet_required" => false, "online_booking_required" => false),
            'last_name' => array('name' => 'Last Name', 'required' => true, "teesheet_required" => false, "online_booking_required" => false),
            'email' => array('name' => 'Email', 'required' => false, "teesheet_required" => false, "online_booking_required" => false),
            'cell_phone_number' => array('name' => 'Cell Phone', 'required' => false),
            'phone_number' => array('name' => 'Phone', 'required' => false, "teesheet_required" => false, "online_booking_required" => false),
            'birthday' => array('name' => 'Birthday', 'required' => false),
            'address_1' => array('name' => 'Address', 'required' => false),
            'city' => array('name' => 'City', 'required' => false),
            'state' => array('name' => 'State', 'required' => false),
            'zip' => array('name' => 'Zip', 'required' => false, "teesheet_required" => false, "online_booking_required" => false),
            'country' => array('name' => 'Country', 'required' => false),
            'account_number' => array('name' => 'Account Number', 'required' => false)
        );

        $this->teetime_data = false;
        $this->send_booking_credentials = true;
    }

    // Search item database and return matched customer IDs
    private function search_customers($search, $force_fuzzy_search = false)
    {
        $original_search = $search;
        $search = str_replace(array('(', ')', '-', '_', ','), '', $search);
        // If search is an account number
        if (strlen($search) > 8 && is_numeric($search) && !$force_fuzzy_search) {
            $this->readOnlyDB->or_where('account_number', $search);
            $this->readOnlyDB->or_where('account_number', $original_search);
            // If just a standard search
        } else {
            $this->readOnlyDB->where(
                "(first_name LIKE '%" . $this->db->escape_like_str($original_search) . "%' OR last_name LIKE '%" . $this->db->escape_like_str($original_search) . "%' " .
                "OR CONCAT(first_name,' ',last_name) LIKE '%" . $this->db->escape_like_str($original_search) . "%' " .
                "OR phone_number LIKE '%" . $this->db->escape_like_str($search) . "%' OR email LIKE '%" . $this->db->escape_like_str($original_search) . "%'" .
                "OR cell_phone_number LIKE '" . $this->db->escape_like_str(str_replace(array('(', ')', '-', '_', ',', ' '), '', $search)) . "%'" .
                " OR account_number LIKE '%" . $this->db->escape_like_str($search) . "%'" .
                " OR account_number LIKE '%" . $this->db->escape_like_str($original_search) . "%')"
            );
        }

        $this->readOnlyDB->select('people.person_id');
        $this->readOnlyDB->from('people');
        $this->readOnlyDB->join('customers', 'customers.person_id = people.person_id', 'inner');
        $this->readOnlyDB->where("customers.deleted", 0);
        $this->readOnlyDB->where("customers.hide_from_search", 0);
        $this->readOnlyDB->where_in('customers.course_id', $this->course_ids);
        $this->readOnlyDB->order_by("last_name, first_name", "ASC");
        $this->readOnlyDB->limit(30);
        $rows = $this->readOnlyDB->get()->result_array();

        if (count($rows) == 0 && !$force_fuzzy_search) {
            return $this->search_customers($search, true);
        }

        $ids = array();
        foreach ($rows as $row) {
            if (empty($row['person_id'])) {
                continue;
            }
            $ids[] = (int)$row['person_id'];
        }

        return $ids;
    }
}