<?php
class Easypicker_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    public function get($pin_number){

        $course_id = $this->session->userdata('course_id');

        // Select list of employees with photos
        $this->db->from('easypicker_pins');
        //$this->db->where('date_used IS NULL');
        //$this->db->where('course_id', $course_id);
        $this->db->where('pin', $pin_number);
        $this->db->limit(1);
        $row = $this->db->get()->row_array();

        return $row;
    }

    public function generate($bucket_size){
        $unique_pin = false;
        $tries = 0;

        while ($unique_pin && $tries < 1000) {
            $new_pin = rand(1, 999999);
            $existing_pin = $this->get(array('pin_number'=>$new_pin));

            if (empty($existing_pin)) {
                $unique_pin = $new_pin;
            }

            $tries++;
        }

        if ($tries == 1000) {
            return false;
        }

        $data = array(
            'course_id' => $this->config->item('course_id'),
            'pin' => $unique_pin,
            'bucket_size' => $bucket_size,
            'number_of_buckets' => 1,
            'date_created' => gmdate(),
            'valid' => 1
        );
        $this->db->insert('easypicker_pins', $data);

        return $unique_pin;
    }

    public function delete($pin_number){
        $this->db->where('pin', $pin_number);
        $success = $this->db->update('easypicker_pins', array('date_used'=>gmdate()));

        return array('success'=>$success);
    }
}
