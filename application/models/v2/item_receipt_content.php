<?php
class Item_receipt_content extends CI_Model {
	
	function delete($receipt_content_id){
		
		if (empty($receipt_content_id)){
			return false;
		}
		$this->db->delete('item_receipt_content', ['receipt_content_id' => $receipt_content_id]);
		$deleted = $this->db->affected_rows();
		
		$this->db->update('items', ['receipt_content_id' => null], [
		    'course_id' => $this->session->userdata('course_id'),
            'receipt_content_id' => $receipt_content_id
        ]);

		return $deleted;
	}

	function save($receipt_content_id, $data = []){

		$data = elements(['name', 'content', 'signature_line', 'separate_receipt'], $data);
		$data['course_id'] = (int) $this->session->userdata('course_id');

		if(empty($receipt_content_id)){
			$this->db->insert('item_receipt_content', $data);
			$receipt_content_id = $this->db->insert_id();
		
		}else{
			$this->db->where('receipt_content_id', $receipt_content_id);
			$this->db->update('item_receipt_content', $data);
		}

		return $receipt_content_id;
	}

	function save_item_receipt_content($receipt_content_id, $item_ids){

	    if(empty($receipt_content_id)){
	        $receipt_content_id = null;
        }

        if(!empty($item_ids)){
            $this->db->where_in('item_id', $item_ids);
            $this->db->update('items', ['receipt_content_id' => $receipt_content_id]);
        }
    }

	function get($params = []){

	    $this->db->from('item_receipt_content');
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $rows = $this->db->get()->result_array();

        if(empty($rows)){
            return [];
        }

        foreach($rows as &$row){
            $row['receipt_content_id'] = (int) $row['receipt_content_id'];
            $row['signature_line'] = ($row['signature_line'] == 1);
            $row['separate_receipt'] = ($row['separate_receipt'] == 1);
        }

        return $rows;
    }
}