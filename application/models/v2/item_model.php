<?php
class Item_model extends CI_Model {

	function __construct(){
		parent::__construct();
		parent::__construct();
		$this->loyalty_rates = null;
	}
	
	function save($item_id = null, $data){
		
		$pass_data = elements(['pass_price_class_id', 'pass_restrictions', 
			'pass_days_to_expiration', 'pass_expiration_date', 'pass_multi_customer','pass_enroll_loyalty'], $data, null);

		$service_fee_data = elements(['parent_item_percent', 'whichever_is', 'service_fee_id'], $data, null);

		$data = elements(array('name', 'department', 'category', 'receipt_content_id',
			'subcategory', 'supplier_id', 'item_number', 'description',
			'image_id', 'cost_price', 'unit_price','unit_price_includes_tax',
            'inventory_level', 'inventory_unlimited','erange_size',
            'quickbooks_income','quickbooks_cogs','quickbooks_assets',
			'max_discount', 'quantity', 'is_unlimited', 'reorder_level',
			'food_and_beverage', 'number_of_sides', 'location', 'gl_code',
			'allow_alt_description', 'is_serialized', 'item_type',
			'invisible', 'deleted', 'is_side', 'soup_or_salad', 'add_on_price',
			'print_priority', 'kitchen_printer', 'do_not_print', 'inactive',
            'meal_course_id','prompt_meal_course','do_not_print_customer_receipt',
			'is_fee', 'taxes', 'supplier_id', 'is_shared', 'upcs', 'customer_groups',
			'force_tax','is_service_fee','unit_price_includes_tax', 'service_fees'), $data, null);

		$course_id = (int) $this->session->userdata('course_id');
		$course_ids = array();
		$this->Course->get_linked_course_ids($course_ids, false, $course_id);
		$this->course_ids = $course_ids;
		
		$taxes = $data['taxes'];
		$upcs = $data['upcs'];
		$service_fees = $data['service_fees'];
		$customer_groups = $data['customer_groups'];
		$data['quantity'] = $data['inventory_level'];
		$data['is_unlimited'] = $data['inventory_unlimited'];
		
		unset(
			$data['taxes'], 
			$data['inventory_level'], 
			$data['inventory_unlimited'],
			$data['upcs'],
			$data['customer_groups'],
			$data['service_fees']
		);

		foreach($data as $key => $val){
			if($key == 'force_tax'){
				continue;
			}
			if($val === null){
				unset($data[$key]);
			}
		}

		if(!empty($data['item_number'])){
			$item_found_id = $this->check_item_number($data['item_number']);

			if($item_found_id && (int) $item_found_id != (int) $item_id){
				$this->error = "The UPC '{$data['item_number']}' is already taken";
				return false;
			}
		}	
		
		if(isset($data['is_shared'])){
			$data['is_shared'] = (int) $data['is_shared'];
		}

		if(isset($data['item_number']) && empty($data['item_number'])){
			$data['item_number'] = null;
		}

		if(isset($data['is_unlimited'])){
			$data['is_unlimited'] = (int) (bool) $data['is_unlimited'];
		}

		if(isset($data['is_unlimited']) && $data['is_unlimited'] == 1){
			unset($data['quantity'], $data['reorder_level']);
		}

		$type = $data['item_type'];
		if(!empty($data['item_type'])){
			if($data['item_type'] == 'item'){
				$data['is_giftcard'] = 0;
				$data['is_pass'] = 0;
				$data['is_service_fee'] = 0;
			
			}else if($data['item_type'] == 'giftcard'){
				$data['is_giftcard'] = 1;
				$data['is_pass'] = 0;
				$data['is_service_fee'] = 0;
			
			}else if($data['item_type'] == 'pass'){
				$data['is_giftcard'] = 0;
				$data['is_pass'] = 1;
				$data['is_service_fee'] = 0;
			
			}else if($data['item_type'] == 'service_fee'){
				$data['is_giftcard'] = 0;
				$data['is_pass'] = 0;
				$data['is_service_fee'] = 1;
				$data['unit_price_includes_tax'] = 0;
			}
		}
		
		// Do not allow sharing other types of items yet (just passes)
		if($data['item_type'] != 'pass'){
			$data['is_shared'] = 0;
		}
		unset($data['item_type']);

		if(isset($data['supplier_id']) && $data['supplier_id'] == 0){
			$data['supplier_id'] = null;
		}

		// If saving a new item
		if(empty($item_id)){
			$current_inventory_level = 0;
			$data['course_id'] = $course_id;
			$this->db->insert('items', $data);
			$item_id = (int) $this->db->insert_id();
		
		// If updating an existing item
		}else{
			$row = $this->db->select('quantity')
				->from('items')
				->where('item_id', (int) $item_id)
				->get()->row_array();
			$current_inventory_level = (int) $row['quantity'];

			$this->db->where_in('course_id', $this->course_ids);
			$this->db->update('items', $data, array('item_id' => $item_id));
		}

		if(isset($data['quantity']) && empty($data['is_unlimited']) && $data['quantity'] != $current_inventory_level){
			$this->save_inventory($item_id, $data['quantity'], $current_inventory_level);
		}

		$this->save_taxes($item_id, $taxes);
		$this->save_upcs($item_id, $upcs);
		if(!isset($customer_groups)){
			$customer_groups = [];
		}
		$this->save_customer_groups($item_id, array_column($customer_groups, 'group_id'));

		if($type == 'pass'){
			$this->load->model('v2/Pass_item_model');
			$this->Pass_item_model->save($item_id, $pass_data, $data['is_shared']);
		}

		if($type == 'service_fee'){
			$service_fee_data['item_id'] = $item_id;
			$ServiceFee = new \fu\service_fees\ServiceFee();
			$affected = $ServiceFee->save($service_fee_data);
		}

		// Clear out all attached service fees
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$ServiceFee->deleteServiceFeesForItem($item_id);
        
        // Assign service fees to item
		if($type != 'service_fee' && !empty($service_fees)){
            foreach($service_fees as $service_fee){
                $ServiceFee = new \fu\service_fees\ServiceFee($service_fee['service_fee_id']);
                $order = isset($service_fee['order']) ? $service_fee['order'] : 0;
                $ServiceFee->applyToItems($item_id, $order);
            }
        }
		
		return $item_id;
	}

	private function save_inventory($item_id, $inventory_level = 0, $current_inventory_level = 0){

		$this->load->model('Inventory');
		$inv_data = array(
			'trans_date' => date('Y-m-d H:i:s'),
			'trans_items' => $item_id,
			'trans_user' => $this->session->userdata('person_id'),
			'trans_comment' => lang('items_manually_editing_of_quantity'),
			'trans_inventory' => ($inventory_level - $current_inventory_level),
            'course_id' => $this->session->userdata('course_id')
		);

		return $this->Inventory->insert($inv_data);
	}

	private function save_upcs($item_id, $upcs){
		
		$existing_upcs = [];
		if(!empty($upcs)){
			foreach($upcs as $key => $upc){
				if(!empty($upc['upc_id'])){
					$existing_upcs[] = (int) $upc['upc_id'];
				}
			}
		}

		if(!empty($existing_upcs)){
			$this->db->where_not_in('upc_id', $existing_upcs);
		}
		$this->db->delete('items_upcs', [
			'item_id' => (int) $item_id
		]);

		if(empty($upcs)){
			return false;
		}

		$course_id = (int) $this->session->userdata('course_id');
		$item_id = (int) $item_id;
		foreach($upcs as $upc){
			$upc_name = $this->db->escape($upc['upc']);
			$this->db->query("INSERT IGNORE INTO foreup_items_upcs (upc, item_id, course_id)
				VALUES ({$upc_name}, {$item_id}, {$course_id})");
		}

		return true;
	}

	private function save_taxes($item_id, $taxes){
		
		$this->db->delete('items_taxes', [
			'item_id' => (int) $item_id
		]);

		if(empty($taxes) || empty($taxes[0])){
			return false;
		}

		// Get original course_id from item 
		// (in case the item is shared an a different course is editing it)
		$item = $this->db->select('course_id')
			->from('items')
			->where('item_id', $item_id)
			->get()
			->row_array();
		$course_id = (int) $item['course_id'];

		$rows = [];
		foreach($taxes as $tax){
			
			if(empty($tax['name']) || empty($tax['percent'])){
				continue;
			}

			$cumulative = 0;
			if(!empty($tax['cumulative'])){
				$cumulative = $tax['cumulative'];
			}
			
			$rows[] = [
				'name' => $tax['name'],
				'percent' => round($tax['percent'], 3),
				'item_id' => (int) $item_id,
				'course_id' => (int) $course_id,
				'cumulative' => (int) $cumulative
			]; 
		}

		if(!empty($rows)){
			$this->db->insert_batch('items_taxes', $rows);
		}

		return $this->db->affected_rows();
	}
	
	// Search item database and return matched item IDs
	private function search_items($search, $item_type = false){

		$course_id_list = implode(',', $this->course_ids);
		$course_id = (int) $this->session->userdata('course_id');

		$this->db->select('item_id');
		$this->db->from('items');
		$this->db->where("deleted", 0);
		$this->db->where('inactive', 0);
		$this->db->where("(
			course_id = {$course_id} OR (
				is_shared = 1 AND course_id IN ({$course_id_list})
			)
		)");
		$this->db->where('invisible !=', 1);
        $this->db->where("name LIKE '%".$this->db->escape_like_str($search)."%'");
		$this->db->limit(100);

		if(!empty($item_type)){
			if($item_type == 'service_fee'){
				$this->db->where('is_service_fee', 1);
			}
		}

		$rows = $this->db->get()->result_array();

		$ids = array();
		foreach($rows as $row){
			$ids[] = (int) $row['item_id'];
		}

		return $ids;
	}

	// Sort taxes so cumulative taxes are last in list
	private function sort_tax($a, $b){
		if((int) $a['cumulative'] > (int) $b['cumulative']){
			return 1;
		}else{
			return 0;
		}
	}

	function save_customer_groups($item_id, $customer_group_ids = [], $replace = true){
		
		if($replace){
			$this->db->delete('item_customer_groups', ['item_id' => (int) $item_id]);	
		}
		
		if(!is_array($item_id)){
			$item_id = [$item_id];
		}

		$groups = [];
		if(!empty($customer_group_ids)){
			
			foreach($item_id as $i_id){
				foreach($customer_group_ids as $group_id){
					$groups[] = [
						'customer_group_id' => (int) $group_id,
						'item_id' => (int) $i_id
					];
				}
			}

			$sql = "INSERT IGNORE INTO foreup_item_customer_groups (item_id, customer_group_id) VALUES ";
			foreach($groups as $group){
				$sql .= '('.$group['item_id'].','.$group['customer_group_id'].'),';
			}
			$sql = trim($sql, ',');

			$this->db->query($sql);
		}

		return $this->db->affected_rows();
	}

	function get_upcs($upc_id = null, $params){
		
		$params = elements(['item_id', 'upc'], $params, null);

		$this->db->from('items_upcs')
			->where('course_id', $this->session->userdata('course_id'));

		if(!empty($params['item_id'])){
			$this->db->where('item_id', $params['item_id']);
		}
		
		if(!empty($params['upc'])){
			$this->db->where('upc', $params['upc']);
		}

		return $this->db->get()->result_array();
	}
	
	function check_item_number($number){
		
		$result = $this->db->query("SELECT item_id 
			FROM foreup_items
			WHERE (item_number = ".$this->db->escape($number)." OR item_id = ".(int) $number." OR item_number = ".$this->db->escape(ltrim($number,'0')).")
				AND course_id = ".(int) $this->session->userdata('course_id'));
		$row = $result->row_array();
		
		// If no items were found by item number/item ID, try item UPCs
		if(empty($row['item_id'])){	
			$this->load->model('item_upc');
			$row['item_id'] = $this->item_upc->get_item_id($number);
		}
		
		if(empty($row['item_id'])){
			return false;
		}
		
		return (int) $row['item_id'];
	}

	private function get_tax_array($tax_field){

		$taxes = array();
		$taxesArray = explode(',', $tax_field);

		foreach($taxesArray as $tax){
			$taxArray = explode('^:^', $tax);

			if(empty($taxArray[0]) && empty($taxArray[1])){
				continue;
			}
			$taxes[] = array(
				'name' => $taxArray[0], 
				'percent' => (float) $taxArray[1], 
				'cumulative' => $taxArray[2]
			);
		}
		uasort($taxes, array($this, 'sort_tax'));

		return array_values($taxes);
	}
	
	private function get_modifiers_array($modifiers){
		
		if(empty($modifiers)){
			return array();
		}		
		
		$modifiers = explode("\x1E", $modifiers);
		$modifier_list = array();

		// Loop through each item row, break apart fields
		foreach($modifiers as $key => $modifier_row){
			
			$modifier_array = explode("\x1F", $modifier_row);	
			if(count($modifier_array) < 7){
				continue;
			}

			$modifier = array(
				'modifier_id' => (int) $modifier_array[0],
				'name' => $modifier_array[1],
				'price' => $modifier_array[2],
				'options' => json_decode($modifier_array[3], true),
				'category_id' => (int) $modifier_array[4],
				'required' => (bool) $modifier_array[5],
				'default' => $modifier_array[6],
				'multi_select' => (int) $modifier_array[7],
				'selected_price' => 0,
				'selected_option' => false
			);
			
			// If modifier can have multiple options selected, turn selected options into arrays
			if($modifier['multi_select'] == 1){
				$modifier['selected_option'] = json_decode($modifier['default'], true);
				$modifier['default'] = json_decode($modifier['default'], true);
			}
			
			$modifier['selected_price'] = $this->get_modifier_price($modifier);
			$modifier_list[] = $modifier;
		}
		
		return $modifier_list;
	}
	
	public function get_modifier_price($modifier){
		
		$price = 0.00;
		if(empty($modifier['options']) || empty($modifier['selected_option'])){
			return $price;
		}
		
		foreach($modifier['options'] as $option){
			
			if($modifier['multi_select'] == 1 && is_array($modifier['selected_option'])){
				foreach($modifier['selected_option'] as $selected_label){
					if(strtolower($selected_label) == strtolower($option['label'])){
						$price += (float) round($option['price'], 2);
					}					
				}
				
			}else{
				if(strtolower($modifier['selected_option']) == strtolower($option['label'])){
					$price = (float) round($option['price'], 2);
				}
			}
		}
		
		return $price;		
	}
	
	private function get_sides_data($sides, &$item){
		
		if(empty($sides)){
			return true;
		}
		$sides = explode("\x1E", $sides);

		$item['hidden_sides'] = array();
		$item['side_prices'] = array();
		
		// Loop through each item row, break apart fields
		foreach($sides as $side_row){
			
			$side_array = explode("\x1F", $side_row);
			
			if(empty($side_array[0])){
				continue;
			}
			
			$sideItemId =  $side_array[0];
			$sideHidden = $side_array[1];
			$sidePrice = false;
			
			if($side_array[2] != ''){
				$sidePrice = (float) $side_array[2];
			}
			if($sideHidden){
				$item['hidden_sides'][$sideItemId] = 1;
			}
			if($sidePrice !== false){
				$item['side_prices'][$sideItemId] = $sidePrice;
			}
		}
		unset($item['sides']);
	}

	private function get_upc_array($upc_field){
		
		$upcs = array();
		$upcsArray = explode("\x1E", $upc_field);

		foreach($upcsArray as $upc){
			$upcArray = explode("\x1F", $upc);

			if(empty($upcArray[0]) && empty($upcArray[1])){
				continue;
			}
			$upcs[] = array(
				'upc_id' => (int) $upcArray[0], 
				'upc' => $upcArray[1]
			);
		}

		return array_values($upcs);		
	}
	
	function get_customer_group_array($customer_groups){
		
		$groups = [];
		if(empty($customer_groups)){
			return $groups;
		}
		$customer_groups = explode("\x1E", $customer_groups);

		foreach($customer_groups as $group_row){
			
			$group_array = explode("\x1F", $group_row);
			
			if(empty($group_array[0])){
				continue;
			}

			$group = [
				'group_id' => (int) $group_array[0],
				'label' => $group_array[1]
			];
			
			if(!isset($group_array[2])){
				$cost_plus = null;
			}else{
				$cost_plus = (float) $group_array[2];
			}
			$group['item_cost_plus_percent_discount'] = $cost_plus;
			$groups[] = $group;
		}

		return $groups;
	}

	private function where($params){

		if(!empty($params['item_number'])){
			$this->db->where('item_number', $params['item_number']);
		}
		
		if(empty($params['include_inactive'])){
			$this->db->where('i.inactive', 0);
		}

		if(!empty($params['item_type'])){
			if(in_array('pass', $params['item_type'])){
				$this->db->where('i.is_pass', 1);
			}
		}

		if(!empty($params['item_id'])){
			if(is_array($params['item_id'])){
				$ids = array();
				foreach($params['item_id'] as $item_id){
					$ids[] = (int) $item_id;
				}
				$this->db->where_in('i.item_id', array_unique($ids));

			}else{
				$this->db->where('i.item_id', (int) $params['item_id']);
			}
		}

		$course_id = (int) $this->session->userdata('course_id');
		if(isset($params['course_ids'])){
			$this->db->where_in('i.course_id', $params['course_ids']);
		
		}else{
			$course_id_list = implode(',', $this->course_ids);
			$this->db->where("(i.course_id = {$course_id} OR (i.is_shared = 1 AND i.course_id IN ({$course_id_list})))");
		}

		if(empty($params['include_deleted'])){
			$this->db->where('i.deleted', 0);
		}
		
		if(empty($params['item_id'])){
			$this->db->where('i.invisible', 0);
		}
	}

	public function get($params = []){

		$course_id = $this->session->userdata('course_id');
		$course_ids = array();
		$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);
		$this->course_ids = $course_ids;

		$this->load->model("v2/Printer_group_model");
		$this->load->model("v2/Cart_model");
		$this->load->model("v2/Pass_item_model");
		$feeItemIds = array();
		$itemKitIds = array();
		$this->total = 0;

		$params = elements(array('item_id', 'q', 'item_type', 'include_inactive', 'limit', 
			'offset', 'sort', 'order','course_ids', 'include_deleted'), $params, null);

		if(empty($params['item_type'])){
			$params['item_type'] = false;
		}

		if(!empty($params['item_type']) && !is_array($params['item_type'])){
			$params['item_type'] = [$params['item_type']];
		}

		// If searching items
		if(!empty($params['q'])){

			$this->load->model('Pricing');
			$this->load->model('v2/Tournament_model');
			$this->load->model('v2/Item_kit_model');
			$params['item_id'] = [];

			// Search items table
			if(!$params['item_type'] || in_array('item', $params['item_type'])){
				$params['item_id'] = $this->search_items($params['q']);
			}

			if(!empty($params['item_type'][0]) && $params['item_type'][0] == 'service_fee'){
				$params['item_id'] = $this->search_items($params['q'], 'service_fee');
			}
			
			// Search item kits
			if(!$params['item_type'] || in_array('item_kit', $params['item_type'])){
				$itemKits = $this->Item_kit_model->get(['q' => $params['q']]);
			}

			// Search various green fees/cart fees
			if(!$params['item_type'] || in_array('fee', $params['item_type'])){
				$fees = $this->Pricing->get_fees(array('search' => $params['q']));
				foreach($fees as $fee){
					$feeItemIds[$fee['item_id']] = $fee['item_id'];
				}
			}			
	
			// Search tournaments
			if(!$params['item_type'] || in_array('tournament', $params['item_type'])){
				$tournamentIds = $this->Tournament_model->search($params['q']);
			}			

			$params['item_id'] += array_merge($params['item_id'], array_values($feeItemIds));
			
			if(empty($params['item_id']) && empty($itemKits) && empty($tournamentIds)){
				return array();
			}
		}

		if(empty($params['item_id']) && empty($params['limit'])){
			$params['limit'] = 100;
		}

		if(empty($params['offset'])){
			$params['offset'] = 0;
		}

		if($params['order'] != 'asc' && $params['order'] != 'desc'){
			$params['order'] = 'DESC';
		}

		if(!in_array($params['sort'], ['name', 'item_number', 'department', 'category', 
			'subcategory', 'unit_price', 'cost_price', 'inventory_level'])){
			$params['sort'] = null;
		}	

		// Select list of items with associated taxes
		$rows = array();
		
		if(!empty($params['item_id']) || (empty($params['q']) && empty($params['item_id']))){

			$this->db->select("i.item_id, i.course_id, i.name, i.department,
				i.category, i.subcategory, i.supplier_id, i.item_number, i.description, 
				IF(i.is_side = 1, i.add_on_price, i.unit_price) AS unit_price,
				unit_price AS base_price, i.kitchen_printer, i.inactive, i.do_not_print,
				i.max_discount, i.quantity AS inventory_level, i.is_unlimited AS inventory_unlimited,
				i.cost_price, i.is_giftcard, i.is_side, i.food_and_beverage,
				i.number_of_sides AS number_sides, 0 AS number_salads, 
				0 AS number_soups, i.print_priority, i.is_fee, i.erange_size, i.meal_course_id,
				GROUP_CONCAT(DISTINCT CONCAT(tax.name,'^:^',tax.percent,'^:^',tax.cumulative)) AS taxes,
				i.is_serialized, i.do_not_print_customer_receipt,
				i.unit_price_includes_tax, i.is_shared, i.gl_code,
				i.allow_alt_description, i.reorder_level, i.supplier_id,
				receipt_content.receipt_content_id, receipt_content.content AS receipt_content,
				receipt_content.signature_line AS receipt_content_signature_line, 
				receipt_content.separate_receipt AS receipt_content_separate_receipt,
				i.force_tax, i.is_service_fee, service_fee.parent_item_percent, service_fee.whichever_is,
				service_fee.service_fee_id,

				GROUP_CONCAT(DISTINCT CONCAT_WS(
					0x1F,
					upc.upc_id,
					upc.upc
				) SEPARATOR 0x1E) AS upcs,

				i.is_pass,
				pass.days_to_expiration AS pass_days_to_expiration,
				pass.restrictions AS pass_restrictions,
				pass.price_class_id AS pass_price_class_id,
				pass.expiration_date AS pass_expiration_date,
				pass.multi_customer AS pass_multi_customer,
				pass.enroll_loyalty AS pass_enroll_loyalty,
				
				GROUP_CONCAT(DISTINCT CONCAT_WS(
					0x1F,
					modifier.modifier_id,
					modifier.name,
					modifier.default_price,
					modifier.options,
					modifier.category_id,
					modifier.required,
					IFNULL(item_default_modifier.default, ''),
					modifier.multi_select
				) SEPARATOR 0x1E) AS modifiers,
				
				GROUP_CONCAT(DISTINCT CONCAT_WS(
					0x1F,
					side.side_id,
					side.not_available,
					IFNULL(side.upgrade_price, '')
				) SEPARATOR 0x1E) AS sides,

				GROUP_CONCAT(DISTINCT CONCAT_WS(
					0x1F,
					customer_group.group_id,
					customer_group.label,
					customer_group.item_cost_plus_percent_discount
				) SEPARATOR 0x1E) AS customer_groups,
				
				GROUP_CONCAT(DISTINCT item_printer_group.printer_group_id) AS printer_groups", false);
			$this->db->from('items AS i');
			$this->db->join('pass_definitions AS pass', 'pass.item_id = i.item_id', 'left');
			$this->db->join('service_fees AS service_fee', 'service_fee.item_id = i.item_id', 'left');
			$this->db->join('items_taxes AS tax', 'tax.item_id = i.item_id', 'left');
			$this->db->join('item_sides AS side', 'side.item_id = i.item_id', 'left');
			$this->db->join('items_upcs AS upc', 'upc.item_id = i.item_id', 'left');
			$this->db->join('item_modifiers AS item_default_modifier', "item_default_modifier.item_id = i.item_id", 'left');			
			$this->db->join('modifiers AS modifier', "modifier.modifier_id = item_default_modifier.modifier_id AND modifier.deleted = '0'", 'left');
			$this->db->join('item_printer_groups AS item_printer_group', 'item_printer_group.item_id = i.item_id', 'left');
			$this->db->join('item_customer_groups AS item_customer_group', 'item_customer_group.item_id = i.item_id', 'left');
			$this->db->join('customer_groups AS customer_group', 'customer_group.group_id = item_customer_group.customer_group_id', 'left');
			$this->db->join('item_receipt_content AS receipt_content', 'receipt_content.receipt_content_id = i.receipt_content_id', 'left');

			$this->where($params);

			$this->db->group_by('i.item_id', false);

			if(!empty($params['sort'])){
				$this->db->order_by($params['sort'], $params['order']);
			}
			$this->db->limit($params['limit'], $params['offset']);
			
			// Get rows
			$rows = $this->db->get()->result_array();

			// Count total possible rows
			$this->db->select('COUNT(i.item_id) AS total', false);
			$this->db->from('items AS i');
			$this->where($params);
			$totalRows = $this->db->get()->row_array();
			$this->total = (int) $totalRows['total'];

			// Organize items with taxes as sub-array
			foreach($rows as $key => &$row){

				$item_id = (int) $row['item_id'];
				$row['item_type'] = 'item';
				$row['unit_price_includes_tax'] = (int) $row['unit_price_includes_tax'];

				// Break apart tax column into array
				$row['taxes'] = $this->get_tax_array($row['taxes']);
				
				// Break apart modifiers
				$row['modifiers'] = $this->get_modifiers_array($row['modifiers']);

				$row['customer_groups'] = $this->get_customer_group_array($row['customer_groups']);
				$row['upcs'] = $this->get_upc_array($row['upcs']);

				if(!empty($row['receipt_content_id'])){
					$row['receipt_content'] = [
						'receipt_content_id' => (int) $row['receipt_content_id'],
						'content' => $row['receipt_content'],
						'signature_line' => (bool) $row['receipt_content_signature_line'],
						'separate_receipt' => (bool) $row['receipt_content_separate_receipt']
					];
					unset($row['receipt_content_separate_receipt'],
						$row['receipt_content_signature_line']);
				}else{
					$row['receipt_content'] = false;
				}
				
				if($row['is_side'] == 0){
					$this->get_sides_data($row['sides'], $row);
				}
				
				if($row['food_and_beverage'] == 1){
					$row['number_sides'] = (int) $row['number_sides'];
					$row['number_salads'] = (int) $row['number_salads'];
					$row['number_soups'] = (int) $row['number_soups'];
					$row['print_priority'] = (int) $row['print_priority'];
					
					if($this->session->userdata('multiple_printers') == 1){
						
						$printer_group_ids = explode(',', $row['printer_groups']);
						$row['printers'] = $this->Printer_group_model->get_item_printers(
							$printer_group_ids, 
							$this->session->userdata('terminal_id'),
							(bool) $row['do_not_print']
						);	
					
					}else{
						if($row['kitchen_printer'] == 1){
							$row['printers'][] = $this->config->item('webprnt_hot_ip');
						}else if($row['kitchen_printer'] == 2){
							$row['printers'][] = $this->config->item('webprnt_cold_ip');
						}	
					}
												
				}else{
					unset($row['printers']);
				}
				
				unset($row['kitchen_printer']);
				$row['inventory_level'] = (int) $row['inventory_level'];
				$row['inventory_unlimited'] = (int) $row['inventory_unlimited'];					

				// Attach amount of loyalty points item is worth
				if($this->config->item('use_loyalty') == 1){
					$this->get_loyalty_points($row);
				}

				if($row['is_giftcard']){
					$row['item_type'] = 'giftcard';
					$row['params'] = array(
						'giftcard_number' => '',
						'customer' => '',
						'expiration_date' => '',
						'department' => '',
						'details' => '',
						'action' => 'new'
					);
				}

				if($row['is_pass'] == 1){
					$row['item_type'] = 'pass';
					$row['params'] = array(
						'customer' => '',
						'start_date' => false,
						'end_date' => false
					);
					
					$row['pass_restrictions'] = json_decode($row['pass_restrictions'], true);
					$row['pass_restrictions'] = $this->Pass_item_model->apply_field_labels($row['pass_restrictions']);
				}

				if(stripos($row['item_number'], $this->config->item('course_id').'_seasonal') !== false &&
					$row['category'] == 'Green Fees'){
					$row['item_type'] = 'green_fee';
				}else if(stripos($row['item_number'], $this->config->item('course_id').'_seasonal') !== false &&
					$row['category'] == 'Carts'){
					$row['item_type'] = 'cart_fee';
				}


				if(stripos($row['item_number'], $this->config->item('course_id').'_seasonal_3') !== false ||
					stripos($row['item_number'], $this->config->item('course_id').'_seasonal_4') !== false
				){
					$row['item_type'] = 'cart_fee';
				}



				if($row['is_service_fee']){
					$row['item_type'] = 'service_fee';
				}

                $row['service_fees'] = array();
                $ServiceFee = new \fu\service_fees\ServiceFee();
                $service_fees = $ServiceFee->getServiceFeesForItem($item_id);
                foreach($service_fees as $service_fee){
                    $row['service_fees'][] = $service_fee->getAsArray();
                }
				
				// If current item is a green fee or cart fee
				if(!empty($feeItemIds[$item_id])){

					foreach($fees as &$fee){
						// Merge the item details and fee details
						if($fee['item_id'] == $item_id){
							$fee = array_merge($row, $fee);

							// Attach amount of loyalty points item is worth
							if($this->config->item('use_loyalty') == 1){
								$this->get_loyalty_points($fee);
							}
						}
					}
					// Remove special item from list
					unset($rows[$key]);

				}else{
					if(isset($row['invisible']) && $row['invisible'] == 1){
						unset($rows[$key]);
					}
				}
			}
		}

		// Glue together tournaments, item kits, and fees into one list
		// Items appended in this order: 
		// 1.Items, 2.Tournaments, 3.Item Kits, 4.Fees
		$tournaments = array();
		if(!empty($tournamentIds)){
			$tournaments = $this->Tournament_model->get(array('tournament_id' => $tournamentIds));
			$rows = array_merge($rows, $tournaments);
		}
		
		if(!empty($itemKits)){
			$rows = array_merge($rows, $itemKits);
		}		

		// This is for green fees and cart fees
		if(!empty($fees)){
			$rows = array_merge($rows, $fees);
		}

		// Sort all search results alphabetically
		usort($rows, function($a, $b){
            $a = (array) $a;
            $b = (array) $b;
		    return strcasecmp($a['name'], $b['name']);
		});

		return array_values($rows);
	}
	
	public function get_loyalty_points(&$item){

		if($this->loyalty_rates === null){
			$this->loyalty_rates = array();
			$this->load->model('Customer_loyalty');
			$loyalty = $this->Customer_loyalty->get_rates();

			foreach($loyalty->result_array() as $loyalty_row){
				$newkey = $this->generate_loyaltyarray_lookup_id($loyalty_row);
				$this->loyalty_rates[$loyalty_row['type']][$newkey] = $loyalty_row;
			}
		}

		$cost = false;
		$reward = false;
		$lookup_id = $this->generate_loyaltyarray_lookup_id([
			"value"=>$item['item_id'],
			"price_category"=>$item['price_category'] ?? "",
			"price_class_id"=>$item['price_class_id'] ?? ""
		]);
		if(isset($this->loyalty_rates['item'][$lookup_id])){
			$reward = (float) $this->loyalty_rates['item'][$lookup_id]['points_per_dollar'];
			$cost = (float) $this->loyalty_rates['item'][$lookup_id]['dollars_per_point'];

		}else if(isset($item['timeframe_id']) && isset($this->loyalty_rates['item'][$lookup_id.$item['timeframe_id']])){
			$reward = (float) $this->loyalty_rates['item'][$lookup_id.$item['timeframe_id']]['points_per_dollar'];
			$cost = (float) $this->loyalty_rates['item'][$lookup_id.$item['timeframe_id']]['dollars_per_point'];

		}else if(isset($this->loyalty_rates['department'][$item['department']])){
			$reward = (float) $this->loyalty_rates['department'][$item['department']]['points_per_dollar'];
			$cost = (float) $this->loyalty_rates['department'][$item['department']]['dollars_per_point'];

		}else if(isset($this->loyalty_rates['category'][$item['category']])){
			$reward = (float) $this->loyalty_rates['category'][$item['category']]['points_per_dollar'];
			$cost = (float) $this->loyalty_rates['category'][$item['category']]['dollars_per_point'];

		}else if(isset($this->loyalty_rates['subcategory'][$item['subcategory']])){
			$reward = (float) $this->loyalty_rates['subcategory'][$item['subcategory']]['points_per_dollar'];
			$cost = (float) $this->loyalty_rates['subcategory'][$item['subcategory']]['dollars_per_point'];
		}

		$item['loyalty_points_per_dollar'] = $reward;
		$item['loyalty_dollars_per_point'] = round($cost / 100, 7);
		return true;
	}

	function update_quantity($item_id, $quantity){
		$quantity = (float) $quantity;
		$item_id = (int) $item_id;

		if(empty($item_id)){
			return false;
		}

		$this->db->query("UPDATE foreup_items
			SET quantity = quantity + {$quantity}
			WHERE item_id = {$item_id}
			LIMIT 1");

		return $this->db->affected_rows();
	}
	
	function get_categories($params= array()){
		
		$params = elements(array('food_and_beverage', 'q'), $params, null);
		
		$this->db->select("CONCAT(i.category) AS `key`, i.category AS name, 
			'category' AS type, IFNULL(b.`order`, 0) AS `order`", false)
			->from('items AS i')
			->join('table_buttons AS b', "b.name = i.category AND b.category = '' AND b.sub_category = '' AND b.course_id = i.course_id", 'left')
			->where('i.deleted', 0)
			->where('i.inactive', 0)
			->where('i.course_id', (int) $this->session->userdata('course_id'))
			->group_by('i.category');
		
		if($params['food_and_beverage'] !== null){
			$this->db->where('i.food_and_beverage', (int) $params['food_and_beverage']);
		}
		if(!empty($params['q'])){
			$this->db->like('i.category', $params['q']);
		}		
		$rows = $this->db->get()->result_array();
		
		foreach($rows as &$row){
			$row['order'] = (int) $row['order'];
		}
		return $rows;
	}

	function get_departments($params = array()){
		
		$params = elements(array('food_and_beverage', 'q'), $params, null);
		
		$this->db->select("i.department AS `key`, i.department AS name, 
			'department' AS type", false)
			->from('items AS i')
			->where('i.deleted', 0)
			->where('i.inactive', 0)
			->where('i.course_id', (int) $this->session->userdata('course_id'))
			->group_by('i.department');
		
		if($params['food_and_beverage'] !== null){
			$this->db->where('i.food_and_beverage', (int) $params['food_and_beverage']);
		}
		if(!empty($params['q'])){
			$this->db->like('i.department', $params['q']);
		}		

		$rows = $this->db->get()->result_array();
		return $rows;
	}
	
	function get_sub_categories($params = array()){
		
		$params = elements(array('food_and_beverage', 'q'), $params, null);
		
		$this->db->select("CONCAT(i.category,'/',i.subcategory) AS `key`, 
			i.subcategory AS name, 'sub_category' AS type, i.category AS category, 
			IFNULL(b.`order`, 0) AS `order`", false)
			->from('items AS i')
			->join('table_buttons AS b', "b.name = i.subcategory AND b.category = i.category AND b.course_id = i.course_id", 'left')
			->where('i.deleted', 0)
			->where('i.inactive', 0)
			->where('i.course_id', (int) $this->session->userdata('course_id'))
			->where("i.subcategory != ''")
			->group_by('i.category, i.subcategory');
		
		if($params['food_and_beverage'] !== null){
			$this->db->where('i.food_and_beverage', (int) $params['food_and_beverage']);
		}
		if(!empty($params['q'])){
			$this->db->like('i.subcategory', $params['q']);
		}	
		$rows = $this->db->get()->result_array();

		foreach($rows as &$row){
			$row['order'] = (int) $row['order'];
		}
		return $rows;
	}
	
	function get_special_item($item_type){
		
		$item = false;
		
		switch($item_type){
			
			case 'credit_card_fee':

            $item_data = $this->db->select("item_id")
                ->from('items')
                ->where("item_number", "CreditCardFee")
                ->where("invisible", 1)
                ->where("is_fee", 1)
                ->where("course_id", $this->session->userdata('course_id'))
                ->get()->row_array();

            if(empty($item_data['item_id'])){
                $item_id = $this->save(null, array(
                        'name' => 'Credit Card Fee',
                        'department' => 'Fees',
                        'category' => 'Fees',
                        'subcategory' => 'Credit Card Fees',
                        'item_number' => 'CreditCardFee',
                        'description' => '',
                        'cost_price' => 0,
                        'unit_price' => 0,
                        'max_discount' => 0,
                        'quantity' => 0,
                        'is_unlimited' => 1,
                        'reorder_level' => 0,
                        'is_serialized' => 1,
                        'invisible' => 1,
                        'is_fee' => 1,
                        'course_id' => $this->session->userdata('course_id'))
                );

            }else{
                $item_id = (int) $item_data['item_id'];
            }
            break;

            case 'statement_payment':

                $item_data = $this->db->select("item_id")
                    ->from('items')
                    ->where("item_number", "StatementPayment")
                    ->where("invisible", 1)
                    ->where("is_fee", 1)
                    ->where("course_id", $this->session->userdata('course_id'))
                    ->get()->row_array();

                if(empty($item_data['item_id'])){
                    $item_id = $this->save(null, array(
                            'name' => 'Statement Payment',
                            'department' => 'Account Payments',
                            'category' => 'Account Payments',
                            'subcategory' => 'Account Payments',
                            'item_number' => 'StatementPayment',
                            'item_type' => 'item',
                            'description' => '',
                            'cost_price' => 0,
                            'unit_price' => 0,
                            'max_discount' => 0,
                            'quantity' => 0,
                            'is_unlimited' => 1,
                            'reorder_level' => 0,
                            'is_serialized' => 1,
                            'invisible' => 1,
                            'is_fee' => 1,
                            'course_id' => $this->session->userdata('course_id')
                        )
                    );

                }else{
                    $item_id = (int) $item_data['item_id'];
                }
            break;

			case 'online_giftcard':
				
				$item_data = $this->db->select("item_id")
					->from('items')
					->where("item_number", "OnlineGiftcard")
					->where("invisible", 1)
					->where("course_id", $this->session->userdata('course_id'))
					->get()->row_array();
					
				if(empty($item_data['item_id'])){	
					$item_id = $this->save(null, array(
						'name' => 'Online Giftcard',
						'department' => 'Giftcards',
						'category' => 'Giftcards',
						'subcategory' => 'Giftcards',
						'item_number' => 'OnlineGiftcard',
						'description' => '',
						'cost_price' => 0,
						'unit_price' => 0,
						'max_discount' => 0,
						'quantity' => 0,
						'is_unlimited' => 1,
						'reorder_level' => 0,
						'is_serialized' => 1,
						'is_giftcard' => 1,
						'invisible' => 1,
						'is_fee' => 1,
						'course_id' => $this->session->userdata('course_id'))
					);
				
				}else{
					$item_id = (int) $item_data['item_id'];
				}
			break;

			case 'service_fee':
				
				$item_data = $this->db->select("item_id")
					->from('items')
					->where("item_number", "service_fee")
					->where("invisible", 1)
					->where("is_fee", 1)
					->where("course_id", $this->session->userdata('course_id'))
					->get()->row_array();
					
				if(empty($item_data['item_id'])){	
					$item_id = $this->save(null, array(
						'name' => 'Service Fee',
						'department' => 'Service Fees',
						'category' => 'Service Fees',
						'subcategory' => 'Service Fees',
						'item_number' => 'service_fee',
						'description' => '',
						'cost_price' => 0,
						'unit_price' => 0,
						'max_discount' => 0,
						'quantity' => 0,
						'is_unlimited' => 1,
						'reorder_level' => 0,
						'is_serialized' => 1,
						'invisible' => 1,
						'is_fee' => 1,
						'course_id' => $this->session->userdata('course_id'))
					);
				
				}else{
					$item_id = (int) $item_data['item_id'];
				}
			break;			
		}
		
		if(!empty($item_id)){
			$items = $this->get(array('item_id' => $item_id));

			$item = $items[0];
			if($item['is_giftcard']) {
				$row['item_type'] = 'giftcard';
				$row['type'] = 'giftcard';
			}
			return $item;
		}
		
		return false;
	}

	public function delete($item_id){
		$course_id = $this->session->userdata('course_id');
		$this->db->update('items', ['deleted' => 1], ['item_id' => (int) $item_id, 'course_id' => (int) $course_id]);
		$this->save_upcs($item_id,[]);
		return $this->db->affected_rows();
	}

	public function get_csv(){

		$this->load->helper('report');
		$data = $this->get(['limit' => 100000]);
		$rows = array();
		$row = array(
			"UPC/EAN/ISBN", 
			"Name", 
			"Department", 
			"Category", 
			"Sub-Category", 
			"Cost Price", 
			"Unit Price", 
			"Tax 1 Name",
			"Tax 1 Percent",
			"Tax 2 Name",
			"Tax 2 Percent",
			"Quantity",
			"Reorder Level",
			"Description",
			"Allow Alt Description",
			"Item has Serial Number"
		);
		$rows[] = $row;

		foreach ($data as $r) {

			$row = array(
				$r['item_number'],
				$r['name'],
				$r['department'],
				$r['category'],
				$r['subcategory'],
				$r['cost_price'],
				$r['unit_price'],
				'',
				'',
				'',
				'',
				$r['inventory_level'],
				$r['reorder_level'],
				$r['description'],
				$r['allow_alt_description'],
				''
			);

			if(!empty($r['taxes'][0]['name']) && !empty($r['taxes'][0]['percent'])){
				$row[7] = $r['taxes'][0]['name'];
				$row[8] = $r['taxes'][0]['percent'];
			}

			if(!empty($r['taxes'][1]['name']) && !empty($r['taxes'][1]['percent'])){
				$row[9] = $r['taxes'][1]['name'];
				$row[10] = $r['taxes'][1]['percent'];
			}

			if(!empty($r['item_number'])){
				$row[16] = 'Y';
			}

			$rows[] = $row;
		}

		$content = array_to_csv($rows);

		return $content;
	}

	/**
	 * @param $loyalty_row
	 * @return array
	 */
	private function generate_loyaltyarray_lookup_id($loyalty_row)
	{
		$newkey = $loyalty_row['value'];
		if (!empty($loyalty_row['price_category'])) {
			$newkey .= $loyalty_row['price_category'];
		} else if (!empty($loyalty_row['price_class_id'])) {
			$newkey .= $loyalty_row['price_class_id'];
		}
		if(isset($loyalty_row['limit_timeframe']) && $loyalty_row['limit_timeframe'] == 1){
			$newkey .= $loyalty_row['timeframe_id'];
		}
		return $newkey;
	}
}
