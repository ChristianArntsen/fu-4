<?php
class Punch_card_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function save($punch_card_id, $data){
		
		$punch_card = elements(array('punch_card_number', 'customer', 
			'expiration_date', 'details', 'items'), $data, null);
		$course_id = (int) $this->session->userdata('course_id');
		$punch_card['course_id'] = $course_id;
		
		$items = $punch_card['items'];
		unset($punch_card['items']);
		
		// Parse expiration date
		if(empty($punch_card['expiration_date'])){
			$punch_card['expiration_date'] = '0000-00-00';
		}else{
			$punch_card['expiration_date'] = date('Y-m-d', strtotime($punch_card['expiration_date']));
		}	
		
		if(!empty($punch_card['customer']['person_id'])){
			$punch_card['customer_id'] = (int) $punch_card['customer']['person_id'];
		}else{
			$punch_card['customer_id'] = null;
		}
		unset($punch_card['customer']);
		
		// Save a new punch card
		if(empty($punch_card_id)){
		
			if(!$punch_card['punch_card_number']){
				return false;
			}
			
			if(empty($items)){
				return false;
			}

            if(empty($punch_card['issued_date'])){
                $punch_card['issued_date'] = date('Y-m-d');
            }else{
                $punch_card['issued_date'] = date('Y-m-d', strtotime($punch_card['issued_date']));
            }
            $this->db->insert('punch_cards', $punch_card);
			$punch_card_id = (int) $this->db->insert_id();
		
		// Update an existing punch card
		}else{
			
			foreach($punch_card as $key => $value){
				if($value === null){
					unset($punch_card[$key]);
				}
			}
			
			if(!empty($punch_card)){
				$this->db->update('punch_cards', $punch_card, array('punch_card_id' => $punch_card_id));
			}
		}
		
		// Loop through attached items and save those as well
		if(!empty($items)){
			$this->save_items($punch_card_id, $items);
		}
		
		return $punch_card_id;
	}
	
	function use_punches($punch_card_id, $items){
		
		$rows_updated = 0;
		foreach($items as $item_id => $data){
			
			$used = (int) $data['used'];
			$item_id = (int) $item_id;
			$price_class_id = (int) $data['price_class_id'];
			
			if(empty($used) || empty($item_id)){
				continue;
			}
			
			$price_class_sql = '';
			if($price_class_id != 0){
				$price_class_sql = 'AND price_class_id = '.$price_class_id;
			}
			
			$this->db->query("UPDATE foreup_punch_card_items
				SET used = (used + {$used})
				WHERE punch_card_id = {$punch_card_id}
					AND item_id = {$item_id}
					{$price_class_sql}");

			$rows_updated++;
		}
		
		return $rows_updated;
	}
	
	function save_items($punch_card_id, $items){
		
		$item_rows = array();
		foreach($items as $item){
			$item_rows[] = array(
				'punch_card_id' => $punch_card_id,
				'item_id' => (int) $item['item_id'],
				'price_class_id' => (int) $item['price_class_id'],
				'punches' => (int) $item['quantity'],
				'used' => 0
			);
		}

		if(!empty($item_rows)){
			$this->db->insert_batch('punch_card_items', $item_rows);
		}
		
		return true;		
	}
	
	public function get($params){
		
		$course_id = (int) $this->session->userdata('course_id');
		
		// Get punch card details, including associated items (punches)
		$this->db->select("p.*, 
			person.first_name AS customer_first_name, 
			person.last_name AS customer_last_name,
				
			GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				punch_item.item_id,
				IFNULL(punch_item.price_class_id, 0),
				IFNULL(price_class.name, ''),
				item.name,
				punch_item.punches,
				punch_item.used
			) SEPARATOR 0x1E) AS items", false)
			->from('punch_cards AS p')
			->join('punch_card_items AS punch_item', 'punch_item.punch_card_id = p.punch_card_id', 'left')
			->join('items AS item', 'item.item_id = punch_item.item_id', 'left')
			->join('price_classes AS price_class', 'price_class.class_id = punch_item.price_class_id', 'left')
			->join('people AS person', 'person.person_id = p.customer_id', 'left')
			->where('p.course_id', $course_id)
			->where('p.deleted', 0);	
			
		if(isset($params['punch_card_number'])){
			$this->db->where('p.punch_card_number', $params['punch_card_number']);
		}
		
		if(isset($params['punch_card_id'])){
			$this->db->where('p.punch_card_id', $params['punch_card_id']);
		}		
		
		$punch_cards = $this->db->get()->result_array();
		$itemIds = array();
		
		if(count($punch_cards) <= 1 && empty($punch_cards[0]['punch_card_id'])){
			return array();
		}
		
		foreach($punch_cards as $key => &$row){
			$row['items'] = $this->get_items_array($row['items']);
		}
			
		return $punch_cards;
	}
	
	private function get_items_array($items){
		$items = explode("\x1E", $items);
		
		// Loop through each item row, break apart fields
		foreach($items as &$item_row){
			$item = explode("\x1F", $item_row);
			
			$item = array(
				'item_id' => (int) $item[0],
				'price_class_id' => (int) $item[1],
				'price_class_name' => (int) $item[2],
				'name' => $item[3],
				'punches' => (int) $item[4],
				'used' => (int) $item[5]
			);
			$item_row = $item;
		}
		
		return $items;		
	}
	
	public function delete($params){
		
		if(empty($params['punch_card_id']) && empty($params['punch_card_number'])){
			return false;
		}
		
		$punch_card = $this->get($params);
		$punch_card = $punch_card[0];
		
		if(empty($punch_card)){
			return false;
		}
		
		$this->db->where('punch_card_id', $punch_card['punch_card_id']);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->update('punch_cards', array('deleted' => 1, 'punch_card_number' => null));
		
		return $this->db->affected_rows();
	}
}
