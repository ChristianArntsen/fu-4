<?php
/*
 * Handles any comps that are associated to receipt/sales items
 */
class Item_comp extends CI_Model 
{
	/*
	 * Deletes a comp from an item
	 */
	function delete($sale_id, $receipt_id, $line)
	{
		if (empty($sale_id) || empty($receipt_id) || empty($line)){
			return false;
		}
		
		$this->db->delete('table_comps', array('sale_id' => $sale_id, 'receipt_id' => $receipt_id, 'line' => $line));
		$deleted = $this->db->affected_rows();
		
		return $deleted;
	}
	
	/*
	 * Saves a new comp to a receipt item, if comp already exists, it 
	 * overwrites it
	 */
	function save($sale_id, $line, $item_id, $type = '', $description = '', $amount = 0, $employee_id = false){
		
		if(empty($employee_id)){
			$employee_id = $this->session->userdata('employee_id');
		}

		$course_id = (int) $this->session->userdata('course_id');
		$sale_id = (int) $sale_id;
		$line = (int) $line;
		$item_id = (int) $item_id;
		$type = $this->db->escape($type);
		$description = $this->db->escape($description);
		$amount = (float) round($amount, 2);
		$employee_id = (int) $employee_id;
		
		$this->db->query("INSERT INTO foreup_table_comps
			(sale_id, line, item_id, type, description, amount, course_id, employee_id)
			VALUES ({$sale_id}, {$line}, {$item_id}, {$type}, {$description}, {$amount}, {$course_id}, {$employee_id})
			ON DUPLICATE KEY UPDATE
				description = {$description},
				type = {$type},
				amount = {$amount},
				employee_id = {$employee_id}");
				
		return $this->db->affected_rows();
	}
	
	/*
	 * Saves all comps from F&B table into completed sale table
	 */
	function save_to_sale($sale_id, $rows = array(), $course_id = false){
		
		if(empty($rows)){
			return false;
		}
		
		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}
		$sale_id = (int) $sale_id;
		
		foreach($rows as &$row){
			$row['course_id'] = $course_id;
			$row['sale_id'] = $sale_id;
		}
		
		$this->db->insert_batch('sales_comps', $rows);
		return $this->db->affected_rows();
	}
}
?>
