<?php
class Item_taxes extends CI_Model
{
	/*
	Gets tax info for a particular item
	*/
	function get_info($item_id)
	{
		$manual_taxes = $this->session->userdata('manual_taxes');

		if ($manual_taxes)
		{
			return $this->sale_lib->get_transaction_item_taxes($item_id);
		}
		else {
			$this->db->from('items_taxes');
			$this->db->where('item_id',$item_id);
			$this->db->order_by('cumulative');
			//return an array of taxes for an item
			return $this->db->get()->result_array();
		}
	}

	/*
	Inserts or updates an item's taxes
	*/
	function save(&$items_taxes_data, $item_id)
	{
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		$this->delete($item_id);

		foreach ($items_taxes_data as $row)
		{
			$row['item_id'] = $item_id;
			$this->db->insert('items_taxes',$row);
		}

		$this->db->trans_complete();
		return true;
	}

	function save_multiple(&$items_taxes_data, $item_ids)
	{
		foreach($item_ids as $item_id)
		{
			$this->save($items_taxes_data, $item_id);
		}
	}

	/*
	Deletes taxes given an item
	*/
	function delete($item_id)
	{
		return $this->db->delete('items_taxes', array('item_id' => $item_id));
	}
	
	// Get list of all tax percentages used 
	function get_all_taxes(){
		
		$course_id = (int) $this->session->userdata('course_id');
		$query = $this->db->query("
			SELECT * FROM (
				SELECT default_tax_1_rate AS percent, CONCAT(ROUND(default_tax_1_rate, 3), ' %') AS `key`
				FROM foreup_courses
				WHERE course_id = {$course_id}
				UNION ALL
				SELECT default_tax_2_rate AS percent, CONCAT(ROUND(default_tax_2_rate, 3), ' %') AS `key`
				FROM foreup_courses
				WHERE course_id = {$course_id}
				UNION ALL
				SELECT item_tax.percent, CONCAT(ROUND(item_tax.percent, 3), ' %') AS `key`
				FROM foreup_sales_items_taxes AS item_tax
				INNER JOIN foreup_items AS item
					ON item.item_id = item_tax.item_id
				WHERE item.course_id = {$course_id}	
				UNION ALL
				SELECT item_kit_tax.percent, CONCAT(ROUND(item_kit_tax.percent, 3), ' %') AS `key`
				FROM foreup_sales_item_kits_taxes AS item_kit_tax
				INNER JOIN foreup_item_kits AS item_kit
					ON item_kit.item_kit_id = item_kit_tax.item_kit_id			
				WHERE item_kit.course_id = {$course_id}
			) AS all_taxes
			GROUP BY ROUND(percent, 3)
			ORDER BY ROUND(percent, 3) ASC");			
		
		return $query->result_array();
	}

	function get_info_multiple($ids)
	{
		if(empty($ids))
			return [];
		$this->db->from('items_taxes');
		$this->db->where_in('item_id',$ids);
		$this->db->order_by('cumulative');
		//return an array of taxes for an item
		$return = [];
		$result = $this->db->get()->result_array();
		foreach($result  as $row){
			if(!isset($return[$row['item_id']]))
				$return[$row['item_id']] = [];
			$return[$row['item_id']][]=$row;
		}
		return $return;
	}
}
?>
