<?php
class Customer_credit_card extends CI_Model
{
	function get_info($credit_card_id)
	{
		$this->db->from('customer_credit_cards');
		$this->db->where('credit_card_id', $credit_card_id);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('billing_credit_cards');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
    function get($customer_id, $credit_card_id = 0, $course_id = false)
    {
        $customer_id = (int) !$customer_id || $customer_id == '' ? -1 : $customer_id;
        $credit_card_id = (int) $credit_card_id == '' ? 0 : $credit_card_id;

        $this->db->from('customer_credit_cards AS card');

        if(!empty($course_id)){
            $this->db->join('customers AS customer', 'customer.person_id = card.customer_id', 'inner');
            $this->db->where('customer.course_id', (int) $course_id);
        }

        $this->db->where("(card.customer_id = {$customer_id} OR card.credit_card_id = {$credit_card_id})");
        $this->db->where('card.deleted != ', 1);
        $this->db->where('card.token !=', '');
        $this->db->group_by('card.credit_card_id');
        $result = $this->db->get();

        return $result->result_array();
    }
    function get_multiple($customer_ids)
    {
        if (empty($customer_ids) || !is_array($customer_ids)) {
            return array();
        }

        $this->db->from('customer_credit_cards AS card');
        $this->db->where_in('card.customer_id', $customer_ids);
        $this->db->where('card.deleted != ', 1);
        $this->db->where('card.token !=', '');
        $this->db->group_by('card.credit_card_id');
        $result = $this->db->get();

        return $result->result_array();
    }
    function get_simple_type($credit_card)
    {
        switch($credit_card['card_type']){
            case 'MasterCard':
            case 'M/C':
                $card_type = 'mastercard';
                break;
            case 'Visa':
            case 'VISA':
                $card_type = 'visa';
                break;
            case 'Discover':
            case 'DCVR':
                $card_type = 'discover';
                break;
            case 'American Express':
            case 'AMEX':
                $card_type = 'americanexpress';
                break;
            case 'Diners':
            case 'DINERS':
                $card_type = 'diners';
                break;
            case 'JCB':
                $card_type = 'jcb';
                break;
            case 'Bank Acct':
                $card_type = 'bankaccount';
                break;
            default:
                $card_type = $credit_card['card_type'];
                break;
        }

        return $card_type;
    }
	function charge(&$billing = false, $total)
	{
		$this->load->model('Charge_attempt');
		if (!$billing || !$billing['credit_card_id'] || $billing['credit_card_id'] == ''){
			return false;
		}
		$this->load->model('sale');

		$cc_info = $this->get_info($billing['credit_card_id']);
		$course_info = $this->Course->get_info($billing['course_id']);

 		$ets_key = $cc_info->ets_key != '' ? $cc_info->ets_key : $course_info->ets_key;
		$mercury_id = $cc_info->mercury_id != '' ? $cc_info->mercury_id : $course_info->mercury_id;
		$mercury_password = $cc_info->mercury_password != '' ? $cc_info->mercury_password : $course_info->mercury_password;
		$element_account_id = !empty($cc_info->element_account_id) ? $cc_info->element_account_id : $course_info->element_account_id;
		$element_account_token = !empty($cc_info->element_account_token) ? $cc_info->element_account_token : $course_info->element_account_token;
		$element_application_id = !empty($cc_info->element_application_id) ? $cc_info->element_application_id : $course_info->element_application_id;
		$element_acceptor_id = !empty($cc_info->element_acceptor_id) ? $cc_info->element_acceptor_id : $course_info->element_acceptor_id;
		
		if($ets_key != ''){
            $this->load->library("Hosted_payments_v3");
            $payment = new Hosted_payments_v3();
            $invoice = $this->Sale->add_credit_card_payment(
            	array('ets_id'=>$ets_key,'tran_type'=>'Sale','frequency'=>'Recurring')
            );
            $corr_id = $payment->to_corr_id($invoice);
            $session_token = $payment->sign_in($ets_key, $corr_id);
            $p_resp = $payment->charge($session_token, $corr_id, $cc_info->token, $total, $corr_id);
            $response = $payment->verify($session_token, $corr_id);
            $session = $response->properties;
            if (!isset($session->amount)) {
                // email jhopkins
                $html = "
                    Amount: {$total}<br/>
                   ETS Key: {$ets_key}<br/>
                   Invoice: {$invoice}<br/>
                     Token: {$cc_info->token}<br/>
                     CC ID: {$billing['credit_card_id']}<br/>
                ";
                send_sendgrid(
                    array('jhopkins@foreup.com'),
                    "ETS charge payment failed ",
                    $html,
                    'no-reply@foreup.com',
                    'no-reply@foreup.com'
                );
                // and return
                return false;
            }

			$transaction_time = date('Y-m-d H:i:s', strtotime($session->created));

			$payment_data = array(
				'auth_amount'		=> (string) $session->amount,
				'card_type'			=> (string)	$cc_info->card_type,
				'masked_account'	=> (string)	$cc_info->masked_account,
				'status_message'	=> (string)	$session->message,
				'display_message' 	=> (string)	$session->message,
				'amount'			=> (string) $session->amount,
				'status'			=> (string) $session->status,
				'payment_id'		=> (string) $session->correlationId,
				'trans_post_time'	=> (string) $transaction_time
			);
			$credit_card_id = $billing['credit_card_id'];
			$this->Sale->update_credit_card_payment($invoice, $payment_data);
			$billing['credit_card_payment_id'] = $invoice;
			$billing['payment_type'] = $payment_data['card_type'].' '.$payment_data['masked_account'];

			$this->Charge_attempt->save($billing['credit_card_id'], $billing['invoice_id'],
				$total, date('Y-m-d H:i:s'),
				$session->status,
				$session->message.' '.$session->message,
				$invoice);
			
			if((string) $session->status == 'Completed'){
				$credit_card_id = $billing['credit_card_id'];
				$credit_card_data = array();
				$credit_card_data['status'] = 'Good';
				$this->save($credit_card_data, $credit_card_id, 1);
				if (isset($billing['billing_id']))
					$this->record_charges($credit_card_id, $credit_card_charges, array('total'=>$total), $billing['billing_id']);
				
				return true;
			}
			else {
				// OPTIONS FOR STATI
				// unsupported
				// EXPIRED CARD
				$credit_card_data = array();
				if ($payment_data['status_message'] == 'EXPIRED CARD')
					$credit_card_data['status'] = "Expired";
				if (strpos($payment_data['status_message'], 'unsupported') !== false)
					$credit_card_data['status'] = 'Not Accepted';
				$this->save($credit_card_data, $credit_card_id, 0, 1);
			}

		}else if ($mercury_id != ''){
			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			$invoice = $this->Sale->add_credit_card_payment(
				array('mercury_id'=>$mercury_id,'tran_type'=>'CreditSaleToken','frequency'=>'Recurring')
			);
			$employee = 'Auto Billing';

            $HC->set_frequency('Recurring');
            $HC->set_token($cc_info->token);
			$HC->set_cardholder_name($cc_info->cardholder_name);
			$HC->set_invoice($invoice);
			$HC->set_merchant_credentials($mercury_id, $mercury_password);

            $transaction_results = $HC->token_transaction('Sale', $total, '0.00', '0.00');

            if (isset($transaction_results->PurchaseAmount)) {
                // good transaction proceed
            }
            else {
                // bad transaction trying again - 2nd try
                $transaction_results = $HC->token_transaction('Sale', $total, '0.00', '0.00');
                if (isset($transaction_results->PurchaseAmount)) {
                    // good transaction proceed
                }
                else {
                    // bad transaction trying again - 3rd and final try
                    $transaction_results = $HC->token_transaction('Sale', $total, '0.00', '0.00');
                }
            }

            if (!isset($transaction_results->PurchaseAmount)) {
                // email jhopkins
                $html = "
                    Amount: {$total}<br/>
                    Mercury ID: {$mercury_id}<br/>
                   Invoice: {$invoice}<br/>
                     Token: {$cc_info->token}<br/>
                     CC ID: {$billing['credit_card_id']}<br/>
                ";
                send_sendgrid(
                    array('jhopkins@foreup.com'),
                    "MERCURY charge payment failed ",
                    $html,
                    'no-reply@foreup.com',
                    'no-reply@foreup.com'
                );
                // and return
                return false;
            }

			$transaction_time = date('Y-m-d H:i:s');

			$payment_data = array(
				'acq_ref_data'=>(string)$transaction_results->AcqRefData,
				'auth_code'=>(string)$transaction_results->AuthCode,
				'auth_amount'=>(string)$transaction_results->AuthorizeAmount,
				'avs_result'=>(string)$transaction_results->AVSResult,
				'batch_no'=>(string)$transaction_results->BatchNo,
				'card_type'=>(string)$transaction_results->CardType,
				'cvv_result'=>(string)$transaction_results->CVVResult,
				'gratuity_amount'=>(string)$transaction_results->GratuityAmount,
				'masked_account'=>(string)$transaction_results->Account,
				'status_message'=>(string)$transaction_results->Message,
				'amount'=>(string)$transaction_results->PurchaseAmount,
				'ref_no'=>(string)$transaction_results->RefNo,
				'status'=>(string)$transaction_results->Status,
				'token'=>(string)$transaction_results->Token,
				'process_data'=>(string)$transaction_results->ProcessData,
				'trans_post_time'	=> (string) $transaction_time
			);
			$this->Sale->update_credit_card_payment($invoice, $payment_data);

			$credit_card_id = $billing['credit_card_id'];
			$credit_card_data = array(
				'token'=>$payment_data['token'],
				'token_expiration'=>date('Y-m-d', strtotime('+2 years'))
			);
			$billing['credit_card_payment_id'] = $invoice;
			$billing['payment_type'] = $cc_info->card_type.' '.str_replace('xx', '', $cc_info->masked_account);
			
			$this->Charge_attempt->save($billing['credit_card_id'], $billing['invoice_id'], $total, date('Y-m-d H:i:s'), $payment_data['status'], $payment_data['status_message'], $invoice);			
			
			if ($payment_data['status'] == 'Approved')
			{
				$credit_card_data['status'] = 'Good';
				$this->save($credit_card_data, $credit_card_id, 1);
				if (isset($billing['billing_id']))// the record charges method returns without doing anything
					$this->record_charges($credit_card_id, $credit_card_charges, array('total'=>$total), $billing['billing_id']);
				return true;
			} else {
				// TODO: UPDATE STATUS TO EXPIRED IF EXPIRED IS RETURNED (Or other status depending on reason for failure)
				// NOT ACCEPTED
				// INVLD EXP DATE
				$credit_card_data = array('status' => 'Declined');
				if ($payment_data['status_message'] == 'INVLD EXP DATE')
					$credit_card_data['status'] = "Expired";
				if (strpos($payment_data['status_message'], 'NOT ACCEPTED') !== false)
					$credit_card_data['status'] = 'Not Accepted';
				$this->save($credit_card_data, $credit_card_id, 0, 1);
			}
		
		// If using element merchant
		}else if(!empty($element_account_id)){
			
			$this->load->library('v2/Element_merchant');
			$credentials = array(
				'account_id' => $element_account_id,
				'account_token' => $element_account_token,
				'application_id' => Element_merchant::APPLICATION_ID,
				'acceptor_id' => $element_acceptor_id,
				'terminal_id' => $element_application_id
			);
			
			// Initialize payment library
			$this->element_merchant->init(
				new GuzzleHttp\Client()
			);
			
			// Set up terminal settings
			$this->element_merchant->set_terminal(array(
				'CardholderPresentCode' => 		Element_merchant::CARDHOLDER_PRESENT_CODE_PRESENT,
				'CardInputCode' => 				Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED,
				'CardPresentCode' => 			Element_merchant::CARD_PRESENT_CODE_PRESENT,
				'MotoECICode' => 				Element_merchant::MOTO_ECI_CODE_SINGLE,
				'TerminalCapabilityCode' => 	Element_merchant::TERMINAL_CAPABILITY_CODE_MAGSTRIPE_READER,
				'TerminalEnvironmentCode' => 	Element_merchant::TERMINAL_ENVIRONMENT_CODE_LOCAL_ATTENDED,
				'TerminalType' => 				Element_merchant::TERMINAL_TYPE_POINT_OF_SALE
			));				
			
			// Charge card on file
			$this->element_merchant->credit_card_sale(array(
				'amount' => abs($total),
				'payment_account_id' => $cc_info->token,
				'reference_number' => $cc_info->invoice,
				'ticket_number' => $cc_info->invoice
			));		
			
			// If charge failed for some reason
			if(!$this->element_merchant->success()){
				
				$credit_card_data['status'] = $this->element_merchant->message();
				
				// Update card to show that it had errors
				$this->save($credit_card_data, $cc_info->credit_card_id, 0, 1);
				return false;
			}
			
			$transaction_id = (string) $this->element_merchant
				->response()->xml()
				->Response->Transaction->TransactionID;
			
			$cc_invoice_data = array(
				'tran_type' => 'Sale',
				'element_account_id' => $element_account_id,
				'amount' => $total,
				'auth_amount' => $total,
				'payment_id' => $transaction_id,
				'status' => 'Approved',
				'masked_account' => $cc_info->masked_account,
				'card_type' => $cc_info->card_type,
				'token' => $cc_info->token
			);	
			
			$invoice_id = $this->Sale->add_credit_card_payment($cc_invoice_data);
			
			$billing['invoice_id'] = $invoice_id;
			$billing['credit_card_payment_id'] = $invoice_id;
			$billing['payment_type'] = $cc_info->card_type.' '.$cc_info->masked_account;
			
			// Increment successful charge on card
			$this->save($credit_card_data, $cc_info->credit_card_id, 1);
			
			if(isset($billing['billing_id'])){
				$this->record_charges($credit_card_id, $credit_card_charges, array('total'=>$total), $billing['billing_id']);
			}			
			return true;	
		}

		return false;
	}
	function save(&$credit_card_data, $credit_card_id = -1, $charge_successful = 0, $charge_failed = 0)
	{
		if ($credit_card_id == -1)
		{
			$ets_key = $mercury_id = $mercury_password = '';
			if ($this->config->item('ets_key'))
				$credit_card_data['ets_key'] = $this->config->item('ets_key');
			else if ($this->config->item('mercury_id'))
			{
				$credit_card_data['mercury_id'] = $this->config->item('mercury_id');
				$credit_card_data['mercury_password'] = $this->config->item('mercury_password');
			}
			else if($this->config->item('element_account_id'))
			{
				$credit_card_data['element_account_id'] = $this->config->item('element_account_id');
				$credit_card_data['element_account_token'] = $this->config->item('element_account_token');
				$credit_card_data['element_application_id'] = $this->config->item('element_application_id');
				$credit_card_data['element_acceptor_id'] = $this->config->item('element_acceptor_id');		
			}
			$this->db->insert('customer_credit_cards', $credit_card_data);
			$credit_card_data['credit_card_id'] = $this->db->insert_id();
			$credit_card_id = $credit_card_data['credit_card_id'];
		}
		else {
			$update_string = '';
			$update_array = array();
			foreach($credit_card_data as $index => $cc) {
				$update_array[] = " $index = '$cc' ";
			}
			
			if ($charge_successful)
				$update_array[] = "successful_charges = successful_charges + 1";
			if ($charge_failed)
				$update_array[] = "failed_charges = failed_charges + 1";
			
			if (count($update_array) > 0)
			{
				$update_string = implode(',', $update_array);
	
				$this->db->query("UPDATE foreup_customer_credit_cards SET $update_string WHERE credit_card_id = $credit_card_id LIMIT 1");
			}
			//$this->db->where('credit_card_id', $credit_card_id);
			//$this->db->update('customer_credit_cards', $credit_card_data);
		}
		return $credit_card_id;
	}
	function record_charges($credit_card_id, $items = '', $totals = '', $billing_id = '')
	{
		return;
		//echo "<br/><br/>Recording Charges<br/><br/>";
		$items = ($items != '')?$items:$this->get_charge_items();
		$totals = ($totals != '')?$totals:$this->get_charge_totals();
		//print_r($items);
		//echo "<br/>Totals<br/>";
		//print_r($totals);

		$charge_data = array(
			'credit_card_id'=>$credit_card_id,
			'billing_id'=>$billing_id,
			'person_id'=>$this->session->userdata('person_id'),
			'total'=>$totals['total']
		);
		if ($this->db->insert('billing_charges', $charge_data))
			$charge_data['charge_id'] = $this->db->insert_id();


		$charge_items_data = array();
		foreach ($items as $item)
		{
			$charge_items_data[] = array(
				'charge_id'=>$charge_data['charge_id'],
				'line_number'=>$item['line_number'],
				'description'=>$item['description'],
				'amount'=>$item['amount']
			);
		}
		$this->db->insert_batch('customer_billing_items', $charge_items_data);
		//echo $this->db->last_query();
		$this->clear_charge_items();

		return true;
	}
	function delete($credit_card_id)
	{
		$this->db->where('credit_card_id', $credit_card_id);
		return $this->db->update('billing_credit_cards', array('deleted'=>1));
	}
	function delete_card($customer_id, $credit_card_id)
	{
		$this->db->where('credit_card_id', $credit_card_id);
		$this->db->where('customer_id', $customer_id);
		return $this->db->update('customer_credit_cards', array('deleted'=>1));
	}
	function remove_from_billings($credit_card_id)
	{
		$this->db->where('credit_card_id', $credit_card_id);
		return $this->db->update('customer_billing', array('credit_card_id'=>0));
	}
	function add_charge_item($description, $amount) {
		$items = ($this->session->userdata('course_charge_items'))?$this->session->userdata('course_charge_items'):array();
		$maxkey = 0;
		foreach ($items as $index =>$item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.
			if($maxkey <= $item['line_number'])
				$maxkey = $item['line_number'];
		}

		$insertkey=$maxkey+1;
		$items[$insertkey] = array('line_number'=>$insertkey,'description'=>$description, 'amount'=>$amount);
		$this->session->set_userdata('course_charge_items', $items);

		return array('line_number'=>$insertkey, 'totals'=>$this->get_charge_totals(), 'items'=>$this->session->userdata('course_charge_items'));
	}
	function delete_charge_item($line_number) {
		$items = $this->session->userdata('course_charge_items');
		unset($items[$line_number]);
		$this->session->set_userdata('course_charge_items', $items);
		return array('totals'=>$this->get_charge_totals());
	}
	function update_tax_rate($tax_rate){
		$this->session->set_userdata('course_charge_tax_rate', $tax_rate);
		return array('totals'=>$this->get_charge_totals());
	}
	function clear_charge_items() {
		$this->session->unset_userdata('course_charge_items');
	}
	function get_charge_items()
	{
		$items = $this->session->userdata('course_charge_items');
		return $items;
	}
	function get_charge_totals()
	{
		$subtotal = 0;
		$items = $this->session->userdata('course_charge_items');
		$tax_rate = $this->session->userdata('course_charge_tax_rate');
		foreach ($items as $item)
		{
			$subtotal += $item['amount'];
		}
		return array('subtotal'=>number_format($subtotal, 2), 'taxes'=>number_format($subtotal*$tax_rate/100,2), 'total'=>number_format($subtotal*(1+$tax_rate/100),2));
	}
	function dropdown($person_id, $credit_card_id)
	{
		$customer_credit_cards = array('Select Card');
		$credit_cards = $this->get($person_id, $credit_card_id);
		if (count($credit_cards) > 0)
		{
			foreach($credit_cards as $customer_credit_card)
			{
				$credit_card_id = $credit_card_id != 0 ? $credit_card_id : $customer_credit_card['credit_card_id'];
				$customer_credit_cards[$customer_credit_card['credit_card_id']] = $customer_credit_card['card_type'].' '.$customer_credit_card['masked_account'];
			}
			return form_dropdown('credit_card_id', $customer_credit_cards, $credit_card_id);
		}
		return '<input type="hidden" id="credit_card_id" value="0" />';

	}

	// Receives data returned from ETS or Mercury when credit card is captured
	// and adds the credit card to the database
	function capture_card($person_id, &$credit_card_data){

		$approved = false;
		$this->load->model('sale');

		if($this->config->item('ets_key')){

			$response = $this->input->post('response');
			$ets_response = json_decode($response);
			$transaction_time = date('Y-m-d H:i:s', strtotime($ets_response->created));

			// VERIFYING A POSTed TRANSACTION
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));
			$session_id = $payment->get("session_id");

			$account_id = $ets_response->customers->id;
			$payment->set("action", "verify")
				->set("sessionID", $ets_response->id)
				->set('accountID', $account_id);

			$verify = $payment->send();

			// Convert card type to match mercury card types
			if ((string)$ets_response->customers->cardType != 'UNKNOWN') {
				$ets_card_type = $ets_response->customers->cardType;
				$card_type = '';
				switch($ets_card_type){
					case 'MasterCard':
						$card_type = 'M/C';
					break;
					case 'Visa':
						$card_type = 'VISA';
					break;
					case 'Discover':
						$card_type = 'DCVR';
					break;
					case 'American Express':
						$card_type = 'AMEX';
					break;
					case 'Diners':
						$card_type = 'DINERS';
					break;
					case 'JCB':
						$card_type = 'JCB';
					break;
					default:
						$card_type = $ets_card_type;
					break;
				}
				$masked_account = str_replace('*', '', (string) $verify->customers->cardNumber);
				$expiration = DateTime::createFromFormat('my', $ets_response->customers->cardExpiration);
			}
			else {
				$card_type = 'Bank Acct';
				$masked_account = str_replace('*', '', (string) $verify->customers->accountNumber);
				$expiration = DateTime::createFromFormat('my', date('my', strtotime('+2 years')));
			}

			if((string)$ets_response->status == 'success'){
				$credit_card_data = array(
					'course_id' 		=> $this->session->userdata('course_id'),
					'card_type' 		=> $card_type,
					'masked_account' 	=> $masked_account,
					'cardholder_name' 	=> '',
					'customer_id'		=> $person_id,
					'token' 			=> (string) $ets_response->customers->id,
					'expiration' 		=> $expiration->format('Y-m-01')
				);

				$credit_card_id = $this->save($credit_card_data);
				$approved = true;
			}

		}else if($this->config->item('mercury_id')){

            $this->load->model('Blackline_devices');
            $blackline_info = $this->Blackline_devices->get_terminal_info($this->session->userdata('terminal_id'));
            if ($this->config->item('use_mercury_emv') && $blackline_info) {

                $response = $this->input->post();
                $rd = $response;
                if ((string)$rd['ResultCode'] != 'Approved') {
                    //$this->response(array('success' => false, 'msg' => 'Credit card declined. ' . $rd['Message']), 400);
                    $approved = false;
                }
                else {
                    $amount = (float)$rd['Amount'];
					if ($rd['CardType'] == 'MasterCard') {
						$rd['CardType'] = 'M/C';
					} else if ($rd['CardType'] == 'Visa') {
						$rd['CardType'] = 'VISA';
					} else if ($rd['CardType'] == 'Amex') {
						$rd['CardType'] = 'AMEX';
					} else if ($rd['CardType'] == 'Discover') {
						$rd['CardType'] = 'DCVR';
					} else if ($rd['CardType'] == 'DinersClub') {
						$rd['CardType'] = 'DINERS';
					}
                    //Add card to billing_credit_cards
                    $credit_card_data = array(
                        'course_id' => $this->config->item('course_id'),
                        'customer_id' => $person_id == NULL ? '' : $person_id,
                        'token' => $rd['CardToken'],
                        'token_expiration' => date('Y-m-d', strtotime('+2 years')),
                        'card_type' => $rd['CardType'],
                        'masked_account' => $rd['Last4'],
                        'cardholder_name' => $rd['Name']
                    );
                    $credit_card_id = $this->save($credit_card_data);

                    $invoice_id = $response['RefID'];
                    $payment_data = array(
                        'course_id' => $this->config->item('course_id'),
                        'mercury_id' => $this->config->item('mercury_id'),
                        'tran_type' => $rd['TransType'],
                        'amount' => $amount,
                        'auth_amount' => $rd['Amount'],
                        'card_type' => $rd['CardType'],
                        'frequency' => 'OneTime',
                        'masked_account' => $rd['Last4'],
                        'cardholder_name' => $rd['Name'],
                        'terminal_name' => $rd['TerminalID'],
                        'trans_post_time' => gmdate('Y-m-d H:i:s'),
                        'auth_code' => $rd['AuthCode'],
                        'acq_ref_data' => $rd['ProcessorExtraData1'],
                        'process_data' => $rd['ProcessorExtraData2'],
						'ref_no' => $rd['ProcessorExtraData3'],
						'token' => $rd['CardToken'],
                        'response_code' => $rd['ResultCode'],
                        'status' => $rd['ResultCode'],
                        'status_message' => $rd['Message'],
                        'display_message' => $rd['Message']
                    );

                    $this->sale->update_credit_card_payment($invoice_id, $payment_data);

                    $approved = true;
                }
            }
            else {
                $this->load->library('Hosted_checkout_2');
                $HC = new Hosted_checkout_2();
                $HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));

                $payment_id = $this->session->userdata('payment_id');
                $this->session->unset_userdata('payment_id');
                $HC->set_payment_id($payment_id);
                $verify_results = $HC->verify_payment();
                $HC->complete_payment();

                $invoice = $this->session->userdata('invoice');
                $this->session->unset_userdata('invoice');

                //Add card to billing_credit_cards
                $credit_card_data = array(
                    'course_id'=>$this->session->userdata('course_id'),
                    'customer_id'=>$person_id,
                    'token'=>(string)$verify_results->Token,
                    'token_expiration'=>date('Y-m-d', strtotime('+2 years')),
                    'card_type'=>(string)$verify_results->CardType,
                    'masked_account'=>(string)$verify_results->MaskedAccount,
                    'cardholder_name'=>(string)$verify_results->CardholderName
                );
                $credit_card_id = $this->save($credit_card_data);

                //Update credit card payment data
                $payment_data = array (
                    'course_id'=>$this->session->userdata('course_id'),
                    'mercury_id'=>$this->config->item('mercury_id'),
                    'tran_type'=>(string)$verify_results->TranType,
                    'amount'=>(string)$verify_results->Amount,
                    'auth_amount'=>(string)$verify_results->AuthAmount,
                    'card_type'=>(string)$verify_results->CardType,
                    'frequency'=>'Recurring',
                    'masked_account'=>(string)$verify_results->MaskedAccount,
                    'cardholder_name'=>(string)$verify_results->CardholderName,
                    'ref_no'=>(string)$verify_results->RefNo,
                    'operator_id'=>(string)$verify_results->OperatorID,
                    'terminal_name'=>(string)$verify_results->TerminalName,
                    'trans_post_time'=>(string)$verify_results->TransPostTime,
                    'auth_code'=>(string)$verify_results->AuthCode,
                    'voice_auth_code'=>(string)$verify_results->VoiceAuthCode,
                    'payment_id'=>$payment_id,
                    'acq_ref_data'=>(string)$verify_results->AcqRefData,
                    'process_data'=>(string)$verify_results->ProcessData,
                    'token'=>(string)$verify_results->Token,
                    'response_code'=>(int)$verify_results->ResponseCode,
                    'status'=>(string)$verify_results->Status,
                    'status_message'=>(string)$verify_results->StatusMessage,
                    'display_message'=>(string)$verify_results->DisplayMessage,
                    'avs_result'=>(string)$verify_results->AvsResult,
                    'cvv_result'=>(string)$verify_results->CvvResult,
                    'tax_amount'=>(string)$verify_results->TaxAmount,
                    'avs_address'=>(string)$verify_results->AVSAddress,
                    'avs_zip'=>(string)$verify_results->AVSZip,
                    'payment_id_expired'=>(string)$verify_results->PaymendIDExpired,
                    'customer_code'=>(string)$verify_results->CustomerCode,
                    'memo'=>(string)$verify_results->Memo
                );
                $this->sale->update_credit_card_payment($invoice, $payment_data);
                if ($payment_data['response_code'] === 0 && $payment_data['status'] === "Approved"){
                    $approved = true;
                }
            }
		}else if($this->config->item('element_account_id')){
			
			$this->load->library('v2/Cart_lib');
			$payment_account_id = $this->input->get('PaymentAccountID');
			$invoice_id = $this->session->userdata('invoice');
			$this->session->unset_userdata('invoice');
			
			if(empty($payment_account_id)){
				return false;
			}
			
			$this->load->library('v2/Element_merchant');
			
			// Initialize payment library
			$this->element_merchant->init(
				new GuzzleHttp\Client()
			);

			// Save was successful. Retrieve details on the card 
			$this->element_merchant->payment_account_query(array(
				'payment_account_id' => $payment_account_id
			));

			$card_details = (array) $this->element_merchant->response()->xml()
				->Response->QueryData->Items->Item;	
			
			$exp_date = DateTime::createFromFormat('y-m-d', (string) $card_details['ExpirationYear'].'-'.(string) $card_details['ExpirationMonth'].'-01');
			
			$card_data = array(
				'course_id' => $this->session->userdata('course_id'),
				'customer_id' => $person_id,				
				'token' => (string) $payment_account_id,
				'token_expiration' => '',
				'card_type' => $this->cart_lib->standardize_credit_card_type((string) $card_details['PaymentBrand']),
				'masked_account' => substr((string) $card_details['TruncatedCardNumber'], -4, 4),
				'expiration' => $exp_date->format('Y-m-d'),
				'cardholder_name' => ''
			);
			
			// Update database with card data from Express API
			$credit_card_id = $this->Customer_credit_card->save($card_data);	
			$approved = true;			
		}
		
		$credit_card_data['credit_card_id'] = !empty($credit_card_id) ? $credit_card_id : 0;
		if($approved){
			return $credit_card_data['credit_card_id'];
		}else{
			return false;
		}
	}
}
