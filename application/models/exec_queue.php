<?php
/**
 * Class Models_Analysis
 *
 * @method \models\ExecQueue\Row[] get_many_by
 * @method \models\ExecQueue\Row get_by
 */

class exec_queue extends MY_Model{
    public $_table = 'exec_queue';
    public $_row = 'models\ExecQueue\Row';
    private $construct_time;

    public function __construct()
    {
        parent::__construct();
        $this->construct_time = gmdate(strtotime('now'));
    }


    public function startThread()
    {
        $runningProcesses = $this->getRunningProcesses();
        $processToRun = $this->get_by(["timestamp_started"=>null]);

        if(count($runningProcesses) < 8 && $processToRun){
            //If there is a process to run then get the lock for that process
            $query = $this->db->query("SELECT GET_LOCK('exec_queue_{$processToRun->id}',1) AS lockstatus");
            $row = $query->row();
            if($row->lockstatus == 1){
                $processToRun = $this->get_by(["id"=>$processToRun->id]);
                if(empty($processToRun->timestamp_started) ){
                    $processToRun->timestamp_started = \Carbon\Carbon::now()->toDateTimeString();
                    $this->update($processToRun->id,$processToRun);
                    //If you are able to get the lock, update the process to running, then start again
                    $result = $this->db->query("SELECT RELEASE_LOCK('exec_queue_{$processToRun->id}') AS lockstatus");
                } else {
                    $this->startThread();
                }
            } else {
                $this->startThread();
            }

            $result = shell_exec($processToRun->command);

            echo "<pre>$result</pre>";
            
            $this->db->reconnect();
            $processToRun->timestamp_finished = \Carbon\Carbon::now()->toDateTimeString();
            $this->update($processToRun->id,$processToRun);
            $this->db->query("UPDATE foreup_exec_queue SET count = count + 1 WHERE id = {$processToRun->id}");

            // Only run next thread if we haven't been running for longer than 20 minutes
            if ($this->construct_time > gmdate(strtotime('-20 minutes')))
            {
                $this->startThread();
            }
        }
        return true;
    }

    public function getRunningProcesses()
    {
        return $this->get_many_by(["timestamp_started IS NOT NULL"=>null,"timestamp_finished"=>null]);
    }

    public function alreadyInQueue($parameters)
    {
        $parameters['timestamp_finished'] = null;
        $queued_process = $this->get_by($parameters);

        return count($queued_process) > 0 ? true : false;
    }


}