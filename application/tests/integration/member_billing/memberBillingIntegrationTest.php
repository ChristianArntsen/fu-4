<?php
namespace tests\integration\member_billing;

use Codeception\Specify;
use fu\statementWorkflow\classes\FakeQueue;

class MemberBillingIntegrationTest extends \Codeception\TestCase\Test {

    use Specify;
    private  $statementQueue;
    private  $chargeQueue;
    private static $CI;

    private $chargeJobGenerator,$chargeJobProcessor,$generateStatementJobs,$statementJobProcessor;

	public static function setUpBeforeClass(){

		self::$CI =& get_instance();
	}

	public function _before(){

		$this->statementQueue = new FakeQueue();
		$this->chargeQueue = new FakeQueue();

		$this->chargeJobGenerator = new \fu\statementWorkflow\generateCharges($this->chargeQueue);
		$this->chargeJobProcessor = new \fu\statementWorkflow\generateChargeSale($this->chargeQueue);
		$this->generateStatementJobs = new \fu\statementWorkflow\generateStatementJobs($this->statementQueue);
		$this->statementJobProcessor = new \fu\statementWorkflow\processStatementJobs($this->statementQueue);

		self::resetData();
		self::clearInvoices();
		self::clearStatements();
	}

	public function _after(){
		self::assign_customers();
		unset($this->chargeJobGenerator);
		unset($this->chargeJobProcessor);
		unset($this->generateStatementJobs);
		unset($this->statementJobProcessor);
	}

	public static function clearStatements(&$CI = null){

		if (!isset($CI))$CI =& self::$CI;
        $CI->db->query('SET FOREIGN_KEY_CHECKS = 0');
        $CI->db->query('DELETE FROM foreup_account_statement_charges');
        $CI->db->query('DELETE FROM foreup_account_statement_payments');
        $CI->db->query('DELETE FROM foreup_account_statements WHERE id != 1');
        $CI->db->query("DELETE FROM `foreup_course_increments` WHERE type = 'account_statements'");
        $CI->db->query("INSERT INTO `foreup_course_increments` VALUES (6270, 'account_statements', 1)");
        $CI->db->query('SET FOREIGN_KEY_CHECKS = 1');
    }

    public static function clearInvoices(& $CI = null){
	    if(!isset($CI))$CI = self::$CI;

        $CI->db->query('SET FOREIGN_KEY_CHECKS = 0');
        $CI->db->query('DELETE FROM foreup_invoices WHERE account_recurring_charge_id IS NOT NULL');
        $CI->db->query('SET FOREIGN_KEY_CHECKS = 1');
    }

    public static function resetData(& $CI = null){
		if(!isset($CI))$CI = self::$CI;

        $sql = <<<SQL
            SET FOREIGN_KEY_CHECKS = 0;
            DELETE FROM `foreup_account_recurring_charge_items`;
            DELETE FROM `foreup_account_recurring_charges`;
            DELETE FROM `foreup_account_recurring_statements`;
            DELETE FROM `foreup_repeatable`;
            
            -- Recurring Charge 1 --
            INSERT INTO `foreup_account_recurring_statements` VALUES (1, NULL, '', NULL, '2017-03-20 16:39:07', 12, 6270, 1, NULL, NULL, 1, 1, 1, 1, 0, 1, 14, 1, 0, 3, 1, 1, 1, 1, 1, 'Footer here', 'Thank you for your business!', 'Terms and conditions here', 0, 1, 20.00, 'fixed', 1);
            INSERT INTO `foreup_repeatable` VALUES (1, 'recurring_statement', 1, NULL, '2030-02-01 00:00:00', 'MONTHLY', NULL, NULL, 1, NULL, NULL, NULL, NULL, '1', NULL, NULL, '0', '0', '0', '1');
            
            INSERT INTO `foreup_account_recurring_charges` VALUES (1, 'Testing monthly membership', NULL, '2017-03-20 16:39:07', 12, 1, 6270, 1, NULL, NULL, '', 1);
                        
            INSERT INTO `foreup_account_recurring_charge_items` VALUES (1, 1, 1994, 'item', NULL, 0.00, 2.00, 1, 2);
            INSERT INTO `foreup_repeatable` VALUES (2, 'recurring_charge_item', 1, NULL, '2030-03-05 00:00:00', 'MONTHLY', NULL, NULL, 1, NULL, NULL, NULL, NULL, '5', NULL, NULL, '0', '0', '0', '1');
            
            INSERT INTO `foreup_account_recurring_charge_items` VALUES (2, 1, 5035, 'item', NULL, 0.00, 1.00, 1, 3);
            INSERT INTO `foreup_repeatable` VALUES (3, 'recurring_charge_item', 2, NULL, '2030-03-05 00:00:00', 'MONTHLY', NULL, NULL, 1, NULL, NULL, NULL, NULL, '5', NULL, NULL, '0', '0', '0', '1');
            
            -- Recurring Charge 2 --
            INSERT INTO `foreup_account_recurring_statements` VALUES (2, NULL, '', NULL, '2017-05-10 15:28:48', 12, 6238, 1, NULL, NULL, 1, 1, 1, 1, 0, 1, 14, 1, 0, 3, 1, 4, 1, 1, 1, 'Footer here', 'Thank you for your business!', 'Terms and conditions here', 0, 1, 5.00, 'percent', 1);
            
            INSERT INTO `foreup_repeatable` VALUES (4, 'recurring_statement', 2, NULL, '2030-01-01', 'MONTHLY', NULL, NULL, NULL, NULL, '1,4,7,10', NULL, NULL, '1', NULL, NULL, '0', '0', '0', NULL);
            
            INSERT INTO `foreup_account_recurring_charges` VALUES (2, 'Quarterly Bill', NULL, '2017-05-10 15:28:49', 12, 1, 6238, 1, NULL, NULL, '', 2);
            
            INSERT INTO `foreup_account_recurring_charge_items` VALUES (3, 1, 5044, 'item', 250, 0.00, 1.00, 2, 5);
            INSERT INTO `foreup_repeatable` VALUES (5, 'recurring_charge_item', 3, NULL, '2030-01-31 00:00:00', 'MONTHLY', NULL, NULL, 1, NULL, NULL, NULL, NULL, '-1', NULL, NULL, '0', '0', '0', '1');
            
            INSERT INTO `foreup_account_recurring_charge_items` VALUES (4, 2, 5036, 'item', NULL, 0.00, 1.00, 2, 6);
            INSERT INTO `foreup_repeatable` VALUES (6, 'recurring_charge_item', 4, NULL, '2030-01-01 23:59:59', 'WEEKLY', NULL, '2030-03-01 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, '23', '59', '59', NULL);
            
            -- Recurring Charge 3 --
            INSERT INTO `foreup_account_recurring_statements` VALUES (3, NULL, '', NULL, '2017-03-20 16:39:07', 12, 6270, 1, NULL, NULL, 1, 1, 1, 1, 0, 1, 14, 1, 0, 3, 1, 1, 1, 1, 1, 'Footer here', 'Thank you for your business!', 'Terms and conditions here', 0, 0, 0, 'fixed', 0);
            INSERT INTO `foreup_repeatable` VALUES (7, 'recurring_statement', 3, NULL, '2030-01-01 00:00:00', 'MONTHLY', NULL, '2030-02-01 00:00:00', 1, NULL, NULL, NULL, NULL, '1', NULL, NULL, '0', '0', '0', '1');
            
            INSERT INTO `foreup_account_recurring_charges` VALUES (3, 'Testing membership with end date', NULL, '2017-03-20 16:39:07', 12, 1, 6270, 1, NULL, NULL, '', 3);
                        
            INSERT INTO `foreup_account_recurring_charge_items` VALUES (5, 1, 1994, 'item', NULL, 0.00, 1.00, 3, 8);
            INSERT INTO `foreup_repeatable` VALUES (8, 'recurring_charge_item', 5, NULL, '2030-01-31 00:00:00', 'MONTHLY', NULL, '2030-02-01 00:00:00', 1, NULL, NULL, NULL, NULL, '-1', NULL, NULL, '0', '0', '0', '1');
            
            -- Make sure we update the increments table since we manually inserted a statement
            DELETE FROM `foreup_course_increments` WHERE type = 'account_statements';
            INSERT INTO `foreup_course_increments` VALUES (6270, 'account_statements', 1);
            INSERT INTO `foreup_course_increments` VALUES (6238, 'account_statements', 1);
            
            SET FOREIGN_KEY_CHECKS = 1;
SQL;

        $queries = explode(';', $sql);
        foreach($queries as $query){
            if(empty($query)) continue;
            $CI->db->query(trim($query));
        }
    }

    public static function clearCustomers(& $CI = null){
	    if(!isset($CI))$CI = self::$CI;

        $CI->db->query("DELETE FROM `foreup_account_recurring_charge_customers`");
    }

	public static function assign_customers(& $CI = null){
		if(!isset($CI))$CI = self::$CI;

        // Assign test customers to charges
        // Also zero out any customer/member balances
		$sql = <<<SQL
		SET FOREIGN_KEY_CHECKS = 0;
		DELETE FROM `foreup_account_recurring_charge_customers`;
		INSERT INTO `foreup_account_recurring_charge_customers` VALUES (1, 1, 3399, 6270, NULL);
        INSERT INTO `foreup_account_recurring_charge_customers` VALUES (2, 2, 1010, 6238, NULL);
        INSERT INTO `foreup_account_recurring_charge_customers` VALUES (3, 2, 2022, 6238, NULL);
        INSERT INTO `foreup_account_recurring_charge_customers` VALUES (4, 3, 3399, 6270, NULL);
        
        UPDATE `foreup_customers` SET account_balance = 0, member_account_balance = 0, invoice_balance = 0 WHERE person_id IN (3399,1010,2022);
        DELETE FROM `foreup_account_transactions` WHERE trans_customer IN (3399,1010,2022) OR trans_household IN (3399, 1010, 2022);
        
		SET FOREIGN_KEY_CHECKS = 1;
SQL;

		$queries = explode(';', $sql);
		foreach($queries as $query){
			if(empty($query)) continue;
			$CI->db->query(trim($query));
		}
	}

    public function testNoCustomers(){
	    self::clearCustomers();

	    $chargeJobGenerator = new \fu\statementWorkflow\generateCharges($this->chargeQueue);

	    $currentTime = '2030-01-02';
	    $chargeJobGenerator->run($currentTime);

	    // get the error queue
	    $errors = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomersError');
	    $this->specify("1 error queue entry should be created if no customers assigned to rec charges", function () use ($errors) {
		    $this->assertNotEmpty($errors);
		    $this->assertCount(1, $errors);
		    $err = json_decode($errors[0]->get('Body'), true);
		    $this->assertEquals('No customers added to recurring charge', $err['message']);
	    });
	    self::resetData();
    }

    public function testChargeJobInserter(){
	    self::assign_customers();
        $currentTime = '2030-01-01';
        $chargeJobGenerator = $this->chargeJobGenerator;
        $chargeJobGenerator->run($currentTime);

	    $messages = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomers');
	    $errors = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomersError');

        $this->specify("No new charge jobs should be created on Jan 1st", function() use ($messages){
            $this->assertEmpty($messages);
        });

        $currentTime = '2030-01-02';
        $this->chargeQueue->clearQueues();
        $chargeJobGenerator->run($currentTime);
	    $messages = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomers');
	    $errors = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomersError');

        // Weekly charge is set to run on the 1st, but it should
        // be at the END of the day, so it won't generate till the 2nd
        $this->specify("2 New charge jobs should be created on Jan 2nd", function() use ($messages){
            $this->assertNotEmpty($messages);
            $this->assertCount(2, $messages);
        });

        $currentTime = '2030-03-05';
        $this->chargeQueue->clearQueues();
        $chargeJobGenerator->run($currentTime);
	    $messages = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomers');
	    $errors = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomersError');

	    // TODO: Should we sweep up *all* missed jobs assuming we miss several days
	    // or should we assume that gaps are covered?
	    // The former could result in strange behavior if a user lets their membership lapse
	    // then renews it.
	    // This is perhaps covered by keeping track of date added for a customer.
	    // Probably should require some kind of manual intervention to fill in missed runs

        $this->specify("5 New charge jobs should be created on March 5th. Message queue should have 7 total jobs.", function() use ($messages){
            // Customer 1 should have 3 charges
	        // Customer 2 and 3 should have 2 charges each
	        // I would expect 6 charges, since all of the items need to be charged
        	$this->assertNotEmpty($messages);
            $this->assertCount(7, $messages);
        });
	    $this->chargeQueue->clearQueues();
    }

    public function testChargeGeneration()
    {
	    self::assign_customers();

        $currentTime = '2030-01-02';
	    // the queue is empty, since we started up a new environment
	    // fill the queue
	    $chargeJobGenerator = $this->chargeJobGenerator;
	    $chargeJobGenerator->run($currentTime);

	    // now process the queue
	    $jobProcessor = $this->chargeJobProcessor;
        $jobProcessor->run($currentTime);

        // January 1st (weekly) charge. The charge is not generated until the 2nd, but should be dated on January 1st
        // at the end of the day
        $charges = self::$CI->db->query("SELECT invoice_id, total, paid, account_recurring_charge_id 
            FROM foreup_invoices 
            WHERE account_recurring_charge_id = 2
                AND `date` = '2030-01-01 23:59:59'
            ORDER BY total DESC")->result_array();

        $charge1 = false;
        if(!empty($charges[0])){
            $charge1 = $charges[0];
        }

        $this->specify("2 New invoice charges should be created dated on 2030-01-01 (at the end of the day)", function () use ($charges) {
            // one charge for each of the two customers
        	$this->assertNotEmpty($charges);
            $this->assertCount(2, $charges);
        });

        $this->specify("Jan 1 invoice charge total should be 5.00", function () use ($charge1){
            $this->assertNotEmpty($charge1);
            $this->assertEquals(5.00, $charge1['total']);
        });

        $messages = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomers');
	    $this->specify("All messages should be processed", function() use ($messages){
		    $this->assertEmpty($messages);
	    });

	    $chargeJobGenerator = $this->chargeJobGenerator;
	    $chargeJobGenerator->run($currentTime);

	    $messages = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomers');
	    $this->specify("Should not re-run completed charges", function() use ($messages){
		    $this->assertEmpty($messages);
	    });


	    $currentTime = '2030-01-31';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);


	    // January 31st (monthly) charge
	    $charges = self::$CI->db->query("SELECT invoice_id, total, paid, account_recurring_charge_id 
            FROM foreup_invoices 
            WHERE account_recurring_charge_id = 2
                AND DATE(date) = '2030-01-31'
            ORDER BY total DESC")->result_array();

	    $charge1 = false;
	    if(!empty($charges[0])){
		    $charge1 = $charges[0];
	    }

	    $this->specify("2 New invoice charge should be generated with a 'date' of 2030-01-31", function () use ($charges) {
		    // two customers charged for 1 item
		    // 5.00 weekly charge
		    // would expect 2 charges
	    	$this->assertNotEmpty($charges);
		    $this->assertCount(2, $charges);
	    });

	    $this->specify("Jan 31 invoice charge total should be 267.50", function () use ($charge1){
		    $this->assertNotEmpty($charge1);
		    $this->assertEquals(267.50, $charge1['total']);
	    });

	    $messages = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomers');
	    $this->specify("All messages should be processed", function() use ($messages){
		    $this->assertEmpty($messages);
	    });

	    $currentTime = '2030-03-05';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);

	    // January 5th (monthly) charges
        $charges = self::$CI->db->query("SELECT invoice_id, total, paid, account_recurring_charge_id 
            FROM foreup_invoices 
            WHERE account_recurring_charge_id = 1
                AND DATE(date) = '2030-03-05'
            ORDER BY total DESC")->result_array();

        $this->specify("2 New invoice charges should be generated with a 'date' of 2030-03-05", function () use ($charges) {
            $this->assertNotEmpty($charges);
            $this->assertCount(2, $charges);
        });

        $charge1 = false;
        $charge2 = false;
        if(!empty($charges[0])){
            $charge1 = $charges[0];
        }
        if(!empty($charges[1])){
            $charge2 = $charges[1];
        }

        $this->specify("Jan 5th invoice 1 total should be one of the expected totals", function () use ($charge1){
            $this->assertNotEmpty($charge1);
            $this->assertTrue(in_array($charge1['total'],[13.90,8.00]),'Charge should be one we are expecting');
        });

        $this->specify("Jan 5th invoice 2 total should be one of the expected totals", function () use ($charge2){
            $this->assertNotEmpty($charge2);
	        $this->assertTrue(in_array($charge2['total'],[13.90,8.00]),'Charge should be one we are expecting');
        });

	    $messages = $this->chargeQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringChargeItemsToCustomers');
	    $this->specify("All messages should be processed", function() use ($messages){
		    $this->assertEmpty($messages);
	    });
    }

    public function testStatementJobInserter(){
	    self::assign_customers();

        $currentTime = '2030-04-01';
        $generateStatementJobs = $this->generateStatementJobs;
        $generateStatementJobs->run($currentTime);
        $messages = $this->statementQueue->fetchMessages('testing_ForeupRepeatableAccountRecurringStatementsToCustomers');

        $this->specify("7 New statement jobs should be created", function() use ($messages){
            $this->assertNotEmpty($messages);
            // a message for each of the customers, and two for the end of the queue indicators
            $this->assertCount(7, $messages);
        });

        $statement1Repeatable = self::$CI->db->query("SELECT * FROM foreup_repeatable WHERE id = 1")->row_array();
        $statement2Repeatable = self::$CI->db->query("SELECT * FROM foreup_repeatable WHERE id = 4")->row_array();

        // catching up on skipped jobs. Running before the 2030-04 date...
        $this->specify("Statement 1 repeatable - Next occurrence should be 3/1/2030 at 12:00am", function() use ($statement1Repeatable){
            $this->assertEquals('2030-03-01 00:00:00', $statement1Repeatable['next_occurence']);
        });

        $this->specify("Statement 2 repeatable - Next occurrence should be 5/1/2030 at 12:00am New York time", function() use ($statement2Repeatable){
            $this->assertEquals('2030-03-31 22:00:00', $statement2Repeatable['next_occurence']);
        });
    }

    public function testStatementGeneration(){
	    self::assign_customers();

	    // the queue is empty, since we started up a new environment
	    // fill the queue
	    $chargeJobGenerator = $this->chargeJobGenerator;

	    // now process the queue
	    $jobProcessor = $this->chargeJobProcessor;

	    $generateStatementJobs = $this->generateStatementJobs;//new \fu\statementWorkflow\generateStatementJobs($this->statementQueue);

	    $statementJobProcessor = $this->statementJobProcessor;//new \fu\statementWorkflow\processStatementJobs($this->statementQueue);

	    $currentTime = '2030-01-01';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-01-08';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-01-15';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-01-22';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-01-31';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-02-01';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-02-08';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);


	    $repeatables = self::$CI->db->query("SELECT *
            FROM foreup_repeatable 
            WHERE id = 2")->result_array();
	    $UT_itm = $repeatables[0];
	    $this->assertEquals($UT_itm['next_occurence'],'2030-03-05 00:00:00');

	    $repeatables = self::$CI->db->query("SELECT *
            FROM foreup_repeatable 
            WHERE id = 1")->result_array();
	    $UT_stmt = $repeatables[0];
	    $this->assertEquals($UT_stmt['next_occurence'],'2030-03-01 00:00:00');

	    $repeatables = self::$CI->db->query("SELECT *
            FROM foreup_repeatable 
            WHERE id = 5")->result_array();
	    $NY_itm = $repeatables[0];
	    $this->assertEquals($NY_itm['next_occurence'],'2030-02-27 22:00:00');

	    $repeatables = self::$CI->db->query("SELECT *
            FROM foreup_repeatable 
            WHERE id = 4")->result_array();
	    $NY_stmt = $repeatables[0];
	    $this->assertEquals($NY_stmt['next_occurence'],'2030-03-31 22:00:00');


	    $currentTime = '2030-02-15';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-02-22';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);


	    $repeatables = self::$CI->db->query("SELECT *
            FROM foreup_repeatable 
            WHERE id = 5")->result_array();
	    $NY_itm = $repeatables[0];
	    $this->assertEquals($NY_itm['next_occurence'],'2030-02-27 22:00:00');


	    $currentTime = '2030-02-27 21:00:00';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);


	    $repeatables = self::$CI->db->query("SELECT *
            FROM foreup_repeatable 
            WHERE id = 5")->result_array();
	    $NY_itm = $repeatables[0];
	    $this->assertEquals($NY_itm['next_occurence'],'2030-02-27 22:00:00');


	    $currentTime = '2030-02-27 22:00:00';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);


	    $repeatables = self::$CI->db->query("SELECT *
            FROM foreup_repeatable 
            WHERE id = 5")->result_array();
	    $NY_itm = $repeatables[0];
	    $this->assertEquals($NY_itm['next_occurence'],'2030-03-30 22:00:00');


	    $currentTime = '2030-02-28';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);


	    $currentTime = '2030-03-01';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);


	    $repeatables = self::$CI->db->query("SELECT *
            FROM foreup_repeatable 
            WHERE id = 2")->result_array();
	    $UT_itm = $repeatables[0];
	    $this->assertEquals($UT_itm['next_occurence'],'2030-03-05 00:00:00');


	    $currentTime = '2030-03-05';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);


	    $repeatables = self::$CI->db->query("SELECT *
            FROM foreup_repeatable 
            WHERE id = 2")->result_array();
	    $UT_itm = $repeatables[0];
	    $this->assertEquals($UT_itm['next_occurence'],'2030-04-05 00:00:00');


	    $currentTime = '2030-03-08';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-03-15';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-03-22';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-03-31';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-04-01';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);

		$gen_sent = $generateStatementJobs->messages_sent;
		$stat_rec = $statementJobProcessor->messages_received;
		$stat_sent = $statementJobProcessor->messages_sent;

	    $currentTime = '2030-04-01';

	    $base = $statementJobProcessor->get_next_statement_number(6270);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

		// 3 generated
	    $this->assertEquals($gen_sent+3,$generateStatementJobs->messages_sent,'all jobs enqueued');
	    // 7 statements in 3 jobs
	    $this->assertEquals($stat_rec+7+3,$statementJobProcessor->messages_received,'all jobs received');
	    // 3 passed on
	    $this->assertEquals($stat_sent+3,$statementJobProcessor->messages_sent,'all statements processed');

        $statements = self::$CI->db->query("SELECT number, date_created, due_date, 
				start_date, end_date, total, total_open
            FROM foreup_account_statements 
            WHERE recurring_statement_id IN (1,2)
                AND DATE(date_created) = '{$currentTime}'
            ORDER BY recurring_statement_id ASC")->result_array();

        $this->specify("3 New statements should be generated with a 'date_created' of 2030-04-01", function() use ($statements){
            $this->assertNotEmpty($statements);
            $this->assertCount(3, $statements);

            foreach ($statements as $statement){
	            $this->assertEquals('2030-04-01 00:00:00',$statement['date_created']);
	            $this->assertEquals('2030-04-15 00:00:00',$statement['due_date']);
	            $this->assertEquals('2030-03-31 23:59:59',$statement['end_date']);
            }
        });

        $endDateRepeatableStatements = self::$CI->db->query("SELECT number, date_created, due_date, 
				start_date, end_date, total, total_open
            FROM foreup_account_statements 
            WHERE recurring_statement_id = 3")->result_array();

        $this->specify("Only 2 statements should be generated which are using a repeatable with an end date", function() use ($endDateRepeatableStatements){
            $this->assertNotEmpty($endDateRepeatableStatements);
            $this->assertCount(2, $endDateRepeatableStatements);
        });

        $statement1 = false;
        $statement2 = false;
        $statement3 = false;
        $statement4 = false;
        if(!empty($statements[0])){
            $statement1 = $statements[0];
        }
        if(!empty($statements[1])){
            $statement2 = $statements[1];
        }
        if(!empty($statements[2])){
            $statement3 = $statements[2];
        }
        if(!empty($statements[3])){
            $statement4 = $statements[3];
        }

        // Statement 1 (monthly)
        $this->specify("Statement 1 start date should be 2030-03-01 00:00:00", function () use ($statement1){
            $this->assertNotEmpty($statement1);
            $this->assertEquals('2030-03-01 00:00:00', $statement1['start_date']);
        });

        $this->specify("Statement 1 end date should be 2030-03-31 23:59:59", function () use ($statement1){
            $this->assertNotEmpty($statement1);
            $this->assertEquals('2030-03-31 23:59:59', $statement1['end_date']);
        });

	    $this->specify("Statement 1 total should be 21.90", function () use ($statement1){
		    $this->assertNotEmpty($statement1);
		    $this->assertEquals(21.90, $statement1['total']);
	    });

	    $this->specify("Statement 1 total should be equal to total_open", function () use ($statement1){
		    $this->assertNotEmpty($statement1);
		    $this->assertEquals($statement1['total'], $statement1['total_open']);
	    });

        $statement4Charges = self::$CI->db->query("SELECT * 
            FROM foreup_account_statement_charges
            WHERE statement_id = (
              SELECT id FROM foreup_account_statements 
              WHERE organization_id = 6270
                AND number = $base + 1
            )
        ")->result_array();

        // Statement 4
        $this->specify("Statement 4 should have 4 charges", function () use ($statement4Charges) {
            $this->assertCount(4, $statement4Charges);
        });

        $this->specify("The 2 account balance charges on statement 4 should be dated for 2030-03-01. The other 2 items shoudld be dated 2030-03-05", function () use ($statement4Charges){
            $this->assertNotEmpty($statement4Charges);
            $this->assertEquals('2030-03-01 00:00:00', $statement4Charges[0]['date_charged']);
            $this->assertEquals('2030-03-01 00:00:00', $statement4Charges[1]['date_charged']);
            $this->assertEquals('2030-03-05 00:00:00', $statement4Charges[2]['date_charged']);
            $this->assertEquals('2030-03-05 00:00:00', $statement4Charges[3]['date_charged']);
        });

        // Statement 2 (quarterly)
        $this->specify("Statement 2 start date should be 2030-01-01 00:00:00", function () use ($statement2){
            $this->assertNotEmpty($statement2);
            $this->assertEquals('2030-01-01 00:00:00', $statement2['start_date']);
        });

        $this->specify("Statement 2 end date should be 2030-03-31 23:59:59", function () use ($statement2){
            $this->assertNotEmpty($statement2);
            $this->assertEquals('2030-03-31 23:59:59', $statement2['end_date']);
        });

	    $this->specify("Statement 2 total should be 847.50", function () use ($statement2){
		    // quarterly: 3*267.50 +9*5.00
		    $this->assertNotEmpty($statement2);
		    $this->assertEquals(847.50, $statement2['total']);
	    });

	    $this->specify("Statement 2 total_open should be equal to total", function () use ($statement2){
		    // quarterly: 3*267.50 +9*5.00
		    $this->assertNotEmpty($statement2);
		    $this->assertEquals($statement2['total'], $statement2['total_open']);
	    });
    }

    public function testCatchUp()
    {
	    self::assign_customers();

	    // the queue is empty, since we started up a new environment
	    // fill the queue
	    $chargeJobGenerator = $this->chargeJobGenerator;

	    // now process the queue
	    $jobProcessor = $this->chargeJobProcessor;

	    $generateStatementJobs = $this->generateStatementJobs;

	    $statementJobProcessor = $this->statementJobProcessor;

	    $currentTime = '2030-01-02';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-04-01';

	    $chargeJobGenerator->runUntilEmpty($currentTime);
	    $jobProcessor->runUntilEmpty($currentTime);
	    $generateStatementJobs->runUntilEmpty($currentTime);
	    $statementJobProcessor->runUntilEmpty($currentTime);

	    $statements = self::$CI->db->query("SELECT number, date_created, due_date, start_date, end_date, total 
            FROM foreup_account_statements 
            WHERE recurring_statement_id IN (1,2)
                AND DATE(date_created) = '{$currentTime}'
            ORDER BY recurring_statement_id ASC")->result_array();

	    $this->assertEquals(10, $chargeJobGenerator->run_cycles);
	    $this->assertEquals(5, $generateStatementJobs->run_cycles);

	    $this->assertEquals(3,count($statements));

	    foreach($statements as $statement)
	    {
	    	$this->assertTrue(in_array($statement['total']*1, [21.90, 847.50]));
	    }
    }

    public function testEndOfDayCharges(){

        self::assign_customers();

        $currentTime = '2030-01-01';
        $this->chargeJobGenerator->run($currentTime);
        $this->chargeJobProcessor->run($currentTime);
        $this->generateStatementJobs->run($currentTime);
        $this->statementJobProcessor->run($currentTime);

        $this->assertEquals(0, $this->chargeJobGenerator->messages_sent);
        $this->assertEquals(1, $this->generateStatementJobs->run_cycles);

        $currentTime = '2030-01-02';
        $this->chargeJobGenerator->run($currentTime);
        $this->chargeJobProcessor->run($currentTime);
        $this->generateStatementJobs->run($currentTime);
        $this->statementJobProcessor->run($currentTime);

        // 2 Messages should be sent since there are 2 customers attached to the charge
        $this->assertEquals(2, $this->chargeJobGenerator->messages_sent);
        $this->assertEquals(2, $this->generateStatementJobs->run_cycles);
    }
}