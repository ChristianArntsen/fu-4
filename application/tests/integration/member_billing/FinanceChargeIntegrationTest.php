<?php
namespace tests\integration\member_billing;

use Codeception\Specify;
use fu\statementWorkflow\classes\FakeQueue;

class FinanceChargeIntegrationTest extends \Codeception\TestCase\Test {

    use Specify;
    private  $statementQueue;
    private  $chargeQueue;
    private static $CI;

    private $chargeJobGenerator,$chargeJobProcessor,$generateStatementJobs,$statementJobProcessor,
      $financeChargeGenerator,$financeChargeProcessor;

	public static function setUpBeforeClass(){

		self::$CI =& get_instance();
	}

	public function _before(){

		$this->statementQueue = new FakeQueue();
		$this->chargeQueue = new FakeQueue();

		$this->chargeJobGenerator = new \fu\statementWorkflow\generateCharges($this->chargeQueue);
		$this->chargeJobProcessor = new \fu\statementWorkflow\generateChargeSale($this->chargeQueue);
		$this->generateStatementJobs = new \fu\statementWorkflow\generateStatementJobs($this->statementQueue);
		$this->statementJobProcessor = new \fu\statementWorkflow\processStatementJobs($this->statementQueue);

		$this->financeChargeGenerator = new \fu\statementWorkflow\generateStatementFinanceChargeJobs($this->statementQueue);
		$this->financeChargeProcessor = new \fu\statementWorkflow\processStatementFinanceChargeJobs($this->statementQueue);

		MemberBillingIntegrationTest::resetData(self::$CI);
		MemberBillingIntegrationTest::clearInvoices(self::$CI);
	}

	public function _after(){
		MemberBillingIntegrationTest::assign_customers(self::$CI);
		unset($this->chargeJobGenerator);
		unset($this->chargeJobProcessor);
		unset($this->generateStatementJobs);
		unset($this->statementJobProcessor);
	}

    public function testFinanceChargeJobInserter() {
	    MemberBillingIntegrationTest::assign_customers(self::$CI);
	    MemberBillingIntegrationTest::clearInvoices(self::$CI);
	    MemberBillingIntegrationTest::clearStatements(self::$CI);

	    // the queue is empty, since we started up a new environment
	    // fill the queue
	    $chargeJobGenerator = $this->chargeJobGenerator;

	    // now process the queue
	    $jobProcessor = $this->chargeJobProcessor;

	    $generateStatementJobs = $this->generateStatementJobs;

	    $statementJobProcessor = $this->statementJobProcessor;

	    $financeJobGenerator = $this->financeChargeGenerator;

	    $financeJobProcessor = $this->financeChargeProcessor;

	    $currentTime = '2030-01-01';
	    $chargeJobGenerator->run($currentTime);
	    $jobProcessor->run($currentTime);
	    $generateStatementJobs->run($currentTime);
	    $statementJobProcessor->run($currentTime);

	    $currentTime = '2030-04-01';

	    $chargeJobGenerator->runUntilEmpty($currentTime);
	    $jobProcessor->runUntilEmpty($currentTime);
	    $generateStatementJobs->runUntilEmpty($currentTime);
	    $statementJobProcessor->runUntilEmpty($currentTime);

	    $beforeDate = '2030-04-14';
	    $dueDate = '2030-04-17';

	    $statements = self::$CI->db->query("SELECT number, date_created, due_date, start_date, end_date, total 
            FROM foreup_account_statements 
            WHERE recurring_statement_id IN (1,2)
                AND DATE(date_created) = '{$currentTime}'
            ORDER BY recurring_statement_id ASC")->result_array();

	    foreach($statements as $statement)
	    {
	    	// this is screwy when we pay member and customer accounts...
		    //$this->assertTrue(in_array($statement['total']*1,[21.90,847.50]));
	    }

	    foreach($statements as $statement)
	    {
		    $this->assertTrue(in_array($statement['totalFinanceCharges']*1,[0]));
	    }

	    $financeJobGenerator->run($beforeDate);

	    $this->assertEquals(0,$financeJobGenerator->messages_received);
	    $this->assertEquals(0,$financeJobGenerator->messages_sent);

	    $statements = self::$CI->db->query("SELECT number, date_created, due_date, start_date, end_date, total 
            FROM foreup_account_statements 
            WHERE recurring_statement_id IN (1,2)
                AND DATE(date_created) = '{$currentTime}'
            ORDER BY recurring_statement_id ASC")->result_array();

	    foreach($statements as $statement)
	    {
		    $this->assertTrue(in_array($statement['total']*1,[21.90,847.50]));
	    }

	    foreach($statements as $statement)
	    {
		    $this->assertTrue(in_array($statement['totalFinanceCharges']*1,[0]));
	    }



	    $financeJobGenerator->run($dueDate);

	    $this->assertGreaterThan(0,$financeJobGenerator->messages_received);
	    $this->assertGreaterThan(0,$financeJobGenerator->messages_sent);

	    $statements = self::$CI->db->query("SELECT number, date_created, due_date, start_date, end_date, total 
            FROM foreup_account_statements 
            WHERE recurring_statement_id IN (1,2)
                AND DATE(date_created) = '{$currentTime}'
            ORDER BY recurring_statement_id ASC")->result_array();

	    // jobs have not been processed yet. No change expected.
	    foreach($statements as $statement)
	    {
		    $this->assertTrue(in_array($statement['total']*1,[21.90,847.50]));
	    }

	    foreach($statements as $statement)
	    {
		    $this->assertTrue(in_array($statement['totalFinanceCharges']*1,[0]));
	    }

	    $financeJobProcessor->run($dueDate);
	    $this->assertGreaterThan(0,$financeJobProcessor->messages_received);
	    $this->assertGreaterThan(0,$financeJobProcessor->messages_sent);
	    $this->assertEquals(3,$financeJobProcessor->messages_received);
	    $this->assertEquals(3,$financeJobProcessor->messages_sent);

	    $statements = self::$CI->db->query("SELECT number, date_created, due_date, 
			start_date, end_date, total,
			total_finance_charges
            FROM foreup_account_statements 
            WHERE recurring_statement_id IN (1,2)
                AND DATE(date_created) = '{$currentTime}'
            ORDER BY recurring_statement_id ASC")->result_array();

	    foreach($statements as $statement)
	    {
		    $this->assertTrue(in_array($statement['total']*1, [41.90, 889.88]));
	    }

	    foreach($statements as $statement)
	    {
		    $this->assertTrue(in_array($statement['total_finance_charges']*1, [42.38, 20.00]));
	    }

	    MemberBillingIntegrationTest::resetData(self::$CI);
    }

    public function testChargeGeneration()
    {
	    MemberBillingIntegrationTest::resetData(self::$CI);
    }
}