<?php
use \Salesv2apiGuy;

class ViewRecentSaleCest
{
    var $sale;
    var $sale_id;
    var $sale_number;

    public function _before(Salesv2apiGuy $I)
    {
        $I->login('jackbarnes', 'jackbarnes');
        $I->setHeader('Content-Type', 'application/json');
        $I->setHeader('Api-key', 'no_limits');

        $this->sale_number = $I->grabFromDatabase('foreup_sales', 'max(number)') + 1;
        $this->sale = array('CID' => 0, 'course_id' => 6270, 'sale_number' => 0, 'employee_id' => 12, 'override_authorization_id' => 0, 'table_id' => 0, 'terminal_id' => 1, 'payment_type' => 'Cash: $2.16'.'<br />', 'auto_gratuity' => number_format(0.00, 2), 'deleted_by' => 0, 'deleted_at' => '0000-00-00 00:00:00', 'number' => $this->sale_number, );

        $I->placeInDatabase('foreup_sales', $this->sale);
        $this->sale_id = $I->grabFromDatabase('foreup_sales', 'max(sale_id)');
    }

    public function _after(Salesv2apiGuy $I)
    {

    }

    // tests
    public function tryToTest(Salesv2apiGuy $I)
    {

    }
}