<?php

/*
 * To check if the API responses have changed recently turn debug mode on by adding the -d command when running the test
 * format of notice that API response has changed:
 *
 *      Responses are NOT equal
 *      --Expected
 *      ++Actual
 *          ++<new response message>
 *          --<old response message>
 *
 */

class SalesRegisterCest
{
    var $course_id = 6270;
    var $orig_last_cart_id;

    var $new_cart_id;
    var $new_sale_id;
    var $new_number;
    var $customer;
    var $customer_info;
    var $customer_id = 1000000;
    var $customer_loyalty_points = 20000;
    var $Cart;
    var $Cart_item_id;
    var $Cart_inventory = 10000;
    var $Cart_total = 53.38;
    var $Cart_Green_Fee_Season = 20;
    var $DrPepper;
    var $DrPepper_item_number = null;
    var $DrPepper_item_id;
    var $DrPepper_inventory_level = 200000;
    var $Employee = 'Jack O\'Barnes';
    var $rocketBalls;
    var $rocketBalls_item_number = 885583349767;
    var $rocketBalls_item_id;
    var $rocketBalls_inventory_level = 200000;


    public function _before(Salesv2apiGuy $I)
    {
        $I->login('jackbarnes', 'jackbarnes');
        $I->setHeader('Content-Type', 'application/json');
        $I->setHeader('Api-key', 'no_limits');

        //Get a new cart number from API
        $cart_id = $I->grabFromDatabase('foreup_pos_cart', 'max(cart_id)');
        $I->sendDELETE('/index.php/api/cart/'.$cart_id);
        $this->new_cart_id = json_decode($I->grabResponse())->cart_id;
        $this->new_number = $I->grabFromDatabase('foreup_sales', 'max(number)') + 1;
        $this->new_sale_id = $I->grabFromDatabase('foreup_sales', 'max(sale_id)') + 1;

        $this->customer = [
            'CID' => 0,
            'person_id' => $this->customer_id,
            'course_id' => $this->course_id,
            'api_id' => 0,
            'username' => '',
            'password' => '',
            'member' => 0,
            'price_class' => 1,
            'image_id' => 0,
            'account_number' => '10231352102135',
            'account_balance' => 0.00,
            'account_balance_allow_negative' => 0,
            'account_limit' => 0.00,
            'member_account_balance' => 0.00,
            'member_account_limit' => 0.00,
            'invoice_balance' => 0.00,
            'invoice_email' => '',
            'use_loyalty' => 1,
            'loyalty_points' => $this->customer_loyalty_points,
            'company_name' => '',
            'discount' => 0.00,
            'deleted' => 0,
            'opt_out_email' => 1,
            'opt_out_text' => 1,
            'unsubscribe_all' => 0,
            'course_news_announcements' => 0,
            'teetime_reminders' => 0,
            'status_flag' => 3,
            'require_food_minimum' => 0
        ];
        $this->customer_info = [
            'first_name' => 'Maurice',
            'last_name' => 'DoppelGaenger',
            'phone_number' => '',
            'cell_phone_number'=> 8014567123,
            'email' => 'doppelgaenger@gmail.com',
            'birthday' => '1980-07-24',
            'address_1' => '1546 E Flash Drive',
            'address_2' => '',
            'city' => 'Lehi',
            'state' => 'UT',
            'zip' => '84043',
            'country' => '',
            'comments' => '',
            'person_id' => $this->customer_id,
            'foreup_news_announcements_unsubscribe' => 0
        ];

        $I->InsertAndUpdateOnDuplicate('foreup_people', $this->customer_info);
        $I->InsertAndUpdateOnDuplicate('foreup_customers', $this->customer);

        $this->Cart = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'name' => '18 Hole Cart',
            'category' => 'Carts',
            'item_number' => $this->course_id.'_seasonal_'.$this->Cart_Green_Fee_Season,
            'image_id' => 0,
            'cost_price' => 50.00,
            'unit_price' => 50.00,
            'unit_price_includes_tax' => 0,
            'max_discount' => 10,
            'quantity' => $this->Cart_inventory,
            'is_unlimited' => 1,
            'allow_alt_description' => 0,
            'is_serialized' => 0,
            'is_giftcard' => 0,
            'invisible' => 0,
            'soup_or_salad' => 'none',
            'is_side' => 0,
            'add_on_price' => 0,
            'print_priority' => 0,
            'erange_size' => 0
        ];

        $I->InsertAndUpdateOnDuplicate('foreup_items', $this->Cart);
        $this->Cart_item_id = $I->grabFromDatabase('foreup_items', 'max(item_id)');
        $db_items_taxes_Sales_Tax = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'item_id' => $this->Cart_item_id,
            'name' => 'Sales Tax',
            'percent' => 6.750,
            'cumulative' => 0
        ];
        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', $db_items_taxes_Sales_Tax);

        $this->DrPepper = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'name' => 'Dr Pepper',
            'department' => 'Food & Beverage',
            'category' => 'Beverage',
            'image_id' => 0,
            'cost_price' => 0.50,
            'unit_price' => 2.00,
            'unit_price_includes_tax' => 0,
            'quantity' => $this->DrPepper_inventory_level,
            'is_unlimited' => 1,
            'location' => 0,
            'allow_alt_description' => 0,
            'is_serialized' => 0,
            'is_giftcard' => 0,
            'invisible' => 0,
            'soup_or_salad' => 'none',
            'is_side' => 0,
            'add_on_price' => 0,
            'print_priority' => 1,
            'quickbooks_income' => 0,
            'quickbooks_cogs' => 0,
            'quickbooks_assets' => 0,
            'erange_size' => 0
        ];

        $I->InsertAndUpdateOnDuplicate('foreup_items', $this->DrPepper);
        $this->DrPepper_item_id = $I->grabFromDatabase('foreup_items', 'max(item_id)');

        $db_items_taxes_Sales_Tax = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'item_id' => $this->DrPepper_item_id,
            'name' => 'Sales Tax',
            'percent' => 6.750,
            'cumulative' => 0
        ];
        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', $db_items_taxes_Sales_Tax);

        $db_items_taxes_Sales_Tax = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'item_id' => $this->DrPepper_item_id,
            'name' => 'Sales Tax 2',
            'percent' => 1.050,
            'cumulative' => 0
        ];
        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', $db_items_taxes_Sales_Tax);

        $this->rocketBalls = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'name' => 'RocketBalls',
            'department' => 'Course',
            'category' => 'PS Balls',
            'subcategory' => '',
            'item_number' => $this->rocketBalls_item_number,
            'description' => '',
            'image_id' => 0,
            'cost_price' => 5.00,
            'unit_price' => 8.00,
            'unit_price_includes_tax' => 0,
            'max_discount' => 0,
            'quantity' => $this->rocketBalls_inventory_level,
            'is_unlimited' => 0,
            'reorder_level' => 10,
            'location' => 0,
            'gl_code' => null,
            'allow_alt_description' => 0,
            'is_serialized' => 0,
            'is_giftcard' => 0,
            'invisible' => 0,
            'deleted' => 0,
            'food_and_beverage' => 0,
            'soup_or_salad' => 'none',
            'number_of_sides' => 0,
            'is_side' => 0,
            'add_on_price' => 0,
            'print_priority' => 1,
            'kitchen_printer' => 1,
            'quickbooks_income' => 0,
            'quickbooks_cogs' => 0,
            'quickbooks_assets' => 0,
            'inactive' => 0,
            'erange_size' => 0,
            'is_fee' => 0,
            'meal_course_id' => 0,
            'do_not_print' => 0,
            'prompt_meal_course' => 0,
            'do_not_print_customer_receipt' => 0
        ];

        $I->InsertAndUpdateOnDuplicate('foreup_items', $this->rocketBalls);
        $this->rocketBalls_item_id = $I->grabFromDatabase('foreup_items', 'max(item_id)');
        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', array('course_id' => $this->course_id, 'CID' => 0, 'item_id' => $this->rocketBalls_item_id, 'name' => 'Sales Tax', 'percent' => 6.750, 'cumulative' => 0));
    }

    public function _after(Salesv2apiGuy $I){}

    // tests

    public function cleanupSetup(Salesv2apiGuy $I){
        //This is used in the cleanup() function
        $this->orig_last_cart_id = $I->grabFromDatabase('foreup_pos_cart', 'max(cart_id)');
    }

    public function customerSearchTest(Salesv2apiGuy $I)
    {
        //setup
        $I->wantTo('test the customer search endpoint');

        //search for customer
        $I->sendGET('/index.php/api/customers?q=Maurice%20DoppelGaenger');
        $response = $I->grabResponse();

        //check that search was successful
        $expectedResponse = [
            [
                "person_id" => $this->customer_id,
                "taxable" => true,
                "discount" => "0.00",
                "first_name" => "Maurice",
                "last_name" => "DoppelGaenger",
                "email" => "doppelgaenger@gmail.com",
                "phone_number" => "",
                "cell_phone_number" => "8014567123",
                "birthday" => "1980-07-24",
                "address_1" => "1546 E Flash Drive",
                "address_2" => "",
                "city" => "Lehi",
                "state" => "UT",
                "zip" => "84043",
                "country" => "",
                "comments" => "",
                "image_id" => null,
                "account_number" => "10231352102135",
                "account_balance" => "0.00",
                "account_balance_allow_negative" => "0",
                "member_account_balance" => "0.00",
                "account_limit" => "0.00",
                "member_account_limit" => "0.00",
                "member_account_balance_allow_negative" => "1",
                "invoice_balance" => "0.00",
                "status_flag" => "3",
                "invoice_balance_allow_negative" => "0",
                "use_loyalty" => "1",
                "loyalty_points" => strval($this->customer_loyalty_points),
                "price_class" => "1",
                "member" => "0",
                "require_food_minimum" => "0",
                "username" => null,
                "opt_out_text" => "1",
                "opt_out_email" => "1",
                "unsubscribe_all" => "0",
                "texting_status" => null,
                "course_id" => "6270",
                "photo" => "http://acceptance.foreupsoftware.com/images/profiles/profile_picture.png",
                "passes" => "",
                "groups" => ""
            ]
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);
    }

    public function addRemoveCustomerFromSale(Salesv2apiGuy $I)
    {
        //setup
        $I->wantTo('add & Remove customer from the sale ');

        //add customer to sale
        $payload = [
            "person_id" => $this->customer_id,
            "selected" => true,
            "taxable" => true,
            "discount" => "0.00"
        ];
        $I->sendPUT('/index.php/api/cart/customers/'.$this->customer_id, $payload);
        $response = $I->grabResponse();

        //check that customer was successfully added
        $expectedResponse = [
            "success" => $this->customer_id
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase('foreup_pos_cart_customers', array('customer_id' => $this->customer_id, 'cart_id' => $this->new_cart_id));

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        //Remove customer from sale
        $I->sendDELETE('/index.php/api/cart/customers/'.$this->customer_id);
        $response = $I->grabResponse();

        //Check for successful removal

        $expectedResponse = [
            "success" => 1
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $cart_customers = [
            'customer_id' => $this->customer_id,
            'cart_id' => $this->new_cart_id
        ];
        $I->dontSeeInDatabase('foreup_pos_cart_customers', $cart_customers);
    }

    public function searchForNonExistentCustomer(Salesv2apiGuy $I)
    {
        //setup
        $I->wantTo('Search for a non-existent customer');

        //search for customer
        $I->sendGET('/index.php/api/customers?q=wafawegeahaert3243y3456gss');

        //Check that no customer was found
        $I->seeResponseCodeIs(200);
        $I->seeResponseEquals('[]');
    }

    public function openReg(Salesv2apiGuy $I)
    {
        //initialize variables
        $open_amount = 100;
        $new_register_id = $I->grabFromDatabase('foreup_register_log', 'max(register_log_id)');

        //set up
        $I->wantTo('Open the register.');

        //open register
        date_default_timezone_set("America/Denver");
        $shiftStart = date('Y-m-d H:i:s');
        $payload = [
            "shift_start" => $shiftStart,
            "shift_end" => "0000-00-00 00:00:00",
            "status" => "open",
            "open_amount" => $open_amount,
            "close_amount" => 0,
            "close_check_amount" => 0,
            "cash_sales_amount" => 0,
            "persist" => 0,
            "employee_id" => "12",
            "terminal_id" => "1",
            "counts" => [
                "pennies" => 0,
                "nickels" => 0,
                "dimes" => 0,
                "quarters" => 0,
                "ones" => 0,
                "fives" => 0,
                "tens" => 0,
                "twenties" => 0,
                "fifties" => 0,
                "hundreds" => 0
            ],
            "closable" => false,
            "blind_close" => "1"
        ];
        $I->sendPOST('/index.php/api/register_logs', $payload);
        $response = $I->grabResponse();

        //Check that the register successfully opened.
        $expectedResponse = [
            "register_log_id" => ($new_register_id + 1)
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $I->seeInDatabase('foreup_register_log', array('register_log_id' => $new_register_id + 1));
    }

//    public function continueOpenReg(Salesv2apiGuy $I)
//    {
//
//        //initialize variables
//        $open_amount = 100;
//
//        //set up
//        $I->wantTo('Continue using a previously opened register.');
//        $new_register_id = $I->grabFromDatabase('foreup_register_log', 'max(register_log_id)');
//
//        //open register
//        date_default_timezone_set("America/Denver");
//        $shiftStart = date('Y-m-d H:i:s');
//        $payload = '{"shift_start":"'.$shiftStart.'","shift_end":"0000-00-00 00:00:00","status":"open","open_amount":'.$open_amount.',"close_amount":0,"close_check_amount":0,"cash_sales_amount":0,"persist":0,"employee_id":"12","terminal_id":"1","counts":{"pennies":0,"nickels":0,"dimes":0,"quarters":0,"ones":0,"fives":0,"tens":0,"twenties":0,"fifties":0,"hundreds":0},"closable":false,"blind_close":"1"}';
//        $I->sendPOST('/index.php/api/register_logs', $payload);
//
//        //Check that the register successfully opened.
//        $I->seeResponseCodeIs(200);
//        $I->seeResponseEquals('{"register_log_id":'.($new_register_id + 1).'}');
//        $I->seeInDatabase('foreup_register_log', array('register_log_id' => $new_register_id + 1));
//
//        $new_register_id += 1;
//
//        //log out
//        $I->sendGET('/index.php/home/logout');
//
//        //check for success
//        $I->seeResponseCodeIs(200);
//        $I->seeInDatabase('foreup_register_log', array('register_log_id' => $new_register_id, 'closing_employee_id' => 0));
//
//        //log back in and continue using the old register
//        $I->login('jackbarnes', 'jackbarnes');
//
//        $I->sendPOST('/index.php/api/register_logs/continue');
//
//        //check for success
//        //$I->seeResponseCodeIs(200);
//        $I->seeResponseEquals('{"success":true}');
//        $I->seeInDatabase('foreup_register_log', array('register_log_id' => $new_register_id->register_log_id, 'closing_employee_id' => 0));
//    }

    public function cashInDrawer(Salesv2apiGuy $I){
        //variable setup
        $amount = 8.54;
        $quantity = 1;
        $payment_type = 'cash';
        $payment_description = 'Cash';

        //test setup
        $I->wantTo('test the amount of cash in the cash drawer');

        //clean up the database
        //$I->deleteGreaterThanOrEqualFromDatabase('foreup_sales', array('DATE(sale_time)' => 'DATE(NOW())'));

        //add item to cart
        $payload = [
            "item_id" => "$this->rocketBalls_item_id",
            "item_type" => "item",
            "quantity" => $quantity,
            "unit_price" => "8",
            "discount_percent" => 0,
            "timeframe_id" => null,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $I->seeResponseCodeIs(200);

        //pay for items in cart with cash
        $payload = [
            "type" => "$payment_type",
            "record_id" => 0,
            "amount" => $amount,
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));

        $paymentID = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        //finish the payment by closing the cart
        $payload = [
            "status" => "complete",
            "customers" => [],
            "items" => [
                [
                    "order" => 0,
                    "item_id" => $this->rocketBalls_item_id,
                    "course_id" => "6270",
                    "name" => "RocketBalls",
                    "department" => "Course",
                    "category" => "PS Balls",
                    "subcategory" => "",
                    "supplier_id" => "532",
                    "item_number" => $this->rocketBalls_item_number,
                    "description" => "",
                    "unit_price" => "8",
                    "base_price" => "8.00",
                    "inactive" => "0",
                    "do_not_print" => "0",
                    "max_discount" => "0.00",
                    "inventory_level" => $this->rocketBalls_inventory_level,
                    "inventory_unlimited" => 1,
                    "cost_price" => "5.00",
                    "is_giftcard" => "0",
                    "is_side" => "0",
                    "food_and_beverage" => "0",
                    "number_sides" => "0",
                    "number_salads" => "0",
                    "number_soups" => "0",
                    "print_priority" => "1",
                    "is_fee" => "0",
                    "erange_size" => "0",
                    "meal_course_id" => "0",
                    "taxes" => [
                        [
                            "name" => "Sales Tax",
                            "percent" => 6.75,
                            "cumulative" => "0",
                            "amount" => 0.54
                        ]
                    ],
                    "is_serialized" => "0",
                    "do_not_print_customer_receipt" => "0",
                    "modifiers" => [],
                    "sides" => "",
                    "printer_groups" => null,
                    "item_type" => "item",
                    "loyalty_points_per_dollar" => false,
                    "loyalty_dollars_per_point" => 0,
                    "quantity" => $quantity,
                    "discount_percent" => 0,
                    "selected" => true,
                    "sub_category" => "",
                    "discount" => 0,
                    "subtotal" => 8,
                    "total" => $amount,
                    "tax" => 0.54,
                    "discount_amount" => 0,
                    "non_discount_subtotal" => 8,
                    "printer_ip" => "",
                    "loyalty_cost" => null,
                    "loyalty_reward" => false,
                    "timeframe_id" => null,
                    "special_id" => null,
                    "giftcard_id" => null,
                    "punch_card_id" => null,
                    "teetime_id" => null,
                    "params" => null,
                    "modifier_total" => 0,
                    "erange_code" => null,
                    "line" => 1,
                    "unit_price_includes_tax" => false,
                    "items" => [],
                    "fee_dropdown" => [],
                    "success" => 1
                ]
            ],
            "payments" => [
                [
                    "amount" => 8.54,
                    "type" => $payment_type,
                    "description" => $payment_description,
                    "record_id" => 0,
                    "invoice_id" => 0,
                    "tip_recipient" => 0,
                    "params" => "",
                    "bypass" => false,
                    "success" => true,
                    "payment_id" => $paymentID,
                    "number" => ""
                ]
            ],
            "total" => $amount,
            "subtotal" => 8,
            "tax" => 0.54,
            "num_items" => "1"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));

        $start = date('Y-m-d');
        $end = str_replace(":", "%3A", date('Y-m-d+H:i:s'));

        $I->sendGET('/index.php/api/sales/cash_sales_total?start='.$start.'+00%3A01%3A00&end='.$end.'&employee_id=12&terminal_id=0&persist=0');
        $response = $I->grabResponse();
        codecept_debug("\n\n$response\n\n");

        $expectedResponse = [
            "cash_sales_amount" => $amount
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $this->new_sale_id = $I->grabFromDatabase('foreup_sales', 'max(sale_id)');

        $db_sales = [
            'sale_id' => $this->new_sale_id,
            'payment_type' => 'Cash: $'.doubleval($amount).'<br />'
        ];
        $I->seeInDatabase('foreup_sales', $db_sales);

        $db_sales = [
          'sale_id' => $this->new_sale_id
        ];
        $I->deleteFromDatabase('foreup_sales_items_taxes', $db_sales);
        $I->deleteFromDatabase('foreup_sales_items', $db_sales);
        $I->deleteFromDatabase('foreup_sales_payments', $db_sales);
        $I->deleteFromDatabase('foreup_sales', $db_sales);
    }

    public function closeReg(Salesv2apiGuy $I)
    {
        //initialize variables
        $close_amount = 100;
        $open_amount = 100;
        $pennies = "0";
        $nickels = "0";
        $dimes = "0";
        $quarters = "0";
        $ones = "0";
        $fives = "0";
        $tens = "0";
        $twenties = "0";
        $fifties = "0";
        $hundreds = "0";
        $new_register_id = $I->grabFromDatabase('foreup_register_log', 'max(register_log_id)');

        //set up
        $I->wantTo('Close the register.');

        //open a new register
        date_default_timezone_set("America/Denver");
        $shiftStart = date('Y-m-d H:i:s');
        $payload = [
            "shift_start" => $shiftStart,
            "shift_end" => "0000-00-00 00:00:00",
            "status" => "open",
            "open_amount" => $open_amount,
            "close_amount" => 0,
            "close_check_amount" => 0,
            "cash_sales_amount" => 0,
            "persist" => 0,
            "employee_id" => "12",
            "terminal_id" => "1",
            "counts" => [
                "pennies" => 0,
                "nickels" => 0,
                "dimes" => 0,
                "quarters" => 0,
                "ones" => 0,
                "fives" => 0,
                "tens" => 0,
                "twenties" => 0,
                "fifties" => 0,
                "hundreds" => 0
            ],
            "closable" => false,
            "blind_close" => "1"
        ];
        $I->sendPOST('/index.php/api/register_logs', $payload);
        $new_register_id += 1;

        //close register
        $payload = [
            "shift_start" => $shiftStart,
            "shift_end" => "0000-00-00 00:00:00",
            "status" => "close",
            "open_amount" => $open_amount,
            "close_amount" => $close_amount,
            "close_check_amount" => 0,
            "cash_sales_amount" => 0,
            "persist" => 0,
            "employee_id" => "12",
            "terminal_id" => "1",
            "counts" => [
                "pennies" => $pennies,
                "nickels" => $nickels,
                "dimes" => $dimes,
                "quarters" => $quarters,
                "ones" => $ones,
                "fives" => $fives,
                "tens" => $tens,
                "twenties" => $twenties,
                "fifties" => $fifties,
                "hundreds" => $hundreds
            ],
            "closable" => false,
            "blind_close" => "1",
            "register_log_id" => $new_register_id,
            "active" => false,
            "closing_employee_id" => "12"
        ];
        $I->sendPUT('/index.php/api/register_logs/'.$new_register_id, $payload);

        //check for success
        $expectedResponse = [
            "success" => true,
            "msg" => "Register log updated"
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        $shiftEnd = '0000-00-00 00:00:00';
        $register_logs = [
            'register_log_id' => $new_register_id,
            'shift_end' => $shiftEnd,
            'closing_employee_id' => 0
        ];
        $I->dontSeeInDatabase('foreup_register_log', $register_logs);

    }

    public function closeRegWithCounts(Salesv2apiGuy $I)
    {
        //initialize variables
        $close_amount = 300;
        $open_amount = 300;
        $pennies = "150";
        $nickels = "10";
        $dimes = "30";
        $quarters = "20";
        $ones = "20";
        $fives = "4";
        $tens = "6";
        $twenties = "2";
        $fifties = "1";
        $hundreds = "1";
        $new_register_id = $I->grabFromDatabase('foreup_register_log', 'max(register_log_id)');

        //set up
        $I->wantTo('Close the register with coins and bills set.');

        //open a new register
        date_default_timezone_set("America/Denver");
        $shiftStart = date('Y-m-d H:i:s');
        $payload = '{"shift_start":"'.$shiftStart.'","shift_end":"0000-00-00 00:00:00","status":"open","open_amount":'.$open_amount.',"close_amount":0,"close_check_amount":0,"cash_sales_amount":0,"persist":0,"employee_id":"12","terminal_id":"1","counts":{"pennies":0,"nickels":0,"dimes":0,"quarters":0,"ones":0,"fives":0,"tens":0,"twenties":0,"fifties":0,"hundreds":0},"closable":false,"blind_close":"1"}';
        $I->sendPOST('/index.php/api/register_logs', $payload);
        $new_register_id += 1;

        //close register
        $payload = '{"shift_start":"2015-07-09 15:23:32","shift_end":"0000-00-00 00:00:00","status":"close","open_amount":'.$open_amount.',"close_amount":'.$close_amount.',"close_check_amount":0,"cash_sales_amount":0,"persist":0,"employee_id":"12","terminal_id":"1","counts":{"pennies":"'.$pennies.'","nickels":"'.$nickels.'","dimes":"'.$dimes.'","quarters":"'.$quarters.'","ones":"'.$ones.'","fives":"'.$fives.'","tens":"'.$tens.'","twenties":"'.$twenties.'","fifties":"'.$fifties.'","hundreds":"'.$hundreds.'"},"closable":false,"blind_close":"1","register_log_id":'.$new_register_id.',"active":false,"closing_employee_id":"12"}';
        $I->sendPUT('/index.php/api/register_logs/'.$new_register_id, $payload);

        date_default_timezone_set("America/Denver");
        $shiftEnd = date('Y-m-d H:i:s');

        //check for success
        $I->seeResponseEquals('{"success":true,"shift_end":"'.$shiftEnd.'","msg":"Register log updated"}');
        $I->seeResponseCodeIs(200);
        $shiftEnd = '0000-00-00 00:00:00';
        $I->dontSeeInDatabase('foreup_register_log', array('register_log_id' => $new_register_id, 'shift_end' => $shiftEnd, 'closing_employee_id' => 0));
        $I->seeInDatabase('foreup_register_log_counts', array('register_log_id' => $new_register_id, 'pennies' => $pennies, 'nickels' => $nickels, 'dimes' => $dimes, 'quarters' => $quarters, 'ones' =>$ones, 'fives' =>$fives, 'tens' =>$tens,'twenties' => $twenties, 'fifties' => $fifties, 'hundreds' => $hundreds));
    }

    public function closeRegWithChecks(Salesv2apiGuy $I)
    {
        //initialize variables
        $close_amount = 217.43;
        $open_amount = 100.00;
        $closeCheckAmount = 117.43;
        $amount = 117.43;
        $pennies = "0";
        $nickels = "0";
        $dimes = "0";
        $quarters = "0";
        $ones = "0";
        $fives = "0";
        $tens = "0";
        $twenties = "0";
        $fifties = "0";
        $hundreds = "0";

        //set up
        $I->wantTo('Close the register with checks.');

        $new_register_id = $I->grabFromDatabase('foreup_register_log', 'max(register_log_id)');

        //open a new register
        date_default_timezone_set("America/Denver");
        $shiftStart = date('Y-m-d H:i:s');
        $payload = '{"shift_start":"'.$shiftStart.'","shift_end":"0000-00-00 00:00:00","status":"open","open_amount":'.$open_amount.',"close_amount":0,"close_check_amount":0,"cash_sales_amount":0,"persist":0,"employee_id":"12","terminal_id":"1","counts":{"pennies":0,"nickels":0,"dimes":0,"quarters":0,"ones":0,"fives":0,"tens":0,"twenties":0,"fifties":0,"hundreds":0},"closable":false,"blind_close":"1"}';
        $I->sendPOST('/index.php/api/register_logs', $payload);
        $new_register_id += 1;

        //add item to cart
        $payload = '{"item_id":"5274","item_type":"green_fee","quantity":1,"unit_price":110,"discount_percent":0,"timeframe_id":"13","price_class_id":4,"special_id":false,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":0}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', $payload);

        //pay for item with a check
        $payload = '{"type":"check","record_id":0,"amount":'.$amount.',"tip_recipient":0,"params":null,"bypass":0}';
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', $payload);

        //close register
        $payload = '{"shift_start":"2015-07-09 15:23:32","shift_end":"0000-00-00 00:00:00","status":"close","open_amount":'.$open_amount.',"close_amount":'.$close_amount.',"close_check_amount":'.$closeCheckAmount.',"cash_sales_amount":0,"persist":0,"employee_id":"12","terminal_id":"1","counts":{"pennies":"'.$pennies.'","nickels":"'.$nickels.'","dimes":"'.$dimes.'","quarters":"'.$quarters.'","ones":"'.$ones.'","fives":"'.$fives.'","tens":"'.$tens.'","twenties":"'.$twenties.'","fifties":"'.$fifties.'","hundreds":"'.$hundreds.'"},"closable":false,"blind_close":"1","register_log_id":'.$new_register_id.',"active":false,"closing_employee_id":"12"}';
        $I->sendPUT('/index.php/api/register_logs/'.$new_register_id, $payload);

        date_default_timezone_set("America/Denver");
        $shiftEnd = date('Y-m-d H:i:s');

        //check for success
        $I->seeResponseEquals('{"success":true,"shift_end":"'.$shiftEnd.'","msg":"Register log updated"}');
        $I->seeResponseCodeIs(200);
        $shiftEnd = '0000-00-00 00:00:00';
        $I->dontSeeInDatabase('foreup_register_log', array('register_log_id' => $new_register_id, 'shift_end' => $shiftEnd, 'closing_employee_id' => 0));
        $I->seeInDatabase('foreup_register_log_counts', array('register_log_id' => $new_register_id, 'pennies' => $pennies, 'nickels' => $nickels, 'dimes' => $dimes, 'quarters' => $quarters, 'ones' =>$ones, 'fives' =>$fives, 'tens' =>$tens,'twenties' => $twenties, 'fifties' => $fifties, 'hundreds' => $hundreds));
        $I->seeInDatabase('foreup_register_log', array('register_log_id' => $new_register_id, 'close_check_amount' => $closeCheckAmount));
    }

    public function closeRegWithMissingCash(Salesv2apiGuy $I)
    {
        //initialize variables
        $close_amount = 100;
        $open_amount = 150;
        $pennies = "0";
        $nickels = "0";
        $dimes = "0";
        $quarters = "0";
        $ones = "0";
        $fives = "0";
        $tens = "0";
        $twenties = "0";
        $fifties = "0";
        $hundreds = "1";
        $new_register_id = $I->grabFromDatabase('foreup_register_log', 'max(register_log_id)');

        //set up
        $I->wantTo('Close the register with missing cash.');

        //register opened on login

        //close register
        $payload = '{"shift_start":"2015-07-09 15:23:32","shift_end":"0000-00-00 00:00:00","status":"close","open_amount":'.$open_amount.',"close_amount":'.$close_amount.',"close_check_amount":0,"cash_sales_amount":0,"persist":0,"employee_id":"12","terminal_id":"1","counts":{"pennies":"'.$pennies.'","nickels":"'.$nickels.'","dimes":"'.$dimes.'","quarters":"'.$quarters.'","ones":"'.$ones.'","fives":"'.$fives.'","tens":"'.$tens.'","twenties":"'.$twenties.'","fifties":"'.$fifties.'","hundreds":"'.$hundreds.'"},"closable":false,"blind_close":"1","register_log_id":'.$new_register_id.',"active":false,"closing_employee_id":"12"}';
        $I->sendPUT('/index.php/api/register_logs/'.$new_register_id, $payload);

        date_default_timezone_set("America/Denver");
        $shiftEnd = date('Y-m-d H:i:s');

        //check for success
        $I->seeResponseEquals('{"success":true,"shift_end":"'.$shiftEnd.'","msg":"Register log updated"}');
        $I->seeResponseCodeIs(200);
        $shiftEnd = '0000-00-00 00:00:00';
        $I->dontSeeInDatabase('foreup_register_log', array('register_log_id' => $new_register_id, 'shift_end' => $shiftEnd, 'closing_employee_id' => 0));
        $I->seeInDatabase('foreup_register_log_counts', array('register_log_id' => $new_register_id, 'pennies' => $pennies, 'nickels' => $nickels, 'dimes' => $dimes, 'quarters' => $quarters, 'ones' =>$ones, 'fives' =>$fives, 'tens' =>$tens,'twenties' => $twenties, 'fifties' => $fifties, 'hundreds' => $hundreds));
    }

    public function closeRegWithExtraCash(Salesv2apiGuy $I)
    {
        //initialize variables
        $close_amount = 200;
        $open_amount = 150;
        $pennies = "0";
        $nickels = "0";
        $dimes = "0";
        $quarters = "0";
        $ones = "0";
        $fives = "0";
        $tens = "0";
        $twenties = "0";
        $fifties = "0";
        $hundreds = "2";
        $new_register_id = $I->grabFromDatabase('foreup_register_log', 'max(register_log_id)');

        //set up
        $I->wantTo('Close the register with extra cash.');

        //register opened on login

        //close register
        $payload = '{"shift_start":"2015-07-09 15:23:32","shift_end":"0000-00-00 00:00:00","status":"close","open_amount":'.$open_amount.',"close_amount":'.$close_amount.',"close_check_amount":0,"cash_sales_amount":0,"persist":0,"employee_id":"12","terminal_id":"1","counts":{"pennies":"'.$pennies.'","nickels":"'.$nickels.'","dimes":"'.$dimes.'","quarters":"'.$quarters.'","ones":"'.$ones.'","fives":"'.$fives.'","tens":"'.$tens.'","twenties":"'.$twenties.'","fifties":"'.$fifties.'","hundreds":"'.$hundreds.'"},"closable":false,"blind_close":"1","register_log_id":'.$new_register_id.',"active":false,"closing_employee_id":"12"}';
        $I->sendPUT('/index.php/api/register_logs/'.$new_register_id, $payload);

        date_default_timezone_set("America/Denver");
        $shiftEnd = date('Y-m-d H:i:s');

        //check for success
        $I->seeResponseEquals('{"success":true,"shift_end":"'.$shiftEnd.'","msg":"Register log updated"}');
        $I->seeResponseCodeIs(200);
        $shiftEnd = '0000-00-00 00:00:00';
        $I->dontSeeInDatabase('foreup_register_log', array('register_log_id' => $new_register_id, 'shift_end' => $shiftEnd, 'closing_employee_id' => 0));
        $I->seeInDatabase('foreup_register_log_counts', array('register_log_id' => $new_register_id, 'pennies' => $pennies, 'nickels' => $nickels, 'dimes' => $dimes, 'quarters' => $quarters, 'ones' =>$ones, 'fives' =>$fives, 'tens' =>$tens,'twenties' => $twenties, 'fifties' => $fifties, 'hundreds' => $hundreds));
    }

    public function closeRegWithCashAndChecks(Salesv2apiGuy $I)
    {
        //initialize variables
        $open_amount = 300;
        $close_amount = $open_amount + $this->Cart_total;
        $closeCheckAmount = $this->Cart_total;
        $quantity = 1;
        $pennies = "150";
        $nickels = "10";
        $dimes = "30";
        $quarters = "20";
        $ones = "20";
        $fives = "4";
        $tens = "6";
        $twenties = "2";
        $fifties = "1";
        $hundreds = "1";
        $new_register_id = $I->grabFromDatabase('foreup_register_log', 'max(register_log_id)');

        //set up
        $I->wantTo('Close the register with coins and bills set.');

        //add item to cart
        $payload = '{"item_id":"'.$this->Cart_item_id.'","item_type":"cart_fee","quantity":'.$quantity.',"unit_price":"50","discount_percent":0,"timeframe_id":0,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', $payload);

        //pay for item with a check
        $payload = '{"type":"check","record_id":0,"amount":'.$this->Cart_total.',"tip_recipient":0,"params":null,"bypass":0}';
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', $payload);

        //close register
        $payload = '{"shift_start":"2015-07-09 15:23:32","shift_end":"0000-00-00 00:00:00","status":"close","open_amount":'.$open_amount.',"close_amount":'.$close_amount.',"close_check_amount":0,"cash_sales_amount":0,"persist":0,"employee_id":"12","terminal_id":"1","counts":{"pennies":"'.$pennies.'","nickels":"'.$nickels.'","dimes":"'.$dimes.'","quarters":"'.$quarters.'","ones":"'.$ones.'","fives":"'.$fives.'","tens":"'.$tens.'","twenties":"'.$twenties.'","fifties":"'.$fifties.'","hundreds":"'.$hundreds.'"},"closable":false,"blind_close":"1","register_log_id":'.$new_register_id.',"active":false,"closing_employee_id":"12"}';
        $I->sendPUT('/index.php/api/register_logs/'.$new_register_id, $payload);

        date_default_timezone_set("America/Denver");
        $shiftEnd = date('Y-m-d H:i:s');

        //check for success
        $I->seeResponseEquals('{"success":true,"shift_end":"'.$shiftEnd.'","msg":"Register log updated"}');
        $I->seeResponseCodeIs(200);
        $shiftEnd = '0000-00-00 00:00:00';
        $I->dontSeeInDatabase('foreup_register_log', array('close_check_amount' => $closeCheckAmount, 'register_log_id' => $new_register_id, 'shift_end' => $shiftEnd, 'closing_employee_id' => 0));
        $I->seeInDatabase('foreup_register_log_counts', array('register_log_id' => $new_register_id, 'pennies' => $pennies, 'nickels' => $nickels, 'dimes' => $dimes, 'quarters' => $quarters, 'ones' =>$ones, 'fives' =>$fives, 'tens' =>$tens,'twenties' => $twenties, 'fifties' => $fifties, 'hundreds' => $hundreds));
    }

    public function suspendSale(Salesv2apiGuy $I)
    {
        //setup
        $I->wantTo('Suspend a sale');

        //add item to cart
        $payload = '{"item_id":"'.$this->Cart_item_id.'","item_type":"cart_fee","quantity":1,"unit_price":"50","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', $payload);

        //suspend sale
        $payload = '{"status":"suspended","suspended_name":"5"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, $payload);

        //check for success
        $I->seeResponseEquals('{"success":true,"cart_id":'.($this->new_cart_id + 1).'}');
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $this->new_cart_id, 'status' => 'suspended'));
        $I->seeInDatabase('foreup_pos_cart_items', array('cart_id' => $this->new_cart_id, 'item_id' => $this->Cart_item_id));
    }

    public function suspendSaleWithCustomer(Salesv2apiGuy $I)
    {
        //setup
        $I->wantTo('Suspend a sale with a customer attached');

        //add item to cart
        $payload = '{"item_id":"'.$this->Cart_item_id.'","item_type":"cart_fee","quantity":1,"unit_price":"50","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', $payload);

        //add customer to sale
        $payload = '{"person_id":'.$this->customer_id.',"selected":true,"taxable":true,"discount":"0.00"}';
        $I->sendPUT('/index.php/api/cart/customers/'.$this->customer_id, $payload);

        //suspend sale
        $payload = '{"status":"suspended","suspended_name":"6"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, $payload);

        //check for success
        $I->seeResponseEquals('{"success":true,"cart_id":'.($this->new_cart_id + 1).'}');
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $this->new_cart_id, 'status' => 'suspended'));
        $I->seeInDatabase('foreup_pos_cart_items', array('cart_id' => $this->new_cart_id, 'item_id' => $this->Cart_item_id));
        $I->seeInDatabase('foreup_pos_cart_customers', array('cart_id' => $this->new_cart_id, 'customer_id' => $this->customer_id));
    }

    public function activateSuspendedSale(Salesv2apiGuy $I)
    {
        //setup
        $I->wantTo('activate a suspended sale');

        //add item to cart
        $payload = '{"item_id":"'.$this->Cart_item_id.'","item_type":"cart_fee","quantity":1,"unit_price":"50","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', $payload);

        //suspend sale
        $payload = '{"status":"suspended","suspended_name":"7"}';
        $I->sendPUT('/index.php/api/cart/' . $this->new_cart_id, $payload);

        //un-suspend sale
        $payload = '{"status":"active"}';
        $I->sendPUT('/index.php/api/cart/' . $this->new_cart_id, $payload);

        //check for success
        $I->seeResponseEquals('{"success":"'.($this->new_cart_id).'"}');
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $this->new_cart_id, 'status' => 'active'));
    }

    public function alterSuspendedSale(Salesv2apiGuy $I)
    {
        //setup
        $I->wantTo('alter a suspended sale');

        //add item to cart
        $payload = '{"item_id":"'.$this->Cart_item_id.'","item_type":"cart_fee","quantity":1,"unit_price":"50","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', $payload);

        //suspend sale
        $payload = '{"status":"suspended","suspended_name":"8"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, $payload);

        //activate suspended sale
        $payload = '{"status":"active"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, $payload);

        //alter contents of cart
        $payload = '{"item_id":"'.$this->DrPepper_item_id.'","item_type":"item","quantity":1,"unit_price":"2.00","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';

        //add new items
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/2', $payload);
        $I->seeResponseEquals('{"success":2}');
        $I->seeResponseCodeIs(200);

        //delete old items
        $I->sendDELETE('/index.php/api/cart/'.$this->new_cart_id.'/items/1');
        $I->seeResponseEquals('{"success":0}');
        $I->seeResponseCodeIs(200);

        //check for success in updating cart items
        $I->seeInDatabase('foreup_pos_cart_items', array('cart_id' => $this->new_cart_id, 'item_id' => $this->DrPepper_item_id));
        $I->dontSeeInDatabase('foreup_pos_cart_items', array('cart_id' => $this->new_cart_id, 'item_id' => $this->Cart_item_id));

        //suspend sale again
        $payload = '{"status":"suspended","suspended_name":"8"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, $payload);

        //check for success
        $max_cart = $this->new_cart_id + 2; //each time the cart is suspended a new cart is created.

        $I->seeResponseEquals('{"success":true,"cart_id":'.$max_cart.'}');
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $this->new_cart_id, 'status' => 'suspended'));
        $I->seeInDatabase('foreup_pos_cart_items', array('cart_id' => $this->new_cart_id, 'item_id' => $this->DrPepper_item_id));
        $I->dontSeeInDatabase('foreup_pos_cart_items', array('cart_id' => $this->new_cart_id, 'item_id' => $this->Cart_item_id));
    }

    public function activ8SameSuspendedSaleOnDiffComps(Salesv2apiGuy $I)
    {
        //setup
        $I->wantTo('activate a suspended sale twice on different computers');

        //add item to cart
        $payload = '{"item_id":"'.$this->Cart_item_id.'","item_type":"cart_fee","quantity":1,"unit_price":"50","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', $payload);

        //suspend sale
        $payload = '{"status":"suspended","suspended_name":"11"}';
        $I->sendPUT('/index.php/api/cart/' . $this->new_cart_id, $payload);

        //un-suspend sale
        $payload = '{"status":"active"}';
        $I->sendPUT('/index.php/api/cart/' . $this->new_cart_id, $payload);

        //check for success
        $I->seeResponseEquals('{"success":"'.($this->new_cart_id).'"}');
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $this->new_cart_id, 'status' => 'active'));

        //un-suspend sale
        $payload = '{"status":"active"}';
        $I->sendPUT('/index.php/api/cart/' . $this->new_cart_id, $payload);

        //check for success
        $I->seeResponseEquals('{"msg":"That cart was already opened by '.$this->Employee.'"}');
        $I->seeResponseCodeIs(409);
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $this->new_cart_id, 'status' => 'active'));
    }

    public function suspendTwoSalesWithOneReopened(Salesv2apiGuy $I){
        $I->wantTo('suspend a sale reopen it and then suspend another sale with the same suspended sale #');

    }

    public function switchSuspendedSales(Salesv2apiGuy $I)
    {
        $cart_1_id = $this->new_cart_id;
        //setup
        $I->wantTo('activate a suspended sale then switch to another suspended sale');

        //add item to cart
        $payload = '{"item_id":"'.$this->Cart_item_id.'","item_type":"cart_fee","quantity":1,"unit_price":"50","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';
        $I->sendPUT('/index.php/api/cart/'.$cart_1_id.'/items/1', $payload);

        //suspend sale
        $payload = '{"status":"suspended","suspended_name":"9"}';
        $I->sendPUT('/index.php/api/cart/'.$cart_1_id, $payload);
        $cart_2_id = json_decode($I->grabResponse())->cart_id;

        //add item to cart
        $payload = '{"item_id":"'.$this->Cart_item_id.'","item_type":"cart_fee","quantity":1,"unit_price":"50","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';
        $I->sendPUT('/index.php/api/cart/'.$cart_2_id.'/items/1', $payload);

        //suspend sale
        $payload = '{"status":"suspended","suspended_name":"10"}';
        $I->sendPUT('/index.php/api/cart/'.$cart_2_id, $payload);

        //un-suspend sale
        $payload = '{"status":"active"}';
        $I->sendPUT('/index.php/api/cart/'.$cart_1_id, $payload);

        //check for success
        $I->seeResponseEquals('{"success":"'.($cart_1_id).'"}');
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $cart_1_id, 'status' => 'active'));

        //un-suspend sale
        $payload = '{"status":"active"}';
        $I->sendPUT('/index.php/api/cart/'.$cart_2_id, $payload);

        //check for success
        $I->seeResponseEquals('{"success":"'.($cart_2_id).'"}');
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $cart_2_id, 'status' => 'active'));
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $cart_1_id, 'status' => 'suspended'));
    }

    public function suspendSaleOverExistingSuspendedSale(Salesv2apiGuy $I)
    {
        $cart_1_id = $this->new_cart_id;

        //setup
        $I->wantTo('Suspend a sale over an existing suspended sale');

        //add item to cart
        $payload = '{"item_id":"'.$this->Cart_item_id.'","item_type":"cart_fee","quantity":1,"unit_price":"50","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';
        $I->sendPUT('/index.php/api/cart/'.$cart_1_id.'/items/1', $payload);

        //suspend sale
        $payload = '{"status":"suspended","suspended_name":"12"}';
        $I->sendPUT('/index.php/api/cart/'.$cart_1_id, $payload);
        $cart_2_id = json_decode($I->grabResponse())->cart_id;

        //check for success
        $I->seeResponseEquals('{"success":true,"cart_id":'.($cart_1_id + 1).'}');
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $cart_1_id, 'status' => 'suspended'));
        $I->seeInDatabase('foreup_pos_cart_items', array('cart_id' => $cart_1_id, 'item_id' => $this->Cart_item_id));

        //add item to cart
        $payload = '{"item_id":"'.$this->Cart_item_id.'","item_type":"cart_fee","quantity":1,"unit_price":"50","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":null,"unit_price_includes_tax":false,"erange_size":"0"}';
        $I->sendPUT('/index.php/api/cart/'.$cart_2_id.'/items/1', $payload);

        //suspend sale
        $payload = '{"status":"suspended","suspended_name":"12"}';
        $I->sendPUT('/index.php/api/cart/'.$cart_2_id, $payload);

        //check that the suspension was unsuccessful
        $I->seeResponseEquals('{"success":false,"msg":"A sale already exists with that number. Choose a different number."}');
        $I->seeResponseCodeIs(400);
        $I->dontSeeInDatabase('foreup_pos_cart', array('cart_id' => $cart_2_id, 'status' => 'suspended'));
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $cart_1_id, 'status' => 'suspended'));
    }

    public function createNewBasicCustomer(Salesv2apiGuy $I)
    {
        /*TO DO
         * This may need to be part of a different test suite
         * since this test uses the customers API not the cart
         * API.
        */

        //('{"selected":true,"person_id":null,"first_name":"Tiger","last_name":"Woods","phone_number":"","cell_phone_number":"8014567123","email":"woods@gmail.com","birthday":"07/24/1980","address_1":"1546 E Flash Drive","address_2":"","city":"Lehi","state":"UT","zip":"84043","country":"","comments":"","status_flag":"3","account_number":"","price_class":"1","discount":"0","taxable":"1","require_food_minimum":"0","member":"0","member_account_limit":0,"account_limit":0,"member_account_balance":"0.00","member_account_balance_allow_negative":"0","account_balance":"0.00","account_balance_allow_negative":"0","invoice_balance":"0.00","use_loyalty":"0","loyalty_points":"0","photo":"http://127.0.0.1:8080/images/profiles/profile_picture.png","groups":[],"passes":[],"username":"","opt_out_email":"1","opt_out_text":"1","unsubscribe_all":0,"password":"","confirm_password":""}');
    }

    public function cleanup(Salesv2apiGuy $I)
    {
        $current_last_cart_id = $I->grabFromDatabase('foreup_pos_cart', 'max(cart_id)');
        $I->deleteGreaterThanFromDatabase('foreup_pos_cart', array('cart_id' => $this->orig_last_cart_id));
        $I->deleteGreaterThanFromDatabase('foreup_pos_cart_items', array('cart_id' => $this->orig_last_cart_id));
        $I->deleteGreaterThanFromDatabase('foreup_pos_cart_payments', array('cart_id' => $this->orig_last_cart_id));
        $I->deleteGreaterThanFromDatabase('foreup_pos_cart_customers', array('cart_id' => $this->orig_last_cart_id));
        $I->dontSeeInDatabase('foreup_pos_cart', array('cart_id' => $current_last_cart_id));
        $I->dontSeeInDatabase('foreup_pos_cart_items', array('cart_id' => $current_last_cart_id));
        $I->dontSeeInDatabase('foreup_pos_cart_payments', array('cart_id' => $current_last_cart_id));
        $I->dontSeeInDatabase('foreup_pos_cart_customers', array('cart_id' => $current_last_cart_id));

    }
}