<?php

/*
 * To check if the API responses have changed recently turn debug mode on by adding the -d command when running the test
 * format of notice that API response has changed:
 *
 *      Responses are NOT equal
 *      --Expected
 *      ++Actual
 *          ++<new response message>
 *          --<old response message>
 *
 */

class CartCest
{
    var $new_cart_id;

    var $DrPepper;
    var $DrPepper_item_number = null;
    var $DrPepper_item_id;
    var $DrPepper_item_type = 'item';
    var $DrPepper_inventory_level = 200000;
    var $Person_sup;
    var $Supplier;
    var $Supplier_id;
    var $TestItems;

    var $course_id = 6270;

    public function _before(Salesv2apiGuy $I)
    {
        //log user in for each test
        $I->login('jackbarnes', 'jackbarnes');

        //used by tests to make http calls
        $I->setHeader('Content-Type', 'application/json');
        $I->setHeader('Api-key', 'no_limits');

        //Get a new cart number from API
        $cart_id = $I->grabFromDatabase('foreup_pos_cart', 'max(cart_id)');
        $I->sendDELETE('/index.php/api/cart/'.$cart_id);
        $this->new_cart_id = json_decode($I->grabResponse())->cart_id;

        //$this->TestItems = new SalesV2TestItems();

        $this->Person_sup = [
            'first_name' => 'test',
            'last_name' => 'supplier',
            'phone_number' => '8011234567',
            'cell_phone_number' => '8011234567',
            'email' => 'foreup@mailinator.com'
        ];
        $I->InsertAndUpdateOnDuplicate('foreup_people', $this->Person_sup);
        $this->Supplier['person_id'] = $I->grabFromDatabase('foreup_people', 'person_id', $this->Person_sup);

        $this->Supplier = [
            'CID' => 0,
            'course_id' => $this->course_id,
            'person_id' => $I->grabFromDatabase('foreup_people', 'person_id', $this->Person_sup),
            'company_name' => 'Foreup Test Services Inc.',
        ];
        $I->InsertAndUpdateOnDuplicate('foreup_suppliers', $this->Supplier);
        $this->Supplier_id = $this->Supplier['person_id'];

        $this->DrPepper = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'name' => 'Dr. Pepper',
            'department' => 'Food & Beverage',
            'category' => 'Beverage',
            'supplier_id' => $this->Supplier_id,
            'image_id' => 0,
            'cost_price' => 0.50,
            'unit_price' => 2.00,
            'unit_price_includes_tax' => 0,
            'quantity' => $this->DrPepper_inventory_level,
            'is_unlimited' => 1,
            'location' => 0,
            'allow_alt_description' => 0,
            'is_serialized' => 0,
            'is_giftcard' => 0,
            'invisible' => 0,
            'soup_or_salad' => 'none',
            'is_side' => 0,
            'add_on_price' => 0,
            'print_priority' => 1,
            'quickbooks_income' => 0,
            'quickbooks_cogs' => 0,
            'quickbooks_assets' => 0,
            'erange_size' => 0
        ];
        $I->InsertAndUpdateOnDuplicate('foreup_items', $this->DrPepper);
        $this->DrPepper_item_id = $I->grabFromDatabase('foreup_items', 'max(item_id)');

        $db_Sales_Tax = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'item_id' => $this->DrPepper_item_id,
            'name' => 'Sales Tax',
            'percent' => 6.750,
            'cumulative' => 0
        ];
        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', $db_Sales_Tax);

        $db_Sales_Tax_2 = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'item_id' => $this->DrPepper_item_id,
            'name' => 'Sales Tax 2',
            'percent' => 1.050,
            'cumulative' => 0
        ];
        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', $db_Sales_Tax_2);
    }

    public function _after(Salesv2apiGuy $I)
    {}

    /* System currently gives a warning if you enter an invalid price,
     * but it will still allow you to place a sale
     *
    public function invalidItemPrice(Salesv2apiGuy $I)
    {

    }
    */

    /* System currently gives a warning if you enter an invalid discount amount,
     * but it will still allow you to place a sale
     *
    public function invalidItemDiscount(Salesv2apiGuy $I)
    {

    }
    */

    /* The system currently allows an invalid quantity(negative quantity).
     * It treats a negative quantity as a return even though its in Sale mode.
     *
    public function invalidItemQuantity(Salesv2apiGuy $I)
    {

    }
    */


    public function cartCreatedOnLogin(Salesv2apiGuy $I)
    {
        $I->wantTo('confirm that a cart is created on login');

        $I->setHeader('Content-Type', 'text/plain;charset=UTF-8');
        $I->sendPOST('/index.php/home/set_terminal/1');
        $I->amOnPage('/index.php/v2/home#sales');
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $this->new_cart_id));
    }

    public function addItemToCart(Salesv2apiGuy $I)
    {
        $quantity = 1;
        $payload = [
            "item_id" => $this->DrPepper_item_id,
            "item_type" => $this->DrPepper_item_type,
            "quantity" => $quantity,
            "unit_price" => $this->DrPepper['unit_price'],
            "discount_percent" => 0,
            "timeframe_id" => "1",
            "price_class_id" => 1,
            "special_id" => false,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => 0
        ];
        $expectedResponse = [
            "success" => 1
        ];
        $db_pos_cart_items = [
            'cart_id' => $this->new_cart_id,
            'item_id' => $this->DrPepper_item_id
        ];

        $I->wantTo('test PUT request to cart endpoint');

        //add item to cart
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $response = $I->grabResponse();

        $I->checkAPIForSuccess($expectedResponse,$response);

        //make sure that the item is attached to cart in db
        $I->seeInDatabase('foreup_pos_cart_items', $db_pos_cart_items);
    }

    public function deleteItemFromCart(Salesv2apiGuy $I)
    {
        $quantity = 1;
        $payload = [
            "item_id" => $this->DrPepper_item_id,
            "item_type" => $this->DrPepper_item_type,
            "quantity" => $quantity,
            "unit_price" => $this->DrPepper['unit_price'],
            "discount_percent" => 0,
            "timeframe_id" => "1",
            "price_class_id" => 1,
            "special_id" => false,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => 0
        ];
        $expectedResponse = [
            "success" => 1
        ];
        $db_pos_cart_items = [
            'cart_id' => $this->new_cart_id,
            'item_id' => $this->DrPepper_item_id
        ];

        $I->wantTo('test DELETE item request to cart endpoint');

        //add item to the cart
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $response = $I->grabResponse();

        $I->checkAPIForSuccess($expectedResponse,$response);

        //make sure the item shows up in the database
        $I->seeInDatabase('foreup_pos_cart_items', $db_pos_cart_items);

        //remove the item from the cart
        $I->sendDELETE('/index.php/api/cart/'.$this->new_cart_id.'/items/1');

        //make sure the item is no longer attached to the cart in the Db
        $I->dontSeeInDatabase('foreup_pos_cart_items', $db_pos_cart_items);
    }

    public function itemSearch(Salesv2apiGuy $I)
    {
        $expectedResponse = [
            [
                "item_id" => $this->DrPepper_item_id,
                "course_id" => strval($this->course_id),
                "name" => $this->DrPepper['name'],
                "department" => $this->DrPepper['department'],
                "category" => $this->DrPepper['category'],
                "subcategory" => "",
                "supplier_id" => strval($this->DrPepper['supplier_id']),
                "item_number" => null,
                "description" => "",
                "unit_price" => strval($this->DrPepper['unit_price']),
                "base_price" => number_format($this->DrPepper['unit_price'],
                    2),
                "inactive" => "0",
                "do_not_print" => "0",
                "max_discount" => "100.00",
                "inventory_level" => $this->DrPepper_inventory_level,
                "inventory_unlimited" => $this->DrPepper['is_unlimited'],
                "cost_price" => number_format($this->DrPepper['cost_price'],
                    2),
                "is_giftcard" => "0",
                "is_side" => "0",
                "food_and_beverage" => "0",
                "number_sides" => "0",
                "number_salads" => "0",
                "number_soups" => "0",
                "print_priority" => strval($this->DrPepper['print_priority']),
                "is_fee" => "0",
                "erange_size" => "0",
                "meal_course_id" => "0",
                "taxes" => [
                    [
                        "name" => "Sales Tax 2",
                        "percent" => 1.05,
                        "cumulative" => "0"
                    ],
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => "0"
                    ]
                ],
                "is_serialized" => "0",
                "do_not_print_customer_receipt" => "0",
                "is_pass" => "0",
                "pass_days_to_expiration" => null,
                "pass_restrictions" => null,
                "pass_price_class_id" => null,
                "modifiers" => [],
                "sides" => "",
                "printer_groups" => null,
                "item_type" => "item",
                "loyalty_points_per_dollar" => false,
                "loyalty_dollars_per_point" => 0
            ]
        ];

        $I->wantTo('test the item search endpoint');

        //search for an item
        $I->sendGET('/index.php/api/items?q='.$this->DrPepper['name']);
        $response = $I->grabResponse();

        //make sure the item shows up in the api response
        $I->checkAPIForSuccess($expectedResponse,$response);
    }

    public function non_ExistentItemSearch(Salesv2apiGuy $I)
    {
        $I->wantTo('search for a non-existent item');

        //search for a non existent item
        $I->sendGET('/index.php/api/items?q=asdjfljaasfdsfkljasfkl');

        //make sure the api response is empty
        $I->seeResponseEquals('[]');
        $I->seeResponseCodeIs(200);
    }

    public function deleteCart(Salesv2apiGuy $I)
    {
        $I->wantTo('test DELETE cart request to cart endpoint');
        $max_cart_id = $this->new_cart_id - 1;
        $I->sendDELETE('/index.php/api/cart/'.$max_cart_id);
        $I->dontSeeInDatabase('foreup_pos_cart', array('cart_id' => $max_cart_id));
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $this->new_cart_id));
    }

    public function taxableCart(Salesv2apiGuy $I)
    {
        $payload = [
            "taxable" => false
        ];
        $expectedResponse = [
            "success" => 1
        ];
        $db_pos_cart = [
            'cart_id' => $this->new_cart_id,
            'taxable' => 0
        ];

        $I->wantTo('toggle carts taxable option');

        //set taxable off
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        //check api response for success
        $I->seeResponseContainsJson($expectedResponse);

        //make sure the cart shows taxable is off in Db
        $I->seeInDatabase('foreup_pos_cart', $db_pos_cart);

        //turn taxable on
        $payload["taxable"] = true;
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        //check api response for success
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        //make sure the cart shows taxable is on in Db
        $db_pos_cart['taxable'] = 1;
        $I->seeInDatabase('foreup_pos_cart', $db_pos_cart);
    }

    public function itemNotSelected(Salesv2apiGuy $I)
    {
        $quantity = 1;
        $payload = [
            "item_id" => $this->DrPepper_item_id,
            "item_type" => $this->DrPepper_item_type,
            "quantity" => $quantity,
            "unit_price" => $this->DrPepper['unit_price'],
            "discount_percent" => 0,
            "timeframe_id" => "1",
            "price_class_id" => 1,
            "special_id" => false,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => 0
        ];
        $db_pos_cart_items = [
            'cart_id' => $this->new_cart_id,
            'item_id' => $this->DrPepper_item_id,
            'selected' => 1
        ];
        $expectedResponse = [
            "success" => 1
        ];

        $I->wantTo('deselect an item without deleting it');

        //add item to cart with it selected
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //make sure that the item shows selected in the database
        $I->seeInDatabase('foreup_pos_cart_items', $db_pos_cart_items);

        //deselect the item in the cart
        $payload["selected"] =  false;
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $response = $I->grabResponse();


        $I->checkAPIForSuccess($expectedResponse,$response);

        //make sure the database shows the item is not selected
        $db_pos_cart_items['selected'] = 0;
        $I->seeInDatabase('foreup_pos_cart_items', $db_pos_cart_items);
    }

    public function itemPrice(Salesv2apiGuy $I)
    {
        $quantity = 1;
        $price_increase = 1.00;
        $new_price = $this->DrPepper['unit_price'] + $price_increase;
        $payload = [
            "item_id" => $this->DrPepper_item_id,
            "item_type" => $this->DrPepper_item_type,
            "quantity" => $quantity,
            "unit_price" => $this->DrPepper['unit_price'],
            "discount_percent" => 0,
            "timeframe_id" => "1",
            "price_class_id" => 1,
            "special_id" => false,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => 0
        ];
        $db_pos_cart_items = [
            'cart_id' => $this->new_cart_id,
            'item_id' => $this->DrPepper_item_id,
            'unit_price' => $new_price
        ];
        $expectedResponse = [
            "success" => 1
        ];

        $I->wantTo('test PUT request to cart endpoint');

        //add item to cart
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //update the items price
        $payload['unit_price'] = $new_price;
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $response = $I->grabResponse();

        $I->checkAPIForSuccess($expectedResponse,$response);

        $I->seeInDatabase('foreup_pos_cart_items', $db_pos_cart_items);

        $db_pos_cart_items['unit_price'] = $this->DrPepper['unit_price'];
        $I->dontSeeInDatabase('foreup_pos_cart_items', $db_pos_cart_items);
    }



    public function itemQuantity(Salesv2apiGuy $I)
    {
        $original_quantity = 1;
        $new_quantity = 3;
        $payload = [
            "item_id" => $this->DrPepper_item_id,
            "item_type" => $this->DrPepper_item_type,
            "quantity" => $original_quantity,
            "unit_price" => $this->DrPepper['unit_price'],
            "discount_percent" => 0,
            "timeframe_id" => "1",
            "price_class_id" => 1,
            "special_id" => false,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => 0
        ];
        $expectedResponse = [
            "success" => 1
        ];
        $db_pos_cart_items = [
            'cart_id' => $this->new_cart_id,
            'item_id' => $this->DrPepper_item_id,
            'quantity' => $new_quantity
        ];

        $I->wantTo('change the items quantity');

        //add item to cart
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //update the items quantity
        $payload["quantity"] = $new_quantity;
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $response = $I->grabResponse();

        $I->checkAPIForSuccess($expectedResponse,$response);

        //make sure that the database was updated
        $I->seeInDatabase('foreup_pos_cart_items', $db_pos_cart_items);
    }

    public function itemDiscount(Salesv2apiGuy $I)
    {
        $quantity = 1;
        $original_discount_percent = 0;
        $new_discount_percent = 10;
        $payload = [
            "item_id" => $this->DrPepper_item_id,
            "item_type" => $this->DrPepper_item_type,
            "quantity" => $quantity,
            "unit_price" => $this->DrPepper['unit_price'],
            "discount_percent" => $original_discount_percent,
            "timeframe_id" => "1",
            "price_class_id" => 1,
            "special_id" => false,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => 0
        ];
        $expectedResponse = [
            "success" => 1
        ];
        $db_pos_cart_items = [
            'cart_id' => $this->new_cart_id,
            'item_id' => $this->DrPepper_item_id,
            'discount_percent' => $new_discount_percent
        ];

        $I->wantTo('test the item discount option');

        //add item to the cart
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //change the discount on the cart
        $payload["discount_percent"] = $new_discount_percent;
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $response = $I->grabResponse();

        $I->checkAPIForSuccess($expectedResponse,$response);

        //make sure that the discount percent is updated in the database
        $I->seeInDatabase('foreup_pos_cart_items', $db_pos_cart_items);
    }

    public function saleMode(Salesv2apiGuy $I)
    {
        $payload = [
            "mode" => "sale"
        ];
        $expectedResponse = [
            "success" => 1
        ];
        $db_pos_cart = [
            'cart_id' => $this->new_cart_id,
            'mode' => 'sale'
        ];

        $I->wantTo('test that the cart is set to sale mode');

        $I->updateDatabase('foreup_pos_cart', array('cart_id'=> $this->new_cart_id), array('mode' => 'return'));

        //set the cart to sale mode
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        $I->checkAPIForSuccess($expectedResponse,$response);

        //make sure the database was updated
        $I->seeInDatabase('foreup_pos_cart', $db_pos_cart);
    }

    public function returnMode(Salesv2apiGuy $I)
    {
        $payload = [
            "mode" => "return"
        ];
        $expectedResponse = [
            "success" => 1
        ];
        $db_pos_cart = [
            'cart_id' => $this->new_cart_id,
            'mode' => 'return'
        ];

        $I->wantTo('test that the cart is set to return mode');

        $I->updateDatabase('foreup_pos_cart', array('cart_id'=> $this->new_cart_id), array('mode' => 'sale'));

        //set the cart to sale mode
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        $I->checkAPIForSuccess($expectedResponse,$response);

        //make sure the database was updated
        $I->seeInDatabase('foreup_pos_cart', $db_pos_cart);
    }
}