<?php

/*
 * To check if the API responses have changed recently turn debug mode on by adding the -d command when running the test
 * format of notice that API response has changed:
 *
 *      Responses are NOT equal
 *      --Expected
 *      ++Actual
 *          ++<new response message>
 *          --<old response message>
 *
 */


class PaymentsCest
{
    var $orig_last_cart_id;
    var $new_sale_id;
    var $new_number;
    var $new_cart_id;
    var $next_cart_id;
    var $rocketBalls;
    var $rocketBalls_item_number = 885583349767;
    var $rocketBalls_item_id;
    var $rocketBalls_inventory_level = 200000;
    var $customer;
    var $customer_info;
    var $customer_loyalty_points = 20000;
    var $customer_id = 1000000;
    var $punchCard;
    var $punchCard_id;
    var $punchCard_punches = 20;
    var $course_id = 6270;
    var $punchCard_number = 1000000;
    var $punchCard_info;
    var $DrPepper;
    var $DrPepper_item_number = null;
    var $DrPepper_item_id;
    var $DrPepper_inventory_level = 200000;
    var $giftCard;
    var $giftCard_value = 1000;
    var $giftCard_number = 500;
    var $giftCard_id;
    var $giftCard_issue_date = '2015-07-04';
    var $giftCard_expiration_date = '0000-00-00';
    var $rainCheck;
    var $rainCheck_cart_fee = 50.00;
    var $rainCheck_number = 1000000;
    var $rainCheck_id;
    var $rainCheck_issue_date = '2015-07-04';
    var $rainCheck_date_redeemed = '0000-00-00';
    var $GiftCardItem;
    var $GiftCardItem_id;
    var $Cart;
    var $Cart_item_id;
    var $Cart_inventory = 10000;
    var $Cart_total = 53.38;
    var $Cart_Green_Fee_Season = 20;
    var $CustomPayment;
    var $Custom_payment_type = 'test_payment';
    var $Custom_payment_description = 'Test Payment';
    var $ItemKit;
    var $ItemKit_id;
    var $ItemKit_Taxes;
    var $ItemKit_Item1;
    var $ItemKit_Item2;
    var $Person_sup;
    var $Supplier;
    var $Supplier_id;

    //these variables contain the structure needed to send requests to the API
    var $payment = [
        "approved" => "false",
        "card_type" => "",
        "card_number" => "",
        "cardholder_name" => "",
        "auth_amount" => 0,
        "auth_code" => "",
        "description" => "",
        "tran_type" => "",
        "type" => "",
        "record_id" => 0,
        "amount" => "",
        "tip_recipient" => 0,
        "number" => '',
        "params" => null,
        "bypass" => 0,
        "is_auto_gratuity" => 0,
        "verified" => false
    ];
    var $cashPaymentResponse;
    var $posItem;
    var $f_and_bItem;
    var $cashPayment;

//    var $items;
//    var $payments;
    var $interface;

    public function _before(Salesv2apiGuy $I)
    {
        $I->login('jackbarnes', 'jackbarnes');
        $I->setHeader('Content-Type', 'application/json');
        $I->setHeader('Api-key', 'no_limits');

        $this->new_sale_id = $I->grabFromDatabase('foreup_sales', 'max(sale_id)') + 1;
        $this->new_number = $I->grabFromDatabase('foreup_sales', 'max(number)') + 1;

        //Get a new cart number from API
        $cart_id = $I->grabFromDatabase('foreup_pos_cart', 'max(cart_id)');
        $I->sendDELETE('/index.php/api/cart/'.$cart_id);
        $this->new_cart_id = json_decode($I->grabResponse(), true)["cart_id"];
        $this->next_cart_id = $this->new_cart_id +1;





//        $this->items = new \Codeception\Module\ItemsHelper();
//        $this->items->_addItemsToDatabase();
//
//        $this->payments = new \Codeception\Module\PaymentsHelper($this->new_cart_id, $this->items);

        $this->interface = new \Codeception\Module\SalesV2InterfaceHelper($this->new_cart_id, $this->new_sale_id, $this->new_number, $this->course_id);

        $this->rocketBalls_item_id = $this->interface->_getItemID("RocketBalls");



        //set data that will be sent to the api for a cash payment
        $this->cashPayment = $this->payment;
        $this->cashPayment["description"] = "Cash";
        $this->cashPayment["type"] = "cash";
        unset( $this->cashPayment["number"]);

        //set data that will be sent to the api for adding items to the cart
//        $this->posItem = [
//            "item_id" => "",
//            "item_type" => "item",
//            "quantity" => 1,
//            "unit_price" => "",
//            "discount_percent" => 0,
//            "timeframe_id" => null,
//            "special_id" => null,
//            "punch_card_id" => null,
//            "selected" => true,
//            "params" => null,
//            "unit_price_includes_tax" => false,
//            "erange_size" => 0
//        ];
//
        //response expected from the API for cash payments
        $this->cashPaymentResponse = [
            "success" => true,
            "payment_id" => "",
            "description" => "Cash",
            "amount" => "",
            "type" => "cash",
            "record_id" => 0,
            "number" => "",
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "tran_type" => "",
            "approved" => "false",
            "verified" => 0
        ];
//
//
//
//        $this->GiftCardItem = [
//            'course_id' => $this->course_id,
//            'CID' => 0,
//            'name' => 'Gift Card Test',
//            'department' => 'Giftcards',
//            'category' => 'PS Balls',
//            'image_id' => 0,
//            'cost_price' => 0,
//            'unit_price' => 25.00,
//            'unit_price_includes_tax' => 0,
//            'max_discount' => 0,
//            'quantity' => 1,
//            'is_unlimited' => 0,
//            'reorder_level' => 0,
//            'location' => 0,
//            'gl_code' => null,
//            'allow_alt_description' => 0,
//            'is_serialized' => 1,
//            'is_giftcard' => 1,
//            'invisible' => 0,
//            'deleted' => 0,
//            'food_and_beverage' => 0,
//            'soup_or_salad' => 'none',
//            'number_of_sides' => 0,
//            'is_side' => 0,
//            'add_on_price' => 0,
//            'print_priority' => 0,
//            'kitchen_printer' => 1,
//            'quickbooks_income' => 0,
//            'quickbooks_cogs' => 0,
//            'quickbooks_assets' => 0,
//            'inactive' => 0,
//            'erange_size' => 0,
//            'is_fee' => 0,
//            'meal_course_id' => 0,
//            'do_not_print' => 0,
//            'prompt_meal_course' => 0,
//            'do_not_print_customer_receipt' => 0
//        ];
//
//        $this->ItemKit = [
//            'CID' => 0,
//            'course_id' => 6270,
//            'name' => 'Item Kit Test',
//            'category' => 'Item Kits',
//            'unit_price' => 10.00,
//            'cost_price' => 2.00,
//            'is_punch_card' => 0
//        ];
//
//        $this->ItemKit_Taxes = [
//            'CID' => 0,
//            'course_id' => 0,
//            'name' => 'Sales Tax',
//            'percent' => 6.75
//        ];
//
//        $this->rocketBalls = [
//            'course_id' => $this->course_id,
//            'CID' => 0,
//            'name' => 'RocketBalls',
//            'department' => 'Course',
//            'category' => 'PS Balls',
//            'subcategory' => '',
//            'item_number' => $this->rocketBalls_item_number,
//            'description' => '',
//            'image_id' => 0,
//            'cost_price' => 5.00,
//            'unit_price' => 8.00,
//            'unit_price_includes_tax' => 0,
//            'max_discount' => 0,
//            'quantity' => $this->rocketBalls_inventory_level,
//            'is_unlimited' => 0,
//            'reorder_level' => 10,
//            'location' => 0,
//            'gl_code' => null,
//            'allow_alt_description' => 0,
//            'is_serialized' => 0,
//            'is_giftcard' => 0,
//            'invisible' => 0,
//            'deleted' => 0,
//            'food_and_beverage' => 0,
//            'soup_or_salad' => 'none',
//            'number_of_sides' => 0,
//            'is_side' => 0,
//            'add_on_price' => 0,
//            'print_priority' => 1,
//            'kitchen_printer' => 1,
//            'quickbooks_income' => 0,
//            'quickbooks_cogs' => 0,
//            'quickbooks_assets' => 0,
//            'inactive' => 0,
//            'erange_size' => 0,
//            'is_fee' => 0,
//            'meal_course_id' => 0,
//            'do_not_print' => 0,
//            'prompt_meal_course' => 0,
//            'do_not_print_customer_receipt' => 0
//        ];
//
//        $this->customer = [
//            'CID' => 0,
//            'person_id' => $this->customer_id,
//            'course_id' => $this->course_id,
//            'api_id' => 0, 'username' => '',
//            'password' => '',
//            'member' => 0,
//            'price_class' => 1,
//            'image_id' => 0,
//            'account_number' => '10231352102135',
//            'account_balance' => 0.00,
//            'account_balance_allow_negative' => 0,
//            'account_limit' => 0.00,
//            'member_account_balance' => 0.00,
//            'member_account_limit' => 0.00,
//            'invoice_balance' => 0.00,
//            'invoice_email' => '',
//            'use_loyalty' => 1,
//            'loyalty_points' => $this->customer_loyalty_points,
//            'company_name' => '',
//            'discount' => 0.00,
//            'deleted' => 0,
//            'opt_out_email' => 1,
//            'opt_out_text' => 1,
//            'unsubscribe_all' => 0,
//            'course_news_announcements' => 0,
//            'teetime_reminders' => 0,
//            'status_flag' => 3,
//            'require_food_minimum' => 0
//        ];
//
//        $this->customer_info = [
//            'first_name' => 'Tiger',
//            'last_name' => 'Woods',
//            'phone_number' => '',
//            'cell_phone_number'=> 8014567123,
//            'email' => 'woods2@gmail.com',
//            'birthday' => '1980-07-24',
//            'address_1' => '1546 E Flash Drive',
//            'address_2' => '',
//            'city' => 'Lehi',
//            'state' => 'UT',
//            'zip' => '84043',
//            'country' => '',
//            'comments' => '',
//            'person_id' => $this->customer_id,
//            'foreup_news_announcements_unsubscribe' => 0
//        ];
//
//        $this->punchCard = [
//            'course_id' => $this->course_id,
//            'customer_id' => null,
//            'punch_card_number' => $this->punchCard_number,
//            'expiration_date' => '0000-00-00',
//            'issued_date' => '0000-00-00',
//            'deleted' => 0
//        ];
//
//        $this->DrPepper = [
//            'course_id' => $this->course_id,
//            'CID' => 0,
//            'name' => 'Dr. Pepper',
//            'department' => 'Food & Beverage',
//            'category' => 'Beverage',
//            'image_id' => 0,
//            'cost_price' => 0.50,
//            'unit_price' => 2.00,
//            'unit_price_includes_tax' => 0,
//            'quantity' => $this->DrPepper_inventory_level,
//            'is_unlimited' => 0,
//            'location' => 0,
//            'allow_alt_description' => 0,
//            'is_serialized' => 0,
//            'is_giftcard' => 0,
//            'invisible' => 0,
//            'soup_or_salad' => 'none',
//            'is_side' => 0,
//            'add_on_price' => 0,
//            'print_priority' => 1,
//            'quickbooks_income' => 0,
//            'quickbooks_cogs' => 0,
//            'quickbooks_assets' => 0,
//            'erange_size' => 0
//        ];
//
//        $this->giftCard = [
//            'CID' => 0,
//            'course_id' => $this->course_id,
//            'giftcard_number' => $this->giftCard_number,
//            'value' => $this->giftCard_value,
//            'customer_id' => $this->customer_id,
//            'date_issued' => $this->giftCard_issue_date,
//            'expiration_date' => '0000-00-00'
//        ];
//
//        $this->rainCheck = [
//            'raincheck_number' => $this->rainCheck_number,
//            'teesheet_id' => 27,
//            'date_issued' => $this->rainCheck_issue_date,
//            'date_redeemed' => $this->rainCheck_date_redeemed,
//            'players' => 1,
//            'holes_completed' => 0,
//            'customer_id' => 0,
//            'employee_id' => 12,
//            'green_fee' => 0.00,
//            'cart_fee' => $this->rainCheck_cart_fee,
//            'tax' => ($this->Cart_total - $this->rainCheck_cart_fee),
//            'total' => $this->Cart_total,
//            'cart_price_category' => '1_1_'.$this->Cart_Green_Fee_Season
//        ];
//
//        $this->Cart = [
//            'course_id' => $this->course_id,
//            'CID' => 0,
//            'name' => '18 Hole Cart',
//            'category' => 'Carts',
//            'item_number' => $this->course_id.'_seasonal_'.$this->Cart_Green_Fee_Season,
//            'image_id' => 0,
//            'cost_price' => 50.00,
//            'unit_price' => 50.00,
//            'unit_price_includes_tax' => 0,
//            'max_discount' => 10,
//            'quantity' => $this->Cart_inventory,
//            'is_unlimited' => 1,
//            'allow_alt_description' => 0,
//            'is_serialized' => 0,
//            'is_giftcard' => 0,
//            'invisible' => 0,
//            'soup_or_salad' => 'none',
//            'is_side' => 0,
//            'add_on_price' => 0,
//            'print_priority' => 0,
//            'erange_size' => 0
//        ];
//
//        $this->CustomPayment = [
//            'course_id' => $this->course_id,
//            'label' => $this->Custom_payment_description,
//            'deleted' => 0,
//            'custom_payment_type' => $this->Custom_payment_type
//        ];
//
//        $this->Person_sup = [
//            'first_name' => 'test',
//            'last_name' => 'supplier',
//            'phone_number' => '8011234567',
//            'cell_phone_number' => '8011234567',
//            'email' => 'foreup@mailinator.com'
//        ];
//
//        $this->Supplier = [
//            'CID' => 0,
//            'course_id' => $this->course_id,
//            'person_id' => '',
//            'company_name' => 'Foreup Test Services Inc.',
//        ];
//
//        $I->InsertAndUpdateOnDuplicate('foreup_people', $this->Person_sup);
//        $this->Supplier['person_id'] = $I->grabFromDatabase('foreup_people', 'person_id', $this->Person_sup);
//
//        $I->InsertAndUpdateOnDuplicate('foreup_suppliers', $this->Supplier);
//        $this->Supplier_id = $this->Supplier['person_id'];
//
//        $I->InsertAndUpdateOnDuplicate('foreup_custom_payment_types', $this->CustomPayment);
//
//        $I->InsertAndUpdateOnDuplicate('foreup_items', $this->Cart);
//        $this->Cart_item_id = $I->grabFromDatabase('foreup_items', 'item_id', $this->Cart);
//        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', array('course_id' => $this->course_id, 'CID' => 0, 'item_id' => $this->Cart_item_id, 'name' => 'Sales Tax', 'percent' => 6.750, 'cumulative' => 0));
//
//        $I->InsertAndUpdateOnDuplicate('foreup_punch_cards', $this->punchCard);
//        $this->punchCard_id = $I->grabFromDatabase('foreup_punch_cards', 'punch_card_id', $this->punchCard);
//        $this->punchCard_info = array('punch_card_id' => $this->punchCard_id, 'item_id' => $this->Cart_item_id, 'punches' => $this->punchCard_punches, 'used' => 0);
//        $I->InsertAndUpdateOnDuplicate('foreup_punch_card_items', $this->punchCard_info);
//
//        $I->InsertAndUpdateOnDuplicate('foreup_items', $this->rocketBalls);
//        $this->rocketBalls_item_id = $I->grabFromDatabase('foreup_items', 'item_id', $this->rocketBalls);
//        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', array('course_id' => $this->course_id, 'CID' => 0, 'item_id' => $this->rocketBalls_item_id, 'name' => 'Sales Tax', 'percent' => 6.750, 'cumulative' => 0));
//
//        $I->InsertAndUpdateOnDuplicate('foreup_people', $this->customer_info);
//        $I->InsertAndUpdateOnDuplicate('foreup_customers', $this->customer);
//
//        $I->InsertAndUpdateOnDuplicate('foreup_items', $this->DrPepper);
//        $this->DrPepper_item_id = $I->grabFromDatabase('foreup_items', 'item_id', $this->DrPepper);
//        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', array('course_id' => $this->course_id, 'CID' => 0, 'item_id' => $this->DrPepper_item_id, 'name' => 'Sales Tax', 'percent' => 6.750, 'cumulative' => 0));
//        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', array('course_id' => $this->course_id, 'CID' => 0, 'item_id' => $this->DrPepper_item_id, 'name' => 'Sales Tax 2', 'percent' => 1.050, 'cumulative' => 0));
//
//        $I->InsertAndUpdateOnDuplicate('foreup_item_kits', $this->ItemKit);
//        $this->ItemKit_id = $I->grabFromDatabase('foreup_item_kits', 'item_kit_id', $this->ItemKit);
//
//        $this->ItemKit_Taxes['item_kit_id'] = $this->ItemKit_id;
//        $this->ItemKit_Item1 = [
//            'CID' => 0,
//            'item_kit_id' => $this->ItemKit_id,
//            'course_id' => 0,
//            'item_id' => $this->rocketBalls_item_id,
//            'quantity' => 1
//        ];
//        $this->ItemKit_Item2 = [
//            'CID' => 0,
//            'item_kit_id' => $this->ItemKit_id,
//            'course_id' => 0,
//            'item_id' => $this->DrPepper_item_id,
//            'quantity' => 1
//        ];
//
//        $I->InsertAndUpdateOnDuplicate('foreup_item_kits_taxes', $this->ItemKit_Taxes);
//        $I->InsertAndUpdateOnDuplicate('foreup_item_kit_items', $this->ItemKit_Item1);
//        $I->InsertAndUpdateOnDuplicate('foreup_item_kit_items', $this->ItemKit_Item2);
//
//        $I->InsertAndUpdateOnDuplicate('foreup_giftcards', $this->giftCard);
//        $this->giftCard_id = $I->grabFromDatabase('foreup_giftcards', 'giftcard_id', $this->giftCard);
//
//        $I->InsertAndUpdateOnDuplicate('foreup_items', $this->GiftCardItem);
//        $this->GiftCardItem_id = $I->grabFromDatabase('foreup_items', 'item_id', $this->GiftCardItem);
//
//        $I->InsertAndUpdateOnDuplicate('foreup_rainchecks', $this->rainCheck);
//        $this->rainCheck_id = $I->grabFromDatabase('foreup_rainchecks', 'raincheck_id', array('raincheck_number' => $this->rainCheck['raincheck_number'], 'teesheet_id' => $this->rainCheck['teesheet_id']));
    }

    public function _after(Salesv2apiGuy $I)
    {
        $I->deleteFromDatabase('foreup_inventory', array('trans_items' => $this->rocketBalls_item_id));
    }

    // tests
    public function cashPayment(Salesv2apiGuy $I)
    {
        //set up clean up criteria
        $this->orig_last_cart_id = $this->new_cart_id;

        //variable setup
        $item = "Dr. Pepper";
        $quantity = 1;
        $amount = $this->interface->_totalCost($item, $quantity);

        //purpose of test
        $I->wantTo('test the cash payment option');

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));

        //pay for items in cart with cash
        $response = $this->interface->_cashPayment($amount, true);

        //finish the payment by closing the cart and check for success
        $this->interface->_closeCart(array($item => array()), null, true);
    }

    public function checkPaymentNoCheckNum(Salesv2apiGuy $I)
    {
        //variable setup
        $item = "RocketBalls";
        $quantity = 1;
        $amount = $this->interface->_totalCost($item, $quantity);

        //test setup
        $I->wantTo('test the Check payment option with NO check number');

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));

        //pay for items in cart with check and
        //check that the payments endpoint succeeded
        $response = $this->interface->_checkPayment($amount, "", true);

        //finish the payment by closing the cart and check for success
        $this->interface->_closeCart(array($item => array()), null, true);
    }

    public function checkPaymentWithCheckNum(Salesv2apiGuy $I)
    {
        //variable setup
        $item = "RocketBalls";
        $quantity = 1;
        $amount = $this->interface->_totalCost($item, $quantity);
        $check_number = '2';

        //test setup
        $I->wantTo('test the check payment option WITH check number');

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));

        //pay for items in cart with check and
        //check that the payments endpoint succeeded
        $response = $this->interface->_checkPayment($amount, $check_number, true);

        //finish the payment by closing the cart and check for success
        $this->interface->_closeCart(array($item => array()), null, true);
    }

    public function punchCardPayment(Salesv2apiGuy $I)
    {
        //variable setup
        $item = "Dr. Pepper";
        $punches = 20;
        $punchCard = $this->interface->_paymentFactory('punch_card', $item, array('punches' => $punches));
        $quantity = 1;
        $amount = $this->interface->_totalCost($item, $quantity);

        //test setup
        $I->wantTo('test the punch card payment option');

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));
        $punchCard->_setPunchesUsed($quantity);

        //pay for items in cart with punch card and
        //check that the payments endpoint succeeded
        $response = $this->interface->_punchCardPayment($amount, $punchCard, true);

        //finish the payment by closing the cart and check for success
        $this->interface->_closeCart(array(), null, true);

        $db_punch_card_items = [
            'punch_card_id' => $punchCard->_getPunchCardID(),
            'item_id' => $punchCard->_getItemID(),
            'punches' => $punchCard->_getPunches(),
            'used'=> $punchCard->_getPunchesUsed()
        ];
        $I->seeInDatabase('foreup_punch_card_items', $db_punch_card_items);
    }

    public function emptyPunchCardPayment(Salesv2apiGuy $I)
    {
        //variable setup
        $item = "RocketBalls";
        $punches = 20;
        $punchCard = $this->interface->_paymentFactory('punch_card', $item, array('punches' => $punches));
        $quantity = 1;
        $amount = $this->interface->_totalCost($item, $quantity);
        $errorMsg = "No punches left on card";
        //$errorMsg = "Payment failed to complete.";

        //test setup
        $I->wantTo('test an empty punch card');
        $punchCard->_update_punch_card($punches);

        //add item to cart
        //$this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));
        $this->interface->_addItemToCart_API($item, array('quantity' => $quantity));

        //pay for items in cart with punch card and
        //check that the payments endpoint succeeded
        $response = $this->interface->_punchCardPayment($amount, $punchCard);

        //check that the payments endpoint successfully caught the error
        $expectedPaymentResponse = [
            "success"=>false,
            "msg"=> $errorMsg
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse);
        $I->seeResponseCodeIs(400);

        //Check to make sure that the response format hasn't been updated
        //The test should still be successful even if the response has added new key-value pairs.
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse);

        //check that the inventory did not change
        $db_items = [
            'item_id' => $this->interface->_getItemID($item),
            'quantity' => $this->interface->_getInventoryLevel()
        ];
        $I->seeInDatabase('foreup_items', $db_items);
    }

    public function giftCardPayment(Salesv2apiGuy $I)
    {
        $giftCard = $this->interface->_paymentFactory('giftcard');

        //initialize variables
        $item = "Dr. Pepper";
        $quantity = 1;
        $amount = $this->interface->_totalCost($item, $quantity);

        //test setup
        $I->wantTo('Test a gift card payment');

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));

        //pay for items in cart with punch card and
        //check that the payments endpoint succeeded
        $response = $this->interface->_giftcardPayment($amount, $giftCard, true);

        //finish the payment by closing the cart and check for success
        $this->interface->_closeCart(array(), null, true);

        //check that the gift card balance was updated
        $db_giftCards = [
            'giftcard_id' => $giftCard->_getGiftCardID(),
            'giftcard_number' => $giftCard->_getGiftCardNumber(),
            'value' => ($giftCard->_getGiftCardValue() - $amount),
            'date_issued' => $giftCard->_getDateIssued(),
            'expiration_date' => $giftCard->_getExpirationDate()
        ];
        $I->seeInDatabase('foreup_giftcards', $db_giftCards);
    }

    public function giftCardBalanceTooLow(Salesv2apiGuy $I)
    {
        $giftCard = $this->interface->_paymentFactory('giftcard');

        //initialize variables
        $item = "Dr. Pepper";
        $quantity = 1;
        $payedAmount = 2.00;
        $amount = $this->interface->_totalCost($item, $quantity);

        //setup
        $I->wantTo("test a gift card that doesn't have enough cash");
        $giftCard->_setGiftCardValue($payedAmount);

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));

        //pay for items in cart with gift card and
        //check that the payments endpoint succeeded
        $response = $this->interface->_giftcardPayment($amount, $giftCard, true);

        //check that the inventory did not change
        $db_items = [
            'item_id' => $this->interface->_getItemID($item),
            'quantity' => $this->interface->_getInventoryLevel()
        ];
        $I->seeInDatabase('foreup_items', $db_items);
    }

    public function raincheckPayment(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes_completed = 0;
        $raincheck = $this->interface->_paymentFactory('raincheck', null, array('raincheck_number' => 1000000, 'players' => $players, 'holes_completed' => $holes_completed), $this->interface->_getFeeObject());
        $item = "Green Fee";
        $quantity = 1;
        $amount = $this->interface->_totalCost($item, $quantity);

        //test setup
        $I->wantTo('test the raincheck payment option');

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));

        //pay for items in cart with a raincheck and
        //check that the payments endpoint succeeded
        $response = $this->interface->_raincheckPayment($amount, $raincheck, true);

        //finish the payment by closing the cart and check for success
        $this->interface->_closeCart(array(), null, true);

        $db_rainChecks = [
            'raincheck_id' => $raincheck->_getRaincheckID(),
            'raincheck_number' => $raincheck->_getRaincheckNumber(),
            'teesheet_id' => $raincheck->_getTeeSheetID(),
            'date_redeemed' => "0000-00-00 00:00:00"
        ];
        $I->dontSeeInDatabase('foreup_rainchecks', $db_rainChecks);
    }

    public function previouslyRedeemedRaincheck(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes_completed = 0;
        $raincheck = $this->interface->_paymentFactory('raincheck', null, array('raincheck_number' => 1000000, 'players' => $players, 'holes_completed' => $holes_completed), $this->interface->_getFeeObject());
        $item = "Green Fee";
        $quantity = 1;
        $amount = $this->interface->_totalCost($item, $quantity);

        //test setup
        $I->wantTo('test a previously redeemed raincheck as payment');

        //mark raincheck as already redeemed
        $raincheck->_setDateRedeemed();

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));

        //pay for items in cart with a raincheck
        $response = $this->interface->_raincheckPayment($amount, $raincheck);

        //Check for failure
        $expectedPaymentResponse = [
            "success" => false,
            "msg" => "Raincheck has already been redeemed"
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse);
        $I->seeResponseCodeIs(400);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse);
    }

    public function invalidRaincheck(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes_completed = 0;
        $raincheck = $this->interface->_paymentFactory('raincheck', null, array('raincheck_number' => 1000000, 'players' => $players, 'holes_completed' => $holes_completed), $this->interface->_getFeeObject());
        $item = "Green Fee";
        $quantity = 1;
        $amount = $this->interface->_totalCost($item, $quantity);

        //test setup
        $I->wantTo('test an invalid raincheck as payment');

        //make sure the raincheck is invalid
        $raincheck->_invalidateRaincheck();

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));

        //pay for items in cart with a raincheck
        $response = $this->interface->_raincheckPayment($amount, $raincheck);

        //Check for failure
        $expectedPaymentResponse = [
            "success" => false,
            "msg" => "Raincheck not found"
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse);
        $I->seeResponseCodeIs(400);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse);
    }

    public function loyaltyPointsPayment(Salesv2apiGuy $I)
    {
        $customer = new \Codeception\Module\CustomersHelper($this->course_id);
        $customer->_createCustomer();
        //instantiate variables
        $item = "RocketBalls";
        $quantity = 1;
        //tax is not applied to loyalty point purchases
        $amount = $this->interface->_subtotal(array($item));

        //Set up
        $I->wantTo('use loyalty points as a payment');

        //create a loyalty rate
        $loyaltyRate = $this->interface->_paymentFactory('loyalty_points', $item);

        //add customer to sale
        $customer->_addCustomerToCart_API();

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));

        //pay with loyalty points
        $response = $this->interface->_loyaltyPayment($amount, $loyaltyRate, $customer->_getID(), $item, true);

        //finish the payment by closing the cart and check for success
        $this->interface->_closeCart(array(), null, true, $loyaltyRate);

        $db_sales_items = [
            'sale_id' => $this->new_sale_id,
            'item_id' => $this->interface->_getItemID($item),
            'quantity_purchased' => $quantity,
            'subtotal' => $amount,
            'tax' => 0,
            'total' => $amount
        ];
        $I->seeInDatabase('foreup_sales_items', $db_sales_items);
        //check that the loyalty balance was updated
        $db_customers = [
            'person_id' => $customer->_getID(),
            'loyalty_points' => ($customer->_getBeginningLoyaltyPoints() - $loyaltyRate->_getPointsUsed($amount))
        ];
        $I->seeInDatabase('foreup_customers', $db_customers);
    }

    public function noLoyaltyPoints(Salesv2apiGuy $I)
    {
        $customer = new \Codeception\Module\CustomersHelper($this->course_id);
        $customer->_createCustomer();
        //instantiate variables
        $item = "RocketBalls";
        $quantity = 1;
        $points = 0;
        $points_spent = 0;
        //tax is not applied to loyalty point purchases
        $amount = $this->interface->_subtotal(array($item));
        //create a loyalty rate
        $loyaltyRate = $this->interface->_paymentFactory('loyalty_points', $item);
        $customer->_updateLoyaltyPoints($points);
        $payment_type = $loyaltyRate->_getPaymentType();
        $payment_description = 'Loyalty';

        //Set up
        $I->wantTo('test no loyalty points available');

        //add customer to sale
        $customer->_addCustomerToCart_API();

        //add item to cart
        $this->interface->_addItemToCart_DB($item, array('quantity' => $quantity));

        //pay with loyalty points
        $response = $this->interface->_loyaltyPayment($amount, $loyaltyRate, $customer->_getID(), $item);

        //check that the payment failed
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $payment_type));

        $expectedPaymentResponse = $this->interface->_getExpectedResponse($loyaltyRate->_getPaymentDescription());
        $expectedPaymentResponse['number'] = $points_spent;
        $expectedPaymentResponse['amount'] = 0;
        $I->seeResponseContainsJson($expectedPaymentResponse);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse);

        //check that the loyalty balance was NOT updated
        $db_customers = [
            'person_id' => $customer->_getID(),
            'loyalty_points' => $points
        ];
        $I->seeInDatabase('foreup_customers', $db_customers);
    }

    public function notEnoughLoyaltyPoints(Salesv2apiGuy $I)
    {
        //instantiate variables
        $quantity = 1;
        $amount = 8.54;
        $payed = 4;
        $points = 400;
        $points_spent = 400;
        $payment_type = 'loyalty_points';
        $payment_description = 'Loyalty';


        //Set up
        $I->wantTo('test not enough loyalty points available');
        //Make sure that the item being purchased qualifies for loyalty points
        $loyaltyRate = [
            'course_id' => 6270,
            'label' => 'Loyalty Test',
            'type' => 'item',
            'value' => $this->rocketBalls_item_id,
            'points_per_dollar' => 50,
            'dollars_per_point' => 1,
            'tee_time_index' => 3,
            'price_category' => 1
        ];
        $I->haveInDatabase('foreup_loyalty_rates', $loyaltyRate);
        //Change the loyalty points so there aren't enough
        $I->updateDatabase('foreup_customers', array('person_id' => $this->customer_id), array('loyalty_points' => $points));

        //add customer to sale
        $payload = [
            "person_id" => $this->customer_id,
            "selected" => true,
            "taxable" => true,
            "discount" => "0.00"
        ];
        $I->sendPUT('/index.php/api/cart/customers/'.$this->customer_id, json_encode($payload));

        //add items to sale
        $payload = [
            "item_id" => $this->rocketBalls_item_id,
            "item_type" => "item",
            "quantity" => $quantity,
            "unit_price" => "8",
            "discount_percent" => 0,
            "timeframe_id" => null,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //pay with loyalty points
        $payload = [
            "type" => "loyalty_points",
            "record_id" => $this->customer_id,
            "amount" => $amount,
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check payment succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $payment_type));
        $payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        $expectedPaymentResponse = [
            "success" => true,
            "payment_id" => intval($payment_id),
            "description" => $payment_description,
            "amount" => $payed,
            "type" => $payment_type,
            "record_id" => $this->customer_id,
            "number" => $points_spent,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "approved" => false
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse);

        //check that the loyalty balance was NOT updated
        $db_customers = [
            'person_id' => $this->customer_id,
            'loyalty_points' => $points
        ];
        $I->seeInDatabase('foreup_customers', $db_customers);
    }

    public function unqualifiedItemLoyaltyPoints(Salesv2apiGuy $I)
    {
        //instantiate variables
        $quantity = 1;
        $amount = 8.54;
        $payment_type = 'loyalty_points';
        $payment_description = 'Loyalty';

        //Set up
        $I->wantTo("Purchase an item with Loyalty points that doesn't qualify for them.");

        //add customer to sale
        $payload = [
            "person_id" => $this->customer_id,
            "selected" => true,
            "taxable" => true,
            "discount" => "0.00"
        ];
        $I->sendPUT('/index.php/api/cart/customers/'.$this->customer_id, json_encode($payload));

        //add item to sale
        $payload = [
            "item_id" => $this->rocketBalls_item_id,
            "item_type" => "item",
            "quantity" => $quantity,
            "unit_price" => "8",
            "discount_percent" => 0,
            "timeframe_id" => null,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //pay with loyalty points
        $payload = [
            "type" => "loyalty_points",
            "record_id" => $this->customer_id,
            "amount" => $amount,
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check payment succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $payment_type));
        $payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        $expectedPaymentResponse = [
            "success" => true,
            "payment_id" => intval($payment_id),
            "description" => $payment_description,
            "amount" => 0,
            "type" => $payment_type,
            "record_id" => $this->customer_id,
            "number" => 0,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "approved" => false
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse);

        //check that the loyalty balance was NOT updated
        $db_customers = [
            'person_id' => $this->customer_id,
            'loyalty_points' => $this->customer_loyalty_points
        ];
        $I->seeInDatabase('foreup_customers', $db_customers);

        //check that the inventory was not updated
        $db_items = [
            'item_id' => $this->rocketBalls_item_id,
            'quantity' => $this->rocketBalls_inventory_level
        ];
        $I->seeInDatabase('foreup_items', $db_items);

    }

    public function twoItemsWithLoyaltyPoints(Salesv2apiGuy $I)
    {
        //instantiate variables
        $quantity = 1;
        $amount = 10.70;
        $payedAmount = 8.54;
        $points_spent = 854;
        $payment_type = 'loyalty_points';
        $payment_description = 'Loyalty';

        //Set up
        //Purchase two items. One can be purchased with loyalty points
        //the other CAN'T.
        $I->wantTo('Test invalid item and valid item with loyalty points');

        $loyaltyRate = [
            'course_id' => 6270,
            'label' => 'Loyalty Test',
            'type' => 'item',
            'value' => $this->rocketBalls_item_id,
            'points_per_dollar' => 50,
            'dollars_per_point' => 1,
            'tee_time_index' => 3,
            'price_category' => 1
        ];
        $I->haveInDatabase('foreup_loyalty_rates', $loyaltyRate);


        //add customer to sale
        $payload = [
            "person_id" => $this->customer_id,
            "selected" => true,
            "taxable" => true,
            "discount" => "0.00"
        ];
        $I->sendPUT('/index.php/api/cart/customers/'.$this->customer_id, json_encode($payload));

        //add items to sale
        $payload = [
            "item_id" => $this->rocketBalls_item_id,
            "item_type" => "item",
            "quantity" => $quantity,
            "unit_price" => "8",
            "discount_percent" => 0,
            "timeframe_id" => null,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $payload = [
            "item_id" => $this->DrPepper_item_id,
            "item_type" => "item",
            "quantity" => $quantity,
            "unit_price" => "2.00",
            "discount_percent" => 0,
            "timeframe_id" => null,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/2', json_encode($payload));

        //pay with loyalty points
        $payload = [
            "type" => "loyalty_points",
            "record_id" => $this->customer_id,
            "amount" => $amount,
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check payment succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $payment_type));
        $payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));
        $expectedPaymentResponse = [
            "success" => true,
            "payment_id" => intval($payment_id),
            "description" => $payment_description,
            "amount" => $payedAmount,
            "type" => $payment_type,
            "record_id" => $this->customer_id,
            "number" => $points_spent,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "tran_type" => '',
            "approved" => false,
            "verified" => 0
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse);

        //check that the loyalty balance was NOT updated
        $db_customers = [
            'person_id' => $this->customer_id,
            'loyalty_points' => $this->customer_loyalty_points
        ];
        $I->seeInDatabase('foreup_customers', $db_customers);

        //check that the inventory was not updated
        $db_items = [
            'item_id' => $this->rocketBalls_item_id,
            'quantity' => $this->rocketBalls_inventory_level
        ];
        $I->seeInDatabase('foreup_items', $db_items);

    }

    public function splitPaymentGiftCard_Cash(Salesv2apiGuy $I)
    {
        $partial_amount = 25.00;
        $amount = 53.38;
        $subtotal = 50;
        $payment_type = 'gift_card';
        $payment_description = 'Gift Card';
        $gift_card_balance = '25.00';
        $quantity = 1;
        $new_record_id = $this->punchCard_id;

        //setup
        $I->wantTo('Test a split payment with a Gift Card and Cash');
        $I->updateDatabase('foreup_giftcards', array('giftcard_id' => $this->giftCard_id), array('value' => $partial_amount));

        //add item to cart
        $payload = [
            "item_id" => $this->Cart_item_id,
            "item_type" => "cart_fee",
            "quantity" => $quantity,
            "unit_price" => "50",
            "discount_percent" => 0,
            "timeframe_id" => 0,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //pay for items in cart with gift card
        $payload = [
            "type" => $payment_type,
            "record_id" => 0,
            "amount" => $amount,
            "tip_recipient" => 0,
            "number" => $this->giftCard_number,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check that the payments endpoint succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $payment_type));
        $paymentID1 = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id, 'type' => $payment_type));

        $expectedPaymentResponse1 = [
            "success" => true,
            "payment_id" => intval($paymentID1),
            "description" => $payment_description.':'.$this->giftCard_number,
            "amount" => intval($partial_amount), //API is returning an int but $partial_amount is defined as a double
            "type" => $payment_type,
            "record_id" => intval($this->giftCard_id),
            "number" => $this->giftCard_number,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            'tran_type' => '',
            'approved' => false,
            'verified' => 0,
            "gift_card" => [
                "CID" => "0",
                "giftcard_id" => $this->giftCard_id,
                "course_id" => "6270",
                "giftcard_number" => strval($this->giftCard_number),
                "value" => $gift_card_balance,
                "customer_id" => strval($this->customer_id),
                "details" => "",
                "date_issued" => $this->giftCard_issue_date,
                "expiration_date" => $this->giftCard_expiration_date,
                "department" => "",
                "category" => "",
                "deleted" => "0"
            ]
        ];

        $I->seeResponseContainsJson($expectedPaymentResponse1);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse1);

        $payment_type = 'cash';
        $payment_description = 'Cash';
        //pay for items in cart with cash
        $payload = [
            "type" => $payment_type,
            "record_id" => 0,
            "amount" => ($amount - $partial_amount),
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check that the payments endpoint succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $payment_type));
        $paymentID2 = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id, 'type' => $payment_type));

        $expectedPaymentResponse2 = [
            "success" => true,
            "payment_id" => intval($paymentID2),
            "description" => $payment_description,
            "amount" => 28.379999999999999,
            "type" => $payment_type,
            "record_id" => 0,
            "number" => "",
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "tran_type" => '',
            "approved" => false,
            "verified" => 0
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse2);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse2);

        //finish the payment by closing the cart
        $payload = [
            "status" => "complete",
            "customers" => [],
            "items" => [
                [
                    "item_id" => $this->Cart_item_id,
                    "course_id" => "6270",
                    "name" => "18 Hole Cart",
                    "department" => "",
                    "category" => "Carts",
                    "subcategory" => "",
                    "supplier_id" => null,
                    "item_number" => "6270_seasonal_20",
                    "description" => "",
                    "unit_price" => "50",
                    "base_price" => "50.00",
                    "inactive" => "0",
                    "do_not_print" => "0",
                    "max_discount" => "10.00",
                    "inventory_level" => $this->Cart_inventory,
                    "inventory_unlimited" => 1,
                    "cost_price" => "50.00",
                    "is_giftcard" => "0",
                    "is_side" => "0",
                    "food_and_beverage" => "0",
                    "number_sides" => "0",
                    "number_salads" => "0",
                    "number_soups" => "0",
                    "print_priority" => "0",
                    "is_fee" => "0",
                    "erange_size" => "0",
                    "meal_course_id" => "0",
                    "taxes" => [
                        [
                            "name" => "Sales Tax",
                            "percent" => 6.75,
                            "cumulative" => "0",
                            "amount" => 3.38
                        ]
                    ],
                    "is_serialized" => "0",
                    "do_not_print_customer_receipt" => "0",
                    "modifiers" => [],
                    "sides" => "",
                    "printer_groups" => null,
                    "item_type" => "cart_fee",
                    "loyalty_points_per_dollar" => false,
                    "loyalty_dollars_per_point" => 0,
                    "quantity" => 1,
                    "discount_percent" => 0,
                    "selected" => true,
                    "sub_category" => "",
                    "discount" => 0,
                    "subtotal" => 50,
                    "total" => 53.38,
                    "tax" => 3.38,
                    "discount_amount" => 0,
                    "non_discount_subtotal" => 50,
                    "printer_ip" => "",
                    "loyalty_cost" => null,
                    "loyalty_reward" => false,
                    "timeframe_id" => null,
                    "special_id" => null,
                    "giftcard_id" => null,
                    "punch_card_id" => null,
                    "teetime_id" => null,
                    "params" => null,
                    "modifier_total" => 0,
                    "erange_code" => null,
                    "line" => 1,
                    "unit_price_includes_tax" => false,
                    "items" => [],
                    "fees" => [
                        [
                            "price_class_id" => 3,
                            "price_class_name" => "Junior",
                            "timeframe_id" => 12,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 110,
                            "base_price" => 110,
                            "name" => "18 Cart - Junior Sometimes",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 4,
                            "price_class_name" => "Member",
                            "timeframe_id" => 13,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 1110,
                            "base_price" => 1110,
                            "name" => "18 Cart - Member All Times",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 1,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 3,
                            "teesheet_id" => 71,
                            "teesheet_name" => "TESTER",
                            "season_id" => 8,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 4,
                            "teesheet_id" => 67,
                            "teesheet_name" => "Jack Barnes",
                            "season_id" => 9,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 16,
                            "teesheet_id" => 26,
                            "teesheet_name" => "Exec 9 holes",
                            "season_id" => 11,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 40,
                            "base_price" => 40,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 14,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 1,
                            "base_price" => 1,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 2,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "18 Cart - Regular Change label color",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 15,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => false,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 444,
                            "base_price" => 444,
                            "name" => "18 Cart - Regular Not-default",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 2,
                            "price_class_name" => "Senior",
                            "timeframe_id" => 11,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 10,
                            "base_price" => 10,
                            "name" => "18 Cart - Senior Whenever",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 3,
                            "price_class_name" => "Junior",
                            "timeframe_id" => 12,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 110,
                            "base_price" => 110,
                            "name" => "9 Cart - Junior Sometimes",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 4,
                            "price_class_name" => "Member",
                            "timeframe_id" => 13,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 1110,
                            "base_price" => 1110,
                            "name" => "9 Cart - Member All Times",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 14,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 1,
                            "base_price" => 1,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 4,
                            "teesheet_id" => 67,
                            "teesheet_name" => "Jack Barnes",
                            "season_id" => 9,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 1,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 3,
                            "teesheet_id" => 71,
                            "teesheet_name" => "TESTER",
                            "season_id" => 8,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 16,
                            "teesheet_id" => 26,
                            "teesheet_name" => "Exec 9 holes",
                            "season_id" => 11,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 20,
                            "base_price" => 20,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 2,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "9 Cart - Regular Change label color",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 15,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => false,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 222,
                            "base_price" => 222,
                            "name" => "9 Cart - Regular Not-default",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 2,
                            "price_class_name" => "Senior",
                            "timeframe_id" => 11,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 10,
                            "base_price" => 10,
                            "name" => "9 Cart - Senior Whenever",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ]
                    ],
                    "fee_dropdown" => [
                        "c59" => "18 Cart - Junior Sometimes",
                        "c60" => "18 Cart - Member All Times",
                        "c61" => "18 Cart - Regular ",
                        "c62" => "18 Cart - Regular ",
                        "c63" => "18 Cart - Regular ",
                        "c64" => "18 Cart - Regular ",
                        "c65" => "18 Cart - Regular ",
                        "c66" => "18 Cart - Regular Change label color",
                        "c67" => "18 Cart - Regular Not-default",
                        "c68" => "18 Cart - Senior Whenever",
                        "c79" => "9 Cart - Junior Sometimes",
                        "c80" => "9 Cart - Member All Times",
                        "c81" => "9 Cart - Regular ",
                        "c82" => "9 Cart - Regular ",
                        "c83" => "9 Cart - Regular ",
                        "c84" => "9 Cart - Regular ",
                        "c85" => "9 Cart - Regular ",
                        "c86" => "9 Cart - Regular Change label color",
                        "c87" => "9 Cart - Regular Not-default",
                        "c88" => "9 Cart - Senior Whenever"
                    ],
                    "customer_initials" => false,
                    "color" => false,
                    "selected_fee" => false,
                    "success" => 1
                ]
            ],
            "payments" => [
                [
                    "amount" => $partial_amount,
                    "number" => $this->giftCard_number,
                    "type" => "gift_card",
                    "description" => 'Gift Card:'.$this->giftCard_number,
                    "record_id" => $new_record_id,
                    "invoice_id" => 0,
                    "tip_recipient" => 0,
                    "params" => "",
                    "bypass" => false,
                    "success" => true,
                    "payment_id" => $paymentID1,
                    "gift_card" => [
                        "CID" => "0",
                        "giftcard_id" => $this->giftCard_id,
                        "course_id" => $this->course_id,
                        "giftcard_number" => $this->giftCard_number,
                        "value" => doubleval($partial_amount),
                        "customer_id" => $this->customer_id,
                        "details" => "",
                        "date_issued" => "2015-07-04",
                        "expiration_date" => "0000-00-00",
                        "department" => "",
                        "category" => "",
                        "deleted" => "0"
                    ]
                ],
                [
                    "amount" => ($amount - $partial_amount),
                    "type" => "cash",
                    "description" => "Cash",
                    "record_id" => 0,
                    "invoice_id" => 0,
                    "tip_recipient" => 0,
                    "params" => "",
                    "bypass" => false,
                    "success" => true,
                    "payment_id" => $paymentID2,
                    "number" => ""
                ]
            ],
            "total" => $amount,
            "subtotal" => 50,
            "tax" => 3.38,
            "num_items" => $quantity
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        //Check that the cart successfully closed
        $expectedCartResponse = [
            "sale_id" => $this->new_sale_id,
            "number" => $this->new_number,
            "loyalty_points_spent" => 0,
            "cart_id" => $this->next_cart_id,
            "success" => true
        ];
        $I->seeResponseContainsJson($expectedCartResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedCartResponse);

        //check that the sales tables are updated
        $db_sales = [
            'sale_id' => $this->new_sale_id,
            'number' => $this->new_number,
            'payment_type' => 'Gift Card:'.$this->giftCard_number.': $'.$partial_amount.'.00<br />Cash: $'.($amount-$partial_amount).'<br />'
        ];
        $I->seeInDatabase('foreup_sales', $db_sales);
        $db_sales_items = [
            'sale_id' => $this->new_sale_id,
            'item_id' => $this->Cart_item_id,
            'quantity_purchased' => $quantity,
            'subtotal' => $subtotal,
            'tax' => ($amount - $subtotal),
            'total' => $amount
        ];
        $I->seeInDatabase('foreup_sales_items', $db_sales_items);
    }

    public function splitPaymentLoyaltyPoints_Cash(Salesv2apiGuy $I)
    {
        //instantiate variables
        $quantity = 1;
        $amount = 8.54;
        $subtotal = 8;
        $lp_payed = 4;
        $points = 400;
        $points_spent = 400;
        $lp_payment_type = 'loyalty_points';
        $lp_payment_description = 'Loyalty';
        $cash_payment_type = 'cash';
        $cash_payment_description = 'Cash';


        //setup
        $I->wantTo('Test a split payment with Loyalty Points and Cash');
        //Make sure that the item being purchased qualifies for loyalty points
        $loyaltyRate = [
            'course_id' => 6270,
            'label' => 'Loyalty Test',
            'type' => 'item',
            'value' => $this->rocketBalls_item_id,
            'points_per_dollar' => 50,
            'dollars_per_point' => 1,
            'tee_time_index' => 3,
            'price_category' => 1
        ];
        $I->haveInDatabase('foreup_loyalty_rates', $loyaltyRate);
        //Change the loyalty points so there aren't enough
        $I->updateDatabase('foreup_customers', array('person_id' => $this->customer_id), array('loyalty_points' => $points));

        //add customer to sale
        $payload = [
            "person_id" => $this->customer_id,
            "selected" => true,
            "taxable" => true,
            "discount" => "0.00"
        ];
        $I->sendPUT('/index.php/api/cart/customers/'.$this->customer_id, json_encode($payload));

        //add items to sale
        $payload = [
            "item_id" => $this->rocketBalls_item_id,
            "item_type" => "item",
            "quantity" => $quantity,
            "unit_price" => "8",
            "discount_percent" => 0,
            "timeframe_id" => null,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //pay with loyalty points
        $payload = [
            "type" => "loyalty_points",
            "record_id" => $this->customer_id,
            "amount" => $amount,
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check that payment succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $lp_payment_type));
        $lp_payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        $expectedPaymentResponse1 = [
            "success" => true,
            "payment_id" => intval($lp_payment_id),
            "description" => $lp_payment_description,
            "amount" => $lp_payed,
            "type" => $lp_payment_type,
            "record_id" => $this->customer_id,
            "number" => $points_spent,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "tran_type" => '',
            "approved" => false,
            "verified" => 0
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse1);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse1);

        //pay remaining balance with cash
        $payload = [
            "type" => $cash_payment_type,
            "record_id" => 0,
            "amount" => ($amount - $lp_payed),
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check payment succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $cash_payment_type));
        $cash_payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id, 'type' => $cash_payment_type));

        $expectedPaymentResponse2 = [
            "success" => true,
            "payment_id" => intval($cash_payment_id),
            "description" => $cash_payment_description,
            "amount" => round(($amount - $lp_payed), 2),
            "type" => $cash_payment_type,
            "record_id" => 0,
            "number" => "",
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "tran_type" => '',
            "approved" => false,
            "verified" => 0
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse2);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse2);

        //finish by closing the cart
        $payload = [
            "status" => "complete",
            "customers" => [
                [
                    "person_id" => $this->customer_id,
                    "taxable" => true,
                    "discount" => "0.00",
                    "first_name" => "Tiger",
                    "last_name" => "Woods",
                    "email" => "woods2@gmail.com",
                    "phone_number" => "",
                    "cell_phone_number" => "8014567123",
                    "birthday" => "1980-07-24",
                    "address_1" => "1546 E Flash Drive",
                    "address_2" => "",
                    "city" => "Lehi",
                    "state" => "UT",
                    "zip" => "84043",
                    "country" => "",
                    "comments" => "",
                    "image_id" => "0",
                    "account_number" => "10231352102135",
                    "account_balance" => "0.00",
                    "account_balance_allow_negative" => "0",
                    "member_account_balance" => "0.00",
                    "account_limit" => "0.00",
                    "member_account_limit" => "0.00",
                    "member_account_balance_allow_negative" => "1",
                    "invoice_balance" => "0.00",
                    "status_flag" => "3",
                    "invoice_balance_allow_negative" => "0",
                    "use_loyalty" => "1",
                    "loyalty_points" => $this->customer_loyalty_points,
                    "price_class" => "1",
                    "member" => "0",
                    "require_food_minimum" => "0",
                    "username" => null,
                    "opt_out_text" => "1",
                    "opt_out_email" => "1",
                    "unsubscribe_all" => "0",
                    "texting_status" => null,
                    "course_id" => $this->course_id,
                    "photo" => "http => //127.0.0.1 => 8080/images/profiles/profile_picture.png",
                    "passes" => [],
                    "groups" => [],
                    "selected" => true,
                    "member_account_label" => "Making a nickname",
                    "customer_account_label" => "Pro Shop",
                    "success" => $this->customer_id
                ]
            ],
            "items" => [
                [
                    "item_id" => $this->rocketBalls_item_id,
                    "course_id" => $this->course_id,
                    "name" => "RocketBalls",
                    "department" => "Course",
                    "category" => "PS Balls",
                    "subcategory" => "",
                    "supplier_id" => "532",
                    "item_number" => "885583349767",
                    "description" => "",
                    "unit_price" => "8",
                    "base_price" => "8.00",
                    "inactive" => "0",
                    "do_not_print" => "0",
                    "max_discount" => "0.00",
                    "inventory_level" => $this->rocketBalls_inventory_level,
                    "inventory_unlimited" => 1,
                    "cost_price" => "5.00",
                    "is_giftcard" => "0",
                    "is_side" => "0",
                    "food_and_beverage" => "0",
                    "number_sides" => "0",
                    "number_salads" => "0",
                    "number_soups" => "0",
                    "print_priority" => "1",
                    "is_fee" => "0",
                    "erange_size" => "0",
                    "meal_course_id" => "0",
                    "taxes" => [
                        [
                            "name" => "Sales Tax",
                            "percent" => 6.75,
                            "cumulative" => "0",
                            "amount" => 0.54
                        ]
                    ],
                    "is_serialized" => "0",
                    "do_not_print_customer_receipt" => "0",
                    "modifiers" => [],
                    "sides" => "",
                    "printer_groups" => null,
                    "item_type" => "item",
                    "loyalty_points_per_dollar" => 50,
                    "loyalty_dollars_per_point" => 0.01,
                    "quantity" => 1,
                    "discount_percent" => 0,
                    "selected" => true,
                    "sub_category" => "",
                    "discount" => 0,
                    "subtotal" => 8,
                    "total" => $amount,
                    "tax" => 0.54,
                    "discount_amount" => 0,
                    "non_discount_subtotal" => 8,
                    "printer_ip" => "",
                    "loyalty_cost" => 800,
                    "loyalty_reward" => $points,
                    "timeframe_id" => null,
                    "special_id" => null,
                    "giftcard_id" => null,
                    "punch_card_id" => null,
                    "teetime_id" => null,
                    "params" => null,
                    "modifier_total" => 0,
                    "erange_code" => null,
                    "line" => 1,
                    "unit_price_includes_tax" => false,
                    "items" => [],
                    "fee_dropdown" => [],
                    "success" => 1
                ]
            ],
            "payments" => [
                [
                    "amount" => $lp_payed,
                    "type" => $lp_payment_type,
                    "description" => $lp_payment_description,
                    "record_id" => $this->customer_id,
                    "invoice_id" => 0,
                    "tip_recipient" => 0,
                    "params" => "",
                    "bypass" => false,
                    "success" => true,
                    "payment_id" => $lp_payment_id,
                    "number" => $points_spent
                ],
                [
                    "amount" => ($amount - $lp_payed),
                    "type" => $cash_payment_type,
                    "description" => $cash_payment_description,
                    "record_id" => 0,
                    "invoice_id" => 0,
                    "tip_recipient" => 0,
                    "params" => "",
                    "bypass" => false,
                    "success" => true,
                    "payment_id" => $cash_payment_id,
                    "number" => ""
                ]
            ],
            "total" => $amount,
            "subtotal" => 8,
            "tax" => 0.54,
            "num_items" => $quantity
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        //Check that the cart successfully closed
        $expectedCartResponse = [
            "sale_id" => $this->new_sale_id,
            "number" => $this->new_number,
            "loyalty_points_spent" => $points_spent,
            "loyalty_points_earned" => 212,
            "cart_id" => $this->next_cart_id,
            "success" => true
        ];
        $I->seeResponseContainsJson($expectedCartResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedCartResponse);

        //check that the sales tables are updated
        $db_sales = [
            'sale_id' => $this->new_sale_id,
            'number' => $this->new_number,
            'payment_type' => 'Loyalty: $'.$lp_payed.'.00<br />Cash: $'.($amount-$lp_payed).'<br />'
        ];
        $I->seeInDatabase('foreup_sales', $db_sales);
        $db_sales_items = [
            'sale_id' => $this->new_sale_id,
            'item_id' => $this->rocketBalls_item_id,
            'quantity_purchased' => $quantity,
            'subtotal' => $subtotal,
            'tax' => ($amount - $subtotal),
            'total' => $amount
        ];
        $I->seeInDatabase('foreup_sales_items', $db_sales_items);
    }

    public function splitPaymentRaincheck_Cash(Salesv2apiGuy $I)
    {
        $amount = 53.38;
        $rc_payed = 43.38;
        $rc_subtotal = 40;
        $quantity = 1;
        $rc_payment_type = 'raincheck';
        $rc_payment_description = 'Raincheck';
        $cash_payment_type = 'cash';
        $cash_payment_description = 'Cash';
        $subtotal = 50;

        //test setup
        $I->wantTo('Test a split payment with a Raincheck and Cash');
        $I->updateDatabase('foreup_rainchecks', array('raincheck_id' => $this->rainCheck_id), array('total' => $rc_payed, 'cart_fee' => $rc_subtotal));

        //add item to cart
        $payload = [
            "item_id" => $this->Cart_item_id,
            "item_type" => "cart_fee",
            "quantity" => $quantity,
            "unit_price" => $subtotal,
            "discount_percent" => 0,
            "timeframe_id" => 1,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //pay for item with raincheck
        $payload = [
            "type" => $rc_payment_type,
            "record_id" => 0,
            "amount" => $amount,
            "tip_recipient" => 0,
            "number" => $this->rainCheck_number,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //Check for success
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $rc_payment_type));
        $rc_payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        $expectedPaymentResponse1 = [
            "success" => true,
            "payment_id" => intval($rc_payment_id),
            "description" => $rc_payment_description.' '.$this->rainCheck_number,
            "amount" => $rc_payed,
            "type" => $rc_payment_type,
            "record_id" => intval($this->rainCheck_id),
            "number" => $this->rainCheck_number,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "tran_type" => '',
            "approved" => false,
            "verified" => 0,
            "raincheck" => [
                "raincheck_id" => $this->rainCheck_id,
                "raincheck_number" => strval($this->rainCheck_number),
                "teesheet_id" => "27",
                "teetime_id" => "",
                "teetime_date" => "0000-00-00",
                "teetime_time" => "0",
                "date_issued" => $this->rainCheck_issue_date." 00:00:00",
                "date_redeemed" => "0000-00-00 00:00:00",
                "players" => "1",
                "holes_completed" => "0",
                "customer_id" => "0",
                "employee_id" => "12",
                "green_fee" => false,
                "cart_fee" => [
                    "unit_price" => $rc_subtotal,
                    "price_class_id" => 1,
                    "timeframe_id" => 1,
                    "item_number" => 20,
                    "item_id" => intval($this->Cart_item_id)
                ],
                "tax" => 3.38,
                "total" => $rc_payed,
                "expiry_date" => "0000-00-00 00:00:00",
                "deleted" => "0",
                "subtotal" => $rc_subtotal
            ]
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse1);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse1);

        //pay remaining balance with cash
        $payload = [
            "type" => $cash_payment_type,
            "record_id" => 0,
            "amount" => ($amount - $rc_payed),
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check payment succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $cash_payment_type));
        $cash_payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id, 'type' => $cash_payment_type));

        $expectedPaymentResponse2 = [
            "success" => true,
            "payment_id" => intval($cash_payment_id),
            "description" => $cash_payment_description,
            "amount" => intval(($amount - $rc_payed)), //subtraction returns a double API response contains an int
            "type" => $cash_payment_type,
            "record_id" => 0,
            "number" => "",
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "tran_type" => '',
            "approved" => false,
            "verified" => 0
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse2);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse2);

        //finish by closing the cart
        $payload = [
            "status" => "complete",
            "customers" => [],
            "items" => [
                [
                    "item_id" => $this->Cart_item_id,
                    "course_id" => $this->course_id,
                    "name" => "18 Hole Cart",
                    "department" => "",
                    "category" => "Carts",
                    "subcategory" => "",
                    "supplier_id" => null,
                    "item_number" => $this->course_id.'_seasonal_'.$this->Cart_Green_Fee_Season,
                    "description" => "",
                    "unit_price" => $subtotal,
                    "base_price" => "50.00",
                    "inactive" => "0",
                    "do_not_print" => "0",
                    "max_discount" => "10.00",
                    "inventory_level" => $this->Cart_inventory,
                    "inventory_unlimited" => 1,
                    "cost_price" => "50.00",
                    "is_giftcard" => "0",
                    "is_side" => "0",
                    "food_and_beverage" => "0",
                    "number_sides" => "0",
                    "number_salads" => "0",
                    "number_soups" => "0",
                    "print_priority" => "0",
                    "is_fee" => "0",
                    "erange_size" => "0",
                    "meal_course_id" => "0",
                    "taxes" => [
                        [
                            "name" => "Sales Tax",
                            "percent" => 6.75,
                            "cumulative" => "0",
                            "amount" => 3.38
                        ]
                    ],
                    "is_serialized" => "0",
                    "do_not_print_customer_receipt" => "0",
                    "is_pass" => "0",
                    "pass_days_to_expiration" => null,
                    "pass_restrictions" => null,
                    "pass_price_class_id" => null,
                    "modifiers" => [],
                    "sides" => "",
                    "printer_groups" => null,
                    "item_type" => "cart_fee",
                    "loyalty_points_per_dollar" => false,
                    "loyalty_dollars_per_point" => 0,
                    "quantity" => 1,
                    "discount_percent" => 0,
                    "selected" => true,
                    "params" => [],
                    "sub_category" => "",
                    "discount" => 0,
                    "subtotal" => $subtotal,
                    "total" => $amount,
                    "tax" => 3.38,
                    "discount_amount" => 0,
                    "non_discount_subtotal" => 50,
                    "printer_ip" => "",
                    "loyalty_cost" => null,
                    "loyalty_reward" => false,
                    "timeframe_id" => null,
                    "special_id" => null,
                    "giftcard_id" => null,
                    "punch_card_id" => null,
                    "teetime_id" => null,
                    "modifier_total" => 0,
                    "erange_code" => null,
                    "line" => 1,
                    "unit_price_includes_tax" => false,
                    "items" => [],
                    "fees" => [
                        [
                            "price_class_id" => 3,
                            "price_class_name" => "Junior",
                            "timeframe_id" => 12,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 110,
                            "base_price" => 110,
                            "name" => "18 Cart - Junior Sometimes",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 4,
                            "price_class_name" => "Member",
                            "timeframe_id" => 13,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 1110,
                            "base_price" => 1110,
                            "name" => "18 Cart - Member All Times",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 1,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 16,
                            "teesheet_id" => 26,
                            "teesheet_name" => "Exec 9 holes",
                            "season_id" => 11,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 40,
                            "base_price" => 40,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 4,
                            "teesheet_id" => 67,
                            "teesheet_name" => "Jack Barnes",
                            "season_id" => 9,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 3,
                            "teesheet_id" => 71,
                            "teesheet_name" => "TESTER",
                            "season_id" => 8,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 14,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 1,
                            "base_price" => 1,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 2,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "18 Cart - Regular Change label color",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 15,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 444,
                            "base_price" => 444,
                            "name" => "18 Cart - Regular Not-default",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 2,
                            "price_class_name" => "Senior",
                            "timeframe_id" => 11,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 10,
                            "base_price" => 10,
                            "name" => "18 Cart - Senior Whenever",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 3,
                            "price_class_name" => "Junior",
                            "timeframe_id" => 12,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 110,
                            "base_price" => 110,
                            "name" => "9 Cart - Junior Sometimes",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 4,
                            "price_class_name" => "Member",
                            "timeframe_id" => 13,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 1110,
                            "base_price" => 1110,
                            "name" => "9 Cart - Member All Times",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 14,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 1,
                            "base_price" => 1,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 1,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 16,
                            "teesheet_id" => 26,
                            "teesheet_name" => "Exec 9 holes",
                            "season_id" => 11,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 20,
                            "base_price" => 20,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 4,
                            "teesheet_id" => 67,
                            "teesheet_name" => "Jack Barnes",
                            "season_id" => 9,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 3,
                            "teesheet_id" => 71,
                            "teesheet_name" => "TESTER",
                            "season_id" => 8,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 2,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "9 Cart - Regular Change label color",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 15,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 222,
                            "base_price" => 222,
                            "name" => "9 Cart - Regular Not-default",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 2,
                            "price_class_name" => "Senior",
                            "timeframe_id" => 11,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 10,
                            "base_price" => 10,
                            "name" => "9 Cart - Senior Whenever",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ]
                    ],
                    "success" => 1
                ]
            ],
            "payments" => [
                [
                    "amount" => $rc_payed,
                    "number" => $this->rainCheck_number,
                    "type" => $rc_payment_type,
                    "description" => $rc_payment_description.':'.$this->rainCheck_number,
                    "record_id" => $this->rainCheck_id,
                    "approved" => "false",
                    "invoice_id" => 0,
                    "card_type" => "",
                    "card_number" => "",
                    "cardholder_name" => "",
                    "auth_amount" => 0,
                    "auth_code" => "",
                    "tip_recipient" => 0,
                    "tran_type" => "",
                    "params" => "",
                    "bypass" => false,
                    "is_auto_gratuity" => 0,
                    "success" => true,
                    "payment_id" => $rc_payment_id,
                    "merchant_data" => false,
                    "verified" => 0,
                    "raincheck" => [
                        "raincheck_id" => $this->rainCheck_id,
                        "raincheck_number" => $this->rainCheck_number,
                        "teesheet_id" => "27",
                        "teetime_id" => "",
                        "teetime_date" => "0000-00-00",
                        "teetime_time" => "0",
                        "date_issued" => $this->rainCheck_issue_date,
                        "date_redeemed" => $this->rainCheck_date_redeemed,
                        "players" => "1",
                        "holes_completed" => "0",
                        "customer_id" => "0",
                        "employee_id" => "12",
                        "green_fee" => false,
                        "cart_fee" => [
                            "unit_price" => $rc_subtotal,
                            "price_class_id" => 1,
                            "timeframe_id" => 1,
                            "item_number" => 20,
                            "item_id" => $this->Cart_item_id
                        ],
                        "tax" => 3.38,
                        "total" => $rc_payed,
                        "expiry_date" => "0000-00-00 00:00:00",
                        "deleted" => "0",
                        "subtotal" => $rc_subtotal
                    ]
                ],
                [
                    "amount" => ($amount - $rc_payed),
                    "type" => $cash_payment_type,
                    "description" => $cash_payment_description,
                    "record_id" => 0,
                    "approved" => "false",
                    "invoice_id" => 0,
                    "card_type" => "",
                    "card_number" => "",
                    "cardholder_name" => "",
                    "auth_amount" => 0,
                    "auth_code" => "",
                    "tip_recipient" => 0,
                    "tran_type" => "",
                    "params" => "",
                    "bypass" => false,
                    "is_auto_gratuity" => 0,
                    "success" => true,
                    "payment_id" => $cash_payment_id,
                    "number" => "",
                    "merchant_data" => false,
                    "verified" => 0
                ]
            ],
            "total" => $amount,
            "subtotal" => $subtotal,
            "tax" => 3.38,
            "num_items" => $quantity
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();



        //Check that the cart successfully closed
        $expectedCartResponse = [
            "sale_id" => $this->new_sale_id,
            "number" => $this->new_number,
            "loyalty_points_spent" => 0,
            "cart_id" => $this->next_cart_id,
            "success" => true
        ];
        $I->seeResponseContainsJson($expectedCartResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedCartResponse);

        //check that the sales tables are updated
        $I->seeInDatabase('foreup_sales', array('sale_id' => $this->new_sale_id, 'number' => $this->new_number, 'payment_type' => ''.$rc_payment_description.' '.$this->rainCheck_number.': $'.$rc_payed.'<br />Cash: $'.($amount-$rc_payed).'.00<br />'));
        $I->seeInDatabase('foreup_sales_items', array('sale_id' => $this->new_sale_id, 'item_id' => $this->Cart_item_id, 'quantity_purchased' => $quantity, 'subtotal' => $subtotal, 'tax' => ($amount - $subtotal), 'total' => $amount));

    }

    public function splitPaymentGiftCard_Raincheck(Salesv2apiGuy $I)
    {
        $amount = 53.38;
        $rc_payed = 43.38;
        $rc_subtotal = 40;
        $quantity = 1;
        $rc_payment_type = 'raincheck';
        $rc_payment_description = 'Raincheck';
        $gc_payment_type = 'gift_card';
        $gc_payment_description = 'Gift Card';
        $gift_card_balance = '1000.00';
        $subtotal = 50;

        //test setup
        $I->wantTo('Test a split payment with a Gift Card and a Raincheck');
        $I->updateDatabase('foreup_rainchecks', array('raincheck_id' => $this->rainCheck_id), array('total' => $rc_payed, 'cart_fee' => $rc_subtotal));

        //add item to cart
        $payload = [
            "item_id" => $this->Cart_item_id,
            "item_type" => "cart_fee",
            "quantity" => $quantity,
            "unit_price" => $subtotal,
            "discount_percent" => 0,
            "timeframe_id" => 1,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $I->seeResponseCodeIs(200);

        //pay for item with raincheck
        $payload = [
            "type" => $rc_payment_type,
            "record_id" => 0,
            "amount" => $amount,
            "tip_recipient" => 0,
            "number" => $this->rainCheck_number,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //Check for success
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $rc_payment_type));
        $rc_payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        $expectedPaymentResponse1 = [
            "success" => true,
            "payment_id" => intval($rc_payment_id),
            "description" => $rc_payment_description.' '.$this->rainCheck_number,
            "amount" => $rc_payed,
            "type" => $rc_payment_type,
            "record_id" => intval($this->rainCheck_id),
            "number" => $this->rainCheck_number,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "tran_type" => '',
            "approved" => false,
            "verified" => 0,
            "raincheck" => [
                "raincheck_id" => $this->rainCheck_id,
                "raincheck_number" => strval($this->rainCheck_number),
                "teesheet_id" => "27",
                "teetime_id" => "",
                "teetime_date" => "0000-00-00",
                "teetime_time" => "0",
                "date_issued" => $this->rainCheck_issue_date.' 00:00:00',
                "date_redeemed" => "0000-00-00 00:00:00",
                "players" => "1",
                "holes_completed" => "0",
                "customer_id" => "0",
                "employee_id" => "12",
                "green_fee" => false,
                "cart_fee" => [
                    "unit_price" => $rc_subtotal,
                    "price_class_id" => 1,
                    "timeframe_id" => 1,
                    "item_number" => 20,
                    "item_id" => intval($this->Cart_item_id)
                ],
                "tax" => 3.38,
                "total" => $rc_payed,
                "expiry_date" => "0000-00-00 00:00:00",
                "deleted" => "0",
                "subtotal" => $rc_subtotal
            ]
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse1);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse1);

        //pay remaining balance with gift card
        $payload = [
            "type" => $gc_payment_type,
            "record_id" => 0,
            "amount" => ($amount - $rc_payed),
            "tip_recipient" => 0,
            "number" => $this->giftCard_number,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check that the payments endpoint succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $gc_payment_type));
        $gc_payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id, 'type' => $gc_payment_type));

        $expectedPaymentResponse2 = [
            "success" => true,
            "payment_id" => intval($gc_payment_id),
            "description" => $gc_payment_description.':'.$this->giftCard_number,
            "amount" => intval(($amount - $rc_payed)),
            "type" => $gc_payment_type,
            "record_id" => intval($this->giftCard_id),
            "number" => $this->giftCard_number,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "tran_type" => '',
            "approved" => false,
            "verified" => 0,
            "gift_card" => [
                "CID" => "0",
                "giftcard_id" => $this->giftCard_id,
                "course_id" => "6270",
                "giftcard_number" => strval($this->giftCard_number),
                "value" => $gift_card_balance,
                "customer_id" => strval($this->customer_id),
                "details" => "",
                "date_issued" => $this->giftCard_issue_date,
                "expiration_date" => $this->giftCard_expiration_date,
                "department" => "",
                "category" => "",
                "deleted" => "0"
            ],
            "status" => false,
            "msg" => "Giftcard balance is $1,000.00"
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse2);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse2);

        //finish by closing the cart
        $payload = [
            "status" => "complete",
            "customers" => [],
            "items" => [
                [
                    "item_id" => $this->Cart_item_id,
                    "course_id" => $this->course_id,
                    "name" => "18 Hole Cart",
                    "department" => "",
                    "category" => "Carts",
                    "subcategory" => "",
                    "supplier_id" => null,
                    "item_number" => $this->course_id.'_seasonal_'.$this->Cart_Green_Fee_Season,
                    "description" => "",
                    "unit_price" => $subtotal,
                    "base_price" => "50.00",
                    "inactive" => "0",
                    "do_not_print" => "0",
                    "max_discount" => "10.00",
                    "inventory_level" => $this->Cart_inventory,
                    "inventory_unlimited" => 1,
                    "cost_price" => "50.00",
                    "is_giftcard" => "0",
                    "is_side" => "0",
                    "food_and_beverage" => "0",
                    "number_sides" => "0",
                    "number_salads" => "0",
                    "number_soups" => "0",
                    "print_priority" => "0",
                    "is_fee" => "0",
                    "erange_size" => "0",
                    "meal_course_id" => "0",
                    "taxes" => [
                        [
                            "name" => "Sales Tax",
                            "percent" => 6.75,
                            "cumulative" => "0",
                            "amount" => 3.38
                        ]
                    ],
                    "is_serialized" => "0",
                    "do_not_print_customer_receipt" => "0",
                    "is_pass" => "0",
                    "pass_days_to_expiration" => null,
                    "pass_restrictions" => null,
                    "pass_price_class_id" => null,
                    "modifiers" => [],
                    "sides" => "",
                    "printer_groups" => null,
                    "item_type" => "cart_fee",
                    "loyalty_points_per_dollar" => false,
                    "loyalty_dollars_per_point" => 0,
                    "quantity" => 1,
                    "discount_percent" => 0,
                    "selected" => true,
                    "params" => [],
                    "sub_category" => "",
                    "discount" => 0,
                    "subtotal" => $subtotal,
                    "total" => $amount,
                    "tax" => 3.38,
                    "discount_amount" => 0,
                    "non_discount_subtotal" => 50,
                    "printer_ip" => "",
                    "loyalty_cost" => null,
                    "loyalty_reward" => false,
                    "timeframe_id" => null,
                    "special_id" => null,
                    "giftcard_id" => null,
                    "punch_card_id" => null,
                    "teetime_id" => null,
                    "modifier_total" => 0,
                    "erange_code" => null,
                    "line" => 1,
                    "unit_price_includes_tax" => false,
                    "items" => [],
                    "fees" => [
                        [
                            "price_class_id" => 3,
                            "price_class_name" => "Junior",
                            "timeframe_id" => 12,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 110,
                            "base_price" => 110,
                            "name" => "18 Cart - Junior Sometimes",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 4,
                            "price_class_name" => "Member",
                            "timeframe_id" => 13,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 1110,
                            "base_price" => 1110,
                            "name" => "18 Cart - Member All Times",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 1,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 3,
                            "teesheet_id" => 71,
                            "teesheet_name" => "TESTER",
                            "season_id" => 8,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 4,
                            "teesheet_id" => 67,
                            "teesheet_name" => "Jack Barnes",
                            "season_id" => 9,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 16,
                            "teesheet_id" => 26,
                            "teesheet_name" => "Exec 9 holes",
                            "season_id" => 11,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 40,
                            "base_price" => 40,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 14,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 1,
                            "base_price" => 1,
                            "name" => "18 Cart - Regular ",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 2,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "18 Cart - Regular Change label color",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 15,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => false,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 444,
                            "base_price" => 444,
                            "name" => "18 Cart - Regular Not-default",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 2,
                            "price_class_name" => "Senior",
                            "timeframe_id" => 11,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 18,
                            "unit_price" => 10,
                            "base_price" => 10,
                            "name" => "18 Cart - Senior Whenever",
                            "item_id" => "5276",
                            "item_number" => 3,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 3,
                            "price_class_name" => "Junior",
                            "timeframe_id" => 12,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 110,
                            "base_price" => 110,
                            "name" => "9 Cart - Junior Sometimes",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 4,
                            "price_class_name" => "Member",
                            "timeframe_id" => 13,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 1110,
                            "base_price" => 1110,
                            "name" => "9 Cart - Member All Times",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 14,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 1,
                            "base_price" => 1,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 4,
                            "teesheet_id" => 67,
                            "teesheet_name" => "Jack Barnes",
                            "season_id" => 9,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 1,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 3,
                            "teesheet_id" => 71,
                            "teesheet_name" => "TESTER",
                            "season_id" => 8,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 0,
                            "base_price" => 0,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 16,
                            "teesheet_id" => 26,
                            "teesheet_name" => "Exec 9 holes",
                            "season_id" => 11,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 20,
                            "base_price" => 20,
                            "name" => "9 Cart - Regular ",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 2,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 50,
                            "base_price" => 50,
                            "name" => "9 Cart - Regular Change label color",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 1,
                            "price_class_name" => "Regular",
                            "timeframe_id" => 15,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 10,
                            "special_id" => false,
                            "is_today" => false,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 222,
                            "base_price" => 222,
                            "name" => "9 Cart - Regular Not-default",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ],
                        [
                            "price_class_id" => 2,
                            "price_class_name" => "Senior",
                            "timeframe_id" => 11,
                            "teesheet_id" => 27,
                            "teesheet_name" => "Main course 2",
                            "season_id" => 7,
                            "special_id" => false,
                            "is_today" => true,
                            "item_type" => "cart_fee",
                            "holes" => 9,
                            "unit_price" => 10,
                            "base_price" => 10,
                            "name" => "9 Cart - Senior Whenever",
                            "item_id" => "5277",
                            "item_number" => 4,
                            "taxes" => [
                                [
                                    "name" => "Sales Tax",
                                    "percent" => 6.75,
                                    "cumulative" => 0
                                ]
                            ],
                            "quantity" => 1,
                            "selected" => true,
                            "unit_price_includes_tax" => false,
                            "max_discount" => 0,
                            "type" => "",
                            "price" => 0
                        ]
                    ],
                    "success" => 1
                ]
            ],
            "payments" => [
                [
                    "amount" => $rc_payed,
                    "number" => $this->rainCheck_number,
                    "type" => $rc_payment_type,
                    "description" => $rc_payment_description.' '.$this->rainCheck_number,
                    "record_id" => $this->rainCheck_id,
                    "approved" => "false",
                    "invoice_id" => 0,
                    "card_type" => "",
                    "card_number" => "",
                    "cardholder_name" => "",
                    "auth_amount" => 0,
                    "auth_code" => "",
                    "tip_recipient" => 0,
                    "tran_type" => "",
                    "params" => "",
                    "bypass" => false,
                    "is_auto_gratuity" => 0,
                    "success" => true,
                    "payment_id" => $rc_payment_id,
                    "merchant_data" => false,
                    "verified" => 0,
                    "raincheck" => [
                        "raincheck_id" => $this->rainCheck_id,
                        "raincheck_number" => $this->rainCheck_number,
                        "teesheet_id" => "27",
                        "teetime_id" => "",
                        "teetime_date" => "0000-00-00",
                        "teetime_time" => "0",
                        "date_issued" => $this->rainCheck_issue_date,
                        "date_redeemed" => $this->rainCheck_date_redeemed,
                        "players" => "1",
                        "holes_completed" => "0",
                        "customer_id" => "0",
                        "employee_id" => "12",
                        "green_fee" => false,
                        "cart_fee" => [
                            "unit_price" => $rc_subtotal,
                            "price_class_id" => 1,
                            "timeframe_id" => 1,
                            "item_number" => 20,
                            "item_id" => $this->Cart_item_id
                        ],
                        "tax" => 3.38,
                        "total" => $rc_payed,
                        "expiry_date" => "0000-00-00 00:00:00",
                        "deleted" => "0",
                        "subtotal" => $rc_subtotal
                    ]
                ],
                [
                    "amount" => ($amount - $rc_payed),
                    "number" => $this->giftCard_number,
                    "type" => $gc_payment_type,
                    "description" => $gc_payment_description.' '.$this->giftCard_number,
                    "record_id" => $this->giftCard_id,
                    "approved" => "false",
                    "invoice_id" => 0,
                    "card_type" => "",
                    "card_number" => "",
                    "cardholder_name" => "",
                    "auth_amount" => 0,
                    "auth_code" => "",
                    "tip_recipient" => 0,
                    "tran_type" => "",
                    "params" => "",
                    "bypass" => false,
                    "is_auto_gratuity" => 0,
                    "success" => true,
                    "payment_id" => $gc_payment_id,
                    "merchant_data" => false,
                    "verified" => 0,
                    "gift_card" => [
                        "CID" => "0",
                        "giftcard_id" => $this->giftCard_id,
                        "course_id" => $this->course_id,
                        "giftcard_number" => $this->giftCard_number,
                        "value" => $gift_card_balance,
                        "customer_id" => $this->customer_id,
                        "details" => "",
                        "date_issued" => $this->giftCard_issue_date,
                        "expiration_date" => $this->giftCard_expiration_date,
                        "department" => "",
                        "category" => "",
                        "deleted" => "0"
                    ],
                    "status" => false,
                    "msg" => "Giftcard balance is $1,000.00"
                ]
            ],
            "total" => $amount,
            "subtotal" => $subtotal,
            "tax" => 3.38,
            "num_items" => $quantity
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        //Check that the cart successfully closed
        $expectedCartResponse = [
            "sale_id" => $this->new_sale_id,
            "number" => $this->new_number,
            "loyalty_points_spent" => 0,
            "cart_id" => $this->next_cart_id,
            "success" => true
        ];
        $I->seeResponseContainsJson($expectedCartResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedCartResponse);

        //check that the sales tables are updated
        $db_sales = [
            'sale_id' => $this->new_sale_id,
            'number' => $this->new_number,
            'payment_type' => ''.$rc_payment_description.' '.$this->rainCheck_number.': $'.$rc_payed.'<br />'.$gc_payment_description.':'.$this->giftCard_number.': $'.($amount-$rc_payed).'.00<br />'
        ];
        $I->seeInDatabase('foreup_sales', $db_sales);
        $db_sales_items = [
            'sale_id' => $this->new_sale_id,
            'item_id' => $this->Cart_item_id,
            'quantity_purchased' => $quantity,
            'subtotal' => $subtotal,
            'tax' => ($amount - $subtotal),
            'total' => $amount
        ];
        $I->seeInDatabase('foreup_sales_items', $db_sales_items);
    }

    public function splitPaymentGiftCard_LoyaltyPoints(Salesv2apiGuy $I)
    {
        $quantity = 1;
        $amount = 8.54;
        $subtotal = 8;
        $lp_payed = 4;
        $points = 400;
        $points_spent = 400;
        $lp_payment_type = 'loyalty_points';
        $lp_payment_description = 'Loyalty';
        $gc_payment_type = 'gift_card';
        $gc_payment_description = 'Gift Card';
        $gift_card_balance = '1000.00';
        $gc_msg = "Giftcard balance is $1,000.00";

        //setup
        $I->wantTo('Test a split payment with a Gift Card and Loyalty Points');
        //Make sure that the item being purchased qualifies for loyalty points
        $loyaltyRates = [
            'course_id' => 6270,
            'label' => 'Loyalty Test',
            'type' => 'item',
            'value' => $this->rocketBalls_item_id,
            'points_per_dollar' => 50,
            'dollars_per_point' => 1,
            'tee_time_index' => 3,
            'price_category' => 1
        ];
        $I->haveInDatabase('foreup_loyalty_rates', $loyaltyRates);
        //Change the loyalty points so there aren't enough
        $I->updateDatabase('foreup_customers', array('person_id' => $this->customer_id), array('loyalty_points' => $points));

        //add customer to sale
        $payload = [
            "person_id" => $this->customer_id,
            "selected" => true,
            "taxable" => true,
            "discount" => "0.00"
        ];
        $I->sendPUT('/index.php/api/cart/customers/'.$this->customer_id, json_encode($payload));

        //add items to sale
        $payload = [
            "item_id" => $this->rocketBalls_item_id,
            "item_type" => "item",
            "quantity" => $quantity,
            "unit_price" => "8",
            "discount_percent" => 0,
            "timeframe_id" => null,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //pay with loyalty points
        $payload = [
            "approved" => "false",
            "card_type" => "",
            "card_number" => "",
            "cardholder_name" => "",
            "auth_amount" => 0,
            "auth_code" => "",
            "description" => "Loyalty Points",
            "tran_type" => "",
            "type" => "loyalty_points",
            "record_id" => $this->customer_id,
            "amount" => $amount,
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0,
            "is_auto_gratuity" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check that payment succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $lp_payment_type));
        $lp_payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        $expectedPaymentResponse1 = [
            "success" => true,
            "payment_id" => intval($lp_payment_id),
            "description" => $lp_payment_description,
            "amount" => $lp_payed,
            "type" => $lp_payment_type,
            "record_id" => $this->customer_id,
            "number" => $points_spent,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            'tran_type' => '',
            'approved' => 'false',
            'verified' => 0,
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse1);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse1);

        //pay remaining balance with gift card
        $payload = [
            "approved" => "false",
            "card_type" => "",
            "card_number" => "",
            "cardholder_name" => "",
            "auth_amount" => 0,
            "auth_code" => "",
            "description" => "Gift Card",
            "tran_type" => "",
            "type" => $gc_payment_type,
            "record_id" => 0,
            "amount" => ($amount - $lp_payed),
            "tip_recipient" => 0,
            "number" => $this->giftCard_number,
            "params" => null,
            "bypass" => 0,
            "is_auto_gratuity" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check that payment succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $gc_payment_type));
        $gc_payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id, 'type' => $gc_payment_type));

        $expectedPaymentResponse2 = [
            "success" => true,
            "payment_id" => intval($gc_payment_id),
            "description" => $gc_payment_description.':'.$this->giftCard_number,
            "amount" => round(($amount - $lp_payed), 2),
            "type" => $gc_payment_type,
            "record_id" => intval($this->giftCard_id),
            "number" => $this->giftCard_number,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            "tran_type" => '',
            "approved" => 'false',
            "verified" => 0,
            "gift_card" => [
                "CID" => "0",
                "giftcard_id" => $this->giftCard_id,
                "course_id" => "6270",
                "giftcard_number" => strval($this->giftCard_number),
                "value" => $gift_card_balance,
                "customer_id" => strval($this->customer_id),
                "details" => "",
                "date_issued" => $this->giftCard_issue_date,
                "expiration_date" => $this->giftCard_expiration_date,
                "department" => "",
                "category" => "",
                "deleted" => "0"
            ],
            "status" => false,
            "msg" => $gc_msg
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse2);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse2);

        //finish by closing the cart
        $payload = [
            "status" => "complete",
            "customers" => [
                [
                    "person_id" => $this->customer_id,
                    "taxable" => true,
                    "discount" => "0.00",
                    "first_name" => "Tiger",
                    "last_name" => "Woods",
                    "email" => "woods2@gmail.com",
                    "phone_number" => "",
                    "cell_phone_number" => "8014567123",
                    "birthday" => "1980-07-24",
                    "address_1" => "1546 E Flash Drive",
                    "address_2" => "",
                    "city" => "Lehi",
                    "state" => "UT",
                    "zip" => "84043",
                    "country" => "",
                    "comments" => "",
                    "image_id" => "0",
                    "account_number" => "10231352102135",
                    "account_balance" => "0.00",
                    "account_balance_allow_negative" => "0",
                    "member_account_balance" => "0.00",
                    "account_limit" => "0.00",
                    "member_account_limit" => "0.00",
                    "member_account_balance_allow_negative" => "1",
                    "invoice_balance" => "0.00",
                    "status_flag" => "3",
                    "invoice_balance_allow_negative" => "0",
                    "use_loyalty" => "1",
                    "loyalty_points" => $this->customer_loyalty_points,
                    "price_class" => "1",
                    "member" => "0",
                    "require_food_minimum" => "0",
                    "username" => null,
                    "opt_out_text" => "1",
                    "opt_out_email" => "1",
                    "unsubscribe_all" => "0",
                    "texting_status" => null,
                    "course_id" => $this->course_id,
                    "photo" => "http => //127.0.0.1 => 8080/images/profiles/profile_picture.png",
                    "passes" => [],
                    "groups" => [],
                    "selected" => true,
                    "member_account_label" => "Making a nickname",
                    "customer_account_label" => "Pro Shop",
                    "success" => $this->customer_id
                ]
            ],
            "items" => [
                [
                    "item_id" => $this->rocketBalls_item_id,
                    "course_id" => $this->course_id,
                    "name" => "RocketBalls",
                    "department" => "Course",
                    "category" => "PS Balls",
                    "subcategory" => "",
                    "supplier_id" => "532",
                    "item_number" => $this->rocketBalls_item_number,
                    "description" => "",
                    "unit_price" => "8",
                    "base_price" => "8.00",
                    "inactive" => "0",
                    "do_not_print" => "0",
                    "max_discount" => "0.00",
                    "inventory_level" => $this->rocketBalls_inventory_level,
                    "inventory_unlimited" => 1,
                    "cost_price" => "5.00",
                    "is_giftcard" => "0",
                    "is_side" => "0",
                    "food_and_beverage" => "0",
                    "number_sides" => "0",
                    "number_salads" => "0",
                    "number_soups" => "0",
                    "print_priority" => "1",
                    "is_fee" => "0",
                    "erange_size" => "0",
                    "meal_course_id" => "0",
                    "taxes" => [
                        [
                            "name" => "Sales Tax",
                            "percent" => 6.75,
                            "cumulative" => "0",
                            "amount" => 0.54
                        ]
                    ],
                    "is_serialized" => "0",
                    "do_not_print_customer_receipt" => "0",
                    "is_pass" => "0",
                    "pass_days_to_expiration" => null,
                    "pass_restrictions" => null,
                    "pass_price_class_id" => null,
                    "modifiers" => [],
                    "sides" => "",
                    "printer_groups" => null,
                    "item_type" => "item",
                    "loyalty_points_per_dollar" => 50,
                    "loyalty_dollars_per_point" => 0.01,
                    "quantity" => 1,
                    "discount_percent" => 0,
                    "selected" => true,
                    "sub_category" => "",
                    "discount" => 0,
                    "subtotal" => 8,
                    "total" => $amount,
                    "tax" => 0.54,
                    "discount_amount" => 0,
                    "non_discount_subtotal" => $subtotal,
                    "printer_ip" => "",
                    "loyalty_cost" => 800,
                    "loyalty_reward" => $points,
                    "timeframe_id" => null,
                    "special_id" => null,
                    "giftcard_id" => null,
                    "punch_card_id" => null,
                    "teetime_id" => null,
                    "params" => null,
                    "modifier_total" => 0,
                    "erange_code" => null,
                    "line" => 1,
                    "unit_price_includes_tax" => false,
                    "items" => [],
                    "success" => 1
                ]
            ],
            "payments" => [
                [
                    "amount" => $lp_payed,
                    "type" => $lp_payment_type,
                    "description" => $lp_payment_description,
                    "record_id" => $this->customer_id,
                    "approved" => "false",
                    "invoice_id" => 0,
                    "card_type" => "",
                    "card_number" => "",
                    "cardholder_name" => "",
                    "auth_amount" => 0,
                    "auth_code" => "",
                    "tip_recipient" => 0,
                    "tran_type" => "",
                    "params" => "",
                    "bypass" => false,
                    "is_auto_gratuity" => 0,
                    "success" => true,
                    "payment_id" => $lp_payment_id,
                    "number" => $points_spent,
                    "merchant_data" => false,
                    "verified" => 0
                ],
                [
                    "amount" => ($amount - $lp_payed),
                    "number" => $this->giftCard_number,
                    "type" => $gc_payment_type,
                    "description" => $gc_payment_description.':'.$this->giftCard_number,
                    "record_id" => $this->giftCard_id,
                    "approved" => "false",
                    "invoice_id" => 0,
                    "card_type" => "",
                    "card_number" => "",
                    "cardholder_name" => "",
                    "auth_amount" => 0,
                    "auth_code" => "",
                    "tip_recipient" => 0,
                    "tran_type" => "",
                    "params" => "",
                    "bypass" => false,
                    "is_auto_gratuity" => 0,
                    "success" => true,
                    "payment_id" => $gc_payment_id,
                    "merchant_data" => false,
                    "verified" => 0,
                    "gift_card" => [
                        "CID" => "0",
                        "giftcard_id" => $this->giftCard_id,
                        "course_id" => $this->course_id,
                        "giftcard_number" => $this->giftCard_number,
                        "value" => $gift_card_balance,
                        "customer_id" => $this->customer_id,
                        "details" => "",
                        "date_issued" => $this->giftCard_issue_date,
                        "expiration_date" => $this->giftCard_expiration_date,
                        "department" => "",
                        "category" => "",
                        "deleted" => "0"
                    ],
                    "status" => false,
                    "msg" => $gc_msg
                ]
            ],
            "total" => $amount,
            "subtotal" => $subtotal,
            "tax" => 0.54,
            "num_items" => $quantity
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        //Check that the cart successfully closed
        $expectedCartResponse = [
            "sale_id" => $this->new_sale_id,
            "number" => $this->new_number,
            "loyalty_points_spent" => $points_spent,
            "loyalty_points_earned" => 212,
            "cart_id" => $this->next_cart_id,
            "success" => true
        ];
        $I->seeResponseContainsJson($expectedCartResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedCartResponse);

        //check that the sales tables are updated
        $db_sales = [
            'sale_id' => $this->new_sale_id,
            'number' => $this->new_number,
            'payment_type' => ''.$lp_payment_description.': $'.$lp_payed.'.00<br />'.$gc_payment_description.':'.$this->giftCard_number.': $'.($amount-$lp_payed).'<br />'
        ];
        $I->seeInDatabase('foreup_sales', $db_sales);
        $db_sales_items = [
            'sale_id' => $this->new_sale_id,
            'item_id' => $this->rocketBalls_item_id,
            'quantity_purchased' => $quantity,
            'subtotal' => $subtotal,
            'tax' => ($amount - $subtotal),
            'total' => $amount
        ];
        $I->seeInDatabase('foreup_sales_items', $db_sales_items);
    }

    public function splitPaymentRaincheck_LoyaltyPoints(Salesv2apiGuy $I){
        $amount = 53.38;
        $subtotal = 50;
        $quantity = 1;
        $rc_payed = 43.38;
        $rc_subtotal = 40;
        $rc_payment_type = 'raincheck';
        $rc_payment_description = 'Raincheck';
        $lp_payed = $amount - $rc_payed;
        $lp_points_spent = ($amount - $rc_payed) * 100;
        $lp_payment_type = 'loyalty_points';
        $lp_payment_description = 'Loyalty';

        //setup
        $I->wantTo('Test a split payment with a Raincheck and Loyalty Points');
        //Make sure that the item being purchased qualifies for loyalty points
        $loyaltyRates = [
            'course_id' => 6270,
            'label' => 'Loyalty Test',
            'type' => 'item',
            'value' => $this->Cart_item_id,
            'points_per_dollar' => 50,
            'dollars_per_point' => 1,
            'tee_time_index' => 3,
            'price_category' => 1
        ];
        $I->haveInDatabase('foreup_loyalty_rates', $loyaltyRates);
        //Alter the rain check so it doesn't cover everything
        $I->updateDatabase('foreup_rainchecks', array('raincheck_id' => $this->rainCheck_id), array('total' => $rc_payed, 'cart_fee' => $rc_subtotal));

        //add item to cart
        $payload = [
            "item_id" => $this->Cart_item_id,
            "item_type" => "cart_fee",
            "quantity" => $quantity,
            "unit_price" => $subtotal,
            "discount_percent" => 0,
            "timeframe_id" => null,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => [],
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));

        //add customer to sale
        $payload = [
            "person_id" => $this->customer_id,
            "selected" => true,
            "taxable" => true,
            "discount" => "0.00"
        ];
        $I->sendPUT('/index.php/api/cart/customers/'.$this->customer_id, json_encode($payload));

        //pay for item with raincheck
        $payload = [
            "type" => $rc_payment_type,
            "record_id" => 0,
            "amount" => $amount,
            "tip_recipient" => 0,
            "number" => $this->rainCheck_number,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //Check for success
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $rc_payment_type));
        $rc_payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        $expectedPaymentResponse1 = [
            "success" => true,
            "payment_id" => intval($rc_payment_id),
            "description" => $rc_payment_description.' '.$this->rainCheck_number,
            "amount" => $rc_payed,
            "type" => $rc_payment_type,
            "record_id" => intval($this->rainCheck_id),
            "number" => $this->rainCheck_number,
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            'tran_type' => '',
            'approved' => false,
            'verified' => 0,
            "raincheck" => [
                "raincheck_id" => $this->rainCheck_id,
                "raincheck_number" => strval($this->rainCheck_number),
                "teesheet_id" => "27",
                "teetime_id" => "",
                "teetime_date" => "0000-00-00",
                "teetime_time" => "0",
                "date_issued" => $this->rainCheck_issue_date." 00:00:00",
                "date_redeemed" => "0000-00-00 00:00:00",
                "players" => "1",
                "holes_completed" => "0",
                "customer_id" => "0",
                "employee_id" => "12",
                "green_fee" => false,
                "cart_fee" => [
                    "unit_price" => $rc_subtotal,
                    "price_class_id" => 1,
                    "timeframe_id" => 1,
                    "item_number" => 20,
                    "item_id" => intval($this->Cart_item_id)
                ],
                "tax" => 3.38,
                "total" => $rc_payed,
                "expiry_date" => "0000-00-00 00:00:00",
                "deleted" => "0",
                "subtotal" => $rc_subtotal
            ]
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse1);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse1);

        //pay with loyalty points
        $payload = [
            "type" => "loyalty_points",
            "record_id" => $this->customer_id,
            "amount" => $lp_payed,
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check that payment succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $lp_payment_type));
        $lp_payment_id = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        $expectedPaymentResponse2 = [
            "success" => true,
            "payment_id" => intval($lp_payment_id),
            "description" => $lp_payment_description,
            "amount" => intval($lp_payed),
            "type" => $lp_payment_type,
            "record_id" => intval($this->customer_id),
            "number" => intval($lp_points_spent),
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            'tran_type' => '',
            'approved' => false,
            'verified' => 0,
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse2);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse2);

        //Finish closing the cart
        $payload = [
            "status" => "complete",
            "customers" => [
                [
                    "person_id" => $this->customer_id,
                    "taxable" => true,
                    "discount" => "0.00",
                    "first_name" => "Tiger",
                    "last_name" => "Woods",
                    "email" => "woods2@gmail.com",
                    "phone_number" => "",
                    "cell_phone_number" => "8014567123",
                    "birthday" => "1980-07-24",
                    "address_1" => "1546 E Flash Drive",
                    "address_2" => "",
                    "city" => "Lehi",
                    "state" => "UT",
                    "zip" => "84043",
                    "country" => "",
                    "comments" => "",
                    "image_id" => null,
                    "account_number" => "10231352102135",
                    "account_balance" => "0.00",
                    "account_balance_allow_negative" => "0",
                    "member_account_balance" => "0.00",
                    "account_limit" => "0.00",
                    "member_account_limit" => "0.00",
                    "member_account_balance_allow_negative" => "1",
                    "invoice_balance" => "0.00",
                    "status_flag" => "3",
                    "invoice_balance_allow_negative" => "0",
                    "use_loyalty" => "1",
                    "loyalty_points" => $this->customer_loyalty_points,
                    "price_class" => "1",
                    "member" => "0",
                    "require_food_minimum" => "0",
                    "username" => null,
                    "opt_out_text" => "1",
                    "opt_out_email" => "1",
                    "unsubscribe_all" => "0",
                    "texting_status" => null,
                    "course_id" => $this->course_id,
                    "photo" => "http => //127.0.0.1 => 8080/images/profiles/profile_picture.png",
                    "passes" => [],
                    "groups" => [],
                    "selected" => true,
                    "member_account_label" => "Making a nickname",
                    "customer_account_label" => "Pro Shop",
                    "success" => $this->customer_id
                ]
            ],
            "payments" => [
                [
                    "amount" => $rc_payed,
                    "number" => $this->rainCheck_number,
                    "type" => $rc_payment_type,
                    "description" => $rc_payment_description.':'.$this->rainCheck_number,
                    "record_id" => $this->rainCheck_id,
                    "invoice_id" => 0,
                    "tip_recipient" => 0,
                    "params" => "",
                    "bypass" => false,
                    "success" => true,
                    "payment_id" => $rc_payment_id,
                    "merchant_data" => false,
                    "raincheck" => [
                        "raincheck_id" => $this->rainCheck_id,
                        "raincheck_number" => $this->rainCheck_number,
                        "teesheet_id" => "27",
                        "teetime_id" => "",
                        "teetime_date" => "0000-00-00",
                        "teetime_time" => "0",
                        "date_issued" => $this->rainCheck_issue_date,
                        "date_redeemed" => $this->rainCheck_date_redeemed,
                        "players" => "1",
                        "holes_completed" => "0",
                        "customer_id" => "0",
                        "employee_id" => "12",
                        "green_fee" => false,
                        "cart_fee" => [
                            "unit_price" => $this->Cart['unit_price'],
                            "price_class_id" => 1,
                            "timeframe_id" => 1,
                            "item_number" => 20,
                            "item_id" => $this->Cart_item_id
                        ],
                        "tax" => ($amount - $subtotal),
                        "total" => $rc_payed,
                        "expiry_date" => "0000-00-00 00:00:00",
                        "deleted" => "0",
                        "subtotal" => $subtotal
                    ]
                ],
                [
                    "amount" => ($amount - $rc_payed),
                    "type" => $lp_payment_type,
                    "description" => $lp_payment_description,
                    "record_id" => $this->customer_id,
                    "invoice_id" => 0,
                    "tip_recipient" => 0,
                    "params" => "",
                    "bypass" => false,
                    "success" => true,
                    "payment_id" => $lp_payment_id,
                    "number" => $lp_points_spent,
                    "merchant_data" => false
                ]
            ],
            "total" => $amount,
            "subtotal" => $subtotal,
            "tax" => ($amount - $subtotal),
            "num_items" => $quantity
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        //Check that the cart successfully closed
        $expectedCartResponse = [
            "sale_id" => $this->new_sale_id,
            "number" => $this->new_number,
            "loyalty_points_spent" => intval($lp_points_spent),
            "loyalty_points_earned" => 0,
            "cart_id" => $this->next_cart_id,
            "success" => true
        ];
        $I->seeResponseContainsJson($expectedCartResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedCartResponse);

        //check that the sales tables are updated
        $db_sales = [
            'sale_id' => $this->new_sale_id,
            'number' => $this->new_number,
            'payment_type' => ''.$rc_payment_description.' '.$this->rainCheck_number.': $'.$rc_payed.'<br />'.$lp_payment_description.': $'.number_format(($amount-$rc_payed), 2).'<br />'
        ];
        $I->seeInDatabase('foreup_sales', $db_sales);
        $db_sales_items = [
            'sale_id' => $this->new_sale_id,
            'item_id' => $this->Cart_item_id,
            'quantity_purchased' => $quantity,
            'subtotal' => $subtotal,
            'tax' => ($amount - $subtotal),
            'total' => $amount
        ];
        $I->seeInDatabase('foreup_sales_items', $db_sales_items);
    }

    public function customPayment(Salesv2apiGuy $I)
    {
        //variable setup
        $amount = 8.54;
        $quantity = 1;
        $subtotal = 8;

        //test setup
        $I->wantTo('test the custom payment option');

        //add item to cart
        $payload = [
            "item_id" => $this->rocketBalls_item_id,
            "item_type" => "item",
            "quantity" => $quantity,
            "unit_price" => "8",
            "discount_percent" => 0,
            "timeframe_id" => null,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => null,
            "unit_price_includes_tax" => false,
            "erange_size" => "0"
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $I->seeResponseCodeIs(200);

        //pay for items in cart with cash
        $payload = [
            "type" => $this->Custom_payment_type,
            "record_id" => 0,
            "amount" => $amount,
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $response = $I->grabResponse();

        //check that the payments endpoint succeeded
        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $this->Custom_payment_type));
        $paymentID = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        $expectedPaymentResponse = [
            "success" => true,
            "payment_id" => intval($paymentID),
            "description" => $this->Custom_payment_description,
            "amount" => $amount,
            "type" => $this->Custom_payment_type,
            "record_id" => 0,
            "number" => "",
            "params" => "",
            "bypass" => false,
            "merchant_data" => false,
            'tran_type' => '',
            'approved' => false,
            'verified' => 0
        ];
        $I->seeResponseContainsJson($expectedPaymentResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedPaymentResponse);

        //finish closing the cart
        $payload = [
            "status" => "complete",
            "customers" => [],
            "payments" => [
                [
                    "amount" => $amount,
                    "type" => $this->Custom_payment_type,
                    "description" => $this->Custom_payment_description,
                    "record_id" => 0,
                    "invoice_id" => 0,
                    "tip_recipient" => 0,
                    "params" => "",
                    "bypass" => false,
                    "success" => true,
                    "payment_id" => $paymentID,
                    "number" => "",
                    "merchant_data" => false
                ]
            ],
            "total" => $amount,
            "subtotal" => 8,
            "tax" => 0.54,
            "num_items" => $quantity
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        $this->new_sale_id = $I->grabFromDatabase('foreup_sales', 'max(sale_id)');
        $this->new_number = $I->grabFromDatabase('foreup_sales', 'max(number)');

        $expectedCartResponse = [
            "sale_id" => intval($this->new_sale_id),
            "number" => intval($this->new_number),
            "loyalty_points_spent" => 0,
            "cart_id" => $this->next_cart_id,
            "success" => true
        ];
        $I->seeResponseContainsJson($expectedCartResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedCartResponse);

        //check that the sales tables are updated
        $db_sales = [
            'sale_id' => $this->new_sale_id,
            'number' => $this->new_number,
            'payment_type' => ''.$this->Custom_payment_description.': $'.$amount.'<br />'
        ];
        $I->seeInDatabase('foreup_sales', $db_sales);
        $db_sales_items = [
            'sale_id' => $this->new_sale_id,
            'item_id' => $this->rocketBalls_item_id,
            'quantity_purchased' => $quantity,
            'subtotal' => $subtotal,
            'tax' => ($amount - $subtotal),
            'total' => $amount
        ];
        $I->seeInDatabase('foreup_sales_items', $db_sales_items);
    }

    public function purchaseItemKit(Salesv2apiGuy $I){
        $quantity = 1;
        $amount = 10.68;
        $tax = .68;
        $subtotal = 10;
        $payment_type = 'cash';
        $payment_description = 'Cash';
        //test setup
        $I->wantTo('test purchasing an ItemKit');

        //add item to cart
        $payload = [
            "item_id"=>$this->ItemKit_id,
            "item_type"=>"item_kit",
            "quantity"=>$quantity,
            "unit_price"=>doubleval($this->ItemKit['unit_price']),
            "discount_percent"=>0,
            "timeframe_id"=>null,
            "special_id"=>null,
            "punch_card_id"=>null,
            "selected"=>true,
            "params"=>null,
            "unit_price_includes_tax"=>false,
            "erange_size"=> 0
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id.'/items/1', json_encode($payload));
        $I->seeResponseCodeIs(200);

        //pay for items in cart with cash
        $payload = [
            "approved" => "false",
            "card_type" => "",
            "card_number" => "",
            "cardholder_name" => "",
            "auth_amount" => 0,
            "auth_code" => "",
            "description" => $payment_description,
            "tran_type" => "",
            "type" => $payment_type,
            "record_id" => 0,
            "amount" => $amount,
            "tip_recipient" => 0,
            "params" => null,
            "bypass" => 0,
            "is_auto_gratuity" => 0,
            "verified" => 1
        ];
        $I->sendPOST('/index.php/api/cart/'.$this->new_cart_id.'/payments', json_encode($payload));
        $I->seeResponseCodeIs(200);

        $I->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->new_cart_id, 'type' => $payment_type));
        $paymentID = $I->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->new_cart_id));

        //finish the payment by closing the cart
        $payload = [
            "status" => "complete",
            "customers" => [],
            "items" => [
                [
                    "item_id" => strval($this->ItemKit_id),
                    "course_id" => strval($this->course_id),
                    "name" => $this->ItemKit['name'],
                    "department" => "",
                    "category" => $this->ItemKit['category'],
                    "subcategory" => "",
                    "item_number" => null,
                    "description" => "",
                    "unit_price" => strval($this->ItemKit['unit_price']),
                    "base_price" => strval($this->ItemKit['unit_price']),
                    "max_discount" => "100",
                    "inventory_level" => "0",
                    "inventory_unlimited" => "1",
                    "cost_price" => strval($this->ItemKit['cost_price']),
                    "is_punch_card" => "0",
                    "is_serialized" => "0",
                    "items" => [
                        [
                            "item_id" => $this->ItemKit_Item1['item_id'],
                            "name" => $this->rocketBalls['name'],
                            "department" => "Course",
                            "category" => "PS Balls",
                            "subcategory" => "",
                            "quantity" => $this->ItemKit_Item1['quantity'],
                            "price_class_id" => "",
                            "price_class_name" => ""
                        ],
                        [
                            "item_id" => $this->ItemKit_Item2['item_id'],
                            "name" => $this->DrPepper['name'],
                            "department" => "Food & Beverage",
                            "category" => "Beverage",
                            "subcategory" => "",
                            "quantity" => $this->ItemKit_Item2['quantity'],
                            "price_class_id" => "",
                            "price_class_name" => ""
                        ]
                    ],
                    "taxes" => [
                        [
                            "name" => $this->ItemKit_Taxes['name'],
                            "percent" => $this->ItemKit_Taxes['percent'],
                            "cumulative" => "0",
                            "amount" => $tax
                        ]
                    ],
                    "item_type" => "item_kit",
                    "loyalty_points_per_dollar" => false,
                    "loyalty_dollars_per_point" => 0,
                    "quantity" => $quantity,
                    "discount_percent" => 0,
                    "selected" => true,
                    "sub_category" => "",
                    "discount" => 0,
                    "subtotal" => $subtotal,
                    "total" => $amount,
                    "tax" => $tax,
                    "discount_amount" => 0,
                    "non_discount_subtotal" => $subtotal,
                    "printer_ip" => "",
                    "print_priority" => 0,
                    "loyalty_cost" => null,
                    "loyalty_reward" => false,
                    "timeframe_id" => null,
                    "special_id" => null,
                    "giftcard_id" => null,
                    "punch_card_id" => null,
                    "teetime_id" => null,
                    "params" => null,
                    "modifier_total" => 0,
                    "food_and_beverage" => 0,
                    "erange_size" => 0,
                    "erange_code" => null,
                    "line" => 1,
                    "unit_price_includes_tax" => false,
                    "success" => 1
                ]
            ],
            "payments" => [
                [
                    "amount" => $amount,
                    "type" => $payment_type,
                    "description" => $payment_description,
                    "record_id" => 0,
                    "approved" => "false",
                    "verified" => 1,
                    "invoice_id" => 0,
                    "card_type" => "",
                    "card_number" => "",
                    "cardholder_name" => "",
                    "auth_amount" => 0,
                    "auth_code" => "",
                    "tip_recipient" => 0,
                    "tran_type" => "",
                    "params" => "",
                    "bypass" => false,
                    "is_auto_gratuity" => 0,
                    "success" => true,
                    "payment_id" => $paymentID,
                    "number" => "",
                    "merchant_data" => false
                ]
            ],
            "total" => $amount,
            "subtotal" => $this->ItemKit['unit_price'],
            "tax" => $tax,
            "num_items" => strval($quantity)
        ];
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, json_encode($payload));
        $response = $I->grabResponse();

        $this->new_sale_id = $I->grabFromDatabase('foreup_sales', 'max(sale_id)');
        $this->new_number = $I->grabFromDatabase('foreup_sales', 'max(number)');

        //Check that the cart successfully closed
        $expectedCartResponse = [
            "sale_id" => intval($this->new_sale_id),
            "number" => intval($this->new_number),
            "loyalty_points_spent" => 0,
            "cart_id" => $this->next_cart_id,
            "success" => true
        ];
        $I->seeResponseContainsJson($expectedCartResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedCartResponse);

        $I->seeInDatabase('foreup_items', array('item_id' => $this->DrPepper_item_id, 'quantity' => ($this->DrPepper['quantity'] - $quantity)));
        $I->seeInDatabase('foreup_items', array('item_id' => $this->rocketBalls_item_id, 'quantity' => ($this->rocketBalls['quantity'] - $quantity)));
    }

    public function purchaseNewGiftCard(Salesv2apiGuy $I){

    }

    public function reloadGiftCard(Salesv2apiGuy $I){

    }

    public function creditCardPayment(Salesv2apiGuy $I)
    {

    }

    //Coupon payment option is currently NOT operational

//    public function couponPaymentDiscountPercentage(Salesv2apiGuy $I)
//    {
//
//    }
//
//    public function couponPaymentDiscountDollarAmount(Salesv2apiGuy $I)
//    {
//
//    }
//
//    public function couponPaymentBOGOPercentage(Salesv2apiGuy $I)
//    {
//
//    }
//
//    public function couponPaymentBOGOFree(Salesv2apiGuy $I)
//    {
//
//    }
//
//    public function expiredCoupon(Salesv2apiGuy $I)
//    {
//
//    }

    public function cleanup(Salesv2apiGuy $I)
    {
        $current_last_cart_id = $I->grabFromDatabase('foreup_pos_cart', 'max(cart_id)');
        $I->deleteGreaterThanFromDatabase('foreup_pos_cart', array('cart_id' => $this->orig_last_cart_id));
        //$I->deleteGreaterThanFromDatabase('foreup_pos_cart_items', array('cart_id' => $this->orig_last_cart_id));
        $I->deleteGreaterThanFromDatabase('foreup_pos_cart_payments', array('cart_id' => $this->orig_last_cart_id));
        $I->deleteGreaterThanFromDatabase('foreup_pos_cart_customers', array('cart_id' => $this->orig_last_cart_id));
        $I->dontSeeInDatabase('foreup_pos_cart', array('cart_id' => $current_last_cart_id));
        $I->dontSeeInDatabase('foreup_pos_cart_items', array('cart_id' => $current_last_cart_id));
        $I->dontSeeInDatabase('foreup_pos_cart_payments', array('cart_id' => $current_last_cart_id));
        $I->dontSeeInDatabase('foreup_pos_cart_customers', array('cart_id' => $current_last_cart_id));
    }
}