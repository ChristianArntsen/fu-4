<?php
use \Salesv2apiGuy;

class ReturnsCest
{

    var $orig_last_cart_id;
    var $new_cart_id;

    public function _before(Salesv2apiGuy $I)
    {
        $I->login('jackbarnes', 'jackbarnes');
        $I->setHeader('Content-Type', 'application/json');
        $I->setHeader('Api-key', 'no_limits');

        $this->new_cart_id = $I->grabFromDatabase('foreup_pos_cart', 'max(cart_id)') + 1;
    }

    public function _after(Salesv2apiGuy $I)
    {
    }

    // tests
    public function cartModeSetToReturn(Salesv2apiGuy $I)
    {
        $this->orig_last_cart_id = $I->grabFromDatabase('foreup_pos_cart', 'max(cart_id)');

        $I->wantTo('test that the cart is set to return mode');

        $payload = '{"mode":"return"}';
        $I->sendPUT('/index.php/api/cart/'.$this->new_cart_id, $payload);
        $I->canSeeResponseEquals('{"success":1}');
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase('foreup_pos_cart', array('cart_id' => $this->new_cart_id, 'mode' => 'return'));
    }

    //The return option uses the same functions as purchasing something.
    //The only difference is that the amounts passed in are negative.
    //We should probably wait to implement a test till we've done everything else.
    public function cashReturn(Salesv2apiGuy $I)
    {

    }


    //The return option uses the same functions as purchasing something.
    //The only difference is that the amounts passed in are negative.
    //We should probably wait to implement a test till we've done everything else.
    public function creditCardReturn(Salesv2apiGuy $I)
    {

    }


    //make sure that inventory is adjusted correctly after returning an item kit
    public function returnItemKit(Salesv2apiGuy $I)
    {

    }
}