<?php
namespace reports;


use \Codeception\Verify;

class ReportColumnTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testBasicColumn()
    {
        $column = new \fu\reports\ReportColumn("test");
        verify($column->getAlias())->equals(false);
        verify($column->getColumn())->equals("test");
        verify($column->getColumnName())->equals("test");
        verify($column->getJoin())->equals(false);
        verify($column->getTableName())->equals(null);
        verify($column->hasRelation())->equals(false);
        verify($column->isAggregate())->equals(false);
        verify($column->toString())->equals("test");
    }
    public function testColumnWRelationship()
    {
        $column = new \fu\reports\ReportColumn("table.test");
        verify($column->getAlias())->equals(false);
        verify($column->getColumn())->equals("table.test");
        verify($column->getColumnName())->equals("test");
        verify($column->getJoin())->equals("table");
        verify($column->getTableName())->equals("table");
        verify($column->hasRelation())->equals(true);
        verify($column->isAggregate())->equals(false);
        verify($column->toString())->equals("table.test");
    }
    public function testBasicColumnWAggregate()
    {
        $column = new \fu\reports\ReportColumn("max(test)");
        verify($column->getAlias())->equals(false);
        verify($column->getColumn())->equals("max(test) ");
        verify($column->getColumnName())->equals("max(test) ");
        verify($column->getJoin())->equals(null);
        verify($column->getTableName())->equals(null);
        verify($column->hasRelation())->equals(false);
        verify($column->isAggregate())->equals(true);
        verify($column->toString())->equals("max(test) ");
    }
    public function testColumnWAggregateWRelation()
    {
        $column = new \fu\reports\ReportColumn("max(table.test)");
        verify($column->getAlias())->equals(false);
        verify($column->getColumn())->equals("max(table.test)");
        verify($column->getColumnName())->equals("test");
        verify($column->getJoin())->equals("table");
        verify($column->getTableName())->equals("table");
        verify($column->hasRelation())->equals(true);
        verify($column->isAggregate())->equals(true);
        verify($column->toString())->equals("max(table.test)");
    }
}