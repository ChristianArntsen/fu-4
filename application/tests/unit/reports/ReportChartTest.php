<?php
namespace reports;


use \Codeception\Verify;

class ReportChartTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testChartWithAggregateFilter()
    {
        $input = [
            "range"=> "count(sale_id)",
            "visual_grouping"=> "month",
            "visual_width"=> "year",
            'category' =>"date",
            'filters' => [
                [
                    "boolean"=> "AND",
                    "column"=> "sum(profit)",
                    "operator"=> ">",
                    "value"=> "1"
                ]
            ],
            'group' =>['sale_id'],
            'columns' => [
                0 => 'sale_id',
                1 => 'profit'
            ],
            'order' => [
                0 => 'sale_id',
            ],
            "date_range"=>[
                "column"=>"sale_date",
                "type"=>"range",
                "start_value"=>"2015-04-08",
                "end_value"=>"2015-05-12",
                "editMode"=>false
            ],
        ];
        $reportParameters = $this->createDataReportParam($input);
        $reportParameters = $reportParameters->prepareRequiredChartFields();
        $report = new \fu\reports\ReportTypes\SalesReport($reportParameters);

        $data = $report->getData();
    }
    private function createDataReportParam($input)
    {
        $reportParameters = new \fu\reports\ReportChartsParameters();
        isset($input['filters']) ? $reportParameters->setFilters($input['filters']) : "";
        isset($input['date_range']) ? $reportParameters->setDateRange($input['date_range']) : "";
        isset($input['visual_grouping']) ? $reportParameters->setVisualGrouping($input['visual_grouping']): "";
        isset($input['visual_width']) ? $reportParameters->setVisualWidth($input['visual_width']): "";
        isset($input['range']) ? $reportParameters->setRange($input['range']): "";
        isset($input['category']) ? $reportParameters->setCategory($input['category']): "";
        isset($input['split_by'])?$reportParameters->setSplitBy($input['split_by']):"";

        return $reportParameters;
    }
}