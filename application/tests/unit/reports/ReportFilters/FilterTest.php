<?php
namespace reports\ReportFilters;

use \Codeception\Verify;
class FilterTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testBasicFilter()
    {
        $filterObj = new \fu\reports\ReportFilters\Filter([
            "column"=>"somecolumn",
            "operator"=>"=",
            "value"=>"somevalue"
        ]);
        verify($filterObj->getColumn())->equals("somecolumn");
        verify($filterObj->getSubJoin())->equals(null);
        verify($filterObj->toString())->equals("somecolumn = 'somevalue'");
        verify($filterObj->whereParameters())->equals(["somecolumn","=","somevalue"]);
        verify(count($filterObj->getFilterObjects()))->equals(1);
    }


    public function testFilterWAggregate()
    {
        $filterObj = new \fu\reports\ReportFilters\Filter([
            "column"=>"table.somecolumn",
            "operator"=>"=",
            "value"=>"somevalue"
        ]);
        verify($filterObj->getColumn())->equals("table.somecolumn");
        verify($filterObj->getSubJoin())->equals(null);
        verify($filterObj->toString())->equals("table.somecolumn = 'somevalue'");
        verify($filterObj->whereParameters())->equals(["table.somecolumn","=","somevalue"]);
        verify(count($filterObj->getFilterObjects()))->equals(1);
    }

    public function testFilterWAggregateWJoin()
    {
        $filterObj = new \fu\reports\ReportFilters\Filter([
            "column"=>"max(table.somecolumn)",
            "operator"=>"=",
            "value"=>"somevalue"
        ]);
        verify($filterObj->getColumn())->equals("t.`table.somecolumn`");
        verify($filterObj->getSubJoin()['aggregate'])->equals("max");
        verify($filterObj->getSubJoin()['columnName'])->equals("table.somecolumn");
        verify($filterObj->getSubJoin()['newName'])->equals("max_table_somecolumn");
        verify($filterObj->getSubJoin()['joinTable'])->equals("table");
        verify($filterObj->toString())->equals("t.`table.somecolumn` = 'somevalue'");
        verify($filterObj->whereParameters())->equals(["t.`max_table_somecolumn`","=","somevalue"]);
        verify(count($filterObj->getFilterObjects()))->equals(1);
    }

    public function testCombinedColumnsFilter()
    {
        $filterObj = new \fu\reports\ReportFilters\Filter([
            "column"=>"`max(total)` + `sum(subtotal)`",
            "operator"=>"=",
            "value"=>"somevalue"
        ]);
    }
}