<?php
namespace reports\ReportFilters;


use \Codeception\Verify;
class MovingTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testMe()
    {
        $filterObj = new \fu\reports\ReportFilters\Moving([
            "column"=>"somecolumn",
            "operator"=>"=",
            "type"=>"moving",
            "movingRange"=>"lastmonth",
            "typeOfMovingRange"=>"label",
        ]);
        verify(count($filterObj->getFilterObjects()))->equals(2);

    }

}