<?php
namespace reports\ReportFilters;


use \Codeception\Verify;
class TeetimeTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testTeetimeConversionFilter()
    {
        $filterObj = new \fu\reports\ReportFilters\Teetime([
            "column"=>"somecolumn",
            "operator"=>"=",
            "startValue"=>"201501010101",
            "endValue"=>"201501010101",
        ]);
        verify(count($filterObj->getFilterObjects()))->equals(2);
    }

}