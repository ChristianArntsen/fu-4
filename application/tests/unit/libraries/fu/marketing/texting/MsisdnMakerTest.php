<?php

namespace libraries\fu\marketing\texting;

use Codeception\Module\UnitHelper;
use fu\marketing\texting\MsisdnMaker;

class MsisdnMakerTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    private $valid_numbers =
        array(
                '9202037403',
                '920-407-0029',
                '920-284-8895',
                '916-797-2884',
                '801-499-9688',
                '(860) 324-4080',
                '+1-801-358-3516',
                '+1 (712) 262-9199'
                                    );

    private $invalid_numbers =
        array(
                '859-5801',
                '8583859945x0',
                '(815) 933-22814'
                                    );

    private $customers =
        array(
            array('person_id' => 19, 'course_id' => 6270, 'cell_phone_number' => '+1 (712) 262-9199'),
            array('person_id' => 21, 'course_id' => 6270, 'cell_phone_number' => '8583859945x0'),
            array('person_id' => 22, 'course_id' => 6270, 'phone_number' => '(860) 324-4080')
        );

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testValidNumbers()
    {
        $msisdn_maker = new MsisdnMaker();

        foreach ($this->valid_numbers as $number) {
            $this->assertNull($msisdn_maker->set($number));
        }
    }

    public function testInvalidNumbers()
    {
        $msisdn_maker = new MsisdnMaker();

        foreach ($this->invalid_numbers as $number) {
            $this->assertNull($msisdn_maker->set($number));
        }
    }

}