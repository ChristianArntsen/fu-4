<?php
namespace libraries\fu\marketing\RecipientRetriever;


use Codeception\Module\UnitHelper;
use \Codeception\Verify;

class unsent2Test extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
   /**
    * @var \UnitTester
    */
    protected $tester;

    protected function _before()
    {

        //@var \Codeception\Actor\UnitTester $this->tester
        $this->campaign = ["course_id"=>1];
        $this->campaign = (object)$this->campaign;
        \Codeception\Specify\Config::setIgnoredProperties(['countOfEveryone']);
    }

    protected function _after()
    {
    }

    // tests
    public function testException()
    {
        $this->specify("Exception thrown",function(){

            $unsent  = new \fu\marketing\recipientRetriever\unsent();
            $unsent->setPossibleRecipients([]);
        }, ['throws' => 'fu\Exceptions\InvalidArgument']);
    }

    public function testEveryone()
    {
        $this->countOfEveryone = 0;
        $this->specify("Check everyone",function() {
            $possibleRecipients = [
                "individuals" => [],
                "groups" => [0], //everyone group id
            ];


            $recipientCount = $this->getNumberOfRecipients($possibleRecipients);
            verify($recipientCount)->equals(1895);
            $this->countOfEveryone = $recipientCount;
        });

        $this->specify("When pairing a group with the Everyone group, it should show unique people still.",function() {
            $possibleRecipients = [
                "individuals" => [],
                "groups" => [0, 1],
            ];
            $recipientCount = $this->getNumberOfRecipients($possibleRecipients);
            verify($recipientCount)->equals(1895);
            verify($this->countOfEveryone)->equals($recipientCount);
        });



        $this->specify("Check course sharing customers",function(){
            $possibleRecipients = [
                "individuals"=>[],
                "groups"=>[0],
            ];
            $this->campaign->course_id = 2;
            $recipientCount = $this->getNumberOfRecipients($possibleRecipients);
            verify($recipientCount)->equals(0);
        });
    }

    public function testSingleGroup()
    {
        $this->specify("Check for a single group",function(){
            $possibleRecipients = [
                "individuals"=>[],
                "groups"=>[1],
            ];
            $recipientCount = $this->getNumberOfRecipients($possibleRecipients);
            verify($recipientCount)->equals(0);
        });
        $this->specify("Check for a group and an individual",function(){
            $possibleRecipients = [
                "individuals"=>[2],
                "groups"=>[1],
            ];
            $recipientCount = $this->getNumberOfRecipients($possibleRecipients);
            verify($recipientCount)->equals(1);
        });
        $this->specify("Check for a group that doesn't exist",function(){
            $possibleRecipients = [
                "individuals"=>[],
                "groups"=>[-1],
            ];

            $recipientCount = $this->getNumberOfRecipients($possibleRecipients);
            verify($recipientCount)->equals(0);
        });
    }

    public function testSingleIndividual()
    {
        $this->specify("Check for a single user that doesn't exist in a group",function(){
            $possibleRecipients = [
                "individuals"=>[2],
                "groups"=>[],
            ];

            $recipientCount = $this->getNumberOfRecipients($possibleRecipients);
            verify($recipientCount)->equals(1);
        });
        $this->specify("Check for a user in a group and outside a group",function(){
            $possibleRecipients = [
                "individuals"=>[1,2],
                "groups"=>[],
            ];

            $recipientCount = $this->getNumberOfRecipients($possibleRecipients);
            verify($recipientCount)->equals(1);
        });


        $this->specify("Check for a user that doesn't exist",function(){
            $possibleRecipients = [
                "individuals"=>[-1],
                "groups"=>[],
            ];

            $recipientCount = $this->getNumberOfRecipients($possibleRecipients);
            verify($recipientCount)->equals(0);
        });
    }

    public function getNumberOfRecipients($possibleRecipients)
    {
        $getNumberOfRecipients = new \fu\marketing\recipientRetriever\unsent();
        $getNumberOfRecipients->setPossibleRecipients($possibleRecipients);
        $temp = $this->campaign;
        $getNumberOfRecipients->setCampaign($temp);
        $recipients = $getNumberOfRecipients->getRecipients();
        return count($recipients);
    }
}