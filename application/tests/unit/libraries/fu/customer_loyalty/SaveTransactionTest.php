<?php

namespace libraries\fu\customer_loyalty;

use fu\TestingHelpers\Actor;
use fu\TestingHelpers\Customer;
use fu\TestingHelpers\Package;

class SaveTransactionTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $CI = & get_instance();
        $CI->load->model('Customer');
        $CI->load->model('Customers_loyalty_package');
        $CI->load->model('Loyalty_package');

        $this->actor = new Actor();
        $this->actor->login();

    }

    protected function _after()
    {
        $CI = & get_instance();
    }

    public function testGetRates()
    {

        $CI = & get_instance();

        $this->specify("Save transaction null package should update person's loyalty balance", function() use ($CI) {
            $customer = new Customer();

            $results = $CI->Customer_loyalty->save_transaction($customer->get_id(), 'Test transaction', 123456.00, 'Test transaction details', $sale_id='');

            $customer_details = $CI->Customer->get_info($customer->get_id(), $CI->session->userdata('course_id'));

            $customer->destroy();

            $this->assertTrue($results);
            $this->assertEquals(123456, $customer_details->loyalty_points);
        });

        $this->specify("Save transaction with package should update person's package loyalty balance", function() use ($CI) {
            $customer = new Customer();
            $package = new Package();
            $customer_loyalty_package = [
                'course_id' => $CI->session->userdata('course_id'),
                'customer_id' => $customer->get_id(),
                'loyalty_package_id' => $package->get_id()
            ];
            $CI->Customers_loyalty_package->save($customer_loyalty_package);

            $results = $CI->Customer_loyalty->save_transaction($customer->get_id(), 'Test transaction', 654321.00, 'Test transaction details', '', false, $package->get_id());
            $loyalty_package = $CI->Customers_loyalty_package->get($customer->get_id(), $package->get_id())->result_object();

            $CI->db->where('id', $customer_loyalty_package['id']);
            $CI->db->delete('customers_loyalty_packages');
            $package->destroy();
            $customer->destroy();

            $this->assertTrue($results);
            $this->assertEquals(654321.00, $loyalty_package[0]->loyalty_balance);
        });
    }
}