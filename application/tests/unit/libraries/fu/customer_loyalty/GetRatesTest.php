<?php

namespace libraries\fu\customer_loyalty;

use fu\TestingHelpers\Actor;
use fu\TestingHelpers\Package;

class GetRatesTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $default_loyalty;
    protected $package_a;
    protected $package_a_loyalty;
    protected $package_b;
    protected $package_b_loyalty;


    protected function _before()
    {
        $CI = & get_instance();
        $CI->load->model('Customer_loyalty');
        $CI->load->model('Loyalty_package');

        $this->actor = new Actor();
        $this->default_loyalty = [
            [
                'label'				=> 'Item A',
                'course_id'			=> $CI->session->userdata('course_id'),
                'type'				=> 'department',
                'value'				=> 'Food & Beverage',
                'value_label'		=> 'Food & Beverage',
                'tee_time_index'	=> 0,
                'price_category'	=> 0,
                'points_per_dollar'	=> 1.00,
                'dollars_per_point'	=> 1.00
            ], [
                'label'				=> 'Item B',
                'course_id'			=> $CI->session->userdata('course_id'),
                'type'				=> 'department',
                'value'				=> 'Food & Beverage',
                'value_label'		=> 'Food & Beverage',
                'tee_time_index'	=> 0,
                'price_category'	=> 0,
                'points_per_dollar'	=> 1.00,
                'dollars_per_point'	=> 1.00
            ]
        ];

        foreach ($this->default_loyalty as &$loyalty) {
            $CI->Customer_loyalty->save($loyalty);
        }

        $this->package_a = new Package();

        $this->package_a_loyalty = [
            [
                'label'				 => 'Item C',
                'course_id'			 => $CI->session->userdata('course_id'),
                'loyalty_package_id' => $this->package_a->get_id(),
                'type'				 => 'department',
                'value'				 => 'Food & Beverage',
                'value_label'		 => 'Food & Beverage',
                'tee_time_index'	 => 0,
                'price_category'	 => 0,
                'points_per_dollar'	 => 1.00,
                'dollars_per_point'	 => 1.00,
	            'timeframe_id'      => null,
	            'limit_timeframe'   => "0"
            ], [
                'label'				 => 'Item D',
                'course_id'			 => $CI->session->userdata('course_id'),
                'loyalty_package_id' => $this->package_a->get_id(),
                'type'				 => 'department',
                'value'				 => 'Food & Beverage',
                'value_label'		 => 'Food & Beverage',
                'tee_time_index'	 => 0,
                'price_category'	 => 0,
                'points_per_dollar'	 => 1.00,
                'dollars_per_point'	 => 1.00,
		        'timeframe_id'      => null,
		        'limit_timeframe'   => "0"
            ]
        ];

        foreach ($this->package_a_loyalty as &$loyalty) {
            $CI->Customer_loyalty->save($loyalty);
        }


        $this->package_b = new Package();

        $this->package_b_loyalty = [
            [
                'label'				 => 'Item E',
                'course_id'			 => $CI->session->userdata('course_id'),
                'loyalty_package_id' => $this->package_b->get_id(),
                'type'				 => 'department',
                'value'				 => 'Food & Beverage',
                'value_label'		 => 'Food & Beverage',
                'tee_time_index'	 => 0,
                'price_category'	 => 0,
                'points_per_dollar'	 => 1.00,
                'dollars_per_point'	 => 1.00
            ], [
                'label'				 => 'Item F',
                'course_id'			 => $CI->session->userdata('course_id'),
                'loyalty_package_id' => $this->package_b->get_id(),
                'type'				 => 'department',
                'value'				 => 'Food & Beverage',
                'value_label'		 => 'Food & Beverage',
                'tee_time_index'	 => 0,
                'price_category'	 => 0,
                'points_per_dollar'	 => 1.00,
                'dollars_per_point'	 => 1.00
            ]
        ];

        foreach ($this->package_b_loyalty as &$loyalty) {
            $CI->Customer_loyalty->save($loyalty);
        }
    }

    protected function _after()
    {
        $CI = & get_instance();

        $this->package_a->destroy();
        $this->package_b->destroy();

        foreach ($this->default_loyalty as $loyalty) {
            $CI->db->where('loyalty_rate_id', $loyalty['loyalty_rate_id']);
            $CI->db->delete('loyalty_rates');
        }

        foreach ($this->package_a_loyalty as $loyalty) {
            $CI->db->where('loyalty_rate_id', $loyalty['loyalty_rate_id']);
            $CI->db->delete('loyalty_rates');
        }

        foreach ($this->package_b_loyalty as $loyalty) {
            $CI->db->where('loyalty_rate_id', $loyalty['loyalty_rate_id']);
            $CI->db->delete('loyalty_rates');
        }
    }

    public function testGetRates()
    {
        $this->actor->login();

        $CI = & get_instance();

        $this->specify("Can get rates for default (null) package", function() use ($CI) {
            $results = $CI->Customer_loyalty->get_rates()->result_object();
            $invalid_results = array_filter($results, function($rate) {
                return !!$rate->loyalty_package_id;
            });

            $this->assertEmpty($invalid_results, 'Uh oh, we returned non-default rates');
        });

        $this->specify("Can get rates for not null package.", function() use ($CI) {
            $results = $CI->Customer_loyalty->get_rates($this->package_a->get_id())->result_object();
            $this->assertCount(count($this->package_a_loyalty), $results);
            foreach($results as $rate) {
                $this->assertContains((array)$rate, $this->package_a_loyalty);
            }
        });
    }
}