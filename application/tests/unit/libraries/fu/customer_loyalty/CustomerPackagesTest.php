<?php

namespace libraries\fu\customer_loyalty;

use fu\TestingHelpers\Actor;
use fu\TestingHelpers\Customer;
use fu\TestingHelpers\Package;

class CustomerPackagesTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $package_a;
    protected $customer_loyalty_package;
    protected $customer;

    protected function _before()
    {
        $CI = & get_instance();
        $CI->load->model('Customers_loyalty_package');
        $CI->load->model('Loyalty_package');

        $this->actor = new Actor();
        $this->actor->login();
        $this->customer = new Customer();

        $this->package_a = new Package();

        $this->customer_loyalty_package = [
            'course_id' => $CI->session->userdata('course_id'),
            'customer_id' => $this->customer->get_id(),
            'loyalty_package_id' => $this->package_a->get_id()
        ];
        $CI->Customers_loyalty_package->save($this->customer_loyalty_package);
    }

    protected function _after()
    {
        $CI = & get_instance();

        $CI->db->where('id', $this->customer_loyalty_package['id']);
        $CI->db->delete('customers_loyalty_packages');

        $this->customer->destroy();
    }

    public function testAdjustBalance()
    {
        $CI = & get_instance();

        $this->specify("Can adjust package balance up", function() use ($CI) {
            $current_balance = $CI->Customers_loyalty_package->get($this->customer->get_id(), $this->package_a->get_id())->result_object()[0];
            $this->assertEquals(0, $current_balance->loyalty_balance);

            $CI->Customers_loyalty_package->adjust_balance($this->customer->get_id(), $this->package_a->get_id(), 10.0);

            $current_balance = $CI->Customers_loyalty_package->get($this->customer->get_id(), $this->package_a->get_id())->result_object()[0];
            $this->assertEquals(10, $current_balance->loyalty_balance);
        });

        $this->specify("Can adjust package balance down", function() use ($CI) {
            $current_balance = $CI->Customers_loyalty_package->get($this->customer->get_id(), $this->package_a->get_id())->result_object()[0];
            $this->assertEquals(10, $current_balance->loyalty_balance);

            $CI->Customers_loyalty_package->adjust_balance($this->customer->get_id(), $this->package_a->get_id(), -10.0);

            $current_balance = $CI->Customers_loyalty_package->get($this->customer->get_id(), $this->package_a->get_id())->result_object()[0];
            $this->assertEquals(0, $current_balance->loyalty_balance);
        });
    }
}