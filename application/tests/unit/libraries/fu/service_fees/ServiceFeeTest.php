<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 8/2/2016
 * Time: 11:55 AM
 */

namespace libraries\fu\service_fees;


use fu\TestingHelpers\Actor;
use fu\TestingHelpers\TestingObjects\Item;
use fu\TestingHelpers\TestingObjects\Payment;

class ServiceFeeTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;
	/**
	 * @var \UnitTester
	 */
	protected $tester;
	protected function _before()
	{
		$this->actor = new Actor();
	}
	public function testExists()
	{
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$this->assertTrue(isset($ServiceFee));
		$this->assertInstanceOf('\fu\service_fees\ServiceFee',$ServiceFee);

		$this->assertTrue(method_exists($ServiceFee,'__construct'));
		$this->assertTrue(method_exists($ServiceFee,'save'));
		$this->assertTrue(method_exists($ServiceFee,'getTotal'));
		$this->assertTrue(method_exists($ServiceFee,'getSubtotal'));
		$this->assertTrue(method_exists($ServiceFee,'getTax'));
		$this->assertTrue(method_exists($ServiceFee,'getAsArray'));
		$this->assertTrue(method_exists($ServiceFee,'getItem'));
		$this->assertTrue(method_exists($ServiceFee,'setParentItem'));
		$this->assertTrue(method_exists($ServiceFee,'getServiceFee'));
		$this->assertTrue(method_exists($ServiceFee,'getServiceFeeByItem'));
		$this->assertTrue(method_exists($ServiceFee,'getItemByServiceFee'));
		$this->assertTrue(method_exists($ServiceFee,'applyToItems'));
		$this->assertTrue(method_exists($ServiceFee,'removeFromItems'));
		$this->assertTrue(method_exists($ServiceFee,'setParentItem'));
		$this->assertTrue(method_exists($ServiceFee,'calculatePrice'));
		$this->assertTrue(method_exists($ServiceFee,'search'));
		$this->assertTrue(method_exists($ServiceFee,'getHTML'));
		$this->assertTrue(method_exists($ServiceFee,'isApplied'));
		$this->assertTrue(method_exists($ServiceFee,'removeFromItems'));
		$this->assertTrue(method_exists($ServiceFee,'deleteServiceFeesForItem'));
	}

	public function testCreateAndDestroy()
	{
		$CI = get_instance();
		$CI->db->trans_start(true);

		$ServiceFee = new \fu\service_fees\ServiceFee();
		global $sf_item;

		$this->assertEquals(1,$ServiceFee->save($sf_item));
		$this->assertTrue($ServiceFee->exists());
		$serialized = $ServiceFee->getAsArray();
		$this->assertTrue(is_array($serialized));

        // why would this suddenly register as a string?
        // It is stored as int in database
        // It is not passed by http request
		//$this->assertTrue(is_integer($serialized['service_fee_id']));

        $this->assertTrue($ServiceFee->isServiceFee($serialized['item_id']));
		$this->assertTrue(is_integer($serialized['item_id']));
		$this->assertTrue(is_numeric($serialized['unit_price']));
		$this->assertTrue(is_numeric($serialized['parent_item_percent']));
		$this->assertEquals('more',$serialized['whichever_is']);
		$this->assertEquals(1,$serialized['is_service_fee']);
		$this->assertEquals('service_fee',$serialized['item_type']);

		// TODO: This breaks from TaxIncludedTest.php testTaxIncluded
		//*
		$this->assertContains(array(
			"value"=> (string) $serialized['service_fee_id'],
			"label"=>"Test SF 1"),
			$ServiceFee->search("Test SF 1"));
		//*/

		$this->assertEquals(1,$ServiceFee->delete());
		$this->assertFalse($ServiceFee->exists());

		$CI->db->trans_complete();
	}

	public function testCalculatePrice()
	{
		$CI = get_instance();
		$CI->db->trans_start(true);
		$ServiceFee = new \fu\service_fees\ServiceFee();
		global $sf_item;

		$sf_item2 = $sf_item;

		$this->assertEquals(1,$ServiceFee->save($sf_item2));
		$this->assertEquals(10.00,$ServiceFee->getSubtotal());
		$this->assertEquals(1.00,$ServiceFee->getTax());
		$this->assertEquals(0.00,$ServiceFee->getTax(false));
		$this->assertEquals(11.00,$ServiceFee->getTotal());
		$this->assertEquals(10.00,$ServiceFee->getTotal(false));

        // tax included more for the benefit of the item price code than service fees
        $sf_item2['unit_price_includes_tax'] = 1;

        $this->assertEquals(1,$ServiceFee->save($sf_item2));
        $this->assertEquals(9.09,$ServiceFee->getSubtotal());
        $this->assertEquals(0.91,$ServiceFee->getTax());
        $this->assertEquals(0.00,$ServiceFee->getTax(false));
        $this->assertEquals(10.00,$ServiceFee->getTotal());
        $this->assertEquals(9.09,$ServiceFee->getTotal(false));

        $sf_item2['unit_price_includes_tax'] = 0;
        $sf_item2['force_tax'] = 0;

        $this->assertEquals(1,$ServiceFee->save($sf_item2));
        $this->assertEquals(10.00,$ServiceFee->getSubtotal());
        $this->assertEquals(0.00,$ServiceFee->getTax());
        $this->assertEquals(0.00,$ServiceFee->getTax(false));
        $this->assertEquals(10.00,$ServiceFee->getTotal());
        $this->assertEquals(10.00,$ServiceFee->getTotal(false));

		$sf_item2['force_tax'] = 1;

		$this->assertEquals(1,$ServiceFee->save($sf_item2));
		$this->assertEquals(10.00,$ServiceFee->getSubtotal());
		$this->assertEquals(1.00,$ServiceFee->getTax());
		$this->assertEquals(1.00,$ServiceFee->getTax(false));
		$this->assertEquals(11.00,$ServiceFee->getTotal());
		$this->assertEquals(11.00,$ServiceFee->getTotal(false));

		$sf_item2['whichever_is'] = 'less';

		$this->assertEquals(1,$ServiceFee->save($sf_item2));
		$this->assertEquals(0.00,$ServiceFee->getSubtotal());
		$this->assertEquals(0.00,$ServiceFee->getTax());
		$this->assertEquals(0.00,$ServiceFee->getTax(false));
		$this->assertEquals(0.00,$ServiceFee->getTotal());
		$this->assertEquals(0.00,$ServiceFee->getTotal(false));

		$this->assertEquals(1,$ServiceFee->delete());
		$this->assertFalse($ServiceFee->exists());
		$CI->db->trans_complete();
	}

	public function testParentItem()
	{
		$CI = get_instance();
		$CI->db->trans_start(true);
		global $sf_item;

		$sf_item2 = $sf_item;
		$sf_item3 = $sf_item;
		$ParentItem = new \fu\service_fees\ServiceFee();
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$sf_item2['unit_price'] = 50;
		$sf_item2['name'] = 'The parent';
		$this->assertEquals(1,$ParentItem->save($sf_item2));
		$this->assertEquals(1,$ServiceFee->save($sf_item3));
		$parent_serial = $ParentItem->getAsArray();

        $this->assertTrue(is_numeric($parent_serial['item_id']));

        // this should fail, since parent item is a service fee
        $this->assertEquals(0,$ServiceFee->applyToItems($parent_serial['item_id']));

        // make the parent not a service fee
        $CI = get_instance();
        $CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");

		$this->assertEquals(1,$ServiceFee->applyToItems($parent_serial['item_id']));

		$this->assertEquals(25.00,$ServiceFee->getSubtotal());
		$this->assertEquals(2.50,$ServiceFee->getTax());
		$this->assertEquals(0.00,$ServiceFee->getTax(false));
		$this->assertEquals(27.50,$ServiceFee->getTotal());
		$this->assertEquals(25.00,$ServiceFee->getTotal(false));

		$ob_array = $ServiceFee->getServiceFeesForItem($parent_serial['item_id']);
		$this->assertTrue(is_array($ob_array));
		$this->assertEquals(1,count($ob_array));
		$this->assertTrue(is_object($ob_array[0]));
		$this->assertInstanceOf('\fu\service_fees\ServiceFee',$ob_array[0]);
		$SF_array = $ob_array[0]->getAsArray();
		$SF_actual = $ServiceFee->getAsArray();
		$this->assertEquals($sf_item3['name'],$SF_array['name']);
		$this->assertEquals($SF_actual['item_id'],$SF_array['item_id']);
		$ob_array[0]->getTotal();
		$this->assertEquals($ServiceFee->getTotal(),$ob_array[0]->getTotal());

		$CI->db->trans_complete();
	}

	public function testParentItemForceYesTax()
	{
		global $sf_item;
		$CI = get_instance();
		$CI->db->trans_start(true);

		$sf_item2 = $sf_item;
		$sf_item3 = $sf_item;
		$ParentItem = new \fu\service_fees\ServiceFee();
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$sf_item2['unit_price'] = 50;
		$sf_item2['force_tax']=1;
		$sf_item2['name'] = 'The parent';
		$sf_item2['selected'] = true;
		$this->assertEquals(1,$ParentItem->save($sf_item2));
		$this->assertEquals(1,$ServiceFee->save($sf_item3));
		$parent_serial = $ParentItem->getAsArray();

		$this->assertTrue(is_numeric($parent_serial['item_id']));

		// this should fail, since parent item is a service fee
		$this->assertEquals(0,$ServiceFee->applyToItems($parent_serial['item_id']));

		// make the parent not a service fee
		$CI = get_instance();
		$CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");

		$this->assertEquals(1,$ServiceFee->applyToItems($parent_serial['item_id']));

		$this->assertEquals(25.00,$ServiceFee->getSubtotal());
		$this->assertEquals(2.50,$ServiceFee->getTax());
		$this->assertEquals(0.00,$ServiceFee->getTax(false));
		$this->assertEquals(27.50,$ServiceFee->getTotal());
		$this->assertEquals(25.00,$ServiceFee->getTotal(false));

		$ob_array = $ServiceFee->getServiceFeesForItem($parent_serial['item_id']);
		$this->assertTrue(is_array($ob_array));
		$this->assertEquals(1,count($ob_array));
		$this->assertTrue(is_object($ob_array[0]));
		$this->assertInstanceOf('\fu\service_fees\ServiceFee',$ob_array[0]);
		$SF_array = $ob_array[0]->getAsArray();
		$SF_actual = $ServiceFee->getAsArray();
		$this->assertEquals($sf_item3['name'],$SF_array['name']);
		$this->assertEquals($SF_actual['item_id'],$SF_array['item_id']);
		$ob_array[0]->getTotal();
		$this->assertEquals($ServiceFee->getTotal(),$ob_array[0]->getTotal());


		// try it in a sale now
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $ParentItem->getAsArray();
		$item['service_fees'] = array($ServiceFee->getAsArray());
		$item['taxes'][0]['name']='IFTY';
		$item['service_fees'][0]['taxes'][0]['name']='SFFTY';
		$payment = $paymentObj->getArray();
		$payment['amount'] = 27.5 + 50;


		$CI = get_instance();

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_cart(array('taxable'=>0));
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$totals = $cart_model->get_item_totals(true);
		$this->specify('Check that the totals are correct for the item and service fee',function() use ($totals,$CI){
			verify($totals)->notEmpty();
			verify($totals['tax'])->equals(5.0);
			verify($totals['subtotal'])->equals(75);
			verify($totals['total'])->equals(80.0);
		});

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);

		$this->assertNotNull($response['sale_id']);

		$this->specify("Check that all sales tax was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->isEmpty();

		});

		$this->specify("Check that sale was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(0);
			verify($results['subtotal'])->equals(25.00);

		});

		$this->specify("Check that all sales tax was saved correctly for Parent Item.",function() use ($response,$item){
			$CI=get_instance();
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(10.000);
			verify($results['amount'])->equals(5);

		});

		$this->specify("Check that sale was saved correctly for Parent Item.",function() use ($response,$item){
			$CI=get_instance();
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(5);
			verify($results['subtotal'])->equals(50.00);

		});

		$CI->db->trans_complete();
	}

	public function testParentItemForceNoTax()
	{
		global $sf_item;
		$CI = get_instance();
		$CI->db->trans_start(true);

		$sf_item2 = $sf_item;
		$sf_item3 = $sf_item;
		$ParentItem = new \fu\service_fees\ServiceFee();
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$sf_item2['unit_price'] = 50;
		$sf_item2['force_tax']=0;
		$sf_item2['name'] = 'The parent';
		$sf_item2['selected'] = true;
		$this->assertEquals(1,$ParentItem->save($sf_item2));
		$this->assertEquals(1,$ServiceFee->save($sf_item3));
		$parent_serial = $ParentItem->getAsArray();

		$this->assertTrue(is_numeric($parent_serial['item_id']));

		// this should fail, since parent item is a service fee
		$this->assertEquals(0,$ServiceFee->applyToItems($parent_serial['item_id']));

		// make the parent not a service fee
		$CI = get_instance();
		$CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");

		$this->assertEquals(1,$ServiceFee->applyToItems($parent_serial['item_id']));

		$this->assertEquals(25.00,$ServiceFee->getSubtotal());
		$this->assertEquals(2.50,$ServiceFee->getTax());
		$this->assertEquals(0.00,$ServiceFee->getTax(false));
		$this->assertEquals(27.50,$ServiceFee->getTotal());
		$this->assertEquals(25.00,$ServiceFee->getTotal(false));

		$ob_array = $ServiceFee->getServiceFeesForItem($parent_serial['item_id']);
		$this->assertTrue(is_array($ob_array));
		$this->assertEquals(1,count($ob_array));
		$this->assertTrue(is_object($ob_array[0]));
		$this->assertInstanceOf('\fu\service_fees\ServiceFee',$ob_array[0]);
		$SF_array = $ob_array[0]->getAsArray();
		$SF_actual = $ServiceFee->getAsArray();
		$this->assertEquals($sf_item3['name'],$SF_array['name']);
		$this->assertEquals($SF_actual['item_id'],$SF_array['item_id']);
		$ob_array[0]->getTotal();
		$this->assertEquals($ServiceFee->getTotal(),$ob_array[0]->getTotal());


		// try it in a sale now
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $ParentItem->getAsArray();
		$item['service_fees'] = array($ServiceFee->getAsArray());
		$payment = $paymentObj->getArray();
		$payment['amount'] = 27.5 + 50;


		$CI = get_instance();

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$totals = $cart_model->get_item_totals(true);
		$this->specify('Check that the totals are correct for the item and service fee',function() use ($totals,$CI){
			verify($totals)->notEmpty();
			verify($totals['tax'])->equals(2.5);
			verify($totals['subtotal'])->equals(75);
			verify($totals['total'])->equals(77.5);
		});

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);

		$this->assertNotNull($response['sale_id']);

		$this->specify("Check that all sales tax was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(10.000);
			verify($results['amount'])->equals(2.5);

		});

		$this->specify("Check that sale was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(2.50);
			verify($results['subtotal'])->equals(25.00);

		});

		$this->specify("Check that all sales tax was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->isEmpty();
			//verify($results['percent'])->equals(10.000);
			//verify($results['amount'])->equals(5);

		});

		$this->specify("Check that sale was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(0);
			verify($results['subtotal'])->equals(50.00);

		});

		$CI->db->trans_complete();
	}

	public function testParentItemCustomerNonTaxable()
	{
		global $sf_item;

		$CI = get_instance();
		$CI->db->trans_start(true);
		$sf_item2 = $sf_item;
		$sf_item3 = $sf_item;
		$ParentItem = new \fu\service_fees\ServiceFee();
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$sf_item2['unit_price'] = 50;
		$sf_item2['name'] = 'The parent';
		$sf_item2['selected'] = true;
		$this->assertEquals(1,$ParentItem->save($sf_item2));
		$this->assertEquals(1,$ServiceFee->save($sf_item3));
		$parent_serial = $ParentItem->getAsArray();

		$this->assertTrue(is_numeric($parent_serial['item_id']));

		// this should fail, since parent item is a service fee
		$this->assertEquals(0,$ServiceFee->applyToItems($parent_serial['item_id']));

		// make the parent not a service fee
		$CI = get_instance();
		$CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");

		$this->assertEquals(1,$ServiceFee->applyToItems($parent_serial['item_id']));

		$ob_array = $ServiceFee->getServiceFeesForItem($parent_serial['item_id']);
		$this->assertTrue(is_array($ob_array));
		$this->assertEquals(1,count($ob_array));
		$this->assertTrue(is_object($ob_array[0]));
		$this->assertInstanceOf('\fu\service_fees\ServiceFee',$ob_array[0]);
		$SF_array = $ob_array[0]->getAsArray();
		$SF_actual = $ServiceFee->getAsArray();
		$this->assertEquals($sf_item3['name'],$SF_array['name']);
		$this->assertEquals($SF_actual['item_id'],$SF_array['item_id']);
		$ob_array[0]->getTotal();
		$this->assertEquals($ServiceFee->getTotal(),$ob_array[0]->getTotal());


		// try it in a sale now
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $ParentItem->getAsArray();
		$item['service_fees'] = array($ServiceFee->getAsArray());
		$payment = $paymentObj->getArray();
		$payment['amount'] = 25 + 50;


		$CI = get_instance();

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_customer(418,array('selected'=>1, 'taxable'=>0, 'discount'=>0));
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$totals = $cart_model->get_item_totals(true);
		$this->specify('Check that the totals are correct for the item and service fee',function() use ($totals,$CI){
			verify($totals)->notEmpty();
			verify($totals['tax'])->equals(0.00);
			verify($totals['subtotal'])->equals(75);
			verify($totals['total'])->equals(75);
		});

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);

		$this->assertNotNull($response['sale_id']);

		$this->specify("Check that all sales tax was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->isEmpty();

		});

		$this->specify("Check that sale was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(0.00);
			verify($results['subtotal'])->equals(25.00);

		});

		$this->specify("Check that all sales tax was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->isEmpty();

		});

		$this->specify("Check that sale was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(0);
			verify($results['subtotal'])->equals(50.00);

		});

		$CI->db->trans_complete();
	}

	public function testParentItemTaxIncluded()
	{
		global $sf_item;

		$CI = get_instance();
		$CI->db->trans_start(true);
		$sf_item2 = $sf_item;
		$sf_item3 = $sf_item;
		$ParentItem = new \fu\service_fees\ServiceFee();
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$sf_item2['unit_price'] = 50;
		$sf_item2['unit_price_includes_tax'] = 1;
		$sf_item2['name'] = 'The parent';
		$sf_item2['selected'] = true;
		$this->assertEquals(1,$ParentItem->save($sf_item2));
		$this->assertEquals(1,$ServiceFee->save($sf_item3));
		$parent_serial = $ParentItem->getAsArray();

		$this->assertTrue(is_numeric($parent_serial['item_id']));

		// this should fail, since parent item is a service fee
		$this->assertEquals(0,$ServiceFee->applyToItems($parent_serial['item_id']));

		// make the parent not a service fee
		$CI = get_instance();
		$CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");

		$this->assertEquals(1,$ServiceFee->applyToItems($parent_serial['item_id']));

		$ob_array = $ServiceFee->getServiceFeesForItem($parent_serial['item_id']);
		$this->assertTrue(is_array($ob_array));
		$this->assertEquals(1,count($ob_array));
		$this->assertTrue(is_object($ob_array[0]));
		$this->assertInstanceOf('\fu\service_fees\ServiceFee',$ob_array[0]);
		$SF_array = $ob_array[0]->getAsArray();
		$SF_actual = $ServiceFee->getAsArray();
		$this->assertEquals($sf_item3['name'],$SF_array['name']);
		$this->assertEquals($SF_actual['item_id'],$SF_array['item_id']);
		$ob_array[0]->getTotal();
		$this->assertEquals($ServiceFee->getTotal(),$ob_array[0]->getTotal());


		// try it in a sale now
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $ParentItem->getAsArray();
		$item['service_fees'] = array($ServiceFee->getAsArray());
		$payment = $paymentObj->getArray();
		$payment['amount'] = 27.50 + 50;


		$CI = get_instance();

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$totals = $cart_model->get_item_totals(true);
		$this->specify('Check that the totals are correct for the item and service fee',function() use ($totals,$CI){
			verify($totals)->notEmpty();
			verify($totals['tax'])->equals(6.82);
			verify($totals['subtotal'])->equals(68.18);
			verify($totals['total'])->equals(75.00);
		});

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);

		$this->assertNotNull($response['sale_id']);

		$this->specify("Check that all sales tax was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(10.000);
			verify($results['amount'])->equals(2.27);

		});

		$this->specify("Check that sale was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(2.27);
			verify($results['subtotal'])->equals(22.73);
			verify($results['total'])->equals(25.00);

		});

		$this->specify("Check that all sales tax was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(10.000);
			verify($results['amount'])->equals(4.55);

		});

		$this->specify("Check that sale was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(4.55);
			verify($results['subtotal'])->equals(45.45);
			verify($results['total'])->equals(50.00);

		});

		$CI->db->trans_complete();
	}

	public function testParentItemTaxIncludedDiscountCustomer()
	{
		global $sf_item;

		$CI = get_instance();
		$CI->db->trans_start(true);
		$sf_item2 = $sf_item;
		$sf_item3 = $sf_item;
		$ParentItem = new \fu\service_fees\ServiceFee();
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$sf_item2['unit_price'] = 50;
		$sf_item2['unit_price_includes_tax'] = 1;
		$sf_item2['name'] = 'The parent';
		$sf_item2['selected'] = true;
		$this->assertEquals(1,$ParentItem->save($sf_item2));
		$this->assertEquals(1,$ServiceFee->save($sf_item3));
		$parent_serial = $ParentItem->getAsArray();

		$this->assertTrue(is_numeric($parent_serial['item_id']));

		// this should fail, since parent item is a service fee
		$this->assertEquals(0,$ServiceFee->applyToItems($parent_serial['item_id']));

		// make the parent not a service fee
		$CI = get_instance();
		$CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");

		$this->assertEquals(1,$ServiceFee->applyToItems($parent_serial['item_id']));

		$ob_array = $ServiceFee->getServiceFeesForItem($parent_serial['item_id']);
		$this->assertTrue(is_array($ob_array));
		$this->assertEquals(1,count($ob_array));
		$this->assertTrue(is_object($ob_array[0]));
		$this->assertInstanceOf('\fu\service_fees\ServiceFee',$ob_array[0]);
		$SF_array = $ob_array[0]->getAsArray();
		$SF_actual = $ServiceFee->getAsArray();
		$this->assertEquals($sf_item3['name'],$SF_array['name']);
		$this->assertEquals($SF_actual['item_id'],$SF_array['item_id']);
		$ob_array[0]->getTotal();
		$this->assertEquals($ServiceFee->getTotal(),$ob_array[0]->getTotal());


		// try it in a sale now
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $ParentItem->getAsArray();
		$item['service_fees'] = array($ServiceFee->getAsArray());
		$payment = $paymentObj->getArray();
		$payment['amount'] = 27.50 + 50;


		$CI = get_instance();

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_customer(7943,array('selected'=>1, 'taxable'=>1, 'discount'=>10.00));
		$item['discount_percent']=10; // as though we get it pre-calculated from the frontend...
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$totals = $cart_model->get_item_totals(true);
		$this->specify('Check that the totals are correct for the item and service fee',function() use ($totals,$CI){
			verify($totals)->notEmpty();
			verify($totals['tax'])->equals(6.14);
			verify($totals['subtotal'])->equals(61.37);
			verify($totals['total'])->equals(67.51);
		});

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);

		$this->assertNotNull($response['sale_id']);

		$this->specify("Check that all sales tax was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(10.000);
			verify($results['amount'])->equals(2.05);

		});

		$this->specify("Check that sale was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(2.05);
			verify($results['subtotal'])->equals(20.46);
			verify($results['total'])->equals(22.51);

		});

		$this->specify("Check that all sales tax was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(10.000);
			verify($results['amount'])->equals(4.09);

		});

		$this->specify("Check that sale was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(4.09);
			verify($results['subtotal'])->equals(40.91);
			verify($results['total'])->equals(45.00);

		});

		$CI->db->trans_complete();
	}

	public function testParentItemTaxIncludedSFForceNoTax()
	{
		global $sf_item;

		$CI = get_instance();
		$CI->db->trans_start(true);
		$sf_item2 = $sf_item;
		$sf_item3 = $sf_item;
		$ParentItem = new \fu\service_fees\ServiceFee();
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$sf_item2['unit_price'] = 50;
		$sf_item2['unit_price_includes_tax'] = 1;
		$sf_item2['name'] = 'The parent';
		$sf_item2['selected'] = true;
		$sf_item3['force_tax'] = 0;
		$this->assertEquals(1,$ParentItem->save($sf_item2));
		$this->assertEquals(1,$ServiceFee->save($sf_item3));
		$parent_serial = $ParentItem->getAsArray();

		$this->assertTrue(is_numeric($parent_serial['item_id']));

		// this should fail, since parent item is a service fee
		$this->assertEquals(0,$ServiceFee->applyToItems($parent_serial['item_id']));

		// make the parent not a service fee
		$CI = get_instance();
		$CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");

		$this->assertEquals(1,$ServiceFee->applyToItems($parent_serial['item_id']));

		$ob_array = $ServiceFee->getServiceFeesForItem($parent_serial['item_id']);
		$this->assertTrue(is_array($ob_array));
		$this->assertEquals(1,count($ob_array));
		$this->assertTrue(is_object($ob_array[0]));
		$this->assertInstanceOf('\fu\service_fees\ServiceFee',$ob_array[0]);
		$SF_array = $ob_array[0]->getAsArray();
		$SF_actual = $ServiceFee->getAsArray();
		$this->assertEquals($sf_item3['name'],$SF_array['name']);
		$this->assertEquals($SF_actual['item_id'],$SF_array['item_id']);
		$ob_array[0]->getTotal();
		$this->assertEquals($ServiceFee->getTotal(),$ob_array[0]->getTotal());


		// try it in a sale now
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $ParentItem->getAsArray();
		$item['service_fees'] = array($ServiceFee->getAsArray());
		$payment = $paymentObj->getArray();
		$payment['amount'] = 27.50 + 50;


		$CI = get_instance();

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$totals = $cart_model->get_item_totals(true);
		$this->specify('Check that the totals are correct for the item and service fee',function() use ($totals,$CI){
			verify($totals)->notEmpty();
			verify($totals['tax'])->equals(4.55);
			verify($totals['subtotal'])->equals(68.18);
			verify($totals['total'])->equals(72.73);
		});

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);

		$this->assertNotNull($response['sale_id']);

		$this->specify("Check that all sales tax was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->isEmpty();

		});

		$this->specify("Check that sale was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(0.00);
			verify($results['subtotal'])->equals(22.73);
			verify($results['total'])->equals(22.73);

		});

		$this->specify("Check that all sales tax was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(10.000);
			verify($results['amount'])->equals(4.55);

		});

		$this->specify("Check that sale was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(4.55);
			verify($results['subtotal'])->equals(45.45);
			verify($results['total'])->equals(50.00);

		});

		$CI->db->trans_complete();
	}

	public function testParentItemTaxIncludedSFForceYesTax()
	{
		global $sf_item;

		$CI = get_instance();
		$CI->db->trans_start(true);
		$sf_item2 = $sf_item;
		$sf_item3 = $sf_item;
		$ParentItem = new \fu\service_fees\ServiceFee();
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$sf_item2['unit_price'] = 50;
		$sf_item2['unit_price_includes_tax'] = 1;
		$sf_item2['name'] = 'The parent';
		$sf_item2['selected'] = true;
		$sf_item3['force_tax'] = 1;
		$this->assertEquals(1,$ParentItem->save($sf_item2));
		$this->assertEquals(1,$ServiceFee->save($sf_item3));
		$parent_serial = $ParentItem->getAsArray();

		$this->assertTrue(is_numeric($parent_serial['item_id']));

		// this should fail, since parent item is a service fee
		$this->assertEquals(0,$ServiceFee->applyToItems($parent_serial['item_id']));

		// make the parent not a service fee
		$CI = get_instance();
		$CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");

		$this->assertEquals(1,$ServiceFee->applyToItems($parent_serial['item_id']));

		$ob_array = $ServiceFee->getServiceFeesForItem($parent_serial['item_id']);
		$this->assertTrue(is_array($ob_array));
		$this->assertEquals(1,count($ob_array));
		$this->assertTrue(is_object($ob_array[0]));
		$this->assertInstanceOf('\fu\service_fees\ServiceFee',$ob_array[0]);
		$SF_array = $ob_array[0]->getAsArray();
		$SF_actual = $ServiceFee->getAsArray();
		$this->assertEquals($sf_item3['name'],$SF_array['name']);
		$this->assertEquals($SF_actual['item_id'],$SF_array['item_id']);
		$ob_array[0]->getTotal();
		$this->assertEquals($ServiceFee->getTotal(),$ob_array[0]->getTotal());


		// try it in a sale now
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $ParentItem->getAsArray();
		$item['service_fees'] = array($ServiceFee->getAsArray());
		$payment = $paymentObj->getArray();
		$payment['amount'] = 27.50 + 50;


		$CI = get_instance();

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_customer(418,array('selected'=>1, 'taxable'=>0, 'discount'=>0));
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$totals = $cart_model->get_item_totals(true);
		$this->specify('Check that the totals are correct for the item and service fee',function() use ($totals,$CI){
			verify($totals)->notEmpty();
			verify($totals['tax'])->equals(2.27);
			verify($totals['subtotal'])->equals(68.18);
			verify($totals['total'])->equals(70.45);
		});

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);

		$this->assertNotNull($response['sale_id']);

		$this->specify("Check that all sales tax was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(10.000);
			verify($results['amount'])->equals(2.27);

		});

		$this->specify("Check that sale was saved correctly for Service Fee.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['service_fees'][0]['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(2.27);
			verify($results['subtotal'])->equals(22.73);
			verify($results['total'])->equals(25.00);

		});

		$this->specify("Check that all sales tax was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->isEmpty();

		});

		$this->specify("Check that sale was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(0.00);
			verify($results['subtotal'])->equals(45.45);
			verify($results['total'])->equals(45.45);

		});

		$CI->db->trans_complete();
	}

	public function testItemTaxes()
	{
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $itemObj->getArray();
		$payment = $paymentObj->getArray();

		$CI = get_instance();
		$CI->db->trans_start(true);

		$this->actor->login();
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);
		$this->assertNotNull($response['sale_id']);

		//Get taxes for the sale_id
		$results =
			$CI->db->from("sales_items_taxes")
				->where("sale_id",$response['sale_id'])->get()->row_array();

		$this->assertNotEmpty($results);
		$this->assertEquals($results['percent'],7.000);
		$this->assertNotEquals((int)$results['amount'],0);

		$CI->db->trans_complete();
	}

	public function testCleanup(){
        $CI = get_instance();

        $CI->db->query("
          DELETE FROM foreup_item_service_fees
          WHERE item_id in
          (SELECT item_id FROM foreup_items i
          WHERE i.course_id = 0);
        ");

        $CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id in
          (SELECT item_id FROM foreup_items i
          WHERE i.course_id = 0);
        ");

        $CI->db->query("
          DELETE FROM foreup_items
          WHERE course_id = 0
          AND name IN ('Test SF 1','The parent', 'The child');
        ");

        $this->assertEquals(2+2,4);
    }
}

global $sf_item;
$sf_item = array(
	'course_id'=> 6270,
	'name' => "Test SF 1",
	'department' => 'Corporate',
	'category' => 'Fee',
	'subcategory' => null,
	'item_number' => null,
	'description' => 'some fee!',
	'image_id' => null,
	'cost_price' => 0,
	'unit_price' => 10,
	'unit_price_includes_tax' => 0,
	'inventory_level' =>0,
	'inventory_unlimited' => 1,
	'erange_size' => null, // what is this?
	'quickbooks_income' => null,
	'quickbooks_cogs' => null,
	'quickbooks_assets' => null,
	'max_discount' => 0,
	'quantity' => null,
	'is_unlimited' => 1,
	'reorder_level' => null,
	'food_and_beverage' => null,
	'number_of_sides' => null,
	'location' => null,
	'gl_code' => null,
	'allow_alt_description' => null,
	'is_serialized' => null,
	'invisible' => null,
	'deleted' => null,
	'is_side' => null,
	'soup_or_salad' => null,
	'add_on_price' => null,
	'print_priority' => 1,
	'kitchen_printer' => null,
	'do_not_print' => null,
	'inactive' => null,
	'meal_course_id' => null,
	'prompt_meal_course' => null,
	'do_not_print_customer_receipt' => null,
	'is_fee' => null, // not used
	'taxes' => array(
		array('name' => 'Sales Tax', 'percent' => 10, 'cumulative' => 0)
	),
	'supplier_id' => null,
	'is_shared' => null, // not used?
	'force_tax' => null,

	'item_type' => 'service_fee',
	'parent_item_percent' =>50,
	'whichever_is' => 'more',
	'is_service_fee' => 1
);
