<?php

namespace libraries\fu\acl;

use Codeception\Module\UnitHelper;
use fu\acl\Acl;
use fu\acl\Factory;
use fu\acl\Permission;

class AclTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;


    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testRoles()
    {
        $personId = 114;
        $roleId = 1;
        $subRoleId = 7;
        $courseId = 6270;
        $CI = & get_instance();
        $CI->load->model("Acl_roles");




        $this->specify("Check rollow permissions work at a basic level.",function() use ($personId,$roleId,$courseId) {
            $aclRoles = new \Acl_roles();
            $aclRoles->set_acl_role($personId,$roleId);

            $permission = new \fu\acl\Permissions\Teetimes\Checkedin(["delete"]);

            $acl = new Acl($personId,$courseId);


            $acl->roleAllow($roleId,$permission);
            verify($acl->can($permission))->equals(true);


            $acl->roleDeny($roleId,$permission);
            verify($acl->can($permission))->equals(false);

            $aclRoles->remove_acl_role($personId,$roleId);
        });


        $this->specify("Check role inheritance.",function() use ($personId,$roleId,$courseId,$subRoleId) {
            $aclRoles = new \Acl_roles();
            $aclRoles->set_acl_role($personId,$subRoleId);

            $permission = new \fu\acl\Permissions\Teetimes\Checkedin(["delete"]);

            $acl = new Acl($personId,$courseId);


            $acl->roleAllow($roleId,$permission);
            verify($acl->can($permission))->equals(true);


            $acl->roleDeny($roleId,$permission);
            verify($acl->can($permission))->equals(false);


            $aclRoles->remove_acl_role($personId,$roleId);
        });


    }

}