<?php
namespace libraries\fu\service_fees;


use fu\TestingHelpers\Actor;
use fu\TestingHelpers\TestingObjects\Item;
use fu\TestingHelpers\TestingObjects\Payment;

class CartLibPricingTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;


	protected function _before()
	{
		if(!isset($this->actor)) {
			$CI = get_instance();
			$this->actor = new Actor();
			$this->actor->login();
			$CI->load->library('v2/cart_lib');
		}
	}

	public function testCalculateSubtotal()
	{
		$CI = get_instance();
		$this->specify("Check that calculate_subtotal.",function() use ($CI){
			verify(method_exists($CI->cart_lib,'calculate_subtotal'))->true();
		});

		$this->specify("Check that calculate_subtotal works.",function() use ($CI){
			verify($CI->cart_lib->calculate_subtotal(100,2,50))->equals(100.00);
		});
	}

	public function testCalculateTax()
	{
		$CI = get_instance();
		$this->specify("Check that calculate_tax.",function() use ($CI){
			verify(method_exists($CI->cart_lib,'calculate_tax'))->true();
		});

		$this->specify("Check that calculate_tax works.",function() use ($CI){
			$taxes = array(
				array('name' => 'Sales Tax 1', 'percent' => 6.75, 'cumulative' => 0),
				array('name' => 'Sales Tax 2', 'percent' => 3.25, 'cumulative' => 0)
			);
			verify($CI->cart_lib->calculate_tax(100.00,$taxes,false))->equals(10.00);
			verify($taxes[0]['amount'])->equals(6.75);
			verify($taxes[1]['amount'])->equals((3.25));
			verify($CI->cart_lib->calculate_tax(100.00,$taxes,true))->equals(9.09);
			verify($taxes[0]['amount'])->equals(6.14);
			verify($taxes[1]['amount'])->equals((2.95));
		});
	}

	public function testCalculateItemTax()
	{
		$CI = get_instance();
		$this->specify("Check that calculate_item_tax",function() use ($CI){
			verify(method_exists($CI->cart_lib,'calculate_item_tax'))->true();
		});

		$this->specify("Check that calculate_item_tax works.",function() use ($CI){
			verify($CI->cart_lib->calculate_item_tax(3.00,5228,'item'))->equals(0.23);
		});
	}
}

?>
