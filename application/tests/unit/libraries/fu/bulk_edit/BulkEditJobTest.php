<?php
namespace libraries\fu\bulk_edit {

    use fu\TestingHelpers\CodeIgniterMocks\Customer;

    class BulkEditTest extends \Codeception\TestCase\Test
    {
        use \Codeception\Specify;

        protected function _before(){
            \Codeception\Specify\Config::setDeepClone(false);
        }

        public function testBulkEdit(){

            $job_data = [
                'record_ids' => [1,2,3,4,5],
                'type' => 'customers'
            ];

            $transformer = new \fu\bulk_edit\RecordTransformers\Customer( new Customer() );
            $job = new \fu\bulk_edit\BulkEditJob($job_data);
            $job->set_transformer($transformer);
            $job->transform();

            $results = $job->to_array();

            $this->specify('Total records should be 5', function() use ($results){
                verify($results['total_records'])->equals(5);
            });

            $this->specify('Failed records should be 5', function() use ($results){
                verify($results['records_failed'])->equals(5);
            });

            $this->specify('Completed records should be 0', function() use ($results){
                verify($results['records_completed'])->equals(0);
            });

            $this->specify('Percent complete should be 100', function() use ($results){
                verify($results['percent_complete'])->equals(100);
            });

            $this->specify('Transformed record IDs should be an empty array', function() use ($results){
                verify( count($results['response']['transformed_record_ids']) )->equals(0);
            });

            $this->specify('Error list should contain 5 error messages', function() use ($results){
                verify(count($results['response']['errors']))->equals(5);
            });
        }

        public function testBulkEditHelper(){

            $newList = [
                ['id' => 1],
                ['id' => 2]
            ];

            $existingList = [
                ['id' => 3]
            ];

            $bulkEditHelper = new \fu\bulk_edit\RecordTransformers\Helper();
            $existingList = $bulkEditHelper::listAdd($existingList, 'id', $newList);

            $this->specify('List should have 3 records', function() use ($existingList){
                verify(count($existingList))->equals(3);
            });

            $existingList = $bulkEditHelper::listAdd(null, 'id', $newList);

            $this->specify('A null existing list should now have 2 records', function() use ($existingList){
                verify(count($existingList))->equals(2);
            });

            $this->specify('An empty array list should now have 2 records', function() use ($existingList){
                verify(count($existingList))->equals(2);
            });

            $existingList = [
                ['id' => 3],
                ['id' => 4]
            ];
            $existingList = $bulkEditHelper::listRemove($existingList, 'id', [['id' => 50]]);

            $this->specify('No records should be removed from list', function() use ($existingList){
                verify(count($existingList))->equals(2);
            });

            $existingList = [
                ['id' => 3],
                ['id' => 4]
            ];
            $existingList = $bulkEditHelper::listRemove($existingList, 'id', [['id' => 3]]);

            $this->specify('1 record should be removed from list', function() use ($existingList){
                verify(count($existingList))->equals(1);
            });
        }

        function testCustomerBulkEdit(){

            $job_data = [
                'record_ids' => [1, 2],
                'type' => 'customers',
                'settings' => [

                    // A few scalar fields to test with
                    'state' => [
                        'action' => 'set',
                        'value' => 'CA'
                    ],
                    'city' => [
                        'action' => 'set',
                        'value' => 'SanDiego'
                    ],
                    'email' => [
                        'action' => 'set',
                        'value' => ''
                    ],
                    'member_account_balance' => [
                        'action' => 'increase',
                        'value' => 100
                    ],
                    'loyalty_points' => [
                        'action' => 'decrease',
                        'value' => 125
                    ],
                    'password' => [
                        'action' => 'set',
                        'value' => 'newPassword'
                    ],
                    'member' => [
                        'action' => 'set',
                        'value' => 0
                    ],

                    // Non-scalar fields
                    'groups' => [
                        'action' => 'add',
                        'value' => [
                            ['group_id' => 1]
                        ]
                    ],
                    'passes' => [
                        'action' => 'set',
                        'value' => [
                            ['pass_id' => 1],
                            ['pass_id' => 2],
                        ]
                    ],
                    'minimum_charges' => [
                        'action' => 'remove',
                        'value' => [
                            ['minimum_charge_id' => 10]
                        ]
                    ],
                    'household_members' => [
                        'action' => 'set',
                        'value' => []
                    ]
                ]
            ];

            $customerModelMock = new Customer();
            $transformer = new \fu\bulk_edit\RecordTransformers\Customer($customerModelMock);
            $transformer->setCourseId(1234);

            $job = new \fu\bulk_edit\BulkEditJob($job_data);
            $job->set_transformer($transformer);
            $job->transform();

            $results = $job->to_array();

            $this->specify('Total records should be 2', function() use ($results){
                verify($results['total_records'])->equals(2);
            });

            $this->specify('Failed records should be 0', function() use ($results){
                verify($results['records_failed'])->equals(0);
            });

            $this->specify('Completed records should be 2', function() use ($results){
                verify($results['records_completed'])->equals(2);
            });

            // Test transformer on scalar fields
            $transformer->setRecordId(1);
            $transformer->transform($job_data['settings']);
            $newData = $transformer->getNewData($job_data['settings']);
            $person = $customerModelMock->get(['person_id' => 1]);
            $person = $person[0];

            $this->specify('City and state should match new data', function() use ($newData){
                verify($newData['city'])->equals('SanDiego');
                verify($newData['state'])->equals('CA');
            });

            $this->specify('Email should be empty', function() use ($newData){
                verify($newData['email'])->equals('');
            });

            $this->specify('Member should be 0 (false)', function() use ($newData){
                verify($newData['member'])->equals(0);
            });

            $this->specify('New member_balance should be 100 dollars greater than previous balance', function() use ($newData, $person){
                verify($newData['member_account_balance'])->equals( $person['member_account_balance'] + 100 );
            });

            $this->specify('Loyalty should be 125 points less than previous total', function() use ($newData, $person){
                verify($newData['loyalty_points'])->equals( $person['loyalty_points'] - 125 );
            });

            $this->specify("Password should be set, existing username should also be included", function() use ($newData, $person){
                verify($newData['password'])->equals('newPassword');
                verify($newData['username'])->notEmpty();
            });

            // Test non-scalar fields
            $this->specify("1 new group should be added to group list", function() use ($newData, $person){
                verify(count($newData['groups']))->equals(3);
                verify($newData['groups'][2])->notEmpty();
                verify($newData['groups'][2]['group_id'])->equals(1);
            });

            $this->specify("Only 2 new passes should be set", function() use ($newData, $person){
                verify(count($newData['passes']))->equals(2);

                verify($newData['passes'][0])->notEmpty();
                verify($newData['passes'][0]['pass_id'])->equals(1);

                verify($newData['passes'][1])->notEmpty();
                verify($newData['passes'][1]['pass_id'])->equals(2);
            });

            $this->specify("Only 1 minimum charge should remain. Minimum charge with ID 10 should be removed", function() use ($newData, $person){
                verify(count($newData['minimum_charges']))->equals(1);
                verify($newData['minimum_charges'][0])->notEmpty();
                verify($newData['minimum_charges'][0]['minimum_charge_id'])->equals(12);
            });

            $this->specify("Household members should be cleared", function() use ($newData, $person){
                verify(count($newData['household_members']))->equals(0);
            });
        }
    }
}