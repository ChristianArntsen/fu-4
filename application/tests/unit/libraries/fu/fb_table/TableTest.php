<?php
// TableTest.php
// Beach head for testing F&B Tables

namespace libraries\fu\service_fees;

use fu\TestingHelpers\Actor;
use fu\TestingHelpers\TestingObjects\FBItem;
use fu\TestingHelpers\TestingObjects\Payment;

class TableTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	protected function _before()
	{
		if (!isset($this->actor)) {
			$CI = get_instance();
			$this->actor = new Actor();
			$this->actor->login();
			$CI->load->model('Table');
			$this->table = new \Table();
		}
	}

	public function testSave()
	{
		$table = $this->table;
		$this->specify("Check that the save method exists",function() use ($table){
			verify(method_exists($table,'save'))->true();
		});

		$this->specify("Check that the save method works", function() use ($table){
			$sale_id = $table->save(array(),21557,12,'',array(),false,404);

			verify(is_int($sale_id*1))->true();
			verify($sale_id)->greaterThan(0);
		});
	}

	public function testGetIdByTableNumber()
	{
		$table = $this->table;
		$this->specify("Check that the get_id_by_table_number method exists",function() use ($table){
			verify(method_exists($table,'get_id_by_table_number'))->true();
		});

		$this->specify("Check that the get_id_by_table_number method works",function() use ($table){
			$sale_id = $table->get_id_by_table_number(404);
			verify(is_int($sale_id*1))->true();
			verify($sale_id)->greaterThan(0);
		});
	}

	public function testExists()
	{
		$table = $this->table;
		$this->specify("Check that the exists method exists",function() use ($table){
			verify(method_exists($table,'exists'))->true();
		});

		$this->specify("Check that the exists method works",function() use ($table){
			$sale_id = $table->get_id_by_table_number(404);
			verify($table->exists($sale_id))->true();
		});
	}

	public function testSaveCustomer()
	{
		$table = $this->table;
		$this->specify("Check that the save_customer method exists",function() use ($table){
			verify(method_exists($table,'save_customer'))->true();
		});
	}

	public function testGetCustomer()
	{
		$table = $this->table;
		$this->specify("Check that the get_customer method exists",function() use ($table){
			verify(method_exists($table,'get_customer'))->true();
		});
	}

	public function testSaveItem()
	{
		$itemObj = new FBItem();
		$item = $itemObj->getArray();
		$item['line']=1;
		$item['seat']=1;
		$item['is_ordered']=0;
		$item['quantity']=1;
		$item['discount']=0;
		$item['price']=3.00;
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(404);
		$this->specify("Check that the save_item method exists",function() use ($table){
			verify(method_exists($table,'save_item'))->true();
		});


		$this->specify("Check that the save_item method works",function() use ($table,$item,$sale_id){
			verify($table->save_item($sale_id, $item['line'], $item))->true();
		});
	}

	public function testGetItem()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(404);
		$this->specify("Check that the get_item method exists",function() use ($table){
			verify(method_exists($table,'get_item'))->true();
		});

		$this->specify("Check that the get_item method works",function() use ($table,$sale_id){
			$item = $table->get_item(1,$sale_id);
			verify(is_array($item))->true();
			verify(array_key_exists('item_id',$item));
			verify($item['price'])->equals(3.00);
			verify($item['quantity'])->equals(1.00);
			verify($item['is_ordered'])->equals(0);
			verify($item['is_paid'])->equals(0);
			verify($item['total_splits'])->equals(0);
			verify($item['force_tax'])->null();
		});
	}

	public function testGetItems()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(404);
		$this->specify("Check that the get_items method exists",function() use ($table){
			verify(method_exists($table,'get_items'))->true();
		});

		$this->specify("Check that the get_items method works",function() use ($table,$sale_id){
			$items = $table->get_items($sale_id);
			verify(is_array($items))->true();
			verify(array_key_exists(0,$items))->true();
			verify(array_key_exists(1,$items))->false();
		});
	}

	public function testDeleteItem()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(404);
		$this->specify("Check that the delete_item method exists",function() use ($table){
			verify(method_exists($table,'delete_item'))->true();
		});

		$itemObj = new FBItem();
		$item = $itemObj->getArray();
		$item['line']=2;
		$item['seat']=1;
		$item['is_ordered']=0;
		$item['quantity']=1;
		$item['discount']=0;
		$item['price']=3.00;
		$table = $this->table;
		$table->save_item($sale_id, $item['line'], $item);

		$this->specify("check that the delete_item method works",function() use ($table,$sale_id){
			$item = $table->get_item(2,$sale_id);
			verify($item['line'])->equals(2);
			$items = $table->get_items($sale_id);
			verify(is_array($items))->true();
			verify(array_key_exists(0,$items))->true();
			verify(array_key_exists(1,$items))->true();
			verify(array_key_exists(2,$items))->false();

			$table->delete_item($sale_id,2);

			$item = $table->get_item(2,$sale_id);
			verify(empty($item))->true();
			$items = $table->get_items($sale_id);
			verify(array_key_exists(0,$items))->true();
			verify($items[0]['line'])->equals(1);
			verify(array_key_exists(1,$items))->false();

		});
	}

	public function testGetSaleItems()
	{
		$table = $this->table;
		$this->specify("Check that the get_sale_items method exists",function() use ($table){
			verify(method_exists($table,'get_sale_items'))->true();
		});
	}

	public function testGetSaleTotal()
	{
		$table = $this->table;
		$this->specify("Check that the get_sale_total method exists",function() use ($table){
			verify(method_exists($table,'get_sale_total'))->true();
		});
	}

	public function testAddPayment()
	{
		$table = $this->table;
		$this->specify("Check that the add_payment method exists",function() use ($table){
			verify(method_exists($table,'add_payment'))->true();
		});
	}

	public function testGetSalePayments()
	{
		$table = $this->table;
		$this->specify("Check that the get_sale_payments method exists",function() use ($table){
			verify(method_exists($table,'get_sale_payments'))->true();
		});
	}

	public function testGetPayments()
	{
		$table = $this->table;
		$this->specify("Check that the get_payments method exists",function() use ($table){
			verify(method_exists($table,'get_payments'))->true();
		});
	}

	public function testDeletePayment()
	{
		$table = $this->table;
		$this->specify("Check that the delete_payment method exists",function() use ($table){
			verify(method_exists($table,'delete_payment'))->true();
		});
	}

	public function testMarkItemsPaid()
	{
		$table = $this->table;
		$this->specify("Check that the mark_items_paid method exists",function() use ($table){
			verify(method_exists($table,'mark_items_paid'))->true();
		});
	}

	public function testMarkItemsUnpaid()
	{
		$table = $this->table;
		$this->specify("Check that the mark_items_unpaid method exists",function() use ($table){
			verify(method_exists($table,'mark_items_unpaid'))->true();
		});
	}

	public function testDelete()
	{
		$table = $this->table;
		$this->specify("Check that the delete method exists",function() use ($table){
			verify(method_exists($table,'delete'))->true();
		});

		$this->specify("Check that the delete method works",function() use ($table){
			$sale_id = $table->get_id_by_table_number(404);
			verify($table->exists($sale_id))->true();
			verify($table->delete($sale_id))->true();
			verify($table->exists($sale_id))->false();
		});
	}
}
?>

