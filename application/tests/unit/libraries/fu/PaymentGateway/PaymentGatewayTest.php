<?php
namespace fu\PaymentGateway;

use fu\PaymentGateway\MercuryPaymentGateway;

class PaymentGatewayTest extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	private $gateway;

	protected function _before()
	{
		$this->gateway = new MercuryPaymentGateway([]);

	}

	public function testInterface()
	{
		$this->assertInstanceOf('fu\PaymentGateway\PaymentGateway',$this->gateway);
		$this->assertInstanceOf('fu\PaymentGateway\MercuryPaymentGateway',$this->gateway);
		$this->assertTrue(method_exists($this->gateway,'return'));
		$this->assertTrue(method_exists($this->gateway,'charge'));
		$this->assertTrue(method_exists($this->gateway,'void'));
		$this->assertTrue(method_exists($this->gateway,'setStoredPayment'));
		$this->assertTrue(method_exists($this->gateway,'getAuthorizedAmount'));
		$this->assertTrue(method_exists($this->gateway,'getLastError'));

	}

	public function testChargeNumeric()
	{
		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Amount must be numeric');
		$this->gateway->charge('not numeric');

	}

	public function testRefundNumeric()
	{
		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Amount must be numeric');
		$this->gateway->return('not numeric');

	}

	public function testQ()
	{
		$this->setExpectedException('TypeError',
			'Argument 1 passed to fu\PaymentGateway\MercuryPaymentGateway::setStoredPayment() '.
			'must be an instance of fu\PaymentGateway\StoredPaymentInfo, string given');
		$this->gateway->setStoredPayment('not numeric');

	}

}

?>