<?php
namespace fu\PaymentGateway;

use foreup\rest\models\entities\ForeupCourses;

class PaymentGatewayFactoryTest extends \Codeception\TestCase\Test
{
	private $factory;


	protected function _before()
	{
		//$this->factory = new PaymentGatewayFactory();

	}

	public function testConstructErrors()
	{
		$this->setExpectedException('TypeError',
			'Argument 1 passed to fu\PaymentGateway\PaymentGatewayFactory::__construct() must be of the type array, string given');
		new PaymentGatewayFactory('not course or terminal');
	}

	public function testConstructNoId()
	{
		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Missing id parameter in PaymentGatewayFactory config');
		new PaymentGatewayFactory(['ets_key'=>';ajlkffa;jkl;asdfa;aflsd']);
	}

	public function testConstructEts()
	{
		$factory = new PaymentGatewayFactory(['id'=>6270,'ets_key'=>'494691720']);
		$this->assertInstanceOf('fu\PaymentGateway\PaymentGatewayFactory',$factory);
		$gateway = $factory->getPaymentGateway();
		$this->assertInstanceOf('fu\PaymentGateway\EtsPaymentGateway',$gateway);
	}

	public function testConstructE2e()
	{
		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'E2ePaymentGateway not implemented');
		new PaymentGatewayFactory(['id'=>12345,'mercury_e2e_id'=>';klj;afsjasj;lasdfkl;fasd','mercury_e2e_password'=>'jk;s;afsd;;a;asdf']);
	}

	public function testConstructMercury()
	{
		$factory = new PaymentGatewayFactory(['id'=>6270,'mercury_id'=>'494691720','mercury_password'=>'KRD%8rw#+p9C13,T']);
		$this->assertInstanceOf('fu\PaymentGateway\PaymentGatewayFactory',$factory);
		$gateway = $factory->getPaymentGateway();
		$this->assertInstanceOf('fu\PaymentGateway\MercuryPaymentGateway',$gateway);
	}
}

?>