<?php
namespace fu\PaymentGateway;

class MercuryAchPaymentGatewayTest extends \Codeception\TestCase\Test
{
	/**
	 * @var MercuryAchPaymentGateway
	 */
	private $gateway;

	protected function _before()
	{
		$this->gateway = new MercuryAchPaymentGateway(['id'=>6270,'mercury_id'=>'494691720','mercury_password'=>'KRD%8rw#+p9C13,T']);

	}

	public function testChargeInvalidArgument()
	{
		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Amount must be numeric');
		$this->gateway->charge('not numeric');

	}

	public function testSetStoredPaymentInvalidArgument()
	{
		$this->setExpectedException('TypeError',
			'Argument 1 passed to fu\PaymentGateway\MercuryAchPaymentGateway::setStoredPayment() '.
			'must be an instance of fu\PaymentGateway\StoredPaymentInfo, string given');
		$this->gateway->setStoredPayment('not numeric');

	}

	public function testCharge()
	{
		$config = array(
			'element_account_id' => 1045621,
			'element_account_token' => '4C779213331677A5A89D09F2F71C2D7884F84A08DAB0165F29385BBF0648E02827A2C601',
			'element_acceptor_id' => 3928907,
			'element_application_id' => 0,
			"cardholder_name"=>'Mercury ACH Test',
			"invoice"=>123
		);

		$storedPayment = new StoredPaymentInfo('ach',$config['element_account_token'],
			$config['invoice'],$config['cardholder_name'],$config['element_account_id'],'ACH');
		$credentials = new PaymentGatewayCredentials(0,'mercuryAch',$config);

		$this->gateway->setStoredPayment($storedPayment);
		$this->gateway->setPaymentGatewayCredentials($credentials);

		$this->gateway->charge(33.39,true);
	}

}
?>
