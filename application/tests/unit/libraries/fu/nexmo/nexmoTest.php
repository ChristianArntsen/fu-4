<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 6/12/2017
 * Time: 11:45 AM
 */
class nexmoTest extends \Codeception\TestCase\Test
{
	public function atestSearch()
	{
		$CI = get_instance();
		$CI->load->model("Course");
		$course_info = $CI->Course->get_info(6270);
		$course_info->phone = "18013765";
		$nexmo = new \fu\nexmo\Nexmo($course_info,$CI->db,$CI->config->item('api_key', 'nexmo'),$CI->config->item('api_secret', 'nexmo'));

		$hasNumber = $nexmo->has_number();
		$numberBought = $nexmo->buy_number(false);

		self::assertTrue(!empty($numberBought));
		$nexmo->save_number($numberBought);
		$to = "18013765991";
		$success = $nexmo->send_message($to,"Yo");
		self::assertTrue($success['messages'][0]->status == 0);
	}

	public function testHistory()
	{
		$CI = get_instance();
		$CI->load->model("Course");
		$course_info = $CI->Course->get_info(6270);
		$course_info->phone = "18013765";
		$nexmo = new \fu\nexmo\Nexmo($course_info,$CI->db,$CI->config->item('api_key', 'nexmo'),$CI->config->item('api_secret', 'nexmo'));

		$CI->load->model("Personal_messages");
		$CI->Personal_messages->set_course_id(6238);
		$history = $CI->Personal_messages->get_message_history(568);
		$new_message = $CI->Personal_messages->has_new_messages("2017-06-01 12:30:01",568);
	}
}