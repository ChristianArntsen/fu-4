<?php
// ReceiptTest.php
// Beach head for testing F&B Receipts

namespace libraries\fu\service_fees;

use fu\TestingHelpers\Actor;
use fu\TestingHelpers\TestingObjects\FBItem;
use fu\TestingHelpers\TestingObjects\Payment;

class ReceiptTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	protected function _before()
	{
		if (!isset($this->actor)) {
			$CI = get_instance();
			$this->actor = new Actor();
			$this->actor->login();
			$CI->load->model('Table');
			$this->table = new \Table();
			$CI->load->model('table_receipt');
			$this->receipt = new \Table_Receipt();
		}
	}

	public function testNewReceipt()
	{
		$table = $this->table;
		$this->sale_id = $sale_id = $table->save(array(),21557,12,'',false,false,501);
		//verify($this->table->delete($this->sale_id))->true();
		$receipt = $this->receipt;
		$this->specify("Check that the new_receipt method exists",function() use ($receipt){
			verify(method_exists($receipt,'new_receipt'))->true();
		});

		$this->specify("Check that the new_receipt method works",function() use ($receipt,$sale_id){
			$receipt_id = $receipt->new_receipt($sale_id);
			verify(is_int($receipt_id*1))->true();
			verify($receipt_id)->greaterThan(0);
		});
	}

	public function testGet()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$this->specify("Check that the get method exists",function() use ($receipt){
			verify(method_exists($receipt,'get'))->true();
		});

		$this->specify("Check that the get method works",function() use ($receipt,$sale_id){
			$receipts = $receipt->get($sale_id);
			verify(is_array($receipts))->true();
			verify(array_key_exists(0,$receipts))->true();
			verify(array_key_exists(1,$receipts))->false();
		});
	} 

	public function testDelete()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$this->specify("Check that the delete method exists",function() use ($receipt){
			verify(method_exists($receipt,'delete'))->true();
		});

		$this->specify("Check that the delete method works",function() use ($receipt,$sale_id){
			$receipts = $receipt->get($sale_id);
			verify(is_array($receipts))->true();
			verify(array_key_exists(0,$receipts))->true();
			verify(array_key_exists(1,$receipts))->false();

			verify($receipt->delete($sale_id,$receipts[0]['receipt_id']));

			$receipts = $receipt->get($sale_id);
			verify(is_array($receipts))->true();
			verify(array_key_exists(0,$receipts))->false();
		});

	}

	public function testSave()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$this->specify("Check that the save method exists",function() use ($receipt){
			verify(method_exists($receipt,'save'))->true();
		});

		$receipt_id=$receipt->save($sale_id);
		$this->receipt_id=$receipt_id;
		$this->specify("Check that the save method works",function() use ($receipt,$sale_id,$receipt_id){
			verify(is_int($receipt_id*1))->true();
			verify($receipt_id)->greaterThan(0);
		});
	}

	public function testGetItems()
	{
		$itemObj = new FBItem();
		$item = $itemObj->getArray();
		$item['line']=1;
		$item['seat']=1;
		$item['is_ordered']=0;
		$item['quantity']=1;
		$item['discount']=0;
		$item['price']=3.00;
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$receipts = $receipt->get($sale_id);
		$receipt_id = $receipts[0]['receipt_id'];


		$this->specify("Check that the get_items method exists",function() use ($receipt){
			verify(method_exists($receipt,'get_items'))->true();
		});

		// save the item first to the table
		$this->table->save_item($sale_id,$item['line'],$item);
		// then assign it to the receipt
		verify($receipt->save($sale_id,$receipt_id,array($item)))->equals($receipt_id);

		$this->specify("Check that the get_items method works",function() use ($receipt,$sale_id,$receipt_id){
			$items = $receipt->get_items($sale_id,$receipt_id);

			verify(is_array($items))->true();
			verify(array_key_exists(0,$items))->true();
			verify(array_key_exists(1,$items))->false();
		});
	}

	public function testHasPayments()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$this->specify("Check that the has_payments method exists",function() use ($receipt){
			verify(method_exists($receipt,'has_payments'))->true();
		});
	}

	public function testCanDeleteItem()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$receipts = $receipt->get($sale_id);
		$receipt_id = $receipts[0]['receipt_id'];
		$items = $receipt->get_items($sale_id,$receipt_id);
		$item = $items[0];

		$this->specify("Check that the can_delete_item method exists",function() use ($receipt){
			verify(method_exists($receipt,'can_delete_item'))->true();
		});


		$this->specify("Check that the can_delete_item method works",function() use ($receipt,$item,$sale_id,$receipt_id){
			$can_delete = $receipt->can_delete_item($sale_id,1);
			verify($can_delete)->false();// Will not let the receipt delete non-split items

			$new_receipt_id = $receipt->new_receipt($sale_id);
			$receipt->save($sale_id,$new_receipt_id,array($item));// simulates a split

			$can_delete = $receipt->can_delete_item($sale_id,1);
			verify($can_delete)->true();// Will not let the receipt delete non-split items
		});


	}

	public function testDeleteItem()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$receipts = $receipt->get($sale_id);
		$receipt_id = $receipts[0]['receipt_id'];
		$this->specify("Check that the delete_item method exists",function() use ($receipt){
			verify(method_exists($receipt,'delete_item'))->true();
		});

		$this->specify("Check that the delete_item method works",function() use ($receipt,$sale_id,$receipt_id){
			$items = $receipt->get_items($sale_id,$receipt_id);
			verify(is_array($items))->true();
			verify(array_key_exists(0,$items))->true();
			verify(array_key_exists(1,$items))->false();

			verify($receipt->delete_item($items[0]['item_id'],$items[0]['line'],$sale_id,1))->true();

			$items = $receipt->get_items($sale_id,$receipt_id);
			verify(is_array($items))->true();
			verify(array_key_exists(0,$items))->false();
		});
	}

	public function testCalculateAutoGratuity()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$this->specify("Check that the calculate_auto_gratuity method exists",function() use ($receipt){
			verify(method_exists($receipt,'calculate_auto_gratuity'))->true();
		});

		$this->specify("Check that the calculate_auto_gratuity method works",function() use ($receipt){
			$total = 100;
			$gratuity = 21.75;
			verify($receipt->calculate_auto_gratuity($total,$gratuity))->equals(21.75);
			verify($receipt->calculate_auto_gratuity($total,$gratuity,'dollar'))->equals(21.75);
		});

	}

	public function testGetServiceFee()
	{
		$CI = get_instance();
		$CI->db->trans_start(true);

		$q = $CI->db->select('service_fee_active,service_fee_tax_1_rate')->from('courses')
			->where('course_id',6270);
		$old_ag_settings = $q->get()->result_array();
		verify(is_array($old_ag_settings))->true();
		verify(count($old_ag_settings))->equals(1);
		verify(is_array($old_ag_settings[0]))->true();
		verify(count($old_ag_settings[0]))->equals(2);

		$CI->db->update('courses',array('service_fee_active'=>1,'service_fee_tax_1_rate'=>10.00),array('course_id'=>6270));

		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$this->specify("Check that the get_service_fee method exists",function() use ($receipt){
			verify(method_exists($receipt,'get_service_fee'))->true();
		});

		$total = 100;
		$gratuity = 21.75;
		$pct_taxbl = $receipt->get_service_fee($total, $gratuity);
		$pct_notax = $receipt->get_service_fee($total, $gratuity,'percentage',0);
		$dlr_taxbl = $receipt->get_service_fee($total, $gratuity,'dollar');
		$dlr_notax = $receipt->get_service_fee($total, $gratuity,'dollar',0);

		$this->specify("Check that the get_service_fee method works",function() use ($pct_taxbl){
			verify(is_array($pct_taxbl));
			verify($pct_taxbl['subtotal'])->equals(21.75);
			//TODO: add tax to the service fee object
			verify($pct_taxbl['tax'])->greaterThan(0);
		});

		$this->specify("Check that the get_service_fee method works with no tax",function() use ($pct_notax){
			verify(is_array($pct_notax));
			verify($pct_notax['subtotal'])->equals(21.75);
		});

		$this->specify("Check that the get_service_fee method works with dollar amount",function() use ($dlr_taxbl){
			verify(is_array($dlr_taxbl));
			verify($dlr_taxbl['subtotal'])->equals(21.75);
			//TODO: add tax to the service fee object
			verify($dlr_taxbl['tax'])->greaterThan(0);
		});

		$this->specify("Check that the get_service_fee method works with dollar amount and no tax",function() use ($dlr_notax){
			verify(is_array($dlr_notax));
			verify($dlr_notax['subtotal'])->equals(21.75);
		});
		$CI->db->update('courses',array('service_fee_active'=>$old_ag_settings[0]['service_fee_active'],'service_fee_tax_1_rate'=>$old_ag_settings[0]['service_fee_tax_1_rate']),array('course_id'=>6270));
		$CI->db->trans_complete();
	}

	public function testIsPaid()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$this->specify("Check that the is_paid method exists",function() use ($receipt){
			verify(method_exists($receipt,'is_paid'))->true();
		});

		$this->specify("Check that the is_paid method works",function() use ($receipt){
			verify($receipt->is_paid())->false();
		});
	}

	public function testCleanup()
	{
		$table = $this->table;
		$sale_id = $table->get_id_by_table_number(501);
		$receipt = $this->receipt;
		$this->specify("Check that the delete method exists",function() use ($receipt){
			verify(method_exists($receipt,'delete'))->true();
		});

		verify($this->table->delete($sale_id))->true();
	}


}
?>

