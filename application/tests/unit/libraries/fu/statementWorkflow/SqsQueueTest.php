<?php
namespace fu\statementWorkflow\classes;

use Codeception\Specify;

class SqsQueueTest extends \Codeception\TestCase\Test
{
	use Specify;
	private $queue,$fakeQueue;
	protected function _before()
	{
		$this->queue = new SqsQueue();
		$this->fakeQueue = new FakeQueue();
	}

	protected function _after()
	{
		//
	}

	public function testInterface()
	{
		$object = $this->queue;

		$this->assertTrue(method_exists($object,'getQueue'));
		$this->assertTrue(method_exists($object,'getQueueClient'));
		$this->assertTrue(method_exists($object,'deleteQueue'));
		$this->assertTrue(method_exists($object,'enqueueMessage'));
		$this->assertTrue(method_exists($object,'enqueueMessages'));
		$this->assertTrue(method_exists($object,'fetchMessages'));
		$this->assertTrue(method_exists($object,'deleteMessage'));
		$this->assertTrue(method_exists($object,'getLastError'));
		$this->assertTrue(method_exists($object,'resetLastError'));

		$object = $this->fakeQueue;

		$this->assertTrue(method_exists($object,'getQueue'));
		$this->assertTrue(method_exists($object,'getQueueClient'));
		$this->assertTrue(method_exists($object,'deleteQueue'));
		$this->assertTrue(method_exists($object,'enqueueMessage'));
		$this->assertTrue(method_exists($object,'enqueueMessages'));
		$this->assertTrue(method_exists($object,'fetchMessages'));
		$this->assertTrue(method_exists($object,'deleteMessage'));
		$this->assertTrue(method_exists($object,'getLastError'));
		$this->assertTrue(method_exists($object,'resetLastError'));
	}

	public function testFakeQueue()
	{
		$queue = $this->fakeQueue;

		$string = 'message string';

		$queue->getQueue('test-queue');

		$queue->enqueueMessage('test-queue',$string);

		$messages = $queue->fetchMessages('test-queue');

		$this->assertEquals(1,count($messages));
		$this->assertEquals($messages[0]->get('Body'),$string);

		$queue->deleteMessage('test-queue',$messages[0]);

		$messages2 = $queue->fetchMessages('test-queue');

		$this->assertEmpty($messages2);

		$strings = ['string1', 'string2'];

		$queue->enqueueMessages('test-queue',$strings);


		$messages = $queue->fetchMessages('test-queue');

		$this->assertEquals(2,count($messages));
		foreach($messages as $message){
			$this->assertContains($message->get('Body'),$strings);
		}

		$queue->deleteMessage('test-queue',$messages[0]);

		$messages = $queue->fetchMessages('test-queue');

		$this->assertEquals(1,count($messages));

		$queue->deleteMessage('test-queue',$messages[0]);

		$messages2 = $queue->fetchMessages('test-queue');

		$this->assertEmpty($messages2);

		$queue->deleteQueue('test-queue');
	}
}
?>