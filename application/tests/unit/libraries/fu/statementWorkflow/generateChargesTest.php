<?php
namespace fu\statementWorkflow\classes;

use Codeception\Specify;
use foreup\rest\models\entities\ForeupRepeatableAccountRecurringChargeItems;
use fu\statementWorkflow\generateCharges;
use fu\statementWorkflow\generateChargeSale;
use \models\Repeatable\Row as RepeatableObject;
use fu\application\tests\_helpers\memberBillingSetupHelper;

class generateChargesTest extends \Codeception\TestCase\Test
{
	use Specify;
	private $generator, $processor, $fakeQueue;
	private static $CI;

	public static function setUpBeforeClass(){

		self::$CI =& get_instance();
	}

	protected function _before()
	{
		$this->fakeQueue = new FakeQueue();
		$this->generator = new generateCharges($this->fakeQueue);
		$this->processor = new generateChargeSale($this->fakeQueue);

	}

	protected function _after()
	{
		unset($this->generator);
		unset($this->processor);

	}

	private static function resetData(){

		$sql = <<<SQL
            SET FOREIGN_KEY_CHECKS = 0;

            DELETE FROM `foreup_account_recurring_charge_items`;
            DELETE FROM `foreup_account_recurring_charges`;
            DELETE FROM `foreup_account_recurring_statements`;
            DELETE FROM `foreup_repeatable`;

            -- Recurring Charge 1 --
            INSERT INTO `foreup_account_recurring_statements` VALUES (1, NULL, '', NULL, '2017-03-20 16:39:07', 12, 6270, 1, NULL, NULL, 1, 1, 1, 1, 0, 1, 14, 1, 0, 3, 1, 4, 1, 1, 1, 'Footer here', 'Thank you for your business!', 'Terms and conditions here', 1, 1, 20.00, 'fixed', 1);
            INSERT INTO `foreup_repeatable` VALUES (1, 'recurring_statement', 1, NULL, '2030-02-01 00:00:00', 'MONTHLY', NULL, NULL, 1, NULL, NULL, NULL, NULL, '1', NULL, NULL, '0', '0', '0', '1');

            INSERT INTO `foreup_account_recurring_charges` VALUES (1, 'Testing monthly membership', NULL, '2017-03-20 16:39:07', 12, 1, 6270, 1, NULL, NULL, '', 1);

            INSERT INTO `foreup_account_recurring_charge_items` VALUES (1, 1, 1994, 'item', NULL, 0.00, 2.00, 1, 2);
            INSERT INTO `foreup_repeatable` VALUES (2, 'recurring_charge_item', 1, NULL, '2030-03-05 00:00:00', 'MONTHLY', NULL, NULL, 1, NULL, NULL, NULL, NULL, '5', NULL, NULL, '0', '0', '0', '1');

            INSERT INTO `foreup_account_recurring_charge_items` VALUES (2, 1, 5035, 'item', NULL, 0.00, 1.00, 1, 3);
            INSERT INTO `foreup_repeatable` VALUES (3, 'recurring_charge_item', 2, NULL, '2030-03-05 00:00:00', 'MONTHLY', NULL, NULL, 1, NULL, NULL, NULL, NULL, '5', NULL, NULL, '0', '0', '0', '1');

            -- Recurring Charge 2 --
            INSERT INTO `foreup_account_recurring_statements` VALUES (2, NULL, '', NULL, '2017-05-10 15:28:48', 12, 6238, 1, NULL, NULL, 1, 1, 1, 1, 0, 1, 14, 1, 0, 3, 1, 4, 1, 1, 1, 'Footer here', 'Thank you for your business!', 'Terms and conditions here', 1, 1, 5.00, 'percent', 1);

            INSERT INTO `foreup_repeatable` VALUES (4, 'recurring_statement', 2, NULL, '2030-01-01', 'MONTHLY', NULL, NULL, NULL, NULL, '1,4,7,10', NULL, NULL, '1', NULL, NULL, '0', '0', '0', NULL);

            INSERT INTO `foreup_account_recurring_charges` VALUES (2, 'Quarterly Bill', NULL, '2017-05-10 15:28:49', 12, 1, 6238, 1, NULL, NULL, '', 2);

            INSERT INTO `foreup_account_recurring_charge_items` VALUES (3, 1, 5044, 'item', 250, 0.00, 1.00, 2, 5);
            INSERT INTO `foreup_repeatable` VALUES (5, 'recurring_charge_item', 3, NULL, '2030-01-31 00:00:00', 'MONTHLY', NULL, NULL, 1, NULL, NULL, NULL, NULL, '-1', NULL, NULL, '0', '0', '0', '1');

            INSERT INTO `foreup_account_recurring_charge_items` VALUES (4, 2, 5036, 'item', NULL, 0.00, 1.00, 2, 6);
            INSERT INTO `foreup_repeatable` VALUES (6, 'recurring_charge_item', 4, NULL, '2030-01-01 00:00:00', 'WEEKLY', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, '0', '0', '0', NULL);

            -- Make sure we update the increments table since we manually inserted a statement
            -- DELETE FROM `foreup_course_increments` WHERE type = 'account_statements';
            -- INSERT INTO `foreup_course_increments` VALUES (6270, 'account_statements', 1);
            -- INSERT INTO `foreup_course_increments` VALUES (6238, 'account_statements', 1);

            SET FOREIGN_KEY_CHECKS = 1;
SQL;

		$queries = explode(';', $sql);
		foreach($queries as $query){
			if(empty($query)) continue;
			self::$CI->db->query(trim($query));
		}
	}


	public function testInterface()
	{
		$generator = $this->generator;
		$processor = $this->processor;

		$this->assertTrue(method_exists($generator,'__construct'));
		$this->assertTrue(method_exists($generator,'_get_queue'));
		$this->assertTrue(method_exists($generator,'run'));
		$this->assertTrue(method_exists($generator,'fetchRepeatables'));
		$this->assertTrue(method_exists($generator,'fetchRepeatableCustomers'));
		$this->assertTrue(method_exists($generator,'fetchItems'));
		$this->assertTrue(method_exists($generator,'fetchCharges'));
		$this->assertTrue(method_exists($generator,'repeatableEntityToArray'));
		$this->assertTrue(method_exists($generator,'fetchRecurringChargeCustomers'));
		$this->assertTrue(method_exists($generator,'customersEntityToArray'));

		$this->assertTrue(method_exists($processor,'__construct'));
		$this->assertTrue(method_exists($processor,'_get_queue'));
		$this->assertTrue(method_exists($processor,'_set_input_queue'));
		$this->assertTrue(method_exists($processor,'run'));
		$this->assertTrue(method_exists($processor,'associateInvoiceWithCharge'));
		$this->assertTrue(method_exists($processor,'handle_error'));
		$this->assertTrue(method_exists($processor,'extractMessage'));
		$this->assertTrue(method_exists($processor,'generateInvoiceObject'));
		$this->assertTrue(method_exists($processor,'saveInvoice'));
//		$this->assertTrue(method_exists($processor,'repeatableEntityToArray'));

	}

	public function testFetch()
	{
		$generator = $this->generator;
		$res = $generator->fetchRepeatables('2217-03-08');
		$this->assertGreaterThan(0,$res->count());
		$res2 = $res->next();
		$this->assertNotEmpty($res2);
		$item = $res2->getItem();
		$this->assertNotEmpty($item);
		$charge = $item->getRecurringCharge();
		$this->assertNotEmpty($charge);
		$res = $generator->fetchItems($res);
		$this->assertGreaterThan(0,$res->count());
		$res2 = $res->next();
		$this->assertNotEmpty($res2);
		$res = $generator->fetchCharges($res);
		$this->assertGreaterThan(0,$res->count());

	}

	public function testRepeatableEntityToArray()
	{
		$generator = $this->generator;

		$repeatable = new ForeupRepeatableAccountRecurringChargeItems();
		$repeatable->setResourceId(1);
		$repeatable->setFreq('MONTHLY');
		$repeatable->setBymonthday(['15']);
		$repeatable->validate();
		$result = $generator->repeatableEntityToArray($repeatable);
		$this->assertNotEmpty($result);
		$ro = new RepeatableObject();
		$ro->createFromArray($result);
		$next_time = $ro->getNextCalculatedOccurrence();
		$repeatable->setNextOccurence($next_time);
		$repeatable->validate();

	}

	public function testQueue()
	{

		self::resetData();
		$prefix = '';
		if(ENVIRONMENT !== 'production'){
			$prefix = ENVIRONMENT.'_';
		}

		$this->assertEquals('testing_',$prefix);

		$generator = $this->generator;
		$queue = &$generator->_get_queue();
		$generator->run('2117-04-06',true);
		$messages = $queue->fetchMessages($prefix.'ForeupRepeatableAccountRecurringChargeItemsToCustomers');
		$this->assertNotEmpty($messages);
		$body = $messages[0]->get('Body');
		$this->assertNotEmpty($body);
		$result = json_decode($body,true);
		$this->assertNotEmpty($result);
		$ro = new RepeatableObject();
		$ro->createFromArray($result['repeatable']);
		$next_time = $ro->getNextCalculatedOccurrence();
		$this->assertNotEmpty($next_time);
		$this->assertNotEmpty($result['customer']);
		$this->assertGreaterThan(0,$result['customer']['person_id']);

		$this->processor->run('2117-08-03', true);
	}

	public function testProrating(){

	    $startDate = new \DateTime('2000-01-01');
        $endDate = new \DateTime('2000-02-01');
	    $curDate = new \DateTime('2000-01-08');

	    $amount = 100.00;
	    $prorated = $this->processor->getProratedAmount($amount, $startDate, $endDate, $curDate);
        $this->assertEquals(77.40, $prorated);

        $amount = 43.75;
        $prorated = $this->processor->getProratedAmount($amount, $startDate, $endDate, $curDate);
        $this->assertEquals(33.86, $prorated);

        $startDate = new \DateTime('2000-01-01');
        $endDate = new \DateTime('2000-04-01');
        $curDate = new \DateTime('2000-02-02');

        $amount = 77.77;
        $prorated = $this->processor->getProratedAmount($amount, $startDate, $endDate, $curDate);
        $this->assertEquals(50.39, $prorated);

        $curDate = new \DateTime('2000-03-31');
        $amount = 100.00;
        $prorated = $this->processor->getProratedAmount($amount, $startDate, $endDate, $curDate);
        $this->assertEquals(1.10, $prorated);

        $curDate = new \DateTime('2000-01-01');
        $amount = 100.00;
        $prorated = $this->processor->getProratedAmount($amount, $startDate, $endDate, $curDate);
        $this->assertEquals(100.00, $prorated);

        $amount = 0.00;
        $prorated = $this->processor->getProratedAmount($amount, $startDate, $endDate, $curDate);
        $this->assertEquals(0.00, $prorated);

        $startDate = new \DateTime('2000-01-01');
        $endDate = new \DateTime('2000-01-01');
        $curDate = new \DateTime('2000-01-01');

        $amount = 10.00;
        $prorated = $this->processor->getProratedAmount($amount, $startDate, $endDate, $curDate);
        $this->assertEquals(0.00, $prorated);
    }

    public function testGenerateProratedInvoice(){

        self::$CI->db->query("UPDATE foreup_repeatable SET last_ran = '2030-02-05 00:00:00' WHERE id = 2");

        $silex =& $this->processor->silex;
        $repeatableId = 2;
        $chargeCustomerId = 3;
        $currentTime = '2030-02-20';

        $chargeItems = $silex->db->getRepository(
            'e:ForeupRepeatableAccountRecurringChargeItems'
        );
        $chargeCustomers = $silex->db->getRepository(
            'e:ForeupAccountRecurringChargeCustomers'
        );

        $ret = [];
        $ret['repeatable'] = $chargeItems->find($repeatableId);
        $ret['item'] = $ret['repeatable']->getItem();
        $ret['charge'] = $ret['item']->getRecurringCharge();
        $ret['courseId'] = $ret['charge']->getOrganizationId();
        $ret['customer'] =  $chargeCustomers->find($chargeCustomerId);
        $ret['current_time'] = $currentTime;
        $ret['prorate'] = true;

        $invoiceData = $this->processor->generateInvoiceObject($ret, $currentTime, $test = true);

        $this->assertEquals(3.22, $invoiceData['items'][0]['unit_price']);

        // Roll back database changes made for this test
        self::$CI->db->query("UPDATE foreup_repeatable SET last_ran = NULL WHERE id = 2");
    }
}