<?php
namespace fu\statementWorkflow\classes;

use Doctrine\Common\Collections\Criteria;
use fu\statementWorkflow\generateCharges;
use fu\statementWorkflow\generateChargeSale;
use fu\statementWorkflow\generateStatementFinanceChargeJobs;
use fu\statementWorkflow\generateStatementJobs;
use fu\statementWorkflow\processStatementFinanceChargeJobs;
use fu\statementWorkflow\processStatementJobs;
use tests\integration\member_billing\MemberBillingIntegrationTest;

class generateFinanceChargesTest extends \Codeception\TestCase\Test
{
	/**
	 * @var generateStatementFinanceChargeJobs
	 */
	private $financeGenerator;

	/**
	 * @var processStatementFinanceChargeJobs
	 */
	private $processor3;

	/**
	 * @var FakeQueue
	 */
	private $fakeQueue;


	public function _before()
	{
		$this->fakeQueue = new FakeQueue();

		$this->chargeGenerator = new generateCharges($this->fakeQueue);
		$this->processor1 = new generateChargeSale($this->fakeQueue);

		$this->statementGenerator = new generateStatementJobs($this->fakeQueue);
		$this->processor2 = new processStatementJobs($this->fakeQueue);

		$this->financeGenerator = new generateStatementFinanceChargeJobs($this->fakeQueue);
		$this->processor3 = new processStatementFinanceChargeJobs($this->fakeQueue);

	}

	protected function _after()
	{
		unset($this->chargeGenerator);
		unset($this->processor1);
		unset($this->statementGenerator);
		unset($this->processor2);
		unset($this->financeGenerator);
		unset($this->processor3);

	}

	public function testInterface()
	{
		$generator = $this->financeGenerator;
		$processor = $this->processor3;

		$this->assertTrue(method_exists($generator,'__construct'));
		$this->assertTrue(method_exists($generator,'__destruct'));
		$this->assertTrue(method_exists($generator,'run'));
		$this->assertTrue(method_exists($generator,'queueStatementChargeJob'));
		$this->assertTrue(method_exists($generator,'getDelayedFinanceStatements'));
		$this->assertTrue(method_exists($generator,'filterNotDelayedEnough'));

		$this->assertTrue(method_exists($processor,'__construct'));
		$this->assertTrue(method_exists($processor,'_get_queue'));
		$this->assertTrue(method_exists($processor,'_set_input_queue'));
		$this->assertTrue(method_exists($processor,'handle_error'));
		$this->assertTrue(method_exists($processor,'run'));
		$this->assertTrue(method_exists($processor,'extractMessage'));

	}

	public function testQueue()
	{
		MemberBillingIntegrationTest::resetData($this->financeGenerator->silex->CI);
		MemberBillingIntegrationTest::assign_customers($this->financeGenerator->silex->CI);
		MemberBillingIntegrationTest::clearInvoices($this->financeGenerator->silex->CI);
		MemberBillingIntegrationTest::clearStatements($this->financeGenerator->silex->CI);

		$generator = $this->financeGenerator;
		$processor = $this->processor3;
		$queue = &$generator->_get_queue();

		$this->chargeGenerator->runUntilEmpty('2030-04-01');
		$this->processor1->runUntilEmpty('2030-04-01');

		$this->statementGenerator->runUntilEmpty('2030-04-01');
		$this->processor2->runUntilEmpty('2030-04-01');

		$count = $generator->run('2030-04-25');

		$this->assertGreaterThan(0,$count);
		$this->assertEquals(3,$count);
		$jobs = $queue->fetchMessages('testing_processStatementFinanceChargeJobs');

		$this->assertEquals(3,count($jobs));


		// mix of CI and silex does not do well with transactions
		//$processor->silex->db->getConnection()->beginTransaction();
		$processor->run('2030-04-25');

		$repo = $processor->silex->db->getRepository('e:ForeupAccountStatements');

		$criteria = new Criteria();
		$criteria->where($criteria->expr()->gt('totalFinanceCharges',0.00));
		$criteria->andWhere($criteria->expr()->eq('dateCreated',new \DateTime('2030-04-01')));

		$res = $repo->matching($criteria);
		$res = $res->toArray();

		$this->assertGreaterThan(0,count($res));
		$this->assertEquals(3,count($res));

		foreach($res as $statement){
			$this->assertTrue(in_array($statement->getTotalFinanceCharges(),[42.38,20.00]));
			$this->assertTrue(in_array($statement->getTotal(),[41.90,889.88]));
		}

		//$processor->silex->db->getConnection()->rollBack();

	}
}

?>
