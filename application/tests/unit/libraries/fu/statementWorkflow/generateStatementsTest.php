<?php
namespace tests\unit\libraries\fu\statementWorkflow;

use Codeception\Specify;
use fu\statementWorkflow\generateStatementJobs;
use fu\statementWorkflow\processStatementChargeJobs;
use fu\statementWorkflow\processStatementJobs;
use fu\statementWorkflow\processStatementPdfJobs;
use fu\statementWorkflow\classes\FakeQueue;
use \models\Repeatable\Row as RepeatableObject;
use tests\integration\member_billing\MemberBillingIntegrationTest;

class generateStatementsTest extends \Codeception\TestCase\Test
{
	//
	use Specify;

	/**
	 * @var generateStatementJobs
	 */
	private $generator;

	/**
	 * @var processStatementJobs
	 */
	private $processor1;

	/**
	 * @var processStatementPdfJobs
	 */
	private $processor2;

	/**
	 * @var processStatementChargeJobs
	 */
	private $processor3;

	/**
	 * @var FakeQueue
	 */
	private $fakeQueue;

	protected function _before()
	{
		$this->fakeQueue = new FakeQueue();
		$this->generator = new generateStatementJobs($this->fakeQueue);
		$this->processor1 = new processStatementJobs($this->fakeQueue);
		$this->processor2 = new processStatementPdfJobs($this->fakeQueue);
		$this->processor3 = new processStatementChargeJobs($this->fakeQueue);

	}

	protected function _after()
	{
		unset($this->generator);
		unset($this->processor1);
		unset($this->processor2);
		unset($this->processor3);

	}

	public function testInterface()
	{
		$generator = $this->generator;
		$processor = $this->processor1;
		$processor2 = $this->processor2;
		$processor3 = $this->processor3;

		$this->assertTrue(method_exists($generator,'__construct'));
		$this->assertTrue(method_exists($generator,'_get_queue'));
		$this->assertTrue(method_exists($generator,'run'));
		$this->assertTrue(method_exists($generator,'fetchRepeatables'));
		$this->assertTrue(method_exists($generator,'customersEntityToArray'));

		$this->assertTrue(method_exists($processor,'__construct'));
		$this->assertTrue(method_exists($processor,'_get_queue'));
		$this->assertTrue(method_exists($processor,'_set_input_queue'));
		$this->assertTrue(method_exists($processor,'run'));
		$this->assertTrue(method_exists($processor,'handle_error'));
		$this->assertTrue(method_exists($processor,'extractMessage'));
		$this->assertTrue(method_exists($processor,'getStartStopTimes'));
		$this->assertTrue(method_exists($processor,'repeatableEntityToArray'));
		$this->assertTrue(method_exists($processor,'queueStatementPdfRenderJob'));
		$this->assertTrue(method_exists($processor,'getInvoices'));
		$this->assertTrue(method_exists($processor,'getInvoiceItems'));
		$this->assertTrue(method_exists($processor,'constructStatement'));
		$this->assertTrue(method_exists($processor,'storeItems'));
		$this->assertTrue(method_exists($processor,'get_next_statement_number'));

		$this->assertTrue(method_exists($processor2,'__construct'));
		$this->assertTrue(method_exists($processor2,'_get_queue'));
		$this->assertTrue(method_exists($processor2,'_set_input_queue'));
		$this->assertTrue(method_exists($processor2,'handle_error'));
		$this->assertTrue(method_exists($processor2,'run'));
		$this->assertTrue(method_exists($processor2,'zipGroup'));
		$this->assertTrue(method_exists($processor2,'sendCourseEmail'));
		$this->assertTrue(method_exists($processor2,'sendEmail'));
		$this->assertTrue(method_exists($processor2,'storePdfOnS3'));
		$this->assertTrue(method_exists($processor2,'storeZipOnS3'));
		$this->assertTrue(method_exists($processor2,'extractMessage'));
		$this->assertTrue(method_exists($processor2,'getAccountTransactions'));
		$this->assertTrue(method_exists($processor2,'getSales'));
		$this->assertTrue(method_exists($processor2,'getCustomerTransactions'));
		$this->assertTrue(method_exists($processor2,'getMemberTransactions'));
		$this->assertTrue(method_exists($processor2,'getInvoiceCharges'));
		$this->assertTrue(method_exists($processor2,'getCourseInfo'));
		$this->assertTrue(method_exists($processor2,'getCustomerInfo'));

		$this->assertTrue(method_exists($processor3,'__construct'));
		$this->assertTrue(method_exists($processor3,'_get_queue'));
		$this->assertTrue(method_exists($processor3,'_set_input_queue'));
		$this->assertTrue(method_exists($processor3,'run'));
		$this->assertTrue(method_exists($processor3,'handle_error'));
		$this->assertTrue(method_exists($processor3,'delayDaysCheck'));
		$this->assertTrue(method_exists($processor3,'extractMessage'));

	}

	public function testFetch()
	{
		$generator = $this->generator;
		$res = $generator->fetchRepeatables('2117-06-03');
		$this->assertGreaterThan(0,$res->count());
		$res2 = $res->current();
		$this->assertNotEmpty($res2);
		$statement = $res2->getStatement();
		$this->assertNotEmpty($statement);
		$charge = $statement->getRecurringCharge();
		$this->assertNotEmpty($charge);

	}

	public function testQueue()
	{
		if(ENVIRONMENT === 'jenkins'){
			//wkhtmltopdf does not work on the jenkins server
			return;
		}

		$prefix = '';
		if(ENVIRONMENT !== 'production'){
			$prefix = ENVIRONMENT.'_';
		}

		$this->assertEquals('testing_',$prefix);

		$generator =& $this->generator;

		MemberBillingIntegrationTest::clearStatements($generator->silex->CI);

		$generator->run('2030-03-03',true);
		$queue = $generator->_get_queue();
		$messages = $queue->fetchMessages($prefix.'ForeupRepeatableAccountRecurringStatementsToCustomers');
		$this->assertNotEmpty($messages);
		$body = $messages[0]->get('Body');
		$this->assertNotEmpty($body);
		$result = json_decode($body,true);
		$this->assertNotEmpty($result);
		$ro = new RepeatableObject();
		$ro->createFromArray($result['repeatable']);
		$next_time = $ro->getNextCalculatedOccurrence();
		$this->assertNotEmpty($next_time);
		$this->assertNotEmpty($result['customer']);
		$this->assertGreaterThan(0,$result['customer']['person_id']);

		$this->processor1->runUntilEmpty('2030-03-03', true);

		$this->processor2->runUntilEmpty('2030-03-03', true);

		$this->processor2->zipGroup('6270/1/2017-03-03','/tmp/processStatementPdfJobs/');


		$url = $this->processor2->storeZipOnS3('/processStatementPdfJobs/6270/1/2017-03-03/archive.zip',
			'/tmp/processStatementPdfJobs/6270/1/2017-03-03/archive.zip');



		// Not saving statement to database, so this is not currently working...
		//$this->processor3->run('2030-03-03',true);

	}

}

?>
