<?php
namespace fu\statementWorkflow\classes;

use foreup\rest\models\entities\ForeupAccountRecurringChargeCustomers;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupRepeatableAccountRecurringStatements;
use fu\statementWorkflow\generateStatementJobs;
use fu\statementWorkflow\processStatementJobs;

class statementGenerationTimingTest extends \Codeception\TestCase\Test
{
	private $rep_template = [-1,null,null,'MONTHLY',null,null,
		null,null,null,null,null,null,null,null,null,null,null];

	/**
	 * @var FakeQueue
	 */
	private $fakeQueue;

	/**
	 * @var generateStatementJobs
	 */
	private $generator;

	/**
	 * @var processStatementJobs
	 */
	private $processor;

	public function _before()
	{
		$this->fakeQueue = new FakeQueue();
		$this->generator = new generateStatementJobs($this->fakeQueue);
		$this->processor = new processStatementJobs($this->fakeQueue);


		$this->statementRepo = $this->generator->silex->db->getRepository('e:ForeupAccountRecurringStatements');
		$this->employeeRepo = $this->generator->silex->db->getRepository('e:ForeupEmployees');

	}

	protected function _after()
	{
		unset($this->generator);
		unset($this->processor);

	}

	protected function getRecStatement()
	{
		$entity = new ForeupAccountRecurringStatements();

		$entity->setOrganizationId(6270);
		$entity->setCreatedBy($this->employeeRepo->find(12));
		$date = new \DateTime();
		$entity->setDateCreated($date);

		return $entity;
	}

	protected function getRepeatable(array $array,array $transform = null)
	{
		if(isset($transform)){
			foreach($transform as $key=>$value){
				$array[$key] = $value;
			}
		}

		$entity = new ForeupRepeatableAccountRecurringStatements();

		$entity->setStatement($array[0]);
		$entity->setLastRan($array[1]);
		$entity->setNextOccurence($array[2]);
		$entity->setFreq($array[3]);
		$entity->setDtstart($array[4]);
		$entity->setUntil($array[5]);
		$entity->setInterval($array[6]);
		$entity->setCount($array[7]);
		$entity->setBymonth($array[8]);
		$entity->setByweekno($array[9]);
		$entity->setByyearday($array[10]);
		$entity->setBymonthday($array[11]);
		$entity->setByday($array[12]);
		$entity->setWkst($array[13]);
		$entity->setByhour($array[14]);
		$entity->setByminute($array[15]);
		$entity->setBysecond($array[16]);
		$entity->setBysetpos($array[17]);


		return $entity;
	}

	public function testMonthly()
	{
		$rep_array = $this->rep_template;


		$customer = new ForeupAccountRecurringChargeCustomers();
		$customer->setCustomer(new ForeupCustomers());

		$this->generator->silex->db->getConnection()->beginTransaction();
		$statement = $this->getRecStatement();
		$transform = [0=>$statement,2=>new \DateTime('1776-01-01 23:59:59'),4=>new \DateTime('1776-01-01 00:00:00'),11=>[1],14=>[23],15=>[59],16=>[59],17=>[1]];

		$this->generator->silex->db->persist($statement);
		$this->generator->silex->db->flush($statement);

		$repeatable = $this->getRepeatable($rep_array,$transform);
		$this->generator->silex->db->persist($repeatable);
		$this->generator->silex->db->flush($repeatable);

		$repeatables = $this->generator->fetchRepeatables('1776-01-01 00:00:00')->toArray();
		$this->assertEmpty($repeatables);

		$repeatables = $this->generator->fetchRepeatables('1776-01-02 00:00:00')->toArray();
		$this->assertNotEmpty($repeatables);
		$this->assertCount(1,$repeatables);

		$next = $this->generator->getNextOccurence('1776-01-02 00:00:00',$repeatable);
		$this->assertEquals('1776-02-01T23:59:59+0000', $next->format(DATE_ISO8601));

		$this->generator->silex->db->getConnection()->rollBack();



		$this->generator->silex->db->getConnection()->beginTransaction();
		$statement = $this->getRecStatement();
		$transform = [0=>$statement,2=>new \DateTime('1776-01-05 00:00:00'),4=>new \DateTime('1776-01-01 00:00:00'),11=>[5],14=>[0],15=>[0],16=>[0],17=>[1]];

		$this->generator->silex->db->persist($statement);
		$this->generator->silex->db->flush($statement);

		$repeatable = $this->getRepeatable($rep_array,$transform);
		$this->generator->silex->db->persist($repeatable);
		$this->generator->silex->db->flush($repeatable);

		$repeatables = $this->generator->fetchRepeatables('1776-01-01 00:00:00')->toArray();
		$this->assertEmpty($repeatables);

		$repeatables = $this->generator->fetchRepeatables('1776-01-04 23:59:59')->toArray();
		$this->assertEmpty($repeatables);

		$repeatables = $this->generator->fetchRepeatables('1776-01-05 00:00:00')->toArray();
		$this->assertNotEmpty($repeatables);
		$this->assertCount(1,$repeatables);

		$next = $this->generator->getNextOccurence('1776-01-05 00:00:01',$repeatable);
		$this->assertEquals('1776-02-05T00:00:00+0000', $next->format(DATE_ISO8601));

		$next = $this->generator->getNextOccurence('1776-02-05 00:00:01',$repeatable);
		$this->assertEquals('1776-03-05T00:00:00+0000', $next->format(DATE_ISO8601));

		$this->generator->silex->db->getConnection()->rollBack();



		$this->generator->silex->db->getConnection()->beginTransaction();
		$statement = $this->getRecStatement();
		$transform = [0=>$statement,2=>new \DateTime('1776-03-01 00:00:00'),4=>new \DateTime('1776-01-01 00:00:00'),8=>[3,6,9],11=>[1],14=>[0],15=>[0],16=>[0],17=>[1]];

		$this->generator->silex->db->persist($statement);
		$this->generator->silex->db->flush($statement);

		$repeatable = $this->getRepeatable($rep_array,$transform);
		$this->generator->silex->db->persist($repeatable);
		$this->generator->silex->db->flush($repeatable);

		$repeatables = $this->generator->fetchRepeatables('1776-01-01 00:00:00')->toArray();
		$this->assertEmpty($repeatables);

		$repeatables = $this->generator->fetchRepeatables('1776-02-04 23:59:59')->toArray();
		$this->assertEmpty($repeatables);

		$repeatables = $this->generator->fetchRepeatables('1776-02-29 23:59:59')->toArray();
		$this->assertEmpty($repeatables);

		$repeatables = $this->generator->fetchRepeatables('1776-03-01 00:00:00')->toArray();
		$this->assertNotEmpty($repeatables);
		$this->assertCount(1,$repeatables);

		$next = $this->generator->getNextOccurence('1776-01-01 00:00:01',$repeatable);
		$this->assertEquals('1776-03-01T00:00:00+0000', $next->format(DATE_ISO8601));

		$next = $this->generator->getNextOccurence('1776-03-01 00:00:01',$repeatable);
		$this->assertEquals('1776-06-01T00:00:00+0000', $next->format(DATE_ISO8601));

		$next = $this->generator->getNextOccurence('1776-06-01 00:00:01',$repeatable);
		$this->assertEquals('1776-09-01T00:00:00+0000', $next->format(DATE_ISO8601));

		$next = $this->generator->getNextOccurence('1776-09-01 00:00:01',$repeatable);
		$this->assertEquals('1777-03-01T00:00:00+0000', $next->format(DATE_ISO8601));

		$times = $this->processor->getStartStopTimes(['statement'=>$statement,'repeatable'=>$repeatable->toRuleArray(),'customer'=>$customer],'1776-09-01 00:00:01',true);

		$this->generator->silex->db->getConnection()->rollBack();
	}
}

?>