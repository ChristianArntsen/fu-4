<?php
namespace fu\statementWorkflow\classes;

use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\repositories\AccountStatementsRepository;
use fu\statementWorkflow\generateDelayedStatementChargeJobs;
use fu\statementWorkflow\processStatementChargeJobs;

class generateDelayedStatementChargeJobsTest extends \Codeception\TestCase\Test
{
	/**
	 * @var generateDelayedStatementChargeJobs
	 */
	private $generator;

	/**
	 * @var FakeQueue
	 */
	private $fakeQueue;


	public function _before()
	{
		$this->fakeQueue = new FakeQueue();
		$this->generator = new generateDelayedStatementChargeJobs($this->fakeQueue);
		$this->processor3 = new processStatementChargeJobs($this->fakeQueue);
	}

	protected function _after()
	{
		unset($this->generator);
		unset($this->processor3);

	}

	public function testInterface()
	{
		$generator = $this->generator;

		$this->assertTrue(method_exists($generator,'__construct'));
		$this->assertTrue(method_exists($generator,'__destruct'));
		$this->assertTrue(method_exists($generator,'run'));
		$this->assertTrue(method_exists($generator,'queueStatementChargeJob'));
		$this->assertTrue(method_exists($generator,'getDelayedAutopayStatements'));
		$this->assertTrue(method_exists($generator,'filterNotDelayedEnough'));

	}

	public function testNotDelayedEnough()
	{
		$gen = $this->generator;
		$current_time = new \DateTime('2778-07-07');

		$statements = [];

		// not delayed enough
		$statement1 = new ForeupAccountStatements();
		$statement1->setAutopayDelayDays(5);
		$statement1->setDateCreated(new \DateTime('2778-07-05'));
		$statements[] = $statement1;

		// strange future value
		$statement1 = new ForeupAccountStatements();
		$statement1->setAutopayDelayDays(5);
		$statement1->setDateCreated(new \DateTime('2778-07-12'));
		$statements[] = $statement1;

		// delayed enough
		$statement2 = new ForeupAccountStatements();
		$statement2->setAutopayDelayDays(2);
		$statement2->setDateCreated(new \DateTime('2778-07-04'));
		$statements[] = $statement2;

		$results = $gen->filterNotDelayedEnough($current_time,$statements);

		$this->assertTrue(is_array($results));
		$this->assertEquals(1,count($results));
		$this->assertEquals(2,$results[0]->getAutopayDelayDays());

	}
}

?>