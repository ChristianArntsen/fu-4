<?php
namespace fu\statementWorkflow\classes;

use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\models\entities\ForeupItems;
use foreup\rest\models\entities\ForeupPeople;

class AccountChargerTest extends \Codeception\TestCase\Test {

	/**
	 * @var silex_handler
	 */
	private $silex;

	/**
	 * @var AccountCharger
	 */
	private $charger;

	protected function _before()
	{
		$this->silex = new silex_handler();
		$this->charger = new AccountCharger($this->silex);

	}

	protected function _after()
	{
		$this->silex->db->getConnection()->close();
		unset($this->silex);

	}

	public function testInterface()
	{
		$this->assertTrue(method_exists($this->charger,'associateInvoiceWithRecurringCharge'));
		$this->assertTrue(method_exists($this->charger,'payAccountBalances'));
		$this->assertTrue(method_exists($this->charger,'payBalance'));
		$this->assertTrue(method_exists($this->charger,'associateInvoiceWithCharge'));
		$this->assertTrue(method_exists($this->charger,'generateInvoiceObject'));

	}

	public function testGenerateInvoiceObject()
	{
		$item = new ForeupItems();
		$customer = new ForeupCustomers();
		$customer->setCourseId(456);
		$person = new ForeupPeople();
		$person->setPersonId(123);
		$customer->setPerson($person);
		$name = 'Some great name';
		$amount = 1.23;
		$currentTime = new \DateTime('1776-07-04');

		$object = $this->charger->generateInvoiceObject($item,$customer,$name,$amount,$currentTime);

		$this->assertEquals(123,$object['person_id']);
		$this->assertEquals(456,$object['course_id']);
		$this->assertEquals(0,$object['employee_id']);
		$this->assertEquals('Some great name',$object['name']);
		$this->assertEquals('1776-07-04T00:00:00-0700',$object['bill_start']);
		$this->assertEquals('1776-07-04T00:00:00-0700',$object['bill_end']);
		$this->assertTrue(is_array($object['items']));
		$this->assertArrayHasKey(0,$object['items']);
		$this->assertEquals(1,count($object['items']));
	}

	public function testAssociateInvoiceWithCharge()
	{
		$charge = new ForeupAccountRecurringCharges();

		$invoice = $this->charger->associateInvoiceWithCharge($charge,123,true);

		$this->assertInstanceOf('foreup\rest\models\entities\ForeupInvoices',$invoice);

		$this->assertInstanceOf('foreup\rest\models\entities\ForeupAccountRecurringCharges',$invoice->getRecurringCharge());

		$this->assertEquals($charge,$invoice->getRecurringCharge());

	}

	public function testPayBalance()
	{
		$item = new ForeupItems();
		$customer = new ForeupCustomers();
		$customer->setCourseId(456);
		$person = new ForeupPeople();
		$person->setPersonId(123);
		$customer->setPerson($person);
		$name = 'Some great name';
		$amount = 1.23;
		$currentTime = new \DateTime('1776-07-04');
		$statement = new ForeupAccountStatements();
		$statement->setCustomer($customer);

		$invoice = $this->charger->payBalance(6270,$item,$customer,$name,$amount,$currentTime,true);

		$this->assertTrue(is_int($invoice->getSale()->getSaleId()));
		$this->assertGreaterThan(0,$invoice->getSale()->getSaleId());
		$this->assertTrue(is_float($invoice->getTotal()));

	}

	public function testPayAccountBalances()
	{
		$customer = new ForeupCustomers();
		$customer->setCourseId(456);
		$person = new ForeupPeople();
		$person->setPersonId(12);
		$customer->setPerson($person);
		$statement = new ForeupAccountRecurringStatements();
		$charge = new ForeupAccountRecurringCharges();
		$statement->testing_override('recurringCharge',$charge);
		$statement ->setOrganizationId(6270);
		$currentTime = new \DateTime('1776-07-04');

		$result = $this->charger->payAccountBalances($customer,$statement,$currentTime,true);

		$this->assertEmpty($result);

		$statement->setPayMemberBalance(true);
		$statement->setPayCustomerBalance(true);

		$result = $this->charger->payAccountBalances($customer,$statement,$currentTime,true);

	}
}

?>
