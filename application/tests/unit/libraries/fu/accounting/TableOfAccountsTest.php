<?php
// TableOfAccountsTest.php

namespace libraries\fu\accounting;

class TableOfAccountsTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;
	private $TOA;

	protected function _before()
	{
		//$this->TOA = new \fu\accounting\TableOfAccounts();
		\Codeception\Specify\Config::setDeepClone(false);
	}

	public function testNewAccount()
	{
		$course_id = 6270;
		$table = new \fu\accounting\TableOfAccounts();

		$this->specify("Check that the post method exists",function() use ($table){
			verify(method_exists($table,'post'))->true();
		});

		$account_id = $table->post($course_id,'Test Account','asset','Some description',12345);

		$this->specify("Check that the post method works",function() use ($table,$account_id){
			verify(is_numeric($account_id))->true();
			verify(is_int($account_id*1))->true();
			verify($account_id)->greaterThan(0);
			verify($table->get_last_error())->isEmpty();
		});


		$this->specify("Check that the delete method exists",function() use (&$table){
			verify(method_exists($table,'delete'))->true();
		});

		$this->specify("Check that the delete method works",function() use ($table,$account_id){
			$rows_deleted = $table->delete($account_id,null,true);
			verify(is_numeric($rows_deleted))->true();
			verify(is_int($rows_deleted*1))->true();
			verify($rows_deleted)->equals(1);
			verify($rows_deleted)->greaterThan(0);
			verify($table->get_last_error())->isEmpty();
		});
	}

	public function testPost()
	{
		$course_id = 6270;
		$table = new \fu\accounting\TableOfAccounts();

		$this->specify("Check that the post fails without course_id parameter",function() use (&$table,$course_id){

			$account_id = $table->post(null,'Test Account','asset','Some description',12345);

			verify(is_numeric($account_id))->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->post Error: course_id is required, and must be an integer.');
		});

		$this->specify("Check that the post fails without name parameter",function() use (&$table,$course_id){

			$account_id = $table->post($course_id,null,'asset','Some description',12345);

			verify(is_numeric($account_id))->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->post Error: name is required, and must be a string.');
		});

		$this->specify("Check that the post fails without account_type parameter",function() use (&$table,$course_id){

			$account_id = $table->post($course_id,'Test Account',null,'Some description',12345);

			verify(is_numeric($account_id))->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->post Error: account_type is required, '.
				'and must be a string with one of the following values: '.
				'asset liability revenue expense.');
		});

		$this->specify("Check that the post fails with invalid account_type parameter",function() use (&$table,$course_id){

			$account_id = $table->post($course_id,'Test Account','some string','Some description',12345);

			verify(is_numeric($account_id))->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->post Error: account_type is required, '.
				'and must be a string with one of the following values: '.
				'asset liability revenue expense.');
		});

		$this->specify("Check that the reset_last_error works",function() use (&$table,$course_id){

			$table->post(null,'Test Account','asset','Some description',12345);

			verify($table->get_last_error())->notEmpty();
			$table->reset_last_error();
			verify($table->get_last_error())->isEmpty();
		});

		$this->specify("Check that the post succeeds with null description parameter",function() use (&$table,$course_id){

			$table->reset_last_error();
			$account_id = $table->post($course_id,'Test Account','asset',null,12345);

			verify(is_numeric($account_id))->true();
			verify($table->get_last_error())->isEmpty();
			$rows_deleted = $table->delete($account_id,null,true);
			verify($rows_deleted)->equals(1);
		});

		$this->specify("Check that the post succeeds with invalid description parameter",function() use (&$table,$course_id){

			$table->reset_last_error();
			$account_id = $table->post($course_id,'Test Account','asset',12345.007,12345);

			verify(is_numeric($account_id))->true();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->post Warning: description must be a string.');

			$rows_deleted = $table->delete($account_id,null,true);
			verify($rows_deleted)->equals(1);
		});

		$this->specify("Check that the post succeeds with null gl_code parameter",function() use (&$table,$course_id){

			$table->reset_last_error();
			$account_id = $table->post($course_id,'Test Account','asset',null,null);

			verify(is_numeric($account_id))->true();
			verify($table->get_last_error())->isEmpty();
			$rows_deleted = $table->delete($account_id,null,true);
			verify($rows_deleted)->equals(1);
		});

		$this->specify("Check that the post succeeds with invalid gl_code parameter",function() use (&$table,$course_id){

			$table->reset_last_error();
			$account_id = $table->post($course_id,'Test Account','asset',null,12345.007);

			verify(is_numeric($account_id))->true();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->post Warning: gl_code must be an integer or integer string.');

			$rows_deleted = $table->delete($account_id,null,true);
			verify($rows_deleted)->equals(1);
		});
	}

	public function testDelete()
	{
		$course_id = 6270;
		$table = new \fu\accounting\TableOfAccounts();

		$account_id = $table->post($course_id,'Test Account','asset','Some description',12345);

		$this->specify("Check that the delete method fails without course_id parameter",function() use (&$table,$course_id,$account_id){

			$rows_deleted = $table->delete(null,'Test Account');

			verify(is_numeric($rows_deleted))->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->get Error: id is required, '.
					'and must be an integer corresponding to account id, '.
					'or the course id accompanied by the name parameter.');
		});

		$this->specify("Check that the delete method fails without invalid id parameter",function() use (&$table,$course_id,$account_id){

			$rows_deleted = $table->delete('invalid parameter...','Test Account');

			verify(is_numeric($rows_deleted))->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->get Error: id is required, '.
					'and must be an integer corresponding to account id, '.
					'or the course id accompanied by the name parameter.');
		});

		$this->specify("Check that the delete method fails with invalid name parameter",function() use (&$table,$course_id,$account_id){

			$rows_deleted = $table->delete($course_id,13.77);

			verify(is_numeric($rows_deleted))->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->get Error: name must be a string.');
		});

		$this->specify("Check that the delete method works with just id",function() use (&$table,$course_id,$account_id){

			$table->reset_last_error();
			$rows_deleted = $table->delete($account_id);

			verify(is_numeric($rows_deleted))->true();
			verify($rows_deleted)->equals(1);
			verify($table->get_last_error())->isEmpty();

			$rows_deleted = $table->delete($account_id,null,true);

			verify(is_numeric($rows_deleted))->true();
			verify($rows_deleted)->equals(1);
			verify($table->get_last_error())->isEmpty();
		});

		$account_id = $table->post($course_id,'Test Account','asset','Some description',12345);

		$this->specify("Check that the delete method works with course_id and name",function() use (&$table,$course_id,$account_id){

			$table->reset_last_error();
			$rows_deleted = $table->delete($course_id,'Test Account');

			verify(is_numeric($rows_deleted))->true();
			verify($rows_deleted)->equals(1);
			verify($table->get_last_error())->isEmpty();

			$rows_deleted = $table->delete($course_id,'Test Account',true);

			verify(is_numeric($rows_deleted))->true();
			verify($rows_deleted)->equals(1);
			verify($table->get_last_error())->isEmpty();
		});

	}

	public function testGet()
	{
		$course_id = 6270;
		$table = new \fu\accounting\TableOfAccounts();

		$account_id = $table->post($course_id,'Test Account','asset','Some description',12345);
		$account_id2 = $table->post($course_id,'Test Account Too','asset','Some description',12345);

		$this->specify("Check that the get method fails with invalid string input",function() use (&$table,$course_id,$account_id){

			$rows = $table->get('not int');

			verify($rows)->isEmpty();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->get Error: parameters attribute is required, '.
				'and must be an integer corresponding to account id, '.
				'or the course id accompanied by the name parameter.');
		});

		$this->specify("Check that the get method fails with invalid float input",function() use (&$table,$course_id,$account_id){

			$rows = $table->get(8.55);

			verify($rows)->isEmpty();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->get Error: account_id must be an integer.');
		});

		$this->specify("Check that the get method fails with invalid course_id parameter",function() use (&$table,$course_id,$account_id){

			$rows = $table->get(array('course_id'=>'not int'));

			verify($rows)->isEmpty();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->get Error: course_id must be an integer.');
		});

		$this->specify("Check that the get method fails with invalid account_id parameter",function() use (&$table,$course_id,$account_id){

			$rows = $table->get(array('account_id'=>'not int'));

			verify($rows)->isEmpty();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->get Error: account_id must be an integer.');
		});

		$this->specify("Check that the get method fails with invalid name parameter",function() use (&$table,$course_id,$account_id){

			$rows = $table->get(array('course_id'=>$course_id,'name'=>1337));

			verify($rows)->isEmpty();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->get Error: name must be a string.');
		});

		$this->specify("Check that the get method fails with lone name parameter",function() use (&$table,$course_id,$account_id){

			$rows = $table->get(array('name'=>'Test Account'));

			verify($rows)->isEmpty();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->get Error: name must be accompanied by either a course_id or account_id.');
		});

		$this->specify("Check that the get method works with account_id parameter",function() use (&$table,$course_id,$account_id){
			$table->reset_last_error();
			$rows = $table->get(array('account_id'=>$account_id));

			verify($table->get_last_error())->isEmpty();
			verify($rows)->notEmpty();
			verify(count($rows))->equals(1);
			verify($table->get_last_error())->isEmpty();
		});

		$this->specify("Check that the get method works with course_id parameter",function() use (&$table,$course_id,$account_id){
			$table->reset_last_error();
			$rows = $table->get(array('course_id'=>$course_id));

			verify($table->get_last_error())->isEmpty();
			verify($rows)->notEmpty();
			verify(count($rows))->greaterThan(1);
			verify($table->get_last_error())->isEmpty();
		});

		$this->specify("Check that the get method works with course_id parameter",function() use (&$table,$course_id,$account_id){
			$table->reset_last_error();
			$rows = $table->get(array('course_id'=>$course_id,'name'=>'Test Account'));

			verify($table->get_last_error())->isEmpty();
			verify($rows)->notEmpty();
			verify(count($rows))->equals(1);
			verify($table->get_last_error())->isEmpty();
		});


		$rows_deleted = $table->delete($course_id,'Test Account',true);
		verify($rows_deleted)->equals(1);


		$rows_deleted = $table->delete($course_id,'Test Account Too',true);
		verify($rows_deleted)->equals(1);
	}

	public function testPatch()
	{
		$course_id = 6270;
		$table = new \fu\accounting\TableOfAccounts();

		$account_id = $table->post($course_id,'Test Account','asset','Some description',12345);

		$this->specify("Check that the patch method fails with invalid course_id parameter",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$rows = $table->patch(array('course_id'=>'not int','name'=>'valid name'));

			verify($rows)->isEmpty();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->patch Error: course_id must be an integer.');
	    });

		$this->specify("Check that the patch method fails with invalid account_id parameter",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$rows = $table->patch(array('account_id'=>'not int'));

			verify($rows)->isEmpty();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->patch Error: account_id must be an integer.');
		});

		$this->specify("Check that the patch method fails with invalid name parameter",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$rows = $table->patch(array('course_id'=>$course_id,'name'=>1337));

			verify($rows)->isEmpty();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->patch Error: name must be a string.');
		});

		$this->specify("Check that the patch method fails with non-existent node",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$rows = $table->patch(array('course_id'=>$course_id,'name'=>'does not exist'));

			verify($rows)->isEmpty();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->patch Error: row does not exist.');
		});

		// patch with account id

		$this->specify("Check that the patch method works with account id",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$updated_id = $table->patch(array('account_id'=>$account_id,'description'=>'Newer description'));

			verify($updated_id)->equals($account_id);

			verify($table->get_last_error())->isEmpty();

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['description'])->equals('Newer description');
		});

		// patch the name
		$this->specify("Check that the patch method works updating name",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$updated_id = $table->patch(array('account_id'=>$account_id,'name'=>'New Name'));

			verify($updated_id)->equals($account_id);

			verify($table->get_last_error())->isEmpty();

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['name'])->equals('New Name');
		});

		// patch with course id and name
		$this->specify("Check that the patch method works updating name",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$updated_id = $table->patch(array('account_id'=>$account_id,'name'=>'New Name','description'=>'Older description'));

			verify($table->get_last_error())->isEmpty();
			verify($updated_id)->equals($account_id);

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['description'])->equals('Older description');
		});

		// patch the gl_code
		$this->specify("Check that the patch method works updating gl_code",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$updated_id = $table->patch(array('course_id'=>$course_id,'name'=>'New Name','gl_code'=>8675309));

			verify($updated_id)->equals($account_id);

			verify($table->get_last_error())->isEmpty();

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['description'])->equals('Older description');
			verify($rows[0]['gl_code'])->equals(8675309);
		});

		// patch the account_type
		$this->specify("Check that the patch method works updating account_type",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$updated_id = $table->patch(array('course_id'=>$course_id,'name'=>'New Name','account_type'=>'expense'));

			verify($updated_id)->equals($account_id);

			verify($table->get_last_error())->isEmpty();

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['description'])->equals('Older description');
			verify($rows[0]['gl_code'])->equals(8675309);
			verify($rows[0]['account_type'])->equals('expense');
		});

		// patch several things at once with account id
		$this->specify("Check that the patch method works updating multiple fields",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$updated_id = $table->patch(array('account_id'=>$account_id,'name'=>'Newest Name','account_type'=>'revenue','gl_code'=>911));

			verify($updated_id)->equals($account_id);

			verify($table->get_last_error())->isEmpty();

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['description'])->equals('Older description');
			verify($rows[0]['gl_code'])->equals(911);
			verify($rows[0]['account_type'])->equals('revenue');
			verify($rows[0]['name'])->equals('Newest Name');
		});

		// patch several things at once with name and course id




		$rows_deleted = $table->delete($account_id,null,true);
		verify($rows_deleted)->equals(1);
	}

	public function testPut()
	{

		$course_id = 6270;
		$table = new \fu\accounting\TableOfAccounts();

		$account_id = $table->put($course_id,'Test Account','asset',null,'Some description',12345);
		$this->specify('Should insert a new row',function () use ($table,$account_id){

			verify($table->get_last_error())->isEmpty();
			verify(is_numeric($account_id))->true();
			verify(is_int($account_id*1))->true();
			verify($account_id)->greaterThan(0);

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['name'])->equals('Test Account');
			verify($rows[0]['deleted'])->equals(0);
			verify($rows[0]['description'])->equals('Some description');
			verify($rows[0]['account_type'])->equals('asset');
			verify($rows[0]['course_id'])->equals(6270);
			verify($rows[0]['account_id'])->equals($account_id);
		});

		$this->specify("Check that the put method fails with invalid course_id parameter",function() use ($table,$course_id,$account_id){

			$account_id = $table->put('invalid','Test Account','asset',null,'Some description',12345);

			verify($account_id)->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->put Error: course_id is required, and must be an integer.');
		});

		$this->specify("Check that the put method fails with invalid account_id parameter",function() use ($table,$course_id,$account_id){

			$account_id = $table->put($course_id,'Test Account','asset','invalid','Some description',12345);

			verify($account_id)->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->put Error: account_id must be null, integer or integer string.');
		});

		$this->specify("Check that the put method fails with invalid name parameter",function() use ($table,$course_id,$account_id){

			$account_id = $table->put($course_id,1337,'asset',null,'Some description',12345);

			verify($account_id)->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->put Error: name is required, and must be a string.');
		});

		$this->specify("Check that the put method fails with missing name parameter",function() use ($table,$course_id,$account_id){

			$account_id = $table->put($course_id,null,'asset',null,'Some description',12345);

			verify($account_id)->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->put Error: name is required, and must be a string.');
		});

		$this->specify("Check that the put method fails with invalid account_type parameter",function() use ($table,$course_id,$account_id){

			$account_id = $table->put($course_id,'Test Account','invalid',null,'Some description',12345);

			verify($account_id)->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->put Error: account_type is required, '.
				'and must be a string with one of the following values: '.
				'asset liability revenue expense.');
		});

		$this->specify("Check that the put method fails with missing account_type parameter",function() use ($table,$course_id,$account_id){

			$account_id = $table->put($course_id,'Test Account',null,null,'Some description',12345);

			verify($account_id)->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->put Error: account_type is required, '.
				'and must be a string with one of the following values: '.
				'asset liability revenue expense.');
		});

		$this->specify("Check that the put method fails when missing course_id parameter",function() use ($table,$course_id,$account_id){

			$account_id = $table->put(null,'Test Account','asset',$account_id,'Some description',12345);

			verify($account_id)->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->put Error: course_id is required, and must be an integer.');
		});

		$this->specify("Check that the put method fails when missing course_id and account_id parameters",function() use ($table,$course_id,$account_id){

			$account_id = $table->put(null,'Test Account','asset',null,'Some description',12345);

			verify($account_id)->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->put Error: not enough information to insert OR update a row. Please include minimum of account_id or combination of course_id and name.');
		});

		$this->specify("Check that the put method fails when updating faux row",function() use ($table,$course_id,$account_id){

			$account_id = $table->put($course_id,'Test Account','asset',$account_id-1,'Some description',12345);

			verify($account_id)->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->put Error: account_id not found.');
		});

		$this->specify("Check that the put method fails when updating wrong course_id",function() use ($table,$course_id,$account_id){

			$account_id = $table->put($course_id-1,'Test Account','asset',$account_id,'Some description',12345);

			verify($account_id)->false();
			verify($table->get_last_error())->notEmpty();
			verify($table->get_last_error())->equals('TableOfAccounts->put Error: account_id not found.');
		});

		// we can change the name
		$this->specify("Check that we can change the name",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$account_id2 = $table->put($course_id,'Test Account Too','asset',$account_id,'Some description',12345);

			verify($account_id2)->equals($account_id);
			verify($table->get_last_error())->isEmpty();

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['name'])->equals('Test Account Too');
		});

		// we can change the account type
		$this->specify("Check that we can change the account_type",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$account_id2 = $table->put($course_id,'Test Account','liability',$account_id,'Some description',12345);

			verify($account_id2)->equals($account_id);
			verify($table->get_last_error())->isEmpty();

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['account_type'])->equals('liability');
		});

		// we can change the description
		$this->specify("Check that we can change the description",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$account_id2 = $table->put($course_id,'Test Account','asset',$account_id,'Some description too',12345);

			verify($account_id2)->equals($account_id);
			verify($table->get_last_error())->isEmpty();

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['description'])->equals('Some description too');
		});

		// we can change the gl code
		$this->specify("Check that we can change the gl_code",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();
			$account_id2 = $table->put($course_id,'Test Account','asset',$account_id,'Some description',123456);

			verify($account_id2)->equals($account_id);
			verify($table->get_last_error())->isEmpty();

			$rows = $table->get(array('account_id'=>$account_id));

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['gl_code'])->equals(123456);
		});

		// we will undelete
		$this->specify("Check that put unsets soft delete status",function() use ($table,$course_id,$account_id){
			$table->reset_last_error();

			$rows_deleted = $table->delete($account_id,null);
			verify($rows_deleted)->equals(1);

			$rows = $table->get(array('account_id'=>$account_id),true);

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['deleted'])->equals(1);

			$account_id2 = $table->put($course_id,'Test Account','asset',$account_id,'Some description',123456);
			verify($account_id2)->equals($account_id);
			verify($table->get_last_error())->isEmpty();

			$rows = $table->get(array('account_id'=>$account_id),true);

			verify($rows)->notEmpty();
			verify($table->get_last_error())->isEmpty();
			verify($rows[0]['deleted'])->equals(0);
		});


		$rows_deleted = $table->delete($account_id,null,true);
		verify($rows_deleted)->equals(1);
	}
}

?>