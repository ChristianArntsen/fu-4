<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class ItemsHelper extends \Codeception\Module
{
    private $Items = [];
    private $db_Requirements = "db-requirements";
    private $course_id;
    private $inventory_level = 200000;
    private $FeeItems;



    /*
     * Codeception doesn't allow the constructor to call functions that make
     * references to outside modules like Db, DbHelper, or REST modules.
     * When Creating an Items object your tests need to call _addItemsToDatabase();
     * to finish setting up items they will be using.
    */
    function __construct($course_id)
    {
        parent::__construct();
        $this->course_id = $course_id;

        //get basic item info set up
        $tax = [
            [
                'course_id' => $this->course_id,
                'CID' => 0,
                'name' => 'Sales Tax',
                'percent' => 6.750,
                'cumulative' => 0
            ],
            [
                'course_id' => $this->course_id,
                'CID' => 0,
                'name' => 'Sales Tax 2',
                'percent' => 1.050,
                'cumulative' => 0
            ]
        ];
        $this->Items["Dr. Pepper"] = $this->_createItem('Dr. Pepper', 'Course', 'PS Balls', 1, 0.50, 2.00, "item", $tax);

        $tax = [
            [
                'course_id' => $this->course_id,
                'CID' => 0,
                'name' => 'Sales Tax',
                'percent' => 6.750,
                'cumulative' => 0
            ]
        ];
        $this->Items["RocketBalls"] = $this->_createItem('RocketBalls', 'Food & Beverage', 'Beverage', 885583349768, 5.00, 8.00, "item", $tax);

        //Green fee and Cart Fee setup should be configured in FeeItemsHelper.php
        $this->FeeItems = new FeeItemsHelper($course_id);
    }

    /**
     * Sets up the structure for a specific item in the internal array $Items.
     *
     * @param  string $name - item name
     * @param  string $department - items department
     * @param  string $category - items category
     * @param  int $itemNum - items item number
     * @param  int $cost_price - cost of the item
     * @param  int $unit_price - price of the item
     * @param  string $item_type - type of item
     * @param  array $tax(optional) - array of values related to the items taxes
     * @param  int $item_id(optional) - id of the item
     * @return array - array with the desired items data
     */
    private function _createItem($name, $department, $category, $itemNum, $cost_price, $unit_price, $item_type, array $tax = null, $item_id = -1){
        //set data for item
        $item = [
            "item_id" => $item_id,
            "item_type" => $item_type,
            $this->db_Requirements => [
                'course_id' => $this->course_id,
                'CID' => 0,
                'name' => $name,
                'department' => $department,
                'category' => $category,
                'subcategory' => '',
                'supplier_id' => null,
                'item_number' => $itemNum,
                'description' => '',
                'image_id' => 0,
                'cost_price' => $cost_price,
                'unit_price' => $unit_price,
                'unit_price_includes_tax' => 0,
                'max_discount' => 0,
                'quantity' => $this->inventory_level,
                'is_unlimited' => 0,
                'reorder_level' => 10,
                'location' => 0,
                'gl_code' => null,
                'allow_alt_description' => 0,
                'is_serialized' => 0,
                'is_giftcard' => 0,
                'invisible' => 0,
                'deleted' => 0,
                'food_and_beverage' => 0,
                'soup_or_salad' => 'none',
                'number_of_sides' => 0,
                'is_side' => 0,
                'add_on_price' => 0,
                'print_priority' => 1,
                'kitchen_printer' => 1,
                'quickbooks_income' => 0,
                'quickbooks_cogs' => 0,
                'quickbooks_assets' => 0,
                'inactive' => 0,
                'erange_size' => 0,
                'is_fee' => 0,
                'meal_course_id' => 0,
                'do_not_print' => 0,
                'prompt_meal_course' => 0,
                'do_not_print_customer_receipt' => 0
            ]
        ];

        if($tax != null){
            $item["Tax"] = $tax;
        }

        if($category == 'Green Fees'){
            $item[$this->db_Requirements]['is_unlimited'] = 1;
            $item[$this->db_Requirements]['quantity'] = $this->FeeItems->_getInventoryLevel();
            $item[$this->db_Requirements]['reorder_level'] = $this->FeeItems->_getReorderLevel();
            $item['Green Fees'] = [
                "price_class_id" => $this->FeeItems->_getPriceClassID(),
                "price_class_name" => $this->FeeItems->_getPriceClassName(),
                "timeframe_id" => $this->FeeItems->_getTimeframeID(),
                "teesheet_id" => $this->FeeItems->_getTeeSheetID(),
                "teesheet_name" => $this->FeeItems->_getTeeSheetName(),
                "season_id" => $this->FeeItems->_getSeasonID(),
                "season_name" => $this->FeeItems->_getSeasonName(),
                "special_id" => false,
                "is_today" => true,
                "holes" => $this->FeeItems->_getNumHoles(),
                "params" => [
                    "teesheet_id" => $this->FeeItems->_getTeeSheetID(),
                    "holes" => $this->FeeItems->_getNumHoles(),
                    "season_id" => $this->FeeItems->_getSeasonID()
                ]
            ];
        }


        //return items data
        return  $item;
    }

    /**
     * Add items in the Items class to the database.
     * Must be called before Items object can be used in tests
     *
     * @return null  no return value
     */
    public function _addItemsToDatabase(){
        $this->FeeItems->_createFees();
        $DbHelper = $this->getModule('DbHelper');
        $Db = $this->getModule('Db');

        foreach($this->Items as $key => $item){
            //if items db-requirements aren't set skip the item
            if(isset($item[$this->db_Requirements])){

                codecept_debug("\n* Adding ".$key. " to the database");

                //add item to database
                $DbHelper->InsertAndUpdateOnDuplicate('foreup_items', $item[$this->db_Requirements]);

                //get item id from database
                $item["item_id"] = $Db->grabFromDatabase('foreup_items', 'max(item_id)', $item[$this->db_Requirements]);

                //check if item has any tax
                if(isset($item["Tax"])) {
                    foreach($item["Tax"] as $k => $tax) {
                        $item["Tax"][$k]["item_id"] = $item["item_id"];
                        $DbHelper->InsertAndUpdateOnDuplicate('foreup_items_taxes', $item["Tax"][$k]);
                    }
                }
            }
            $this->Items[$key] = $item;
        }

        $this->Items['Green Fee'] = $this->_createItem($this->FeeItems->_getName(),$this->FeeItems->_getDepartment(),$this->FeeItems->_getCategory(),1,$this->FeeItems->_getCostPrice(),$this->FeeItems->_getUnitPrice(),$this->FeeItems->_getItemType(),$this->FeeItems->_getTaxInfo(),$this->FeeItems->_getItemID());
    }

    //add an item to a cart using the API
    public function _addItemToCart_API($item, $cart_id, array $values = null){
        $Rest = $this->getModule('REST');
        codecept_debug("\n* Adding ".$item. " to the cart using the API");

        $API_Add_Item = [
            "item_id" => $this->Items[$item]["item_id"],
            "item_type" => "item",
            "quantity" => 1,
            "unit_price" => $this->Items[$item][$this->db_Requirements]["unit_price"],
            "discount_percent" => 0,
            "timeframe_id" => null,
            "special_id" => null,
            "punch_card_id" => null,
            "selected" => true,
            "params" => [],
            "unit_price_includes_tax" => boolval($this->Items[$item][$this->db_Requirements]["unit_price_includes_tax"]),
            "erange_size" => strval($this->Items[$item][$this->db_Requirements]["erange_size"])
        ];

        if($values != null) {
            $API_Add_Item = array_merge($API_Add_Item, $values);
        }

        //add item to cart
        $Rest->sendPUT('/index.php/api/cart/'.$cart_id.'/items/1', json_encode($API_Add_Item));
        $response = $Rest->grabResponse();
    }

    /**
     * Add an item to a cart using the database.
     * Uses array_merge() to overwrite default values with $values for columns
     * in the foreup_pos_cart_items table. Values related to the item specified
     * by $item will automatically be pulled from the Items class.
     *
     * @param  string $item - item name
     * @param  $cart_id - cart to place item in
     * @param  array $values - (optional) desired values not tracked by ItemsHelper objects
     * @return null  no return value
     */
    public function _addItemToCart_DB($item, $cart_id, array $values = null){
        //get connection to database
        $DbHelper = $this->getModule('DbHelper');
        codecept_debug("\n* Adding ".$item. " to the cart using the database");

        //set values needed by database
        $db_requirements = [
            "cart_id" => $cart_id,
            "line" => 1,
            "item_id" => $this->Items[$item]["item_id"],
            "quantity" => 1,
            "unit_price" => $this->Items[$item][$this->db_Requirements]["unit_price"],
            "discount_percent" => 0.00,
            "item_type" => $this->Items[$item]["item_type"],
            "item_number" => 0,
            "price_class_id" => 0,
            "timeframe_id" => 0,
            "special_id" => 0,
            "params" => "",
            "punch_card_id" => 0,
            "tournament_id" => 0,
            "invoice_id" => 0,
            "selected" => 1,
            "unit_price_includes_tax" => 0
        ];

        //merge user supplied values
        if($values != null) {
            $db_requirements = array_merge($db_requirements, $values);
        }

        if($this->Items[$item]["item_type"] === 'green_fee'){
            $db_requirements['price_class_id'] = $this->FeeItems->_getPriceClassID();
            $db_requirements['timeframe_id'] = $this->FeeItems->_getTimeframeID();
            $db_requirements['params'] = $this->FeeItems->_getParams();
            $db_requirements['unit_price_includes_tax'] = $this->FeeItems->_getUnitPriceIncludesTax();
        }

        //place items into cart
        $DbHelper->placeInDatabase('foreup_pos_cart_items', $db_requirements);
    }

    /**
     * Helper function for calculating the total tax
     *
     * @param  string $item - item name
     * @param  float $cost - cost of $item
     * @return float - tax for $item
     */
    private function calculateTax($item, $cost){
        $tax = 0;
        foreach($this->Items[$item]["Tax"] as $t) {
            $tax += $cost * ($t["percent"] / 100);
        }
        return $tax;
    }

    /**
     * Helper function for calculating a single tax rate
     *
     * @param  float $cost - cost of $item
     * @param  float $taxPercent - tax percentage
     * @return float - tax for $ amount specified in $cost.  value rounded to two decimal places.
     */
    public function tax($cost, $taxPercent){
        return round($cost * ($taxPercent / 100), 2, PHP_ROUND_HALF_UP);
    }

    /**
     * Calculates the tax for $item and returns the total
     * cost of $item
     *
     * @param  string $item - item name
     * @param  int $quantity(optional) - how many items
     * @return float - total cost of $item
     */
    public function _totalCost($item, $quantity = 1){
        $taxIncluded = $this->Items[$item][$this->db_Requirements]["unit_price_includes_tax"];
        $cost = $this->Items[$item][$this->db_Requirements]["unit_price"] * $quantity;
        $tax = null;

        if(!$taxIncluded && isset($this->Items[$item]["Tax"])){
            $tax = $this->calculateTax($item, $cost);
            $cost += round($tax, 2, PHP_ROUND_HALF_UP);
        }
        return $cost;
    }

    /**
     * Calculates the tax for $item(mult. items) and returns the tax on
     * the items.
     *
     * @param  string $item - item name
     * @return float - tax applied to $item
     */
    public function _tax($item){
        $taxIncluded = $this->Items[$item][$this->db_Requirements]["unit_price_includes_tax"];
        $cost = $this->Items[$item][$this->db_Requirements]["unit_price"];
        $tax = 0;

        if(!$taxIncluded && isset($this->Items[$item]["Tax"])){
            $tax = round($this->calculateTax($item, $cost), 2, PHP_ROUND_HALF_UP);
        }

        return $tax;
    }

    /**
     * Calculates the subtotal for a list of items
     *
     * @param  array $itemNames - item names
     * @return float - subtotal of items in $itemNames
     */
    public function _subtotal(array $itemNames){
        $subtotal = 0;
        foreach($itemNames as $name){
            $subtotal += $this->Items[$name][$this->db_Requirements]['unit_price'];
        }
        return $subtotal;
    }

    /**
     * Returns all information about the item specified in $item
     * that is stored in the private data member $Items.
     *
     * @param  string $item - item name
     * @return array
     */
    public function _getItem($item){
        return $this->Items[$item];
    }

    /**
     * returns the item number for $item
     *
     * @param  string $item - item name
     * @return int - item number
     */
    public function _getItemNumber($item){
        return $this->Items[$item][$this->db_Requirements]["item_number"];
    }

    /**
     * Returns the inventory level
     *
     * @return int - inventory level
     */
    public function _getInventoryLevel(){
        return $this->inventory_level;
    }

    /**
     * Returns the id of $item
     *
     * @param  string $item - item name
     * @return int - id of $item
     */
    public function _getItemID($item){
        return $this->Items[$item]["item_id"];
    }

    /**
     * takes an items ID and returns its name
     *
     * @param  int $itemID - item id
     * @return string - name of the item with $itemID or null if no match is found
     */
    public function _getItemName($itemID){
       foreach($this->Items as $item){
           if($item['item_id'] == $itemID){
               return $item[$this->db_Requirements]['name'];
           }
       }
        return null;
    }

    /**
     * Returns the Fee object Stored in this class
     *
     * @return object - Fee object containing green fees or cart fees
     */
    public function _getFeeObject(){
        return $this->FeeItems;
    }
}
