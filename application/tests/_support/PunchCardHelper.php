<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class PunchCardHelper extends \Codeception\Module
{
    private $course_id;
    private $payment_type = "punch_card";
    private $payment_description = "Punch Card:";
    private $punch_card_number = 1000000;
    private $item_id;
    private $punches;
    private $expiration_date = '0000-00-00';
    private $issued_date = '0000-00-00';
    private $punch_card_id;
    private $punches_used = 0;
    //if the item doesn't have a price class it should be set to 0
    private $item_price_class = 0;

    function __construct($course_id)
    {
        parent::__construct();
        $this->course_id = $course_id;
        $this->payment_description .= $this->punch_card_number;
        $this->issued_date = date("Y-m-d");
    }

    /**
     * Create a punch card by placing it in the database
     *
     * @param  int $item_id - the id of the item attached to the punch card
     * @param  int $punches - (optional) the number of punches the punch card can have
     */
    public function _createPunchCard($item_id, $punches = 10){
        //get connection to database
        $Db = $this->getModule('Db');
        $DbHelper = $this->getModule('DbHelper');

        //save info passed in
        $this->item_id = $item_id;
        $this->punches = $punches;

        $punchCard['card'] = [
            'course_id' => $this->course_id,
            'punch_card_number' => $this->punch_card_number,
            'expiration_date' => $this->expiration_date,
            'issued_date' => $this->issued_date,
        ];
        $DbHelper->InsertAndUpdateOnDuplicate('foreup_punch_cards', $punchCard['card']);

        $this->punch_card_id = $Db->grabFromDatabase('foreup_punch_cards', 'punch_card_id', $punchCard['card']);

        $punchCard['items'] = [
            'punch_card_id' => $this->punch_card_id,
            'item_id' => $this->item_id,
            'punches' => $this->punches,
            'used' => $this->punches_used,
            'price_class_id' => $this->item_price_class
        ];
        $DbHelper->InsertAndUpdateOnDuplicate('foreup_punch_card_items', $punchCard['items']);
    }

    public function _getPunchCardID(){
        return $this->punch_card_id;
    }

    public function _getPaymentDescription(){
        return $this->payment_description;
    }

    public function _getPaymentType(){
        return $this->payment_type;
    }

    public function _getPunchCardNumber(){
        return $this->punch_card_number;
    }

    public function _getExpirationDate(){
        return $this->expiration_date;
    }

    public function _getIssueDate(){
        return $this->issued_date;
    }

    public function _getItemID(){
        return $this->item_id;
    }

    public function _getPunches(){
        return $this->punches;
    }

    public function _setPunches($punches){
        $DbHelper = $this->getModule('DbHelper');
        $this->punches = $punches;
        $DbHelper->updateDatabase('foreup_punch_card_items', array('punch_card_id' => $this->punch_card_id), array('punches' => $this->punches));
    }

    public function _getPunchesUsed(){
        return $this->punches_used;
    }

    public function _setPunchesUsed($uses){
        $DbHelper = $this->getModule('DbHelper');
        $this->punches_used = $uses;
        //$DbHelper->updateDatabase('foreup_punch_card_items', array('punch_card_id' => $this->punch_card_id), array('used' => $this->punches_used));
    }

    public function _update_punch_card($uses){
        $DbHelper = $this->getModule('DbHelper');
        $this->punches_used = $uses;
        $DbHelper->updateDatabase('foreup_punch_card_items', array('punch_card_id' => $this->punch_card_id), array('used' => $this->punches_used, 'punches' => $this->punches));
    }
}
