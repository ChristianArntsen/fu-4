<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class PersonHelper extends \Codeception\Module
{
    private $person_data = [
        'first_name' => "Nikola",
        'last_name' => "Tesla",
        'phone_number' => "8017894561",
        'cell_phone_number' => "",
        'email' => "tesla@mailinator.com",
        'birthday' => '1856-07-10',
        'address_1' => '141 Tesla Ln.',
        'address_2' => "",
        'city' => 'Siberia',
        'state' => 'RI',
        'zip' => '02814',
        'country' => "",
        'comments' => "",
        'foreup_news_announcements_unsubscribe' => 0
    ];

    function __construct(array $info = null){
        parent::__construct();
        if($info !== null) {
            $this->person_data = array_merge($this->person_data, array_intersect_key($info, $this->person_data));
        }
    }

    public function _createPerson(){
        $Db = $this->getModule('Db');
        $DbHelper = $this->getModule('DbHelper');

        $DbHelper->InsertAndUpdateOnDuplicate('foreup_people', $this->person_data);
        $this->person_data['person_id'] = $Db->grabFromDatabase('foreup_people', 'person_id', $this->person_data);
    }

    public function _getPersonID(){
        return $this->person_data['person_id'];
    }

    public function _getFullName(){
        return $this->person_data['first_name']." ".$this->person_data['last_name'];
    }
}
