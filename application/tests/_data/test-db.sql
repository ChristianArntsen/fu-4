CREATE DATABASE  IF NOT EXISTS `test_db` /*!40100 DEFAULT CHARACTER SET utf16 */;
USE `test_db`;
-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: test_db
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `foreup_account_transactions`
--

DROP TABLE IF EXISTS `foreup_account_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_account_transactions` (
  `account_type` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(10) unsigned NOT NULL,
  `invoice_id` int(10) unsigned NOT NULL,
  `course_id` int(11) NOT NULL,
  `trans_customer` int(11) NOT NULL DEFAULT '0',
  `trans_household` int(11) NOT NULL,
  `trans_user` int(11) DEFAULT NULL,
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_description` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `balance_transfer` tinyint(4) NOT NULL,
  `running_balance` decimal(15,2) DEFAULT NULL,
  `hide_on_invoices` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`trans_id`),
  KEY `foreup_account_transactions_ibfk_1` (`trans_customer`),
  KEY `foreup_account_transactions_ibfk_2` (`trans_user`),
  KEY `sale_id` (`sale_id`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5670 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_actions`
--

DROP TABLE IF EXISTS `foreup_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_actions` (
  `action_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `employee_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `action_object_type` varchar(255) NOT NULL,
  `action_object_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_ad_clicks`
--

DROP TABLE IF EXISTS `foreup_ad_clicks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_ad_clicks` (
  `ad_id` int(11) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `campaign_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_ad_views`
--

DROP TABLE IF EXISTS `foreup_ad_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_ad_views` (
  `ad_id` int(11) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `campaign_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_ads`
--

DROP TABLE IF EXISTS `foreup_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_ads` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `limit` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_api_key_permissions`
--

DROP TABLE IF EXISTS `foreup_api_key_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_api_key_permissions` (
  `api_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `sales` tinyint(4) NOT NULL,
  `extended_permissions` tinyint(4) NOT NULL,
  `tee_time_sales` tinyint(4) NOT NULL,
  UNIQUE KEY `course_id` (`api_id`,`course_id`),
  KEY `api_id` (`api_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_api_keys`
--

DROP TABLE IF EXISTS `foreup_api_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_api_limits`
--

DROP TABLE IF EXISTS `foreup_api_limits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_api_limits` (
  `uri` varchar(255) NOT NULL,
  `api_key` varchar(40) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_api_logs`
--

DROP TABLE IF EXISTS `foreup_api_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_api_logs` (
  `uri` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `params` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `authorized` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_app_files`
--

DROP TABLE IF EXISTS `foreup_app_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_app_files` (
  `CID` int(11) NOT NULL,
  `file_id` int(10) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_data` longblob NOT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_auto_mailer_campaigns`
--

DROP TABLE IF EXISTS `foreup_auto_mailer_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_auto_mailer_campaigns` (
  `auto_mailer_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `trigger` tinyint(4) NOT NULL,
  `days_from_trigger` tinyint(4) NOT NULL,
  PRIMARY KEY (`auto_mailer_id`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_auto_mailer_recipients`
--

DROP TABLE IF EXISTS `foreup_auto_mailer_recipients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_auto_mailer_recipients` (
  `person_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `auto_mailer_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`person_id`,`auto_mailer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_auto_mailers`
--

DROP TABLE IF EXISTS `foreup_auto_mailers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_auto_mailers` (
  `auto_mailer_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`auto_mailer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_billing`
--

DROP TABLE IF EXISTS `foreup_billing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_billing` (
  `billing_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` mediumint(9) NOT NULL,
  `contact_email` varchar(40) NOT NULL,
  `tax_name` varchar(255) NOT NULL,
  `tax_rate` float(15,3) NOT NULL,
  `product` tinyint(4) NOT NULL,
  `start_date` date NOT NULL,
  `period_start` tinyint(4) NOT NULL,
  `period_end` tinyint(4) NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `email_limit` mediumint(9) NOT NULL,
  `text_limit` mediumint(9) NOT NULL,
  `annual` tinyint(4) NOT NULL,
  `monthly` tinyint(4) NOT NULL,
  `teetimes` tinyint(4) NOT NULL,
  `free` tinyint(4) NOT NULL,
  `annual_amount` float(15,2) NOT NULL,
  `monthly_amount` float(15,2) NOT NULL,
  `teetimes_daily` tinyint(4) NOT NULL,
  `teetimes_weekly` tinyint(4) NOT NULL,
  `annual_month` tinyint(4) NOT NULL,
  `annual_day` tinyint(4) NOT NULL,
  `monthly_day` tinyint(4) NOT NULL,
  `teesheet_id` mediumint(9) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `last_billing_attempt` date NOT NULL,
  `started` tinyint(4) NOT NULL,
  `charged` tinyint(4) NOT NULL,
  `emailed` tinyint(4) NOT NULL,
  PRIMARY KEY (`billing_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_billing_charge_items`
--

DROP TABLE IF EXISTS `foreup_billing_charge_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_billing_charge_items` (
  `charge_id` int(11) NOT NULL,
  `line_number` tinyint(4) NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` float(15,2) NOT NULL,
  UNIQUE KEY `charge_id` (`charge_id`,`line_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_billing_charges`
--

DROP TABLE IF EXISTS `foreup_billing_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_billing_charges` (
  `charge_id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_card_id` int(11) NOT NULL,
  `billing_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total` float(15,2) NOT NULL,
  PRIMARY KEY (`charge_id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_billing_credit_cards`
--

DROP TABLE IF EXISTS `foreup_billing_credit_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_billing_credit_cards` (
  `credit_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` mediumint(9) NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_expiration` date NOT NULL,
  `card_type` varchar(255) NOT NULL,
  `cardholder_name` varchar(255) NOT NULL,
  `expiration` date NOT NULL,
  `masked_account` varchar(255) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`credit_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_booking_class_groups`
--

DROP TABLE IF EXISTS `foreup_booking_class_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_booking_class_groups` (
  `booking_class_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`booking_class_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_booking_class_validators`
--

DROP TABLE IF EXISTS `foreup_booking_class_validators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_booking_class_validators` (
  `booking_class_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_booking_classes`
--

DROP TABLE IF EXISTS `foreup_booking_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_booking_classes` (
  `booking_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `teesheet_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price_class` varchar(255) NOT NULL,
  `online_booking_protected` tinyint(4) NOT NULL,
  `require_credit_card` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `online_open_time` varchar(20) NOT NULL,
  `online_close_time` varchar(20) NOT NULL,
  `days_in_booking_window` smallint(6) NOT NULL,
  `minimum_players` tinyint(4) NOT NULL,
  `limit_holes` tinyint(4) NOT NULL,
  `booking_carts` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `use_customer_pricing` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_aggregate` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pay_online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`booking_class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_checkins`
--

DROP TABLE IF EXISTS `foreup_checkins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_checkins` (
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`course_id`,`time`),
  KEY `user_id` (`user_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_communications`
--

DROP TABLE IF EXISTS `foreup_communications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_communications` (
  `type` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recipient_count` tinyint(4) NOT NULL,
  `course_id` int(11) NOT NULL,
  `from` varchar(255) NOT NULL,
  `campaign_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_course_app_data`
--

DROP TABLE IF EXISTS `foreup_course_app_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_course_app_data` (
  `course_id` int(11) NOT NULL,
  `course_latitude` decimal(10,6) NOT NULL,
  `course_longitude` decimal(10,6) NOT NULL,
  `red` float NOT NULL,
  `green` float NOT NULL,
  `blue` float NOT NULL,
  `alpha` float NOT NULL,
  `home_view` int(11) NOT NULL,
  `gps_view` int(11) NOT NULL,
  `scorecard_view` int(11) NOT NULL,
  `teetime_view` int(11) NOT NULL,
  `food_and_beverage_view` int(11) NOT NULL,
  `last_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_course_areas`
--

DROP TABLE IF EXISTS `foreup_course_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_course_areas` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_course_group_management`
--

DROP TABLE IF EXISTS `foreup_course_group_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_course_group_management` (
  `group_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_course_group_members`
--

DROP TABLE IF EXISTS `foreup_course_group_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_course_group_members` (
  `group_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_course_groups`
--

DROP TABLE IF EXISTS `foreup_course_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_course_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `shared_tee_sheet` tinyint(4) NOT NULL DEFAULT '1',
  `shared_customers` tinyint(4) NOT NULL DEFAULT '1',
  `shared_giftcards` tinyint(4) NOT NULL DEFAULT '1',
  `shared_price_classes` tinyint(4) NOT NULL DEFAULT '0',
  `aggregate_booking` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_course_increments`
--

DROP TABLE IF EXISTS `foreup_course_increments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_course_increments` (
  `course_id` int(10) unsigned NOT NULL,
  `type` enum('sales') NOT NULL,
  `current_index` int(10) unsigned NOT NULL DEFAULT '1',
  UNIQUE KEY `course type` (`course_id`,`type`),
  KEY `course_id` (`course_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_course_indices`
--

DROP TABLE IF EXISTS `foreup_course_indices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_course_indices` (
  `course_id` int(10) unsigned NOT NULL,
  `index_type` enum('sales') NOT NULL,
  `next_index` int(10) unsigned NOT NULL DEFAULT '1',
  KEY `course_id` (`course_id`),
  KEY `type` (`index_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_course_messages`
--

DROP TABLE IF EXISTS `foreup_course_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_course_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date_posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `read` tinyint(4) NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_courses`
--

DROP TABLE IF EXISTS `foreup_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `active_course` tinyint(4) NOT NULL DEFAULT '1',
  `demo` tinyint(4) NOT NULL,
  `maintenance_mode` tinyint(4) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `area_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `course_app_data_id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `address` varchar(40) NOT NULL,
  `city` varchar(40) NOT NULL,
  `state` varchar(20) NOT NULL,
  `state_name` varchar(50) NOT NULL,
  `postal` varchar(10) NOT NULL,
  `woeid` varchar(25) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `country` varchar(40) NOT NULL,
  `area_code` varchar(10) NOT NULL,
  `timezone` varchar(20) NOT NULL,
  `DST` varchar(20) NOT NULL,
  `latitude_centroid` float(15,2) NOT NULL,
  `latitude_poly` varchar(30) NOT NULL,
  `longitude_centroid` float(15,6) NOT NULL,
  `longitude_poly` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `holes` varchar(10) NOT NULL,
  `type` varchar(10) NOT NULL,
  `county` varchar(40) NOT NULL,
  `open_time` varchar(10) NOT NULL DEFAULT '0600',
  `close_time` varchar(10) NOT NULL DEFAULT '1900',
  `increment` varchar(10) NOT NULL DEFAULT '8',
  `frontnine` varchar(10) NOT NULL DEFAULT '200',
  `early_bird_hours_begin` varchar(10) NOT NULL,
  `early_bird_hours_end` varchar(10) NOT NULL,
  `morning_hours_begin` varchar(10) NOT NULL,
  `morning_hours_end` varchar(10) NOT NULL,
  `afternoon_hours_begin` varchar(10) NOT NULL,
  `afternoon_hours_end` varchar(10) NOT NULL,
  `twilight_hour` varchar(10) NOT NULL DEFAULT '1400',
  `super_twilight_hour` varchar(10) NOT NULL DEFAULT '2399',
  `holidays` tinyint(4) NOT NULL,
  `online_booking` tinyint(1) NOT NULL DEFAULT '0',
  `online_booking_protected` tinyint(4) NOT NULL,
  `min_required_players` tinyint(4) NOT NULL DEFAULT '2',
  `min_required_carts` tinyint(4) NOT NULL,
  `min_required_holes` tinyint(4) NOT NULL,
  `booking_rules` text NOT NULL,
  `auto_mailers` tinyint(4) NOT NULL,
  `config` int(1) NOT NULL DEFAULT '1',
  `courses` int(1) NOT NULL DEFAULT '0',
  `customers` int(1) NOT NULL DEFAULT '0',
  `dashboards` int(11) NOT NULL,
  `employees` int(1) NOT NULL DEFAULT '0',
  `events` tinyint(4) NOT NULL,
  `food_and_beverage` tinyint(4) NOT NULL,
  `giftcards` int(1) NOT NULL DEFAULT '0',
  `invoices` tinyint(4) NOT NULL,
  `item_kits` int(1) NOT NULL DEFAULT '0',
  `items` int(1) NOT NULL DEFAULT '0',
  `marketing_campaigns` int(1) NOT NULL DEFAULT '0',
  `promotions` int(1) NOT NULL DEFAULT '0',
  `quickbooks` tinyint(4) NOT NULL,
  `receivings` int(1) NOT NULL DEFAULT '0',
  `recipients` tinyint(4) NOT NULL,
  `reports` int(1) NOT NULL DEFAULT '0',
  `reservations` tinyint(4) NOT NULL,
  `sales` int(1) NOT NULL DEFAULT '0',
  `schedules` tinyint(4) NOT NULL,
  `suppliers` int(1) NOT NULL DEFAULT '0',
  `teesheets` int(1) NOT NULL DEFAULT '1',
  `tournaments` tinyint(4) NOT NULL DEFAULT '0',
  `currency_symbol` varchar(3) NOT NULL DEFAULT '$',
  `currency_symbol_placement` varchar(20) NOT NULL,
  `currency_format` varchar(20) NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `date_format` varchar(20) NOT NULL DEFAULT 'middle_endian',
  `default_tax_1_name` varchar(255) NOT NULL DEFAULT 'Sales Tax',
  `default_tax_1_rate` varchar(20) NOT NULL,
  `default_tax_2_cumulative` varchar(20) NOT NULL DEFAULT '0',
  `default_tax_2_name` varchar(255) NOT NULL DEFAULT 'Sales Tax 2',
  `default_tax_2_rate` varchar(20) NOT NULL,
  `unit_price_includes_tax` tinyint(4) NOT NULL,
  `customer_credit_nickname` varchar(255) NOT NULL,
  `credit_department` tinyint(4) NOT NULL,
  `credit_department_name` varchar(255) NOT NULL,
  `credit_category` tinyint(4) NOT NULL,
  `credit_category_name` varchar(255) NOT NULL,
  `member_balance_nickname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `reservation_email` varchar(255) NOT NULL,
  `billing_email` varchar(255) NOT NULL,
  `last_invoices_sent` date NOT NULL,
  `display_email_ads` tinyint(4) NOT NULL,
  `fax` varchar(40) NOT NULL,
  `language` varchar(20) NOT NULL DEFAULT 'english',
  `mailchimp_api_key` varchar(255) NOT NULL,
  `number_of_items_per_page` varchar(10) NOT NULL DEFAULT '20',
  `hide_back_nine` tinyint(4) NOT NULL,
  `print_after_sale` varchar(10) NOT NULL DEFAULT '0',
  `webprnt` tinyint(4) NOT NULL,
  `webprnt_ip` varchar(20) NOT NULL,
  `webprnt_hot_ip` varchar(30) NOT NULL,
  `webprnt_cold_ip` varchar(30) NOT NULL,
  `webprnt_label_ip` varchar(30) NOT NULL,
  `print_two_receipts` tinyint(4) NOT NULL DEFAULT '0',
  `print_two_signature_slips` tinyint(4) NOT NULL,
  `print_two_receipts_other` tinyint(4) NOT NULL DEFAULT '0',
  `tip_line` tinyint(4) NOT NULL,
  `updated_printing` tinyint(4) NOT NULL,
  `after_sale_load` tinyint(4) NOT NULL,
  `fnb_login` tinyint(4) NOT NULL,
  `teesheet_updates_automatically` tinyint(4) NOT NULL,
  `teetime_confirmations` tinyint(4) NOT NULL,
  `teetime_reminders` tinyint(4) NOT NULL,
  `auto_split_teetimes` tinyint(4) NOT NULL,
  `teesheet_refresh_rate` tinyint(4) NOT NULL DEFAULT '90',
  `send_reservation_confirmations` tinyint(4) NOT NULL DEFAULT '1',
  `receipt_printer` varchar(255) NOT NULL,
  `print_credit_card_receipt` tinyint(4) NOT NULL,
  `print_sales_receipt` tinyint(4) NOT NULL DEFAULT '1',
  `print_tip_line` tinyint(4) NOT NULL DEFAULT '1',
  `cash_drawer_on_cash` tinyint(4) NOT NULL,
  `track_cash` varchar(10) NOT NULL,
  `blind_close` tinyint(4) NOT NULL,
  `separate_courses` tinyint(4) NOT NULL DEFAULT '0',
  `deduct_tips` tinyint(4) NOT NULL,
  `use_terminals` tinyint(4) NOT NULL,
  `use_loyalty` tinyint(4) NOT NULL DEFAULT '0',
  `return_policy` text NOT NULL,
  `time_format` varchar(20) NOT NULL DEFAULT '12_hour',
  `website` varchar(255) NOT NULL,
  `open_sun` tinyint(1) NOT NULL DEFAULT '1',
  `open_mon` tinyint(1) NOT NULL DEFAULT '1',
  `open_tue` tinyint(1) NOT NULL DEFAULT '1',
  `open_wed` tinyint(1) NOT NULL DEFAULT '1',
  `open_thu` tinyint(1) NOT NULL DEFAULT '1',
  `open_fri` tinyint(1) NOT NULL DEFAULT '1',
  `open_sat` tinyint(1) NOT NULL DEFAULT '1',
  `weekend_fri` tinyint(1) NOT NULL DEFAULT '1',
  `weekend_sat` tinyint(1) NOT NULL DEFAULT '1',
  `weekend_sun` tinyint(1) NOT NULL DEFAULT '1',
  `simulator` smallint(1) NOT NULL,
  `test_course` int(1) NOT NULL DEFAULT '0',
  `at_login` varchar(20) NOT NULL,
  `at_password` varchar(20) NOT NULL,
  `at_test` tinyint(1) NOT NULL DEFAULT '1',
  `mercury_id` varchar(255) NOT NULL,
  `mercury_password` varchar(255) NOT NULL,
  `mercury_e2e_id` varchar(255) NOT NULL,
  `mercury_e2e_password` varchar(255) NOT NULL,
  `ets_key` varchar(255) NOT NULL,
  `e2e_account_id` varchar(255) NOT NULL,
  `e2e_account_key` varchar(255) NOT NULL,
  `use_ets_giftcards` tinyint(4) NOT NULL,
  `facebook_page_id` varchar(255) NOT NULL,
  `facebook_page_name` varchar(255) NOT NULL,
  `facebook_extended_access_token` varchar(255) NOT NULL,
  `allow_friends_to_invite` tinyint(1) NOT NULL,
  `payment_required` tinyint(1) NOT NULL,
  `send_email_reminder` tinyint(1) NOT NULL,
  `send_text_reminder` tinyint(1) NOT NULL,
  `seasonal_pricing` tinyint(1) NOT NULL DEFAULT '0',
  `include_tax_online_booking` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `foreup_discount_percent` double(15,2) NOT NULL DEFAULT '25.00',
  `no_show_policy` varchar(2048) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `reservation_email_text` text NOT NULL,
  `reservation_email_photo` int(10) unsigned NOT NULL,
  `ibeacon_major_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ibeacon_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `auto_gratuity` decimal(15,2) NOT NULL,
  `minimum_food_spend` float NOT NULL,
  `hide_employee_last_name_receipt` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `quickbooks_export_settings` text NOT NULL,
  `food_and_beverage_v2` tinyint(1) DEFAULT NULL,
  `sales_v2` tinyint(1) DEFAULT NULL,
  `require_employee_pin` tinyint(4) NOT NULL,
  `erange_id` varchar(255) NOT NULL,
  `erange_password` varchar(255) NOT NULL,
  `credit_card_fee` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `multiple_printers` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `require_guest_count` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `default_kitchen_printer` int(10) unsigned NOT NULL DEFAULT '0',
  `clean_out_nightly` tinyint(4) NOT NULL DEFAULT '0',
  `element_account_id` varchar(255) DEFAULT NULL,
  `element_account_token` varchar(255) DEFAULT NULL,
  `element_application_id` varchar(255) DEFAULT NULL,
  `element_acceptor_id` varchar(255) DEFAULT NULL,
  `use_course_firing` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `course_firing_include_items` tinyint(1) NOT NULL DEFAULT '0',
  `tee_sheet_speed_up` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `use_kitchen_buzzers` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hide_modifier_names_kitchen_receipts` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `food_bev_sort_by_seat` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `disable_facebook_widget` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `service_fee_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `service_fee_tax_1_name` varchar(255) NOT NULL DEFAULT 'Sales Tax 1',
  `service_fee_tax_1_rate` decimal(15,2) unsigned NOT NULL DEFAULT '0.00',
  `service_fee_tax_2_name` varchar(255) NOT NULL DEFAULT 'Sales Tax 2',
  `service_fee_tax_2_rate` decimal(15,2) unsigned NOT NULL DEFAULT '0.00',
  `default_register_log_open` decimal(15,2) NOT NULL DEFAULT '100.00',
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18886 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_courses_information`
--

DROP TABLE IF EXISTS `foreup_courses_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_courses_information` (
  `course_id` int(11) NOT NULL,
  `stage` enum('demo','setup','live','cancelled') NOT NULL,
  `course_type` enum('public','semi','private','simulator','other') NOT NULL,
  `course_health` enum('red','yellow','green') NOT NULL,
  `mobile_app` enum('Y','N') NOT NULL,
  `sales_rep` int(11) DEFAULT NULL,
  `course_value` int(2) NOT NULL,
  `sphere_influence` int(2) NOT NULL,
  `apr` decimal(10,2) NOT NULL,
  `date_sold` date NOT NULL,
  `date_contract_ends` date NOT NULL,
  `date_cancelled` date NOT NULL,
  PRIMARY KEY (`course_id`),
  KEY `sales_rep` (`sales_rep`),
  CONSTRAINT `FK_foreup_courses_information_foreup_courses` FOREIGN KEY (`course_id`) REFERENCES `foreup_courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_foreup_courses_information_foreup_employees` FOREIGN KEY (`sales_rep`) REFERENCES `foreup_employees` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_courses_information_setup`
--

DROP TABLE IF EXISTS `foreup_courses_information_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_courses_information_setup` (
  `course_id` int(11) NOT NULL,
  `setup_rep` int(11) DEFAULT NULL,
  `date_golive_goal` date NOT NULL,
  `date_imports_completed` date NOT NULL,
  `date_training_completed` date NOT NULL,
  `date_hardward_setup` date NOT NULL,
  `date_live` date NOT NULL,
  PRIMARY KEY (`course_id`),
  KEY `sales_rep` (`setup_rep`),
  CONSTRAINT `foreup_courses_information_setup_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `foreup_courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreup_courses_information_setup_ibfk_2` FOREIGN KEY (`setup_rep`) REFERENCES `foreup_employees` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_courses_information_website`
--

DROP TABLE IF EXISTS `foreup_courses_information_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_courses_information_website` (
  `course_id` int(11) NOT NULL,
  `website_rep` int(11) DEFAULT NULL,
  `stage` int(11) DEFAULT NULL,
  `date_sold` date NOT NULL,
  `date_golive_goal` date NOT NULL,
  `date_started` date NOT NULL,
  `date_sent_for_review` date NOT NULL,
  `date_live` date NOT NULL,
  PRIMARY KEY (`course_id`),
  KEY `sales_rep` (`website_rep`),
  KEY `FK_foreup_steps` (`stage`),
  CONSTRAINT `FK_foreup_steps` FOREIGN KEY (`stage`) REFERENCES `foreup_courses_information_website_steps` (`step_number`),
  CONSTRAINT `foreup_courses_information_website_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `foreup_courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreup_courses_information_website_ibfk_2` FOREIGN KEY (`website_rep`) REFERENCES `foreup_employees` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_courses_information_website_steps`
--

DROP TABLE IF EXISTS `foreup_courses_information_website_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_courses_information_website_steps` (
  `step_number` int(11) NOT NULL,
  `step_name` varchar(255) NOT NULL,
  PRIMARY KEY (`step_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_credit_card_transaction_steps`
--

DROP TABLE IF EXISTS `foreup_credit_card_transaction_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_credit_card_transaction_steps` (
  `course_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `initialized` tinyint(4) NOT NULL,
  `response_received` tinyint(4) NOT NULL,
  `approved` tinyint(4) NOT NULL,
  `payment_added` tinyint(4) NOT NULL,
  `sale_completed` tinyint(4) NOT NULL,
  UNIQUE KEY `transaction_id` (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_custom_payment_types`
--

DROP TABLE IF EXISTS `foreup_custom_payment_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_custom_payment_types` (
  `course_id` int(10) unsigned DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `payment_id` varchar(255) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `custom_payment_type` varchar(255) DEFAULT '0',
  KEY `course_id` (`course_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_customer_billing`
--

DROP TABLE IF EXISTS `foreup_customer_billing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_customer_billing` (
  `billing_id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `course_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `email_invoice` tinyint(4) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `description` varchar(255) NOT NULL,
  `frequency` smallint(6) unsigned NOT NULL,
  `frequency_period` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `frequency_on` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `frequency_on_date` date NOT NULL,
  `due_days` smallint(5) unsigned NOT NULL,
  `stop_after` smallint(5) unsigned DEFAULT NULL,
  `last_invoice_generation_attempt` date NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `auto_bill_delay` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `last_billing_attempt` date NOT NULL,
  `started` tinyint(4) NOT NULL,
  `charged` tinyint(4) NOT NULL,
  `emailed` tinyint(4) NOT NULL,
  `show_account_transactions` tinyint(1) NOT NULL DEFAULT '0',
  `attempt_limit` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`billing_id`),
  KEY `course_id` (`course_id`),
  KEY `person_id` (`person_id`),
  KEY `start_date` (`start_date`,`end_date`),
  KEY `batch_id` (`batch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_customer_billing_items`
--

DROP TABLE IF EXISTS `foreup_customer_billing_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_customer_billing_items` (
  `billing_id` int(11) NOT NULL,
  `line` tinyint(4) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `item_type` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `quantity` smallint(6) NOT NULL,
  `price` float(15,2) NOT NULL,
  `tax_percentage` float(15,2) NOT NULL,
  UNIQUE KEY `billing_id` (`billing_id`,`line`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_customer_credit_cards`
--

DROP TABLE IF EXISTS `foreup_customer_credit_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_customer_credit_cards` (
  `credit_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `ets_key` varchar(255) NOT NULL,
  `mercury_id` varchar(255) NOT NULL,
  `mercury_password` varchar(255) NOT NULL,
  `recurring` tinyint(4) NOT NULL,
  `course_id` mediumint(9) NOT NULL,
  `customer_id` mediumint(9) NOT NULL,
  `tee_time_id` varchar(21) NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_expiration` date NOT NULL,
  `card_type` varchar(255) NOT NULL,
  `cardholder_name` varchar(255) NOT NULL,
  `expiration` date NOT NULL,
  `masked_account` varchar(255) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `successful_charges` smallint(6) NOT NULL,
  `failed_charges` smallint(6) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`credit_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_customer_food_spending`
--

DROP TABLE IF EXISTS `foreup_customer_food_spending`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_customer_food_spending` (
  `course_id` int(11) unsigned NOT NULL,
  `person_id` int(11) unsigned NOT NULL,
  `minimum` decimal(15,2) NOT NULL DEFAULT '0.00',
  `spent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `amount` decimal(15,2) DEFAULT '0.00',
  `month` int(2) unsigned NOT NULL,
  `year` int(4) unsigned NOT NULL,
  `transaction_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_customer_group_members`
--

DROP TABLE IF EXISTS `foreup_customer_group_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_customer_group_members` (
  `group_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_customer_groups`
--

DROP TABLE IF EXISTS `foreup_customer_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_customer_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_customer_pass_members`
--

DROP TABLE IF EXISTS `foreup_customer_pass_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_customer_pass_members` (
  `pass_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  PRIMARY KEY (`pass_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_customer_passes`
--

DROP TABLE IF EXISTS `foreup_customer_passes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_customer_passes` (
  `pass_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`pass_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_customers`
--

DROP TABLE IF EXISTS `foreup_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_customers` (
  `CID` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `course_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `member` smallint(6) NOT NULL,
  `price_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) unsigned NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_balance` decimal(15,2) NOT NULL,
  `account_balance_allow_negative` tinyint(4) NOT NULL,
  `account_limit` decimal(15,2) NOT NULL,
  `member_account_balance` decimal(15,2) NOT NULL,
  `member_account_balance_allow_negative` tinyint(4) NOT NULL DEFAULT '1',
  `member_account_limit` decimal(15,2) NOT NULL,
  `invoice_balance` decimal(15,2) NOT NULL,
  `invoice_balance_allow_negative` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `invoice_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `use_loyalty` tinyint(4) NOT NULL DEFAULT '1',
  `loyalty_points` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `discount` float(5,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `opt_out_email` tinyint(4) NOT NULL DEFAULT '0',
  `opt_out_text` tinyint(4) NOT NULL DEFAULT '0',
  `unsubscribe_all` tinyint(1) NOT NULL,
  `course_news_announcements` tinyint(1) NOT NULL,
  `teetime_reminders` tinyint(1) NOT NULL,
  `status_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Red, 2 = Yellow, 3 = Green',
  `require_food_minimum` smallint(6) NOT NULL,
  UNIQUE KEY `course customer` (`course_id`,`person_id`),
  UNIQUE KEY `account_number2` (`course_id`,`account_number`),
  KEY `person_id` (`person_id`),
  KEY `deleted` (`deleted`),
  KEY `username` (`username`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `foreup_customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `foreup_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_daily_sync`
--

DROP TABLE IF EXISTS `foreup_daily_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_daily_sync` (
  `date` date NOT NULL,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_dashboard_widget_metrics`
--

DROP TABLE IF EXISTS `foreup_dashboard_widget_metrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_dashboard_widget_metrics` (
  `widget_id` int(10) unsigned NOT NULL,
  `metric_category` varchar(255) NOT NULL,
  `metric` varchar(255) NOT NULL,
  `relative_period_num` tinyint(2) NOT NULL,
  `position` int(10) unsigned NOT NULL,
  PRIMARY KEY (`widget_id`,`metric_category`,`metric`,`relative_period_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_dashboard_widgets`
--

DROP TABLE IF EXISTS `foreup_dashboard_widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_dashboard_widgets` (
  `widget_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `period` varchar(24) NOT NULL,
  `data_grouping` varchar(8) NOT NULL,
  `pos_x` int(10) unsigned NOT NULL DEFAULT '0',
  `pos_y` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `dashboard_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`widget_id`),
  KEY `metric_id` (`dashboard_id`,`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_dashboards`
--

DROP TABLE IF EXISTS `foreup_dashboards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_dashboards` (
  `dashboard_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `course_id` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`dashboard_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_employee_permissions`
--

DROP TABLE IF EXISTS `foreup_employee_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_employee_permissions` (
  `person_id` int(11) NOT NULL,
  `sales_stats` tinyint(4) NOT NULL,
  UNIQUE KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_employees`
--

DROP TABLE IF EXISTS `foreup_employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_employees` (
  `course_id` int(11) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `TSID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_level` tinyint(4) NOT NULL DEFAULT '1',
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pin` mediumint(9) DEFAULT NULL,
  `card` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_id` int(11) unsigned NOT NULL,
  `person_id` int(10) NOT NULL,
  `activated` int(1) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL,
  `zendesk` tinyint(4) NOT NULL,
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `pin` (`course_id`,`pin`),
  UNIQUE KEY `card` (`course_id`,`card`),
  KEY `person_id` (`person_id`),
  KEY `deleted` (`deleted`),
  CONSTRAINT `foreup_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `foreup_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_event_people`
--

DROP TABLE IF EXISTS `foreup_event_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_event_people` (
  `teetime_id` varchar(21) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `checked_in` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `paid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cart` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_checked_in` datetime DEFAULT NULL,
  `date_paid` datetime DEFAULT NULL,
  `player_position` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hole` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `teetime_id` (`teetime_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_favorites`
--

DROP TABLE IF EXISTS `foreup_favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_favorites` (
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`course_id`,`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_fee_types`
--

DROP TABLE IF EXISTS `foreup_fee_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_fee_types` (
  `course_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `price_category_1` varchar(255) NOT NULL,
  `price_category_2` varchar(255) NOT NULL,
  `price_category_3` varchar(255) NOT NULL,
  `price_category_4` varchar(255) NOT NULL,
  `price_category_5` varchar(255) NOT NULL,
  `price_category_6` varchar(255) NOT NULL,
  `price_category_7` varchar(255) NOT NULL,
  `price_category_8` varchar(255) NOT NULL,
  `price_category_9` varchar(255) NOT NULL,
  `price_category_10` varchar(255) NOT NULL,
  `price_category_11` varchar(255) NOT NULL,
  `price_category_12` varchar(255) NOT NULL,
  `price_category_13` varchar(255) NOT NULL,
  `price_category_14` varchar(255) NOT NULL,
  `price_category_15` varchar(255) NOT NULL,
  `price_category_16` varchar(255) NOT NULL,
  `price_category_17` varchar(255) NOT NULL,
  `price_category_18` varchar(255) NOT NULL,
  `price_category_19` varchar(255) NOT NULL,
  `price_category_20` varchar(255) NOT NULL,
  `price_category_21` varchar(255) NOT NULL,
  `price_category_22` varchar(255) NOT NULL,
  `price_category_23` varchar(255) NOT NULL,
  `price_category_24` varchar(255) NOT NULL,
  `price_category_25` varchar(255) NOT NULL,
  `price_category_26` varchar(255) NOT NULL,
  `price_category_27` varchar(255) NOT NULL,
  `price_category_28` varchar(255) NOT NULL,
  `price_category_29` varchar(255) NOT NULL,
  `price_category_30` varchar(255) NOT NULL,
  `price_category_31` varchar(255) NOT NULL,
  `price_category_32` varchar(255) NOT NULL,
  `price_category_33` varchar(255) NOT NULL,
  `price_category_34` varchar(255) NOT NULL,
  `price_category_35` varchar(255) NOT NULL,
  `price_category_36` varchar(255) NOT NULL,
  `price_category_37` varchar(255) NOT NULL,
  `price_category_38` varchar(255) NOT NULL,
  `price_category_39` varchar(255) NOT NULL,
  `price_category_40` varchar(255) NOT NULL,
  `price_category_41` varchar(255) NOT NULL,
  `price_category_42` varchar(255) NOT NULL,
  `price_category_43` varchar(255) NOT NULL,
  `price_category_44` varchar(255) NOT NULL,
  `price_category_45` varchar(255) NOT NULL,
  `price_category_46` varchar(255) NOT NULL,
  `price_category_47` varchar(255) NOT NULL,
  `price_category_48` varchar(255) NOT NULL,
  `price_category_49` varchar(255) NOT NULL,
  `price_category_50` varchar(255) NOT NULL,
  PRIMARY KEY (`course_id`,`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_fees`
--

DROP TABLE IF EXISTS `foreup_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_fees` (
  `schedule_id` int(11) NOT NULL,
  `item_number` varchar(255) NOT NULL,
  `price_category_1` decimal(15,2) NOT NULL,
  `price_category_2` decimal(15,2) NOT NULL,
  `price_category_3` decimal(15,2) NOT NULL,
  `price_category_4` decimal(15,2) NOT NULL,
  `price_category_5` decimal(15,2) NOT NULL,
  `price_category_6` decimal(15,2) NOT NULL,
  `price_category_7` decimal(15,2) NOT NULL,
  `price_category_8` decimal(15,2) NOT NULL,
  `price_category_9` decimal(15,2) NOT NULL,
  `price_category_10` decimal(15,2) NOT NULL,
  `price_category_11` decimal(15,2) NOT NULL,
  `price_category_12` decimal(15,2) NOT NULL,
  `price_category_13` decimal(15,2) NOT NULL,
  `price_category_14` decimal(15,2) NOT NULL,
  `price_category_15` decimal(15,2) NOT NULL,
  `price_category_16` decimal(15,2) NOT NULL,
  `price_category_17` decimal(15,2) NOT NULL,
  `price_category_18` decimal(15,2) NOT NULL,
  `price_category_19` decimal(15,2) NOT NULL,
  `price_category_20` decimal(15,2) NOT NULL,
  `price_category_21` decimal(15,2) NOT NULL,
  `price_category_22` decimal(15,2) NOT NULL,
  `price_category_23` decimal(15,2) NOT NULL,
  `price_category_24` decimal(15,2) NOT NULL,
  `price_category_25` decimal(15,2) NOT NULL,
  `price_category_26` decimal(15,2) NOT NULL,
  `price_category_27` decimal(15,2) NOT NULL,
  `price_category_28` decimal(15,2) NOT NULL,
  `price_category_29` decimal(15,2) NOT NULL,
  `price_category_30` decimal(15,2) NOT NULL,
  `price_category_31` decimal(15,2) NOT NULL,
  `price_category_32` decimal(15,2) NOT NULL,
  `price_category_33` decimal(15,2) NOT NULL,
  `price_category_34` decimal(15,2) NOT NULL,
  `price_category_35` decimal(15,2) NOT NULL,
  `price_category_36` decimal(15,2) NOT NULL,
  `price_category_37` decimal(15,2) NOT NULL,
  `price_category_38` decimal(15,2) NOT NULL,
  `price_category_39` decimal(15,2) NOT NULL,
  `price_category_40` decimal(15,2) NOT NULL,
  `price_category_41` decimal(15,2) NOT NULL,
  `price_category_42` decimal(15,2) NOT NULL,
  `price_category_43` decimal(15,2) NOT NULL,
  `price_category_44` decimal(15,2) NOT NULL,
  `price_category_45` decimal(15,2) NOT NULL,
  `price_category_46` decimal(15,2) NOT NULL,
  `price_category_47` decimal(15,2) NOT NULL,
  `price_category_48` decimal(15,2) NOT NULL,
  `price_category_49` decimal(15,2) NOT NULL,
  `price_category_50` decimal(15,2) NOT NULL,
  PRIMARY KEY (`schedule_id`,`item_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_giftcard_transactions`
--

DROP TABLE IF EXISTS `foreup_giftcard_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_giftcard_transactions` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `trans_giftcard` int(11) NOT NULL,
  `trans_customer` int(11) NOT NULL,
  `trans_user` int(11) NOT NULL,
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_description` text NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`trans_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5545 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_giftcards`
--

DROP TABLE IF EXISTS `foreup_giftcards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_giftcards` (
  `CID` int(11) NOT NULL,
  `giftcard_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `giftcard_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` decimal(15,2) NOT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `date_issued` date NOT NULL,
  `expiration_date` date NOT NULL,
  `department` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_number` (`giftcard_number`,`course_id`),
  KEY `deleted` (`deleted`),
  KEY `foreup_giftcards_ibfk_1` (`customer_id`),
  CONSTRAINT `foreup_giftcards_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `foreup_customers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37256 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_green_fee_types`
--

DROP TABLE IF EXISTS `foreup_green_fee_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_green_fee_types` (
  `course_id` int(11) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `price_category_1` varchar(255) NOT NULL,
  `price_category_2` varchar(255) NOT NULL,
  `price_category_3` varchar(255) NOT NULL,
  `price_category_4` varchar(255) NOT NULL,
  `price_category_5` varchar(255) NOT NULL,
  `price_category_6` varchar(255) NOT NULL,
  `price_category_7` varchar(255) NOT NULL,
  `price_category_8` varchar(255) NOT NULL,
  `price_category_9` varchar(255) NOT NULL,
  `price_category_10` varchar(255) NOT NULL,
  `price_category_11` varchar(255) NOT NULL,
  `price_category_12` varchar(255) NOT NULL,
  `price_category_13` varchar(255) NOT NULL,
  `price_category_14` varchar(255) NOT NULL,
  `price_category_15` varchar(255) NOT NULL,
  `price_category_16` varchar(255) NOT NULL,
  `price_category_17` varchar(255) NOT NULL,
  `price_category_18` varchar(255) NOT NULL,
  `price_category_19` varchar(255) NOT NULL,
  `price_category_20` varchar(255) NOT NULL,
  `price_category_21` varchar(255) NOT NULL,
  `price_category_22` varchar(255) NOT NULL,
  `price_category_23` varchar(255) NOT NULL,
  `price_category_24` varchar(255) NOT NULL,
  `price_category_25` varchar(255) NOT NULL,
  `price_category_26` varchar(255) NOT NULL,
  `price_category_27` varchar(255) NOT NULL,
  `price_category_28` varchar(255) NOT NULL,
  `price_category_29` varchar(255) NOT NULL,
  `price_category_30` varchar(255) NOT NULL,
  `price_category_31` varchar(255) NOT NULL,
  `price_category_32` varchar(255) NOT NULL,
  `price_category_33` varchar(255) NOT NULL,
  `price_category_34` varchar(255) NOT NULL,
  `price_category_35` varchar(255) NOT NULL,
  `price_category_36` varchar(255) NOT NULL,
  `price_category_37` varchar(255) NOT NULL,
  `price_category_38` varchar(255) NOT NULL,
  `price_category_39` varchar(255) NOT NULL,
  `price_category_40` varchar(255) NOT NULL,
  `price_category_41` varchar(255) NOT NULL,
  `price_category_42` varchar(255) NOT NULL,
  `price_category_43` varchar(255) NOT NULL,
  `price_category_44` varchar(255) NOT NULL,
  `price_category_45` varchar(255) NOT NULL,
  `price_category_46` varchar(255) NOT NULL,
  `price_category_47` varchar(255) NOT NULL,
  `price_category_48` varchar(255) NOT NULL,
  `price_category_49` varchar(255) NOT NULL,
  `price_category_50` varchar(255) NOT NULL,
  PRIMARY KEY (`course_id`,`teesheet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_green_fees`
--

DROP TABLE IF EXISTS `foreup_green_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_green_fees` (
  `teesheet_id` int(11) NOT NULL,
  `item_number` varchar(255) NOT NULL,
  `price_category_1` decimal(15,2) NOT NULL,
  `price_category_2` decimal(15,2) NOT NULL,
  `price_category_3` decimal(15,2) NOT NULL,
  `price_category_4` decimal(15,2) NOT NULL,
  `price_category_5` decimal(15,2) NOT NULL,
  `price_category_6` decimal(15,2) NOT NULL,
  `price_category_7` decimal(15,2) NOT NULL,
  `price_category_8` decimal(15,2) NOT NULL,
  `price_category_9` decimal(15,2) NOT NULL,
  `price_category_10` decimal(15,2) NOT NULL,
  `price_category_11` decimal(15,2) NOT NULL,
  `price_category_12` decimal(15,2) NOT NULL,
  `price_category_13` decimal(15,2) NOT NULL,
  `price_category_14` decimal(15,2) NOT NULL,
  `price_category_15` decimal(15,2) NOT NULL,
  `price_category_16` decimal(15,2) NOT NULL,
  `price_category_17` decimal(15,2) NOT NULL,
  `price_category_18` decimal(15,2) NOT NULL,
  `price_category_19` decimal(15,2) NOT NULL,
  `price_category_20` decimal(15,2) NOT NULL,
  `price_category_21` decimal(15,2) NOT NULL,
  `price_category_22` decimal(15,2) NOT NULL,
  `price_category_23` decimal(15,2) NOT NULL,
  `price_category_24` decimal(15,2) NOT NULL,
  `price_category_25` decimal(15,2) NOT NULL,
  `price_category_26` decimal(15,2) NOT NULL,
  `price_category_27` decimal(15,2) NOT NULL,
  `price_category_28` decimal(15,2) NOT NULL,
  `price_category_29` decimal(15,2) NOT NULL,
  `price_category_30` decimal(15,2) NOT NULL,
  `price_category_31` decimal(15,2) NOT NULL,
  `price_category_32` decimal(15,2) NOT NULL,
  `price_category_33` decimal(15,2) NOT NULL,
  `price_category_34` decimal(15,2) NOT NULL,
  `price_category_35` decimal(15,2) NOT NULL,
  `price_category_36` decimal(15,2) NOT NULL,
  `price_category_37` decimal(15,2) NOT NULL,
  `price_category_38` decimal(15,2) NOT NULL,
  `price_category_39` decimal(15,2) NOT NULL,
  `price_category_40` decimal(15,2) NOT NULL,
  `price_category_41` decimal(15,2) NOT NULL,
  `price_category_42` decimal(15,2) NOT NULL,
  `price_category_43` decimal(15,2) NOT NULL,
  `price_category_44` decimal(15,2) NOT NULL,
  `price_category_45` decimal(15,2) NOT NULL,
  `price_category_46` decimal(15,2) NOT NULL,
  `price_category_47` decimal(15,2) NOT NULL,
  `price_category_48` decimal(15,2) NOT NULL,
  `price_category_49` decimal(15,2) NOT NULL,
  `price_category_50` decimal(15,2) NOT NULL,
  PRIMARY KEY (`teesheet_id`,`item_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_group_members`
--

DROP TABLE IF EXISTS `foreup_group_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_group_members` (
  `group_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_groups`
--

DROP TABLE IF EXISTS `foreup_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_help_post_ratings`
--

DROP TABLE IF EXISTS `foreup_help_post_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_help_post_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_help_posts`
--

DROP TABLE IF EXISTS `foreup_help_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_help_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `contents` text,
  `deleted` int(1) DEFAULT '0',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `views` int(11) DEFAULT '0',
  `keywords` varchar(255) DEFAULT NULL,
  `related_articles` text,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title` (`title`,`contents`),
  FULLTEXT KEY `title_2` (`title`,`keywords`)
) ENGINE=MyISAM AUTO_INCREMENT=123 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_help_topics`
--

DROP TABLE IF EXISTS `foreup_help_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_help_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_holes`
--

DROP TABLE IF EXISTS `foreup_holes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_holes` (
  `hole_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_id` int(11) NOT NULL,
  `par` int(3) NOT NULL,
  `hole_number` int(3) NOT NULL,
  `front_latitude` decimal(10,6) NOT NULL,
  `front_longitude` decimal(10,6) NOT NULL,
  `center_latitude` decimal(10,6) NOT NULL,
  `center_longitude` decimal(10,6) NOT NULL,
  `back_latitude` decimal(10,6) NOT NULL,
  `back_longitude` decimal(10,6) NOT NULL,
  `hole_latitude` decimal(10,6) NOT NULL,
  `hole_longitude` decimal(10,6) NOT NULL,
  `middle_latitude` decimal(10,6) NOT NULL,
  `middle_longitude` decimal(10,6) NOT NULL,
  `tee_1_latitude` decimal(10,6) NOT NULL,
  `tee_1_longitude` decimal(10,6) NOT NULL,
  `tee_2_latitude` decimal(10,6) NOT NULL,
  `tee_2_longitude` decimal(10,6) NOT NULL,
  `tee_3_latitude` decimal(10,6) NOT NULL,
  `tee_3_longitude` decimal(10,6) NOT NULL,
  `tee_4_latitude` decimal(10,6) NOT NULL,
  `tee_4_longitude` decimal(10,6) NOT NULL,
  `tee_5_latitude` decimal(10,6) NOT NULL,
  `tee_5_longitude` decimal(10,6) NOT NULL,
  `tee_6_latitude` decimal(10,6) NOT NULL,
  `tee_6_longitude` decimal(10,6) NOT NULL,
  `tee_7_latitude` decimal(10,6) NOT NULL,
  `tee_7_longitude` decimal(10,6) NOT NULL,
  `tee_8_latitude` decimal(10,6) NOT NULL,
  `tee_8_longitude` decimal(10,6) NOT NULL,
  PRIMARY KEY (`hole_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_holidays`
--

DROP TABLE IF EXISTS `foreup_holidays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_holidays` (
  `course_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`course_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_home_articles`
--

DROP TABLE IF EXISTS `foreup_home_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_home_articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `column` tinyint(4) NOT NULL,
  `img` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_household_members`
--

DROP TABLE IF EXISTS `foreup_household_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_household_members` (
  `household_id` int(11) NOT NULL,
  `household_member_id` int(11) NOT NULL,
  UNIQUE KEY `household_member_id` (`household_member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_households`
--

DROP TABLE IF EXISTS `foreup_households`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_households` (
  `household_id` int(11) NOT NULL AUTO_INCREMENT,
  `household_head_id` int(11) NOT NULL,
  PRIMARY KEY (`household_id`),
  UNIQUE KEY `household_head_id` (`household_head_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_ibeacon`
--

DROP TABLE IF EXISTS `foreup_ibeacon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_ibeacon` (
  `beacon_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `major_id` mediumint(8) unsigned NOT NULL,
  `minor_id` mediumint(8) unsigned NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `terminal_id` int(10) unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `received` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`beacon_id`),
  KEY `course_id` (`course_id`,`terminal_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_images`
--

DROP TABLE IF EXISTS `foreup_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_images` (
  `image_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) unsigned NOT NULL,
  `module` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(5) unsigned NOT NULL,
  `height` int(5) unsigned NOT NULL,
  `filesize` decimal(7,2) unsigned NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `saved` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_inventory`
--

DROP TABLE IF EXISTS `foreup_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_inventory` (
  `CID` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_inventory` decimal(15,2) NOT NULL DEFAULT '0.00',
  `sale_id` int(11) NOT NULL,
  `suspended_sale_id` int(11) NOT NULL,
  PRIMARY KEY (`trans_id`),
  KEY `foreup_inventory_ibfk_1` (`trans_items`),
  KEY `foreup_inventory_ibfk_2` (`trans_user`),
  KEY `course_id` (`course_id`) USING BTREE,
  KEY `sale_id` (`sale_id`) USING BTREE,
  KEY `suspended_sale_id` (`suspended_sale_id`) USING BTREE,
  CONSTRAINT `foreup_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `foreup_items` (`item_id`),
  CONSTRAINT `foreup_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `foreup_employees` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8671 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_inventory_audit_items`
--

DROP TABLE IF EXISTS `foreup_inventory_audit_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_inventory_audit_items` (
  `inventory_audit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `current_count` smallint(6) NOT NULL,
  `manual_count` smallint(6) NOT NULL,
  KEY `inventory_audit_id` (`inventory_audit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_inventory_audits`
--

DROP TABLE IF EXISTS `foreup_inventory_audits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_inventory_audits` (
  `inventory_audit_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`inventory_audit_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_invoice_changes`
--

DROP TABLE IF EXISTS `foreup_invoice_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_invoice_changes` (
  `invoice_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `previous_paid` float(15,2) NOT NULL,
  `paid` float(15,2) NOT NULL,
  `previous_overdue` float(15,2) NOT NULL,
  `overdue` float(15,2) NOT NULL,
  `notes` varchar(255) NOT NULL,
  `previous_date` datetime DEFAULT NULL,
  `new_date` datetime DEFAULT NULL,
  KEY `invoice_id` (`invoice_id`,`course_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_invoice_charge_attempts`
--

DROP TABLE IF EXISTS `foreup_invoice_charge_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_invoice_charge_attempts` (
  `charge_attempt_id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_card_charge_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `date` datetime NOT NULL,
  `success` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`charge_attempt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_invoice_items`
--

DROP TABLE IF EXISTS `foreup_invoice_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_invoice_items` (
  `invoice_id` int(11) NOT NULL,
  `line_number` tinyint(4) NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` smallint(6) NOT NULL,
  `amount` float(15,2) NOT NULL,
  `paid_amount` float(15,2) NOT NULL,
  `paid_off` datetime NOT NULL,
  `tax` float(15,2) NOT NULL,
  `pay_account_balance` tinyint(4) NOT NULL,
  `pay_member_balance` tinyint(4) NOT NULL,
  UNIQUE KEY `invoice_id` (`invoice_id`,`line_number`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_invoices`
--

DROP TABLE IF EXISTS `foreup_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_invoices` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_number` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL DEFAULT 'Invoice',
  `category` varchar(255) NOT NULL DEFAULT 'Invoice',
  `subcategory` varchar(255) NOT NULL DEFAULT '',
  `day` tinyint(4) NOT NULL,
  `course_id` int(11) NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `billing_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bill_start` datetime NOT NULL,
  `bill_end` datetime NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `overdue_total` decimal(15,2) NOT NULL,
  `previous_payments` decimal(15,2) NOT NULL,
  `paid` decimal(15,2) NOT NULL,
  `overdue` decimal(15,2) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `credit_card_payment_id` int(11) NOT NULL,
  `email_invoice` tinyint(4) NOT NULL,
  `last_billing_attempt` date NOT NULL,
  `started` tinyint(4) NOT NULL,
  `charged_no_sale` tinyint(4) NOT NULL,
  `charged` tinyint(4) NOT NULL,
  `emailed` tinyint(4) NOT NULL,
  `send_date` date NOT NULL,
  `due_date` date NOT NULL,
  `auto_bill_date` date NOT NULL,
  `pay_customer_account` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pay_member_account` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show_account_transactions` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sale_id` int(10) unsigned NOT NULL,
  `notes` varchar(255) NOT NULL,
  `attempt_limit` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`invoice_id`),
  KEY `credit_card_payment_id` (`credit_card_payment_id`),
  KEY `course_id` (`course_id`),
  KEY `person_id` (`person_id`),
  KEY `billing_id` (`billing_id`),
  KEY `date` (`date`)
) ENGINE=InnoDB AUTO_INCREMENT=584 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_item_kit_items`
--

DROP TABLE IF EXISTS `foreup_item_kit_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_item_kit_items` (
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(15,2) NOT NULL,
  `price_class_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  KEY `foreup_item_kit_items_ibfk_2` (`item_id`),
  CONSTRAINT `foreup_item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`) ON DELETE CASCADE,
  CONSTRAINT `foreup_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_item_kits`
--

DROP TABLE IF EXISTS `foreup_item_kits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_item_kits` (
  `CID` int(11) NOT NULL,
  `item_kit_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `item_kit_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_price` decimal(15,2) DEFAULT NULL,
  `cost_price` decimal(15,2) DEFAULT NULL,
  `quickbooks_income` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `quickbooks_cogs` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `quickbooks_assets` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `is_punch_card` tinyint(4) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_kit_id`),
  UNIQUE KEY `course_id` (`course_id`,`item_kit_number`),
  KEY `name` (`name`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_item_kits_taxes`
--

DROP TABLE IF EXISTS `foreup_item_kits_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_item_kits_taxes` (
  `CID` int(11) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `course_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_kit_id`,`name`,`percent`),
  CONSTRAINT `foreup_item_kits_taxes_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_item_modifiers`
--

DROP TABLE IF EXISTS `foreup_item_modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_item_modifiers` (
  `item_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `override_price` decimal(10,2) DEFAULT NULL,
  `override_required` tinyint(4) NOT NULL,
  `default` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`item_id`,`modifier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_item_printer_groups`
--

DROP TABLE IF EXISTS `foreup_item_printer_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_item_printer_groups` (
  `item_id` int(10) unsigned NOT NULL,
  `printer_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`item_id`,`printer_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_item_sides`
--

DROP TABLE IF EXISTS `foreup_item_sides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_item_sides` (
  `item_id` int(11) NOT NULL,
  `side_id` int(11) NOT NULL,
  `not_available` tinyint(4) NOT NULL DEFAULT '0',
  `default` tinyint(4) NOT NULL,
  `upgrade_price` float(15,2) NOT NULL,
  UNIQUE KEY `item_side` (`item_id`,`side_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_items`
--

DROP TABLE IF EXISTS `foreup_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_items` (
  `course_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) unsigned NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `unit_price_includes_tax` tinyint(4) NOT NULL,
  `max_discount` decimal(15,2) NOT NULL DEFAULT '100.00',
  `quantity` decimal(15,2) NOT NULL DEFAULT '0.00',
  `is_unlimited` tinyint(1) NOT NULL,
  `reorder_level` int(15) NOT NULL DEFAULT '0',
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `gl_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `is_giftcard` tinyint(4) NOT NULL,
  `invisible` tinyint(4) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `food_and_beverage` tinyint(1) NOT NULL DEFAULT '0',
  `soup_or_salad` enum('none','soup','salad','either','both') COLLATE utf8_unicode_ci NOT NULL,
  `number_of_sides` tinyint(4) NOT NULL DEFAULT '0',
  `is_side` tinyint(4) NOT NULL,
  `add_on_price` float NOT NULL,
  `print_priority` tinyint(4) NOT NULL,
  `kitchen_printer` tinyint(4) NOT NULL DEFAULT '1',
  `quickbooks_income` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `quickbooks_cogs` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `quickbooks_assets` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `erange_size` tinyint(4) NOT NULL,
  `is_fee` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `meal_course_id` int(10) unsigned NOT NULL DEFAULT '0',
  `do_not_print` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `prompt_meal_course` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `do_not_print_customer_receipt` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `item_number2` (`course_id`,`item_number`),
  KEY `foreup_items_ibfk_1` (`supplier_id`),
  KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `deleted` (`deleted`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `foreup_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `foreup_suppliers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5352 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_items_taxes`
--

DROP TABLE IF EXISTS `foreup_items_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_items_taxes` (
  `course_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `item_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`,`name`,`percent`),
  CONSTRAINT `foreup_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_items_upcs`
--

DROP TABLE IF EXISTS `foreup_items_upcs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_items_upcs` (
  `upc_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `upc` varchar(255) NOT NULL,
  PRIMARY KEY (`upc_id`),
  UNIQUE KEY `course_upc` (`course_id`,`upc`),
  KEY `item_id` (`item_id`,`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_links`
--

DROP TABLE IF EXISTS `foreup_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_links` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tee_1_name` varchar(255) NOT NULL,
  `tee_2_name` varchar(255) NOT NULL,
  `tee_3_name` varchar(255) NOT NULL,
  `tee_4_name` varchar(255) NOT NULL,
  `tee_5_name` varchar(255) NOT NULL,
  `tee_6_name` varchar(255) NOT NULL,
  `tee_7_name` varchar(255) NOT NULL,
  `tee_8_name` varchar(255) NOT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_loyalty_rates`
--

DROP TABLE IF EXISTS `foreup_loyalty_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_loyalty_rates` (
  `loyalty_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` enum('all','department','category','subcategory','item','item_kit') NOT NULL,
  `value` varchar(255) NOT NULL,
  `points_per_dollar` double(15,2) NOT NULL,
  `dollars_per_point` double(15,2) NOT NULL,
  `tee_time_index` tinyint(4) NOT NULL,
  `price_category` tinyint(4) NOT NULL,
  PRIMARY KEY (`loyalty_rate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_loyalty_transactions`
--

DROP TABLE IF EXISTS `foreup_loyalty_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_loyalty_transactions` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `trans_customer` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) DEFAULT NULL,
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_description` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`trans_id`),
  KEY `foreup_loyalty_transactions_ibfk_1` (`trans_customer`),
  KEY `foreup_loyalty_transactions_ibfk_2` (`trans_user`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_marketing_campaigns`
--

DROP TABLE IF EXISTS `foreup_marketing_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_marketing_campaigns` (
  `campaign_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `template` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `content_2` text NOT NULL,
  `content_3` text NOT NULL,
  `content_4` text NOT NULL,
  `content_5` text NOT NULL,
  `content_6` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `images` text NOT NULL,
  `image_2` varchar(255) NOT NULL,
  `image_3` varchar(255) NOT NULL,
  `image_4` varchar(255) NOT NULL,
  `send_date` datetime NOT NULL,
  `send_date_cst` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `recipients` mediumtext,
  `recipient_count` smallint(6) NOT NULL,
  `person_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `d` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `template_path` varchar(1000) NOT NULL,
  `logo_path` varchar(1000) DEFAULT NULL,
  `header` varchar(1000) DEFAULT NULL,
  `queued` tinyint(4) NOT NULL,
  `is_sent` int(1) NOT NULL DEFAULT '0',
  `reported` tinyint(4) NOT NULL,
  `attempts` tinyint(4) NOT NULL,
  `run_time` date NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `promotion_id` varchar(100) NOT NULL DEFAULT '-1',
  `marketing_template_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `from_name` varchar(255) DEFAULT NULL,
  `from_email` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `rendered_html` text,
  `json_content` text,
  `logo` varchar(255) DEFAULT NULL,
  `logo_align` varchar(10) DEFAULT 'left',
  `version` int(11) DEFAULT '1',
  `remote_hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`campaign_id`),
  UNIQUE KEY `remote_hash` (`remote_hash`)
) ENGINE=InnoDB AUTO_INCREMENT=480 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_marketing_images`
--

DROP TABLE IF EXISTS `foreup_marketing_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_marketing_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) DEFAULT NULL,
  `path` varchar(500) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_marketing_templates`
--

DROP TABLE IF EXISTS `foreup_marketing_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_marketing_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `template` text NOT NULL,
  `name` varchar(2555) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `style` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_marketing_texting`
--

DROP TABLE IF EXISTS `foreup_marketing_texting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_marketing_texting` (
  `person_id` int(10) NOT NULL,
  `course_id` int(11) NOT NULL,
  `msisdn` varchar(255) DEFAULT NULL,
  `valid_phone` tinyint(4) NOT NULL,
  `texting_status` enum('new','invited_not_subscribed','subscribed','unsuscribed') NOT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`person_id`,`course_id`),
  KEY `foreup_marketing_texting_fk2` (`course_id`),
  KEY `foreup_marketing_texting_fk1` (`person_id`),
  CONSTRAINT `foreup_marketing_texting_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `foreup_people` (`person_id`),
  CONSTRAINT `foreup_marketing_texting_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `foreup_courses` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_meal_courses`
--

DROP TABLE IF EXISTS `foreup_meal_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_meal_courses` (
  `meal_course_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` smallint(6) NOT NULL DEFAULT '0',
  `course_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`meal_course_id`),
  KEY `order` (`order`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_member_account_transactions`
--

DROP TABLE IF EXISTS `foreup_member_account_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_member_account_transactions` (
  `CID` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `invoice_id` int(10) unsigned NOT NULL,
  `trans_customer` int(11) NOT NULL DEFAULT '0',
  `trans_household` int(11) NOT NULL,
  `trans_user` int(11) DEFAULT NULL,
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_description` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`trans_id`),
  KEY `foreup_member_account_transactions_ibfk_1` (`trans_customer`),
  KEY `foreup_member_account_transactions_ibfk_2` (`trans_user`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_menu_items`
--

DROP TABLE IF EXISTS `foreup_menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_menu_items` (
  `menu_id` varchar(100) DEFAULT NULL,
  `item_id` varchar(100) DEFAULT NULL,
  UNIQUE KEY `menu_item` (`menu_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_menus`
--

DROP TABLE IF EXISTS `foreup_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_menus` (
  `menu_id` int(255) NOT NULL AUTO_INCREMENT,
  `start_time` int(100) DEFAULT NULL,
  `end_time` int(100) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `course_id` varchar(100) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_minimum_charges`
--

DROP TABLE IF EXISTS `foreup_minimum_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_minimum_charges` (
  `course_id` int(10) unsigned NOT NULL,
  `minimum_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `amount` decimal(15,2) unsigned NOT NULL,
  `frequency` enum('yearly','quarterly','monthly') NOT NULL,
  `charge_day` int(10) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`minimum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_modifiers`
--

DROP TABLE IF EXISTS `foreup_modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_modifiers` (
  `modifier_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` tinyint(4) NOT NULL DEFAULT '1',
  `required` tinyint(4) NOT NULL,
  `default_price` decimal(10,2) NOT NULL,
  `options` text COLLATE utf8_unicode_ci,
  `date_created` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `multi_select` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`modifier_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_modules`
--

DROP TABLE IF EXISTS `foreup_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_modules` (
  `name_lang_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc_lang_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  UNIQUE KEY `name_lang_key` (`name_lang_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_notes`
--

DROP TABLE IF EXISTS `foreup_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_notes` (
  `course_id` int(11) NOT NULL,
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) NOT NULL,
  `recipient` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `message` text NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_online_hours`
--

DROP TABLE IF EXISTS `foreup_online_hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_online_hours` (
  `teesheet_id` int(10) unsigned NOT NULL,
  `sun_open` smallint(5) unsigned DEFAULT '0',
  `sun_close` smallint(5) unsigned DEFAULT '2359',
  `mon_open` smallint(5) unsigned DEFAULT '0',
  `mon_close` smallint(5) unsigned DEFAULT '2359',
  `tue_open` smallint(5) unsigned DEFAULT '0',
  `tue_close` smallint(5) unsigned DEFAULT '2359',
  `wed_open` smallint(5) unsigned DEFAULT '0',
  `wed_close` smallint(5) unsigned DEFAULT '2359',
  `thu_open` smallint(5) unsigned DEFAULT '0',
  `thu_close` smallint(5) unsigned DEFAULT '2359',
  `fri_open` smallint(5) unsigned DEFAULT '0',
  `fri_close` smallint(5) unsigned DEFAULT '2359',
  `sat_open` smallint(5) unsigned DEFAULT '0',
  `sat_close` smallint(5) unsigned DEFAULT '2359',
  PRIMARY KEY (`teesheet_id`),
  UNIQUE KEY `tue_open` (`tue_open`,`tue_close`),
  KEY `wed_open` (`wed_open`,`wed_close`),
  KEY `thu_open` (`thu_open`,`thu_close`),
  KEY `fri_open` (`fri_open`,`fri_close`),
  KEY `sat_open` (`sat_open`,`sat_close`),
  KEY `sun_open` (`sun_open`,`sun_close`),
  KEY `mon_open` (`mon_open`,`mon_close`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_people`
--

DROP TABLE IF EXISTS `foreup_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_people` (
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `address_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(10) NOT NULL AUTO_INCREMENT,
  `foreup_news_announcements_unsubscribe` tinyint(1) NOT NULL,
  PRIMARY KEY (`person_id`),
  KEY `email` (`email`),
  KEY `person_orderby` (`last_name`,`first_name`,`person_id`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1000001 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_permissions`
--

DROP TABLE IF EXISTS `foreup_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_permissions` (
  `CID` int(11) NOT NULL,
  `module_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(10) NOT NULL,
  PRIMARY KEY (`module_id`,`person_id`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `foreup_permissions_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `foreup_employees` (`person_id`),
  CONSTRAINT `foreup_permissions_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `foreup_modules` (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_photo_flags`
--

DROP TABLE IF EXISTS `foreup_photo_flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_photo_flags` (
  `photo_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`photo_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_photo_likes`
--

DROP TABLE IF EXISTS `foreup_photo_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_photo_likes` (
  `photo_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `like` tinyint(4) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_photos`
--

DROP TABLE IF EXISTS `foreup_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_photos` (
  `photo_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `details` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`photo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_pos_cart`
--

DROP TABLE IF EXISTS `foreup_pos_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_pos_cart` (
  `cart_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `mode` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sale',
  `suspended_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teetime_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_activity` datetime NOT NULL,
  `taxable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `override_authorization_id` int(10) unsigned DEFAULT NULL,
  `return_sale_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=581 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_pos_cart_customers`
--

DROP TABLE IF EXISTS `foreup_pos_cart_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_pos_cart_customers` (
  `cart_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `selected` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cart_id`,`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_pos_cart_item_modifiers`
--

DROP TABLE IF EXISTS `foreup_pos_cart_item_modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_pos_cart_item_modifiers` (
  `cart_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cart_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_pos_cart_item_side_modifiers`
--

DROP TABLE IF EXISTS `foreup_pos_cart_item_side_modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_pos_cart_item_side_modifiers` (
  `cart_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `side_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `side_position` smallint(5) unsigned NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cart_id`,`line`,`side_type`,`side_position`,`modifier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_pos_cart_item_sides`
--

DROP TABLE IF EXISTS `foreup_pos_cart_item_sides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_pos_cart_item_sides` (
  `cart_id` int(10) unsigned NOT NULL,
  `cart_line` smallint(5) unsigned NOT NULL,
  `position` smallint(5) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`cart_id`,`cart_line`,`position`,`type`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_pos_cart_items`
--

DROP TABLE IF EXISTS `foreup_pos_cart_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_pos_cart_items` (
  `cart_id` int(10) unsigned NOT NULL,
  `line` mediumint(8) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `quantity` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(5,2) NOT NULL,
  `item_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `item_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_class_id` int(10) unsigned DEFAULT NULL,
  `timeframe_id` int(10) unsigned DEFAULT NULL,
  `special_id` int(10) unsigned DEFAULT NULL,
  `params` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `punch_card_id` int(10) unsigned DEFAULT NULL,
  `tournament_id` int(10) unsigned NOT NULL,
  `invoice_id` int(10) unsigned DEFAULT NULL,
  `selected` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `unit_price_includes_tax` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cart_id`,`line`),
  KEY `line` (`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_pos_cart_payments`
--

DROP TABLE IF EXISTS `foreup_pos_cart_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_pos_cart_payments` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(10) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `type` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `params` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  UNIQUE KEY `payment_type` (`type`,`record_id`,`cart_id`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_potential_teetimes`
--

DROP TABLE IF EXISTS `foreup_potential_teetimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_potential_teetimes` (
  `potential_teetime` bigint(20) NOT NULL DEFAULT '0',
  `teesheet_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`potential_teetime`),
  UNIQUE KEY `teesheet_id_time` (`teesheet_id`,`potential_teetime`),
  KEY `teesheet_id` (`teesheet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_price_classes`
--

DROP TABLE IF EXISTS `foreup_price_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_price_classes` (
  `class_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cart` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `color` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`class_id`),
  UNIQUE KEY `course_id` (`course_id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_price_colors`
--

DROP TABLE IF EXISTS `foreup_price_colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_price_colors` (
  `course_id` int(10) unsigned NOT NULL,
  `price_category` smallint(5) unsigned NOT NULL,
  `color` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`course_id`,`price_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_prices`
--

DROP TABLE IF EXISTS `foreup_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_prices` (
  `teesheet_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `default` tinyint(4) NOT NULL,
  `price_category` varchar(255) NOT NULL,
  `price_1` decimal(15,2) NOT NULL,
  `price_2` decimal(15,2) NOT NULL,
  `price_3` decimal(15,2) NOT NULL,
  `price_4` decimal(15,2) NOT NULL,
  `is_holiday` tinyint(4) NOT NULL,
  `season_start` date NOT NULL,
  `season_end` date NOT NULL,
  `time_start` smallint(6) NOT NULL,
  `time_end` smallint(6) NOT NULL,
  `Mon` tinyint(4) NOT NULL,
  `Tue` tinyint(4) NOT NULL,
  `Wed` tinyint(4) NOT NULL,
  `Thu` tinyint(4) NOT NULL,
  `Fri` tinyint(4) NOT NULL,
  `Sat` tinyint(4) NOT NULL,
  `Sun` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_printer_groups`
--

DROP TABLE IF EXISTS `foreup_printer_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_printer_groups` (
  `printer_group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_printer_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`printer_group_id`),
  KEY `course_id` (`course_id`),
  KEY `default_printer_id` (`default_printer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_printers`
--

DROP TABLE IF EXISTS `foreup_printers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_printers` (
  `printer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`printer_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_promotion_definitions`
--

DROP TABLE IF EXISTS `foreup_promotion_definitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_promotion_definitions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `number_used` int(255) DEFAULT '0',
  `number_issued` int(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `online_code` varchar(255) DEFAULT NULL,
  `amount_type` varchar(100) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `valid_for` varchar(255) DEFAULT NULL,
  `Sunday` tinyint(1) DEFAULT '1',
  `Monday` tinyint(1) DEFAULT '1',
  `Tuesday` tinyint(1) NOT NULL DEFAULT '1',
  `Wednesday` tinyint(1) NOT NULL DEFAULT '1',
  `Thursday` tinyint(1) NOT NULL DEFAULT '1',
  `Friday` tinyint(1) NOT NULL DEFAULT '1',
  `Saturday` tinyint(1) NOT NULL DEFAULT '1',
  `valid_between_from` varchar(255) DEFAULT NULL,
  `valid_between_to` varchar(255) DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  `buy_quantity` int(11) DEFAULT NULL,
  `get_quantity` int(11) DEFAULT NULL,
  `min_purchase` double DEFAULT NULL,
  `additional_details` text,
  `rules` text,
  `text_text` text,
  `email_text` text,
  `expiration_date` date DEFAULT '3000-01-01',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `bogo` enum('bogo','discount') NOT NULL,
  `coupon_limit` enum('one','unlimited') NOT NULL,
  `item_id` int(255) NOT NULL DEFAULT '0',
  `discount_type` tinyint(10) NOT NULL DEFAULT '0',
  `category_type` varchar(100) NOT NULL DEFAULT '',
  `subcategory_type` varchar(100) NOT NULL DEFAULT '',
  `department_type` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_promotions`
--

DROP TABLE IF EXISTS `foreup_promotions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_promotions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_definition_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `expiration_date` date DEFAULT NULL,
  `redeemed` char(1) DEFAULT 'N',
  `deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_punch_card_items`
--

DROP TABLE IF EXISTS `foreup_punch_card_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_punch_card_items` (
  `punch_card_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `punches` smallint(6) NOT NULL,
  `used` smallint(6) NOT NULL,
  `price_class_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`punch_card_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_punch_cards`
--

DROP TABLE IF EXISTS `foreup_punch_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_punch_cards` (
  `punch_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `punch_card_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiration_date` date DEFAULT NULL,
  `issued_date` date NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`punch_card_id`),
  UNIQUE KEY `course_id` (`course_id`,`punch_card_number`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_queue_marketing`
--

DROP TABLE IF EXISTS `foreup_queue_marketing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_queue_marketing` (
  `queue_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `send_date` date NOT NULL,
  `run_date` date NOT NULL,
  `attempts` tinyint(4) NOT NULL,
  `completed` tinyint(4) NOT NULL,
  PRIMARY KEY (`queue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_queue_reports`
--

DROP TABLE IF EXISTS `foreup_queue_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_queue_reports` (
  `queue_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `run_date` date NOT NULL,
  `attempts` tinyint(4) NOT NULL,
  `completed` tinyint(4) NOT NULL,
  PRIMARY KEY (`queue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_quickbooks_accounts`
--

DROP TABLE IF EXISTS `foreup_quickbooks_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_quickbooks_accounts` (
  `course_id` int(10) unsigned NOT NULL,
  `quickbooks_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `parent_quickbooks_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_number` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sub_level` tinyint(3) unsigned NOT NULL,
  `account_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `balance` decimal(10,2) unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `map_to` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`course_id`,`quickbooks_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_quickbooks_sync`
--

DROP TABLE IF EXISTS `foreup_quickbooks_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_quickbooks_sync` (
  `course_id` int(10) unsigned NOT NULL,
  `record_id` int(10) unsigned NOT NULL,
  `record_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `quickbooks_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_synced` datetime DEFAULT NULL,
  PRIMARY KEY (`course_id`,`record_id`,`record_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_quickbutton_items`
--

DROP TABLE IF EXISTS `foreup_quickbutton_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_quickbutton_items` (
  `quickbutton_id` int(11) NOT NULL,
  `item_id` varchar(20) DEFAULT NULL,
  `item_kit_id` varchar(20) DEFAULT NULL,
  `order` tinyint(4) NOT NULL,
  UNIQUE KEY `item_kit_id` (`quickbutton_id`,`item_kit_id`),
  UNIQUE KEY `item_id` (`quickbutton_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_quickbuttons`
--

DROP TABLE IF EXISTS `foreup_quickbuttons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_quickbuttons` (
  `quickbutton_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `tab` tinyint(4) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL,
  `color` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`quickbutton_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_rainchecks`
--

DROP TABLE IF EXISTS `foreup_rainchecks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_rainchecks` (
  `raincheck_id` int(11) NOT NULL AUTO_INCREMENT,
  `raincheck_number` int(11) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `teetime_id` varchar(40) NOT NULL,
  `teetime_date` date NOT NULL DEFAULT '0000-00-00',
  `teetime_time` smallint(5) unsigned NOT NULL DEFAULT '0',
  `date_issued` datetime NOT NULL,
  `date_redeemed` datetime NOT NULL,
  `players` tinyint(4) NOT NULL,
  `holes_completed` tinyint(4) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `green_fee` float(15,2) NOT NULL,
  `cart_fee` float(15,2) NOT NULL,
  `tax` float(15,2) NOT NULL,
  `total` float(15,2) NOT NULL,
  `green_fee_price_category` varchar(255) NOT NULL,
  `cart_price_category` varchar(255) NOT NULL,
  `expiry_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`raincheck_id`),
  UNIQUE KEY `raincheck_number_teesheet` (`raincheck_number`,`teesheet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_receivings`
--

DROP TABLE IF EXISTS `foreup_receivings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_receivings` (
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `receiving_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_cost` decimal(15,2) NOT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`receiving_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `employee_id` (`employee_id`),
  KEY `deleted` (`deleted`),
  CONSTRAINT `foreup_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `foreup_employees` (`person_id`),
  CONSTRAINT `foreup_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `foreup_suppliers` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_receivings_items`
--

DROP TABLE IF EXISTS `foreup_receivings_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_receivings_items` (
  `CID` int(11) NOT NULL,
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` int(10) NOT NULL DEFAULT '0',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `foreup_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`),
  CONSTRAINT `foreup_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `foreup_receivings` (`receiving_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_register_log`
--

DROP TABLE IF EXISTS `foreup_register_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_register_log` (
  `register_log_id` int(10) NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) NOT NULL,
  `course_id` smallint(6) NOT NULL,
  `terminal_id` int(11) NOT NULL,
  `shift_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shift_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `open_amount` double(15,2) NOT NULL,
  `close_amount` double(15,2) NOT NULL,
  `close_check_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `cash_sales_amount` double(15,2) NOT NULL,
  `check_sales_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `persist` tinyint(4) NOT NULL,
  `closing_employee_id` int(11) NOT NULL,
  PRIMARY KEY (`register_log_id`),
  KEY `phppos_register_log_ibfk_1` (`employee_id`),
  CONSTRAINT `foreup_register_log_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `foreup_employees` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=556 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_register_log_counts`
--

DROP TABLE IF EXISTS `foreup_register_log_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_register_log_counts` (
  `register_log_id` int(11) NOT NULL,
  `pennies` smallint(6) NOT NULL,
  `nickels` smallint(6) NOT NULL,
  `dimes` smallint(6) NOT NULL,
  `quarters` smallint(6) NOT NULL,
  `ones` smallint(6) NOT NULL,
  `fives` smallint(6) NOT NULL,
  `tens` smallint(6) NOT NULL,
  `twenties` smallint(6) NOT NULL,
  `fifties` smallint(6) NOT NULL,
  `hundreds` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_report_base_Tables`
--

DROP TABLE IF EXISTS `foreup_report_base_Tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_report_base_Tables` (
  `base_table` varchar(255) NOT NULL,
  PRIMARY KEY (`base_table`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_report_columns`
--

DROP TABLE IF EXISTS `foreup_report_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_report_columns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_type` varchar(255) NOT NULL DEFAULT 'sales',
  `column` varchar(512) DEFAULT NULL,
  `table` varchar(512) DEFAULT NULL,
  `label` varchar(512) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `type` enum('number','text','dropdown','date_range') NOT NULL,
  `possible_values` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_foreup_report_columns_foreup_report_base_Tables` (`report_type`),
  CONSTRAINT `FK_foreup_report_columns_foreup_report_base_Tables` FOREIGN KEY (`report_type`) REFERENCES `foreup_report_base_Tables` (`base_table`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_report_linkouts`
--

DROP TABLE IF EXISTS `foreup_report_linkouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_report_linkouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) DEFAULT NULL,
  `linked_report_id` int(11) DEFAULT NULL,
  `label` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_foreup_report_linkouts_foreup_reports` (`report_id`),
  KEY `FK_foreup_report_linkouts_foreup_reports_2` (`linked_report_id`),
  CONSTRAINT `FK_foreup_report_linkouts_foreup_reports` FOREIGN KEY (`report_id`) REFERENCES `foreup_reports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_foreup_report_linkouts_foreup_reports_2` FOREIGN KEY (`linked_report_id`) REFERENCES `foreup_reports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_report_linkouts_columns`
--

DROP TABLE IF EXISTS `foreup_report_linkouts_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_report_linkouts_columns` (
  `linkout_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  KEY `FK_foreup_report_linkouts_columns_foreup_report_linkouts` (`linkout_id`),
  KEY `FK_foreup_report_linkouts_columns_foreup_reports_columns` (`column_id`),
  CONSTRAINT `FK_foreup_report_linkouts_columns_foreup_reports_columns` FOREIGN KEY (`column_id`) REFERENCES `foreup_reports_columns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_foreup_report_linkouts_columns_foreup_report_linkouts` FOREIGN KEY (`linkout_id`) REFERENCES `foreup_report_linkouts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_report_tables`
--

DROP TABLE IF EXISTS `foreup_report_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_report_tables` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('pending','open','deleted') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3222 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_report_tags`
--

DROP TABLE IF EXISTS `foreup_report_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_report_tags` (
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_reports`
--

DROP TABLE IF EXISTS `foreup_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base` varchar(255) NOT NULL DEFAULT 'sales',
  `course_id` int(11) NOT NULL DEFAULT '-1',
  `type` enum('report','template') NOT NULL,
  `ordering` varchar(1024) DEFAULT NULL,
  `grouping` varchar(1024) DEFAULT NULL,
  `date_range` varchar(1024) DEFAULT NULL,
  `filters` text,
  `description` varchar(1024) NOT NULL,
  `title` varchar(255) NOT NULL,
  `data` text,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `tags` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_foreup_reports_foreup_report_base_Tables` (`base`),
  CONSTRAINT `FK_foreup_reports_foreup_report_base_Tables` FOREIGN KEY (`base`) REFERENCES `foreup_report_base_Tables` (`base_table`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_reports_47350225ad1db38c86b2eae67e9cfe60`
--

DROP TABLE IF EXISTS `foreup_reports_47350225ad1db38c86b2eae67e9cfe60`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_reports_47350225ad1db38c86b2eae67e9cfe60` (
  `deleted` int(11) NOT NULL DEFAULT '0',
  `sale_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `serialnumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `teetime_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `terminal_id` int(11) NOT NULL DEFAULT '0',
  `sale_id` int(11) NOT NULL DEFAULT '0',
  `number` int(11) unsigned DEFAULT NULL,
  `line` int(11) NOT NULL DEFAULT '0',
  `timeframe_id` varchar(10) DEFAULT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_kit_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_unit_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actual_category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(511) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory` varchar(298) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gl_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total` decimal(15,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `profit` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total_cost` decimal(15,2) NOT NULL DEFAULT '0.00',
  `receipt_only` tinyint(4) NOT NULL DEFAULT '0',
  KEY `sale_id` (`sale_id`),
  KEY `number` (`number`),
  KEY `deleted` (`deleted`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `teetime_id` (`teetime_id`),
  KEY `sale_date` (`sale_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_reports_49f13f9ccf80de43c57785e682865067`
--

DROP TABLE IF EXISTS `foreup_reports_49f13f9ccf80de43c57785e682865067`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_reports_49f13f9ccf80de43c57785e682865067` (
  `deleted` int(11) NOT NULL DEFAULT '0',
  `sale_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `serialnumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `teetime_id` varchar(21) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `terminal_id` int(11) NOT NULL DEFAULT '0',
  `sale_id` int(11) NOT NULL DEFAULT '0',
  `number` int(11) unsigned DEFAULT NULL,
  `line` int(11) NOT NULL DEFAULT '0',
  `timeframe_id` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) DEFAULT NULL,
  `item_kit_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_unit_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actual_category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(511) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory` varchar(298) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gl_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total` decimal(15,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `profit` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total_cost` decimal(15,2) NOT NULL DEFAULT '0.00',
  `receipt_only` tinyint(4) NOT NULL DEFAULT '0',
  KEY `sale_id` (`sale_id`),
  KEY `number` (`number`),
  KEY `deleted` (`deleted`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `sale_date` (`sale_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_reports_8eb71abbc8549848ffe6ca74937e07c5`
--

DROP TABLE IF EXISTS `foreup_reports_8eb71abbc8549848ffe6ca74937e07c5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_reports_8eb71abbc8549848ffe6ca74937e07c5` (
  `deleted` int(11) NOT NULL DEFAULT '0',
  `sale_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `serialnumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `teetime_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `terminal_id` int(11) NOT NULL DEFAULT '0',
  `sale_id` int(11) NOT NULL DEFAULT '0',
  `number` int(11) unsigned DEFAULT NULL,
  `line` int(11) NOT NULL DEFAULT '0',
  `timeframe_id` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) DEFAULT NULL,
  `item_kit_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_unit_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actual_category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(511) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory` varchar(298) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gl_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total` decimal(15,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `profit` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total_cost` decimal(15,2) NOT NULL DEFAULT '0.00',
  `receipt_only` tinyint(4) NOT NULL DEFAULT '0',
  KEY `sale_id` (`sale_id`),
  KEY `number` (`number`),
  KEY `deleted` (`deleted`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `sale_date` (`sale_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_reports_columns`
--

DROP TABLE IF EXISTS `foreup_reports_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_reports_columns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `column_id` int(11) DEFAULT NULL,
  `custom_column` varchar(255) NOT NULL,
  `aggregate` enum('sum','count','max','day','month','year') DEFAULT NULL,
  `filterable` enum('Y','N') NOT NULL DEFAULT 'Y',
  `visible` enum('Y','N') NOT NULL DEFAULT 'Y',
  `selectable` enum('Y','N') NOT NULL DEFAULT 'Y',
  `sortable` enum('Y','N') NOT NULL DEFAULT 'Y',
  `label` varchar(255) DEFAULT NULL,
  `locked` enum('Y','N') NOT NULL DEFAULT 'Y',
  `type` enum('number','text','date_range','teetime') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_foreup_reports_columns_foreup_reports` (`report_id`),
  KEY `FK_foreup_reports_columns_foreup_report_columns` (`column_id`),
  CONSTRAINT `FK_foreup_reports_columns_foreup_reports` FOREIGN KEY (`report_id`) REFERENCES `foreup_reports` (`id`),
  CONSTRAINT `FK_foreup_reports_columns_foreup_report_columns` FOREIGN KEY (`column_id`) REFERENCES `foreup_report_columns` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_reports_summary`
--

DROP TABLE IF EXISTS `foreup_reports_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_reports_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `column_id` int(11) DEFAULT NULL,
  `custom_column` varchar(255) NOT NULL,
  `aggregate` enum('sum','count','max','day','month','year') DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_foreup_reports_columns_foreup_reports` (`report_id`),
  KEY `FK_foreup_reports_columns_foreup_report_columns` (`column_id`),
  CONSTRAINT `foreup_reports_summary_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `foreup_reports` (`id`),
  CONSTRAINT `foreup_reports_summary_ibfk_2` FOREIGN KEY (`column_id`) REFERENCES `foreup_report_columns` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_reservations`
--

DROP TABLE IF EXISTS `foreup_reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_reservations` (
  `reservation_id` varchar(21) NOT NULL,
  `track_id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `start` bigint(20) NOT NULL,
  `end` bigint(20) NOT NULL,
  `allDay` varchar(5) NOT NULL,
  `player_count` smallint(6) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `carts` float NOT NULL,
  `paid_player_count` tinyint(4) NOT NULL DEFAULT '0',
  `paid_carts` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `details` text NOT NULL,
  `side` varchar(10) NOT NULL,
  `className` varchar(40) NOT NULL,
  `person_id` int(10) NOT NULL,
  `person_name` varchar(255) NOT NULL,
  `person_id_2` int(11) NOT NULL,
  `person_name_2` varchar(255) NOT NULL,
  `person_id_3` int(11) NOT NULL,
  `person_name_3` varchar(255) NOT NULL,
  `person_id_4` int(11) NOT NULL,
  `person_name_4` varchar(255) NOT NULL,
  `person_id_5` int(11) NOT NULL,
  `person_name_5` varchar(255) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_booked` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_cancelled` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `send_confirmation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `confirmation_emailed` tinyint(4) NOT NULL,
  `booking_source` varchar(255) NOT NULL,
  `booker_id` varchar(255) NOT NULL,
  `canceller_id` int(11) NOT NULL,
  `teed_off_time` datetime NOT NULL,
  `turn_time` datetime NOT NULL,
  `finish_time` datetime NOT NULL,
  PRIMARY KEY (`reservation_id`),
  KEY `track_id` (`track_id`),
  KEY `start` (`start`),
  KEY `end` (`end`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_restaurant_reservations`
--

DROP TABLE IF EXISTS `foreup_restaurant_reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_restaurant_reservations` (
  `reservation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guest_count` smallint(5) unsigned DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `date_created` datetime NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `comments` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checked_in` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`reservation_id`),
  KEY `course_id` (`course_id`),
  KEY `employee_id` (`employee_id`),
  KEY `customer_id` (`customer_id`),
  KEY `reservation_time` (`date`,`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_reviews`
--

DROP TABLE IF EXISTS `foreup_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `value_1` tinyint(4) NOT NULL,
  `value_2` tinyint(4) NOT NULL,
  `value_3` tinyint(4) NOT NULL,
  `description` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales`
--

DROP TABLE IF EXISTS `foreup_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales` (
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `sale_number` int(11) NOT NULL,
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `teetime_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `override_authorization_id` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `table_id` int(10) unsigned NOT NULL,
  `terminal_id` int(11) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auto_gratuity` decimal(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `deleted_by` int(11) NOT NULL,
  `deleted_at` datetime NOT NULL,
  `mobile_guid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` int(10) unsigned DEFAULT NULL,
  `guest_count` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `mobile_guid` (`mobile_guid`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `deleted` (`deleted`),
  KEY `course_id` (`course_id`),
  KEY `course_id_sale_time` (`course_id`,`sale_time`),
  KEY `course_sale` (`course_id`,`sale_time`),
  CONSTRAINT `foreup_sales_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `foreup_customers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5192 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_comps`
--

DROP TABLE IF EXISTS `foreup_sales_comps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_comps` (
  `sale_id` int(10) unsigned NOT NULL,
  `line` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `dollar_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `course_id` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sale_id`,`line`,`item_id`),
  KEY `course_id` (`course_id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_invoices`
--

DROP TABLE IF EXISTS `foreup_sales_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_invoices` (
  `sale_id` int(10) NOT NULL,
  `invoice_id` int(10) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `teesheet` varchar(255) NOT NULL,
  `price_category` tinyint(4) NOT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL,
  `invoice_cost_price` decimal(15,2) NOT NULL,
  `invoice_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL,
  `tax` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `profit` decimal(15,2) NOT NULL,
  `total_cost` decimal(15,2) NOT NULL,
  `receipt_only` tinyint(4) NOT NULL,
  UNIQUE KEY `sale_invoice_line` (`sale_id`,`invoice_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_invoices_taxes`
--

DROP TABLE IF EXISTS `foreup_sales_invoices_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_invoices_taxes` (
  `sale_id` int(10) NOT NULL,
  `invoice_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`invoice_id`,`line`,`name`,`percent`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_item_kits`
--

DROP TABLE IF EXISTS `foreup_sales_item_kits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_item_kits` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_kit_id` int(10) NOT NULL DEFAULT '0',
  `invoice_id` int(10) unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teesheet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_category` tinyint(4) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_kit_cost_price` decimal(15,2) NOT NULL,
  `item_kit_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL,
  `tax` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `profit` decimal(15,2) NOT NULL,
  `total_cost` decimal(15,2) NOT NULL,
  `receipt_only` tinyint(4) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`),
  KEY `item_kit_id` (`item_kit_id`),
  CONSTRAINT `foreup_sales_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`),
  CONSTRAINT `foreup_sales_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_item_kits_taxes`
--

DROP TABLE IF EXISTS `foreup_sales_item_kits_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_item_kits_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_kit_id`),
  CONSTRAINT `foreup_sales_item_kits_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_item_kits` (`sale_id`),
  CONSTRAINT `foreup_sales_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_items`
--

DROP TABLE IF EXISTS `foreup_sales_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teesheet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_category` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timeframe_id` int(10) unsigned DEFAULT '0',
  `special_id` int(10) unsigned DEFAULT '0',
  `price_class_id` int(10) unsigned DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `num_splits` smallint(5) unsigned NOT NULL DEFAULT '1',
  `is_side` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `unit_price_includes_tax` tinyint(4) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL,
  `tax` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `profit` decimal(15,2) NOT NULL,
  `total_cost` decimal(15,2) NOT NULL,
  `receipt_only` tinyint(4) NOT NULL,
  `erange_code` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  KEY `sale_id` (`sale_id`),
  CONSTRAINT `foreup_sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_items_modifiers`
--

DROP TABLE IF EXISTS `foreup_sales_items_modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_items_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `selected_option` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selected_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_items_taxes`
--

DROP TABLE IF EXISTS `foreup_sales_items_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `foreup_sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_items` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_items_temp`
--

DROP TABLE IF EXISTS `foreup_sales_items_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_items_temp` (
  `deleted` int(11) NOT NULL DEFAULT '0',
  `sale_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `serialnumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `teetime_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `terminal_id` int(11) NOT NULL DEFAULT '0',
  `sale_id` int(11) NOT NULL DEFAULT '0',
  `number` int(11) unsigned DEFAULT NULL,
  `line` int(11) NOT NULL DEFAULT '0',
  `timeframe_id` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) DEFAULT NULL,
  `item_kit_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_unit_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actual_category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(511) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory` varchar(298) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gl_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total` decimal(15,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `profit` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total_cost` decimal(15,2) NOT NULL DEFAULT '0.00',
  `receipt_only` tinyint(4) NOT NULL DEFAULT '0',
  KEY `sale_id` (`sale_id`),
  KEY `number` (`number`),
  KEY `deleted` (`deleted`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `sale_date` (`sale_date`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_items_temp_test`
--

DROP TABLE IF EXISTS `foreup_sales_items_temp_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_items_temp_test` (
  `deleted` int(11) NOT NULL DEFAULT '0',
  `sale_date` date DEFAULT NULL,
  `sale_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(10) DEFAULT NULL,
  `item_kit_id` int(11) DEFAULT NULL,
  `supplier_id` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_unit_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subcategory` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  `subtotal` decimal(45,8) DEFAULT NULL,
  `line` int(11) NOT NULL DEFAULT '0',
  `serialnumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` decimal(53,2) DEFAULT NULL,
  `tax` decimal(65,15) DEFAULT NULL,
  `profit` decimal(46,8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_payments`
--

DROP TABLE IF EXISTS `foreup_sales_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_payments` (
  `sale_id` int(10) NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reports_only` tinyint(4) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `tip_recipient` int(11) NOT NULL,
  `record_id` int(10) NOT NULL,
  `number` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  KEY `sale_id` (`sale_id`),
  KEY `invoice_id` (`invoice_id`),
  CONSTRAINT `foreup_sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_payments_credit_cards`
--

DROP TABLE IF EXISTS `foreup_sales_payments_credit_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_payments_credit_cards` (
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `mercury_id` varchar(255) NOT NULL,
  `ets_id` varchar(255) NOT NULL,
  `tran_type` varchar(255) NOT NULL,
  `amount` float(15,2) NOT NULL,
  `auth_amount` float(15,2) NOT NULL,
  `card_type` varchar(255) NOT NULL,
  `masked_account` varchar(255) NOT NULL,
  `cardholder_name` varchar(255) NOT NULL,
  `ref_no` varchar(255) NOT NULL,
  `invoice` int(11) NOT NULL AUTO_INCREMENT,
  `operator_id` varchar(255) NOT NULL,
  `terminal_name` varchar(255) NOT NULL,
  `trans_post_time` datetime NOT NULL,
  `auth_code` varchar(255) NOT NULL,
  `voice_auth_code` varchar(255) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `acq_ref_data` varchar(255) NOT NULL,
  `process_data` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_used` tinyint(4) NOT NULL,
  `amount_refunded` float(15,2) NOT NULL,
  `frequency` varchar(9) NOT NULL,
  `response_code` smallint(6) NOT NULL,
  `status` varchar(30) NOT NULL,
  `status_message` varchar(255) NOT NULL,
  `display_message` varchar(255) NOT NULL,
  `avs_result` varchar(20) NOT NULL,
  `cvv_result` varchar(20) NOT NULL,
  `tax_amount` float(15,2) NOT NULL,
  `avs_address` varchar(255) NOT NULL,
  `avs_zip` varchar(20) NOT NULL,
  `payment_id_expired` varchar(10) NOT NULL,
  `customer_code` varchar(255) NOT NULL,
  `memo` varchar(255) NOT NULL,
  `batch_no` varchar(6) NOT NULL,
  `gratuity_amount` float(15,2) NOT NULL,
  `voided` int(1) NOT NULL,
  `mobile_guid` varchar(255) NOT NULL,
  PRIMARY KEY (`invoice`)
) ENGINE=InnoDB AUTO_INCREMENT=5025 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_suspended`
--

DROP TABLE IF EXISTS `foreup_sales_suspended`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_suspended` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `course_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `table_id` (`course_id`,`table_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `deleted` (`deleted`),
  CONSTRAINT `foreup_sales_suspended_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `foreup_employees` (`person_id`),
  CONSTRAINT `foreup_sales_suspended_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `foreup_customers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_suspended_item_kits`
--

DROP TABLE IF EXISTS `foreup_sales_suspended_item_kits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_suspended_item_kits` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_kit_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_kit_cost_price` decimal(15,2) NOT NULL,
  `item_kit_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`),
  KEY `item_kit_id` (`item_kit_id`),
  CONSTRAINT `foreup_sales_suspended_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`),
  CONSTRAINT `foreup_sales_suspended_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_suspended` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_suspended_item_kits_taxes`
--

DROP TABLE IF EXISTS `foreup_sales_suspended_item_kits_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_suspended_item_kits_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_kit_id`),
  CONSTRAINT `foreup_sales_suspended_item_kits_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_suspended_item_kits` (`sale_id`),
  CONSTRAINT `foreup_sales_suspended_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_suspended_items`
--

DROP TABLE IF EXISTS `foreup_sales_suspended_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_suspended_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_category` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `foreup_sales_suspended_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`),
  CONSTRAINT `foreup_sales_suspended_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_suspended` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_suspended_items_modifiers`
--

DROP TABLE IF EXISTS `foreup_sales_suspended_items_modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_suspended_items_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `selected_option` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selected_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_suspended_items_taxes`
--

DROP TABLE IF EXISTS `foreup_sales_suspended_items_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_suspended_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `foreup_sales_suspended_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_suspended_items` (`sale_id`),
  CONSTRAINT `foreup_sales_suspended_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_suspended_payments`
--

DROP TABLE IF EXISTS `foreup_sales_suspended_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_suspended_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  CONSTRAINT `foreup_sales_suspended_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_suspended` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sales_tournaments`
--

DROP TABLE IF EXISTS `foreup_sales_tournaments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sales_tournaments` (
  `sale_id` int(10) NOT NULL,
  `tournament_id` int(10) NOT NULL,
  `teesheet` varchar(255) NOT NULL,
  `price_category` tinyint(4) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL,
  `tournament_cost_price` decimal(15,2) NOT NULL,
  `tournament_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL,
  `taxes_paid` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_schedules`
--

DROP TABLE IF EXISTS `foreup_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_schedules` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `online_open_time` varchar(10) NOT NULL DEFAULT '0900',
  `online_close_time` varchar(10) NOT NULL DEFAULT '1800',
  `online_time_limit` smallint(6) NOT NULL DEFAULT '120',
  `increment` float(4,2) NOT NULL DEFAULT '8.00',
  `frontnine` smallint(6) NOT NULL DEFAULT '200',
  `days_out` tinyint(4) NOT NULL,
  `days_in_booking_window` tinyint(4) NOT NULL DEFAULT '7',
  `minimum_players` tinyint(4) NOT NULL DEFAULT '2',
  `limit_holes` tinyint(4) NOT NULL,
  `booking_carts` tinyint(4) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `default_price_class` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`schedule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_season_price_classes`
--

DROP TABLE IF EXISTS `foreup_season_price_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_season_price_classes` (
  `season_id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`season_id`,`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_seasonal_timeframes`
--

DROP TABLE IF EXISTS `foreup_seasonal_timeframes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_seasonal_timeframes` (
  `timeframe_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL,
  `season_id` int(10) unsigned NOT NULL,
  `timeframe_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `monday` tinyint(1) unsigned NOT NULL,
  `tuesday` tinyint(1) unsigned NOT NULL,
  `wednesday` tinyint(1) unsigned NOT NULL,
  `thursday` tinyint(1) unsigned NOT NULL,
  `friday` tinyint(1) unsigned NOT NULL,
  `saturday` tinyint(1) unsigned NOT NULL,
  `sunday` tinyint(1) unsigned NOT NULL,
  `price1` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price2` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price3` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price4` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price5` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price6` decimal(15,2) NOT NULL DEFAULT '0.00',
  `start_time` smallint(5) unsigned NOT NULL,
  `end_time` smallint(5) unsigned NOT NULL,
  `default` tinyint(1) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`timeframe_id`),
  KEY `class_id` (`class_id`),
  KEY `season_id` (`season_id`),
  KEY `start_time` (`start_time`,`end_time`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_seasons`
--

DROP TABLE IF EXISTS `foreup_seasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_seasons` (
  `season_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `teesheet_id` int(10) unsigned NOT NULL,
  `season_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `holiday` tinyint(1) unsigned NOT NULL,
  `increment` decimal(15,2) NOT NULL,
  `front_nine` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `online_booking` tinyint(1) unsigned NOT NULL,
  `online_open_time` smallint(5) unsigned NOT NULL,
  `online_close_time` smallint(5) unsigned NOT NULL,
  `default` tinyint(1) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`season_id`),
  KEY `start_date` (`start_date`,`end_date`),
  KEY `holiday` (`holiday`),
  KEY `course_id` (`course_id`),
  KEY `online_open_time` (`online_open_time`,`online_close_time`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sendhub_accounts`
--

DROP TABLE IF EXISTS `foreup_sendhub_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sendhub_accounts` (
  `phone_number` varchar(10) NOT NULL,
  `sendhub_id` varchar(255) NOT NULL,
  `text_reminder_unsubscribe` tinyint(1) NOT NULL,
  UNIQUE KEY `phone_number` (`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sendhub_message_contacts`
--

DROP TABLE IF EXISTS `foreup_sendhub_message_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sendhub_message_contacts` (
  `message_id` int(11) NOT NULL,
  `sendhub_id` int(11) NOT NULL,
  KEY `message_id` (`message_id`,`sendhub_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sendhub_messages`
--

DROP TABLE IF EXISTS `foreup_sendhub_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sendhub_messages` (
  `campaign_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `sent` datetime NOT NULL,
  `confirmed` tinyint(4) NOT NULL,
  KEY `campaign_id` (`campaign_id`,`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_session_history`
--

DROP TABLE IF EXISTS `foreup_session_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_session_history` (
  `session_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `url` text NOT NULL,
  `sess_data` mediumtext NOT NULL,
  `basket_data` text NOT NULL,
  PRIMARY KEY (`session_log_id`),
  KEY `course_id` (`course_id`,`date`),
  KEY `session_id` (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=517 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_sessions`
--

DROP TABLE IF EXISTS `foreup_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_special_times`
--

DROP TABLE IF EXISTS `foreup_special_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_special_times` (
  `special_id` int(11) NOT NULL,
  `time` int(4) NOT NULL,
  PRIMARY KEY (`special_id`,`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_specials`
--

DROP TABLE IF EXISTS `foreup_specials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_specials` (
  `special_id` int(11) NOT NULL AUTO_INCREMENT,
  `teesheet_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `price_1` decimal(15,2) NOT NULL,
  `price_2` decimal(15,2) NOT NULL,
  `price_3` decimal(15,2) NOT NULL,
  `price_4` decimal(15,2) NOT NULL,
  `price_5` decimal(15,2) NOT NULL,
  `price_6` decimal(15,2) NOT NULL,
  `monday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `tuesday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `wednesday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `thursday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `friday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `saturday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sunday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_recurring` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_aggregate` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`special_id`),
  KEY `teesheet_id` (`teesheet_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_subscription_actions`
--

DROP TABLE IF EXISTS `foreup_subscription_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_subscription_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `action_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_suppliers`
--

DROP TABLE IF EXISTS `foreup_suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_suppliers` (
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `account_number` (`course_id`,`account_number`),
  KEY `person_id` (`person_id`),
  KEY `deleted` (`deleted`),
  CONSTRAINT `foreup_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `foreup_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_buttons`
--

DROP TABLE IF EXISTS `foreup_table_buttons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_buttons` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sub_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`name`,`course_id`,`category`,`sub_category`),
  KEY `order` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_comps`
--

DROP TABLE IF EXISTS `foreup_table_comps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_comps` (
  `sale_id` int(10) unsigned NOT NULL,
  `line` smallint(5) unsigned NOT NULL DEFAULT '0',
  `item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `course_id` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sale_id`,`line`,`item_id`),
  KEY `course_id` (`course_id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_customers`
--

DROP TABLE IF EXISTS `foreup_table_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_customers` (
  `sale_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`sale_id`,`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_item_kits`
--

DROP TABLE IF EXISTS `foreup_table_item_kits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_item_kits` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_kit_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_kit_cost_price` decimal(15,2) NOT NULL,
  `item_kit_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`),
  KEY `item_kit_id` (`item_kit_id`),
  CONSTRAINT `foreup_table_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`),
  CONSTRAINT `foreup_table_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_tables` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_item_kits_taxes`
--

DROP TABLE IF EXISTS `foreup_table_item_kits_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_item_kits_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_kit_id`),
  CONSTRAINT `foreup_table_item_kits_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_table_item_kits` (`sale_id`),
  CONSTRAINT `foreup_table_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_item_side_modifiers`
--

DROP TABLE IF EXISTS `foreup_table_item_side_modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_item_side_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `side_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `side_position` smallint(5) unsigned NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sale_id`,`line`,`side_type`,`side_position`,`modifier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_item_sides`
--

DROP TABLE IF EXISTS `foreup_table_item_sides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_item_sides` (
  `sale_id` int(10) unsigned NOT NULL,
  `cart_line` smallint(5) unsigned NOT NULL,
  `position` smallint(5) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`sale_id`,`cart_line`,`position`,`type`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_items`
--

DROP TABLE IF EXISTS `foreup_table_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `seat` smallint(3) unsigned NOT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `is_ordered` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_paid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `total_splits` smallint(5) unsigned NOT NULL,
  `paid_splits` smallint(5) unsigned NOT NULL,
  `meal_course_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `foreup_table_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`),
  CONSTRAINT `foreup_table_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_tables` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_items_modifiers`
--

DROP TABLE IF EXISTS `foreup_table_items_modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_items_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sale_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_layout_objects`
--

DROP TABLE IF EXISTS `foreup_table_layout_objects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_layout_objects` (
  `object_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `layout_id` int(10) unsigned NOT NULL,
  `label` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_x` decimal(7,1) unsigned DEFAULT '0.0',
  `pos_y` decimal(7,1) unsigned DEFAULT '0.0',
  `width` smallint(4) unsigned DEFAULT NULL,
  `height` smallint(4) unsigned DEFAULT NULL,
  `rotation` smallint(3) unsigned DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`object_id`),
  UNIQUE KEY `object_label` (`layout_id`,`label`),
  KEY `layout_id` (`layout_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_layouts`
--

DROP TABLE IF EXISTS `foreup_table_layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_layouts` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order` tinyint(3) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`layout_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_payments`
--

DROP TABLE IF EXISTS `foreup_table_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_payments` (
  `sale_id` int(10) NOT NULL,
  `receipt_id` int(11) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `credit_card_invoice_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  KEY `credit_card_invoice_id` (`credit_card_invoice_id`),
  CONSTRAINT `foreup_table_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_tables` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_receipt_item_kits`
--

DROP TABLE IF EXISTS `foreup_table_receipt_item_kits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_receipt_item_kits` (
  `receipt_id` int(10) unsigned NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `item_kit_id` int(10) unsigned NOT NULL,
  `line` smallint(4) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`receipt_id`,`sale_id`,`item_kit_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_receipt_items`
--

DROP TABLE IF EXISTS `foreup_table_receipt_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_receipt_items` (
  `receipt_id` int(10) unsigned NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `line` smallint(4) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`receipt_id`,`sale_id`,`item_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_receipts`
--

DROP TABLE IF EXISTS `foreup_table_receipts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_receipts` (
  `receipt_id` int(10) unsigned NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `taxable` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `customer_id` int(10) unsigned DEFAULT NULL,
  `auto_gratuity` decimal(15,2) NOT NULL,
  `date_paid` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sale_id`,`receipt_id`),
  KEY `sale_id` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_ticket_item_kits`
--

DROP TABLE IF EXISTS `foreup_table_ticket_item_kits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_ticket_item_kits` (
  `ticket_id` int(10) unsigned NOT NULL,
  `item_kit_id` int(10) unsigned NOT NULL,
  `line` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`ticket_id`,`item_kit_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_ticket_items`
--

DROP TABLE IF EXISTS `foreup_table_ticket_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_ticket_items` (
  `ticket_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `line` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`ticket_id`,`item_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_table_tickets`
--

DROP TABLE IF EXISTS `foreup_table_tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_table_tickets` (
  `sale_id` int(10) unsigned NOT NULL,
  `ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_number` mediumint(8) unsigned NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `printed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_ordered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_completed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ticket_id`),
  KEY `sale_id` (`sale_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_tables`
--

DROP TABLE IF EXISTS `foreup_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_tables` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `manager_auth_id` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `course_id` int(11) NOT NULL,
  `table_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guest_count` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `table_id` (`course_id`,`table_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `deleted` (`deleted`),
  CONSTRAINT `foreup_tables_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `foreup_employees` (`person_id`),
  CONSTRAINT `foreup_tables_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `foreup_customers` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_teesheet`
--

DROP TABLE IF EXISTS `foreup_teesheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_teesheet` (
  `teesheet_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `TSID` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `associated_courses` text NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `online_open_time` varchar(10) NOT NULL DEFAULT '0900',
  `online_close_time` varchar(10) NOT NULL DEFAULT '1800',
  `days_out` smallint(6) NOT NULL,
  `days_in_booking_window` smallint(6) NOT NULL,
  `minimum_players` tinyint(4) NOT NULL DEFAULT '2',
  `limit_holes` tinyint(4) NOT NULL DEFAULT '0',
  `booking_carts` tinyint(4) NOT NULL,
  `increment` decimal(4,1) NOT NULL DEFAULT '8.0',
  `frontnine` smallint(6) NOT NULL DEFAULT '200',
  `fntime` smallint(6) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `default_price_class` varchar(30) NOT NULL,
  `color` char(7) NOT NULL,
  `online_booking` tinyint(1) NOT NULL,
  `online_rate_setting` tinyint(4) NOT NULL,
  `require_credit_card` tinyint(1) unsigned NOT NULL,
  `send_thank_you` tinyint(4) NOT NULL,
  `thank_you_campaign_id` int(11) NOT NULL,
  `thank_you_next_send` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `specials_enabled` tinyint(1) NOT NULL,
  `hide_online_prices` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show_on_aggregate` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pay_online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `block_online_booking` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`teesheet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_teesheet_notes`
--

DROP TABLE IF EXISTS `foreup_teesheet_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_teesheet_notes` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `teesheet_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `note` text NOT NULL,
  PRIMARY KEY (`note_id`),
  UNIQUE KEY `teesheet_id_2` (`teesheet_id`,`date`),
  KEY `teesheet_id` (`teesheet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_teesheet_restrictions`
--

DROP TABLE IF EXISTS `foreup_teesheet_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_teesheet_restrictions` (
  `teesheet_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `limit` varchar(255) NOT NULL,
  `Mon` tinyint(4) NOT NULL,
  `Tue` tinyint(4) NOT NULL,
  `Wed` tinyint(4) NOT NULL,
  `Thu` tinyint(4) NOT NULL,
  `Fri` tinyint(4) NOT NULL,
  `Sat` tinyint(4) NOT NULL,
  `Sun` tinyint(4) NOT NULL,
  `start_time` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_teetime`
--

DROP TABLE IF EXISTS `foreup_teetime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_teetime` (
  `TTID` varchar(21) NOT NULL,
  `reround` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL,
  `status` enum('','checked in','deleted','teed off','walked in') NOT NULL,
  `start` bigint(20) NOT NULL,
  `end` bigint(20) NOT NULL,
  `allDay` varchar(5) NOT NULL,
  `player_count` smallint(6) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `carts` float NOT NULL,
  `paid_player_count` tinyint(4) NOT NULL DEFAULT '0',
  `paid_carts` tinyint(4) NOT NULL DEFAULT '0',
  `clubs` tinyint(4) NOT NULL,
  `title` varchar(50) NOT NULL,
  `cplayer_count` tinyint(4) NOT NULL,
  `choles` tinyint(4) NOT NULL,
  `ccarts` float NOT NULL,
  `cpayment` varchar(10) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `details` text NOT NULL,
  `side` varchar(10) NOT NULL,
  `className` varchar(40) NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `price_class_1` mediumint(9) NOT NULL,
  `person_paid_1` tinyint(4) NOT NULL,
  `cart_paid_1` tinyint(4) NOT NULL,
  `person_name` varchar(255) NOT NULL,
  `person_id_2` int(11) NOT NULL,
  `price_class_2` tinyint(4) NOT NULL,
  `person_paid_2` tinyint(4) NOT NULL,
  `cart_paid_2` tinyint(4) NOT NULL,
  `person_name_2` varchar(255) NOT NULL,
  `person_id_3` int(11) NOT NULL,
  `price_class_3` tinyint(4) NOT NULL,
  `person_paid_3` tinyint(4) NOT NULL,
  `cart_paid_3` tinyint(4) NOT NULL,
  `person_name_3` varchar(255) NOT NULL,
  `person_id_4` int(11) NOT NULL,
  `price_class_4` tinyint(4) NOT NULL,
  `person_paid_4` tinyint(4) NOT NULL,
  `cart_paid_4` tinyint(4) NOT NULL,
  `person_name_4` varchar(255) NOT NULL,
  `person_id_5` int(11) NOT NULL,
  `price_class_5` tinyint(4) NOT NULL,
  `person_paid_5` tinyint(4) NOT NULL,
  `cart_paid_5` tinyint(4) NOT NULL,
  `person_name_5` varchar(255) NOT NULL,
  `default_cart_fee` smallint(5) unsigned NOT NULL,
  `default_price_category` smallint(5) unsigned NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_booked` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_cancelled` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `send_confirmation` tinyint(4) NOT NULL,
  `confirmation_emailed` tinyint(4) NOT NULL,
  `thank_you_emailed` tinyint(4) NOT NULL,
  `booking_source` varchar(255) NOT NULL,
  `booker_id` varchar(255) NOT NULL,
  `canceller_id` int(11) NOT NULL,
  `teetime_id` int(11) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `teed_off_time` datetime NOT NULL,
  `turn_time` datetime NOT NULL,
  `finish_time` datetime NOT NULL,
  `api_id` int(11) NOT NULL,
  `raincheck_players_issued` tinyint(4) NOT NULL,
  `front_paid` tinyint(4) NOT NULL,
  `back_paid` tinyint(4) NOT NULL,
  `front_player_count` tinyint(4) NOT NULL,
  `back_player_count` tinyint(4) NOT NULL,
  `front_carts` tinyint(4) NOT NULL,
  `back_carts` tinyint(4) NOT NULL,
  `front_paid_carts` tinyint(4) NOT NULL,
  `back_paid_carts` tinyint(4) NOT NULL,
  `cart_num_1` varchar(20) NOT NULL,
  `cart_num_2` varchar(20) NOT NULL,
  `promo_id` int(10) unsigned DEFAULT NULL,
  `aggregate_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`TTID`),
  KEY `teesheet_id` (`teesheet_id`),
  KEY `start` (`start`),
  KEY `end` (`end`),
  KEY `status` (`status`),
  KEY `teesheet_id_start` (`teesheet_id`,`start`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_teetime_standbys`
--

DROP TABLE IF EXISTS `foreup_teetime_standbys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_teetime_standbys` (
  `standby_id` int(11) NOT NULL AUTO_INCREMENT,
  `teesheet_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `time` datetime NOT NULL,
  `players` tinyint(4) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `details` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  PRIMARY KEY (`standby_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_teetimes_bartered`
--

DROP TABLE IF EXISTS `foreup_teetimes_bartered`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_teetimes_bartered` (
  `teetime_id` varchar(21) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `start` bigint(20) NOT NULL,
  `end` bigint(20) NOT NULL,
  `player_count` tinyint(4) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `carts` float NOT NULL,
  `date_booked` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `booker_id` varchar(255) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `api_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`teetime_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_terminal_printers`
--

DROP TABLE IF EXISTS `foreup_terminal_printers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_terminal_printers` (
  `terminal_id` int(10) unsigned NOT NULL,
  `printer_group_id` int(10) unsigned NOT NULL,
  `printer_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`terminal_id`,`printer_group_id`,`printer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_terminals`
--

DROP TABLE IF EXISTS `foreup_terminals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_terminals` (
  `terminal_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `quickbutton_tab` tinyint(4) NOT NULL DEFAULT '1',
  `course_id` int(11) NOT NULL,
  `mercury_id` varchar(255) NOT NULL,
  `mercury_password` varchar(255) NOT NULL,
  `mercury_e2e_id` varchar(255) NOT NULL,
  `mercury_e2e_password` varchar(255) NOT NULL,
  `ets_key` varchar(255) NOT NULL,
  `e2e_account_id` varchar(255) NOT NULL,
  `e2e_account_key` varchar(255) NOT NULL,
  `auto_print_receipts` tinyint(4) DEFAULT NULL,
  `webprnt` tinyint(4) DEFAULT NULL,
  `receipt_ip` varchar(20) NOT NULL,
  `hot_webprnt_ip` varchar(20) NOT NULL,
  `cold_webprnt_ip` varchar(20) NOT NULL,
  `use_register_log` tinyint(4) DEFAULT NULL,
  `cash_register` tinyint(4) DEFAULT NULL,
  `print_tip_line` tinyint(4) DEFAULT NULL,
  `signature_slip_count` tinyint(4) DEFAULT NULL,
  `credit_card_receipt_count` tinyint(4) DEFAULT NULL,
  `non_credit_card_receipt_count` tinyint(4) DEFAULT NULL,
  `ibeacon_minor_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `persistent_logs` tinyint(4) NOT NULL,
  `after_sale_load` tinyint(4) DEFAULT NULL,
  `default_kitchen_printer` int(10) NOT NULL,
  `return_policy` varchar(4096) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`terminal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_timeclock_entries`
--

DROP TABLE IF EXISTS `foreup_timeclock_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_timeclock_entries` (
  `employee_id` int(11) NOT NULL,
  `terminal_id` int(11) NOT NULL,
  `shift_start` datetime NOT NULL,
  `shift_end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_tips`
--

DROP TABLE IF EXISTS `foreup_tips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_tips` (
  `tip_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `hole` int(11) NOT NULL,
  `overall_specific` tinyint(4) NOT NULL,
  `value_1` tinyint(4) NOT NULL,
  `value_2` tinyint(4) NOT NULL,
  `value_3` tinyint(4) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`tip_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_tournament_inventory_items`
--

DROP TABLE IF EXISTS `foreup_tournament_inventory_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_tournament_inventory_items` (
  `tournament_inventory_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`tournament_inventory_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_tournament_winners`
--

DROP TABLE IF EXISTS `foreup_tournament_winners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_tournament_winners` (
  `tournament_winner_id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `description` varchar(180) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`tournament_winner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_tournaments`
--

DROP TABLE IF EXISTS `foreup_tournaments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_tournaments` (
  `tournament_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `green_fee` decimal(15,2) NOT NULL,
  `carts_issued` tinyint(4) NOT NULL,
  `cart_fee` decimal(15,2) NOT NULL,
  `customer_credit_fee` decimal(15,2) NOT NULL,
  `member_credit_fee` decimal(15,2) NOT NULL,
  `pot_fee` decimal(15,2) NOT NULL,
  `tax_included` tinyint(4) NOT NULL,
  `inventory_item_taxes` decimal(15,2) NOT NULL,
  `total_cost` decimal(15,2) NOT NULL,
  `accumulated_pot` decimal(15,2) NOT NULL,
  `remaining_pot_balance` decimal(15,2) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`tournament_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_tracks`
--

DROP TABLE IF EXISTS `foreup_tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_tracks` (
  `track_id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `reround_track_id` tinyint(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `online_booking` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`track_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_unsubscribes`
--

DROP TABLE IF EXISTS `foreup_unsubscribes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_unsubscribes` (
  `email` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `unsubscribe_all` tinyint(1) NOT NULL,
  `course_news_announcements` tinyint(1) NOT NULL,
  `teetime_reminders` tinyint(1) NOT NULL,
  `foreup_news_announcements` tinyint(1) NOT NULL,
  `comments` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_users`
--

DROP TABLE IF EXISTS `foreup_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_users` (
  `person_id` int(10) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `twitter_id` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_weather`
--

DROP TABLE IF EXISTS `foreup_weather`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_weather` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recorded_at` datetime NOT NULL,
  `temp` float NOT NULL,
  `conditions` varchar(150) NOT NULL,
  `low` tinyint(4) NOT NULL,
  `high` tinyint(4) NOT NULL,
  `zip` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `recorded_at` (`course_id`,`recorded_at`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foreup_zip_code`
--

DROP TABLE IF EXISTS `foreup_zip_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreup_zip_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `county` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state_prefix` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `area_code` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `time_zone` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zip_code` (`zip_code`)
) ENGINE=MyISAM AUTO_INCREMENT=42625 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quickbooks_config`
--

DROP TABLE IF EXISTS `quickbooks_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quickbooks_config` (
  `quickbooks_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cfgkey` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cfgval` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cfgtype` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cfgopts` text COLLATE utf8_unicode_ci NOT NULL,
  `write_datetime` datetime NOT NULL,
  `mod_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quickbooks_connection`
--

DROP TABLE IF EXISTS `quickbooks_connection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quickbooks_connection` (
  `quickbooks_connection_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `certificate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `application_id` int(10) unsigned NOT NULL,
  `application_login` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lasterror_num` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lasterror_msg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_datetime` datetime NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_connection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quickbooks_ident`
--

DROP TABLE IF EXISTS `quickbooks_ident`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quickbooks_ident` (
  `quickbooks_ident_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_object` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `unique_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_ident` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `editsequence` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `extra` text COLLATE utf8_unicode_ci,
  `map_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_ident_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quickbooks_log`
--

DROP TABLE IF EXISTS `quickbooks_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quickbooks_log` (
  `quickbooks_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `batch` int(10) unsigned NOT NULL,
  `msg` text NOT NULL,
  `log_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_log_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `batch` (`batch`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quickbooks_notify`
--

DROP TABLE IF EXISTS `quickbooks_notify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quickbooks_notify` (
  `quickbooks_notify_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_object` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `unique_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_ident` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `errnum` int(10) unsigned DEFAULT NULL,
  `errmsg` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `write_datetime` datetime NOT NULL,
  `mod_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_notify_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quickbooks_queue`
--

DROP TABLE IF EXISTS `quickbooks_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quickbooks_queue` (
  `quickbooks_queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ident` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `extra` text COLLATE utf8_unicode_ci,
  `qbxml` text COLLATE utf8_unicode_ci,
  `priority` int(10) unsigned DEFAULT '0',
  `qb_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `msg` text COLLATE utf8_unicode_ci,
  `enqueue_datetime` datetime NOT NULL,
  `dequeue_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`quickbooks_queue_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `priority` (`priority`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`,`qb_status`),
  KEY `qb_status` (`qb_status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quickbooks_recur`
--

DROP TABLE IF EXISTS `quickbooks_recur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quickbooks_recur` (
  `quickbooks_recur_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ident` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `extra` text COLLATE utf8_unicode_ci,
  `qbxml` text COLLATE utf8_unicode_ci,
  `priority` int(10) unsigned DEFAULT '0',
  `run_every` int(10) unsigned NOT NULL,
  `recur_lasttime` int(10) unsigned NOT NULL,
  `enqueue_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_recur_id`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quickbooks_ticket`
--

DROP TABLE IF EXISTS `quickbooks_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quickbooks_ticket` (
  `quickbooks_ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ticket` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `processed` int(10) unsigned DEFAULT '0',
  `lasterror_num` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lasterror_msg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipaddr` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_ticket_id`),
  KEY `ticket` (`ticket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quickbooks_user`
--

DROP TABLE IF EXISTS `quickbooks_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quickbooks_user` (
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qb_company_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_initialized` tinyint(1) NOT NULL DEFAULT '0',
  `qbwc_wait_before_next_update` int(10) unsigned DEFAULT '0',
  `qbwc_min_run_every_n_seconds` int(10) unsigned DEFAULT '0',
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`qb_username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `session_data`
--

DROP TABLE IF EXISTS `session_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_data` (
  `session_id` varchar(32) NOT NULL DEFAULT '',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `session_data` mediumblob NOT NULL,
  `session_expire` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `temptable2`
--

DROP TABLE IF EXISTS `temptable2`;
/*!50001 DROP VIEW IF EXISTS `temptable2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `temptable2` (
  `deleted` tinyint NOT NULL,
  `sale_date` tinyint NOT NULL,
  `serialnumber` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `teetime_id` tinyint NOT NULL,
  `terminal_id` tinyint NOT NULL,
  `sale_id` tinyint NOT NULL,
  `number` tinyint NOT NULL,
  `line` tinyint NOT NULL,
  `timeframe_id` tinyint NOT NULL,
  `comment` tinyint NOT NULL,
  `payment_type` tinyint NOT NULL,
  `customer_id` tinyint NOT NULL,
  `employee_id` tinyint NOT NULL,
  `item_id` tinyint NOT NULL,
  `item_kit_id` tinyint NOT NULL,
  `invoice_id` tinyint NOT NULL,
  `supplier_id` tinyint NOT NULL,
  `quantity_purchased` tinyint NOT NULL,
  `item_cost_price` tinyint NOT NULL,
  `item_unit_price` tinyint NOT NULL,
  `item_number` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `actual_category` tinyint NOT NULL,
  `department` tinyint NOT NULL,
  `category` tinyint NOT NULL,
  `subcategory` tinyint NOT NULL,
  `gl_code` tinyint NOT NULL,
  `discount_percent` tinyint NOT NULL,
  `subtotal` tinyint NOT NULL,
  `total` tinyint NOT NULL,
  `tax` tinyint NOT NULL,
  `profit` tinyint NOT NULL,
  `total_cost` tinyint NOT NULL,
  `receipt_only` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `temptable2`
--

/*!50001 DROP TABLE IF EXISTS `temptable2`*/;
/*!50001 DROP VIEW IF EXISTS `temptable2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `temptable2` AS (select `s`.`deleted` AS `deleted`,`s`.`sale_time` AS `sale_date`,`si`.`serialnumber` AS `serialnumber`,`si`.`description` AS `description`,`s`.`teetime_id` AS `teetime_id`,`s`.`terminal_id` AS `terminal_id`,`s`.`sale_id` AS `sale_id`,`s`.`number` AS `number`,`si`.`line` AS `line`,`si`.`timeframe_id` AS `timeframe_id`,`s`.`comment` AS `comment`,`s`.`payment_type` AS `payment_type`,`s`.`customer_id` AS `customer_id`,`s`.`employee_id` AS `employee_id`,`i`.`item_id` AS `item_id`,NULL AS `item_kit_id`,NULL AS `invoice_id`,`i`.`supplier_id` AS `supplier_id`,`si`.`quantity_purchased` AS `quantity_purchased`,`si`.`item_cost_price` AS `item_cost_price`,`si`.`item_unit_price` AS `item_unit_price`,`i`.`item_number` AS `item_number`,`i`.`name` AS `name`,`i`.`category` AS `actual_category`,if((`si`.`item_id` = 0),'Invoice Line Items',`i`.`department`) AS `department`,if((`si`.`item_id` = 0),'Invoice Line Items',`i`.`category`) AS `category`,if(((`si`.`price_category` <> '') and (`tf`.`timeframe_name` is not null)),concat(`si`.`price_category`,' - ',`tf`.`timeframe_name`),if((`si`.`price_category` <> ''),concat(`si`.`price_category`,' - No Timeframe'),`i`.`subcategory`)) AS `subcategory`,`i`.`gl_code` AS `gl_code`,`si`.`discount_percent` AS `discount_percent`,`si`.`subtotal` AS `subtotal`,`si`.`total` AS `total`,`si`.`tax` AS `tax`,`si`.`profit` AS `profit`,`si`.`total_cost` AS `total_cost`,`si`.`receipt_only` AS `receipt_only` from (((`foreup_sales_items` `si` join `foreup_sales` `s` on((`si`.`sale_id` = `s`.`sale_id`))) left join `foreup_items` `i` on((`si`.`item_id` = `i`.`item_id`))) left join `foreup_seasonal_timeframes` `tf` on((`si`.`timeframe_id` = `tf`.`timeframe_id`))) where ((1 = 1) and (`s`.`course_id` = 7566) and (`s`.`sale_time` between '2014-03-01' and '2015-03-30'))) union all (select `s`.`deleted` AS `deleted`,`s`.`sale_time` AS `sale_date`,'' AS `serialnumber`,`si`.`description` AS `description`,`s`.`teetime_id` AS `teetime_id`,`s`.`terminal_id` AS `terminal_id`,`s`.`sale_id` AS `sale_id`,`s`.`number` AS `number`,`si`.`line` AS `line`,'' AS `Name_exp_45`,`s`.`comment` AS `comment`,`s`.`payment_type` AS `payment_type`,`s`.`customer_id` AS `customer_id`,`s`.`employee_id` AS `employee_id`,NULL AS `item_id`,`i`.`item_kit_id` AS `item_kit_id`,NULL AS `invoice_id`,NULL AS `supplier_id`,`si`.`quantity_purchased` AS `quantity_purchased`,`si`.`item_kit_cost_price` AS `item_kit_cost_price`,`si`.`item_kit_unit_price` AS `item_kit_unit_price`,`i`.`item_kit_number` AS `item_kit_number`,`i`.`name` AS `name`,`i`.`category` AS `actual_category`,`i`.`department` AS `department`,if(((`si`.`price_category` <> '') and (`si`.`teesheet` <> '')),concat_ws(' ',`si`.`teesheet`,`i`.`category`),`i`.`category`) AS `category`,if((`si`.`price_category` <> ''),`si`.`price_category`,`i`.`subcategory`) AS `subcategory`,NULL AS `gl_code`,`si`.`discount_percent` AS `discount_percent`,`si`.`subtotal` AS `subtotal`,`si`.`total` AS `total`,`si`.`tax` AS `tax`,`si`.`profit` AS `profit`,`si`.`total_cost` AS `total_cost`,`si`.`receipt_only` AS `receipt_only` from ((`foreup_sales_item_kits` `si` join `foreup_sales` `s` on((`si`.`sale_id` = `s`.`sale_id`))) left join `foreup_item_kits` `i` on((`si`.`item_kit_id` = `i`.`item_kit_id`))) where ((1 = 1) and (`s`.`course_id` = 7566) and (`s`.`sale_time` between '2014-03-01' and '2015-03-30'))) union all (select `s`.`deleted` AS `deleted`,`s`.`sale_time` AS `sale_date`,'' AS `serialnumber`,`si`.`description` AS `description`,`s`.`teetime_id` AS `teetime_id`,`s`.`terminal_id` AS `terminal_id`,`s`.`sale_id` AS `sale_id`,`s`.`number` AS `number`,`si`.`line` AS `line`,'' AS `Name_exp_80`,`s`.`comment` AS `comment`,`s`.`payment_type` AS `payment_type`,`s`.`customer_id` AS `customer_id`,`s`.`employee_id` AS `employee_id`,NULL AS `item_id`,NULL AS `item_kit_id`,`i`.`invoice_id` AS `invoice_id`,NULL AS `supplier_id`,`si`.`quantity_purchased` AS `quantity_purchased`,`si`.`invoice_cost_price` AS `invoice_cost_price`,`si`.`invoice_unit_price` AS `invoice_unit_price`,NULL AS `item_kit_number`,`i`.`name` AS `name`,`i`.`category` AS `actual_category`,`i`.`department` AS `department`,if(((`si`.`price_category` <> '') and (`si`.`teesheet` <> '')),concat_ws(' ',`si`.`teesheet`,`i`.`category`),`i`.`category`) AS `category`,if((`si`.`price_category` <> ''),`si`.`price_category`,`i`.`subcategory`) AS `subcategory`,NULL AS `gl_code`,`si`.`discount_percent` AS `discount_percent`,`si`.`subtotal` AS `subtotal`,`si`.`total` AS `total`,`si`.`tax` AS `tax`,`si`.`profit` AS `profit`,`si`.`total_cost` AS `total_cost`,`si`.`receipt_only` AS `receipt_only` from ((`foreup_sales_invoices` `si` join `foreup_sales` `s` on((`si`.`sale_id` = `s`.`sale_id`))) left join `foreup_invoices` `i` on((`si`.`invoice_id` = `i`.`invoice_id`))) where ((1 = 1) and (`s`.`course_id` = 7566) and (`s`.`sale_time` between '2014-03-01' and '2015-03-30'))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-03 14:28:07
