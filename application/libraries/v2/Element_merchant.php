<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Element_merchant {

	const APPLICATION_NAME = 'ForeUP';
	const APPLICATION_VERSION = '1.0.0';
	const APPLICATION_ID = 2590;

	const EXPRESS_TRANSACTION_URL = 'https://transaction.elementexpress.com';
	const EXPRESS_REPORTING_URL = 'https://reporting.elementexpress.com';
	const EXPRESS_SERVICES_URL = 'https://services.elementexpress.com';
	const HOSTED_PAYMENTS_URL = 'https://transaction.hostedpayments.com';

	const EXPRESS_TRANSACTION_TEST_URL = 'https://certtransaction.elementexpress.com';
	const EXPRESS_REPORTING_TEST_URL = 'https://certreporting.elementexpress.com';
	const EXPRESS_SERVICES_TEST_URL = 'https://certservices.elementexpress.com';
	const HOSTED_PAYMENTS_TEST_URL = 'https://certtransaction.hostedpayments.com';	

	const TRANSACTION_NAMESPACE = 'https://transaction.elementexpress.com';
	const REPORTING_NAMESPACE = 'https://reporting.elementexpress.com';
	const SERVICES_NAMESPACE = 'https://services.elementexpress.com';

	const CONTENT_TYPE = 'text/xml';

	const RESPONSE_APPROVED = 0;
	const RESPONSE_PARTIAL_APPROVAL = 5;
	const RESPONSE_DECLINE = 20;
	const RESPONSE_EXPIRED_CARD = 21;
	const RESPONSE_DUPLICATE_APPROVED = 22;
	const RESPONSE_DUPLICATE = 23;
	const RESPONSE_PICK_UP_CARD = 24;
	const RESPONSE_CALL_ISSUER = 25;
	const RESPONSE_BALANCE_NOT_AVAILABLE = 30;
	const RESPONSE_NOT_DEFINED = 90;
	const RESPONSE_INVALID_ACCOUNT = 102;
	const RESPONSE_INVALID_REQUEST = 103;
	const RESPONSE_AUTHORIZATION_FAILED = 104;
	const RESPONSE_NOT_ALLOWED = 105;
	const RESPONSE_OUT_OF_BALANCE = 120;
	const RESPONSE_COMMUNICATION_ERROR = 1001;
	const RESPONSE_HOST_ERROR = 1002;
	const RESPONSE_ERROR = 1009;

	const TRANSACTION_SETUP_DEFAULT = 0;
	const TRANSACTION_SETUP_CREDIT_CARD_SALE = 1;
	const TRANSACTION_SETUP_CREDIT_CARD_AUTH = 2;
	const TRANSACTION_SETUP_CREDIT_CARD_AVS = 3;
	const TRANSACTION_SETUP_CREDIT_CARD_FORCE = 4;
	const TRANSACTION_SETUP_DEBIT_CARD_SALE = 5;
	const TRANSACTION_SETUP_CHECK_SALE = 6;
	const TRANSACTION_SETUP_PAYMENT_ACCOUNT_CREATE = 7;
	const TRANSACTION_SETUP_PAYMENT_ACCOUNT_UPDATE = 8;
	const TRANSACTION_SETUP_SALE = 9;

	const PAYMENT_ACCOUNT_CREDIT_CARD = 0;
	const PAYMENT_ACCOUNT_CHECKING = 1;
	const PAYMENT_ACCOUNT_SAVINGS = 2;
	const PAYMENT_ACCOUNT_ACH = 3;
	const PAYMENT_ACCOUNT_OTHER = 4;

	const MARKET_CODE_DEFAULT = 0;
	const MARKET_CODE_AUTO_RENTAL = 1;
	const MARKET_CODE_DIRECT_MARKETING = 2;
	const MARKET_CODE_ECOMMERCE = 3;
	const MARKET_CODE_FOOD_RESTAURANT = 4;
	const MARKET_CODE_HOTEL_LODGING = 5;
	const MARKET_CODE_PETROLEUM = 6;
	const MARKET_CODE_RETAIL = 7;
	const MARKET_CODE_QSR = 8;

	const CARD_INPUT_CODE_DEFAULT = 0;
	const CARD_INPUT_CODE_UNKNOWN = 1;
	const CARD_INPUT_CODE_MAGSTRIPE_READ = 2;
	const CARD_INPUT_CODE_CONTACTLESS_MAGSTRIPE_READ = 3;
	const CARD_INPUT_CODE_MANUAL_KEYED = 4;
	const CARD_INPUT_CODE_MANUAL_KEYED_MAGSTRIPE_FAILURE = 5;

	const CARDHOLDER_PRESENT_CODE_DEFAULT = 0;
	const CARDHOLDER_PRESENT_CODE_UNKNOWN = 1;
	const CARDHOLDER_PRESENT_CODE_PRESENT = 2;
	const CARDHOLDER_PRESENT_CODE_NOT_PRESENT = 3;
	const CARDHOLDER_PRESENT_CODE_MAIL_ORDER = 4;
	const CARDHOLDER_PRESENT_CODE_PHONE_ORDER = 5;
	const CARDHOLDER_PRESENT_CODE_STANDING_AUTH = 6;
	const CARDHOLDER_PRESENT_CODE_ECOMMERCE = 7;

	const CARD_PRESENT_CODE_DEFAULT = 0;
	const CARD_PRESENT_CODE_UNKNOWN = 1;
	const CARD_PRESENT_CODE_PRESENT = 2;
	const CARD_PRESENT_CODE_NOT_PRESENT = 3;

	const MOTO_ECI_CODE_DEFAULT = 0;
	const MOTO_ECI_CODE_NOT_USED = 1;
	const MOTO_ECI_CODE_SINGLE = 2;
	const MOTO_ECI_CODE_RECURRING = 3;
	const MOTO_ECI_CODE_INSTALLMENT = 4;
	const MOTO_ECI_CODE_SECURE_ECOMMERCE = 5;
	const MOTO_ECI_CODE_NON_AUTHENTICATED_SECURE_TRANSACTION = 6;
	const MOTO_ECI_CODE_NON_AUTHENTICATED_SECURE_ECOMMERCE_TRANSACTION = 7;
	const MOTO_ECI_CODE_NON_SECURE_ECOMMERCE_TRANSACTION = 8;

	const TERMINAL_CAPABILITY_CODE_DEFAULT = 0;
	const TERMINAL_CAPABILITY_CODE_UNKNOWN = 1;
	const TERMINAL_CAPABILITY_CODE_NO_TERMINAL = 2;
	const TERMINAL_CAPABILITY_CODE_MAGSTRIPE_READER = 3;
	const TERMINAL_CAPABILITY_CODE_CONTACTLESS_MAGSTRIPE_READER = 4;
	const TERMINAL_CAPABILITY_CODE_KEY_ENTERED = 5;

	const TERMINAL_ENVIRONMENT_CODE_DEFAULT = 0;
	const TERMINAL_ENVIRONMENT_CODE_NO_TERMINAL = 1;
	const TERMINAL_ENVIRONMENT_CODE_LOCAL_ATTENDED = 2;
	const TERMINAL_ENVIRONMENT_CODE_LOCAL_UNATTENDED = 3;
	const TERMINAL_ENVIRONMENT_CODE_REMOTE_ATTENDED = 4;
	const TERMINAL_ENVIRONMENT_CODE_REMOTE_UNATTENDED = 5;
	const TERMINAL_ENVIRONMENT_CODE_ECOMMERCE = 6;

	const TERMINAL_TYPE_DEFAULT = 0;
	const TERMINAL_TYPE_POINT_OF_SALE = 1;
	const TERMINAL_TYPE_ECOMMERCE = 2;
	const TERMINAL_TYPE_MOTO = 3;
	const TERMINAL_TYPE_FUEL_PUMP = 4;
	const TERMINAL_TYPE_ATM = 5;
	const TERMINAL_TYPE_VOICE = 6;

	const REVERSAL_TYPE_SYSTEM = 0;
	const REVERSAL_TYPE_FULL = 1;
	const REVERSAL_TYPE_PARTIAL = 2;

	private $credentials = null;
	private $application = null;
	private $terminal = null;
	private $xml_request = '';

	private $error = false;
	private $msg = false;
	private $response = null;
	private $testing = false;

	private function reset_error(){
		$this->error = false;
		$this->msg = null;
		$this->xml_request = '';
	}

	// Convert an array into XML data
	private function generate_xml($data, $root_xml){

		$xml_object = new SimpleXMLElement($root_xml);
		$this->array_to_xml($data, $xml_object);

		return $xml_object->asXML();
	}

	// Iterates recusively through an array and adds each array value as
	// a node to an XML object
	private function array_to_xml($array_data, &$xml_object){

		foreach($array_data as $key => $value) {

			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $xml_object->addChild("$key");
					$this->array_to_xml($value, $subnode);
				}
				else{
					$subnode = $xml_object->addChild("item{$key}");
					$this->array_to_xml($value, $subnode);
				}

			}else{
				$xml_object->addChild("$key", utf8_encode($value));
			}
		}
	}

	// Saves the response of an API call and determines if there was an error
	private function parse_response($response){

		$this->response = $response;
		$CI =& get_instance();
		
		// If there was an error with the request
		if($response->getStatusCode() >= 400){
			$this->error = true;
			$this->msg = $response->getReasonPhrase();
			
			error_log('ELEMENT API ERROR - Received HTTP error status code: '.$response->getStatusCode().' [URL] '.$response->getEffectiveUrl().
				' [COURSE_ID '.$CI->session->userdata('course_id').'] [REQUEST] '.
				$this->get_request_xml().' [RESPONSE] '.$response->getBody());
			
			return false;
		}
		
		try {
			$xml_obj = $response->xml();
			$code = (int) $xml_obj->Response->ExpressResponseCode;
		
		}catch(Exception $e){
			error_log('ELEMENT API ERROR - Error parsing response XML [URL] '.
				$response->getEffectiveUrl(). ' [COURSE_ID '.$CI->session->userdata('course_id').'] [REQUEST] '.
				$this->get_request_xml().' [RESPONSE] '.$response->getBody());
				
			$this->error = true;
			$this->msg = 'Error: Merchant response parse error';
			return false;
		}

		// If there was an error with the API
		if(self::RESPONSE_APPROVED != $code &&
			self::RESPONSE_PARTIAL_APPROVAL != $code &&
			self::RESPONSE_DUPLICATE_APPROVED != $code){

			$this->error = true;

		}else{
			$this->error = false;
		}

		$this->msg = (string) $xml_obj->Response->ExpressResponseMessage;
		return true;
	}
	
	public function set_testing($testing){
		$this->testing = (bool) $testing;
		return $this;
	}

	public function get_transaction_url(){
		if($this->testing){
			return self::EXPRESS_TRANSACTION_TEST_URL;
		}
		return self::EXPRESS_TRANSACTION_URL;
	}
	
	public function get_reporting_url(){
		if($this->testing){
			return self::EXPRESS_REPORTING_TEST_URL;
		}
		return self::EXPRESS_REPORTING_URL;		
	}
	
	public function get_services_url(){
		if($this->testing){
			return self::EXPRESS_SERVICES_TEST_URL;
		}
		return self::EXPRESS_SERVICES_URL;		
	}	
	
	public function get_hosted_payments_url(){
		if($this->testing){
			return self::HOSTED_PAYMENTS_TEST_URL;
		}
		return self::HOSTED_PAYMENTS_URL;		
	}			
	
	public function get_request_xml(){
		return $this->xml_request;
	}
	
	public function set_terminal($params){
		if(empty($params)){
			return $this;
		}
		$this->terminal = array_merge($this->terminal, $params);
		return $this;
	}

	public function init(GuzzleHttp\Client $http, $params = null){

		$this->http = $http;
		$CI =& get_instance();

		if(empty($params)){
			$params = array(
				'account_id' => $CI->config->item('element_account_id'),
				'account_token' => $CI->config->item('element_account_token'),
				'application_id' => self::APPLICATION_ID,
				'acceptor_id' => $CI->config->item('element_acceptor_id'),
				'terminal_id' => $CI->config->item('element_application_id')
			);
		}
		
		if($CI->config->item('demo') == 1){
			$this->testing = true;
		}elseif(isset($params['testing']) && $params['testing']){
			$this->testing = true;
		}
		
		$this->credentials = array(
			'AccountID' => $params['account_id'],
			'AccountToken' => $params['account_token'],
			'AcceptorID' => $params['acceptor_id']
		);

		$this->application = array(
			'ApplicationID' => $params['application_id'],
			'ApplicationName' => self::APPLICATION_NAME,
			'ApplicationVersion' => self::APPLICATION_VERSION
		);

		$this->terminal = array(
			'TerminalID' => 				$params['terminal_id'],
			'CardholderPresentCode' => 		self::CARDHOLDER_PRESENT_CODE_PRESENT,
			'CardInputCode' => 				self::CARD_INPUT_CODE_MAGSTRIPE_READ,
			'CardPresentCode' => 			self::CARD_PRESENT_CODE_PRESENT,
			'CVVPresenceCode' => 			0,
			'MotoECICode' => 				self::MOTO_ECI_CODE_NOT_USED,
			'TerminalCapabilityCode' => 	self::TERMINAL_CAPABILITY_CODE_MAGSTRIPE_READER,
			'TerminalEnvironmentCode' => 	self::TERMINAL_ENVIRONMENT_CODE_LOCAL_ATTENDED,
			'TerminalType' => 				self::TERMINAL_TYPE_POINT_OF_SALE
		);

		$this->headers = array(
			'Content-type' => self::CONTENT_TYPE
		);

		$this->curl_config = array(
			CURLOPT_FOLLOWLOCATION => 1,
			CURLOPT_MAXREDIRS => 5,
            CURLOPT_TIMEOUT => 15,
            CURLOPT_CONNECTTIMEOUT => 0,
            CURLOPT_RETURNTRANSFER => 1
		);
	}

	public function success(){
		return !$this->error;
	}

	public function message(){
		return $this->msg;
	}

	public function response(){
		return $this->response;
	}

	// Initialize a Hosted Payment iframe
	public function init_hosted_payment($payment){

		$this->reset_error();

		if(!isset($payment['transaction_setup_method'])){
			$payment['transaction_setup_method'] = self::TRANSACTION_SETUP_CREDIT_CARD_SALE;
		}

		if(!isset($payment['market_code'])){
			$payment['market_code'] = self::MARKET_CODE_DEFAULT;
		}

		if(!isset($payment['reference_number'])){
			$payment['reference_number'] = 0;
		}

		if(!isset($payment['ticket_number'])){
			$payment['ticket_number'] = 0;
		}

		$request_body = array(
			'TransactionSetup' => array(
				'TransactionSetupMethod' => $payment['transaction_setup_method'],
				'Embedded' => true,
				'AutoReturn' => true,
				'ReturnURL' => $payment['return_url']
			),
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Terminal' => $this->terminal,
			'Transaction' => array(
				'MarketCode' => $payment['market_code'],
				'ReferenceNumber' => $payment['reference_number'],
				'TicketNumber' => $payment['ticket_number']
			)
		);

		if(isset($payment['amount'])){
			$request_body['Transaction']['TransactionAmount'] = number_format((float)  $payment['amount'], 2, '.', '');
		}

		if(isset($payment['card_input_code'])){
			$request_body['Terminal']['CardInputCode'] = (int) $payment['card_input_code'];
		}

		if(isset($payment['payment_account_type'])){
			$request_body['PaymentAccount']['PaymentAccountType'] = (int) $payment['payment_account_type'];
		}

		if(isset($payment['billing_address1'])){
			$request_body['Address']['BillingAddress1'] = $payment['billing_address1'];
		}

		if(isset($payment['billing_zipcode'])){
			$request_body['Address']['BillingZipcode'] = $payment['billing_zipcode'];
		}

		if(isset($payment['payment_account_reference_number'])){
			$request_body['PaymentAccount']['PaymentAccountReferenceNumber'] = $payment['payment_account_reference_number'];
		}

		$this->xml_request = $this->generate_xml($request_body, "<TransactionSetup xmlns='".self::TRANSACTION_NAMESPACE."'></TransactionSetup>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Used in conjuction with the above function,
	// returns a URL for Hosted Payment iframe
	public function hosted_payment_url(){

		if(empty($this->response) || !($this->response->xml() instanceof SimpleXMLElement)){
			return false;
		}
		$setup_id = (string) $this->response->xml()->Response->Transaction->TransactionSetupID;

		if(empty($setup_id)){
			return false;
		}
		
		return $this->get_hosted_payments_url().'?TransactionSetupID='.$setup_id;
	}

	// Query past credit card transactions
	public function transaction_query($params){

		$this->reset_error();
		$parameters = array();

		if(isset($params['transaction_id'])){
			$parameters['TransactionID'] = $params['transaction_id'];
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Parameters' => $parameters
		);

		$this->xml_request = $this->generate_xml($request_body, "<TransactionQuery xmlns='".self::REPORTING_NAMESPACE."'></TransactionQuery>");

		// Send request
		$response = $this->http->post($this->get_reporting_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));
		
		$this->parse_response($response);
		return $this;
	}

	// Query past credit card transactions
	public function payment_account_query($params){

		$this->reset_error();
		$parameters = array();

		if(isset($params['payment_account_id'])){
			$parameters['PaymentAccountID'] = $params['payment_account_id'];
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'PaymentAccountParameters' => $parameters
		);

		$this->xml_request = $this->generate_xml($request_body, "<PaymentAccountQuery xmlns='".self::SERVICES_NAMESPACE."'></PaymentAccountQuery>");

		// Send request
		$response = $this->http->post($this->get_services_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Make sure credentials are valid and Element API is OK
	public function health_check(){

		$this->reset_error();
		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Terminal' => $this->terminal
		);

		$this->xml_request = $this->generate_xml($request_body, "<HealthCheck xmlns='".self::TRANSACTION_NAMESPACE."'></HealthCheck>");

		// Send request
		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Void a completed credit card transaction
	public function credit_card_void($payment){

		$this->reset_error();

		if(!isset($payment['reference_number'])){
			$payment['reference_number'] = 0;
		}

		if(!isset($payment['ticket_number'])){
			$payment['ticket_number'] = 0;
		}

		if(!isset($payment['market_code'])){
			$payment['market_code'] = self::MARKET_CODE_RETAIL;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Terminal' => $this->terminal,
			'Transaction' => array(
				'TransactionID' => $payment['transaction_id'],
				'ReferenceNumber' => substr($payment['reference_number'], 0, 16),
				'TicketNumber' => substr($payment['ticket_number'], -6, 6),
				'MarketCode' => (int) $payment['market_code']
			)
		);

		$this->xml_request = $this->generate_xml($request_body, "<CreditCardVoid xmlns='".self::TRANSACTION_NAMESPACE."'></CreditCardVoid>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Return a previous credit card transaction
	public function credit_card_return($payment){

		$this->reset_error();

		if(!isset($payment['reference_number'])){
			$payment['reference_number'] = 0;
		}

		if(!isset($payment['ticket_number'])){
			$payment['ticket_number'] = 0;
		}

		if(!isset($payment['market_code'])){
			$payment['market_code'] = self::MARKET_CODE_RETAIL;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Terminal' => $this->terminal,
			'Transaction' => array(
				'TransactionID' => $payment['transaction_id'],
				'TransactionAmount' => number_format((float)  $payment['amount'], 2, '.', ''),
				'ReferenceNumber' => $payment['reference_number'],
				'TicketNumber' => substr($payment['ticket_number'], -6, 6),
				'MarketCode' => $payment['market_code']
			)
		);

		$this->xml_request = $this->generate_xml($request_body, "<CreditCardReturn xmlns='".self::TRANSACTION_NAMESPACE."'></CreditCardReturn>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Reverse a CreditCardSale or CreditCardAuthorization
	public function credit_card_reversal($payment){

		$this->reset_error();

		if(!isset($payment['market_code'])){
			$payment['market_code'] = self::MARKET_CODE_RETAIL;
		}

		if(!isset($payment['reference_number'])){
			$payment['reference_number'] = 0;
		}

		if(!isset($payment['ticket_number'])){
			$payment['ticket_number'] = 0;
		}

		if(!isset($payment['reversal_type'])){
			$payment['reversal_type'] = 0;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Terminal' => $this->terminal,
			'Transaction' => array(
				'TransactionAmount' => number_format((float)  $payment['amount'], 2, '.', ''),
				'ReferenceNumber' => $payment['reference_number'],
				'TicketNumber' => substr($payment['ticket_number'], -6, 6),
				'MarketCode' => $payment['market_code'],
				'ReversalType' => $payment['reversal_type']
			)
		);

		if(isset($payment['total_authorized_amount'])){
			$request_body['Transaction']['TotalAuthorizedAmount'] = number_format((float)  $payment['total_authorized_amount'], 2, '.', '');
		}

		if(isset($payment['transaction_id'])){
			$request_body['Transaction']['TransactionID'] = $payment['transaction_id'];
		}

		if(isset($payment['payment_account_id'])){
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}

		if(isset($payment['encrypted_track'])){
			$request_body['Card']['MagneprintData'] = $payment['encrypted_track'];
		}

		$this->xml_request = $this->generate_xml($request_body, "<CreditCardReversal xmlns='".self::TRANSACTION_NAMESPACE."'></CreditCardReversal>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Return an amount to a swiped credit card
	public function credit_card_credit($payment){

		$this->reset_error();

		if(!isset($payment['market_code'])){
			$payment['market_code'] = self::MARKET_CODE_RETAIL;
		}

		if(!isset($payment['reference_number'])){
			$payment['reference_number'] = 0;
		}

		if(!isset($payment['ticket_number'])){
			$payment['ticket_number'] = 0;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Terminal' => $this->terminal,
			'Transaction' => array(
				'TransactionAmount' => number_format((float)  $payment['amount'], 2, '.', ''),
				'MarketCode' => $payment['market_code'],
				'ReferenceNumber' => $payment['reference_number'],
				'TicketNumber' => substr($payment['ticket_number'], -6, 6)
			)
		);

		if(isset($payment['payment_account_id'])){
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}

		if(isset($payment['encrypted_track'])){
			$request_body['Card']['MagneprintData'] = $payment['encrypted_track'];
		}

		$this->xml_request = $this->generate_xml($request_body, "<CreditCardCredit xmlns='".self::TRANSACTION_NAMESPACE."'></CreditCardCredit>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	public function credit_card_authorization($payment){

		$this->reset_error();

		if(!isset($payment['market_code'])){
			$payment['market_code'] = self::MARKET_CODE_RETAIL;
		}

		if(!isset($payment['reference_number'])){
			$payment['reference_number'] = 0;
		}

		if(!isset($payment['ticket_number'])){
			$payment['ticket_number'] = 0;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Terminal' => $this->terminal,
			'Transaction' => array(
				'TransactionAmount' => number_format((float)  $payment['amount'], 2, '.', ''),
				'MarketCode' => $payment['market_code'],
				'ReferenceNumber' => $payment['reference_number'],
				'TicketNumber' => substr($payment['ticket_number'], -6, 6)
			)
		);

		if(isset($payment['payment_account_id'])){
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}

		if(isset($payment['encrypted_track'])){
			$request_body['Card']['MagneprintData'] = $payment['encrypted_track'];
		}

		$this->xml_request = $this->generate_xml($request_body, "<CreditCardAuthorization xmlns='".self::TRANSACTION_NAMESPACE."'></CreditCardAuthorization>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	public function credit_card_authorization_completion($payment){

		$this->reset_error();

		if(!isset($payment['market_code'])){
			$payment['market_code'] = self::MARKET_CODE_RETAIL;
		}

		if(!isset($payment['reference_number'])){
			$payment['reference_number'] = 0;
		}

		if(!isset($payment['ticket_number'])){
			$payment['ticket_number'] = 0;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Terminal' => $this->terminal,
			'Transaction' => array(
				'TransactionAmount' => number_format((float)  $payment['amount'], 2, '.', ''),
				'MarketCode' => $payment['market_code'],
				'TransactionID' => $payment['transaction_id'],
				'ReferenceNumber' => $payment['reference_number'],
				'TicketNumber' => substr($payment['ticket_number'], -6, 6)
			)
		);

		if(isset($payment['payment_account_id'])){
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}

		if(isset($payment['encrypted_track'])){
			$request_body['Card']['MagneprintData'] = $payment['encrypted_track'];
		}

		$this->xml_request = $this->generate_xml($request_body, "<CreditCardAuthorizationCompletion xmlns='".self::TRANSACTION_NAMESPACE."'></CreditCardAuthorizationCompletion>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Do an amount adjustment on a credit card transaction already completed
	public function credit_card_adjustment($payment){

		$this->reset_error();

		if(!isset($payment['market_code'])){
			$payment['market_code'] = self::MARKET_CODE_RETAIL;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Terminal' => $this->terminal,
			'Transaction' => array(
				'TransactionAmount' => number_format((float)  $payment['amount'], 2, '.', ''),
				'MarketCode' => $payment['market_code'],
				'TransactionID' => $payment['transaction_id'],
				'TicketNumber' => substr($payment['ticket_number'], -6, 6),
				'CommercialCardCustomerCode' => 0
			)
		);

		$this->xml_request = $this->generate_xml($request_body, "<CreditCardAdjustment xmlns='".self::TRANSACTION_NAMESPACE."'></CreditCardAdjustment>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Standard swiped credit card sale
	public function credit_card_sale($payment){

		$this->reset_error();

		if(!isset($payment['market_code'])){
			$payment['market_code'] = self::MARKET_CODE_RETAIL;
		}

		if(!isset($payment['reference_number'])){
			$payment['reference_number'] = 0;
		}

		if(!isset($payment['ticket_number'])){
			$payment['ticket_number'] = 0;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Terminal' => $this->terminal,
			'Transaction' => array(
				'MarketCode' => $payment['market_code'],
				'TransactionAmount' => number_format((float)  $payment['amount'], 2, '.', ''),
				'ReferenceNumber' => $payment['reference_number'],
				'TicketNumber' => substr($payment['ticket_number'], -6, 6)
			)
		);

		if(isset($payment['payment_account_id'])){
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}

		if(isset($payment['encrypted_track'])){
			$request_body['Card']['MagneprintData'] = $payment['encrypted_track'];
		}

		$this->xml_request = $this->generate_xml($request_body, "<CreditCardSale xmlns='".self::TRANSACTION_NAMESPACE."'></CreditCardSale>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Saves a credit card (or bank account) for charging later on
	public function payment_account_create($payment){

		$this->reset_error();

		if(!isset($payment['payment_account_type'])){
			$payment['payment_account_type'] = 0;
		}

		if(!isset($payment['payment_account_reference_number'])){
			$this->error = true;
			$this->msg = 'PaymentAccountReferenceNumber required';
			return $this;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'PaymentAccount' => array(
				'PaymentAccountType' => (int) $payment['payment_account_type'],
				'PaymentAccountReferenceNumber' => $payment['payment_account_reference_number'],
				'PASSUpdaterBatchStatus' => 0,
				'PASSUpdaterOption' => 0
			)
		);

		if($payment['payment_account_type'] === 0){
			$request_body['Card'] = array(
				'MagneprintData' => $payment['encrypted_track']
			);
		}

		if($payment['payment_account_type'] === 3){
			$request_body['DemandDepositAccount'] = [];
			$request_body['DemandDepositAccount']['AccountNumber'] = $payment['account_number'];

			$request_body['DemandDepositAccount']['RoutingNumber'] = $payment['routing_number'];
		}

		$this->xml_request = $this->generate_xml($request_body, "<PaymentAccountCreate xmlns='".self::SERVICES_NAMESPACE."'></PaymentAccountCreate>");
		
		$response = $this->http->post($this->get_services_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Creates a new payment account based on a past transaction ID
	public function payment_account_create_with_trans_id($payment){

		$this->reset_error();

		if(!isset($payment['payment_account_type'])){
			$payment['payment_account_type'] = 0;
		}

		if(!isset($payment['payment_account_reference_number'])){
			$this->error = true;
			$this->msg = 'PaymentAccountReferenceNumber required';
			return $this;
		}

		if(!isset($payment['transaction_id'])){
			$this->error = true;
			$this->msg = 'TransactionID required';
			return $this;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'PaymentAccount' => array(
				'PaymentAccountType' => (int) $payment['payment_account_type'],
				'PaymentAccountReferenceNumber' => $payment['payment_account_reference_number'],
				'PASSUpdaterBatchStatus' => 0,
				'PASSUpdaterOption' => 0
			),
			'Transaction' => array(
				'TransactionID' => $payment['transaction_id']
			)
		);

		$this->xml_request = $this->generate_xml($request_body, "<PaymentAccountCreateWithTransID xmlns='".self::SERVICES_NAMESPACE."'></PaymentAccountCreateWithTransID>");

		$response = $this->http->post($this->get_services_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Updates a saved credit card
	public function payment_account_update($payment){

		$this->reset_error();

		if(!isset($payment['payment_account_type'])){
			$payment['payment_account_type'] = 0;
		}

		if(!isset($payment['payment_account_reference_number'])){
			$this->error = true;
			$this->msg = 'PaymentAccountReferenceNumber required';
			return $this;
		}

		if(!isset($payment['payment_account_id'])){
			$this->error = true;
			$this->msg = 'PaymentAccountID required';
			return $this;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'PaymentAccount' => array(
				'PaymentAccountType' => (int) $payment['payment_account_type'],
				'PaymentAccountReferenceNumber' => $payment['payment_account_reference_number'],
				'PaymentAccountID' => $payment['payment_account_id']
			),
			'Card' => array(
				'MagneprintData' => $payment['encrypted_track']
			)
		);

		$this->xml_request = $this->generate_xml($request_body, "<PaymentAccountUpdate xmlns='".self::SERVICES_NAMESPACE."'></PaymentAccountUpdate>");

		$response = $this->http->post($this->get_services_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	// Deletes a saved payment account
	public function payment_account_delete($payment_account_id = null){

		$this->reset_error();

		if(empty($payment_account_id)){
			$this->error = true;
			$this->msg = 'PaymentAccountID is required';
			return $this;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'PaymentAccount' => array(
				'PaymentAccountID' => $payment_account_id,
			)
		);

		$this->xml_request = $this->generate_xml($request_body, "<PaymentAccountDelete xmlns='".self::SERVICES_NAMESPACE."'></PaymentAccountDelete>");

		$response = $this->http->post($this->get_services_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	public function check_verification($payment)
	{
		$this->reset_error();

		if(!isset($payment['payment_account_type'])){
			$payment['payment_account_type'] = 3;
		}

		if(!isset($payment['transaction_amount'])){
			$payment['transaction_amount'] = 1.00;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Transaction' => array(
				'ReferenceNumber' => $payment['payment_account_reference_number'],
				'TransactionAmount' => $payment['transaction_amount'],
				'MarketCode' => (int) $payment['market_code']
			),
			'Terminal' => array(
				'TerminalID' => $this->terminal['TerminalID'],
				'LaneNumber' => 0
			),
			'DemandDepositAccount' => [
				'DDAAccountType'=>0
			]
		);

		if(!isset($payment['payment_account_id'])){

			if(!isset($payment['account_number'])){
				$this->error = true;
				$this->msg = 'AccountNumber required';
				return $this;
			}

			if(!isset($payment['Routing_number'])){
				$this->error = true;
				$this->msg = 'RoutingNumber required';
				return $this;
			}

			$request_body['DemandDepositAccount'] = ['DDAAccountType'=>0];
			$request_body['DemandDepositAccount']['AccountNumber'] = $payment['account_number'];

			$request_body['DemandDepositAccount']['RoutingNumber'] = $payment['routing_number'];
		}else{
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}

		$this->xml_request = $this->generate_xml($request_body, "<CheckVerification xmlns='".self::TRANSACTION_NAMESPACE."'></CheckVerification>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);
		return $this;
	}

	/*
	 * The Sale transaction is the most basic and most common transaction,
	 * used for the sale/purchase of goods or services.
	 */
	public function check_sale($payment)
	{

		if(!isset($payment['transaction_amount'])){
			$this->error = true;
			$this->msg = 'TransactionAmount required';
			return $this;
		}


		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Transaction' => array(
				'ReferenceNumber' => $payment['payment_account_reference_number'],
				'TransactionAmount' => $payment['transaction_amount'],
				'MarketCode' => (int) $payment['market_code']
			),
			'Terminal' => array(
				'TerminalID' => $this->terminal['TerminalID'],
				'LaneNumber' => 0
			),
			'DemandDepositAccount' => [
				'DDAAccountType'=>0
			]
		);

		if(!isset($payment['payment_account_id'])){

			if(!isset($payment['account_number'])){
				$this->error = true;
				$this->msg = 'AccountNumber required';
				return $this;
			}

			if(!isset($payment['Routing_number'])){
				$this->error = true;
				$this->msg = 'RoutingNumber required';
				return $this;
			}

			$request_body['DemandDepositAccount'] = ['DDAAccountType'=>0];
			$request_body['DemandDepositAccount']['AccountNumber'] = $payment['account_number'];

			$request_body['DemandDepositAccount']['RoutingNumber'] = $payment['routing_number'];
		}else{
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}


		$this->xml_request = $this->generate_xml($request_body, "<CheckSale xmlns='".self::TRANSACTION_NAMESPACE."'></CheckSale>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);

		return $this;
	}

	/*
	 * A CheckCredit transaction is used to refund a consumer for a previous transaction.
	 * A Return transaction is like a Credit transaction except it requires a TransactionID
	 * from the original dependent transaction.
	 */
	public function check_credit($payment)
	{
		if(!isset($payment['transaction_amount'])){
			$this->error = true;
			$this->msg = 'TransactionAmount required';
			return $this;
		}


		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Transaction' => array(
				'ReferenceNumber' => $payment['payment_account_reference_number'],
				'TransactionAmount' => $payment['transaction_amount'],
				'MarketCode' => (int) $payment['market_code']
			),
			'Terminal' => array(
				'TerminalID' => $this->terminal['TerminalID'],
				'LaneNumber' => 0
			),
			'DemandDepositAccount' => [
				'DDAAccountType'=>0
			]
		);

		if(!isset($payment['payment_account_id'])){

			if(!isset($payment['account_number'])){
				$this->error = true;
				$this->msg = 'AccountNumber required';
				return $this;
			}

			if(!isset($payment['Routing_number'])){
				$this->error = true;
				$this->msg = 'RoutingNumber required';
				return $this;
			}

			$request_body['DemandDepositAccount'] = ['DDAAccountType'=>0];
			$request_body['DemandDepositAccount']['AccountNumber'] = $payment['account_number'];

			$request_body['DemandDepositAccount']['RoutingNumber'] = $payment['routing_number'];
		}else{
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}


		$this->xml_request = $this->generate_xml($request_body, "<CheckCredit xmlns='".self::TRANSACTION_NAMESPACE."'></CheckCredit>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);

		return $this;
	}

	/*
	 * A CheckReturn transaction is used to refund a consumer for a previous transaction.
	 * A Return transaction requires a TransactionID from the original dependent transaction.
	 */
	public function check_return($payment)
	{
		if(!isset($payment['transaction_amount'])){
			$this->error = true;
			$this->msg = 'TransactionAmount required';
			return $this;
		}

		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Transaction' => array(
				'ReferenceNumber' => $payment['payment_account_reference_number'],
				'TransactionAmount' => $payment['transaction_amount'],
				'MarketCode' => (int) $payment['market_code'],
				'TransactionID' => $payment['transaction_id']
			),
			'Terminal' => array(
				'TerminalID' => $this->terminal['TerminalID'],
				'LaneNumber' => 0
			)
		);

		if(!isset($payment['payment_account_id'])){

			if(!isset($payment['account_number'])){
				$this->error = true;
				$this->msg = 'AccountNumber required';
				return $this;
			}

			if(!isset($payment['Routing_number'])){
				$this->error = true;
				$this->msg = 'RoutingNumber required';
				return $this;
			}

			$request_body['DemandDepositAccount'] = ['DDAAccountType'=>0];
			$request_body['DemandDepositAccount']['AccountNumber'] = $payment['account_number'];

			$request_body['DemandDepositAccount']['RoutingNumber'] = $payment['routing_number'];
		}else{
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}


		$this->xml_request = $this->generate_xml($request_body, "<CheckReturn xmlns='".self::TRANSACTION_NAMESPACE."'></CheckReturn>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);

		return $this;
	}

/*
 * A CheckVoidtransaction is used to completely back out a previous
 * Check Sale, Credit, or Return transaction.
 * This applies to all transactions that were entered into the current batch,
 * and only for the full dollar amount of the original transaction.
 */
	public function check_void($payment)
	{
		if(!isset($payment['transaction_amount'])){
			$this->error = true;
			$this->msg = 'TransactionAmount required';
			return $this;
		}


		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Transaction' => array(
				'ReferenceNumber' => $payment['payment_account_reference_number'],
				'TransactionAmount' => $payment['transaction_amount'],
				'MarketCode' => (int) $payment['market_code'],
				'TransactionID' => $payment['transaction_id']
			),
			'Terminal' => array(
				'TerminalID' => $this->terminal['TerminalID'],
				'LaneNumber' => 0
			)
		);

		if(!isset($payment['payment_account_id'])){

			if(!isset($payment['account_number'])){
				$this->error = true;
				$this->msg = 'AccountNumber required';
				return $this;
			}

			if(!isset($payment['Routing_number'])){
				$this->error = true;
				$this->msg = 'RoutingNumber required';
				return $this;
			}

			$request_body['DemandDepositAccount'] = ['DDAAccountType'=>0];
			$request_body['DemandDepositAccount']['AccountNumber'] = $payment['account_number'];

			$request_body['DemandDepositAccount']['RoutingNumber'] = $payment['routing_number'];
		}else{
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}


		$this->xml_request = $this->generate_xml($request_body, "<CheckVoid xmlns='".self::TRANSACTION_NAMESPACE."'></CheckVoid>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);

		return $this;
	}

/*
 * A Communications/System Reversal is generated when
 * there is a problem delivering the response back to the POS.
 * It is the transaction requestor’s responsibility to
 * generate a Reversal transaction for all transactions that do not receive responses.
 */
	public function check_reversal($payment)
	{
		if(!isset($payment['transaction_amount'])){
			$this->error = true;
			$this->msg = 'TransactionAmount required';
			return $this;
		}


		$request_body = array(
			'Credentials' => $this->credentials,
			'Application' => $this->application,
			'Transaction' => array(
				'ReferenceNumber' => $payment['payment_account_reference_number'],
				'TransactionAmount' => $payment['transaction_amount'],
				'TransactionID' => $payment['transaction_id'],
				'ReversalType' => $this::REVERSAL_TYPE_SYSTEM
			),
			'Terminal' => array(
				'TerminalID' => $this->terminal['TerminalID'],
				'LaneNumber' => 0
			)
		);

		if(!isset($payment['payment_account_id'])){

			if(!isset($payment['account_number'])){
				$this->error = true;
				$this->msg = 'AccountNumber required';
				return $this;
			}

			if(!isset($payment['Routing_number'])){
				$this->error = true;
				$this->msg = 'RoutingNumber required';
				return $this;
			}

			$request_body['DemandDepositAccount'] = ['DDAAccountType'=>0];
			$request_body['DemandDepositAccount']['AccountNumber'] = $payment['account_number'];

			$request_body['DemandDepositAccount']['RoutingNumber'] = $payment['routing_number'];
		}else{
			$request_body['PaymentAccount']['PaymentAccountID'] = $payment['payment_account_id'];
		}


		$this->xml_request = $this->generate_xml($request_body, "<CheckReversal xmlns='".self::TRANSACTION_NAMESPACE."'></CheckReversal>");

		$response = $this->http->post($this->get_transaction_url(), array(
			'headers' => $this->headers,
			'body' => $this->xml_request,
			'config' => array('curl' => $this->curl_config)
		));

		$this->parse_response($response);

		return $this;
	}
}
/* End of file Element_merchant.php */
