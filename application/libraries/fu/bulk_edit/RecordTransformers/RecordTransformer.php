<?php
namespace fu\bulk_edit\RecordTransformers;

abstract class RecordTransformer {

    public $Model = false;
    public $courseId = false;

    public abstract function transform($settings);
    public abstract function getError();
    public abstract function setRecordId($courseId);

    public function setCourseId($courseId){
        $this->courseId = (int) $courseId;
    }
}