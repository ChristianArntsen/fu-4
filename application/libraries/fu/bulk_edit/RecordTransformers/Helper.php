<?php
namespace fu\bulk_edit\RecordTransformers;

class Helper {

    static function listAdd($list, $uniqueKey, $newRecords){

        // Check if this record already exists, if so, do nothing
        foreach($list as $x => $record) {
            foreach($newRecords as $y => $newRecord){
                if(!empty($record[$uniqueKey]) && $record[$uniqueKey] == $newRecord[$uniqueKey]) {
                    unset($newRecords[$y]);
                }
            }
        }

        if(!empty($newRecords)){

            if(empty($list)){
                return $newRecords;
            }
            $list = array_merge($list, $newRecords);
        }

        return $list;
    }

    static function listRemove($list, $uniqueKey, $newRecords){

        // Check if list contains the record we are attempting to remove
        foreach($list as $x => $record){
            foreach($newRecords as $y => $newRecord){
                if(!empty($record[$uniqueKey]) && $record[$uniqueKey] == $newRecord[$uniqueKey]) {
                    unset($list[$x]);
                }
            }
        }

        return array_values($list);
    }
}