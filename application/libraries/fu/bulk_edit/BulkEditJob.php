<?php
namespace fu\bulk_edit;

use Carbon\Carbon;

class BulkEditJob {

    private $id;
    private $course_id;
    private $response = [];
    private $settings = [];
    private $record_ids = [];
    private $started_at = false;
    private $completed_at = false;
    private $total_duration = 0; // Duration the job took in seconds
    private $status = 'pending';
    private $type;
    private $percent_complete = 0;
    private $total_records = 0;
    private $records_completed = 0;
    private $records_failed = 0;

    private $errors = [];
    private $failed_record_ids = [];
    private $transformed_record_ids = [];

    private $transformer;
    private $progress_updater = false;

    public function __construct($job){

        if(!empty($job['id'])) $this->id = $job['id'];
        if(!empty($job['course_id'])) $this->course_id = $job['course_id'];
        if(!empty($job['type'])) $this->type = $job['type'];
        if(!empty($job['percent_complete'])) $this->percent_complete = (int) $job['percent_complete'];
        if(!empty($job['total_records'])) $this->total_records = (int) $job['total_records'];
        if(!empty($job['records_completed'])) $this->records_completed = (int) $job['records_completed'];
        if(!empty($job['records_failed'])) $this->records_failed = (int) $job['records_failed'];
        if(!empty($job['status'])) $this->status = $job['status'];
        if(!empty($job['settings'])) $this->settings = $job['settings'];
        if(!empty($job['record_ids'])) $this->record_ids = $job['record_ids'];

        $this->initialize();
    }

    private function initialize(){

        $transformer = false;
        switch($this->type){
            case 'customers':
                $transformer = new RecordTransformers\Customer();
                break;
        }
        $transformer->setCourseId($this->course_id);

        $this->set_transformer($transformer);
    }

    private function calculate_percent_complete(){
        if(($this->records_completed == 0 && $this->records_failed == 0) || $this->total_records == 0) return false;
        $percentage = floor((($this->records_completed + $this->records_failed) / $this->total_records) * 100);
        return $percentage;
    }

    private function set_percent_complete($percentage = 0){
        $this->percent_complete = (int) $percentage;
    }

    public function is_importing(){
        return $this->status == 'in-progress';
    }

    public function is_completed(){
        return $this->status == 'completed';
    }

    public function is_cancelled(){
        return $this->status == 'cancelled';
    }

    public function is_done(){
        return $this->status == 'cancelled' || $this->status == 'completed';
    }

    public function set_transformer(RecordTransformers\RecordTransformer $transformer){
        $this->transformer = $transformer;
    }

    public function update_status($status = 'pending'){

        switch($status){
            case 'in-progress':
                $this->started_at = Carbon::now();
            break;
            case 'completed':
            case 'cancelled':
                $this->completed_at = Carbon::now();
                $this->total_duration = (int) $this->started_at->diffInSeconds($this->completed_at);
            break;
        }

        $this->status = $status;
        return true;
    }

    public function transform(){

        $this->total_records = count($this->record_ids);
        $this->records_completed = 0;
        $this->records_failed = 0;

        $this->update_status('in-progress');

        // Loop through each record that needs transformed
        foreach($this->record_ids as $recordId){
            $this->transform_record($recordId);
        }

        $status = 'completed';
        if($this->status == 'cancelled'){
            $status = 'cancelled';
        }
        $this->update_status($status);
        $this->update_progress();
    }

    public function set_progress_callback($update_callback){
        $this->progress_updater = $update_callback;
    }

    private function update_progress(){

        if(!$this->progress_updater){
            return false;
        }
        $data = [
            'records_completed' => $this->records_completed,
            'records_failed' => $this->records_failed,
            'percent_complete' => $this->percent_complete,
            'status' => $this->status,
            'total_records' => $this->total_records,
            'total_duration' => $this->total_duration,
            'started_at' => null,
            'completed_at' => null
        ];

        if($this->started_at){
            $data['started_at'] = $this->started_at->toDateTimeString();
        }
        if($this->completed_at){
            $data['completed_at'] = $this->completed_at->toDateTimeString();
        }

        call_user_func($this->progress_updater, $data, $this);

        return true;
    }

    private function transform_record($recordId){

        $this->transformer->setRecordId($recordId);
        $success = $this->transformer->transform($this->settings);

        // Transform SUCCESS
        if($success){
            $this->records_completed++;
            $this->transformed_record_ids[] = (int) $recordId;

        // Transform FAILED
        }else{
            $this->records_failed++;
            if($this->transformer->getError()){
                $this->errors[(int) $recordId] = $this->transformer->getError();
            }
        }

        $this->set_percent_complete( $this->calculate_percent_complete() );
        $this->update_progress();

        return true;
    }

    public function to_array(){
        return [
            'records_completed' => $this->records_completed,
            'records_failed' => $this->records_failed,
            'percent_complete' => $this->percent_complete,
            'status' => $this->status,
            'total_records' => $this->total_records,
            'total_duration' => $this->total_duration,
            'response' => [
                'errors' => $this->errors
            ]
        ];
    }
}