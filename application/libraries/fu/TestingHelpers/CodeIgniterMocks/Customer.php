<?php
namespace fu\TestingHelpers\CodeIgniterMocks;

class Customer {

    public $error = false;

    public static $stubs = [
        1 => [
            'first_name' => 'John',
            'last_name' => 'Smith',
            'address_1' => '123 Test St.',
            'address_2' => '#500',
            'city' => 'Boomtown',
            'state' => 'ID',
            'country' => 'US',
            'zip' => '83401',
            'email' => 'testemail@mailinator.com',
            'phone_number' => '1231231234',
            'cell_phone_number' => '9991231234',
            'birthday' => '5/7/1970',
            'date_created' => '10/1/2013',
            'comments' => 'Just boring ole John Smith...',
            'status_flag' => 1,
            'account_number' => '0987654321',
            'price_class' => 10,
            'discount' => 0.00,
            'member' => 1,
            'member_account_balance' => -500,
            'member_account_balance_allow_negative' => 1,
            'member_account_limit' => -1000,
            'account_balance' => 0,
            'account_balance_allow_negative' => 0,
            'invoice_balance' => -1200.53,
            'invoice_email' => 'testemail@mailinator.com',
            'taxable' => 1,
            'require_food_minimum' => 1,
            'use_loyalty' => 1,
            'loyalty_points' => 1546,
            'username' => 'jsmith',
            'passes' => [
                [
                    'pass_id' => 100,
                    'Senior Pass'
                ],
                [
                    'pass_id' => 101,
                    'Test Pass'
                ]
            ],
            'groups' => [
                [
                    'group_id' => 50,
                    'name' => 'Seniors'
                ],
                [
                    'group_id' => 25,
                    'name' => 'Special Members'
                ]
            ],
            'minimum_charges' => [
                [
                    'minimum_charge_id' => 10,
                    'name' => 'Senior Minimum'
                ],
                [
                    'minimum_charge_id' => 12,
                    'name' => 'Another Minimum'
                ]
            ],
            'household_members' => [
                [
                    'household_id' => 1,
                    'person_id' => 2
                ]
            ]
        ],
        2 => [
            'first_name' => 'Jane',
            'last_name' => 'Doe',
            'address_1' => '333 Test St.',
            'address_2' => 'Suite 100',
            'city' => 'Cooltown',
            'state' => 'UT',
            'country' => 'US',
            'zip' => '90210',
            'email' => 'testemail123@mailinator.com',
            'phone_number' => '1112223333',
            'cell_phone_number' => '5556661111',
            'birthday' => '8/7/1975',
            'date_created' => '11/5/2014',
            'comments' => 'Who is Jane Doe?',
            'status_flag' => 1,
            'account_number' => '0987654322',
            'price_class' => 8,
            'discount' => 10.00,
            'member' => 1,
            'member_account_balance' => -436.70,
            'member_account_balance_allow_negative' => 1,
            'member_account_limit' => -500,
            'account_balance' => 50.75,
            'account_balance_allow_negative' => 0,
            'invoice_balance' => 0,
            'invoice_email' => 'testemail123@mailinator.com',
            'taxable' => 1,
            'require_food_minimum' => 0,
            'use_loyalty' => 1,
            'loyalty_points' => 12,
            'username' => 'jdoe',
            'passes' => [

            ],
            'groups' => [
                [
                    'group_id' => 60,
                    'name' => 'Girls only'
                ]
            ],
            'minimum_charges' => [
                [
                    'minimum_charge_id' => 11,
                    'name' => 'Regular Minimum'
                ],
                [
                    'minimum_charge_id' => 12,
                    'name' => 'Another Minimum'
                ]
            ],
            'household_members' => []
        ]
    ];

    public function get($params = null){

        if(!empty($params['person_id'])){
            if(!empty(self::$stubs[ (int) $params['person_id'] ])){
                return [self::$stubs[ (int) $params['person_id'] ]];
            }
            return false;
        }
    }

    public function save($person_id = null, $data){

        static $id = 1;
        $this->error = false;

        if(empty($person_id) && empty($data['first_name'])){
            $this->error = 'First name required';
            return false;
        }

        return $id++;
    }

    public function get_group_id($group_name = null){
        return 1;
    }
}