<?php
namespace fu\TestingHelpers\TestingObjects;

class Item extends TestingObject
{
	private $item = '{"item_id":"5098","item_type":"item","quantity":1,"unit_price":"30","discount_percent":0,"timeframe_id":null,"special_id":null,"punch_card_id":null,"selected":true,"params":{},"unit_price_includes_tax":false,"erange_size":"0"}';
	public function getObject()
	{
		return json_decode($this->item);
	}
	public function getArray()
	{
		return json_decode($this->item,true);
	}

}