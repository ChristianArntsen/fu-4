<?php
namespace fu\TestingHelpers\TestingObjects;

class SFItem extends TestingObject
{
	private $item = 'null';

	function __construct()
	{
		$this->item = json_encode(array(
			'course_id'=> 6270,
			'name' => "Test SF 1",
			'department' => 'Corporate',
			'category' => 'Fee',
			'subcategory' => null,
			'item_number' => null,
			'description' => 'some fee!',
			'image_id' => null,
			'cost_price' => 1.00,
			'unit_price' => 100.00,
			'unit_price_includes_tax' => 0,
			'inventory_level' =>0,
			'inventory_unlimited' => 1,
			'erange_size' => null, // what is this?
			'quickbooks_income' => null,
			'quickbooks_cogs' => null,
			'quickbooks_assets' => null,
			'max_discount' => 0,
			'quantity' => null,
			'is_unlimited' => 1,
			'reorder_level' => null,
			'food_and_beverage' => null,
			'number_of_sides' => null,
			'location' => null,
			'gl_code' => null,
			'allow_alt_description' => null,
			'is_serialized' => null,
			'invisible' => null,
			'deleted' => null,
			'is_side' => null,
			'soup_or_salad' => null,
			'add_on_price' => null,
			'print_priority' => 1,
			'kitchen_printer' => null,
			'do_not_print' => null,
			'inactive' => null,
			'meal_course_id' => null,
			'prompt_meal_course' => null,
			'do_not_print_customer_receipt' => null,
			'is_fee' => null, // not used
			'taxes' => array(
				array('name' => 'Sales Tax 1', 'percent' => 6.75, 'cumulative' => 0),
				array('name' => 'Sales Tax 2', 'percent' => 3.25, 'cumulative' => 0)
			),
			'supplier_id' => null,
			'is_shared' => null, // not used?
			'force_tax' => null,

			'item_type' => 'service_fee',
			'parent_item_percent' =>50,
			'whichever_is' => 'more',
			'is_service_fee' => 1
		)
	);
	}

	public function getObject()
	{
		return json_decode($this->item);
	}
	public function getArray()
	{
		return json_decode($this->item,true);
	}

}