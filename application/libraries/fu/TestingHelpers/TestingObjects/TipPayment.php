<?php
namespace fu\TestingHelpers\TestingObjects;

class TipPayment extends TestingObject
{
	private $payment = '{"approved":"false","card_type":"","card_number":"","cardholder_name":"","auth_amount":0,"auth_code":"","description":"Cash","tran_type":"","type":"cash","record_id":0,"amount":5.55,"tip_recipient":12,"params":null,"bypass":0,"is_auto_gratuity":0,"verified":false,"refund_reason":"","refund_comment":""}';
	public function getObject()
	{
		return json_decode($this->payment);
	}
	public function getArray()
	{
		return json_decode($this->payment,true);
	}

}

?>

