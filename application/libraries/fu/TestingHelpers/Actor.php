<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 8/25/2016
 * Time: 2:10 PM
 */

namespace fu\TestingHelpers;


class Actor
{
	public function login(){
		$CI = get_instance();
		//TODO: There should be a helper to easily login through tests and to accept a config file for credentials
		$CI->Employee->login("jackbarnes", "jackbarnes");
	}
	public function logout(){
		$CI = get_instance();
		//TODO: There should be a helper to easily login through tests and to accept a config file for credentials
		$CI->session->sess_destroy();
	}
}