<?php

namespace fu\TestingHelpers;


class Customer
{
    protected $person;
    protected $customer;

    protected function random_string($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    protected function random_email($length = 10) {
        return $this->random_string($length) . '@' . $this->random_string($length) . '.com';
    }

    public function __construct() {
        $CI = get_instance();

        $this->person = [
            'first_name' => $this->random_string(),
            'last_name' => $this->random_string(),
            'email' => $this->random_email(),
            'phone_number' => '1231231234'
        ];

        $this->customer = [
            'course_id' => $CI->session->userdata('course_id'),
            'username' => $this->random_email(),
            'password' => $this->random_string()
        ];
        $CI->Customer->save($this->person, $this->customer);
    }

    public function destroy() {
        $CI = get_instance();

        $CI->db->where('person_id', $this->person['person_id']);
        $CI->db->delete('users');

        $CI->db->where('customer_id', $this->customer['person_id']);
        $CI->db->delete('customers_loyalty_packages');

        $CI->db->where('person_id', $this->person['person_id']);
        $CI->db->delete('customers');

        $CI->db->where('person_id', $this->person['person_id']);
        $CI->db->delete('people');
    }

    public function get_id() {
        return $this->customer['person_id'];
    }

    public function get_use_loyalty() {
        return $this->customer['use_loyalty'];
    }
}