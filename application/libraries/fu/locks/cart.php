<?php

namespace fu\locks;


class cart extends lock
{
    public function __construct($fields)
    {
        parent::__construct($fields);
        $this->required_fields = ["cart_id"];
        $this->namespace = "cart";
    }

}