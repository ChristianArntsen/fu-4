<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 7/1/2016
 * Time: 4:23 PM
 */

namespace fu\locks;


use fu\Exceptions\LockException;

class lock
{
    protected $required_fields,$fields,$namespace;

    public function __construct($fields)
    {
        $this->CI = & get_instance();
        foreach($fields as $field=>$value)
        {
            $this->set_field($field,$value);
        }
    }

    public function set_field($field,$value)
    {
        $this->fields[$field] = $value;
    }

    public function get_lock()
    {
        $query = $this->CI->db->query("SELECT GET_LOCK('". $this->generate_name()."',10) AS lockstatus");
        $row = $query->row();
        if($row->lockstatus == 1){
            return true;
        } else {
            return false;
        }
    }

    public function release_lock()
    {
        $query = $this->CI->db->query("SELECT RELEASE_LOCK('". $this->generate_name()."') AS lockstatus");
        $row = $query->row();
        if($row->lockstatus == 1){
            return true;
        } else {
            return false;
        }
    }

    /**
     * @throws LockException
     */
    private function generate_name()
    {
        $lock_name = $this->namespace;
        foreach ($this->required_fields as $required_field) {
            if (empty($this->fields[$required_field])) {
                throw new LockException("Missing all the required fields for this lock.  Requires: " . implode(",", $this->required_fields) . " Contains: " . implode(",", array_keys($this->fields)));
            }
            $lock_name .= "_" . $this->fields[$required_field];
        }
        return $lock_name;
    }
}