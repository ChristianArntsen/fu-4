<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 7/1/2016
 * Time: 4:20 PM
 */

namespace fu\locks;


class teetime_slot extends lock
{
    public function __construct($fields)
    {
        parent::__construct($fields);
        $this->required_fields = ["teetime_start","teesheet_id"];
        $this->namespace = "teetime_slow";
    }

}