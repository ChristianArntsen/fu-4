<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 8/17/2015
 * Time: 2:38 PM
 */

namespace fu;


use Carbon\Carbon;
use fu\Teetime\Player;
use fu\Teetime\PlayerList;

class Teetime {

	private $onlyCustomers = true;
    /*
     * Example input "201400031831" = 2014-01-03 18:31
     * @param $teesheetTimestamp
     * @return \Carbon\Carbon
     */
    public static function convertTeetimeToDatetime($teesheetTimestamp)
    {
        $zeroBasedMonth = 1000000 ;
        $teesheetTimestamp += $zeroBasedMonth;
        if ($teesheetTimestamp%100 >59)
            $teesheetTimestamp += 40;

	    $teesheetTimestamp = floor($teesheetTimestamp);
	    $teesheetTimestamp = str_pad($teesheetTimestamp,12,"0");
	    try {
		    $date = \Carbon\Carbon::createFromFormat('YmdHi',$teesheetTimestamp);
		    return $date->toDateTimeString();
	    } catch(\InvalidArgumentException $e){
		    newrelic_notice_error("Problem converting tee time.",$e);
	    }

	    return Carbon::now()->toDateString();
    }

    public static function convertDatetimeToTeetime(Carbon $datetime)
    {
	    $zeroBasedMonth = 1000000 ;
		$teetime = $datetime->format('YmdHi');
		$teetime -= $zeroBasedMonth;

		return $teetime;
    }

    /*
     * Example input "1831" = 18:31
     * @param $openTime
     * @return \Carbon\Carbon
     */
    public static function convertOpenTimeToDatetime($teesheetTimestamp)
    {
	$date = \Carbon\Carbon::createFromFormat('YmdHi',floor($teesheetTimestamp));
        return $date;//->toDateTimeString();
    }


    /**
     * @param $teetime
     * @return Player[]
     */
    public function parseTeetimePlayers($teetime)
    {
    	if(is_array($teetime)){
    		$teetime = (object)$teetime;
	    }
        $playerList = [];
        for($i = 1;$i<=5;$i++)
        {
            $player_field = $i==1?"person_id":"person_id_$i";
            $player_name = $i==1?"person_name":"person_name_$i";
            if($this->onlyCustomers && (!isset($teetime->$player_field) || $teetime->$player_field ===0 || empty($teetime->$player_field))){
                continue;
            }

            if(!isset($teetime->{"price_class_".$i})){
	            $teetime->{"price_class_".$i} = 0;
            }
	        if(!isset($teetime->{"cart_paid_".$i})){
		        $teetime->{"cart_paid_".$i} = 0;
	        }
	        if(!isset($teetime->{"person_paid_".$i})){
		        $teetime->{"person_paid_".$i} = 0;
	        }
	        if(property_exists($teetime,$player_name) ){
		        $name = $teetime->$player_name;
	        } else {
		        $name = "NA";
	        }
            $player = new Player(
                $teetime->$player_field,
                $teetime->{"price_class_".$i},
                $teetime->{"cart_paid_".$i},
	            $name,
                $teetime->{"person_paid_".$i}
            );
	        $player->setPosition($i);

            $playerList[] = $player;
        }

        return $playerList;
    }


	/**
	 * @param Player[] $list
     */
    public function parseListToTeetime($list,$teetime)
    {
        for($i=1;$i<=5;$i++)
        {
            if(isset($list[$i-1])){
                $player = $list[$i-1];
                if($i == 1){
                    $teetime->person_id = $player->getPersonId();
                    $teetime->price_class_1 = $player->getPriceClass();
                    $teetime->person_paid_1 = $player->getPersonPaid();
                    $teetime->cart_paid_1 = $player->getCartPaid();
                    $teetime->person_name = $player->getPersonName();
                } else {
                    $teetime->{"person_id_".$i} = $player->getPersonId();
                    $teetime->{"price_class_".$i} = $player->getPriceClass();
                    $teetime->{"person_paid_".$i} = $player->getPersonPaid();
                    $teetime->{"cart_paid_".$i} = $player->getCartPaid();
                    $teetime->{"person_name_".$i} = $player->getPersonName();
                }
            } else {
                $teetime->{"person_id_".$i} = 0;
                $teetime->{"price_class_".$i} = 0;
                $teetime->{"person_paid_".$i} = 0;
                $teetime->{"cart_paid_".$i} =0;
                $teetime->{"person_name_".$i} =""  ;
            }

        }

        return $teetime;
    }

	/**
	 * @return bool
	 */
	public function isOnlyCustomers()
	{
		return $this->onlyCustomers;
	}

	/**
	 * @param bool $onlyCustomers
	 */
	public function setOnlyCustomers($onlyCustomers)
	{
		$this->onlyCustomers = $onlyCustomers;
	}


}
