<?php
namespace fu;

class Teesheet {
    private $teesheet;
    private $front_or_back;
    /**
     * @var \Carbon\Carbon
     */
    private $startDate;
    /**
     * @var \Carbon\Carbon
     */
    private $endDate;

    /**
     * @var \Carbon\Carbon
     */
    private $date;

    public function __construct($teesheet,$date)
    {
        $this->CI = & get_instance();
        $this->teesheet = $teesheet;
        $this->date = $date;
        $this->generateStartEndDateTime();

    }

    /**
     * @return \Carbon\Carbon
     */
    public function getPotentialTeetimes()
    {
        $potentialTimes = [];
        $incrementedTime = $this->startDate->copy();
        while($incrementedTime->diffInSeconds($this->endDate,false) > 0){
            $dateTimeString = $incrementedTime->format("Y-m-d H:i:00");


            $potentialTimes[$dateTimeString] = 0;
            $incrementedTime->addSeconds($this->teesheet->increment * 60);
        }
        $this->potentialTeetimes = $potentialTimes;
        return $potentialTimes;
    }

    public function getExistingTeetimes()
    {
        $this->CI->db->from("teetime")
            ->select([
                "teesheet_id",
                "start",
                "start_datetime",
                "end",
                "end_datetime",
                "player_count",
                "type",
                "side",
                "status",
                "holes",
                "person_name AS person_name_1",
				"person_name_2",
				"person_name_3",
				"person_name_4",
				"person_name_5",
				"person_id AS person_id_1",
				"person_id_2",
				"person_id_3",
				"person_id_4",
				"person_id_5"])
            ->where("start_datetime >=",$this->startDate->toDateTimeString())
            ->where("end_datetime <=",$this->endDate->toDateTimeString())
            ->where("teesheet_id =",$this->teesheet->teesheet_id)
            ->where("status != 'deleted'");
        if(isset($this->front_or_back)){
            //Adding this where statement messes with the index and makes it 20x slower
            //$this->CI->db->where("side =",$this->front_or_back);
        }
        $results = $this->CI->db->get()->result_array();
        foreach($results as $key=>&$row){
            if(isset($this->front_or_back) && isset($row['side']) && $row['side']!=$this->front_or_back){
                unset($results[$key]);
                continue;
            }
            $row['start_datetime'] = new \Carbon\Carbon($row['start_datetime']);
            $row['end_datetime'] = new \Carbon\Carbon($row['end_datetime']);
        }
        return $results;
    }



    /**
     * @return mixed
     */
    public function getFrontOrBack()
    {
        return $this->front_or_back;
    }

    /**
     * @param mixed $front_or_back
     */
    public function setFrontOrBack($front_or_back)
    {
        $this->front_or_back = $front_or_back;
    }

    private function generateStartEndDateTime()
    {
        $course_info = $this->CI->course->get_info($this->teesheet->course_id);
        $date_string = date('Ymd');
        $startTime = Teetime::convertOpenTimeToDatetime($date_string.$course_info->open_time);
        $startDate = $this->date->copy();
        $startDate->hour = $startTime->hour;
        $startDate->minute = $startTime->minute;
        $startDate->second = 0;

        $endTime = Teetime::convertOpenTimeToDatetime($date_string.$course_info->close_time);
        $endDate = $startDate->copy();
        $endDate->hour = $endTime->hour;
        $endDate->minute = $endTime->minute;

        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }
    public function seekNextPotentialTeetime($existingTimestamp)
    {
        //Find the next
        $existingTimestamp->addMinutes($this->teesheet->increment);
        if(isset($this->potentialTeetimes[$existingTimestamp->format("Y-m-d H:i:00")] )){
            return $existingTimestamp;
        }
        $existingTimestamp->addMinute();
        return $existingTimestamp;
    }

}