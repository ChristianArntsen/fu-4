<?php
namespace fu\passes\Rule;

class Condition_factory {
	
	public static function create($field, $data, $pass){
		
		$condition = false;

		switch($field){
			
			case 'teesheet':
				$condition = new Condition\Teesheet($data, $pass);
			break;
			
			case 'timeframe':
				$condition = new Condition\Date_time($data, $pass);
			break;
			
			case 'total_used_per_day':
			case 'total_used_per_week':
			case 'total_used_per_month':
			case 'total_used_per_year':
			case 'total_used':
				$condition = new Condition\Total_used($data, $pass);
			break;
			
			case 'days_since_purchase':
				$condition = new Condition\Days_since_purchase($data, $pass);
			break;

			default:
				return false;
		}

		return $condition;
	}
}