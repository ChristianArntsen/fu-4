<?php
namespace fu\passes\Rule\Condition\Parameter;

class Price_class extends Parameter {
	
	const FIELD = 'price_class_id';
	private $price_class_id = false;

	public function __construct($data = []){
		
		if(empty($data)){
			return false;
		}
		$this->price_class_id = (int) $data['id'];
	}

	public function isMatch($use){

		if(!isset($use['price_class_id'])){
			return false;
		}

		return ((int) $use['price_class_id'] == (int) $this->price_class_id);
	}
}