<?php
namespace fu\passes\Rule\Condition\Parameter;

class Timeframe extends Parameter {
	
	const FIELD = 'timeframe';
	private $day_of_week = [];
	private $time = false;
	private $date = false;

	public function __construct($timeframe){
		
		if(empty($timeframe)){
			return false;
		}

		if(!empty($timeframe['time'])){
			$this->time = $timeframe['time'];
		}
		if(!empty($timeframe['date'])){
			$this->date = $timeframe['date'];
		}
		if(!empty($timeframe['day_of_week'])){
			$this->day_of_week = $timeframe['day_of_week'];
		}			
	}

	public function isMatch($use){

		if(empty($use['date'])){
			return false;
		}
		$dateTime = \Carbon\Carbon::parse($use['date']);

		return (
			$this->dayIsValid($dateTime) &&
			$this->timeIsValid($dateTime) &&
			$this->dateIsValid($dateTime) 
		);
	}

	private function timeIsValid(\Carbon\Carbon $dateTime){
		
		if($this->time === false){
			return true;
		}
		$time = (int) $dateTime->format('Hi');

		$startTime = (int) $this->time[0];
		$endTime = (int) $this->time[1];

		return ($time >= $startTime && $time < $endTime);
	}

	private function dayIsValid(\Carbon\Carbon $dateTime){
	
		return (in_array($dateTime->dayOfWeek, $this->day_of_week));
	}

	private function dateIsValid(\Carbon\Carbon $dateTime){
		
		$startDate = false;
		if(!empty($this->date[0])){
			$startDate = \Carbon\Carbon::parse($this->date[0]);
		}
		
		$endDate = false;
		if(!empty($this->date[1])){
			$endDate = \Carbon\Carbon::parse($this->date[1]);
		}

		if(empty($startDate) && empty($endDate)){
			return true;
		}

		if(!$dateTime->between($startDate, $endDate)){
			return false;
		}

		return true;	
	}
}