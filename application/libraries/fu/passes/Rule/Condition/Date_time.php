<?php
namespace fu\passes\Rule\Condition;

class Date_time extends Condition {
	
	public function isTrue(){

		if(!$this->pass->getCurrentUse()){
			$use = [
				'date' => \Carbon\Carbon::now()->toDateTimeString()
			];
		}else{
			$use = $this->pass->getCurrentUse();
		}
		
		if(empty($this->value)){
			return false;
		}

		foreach($this->value as $value){
			if($value->isMatch($use)){
				return true;
			}
		}

		return false;
	}
}