<?php
namespace fu\passes\Rule\Condition;
use Carbon\Carbon;

class Total_used extends Condition {

	private $period = false;
	private $periods = [
		'day',
		'week',
		'month',
		'year'
	];

	public function isTrue(){

		switch($this->field){
			case 'total_used_per_day':
				$this->setPeriod('day');
			break;
			case 'total_used_per_week':
				$this->setPeriod('week');
			break;
			case 'total_used_per_month':
				$this->setPeriod('month');
			break;
			case 'total_used_per_year':
				$this->setPeriod('year');
			break;
			default:
				$this->setPeriod(false);			
		}

		$total = $this->getTotal();
		return self::compare($total, $this->operator, $this->value);
	}

	// If transactions need to be counted in a window (eg. per day, week etc)
	public function setPeriod($period = false){

		if(!$period || !in_array($period, $this->periods)){
			return false;
		}
		$this->period = $period;
		return $this;
	}

	private function getTotal(){

		$uses = $this->getFilteredUses();
		$total = 0;

		if(empty($uses)){
			return $total;
		}

		foreach($uses as $use){
			if(!isset($use['count'])){
				$qty = 1;
			}else{
				$qty = $use['count'];
			}
			$total += $qty;
		}
		
		return $total;
	}

	protected function getFilteredUses(){

		$passUses = $this->pass->getUses();
		if(empty($passUses)){
			return $passUses;
		}
		
		$filterGroups = [];
		$filteredUses = [];
		
		if(!empty($this->filter)){

			// Group filters by field
			foreach($this->filter as $filter){
				$filterGroups[$filter::FIELD][] = $filter;
			}

			foreach($passUses as $use){
				if($this->filtersMatch($filterGroups, $use)){
					$filteredUses[] = $use;
				}
			}			
		
		}else{
			$filteredUses = $passUses;
		}

		// Apply further filtering to transactions by "period" (if applicable)
		$filteredUses = $this->filterByPeriod($filteredUses);

		return $filteredUses;
	}

	protected function filtersMatch($filterGroups, $use){

		// Loop through each filter group
		foreach($filterGroups as $field => $filters){
			
			$fieldMatched = false;
			if(empty($filters)){
				continue;
			}

			// If at least 1 filter matches, the group passes
			foreach($filters as $filter){
				if($filter->isMatch($use)){
					$fieldMatched = true;
					break;
				}				
			}

			// If one of the groups failed to pass, the transaction is filtered out
			if($fieldMatched === false){
				return false;
			}
		}

		return true;
	}

	protected function filterByPeriod($uses){

		if(empty($uses) || !$this->period){
			return $uses;
		}
		$periodStart = Carbon::now();
		$periodEnd = Carbon::now();

		switch($this->period){
			
			case 'day':
				$periodStart->startOfDay();
				$periodEnd->endOfDay();
			break;
			case 'week':
				$periodStart->startOfWeek();
				$periodEnd->endOfWeek();
			break;
			case 'month':
				$periodStart->startOfMonth();
				$periodEnd->endOfMonth();
			break;
			case 'year':
				$periodStart->startOfYear();
				$periodEnd->endOfYear();
			break;
		}

		$filteredUses = [];
		foreach($uses as $use){
			
			if(empty($use['date'])){
				continue;
			}

			$useDate = Carbon::parse($use['date']);
			if($useDate->between($periodStart, $periodEnd)){
				$filteredUses[] = $use;
			}
		}

		return $filteredUses;		
	}
}