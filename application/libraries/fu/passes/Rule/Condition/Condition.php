<?php
namespace fu\passes\Rule\Condition;

abstract class Condition {
	
	protected $field = false;
	protected $operator = '=';
	protected $value = false;
	protected $filter = false;

	public function __construct($conditionData, $pass){
		
		if(!empty($conditionData['field'])){
			$this->field = $conditionData['field'];
		}

		if(isset($conditionData['operator'])){
			$this->operator = $conditionData['operator'];
		}
		
		if(!empty($pass)){
			$this->setPass($pass);
		}

		if(!empty($conditionData['value'])){
			if(!is_array($conditionData['value'])){
				$this->addValue($conditionData['value']);
			}else{
				foreach($conditionData['value'] as $value){
					$this->addValue($value);
				}				
			}
		}

		if(!empty($conditionData['filter'])){
			foreach($conditionData['filter'] as $filter){
				$this->addFilter($filter, $filter['field']);
			}
		}
	}

	public static function compare($value, $operator, $result){

		switch($operator){
			
			case '<':
				return ($value < $result);
			break;
			case '<=':
				return ($value <= $result);
			break;
			case '=':
				return ($value == $result);
			break;
			case '>=':
				return ($value >= $result);
			break;
			case '>':
				return ($value > $result);
			break;
		}

		return true;
	}

	public function setPass($pass){
		
		$this->pass = $pass;
		return $this;
	}

	public function addValue($data){
		
		$value = Parameter_factory::create($this->field, $data);
		if(!$value){
			return $this;
		}
		
		if($value instanceof Parameter\Parameter){
			$this->value[] = $value;
		}else{
			$this->value = $value;
		}
		
		return $this;
	}

	public function addFilter($data, $field){
		
		$filter = Parameter_factory::create($field, $data);
		if(!$filter){
			return $this;
		}

		$this->filter[] = $filter;
		return $this;
	}

	public function setOperator($operator){
		
		$this->operator = $operator;
		return $this;
	}

	// Set the data we will be testing to see if it passes our condition
	public function setVariable($data){
		
		$this->variable = $data;
		return $this;
	}

	abstract public function isTrue();
}