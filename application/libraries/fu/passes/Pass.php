<?php
namespace fu\passes;

class Pass {
	
	private $rules = [];
	private $defaultPriceClassId = false;
	private $passId;
	private $validationError;
	private $uses = [];
	private $currentUse = [];
	private $purchaseDate = null;
	private $expirationDate = null;
	private $startDate = null;

	public function __construct($passData = []){
		
		if(!empty($passData['pass_id'])){
			$this->setPassId((int) $passData['pass_id']);
		}

		if(!empty($passData['restrictions'])){
			foreach($passData['restrictions'] as $rule){
				$this->addRule($rule);
			}
		}

		if(!empty($passData['uses'])){
			$this->setUses($passData['uses']);
		}

		if(!empty($passData['expiration_date'])){
			$this->setExpirationDate($passData['expiration_date']);
		}else if(!empty($passData['end_date'])){
			$this->setExpirationDate($passData['end_date']);
		}

		if(!empty($passData['start_date'])){
			$this->setStartDate($passData['start_date']);
		}

		if(!empty($passData['date_created'])){
			$this->setPurchaseDate($passData['date_created']);
		}

		if(!empty($passData['default_price_class_id'])){
			$this->setDefaultPriceClassId($passData['default_price_class_id']);
			
			$this->addRule([
				'name' => 'Default',
				'price_class_id' => $this->defaultPriceClassId,
				'rule_number' => $this->getNextRuleNumber(),
			]);
		}		
	}

	private function getNextRuleNumber(){
		
		$rule_number = 0;
		if(empty($this->rules)){
			return $rule_number;
		}

		foreach($this->rules as $rule){
			if($rule->getOrder() > $rule_number){
				$rule_number = $rule->getOrder();
			}
		}

		return $rule_number + 1;
	}

	public function setDefaultPriceClassId($priceClassId){
		
		$this->defaultPriceClassId = (int) $priceClassId;
		return $this;
	}

	public function setPurchaseDate($date){

		$this->purchaseDate = \Carbon\Carbon::parse($date);
		return $this;
	}

	public function getPurchaseDate(){

		return $this->purchaseDate;
	}

	public function setUses($uses){

		$this->uses = $uses;
		return $this;
	}

	public function getUses(){

		return $this->uses;
	}

	public function setCurrentUse($use){

		$this->currentUse = $use;
		return $this;
	}

	public function getCurrentUse(){

		return $this->currentUse;
	}

	public function setExpirationDate($expiration){

		$this->expirationDate = \Carbon\Carbon::parse($expiration);
		return $this;
	}

	public function setStartDate($start){

		$this->startDate = \Carbon\Carbon::parse($start);
		return $this;
	}

	public function isExpired(){
		
		if(empty($this->expirationDate)){
			return false;
		}

		if($this->expirationDate->isPast()){
			return true;
		}
		return false;
	}

	public function isBeforeStartDate(){
		
		if(empty($this->startDate)){
			return false;
		}

		if($this->startDate->isFuture()){
			return true;
		}
		return false;
	}

	public function addRule($ruleData){
		
		$this->rules[] = new Rule($ruleData, $this);
		return $this;
	}

	public function getRuleValidity(){
		
		$this->sortRules();

		$rules = [];
		/** @var Rule $rule */
		foreach($this->rules as $rule){
			
			$ruleOutput = [
				'rule_number' => $rule->getOrder(),
				'name' => $rule->getName(),
				'is_valid' => $rule->isValid(),
				'price_class_id' => $rule->getPriceClassId(),
				'is_applied' => $rule->getApplied(),
				'type'=>$rule->getType(),
				'items'=>$rule->getItems(),
				"uniq_id"=>$rule->getUniqId()
			];

			$rules[] = $ruleOutput;
		}

		return $rules;
	}
		
	public function setApplied($ruleNumber = false){
		
		foreach($this->rules as $rule){
			if($rule->isValid() && ($rule->getOrder() === (int) $ruleNumber || $ruleNumber === false)){
				$rule->setApplied(true);
				break;
			}
		}
	}

	// The pass is valid if it has at least 1 valid rule
	public function isValid(){
		
		foreach($this->rules as $rule){
			if($rule->isValid()){
				return true;
			}
		}
	}

	public function setPassId($passId){
		
		$this->passId = $passId;
		return $this;
	}

	public function getPassId(){
		
		return $this->passId;
	}
	
	private function sortRules(){
		
		if(empty($this->rules)){
			return $this;
		}

		usort($this->rules, function($ruleA, $ruleB){
			if($ruleA->getOrder() == $ruleB->getOrder()){ 
				return 0; 
			}
			if($ruleA->getOrder() > $ruleB->getOrder()){
				return 1;
			}else{
				return -1;
			}
		});

		return $this;
	}
}