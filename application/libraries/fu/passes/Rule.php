<?php
namespace fu\passes;

class Rule {
	
	private $order = 0;
	private $passId = false;
	private $invalidReason = '';	
	private $conditions = [];
	private $priceClassId = false;
	private $pass = false;
	private $name = '';
	private $type = 'teetimes';
	private $items = [];
	private $isApplied = false;
	private $uniqId = null;

	public function __construct($ruleData = [], $pass = false){
		
		if(!empty($ruleData['price_class_id'])){
			$this->setPriceClassId($ruleData['price_class_id']);
		}
		if(!empty($ruleData['uniq_id'])){
			$this->uniqId = $ruleData['uniq_id'];
		}

		if(isset($ruleData['rule_number'])){
			$this->order = (int) $ruleData['rule_number'];
		}

		if(isset($ruleData['name'])){
			$this->name = $ruleData['name'];
		}
		
		if(!empty($ruleData['conditions'])){
			foreach($ruleData['conditions'] as $condition){
				$this->addCondition($condition, $pass);
			}
		}

		if(!empty($pass)){
			$this->setPass($pass);
		}

		if(isset($ruleData['type'])){
			$this->setType($ruleData['type']);
		}
		if(isset($ruleData['items'])){
			$this->setItems($ruleData['items']);
		}
	}

	public function isValid(){

		if(empty($this->conditions)){
			return true;
		}

		$passValid = (!$this->pass->isBeforeStartDate() && !$this->pass->isExpired());
		if(!$passValid){
			return false;
		}

		// All conditions must be TRUE for the rule to be valid
		foreach($this->conditions as $condition){
			if(!$condition->isTrue()){
				return false;
			}
		}

		return true;
	}

	public function setPass($pass){
		
		$this->pass = $pass;
		foreach($this->conditions as $condition){
			$condition->setPass($pass);
		}
		return $this;
	}

	public function setApplied($isApplied){
		
		$this->isApplied = (bool) $isApplied;
		return $this;
	}

	public function getApplied(){
		
		return $this->isApplied;
	}

	public function getPriceClassId(){
		
		return $this->priceClassId;
	}

	public function setPriceClassId($priceClassId){
		
		return $this->priceClassId = (int) $priceClassId;
	}

	public function addCondition($condition = false, $pass){
		
		if(empty($condition)){
			return $this;
		}
		$this->conditions[] = Rule\Condition_factory::create($condition['field'], $condition, $pass);
		return $this;
	}

	public function getInvalidReason(){
		
		return $this->invalidReason;
	}

	public function getOrder(){

		return $this->order;
	}

	public function getName(){

		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return array
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @param array $items
	 */
	public function setItems($items)
	{
		$this->items = $items;
	}

	/**
	 * @return mixed|null
	 */
	public function getUniqId()
	{
		return $this->uniqId;
	}

	/**
	 * @param mixed|null $uniqId
	 */
	public function setUniqId($uniqId)
	{
		$this->uniqId = $uniqId;
	}


}