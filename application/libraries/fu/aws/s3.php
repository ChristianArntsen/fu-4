<?php
namespace fu\aws;
use Aws\S3\S3Client;


class s3 {
    
    private $s3,$bucket;
    private $is_stream = false;

    public function __construct($s3Config = null)
    {
    	if(!isset($s3Config)) {
		    $this->CI = &get_instance();
		    $s3Config = $this->CI->config->item('s3');
	    }
        // Instantiate the S3 client using your credential profile
        $this->client = new S3Client([
        'region'  => 'us-west-2',
        'credentials' => [
            'key'    => $s3Config["access_key"],
            'secret' => $s3Config["secret"],
        ],
        'version' => '2006-03-01'
    ]);


        $this->fileBucket = $s3Config['buckets']['files'];
        $this->invoiceBucket = $s3Config['buckets']['invoices'];
        $this->marketingBucket = $s3Config['buckets']['marketing'];
        $this->signatureBucket = $s3Config['buckets']['signatures'];
        $this->reportsBucket = $s3Config['buckets']['reports'];
        $this->dataImportBucket = $s3Config['buckets']['data_import'];
        $this->accountStatementsBucket = $s3Config['buckets']['account_statements'];

    }

    public function set_is_stream($val){
        $this->is_stream = (bool) $val;
    }

    public function uploadToFileBucket($source,$dest)
    {
        $this->transfer($source,$this->fileBucket,$dest);
    }
    public function uploadToInvoiceBucket($source,$dest)
    {
        $this->transfer($source,$this->invoiceBucket,$dest);
    }
    public function uploadToDataImportBucket($source,$dest)
    {
        return $this->transfer($source,$this->dataImportBucket,$dest);
    }
    public function uploadToMarketingBucket($source,$dest)
    {
        $this->transfer($source,$this->marketingBucket,$dest);
    }
    public function uploadToSignatureBucket($source,$dest)
    {
        $this->transfer($source,$this->signatureBucket,$dest);
    }
	public function uploadToReportBucket($source,$dest)
	{
		$this->transfer($source,$this->reportsBucket,$dest);
		return $dest;
	}
	public function uploadToAccountStatementsBucket($source,$dest,$public = false)
	{
		return $this->transfer($source,$this->accountStatementsBucket,$dest,$public);

	}

	public function downloadAccountStatements($s3_directory,$target_directory,$options = [])
	{
		return $this->client->downloadBucket($target_directory,$this->accountStatementsBucket,$s3_directory,$options);
	}

    public function getObject($bucket,$keyname)
    {
        return $this->client->getObject(array(
            'Bucket' => $bucket,
            'Key'    => $keyname
        ));

    }

    public function transfer($source,$bucket,$key,$public = false)
    {
        $params = array(
            'Bucket'     => $bucket,
            'Key'        => $key
        );

        if($this->is_stream){
            $params['Body'] = $source;
        }else{
            $params['SourceFile'] = $source;
        }

        if($public){
        	$params['ACL'] = 'public-read';
        }

        $result = $this->client->putObject($params);
        return $result->get("ObjectURL");
    }
}