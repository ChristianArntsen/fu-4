<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 3/19/2015
 * Time: 12:25 PM
 */

namespace fu\reports\ReportFilters;



class Teetime extends Date {

     private $zeroBasedMonth = 1000000 ;

    public function __construct($filter,$custom_values = null){
        parent::__construct($filter,$custom_values);


    }

    public function getFilterObjects()
    {
        $startFilter = clone $this;
        $endFilter = clone $this;
        $startFilter->value = $this->convertDateToTeetime($this->startValue);
        $startFilter->operator = ">=";
        $endFilter->value =  $this->convertDateToTeetime($this->endValue);
        $endFilter->operator = "<";
        return [$startFilter,$endFilter];
    }

    private function convertDateToTeetime($date)
    {
        $date = new \Carbon\Carbon($date);
        $date = $date->format("YmdHi")-$this->zeroBasedMonth;
        return $date;
    }

}