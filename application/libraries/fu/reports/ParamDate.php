<?php

namespace fu\reports;

use Carbon\Carbon;
use fu\Exceptions\InvalidArgument;
use fu\reports\MovingRanges\Yesterday;

class ParamDate {

    private $column,$grouping,$type,$start,$end,$currentOrLast,$movingRange;


    public function __construct($date)
    {
        $date = (array)$date;
        $this->column = isset($date['column']) ? $date['column']:null;
        $this->type = isset($date['type']) ? $date['type']:null;
        $this->start = isset($date['start_value']) ? $date['start_value']:null;
        $this->end = isset($date['end_value']) ? $date['end_value']:null;
        $this->typeOfMovingRange = isset($date['typeOfMovingRange']) ? $date['typeOfMovingRange']:"days";
        $this->movingRange = isset($date['movingRange']) ? $date['movingRange']:"yesterday";
	    if($this->type == "moving"){
		    $this->setRangeFromDynamicType();
	    }
    }

    public function setRangeFromDynamicType(){
		//If it's current, go back number of days,
		//If it's last, get
		if($this->typeOfMovingRange == "label"){
			$date = MovingRangeFactory::create($this->movingRange);
		} else if($this->typeOfMovingRange == "days"){
			$date = new \fu\reports\MovingRanges\NumberOfDays($this->movingRange);
		} else if($this->typeOfMovingRange == "months"){
            $date = new \fu\reports\MovingRanges\MonthsAgo($this->movingRange);
        }else {
			throw new InvalidArgument("Unknown dynamic type: ".$this->typeOfMovingRange);
		}

        $this->start = $date->getStartTimeStamp();
        $this->end = $date->getEndTimeStamp();
	}

	/**
	 * @return mixed
	 */
	public function getTypeOfMovingRange()
	{
		return $this->typeOfMovingRange;
	}

	/**
	 * @param mixed $currentOrLast
	 */
	public function setTypeOfMovingRange($currentOrLast)
	{
		$this->typeOfMovingRange = $currentOrLast;
	}

	/**
	 * @return mixed
	 */
	public function getMovingRange()
	{
		return $this->movingRange;
	}

	/**
	 * @param mixed $movingRange
	 */
	public function setMovingRange($movingRange)
	{
		$this->movingRange = $movingRange;
	}
    /**
     * @return null
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @param null $column
     */
    public function setColumn($column)
    {
        $this->column = $column;
    }

    /**
     * @return null
     */
    public function getGrouping()
    {
        return $this->grouping;
    }

    /**
     * @param null $grouping
     */
    public function setGrouping($grouping)
    {
        $aggregates = new Aggregations();
        $possibleGroupings = $aggregates->getDateAggregations();
        if(!in_array($grouping,$possibleGroupings)){
            throw new InvalidArgument("The date grouping, $grouping, doesn't exist");
        }
        $this->grouping = $grouping;
    }

    /**
     * @return null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param null $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return null
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param null $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return null
     */
    public function getEnd()
    {
        if(isset($this->end)){
            $endOfDay = new \Carbon\Carbon($this->end);
            return $endOfDay->endOfDay()->toDateTimeString();
        }
        return $this->end;
    }

    /**
     * @param null $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    public function getColumnWGrouping()
    {
        if(isset($this->grouping)){
            return $grouping = $this->getGrouping()."(".$this->getColumn().")";

        }else
            return $this->column;
    }
}