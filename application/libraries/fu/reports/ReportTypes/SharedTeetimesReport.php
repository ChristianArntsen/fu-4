<?php
namespace fu\reports\ReportTypes;

class SharedTeetimesReport extends SalesReport {
    public function __construct($reportParameters){

        parent::__construct($reportParameters);
	    $this->baseTable = "foreup_sales_items";
	    $course_model = new \Course();
	    $course_ids = array();
	    $course_model->get_linked_course_ids($course_ids, 'shared_customers');
	    $this->defaultFilters = [
		    "teetime.deleted"=>
			    new \fu\reports\ReportFilters\Filter([
				    "column"=>"teetime.status",
				    "operator"=>"!=",
				    "value"=>"deleted",
				    "boolean"=>"AND",
			    ]),
		    "teetime.reround"=>
			    new \fu\reports\ReportFilters\Filter([
				    "column"=>"teetime.reround",
				    "operator"=>"=",
				    "value"=>"0",
				    "boolean"=>"AND",
			    ]),
		    "type"=>
			    new \fu\reports\ReportFilters\Filter([
				    "column"=>"type",
				    "operator"=>"=",
				    "value"=>"green_fee",
				    "boolean"=>"AND",
			    ]),
		    "security"=>
			    new \fu\reports\ReportFilters\Filter([
				    "column"=>"sales.course_id",
				    "operator"=>"=",
				    "value"=>$course_ids,
				    "boolean"=>"AND",
			    ])
	    ];
    }


	protected function knownJoins(){
		return [
			"employees"=>[
				"table"=>"people as employees",
				"where"=> $this->baseTable.".employee_id = employees.person_id",
				"type"=>"INNER JOIN"
			],
			"customer_groups"=>[
				"table"=>"customer_groups as customer_groups",
				"where"=>"customer_group_members.group_id = customer_groups.group_id",
				"type"=>"LEFT",
				"requires"=>"customer_group_members",
			],
			"customer_group_members"=>[
				"table"=>"customer_group_members as customer_group_members",
				"where"=>"customers.person_id = customer_group_members.person_id",
				"type"=>"LEFT",
				"requires"=>"customers",
			],
			"suppliers"=>[
				"table"=>"suppliers as suppliers",
				"where"=> $this->baseTable.".supplier_id = suppliers.person_id",
				"type"=>"LEFT"
			],
			"sales_comps"=>[
				"table"=>"sales_comps as sales_comps",
				"where"=> $this->baseTable.".sale_id = sales_comps.sale_id AND ".$this->baseTable.".item_id = sales_comps.item_id AND ".$this->baseTable.".line = sales_comps.line",
				"type"=>"LEFT"
			],
			"sales_items_price_classes"=>[
				"table"=>"price_classes as sales_items_price_classes",
				"where"=>$this->baseTable.".price_class_id = sales_items_price_classes.class_id",
				"type"=>"LEFT"
			],
			"customers"=>[
				"table"=>"people as customers",
				"where"=>"sales.customer_id = customers.person_id",
				"type"=>"LEFT",
				"requires"=>"sales"
			],
			"customer_table"=>[
				"table"=>"customers as customer_table",
				"where"=>"customers.person_id = customer_table.person_id",
				"type"=>"LEFT",
				"requires"=>"customers",
			],
			"price_classes"=>[
				"table"=>"price_classes as price_classes",
				"where"=>"customer_table.price_class = price_classes.class_id",
				"type"=>"LEFT",
				"requires"=>"customer_table",
			],
			"items"=>[
				"table"=>"items as items",
				"where"=>$this->baseTable.".item_id = items.item_id",
				"type"=>"LEFT"
			],
			"item_kits"=>[
				"table"=>"item_kits as item_kits",
				"where"=>$this->baseTable.".item_kit_id = item_kits.item_kit_id",
				"type"=>"LEFT"
			],
			"payments"=>[
				"table"=>"sales_payments as payments",
				"where"=>$this->baseTable.".sale_id = payments.sale_id",
				"type"=>"LEFT"
			],
			"tip_recipients"=>[
				"table"=>"people as tip_recipients",
				"where"=>"payments.tip_recipient = tip_recipients.person_id",
				"type"=>"left",
				"requires"=>"payments",
			],
			"terminals"=>[
				"table"=>"terminals as terminals",
				"where"=>$this->baseTable.".terminal_id = terminals.terminal_id",
				"type"=>"LEFT"
			],
			"sales_payments"=>[
				"table"=>"sales_payments as sales_payments",
				"where"=>$this->baseTable.".sale_id = sales_payments.sale_id",
				"type"=>"LEFT"
			],
			"punch_card_items"=>[
				"table"=>"punch_card_items as punch_card_items",
				"where"=>"punch_card_items.item_id = items.item_id",
				"type"=>"left",
				"requires"=>"items",
			],
			"punch_cards"=>[
				"table"=>"punch_cards as punch_cards",
				"where"=>"punch_card_items.punch_card_id = punch_cards.punch_card_id",
				"type"=>"left",
				"requires"=>"punch_card_items",
			],
			"sales"=>[
				"table"=>"sales as sales",
				"where"=>"sales.sale_id = ".$this->baseTable.".sale_id",
				"type"=>"left"
			],
			"teetime"=>[
				"table"=>"teetime as teetime",
				"where"=>"teetime.TTID = sales.teetime_id",
				"type"=>"left",
				"requires"=>"sales",
			],
			"teesheet"=>[
				"table"=>"teesheet as teesheet",
				"where"=>"teetime.teesheet_id = teesheet.teesheet_id",
				"type"=>"left",
				"requires"=>"teetime",
			],
			"item_taxes"=>[
				"table"=>"sales_items_taxes as item_taxes",
				"where"=>$this->baseTable.".sale_id = item_taxes.sale_id AND ".$this->baseTable.".item_id = item_taxes.item_id",
				"type"=>"left"
			],
			"invoices"=>[
				"table"=>"invoices as invoices",
				"where"=>$this->baseTable.".sale_id = invoices.sale_id",
				"type"=>"left"
			],
			"weather"=>[
				"table"=>"weather as weather",
				"where"=>
					"items.course_id = weather.course_id AND ".
					"DATE(teetime.start_datetime) = weather.day ",
				"type"=>"left",
				"requires"=>"items"
			]
		];
	}

	protected function createBaseTable()
	{
		return;
	}
}