<?php
/*
 * Not finished
 */
namespace fu\reports\ReportTypes;
class CustomersReport extends Report{
    protected $baseTable = "foreup_customers" ;
	protected $prefix = "foreup";
    protected $customerColumn = "people.person_id";

    public function __construct($reportParameters){
        $this->defaultFilters = [
            "deleted" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"deleted",
                    "operator"=>"=",
                    "value"=>"0",
                    "boolean"=>"AND",
                ]),
            "hide_from_search" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"hide_from_search",
                    "operator"=>"=",
                    "value"=>"0",
                    "boolean"=>"AND",
                ])
        ];
        parent::__construct($reportParameters);
    }
    protected function knownJoins(){
	    $course_model = new \Course();
	    $course_ids = array();
	    $course_model->get_linked_course_ids($course_ids, 'shared_customers');


        return [
            "people"=>[
                "table"=>"people as people",
                "where"=> $this->baseTable.".person_id = people.person_id",
                "type"=>"LEFT"
            ],
            "price_class"=>[
                "table"=>"price_classes as price_class",
                "where"=> $this->baseTable.".price_class = price_class.class_id",
                "type"=>"LEFT"
            ],
            "customer_minimum_charges"=>[
                "table"=>"customer_minimum_charges as customer_minimum_charges",
                "where"=>$this->baseTable.".person_id = customer_minimum_charges.person_id",
                "type"=>"LEFT"
            ],
            "minimum_charges"=>[
                "table"=>"minimum_charges as minimum_charges",
                "where"=>"customer_minimum_charges.minimum_charge_id = minimum_charges.minimum_charge_id",
                "type"=>"LEFT",
                "requires"=>"customer_minimum_charges"
            ],
            "account_transactions"=>[
                "table"=>"account_transactions as account_transactions",
                "where"=>$this->baseTable.".person_id = account_transactions.trans_customer AND customer_minimum_charges.minimum_charge_id = account_transactions.minimum_charge_id AND account_transactions.trans_description LIKE '%Spent%'",
                "type"=>"LEFT",
                "requires"=>"customer_minimum_charges"
            ],
	        "all_account_transactions"=>[
		        "table"=>"account_transactions as all_account_transactions",
		        "where"=>$this->baseTable.".person_id = all_account_transactions.trans_customer AND all_account_transactions.course_id IN (".implode(',', $course_ids).") ",
		        "type"=>"LEFT",
		        "requires"=>"customer_minimum_charges"
	        ],
            "passes"=>[
                "table"=>"passes as passes",
                "where"=>"passes.customer_id = {$this->baseTable}.person_id AND passes.deleted = 0 AND passes.course_id IN (".implode(',', $course_ids).")",
                "type"=>"LEFT"
            ],
            "passes_item"=>[
                "table"=>"items as passes_item",
                "where"=>"passes.pass_item_id = passes_item.item_id",
                "type"=>"LEFT",
                "requires"=>"passes"
            ],
            "customer_group_members"=>[
                "table"=>"customer_group_members as customer_group_members",
                "where"=>$this->baseTable.".person_id = customer_group_members.person_id",
                "type"=>"LEFT"
            ],
            "customer_groups"=>[
                "table"=>"customer_groups as customer_groups",
                "where"=>"{$this->baseTable}.course_id = customer_groups.course_id AND customer_group_members.group_id = customer_groups.group_id",
                "type"=>"LEFT",
                "requires"=>"customer_group_members"
            ],
            "courses"=>[
                "table"=>"courses as courses",
                "where"=>$this->baseTable.".course_id = courses.course_id",
                "type"=>"LEFT"
            ]
        ];
    }


    protected $knownColumns = [
        "foreup_customers"=>[
            "username",
            "account_number",
            "account_balance",
            "account_limit",
            "member_account_balance",
            "member_account_limit",
	        "opt_out_email",
            "taxable",
            "deleted",
            "hide_from_search",
	        "person_id"
        ],
        "courses"=>[
            "name"
        ],
        "customer_groups"=>[
            "label"
        ],
        "people"=>[
            "first_name",
            "last_name",
            "phone_number",
            "cell_phone_number",
            "email",
            "birthday",
            "address_1",
            "city",
            "state",
            "zip",
            "country",
            "comments",
            "person_id",
        ],
        "customer_minimum_charges"=>[
            "minimum_charge_id"
        ],
        "minimum_charges"=>[
            "minimum_charge_id",
            "name",
            "last_ran"
        ],
        "account_transactions"=>[
            "account_type",
            "trans_comment",
            "trans_date",
            "trans_description",
            "trans_amount",
            "running_balance",
            "hide_on_invoices",
            "minimum_charge_id"
        ],
	    "all_account_transactions"=>[
		    "account_type",
		    "trans_comment",
		    "trans_date",
		    "trans_description",
		    "trans_amount",
		    "running_balance",
		    "hide_on_invoices",
		    "minimum_charge_id"
	    ],
        "passes"=>[
            "pass_id",
            "start_date",
            "end_date",
            "is_valid",
            "deleted",
            "course_id",
            "date_created"
        ],
        "passes_item"=>[
            "name",
            "department",
            "category",
            "unit_price"
        ],
        "price_class"=>[
            "name",
            "cart",
            "color",
            "date_created",
            "default",
            "cart_gl_code",
            "green_gl_code"
        ]
    ];

    protected function addSecurity()
    {
        $course_model = new \Course();
        $course_ids = array();
        $course_model->get_linked_course_ids($course_ids, 'shared_customers');
        $this->db->where_in("customers.course_id",$course_ids);
    }

}