<?php
namespace fu\reports\ReportTypes;

class RaincheckReport extends Report{
    protected $baseTable = "foreup_rainchecks" ;
	protected $prefix = "foreup";
    protected $customerColumn = "customers.person_id";

    public function __construct($reportParameters){

        $this->CI = & get_instance();
        $this->defaultFilters = [
            "deleted" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"deleted",
                    "operator"=>"=",
                    "value"=>"0",
                    "boolean"=>"AND",
                ]),
            "course_id" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"teesheet.course_id",
                    "operator"=>"=",
                    "value"=>$this->CI->session->userdata('course_id'),
                    "boolean"=>"AND",
                ])
        ];
        parent::__construct($reportParameters);
    }

    protected function knownJoins(){
        return [
            "employees"=>[
                "table"=>"people as employees",
                "where"=> $this->baseTable.".employee_id = employees.person_id",
                "type"=>"INNER JOIN"
            ],
            "customers"=>[
                "table"=>"people as customers",
                "where"=>$this->baseTable.".customer_id = customers.person_id",
                "type"=>"LEFT"
            ],
            "teesheet"=>[
                "table"=>"teesheet as teesheet",
                "where"=>$this->baseTable.".teesheet_id = teesheet.teesheet_id",
                "type"=>"LEFT"
            ],
            "teetime"=>[
                "table"=>"teetime as teetime",
                "where"=>$this->baseTable.".teetime_id = teetime.TTID",
                "type"=>"LEFT"
            ]
        ];
    }


    protected $knownColumns = [
            "foreup_rainchecks"=>[
                "raincheck_id",
                "raincheck_number",
                "teesheet_id",
                "teetime_id",
                "teetime_date",
                "teetime_time",
                "date_issued",
                "date_redeemed",
                "players",
                "holes_completed",
                "customer_id",
                "employee_id",
                "green_fee",
                "cart_fee",
                "tax",
                "total",
                "expiry_date",
                "deleted"
            ],
            "employees"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
            "customers"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
            "teetime"=>[
                "type",
                "start",
                "start_datetime",
                "end",
                "end_datetime",
                "allDay",
                "title",
                "details",
                "status",
            ],
            "teesheet"=>[
                "course_id",
                "title",
                "holes"
            ]
    ];


    protected function addSecurity()
    {
    }

}