<?php
namespace fu\reports\ReportTypes;
class ItemsReport extends Report{
    protected $baseTable = "foreup_items" ;
	protected $prefix = "foreup";
    protected function knownJoins(){
        return [
            "taxes"=>[
                "table"=>"items_taxes as taxes",
                "where"=> $this->baseTable.".item_id= taxes.item_id",
                "type"=>"INNER JOIN"
            ],
            "upcs"=>[
                "table"=>"items_upcs as upcs",
                "where"=> $this->baseTable.".item_id = upcs.item_id",
                "type"=>"INNER JOIN"
            ],
            "inventory"=>[
                "table"=>"inventory as inventory",
                "where"=> $this->baseTable.".item_id = inventory.trans_items",
                "type"=>"LEFT JOIN"
            ],
	        "suppliers"=>[
		        "table"=>"suppliers as suppliers",
		        "where"=> $this->baseTable.".supplier_id = suppliers.person_id",
		        "type"=>"LEFT"
	        ]
        ];
    }
    protected function addSecurity()
    {
	    $this->db->where("items.course_id",$this->CI->session->userdata('course_id'));
    }

    protected $knownColumns = [
        "foreup_items"=>[
                "course_id",
                "CID",
                "name",
                "department",
                "category",
                "subcategory",
                "supplier_id",
                "item_number",
                "description",
                "image_id",
                "cost_price",
                "unit_price",
                "unit_price_includes_tax",
                "max_discount",
                "quantity",
                "is_unlimited",
                "reorder_level",
                "location",
                "item_id",
                "gl_code",
                "allow_alt_description",
                "is_serialized",
                "is_giftcard",
                "invisible",
                "deleted",
                "food_and_beverage",
                "soup_or_salad",
                "number_of_sides",
                "is_side",
                "add_on_price",
                "print_priority",
                "kitchen_printer",
                "quickbooks_income",
                "quickbooks_cogs",
                "quickbooks_assets",
                "erange_size",
                "is_fee"
            ],
        "inventory"=>[
            "CID",
            "trans_id",
            "course_id",
            "trans_items",
            "trans_users",
            "trans_date",
            "trans_comment",
            "trans_inventory",
            "sale_id",
            "suspended_sale_id"
        ],
        "taxes"=>[
            "name",
            "percent",
            "cumulative",
        ],
        "upcs"=>[
            "upc",
        ],
	    "suppliers"=>[
		    "company_name",
		    "fax_number",
		    "account_number"
	    ]
    ];



}