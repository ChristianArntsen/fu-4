<?php
/*
 * Not finished
 */
namespace fu\reports\ReportTypes;
class InvoicesReport extends Report{
    protected $baseTable = "foreup_invoices" ;
	protected $prefix = "foreup";
    protected $customerColumn = "customers.person_id";

    public function __construct($reportParameters){
        $this->defaultFilters = [
            "customers.deleted" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"customers.deleted",
                    "operator"=>"=",
                    "value"=> 0,
                    "boolean"=>"AND",
                ]),
            "deleted" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"deleted",
                    "operator"=>"=",
                    "value"=> 0,
                    "boolean"=>"AND",
                ])
        ];
        parent::__construct($reportParameters);
    }

    protected function knownJoins(){
        return [
            "customers"=>[
                "table"=>"customers as customers",
                "where"=>$this->baseTable.".person_id = customers.person_id",
                "type"=>"LEFT"
            ],
            "people"=>[
                "table"=>"people as people",
                "where"=> $this->baseTable.".person_id = people.person_id",
                "type"=>"LEFT JOIN"
            ]
        ];
    }
    protected $knownColumns = [
        "foreup_invoices"=>[
            "invoice_id",
            "invoice_number",
            "name",
            "bill_start",
            "bill_end",
            "date",
            "total",
            "overdue_total",
            "previous_payments",
            "paid",
            "overdue",
            "deleted",
            "last_billing_Attempt",
            "email_invoice",
            "send_date",
            "due_date",
            "auto_bill_date",
            "deleted"
        ],
        "people"=>[
            "first_name",
            "last_name",
            "phone_number",
            "cell_phone_number",
            "email",
            "birthday",
            "address_1",
            "city",
            "state",
            "zip",
            "country",
            "comments",
            "person_id"
        ],
        "customers"=>[
            "first_name",
            "last_name",
            "phone_number",
            "cell_phone_number",
            "email",
            "birthday",
            "address_1",
            "city",
            "state",
            "zip",
            "country",
            "comments",
            "person_id",
            "deleted"
        ]
    ];

    protected function addSecurity()
    {
        $this->db->where("foreup_invoices.course_id",$this->CI->session->userdata('course_id'));
    }

}