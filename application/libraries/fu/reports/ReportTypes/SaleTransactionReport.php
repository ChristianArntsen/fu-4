<?php
namespace fu\reports\ReportTypes;

class SaleTransactionReport extends Report{
    protected $baseTable = "foreup_sales" ;
	protected $prefix = "foreup";
    protected $customerColumn = "customer_id";

    public function __construct($reportParameters){
        $this->defaultFilters = [
            "deleted" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"deleted",
                    "operator"=>"=",
                    "value"=>"0",
                    "boolean"=>"AND",
                ]),
            "deleted" =>
                new \fu\reports\ReportFilters\Filter([
	                "column"=>"deleted",
	                "operator"=>"=",
	                "value"=>"0",
	                "boolean"=>"AND",
                ])
        ];
        parent::__construct($reportParameters);
    }

    protected function knownJoins(){
        return [
            "employees"=>[
                "table"=>"people as employees",
                "where"=> $this->baseTable.".employee_id = employees.person_id",
                "type"=>"LEFT"
            ],
            "customers"=>[
                "table"=>"people as customers",
                "where"=>$this->baseTable.".customer_id = customers.person_id",
                "type"=>"LEFT"
            ],
            "payments"=>[
                "table"=>"sales_payments as payments",
                "where"=>$this->baseTable.".sale_id = payments.sale_id",
                "type"=>"LEFT"
            ],
	        "sales_payments_credit_cards"=>[
		        "table"=>"sales_payments_credit_cards as sales_payments_credit_cards",
		        "where"=>"payments.invoice_id = sales_payments_credit_cards.invoice",
		        "type"=>"LEFT",
		        "requires"=>"payments",
	        ],
            "tip_recipients"=>[
                "table"=>"people as tip_recipients",
                "where"=>"payments.tip_recipient = tip_recipients.person_id",
                "type"=>"LEFT",
                "requires"=>"payments",
            ],
	        "terminals"=>[
		        "table"=>"terminals as terminals",
		        "where"=>$this->baseTable.".terminal_id = terminals.terminal_id",
		        "type"=>"LEFT"
	        ]
        ];
    }


    protected $knownColumns = [
            "employees"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
            "customers"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
		    "payments"=>[
			    "type",
			    "payment_type",
			    "reports_only",
			    "payment_amount",
			    "tip_recipient",
			    "is_tip"
		    ],
            "tip_recipients"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id"
            ],
	        "foreup_sales"=>[
	            "teetime_id",
	            "guest_count",
	            "table_id",
		        "sale_time",
		        "sale_id",
		        "number",
		        "deleted",
		        "employee_id",
		        "course_id",
		        "refund_reason",
		        "refund_comment"
	        ],
		    "terminals"=>[
			    "label"
		    ],
	        "sales_payments_credit_cards"=>[
	        	"tran_type",
	        	"amount",
	        	"auth_amount",
	        	"card_type",
	        	"masked_account",
	        	"cardholder_name",
	        	"ref_no",
	        	"trans_post_time",
	        	"frequency",
	        	"status",
	        	"status_message",
	        	"display_message"
	        ]
    ];

	protected function addSecurity()
	{


		if($this->sharedReporting){
			$course_model = new \Course();
			$course_ids = array();
			$course_model->get_linked_course_ids($course_ids, 'shared_customers');
			$this->db->where_in("foreup_sales.course_id",$course_ids);
		} else {
			$this->db->where("foreup_sales.course_id",$this->CI->session->userdata('course_id'),false);
		}
	}

}