<?php
namespace fu\reports\ReportTypes;
class Loyalty extends Report{

	public function __construct($reportParameters)
	{
		parent::__construct($reportParameters);

		$course_model = new \Course();
		$course_ids = array();
		$course_model->get_linked_course_ids($course_ids, 'shared_tee_sheet');


		$this->defaultFilters = [
			"course_ids" =>
				new \fu\reports\ReportFilters\Filter([
					"column"=>"sales.course_id",
					"operator"=>"=",
					"value"=>$course_ids,
					"boolean"=>"AND",
				])
		];
	}

	protected $baseTable = "foreup_loyalty_transactions" ;
	protected $prefix = "foreup";
    protected function knownJoins(){
        return [
	        "customers"=>[
		        "table"=>"people as customers",
		        "where"=>$this->baseTable.".trans_customer = customers.person_id",
		        "type"=>"LEFT"
	        ],
	        "transaction_course"=>[
		        "table"=>"courses as transaction_course",
		        "where"=>"sales.course_id = transaction_course.course_id",
		        "type"=>"LEFT",
		        "requires"=>"sales"
	        ],
	        "customers_table"=>[
		        "table"=>"customers as customers_table",
		        "where"=>"customers.person_id = customers_table.person_id",
		        "type"=>"LEFT",
		        "requires"=>"customers",
	        ],
	        "sales"=>[
		        "table"=>"sales as sales",
		        "where"=>$this->baseTable.".sale_id = sales.sale_id",
		        "type"=>"LEFT",
		        "requires"=>"customers",
	        ],
	        "courses"=>[
		        "table"=>"courses as courses",
		        "where"=>"customers_table.course_id = courses.course_id",
		        "type"=>"LEFT",
		        "requires"=>"customers_table"
	        ]
        ];
    }


    protected $knownColumns = [
    	"sales"=>[
    		"sale_id",
		    "customer_id",
		    "course_id"
	    ],
        "foreup_loyalty_transactions"=>[
            "trans_id",
            "course_id",
            "sale_id",
            "trans_customer",
            "trans_user",
            "trans_date",
            "trans_comment",
            "trans_description",
            "trans_amount"
        ],
	    "transaction_course"=>[
	        "name"
	    ],
	    "courses"=>[
		    "name"
	    ],
	    "customers"=>[
		    "first_name",
		    "last_name",
		    "phone_number",
		    "cell_phone_number",
		    "email",
		    "birthday",
		    "address_1",
		    "city",
		    "state",
		    "zip",
		    "country",
		    "comments",
		    "person_id",
	    ],
	    "customers_table"=>[
		    "customer_id",
		    "CID",
		    "person_id",
		    "course_id",
		    "api_id",
		    "username",
		    "member",
		    "price_class",
		    "image_id",
		    "account_number",
		    "account_balance",
		    "account_balance_allow_negative",
		    "account_limit",
		    "member_account_balance",
		    "member_account_balance_allow_negative",
		    "member_account_limit",
		    "invoice_balance",
		    "invoice_balance_allow_negative",
		    "invoice_email",
		    "use_loyalty",
		    "loyalty_points",
		    "company_name",
		    "taxable",
		    "discount",
		    "deleted",
		    "opt_out_email",
		    "opt_out_text",
		    "unsubscribe_all",
		    "course_news_announcements",
		    "teetime_reminders",
		    "status_flag",
		    "hide_from_search",
		    "date_created"
	    ]
    ];



}