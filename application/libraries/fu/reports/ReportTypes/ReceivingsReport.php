<?php
namespace fu\reports\ReportTypes;
class ReceivingsReport extends Report{
    protected $baseTable = "receivings_items" ;
	protected $prefix = "foreup";
    protected function knownJoins(){
        return [
            "receivings"=>[
                "table"=>"receivings as receivings",
                "where"=> $this->baseTable.".receiving_id= receivings.receiving_id",
                "type"=>"INNER JOIN"
            ],
            "items"=>[
                "table"=>"items as items",
                "where"=> $this->baseTable.".item_id = items.item_id",
                "type"=>"INNER JOIN"
            ]
        ];
    }


    protected $knownColumns = [
        "receivings_items"=>[
            "receiving_id",
            "item_id",
            "serialnumber",
            "line",
            "quantity_purchased",
            "item_cost_price",
            "item_unit_price",
            "discount_purchase",
        ],
        "receivings"=>[
            "course_id",
            "receiving_time",
            "supplier_id",
            "employee_id",
            "comment",
            "receiving_id",
            "payment_type",
            "shipping_cost",
            "reference",
            "invoice_number",
            "deleted",
        ],
        "items"=>[
            "course_id",
            "CID",
            "name",
            "department",
            "category",
            "subcategory",
            "supplier_id",
            "item_number",
            "description",
            "image_id",
            "cost_price",
            "unit_price",
            "unit_price_includes_tax",
            "max_discount",
            "quantity",
            "is_unlimited",
            "reorder_level",
            "location",
            "item_id",
            "gl_code",
            "allow_alt_description",
            "is_serialized",
            "is_giftcard",
            "invisible",
            "deleted",
            "food_and_beverage",
            "soup_or_salad",
            "number_of_sides",
            "is_side",
            "add_on_price",
            "print_priority",
            "kitchen_printer",
            "quickbooks_income",
            "quickbooks_cogs",
            "quickbooks_assets",
            "erange_size",
            "is_fee"
        ]
    ];



}