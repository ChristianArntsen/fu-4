<?php
namespace fu\reports\ReportTypes;

use \fu\reports\TemporaryTables\SalesTable;
class SalesReport extends Report{
    protected $baseTable = "foreup_sales_items_temp" ;
	protected $prefix = "foreup";
    protected $customerColumn = "customer_id";

    public function __construct($reportParameters){
        $this->defaultFilters = [
            "deleted" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"deleted",
                    "operator"=>"=",
                    "value"=>"0",
                    "boolean"=>"AND",
                ]),
            "invoices"=>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"invoices.sale_id",
                    "operator"=>" IS ",
                    "value"=>"NULL",
                    "boolean"=>"AND",
                ])
        ];
        parent::__construct($reportParameters);
    }

    protected function knownJoins(){
        return [
            "employees"=>[
                "table"=>"people as employees",
                "where"=> $this->baseTable.".employee_id = employees.person_id",
                "type"=>"LEFT"
            ],
            "customer_groups"=>[
                "table"=>"customer_groups as customer_groups",
                "where"=>"customer_group_members.group_id = customer_groups.group_id",
                "type"=>"LEFT",
                "requires"=>"customer_group_members",
            ],
            "customer_group_members"=>[
                "table"=>"customer_group_members as customer_group_members",
                "where"=>"customers.person_id = customer_group_members.person_id",
                "type"=>"LEFT",
                "requires"=>"customers",
            ],
            "sales_items"=>[
                "table"=>"sales_items as sales_items",
                "where"=> $this->baseTable.".sale_id = sales_items.sale_id AND ".$this->baseTable.".item_id = sales_items.item_id AND ".$this->baseTable.".line = sales_items.line",
                "type"=>"LEFT"
            ],
            "suppliers"=>[
                "table"=>"suppliers as suppliers",
                "where"=> $this->baseTable.".supplier_id = suppliers.person_id",
                "type"=>"LEFT"
            ],
            "sales_comps"=>[
                "table"=>"sales_comps as sales_comps",
                "where"=> $this->baseTable.".sale_id = sales_comps.sale_id AND ".$this->baseTable.".item_id = sales_comps.item_id AND ".$this->baseTable.".line = sales_comps.line",
                "type"=>"LEFT"
            ],
	        "sales_comps_employee"=>[
		        "table"=>"people as sales_comps_employee",
		        "where"=> "sales_comps.employee_id = sales_comps_employee.person_id ",
		        "type"=>"LEFT",
		        "requires"=>"sales_comps"
	        ],
            "sales_items_price_classes"=>[
                "table"=>"price_classes as sales_items_price_classes",
                "where"=>"sales_items.price_class_id = sales_items_price_classes.class_id",
                "type"=>"LEFT",
                "requires"=>"sales_items",
            ],
            "customers"=>[
                "table"=>"people as customers",
                "where"=>$this->baseTable.".customer_id = customers.person_id",
                "type"=>"LEFT"
            ],
            "customer_table"=>[
                "table"=>"customers as customer_table",
                "where"=>"customers.person_id = customer_table.person_id",
                "type"=>"LEFT",
                "requires"=>"customers",
            ],
            "price_classes"=>[
                "table"=>"price_classes as price_classes",
                "where"=>"customer_table.price_class = price_classes.class_id",
                "type"=>"LEFT",
                "requires"=>"customer_table",
            ],
            "items"=>[
                "table"=>"items as items",
                "where"=>$this->baseTable.".item_id = items.item_id",
                "type"=>"LEFT"
            ],
            "item_kits"=>[
                "table"=>"item_kits as item_kits",
                "where"=>$this->baseTable.".item_kit_id = item_kits.item_kit_id",
                "type"=>"LEFT"
            ],
            "payments"=>[
                "table"=>"sales_payments as payments",
                "where"=>$this->baseTable.".sale_id = payments.sale_id",
                "type"=>"LEFT"
            ],
            "tip_recipients"=>[
                "table"=>"people as tip_recipients",
                "where"=>"payments.tip_recipient = tip_recipients.person_id",
                "type"=>"left",
                "requires"=>"payments",
            ],
	        "terminals"=>[
		        "table"=>"terminals as terminals",
		        "where"=>$this->baseTable.".terminal_id = terminals.terminal_id",
		        "type"=>"LEFT"
	        ],
            "sales_payments"=>[
                "table"=>"sales_payments as sales_payments",
                "where"=>$this->baseTable.".sale_id = sales_payments.sale_id",
                "type"=>"LEFT"
            ],
            "punch_card_items"=>[
                "table"=>"punch_card_items as punch_card_items",
                "where"=>"punch_card_items.item_id = items.item_id",
                "type"=>"left",
                "requires"=>"items",
            ],
            "punch_cards"=>[
                "table"=>"punch_cards as punch_cards",
                "where"=>"punch_card_items.punch_card_id = punch_cards.punch_card_id",
                "type"=>"left",
                "requires"=>"punch_card_items",
            ],
            "sales"=>[
                "table"=>"sales as sales",
                "where"=>"sales.sale_id = ".$this->baseTable.".sale_id",
                "type"=>"left"
            ],
            "teetime"=>[
                "table"=>"teetime as teetime",
                "where"=>"teetime.TTID = sales.teetime_id",
                "type"=>"left",
                "requires"=>"sales",
            ],
            "teesheet"=>[
                "table"=>"teesheet as teesheet",
                "where"=>"teetime.teesheet_id = teesheet.teesheet_id",
                "type"=>"left",
                "requires"=>"teetime",
            ],
            "item_taxes"=>[
                "table"=>"sales_items_taxes as item_taxes",
                "where"=>$this->baseTable.".sale_id = item_taxes.sale_id AND ".$this->baseTable.".item_id = item_taxes.item_id",
                "type"=>"left"
            ],
            "invoices"=>[
                "table"=>"invoices as invoices",
                "where"=>$this->baseTable.".sale_id = invoices.sale_id",
                "type"=>"left"
            ],
            "weather"=>[
                "table"=>"weather as weather",
                "where"=>
                    "items.course_id = weather.course_id AND ".
                    "DATE(teetime.start_datetime) = weather.day ",
                "type"=>"left",
                "requires"=>"items"
            ]
        ];
    }


    protected $knownColumns = [
        "foreup_sales_items_temp"=>[
                "deleted",
                "sale_date",
                "serialnumber",
                "description",
                "teetime_id",
                "terminal_id",
                "sale_id",
                "line",
                "timeframe_id",
                "comment",
                "payment_type",
                "customer_id",
                "employee_id",
                "item_id",
                "item_kit_id",
                "invoice_id",
                "supplier_id",
                "quantity_purchased",
                "item_cost_price",
                "item_unit_price",
                "item_number",
                "name",
                "actual_category",
                "department",
                "isn",
                "subcategory",
                "gl_code",
                "discount_percent",
                "subtotal",
                "total",
                "tax",
                "profit",
                "total_cost",
                "receipt_only"
            ],
            "customer_groups"=>[
                "label"
            ],
            "customer_group_members"=>[

            ],
            "sales_comps"=>[
                "line",
                "type",
                "description",
                "amount",
                "dollar_amount"
            ],
		    "sales_comps_employee"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
            "suppliers"=>[
                "company_name",
                "fax_number",
                "account_number"
            ],
            "sales_items"=>[
		    "price_class_id",
		    "type",
		    "teesheet",
		    "price_category",
		    "serialnumber",
		    "description",
		    "teetime_id",
		    "terminal_id",
		    "sale_id",
		    "line",
		    "timeframe_id",
		    "comment",
		    "payment_type",
		    "customer_id",
		    "employee_id",
		    "item_id",
		    "item_kit_id",
		    "invoice_id",
		    "supplier_id",
		    "quantity_purchased",
		    "item_cost_price",
		    "item_unit_price",
		    "item_number",
		    "name",
		    "actual_category",
		    "department",
		    "isn",
		    "subcategory",
		    "gl_code",
		    "discount_percent",
		    "subtotal",
		    "total",
		    "tax",
		    "profit",
		    "total_cost",
		    "receipt_only"
	    ],"foreup_sales_items"=>[
		    "price_class_id",
		    "type",
		    "teesheet",
		    "price_category",
		    "serialnumber",
		    "description",
		    "teetime_id",
		    "terminal_id",
		    "sale_id",
		    "line",
		    "timeframe_id",
		    "comment",
		    "payment_type",
		    "customer_id",
		    "employee_id",
		    "item_id",
		    "item_kit_id",
		    "invoice_id",
		    "supplier_id",
		    "quantity_purchased",
		    "item_cost_price",
		    "item_unit_price",
		    "item_number",
		    "name",
		    "actual_category",
		    "department",
		    "isn",
		    "subcategory",
		    "gl_code",
		    "discount_percent",
		    "subtotal",
		    "total",
		    "tax",
		    "profit",
		    "total_cost",
		    "receipt_only"
	    ],
            "sales_items_price_classes"=>[
                "class_id",
                "name",
                "cart",
            ],
            "employees"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
            "customers"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],"items"=>[
                "course_id",
                "CID",
                "name",
                "department",
                "category",
                "subcategory",
                "supplier_id",
                "item_number",
                "description",
                "image_id",
                "cost_price",
                "unit_price",
                "unit_price_includes_tax",
                "max_discount",
                "quantity",
                "is_unlimited",
                "reorder_level",
                "location",
                "item_id",
                "gl_code",
                "allow_alt_description",
                "is_serialized",
                "is_giftcard",
                "invisible",
                "deleted",
                "food_and_beverage",
                "soup_or_salad",
                "number_of_sides",
                "is_side",
                "add_on_price",
                "print_priority",
                "kitchen_printer",
                "quickbooks_income",
                "quickbooks_cogs",
                "quickbooks_assets",
                "erange_size",
                "is_fee"
            ],
            "item_kits"=>[
                "CID",
                "item_kit_id",
                "course_id",
                "item_kit_number",
                "name",
                "description",
                "department",
                "category",
                "subcategory",
                "unit_price",
                "cost_price",
                "quickbooks_income",
                "quickbooks_cogs",
                "quickbooks_assets",
                "is_punch_card",
                "deleted"
            ],
		    "terminals"=>[
			    "label"
		    ],
            "sales_payments"=>[
                "payment_type",
                "type",
                "reports_only",
                "payment_amount",
                "invoice_id",
                "tip_recipient",
            ],
            "tip_recipients"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id"
            ],
            "punch_cards"=>[
                "punch_card_id",
                "punch_card_number",
                "details",
                "expiration_date",
                "issued_date",
                "deleted",
            ],
            "punch_card_items"=>[
                "punches",
                "used",
                "price_class_id",
            ],
        "sales"=>[
            "teetime_id",
            "guest_count",
            "table_id",
	        "sale_time",
	        "course_id",
	        "refund_comment",
	        "refund_reason"
        ],
        "teetime"=>[
            "type",
            "start",
            "start_datetime",
            "end",
            "end_datetime",
            "allDay",
            "title",
            "details",
            "status",
            "booking_source",
            "reround",
            "player_count",
            "date_booked",
            "date_cancelled",
        ],
        "teesheet"=>[
            "title"
        ],
        "payments"=>[
            "type",
            "payment_type",
            "reports_only",
            "payment_amount",
            "tip_recipient",
            "is_tip"
        ],
        "item_taxes"=>[
            "line",
            "name",
            "percent",
            "cumulative"
        ],
        "price_classes"=>[
            "class_id",
            "name",
            "cart",
        ],
        "invoices"=>[
            "id",
            "sale_id"
        ],
        "weather"=>[
            "weather_entry_id",
            "course_id",
            "date_collected",
            "day",
            "date",
            "summary",
            "icon",
            "precipIntensity",
            "precipProbability",
            "temperature",
            "dewPoint",
            "humidity",
            "windSpeed",
            "windBearing",
            "pressure",
        ]
    ];

    protected function createBaseTable()
    {
	    if (extension_loaded('newrelic'))
		    newrelic_ignore_transaction();
        $table = new SalesTable();
        $filters = [];
        foreach($this->reportParameters->getFilters() as $filterObj){
            foreach($filterObj->getFilterObjects() as $filter){
                $filters[] =$filter;
            }
        }
        $table->setFilters($filters);
        $table->generateIfNeeded();
        $this->setBasetableToNewtable($table);

    }

    /**
     * @param $table
     */
    protected function setBasetableToNewtable($table)
    {
        $this->knownColumns[$table->getTableName()] = $this->knownColumns[$this->baseTable];
        unset($this->knownColumns[$this->baseTable]);
        $this->baseTable = $table->getTableName();
    }


}