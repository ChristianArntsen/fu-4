<?php
namespace fu\reports\ReportTypes;

use fu\reports\TemporaryTables\SharedSalesTable;

class SharedSalesReport extends SalesReport {
	protected $baseTable = "foreup_sales_items_temp" ;
    public function __construct($reportParameters){

        parent::__construct($reportParameters);
	    $course_model = new \Course();
	    $course_ids = array();
	    $course_model->get_linked_course_ids($course_ids, 'shared_customers');
	    $this->defaultFilters = [
		    "security"=>
			    new \fu\reports\ReportFilters\Filter([
				    "column"=>"sales.course_id",
				    "operator"=>"=",
				    "value"=>$course_ids,
				    "boolean"=>"AND",
			    ])
		    ];
    }

	protected function createBaseTable()
	{
		if (extension_loaded('newrelic'))
			newrelic_ignore_transaction();
		$table = new SharedSalesTable();
		$filters = [];
		foreach($this->reportParameters->getFilters() as $filterObj){
			foreach($filterObj->getFilterObjects() as $filter){
				$filters[] =$filter;
			}
		}
		$table->setFilters($filters);
		$table->generateIfNeeded();
		$this->setBasetableToNewtable($table);

	}

}