<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 3/19/2015
 * Time: 12:25 PM
 */

namespace fu\reports;


use fu\Exceptions\InvalidArgument;

class ReportSelectStatement {

	private $columns;
	private $statement;
	private $original;

	public function __construct($columnString){
		$this->original = $columnString;
		preg_match_all("/`(.*?)`/",$columnString,$matches);
		if(count($matches[1]) > 0){
			$columns = [];
			foreach($matches[1] as $match){
				if(!in_array($match,$columns)){
					$columns[] = $match;
				}
			}
			foreach($columns as $key=>$value){
				$columnString = preg_replace("/`".preg_quote($value)."`/","[".$key."]",$columnString);
				$this->columns[$key] = new ReportColumn($value);
			}

			$this->statement = $columnString;
		} else {
			$this->columns[0] = new ReportColumn($columnString);
			$this->statement = "[0]";
		}

    }

	public function getColumns(){
		return $this->columns;
	}
	public function getOriginal(){
		return $this->original;
	}
	public function getStatement(){
		return $this->statement;
	}

	public function getCompiledName($baseTable){
		$stringOutput = $this->statement;
		foreach($this->columns as $key=>$value){
			$stringOutput = preg_replace("/\[".$key."\]/","".$value->toString($baseTable)."",$stringOutput);
		}
		return $stringOutput;
	}
	public function toString($baseTable){
		$stringOutput = $this->getCompiledName($baseTable);

		$column = new ReportColumn($stringOutput);
		if($this->statement != "[0]" && count($this->columns)==1 && substr_count($this->getStatement(),"[0]") <= 1){
			if(! $column->isAggregate()){
				throw new InvalidArgument("Invalid column");
			}
		}


 		$stringOutput = $this->attachLabel($stringOutput);
		return $stringOutput;
	}
	private function attachLabel($stringOutput){
		//$original = mysqli::escape_string($this->original);
		$original = $this->original;
		$stringOutput = "{$stringOutput} AS '{$original}'";
		return $stringOutput;
	}

	public function setOriginal($original)
	{
		$this->original=$original;
	}



}