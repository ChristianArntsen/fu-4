<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 2/11/2016
 * Time: 6:20 PM
 */

namespace fu\reports\Printers;


abstract class Printer
{
	/**
	 * @var
	 */
	protected $report;
	/**
	 * @var
	 */
	protected $data;
	/**
	 * @var ReportParameters
	 */
	protected $request_parameters;

	protected $meta_data;
	protected $totals;
	protected $name_to_column_map;


	abstract function generate();
	abstract function save();
	abstract function add_meta_data($data);


	protected function __constructor()
	{
		$this->totals = [];
		foreach($this->report->columns as $column){

			$this->name_to_column_map[$this->render_complete_name($column)] = $column;
		}
	}

	protected function column_to_label($column)
	{
		$this->pregenerate_labels($column);
		if(isset($this->column_labels[$column])){
			return $this->column_labels[$column];
		} else {
			return $column;
		}
	}

	/**
	 * @param $column
	 */
	protected function pregenerate_labels($column)
	{
		if (!isset($this->column_labels)) {
			$column_labels = [];
			foreach ($this->report->columns as $column) {
				$completeName = $this->render_complete_name($column);

				if ($column->label_override) {
					$column_labels[$completeName] = $column->label_override;
				} else {
					$column_labels[$completeName] = $column->label;
				}
			}
			$this->column_labels = $column_labels;
		}
	}

	protected function render_complete_name($column)
	{
		$completeName = $column->column;
		if ($column->table != "") {
			$completeName = "{$column->table}.{$column->column}";
		} else {
			$completeName = $column->column;
		}
		if ($column->aggregate) {
			$completeName = "{$column->aggregate}({$completeName})";
		}
		if ($column->custom_column) {
			$completeName = $column->custom_column;
		}
		return $completeName;
	}

	protected function add_to_totals($column,$value)
	{
		$column_map = $this->name_to_column_map;



		if(!isset($this->totals[$column])){
			$this->totals[$column] = 0;
		}
		if($column_map[$column]->type == "number"){
			if(strpos($column, '_id') !== false){
				$this->totals[$column] = "";
			} else {
				$this->totals[$column] += round($value,2);
			}
		} else {
			if(empty($this->totals[$column]))
				$this->totals[$column] = [];
			if(empty($value))
				$value = "empty";
			if($column_map[$column]->type == "date_range"){
				$value = substr($value,0,10);
			}
			$this->totals[$column][$value] = 1;
		}
	}
}