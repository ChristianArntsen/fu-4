<?php
namespace fu\reports\Printers;

use Carbon\Carbon;
use fu\aws\s3;
use fu\reports\ReportParameters;

class Excel extends Printer
{

	private $excel_document;
	private $starting_row_meta_data = 2;


	public function __construct($report,$data,$request_parameters)
	{
		$this->report = $report;
		$this->data = $data;
		$this->request_parameters = $request_parameters;
		$this->excel_document = new \PHPExcel();
		$this->excel_document->setActiveSheetIndex(0);
		$this->meta_data = [];
		$this->date_range = "";
		parent::__constructor();
	}

	public function generate()
	{
		$excelTitle = new \PHPExcel_RichText();
		$excelTitle->createText("");
		$titleObject = $excelTitle->createTextRun($this->report->title);
		$titleObject->getFont()->setBold(true)->setSize(16);

		$date_filters = $this->request_parameters->getDateFilters();
		if(count($date_filters) > 0){
			$start_date = Carbon::parse($date_filters[0]->getValue());
			$end_date = Carbon::parse($date_filters[1]->getValue());

			$dateRange = new \PHPExcel_RichText();
			$range = $dateRange->createTextRun("Date Range");
			$range->getFont()->setBold(true);
			$this->date_range = $start_date->format("F d, Y")." to ".$end_date->format("F d, Y");
			$dateRange->createTextRun($this->date_range);
		}


		$formatting = new \PHPExcel_Style_Conditional();
		$formatting->setConditions("");


		$date_columns = [];
		$numbers = [];
		foreach($this->report->columns as $column){
			if($column->type == "date_range"){
				$date_columns[$this->render_complete_name($column)] = "date_range";
			} else if($column->type == "number"){
				$numbers[$this->render_complete_name($column)] = "number";
			}
		}



		$objPHPExcel = $this->excel_document;
		//$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
		//	->setLastModifiedBy("Maarten Balliauw")
		//	->setTitle("Office 2007 XLSX Test Document")
		//	->setSubject("Office 2007 XLSX Test Document")
		//	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
		//	->setKeywords("office 2007 openxml php")
		//	->setCategory("Test result file");


		$objPHPExcel->getActiveSheet()->getCell('A1')->setValue($excelTitle);
		$objPHPExcel->getActiveSheet()->getCell('E1')->setValue($dateRange);


		$start_filter_row = 2;
		$current_filter_row = $start_filter_row;
		foreach($this->request_parameters->getOrigFilter() as $filter)
		{
			$column = $this->column_to_label($filter->getColumn());
			if(is_array($filter->getValue())){
				$values = implode(",",$filter->getValue());
			} else {
				$values = $filter->getValue();
			}
			$text = "{$column} {$filter->getOperator()} {$values}";

			$filter_text = new \PHPExcel_RichText();
			$category = $filter_text->createTextRun();
			$category->getFont()->setBold(true);
			$filter_text->createTextRun("$text");
			$objPHPExcel->getActiveSheet()->getCell('E'.$current_filter_row++)->setValue($filter_text);
		}




		$columns = $this->request_parameters->getColumns();
		$headers = [];
		foreach($columns as $column){
			$headers[] = $this->column_to_label($column->getOriginal());
		}
		$objPHPExcel->getActiveSheet()->fromArray(
			(array)$headers,NULL,"A7"
		);
		$objPHPExcel->getActiveSheet()->setAutoFilter('A7:'.\PHPExcel_Cell::stringFromColumnIndex(count($headers)-1)."7");

		$odd_row_style = array(
			'fill' => array(
				'type' => \PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'CCCCCC')
			),
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => 'AAAAAA')
				)
			)
		);
		$rowNumber = 8;
		while($row = $this->data->fetch_object()){

			$columnNumber = 0;
			foreach($row as $key => $value){
				$colString = \PHPExcel_Cell::stringFromColumnIndex($columnNumber);
				$columnNumber++;
				if($this->name_to_column_map[$key]->type == "date_range"){
					$row->$key = Carbon::parse($value)->format("F d, Y g:i A");
				} else if($this->name_to_column_map[$key]->type == "number"){
					//$row->$key = number_format($value,2);
					$objPHPExcel->getActiveSheet()->getStyle($colString.$rowNumber)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
				}
			}
			$objPHPExcel->getActiveSheet()->fromArray(
				(array)$row,NULL,"A".$rowNumber
			);
			//if($rowNumber%2 == 0){
			//	$objPHPExcel->getActiveSheet()
			//		->getStyle("A".$rowNumber.':'.\PHPExcel_Cell::stringFromColumnIndex(count($headers)-1).$rowNumber)
			//		->applyFromArray($odd_row_style);
			//}

			$rowNumber++;
		}

		$objPHPExcel->getActiveSheet()->setShowGridlines(true);
		$columnEnding = \PHPExcel_Cell::stringFromColumnIndex(count($headers)-1);
		$objPHPExcel->getActiveSheet()->getStyle('A7:'.$columnEnding."7")->applyFromArray(
			array(
				'fill' 	=> array(
					'type'		=> \PHPExcel_Style_Fill::FILL_SOLID,
					'color'		=> array('argb' => 'FFCCFFCC')
				),
				'borders' => array(
					'outline' => array(
						'style' => \PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			)
		);


	}

	public function save()
	{
		$CI = & get_instance();
		$objWriter = \PHPExcel_IOFactory::createWriter($this->excel_document, 'Excel2007');
		$report_name = sha1(serialize($this->report).time()).".xlsx";
		//Save file to s3
		if (!file_exists('reports/')) {
			mkdir('reports/', 0777, true);
		}
		$objWriter->save("reports/$report_name");
		$s3 = new s3();
		$url = $s3->uploadToReportBucket("reports/$report_name",$report_name);
		$CI->load->model("report_exports");
		$report_exports = new \Report_Exports();
		$report_exports->create([
			"name"=>$report_name,
			"human_readable_name"=>$this->report->title." - ".$this->date_range
		]);
		return ["destination"=>$url];
	}

	public function add_meta_data($data)
	{
		$objPHPExcel = $this->excel_document;
		$row_number = $this->starting_row_meta_data + count($this->meta_data);
		$this->meta_data[] = $data;
		$objPHPExcel->getActiveSheet()->getCell('A'.$row_number)->setValue($data);
	}

}