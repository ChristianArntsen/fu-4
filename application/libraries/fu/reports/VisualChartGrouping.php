<?php

namespace fu\reports;


use fu\Exceptions\InvalidArgument;
use fu\reports\VisualWidths\Day;
use fu\reports\VisualWidths\Month;
use fu\reports\VisualWidths\Week;
use fu\reports\VisualWidths\Year;
use fu\reports\VisualWidths\Range;

class VisualChartGrouping{

	protected $width,$grouping,$date_column;

	public function __construct()
	{
		$aggregations = new Aggregations();
		$this->possibleGroupings = array_merge($aggregations->getDateAggregations(),["range"]);
	}

	protected function createGroupingColumn($grouping)
	{
		return $grouping."(".$this->getDateColumn().")";
	}

	/**
	 * @return mixed
	 */
	public function getWidth()
	{
		return $this->width;
	}

	/**
	 * @param mixed $width
	 */
	public function setWidth($width)
	{
		$this->width = $width;
	}

	/**
	 * @return mixed
	 */
	public function getGrouping()
	{
		return $this->grouping;
	}

	/**
	 * @param mixed $grouping
	 */
	public function setGrouping($grouping)
	{
		$this->grouping = $grouping;
	}

	/**
	 * @return mixed
	 */
	public function getDateColumn()
	{
		return $this->date_column;
	}

	/**
	 * @param mixed $date_column
	 */
	public function setDateColumn($date_column)
	{
		$this->date_column = $date_column;
	}

	public static function createVisualWidthObject($width)
	{
		if($width == "day"){
			return new Day();
		} else if($width == "month"){
			return new Month();
		} else if($width == "week"){
			return new Week();
		} else if($width == "year"){
			return new Year();
		} else if($width == "range"){
			return new Range();
		} else {
			throw new InvalidArgument("Invalid width given");
		}
	}

}