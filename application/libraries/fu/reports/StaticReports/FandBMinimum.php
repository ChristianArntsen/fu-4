<?php

namespace fu\reports\StaticReports;


use Carbon\Carbon;

class FandBMinimum extends Report
{

    public function render()
    {
        $this->CI->load->model("minimum_charge");
        $this->CI->load->model("course");
        $this->CI->load->model("appconfig");
        $this->CI->load->model("terminal");
        $this->CI->load->model("employee");
        if(count($this->filters)!= 1){
            $allCharges = $this->CI->minimum_charge->get();
            if(count($allCharges) > 0){
                $charge = $allCharges[0];
            }
        } else {
            $filter = $this->filters[0];
            $charge = $this->CI->minimum_charge->get([
                "charge_id"=>$filter->getValue()
            ])[0];
        }



        $course_info = $this->CI->Course->get_info($this->CI->session->userdata("course_id"));
        $minimum_charge_id = $charge['minimum_charge_id'];

        // Customers needing charged
        $customers = $this->CI->minimum_charge->get_pending_charge_customers([$minimum_charge_id]);
        $customers = $customers[$minimum_charge_id];
        $filters = $charge['item_filters'];



        $charge_end = Carbon::now();
        $charge_end->addDay();
        $charge_end->endOfDay();

        if(!empty($charge['last_ran']) && $charge['last_ran'] != '0000-00-00 00:00:00'){
            $charge_start = Carbon::parse($charge['last_ran'])->endOfDay();
        } else {
            $charge_start = Carbon::parse($charge['start_date'])->year(Carbon::now()->year);
            if($charge_start->gt($charge_end)){
                $charge_start = $charge_start->modify('-1 year');
            }
        }


        $data['customers'] = $this->CI->minimum_charge->get_total_spending($customers, $charge['course_id'], $filters, $charge_start->toDateTimeString(), $charge_end->toDateTimeString());

        $customer_ids = [];
        foreach($data['customers'] as $key=>$customer)
        {
            $customer_ids[$customer['customer_id']] = $key;
        }
        $this->CI->load->model("customer");
        $customer_info = $this->CI->customer->get_multiple_info(array_keys($customer_ids));
        foreach($customer_info->result_array() as $info)
        {
            $row_in_customers_array = $customer_ids[$info['person_id']];
            $data['customers'][$row_in_customers_array]["customer_information"] = $info;
        }

        $data['course_name'] =$course_info->name;
        $data['logo'] = $this->CI->appconfig->get_logo_image("",$this->CI->session->userdata("course_id"));
        $data['date'] = Carbon::now()->format("M j Y h:i a");
        $data['charge'] = $charge;
        $data['total_charges'] = 0;
        $data['total_left'] = 0;
        $data['charge_start'] = $charge_start;
        $data['charge_end'] = $charge_end;

        $rendered = $this->CI->load->view('reportsv2/FandBMinimum',$data,true);
        return $rendered;
    }
}