<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 3/3/2016
 * Time: 11:55 AM
 */

namespace fu\reports\StaticReports;


class Factory
{
    public static function create($report_type)
    {
        if($report_type == "RegisterLog"){
            return new RegisterLog();
        } else if($report_type == "FandBMinimum"){
            return new FandBMinimum();
        }  else if($report_type == "Courses"){
            return new CoursesReport();
        }  else if($report_type == "MembershipSummary"){
	        return new MembershipSummary();
        }  else if($report_type == "PaymentsAndSales"){
	        return new PaymentsAndSales();
        }  else if($report_type == "SellThru"){
	        return new SellThru();
        }  else {
            throw new \Exception("Invalid Report Type");
        }

    }
}