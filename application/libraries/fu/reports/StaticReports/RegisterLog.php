<?php

namespace fu\reports\StaticReports;


use Carbon\Carbon;

class RegisterLog extends Report
{

    public function render()
    {
        $this->CI->load->model("v2/Register_log_model");
        $this->CI->load->model("course");
        $this->CI->load->model("appconfig");
        $this->CI->load->model("terminal");
        $this->CI->load->model("employee");

        if(count($this->filters)!= 1){
            $this->register_log = $this->CI->Register_log_model->get_with_joins([
                "closing_employee_id"=>$this->CI->session->userdata("employee_id"),
                "course_id"=>$this->CI->session->userdata("course_id"),
                "limit"=>1
            ]);
            if(count($this->register_log) == 1){
                $this->register_log = $this->register_log[0];
            } else {
                return "";
            }
        } else {
            $filter = $this->filters[0];
            $this->register_log = $this->CI->Register_log_model->get_with_joins([
                "register_log_id"=>$filter->getValue()
            ]);
        }

        $course_info = $this->CI->Course->get_info($this->register_log['course_id']);

        $payment_info = $this->getPaymentsCollectedData();
        $gl_summary = $this->getGLData();
        $data = array_merge($payment_info,$gl_summary);
        $data['shift_end'] = Carbon::parse($this->register_log['shift_end'])->format("M j Y h:i a");
        $data['shift_start'] = Carbon::parse($this->register_log['shift_start'])->format("M j Y h:i a");
        $data['course_name'] =$course_info->name;
        $data['logo'] = $this->CI->appconfig->get_logo_image("",$this->register_log['course_id']);
        $data['date'] = Carbon::now()->format("M j Y h:i a");
        $data['opening_employee'] = $this->register_log['opening_employee.first_name'] ." ".$this->register_log['opening_employee.last_name'];
        $data['closing_employee'] = $this->register_log['closing_employee.first_name'] ." ".$this->register_log['closing_employee.last_name'];
        $data['terminal_name'] = $this->register_log['terminals.label'];


        $rendered = $this->CI->load->view('reportsv2/RegisterLog',$data,true);
        return $rendered;
    }

    private function getGLData()
    {
        $register_log = $this->register_log;

        $between = "BETWEEN '{$register_log['shift_start']}' AND '{$register_log['shift_end']}'";
        //Get sales items, grouped by GL, Payment Type
        $query = $this->CI->db
            ->from("sales")
            ->join("sales_items si","sales.sale_id = si.sale_id","LEFT")
            ->join("items i","si.item_id = i.item_id","LEFT")
            //->join("sales_payments","sales.sale_id = sales_payments.sale_id","LEFT")
            ->join("price_classes pc","pc.class_id = si.price_class_id AND si.price_class_id != 0","LEFT")
            ->where("sales.sale_time ".$between)
            ->where("sales.course_id",$register_log['course_id'])
            ->where("terminal_id",$register_log['terminal_id'])
            ->where("sales.deleted",0)
            ->select("
                CASE
					WHEN i.category = 'Green Fees' AND si.price_class_id != 0 AND si.price_class_id IS NOT NULL THEN pc.green_gl_code
					WHEN i.category = 'Carts' AND si.price_class_id != 0 AND si.price_class_id IS NOT NULL THEN pc.cart_gl_code
					ELSE i.gl_code
				END AS gl_code",false)
            //->select("sales_payments.type as type")
            ->select("SUM(quantity_purchased * item_unit_price * .01 * discount_percent) as discount")
            ->select("SUM(si.total) as total")
            ->compileSelect();
        $this->CI->db->reset_query_builder();
        $query = $this->CI->db->query($query . " GROUP BY CASE WHEN i.category = 'Green Fees' AND si.price_class_id != 0 AND si.price_class_id IS NOT NULL THEN pc.green_gl_code WHEN i.category = 'Carts' AND si.price_class_id != 0 AND si.price_class_id IS NOT NULL THEN pc.cart_gl_code ELSE i.gl_code END");

        $results = $query->result_object();
        $gl = [];
        $gl_codes = [];
        $totals = [
            "total"=>0,
            "discount"=>0
        ];
        foreach ($results as &$row)
        {
            if($row->gl_code == ""){
                $row->gl_code = "No GL Code";
            }
            if(!isset($gl_codes[$row->gl_code])){
                $gl_codes[$row->gl_code] = [
                    "total"=>0,
                    "discount"=>0,
                    "type"=>"type"
                ];
                $gl[$row->gl_code]= [];
            }
            if("type" == "change"){
                continue;
            }
            $gl_codes[$row->gl_code]["total"] += $row->total;
            $gl_codes[$row->gl_code]["discount"] += $row->discount;
            $totals['total']+=$row->total;
            $totals['discount']+=$row->discount;

            //Grab a payment for each sale that had the current GL code, take the si.total
            //Check if more than one gl code was shared in a sale
            //
            //$gl[$row->gl_code] [] = [
            //    "payment_type"=>"type",
            //    "total"=>$row->total,
            //    "discount"=>$row->discount,
            //];
        }
        return [
            "gl_codes"=>$gl_codes,
            "gl"=>$gl,
            "gl_totals"=>$totals,
        ];
    }

    private function getPaymentsCollectedData()
    {
        $register_log = $this->register_log;

        $between = "BETWEEN '{$register_log['shift_start']}' AND '{$register_log['shift_end']}'";
        $query = $this->CI->db
            ->from("sales")
            ->join("sales_payments","sales.sale_id = sales_payments.sale_id","LEFT")
            ->where("course_id",$register_log['course_id'])
            ->where("terminal_id",$register_log['terminal_id'])
            ->where("sales.sale_time ".$between)
            ->select("sales_payments.type as 'type'")
            ->select("SUM(CASE WHEN payment_amount>0 AND deleted = 0 THEN payment_amount ELSE 0 END) as new")
            ->select("SUM(CASE WHEN payment_amount<0 AND deleted = 0 THEN payment_amount ELSE 0 END) as refunds")
            ->select("SUM(CASE WHEN deleted = 1 THEN payment_amount ELSE 0 END) as voids")
            ->select("SUM(CASE WHEN deleted = 0 THEN payment_amount ELSE 0 END) as total")
            ->group_by("sales_payments.type")
            ->get();
        $totals = [
            "new"=>0,
            "refunds"=>0,
            "voids"=>0,
            "total"=>0,
            "collected"=>0,
            "difference"=>0,
        ];
        $results = $query->result_object();
        $total_cash_check = 0;
        $total_credit_card = 0;
        foreach ($results as $key => &$row)
        {
            $row->collected = 0;
            $row->difference = 0;
            if($row->type == "cash"){
                $change = 0;
                foreach ($results as $search)
                {
                    if($search->type == "change"){

                        $row->new += $search->refunds;
                        $row->total += $search->refunds;
                    }
                }
                $row->collected = ($register_log['close_amount']-$register_log['open_amount']);
                $row->difference = $register_log['cash_sales_amount'] - $row->collected ;
                $total_cash_check +=  $row->total;
            } else if ($row->type == "check"){
                $row->collected = $register_log['close_check_amount'];
                $row->difference = $register_log['check_sales_amount'] - ($register_log['close_check_amount']);
                $total_cash_check +=  $row->total;
            } else if($row->type == "credit_card"){
                $row->collected = $row->total;
                $total_credit_card += $row->total;
            } else if($row->type == "change"){
                unset($results[$key]);
                continue;
            }
            $payments[$row->type] = $row;

            foreach($totals  as $key=>$total){
                $totals[$key] += (double)$row->{$key};
            }
        }

        return [
            "payments"=>$results,
            "totals"=>$totals,
            "total_cash_check"=>$total_cash_check,
            "total_credit_card"=>$total_credit_card
        ];
    }
}