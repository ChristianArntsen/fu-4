<?php

namespace fu\reports\StaticReports;


use Carbon\Carbon;

class CoursesReport extends Report
{

    public function render()
    {

        $this->CI->load->model("appconfig");
        //$filter = $this->filters[0];
        //$filter->getValue();

        $rows = [];

        $today = Carbon::now();
        $first_sale = Carbon::parse("2011-11-01");
        $months = $today->diffInMonths($first_sale);

        $date = Carbon::now()->subMonth($months);
        while($date < Carbon::now()){

            $query = $this->CI->db->query("
                SELECT start,new,cancelled,`new`-`cancelled` as net_new,`cancelled` / `start` as churn_rate,`end`/`start` - 1 AS growth_rate,`end`
                FROM (
                    SELECT
                     SUM(case WHEN ci.date_sold < '{$date->startOfMonth()}' AND (ci.date_cancelled = '0000-00-00' OR ci.date_cancelled >= '{$date->startOfMonth()}') then 1 else 0 end) as 'start',
                     SUM(case WHEN ci.date_sold <= '{$date->endOfMonth()}' AND ci.date_sold >= '{$date->startOfMonth()}' then 1 else 0 end) as 'new',
                     SUM(case WHEN ci.date_cancelled <= '{$date->endOfMonth()}' AND ci.date_cancelled >= '{$date->startOfMonth()}' then 1 else 0 end) as cancelled,
                     SUM(case WHEN ci.date_sold < '{$date->endOfMonth()}' AND (ci.date_cancelled = '0000-00-00' OR ci.date_cancelled > '{$date->endOfMonth()}') then 1 else 0 end) as 'end'
                    FROM foreup_courses
                    LEFT JOIN foreup_courses_information ci ON foreup_courses.course_id = ci.course_id
                    WHERE foreup_courses.demo = 0
                ) x

            ");
            $row = $query->row();
            $row->row_title = $date->format('F Y');
            $rows[] = $row;

            $date->firstOfMonth();
            $date->addMonth(1);
        }


        $data['table'] = $rows;
        $data['date'] = Carbon::now();
        $data['logo'] = $this->CI->appconfig->get_logo_image("",$this->CI->session->userdata("course_id"));
        $data['date'] = Carbon::now()->format("M j Y h:i a");

        $rendered = $this->CI->load->view('reportsv2/Courses',$data,true);
        return $rendered;
    }
}