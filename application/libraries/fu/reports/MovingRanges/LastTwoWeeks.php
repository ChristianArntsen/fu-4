<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class LastTwoWeeks implements MovingRange
{
	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subWeek(2)->startOfWeek()->subDay();
		return $date->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subWeek(1)->endOfWeek()->subDay(1);
		return $date->toDateTimeString();
	}
} 