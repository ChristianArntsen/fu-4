<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class Minutes implements MovingRange {
	private $minutes;
	public function __construct($minutes)
	{
		$this->minutes = $minutes;
	}

	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subMinutes($this->minutes);
		return $date->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subMinutes($this->minutes);
		return $date->toDateTimeString();
	}
}