<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class LastQuarter implements MovingRange
{
	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subMonth(3);
		return $date->firstOfQuarter()->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subMonth(3);
		return $date->lastOfQuarter()->toDateTimeString();
	}
} 