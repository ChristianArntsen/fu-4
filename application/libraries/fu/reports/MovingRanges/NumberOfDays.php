<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class NumberOfDays implements MovingRange {
	private $days;
	public function __construct($days)
	{
		$this->days = $days;
	}

	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->startOfDay()->subDay($this->days);
		return $date->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		return $date->subDay()->endOfDay()->toDateTimeString();
	}
}