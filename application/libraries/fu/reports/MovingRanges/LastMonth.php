<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class LastMonth implements MovingRange
{
	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->firstOfMonth();
		$date->subMonth(1);
		return $date->startOfMonth()->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->firstOfMonth();
		$date->subMonth(1);
		return $date->endOfMonth()->toDateTimeString();
	}
} 