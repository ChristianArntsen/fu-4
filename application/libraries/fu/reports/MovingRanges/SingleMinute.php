<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


use Carbon\Carbon;

class SingleMinute implements MovingRange {
	private $days;
	private $date;
	public function __construct($days,$date = null)
	{
		if(isset($date)){
			$this->date = $date;
		} else {
			$this->date = \Carbon\Carbon::now();
		}
		$this->days = $days;
	}

	public function getStartTimeStamp()
	{
		$date = $this->date->copy();
		if(isset($this->timezone)){
			$date->timezone($this->timezone);
		}
		$date->addMinutes($this->days);
		return $date->second(0)->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = $this->date->copy();
		if(isset($this->timezone)){
			$date->timezone($this->timezone);
		}
		$date->addMinutes($this->days);
		return $date->second(59)->toDateTimeString();
	}

	public function setTimezone($timezone)
	{
		$this->timezone = $timezone;
	}
}