<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class LastYear implements MovingRange
{
	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subYear(1);
		return $date->startOfYear()->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subYear(1);
		return $date->endOfYear()->toDateTimeString();
	}
}