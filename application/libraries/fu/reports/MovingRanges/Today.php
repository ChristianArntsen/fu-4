<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class Today implements MovingRange {

	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		return $date->startOfDay()->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		return $date->endOfDay()->toDateTimeString();
	}
} 