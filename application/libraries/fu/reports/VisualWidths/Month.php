<?php
namespace fu\reports\VisualWidths;

use Carbon\Carbon;
use fu\Exceptions\InvalidArgument;

class Month extends \fu\reports\VisualChartGrouping
{
    public function getStartOfWidth()
    {
        $start = new \Carbon\Carbon();
        return $start->startOfMonth();
    }
    public function getEndOfWidth()
    {
        $start = new \Carbon\Carbon();
        return $start->endOfMonth();
    }
    public function getRequiredGrouping()
    {
        if($this->grouping == "day"){
            $grouping = [$this->createGroupingColumn("day")];
        } else {
            throw new InvalidArgument("Invalid grouping/width pair.");
        }
        return $grouping;
    }

}