<?php
namespace fu\reports\VisualWidths;

use Carbon\Carbon;
use fu\Exceptions\InvalidArgument;

class Day extends \fu\reports\VisualChartGrouping
{
    public function getStartOfWidth()
    {
        $start = new \Carbon\Carbon();
        return $start->startOfDay();
    }
    public function getEndOfWidth()
    {
        $start = new \Carbon\Carbon();
        return $start->endOfDay();
    }
    public function getRequiredGrouping()
    {
        if($this->grouping == "hour"){
            $grouping = [$this->createGroupingColumn("hour")];
        } else {
            throw new InvalidArgument("Invalid grouping/width pair.");
        }
        return $grouping;
    }

}