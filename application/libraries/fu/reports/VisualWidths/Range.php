<?php
namespace fu\reports\VisualWidths;


class Range extends \fu\reports\VisualChartGrouping
{
    public function getStartOfWidth()
    {
        return null;
    }
    public function getEndOfWidth()
    {
        return null;
    }
    public function getRequiredGrouping()
    {
        $groupings = ["hour","day","week","month","year"];
        $groupingArray = [];
        //Day should include month/year
        if(isset($this->grouping)){

            $index = array_search($this->grouping,$groupings);
            if($index<0){
                return [];
            }
            while($index < count($groupings)){
                $groupingArray[] = $groupings[$index]."(".$this->getDateColumn().")";
                $index++;
            }


        } else if($this->grouping =="quarter"){
            $groupingArray = ["quarter","year"];
        }

        return $groupingArray;
    }

}