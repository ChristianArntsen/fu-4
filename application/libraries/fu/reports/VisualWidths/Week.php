<?php
namespace fu\reports\VisualWidths;

use Carbon\Carbon;
use fu\Exceptions\InvalidArgument;

class Week extends \fu\reports\VisualChartGrouping
{
    public function getStartOfWidth()
    {
        $start = new \Carbon\Carbon();
        return $start->startOfWeek();
    }
    public function getEndOfWidth()
    {
        $start = new \Carbon\Carbon();
        return $start->endOfWeek();
    }
    public function getRequiredGrouping()
    {
        if($this->grouping == "dayofweek"){
            $grouping = [$this->createGroupingColumn("dayofweek")];
        } else if($this->grouping == "hour"){
            $grouping = [$this->createGroupingColumn("hour"),$this->createGroupingColumn("dayofweek")];
        } else {
            throw new InvalidArgument("Invalid grouping/width pair.");
        }
        return $grouping;
    }

}