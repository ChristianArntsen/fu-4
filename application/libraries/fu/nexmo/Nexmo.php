<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 6/12/2017
 * Time: 11:17 AM
 */

namespace fu\nexmo;


use DoctrineProxies\__CG__\foreup\rest\models\entities\ForeupCourses;
use fu\marketing\texting\MsisdnMaker;

class Nexmo
{

	/** NexmoAccount */
	private $nexmo_account;
	/** @var NexmoMessage */
	private $nexmo_message;

	public function __construct($course_info,$db,$api_key,$api_secret)
	{
		$this->course_info = $course_info;
		$this->db = $db;

		$this->api_key = $api_key;
		$this->api_secret = $api_secret;
		$this->nexmo_account = new NexmoAccount($this->api_key,$this->api_secret);
		$this->nexmo_message = new NexmoMessage($this->api_key,$this->api_secret);
	}

	public function has_number()
	{
		$existing_number = $this->db->from("course_virtual_numbers")
			->where("course_id",$this->course_info->course_id)
			->get()->row();

		if(empty($existing_number)){
			return false;
		}

		return true;
	}

	public function get_number()
	{
		$existing_number = $this->db->from("course_virtual_numbers")
			->where("course_id",$this->course_info->course_id)
			->get()->row();

		if(empty($existing_number)){
			return false;
		}

		return $existing_number;
	}

	public function save_number($number)
	{
		return $this->db->insert("course_virtual_numbers",[
			"course_id"=>$this->course_info->course_id,
			"virtual_number"=>$number
		]);
	}

	public function set_forwarded_number($country,$msisdn,$to)
	{
		$msisdnMaker = new MsisdnMaker();
		$msisdnMaker->set($to);
		$to = $msisdnMaker->get();
		$this->nexmo_account->updateNumber($country,$msisdn,$to);
	}

	public function buy_number($process_request = true)
	{
		$country = $this->course_info->country;
		$country_code = "US";
		$number_to_buy = false;
		if($country == "UK"){
			$country_code = "UK";
		} else if($country == "CA"){
			$country_code = "CA";
		}

		$pattern = preg_replace("/[^0-9]/", "", $this->course_info->phone);
		if(strlen($pattern) == 10){
			$pattern = "1".$pattern;
		}
		$pattern = substr_replace($pattern, "", -4);
		while(!$number_to_buy){
			$search_results = $this->nexmo_account->numbersSearch($country_code,$pattern);
			if(!empty($search_results)){
				$number_to_buy = $search_results[0]['msisdn'];
			}
			$pattern = substr_replace($pattern, "", -1);
		}

		if($process_request){
			$bought = $this->nexmo_account->numbersBuy($country_code,$number_to_buy);
		} else {
			$bought = true;
		}

		if($bought){
			$this->save_number($number_to_buy);

			if(!empty($this->course_info->phone)){
				$this->set_forwarded_number($country_code,$number_to_buy,$this->course_info->phone);
			}


			return $number_to_buy;
		} else {
			return false;
		}
	}

	public function send_message($to,$message,$employee_id = null)
	{
		$virtualnumber = $this->get_number();
		if(!$virtualnumber){
			//Let's purchase a number for the course
			$virtualnumber = $this->buy_number();
			if(empty($virtualnumber)){
				throw new \Exception("No virtual number assigned for this course. ".$this->course_info->course_id);
			}
		}



		$response = $this->nexmo_message->sendText($to,$virtualnumber->virtual_number,$message);
		if(empty($response['messagecount'])){
			return ["success"=>false,"msg"=>"Unknown Error."];
		}
		$status =  $response['messages'][0]->status;
		switch ($status) {
			case 0:
				return ["success"=>true,"msg"=>"Success"];
				break;
			case 1:
				return ["success"=>false,"msg"=>"We were throttled due to too many messages going out."];
				break;
			case 7:
				return ["success"=>false,"msg"=>"The number you're attempting to reach is black listed."];
				break;
			case 12:
				return ["success"=>false,"msg"=>"The message was too long"];
				break;
			default:
				return ["success"=>false,"msg"=>"Error Code: $status"];
		}
	}


}