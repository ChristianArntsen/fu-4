<?php
namespace fu;

class EventDispatcher {

	private static $instance;


	/**
	 * @return EventDispatcher
	 */
	public static function getInstance()
	{
		if (null === static::$instance) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	protected function __construct()
	{
		$this->eventList = [];
	}

	private $eventList;

	public function addEvent($name)
	{
		$this->eventList [] = $name;
	}

	public function getEvents()
	{
		return $this->eventList;
	}

}