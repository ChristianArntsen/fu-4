<?php
// Journal.php
// Recording Transactions on table of accounts

namespace fu\accounting;

class Transactions {
	protected $CI;
	protected $table;
	protected $last_error;

	function __construct($table = 'transactions')
	{
		$this->CI = get_instance();
		$this->table = $table;
	}

	public function get_last_error() {
		return $this->last_error;
	}

	public function reset_last_error() {
		$this->last_error = null;
	}

	public function post($reference_id,$employee_id,$transaction_time = null,$notes = null,$comment = null){
		// reference_id is intended as a fkey to an existing foreup table, such as sales
		$this->last_error = 'Transactions->post Error: not implemented';
		return false;
	}

	public function put($reference_id,$employee_id, $transaction_id = null, $transaction_time = null,$notes = null,$comment = null){
		$this->last_error = 'Transactions->put Error: not implemented';
		return false;
	}

	public function patch($parameters){
		$this->last_error = 'Transactions->patch Error: not implemented';
		return false;
	}

	public function delete($id, $hard = false){
		$this->last_error = 'Transactions->delete Error: not implemented';
		return false;
	}

	public function get($parameters, $inc_deleted = false){
		$this->last_error = 'Transactions->get Error: not implemented';
		return false;
	}

	protected function validate_id($id){
		if(!is_numeric($id)||!is_int($id*1)) return false;
		return true;
	}
}

?>