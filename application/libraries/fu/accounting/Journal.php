<?php
// Journal.php
// Recording Debits and Credits for accounting Transactions

namespace fu\accounting;

class Journal {
	protected $CI;
	protected $table;
	protected $last_error;

	function __construct($table = 'journal')
	{
		$this->CI = get_instance();
		$this->table = $table;
	}

	public function get_last_error() {
		return $this->last_error;
	}

	public function reset_last_error() {
		$this->last_error = null;
	}

	public function post($transaction_id,$account_id,$d_c,$amount,$notes = null,$comment = null){
		$this->last_error = 'Journal->post Error: not implemented';
		return false;
	}

	public function put($transaction_id,$account_id,$d_c,$amount,$notes = null,$comment = null){
		$this->last_error = 'Journal->put Error: not implemented';
		return false;
	}

	public function patch($parameters){
		$this->last_error = 'Journal->patch Error: not implemented';
		return false;
	}

	public function delete($id){
		$this->last_error = 'Journal->delete Error: not implemented';
		return false;
	}

	public function get($parameters, $inc_deleted = false){
		$this->last_error = 'Journal->get Error: not implemented';
		return false;
	}

	protected function validate_id($id){
		if(!is_numeric($id)||!is_int($id*1)) return false;
		return true;
	}
}

?>