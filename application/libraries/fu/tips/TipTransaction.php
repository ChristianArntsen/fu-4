<?php
// TipTransaction.php
// High level interface for recording and editing tip transactions
namespace fu\tips;
use \fu\tips\TipsJournal;
use \fu\accounting\TableOfAccounts;

class TipTransaction extends TipsTransactions {
	private $transaction_id;
	private $transaction;
	private $journal_entries;
	private $journal;
	private $accounts;
	private $course_id;

	function __construct($course_id, $transaction_id = null, $account_table = 'table_of_accounts',$transaction_table = 'tip_transactions',$journal_table = 'tip_journal')
	{
		$this->journal_entries = array();
		$this->journal = new TipsJournal($journal_table);
		$this->accounts = new TableOfAccounts($account_table);
		$this->course_id = $course_id;
		parent::__construct($transaction_table);
		$this->table = $transaction_table;
		$this->set_transaction_id($transaction_id);
	}

	public function get_accounts($course_id = null,$validate_only = false){
		if(!$course_id) {
			$course_id = $this->course_id;
		}
		return $this->accounts->get(array('course_id'=>$course_id),false,$validate_only);
	}

	public function get_tip_expense_account($course_id,$validate_only = false){
		return $this->get_specialized_account($course_id,'tips_expense','expense','An expense account used for tracking tips owed to waitstaff.',$validate_only);
	}

	public function get_net_tips_payable_account($course_id,$validate_only = false){
		return $this->get_specialized_account($course_id,'net_tips_payable','liability','A liability account used for tracking unallocated tips owed to waitstaff.',$validate_only);
	}

	public function get_specialized_account($course_id,$name,$type,$description,$validate_only = false)
	{
		if (!$course_id) {
			$course_id = $this->course_id;
		}

		if(!is_numeric($course_id) || !is_int($course_id*1)){
			$this->last_error = 'TipTransaction->get_specialized_account Error: course_id must be an integer.';
			return false;
		}

		$result = $this->accounts->get(array('name' => $name, 'course_id' => $course_id),false,$validate_only);
		if (empty($result)) {
			$account_id = $this->accounts->post($course_id, $name,
				$type, $description);
			$result = $this->accounts->get($account_id,false, $validate_only);
		}
		if($validate_only)return $result;
		return $result[0];
	}

	public function set_transaction_id($transaction_id,$validate_only = false){
		$transactions = $this->get($transaction_id,false,$validate_only);
		if(!$transactions)return false;

		if($validate_only)return true;

		if( count($transactions) === 1){
			$this->transaction_id = $transaction_id;
			$this->transaction = $transactions[0];
			$this->journal_entries = $this->journal->get(array('transaction_id'=>$transaction_id));
			return true;
		}else{
			$this->last_error = 'TipTransaction->set_transaction_id Error: transaction_id not found.';
			return false;
		}
	}

	public function save($sale_id,$employee_id,$transaction_time = null,$notes = null,$comment = null,$validate_only = false){
		$id = parent::post($sale_id,$employee_id,$transaction_time,$notes,$comment,$validate_only);
		if($validate_only && $id){
			$this->transaction=array('transaction_id'=>1,'sale_id'=>1,'employee_id'=>1,'transaction_time'=>null,
				'notes'=>null,'comment'=>null);
			$this->transaction_id = 1;
			return 1;
		}
		if(!$id)return $id;

		$this->set_transaction_id($id);
	}

	public function get($key,$inc_deleted=false,$validate_only=false){
		if(is_string($key)&&!is_numeric($key)){
			if(!isset($this->transaction_id)){
				$this->last_error = 'TipTransaction->get Error: no transaction loaded. Please use set_transaction_id.';
				return false;
			}
			// override get method
			if(!in_array($key,array('transaction_id','employee_id','transaction_time','notes','comment'))){
				$this->last_error = 'TipTransaction->get Error: cannot get that key.';
				return false;
			}
			if($validate_only)return true;
			return $this->transaction[$key];
		}
		else{
			return parent::get($key,$inc_deleted,$validate_only);
		}
	}

	public function set($key,$value){
		if(!isset($this->transaction_id)){
			$this->last_error = 'TipTransaction->set Error: no transaction loaded. Please use set_transaction_id.';
			return false;
		}
		if(!in_array($key,array('employee_id','transaction_time','notes','comment'))){
			$this->last_error = 'TipTransaction->set Error: cannot set that key.';
			return false;
		}
		$this->transaction[$key] = $value;
		return true;
	}

	public function reload(){
		if(!$this->transaction_id){
			$this->last_error = 'TipTransaction->reload Error: no transaction loaded.';
			return false;
		}
		return $this->set_transaction_id($this->transaction_id);
	}

	public function blank_journal_entry(){
		if(!$this->transaction_id){
			$this->last_error = 'TipTransaction->blank_journal_entry Error: no transaction loaded.';
			return false;
		}
		return array('transaction_id'=>$this->transaction_id,'account_id'=>null,'employee_id'=>null,'transaction_time'=>null,'notes'=>null,'comment'=>null);
	}

	public function put_journal_entry($entry){
		if(!$this->transaction_id){
			$this->last_error = 'TipTransaction->put_journal_entry Error: no transaction loaded.';
			return false;
		}
		$entry['transaction_id'] = $this->transaction_id;
		$entry['transaction_id'] = $this->transaction_id;
		if(!$this->journal->post($entry['transaction_id'],$entry['d_c'],$entry['amount'],$entry['account_id'],$entry['employee_id'],$entry['notes'],$entry['comment'],true)){
			$this->last_error = 'TipTransaction->put_journal_entry Error: '.$this->journal->get_last_error();
			return false;
		}
		$entry['deleted'] = 0;

		foreach($this->journal_entries as &$existing){
			if($existing['transaction_id']===$entry['transaction_id'] &&
				$existing['account_id']===$entry['account_id'] &&
				$existing['employee_id']===$entry['employee_id']){
				$existing = $entry;
				return true;
			}
		}
		$this->journal_entries[] = $entry;
		return true;
	}

	public function get_journal_entry($account_id=null,$employee_id=null){
		if(!$this->transaction_id){
			$this->last_error = 'TipTransaction->get_journal_entry Error: no transaction loaded.';
			return false;
		}
		if(isset($account_id)||isset($employee_id))
		foreach($this->journal_entries as $entry){
			if($entry['account_id']==$account_id && $entry['employee_id'] == $employee_id){
				if($entry['deleted'] == 1)return false;
				return $entry;
			}
		}
		$this->last_error = 'TipTransaction->get_journal_entry Warning: journal entry not found.';
		return false;
	}

	public function get_journal_entries(){
		$ret = $this->journal_entries;
		return $ret;
	}

	public function delete_journal_entry($entry){
		if(!$this->transaction_id){
			$this->last_error = 'TipTransaction->delete_journal_entry Error: no transaction loaded.';
			return false;
		}
		$entry['transaction_id'] = $this->transaction_id;
		$entry['transaction_id'] = $this->transaction_id;
		if(!$this->journal->patch($entry,true)){
			$this->last_error = 'TipTransaction->delete_journal_entry Error: '.$this->journal->get_last_error();
			return false;
		}
		if(isset($entry['transaction_id']) && (isset($entry['account_id'])||isset($entry['employee_id'])))
		foreach($this->journal_entries as &$existing){
			if($existing['transaction_id']===$entry['transaction_id'] &&
				$existing['account_id']===$entry['account_id'] &&
				$existing['employee_id']===$entry['employee_id']){
				$existing['deleted'] = 1;
				return true;
			}
		}
		$this->last_error = 'TipTransaction->delete_journal_entry Warning: not found.';
		return false;
	}

	public function sum_debits(){
		if(count($this->journal_entries) === 0){
			$this->last_error = 'TipTransaction->sum_debits Warning: no journal_entries.';
			return 0;
		}

		$debits_sum = 0;
		foreach($this->journal_entries as $entry){
			if($entry['deleted'])continue;
			if($entry['d_c'] === 'debit'){
				$debits_sum += $entry['amount'];
			}else{
				continue;
			}
		}
		return round($debits_sum,2);
	}

	public function sum_credits(){
		if(count($this->journal_entries) === 0){
			$this->last_error = 'TipTransaction->sum_credits Warning: no journal_entries.';
			return 0;
		}

		$credits_sum = 0;
		foreach($this->journal_entries as $entry){
			if($entry['deleted'])continue;
			if($entry['d_c'] === 'debit'){
				continue;
			}elseif($entry['d_c'] === 'credit'){
				$credits_sum += $entry['amount'];
			}
		}
		return round($credits_sum,2);
	}

	public function get_balance(){
		if(count($this->journal_entries) === 0){
			$this->last_error = 'TipTransaction->get_balance Warning: no journal_entries.';
			return 0;
		}

		$debits_sum = $this->sum_debits();
		$credits_sum = $this->sum_credits();
		return round($debits_sum-$credits_sum,2);
	}

	public function is_balanced() {
		if(count($this->journal_entries) === 0){
			$this->last_error = 'TipTransaction->is_balanced Warning: no journal_entries.';
			return true;
		}

		$balance = $this->get_balance();
		if($balance===0.00)return true;
		return false;
	}

	public function commit($validate_only = false){
		// the only new method that alters the database
		// save modifies the existing post
		$this->reset_last_error();
		$this->journal->reset_last_error();
		if(!$this->transaction_id){
			$this->last_error = 'TipTransaction->commit Error: no transaction loaded.';
			return false;
		}
		if(!$this->is_balanced()){
			$this->last_error = 'TipTransaction->commit Error: Transaction not balanced.';
			return false;
		}
		$CI=get_instance();
		$CI->db->trans_begin();
		if(!$this->patch($this->transaction,$validate_only)){
			$CI->db->trans_rollback();
			return false;
		}

		foreach($this->journal_entries as $entry){
			// fill with null values
			$entry = array_merge(array('transaction_id'=>null,'d_c'=>null,'amount'=>null,
				'account_id'=>null,'employee_id'=>null,'notes'=>null,'comment'=>null),$entry);
			if($entry['deleted'] && !$this->journal->delete($entry['transaction_id'],$entry['account_id'],$entry['employee_id'],false,$validate_only)){
				$CI->db->trans_rollback();
				$this->last_error = 'TipTransaction->commit Error: commit failed while deleting journal entry: '.json_encode($entry).': '. $this->journal->get_last_error();
				return false;
			}
			if(!$entry['deleted'] && !$this->journal->put($entry['transaction_id'],$entry['d_c'],$entry['amount'],$entry['account_id'],$entry['employee_id'],$entry['notes'],$entry['comment'],$validate_only)){
				$CI->db->trans_rollback();
				$this->last_error = 'TipTransaction->commit Error: commit failed while saving journal entry: '.json_encode($entry).': '. $this->journal->get_last_error();
				return false;
			}
		}

		if($validate_only){
			$CI->db->trans_rollback();
			return true;
		}

		$CI->db->trans_commit();
		return true;
	}


	// Begin Auto Balance Section
	// TODO: Can probably split this into a separate object
	public function auto_balance($strategy = 'main_employee'){
		// automatically balances a transaction
	}

	public function a_b_scorched_earth($winner = null){ // (reset)
		// auto balance strategy to destroy credits and start from scratch to get the balance

		// get the entries
		$entries = $this->get_journal_entries();

		// if they are a credit, delete. Set winner aside
		$debits = 0;
		$emp_id = $this->get('employee_id');
		foreach ($entries as $entry) {
			if($entry['d_c']==='debit'){
				$debits += $entry['amount'];
			}
			elseif (!isset($winner) && $this->get('employee_id') === $entry['employee_id']){
				$winner = $entry;
			}else {
				$this->delete_journal_entry($entry);
			}
		}
		// set the credit of the $winner to the value of the tip debit
		$debits = round($debits,2);
		$winner['amount'] = $debits;
		$this->put_journal_entry($winner);
	}

	public function a_b_main_employee(){
		// auto balance strategy to adjust the main employee up or down to get the balance

		// get the entries
		$entries = $this->get_journal_entries();

		// get the balance
		$balance = $this->get_balance();

		// if positive balance, add it to the main employee
		$entry = $this->get_journal_entry(null,$this->get('employee_id'));
		if($balance>0.00 || $entry['amount']>abs($balance)){
			$entry['amount'] = round($entry['amount'] + $balance,2);
			$this->put_journal_entry($entry);
		}
		// if negative balance, add it to the main employee. If negative, defer to scorched earth
		else{
			$this->a_b_scorched_earth($entry);
		}
	}

	public function a_b_keep_ratio(){
		// auto balance strategy to keep the same ratio across credit accounts

		// get the entries
		$entries = $this->get_journal_entries();

		// get previous amount
		$new_debits = $this->sum_debits();

		$debits = 0;
		foreach($entries as $entry){
			if($entry['deleted'])continue;
			if($entry['d_c']==='debit')continue;
			$debits +=$entry['amount'];
		}
		// split amongst employees
		foreach($entries as $entry){
			if($entry['deleted'])continue;
			if($entry['d_c']==='debit')continue;
			$entry['amount'] = round(($entry['amount']/$debits)*$new_debits,2);
			$this->put_journal_entry($entry);
		}
		$balance = $this->get_balance();
		if(abs($balance)>0.05){
			throw \Exception('TipTransaction->a_b_keep_ratio Error: Remainder too large');
		}
		if($balance){
			$entry['amount'] = round($entry['amount']+$balance,2);
			$this->put_journal_entry($entry);
		}

		// remainder goes to tip pool?
	}

	public function a_b_tip_pool(){
		// auto balance strategy to adjust the tip pool up or down to get the balance

		// get the entries
		$entries = $this->get_journal_entries();

		// get the balance

		// if positive balance, add it to the tip pool

		// if negative balance, add it to the tip pool. If negative, defer to scorched earth
	}
	// End Auto Balance section


	// Begin special processing section
	// handles scenarios from the front end
	// TODO: Can probably move these into a separate object
	public function process_single_employee_tip($payment,$recording_emp_id){
		$CI = get_instance();
		$this->reset_last_error();
		$CI->load->model('employee');
		$recipient = $CI->employee->get_info($payment['tip_recipient']);
		$existing_transactions = $this->get(array('sale_id' => $payment['sale_id'], 'notes' => $payment['payment_type']));
		if(count($existing_transactions)===1) {
			$this->set_transaction_id($existing_transactions[0]['transaction_id']);
		}elseif (empty(($existing_transactions))){
			$this->save($payment['sale_id'],$recording_emp_id,null,$payment['payment_type']);
		}else{
			// Multiple transactions for single payment type not supported
			return false;
		}
		$debit = $this->blank_journal_entry();
		$expense_account = $this->get_tip_expense_account($this->course_id);
		$debit['account_id']=$expense_account['account_id'];
		$debit['amount'] = $payment['payment_amount'];
		$debit['d_c'] = 'debit';
		$debit['notes'] = 'Tips Expense: '.$payment['payment_type'];

		$this->put_journal_entry($debit);

		// this is risky, because there may be other employees with whom the tip has been split
		// need to handle this case gracefully...
		$entries = $this->get_journal_entries();
		if(count($entries)<=2) {
			$credit = $this->blank_journal_entry();
			$credit['employee_id'] = $recipient->person_id;
			$credit['amount'] = $payment['payment_amount'];
			$credit['d_c'] = 'credit';
			$credit['notes'] = 'Tips Payable: ' . $recipient->last_name . ', ' . $recipient->first_name;


			if($this->put_journal_entry($credit) && $this->commit(false)){
				return true;
			}else{
				throw new \Exception('TipTransaction->process_single_employee_tip: '.$this->get_last_error());
			}
		}else{
			// use our auto balance strategy
			if($debit['amount']*1===0)$this->a_b_scorched_earth();
			else $this->a_b_main_employee();

			if($this->commit(false)){
				return true;
			}else{
				throw new \Exception('TipTransaction->process_single_employee_tip: '.$this->get_last_error());
			}
		}
	}

	public function process_edited_transaction($course_id,$transaction){
		$new_journal_entries = $transaction['journal_entries'];
		unset($transaction['journal_entries']);
		$this->put($transaction['sale_id'],$transaction['employee_id'],$transaction['transaction_id'],$transaction['transaction_time'],$transaction['notes'],$transaction['comment']);
		$this->set_transaction_id($transaction['transaction_id']);
		foreach($new_journal_entries as $entry){
			if($entry['deleted'])$this->delete_journal_entry($entry);
			else $this->put_journal_entry($entry);
		}

		$credit = false;
		if($transaction['balance']>0){
			$ntp = $this->get_net_tips_payable_account($course_id);
			$ntp_id = $ntp['account_id'];
			$credit = $this->blank_journal_entry();
			$credit['account_id'] = $ntp_id;
			$credit['amount'] = $transaction['balance'];
			$credit['d_c'] = 'credit';
			$credit['notes'] = 'Net Tips Payable';
			$this->put_journal_entry($credit);
		}elseif($transaction['balance']==0){
			$ntp = $this->get_net_tips_payable_account($course_id);
			$ntp_id = $ntp['account_id'];
			$credit = $this->blank_journal_entry();
			$credit['account_id'] = $ntp_id;
			$credit['amount'] = $transaction['balance'];
			$credit['d_c'] = 'credit';
			$credit['notes'] = 'Net Tips Payable';
			$credit['deleted'] = 1;
			$this->delete_journal_entry($credit);
		}

		$this->commit();
	}

	// retrieves a transaction for an employee to share their tips
	public function tip_share_transaction($date_string = null,$tzo = 0)
	{
		$CI = get_instance();
		$jr = new \fu\tips\TipsJournalReport();
		if(!isset($date_string)) {
			$start_time = date('Y-m-d') . ' 00:00:00';
			$end_time = date('Y-m-d') . ' 23:59:59';
		}else{
			// put start and end time into GMT
			$offset_seconds = $tzo * 60 * 60;
			$timestamp = strtotime($date_string)+$offset_seconds;
			$day_string = date('Y-m-d',$timestamp - $offset_seconds);
			$GMT_day = strtotime($day_string);
			$start_time = date('Y-m-d H:i:s',$GMT_day+ $offset_seconds);
			$end_time = date('Y-m-d H:i:s',$GMT_day+ $offset_seconds + 24 * 60 * 60);
		}
		$jr->setParams($start_time,$end_time,$CI->session->userdata('person_id'));
		$data = $jr->getData(true);
		$summary = $jr->getSummaryData();
		$emp_info = $CI->Employee->get_logged_in_employee_info();
		$notes = 'Tips Payable: '.$emp_info->last_name.', '.$emp_info->first_name;
		$tid = null;
		foreach($data as $row){
			if($row['transaction_notes']==='Share Tips'){
				$tid = $row['transaction_id'];
				break;
			}
		}
		if(!isset($tid)){
			$CI->load->model('v2/cart_model');
			$response = $CI->cart_model->save_sale('Share Tips',true);
			$sale_id = $response['sale_id'];
			$tid = $this->put($sale_id,$CI->session->userdata('emp_id'),null,null,'Share Tips');
			$this->set_transaction_id($tid);
			$debit = $this->blank_journal_entry();
			$debit['d_c'] = 'debit';
			$debit['employee_id'] = $CI->session->userdata('emp_id');
			$debit['amount']=0;
			$debit['notes'] = $notes;
			$this->put_journal_entry($debit);
			$this->commit();
		}else {
			$this->set_transaction_id($tid);
		}
		$result = $this->get($tid);
		// single result expected
		$result = $result[0];
		$result['journal_entries'] = $this->get_journal_entries();
		$result['total'] = $summary['total'];
		$result['total_debits'] = $jr->get_employee_debit_total();
		$result['total_credits'] = $jr->get_employee_credit_total();

		return $result;
	}

}

