<?php
// TipsJournal.php
// Specializing in Recording Debits and Credits relating to tips
namespace fu\tips;

class TipsJournal extends \fu\accounting\Journal {
	function __construct($table)
	{
		parent::__construct($table);
	}

	private function validate_post($transaction_id,$d_c,$amount,$account_id,$employee_id, $notes,$comment,$method_string = 'post'){
		// required
		// transaction_id,d_c,amount, account_id || employee_id
		if(!isset($employee_id) && !isset($account_id)){
			$this->last_error = 'TipsJournal->'.$method_string.' Error: either account_id or employee id must be set.';
			return false;
		}
		if(!$this->validate_id($transaction_id)){
			$this->last_error = 'TipsJournal->'.$method_string.' Error: transaction_id is required, and must be an integer.';
			return false;
		}
		if(!isset($d_c) || !in_array($d_c,array('debit','credit'))){
			$this->last_error = 'TipsJournal->'.$method_string.' Error: d_c must be a string with the value "debit" or "credit".';
			return false;
		}
		if(!is_numeric($amount) || $amount < 0){
			$this->last_error = 'TipsJournal->'.$method_string.' Error: amount must be a non-negative numeric value.';
			return false;
		}


		if(isset($account_id) && !$this->validate_id($account_id)){
			$this->last_error = 'TipsJournal->'.$method_string.' Error: account_id must be an integer.';
			return false;
		}
		if(isset($employee_id) && !$this->validate_id($employee_id)){
			$this->last_error = 'TipsJournal->'.$method_string.' Error: employee_id must be an integer.';
			return false;
		}
		if(isset($notes) && !is_string($notes)){
			$this->last_error = 'TipsJournal->'.$method_string.' Error: notes must be a string.';
			return false;
		}
		if(isset($comment) && !is_string($comment)){
			$this->last_error = 'TipsJournal->'.$method_string.' Error: comment must be a string.';
			return false;
		}

		return true;
	}

	public function post($transaction_id,$d_c,$amount,$account_id = null,$employee_id = null, $notes = null,$comment = null, $validate_only = false){
		$valid = $this->validate_post($transaction_id,$d_c,$amount,$account_id,$employee_id, $notes,$comment,'post');
		if(!$valid)return false;

		if($validate_only)return true;

		$params = array(
			'transaction_id'=>$transaction_id,
			'd_c'=>$d_c,
			'amount'=>$amount,
			'account_id'=>$account_id,
			'employee_id'=>$employee_id,
			'notes'=>$notes,
			'comment'=>$comment
		);

		$success = false;
		try {
			$success = $this->CI->db->insert($this->table, $params);
		}catch(\Exception $e){
			$this->last_error = 'TipsJournal->post Error: '.$e->getMessage();
			return false;
		}

		if($success) {
			return true;
		}
		else{
			$this->last_error = 'TipsJournal->post Error: '.$this->CI->db->_error_message();
		}
		return false;
	}

	public function put($transaction_id,$d_c,$amount,$account_id = null, $employee_id = null, $notes = null,$comment = null, $validate_only = false){
        $valid = $this->validate_post($transaction_id,$d_c,$amount,$account_id,$employee_id, $notes,$comment,'put');
        if(!$valid)return false;
        if($validate_only)return true;

		$params = array(
			'transaction_id'=>$transaction_id,
			'd_c'=>$d_c,
			'amount'=>$amount,
			'account_id'=>$account_id,
			'employee_id'=>$employee_id,
			'notes'=>$notes,
			'comment'=>$comment,
			'deleted'=>0
		);

		$constraints = array('transaction_id'=>$transaction_id);
		$account_id?$constraints['account_id']=$account_id:null;
		$employee_id?$constraints['employee_id']=$employee_id:null;
		$results = $this->get($constraints,true);
		if(!empty($results)){
			try {
				$success = $this->CI->db->update($this->table, $params, $constraints);
			} catch (\Exception $e) {
				$this->last_error = 'TipsJournal->put Error: ' . $e->getMessage();
				return false;
			}

			if($success) {
				return true;
			}
		}
		else{
			try {
				$success = $this->CI->db->insert($this->table, $params);
			}catch(\Exception $e){
				$this->last_error = 'TipsJournal->put Error: '.$e->getMessage();
				return false;
			}

			if($success) {
				return true;
			}
		}

        return false;
	}

	public function delete($transaction_id,$account_id = null,$employee_id = null, $hard = false, $validate_only = false){
		$params = array('transaction_id'=>$transaction_id);

		if(!isset($employee_id) && !isset($account_id)){
			$this->last_error = 'TipsJournal->delete Error: either account_id or employee_id must be set.';
			return false;
		}
		if(!$this->validate_id($transaction_id)){
			$this->last_error = 'TipsJournal->delete Error: transaction_id is required, and must be an integer.';
			return false;
		}
		if(isset($account_id) && !$this->validate_id($account_id)){
			$this->last_error = 'TipsJournal->delete Error: account_id must be an integer.';
			return false;
		}if(isset($account_id)){
			$params['account_id'] = $account_id;
		}
		if(isset($employee_id) && !$this->validate_id($employee_id)){
			$this->last_error = 'TipsJournal->delete Error: employee_id must be an integer.';
			return false;
		}elseif (isset($employee_id)){
			$params['employee_id'] = $employee_id;
		}

		if($validate_only)return true;

		try {
			$this->CI->db->where('transaction_id', $transaction_id);
			if ($account_id) {
				$this->CI->db->where('account_id', $account_id);
			}
			if ($employee_id) {
				$this->CI->db->where('employee_id', $employee_id);
			}
			if ($hard) {
				$success = $this->CI->db->delete($this->table);
			} else {
				$success = $this->CI->db->update($this->table, array('deleted' => true));
			}
		}
		catch(\Exception $e){
			$this->last_error = 'TableOfAccounts->delete Error: '.$e->getMessage();
			return false;
		}

		if($success) {
			$num = $this->CI->db->affected_rows();
			return $this->CI->db->affected_rows();
		}
		return false;
	}

	public function get($parameters,$inc_deleted = false, $validate_only = false){
		if(!is_array($parameters)){
			$this->last_error = 'TipsJournal->get Error: expecting array of query parameters.';
			return false;
		}
		$parameters = array_intersect_key($parameters,array('transaction_id'=>'_not_set','d_c'=>'_not_set','amount'=>'_not_set','account_id'=>'_not_set','employee_id'=>'_not_set','notes'=>'_not_set','comment'=>'_not_set','test_unvalidated'=>'_not_set'));
		$parameters = array_filter($parameters,function($var){
			return $var !== '_not_set';
		});
		if(empty($parameters)){
			$this->last_error = 'TipsJournal->get Error: expecting array of query parameters.';
			return false;
		}
		foreach($parameters as $key=>$value){
			if(in_array($key,array('transaction_id','account_id','employee_id')) && !$this->validate_id($value)){
				$this->last_error = 'TipsJournal->get Error: '.$key.' must be an integer.';
				return false;
			}elseif (in_array($key,array('d_c','notes','comment')) && !is_string($value)){
				$this->last_error = 'TipsJournal->get Error: '.$key.' must be a string.';
				return false;
			}elseif ($key === 'd_c' && !in_array($value,array('debit','credit'))){
				$this->last_error = 'TipsJournal->get Error: d_c must be a string with the value "debit" or "credit".';
				return false;
			}elseif ($key==='amount' && !is_numeric($value) || $value<0){
				$this->last_error = 'TipsJournal->get Error: amount must be a non-negative numeric value.';
				return false;
			}elseif(!in_array($key,array('transaction_id','d_c','amount','account_id','employee_id','notes','comment'))){
				throw new \Exception('TipsJournal->get UserError: un-validated parameter: '.$key.'. Please add validation.');
			}
		}

		if($validate_only)return true;

		$this->CI->db->select()->from($this->table);
		if(!$inc_deleted){
			$this->CI->db->where('deleted',0);
		}
		$this->CI->db->where($parameters);

		$ret = false;
		try {
			$get = $this->CI->db->get();
			$ret = $get->result_array();
		}
		catch(\Exception $e){
			$this->last_error = 'TipsJournal->get Error: '.$e->getMessage();
			return false;
		}

		return $ret;
	}

	public function patch($parameters,$validate_only = false){
		$parameters2 = array_merge(array('transaction_id'=>null,'d_c'=>null,'amount'=>null,
			'account_id'=>null,'employee_id'=>null,'notes'=>null,'comment'=>null),$parameters);

		$parameters = array_merge(array('transaction_id'=>'_not_set','d_c'=>'_not_set','amount'=>'_not_set',
			'account_id'=>'_not_set','employee_id'=>'_not_set','notes'=>'_not_set','comment'=>'_not_set'),$parameters);

		$parameters = array_filter($parameters,function($var){
			return $var !== '_not_set';
		});

		$valid = $this->validate_post($parameters2['transaction_id'],'debit',1,
			$parameters2['account_id'],$parameters2['employee_id'],
			$parameters2['notes'],$parameters2['comment'],'patch');

		if(isset($parameters['d_c']) && !in_array($parameters['d_c'],array('debit','credit'))){
			$this->last_error = 'TipsJournal->patch Error: d_c must be a string with the value "debit" or "credit".';
			return false;
		}
		if(isset($parameters['amount']) && (!is_numeric($parameters['amount']) || $parameters['amount'] < 0)){
			$this->last_error = 'TipsJournal->patch Error: amount must be a non-negative numeric value.';
			return false;
		}
		if(!$valid)return false;
		if($validate_only)return true;

		$constraints = array('transaction_id'=>$parameters['transaction_id']);
		$parameters['account_id']?$constraints['account_id']=$parameters['account_id']:null;
		$parameters['employee_id']?$constraints['employee_id']=$parameters['employee_id']:null;
		try {
			$success = $this->CI->db->update($this->table, $parameters, $constraints);
		} catch (\Exception $e) {
			$this->last_error = 'TipsJournal->put Error: ' . $e->getMessage();
			return false;
		}

		if($success) {
			return true;
		}

		return false;
	}
}
?>