<?php

namespace fu\acl;


use BeatSwitch\Lock\Callers\Caller;

class User implements Caller
{
    public function __construct($person_id)
    {
        $this->person_id = $person_id;
        $this->CI = & get_instance();
    }

    public function getCallerType()
    {
        return 'users';
    }

    public function getCallerId()
    {
        return $this->person_id;
    }

    public function getCallerRoles()
    {
        if(is_array($this->CI->session->userdata('roles')))
            return $this->CI->session->userdata('roles');
        else {
            $roles = $this->CI->db->from("acl_roles_employees")
                ->select("role_id")
                ->where("employee_id",$this->person_id)
                ->get()
                ->result_array();
            $role_array = [];
            if(count($roles) > 0){
                foreach($roles as $role){
                    $role_array[] = $role['role_id'];
                }
            }
            return $role_array;
        }
    }
}