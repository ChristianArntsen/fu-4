<?php
namespace fu\acl;

use BeatSwitch\Lock\Callers\Caller;
use BeatSwitch\Lock\Drivers\Driver;
use BeatSwitch\Lock\Permissions\Permission;
use BeatSwitch\Lock\Permissions\PermissionFactory;
use BeatSwitch\Lock\Roles\Role;

class CIDriver implements Driver
{
    private $CI;

    public function __construct()
    {
        $this->CI = & get_instance();
    }

    /**
     * Returns all the permissions for a caller
     *
     * @param \BeatSwitch\Lock\Callers\Caller $caller
     * @return \BeatSwitch\Lock\Permissions\Permission[]
     */
    public function getCallerPermissions(Caller $caller)
    {
        $permissions = $this->CI->db->from("foreup_acl_caller_permissions")
            ->where("caller_type",$caller->getCallerType())
            ->where("caller_id",$caller->getCallerId())
            ->get();

        return PermissionFactory::createFromData($permissions->result_array());
    }

    /**
     * Stores a new permission into the driver for a caller
     *
     * @param \BeatSwitch\Lock\Callers\Caller $caller
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return void
     */
    public function storeCallerPermission(Caller $caller, Permission $permission)
    {
        $persistedPermission = new \models\AclCallerPermissions\Row();
        $persistedPermission->caller_type = $caller->getCallerType();
        $persistedPermission->caller_id = $caller->getCallerId();
        $persistedPermission->type = $permission->getType();
        $persistedPermission->action = $permission->getAction();
        $persistedPermission->resource_type = $permission->getResourceType();
        $persistedPermission->resource_id = $permission->getResourceId();

        $this->CI->db->insert('acl_caller_permissions',$persistedPermission);
    }

    /**
     * Removes a permission from the driver for a caller
     *
     * @param \BeatSwitch\Lock\Callers\Caller $caller
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return void
     */
    public function removeCallerPermission(Caller $caller, Permission $permission)
    {
        $this->CI->db
            ->where('caller_type', $caller->getCallerType())
            ->where('caller_id', $caller->getCallerId())
            ->where('type', $permission->getType())
            ->where('action', $permission->getAction())
            ->where('resource_type', $permission->getResourceType())
            ->where('resource_id', $permission->getResourceId());

        $this->CI->db->delete("acl_caller_permissions");
    }

    /**
     * Checks if a permission is stored for a user
     *
     * @param \BeatSwitch\Lock\Callers\Caller $caller
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return bool
     */
    public function hasCallerPermission(Caller $caller, Permission $permission)
    {
        return (bool) $this->CI->db->from("acl_caller_permissions")
            ->where('caller_type', $caller->getCallerType())
            ->where('caller_id', $caller->getCallerId())
            ->where('type', $permission->getType())
            ->where('action', $permission->getAction())
            ->where('resource_type', $permission->getResourceType())
            ->where('resource_id', $permission->getResourceId())
            ->get()
            ->row();
    }

    /**
     * Returns all the permissions for a role
     *
     * @param \BeatSwitch\Lock\Roles\Role $role
     * @return \BeatSwitch\Lock\Permissions\Permission[]
     */
    public function getRolePermissions(Role $role)
    {
        $permissions = $this->CI->db->from("acl_role_permission")
            ->where("role_id", $role->getRoleName())
            ->get();

        return PermissionFactory::createFromData($permissions->result_array());

    }

    /**
     * Stores a new permission for a role
     *
     * @param \BeatSwitch\Lock\Roles\Role $role
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return void
     */
    public function storeRolePermission(Role $role, Permission $permission)
    {

        $persistedPermission = new \models\AclRolePermissions\Row();
        $persistedPermission->role_id = $role->getRoleName();
        $persistedPermission->type = $permission->getType();
        $persistedPermission->action = $permission->getAction();
        $persistedPermission->resource_type = $permission->getResourceType();
        $persistedPermission->resource_id = $permission->getResourceId();

        $this->CI->db->insert('acl_role_permission',$persistedPermission);
    }

    /**
     * Removes a permission for a role
     *
     * @param \BeatSwitch\Lock\Roles\Role $role
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return void
     */
    public function removeRolePermission(Role $role, Permission $permission)
    {
        $this->CI->db
            ->where('role_id', $role->getRoleName())
            ->where('type', $permission->getType())
            ->where('action', $permission->getAction())
            ->where('resource_type', $permission->getResourceType())
            ->where('resource_id', $permission->getResourceId());

        $this->CI->db->delete("acl_role_permission");
    }

    /**
     * Checks if a permission is stored for a role
     *
     * @param \BeatSwitch\Lock\Roles\Role $role
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return bool
     */
    public function hasRolePermission(Role $role, Permission $permission)
    {
        return (bool) $this->CI->db->from("acl_role_permission")
            ->where('role_id', $role->getRoleName())
            ->where('type', $permission->getType())
            ->where('action', $permission->getAction())
            ->where('resource_type', $permission->getResourceType())
            ->where('resource_id', $permission->getResourceId())
            ->get()
            ->row();
    }
}