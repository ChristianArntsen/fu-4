<?php

namespace fu\acl;


use fu\Exceptions\InvalidArgument;

class Permissions
{
    protected $allowedActions,$actions,$humanReadableName;

    public function __construct(Array $actions)
    {
        $this->setActions($actions);
    }

    public function setActions(Array $actions)
    {
        foreach($actions as $action){
            if(!in_array($action,$this->allowedActions)){
                throw new InvalidArgument("The action (".$action.") is not allowed.  Possible actions: ".implode(",",$this->allowedActions));
            }
        }

        $this->actions = $actions;
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function getActionStrings()
    {
        return implode(",",$this->actions);
    }

    public function getResourceName()
    {
        return  get_class($this);
    }

    public function getHumanReadableName()
    {
        if(empty($this->humanReadableName))
            return get_class($this);
        return $this->humanReadableName;
    }

    public function getAvailableActions()
    {
		return $this->allowedActions;
    }
}