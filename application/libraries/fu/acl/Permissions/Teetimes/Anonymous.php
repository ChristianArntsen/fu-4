<?php

namespace fu\acl\Permissions\Teetimes;

class Anonymous extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["create"];
        $this->humanReadableName = "Tee time without a customer";
        parent::__construct($actions);
    }

}