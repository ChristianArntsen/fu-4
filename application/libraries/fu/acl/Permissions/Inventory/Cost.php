<?php

namespace fu\acl\Permissions\Inventory;

class Cost extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["update"];
        $this->humanReadableName = "Inventory Cost";
        parent::__construct($actions);
    }

}