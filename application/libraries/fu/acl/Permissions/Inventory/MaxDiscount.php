<?php

namespace fu\acl\Permissions\Inventory;

class MaxDiscount extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["update"];
        $this->humanReadableName = "Max Discount";
        parent::__construct($actions);
    }

}