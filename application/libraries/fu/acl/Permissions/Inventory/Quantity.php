<?php

namespace fu\acl\Permissions\Inventory;

class Quantity extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["update"];
        $this->humanReadableName = "Inventory Quantity";
        parent::__construct($actions);
    }

}