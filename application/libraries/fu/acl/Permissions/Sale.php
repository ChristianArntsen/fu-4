<?php

namespace fu\acl\Permissions;

class Sale extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["delete_wo_reason"];
        $this->humanReadableName = "Sale";
        parent::__construct($actions);
    }

}