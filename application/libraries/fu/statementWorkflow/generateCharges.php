<?php
namespace fu\statementWorkflow;

use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupRepeatable;
use foreup\rest\models\entities\ForeupRepeatableAccountRecurringChargeItems;
use foreup\rest\resource_transformers\account_recurring_charge_customers_transformer;
use foreup\rest\resource_transformers\repeatable_transformer;
use fu\statementWorkflow\interfaces\queue_interface;
use \models\Repeatable\Row as RepeatableObject;


class generateCharges
{
	use \fu\statementWorkflow\traits\daemon_trait;
	private $queue;

	private $queue_name;
	private $error_queue_name;

	public $run_cycles = 0;
	public $messages_received = 0;
	public $messages_sent = 0;

	public function __construct(queue_interface &$queue)
	{
		$prefix = '';
		$production = true;
		if(ENVIRONMENT !== 'production'){
			$prefix = ENVIRONMENT.'_';
			$production = false;
		}

		$this->queue = $queue;
		$this->queue_name = $prefix.'ForeupRepeatableAccountRecurringChargeItemsToCustomers';
		$this->queue->getQueue($this->queue_name,$production);

		$this->error_queue_name = $prefix.'ForeupRepeatableAccountRecurringChargeItemsToCustomersError';
		$this->queue->getQueue($this->error_queue_name,$production);

		$this->silex = new \fu\statementWorkflow\classes\silex_handler();

		$this->repTransformer = new repeatable_transformer();
		$this->cstTransformer = new account_recurring_charge_customers_transformer();

        date_default_timezone_set('UTC');
	}

	public function _get_queue(){
		return $this->queue;

	}

	public function run($current_time = null, $test = false)
	{
	    $this->run_cycles++;
		// get charges from database
		$repeatables = $this->fetchRepeatables($current_time);
		$courses = $this->silex->db->getRepository('e:ForeupCourses');

		$ro = new RepeatableObject();

		$count = 0;
		foreach($repeatables->getIterator() as $rep){
			// submit jobs to the queue
			$array = $this->repeatableEntityToArray($rep);
			$array['id'] = $rep->getId();
			$item = $rep->getItem();
			$charge = $item->getRecurringCharge();

			if(!empty($charge->getDateDeleted())){
				continue;
			}

            $nextOccurrence = $rep->getNextOccurence();
            if(empty($nextOccurrence)){
                continue;
            }
            $nextOccurrence = $nextOccurrence->format(DATE_ISO8601);
			$count++;
			$this->messages_received++;

			// do not process deleted recurring charges
			if($charge->getDeletedBy()){
				continue;
			}
			$customers = $charge->getAccountRecurringChargeCustomers();

			if(isset($customers)) {
				$customers = $this->customersEntityToArray($customers);
				if(empty($customers)){
					$warning = [
						'message'=>'No customers added to recurring charge',
						'accountRecurringCharge'=>$charge->getId(),
						'organizationId'=>$charge->getOrganizationId()
					];
					$this->queue->enqueueMessage($this->error_queue_name,\json_encode($warning));
				}
				foreach($customers as $customer) {
					$this->queue->enqueueMessage($this->queue_name, \json_encode(['repeatable' => $array, 'customer' => $customer,'current_time'=>$nextOccurrence]));
					$this->messages_sent++;
				}

			}

			if(isset($current_time) && !is_string($current_time))
				$current_time = $current_time->format(\DATE_ISO8601);


			$course_id = $charge->getOrganizationId();
			$course = $courses->find($course_id);
			$time_zone = $course->getTimezone();
			//if(!$test) {
				// update last ran for the repeatable object
				$last_ran = $rep->getNextOccurence();
				$last_string = $last_ran->format('Y-m-d H:i:s');
				$old = new \DateTime($last_string . 'America/Denver');
				$old->setTimezone(new \DateTimeZone($time_zone));
				// does next calc occur
				$next_time = $this->getNextOccurence($old,$rep);
				$nu = $next_time;
                if(isset($next_time)) {
	                $next_string = $next_time->format('Y-m-d H:i:s');
	                $nu = new \DateTime($next_string . $time_zone);
	                $nu->setTimezone(new \DateTimeZone('America/Denver'));
                }
				$rep->setNextOccurence($nu);
				$rep->setLastRan($last_ran);

				// propagage change to database
				$this->silex->db->flush();

			//}
		}

		return $count;

	}

	public function checkCourseTime(ForeupCourses $course,ForeupRepeatable $rep,$currentTime){
		if(is_string($currentTime)){
			$ct = new \DateTime($currentTime);
		}else{
			$ct = $currentTime;
		}
		$lastRan = $rep->getLastRan();
		$nextOccurence = $rep->getNextOccurence();
		$dtstart = $rep->getDtstart();

		$cou = true;
		if($lastRan == $nextOccurence){
			$cou = false;
		}
		return $cou;
	}

	public function getNextOccurence($current_time,ForeupRepeatable $repeatable)
	{
		$array = $repeatable->toRuleArray();
		if(!isset($current_time))$current_time = new \DateTime();
		$str_time = $current_time;
		if(!is_string($current_time)){
			$str_time = $current_time->format(DATE_ISO8601);
		}
		$ro = new RepeatableObject();
		$ro->createFromArray($array);
		$cur_string = $current_time->format(DATE_ISO8601);
		$next_time = $ro->getNextCalculatedOccurrence($str_time);
		if(strtotime($str_time) >= strtotime($next_time->format(DATE_ISO8601))){
			$tmp = new \DateTime($current_time->format(DATE_ISO8601));
			$tmp->add(new \DateInterval('PT1S'));
			$next_time = $ro->getNextCalculatedOccurrence($tmp->format(DATE_ISO8601));
		}
		return $next_time;
	}


	public function fetchRepeatableCustomers(ForeupRepeatableAccountRecurringChargeItems $repeatable){
		$charge = $repeatable->getItem()->getRecurringCharge();
		return $this->fetchRecurringChargeCustomers([$charge]);

	}

	public function fetchRepeatables($current_time = null){

		$repo = $this->silex->db->getRepository('e:ForeupRepeatableAccountRecurringChargeItems');
		$criteria = new \Doctrine\Common\Collections\Criteria();

		if(is_string($current_time))
			$current_time = new \DateTime($current_time);
		//$tmp = $current_time->setTimezone(new \DateTimeZone('America/New_York'));
		$criteria->andWhere($criteria->expr()->orX(
			$criteria->expr()->lt('lastRan',$current_time),
			$criteria->expr()->eq('lastRan',null)
		));
		$criteria->andWhere($criteria->expr()->orX(
			$criteria->expr()->lte('nextOccurence',$current_time),
			$criteria->expr()->eq('nextOccurence',null)
		));
		$criteria->andWhere($criteria->expr()->orX(
			$criteria->expr()->lte('dtstart',$current_time),
			$criteria->expr()->eq('dtstart',null)
		));

		return $repo->matching($criteria);

	}

	public function fetchItems($repeatables){
		$repo = $this->silex->db->getRepository('e:ForeupAccountRecurringChargeItems');
		$criteria = new \Doctrine\Common\Collections\Criteria();

		$criteria->expr()->in('repeated',$repeatables->toArray());

		return $repo->matching($criteria);

	}

	public function fetchCharges($items){
		$repo = $this->silex->db->getRepository('e:ForeupAccountRecurringCharges');
		$criteria = new \Doctrine\Common\Collections\Criteria();

		$criteria->expr()->in('repeated',$items->toArray());

		return $repo->matching($criteria);

	}


	public function fetchRecurringChargeCustomers($charges){
		$repo = $this->silex->db->getRepository('e:ForeupAccountRecurringChargeCustomers');
		$criteria = new \Doctrine\Common\Collections\Criteria();

		if(method_exists($charges,'toArray'))
			$charges = $charges->toArray();
		
		$criteria->expr()->in('recurringCharge',$charges);
		$ret = $repo->matching($criteria);
		return $ret;

	}

	public function repeatableEntityToArray (ForeupRepeatable $entity )
	{
		$array = $entity->toRuleArray();
		return $array;

	}

	public function customersEntityToArray ($entities )
	{
		$array = [];
		if(method_exists($entities,'toArray')){
			$entities = $entities->toArray();
		}
		foreach($entities as $entity) {
			$array[] = $this->cstTransformer->transform($entity);
		}
		return $array;

	}
}

?>