<?php
// dispatcher.php
// Provides a dispatch mechanism for messages, allowing a single queue to be used

namespace fu\statementWorkflow;

use foreup\rest\resource_transformers\employees_transformer;
use fu\statementWorkflow\classes\SqsQueue;
use fu\statementWorkflow\interfaces\daemon_interface;
use fu\statementWorkflow\interfaces\queue_interface;
use fu\statementWorkflow\traits\daemon_trait;
use fu\statementWorkflow\traits\sqs_trait;
use fu\statementWorkflow\classes\silex_handler;

require_once './interfaces/queue_interface.php';
require_once './interfaces/daemon_interface.php';
require_once './traits/sqs_trait.php';

class dispatcher implements daemon_interface
{
	use daemon_trait;

	private $count = 0,$silex,$sqs;

	public function __construct()
	{
		$this->sqs = new SqsQueue();

		$this->sqs->getQueue('test-queue');

		$this->silex = new silex_handler();
	}

	public function run ()
	{
		// get the next message in the queue

		// if no message, return false

		// dispatch the message

		// generate statement
		// email statement
		// generate pdf


		$this->enqueue('some message');

		$res = $this->dequeue();
		print_r($res);

		foreach($res as $message){
			$result = $this->delete($message);
		}
		print "\n$this->count\n";
		if($this->count > 1){
			$this->apoptosis = true;
			$res = $this->sqs->deleteQueue('test-queue');
			print_r($res);
		}
		$this->count++;

		$res = $this->silex->db->getRepository('e:ForeupEmployees')->find(5);
		$transformer = new employees_transformer();
		$res = $transformer->transform($res);
		//print_r( $res);

		// see application/controllers/daemon.php ~1077 for setting course_id
		$CI = \get_instance();
		$CI->load->model('v2/employee_model');
		$res = $CI->employee_model->get(['person_id'=>12]);

		//print_r( $CI->session);

		return false;
	}

	public function dispatch($message){
		// send the message to the proper handler
	}

	public function enqueue($message)
	{
		return $this->sqs->enqueueMessage('test-queue',$message);
	}

	public function dequeue()
	{
		return $this->sqs->fetchMessages('test-queue');
	}

	public function delete ($message)
	{
		return $this->sqs->deleteMessage('test-queue',$message);
	}
}

$dsp = new dispatcher();

$dsp->runAsDaemon();

?>