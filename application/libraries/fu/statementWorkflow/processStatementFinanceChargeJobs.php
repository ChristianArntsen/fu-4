<?php
namespace fu\statementWorkflow;

use foreup\rest\models\entities\ForeupAccountStatements;
use fu\statementWorkflow\classes\AccountCharger;
use fu\statementWorkflow\classes\silex_handler;
use fu\statementWorkflow\interfaces\queue_interface;
use fu\statementWorkflow\traits\daemon_trait;

class processStatementFinanceChargeJobs
{
	use daemon_trait;

	private $queue;
	private $input_queue_name;
	private $error_queue_name;

	/**
	 * @var AccountCharger
	 */
	private $charger;

	/**
	 * @var silex_handler
	 */
	public $silex;

	public $run_cycles = 0;
	public $messages_received = 0;
	public $messages_sent = 0;

	public function __construct(queue_interface &$queue)
	{
		$prefix = '';
		$production = true;
		if(ENVIRONMENT !== 'production'){
			$prefix = ENVIRONMENT.'_';
			$production = false;
		}

		$this->queue = $queue;
		$this->input_queue_name = $prefix.'processStatementFinanceChargeJobs';
		$this->queue->getQueue($this->input_queue_name,$production);

		$this->error_queue_name = $prefix.'processStatementFinanceChargeJobsErrors';
		$this->queue->getQueue($this->error_queue_name,$production);

		$this->silex = new silex_handler();
		$this->charger = new AccountCharger($this->silex);
		$this->CI = \get_instance();

		date_default_timezone_set('UTC');
	}

	public function _get_queue()
	{
		return $this->queue;

	}

	public function _set_input_queue(queue_interface &$queue)
	{
		$this->queue = &$queue;

	}

	private function handle_error($result)
	{
		$this->queue->enqueueMessage($this->error_queue_name, \json_encode($result));

	}

	public function run($current_time = null, $test = false)
	{
		$this->run_cycles++;
		// fetch messages from input queue
		$messages = $this->queue->fetchMessages($this->input_queue_name);

		if(!isset($current_time)) {
			$current_time = new \DateTime();
		}
		elseif(is_string($current_time)){
			$current_time = new \DateTime($current_time);
		}

		$count = 0;
		foreach($messages as $message) {
			$count++;
			$this->messages_received++;
			// extract message contents
			$msg_array = $this->extractMessage($message);

			// do sutuf with the statement

			$cur = new \DateTime($msg_array['statement']->getDueDate()->format(DATE_ISO8601));
			$cur->add(new \DateInterval('P'.$msg_array['statement']->getFinanceChargeAfterDays().'D'));

			if(!empty($this->charger->applyFinanceCharge($msg_array['statement'],$cur,$test))) {
				$this->messages_sent++;
			}
		}

		return $count;

	}

	/**
	 * @param $message
	 * @return array|mixed
	 */
	public function extractMessage($message)
	{
		$ret = [];
		$body = 'null';
		if(method_exists($message,'get'))
			$body = $message->get('Body');
		elseif (is_array($message)){
			$body = $message['Body'];
		}
		$intermediate = \json_decode($body,true);

		if(isset($intermediate['groupEnd']))return $intermediate;

		$rep = $this->silex->db->getRepository('e:ForeupAccountStatements');
		$cst = $this->silex->db->getRepository('e:ForeupCustomers');
		$ret['statement'] = $rep->find($intermediate['id']);
		$ret['courseId'] = $ret['statement']->getRecurringStatement()->getRecurringCharge()->getOrganizationId();
		$ret['customer'] =  $cst->findOneBy(['person'=>$intermediate['personId'],'course'=>$ret['courseId']]);
		$ret['original'] = $intermediate;

		return $ret;

	}
}

?>
