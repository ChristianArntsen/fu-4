<?php
namespace fu\statementWorkflow\classes;

use DoctrineProxies\__CG__\foreup\rest\models\entities\ForeupStatements;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupCourseGroups;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\models\entities\ForeupItems;
use foreup\rest\resource_transformers\items_transformer;

class AccountCharger
{
	private $silex;

	public function __construct(silex_handler &$silex)
	{
		$this->silex = $silex;
		$this->itmTransformer = new items_transformer();

		$this->silex->CI->load->model('invoice');
	}

	public function associateInvoiceWithRecurringCharge(ForeupAccountRecurringCharges $charge,$invoice_id,$test = false)
	{
		$inv = $this->silex->db->getRepository('e:ForeupInvoices');

		if($test) {
			// if this is a test, invoice will not be in the db
			$invoice = new ForeupInvoices();
			$invoice->setRecurringCharge($charge);

		}
		else {
			$invoice = $inv->find($invoice_id);
			$invoice->setRecurringCharge($charge);
			$this->silex->db->flush($invoice);

		}

		return $invoice;

	}

	public function payAccountBalances(ForeupCustomers $customer,ForeupAccountRecurringStatements $statement,\DateTime $currentTime = null,$test = false)
	{
		if(empty($currentTime))$currentTime = new \DateTime();

		$results = [];

		if($statement->getPayCustomerBalance()){
			$item = $this->getCustomerBalanceItem($statement);
			$name = $item->getName().' Payment';
			$invoice = $this->payBalance($statement->getOrganizationId(),$item,$customer,$name,- $customer->getAccountBalance(),$currentTime,$test);

			$results[] = $this->associateInvoiceWithCharge($statement->getRecurringCharge(),$invoice->getId(),$test);
		}
		if($statement->getPayMemberBalance()){
			$item = $this->getMemberBalanceItem($statement);
			$name = $item->getName().' Payment';
			$invoice = $this->payBalance($statement->getOrganizationId(),$item,$customer,$name,- $customer->getMemberAccountBalance(),$currentTime,$test);

			$results[] = $this->associateInvoiceWithCharge($statement->getRecurringCharge(),$invoice->getId(),$test);
		}
		return $results;

	}

	public function createSpecialItem($organization_id,$item_number,$pretty)
	{
		$item = new ForeupItems();
		$item->setCourseId($organization_id);
		$item->setItemNumber($item_number);
		$item->setIsUnlimited(true);
		$item->setInvisible(true);
		$item->setDepartment($pretty);
		$item->setCategory($pretty);
		$item->setSubcategory($pretty);
		$item->setName($pretty);
		$item->setDescription($pretty);
		$item->setCostPrice(0.00);
		$item->setUnitPrice(0.00);
		$item->setIsUnlimited(1);
		$item->setInvisible(1);
		$item->setCid(0);
		$item->setImageId(0);
		$item->setUnitPriceIncludesTax(0);
		$item->setLocation('');
		$item->setAllowAltDescription(0);
		$item->setIsSerialized(0);
		$item->setIsGiftcard(0);
		$item->setSoupOrSalad(0);
		$item->setIsSide(0);
		$item->setAddOnPrice(0);
		$item->setPrintPriority(1);
		$item->setIsFee(0);
		$item->setKitchenPrinter(1);
		$item->setQuickbooksAssets('');
		$item->setQuickbooksIncome('');
		$item->setQuickbooksCogs('');
		$item->setErangeSize(0);

		$this->silex->db->persist($item);
		$this->silex->db->flush();

		return $item;
	}

	public  function getCustomerBalanceItem(ForeupAccountRecurringStatements $statement)
	{
		$repo = $this->silex->db->getRepository('e:ForeupItems');
		$items = $repo->findBy([
			'courseId'=>$statement->getOrganizationId(),
			'itemNumber'=>'customer_balance'
		]);

		if(!empty($items[0])){
			$item = $items[0];
		}else{
			$item = $this->createSpecialItem($statement->getOrganizationId(),'customer_balance','Customer Balance');
		}

		return $item;
	}

	public  function getMemberBalanceItem(ForeupAccountRecurringStatements $statement)
	{
		$repo = $this->silex->db->getRepository('e:ForeupItems');
		$items = $repo->findBy([
			'courseId'=>$statement->getOrganizationId(),
			'itemNumber'=>'member_balance'
		]);

		if(!empty($items[0])){
			$item = $items[0];
		}else{
			$item = $this->createSpecialItem($statement->getOrganizationId(),'member_balance','Member Balance');
		}

		return $item;
	}

	public function getFinanceChargeItem(ForeupAccountStatements $statement)
	{
		$repo = $this->silex->db->getRepository('e:ForeupItems');


		$items = $repo->findBy([
			'courseId'=>$statement->getOrganizationId(),
			'itemNumber'=>'FinanceCharge'
		]);
		if(isset($items[0]))
			return $items[0];
		else{
			$item = new ForeupItems();
			$item->setCourseId($statement->getOrganizationId());
			$item->setItemNumber('FinanceCharge');
			$item->setIsUnlimited(true);
			$item->setInvisible(true);
			$item->setDepartment('Finance Charges');
			$item->setCategory('Finance Charges');
			$item->setSubcategory('Finance Charges');
			$item->setName('Finance Charge');
			$item->setDescription('Finance Charge');
			$item->setCostPrice(0.00);
			$item->setUnitPrice(0.00);
			$item->setIsUnlimited(1);
			$item->setInvisible(1);
			$item->setCid(0);
			$item->setImageId(0);
			$item->setUnitPriceIncludesTax(0);
			$item->setLocation('');
			$item->setAllowAltDescription(0);
			$item->setIsSerialized(0);
			$item->setIsGiftcard(0);
			$item->setSoupOrSalad(0);
			$item->setIsSide(0);
			$item->setAddOnPrice(0);
			$item->setPrintPriority(1);
			$item->setIsFee(0);
			$item->setKitchenPrinter(1);
			$item->setQuickbooksAssets('');
			$item->setQuickbooksIncome('');
			$item->setQuickbooksCogs('');
			$item->setErangeSize(0);

			$this->silex->db->persist($item);
			$this->silex->db->flush($item);
		}
		return $item;
	}

	public function applyFinanceCharge(ForeupAccountStatements &$statement,\DateTime $currentTime = null,$test = false)
	{
		$customer = $statement->getCustomer();

		// double-check that we should be applying a finance charge
		// should be enabled, and shouldn't already have been charged for this statement
		if(
			$statement->getTotalFinanceCharges()*1 > 0 ||
			!$statement->getFinanceChargeEnabled() ||
			$statement->getTotalOpen()*1 === 0
		){
			return;
		}

		$total = (float) $statement->getTotalOpen() + (float) $statement->getPreviousOpenBalance();

		// calculate the finance charge for the statement
		$type = $statement->getFinanceChargeType();
		if($type === 'percent'){
			$finance_total = round($total * ($statement->getFinanceChargeAmount() / 100),2);
		}
		elseif ($type === 'fixed'){
			$finance_total = $statement->getFinanceChargeAmount();
		}else{
			return;
		}


		// get or create the item from the repo
		$item = $this->getFinanceChargeItem($statement);

		// invoke the payBalance method
		$invoice = $this->payBalance($statement->getRecurringStatement()->getOrganizationId(),$item,$customer,$item->getName(),$finance_total,$currentTime,$test);

		// associate the invoice with the charge
		$invoice->setRecurringCharge($statement->getRecurringStatement()->getRecurringCharge());
		$this->silex->db->flush($invoice);
		//$result =  $this->associateInvoiceWithCharge($statement->getRecurringStatement()->getRecurringCharge(),$result['invoice_id'],$test);

		$statement->setTotal(round((float) $statement->getTotal() + (float) $finance_total,2));

		$statement->setTotalFinanceCharges($finance_total);

		$statement->setTotalOpen(round((float) $statement->getTotalOpen() + (float) $finance_total,2));

		if(!$test){
			$this->silex->db->flush($statement);
		}

		return $invoice;
	}

	public function payBalance($course_id, ForeupItems $item,ForeupCustomers $customer,string $name, float $balance,\DateTime $currentTime = null,$test = false){
		if(is_null($currentTime))$currentTime = new \DateTime();
		$item->setQuantity(1);

		$invoice_array = $this->generateInvoiceObject($item,$customer,$name,$balance,$currentTime,$test);
		$invoice_array['date'] = $currentTime->format(DATE_ISO8601);

		if($item->getItemNumber() == 'customer_balance'){
			$invoice_array['pay_customer_account'] = 1;
		}
		if($item->getItemNumber() == 'member_balance'){
			$invoice_array['pay_member_account'] = 1;
		}
		$invoice = $this->saveInvoice($course_id,$customer,$invoice_array,$test);
		//$result = $this->silex->CI->invoice->save($invoice_array, false, $test);

		return $invoice;

	}

	public function getLinkedCourses($course_id)
	{
		$groups = $this->getCourseGroups($course_id);
		$courses = [];
		foreach($groups->getIterator() as $group){
			$crs = $group->getCourses();
			if(method_exists($crs,'toArray')){
				$crs = $crs->toArray();
			}

			$courses = array_merge($courses,$crs);
		}

		return $courses;
	}

	public function getCourseGroups($course_id,$search_type = 'sharedCustomers')
	{
		$repo = $this->silex->db->getRepository('e:ForeupCourses');

		$course = $repo->find($course_id);

		$criteria = new \Doctrine\Common\Collections\Criteria();
		$criteria->where($criteria->expr()->eq($search_type,1));

		return $course->getCourseGroups()->matching($criteria);
	}

	public function associateInvoiceWithCharge(ForeupAccountRecurringCharges $charge,$invoice_id,$test = false)
	{
		return $this->associateInvoiceWithRecurringCharge($charge,$invoice_id,$test);

	}

	public function saveInvoice($course_id,ForeupCustomers $customer,$invoice_array,$test)
	{
		$sales = $this->silex->db->getRepository('e:ForeupSales');

		$invoice = new ForeupInvoices();
		// get overdue

		// get next invoice number
		$invoice_number = $this->getNextInvoiceNumber($course_id);
		$invoice->setInvoiceNumber($invoice_number);

		// set all the parameters
		$invoice->setName($invoice_array['name']);
		$invoice->setDepartment('Invoice');
		$invoice->setCategory('Invoice');
		$invoice->setSubcategory('');
		$invoice->setDay(0);
		$invoice->setCreditCard(null);
		$invoice->setBillingId(0);
		$invoice->setPerson($customer->getPerson());
		//$invoice->setEmployeeId(0);
		$invoice->setCourseId($course_id);
		$invoice->setDate(new \DateTime($invoice_array['date']));
		$invoice->setBillStart(new \DateTime($invoice_array['bill_start']));
		$invoice->setBillEnd(new \DateTime($invoice_array['bill_end']));
		$invoice->setTotal($invoice_array['total']??0.00);
		$invoice->setOverdueTotal($invoice_array['overdue_total']??0.00);
		$invoice->setPreviousPayments($invoice_array['previous_payments']??0.00);
		$invoice->setPaid($invoice_array['paid']??0.00);
		$invoice->setOverdue($invoice_array['overdue']??0.00);
		$invoice->setDeleted(0);
		//$invoice->setCreditCardPaymentId(0);
		$invoice->setEmailInvoice($invoice_array['email_invoice']??0);
		//$invoice->setLastBillingAttempt('000-00-00');
		$invoice->setStarted(0);
		$invoice->setCharged(0);
		$invoice->setChargedNoSale(0);
		$invoice->setEmailed(0);
		//$invoice->setSendDate('000-00-00');
		//$invoice->setDueDate('0000-00-00');
		//$invoice->setAutoBillDate('0000-00-00');
		$invoice->setPayCustomerAccount($invoice_array['pay_customer_account']??0);
		$invoice->setPayMemberAccount($invoice_array['pay_member_account']??0);
		$invoice->setShowAccountTransactions($invoice_array['show_account_transactions']??0);
		$invoice->setSale($sales->find(0));
		$invoice->setNotes($invoice_array['notes']??'');
		$invoice->setAttemptLimit($invoice_array['attempt_limit']??1);

		//$invoice->setRecurringCharge($statement->getRecurringStatement()->getRecurringCharge());
		//$invoice->setAccountRecurringChargeItem($invoice_array['account_recurring_charge_item']);

		// insert into database

		$this->silex->db->persist($invoice);
			$this->silex->db->flush($invoice);
		// sale stuff here
		$sale = $this->silex->CI->invoice->save_items($invoice_array['items'],$invoice->getId()??1,$invoice->getPerson()->getPersonId(),null,$course_id,$invoice_array['date'],false);
		$invoice_total = $sale['total'];
		$sale_id = (int) $sale['sale_id'];
		$sale_number = (int) $sale['sale_number'];

		// maybe customer/member account stuff here?




		$invoice->setTotal($invoice_total);
		//$invoice->setPaid();
		$repo = $this->silex->db->getRepository('e:ForeupSales');
		$invoice->setSale($repo->find($sale_id));

		$invoice->setNotes('');
		if($test){
			$invoice->validate();
		}else {
			$this->silex->db->flush($invoice);
		}
		return $invoice;
	}

	public function getNextInvoiceNumber($course)
	{
		$criteria = new \Doctrine\Common\Collections\Criteria();
		$criteria->where($criteria->expr()->eq('courseId',$course));
		$criteria->orderBy(['invoiceNumber'=>'DESC']);
		$repo = $this->silex->db->getRepository('e:ForeupInvoices');
		$matches = $repo->matching($criteria);
		$first = $matches->first();
		if(!$first){
			$number = 1;
		}else{
			$number = $first->getInvoiceNumber() + 1;
		}
		return $number;
	}

	public function generateInvoiceObject(ForeupItems $item,ForeupCustomers $customer,string $name,float $amount,\DateTime $currentTime=null,$test=false)
	{
		$ret = [];
		$ret['items'] = [$this->itmTransformer->transform($item)];
		$ret['person_id'] = $customer->getPerson()->getPersonId();
		$ret['course_id'] = $customer->getCourseId();
		$ret['employee_id'] = 0;
		$ret['name'] = $name;
		$dt = new \DateTime();
		if(isset($currentTime))$dt=$currentTime;

		// bill_start
		// last ran
		$ret['bill_start'] = $dt->format(DATE_ISO8601);
		// bill_end
		// next occurence
		$ret['bill_end'] = $dt->format(DATE_ISO8601);
		$ret['date'] = $dt->format(DATE_ISO8601);
		$ret['items'][0]['item_type'] = 'item';
		$ret['items'][0]['item_id'] = $item->getItemId();
		$ret['items'][0]['amount'] =  $ret['items'][0]['unit_price'] = $ret['items'][0]['price'] = $amount;

		if(!isset($ret['items'][0]['pay_member_account'])){
			$ret['items'][0]['pay_member_account'] = 0;
		}
		if(!isset($ret['items'][0]['pay_customer_account'])){
			$ret['items'][0]['pay_customer_account'] = 0;
		}

		return $ret;

	}
}
?>
