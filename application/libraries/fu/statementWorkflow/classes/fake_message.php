<?php
namespace fu\statementWorkflow\classes;


class fake_message {
	private $kv_hash = [];
	private $removed = false;
	private $hidden = false;
	private $id;

	public function __construct($id,$body)
	{
		$this->id = $id;
		$this->kv_hash['Body'] = $body;
	}

	public function get($key){
		if(!isset($this->kv_hash[$key]))return null;
        return $this->kv_hash[$key];
	}

	public function delete()
	{
		$this->removed = true;
	}

	public function hide()
	{
		$this->hidden = true;
	}

	public function show()
	{
		$this->hidden = false;
	}

	public function visible()
	{
		return !$this->hidden;
	}

	public function deleted()
	{
		return $this->removed;
	}

	public function get_id()
	{
		return $this->id;
	}
}

?>