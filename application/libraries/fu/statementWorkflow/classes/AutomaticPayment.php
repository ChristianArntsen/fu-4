<?php
namespace fu\statementWorkflow\classes;

use foreup\rest\models\entities\ForeupAccountStatementPayments;
use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupCustomerCreditCards;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\models\entities\ForeupItems;
use foreup\rest\resource_transformers\course_transformer;
use foreup\rest\resource_transformers\customer_credit_card_transformer;
use foreup\rest\resource_transformers\items_transformer;
use fu\PaymentGateway\PaymentGatewayCredentials;
use fu\PaymentGateway\PaymentGatewayException\GeneralFailureException;
use fu\PaymentGateway\PaymentGatewayFactory;
use fu\PaymentGateway\StoredPaymentInfo;

class AutomaticPayment
{
	/**
	 * @var ForeupCustomers
	 */
	private $customer;

	/**
	 * @var ForeupAccountStatements
	 */
	private $statement;

	/**
	 * @var silex_handler
	 */
	private $silex;

	/**
	 * @var \Doctrine\ORM\EntityRepository
	 */
	private $courses;

	/**
	 * @var \Doctrine\ORM\EntityRepository
	 */
	private $cards;

	/**
	 * @var ForeupCourses
	 */
	private $course;

	/**
	 * @var \fu\PaymentGateway\MercuryPaymentGateway
	 */
	private $gateway;

	private $validCard;

	public function __construct(silex_handler & $silex,ForeupCustomers $customer, ForeupAccountStatements &$statement)
	{
		$this->customer = $customer;
		$this->statement = &$statement;

		$course_id = $statement->getRecurringStatement()->getOrganizationId();

		$this->silex = $silex;

		$this->cards = $silex->db->getRepository('e:ForeupCustomerCreditCards');
		$this->courses = $silex->db->getRepository('e:ForeupCourses');
		$this->course = $this->courses->find($course_id);

		// get course info
		$trans = new course_transformer();
		$info = $trans->transform_sensitive($this->course);

		// get payment gateway
		$factory = new PaymentGatewayFactory($info,$this->silex->CI);

		$this->gateway = $factory->getPaymentGateway();

		$this->pullStatementData($statement);

	}

	public function pullStatementData(ForeupAccountStatements $statement)
	{
		$charges = $statement->getCharges();
		$ray = $charges->toArray();
	}

	public function getSavedCreditCards()
	{
		return $this->cards->findBy(['customerId'=>$this->customer->getPerson()->getPersonId(),'courseId'=>$this->customer->getCourseId(),'deleted'=>False]);

	}

	public function validateCard($card)
	{
		$valid = true;

		// check if token is expired
		$now = new \DateTime();
		$expires = $card->getTokenExpiration();
		$d = $now->diff($expires);
		if($d->invert) $valid = false;

		// check if deleted (done when getting the cards)

		// check if status is ok
		$status = $card->getStatus();
		if($status !== 'Good' && $status !== '') $valid = false;

		return $valid;

	}

	public function getCreditCard()
	{
		if(isset($this->validCard))return $this->validCard;
		$validCard = false;

		$cards = $this->getSavedCreditCards();

		if(method_exists($cards,'toArray')){
			$cards = $cards->toArray();
		}

		if(!is_array($cards)){
			return $validCard;
		}

		foreach($cards as $card){
			if($this->validateCard($card)){
				$validCard = $card;
				// take the first valid card we find for now
				break;
			}
		}

		if($validCard)
		  $this->validCard = $validCard;

		return $validCard;

	}

	public function recordSuccess (string $status = 'Good')
	{

		if(!isset($this->validCard)){
			return false;
		}

		$this->validCard->setSuccessfulCharges($this->validCard->getSuccessfulCharges()+1);
		$this->validCard->setStatus($status);
		$this->validCard->setFailedCharges(0);

		$this->silex->db->flush();
	}

	public function recordFailure (string $status = ''){
		if(!isset($this->validCard)){
			return false;
		}

		$this->validCard->setFailedCharges($this->validCard->getFailedCharges()+1);
		$this->validCard->setStatus($status);

		$this->silex->db->flush();
	}

	public function charge($test = false)
	{
		// calculate totals

		// get course info
		$trans = new course_transformer();
		$info = $trans->transform_sensitive($this->course);

		$card = $this->getCreditCard();

		$card_transformer = new customer_credit_card_transformer();
		$card_info = $card_transformer->transform($card);

		$paymentInvoiceId = $this->gateway->saveCreditCardPayment((int) $info[$this->gateway->processor_id_key]); // need to make an invoice

		// get payment gateway
		$spi = new StoredPaymentInfo(
		    'credit_card',
            $card->getToken(),
            $paymentInvoiceId,
            $card->getCardholderName(),
            $card->getId(),
            $card->getCardType()
        );

		$pgc = new PaymentGatewayCredentials(0,$this->gateway->provider,$info);

		$this->gateway->setStoredPayment($spi);
		$this->gateway->setPaymentGatewayCredentials($pgc);

		$course_id = $this->statement->getOrganizationId();

		$success = false;
		if($test) {
			$this->statement->setTotal(14.95);
		}

		// will this ever be the case?
		$previously_paid = $this->statement->getTotalPaid();
		$due = $this->statement->getTotal() - $previously_paid;

		// charge the card
		$success = $this->gateway->charge($due, $paymentInvoiceId, $this->statement->getId());

		if($success) {

		    $authAmount = $this->gateway->getAuthorizedAmount();

            // on success, create sale
			$sale_id = $this->saveSale(
			    $course_id,
                $this->customer->getPerson()->getPersonId(),
				$this->statement->getTotal(),
                $authAmount,
                $paymentInvoiceId,
                $test
            );

            $card = $this->getCreditCard();
            $mapping = ['M/C'=>'mastercard','MasterCard'=>'mastercard','DCVR'=>'discover',
                'AMEX'=>'amex','VISA'=>'visa','Diners'=>'diners'];
            $type = 'credit_card';
            $subType = $mapping[$card->getCardType()];
            $accountNumber = $card->getMaskedAccount();
            $name = $card->getCardholderName();

            $this->applyPayment($sale_id, $authAmount, $type, $subType, $accountNumber, $name, $paymentInvoiceId, $isAutoPayment = true, $test);

		}else {
			// on failure, deal with it

		}

		return $success;
	}

	public function applyPayment($saleId, $authAmount, $type, $subType, $accountNumber, $name, $chargeInvoiceId, $isAutoPayment, $test = false){

	    // distribute payment among each statements paid column until out of money
        $repo = $this->silex->db->getRepository('e:ForeupInvoices');
        $rc = $this->statement->getRecurringStatement()->getRecurringCharge();

        if($test){
            $i1 = new ForeupInvoices();
            $i1->setPaid(0.00);
            $i1->setTotal(14.95);
            $invoices = [$i1];
        }else {
            $invoices = $repo->findBy(['recurringCharge' => $rc, 'person' => $this->statement->getCustomer()->getPerson()], ['date' => 'asc']);
        }
        $remaining = $this->applyPaymentToInvoices($invoices, $authAmount);

        if($remaining > 0.00){
            // too much is left over...
            error_log('Statement ['.$this->statement->getId().'] payment: $'.$remaining.' remaining after paying off all invoices attached to statement');
        }

        return $this->saveStatementPayment($saleId, $authAmount, $type, $subType, $accountNumber, $name, $chargeInvoiceId, $isAutoPayment, $test);
    }

    public function saveStatementPayment($saleId, $authAmount, $type, $subType, $accountNumber, $name, $chargeInvoiceId, $isAutoPayment, $test = false){

        $statementTotal = $this->statement->getTotal();

        $sale_repo = $this->silex->db->getRepository('e:ForeupSales');
        $sale = $sale_repo->find((int) $saleId);

        // if succeeds, record success and apply as payment on statement
        $payment = new ForeupAccountStatementPayments();
        $payment->setDateCreated(new \DateTime());
        $payment->setAmount($authAmount);
        $payment->setType($type);
        $payment->setSubType($subType);
        $payment->setAccountNumber($accountNumber);
        $payment->setName($name);
        $payment->setSale($sale);
        $payment->setCustomer($this->customer);
        $payment->setIsAutopayment($isAutoPayment);
        $payment->setDateCreated(new \DateTime());

        if(!empty($chargeInvoiceId)){
            $inv_repo = $this->silex->db->getRepository('e:ForeupCustomerCreditCards');
            $chargeInvoice = $inv_repo->find($chargeInvoiceId);
            $payment->setChargeInvoice($chargeInvoice);
        }

        $payment->setStatement($this->statement);

        $this->silex->db->persist($payment);

        $totalPaid = $this->statement->getTotalPaid() + $authAmount;
        $this->statement->setTotalPaid($totalPaid);
        $this->statement->setTotalOpen(round($statementTotal - $totalPaid, 2));

        if(!$test) $this->silex->db->flush();

        return $payment->getId();
    }

	public function applyPaymentToInvoices(&$invoices,$remaining)
	{
		foreach($invoices as &$invoice){
			if($remaining <= 0.00)break;
			$paid = $invoice->getPaid();
			$total = $invoice->getTotal();
			$delta = round($total-$paid,2);
			if($delta === 0.00)continue;

			if($delta>$remaining){
				$invoice->setPaid($remaining);
				$remaining = 0;
				break;
			}
			else{
				$remaining -= $delta;
				$invoice->setPaid($delta);
			}
			$remaining = round($remaining,2);
		}

		return $remaining;
	}

	public function saveSale($course_id,$customer_id,$amount,$auth_amount,$payment_id,$test = false)
	{

		// models/invoice.php->charge_invoice ~ line 1531
		$sale_items = [];
		$item = $this->getStatementSaleItem($course_id);
		$transformer = new items_transformer();
		$item = $transformer->snake_case($item);
		//$item['unit_price_includes_tax'] = true;
		$item['unit_price'] = $amount;
		$item['price'] = $amount;
		$item['subtotal'] = $amount;
		$item['line'] = 1;
		//$item['invoice_id'] = $payment_id;
		$sale_items[] = $item;

		$sale_payments = [];
		$sale_payments[] = [
			'payment_type' => 'credit_card',
			'payment_amount' => abs($auth_amount),
			'invoice_id' => $payment_id
		];

		$CI = get_instance();
		return $this->silex->CI->Sale->save($sale_items, $customer_id, 0, 'Auto-billed statement',
			$sale_payments, false, -1, $course_id, null, false, '', 0,0,false,0,true,$test);


	}

	public function getStatementSaleItem($course_id)
	{
		$repo = $this->silex->db->getRepository('e:ForeupItems');

		$results = $repo->findBy(['courseId'=>$course_id,'itemNumber'=>'statement_payment',
			'name'=>'Statement']);

		if(!count($results)){
			$item = new ForeupItems();

			$item->setCourseId($course_id);
			$item->setItemNumber('statement_payment');
			$item->setName('Statement');
			$item->setCostPrice(0.00);
			$item->setUnitPrice(0.00);
			$item->setIsUnlimited(1);
			$item->setInvisible(1);
			$item->setCid(0);
			$item->setDepartment('');
			$item->setCategory('');
			$item->setSubcategory('');
			$item->setDescription('');
			$item->setImageId(0);
			$item->setUnitPriceIncludesTax(0);
			$item->setLocation('');
			$item->setAllowAltDescription(0);
			$item->setIsSerialized(0);
			$item->setIsGiftcard(0);
			$item->setSoupOrSalad(0);
			$item->setIsSide(0);
			$item->setAddOnPrice(0);
			$item->setPrintPriority(1);
			$item->setIsFee(0);
			$item->setKitchenPrinter(1);
			$item->setQuickbooksAssets('');
			$item->setQuickbooksIncome('');
			$item->setQuickbooksCogs('');
			$item->setErangeSize(0);

			$this->silex->db->persist($item);
			$this->silex->db->flush();

		}else{
			$item = $results[0];

		}

		return $item;

	}

	public function validAutopay()
	{
		// can we really autopay?

		// autopay enabled?

		// delay days not exceeded

		// autopay attempts not exceeded

	}

	public function autopayAfter()
	{
		// datetime after which we may auto pay
		// accounts for end date + autopay delay days

	}

	public function test(){
	    echo 'TESTING FROM SILEX'; //JBDEBUG
    }
}
?>