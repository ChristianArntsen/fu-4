<?php
namespace fu\statementWorkflow\classes;

if(ENVIRONMENT !== 'testing' && ENVIRONMENT !== 'jenkins')
    require_once __DIR__.'/../../../../vendor/autoload.php';

use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use fu\statementWorkflow\interfaces\queue_interface;

class SqsQueue implements queue_interface {

    protected $client;
    protected $queue_hash;
    protected $runtime_error_queue;
    protected $user_error_queue;
    protected $last_error;
    protected $runtime_error_list = [];
    protected $user_error_list = [];

    protected function resetLastError() {
        $this->last_error = null;
    }

    public function getLastError() {
        return $this->last_error;
    }

    public function __construct($CI = null)
    {
        $this->getQueueClient($CI);
    }

    protected function getQueueClient($CI)
    {
        if(isset($this->client)) {
            return $this->client;
        }

        if ( ! defined('BASEPATH')) {
            if(is_dir('/var/lib/jenkins/workspace/Unit Tests/application'))
                require_once '/var/lib/jenkins/workspace/Unit Tests/application/tests/unit/_bootstrap.php';
            else
                require_once '/var/www/development.foreupsoftware.com/fu/application/tests/unit/_bootstrap.php';
        }


        if(isset($CI)){
            $s3 = $CI->config->item('s3');
        }else {
            $config = [];
            if (is_dir('/var/lib/jenkins/workspace/Unit Tests/application'))
                require_once '/var/lib/jenkins/workspace/Unit Tests/application/config/config.php';
            else
                require_once __DIR__.'/../../../../config/config.php';
            if(!isset($config['s3'])){
                // try to fall back...
                $CI = get_instance();
                $s3 = $CI->config->item('s3');
            }else {
                $s3 = $config['s3'];
            }
        }

        $this->client = SqsClient::factory(array(
            'region'  => 'us-west-2',
            'credentials' => [
                'key'    => $s3['access_key'],
                'secret' => $s3['secret']
            ],
            'version' => '2012-11-05'
        ));

        return $this->client;
    }

    public function deleteQueue($queue)
    {
        if(is_string($queue)){
            $queue = $this->queue_hash[$queue];
        }
        $queueUrl = $queue->get('QueueUrl');
        try {
            $result = $this->client->deleteQueue([
                'QueueUrl' => $queueUrl
            ]);
        }catch(SqsException $e){
            $this->last_error = $e;
            $this->user_error_list[] = ['operation'=>'deleteQueue','exception'=>$e];
        }
        catch (\Exception $e){
            $this->last_error = $e;
            $this->user_error_list[] = ['operation'=>'deleteQueue','exception'=>$e];
        }

        return $result;
    }

    public function purgeAll()
    {
        foreach($this->queue_hash as $name=>$queue){
            $this->purgeQueue($name);
        }
    }

    public function purgeQueue($name){
        $queue = $this->queue_hash[$name];

        $queueUrl = $queue->get('QueueUrl');
        try {
            $result = $this->client->purgeQueue([
                'QueueUrl' => $queueUrl
            ]);
        }catch(SqsException $e){
            $this->last_error = $e;
            $this->user_error_list[] = ['operation'=>'purgeQueue','exception'=>$e];
        }
        catch (\Exception $e){
            $this->last_error = $e;
            $this->user_error_list[] = ['operation'=>'purgeQueue','exception'=>$e];
        }
    }

    public function getQueue($name, $production = false)
    {
        if(isset($this->queue_hash[$name])){
            return $this->queue_hash[$name];
        }

        if($production){
        	$message_wait = 20;

        	$vis_timeout = 60*5;
        }else{
        	$message_wait = 5;

        	$vis_timeout = 10;
        }

        try {
            $queue = $this->client->createQueue(array(
                'QueueName' => $name.'.fifo',
                'Attributes' => [
                    'ReceiveMessageWaitTimeSeconds' => $message_wait,
                    'FifoQueue' => 'true',
                    'ContentBasedDeduplication' => 'true',
                    'VisibilityTimeout' => $vis_timeout
                ]
            ));
        }catch (SqsException $e){
            $code = $e->getAwsErrorCode();
            $this->last_error = $e;
            print "\n\nCould not get the queue: ".$code."\n\n";
            switch ($code) {
                case 'AWS.SimpleQueueService.QueueDeletedRecently':
                case 'QueueDeletedRecently':
                    sleep(15);
                    return $this->getQueue($name);
                    break;
                case 'AWS.SimpleQueueService.QueueNameExists':
                case 'QueueNameExists':
                    // existing queue with different parameters
                    // put in user error queue
                    $this->user_error_list[] = ['operation'=>'getQueue','exception'=>$e];
                    break;
            }
        }catch(\Exception $e){
            $this->last_error = $e;
            // some other exception happened
            // add to user error queue
            $this->user_error_list[] = ['operation'=>'getQueue','exception'=>$e];
        }

        $this->queue_hash[$name] = $queue;
        return $queue;
        //$queueUrl = $result->get('QueueUrl');
    }

    public function enqueueMessages($queue,$messages,$group = 'group1'){

        if(is_string($queue)){
            $queue = $this->queue_hash[$queue];
        }
        if(!isset($queue))return false;
        $queueUrl = $queue->get('QueueUrl');

        $results = [];

        foreach($messages as $message){
            $results[] = $this->enqueueMessage($queue,$message,$group);
        }

        return $results;
    }

    public function enqueueMessage($queue, $message, $group = 'group1')
    {
        if(is_array($message)){
            return $this->enqueueMessages($queue,$message);
        }
        if(is_string($queue)){
            $queue = $this->queue_hash[$queue];
        }
        if(!isset($queue))return false;
        $queueUrl = $queue->get('QueueUrl');

        if(empty($group)){
            $group = 'group1';
        }

        try {
            $result = $this->client->sendMessage([
                'QueueUrl' => $queueUrl,
                'MessageBody' => $message,
                'MessageGroupId' => $group
            ]);

        }catch (SqsException $e){
            $code = $e->getAwsErrorCode();
            $this->last_error = $e;

            echo($e->getMessage());

            switch ($code) {
                case 'AWS.SimpleQueueService.InvalidMessageContents':
                case 'InvalidMessageContents':
                    // strange characters included in message
                    // add to runtime error queue
                    $this->runtime_error_list[] = ['operation'=>'enqueueMessage',/*'message'=>$message,*/'exception'=>$e];
                    break;
                case 'AWS.SimpleQueueService.UnsupportedOperation':
                case 'UnsupportedOperation':
                    // add to user error queue
                    $this->user_error_list[] = ['operation'=>'enqueueMessage','message'=>$message,'exception'=>$e];
                    break;
            }

        }catch(\Exception $e){
            $this->last_error = $e;
            // some other exception happened
            // add to user error queue
            $this->user_error_list[] = ['operation'=>'enqueueMessage','message'=>$message,'exception'=>$e];
            echo($e->getMessage());
        }

        return $result;
    }

    public function fetchMessages($queue)
    {
        if(is_string($queue)){
            $queue = $this->queue_hash[$queue];
        }
        if(!isset($queue))return false;
        $queueUrl = $queue->get('QueueUrl');

        try {
            $result = $this->client->receiveMessage(array(
                'QueueUrl' => $queueUrl,
                'MaxNumberOfMessages' => 10,
                'WaitTimeSeconds' => 20
            ));
        }catch (SqsException $e){
            $code = $e->getAwsErrorCode();
            $this->last_error = $e;
            switch ($code){
                case 'AWS.SimpleQueueService.OverLimit':
                case 'OverLimit':
                    // perhaps too many in-flight messages
                    // need to wait and retry
                    sleep(10);
                    return $this->fetchMessages($queue);
                    break;
            }
        }catch(\Exception $e){
            $this->last_error = $e;
            // some other exception happened
            // add to user error queue
            $this->user_error_list[] = ['operation'=>'fetchMessages','exception'=>$e];

        }

        $messages = [];
        if(!isset($result) || empty($result->getPath('Messages')))return [];
        foreach ($result->getPath('Messages') as $message) {
            // Do something with the message
            $messages[] = $message;
        }

        return $messages;
    }

    public function getAttributes($queue)
    {
        if(is_string($queue)){
            $queue = $this->queue_hash[$queue];
        }
        if(!isset($queue))return false;
        $queueUrl = $queue->get('QueueUrl');

        try {
            $response = $this->client->getQueueAttributes(array(
                'QueueUrl' => $queueUrl,
                'AttributeNames' => ['All']
            ));

        }catch(\Exception $e){
            $this->last_error = $e;
            // some other exception happened
            // add to user error queue
            $this->user_error_list[] = ['operation'=>'fetchMessages','exception'=>$e];
        }

        return $response->get('Attributes');
    }

    public function deleteMessage ($queue, $message)
    {
        if(is_string($queue)){
            $queue = $this->queue_hash[$queue];
        }
        $queueUrl = $queue->get('QueueUrl');
        try {
            $result = $this->client->deleteMessage([
                'QueueUrl' => $queueUrl, // REQUIRED
                'ReceiptHandle' => $message['ReceiptHandle'] // REQUIRED
            ]);
        }catch(SqsException $e){
            $code = $e->getAwsErrorCode();
            $this->last_error = $e;

            $response = $e->getResponse();

            $exception = $e->getAwsErrorType();
            switch ($code){
                case 'AWS.SimpleQueueService.ReceiptHandleIsInvalid':
                case 'ReceiptHandleIsInvalid':
                    // put in queue for user error
                    $this->user_error_list[] = ['operation'=>'deleteMessage','message'=>$message,'exception'=>$e];

                    break;
                case 'AWS.SimpleQueueService.InvalidFormat':
                case 'InvalidFormat':
                    // put in queue for user error
                    $this->user_error_list[] = ['operation'=>'deleteMessage','message'=>$message,'exception'=>$e];

                    break;
            }
        }catch(\Exception $e){
            $this->last_error = $e;
            // some other exception happened
            // add to user error queue
            $this->user_error_list[] = ['operation'=>'deleteMessage','message'=>$message,'exception'=>$e];
        }
        return $result;
    }
}

?>