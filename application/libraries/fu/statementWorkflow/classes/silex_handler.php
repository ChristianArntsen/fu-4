<?php
namespace fu\statementWorkflow\classes;

//require_once '/var/www/development.foreupsoftware.com/fu/application/vendor/autoload.php';

if(is_dir('../../../application'))//for testing environment
	require_once '../../../api_rest/vendor/autoload.php';
else{
    require_once __DIR__.'/../../../../../api_rest/vendor/autoload.php';
}

use Dflydev\Pimple\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Doctrine\ORM\EntityManager;
use foreup\rest\application\config;
use foreup\rest\application\middleware_loader;
use foreup\rest\application\routes_loader;
use foreup\rest\application\services_loader;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupUsers;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class silex_handler {

	/**
	 * @var Application
	 */
	public $app;
	public $CI;
	/**
	 * @var EntityManager
	 */
	public $db;
	private $middleware_loader, $configuration, $routes_loader, $services_loader;

	protected function createMockDefaultAppAndDeps()
	{
		$app = new Application();

		$app['user'] = 12;
		$app['auth.userObj'] = new testing_uzr(5);
		$app['auth.user'] = array(
			'loggedIn' => true,
			'validated' => true,
			'authorized' => true,
			'uid' => 12,
			'cid' => 6270,
			'level' => (int)5,
			'emp_id' => 12,
			'isEmployee' => 1,
			'isSuperUser' => 1
		);
		$pzn = new ForeupPeople();
		$pzn->setPersonId(12);

		$uzr = new ForeupUsers();
		$uzr->setPerson($pzn);

		$course = new \foreup\rest\models\entities\ForeupCourses();
		$course->setCourseId($app['auth.userObj']->getCid());
		$app['employee'] = new ForeupEmployees();
		$app['employee']->setCourse($course);
		$app['employee']->setUserLevel(5);
		$app['employee']->setPerson($pzn);

		return array($app, null, null);
	}

	protected function createMockDefaultApp()
	{
		list ($app, $connection, $eventManager) = $this->createMockDefaultAppAndDeps();

		return $app;
	}

	public function __construct()
	{
		$this->app = $this->createMockDefaultApp();
		$doctrineOrmServiceProvider = new DoctrineOrmServiceProvider();
		$doctrineOrmServiceProvider->register($this->app);
		$this->configuration = new testing_config($this->app);
		$this->configuration->loadConfig();
		$this->middleware_loader = new testing_middleware_loader($this->app);
		$this->middleware_loader->load_middleware();
		$this->services_loader = new testing_services_loader($this->app);
		$this->services_loader->register_services();
		$this->routes_loader = new testing_routes_loader($this->app);
		$this->routes_loader->bindRoutesToControllers();
		$this->db = $this->app['orm.em'];
		$CI = \get_instance();
		$CI->Employee->login("jackbarnes", "jackbarnes");

		$this->CI = &$CI;
	}

	public function __destruct()
	{
		$conn = $this->app['orm.em']->getConnection();
		if($conn->isConnected())$conn->close();
		$CI = \get_instance();
		//$CI->session->sess_destroy();
	}

	public function reconnect() {
		if(!$this->db->isOpen()){
			$this->db = $this->db->create(
				$this->db->getConnection(),
				$this->db->getConfiguration()
			);
		}
	}
}

class testing_uzr {
	private $level;

	public function __construct($lvl=5)
	{
		$this->setLevel($lvl);
	}

	public function setLevel($lvl){
		$this->level = $lvl;
	}

	public function getLevel() {
		return $this->level;
	}

	public function getUID(){
		return 12;
	}

	public function getAppId(){
		return 134476472;
	}

	public function getIsEmployee(){
		return true;
	}

	public function getCid() {
		return 6270;
	}

	public function getUserLevel() {
		return 5;
	}
}



class testing_routes_loader extends routes_loader {
	// allow non Application to be passed in for testing
	public function __construct(&$app)
	{
		$this->app = $app;

		$this->instantiateControllers();
	}
}

class testing_services_loader extends services_loader {
	// allow non Application to be passed in for testing
	public function __construct(&$app)
	{
		$this->app = $app;
	}
}

class testing_middleware_loader extends middleware_loader {
	// allow non Application to be passed in for testing
	public function __construct(&$app)
	{
		$this->app = $app;
	}
}

class testing_config extends config {
	// allow non Application to be passed in for testing
	public function __construct(&$app)
	{
		$this->app = $app;
	}
}

?>