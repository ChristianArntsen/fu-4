<?php
namespace fu\statementWorkflow;

use foreup\rest\models\entities\ForeupAccountStatements;
use fu\PaymentGateway\PaymentGatewayException\PaymentDeclinedException;
use fu\PaymentGateway\PaymentGatewayException\PaymentGatewayException;
use fu\statementWorkflow\classes\AutomaticPayment;
use fu\statementWorkflow\classes\silex_handler;
use fu\statementWorkflow\interfaces\queue_interface;
use fu\statementWorkflow\traits\daemon_trait;

class processStatementChargeJobs
{
	use daemon_trait;

	private $queue;
	private $input_queue_name;
	private $error_queue_name;

	public $run_cycles = 0;
	public $messages_received = 0;
	public $messages_sent = 0;

	public function __construct(queue_interface &$queue)
	{
		$prefix = '';
		$production = true;
		if(ENVIRONMENT !== 'production'){
			$prefix = ENVIRONMENT.'_';
			$production = false;
		}

		$this->queue = $queue;
		$this->input_queue_name = $prefix.'processStatementChargeJobs';
		$this->queue->getQueue($this->input_queue_name,$production);

		$this->error_queue_name = $prefix.'processStatementChargeJobsErrors';
		$this->queue->getQueue($this->error_queue_name,$production);

		$this->silex = new silex_handler();
		$this->CI = \get_instance();

        date_default_timezone_set('UTC');
	}

	public function __destruct()
	{
		$this->silex->db->getConnection()->close();
		unset($this->silex);
	}

	public function _get_queue()
	{
		return $this->queue;

	}

	public function _set_input_queue(queue_interface &$queue)
	{
		$this->queue = &$queue;

	}

	private function handle_error($result)
	{
		$this->queue->enqueueMessage($this->error_queue_name,\json_encode($result));

	}

	public function run($current_time = null, $test = false)
	{
		$this->run_cycles++;
		// fetch messages from input queue
		$messages = $this->queue->fetchMessages($this->input_queue_name);

		if(!isset($current_time)) {
			$current_time = new \DateTime();
		}
		elseif(is_string($current_time)){
			$current_time = new \DateTime($current_time);
		}

		$count=0;
		foreach($messages as $message) {
			$count++;
			$this->messages_received++;
			// extract message contents
			$msg_array = $this->extractMessage($message);

			$current_time = $msg_array['current_time'];

			$statement = $msg_array['statement'];
			if($test) {
				// populate statement with a known good statement
				$rep = $this->silex->db->getRepository('e:ForeupAccountStatements');
				$statement = $rep->find(1);

				$statement->setTotal(12.34);
				$statement->setTotalPaid(0.00);
				$statement->setTotalOpen(12.34);
				$statement->setAutopayDelayDays(5);

			}

			$prepaid = $statement->getTotalPaid();

			// check delay days
			if(!$this->delayDaysCheck($statement,$current_time)){
				continue;
			}

			$auto_pay = new AutomaticPayment($this->silex,$msg_array['customer'],$statement);

			$success = false;

			// charge card with statement amount
			try {
				$sale_id = $auto_pay->charge($test);
			}
			catch(PaymentDeclinedException $e) {
				// if fails, save failure in database

				$auto_pay->recordFailure($e->getMessage());

			}catch(\Exception $e){
				// get random error
				$error = [];
				$error['message'] = $e->getMessage();
				$error['trace'] = $e->getTraceAsString();
				$result = ['error'=>$error,'message'=>$msg_array];
				$this->handle_error($result);
				$this->queue->deleteMessage($this->input_queue_name,$message);
				continue;
			}

			if($success) {


				// create sale

				$this->messages_sent++;

				// if we make it, delete the message
				$this->queue->deleteMessage($this->input_queue_name, $message);
			}else{
				//
			}
		}

		return $count;

	}

	public function delayDaysCheck(ForeupAccountStatements $statement,\DateTime $currentTime = null)
	{
		// check if delay days is expired
		if(!isset($currentTime))$currentTime = new \DateTime();

		$createdTime = $statement->getDateCreated();
		$delayDays = $statement->getAutopayDelayDays();
		$delta = $currentTime->diff($createdTime);

		if($delta->days < $delayDays) return false;

		return true;
	}

	/**
	 * @param $message
	 * @return array|mixed
	 */
	public function extractMessage($message)
	{
		$ret = [];
		if(method_exists($message,'get'))
			$body = $message->get('Body');
		elseif (is_array($message)){
			$body = $message['Body'];
		}
		$intermediate = \json_decode($body,true);

		if(isset($intermediate['groupEnd']))return $intermediate;

		$rep = $this->silex->db->getRepository('e:ForeupAccountStatements');
		$cst = $this->silex->db->getRepository('e:ForeupCustomers');
		$ret['statement'] = $rep->find($intermediate['id']);
		$ret['courseId'] = $ret['statement']->getRecurringStatement()->getRecurringCharge()->getOrganizationId();
		$ret['customer'] =  $cst->findOneBy(['person'=>$intermediate['personId'],'course'=>$ret['courseId']]);
		$ret['original'] = $intermediate;
		$ret['current_time'] = $intermediate['current_time'];

		return $ret;

	}
}
?>