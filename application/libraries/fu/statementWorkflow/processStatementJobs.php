<?php
namespace fu\statementWorkflow;

use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use DoctrineProxies\__CG__\foreup\rest\models\entities\ForeupStatements;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupAccountStatementCharges;
use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupRepeatable;
use foreup\rest\resource_transformers\account_recurring_charge_customers_transformer;
use foreup\rest\resource_transformers\account_statements_transformer;
use foreup\rest\resource_transformers\invoices_transformer;
use foreup\rest\resource_transformers\person_transformer;
use foreup\rest\resource_transformers\repeatable_transformer;
use fu\statementWorkflow\classes\AccountCharger;
use fu\statementWorkflow\classes\silex_handler;
use fu\statementWorkflow\interfaces\queue_interface;
use fu\statementWorkflow\traits\daemon_trait;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;
use Recurr\Transformer\Constraint\BeforeConstraint;
use function Sodium\add;

class processStatementJobs
{
	use daemon_trait;

	/**
	 * @var queue_interface
	 */
	private $queue;

	private $input_queue_name;
	private $pdf_queue_name;
	private $charge_queue_name;
	private $error_queue_name;

	/**
	 * @var AccountCharger
	 */
	private $charger;

	/**
	 * @var silex_handler
	 */
	private $silex;

	public $run_cycles = 0;
	public $messages_received = 0;
	public $messages_sent = 0;
	public $errors_sent = 0;

	public function __construct(queue_interface &$queue)
	{
		$prefix = '';
		$production = true;
		if(ENVIRONMENT !== 'production'){
			$prefix = ENVIRONMENT.'_';
			$production = false;
		}

		$this->queue = $queue;
		$this->input_queue_name = $prefix.'ForeupRepeatableAccountRecurringStatementsToCustomers';
		$this->queue->getQueue($this->input_queue_name,$production);

		$this->pdf_queue_name = $prefix.'ForeupRenderStatementPdf';
		$this->queue->getQueue($this->pdf_queue_name,$production);

		$this->charge_queue_name = $prefix.'processStatementChargeJobs';
		$this->queue->getQueue($this->charge_queue_name,$production);

		$this->error_queue_name = $prefix.'processStatementJobsErrors';
		$this->queue->getQueue($this->error_queue_name,$production);

		$this->silex = new \fu\statementWorkflow\classes\silex_handler();
		$this->charger = new AccountCharger($this->silex);

		$this->repTransformer = new repeatable_transformer();
		$this->cstTransformer = new account_recurring_charge_customers_transformer();
		$this->stmtTransformer = new invoices_transformer();
		$this->psnTransformer = new person_transformer();
		$this->itmTransformer = new \foreup\rest\resource_transformers\items_transformer();
		$this->CI = \get_instance();

        date_default_timezone_set('UTC');
	}

	public function __destruct()
	{
		$this->silex->db->getConnection()->close();
		unset($this->silex);
	}

	public function _get_queue()
	{

		return $this->queue;

	}

	public function _set_input_queue(\fu\statementWorkflow\interfaces\queue_interface &$queue)
	{

		$this->queue = &$queue;

	}

	private function handle_error($result)
	{
		$this->queue->enqueueMessage($this->error_queue_name,\json_encode($result));
		$this->errors_sent++;

	}

	public function run($current_time = null, $test = false)
	{

		$this->run_cycles++;
		// fetch messages from input queue
		$messages = $this->queue->fetchMessages($this->input_queue_name);

		$count = 0;
		foreach($messages as $message) {
			$count++;
			$this->messages_received++;
			// extract message contents
			$msg_array = $this->extractMessage($message);

			if(isset($msg_array['groupEnd'])){

				// last one for the site
				// pass on the word to the other queues
				$this->queue->enqueueMessage($this->pdf_queue_name,\json_encode($msg_array));
			}
			elseif(!isset($msg_array['statement'])){
				$this->handle_error(['type'=>'malformed message','message'=>$msg_array]);
				$this->queue->deleteMessage($this->input_queue_name,$message);
			}
			else{

				$current_time = $msg_array['current_time'];

				// do the normal stuff
				try {
					$times = $this->getStartStopTimes($msg_array, $current_time,$test);

				}
				catch(\Exception $e){
					$error = [];
					$error['message'] = $e->getMessage();
					$error['trace'] = $e->getTraceAsString();
					$result = ['error'=>$error,'message'=>$msg_array];
					$this->handle_error($result);
					$this->queue->deleteMessage($this->input_queue_name,$message);
					continue;

				}

				$customer = $msg_array['customer']->getCustomer();
				$customer->__load();

				$dateTime = new \DateTime($times['bill_end']);
				$dateTime->add(new \DateInterval('PT1S'));

				$result = $this->charger->payAccountBalances($customer,$msg_array['statement'],$dateTime,$test);


				// store the statement
				$statement = $this->constructStatement($msg_array['courseId'],$msg_array['statement'],
					$customer,$this->silex->app['user'],$dateTime,
					new \DateTime($times['bill_start']),new \DateTime($times['bill_end']),
					$test);

				// queue the statement rendering jobs
				$result = $this->queueStatementPdfRenderJob($statement);

				$result = $this->queueStatementChargeJob($statement,new \DateTime($times['bill_end']));
				$this->messages_sent++;

			}

			// if we make it, delete the message
			$this->queue->deleteMessage($this->input_queue_name,$message);

		}

		return $count;

	}

	public function queueStatementPdfRenderJob(ForeupAccountStatements $statement)
	{

		$transformer = new account_statements_transformer();
		$statement_data = $transformer->transform($statement);
		return $this->queue->enqueueMessage($this->pdf_queue_name,\json_encode($statement_data));

	}

	public function queueStatementChargeJob(ForeupAccountStatements $statement,\DateTime $current_time)
	{
		$transformer = new account_statements_transformer();
		$statement->setDateAutopayQueued($current_time);
		$statement_data = $transformer->transform($statement);
		$this->silex->db->flush($statement);
		return $this->queue->enqueueMessage($this->charge_queue_name,\json_encode($statement_data));
	}

	public function getInvoices(ForeupAccountRecurringStatements $statement,
	                            ForeupCustomers $customer,
	                            \DateTime $startDate,
	                            \DateTime $endDate)
	{

		$rc = $statement->getRecurringCharge();
		$repo = $this->silex->db->getRepository('e:ForeupInvoices');

		$criteria = new Criteria();
		$criteria->where($criteria->expr()->eq('recurringCharge',
			$statement->getRecurringCharge()));

		//$criteria->andWhere($criteria->expr()->eq('personId',$customer->getPersonId()));
		//$criteria->andWhere($criteria->expr()->eq('courseId',$customer->getCourseId()));
		$criteria->andWhere($criteria->expr()->eq('personId',$customer->getPerson()->getPersonId()));
		$criteria->andWhere($criteria->expr()->gte('date',$startDate));
		$criteria->andWhere($criteria->expr()->lte('date',$endDate));

		$invoices = $repo->matching($criteria)->toArray();

		return $invoices;
	}

	public function getInvoiceItems($invoices){

		$ret = [];
		if(!is_array($invoices))$invoices=[$invoices];
		foreach($invoices as $invoice) {
			$tmp = $invoice->getSale()->getItems();
			if(!is_array($tmp)&&method_exists($tmp,'toArray')){
				$tmp=$tmp->toArray();

			}
			$ret = array_merge($ret,$tmp);

		}

		return $ret;
	}

	public function constructStatement($organization_id,
	                                   ForeupAccountRecurringStatements $rec_statement,
	                                   ForeupCustomers $customer,
	                                   $creator,
	                                   \DateTime $dateCreated,
	                                   \DateTime $startDate,
	                                   \DateTime $endDate,
	                                   $test=false)
	{
		$due = new \DateTime($dateCreated->format(DATE_ISO8601));
		$due = $due->add(
			new \DateInterval('P'.$rec_statement->getDueAfterDays().'D')
		);

		$statement = new ForeupAccountStatements();
		$statement->setOrganizationId($organization_id);

		$number = $this->get_next_statement_number($organization_id);
		$statement->setNumber($number);

		$statement->setDateCreated($dateCreated);
		$statement->setDueDate($due);
		$statement->setCreatedBy($creator);
		$statement->setCustomer($customer);
		$statement->setStartDate($startDate);
		$statement->setEndDate($endDate);
		$statement->setRecurringStatement($rec_statement);

		$statement->setSendZeroChargeStatement($rec_statement->getSendZeroChargeStatement());
		$statement->setSendEmail($rec_statement->getSendEmail());
		$statement->setSendMail($rec_statement->getSendMail());
		$statement->setFooterText($rec_statement->getFooterText());
		$statement->setMessageText($rec_statement->getMessageText());
		$statement->setTermsAndConditionsText($rec_statement->getTermsAndConditionsText());
		$statement->setAutopayEnabled($rec_statement->getAutopayEnabled());
		$statement->setIncludeCustomerTransactions($rec_statement->getIncludeCustomerTransactions());
		$statement->setIncludeMemberTransactions($rec_statement->getIncludeMemberTransactions());
		$statement->setPayCustomerBalance($rec_statement->getPayCustomerBalance());
		$statement->setPayMemberBalance($rec_statement->getPayMemberBalance());
		$statement->setFinanceChargeEnabled($rec_statement->getFinanceChargeEnabled());
		$statement->setFinanceChargeAmount($rec_statement->getFinanceChargeAmount());
		$statement->setFinanceChargeType($rec_statement->getFinanceChargeType());
		$statement->setFinanceChargeAfterDays($rec_statement->getFinanceChargeAfterDays());

		$statement->setPreviousOpenBalance($this->getPreviousOpenBalance($organization_id,$rec_statement,$customer));

		$invoices = $this->getInvoices($rec_statement,$customer,$startDate,$endDate);
		$invoiceItems = $this->getInvoiceItems($invoices);

		$retry = false;
		$attempts = 0;

		do {
			try {
				$attempts++;
				$this->silex->reconnect();
				$this->silex->db->persist($statement);
				if(!$test) {
					$this->silex->db->flush();
				}

			} catch (UniqueConstraintViolationException $e) {
				$retry = !$retry;
				$statement->setNumber($this->get_next_statement_number($organization_id));
				echo "\n\n<br>".$e->getMessage()."<br>\n\n";

			}
			if($attempts > 5)break;

		}while($retry);

		// store the items as statement charges
		$total = $this->storeItems($statement,$invoiceItems);

		// get total paid

		// get total open


		$statement->setTotal($total);
		$statement->setTotalOpen($total);

		if($test){
			$statement->validate();
		}else {
			$this->silex->db->flush();
		}

		if($test){
			//$this->silex->db->getConnection()->rollBack();
		}
		else{
			//$this->silex->db->getconnection()->commit();
		}

		return $statement;

	}

	public function getPreviousOpenBalance(int $organizationId,ForeupAccountRecurringStatements $rec_statement,ForeupCustomers $customer)
	{
		$repo = $this->silex->db->getRepository('e:ForeupAccountStatements');
		$test = new ForeupAccountStatements();

		$statements = $repo->findBy(['organizationId'=>$organizationId,'recurringStatement'=>$rec_statement,'customer'=>$customer]);

		if(method_exists($statements,'toArray')){
			$statements = $statements->toArray();

		}

		$sum = 0.00;
		foreach($statements as $statement){
			$sum += $statement->getTotalOpen();
		}

		return $sum;
	}

	private function storeItems($statement,$items,\DateTime $current_time=null)
	{

		$grandTotal = 0.00;
		$line = 1;
		foreach($items as $item)
		{
			$baseItem = $item->getItem();
			$sale = $item->getSale();

			$subtotal = $item->getSubtotal();
			$total = $item->getTotal();
			$tax = $item->getTax();
			$unitPrice = $item->getItemUnitPrice();
			$taxPercentage = 0;
			if($subtotal > 0){
                $taxPercentage = round($tax / ($subtotal / 100), 3);
            }

			$quantity = $item->getQuantityPurchased();

			$dateCharged = $sale->getSaleTime();
			$name = $baseItem->getName();

			$grandTotal += $total;

			$statementCharge = new ForeupAccountStatementCharges();
			$statementCharge->setStatement($statement);
			$statementCharge->setDateCharged($dateCharged);
			$statementCharge->setLine($line);
			$statementCharge->setName($name);
			$statementCharge->setItem($baseItem);
			$statementCharge->setSale($sale);
			$statementCharge->setTotal($total);
			$statementCharge->setSubtotal($subtotal);
			$statementCharge->setTax($tax);
			$statementCharge->setQty($quantity);
			$statementCharge->setUnitPrice($unitPrice);
			$statementCharge->setTaxPercentage($taxPercentage);

			$this->silex->db->persist($statementCharge);

			$this->silex->db->flush($statementCharge);
			$line++;

		}

		$grandTotal = round($grandTotal, 2);

		return $grandTotal;

	}

	public function get_next_statement_number($course_id){

		// this function is pretty generic. Might be useful other places
		$type = 'account_statements';
		$CI = \get_instance();
		//$CI->db->trans_start();
		$CI->db->select('current_index');
		$CI->db->from('course_increments');
		$CI->db->where('course_id', $course_id);
		$CI->db->where('type', $type);
		$CI->db->limit(1);
		$row = $CI->db->get()->row_array();

		if (isset($row) && isset($row['current_index'])) {
			// Return next_index, and save new index
			$CI->db->where('course_id', $course_id);
			$CI->db->where('type', $type);
			$CI->db->update('course_increments',array('current_index'=>$row['current_index']+1));
			//$CI->db->trans_complete();
			return (int) $row['current_index'] + 1;

		}
		else {

			$CI->db->insert('course_increments', array('course_id' => $course_id,
				'type' => $type, 'current_index' => 1));

			//$CI->db->trans_complete();

			return (int) 1;

		}

	}

	/**
	 * @param $message
	 * @return array|mixed
	 */
	public function extractMessage($message)
	{

		$ret = [];
		if(method_exists($message,'get'))
			$body = $message->get('Body');
		elseif (is_array($message)){
			$body = $message['Body'];
		}
		$intermediate = \json_decode($body,true);

		if(isset($intermediate['groupEnd']))return $intermediate;

		$rep = $this->silex->db->getRepository('e:ForeupAccountRecurringStatements');
		$cst = $this->silex->db->getRepository('e:ForeupAccountRecurringChargeCustomers');
		$ret['repeatable'] = $intermediate['repeatable'];
		$ret['statement'] = $rep->find($intermediate['statement']['id']);
		$ret['charge'] = $ret['statement']->getRecurringCharge();
		$ret['customer'] =  $cst->find($intermediate['customer']['id']);
		$ret['courseId'] = $ret['customer']->getCustomer()->getCourseId();
		$ret['current_time'] = $intermediate['current_time'];

		return $ret;

	}

	/**
	 * @param $msg_array
	 * @return array
	 * @throws \Exception
	 */
	public function getStartStopTimes($msg_array, $current_time = null,$test = false){

		$ret = [];

		$array = $msg_array['repeatable'];

		if(!isset($array['DTSTART'])){
			$array['DTSTART']='1979-01-01';

		}

		$bfr = new \DateTime($current_time);
		$bfr->add(new \DateInterval('PT1S'));

		$ro2 = new Rule(array_change_key_case($array,CASE_UPPER));
		$constraint = new BeforeConstraint($bfr);
		$transformer = new ArrayTransformer();
		$rez = $transformer->transform($ro2,$constraint);
		$rez2 = $rez->toArray();
		$rez3 = array_reverse($rez2);

		if(count($rez3) <= 1){
			// We appear to be running before the repeatable was created
			throw new \Exception('Cannot run job before start date');

		}

		$last = $this->getLastCustomerStatement($msg_array['statement'],$msg_array['customer']->getCustomer(),$test);

		// bill_start
		// last ran
		if(isset($last)){
			$ret['bill_start'] = $last->getEndDate()->format(DATE_ISO8601);
		}
		else {
			$ret['bill_start'] = $rez3[1]->getStart()->format(DATE_ISO8601);
		}


		// bill_end
		// next occurence
		$ret['bill_end'] = $rez3[0]->getStart()->sub(new \DateInterval('PT1S'))->format(DATE_ISO8601);

		return $ret;

	}

	public function getLastCustomerStatement(ForeupAccountRecurringStatements $rec_statement,ForeupCustomers $customer,$test = false){

		if($test)return null;

		$repo = $this->silex->db->getRepository('e:ForeupAccountStatements');

		$results = $repo->findBy(['customer'=>$customer,'recurringStatement'=>$rec_statement]);

		if(method_exists($results,'toArray'))$results = $results->toArray();

		$max = null;

		foreach($results as $statement) {
			if (!isset($max)){
				$max = $statement; continue;
			}
			$dt = new \DateTime();
			$ts = $dt->getTimestamp();
			if($statement->getEndDate()->getTimestamp() > $max->getEndDate()->getTimestamp())$max = $statement;
		}

		return $max;

	}

	/**
	 * @param ForeupRepeatable $entity
	 * @return array
	 */
	public function repeatableEntityToArray (ForeupRepeatable $entity )
	{

		$array = $this->repTransformer->transform($entity);
		return $array;

	}

}


?>