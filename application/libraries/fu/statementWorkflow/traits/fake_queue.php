<?php
namespace fu\statementWorkflow\traits;

if(is_dir('/var/lib/jenkins/workspace/Unit Tests/application'))
	require_once '/var/lib/jenkins/workspace/Unit Tests/application/vendor/autoload.php';
else
	require_once __DIR__.'/../../../../vendor/autoload.php';
use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use fu\statementWorkflow\classes\fake_message;

trait fake_queue {
	//
	private $queue_hash = [];
	private $id_counter = 0;

	protected function getQueueClient()
	{
		// does not do anything
		return true;
	}

	protected function getQueue($name)
	{
		if($this->queue_hash[$name]){
			return $this->queue_hash[$name];
		}



		$this->queue_hash[$name] = [];
		return $name;
	}

	public function deleteQueue($name)
	{
		unset($this->queue_hash[$name]);
	}

	public function enqueueMessage($queue, $message)
	{
		$message = new fake_message($this->id_counter++,$message);
		array_push($this->queue_hash[$queue],$message);
	}

	public function fetchMessages($queue)
	{
		$count = 0;
		$ret = [];
		foreach($this->queue_hash[$queue] as $message){
			if($count >= 10)break;
			if($message->visible() && !$message->deleted()){
				$ret[] = $message;
				$count++;
			}
		}

		return $ret;
	}

	public function deleteMessage($queue,$message)
	{
		foreach($this->queue_hash[$queue] as &$msg){
			if($msg->get_id() === $message->get_id()){
				$msg->delete();
			}
		}
	}
}

?>