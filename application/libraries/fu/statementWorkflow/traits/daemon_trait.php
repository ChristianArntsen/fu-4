<?php
namespace fu\statementWorkflow\traits;

trait daemon_trait
{
	private $lastSleep = 1;
	private $apoptosis = false;
	private $max_sleep = 1;
	private $max_age = 30;
	private $age = 0;

	public function runUntilEmpty ($current_time,$test = false){
		$count = 0;
		// TODO: this does not account for processing time
		// probably want to key it to that
		// since processing is where it picks up most entropy
		$this->age += $this->lastSleep;
		while($this->run($current_time,$test)){
			$count++;
		}
		return $count;
	}

	public function runAsDaemon ($current_time)
	{
		while(true)
		{
			$this->heartbeat();

			// run
			$result = $this->run($current_time);
			if($this->apoptosis)return;

			// if nothing in queue, sleep for a bit to save money
			if(!$result)$this->sleep();
			else $this->lastSleep = 1;
		}
	}

	protected function sleep()
	{
		// determine how long to sleep
		if($this->lastSleep < $this->max_sleep)
			$this->lastSleep *= 2;
		if($this->lastSleep > $this->max_sleep)
			$this->lastSleep = $this->max_sleep;

		// sleep
		sleep($this->lastSleep);
	}

	public function setMaxSleep(int $sleep_seconds)
	{
		$this->max_sleep = $sleep_seconds;
	}

	protected function heartbeat()
	{
		if(isset($this->max_age)){
			if($this->age >= $this->max_age)$this->apoptosis = true;

			// perhaps cut a child process loose?
		}

		// let something know that it is still running
	}
}

?>