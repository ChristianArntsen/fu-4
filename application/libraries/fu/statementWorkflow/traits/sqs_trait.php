<?php
namespace fu\statementWorkflow\traits;

if(is_dir('/var/lib/jenkins/workspace/Unit Tests/application'))
	require_once '/var/lib/jenkins/workspace/Unit Tests/application/vendor/autoload.php';
else
	require_once __DIR__.'/../../../../vendor/autoload.php';
use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;

trait sqs_trait { // provides implementation of the queue_interface

	protected $client;
	protected $queue_hash;
	protected $runtime_error_queue;
	protected $user_error_queue;
	protected $last_error;
	protected $runtime_error_list;
	protected $user_error_list;

	protected function resetLastError() {
		$this->last_error = null;
	}

	protected function getLastError() {
		return $this->last_error;
	}

	protected function getQueueClient()
	{
		if(isset($this->client)) {
			return $this->client;
		}

		if ( ! defined('BASEPATH')) {
			if(is_dir('/var/lib/jenkins/workspace/Unit Tests/application'))
				require_once '/var/lib/jenkins/workspace/Unit Tests/application/tests/unit/_bootstrap.php';
			else
				require_once '/var/www/development.foreupsoftware.com/fu/application/tests/unit/_bootstrap.php';
		}
		$config = [];
		require __DIR__.'../../../../config/config.php';

		$this->client = SqsClient::factory(array(
			'region'  => 'us-west-2',
			'credentials' => [
				'key'    => $config['s3']['access_key'],
				'secret' => $config['s3']['secret'],
			]
		));

		return $this->client;
	}

	protected function deleteQueue($queue)
	{
		if(is_string($queue)){
			$queue = $this->queue_hash[$queue];
		}
		$queueUrl = $queue->get('QueueUrl');
		try {
			$result = $this->client->deleteQueue([
				'QueueUrl' => $queueUrl
			]);
		}catch(SqsException $e){
			$this->last_error = $e;
		}
		catch (\Exception $e){
			$this->last_error = $e;
		}

		return $result;
	}

	protected function getQueue($name)
	{
		if($this->queue_hash[$name]){
			return $this->queue_hash[$name];
		}

		try {
			$queue = $this->client->createQueue(array(
				'QueueName' => $name,
				'ReceiveMessageWaitTimeSeconds ' => 20,
				'FifoQueue' => true,
				'VisibilityTimeout' => 60*5
			));
		}catch (SqsException $e){
			$code = $e->getAwsErrorCode();
			$this->last_error = $e;
			print "\n\nCould not get the queue: ".$code."\n\n";
			switch ($code) {
				case 'AWS.SimpleQueueService.QueueDeletedRecently':
				case 'QueueDeletedRecently':
					sleep(15);
					return $this->getQueue($name);
					break;
				case 'AWS.SimpleQueueService.QueueNameExists':
				case 'QueueNameExists':
					// existing queue with different parameters
					// put in user error queue
					break;
			}
		}catch(\Exception $e){
			$this->last_error = $e;
			// some other exception happened
			// add to user error queue
		}

		$this->queue_hash[$name] = $queue;
		return $queue;
		//$queueUrl = $result->get('QueueUrl');
	}

	public function enqueueMessages($queue,$messages){

		if(is_string($queue)){
			$queue = $this->queue_hash[$queue];
		}
		if(!isset($queue))return false;
		$queueUrl = $queue->get('QueueUrl');

		$results = [];

		foreach($messages as $message){
			$results[] = $this->enqueueMessage($queue,$message);
		}

		return $results;
	}

	public function enqueueMessage($queue, $message)
	{
		if(is_array($message)){
			return $this->enqueueMessages($queue,$message);
		}
		if(is_string($queue)){
			$queue = $this->queue_hash[$queue];
		}
		if(!isset($queue))return false;
		$queueUrl = $queue->get('QueueUrl');

		try {
			$result = $this->client->sendMessage(array(
				'QueueUrl' => $queueUrl,
				'MessageBody' => $message,
			));
		}catch (SqsException $e){
			$code = $e->getAwsErrorCode();
			$this->last_error = $e;
			switch ($code) {
				case 'InvalidMessageContents':
					// strange characters included in message
					// add to runtime error queue
					break;
				case 'UnsupportedOperation':
					// add to user error queue
					break;
			}
		}catch(\Exception $e){
			$this->last_error = $e;
			// some other exception happened
			// add to user error queue
		}

		return $result;
	}

	public function fetchMessages($queue)
	{
		if(is_string($queue)){
			$queue = $this->queue_hash[$queue];
		}
		if(!isset($queue))return false;
		$queueUrl = $queue->get('QueueUrl');

		try {
			$result = $this->client->receiveMessage(array(
				'QueueUrl' => $queueUrl,
				'MaxNumberOfMessages' => 10,
				'WaitTimeSeconds' => 20
			));
		}catch (SqsException $e){
			$code = $e->getAwsErrorCode();
			$this->last_error = $e;
			switch ($code){
				case 'OverLimit':
					// perhaps too many in-flight messages
					// need to wait and retry
					break;
			}
		}catch(\Exception $e){
			$this->last_error = $e;
			// some other exception happened
			// add to user error queue
		}

		$messages = [];
		if(!isset($result))return [];
		foreach ($result->getPath('Messages') as $message) {
			// Do something with the message
			$messages[] = $message;
		}

		return $messages;
	}

	public function deleteMessage ($queue, $message)
	{
		if(is_string($queue)){
			$queue = $this->queue_hash[$queue];
		}
		$queueUrl = $queue->get('QueueUrl');
		try {
			$result = $this->client->deleteMessage([
				'QueueUrl' => $queueUrl, // REQUIRED
				'ReceiptHandle' => $message['ReceiptHandle'] // REQUIRED
			]);
		}catch(SqsException $e){
			$code = $e->getAwsErrorCode();
			$this->last_error = $e;

			$response = $e->getResponse();

			$exception = $e->getExceptionType();
			switch ($code){
				case 'ReceiptHandleIsInvalid':
					// put in queue for user error
					break;
				case 'InvalidFormat':
					// put in queue for user error
					break;
			}
		}catch(\Exception $e){
			$this->last_error = $e;
			// some other exception happened
			// add to user error queue
		}
		return $result;
	}
}

?>