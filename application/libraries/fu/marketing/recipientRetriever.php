<?php
namespace fu\marketing;


use fu\Exceptions\InvalidArgument;

abstract class RecipientRetriever {

	protected $texting;
	protected $exclude_groups,$exclude_individuals;
	public function __construct(){

	}

	public function setPossibleRecipients(array $recipients)
	{
		$this->exclude_groups = [];
		$this->exclude_individuals = [];
		if(isset($recipients['groups']) || isset($recipients['individuals']))
		{
			$this->groups = $recipients['groups'] ;
			$this->individuals = $recipients['individuals'];
		} else {
			throw new InvalidArgument();
		}
	}

	public function setIgnoredRecipients(array $recipients)
	{
		if(isset($recipients['exclude_groups']) || isset($recipients['exclude_individuals']))
		{
			$this->exclude_groups = $recipients['exclude_groups'] ;
			$this->exclude_individuals= $recipients['exclude_individuals'];
		} else {
			throw new InvalidArgument();
		}
	}

	public function limitToTexting()
	{
		$this->texting = true;
	}

	public abstract function setDelay($delay);

	public abstract function getRecipients();

    public abstract function setCampaign($campaign);

}