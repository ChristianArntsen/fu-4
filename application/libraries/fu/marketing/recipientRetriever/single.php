<?php
namespace fu\marketing\recipientRetriever;

class single extends \fu\marketing\recipientRetriever{
    public function setIgnoredRecipients(array $recipients)
    {
        // TODO: Implement setIgnoredRecipients() method.
    }

    public function setDelay($delay)
    {
        // TODO: Implement setDelay() method.
    }

    private $recipient;

    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->model('Person');
        $this->CI->load->model('Course');
    }


    public function limitToEmail()
    {
        $this->texting = false;
    }

    public function setCampaign($campaign)
    {
        $this->course_id = $campaign->course_id;
    }

    public function getRecipients()
    {
       return $this->recipient;
    }

    public function setRecipients($recipient)
    {
        $select = $this->CI->db
            ->join("customers","people.person_id = customers.person_id")
            ->select(array("people.person_id","people.email","people.birthday",'customers.course_id',"cell_phone_number","phone_number","first_name","last_name"))
            ->from('people');

        $select->where_in('foreup_people.person_id',$recipient);
        $select->group_by('person_id');
        $select->order_by('person_id');

        $this->recipient = $select->get()->result_array();
    }


}