<?php
namespace fu\marketing\recipientRetriever;

class unsent extends \fu\marketing\recipientRetriever{

    private $possibleRecipients;
    private $campaign_id;

    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->model('Person');
        $this->CI->load->model('Course');
    }


    public function setDelay($delay)
    {
        // TODO: Implement setDelay() method.
    }


    public function limitToEmail()
    {
        $this->texting = false;
    }

    public function setCampaign($campaign)
    {
        $this->course_id = $campaign->course_id;
    }

//'person_id','course_id', 'msisdn', 'texting_status', and 'keyword'.
    public function getRecipients()
    {
        if(!isset($this->course_id)){
            throw new \fu\Exceptions\InvalidArgument("Must set campaign before retrieving recipients.");
        }

        $groups = [];
        $individuals = [];
        $exclude_groups = [];
        $exclude_individuals = [];



        if(count($this->groups) > 0) {
            $groups = $this->getRecipientsInGroup($this->groups);
        }
        if(count($this->exclude_groups) > 0) {
            $exclude_groups = $this->getRecipientsInGroup($this->exclude_groups);
        }

        if(count($this->individuals) > 0){
            $select = $this->CI->db
                ->join("customers","people.person_id = customers.person_id")
                ->select(array("people.person_id","people.email","people.birthday",'customers.course_id',"cell_phone_number","phone_number","first_name","last_name"))
                ->from('people');
            if($this->texting){
                $this->addTextSelectionCriteria();
            }

            $select->where_in('foreup_people.person_id',$this->individuals);
            $select->group_by('person_id');
            $select->order_by('person_id');

            $individuals = $select->get()->result_array();
        }

        if(count($this->exclude_individuals) > 0){
            $select = $this->CI->db
                ->join("customers","people.person_id = customers.person_id")
                ->select(array("people.person_id","people.email","people.birthday",'customers.course_id',"cell_phone_number","phone_number","first_name","last_name"))
                ->from('people');
            if($this->texting){
                $this->addTextSelectionCriteria();
            }

            $select->where_in('foreup_people.person_id',$this->exclude_individuals);
            $select->group_by('person_id');
            $select->order_by('person_id');

            $select->join('customers','people.person_id = customers.person_id');
            $select->where("customers.deleted !=",1);
            $exclude_individuals = $select->get()->result_array();
        }

        $exclude_result = array_merge($exclude_individuals,$exclude_groups);
        $result = array_merge($individuals,$groups);

        //$result = array_diff($result, $exclude_result);

        return $result;
    }


    private function getRecipientsInGroup($passed_groups)
    {
        if (array_search(0, $passed_groups) !== false) {
            $course_ids = array();
            $this->CI->Course->get_linked_course_ids($course_ids, 'shared_customers',$this->course_id );
            $course_ids[] = $this->course_id;
            $select = $this->CI->db
                ->select(array("people.person_id", "people.email", "birthday","cell_phone_number","phone_number","customers.course_id AS course_id","first_name","last_name"))
                ->from('people')
                ->group_by('people.email')
            ;
            $select->join('customers','people.person_id = customers.person_id');
            //$select->join('customer_group_members', 'customer_group_members.person_id = people.person_id', 'left');
            //$select->join('customer_groups', 'customer_groups.group_id = customer_group_members.group_id', 'right');
            $select->where_in('customers.course_id', $course_ids);
            $select->where('email !=','');
        } else if ($passed_groups) {
            $select = $this->CI->db
                ->select(array("people.person_id", "people.email", "birthday","cell_phone_number","phone_number","customer_groups.course_id AS course_id","first_name","last_name"))
                ->from('people');
            $select->join('customer_group_members', 'customer_group_members.person_id = people.person_id', 'left');
            $select->join('foreup_customer_groups', 'customer_groups.group_id = customer_group_members.group_id', 'left');

            $select->join('customers','people.person_id = customers.person_id');
            $select->or_where_in('customer_group_members.group_id',$passed_groups);
        }
        if($this->texting){
            $this->addTextSelectionCriteria();
        }

        $select->where("customers.deleted !=",1);
        $groups = $select->get()->result_array();
        return $groups;
    }


    private function addTextSelectionCriteria()
    {
        $this->CI->db->join("marketing_texting","people.person_id = marketing_texting.person_id","right")
            ->select(["msisdn","texting_status","keyword"])
            ->where("marketing_texting.texting_status","subscribed");
    }
}