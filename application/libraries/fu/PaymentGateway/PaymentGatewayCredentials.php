<?php
namespace fu\PaymentGateway;

use fu\PaymentGateway\PaymentGatewayException\GeneralFailureException;

class PaymentGatewayCredentials
{
	private $id;
	private $provider;
	private $valid_providers = ['mercury','ets','element','apriva','mercuryAch'];
	private $credentials;

	public function __construct(int $id=0, string $provider, array $credentials)
	{
		// will be database key by and by
		$this->id = $id;
		if(!in_array($provider,$this->valid_providers))
			throw new GeneralFailureException('Provider does not exist, or is not implemented');
		$this->provider = $provider;
		$this->setCredentials($credentials);
	}

	public function setCredentials(array $credentials)
	{
		$this->credentials = $credentials;
	}

	public function getCredentials()
	{
		return $this->credentials;
	}

	public function getMercuryCredentials()
	{
		$mercury_keys = ['mercury_id'=>'int','mercury_password'=>'string'];

		return $this->extractCredentials($mercury_keys,'Mercury');

	}

	public function getMercuryE2eCredentials()
	{
		$keys = ['mercury_e2e_password'=>'string','mercury_e2e_id'=>'string'];

		return $this->extractCredentials($keys,'Mercury E2E');
	}

	public function getEtsE2eCredentials()
	{
		$keys = ['e2e_account_id'=>'string','e2e_account_key'=>'string'];

		return $this->extractCredentials($keys,'ETS E2E');
	}

	public function getMercuryAchCredentials()
	{
		return $this->getElementCredentials();
	}

	public function getElementCredentials()
	{
		$keys = ['element_account_id'=>'int','element_account_token'=>'string',
			'element_application_id'=>'int','element_acceptor_id'=>'int'];

		return $this->extractCredentials($keys,'Element');
	}

	public function getAprivaCredentials()
	{
		$keys = ['apriva_username','apriva_password','apriva_product','apriva_key'];

		return $this->extractCredentials($keys,'Apriva');
	}

	public function getETSCredentials()
	{
		$ets_keys = ['ets_key'=>'string'];

		return $this->extractCredentials($ets_keys,'ETS');
	}

	public function extractCredentials($keys,$system) {

		$intersect = array_intersect_key($this->credentials,$keys);

		if(count($intersect)!==count($keys))
		{
			$missing = array_keys(array_diff_key($keys,$intersect));
			throw new GeneralFailureException('Missing configuration parameters for '.$system.': '.\json_encode($missing));
		}

		$this->validateKeys($keys,$intersect);

		return $intersect;
	}

	public function validateKeys ($keys,$intersect)
	{


		foreach($keys as $key=>$value) {
			$valid = true;
			$item = $intersect[$key];
			if ($value === 'int') {
				if (!isset($item) || !is_numeric($item) || !is_int($item * 1)) {
					$valid = false;
				}
			} else {
				if (!isset($item) || !is_string($item) || !strlen($item)) {
					$valid = false;
				}
			}
			if (!$valid) throw new GeneralFailureException('Invalid ' . $key . ' parameter provided. Expected ' . $value . ' but got ' . $item);
		}
	}

}

?>
