<?php
namespace fu\PaymentGateway;

use fu\PaymentGateway\PaymentGatewayException\GeneralFailureException;

class StoredPaymentInfo
{
	protected $token;
	protected $type;
	protected $accountType;
	protected $paymentId;
	protected $cardholderName;
	protected $accountNumber;
	protected $maskedAccount;
	private $validTypes = ['credit_card','ach'];
	private $validAccountTypes = ['Bank','VISA','MasterCard','Discover','JCB','Diners','ACH'];

	public function __construct(string $type, string $token, int $paymentId, string $cardholderName,
	                            $accountNumber,string $accountType,string $maskedAccount = null)
	{
		$this->setType($type);
		$this->setToken($token);
		$this->setAccountType($accountType);
		$this->setPaymentId($paymentId);
		$this->setCardholderName($cardholderName);
		$this->setAccountNumber($accountNumber);
		$this->setMaskedAccount($maskedAccount);

	}

	public function setMaskedAccount($maskedAccount){
		$this->maskedAccount = $maskedAccount;
	}

	public function getMaskedAccount()
	{
		return $this->maskedAccount;
	}

	public function setAccountNumber($accountNumber)
	{
		$this->accountNumber = $accountNumber;

	}

	public function getAccountNumber()
	{
		return $this->accountNumber;

	}

	public function setCardholderName($name)
	{
		$this->cardholderName = $name;

	}

	public function getCardholderName()
	{
		return $this->cardholderName;

	}

	public function setPaymentId($id)
	{
		$this->paymentId = $id;

	}

	public function getPaymentId()
	{
		return $this->paymentId;

	}

	public function setToken($token)
	{
		if(!is_string($token))
		{
			throw new GeneralFailureException('Token must be a string');

		}
		$this->token = $token;

	}

	public function getToken()
	{
		return $this->token;

	}

	public function setType($type)
	{
		if(!is_string($type))
		{
			throw new GeneralFailureException('Type must be a string');

		}

		if(!in_array($type,$this->validTypes))
		{
			throw new GeneralFailureException( 'Type must be in enumeration: ' . implode(',',$this->validTypes) );

		}

	}

	public function getType()
	{
		return $this->type;

	}

	public function setAccountType($type)
	{
		if(!is_string($type))
		{
			throw new GeneralFailureException('Type must be a string');

		}

		if($type==='M/C')$type = 'MasterCard';

		if(!in_array($type,$this->validAccountTypes))
		{
			throw new GeneralFailureException( 'accountType must be in enumeration: ' . implode(',',$this->validAccountTypes) );

		}

	}

	public function getAccountType()
	{
		return $this->accountType;

	}

}

?>