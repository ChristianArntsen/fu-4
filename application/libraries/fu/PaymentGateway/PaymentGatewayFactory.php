<?php
namespace fu\PaymentGateway;

use fu\PaymentGateway\PaymentGatewayException\GeneralFailureException;

class PaymentGatewayFactory
{
	private $paymentGateway;

	private $config;

	private $CI;

	public function __construct(array $config,&$CI = null) // course or terminal
	{
		if(!isset($config['id'])){
			throw new GeneralFailureException('Missing id parameter in PaymentGatewayFactory config');
		}
		if(isset($CI))
			$this->CI = $CI;
		else
			$this->CI = get_instance();

		$this->CI->load->model('sale');
		$this->CI->load->model('Charge_attempt');

		$this->config = $config;

		$conditions_for_mercury_ach = false;

		if( $conditions_for_mercury_ach /* TODO: what do these look like? */){
			$this->paymentGateway = $this->getMercuryAchPaymentGateway();
		}
		elseif(isset($config['mercury_id']) && isset($config['mercury_password'])){
			$this->paymentGateway = $this->getMercuryPaymentGateway();
		}
		elseif (isset($config['ets_key'])){
			$this->paymentGateway = $this->getEtsPaymentGateway();
		}
		elseif(isset($config['mercury_e2e_id'])&&isset($config['mercury_e2e_password'])){
			throw new GeneralFailureException('E2ePaymentGateway not implemented.');
		}
		// here we will add the other options when they are built
		else{
			throw new GeneralFailureException('Payment method incorrect or not implemented');
		}

	}

	public function getPaymentGateway(){
		if(isset($this->paymentGateway))return $this->paymentGateway;
		else{
			throw new GeneralFailureException('Factory failed to initialize properly');
		}
	}

	public function getMercuryAchPaymentGateway()
	{
		return new MercuryAchPaymentGateway($this->config,$this->CI);
	}

	public function getMercuryPaymentGateway()
	{
		return new MercuryPaymentGateway($this->config,$this->CI);
	}

	public function getEtsPaymentGateway()
	{
		return new etsPaymentGateway($this->config,$this->CI);
	}

}

?>