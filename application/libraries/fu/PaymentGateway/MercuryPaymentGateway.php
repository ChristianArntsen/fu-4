<?php
namespace fu\PaymentGateway;

use fu\PaymentGateway\PaymentGatewayException\GeneralFailureException;
use fu\PaymentGateway\PaymentGatewayException\PartialAuthorizationException;
use fu\PaymentGateway\PaymentGatewayException\PaymentDeclinedException;
use function GuzzleHttp\Psr7\_caseless_remove;

class MercuryPaymentGateway implements PaymentGateway
{
	/**
	 * @var StoredPaymentInfo
	 */
	private $storedPayment;

	/**
	 * @var PaymentGatewayCredentials
	 */
	private $paymentGatewayCredentials;

	/**
	 * @var \SimpleXMLElement
	 */
	private $lastResults;

	/**
	 * @var \SimpleXMLElement | null
	 */
	private $lastError;

	private $CI;

	private $config;

	public $processor_id_key = 'mercury_id';

	public $provider = 'mercury';

	public function __construct(array $config,&$CI = null)
	{
		if(isset($CI))
		  $this->CI = $CI;
		else
			$this->CI = get_instance();

		$this->config = $config;
	}

	public function charge($amount,$invoice_id = null, $statement_id = null)
	{
		if(!is_numeric($amount)){
			throw new GeneralFailureException('Amount must be numeric');

		}

		// stored payment info should have been set:
		// course_id, credit_card_id, mercury_id, mercury password,
		// token, cardholder_name
		$config = $this->config;
		if(isset($config['id']))
		  $config['course_id'] = $config['id'];

		$credentials = $this->paymentGatewayCredentials->getMercuryCredentials();

		// validate the config
		//$this->validateConfig($config);

		// hardish things:
		// Hosted_checkout_2
		// move outside of method?:
		// Sale->add_credit_card_payment
		// Charge_attempt->save
		// "$this"->save good status

		// Customer_credit_card->charge
		$CI = get_instance();
		$CI->load->library('Hosted_checkout_2');
		$CI->load->model('Charge_attempt');
		$HC = new \Hosted_checkout_2();

		$HC->set_frequency('Recurring');
		$HC->set_token($this->storedPayment->getToken());
		$HC->set_cardholder_name($this->storedPayment->getCardholderName());
		$HC->set_invoice($this->storedPayment->getAccountNumber());
		$HC->set_merchant_credentials($credentials['mercury_id'], $credentials['mercury_password']);

		$attempts = 0;
		$transaction_results = 'Mercury request failed.';
		while($attempts <3){
			$transaction_results = $HC->token_transaction('Sale', $amount, '0.00', '0.00');
			if(isset($transaction_results->PurchaseAmount))break;
			$attempts ++;
		}

		if(!$transaction_results){
			throw new GeneralFailureException('No response from Mercury server');
		}



		$this->lastResults = $transaction_results;

		// vantive documentation used:
		// https://www.vantiv.com/content/dam/vantiv/developers/eCommerce/HostedCheckout%20eCom%20Integration%20Guide%2012112014%20PDF.pdf
		// See page 43

		$message = isset($transaction_results->Message)? ': '.$transaction_results->Message : '';

		$this->updatePaymentStatus($this->storedPayment->getPaymentId(),$transaction_results);
		$this->CI->Charge_attempt->save($this->storedPayment->getAccountNumber(), null,
			$transaction_results->AuthorizeAmount, date('Y-m-d H:i:s'),
			"'".$transaction_results->Status."'",
			$transaction_results->Status.$message, $this->storedPayment->getPaymentId());

		// return success
		if((string) $transaction_results->Status === 'Approved'){
			if((float) $transaction_results->PurchaseAmount - (float) $transaction_results->AuthorizeAmount !== 0.00){
				throw new PartialAuthorizationException('Payment in full has not been received and additional tender will need to be requested');
			}


			return true;
		}else{
			$this->lastError = $transaction_results;
			$status = (string) $transaction_results->Status;

			switch ($status){
				case 'AuthFail':
					throw new GeneralFailureException('Authentication failed for MerchantID/Password' . $message);
				case 'Declined':
				case 'CardDeclined':
					throw new PaymentDeclinedException('The transaction was declined' . $message);
				case 'Mercury Internal Error':
				case 'MercuryInternalFail':
				case 'MercuryInternalError':
					throw new GeneralFailureException('An error occurred internal to Mercury' . $message);
				case 'ValidateFail':
					throw new GeneralFailureException('Validation of the request failed' . $message);
				case 'Error':
					throw new GeneralFailureException('A transaction processing error occurred' . $message);
				// We should not see the following errors,
				// but I want them to be on the radar in case I misinterpreted the Docs...
				case 'LoadPaymentInfoFail':
					//
				case 'ProcessPaymentFail':
					//
				case 'PreAuthFailDBErr':
					//
				case 'SalesNotCompletedDBErr':
					//
				case 'ValidationCCFail':
					//
				case 'ValidationServerSideFailure':
					//
				case 'ValidateNameFail':
					//
				default:
					throw new GeneralFailureException('Unexpected Status received: '.$status);
			}
		}

	}

	public function return($amount)
	{
		if(!is_numeric($amount)){
			throw new GeneralFailureException('Amount must be numeric');

		}

		// TODO: Implement refund() method.

	}

	public function void()
	{
		// TODO: Implement void() method.
	}

	public function setStoredPayment(StoredPaymentInfo $storedPaymentInfo)
	{
		$this->storedPayment=$storedPaymentInfo;
	}

	public function setPaymentGatewayCredentials(PaymentGatewayCredentials $credentials)
	{
		$this->paymentGatewayCredentials = $credentials;
	}

	public function saveCreditCardPayment(int $processor_id)
	{
		$invoice = $this->CI->Sale->add_credit_card_payment(
			array('mercury_id'=>$processor_id,
				'tran_type'=>'CreditSaleToken',
				'frequency'=>'Recurring'
			)
		);

		return $invoice;
	}

	public function updatePaymentStatus(int $invoice,$transaction_results,$transaction_time = null){
		if(!isset($transaction_time))
			$transaction_time = date('Y-m-d H:i:s');

		$payment_data = array(
			'acq_ref_data'=>(string)$transaction_results->AcqRefData,
			'auth_code'=>(string)$transaction_results->AuthCode,
			'auth_amount'=>(string)$transaction_results->AuthorizeAmount,
			'avs_result'=>(string)$transaction_results->AVSResult,
			'batch_no'=>(string)$transaction_results->BatchNo,
			'card_type'=>(string)$transaction_results->CardType,
			'cvv_result'=>(string)$transaction_results->CVVResult,
			'gratuity_amount'=>(string)$transaction_results->GratuityAmount,
			'masked_account'=>(string)$transaction_results->Account,
			'status_message'=>(string)$transaction_results->Message,
			'amount'=>(string)$transaction_results->PurchaseAmount,
			'ref_no'=>(string)$transaction_results->RefNo,
			'status'=>(string)$transaction_results->Status,
			'token'=>(string)$transaction_results->Token,
			'process_data'=>(string)$transaction_results->ProcessData,
			'trans_post_time'	=> (string) $transaction_time
		);

		$this->CI->Sale->update_credit_card_payment($invoice, $payment_data);

	}

	public function updateCustomerCard($success = false)
	{
		//
	}

	public function getLastError()
	{
		return $this->lastError;
	}

	public function getLastResults()
	{
		// TODO: provide generic PaymentGatewayResults object
		return $this->lastResults;
	}

	public function getAuthorizedAmount()
	{
		return (float) $this->getLastResults()->AuthorizeAmount;
	}

}

?>