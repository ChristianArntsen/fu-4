<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 6/7/2016
 * Time: 12:09 AM
 */

namespace fu\Teetime;


class Player
{
	private $person_id,$price_class,$cart_paid,$person_name,$person_paid,$position;

	public function __construct($person_id,$price_class,$cart_paid,$person_name,$person_paid)
	{
		$this->person_id = $person_id;
		$this->price_class = $price_class;
		$this->cart_paid = $cart_paid;
		$this->person_name = $person_name;
		$this->person_paid = $person_paid;
	}

	/**
	 * @return mixed
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param mixed $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}

	/**
	 * @return mixed
	 */
	public function getPersonPaid()
	{
		return $this->person_paid;
	}

	/**
	 * @param mixed $person_paid
	 */
	public function setPersonPaid($person_paid)
	{
		$this->person_paid = $person_paid;
	}

	/**
	 * @return mixed
	 */
	public function getPersonId()
	{
		return $this->person_id;
	}

	/**
	 * @param mixed $person_id
	 */
	public function setPersonId($person_id)
	{
		$this->person_id = $person_id;
	}

	/**
	 * @return mixed
	 */
	public function getPriceClass()
	{
		return $this->price_class;
	}

	/**
	 * @param mixed $price_class
	 */
	public function setPriceClass($price_class)
	{
		$this->price_class = $price_class;
	}

	/**
	 * @return mixed
	 */
	public function getCartPaid()
	{
		return $this->cart_paid;
	}

	/**
	 * @param mixed $cart_paid
	 */
	public function setCartPaid($cart_paid)
	{
		$this->cart_paid = $cart_paid;
	}

	/**
	 * @return mixed
	 */
	public function getPersonName()
	{
		return $this->person_name;
	}

	/**
	 * @param mixed $person_name
	 */
	public function setPersonName($person_name)
	{
		$this->person_name = $person_name;
	}



}