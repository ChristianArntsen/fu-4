<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 12/28/2015
 * Time: 9:07 PM
 */
namespace fu;

use Doctrine\Common\ClassLoader,
    Doctrine\ORM\Configuration,
    Doctrine\ORM\EntityManager,
    Doctrine\Common\Cache\ArrayCache,
    Doctrine\DBAL\Logging\EchoSQLLogger;

class Doctrine {

    public $em = null;

    public function __construct()
    {

        // Set up caches
        $config = new Configuration;
        $cache = new ArrayCache;
        $config->setMetadataCacheImpl($cache);
        $driverImpl = $config->newDefaultAnnotationDriver(array(APPPATH.'models/Entities'));
        $config->setMetadataDriverImpl($driverImpl);
        $config->setQueryCacheImpl($cache);

        $config->setQueryCacheImpl($cache);

        // Set up logger
        $logger = new EchoSQLLogger;
        $config->setSQLLogger($logger);

        // Proxy configuration
        $config->setProxyDir(APPPATH.'/modules/rest/models/proxies');
        $config->setProxyNamespace('rest/models/proxies');
        $config->setAutoGenerateProxyClasses( TRUE );

        $CI = & get_instance();
        $CI->load->getDatabaseConnection('slave');
        // Database connection information
        $connectionOptions = array(
            'driver' => 'pdo_mysql',
            'user' =>     $CI->db->username,
            'password' => $CI->db->password,
            'host' =>     $CI->db->hostname,
            'dbname' =>   $CI->db->database
        );

        // Create EntityManager
        $this->em = EntityManager::create($connectionOptions, $config);
    }
}