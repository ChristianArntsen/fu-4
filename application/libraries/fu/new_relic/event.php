<?php

namespace fu\new_relic;
use Respect\Validation\Validator;
class event implements \JsonSerializable
{
    public $eventType;
    public function jsonSerialize()
    {
        Validator::stringType()
            ->notEmpty()
            ->setName('eventType')
            ->check($this->eventType);
        return $this;
    }
}