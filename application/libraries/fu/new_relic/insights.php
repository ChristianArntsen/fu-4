<?php

namespace fu\new_relic;
use GuzzleHttp\Client;
use fu\new_relic\event_collection;
use fu\new_relic\event;
use Respect\Validation\Validator;

class insights
{
    private $key;
    private $client;
    public function __construct()
    {
        $this->client = new Client([
            #You need to change it to your account number
            'base_url' => 'https://insights-collector.newrelic.com/v1/accounts/191173/'
        ]);
        $this->key = "afQ_lS5XMthgSfWeuEsiHBd_83-KfRJ_";
        $baseUrl = $this->client->getBaseUrl();
        Validator::notEmpty()
            ->url()
            ->endsWith('/')
            ->setName("URL for NewRelic's Insights API must be valid and have a trailing slash")
            ->assert($baseUrl);
    }
    public function send_events(event_collection $events)
    {
        $promise = $this->client->post('events', [
            'body' => json_encode($events),
            'headers' => [
                'X-Insert-Key' => $this->key,
                'Content-Type' => 'application/json',
            ]
        ]);
        return $promise;
    }

    public function send_event(event $event)
    {
        $events = new event_collection();
        $events->add($event);
        return $this->send_events($events);
    }
}