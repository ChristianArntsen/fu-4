<?php
namespace fu\data_import\RecordInserters;

class RecordInserterHelper {

    public static function is_scientific_notation($value = false){

        if(empty($value)){
            return false;
        }
        $match = preg_match('/[eE][+\-]/', $value);
        return !empty($match);
    }
}