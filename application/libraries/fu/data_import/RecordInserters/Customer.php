<?php
namespace fu\data_import\RecordInserters;

use Carbon\Carbon;

class Customer extends RecordInserter {

    private $error = false;
    private $current_record = false;

    public function __construct($CI_Model = false){

        if(!$CI_Model){
            $CI = get_instance();
            $CI->load->model('v2/Customer_model', 'Model');
            $this->Model =& $CI->Model;
        }else{
            $this->Model = $CI_Model;
        }
    }

    public function has_errors($record_data){

        if(isset($record_data['account_number']) && RecordInserterHelper::is_scientific_notation($record_data['account_number'])){
            return 'Account number is in scientific notation';
        }

        if(isset($record_data['account_balance']) && RecordInserterHelper::is_scientific_notation($record_data['account_balance'])){
            return 'Customer credits is in scientific notation';
        }

        if(isset($record_data['account_balance_limit']) && RecordInserterHelper::is_scientific_notation($record_data['account_balance_limit'])){
            return 'Customer credits limit is in scientific notation';
        }

        if(isset($record_data['member_account_balance']) && RecordInserterHelper::is_scientific_notation($record_data['member_account_balance'])){
            return 'Member account balance is in scientific notation';
        }

        if(isset($record_data['member_account_limit']) && RecordInserterHelper::is_scientific_notation($record_data['member_account_limit'])){
            return 'Member account limit is in scientific notation';
        }

        if(isset($record_data['loyalty_points']) && RecordInserterHelper::is_scientific_notation($record_data['loyalty_points'])){
            return 'Loyalty points is in scientific notation';
        }

        if(isset($record_data['phone_number']) && RecordInserterHelper::is_scientific_notation($record_data['phone_number'])){
            return 'Phone number is in scientific notation';
        }

        if(isset($record_data['cell_phone_number']) && RecordInserterHelper::is_scientific_notation($record_data['cell_phone_number'])){
            return 'Cell phone number is in scientific notation';
        }

        return false;
    }

    public function get_current_record(){
        return $this->current_record;
    }

    public function transform_data($record_data){

        // Translate group names into group IDs
        if(!empty($record_data['groups'])){

            $group_ids = [];
            $groups = explode(',', $record_data['groups']);
            $record_data['groups'] = false;

            foreach($groups as $group_name){
                $group_id = $this->Model->get_group_id($group_name, $record_data['course_id']);
                if(!empty($group_id)){
                    $group_ids[$group_id] = ['group_id' => $group_id];
                }
            }
            $record_data['groups'] = array_values($group_ids);
        }

        if(!empty($record_data['phone_number'])){
            $number = preg_replace("/[^0-9]/", "", $record_data['phone_number']);
            if(strlen($number) < 7){
                unset($record_data['phone_number']);
            }
        }

        if(!empty($record_data['cell_phone_number'])){
            $number = preg_replace("/[^0-9]/", "", $record_data['cell_phone_number']);
            if(strlen($number) < 7){
                unset($record_data['cell_phone_number']);
            }
        }

        if(!empty($record_data['email'])){
            if(!filter_var($record_data['email'], FILTER_VALIDATE_EMAIL)){
                unset($record_data['email']);
            }
        }

        if(!empty($record_data['member'])){
            $record_data['member'] = (strtolower($record_data['member']) == 'y' || $record_data['member'] == 1);
        }else{
            $record_data['member'] = false;
        }

        if(!empty($record_data['use_loyalty'])){
            $record_data['use_loyalty'] = (strtolower($record_data['use_loyalty']) == 'y' || $record_data['use_loyalty'] == 1);
        }else{
            $record_data['use_loyalty'] = false;
        }

        if(!empty($record_data['non_taxable'])){
            $record_data['taxable'] = !(strtolower($record_data['non_taxable']) == 'y' || $record_data['non_taxable'] == 1);
        }else{
            $record_data['taxable'] = true;
        }

        if(!empty($record_data['password'])){
            $record_data['confirm_password'] = $record_data['password'];
        }

        if(!empty($record_data['birthday'])){
            try {
                $date = new Carbon($record_data['birthday'], 'UTC');
                $record_data['birthday'] = $date->toDateString();
            }catch(\Exception $e){
                unset($record_data['birthday']);
            }
        }

        if(!empty($record_data['date_created'])){
            try {
                $date = new Carbon($record_data['date_created'], 'UTC');
                $record_data['date_created'] = $date->toDateString();
            }catch(\Exception $e){
                unset($record_data['date_created']);
            }
        }

        return $record_data;
    }

    public function insert($record_index, $record_data = []){

        $this->error = false;
        $this->current_record = $record_data;

        $record_data = $this->transform_data($record_data);

        if(empty($record_data)){
            $this->error = 'Empty row';
            return false;
        }

        $duplicate_record_index = $this->is_duplicate($record_index, $record_data);
        if($duplicate_record_index !== false){
            $this->error = 'Duplicate of record #'.$duplicate_record_index;
            return false;
        }

        $error = $this->has_errors($record_data);
        if($error){
            $this->error = $error;
            return false;
        }

        // Save customer to database
        $this->Model->send_booking_credentials = false;
        $person_id = $this->Model->save(null, $record_data);

        // If the customer failed to save
        if(empty($person_id)){
            if($this->Model->error){
                $this->error = $this->Model->error;
            }
            return false;
        }

        return (int) $person_id;
    }

    public function get_error(){
        return $this->error;
    }
}