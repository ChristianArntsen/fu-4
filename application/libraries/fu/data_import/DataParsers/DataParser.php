<?php
namespace fu\data_import\DataParsers;

abstract class DataParser {

    private $source_path;
    private $settings;

    abstract public function each($callback);
    abstract public function get_total_records();
}