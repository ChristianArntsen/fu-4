<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 10/9/2015
 * Time: 10:39 AM
 */

namespace fu\reminders;

use Aws\Sqs\SqsClient;

class ReminderQueue
{

    public function __construct($CI)
    {
		$this->CI = $CI;
    }

    public function sendToQueue($reminderId,$timestamp)
    {
	    $s3Config = $this->CI->config->item('s3');
	    $sqsConfig = $this->CI->config->item('sqs');
	    $client = new SqsClient(array(
		    'region' => 'us-west-2',
		    'credentials' => [
			    'key' => $s3Config["access_key"],
			    'secret' => $s3Config["secret"],
		    ],
		    'version' => '2012-11-05'
	    ));

	    $bodyContent = [
		    "reminderId" => $reminderId,
		    "timestamp" => $timestamp
	    ];

	    $client->sendMessage(array(
		    'QueueUrl' => $sqsConfig['reminder_url'],
		    'MessageBody' => json_encode($bodyContent),
	    ));
    }

    public function getMessages()
    {
	    $s3Config = $this->CI->config->item('s3');
	    $sqsConfig = $this->CI->config->item('sqs');
	    $client = SqsClient::factory(array(
		    'region' => 'us-west-2',
		    'credentials' => [
			    'key' => $s3Config["access_key"],
			    'secret' => $s3Config["secret"],
		    ],
		    'version' => '2012-11-05'
	    ));
	    $result = $client->receiveMessage(array(
		    'QueueUrl'    => $sqsConfig['reminder_url'],
		    'WaitTimeSeconds' => 5,
		    'MaxNumberOfMessages' => 10
	    ));

	    return $result;
    }

    public function deleteMessage($message)
    {
	    $s3Config = $this->CI->config->item('s3');
	    $sqsConfig = $this->CI->config->item('sqs');
	    $client = SqsClient::factory(array(
		    'region' => 'us-west-2',
		    'credentials' => [
			    'key' => $s3Config["access_key"],
			    'secret' => $s3Config["secret"],
		    ],
		    'version' => '2012-11-05'
	    ));
	    $client->deleteMessage(array(
		    'QueueUrl' => $sqsConfig['reminder_url'],
		    'ReceiptHandle' => $message['ReceiptHandle'],
	    ));
    }
}