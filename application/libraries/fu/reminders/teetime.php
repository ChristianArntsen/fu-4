<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 10/9/2015
 * Time: 10:39 AM
 */

namespace fu\reminders;


use fu\reports\ReportFactory;

class Teetime
{

    private $range;
    /**
     * @param \fu\reports\MovingRanges\MovingRange $range
     */
    public function __construct($range,$teesheet_id)
    {
        $this->CI = & get_instance();
        $this->CI->load->model("teetime");
        $this->range = $range;



        $datetime = new \fu\reports\ParamDate([
            "column"=>"teetime.start_datetime",
            "typeOfMovingRange"=>"days",
            "movingRange"=>"-1"
        ]);

        $parameters = new \fu\reports\ReportParameters();
        $parameters->setDateFromObject($datetime);

        $teetimeReport = \fu\reports\ReportFactory::create("teetime",[
            "date_range"=>[
                "column"=>"teetime.start_datetime",
                "movingRange"=>"-1",
                "typeOfMovingRange"=>"days",
            ]
        ]);

        $teetimeReport->getData();
    }

    public function sendReminders()
    {

    }
}