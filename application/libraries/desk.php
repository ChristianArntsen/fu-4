<?php
class Desk
{
    function sign_multipass_token($employee_id) {
        $CI =& get_instance();
        $CI->load->model('Employee');

        $subdomain = $CI->config->item('desk_subdomain'); //'foreup.desk.com';
        $api_key   = $CI->config->item('desk_api_key'); //'498cd4029889bfa60168d8fabda9b4bfaf43fac3';
        $employee_info = $CI->Employee->get_info($employee_id);

        // Create the encryption key using a 16 byte SHA1 digest of your api key and subdomain
        $salted = $api_key . $subdomain;
        $digest = hash('sha1', $salted, true);
        $key    = substr($digest, 0, 16);
        // Generate a random 16 byte IV
        $iv = mcrypt_create_iv(16);
        // Build json data
        $user_data = array(
            'uid' => $employee_id,
            'customer_email' => $employee_info->email,
            'customer_name' => $employee_info->first_name.' '.$employee_info->last_name,
            'expires' => date("c", strtotime("+100 years"))
        );
        $data = json_encode($user_data);
        // PHP's mcrypt library does not perform padding by default
        // Pad using standard PKCS#5 padding with block size of 16 bytes
        $pad = 16 - (strlen($data) % 16);
        $data = $data . str_repeat(chr($pad), $pad);
        // Encrypt data using AES128-cbc
        $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128,'','cbc','');
        mcrypt_generic_init($cipher, $key, $iv);
        $multipass = mcrypt_generic($cipher,$data);
        mcrypt_generic_deinit($cipher);
        // Prepend the IV to the encrypted data
        // This will be extracted and used for decryption
        $multipass = $iv . $multipass;
        // Base64 encode the encrypted data
        $multipass = base64_encode($multipass);
        // Build an HMAC-SHA1 signature using the encoded string and your api key
        $signature = hash_hmac("sha1", $multipass, $api_key, true);
        // Base64 encode the signature
        $signature = base64_encode($signature);
        // Finally, URL encode the multipass and signature
        $multipass = urlencode($multipass);
        $signature = urlencode($signature);

        // Save multipass and signature to employee
        $data = array(
            'desk_multipass' => $multipass,
            'desk_signature' => $signature
        );
        $this->update_employee($employee_id, $data);
        // print "multipass: " . $multipass . "\n";
        // print "signature: " . $signature . "\n";
        return $data;
    }

    function update_employee($person_id, $data) {
        $CI =& get_instance();
        $CI->db->where('person_id', $person_id);
        $CI->db->limit(1);
        $CI->db->update('employees', $data);
    }

}