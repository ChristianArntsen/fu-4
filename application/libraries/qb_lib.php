<?php
require_once 'QuickBooks.php';
error_reporting(E_ALL | E_STRICT);

class Qb_lib {
		
	 function __construct(){
	}	
	
	public static function create_map(){
		$map = array(
			QUICKBOOKS_ADD_SALESRECEIPT => array( '_salesreceipt_add_request', '_salesreceipt_add_response' )
		);
		return $map;
	}
	
	public static function create_error_map(){
		$errmap = array(
		'0x80040400' => '_my_function_name_0x80040400_errors', 
		3070 => '_quickbooks_error_stringtoolong',				// Whenever a string is too long to fit in a field, call this function: _quickbooks_error_stringtolong()
		//'*' => '_quickbooks_error_catchall' 				// Using a key value of '*' will catch any errors which were not caught by another error handler
		);
		return $errmap;
	}
	
	public static function create_hooks(){
		$hooks = array(	
			// QUICKBOOKS_HANDLERS_HOOK_LOGINSUCCESS => '_quickbooks_hook_loginsuccess', 	// Run this function whenever a successful login occurs
		);
		return $hooks;
	}
	
	public static function create_driver_options(){
		$driver_options = array(		// See the comments in the QuickBooks/Driver/<YOUR DRIVER HERE>.php file ( i.e. 'Mysql.php', etc. )
		 'max_log_history' => 32000,	// Limit the number of quickbooks_log entries to 1024
		 'max_queue_history' => 1024, 	// Limit the number of *successfully processed* quickbooks_queue entries to 64
		);
		return $driver_options;
	}
	
	public static function create_handler_options(){
		$handler_options = array(
			'deny_concurrent_logins' => false 
		);
		return $handler_options;
	}
	
	public static function create_soap_options(){
		$soap_options = array(		
		);
		return $soap_options;
	}
	
	public static function create_callback_options(){
		$callback_options = array(
		);
		return $callback_options;
	}
	
	
	
	static function create_qbxml($result){
		
		$salesreceipt = new QUICKBOOKS_OBJECT_SALESRECEIPT();
		
		$salesreceipt->setCustomerName($result[0]['customer_name']); //customer name
		$salesreceipt->setTxnDate($result[0]['sale_date']); //date
		$salesreceipt->setRefNumber($result[0]['sale_id']); //str
		
		$cnt = count($result);
		
		if($cnt > 2){
			for($i=1; $i < $cnt ;$i++){
				$salesreceiptline = new QuickBooks_Object_SalesReceipt_SalesReceiptLine();
				$salesreceiptline->setItemName($result[$i]['item_number']); //str
				$salesreceiptline->setDesc($result[$i]['item_name']); //str
				$salesreceiptline->setQuantity($result[$i]['quantity_purchased']);//integer
				$salesreceiptline->setRate(0.00); //decimal
				$salesreceiptline->setAmount($result[$i]['subtotal']); //decimal
				$salesreceiptline->setSalesTaxCodeName('Non'); //str
				$salesreceipt->addSalesReceiptLine($salesreceiptline);
				
			}
			
		}else{
				$salesreceiptline = new QuickBooks_Object_SalesReceipt_SalesReceiptLine();
				$salesreceiptline->setItemName($result[1]['item_number']); //str
				$salesreceiptline->setDesc($result[1]['item_name']); //str
				$salesreceiptline->setQuantity($result[1]['quantity_purchased']);//integer
				$salesreceiptline->setRate(0.00); //decimal
				$salesreceiptline->setAmount($result[1]['subtotal']); //decimal
				$salesreceiptline->setSalesTaxCodeName('Non'); //str
				$salesreceipt->addSalesReceiptLine($salesreceiptline);		
				
		}
		
		$qbxml = $salesreceipt->asQBXML(QUICKBOOKS_ADD_SALESRECEIPT);	
				
		$xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="2.0"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
			'
			.$qbxml.
		'</QBXMLMsgsRq>
		</QBXML>';
		
		return $xml;
	}
	
	function test($result){
		return print_r($result);
	}


}

?>
