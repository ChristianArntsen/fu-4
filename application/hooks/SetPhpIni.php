<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 8/20/2015
 * Time: 1:45 PM
 */

class SetPhpIni
{
    public function setPhpSettings()
    {
        $CI =& get_instance();
        if($CI->config->item('php'))
        {
            $phpIniSettings = $CI->config->item('php');
            foreach($phpIniSettings as $setting=>$value){
                ini_set($setting,$value);
            }
        }
    }
}