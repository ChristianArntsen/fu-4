<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 *  SETTINGS IN THIS FILE ARE INTENDED TO OVERRIDE SETTINGS IN:
 *  application/config/config.php
 */

// Setting this to '' to pair with the root .htaccess (or native config) file rewrites
$config['index_page'] = 'index.php';

// HTTPS enforcement for ALL pages.
$config['force_https'] = false;
$config['load_3rd_party_tools'] = false;


$config['sqs']['course_events_url']="https://sqs.us-west-2.amazonaws.com/651827031983/courseEventsDev";
$config['sqs']['reminder_url']="https://sqs.us-west-2.amazonaws.com/651827031983/DevReminderQueue";