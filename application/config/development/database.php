<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|  ['hostname'] The hostname of your database server.
|  ['username'] The username used to connect to the database
|  ['password'] The password used to connect to the database
|  ['database'] The name of the database you want to connect to
|  ['dbdriver'] The database type. ie: mysql.  Currently supported:
             mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|  ['dbprefix'] You can add an optional prefix, which will be added
|            to the table name when using the  Active Record class
|  ['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|  ['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|  ['cache_on'] TRUE/FALSE - Enables/disables query caching
|  ['cachedir'] The path to the folder where cache files should be stored
|  ['char_set'] The character set used in communicating with the database
|  ['dbcollat'] The character collation used in communicating with the database
|  ['swap_pre'] A default table prefix that should be swapped with the dbprefix
|  ['autoinit'] Whether or not to automatically initialize the database.
|  ['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|                    - good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'admin';
$db['default']['password'] = 'password';
$db['default']['database'] = 'foreup_dev';
$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = 'foreup_';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;
$db['slave'] = $db['default'];


$backup_group = 'backup';

$db['backup']['hostname'] = 'localhost';
$db['backup']['username'] = 'ejacketa_class';
$db['backup']['password'] = 'ejacketa_class';
$db['backup']['database'] = 'ejacketa_pos';
$db['backup']['dbdriver'] = 'mysqli';
$db['backup']['dbprefix'] = 'foreup_';
$db['backup']['pconnect'] = FALSE;
$db['backup']['db_debug'] = TRUE;
$db['backup']['cache_on'] = FALSE;
$db['backup']['cachedir'] = '';
$db['backup']['char_set'] = 'utf8';
$db['backup']['dbcollat'] = 'utf8_general_ci';
$db['backup']['swap_pre'] = '';
$db['backup']['autoinit'] = FALSE;
$db['backup']['stricton'] = FALSE;

$db['slave'] = $db['default'];
$db['jenkins'] = $db['default'];
$db['jenkins']['database'] = 'test_db';
$db['jenkins']['username'] = 'jenkins';
$db['jenkins']['password'] = 'testing';

$db['slave']['failover'][0]=$db['default'];


$db['datastore']['hostname'] = 'localhost';//'23.253.48.189';
$db['datastore']['username'] = 'admin';
$db['datastore']['password'] = 'password';
$db['datastore']['database'] = 'foreup_dev';
$db['datastore']['dbdriver'] = 'mysqli';
$db['datastore']['dbprefix'] = 'foreup_';
$db['datastore']['pconnect'] = FALSE;
$db['datastore']['db_debug'] = TRUE;
$db['datastore']['cache_on'] = FALSE;
$db['datastore']['cachedir'] = '';
$db['datastore']['char_set'] = 'utf8';
$db['datastore']['dbcollat'] = 'utf8_general_ci';
$db['datastore']['swap_pre'] = '';
$db['datastore']['autoinit'] = TRUE;
$db['datastore']['stricton'] = FALSE;

$db['datastore'] = $db['default'];
/* End of file database.php */
/* Location: ./application/config/database.php */
