<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login";
$route['404_override'] = '';
$route['no_access/(:any)'] = "no_access/index/$1";
$route['reports/(summary_giftcards)/(:any)'] = "reports/$1/$2";
$route['reports/summary_giftcards'] = "reports/excel_export";
$route['reports/(summary_account_balances)/(:any)'] = "reports/$1/$2";
$route['reports/summary_account_balances'] = "reports/excel_export";
$route['reports/(detailed_account_transactions)/(:any)'] = "reports/$1/$2";
$route['reports/detailed_account_transactions'] = "reports/excel_export";
$route['reports/(summary_rainchecks)/(:any)'] = "reports/$1/$2";
$route['reports/summary_rainchecks'] = "reports/excel_export";
$route['reports/(summary_tips)/(:any)'] = "reports/$1/$2";
$route['reports/summary_tips'] = "reports/excel_export";
$route['reports/(summary_punch_cards)/(:any)'] = "reports/$1/$2";
$route['reports/summary_punch_cards'] = "reports/excel_export";
$route['reports/(summary_member_passes)/(:any)'] = "reports/$1/$2";
$route['reports/summary_member_passes'] = "reports/excel_export";
$route['reports/(summary_food_minimum)/(:any)'] = "reports/$1/$2";
$route['reports/summary_food_minimum'] = "reports/excel_export";
$route['reports/(summary_:any)/(:any)/(:any)/(:any)'] = "reports/$1/$2/$3/$4";
$route['reports/summary_(:any)'] = "reports/date_input_excel_export/$1";
$route['reports/(graphical_:any)/(:any)/(:any)/(:any)'] = "reports/$1/$2/$3/$4";
$route['reports/graphical_:any'] = "reports/date_input";
$route['reports/(inventory_:any)/(:any)'] = "reports/$1/$2";
$route['reports/inventory_:any'] = "reports/excel_export/inventory";
/** added for register log */
$route['reports/detailed_register_log'] = 'reports/date_input_excel_export_register_log';
$route['reports/(detailed_register_log)/(:any)/(:any)/(:any)'] = 'reports/$1/$2/$3/$4';

$route['reports/detailed_teesheets'] = 'reports/date_input_teesheet';
$route['reports/(detailed_teesheets)/(:any)/(:any)/(:any)'] = 'reports/$1/$2/$3/$4';

$route['reports/detailed_teetimes'] = 'reports/date_input_teesheet';
$route['reports/(detailed_teetimes)/(:any)/(:any)/(:any)'] = 'reports/$1/$2/$3/$4';

$route['reports/(detailed_sales)/(:any)/(:any)'] = "reports/$1/$2/$3";
$route['reports/detailed_sales'] = "reports/date_input_excel_export/sales";
$route['reports/(detailed_taxes)/(:any)/(:any)/(:any)'] = "reports/$1/$2/$3/$4";
$route['reports/detailed_taxes'] = "reports/date_input_excel_export";
$route['reports/(detailed_categories)/(:any)/(:any)'] = "reports/$1/$2/$3";
$route['reports/detailed_categories'] = "reports/date_input_excel_pdf_export";
$route['reports/(detailed_receivings)/(:any)/(:any)/(:any)'] = "reports/$1/$2/$3/$4";
$route['reports/detailed_receivings'] = "reports/date_input_excel_export/receivings";
$route['reports/(detailed_giftcards)/(:any)/(:any)'] = "reports/$1/$2/$3";
$route['reports/detailed_giftcards'] = "reports/detailed_giftcards_input";
$route['reports/(specific_:any)/(:any)/(:any)/(:any)'] = "reports/$1/$2/$3/$4";
$route['reports/specific_customer'] = "reports/specific_customer_input";
$route['reports/specific_employee'] = "reports/specific_employee_input";
$route['reports/specific_supplier'] = "reports/specific_supplier_input";
$route['reports/(deleted_sales)/(:any)/(:any)/(:any)'] = "reports/$1/$2/$3/$4";
$route['reports/(deleted_sales)'] = "reports/date_input_excel_export";

// New online booking
$route['booking/(:num)/(:num)'] = "booking/index/$1/$2";
$route['booking/(:num)'] = "booking/index/$1";

// New Backbone APIs
$route['api/sales/(:num)'] = "api/sales/index/$1";
$route['api/sales/(:num)/(:any)'] = "api/sales/index/$1/$2";
$route['api/customers/(:any)'] = "api/customers/index/$1";
$route['api/customers/(:num)/(:any)'] = "api/customers/index/$1/$2";
$route['api/rainchecks/(:num)'] = "api/rainchecks/index/$1";
$route['api/rainchecks/(:num)/(:any)'] = "api/rainchecks/index/$1/$2";
$route['api/giftcards/(:num)'] = "api/giftcards/index/$1";
$route['api/giftcards/(:num)/(:any)'] = "api/giftcards/index/$1/$2";
$route['api/punch_cards/(:num)'] = "api/punch_cards/index/$1";
$route['api/punch_cards/(:num)/(:any)'] = "api/punch_cards/index/$1/$2";
$route['api/cart/(:num)'] = "api/cart/index/$1";
$route['api/cart/(:num)/(:any)'] = "api/cart/index/$1/$2";
$route['api/register_logs/(:num)'] = "api/register_logs/index/$1";
$route['api/register_logs/(:num)/(:any)'] = "api/register_logs/index/$1/$2";
$route['api/courses/(:num)'] = "api/courses/index/$1";
$route['api/courses/(:num)/(:any)'] = "api/courses/index/$1/$2";
$route['api/fees/(:num)'] = "api/fees/index/$1";
$route['api/fees/(:num)/(:any)'] = "api/fees/index/$1/$2";
$route['api/passes/(:num)'] = "api/passes/index/$1";
$route['api/passes/(:num)/(:any)'] = "api/passes/index/$1/$2";
$route['api/item_kits/(:num)'] = "api/item_kits/index/$1";
$route['api/item_kits/(:num)/(:any)'] = "api/item_kits/index/$1/$2";
$route['api/items/(:any)'] = "api/items/index/$1";
$route['api/items/(:any)/(:any)'] = "api/items/index/$1/$2";
$route['api/teesheets/(:any)'] = "api/teesheets/index/$1";
$route['api/teesheets/(:any)/(:any)'] = "api/teesheets/index/$1/$2";
$route['api/data_import/(:any)'] = "api/data_import/index/$1";
$route['api/data_import/(:any)/(:any)'] = "api/data_import/index/$1/$2";
$route['api/receipt_content/(:any)'] = "api/receipt_content/index/$1";
$route['api/receipt_content/(:any)/(:any)'] = "api/receipt_content/index/$1/$2";

// Contact form
$route['api/contact_forms'] = "api/contact_forms/index";
$route['api/contact_forms/(:num)'] = "api/contact_forms/index/$1";
$route['api/customer_groups'] = "api/contact_forms/groups";
$route['api/contact_form_replies'] = "api/contact_forms/reply";
$route['api/contact_form_replies/(:num)'] = "api/contact_forms/reply/$1";
$route['api/contact_form_keys'] = "api/contact_forms/key";
// No Auth Public Facing Endpoint for Contact Form Submission Collection
$route['api/customer_contact_form'] = "api/contact_forms_public/index";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
$route['api/analytics/(:num)'] = "api/analytics/index/$1";


/*
Laravel type routing and filters
https://github.com/Patroklo/codeigniter-static-laravel-routes
*/

Route::filter('logged_in', function(){


});

Route::pattern('id',        '[0-9]+');
Route::resources('api/reports',[], array('before' => 'logged_in'));
Route::resources('api/report_templates',[], array('before' => 'logged_in'));
Route::resources('api/report_columns',[], array('before' => 'logged_in'));
Route::resources('api/report_tags',[], array('before' => 'logged_in'));

Route::get('api/reports/{name}',"api/reports/show/$1", array('before' => 'logged_in'));
Route::post('api/report_templates/{id}/data_request',"api/report_template/data_request/create/$1", array('before' => 'logged_in'));
Route::post('api/report_templates/{id}/chart_data_request',"api/report_template/chart_data_request/create/$1", array('before' => 'logged_in'));
Route::post('api/customer/{id}/text/{type}',"api/customer/text/create/$1/$2", array('before' => 'logged_in'));

Route::post('api/report_templates/{id}/columns',"api/report_template/columns/create/$1", array('before' => 'logged_in'));
Route::get('api/report_templates/{id}/columns',"api/report_template/columns/index/$1", array('before' => 'logged_in'));
Route::get('api/report_templates/{id}/columns/{id}',"api/report_template/columns/show/$1/$2", array('before' => 'logged_in'));
Route::put('api/report_templates/{id}/columns/{id}',"api/report_template/columns/update/$1/$2", array('before' => 'logged_in'));
Route::delete('api/report_templates/{id}/columns/{id}',"api/report_template/columns/delete/$1/$2", array('before' => 'logged_in'));

Route::post('api/personal_message/{id}/message',"api/personal_messages/message/create/$1", array('before' => 'logged_in'));
Route::get('api/teesheets/recipients',"api/teesheets/recipients", array('before' => 'logged_in'));

Route::post('api/sendgrid_hook/proxy/{id}',"api/sendgrid_hook/proxy/$1", array('before' => 'logged_in'));
$route = array_merge($route,Route::map());