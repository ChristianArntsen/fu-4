<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URLs for custom HTTP / HTTPS redirection
|----------------- ---------------------------------------------------------
|
*/
$config['ssl_redirect_enable']          = true;
if(!isset($_SERVER['SERVER_NAME'])){
    $config['ssl_redirect_enable'] = false;
} else {
    $config['base_url_nonssl_redirect']	    = 'http://'.$_SERVER['SERVER_NAME'];
    $config['base_url_ssl_redirect']	    = 'https://'.$_SERVER['SERVER_NAME'];
}


// Explicit Skips are processed first
// These are SSL / NON-SSL Agnostic. They are skipped from any redirection
$config['ssl_skip_class_list']       = array();
$config['ssl_skip_directory_list']   =  array('api/');
$config['ssl_skip_method_list'] = array();
$config['ssl_skip_on_XMLHttpRequest']   = true; // If X-Requested-With is "XMLHttpRequest" then skip. (This is mostly for AJAX  mixed-content support)

// Explicit Exclusions are processed second
// These are redirected to NON-SSL, but processed before Includes
// This is intended for subsets of the inclusion list.
$config['ssl_exclude_class_list']       = array('items', 'food_and_beverage',"marketing_campaigns","campaign_builder","reports");
$config['ssl_exclude_directory_list']   =  array('v2/');
$config['ssl_exclude_method_list'] = array();

// Inclusions are only processed if not covered by an exclusion
// These are the explicit SSL enforcements
$config['ssl_include_class_list']       = array('home','login',"booking",'app_files');
$config['ssl_include_directory_list']   =  array();
$config['ssl_include_method_list'] = array('logout', 'course_summary');

//$config['ssl_include_class_list']       = array('home');
//$config['ssl_include_directory_list']   =  array();

// DEFAULT is to redirect to NON-SSL when module is enabled.
