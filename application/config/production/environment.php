<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['force_https'] = false;
$config['load_3rd_party_tools'] = true;



// Setting this to '' to pair with the root .htaccess (or native config) file rewrites
$config['index_page'] = 'index.php';

