<style>
    #teetime_form #teetimes_row_2 {
        margin-top:20px;
    }
    #teetime_form div.assigned_carts {
        margin:5px 0px 0px 150px;
        float:left;
    }
    #teetime_form div.paid_players, #teetime_form div.paid_carts {
        float:right;
        margin-top:5px;
    }
    #teetime_form div.paid_players {
        margin-right:60px;
    }
    #teetime_form div.event_controls_holder {
        margin-top:10px;
        float:left;
    }
    #teetime_form #assigned_carts_1, #teetime_form #assigned_carts_2 {
        width: 50px;
    }
    #teetime_form #teetime_people_row_1 {
        padding-top:0px;
    }
    #teetime_form #teetime_people_row_1 .player_info:nth-child(1){
        margin-left:0px;
    }
    #teetime_form .player_number_icon {
        width:20px;
        height:20px;
        display:block;
        float:left;
        color:#ccc;
        font-size:20px;
    }
    #teetime_form .clear_data {
        float:left;
        margin-top:7px;
    }
    #teetime_form #teetime_people_table_holder {
        width: 95%;
    }
    #teetime_form #teetime_table a.pay {
        margin-left:10px;
        height: 220px;
        vertical-align: middle;
        line-height: 220px;
        width: 30px;
        font-size:28px
    }
    #teetime_form #teetime_people_row_1 span.status {
        float:right;
        padding-top:5px;
    }
    #teetime_form .tee_time_person_select {
        margin-left:10px;
    }
    #teetime_form .credit_card_icon, #teetime_form .status_image {
        background:url("../images/icon-sprites-gray.png?v=1.0") no-repeat 0px -305px;
        height:21px;
        width:30px;
        display:block;
        float:left;
        cursor:pointer;
    }
    #teetime_form .status_image {
        background-position: 0px -515px;
        margin-left:10px;
    }
    #teetime_form .player_info {
        float: right;
        margin-left:10px;
        margin-right:10px;
    }
    #teetime_form .paid_row {
        background-color:#CAEAD5;
    }
    #teetime_form tr.no-show {
        background-color:#e8c9c9;
        padding-left: 10px;
    }
    #teetime_form #teetimes_row_3 .event_controls_holder input{
        margin-right:3px;
        vertical-align: middle;
    }
    #teetime_form .event_controls_holder span {
        font-size:14px;
    }
    #teetime_form input#split_teetimes, #teetime_form input#send_confirmation_emails {
        margin-left:15px;
    }
    #teetime_form div.player_buttons {
        width:350px;
    }
    #teetime_form #carts span.carts_buttonset {
        width: 240px;
    }
    #teetime_form #carts {
        float:left;
        margin-right:50px;
    }
    #teetime_form div.marker_box {
        float:left;
        font-size:12px;
        padding:4px 0px 0px 0px;
        margin-left:80px;
    }
    #teetime_form div.employee_details {
        margin-top:0px;
    }
    #teetime_form button.no-show {
        width: 90px;
    }
    #teetime_people_table_holder select, #event_table select, #shotgun_table select {
        max-width:200px;
        text-overflow:ellipsis;
    }
    .required{
        margin-left:-20px;
    }
    #teetime_people_table tr#teetime_people_row_1 td{
        overflow: hidden;
        white-space: nowrap;

    }
    table#teetime_people_table td{
        white-space:nowrap;
    }
</style>

<div id='teetimes_row_2' >
    <div id='players' class='player_buttons'>
        <span class='players_icon'></span>
        <input type="text" id="number_players" name="players" value="<?php echo !empty($teetime_info) && (int) $teetime_info->player_count ? (int) $teetime_info->player_count : 1; ?>" style="width: 40px; float: left; display: block;">
	 		<span class='players_buttonset'>
    			<input type='radio' id='players_1' name='num_players' value='1' <?php echo (empty($teetime_info) || $teetime_info->player_count == '1' || $teetime_info->player_count == '0')?'checked':''; ?>/>
    			<label for='players_1' id='players_1_label'>1</label>
    			<input type='radio' id='players_2' name='num_players' value='2' <?php echo (!empty($teetime_info) && $teetime_info->player_count == '2')?'checked':''; ?>/>
    			<label for='players_2' id='players_2_label'>2</label>
    			<input type='radio' id='players_3' name='num_players' value='3' <?php echo (!empty($teetime_info) && $teetime_info->player_count == '3')?'checked':''; ?>/>
    			<label for='players_3' id='players_3_label'>3</label>
    			<input type='radio' id='players_4' name='num_players' value='4' <?php echo (!empty($teetime_info) && $teetime_info->player_count == '4')?'checked':''; ?>/>
    			<label for='players_4' id='players_4_label'>4</label>
			</span>
        <?php echo form_hidden('current_player_count', !empty($teetime_info) ? $teetime_info->player_count : 0);?>
    </div>
    <div id='carts'>
        <span class='carts_icon'></span>
        <input type="text" id="number_carts" name="carts" value="<?php echo !empty($teetime_info) && (int) $teetime_info->carts; ?>" style="width: 40px; float: left; display: block;">
	 		<span class='carts_buttonset'>
				<input type='radio' id='carts_0' name='num_carts' value='0' <?php echo (!empty($teetime_info) && $teetime_info->carts == '0')?'checked':''; ?>/>
				<label for='carts_0' id='carts_0_label'>0</label>
				<input type='radio' id='carts_1' name='num_carts' value='1' <?php echo (!empty($teetime_info) && $teetime_info->carts == '1')?'checked':''; ?>/>
				<label for='carts_1' id='carts_1_label'>1</label>
				<input type='radio' id='carts_2' name='num_carts' value='2' <?php echo (!empty($teetime_info) && $teetime_info->carts == '2')?'checked':''; ?>/>
				<label for='carts_2' id='carts_2_label'>2</label>
				<input type='radio' id='carts_3' name='num_carts' value='3' <?php echo (!empty($teetime_info) && $teetime_info->carts == '3')?'checked':''; ?>/>
				<label for='carts_3' id='carts_3_label'>3</label>
				<input type='radio' id='carts_4' name='num_carts' value='4' <?php echo (!empty($teetime_info) && $teetime_info->carts == '4')?'checked':''; ?>/>
				<label for='carts_4' id='carts_4_label'>4</label>
			</span>
        <input type='text' id='assigned_carts_1' name="assigned_carts_1" placeholder="Cart #" value='<?php echo !empty($teetime_info->cart_num_1) ? $teetime_info->cart_num_1 : ''; ?>' />
        <input type='text' id='assigned_carts_2' name="assigned_carts_2" placeholder="Cart #" value='<?php echo !empty($teetime_info->cart_num_2) ? $teetime_info->cart_num_2 : ''; ?>' />
    </div>
    <span class='flag_icon'></span>
	 	<span class='holes_buttonset'>
	        <input type='radio' id='teetime_holes_9' name='teetime_holes' value='9' <?php echo (!empty($teetime_info) ? !empty($teetime_info->holes) && ($teesheet_holes == '9' && $teetime_info->holes != '18') || $teetime_info->holes == '9'?'checked':'' : ''); ?>/>
	        <label for='teetime_holes_9' id='teetime_holes_9_label'>9</label>
	        <input type='radio' id='teetime_holes_18' name='teetime_holes' value='18'  <?php echo (!empty($teetime_info) ? !empty($teetime_info->holes) && ($teesheet_holes == '18' && $teetime_info->holes != '9') || $teetime_info->holes == '18'?'checked':'' : ''); ?>/>
	        <label for='teetime_holes_18' id='teetime_holes_18_label'>18</label>
	    </span>
    <div class="clear"></div>
</div>
<!--div id='teetimes_row_4' >
    <div class="paid_players">
        <button id='paid_players' disabled><?php echo !empty($teetime_info->paid_player_count) ? $teetime_info->paid_player_count : 0; ?> paid players</button>
    </div>
    <div class="paid_carts">
        <button id='paid_carts' disabled><?php echo !empty($teetime_info->paid_carts) ? $teetime_info->paid_carts : 0; ?> paid carts</button>
    </div>
    <div class='clear'></div>
</div-->
<div class='popup_divider'></div>
<!--<?php if ($this->config->item('mercury_id') || $this->config->item('ets_key') || $this->config->item('element_account_id')) { ?>
    <div id='tee_time_credit_card_row'>
        <a href='#' id='add_teetime_credit_card' class='colbox2' style="margin-top:5px;">Add Card</a>
        <div id="cc_dropdown_placeholder" style="display:inline;"></div>
        <?php //echo $this->Customer_credit_card->dropdown($teetime_info->person_id, $teetime_info->credit_card_id); ?>
        <span id='charge_for_tee_time'>Charge No Show</span>
        <input type='hidden' id='added_credit_card_id' value='-1' />
        <div class='clear'></div>
    </div>
<?php } ?> -->
<div id='teetimes_row_3'>
    <div id='teetime_people_table_holder'>
        <table id='teetime_people_table'>
            <tbody>
            <tr id="teetime_people_row_1" data-person-position="1">
                <td>
                    <span class="player_1_icon player_number_icon">1</span>
                    <!--span class='clear_data' onclick='clear_player_data(1)'>x</span-->
                </td>
                <td>
                    <input tabindex=1 class='tee_time_title' placeholder='Last, First name' id='teetime_title' name='teetime_title' value='<?php echo (!empty($teetime_info) ? !empty($teetime_info->person_name) ? $teetime_info->person_name:$teetime_info->title : ''); ?>' style='width:160px, margin-right:5px;'/>
                    <input type='hidden' id='person_id' name='person_id' value='<?php echo !empty($teetime_info->person_id) ? $teetime_info->person_id : 0; ?>'/>
                </td>
                <td>
                    <?php if ($this->config->item('mercury_id') || $this->config->item('ets_key') || $this->config->item('element_account_id')) { ?>
                    <div class="credit_card_icon_holder" <?php echo !empty($credit_card_status) ? $credit_card_status : ''; ?>>
                        <div class="credit_card_icon" id="credit_card_icon"><?php echo !empty($teetime_info) ? $teetime_info->person_id : ''; ?></div>
                    </div>
                    <?php } ?>
                </td>
                <td>
                    <div id="status_image" class="status_image <?php echo !empty($image_status) ? $image_status : ''; ?>">
                        <div class="customer_comment" id="customer_comment" style="z-index: 999999; top:auto; left:auto;"><?php echo !empty($teetime_info) && !empty($customer_info) ? $customer_info[$teetime_info->person_id]->comments : ''; ?></div>
                    </div>
                </td>
                <td>
                    <div class='player_info player_details'>
                        <?php if (!empty($teetime_info) && !empty($customer_info) && $teetime_info->person_id != 0 && $customer_info[$teetime_info->person_id]->person_id) {
                            echo anchor("customers/view/". $customer_info[$teetime_info->person_id]->person_id ."/width~1100", "<span id='loaded_customer_name'></span>",  array('class'=>'customer_info_button none','id'=>'customer_button','title'=>lang('customers_window')));
                        }else{
                            echo anchor("teesheets/view_customer/-1/width~1100", "<span id='loaded_customer_name'></span>",  array('class'=>'customer_info_button none','id'=>'customer_button','title'=>lang('customers_window')));
                        } ?>
                    </div>
                </td>
                <td>
                    <input  tabindex=2 type=text value='<?php echo !empty($teetime_info) && !empty($customer_info[$teetime_info->person_id]->email) ? $customer_info[$teetime_info->person_id]->email : ''; ?>' id='email' name='email' placeholder='Email' class='ttemail'/>
                    <?php if($field_settings['email']['teesheet_required'] == 1):?>
                        <span class="required">*</span>
                    <?php endif; ?>
                </td>
                <td>
                    <input  tabindex=3 type=text value='<?php echo !empty($teetime_info) && !empty($customer_info[$teetime_info->person_id]->phone_number) ? $customer_info[$teetime_info->person_id]->phone_number : ''; ?>' id='phone' name='phone' placeholder='888-888-8888' class='ttphone'/>
                    <input type='checkbox' id='save_customer_1' name='save_customer_1' style='display:none'/>
                    <?php if($field_settings['phone_number']['teesheet_required'] == 1):?>
                        <span class="required">*</span>
                    <?php endif; ?>
                </td>
                <td>
                    <input tabindex=4 type='text' id='zip' name='zip' placeholder='<?=lang('common_zip')?>' size=7  value='<?php echo !empty($teetime_info) && !empty($customer_info[$teetime_info->person_id]->zip) ? $customer_info[$teetime_info->person_id]->zip : ''; ?>' />
                    <?php if($field_settings['zip']['teesheet_required'] == 1):?>
                        <span class="required">*</span>
                    <?php endif; ?>
                </td>
                <td>
                    <?php
                    echo form_dropdown('price_class_1', !empty($price_classes) ? $price_classes : array(), !empty($teetime_info) ? $teetime_info->price_class_1 : 0);
                    ?>
                </td>
                <td class="no-show">
                    <button class="terminal_button no-show" data-position="1" data-customer-id="<?php if(empty($teetime_info->person_id)){ echo 0; }else{ echo $teetime_info->person_id; } ?>">No-show</button>
                </td>
                <td>
                    <?php $hidden_style = !$this->config->item('always_list_all') ? 'display:none' : '' ?>
                    <input class='tee_time_person_select' name="tee_time_person_select" type="checkbox" value="1" checked style="<?=$hidden_style?>"/>
                </td>
            </tr>
            <?php for ($i = 2; $i <= 5; $i++) { ?>
                <tr id='teetime_people_row_<? echo $i?>'  class="additional_player_row" data-person-position="<? echo $i?>">
                    <td>
                        <span class="player_<? echo $i?>_icon player_number_icon"><? echo $i?></span>
                        <span class='clear_data' onclick='clear_player_data(<? echo $i?>)'>x</span>
                    </td>
                    <td>
                        <input tabindex=<? echo (($i-1)*4)+1?> class='tee_time_title' placeholder='Last, First name' id='teetime_title_<? echo $i?>' name='teetime_title_<? echo $i?>' value='<?php $person_name = 'person_name_'.$i; echo !empty($teetime_info) ? $teetime_info->$person_name : ''; ?>' style='width:160px, margin-right:5px;'/>
                        <input type='hidden' id='person_id_<? echo $i?>' name='person_id_<? echo $i?>' value='<?php $person_id = 'person_id_'.$i; echo !empty($teetime_info) ? $teetime_info->$person_id : ''; ?>'/>
                    </td>
                    <td>
                        <?php if ($this->config->item('mercury_id') || $this->config->item('ets_key') || $this->config->item('element_account_id')) { ?>
                        <div class="credit_card_icon_holder" <?php echo !empty($credit_card_status) ? $credit_card_status : ''; ?>>
                            <div class="credit_card_icon" id="credit_card_icon_<? echo $i?>"><?php echo !empty($teetime_info) ? $teetime_info->person_id : ''; ?></div>
                        </div>
                        <?php } ?>
                    </td>
                    <td>
                        <div id="status_image" class="status_image <?php echo !empty($image_status) ? $image_status : ''; ?>">
                            <div class="customer_comment" id="customer_comment_<? echo $i?>" style="z-index: 999999; top:auto; left:auto;"><?php echo !empty($teetime_info) && !empty($customer_info) ? $customer_info[$teetime_info->person_id]->comments : ''; ?></div>
                        </div>
                    </td>
                    <td>
                        <div class='player_info player_details'>
                            <?php $person_id = "person_id_".$i;
                            if (!empty($teetime_info) && !empty($customer_info) && $teetime_info->$person_id != 0 && $customer_info[$teetime_info->$person_id]->person_id) {
                                echo anchor("customers/view/". $customer_info[$teetime_info->$person_id]->person_id ."/width~1100", "<span id='loaded_customer_name'></span>",  array('class'=>'none customer_info_button','id'=>'customer_button_'.$i,'title'=>lang('customers_window')));
                            }else{
                                echo anchor("teesheets/view_customer/-1/width~1100", "<span id='loaded_customer_name'></span>",  array('class'=>'none customer_info_button','id'=>'customer_button'.$i,'title'=>lang('customers_window')));
                            } ?>
                        </div>
                    </td>
                    <td>
                        <input tabindex=<? echo (($i-1)*4)+2?> type=text placeholder='Email' value='<?php echo !empty($teetime_info) && !empty($customer_info[$teetime_info->$person_id]) ? $customer_info[$teetime_info->$person_id]->email : ''; ?>' id='email_<? echo $i?>' name='email_<? echo $i?>' class='ttemail'/>
                        <?php if($field_settings['email']['teesheet_required'] == 1):?>
                            <span class="required">*</span>
                        <?php endif; ?>
                    </td>
                    <td>
                        <input  tabindex=<? echo (($i-1)*4)+3?> type=text placeholder='888-888-8888' value='<?php echo !empty($teetime_info) && !empty($customer_info[$teetime_info->$person_id]) ? $customer_info[$teetime_info->$person_id]->phone_number : ''; ?>' id='phone_<? echo $i?>' name='phone_<? echo $i?>' class='ttphone'/>
                        <input type='checkbox' id='save_customer_<? echo $i?>' name='save_customer_<? echo $i?>' style='display:none'/>
                        <?php if($field_settings['phone_number']['teesheet_required'] == 1):?>
                            <span class="required">*</span>
                        <?php endif; ?>
                    </td>
                    <td>
                        <input   tabindex=<? echo (($i-1)*4)+4?> type='text' id='zip_<? echo $i?>' name='zip_<? echo $i?>' placeholder='<?=lang('common_zip')?>' size=7  value='<?php echo !empty($teetime_info) && !empty($customer_info[$teetime_info->$person_id]) ? $customer_info[$teetime_info->$person_id]->zip : ''; ?>' />
                        <?php if($field_settings['zip']['teesheet_required'] == 1):?>
                            <span class="required">*</span>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php
                        $price_class = "price_class_{$i}";
                        $price_classes = (empty($price_classes) ? array() : $price_classes);
                        echo form_dropdown("price_class_{$i}", $price_classes, empty($teetime_info) ? 0 : $teetime_info->$price_class);
                        ?>
                    </td>
                    <td class="no-show">
                        <button class="terminal_button no-show" data-position="<?php echo $i; ?>" data-customer-id="<?php if(empty($teetime_info->$person_id)){ echo 0; }else{ echo $teetime_info->$person_id; } ?>">No-show</button>
                    </td>                    
                    <td>
                        <input class='tee_time_person_select' name="tee_time_person_select_<?=$i?>" type="checkbox" value="1" checked/>
                    </td>
                    <!--td>
			        		<label for='save_customer_<? echo $i?>'>
			        			<span class='checkbox_image' id='save_customer_<? echo $i?>_image'></span>
				        		<input type='checkbox' id='save_customer_<? echo $i?>' name='save_customer_<? echo $i?>' style='display:none'/>
			        		</label>
			        	</td-->
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class='event_controls_holder'>
        <!--a class='expand_players down' href='#' title='Expand'>Expand</a-->
        <input type='checkbox' value='1' name='expand_players' id='expand_players' <?php if($this->config->item('always_list_all')) {?>checked<?php } ?>/><span id='expand_players_text'>List all</span>
        <input type='checkbox' value='1' name='split_teetimes' id='split_teetimes' <?php if($this->config->item('auto_split_teetimes')) {?>checked<?php } ?>/><span id='split_teetime_text'>Split</span>
        <input type='checkbox' value='1' name='send_confirmation_emails' id='send_confirmation_emails' <?php if((!empty($teetime_info->send_confirmation) && $teetime_info->send_confirmation ? ($teetime_info->send_confirmation == 2 ? false : true): $this->config->item('send_reservation_confirmations'))) {?>checked<?php } ?>/><span id='send_confirmation_emails_text'>Send Confirmation Emails</span>
    </div>
    <div class='marker_box'>
        <?php
            $hidden_teed_off = (!empty($teetime_info->teed_off_time) && $teetime_info->teed_off_time != '0000-00-00 00:00:00') ? '' : 'style="display:none;"';
            $teed_off_time = (!empty($teetime_info->teed_off_time)) ? date('h:i a', strtotime($teetime_info->teed_off_time)) : '';
            $hidden_turn_time =  (!empty($teetime_info->turn_time) && $teetime_info->turn_time != '0000-00-00 00:00:00') ? '' : 'style="display:none;"';
            $turn_time = (!empty($teetime_info->turn_time)) ? date('h:i a', strtotime($teetime_info->turn)) : '';
        ?>
        <span id='remove_teed_off_time' class='delete_item' <?=$hidden_teed_off?>>x</span><span id='teed_off_label' <?=$hidden_teed_off?>>Teed Off</span><input <?=$hidden_teed_off?> type='text' value='<?=$teed_off_time ?>' id='teed_off_time' name='teed_off_time'/>
        <span <?=$hidden_turn_time?> id='remove_turn_time' class='delete_item'>x</span><span <?=$hidden_turn_time?> id='turn_label'>Turn</span><input <?=$hidden_turn_time?> type='text' value='<?=$turn_time ?>' id='turn_time' name='turn_time'/>
        <div class='clear'></div>
    </div>
    <div class="employee_details">
        <?php
        if (trim(!empty($teetime_info->employee_name) && $teetime_info->employee_name) != '') {
            ?>
            Booked by <?php echo $teetime_info->employee_name?> - <?php echo date('n/j', strtotime($teetime_info->date_booked.' GMT')); ?> @ <?php echo date('g:ia', strtotime($teetime_info->date_booked.' GMT')); ?>
        <?php } ?>
    </div>
    <div class="clear"></div>
</div>
<div class='popup_divider'></div>
<div>
    <textarea tabindex=20 class='' placeholder='Details' id='details' name='teetime_details' style='width:94%;height:50px;'><?php echo !empty($teetime_info->details) ? $teetime_info->details : ''; ?></textarea>
</div>
<div class='event_buttons_holder'>
    <button id='teetime_delete' class='deletebutton' style='float:left;'>Delete</button>
    	<span class='saveButtons' id="checkin_buttons">
        <?php if ($this->config->item('sales')) {?>
            <span class='pay_icon'></span>
            <?php
            for ($i = 1; $i <= 4; $i++) {
                echo "<button class='purchase_button' id='purchase_$i'>$i</button>";
            } ?>
        <?php } else { ?>
            <span style="float: left; margin-right: 15px; padding-top: 5px; color: #444">Check In</span>
            <?php for ($i = 1; $i <= 4; $i++) {
                echo "<button class='purchase_button' id='checkin_$i'>$i</button>";
            } ?>
        <?php } ?>
            <div style="float: right; overflow: hidden; margin-left: 15px;">
                <button class='purchase_button' id='purchase_multi' style="float: right; width: 90px; padding: 3px 0px;">Check In</button>
                <?php $multi_number = (!empty($teetime_info->player_count) && $teetime_info->player_count - $teetime_info->paid_player_count > 5 ? $teetime_info->player_count - $teetime_info->paid_player_count : 5); ?>
                <input type="text" id="purchase_multi_number" placeholder="#" name="purchase_multi_number" value="<?=$multi_number?>" maxlength="3" style="float: left; width: 35px;">
            </div>
        </span>
    <button id='teetime_save' style='float:left;'><? echo !empty($save_text) ? $save_text : ''; ?></button>
    <button id='teetime_confirm' class='confirmButton' style='display:none'>Confirm</button>
    <button id='teetime_cancel' style='display:none' class='cancelbutton'>Cancel</button>
    <div class='clear'></div>
</div>
<input type='hidden' id='purchase_quantity' name='purchase_quantity' value='0'/>
<input type='hidden' id='checkin' name='checkin' value='0'/>
<input type='hidden' id='delete_teetime' name='delete_teetime' value='0'/>
<input type='hidden' name='start' value='<? echo !empty($teetime_info) ? $teetime_info->start : ''; ?>'/>
<input type='hidden' name='end' value='<? echo !empty($teetime_info) ? $teetime_info->end : ''; ?>'/>
<input type='hidden' name='teesheet_id' value='<? echo !empty($teetime_info) ? $teetime_info->teesheet_id : ''; ?>'/>
<input type='hidden' name='side' value='<? echo !empty($teetime_info) ? $teetime_info->side : ''; ?>'/>
<input type='hidden' name='paid_carts' value='<? echo !empty($teetime_info) ? $teetime_info->paid_carts : ''; ?>'/>
<input type='hidden' name='paid_player_count' value='<? echo !empty($teetime_info) ? $teetime_info->paid_player_count : ''; ?>'/>
<input type='hidden' name='aggregate_group_id' value='<? echo !empty($teetime_info) ? $teetime_info->aggregate_group_id : ''; ?>'/>
<?php echo form_close(); ?>