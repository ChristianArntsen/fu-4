<div class='background'>
<?php
if (!empty($standby_id))
{
    echo form_open('/teesheets/teetime_edit_standby/', array('id'=>'standby_form', 'name'=>'standby_form'));
}
else 
{
    echo form_open('/teesheets/teetime_save_standby/', array('id'=>'standby_form', 'name'=>'standby_form'));
}
?>
<div id='teetime_table'>
	<div id='teetime_people_row_1'>
		<div class='player_info'>
			<input tabindex=1 class='' id='teetime_title' name='teetime_title' placeholder='Last, First name' value='<?php echo $standby_name ?>' style='width:160px, margin-right:5px;'/>
			<input type='hidden' name='id' value='<?php echo $standby_id ?>'/>
			<input type='hidden' id='person_id' name='person_id' value='<?php echo $person_id?>'/>
	   		<input tabindex=1 type=text value='<?php echo $standby_email;?>' id='email' name='email' placeholder='Email' class='ttemail'/>
	   		<input  tabindex=1 type=text value='<?php echo $standby_phone; ?>' id='phone' name='phone'
					<?php if($this->config->item('currency_symbol') == '0' || $this->config->item('currency_symbol') == "$" ):?>
						placeholder='888-888-8888'
					<?php endif; ?>


					class='ttphone'/>
	 	</div>
	 	<span>
                    <?php 
                            
                            if ($standby_holes == '')
                            {
                                $standby_holes = 18;
                            }
                               ?>
		 	<span class='flag_icon'></span>
		 	<span class='sb_holes_buttonset holes_buttonset'>
		        <input type='radio' id='sb_teetime_holes_9' name='teetime_holes' value='9' <?php echo ($standby_holes == 9?'checked':'');?>/>
		        <label for='sb_teetime_holes_9' id='sb_teetime_holes_9_label'>9</label>
		        <input type='radio' id='sb_teetime_holes_18' name='teetime_holes' value='18' <?php echo ($standby_holes == 18?'checked':'');?>/>
		        <label for='sb_teetime_holes_18' id='sb_teetime_holes_18_label'>18</label>
		    </span>
	    </span>
	    <input type='checkbox' id='save_customer_1' name='save_customer_1' style='display:none'/>
		<!--label for='save_customer_1' title='Save to Customers'>
			<span class='checkbox_image' id='save_customer_1_image'></span>
    	</label>
   		<a class='expand_players down' href='#' title='Expand'>Expand</a-->
   		<div class='clear'></div>
   	</div>
   	
    <div id='teetimes_row_2' >
        <div id='players' class='player_buttons'>
        	<span class='players_icon'></span>
	 		<span class='players_buttonset'>
                            <?php 
                            log_message('error', 'standby_players_to_start ' . $standby_players);
                            if ($standby_players == '')
                            {
                                $standby_players = 1;
                            }
                               ?>
    			<input type='radio' id='sb_players_1' name='players' value='1' <?php echo ($standby_players == 1?'checked':'');?>/>
    			<label for='sb_players_1' id='sb_players_1_label'>1</label>
    			<input type='radio' id='sb_players_2' name='players' value='2'<?php echo ($standby_players == 2?'checked':'');?>/>
    			<label for='sb_players_2' id='sb_players_2_label'>2</label>
    			<input type='radio' id='sb_players_3' name='players' value='3'<?php echo ($standby_players == 3?'checked':'');?>/>
    			<label for='sb_players_3' id='sb_players_3_label'>3</label>
    			<input type='radio' id='sb_players_4' name='players' value='4'<?php echo ($standby_players == 4?'checked':'');?>/>
    			<label for='sb_players_4' id='sb_players_4_label'>4</label>
    			<input type='radio' id='sb_players_5' name='players' value='5'<?php echo ($standby_players == 5?'checked':'');?>/>
    			<label for='sb_players_5' id='sb_players_5_label'>5</label>
                       
                        </span>
		</div>
        <?php echo form_dropdown('teesheet_id', $teesheets, $teesheet_id);?>
	    <input type="text" id="standby_date" name="standby_date" value="<?= $my_standby_time != '' ? date('Y-m-d', strtotime($my_standby_time)) : ''?>" />
        <input type="hidden" id="tee_sheet_date" name="tee_sheet_date" value="" />
        <input type='hidden' id='add_standby_to_tee_sheet' name='add_standby_to_tee_sheet' value='0'/>
		<?php echo form_dropdown('standby_time', $teetimes, (int)date('Hi', strtotime($my_standby_time)));?>
	</div>
	
    <div id='teetimes_row_3'>
    	<div id='teetime_people_table_holder'>
    		<table id='teetime_people_table' style='display:none'>
    			<tbody>
    				<tr  id='teetime_people_row_label'>
    					<td colspan='4'>Additional players</td>
    				</tr>

    			</tbody>
    		</table>
   		</div>
	</div>
	
    <div>
    	 <textarea tabindex=20 class='' placeholder='Details'   id='details' name='standby_details' style='width:94%;height:50px;'><?php echo $standby_details; ?></textarea>
    </div>
   <div class='event_buttons_holder'>
       <?php if (!$no_delete)
       {
   		echo "<button id='teetime_delete' class='deletebutton' style='float:left;'>Delete</button>";
       }?>
                        <span class='saveButtons'>
        <?php if ($this->config->item('sales')) {?>
        	
	 		<!--button id='teetime_purchase' disabled>Purchase</button-->
        	
        	<!--button id='purchase_1'>1</button><button id='purchase_2'>2</button><button id='purchase_3'>3</button><button id='purchase_4'>4</button><button id='purchase_5'>5</button-->
        <?php } else { ?>
        	<button id='teetime_checkin'><? echo $checkin_text; ?></button>
        	<input type='hidden' id='checkin' name='checkin' value='0'/>
        <?php } ?>
        </span>
       <button id='teetime_save' style='float:right;'>Save</button>
       <?php if (!empty($standby_id)) { ?>
       <button id='return_to_tee_sheet' style='float:right;'>Add as Tee Time</button>
        <?php } ?>
        <div class='clear'></div>
    </div>

<?php form_close(); ?>
</div>
<script type='text/javascript'>
    
function clear_player_data(row) {
	if (row == 1)
	{
		$('#standy_form #teetime_title').val('');
		$('#standy_form #person_id').val('');
		$('#standy_form #email').val('');
		$('#standy_form #phone').val('');
	}
	else
	{
		$('#standy_form #teetime_title_'+row).val('');
		$('#standy_form #person_id_'+row).val('');
		$('#standy_form #email_'+row).val('');
		$('#standy_form #phone_'+row).val('');
	}
}
function focus_on_title(title) {
	<?php if (empty($reservation_info->type) || $reservation_info->type == 'teetime' || $reservation_info->type == '')
				$focus = 'teetime_title';
		  else if ($reservation_info->type == 'closed')
		  		$focus = 'closed_title';
		  else
				$focus = 'event_title';
		  ?>
		  title = (title == undefined)?'<?php echo $focus;?>':title;
		  //console.log('trying to focus on title: '+title);
	
    $('#standy_form #'+title).focus().select();
}
$(document).ready(function(){
    setTimeout(function(){$('#teetime_title').focus()}, 200);
    $('#return_to_tee_sheet').on('click', function(e){
        $('#add_standby_to_tee_sheet').val(1);
    });
    $('#teesheet_id').on('change', function(e){
        var tee_sheet_id = $(e.target).val();
        $.ajax({
            type: "POST",
            url: "index.php/teesheets/get_teetime_dropdown/",
            data: "teesheet_id=" + tee_sheet_id + "&standby=1"+'&new_date='+ $.datepicker.formatDate('yy-mm-dd', $('#standby_date').datepicker('getDate')),
            dataType: 'json',
            success: function (response) {
                //response = eval('('+response+')');
                $('#standby_time').replaceWith($(response.teetime_dropdown));
                return;
            }
        });
    });
    $('#standby_date').datepicker(
        {
            dateFormat:'yy-mm-dd',
            onClose:function(){
                var tee_sheet_id = $('#teesheet_id').val();
                $.ajax({
                    type: "POST",
                    url: "index.php/teesheets/get_teetime_dropdown/",
                    data: "teesheet_id=" + tee_sheet_id + "&standby=1"+'&new_date='+ $.datepicker.formatDate('yy-mm-dd', $('#standby_date').datepicker('getDate')),
                    dataType: 'json',
                    success: function (response) {
                        //response = eval('('+response+')');
                        $('#standby_time').replaceWith($(response.teetime_dropdown));
                        return;
                    }
                });
            }
        }
    );
    var cal_date = $(".calendar").fullCalendar('getDate');
    var year = cal_date.getFullYear();
    var month = cal_date.getMonth()+1;
    var m_z = month < 10 ? '0' : '';
    month = m_z+''+month;
    var day = cal_date.getDate();
    var d_z = day < 10 ? '0' : '';
    day = d_z+''+day;
    $('#tee_sheet_date').val(year+'-'+month+'-'+day);
    if ($('#standby_date').val() == '') {
        $('#standby_date').val(year+'-'+month+'-'+day);
    }
	<?php if($this->config->item('currency_symbol') == '0'|| $this->config->item('currency_symbol') == "$" ):?>
		$("#standby_form #phone").mask2("(999) 999-9999");
		$("#standby_form #phone_2").mask2("(999) 999-9999");
		$("#standby_form #phone_3").mask2("(999) 999-9999");
		$("#standby_form #phone_4").mask2("(999) 999-9999");
		$("#standby_form #phone_5").mask2("(999) 999-9999");
	<?php endif; ?>

		
	var submitting = false;
	$('#standby_form').bind('keypress', function(e) {
            
        //console.log('e is: ' + e.keyCode + '+++++++++++++++++++++++++++++++');
    	var code = (e.keyCode ? e.keyCode : e.which);
		 if(code == 13) { //Enter keycode
		   //Do something
		   e.preventDefault();
		   $('#standby_form #teetime_save').click();
		 }
                 
    });
    $('#standby_form').validate({
		submitHandler:function(form)
		{
			
			var proceed_with_save = true;
                       
			if ($('#standby_form #teetime_delete').val() == 1)
			{        
                            
                                                            
				var proceed_with_delete = confirm('Are you sure you want to delete this standby entry?');
                                proceed_with_save = false;
                                if (proceed_with_delete)
                                {
                                    console.log('------delete standby------');
                                    
                                    var input = $("<input>").attr("type", "hidden").attr("name", "delete").val("true");
                                    $(form).append(input);
                                    $(form).mask('<?php echo lang('common_wait'); ?>');
                                    //console.log( 'about to submit form');
                                    $(form).ajaxSubmit({
					success:function(response)
					{

                                            var standby_html = '';
						console.dir(response);
						//Calendar_actions.update_teesheet(response.teesheets);		                
                                                $.each(response, function(data){
                                                    standby_html += "<div class='standby_entry' id='";
                                    standby_html += response[data].standby_id + 
                                            "'><div class='teetime_result' id='" + 
                                            response[data].standby_id + "'><div>" +
                                        response[data].name + ' </br>' +
                                        response[data].tee_sheet_title + ' </br>' +
                                        response[data].time + ' - ' +
                                            response[data].holes + ' Holes ' +
                                            
                                            response[data].players + ' Players' + "</div> </div>";
                                    
                                         standby_html += '</div>';
                                                });
		            	        //currently_editing = '';
                                                
                                                $('#teetime_populate_standby_list').html(standby_html);
                                                $('.standby_entry').draggable({zIndex:1000,
                                    helper:'clone', appendTo:'body',
                                                    cursorAt: {left:-2, top:-2}
                              });
                                                $.colorbox.close();
                                        },
					dataType:'json'
                                    });
                                }
			}
			
			if (proceed_with_save)
			{
                            
                            //console.log("I am proceeding with save");
				if (submitting) return;
				submitting = true;
                                var input = $("<input>").attr("type", "hidden").attr("name", "delete").val("false");
                                    $(form).append(input);
				$(form).mask('<?php echo lang('common_wait'); ?>');
				//console.log( 'about to submit form');
				$(form).ajaxSubmit({
					success:function(response)
					{
                        if (response.success && typeof response.teetimes != 'undefined') {
                            var standby_html = '';
                            $.each(response.standby_change, function (data) {

                                standby_html += "<div class='standby_entry' id='";

                                standby_html += response.standby_change[data]['standby_id'] +
                                    "'><div class='teetime_result' id='" +
                                    response.standby_change[data]['standby_id'] + "'><div>" +
                                    response.standby_change[data]['name'] + ' </br>' +
                                    response.standby_change[data]['tee_sheet_title'] + ' </br>' +
                                    response.standby_change[data]['time'] + ' - ' +
                                    response.standby_change[data]['holes'] + ' Holes ' +
                                    response.standby_change[data]['players'] + ' Players' + "</div> </div>";

                                standby_html += '</div>';

                            });

                            $('#teetime_populate_standby_list').html(standby_html);
                            $('.standby_entry').draggable({
                                zIndex: 1000,
                                helper: 'clone', appendTo: 'body',
                                cursorAt: {left:-2, top:-2}
                            });
                            Calendar_actions.update_teesheet(response.teetimes, true, 'auto_update');

                            $.colorbox.close();
                            return;
                        }
						console.dir(response);
						//Calendar_actions.update_teesheet(response.teesheets);
		                
                                                var standby_html = '';
						console.dir(response);
						//Calendar_actions.update_teesheet(response.teesheets);		                
                                                $.each(response, function(data){
                                                    standby_html += "<div class='standby_entry' id='";
                                    standby_html += response[data].standby_id + 
                                            "'><div class='teetime_result' id='" + 
                                            response[data].standby_id + "'><div>" +
                                            response[data].name + ' </br>' +
                                            response[data].tee_sheet_title + ' </br>' +
                                            response[data].time + ' - ' +
                                            response[data].holes + ' Holes ' +
                                            
                                            response[data].players + ' Players' + "</div> </div>";
                                    
                                         standby_html += '</div>';
                                                });
                                                $('#teetime_populate_standby_list').html(standby_html);
                                                
                                                $('.standby_entry').draggable({zIndex:1000,
                                                    helper:'clone', appendTo:'body',
                                                    cursorAt: {left:-2, top:-2}
                                                });
		        		$.colorbox.close();
		            },
					dataType:'json'
				});
			}
			
		},
		errorLabelContainer: '#error_message_box',
 		wrapper: 'li'
	});

    $('#standby_form #teetime_name').focus().select();
	$('#standby_form #expand_players').click(function(e){
    	var checked = $(e.target).attr('checked');
    	if (checked)
    	{
	    	$('#standby_form #teetime_people_table').show();
	    }
    	else
    	{
	    	$('#standby_form #teetime_people_table').hide();
	    }
    	//var x = $('#teetime_form').height();
	    //var y = $('#teetime_form').width();
	
	    $.colorbox.resize();
    }); 
    for (var q = 1; q <= 5; q++) {
		// Save customer event listeners
	    $('#standby_form #save_customer_'+q).click(function() {
	    	if ($(this).attr('checked'))
	        	$(this).prev().addClass('checked');
	        else
	   	    	$(this).prev().removeClass('checked');
	    });      
	}
	$('#standby_form #purchase_1').click(function () {$('#standby_form #purchase_quantity').val(1);});
	$('#standby_form #purchase_2').click(function () {$('#standby_form #purchase_quantity').val(2);});
	$('#standby_form #purchase_3').click(function () {$('#standby_form #purchase_quantity').val(3);});
	$('#standby_form #purchase_4').click(function () {$('#standby_form #purchase_quantity').val(4);});
	$('#standby_form #purchase_5').click(function () {$('#standby_form #purchase_quantity').val(5);});
	$('#standby_form #teetime_checkin').click(function() {$('#standby_form #checkin').val(1);});
	$('#standby_form #teetime_delete').click(function(){$('#standby_form #teetime_delete').val(1);});
	$('#standby_form #event_delete').click(function(){$('#standby_form #delete_teetime').val(1);});
	$('#standby_form #closed_delete').click(function(){$('#standby_form #delete_teetime').val(1);});
	// Button icons and styles
	$('#standby_form #paid_carts').button();
	$('#standby_form #paid_players').button();
	$('#standby_form #teetime_save').button();
    $('#standby_form #event_save').button();
    $('#standby_form #closed_save').button();
    $('#standby_form #teetime_delete').button();
    $('#standby_form #event_delete').button();
    $('#standby_form #closed_delete').button();
    $('#standby_form .purchase_button').parent().buttonset();
    $('#standby_form #teetime_checkin').button();
    $('#standby_form #sb_teetime_holes_9').button();
    $('#standby_form #sb_event_holes_9').button();
    $('#standby_form #teetime_confirm').button();
    $('#standby_form #teetime_cancel').button();
    $('#standby_form .teebuttons').buttonset();
    $('#standby_form .sb_holes_buttonset').buttonset();
    $('#standby_form #players_0').button();
    $('#standby_form #carts_0').button();
    $('#standby_form .players_buttonset').buttonset();
    $('#standby_form .carts_buttonset').buttonset();
    $('#standby_form #players_0_label').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#standby_form #teetime_purchase').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#standby_form #players_0_label').children().css('color','#e9f4fe');

    // Teetime name settings
	$( '#standby_form #teetime_title' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search'); ?>',
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$('#standby_form #teetime_title').val(ui.item.label);
			$('#standby_form #email').val(ui.item.email).removeClass('teetime_email');
			$('#standby_form #phone').val(ui.item.phone_number).removeClass('teetime_phone');
                        
			$('#standby_form #person_id').val(ui.item.value);
                        console.log('data: ----' + $('#standby_form #person_id').val());
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title').val(ui.item.label);
		}
	});
    $('#teetime_title').swipeable({allow_enter:false, auto_complete:'#teetime_title'});
	
	// Phone number events
	/*$('#phone').mask('999-999-9999');
    $('#phone_2').mask('999-999-9999');
    $('#phone_3').mask('999-999-9999');
    $('#phone_4').mask('999-999-9999');
    $('#phone_5').mask('999-999-9999');*/
    $('#standby_form #phone').keypress(function (e) {
    	$('#standby_form #save_customer_1').attr('checked', true);
    	$('#standby_form #save_customer_1').prev().addClass('checked');
    });
    $( '#standby_form #phone' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#standby_form #teetime_title').val(ui.item.name).removeClass('teetime_name');
			$('#standby_form #email').val(ui.item.email).removeClass('teetime_email');
			$('#standby_form #phone').val(ui.item.label);
			$('#standby_form #person_id').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone').val(ui.item.label);
		}
		
	});
    
	// Email
	$('#standby_form #email').keypress(function (e) {
    	$('#standby_form #save_customer_1').attr('checked', true);
    	$('#standby_form #save_customer_1').prev().addClass('checked');
    });
    $( '#standby_form #email' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#standby_form #teetime_title').val(ui.item.name).removeClass('teetime_name');
			$('#standby_form #phone').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#standby_form #email').val(ui.item.label);
			$('#standby_form #person_id').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email').val(ui.item.label);
		}
		
	});
  

    //Calendar_actions.event.make_styled_buttons();
    
    
	// CODE BELOW REQUIRES REVIEW
	
    $('#standby_form #type_teetime').click(function(){
    	$('#standby_form #teetime_table').show();
    	$('#standby_form #closed_table').hide();
    	$('#standby_form #event_table').hide();
    	$.colorbox.resize();
    	focus_on_title('teetime_title');
    });
    $('#standby_form #type_tournament').click(function(){
    	$('#standby_form #teetime_table').hide();
    	$('#standby_form #closed_table').hide();
    	$('#standby_form #event_table').show();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#standby_form #type_league').click(function(){
    	$('#standby_form #teetime_table').hide();
    	$('#standby_form #closed_table').hide();
    	$('#standby_form #event_table').show();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#standby_form #type_event').click(function(){
    	$('#standby_form #teetime_table').hide();
    	$('#standby_form #closed_table').hide();
    	$('#standby_form #event_table').show();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#standby_form #type_closed').click(function(){
    	$('#standby_form #teetime_table').hide();
    	$('#standby_form #closed_table').show();
    	$('#standby_form #event_table').hide();
    	$.colorbox.resize();
    	focus_on_title('closed_title');
    });

	$('#standby_form .teetime_name').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_name');
	});
    $('#standby_form .teetime_phone').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_phone');
	});
	$('#standby_form .teetime_email').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_email');
	});
    $('#standby_form .teetime_details').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_details');
	});
    
      
	
});
</script>