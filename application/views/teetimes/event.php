<div class='event_info'>
    <input class='' id='event_title' name='event_title' placeholder='Title' value='<?php echo !empty($teetime_info) ? $teetime_info->title : ''; ?>' style='width:160px, margin-right:5px;'/>
    <span class='flag_icon'></span>
	 	<span class='holes_buttonset'>
            <input type='radio' id='event_holes_9' name='event_holes' value='9' <?php echo (!empty($teetime_info) && (($teesheet_holes == '9' && $teetime_info->holes != '18') || $teetime_info->holes == '9'))?'checked':''; ?>/>
            <label for='event_holes_9' id='event_holes_9_label'>9</label>
            <input type='radio' id='event_holes_18' name='event_holes' value='18'  <?php echo (!empty($teetime_info) && (($teesheet_holes == '18' && $teetime_info->holes != '9') || $teetime_info->holes == '18'))?'checked':''; ?>/>
            <label for='event_holes_18' id='event_holes_18_label'>18</label>
        </span>
    <div class='player_buttons'>
        <span class='players_icon'></span>
        <div class='players'>
            	<span class='players_buttonset'>
        			<input class='teetime_players' id='event_players' name='event_players' value='<?php echo !empty($teetime_info) ? $teetime_info->player_count : ''; ?>'/>
        		</span>
        </div>
        <span class='carts_icon'></span>
        <div class='carts'>
            	<span class='carts_buttonset'>
    				<input class='teetime_carts' id='event_carts' name='event_carts' value='<?php echo !empty($teetime_info) ? $teetime_info->carts : ''; ?>'/>
    			</span>
        </div>
    </div>
    <div class='clear'></div>
    <div class='popup_divider'></div>
    <div class="row" style="overflow: hidden; padding-right: 15px;">
        <input name="event_person" id="event_person" value="" placeholder="Add players..." style='width: 220px; margin-right:5px; float: left;' />
        <div id="event_person_import"></div>
        <div style="float:right;">
            <?php echo form_dropdown('default_price_category', !empty($price_classes) ? $price_classes : array(), !empty($teetime_info) ? $teetime_info->default_price_category : 0, 'style="float: right;"'); ?>
            <input style='margin-left:15px;' name="use_event_rate" id="use_event_rate" value="1" type="checkbox" <?php if(!empty($teetime_info) && $teetime_info->use_event_rate > 0){ echo 'checked="checked"'; } ?> /> Use Event Rate
            <input name="default_cart_fee" type="hidden" value="0" />
            <input style='margin-left:15px;' name="default_cart_fee" id="default_cart_fee" value="1" type="checkbox" <?php if(!empty($teetime_info) && $teetime_info->default_cart_fee > 0){ echo 'checked="checked"'; } ?> /> Cart Fee
        </div>
    </div>
    <h2 style="font-weight: bold; color: #666; margin-top: 5px; display: block;">
        <span id="player_count" class="count"><?php echo !empty($event_people) ? count($event_people) : 0; ?></span> Players
    </h2>
    <ul id="event_people">
        <?php if(!empty($event_people)){ ?>
            <?php foreach($event_people as $key => $person){ ?>
                <li class="<?php if($person['paid'] == 1){ echo 'paid'; } ?>" id="event_person_<?=$person['event_person_id']?>">
                    <input type="hidden" name="event_people_<?php echo $key ?>" value="<?php echo $person['person_id']; ?>" />
                    <span class="clear_data delete" data-person-id="<?php echo $person['person_id']; ?>">x</span>
                    <span class="name"><?php echo $person['last_name']; ?>, <?php echo $person['first_name']; ?></span>
                    <span class="phone"><?php echo $person['phone_number']; ?></span>
                    <span class="email"><?php echo $person['email']; ?></span>
				<span class="status">
                    <?php if ($this->config->item('sales')) { ?>
					<a href="#" class="pay<?php if($person['paid'] == 1){ echo ' done'; } ?>">
                        $ <?php if($person['paid'] == 1){ echo 'Paid'; }else{ echo 'Pay'; } ?>
                    </a>
                    <?php } ?>
					<a href="#" class="checkin<?php if($person['checked_in'] == 1){ echo ' done'; } ?>">
                        ✔ <?php if($person['checked_in'] == 1){ echo 'Checked In'; }else{ echo 'Check In'; } ?>
                    </a>
                    <span style="clear:both;"></span>
				</span>
                </li>
            <?php } } ?>
    </ul>
</div>
<div class='popup_divider'></div>
<div>
    <textarea tabindex=4 class='details_box' placeholder='Details' id='event_details' name='event_details' style='width:94%;height:50px;'><?php echo !empty($teetime_info) ? $teetime_info->details : ''; ?></textarea>
</div>
<div class='event_buttons_holder'>
 		<span class='saveButtons'>
			<button id='event_save'>Save</button>
    	</span>
    <button id='event_delete' class='deletebutton'>Delete</button>
    <?php if ($this->config->item('sales')) { ?>
    	<span class='saveButtons'>
			<button id='event_checkin'>Checkin</button>
		</span>
    <input type="text" id="checkin_txtbox" placeholder="CheckIn" name="checkin_txtbox" value="1" maxlength="3">
    <?php } ?>
    <div style="clear:both"></div>
</div>