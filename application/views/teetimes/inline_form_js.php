<script type="text/javascript">
var search_icon_time_limit = 1000;
var i = 0;


function update_price_class_dropdown(element, price_class_filter, selected_price_class, filters){
    var price_classes = BackboneApp.data.course.get('price_classes');
    var filtered_price_classes = {};
    if (typeof filters != 'undefined') {
	    var useCache = window.parent.App.data.course.get('teesheet_caching_mode') != "none";
        // Ajax in the new price class options
        $.ajax({
            type: "POST",
	        cached: useCache,
            url: "index.php/teesheets/get_price_class_menu/",
            data: 'start='+filters.start+'&holes='+filters.holes+'&person_id='+filters.person_id+'&carts='+filters.carts+'&tee_sheet_id='+filters.tee_sheet_id+'&return_json=1',
            success: function(response){
                window.parent._.each(response.price_class_list, function (option, index) {
                    var name = option;
                    var class_id = index;
                    if (response.valid_price_classes.indexOf(parseInt(class_id)) >= 0) {
                        filtered_price_classes[class_id] = name;
                    }
                    return false;
                });

                var name = element.prop('name');
                var id = element.prop('id');

                var html = window.parent._.orderedDropdown(filtered_price_classes, {id: id, name: name}, selected_price_class);
                element.replaceWith(html);
            },
            dataType:'json'
        });

    }
    else {
        window.parent._.each($(element).find('option'), function (option) {
            var name = option.text;
            var class_id = option.value;
            if (price_class_filter.indexOf(parseInt(class_id)) >= 0) {
                filtered_price_classes[class_id] = name;
            }
            return false;
        });

        var name = element.prop('name');
        var id = element.prop('id');

        var html = window.parent._.orderedDropdown(filtered_price_classes, {id: id, name: name}, selected_price_class);
        element.replaceWith(html);
    }
}

function event_person_row(data){
    var d = new Date();
    var time = d.getTime();

    var html = '<li id="event_person_'+time+'">' +
        '<input type="hidden" name="event_person_id" value="' + time + '" />' +
        '<input type="hidden" name="event_people_'+i+'" value="' + data.person_id + '" />' +
        '<span class="clear_data delete">x</span>' +
        '<span class="name">' + data.name + '</span>' +
        '<span class="phone">' + data.phone + '</span>' +
        '<span class="email">' + data.email + '</span>' +
        '<span class="status">' +
        <?php if ($this->config->item('sales')) {?>'<a href="#" class="pay"><?=$this->config->item('currency_symbol')?> Pay</a>' +<?php } ?>
        '<a href="#" class="checkin">✔ Check In</a>' +
        '<span style="clear:both;"></span>'+
        '</span>' +
        '</li>';
    i++;
    return {'html':html,'id':time};
}

function clear_player_data(row) {
    if (row == 1)
    {
        $('#teetime_title').val('');
        $('#person_id').val('');
        $('#email').val('');
        $('#phone').val('');
        $('#phone').val('');
        $('#credit_card_id').replaceWith('<input type="hidden" id="credit_card_id" value="0" />');
    }
    else
    {
        $('#teetime_title_'+row).val('');
        $('#person_id_'+row).val('');
        $('#email_'+row).val('');
        $('#phone_'+row).val('');
    }
	inline_form.trigger_dropdown_refresh($('#price_class_'+row));
}

function added_credit_card(cc_id)
{
    $('#added_credit_card_id').val(cc_id);
    update_credit_card_dropdown(0,cc_id);
}

function update_credit_card_dropdown(customer_id, cc_id)
{
    customer_id = !customer_id || customer_id == undefined ? $('#person_id').val() : customer_id;
    cc_id = !cc_id || cc_id == undefined ? $('#added_credit_card_id').val() : cc_id;
    $.ajax({
        type: "POST",
        url: "index.php/teesheets/credit_card_dropdown/"+customer_id+'/'+cc_id,
        data: '',
        success: function(response){
            $('#cc_dropdown_placeholder').html(response);
            $.colorbox2.close();
            $.colorbox.resize();
        },
        dataType:'html'
    });
}
function focus_on_title(title) {
<?php if (empty($teetime_info->type) || ($teetime_info->type == 'teetime' || $teetime_info->type == '')){
        $focus = 'teetime_title';
    }else if (isset($teetime_info->type) && $teetime_info->type == 'closed'){
        $focus = 'closed_title';
    }else{
        $focus = 'event_title';
    } ?>
    title = (title == undefined)?'<?php echo $focus;?>':title;
    $('#'+title).focus().select();
}

function set_session(flag, comments){

}
function initialize_additional_players() {
    $('#teetime_title_2, #teetime_title_3, #teetime_title_4, #teetime_title_5').unbind().removeData();

    <?php if($this->config->item('currency_symbol') == '0' || $this->config->item('currency_symbol') == "$" ):?>
        $("#phone_2").mask2("(999) 999-9999");
        $("#phone_3").mask2("(999) 999-9999");
        $("#phone_4").mask2("(999) 999-9999");
        $("#phone_5").mask2("(999) 999-9999");
    <?php endif; ?>
    for (var i = 2; i < 6; i++) {
        $( '#teetime_title_'+i ).autocomplete({
                source: '<?php echo site_url('teesheets/customer_search/'); ?>',
            delay: 10,
            autoFocus: false,
            minLength: 1,
            select: function(event, ui) {
                event.preventDefault();
                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                var index = $(this).attr('id').replace('teetime_title_', '');
                $('#teetime_title_'+index).val(ui.item.label);
                $('#email_'+index).val(ui.item.email).removeClass('teetime_email');
                $('#phone_'+index).val(ui.item.phone_number).removeClass('teetime_phone');
                $('#person_id_'+index).val(ui.item.value);
                $('#customer_comment_'+index).html(ui.item.comments);

	            var exists = false;
	            $('#price_class_1 option').each(function(){
		            if (this.value == ui.item.price_class) {
			            exists = true;
			            return false;
		            }
	            });
	            if(ui.item.price_class && exists){
		            $('#price_class_'+index).val(ui.item.price_class);
	            } else if(inline_form.defaultPriceClass){
		            $('#price_class_'+index).val(inline_form.defaultPriceClass);
	            }
                if(BackboneApp.data.course.get_setting('limit_fee_dropdown_by_customer') == 1 && ui.item.valid_price_classes){
                    update_price_class_dropdown($('#price_class_'+index), ui.item.valid_price_classes, parseInt(ui.item.price_class));
                }
            },
            search: function(event, ui) {
                if ($('.autocomplete_loader').length < 1) {
                    $(this).after($('<span class="autocomplete_loader" style="margin-left:-26px; margin-bottom:-3px; background-image:url(\'/images/loading.gif\'); width:16px; display:inline-block; height:16px;"></span>'));
                    setTimeout(function(){
                        $('.autocomplete_loader').remove();
                    }, search_icon_time_limit);
                }
            },
            open: function(event, ui) {
                $('.autocomplete_loader').remove();
            },
            focus: function(event, ui) {
                event.preventDefault();
            }
        });
    }
    $('#teetime_title_2').swipeable({allow_enter:false, auto_complete:'#teetime_title_2'});
    $('#teetime_title_3').swipeable({allow_enter:false, auto_complete:'#teetime_title_3'});
    $('#teetime_title_4').swipeable({allow_enter:false, auto_complete:'#teetime_title_4'});
    $('#teetime_title_5').swipeable({allow_enter:false, auto_complete:'#teetime_title_5'});
    $('#teetime_title_2, #teetime_title_3, #teetime_title_4, #teetime_title_5').keyup(function(){
        var focus = $(this);
        if (focus.val() == '')
        {
            var index = focus.attr('id').replace('teetime_title_', '');
            index = (index == 'teetime_title' ? 1 :index);

            clear_player_data(index);
        }
    });
    $('.tee_time_title').on('keyup', function(){
        inline_form.set_available_rows();
    });
    for (var i = 2; i < 6; i++) {
        $('#save_customer_'+i).attr('checked', false);
        $('#save_customer_'+i).prev().removeClass('checked');
        $('#phone_'+i+', #zip_'+i+', #email_'+i).keypress(function (e) {
            var index = $(this).attr('id').replace('phone_', '').replace('zip_', '').replace('email_', '');
            $('#save_customer_'+index).attr('checked', true);
            $('#save_customer_'+index).prev().addClass('checked');
        });
        $( '#phone_'+i ).autocomplete({
            source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
            delay: 200,
            autoFocus: false,
            minLength: 1,
            select: function(event, ui)	{
                event.preventDefault();
                var index = $(this).attr('id').replace('phone_', '').replace('zip_', '').replace('email_', '');
                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                $('#teetime_title_'+index).val(ui.item.name).removeClass('teetime_name');
                $('#email_'+index).val(ui.item.email).removeClass('teetime_email');
                $('#phone_'+index).val(ui.item.label);
                $('#person_id_'+index).val(ui.item.value);

                if(BackboneApp.data.course.get_setting('limit_fee_dropdown_by_customer') == 1 && ui.item.valid_price_classes){
                    update_price_class_dropdown($('#price_class_'+index), ui.item.valid_price_classes, parseInt(ui.item.price_class));
                }
            },
            search: function(event, ui) {
                if ($('.autocomplete_loader').length < 1) {
                    $(this).after($('<span class="autocomplete_loader" style="margin-left:-26px; margin-bottom:-3px; background-image:url(\'/images/loading.gif\'); width:16px; display:inline-block; height:16px;"></span>'));
                    setTimeout(function(){
                        $('.autocomplete_loader').remove();
                    }, search_icon_time_limit);
                }
            },
            open: function(event, ui) {
                $('.autocomplete_loader').remove();
            },
            focus: function(event, ui) {
                event.preventDefault();
            }

        });
        $( '#email_'+i ).autocomplete({
            source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
            delay: 200,
            autoFocus: false,
            minLength: 1,
            select: function(event, ui)	{
                event.preventDefault();
                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                var index = $(this).attr('id').replace('phone_', '').replace('zip_', '').replace('email_', '');
                $('#teetime_title_'+index).val(ui.item.name).removeClass('teetime_name');
                $('#phone_'+index).val(ui.item.phone_number).removeClass('teetime_phone');
                $('#email_'+index).val(ui.item.label);
                $('#person_id_'+index).val(ui.item.value);

                if(BackboneApp.data.course.get_setting('limit_fee_dropdown_by_customer') == 1 && ui.item.valid_price_classes){
                    update_price_class_dropdown($('#price_class_'+index), ui.item.valid_price_classes, parseInt(ui.item.price_class));
                }
            },
            search: function(event, ui) {
                if ($('.autocomplete_loader').length < 1) {
                    $(this).after($('<span class="autocomplete_loader" style="margin-left:-26px; margin-bottom:-3px; background-image:url(\'/images/loading.gif\'); width:16px; display:inline-block; height:16px;"></span>'));
                    setTimeout(function(){
                        $('.autocomplete_loader').remove();
                    }, search_icon_time_limit);
                }
            },
            open: function(event, ui) {
                $('.autocomplete_loader').remove();
            },
            focus: function(event, ui) {
            event.preventDefault();
        }

        });
    }
}

function post_person_form_submit(response)
{
    $('#teetime_title', window.parent.document).val($('#last_name').val()+","+$('#first_name').val());
    $("[name='email']", window.parent.document).val($("[name='email']").val());
    $('#phone', window.parent.document).val($('#phone_number').val());
    $("[name='zip']", window.parent.document).val($("[name='zip']").val());
    $('#price_class_1', window.parent.document).val($('#price_class').val());
    var message = response.message;
    if (message.indexOf('added') != -1) {
        $("#teetime_title").autocomplete( "search", $('#teetime_title').val() );
    }
    $.colorbox2.close();
}


// Disabling initialization since it will initialize on loading each tee time (and there's no data when the page loads
$(document).ready(function(){
});

var inline_form = {
    empty_tee_time: {
        'teetime_info':{
            'TTID':'',
            'start':'',
            'end':'',
            'type':'teetime',
            'person_id':0,
            'holes':'<?= $this->session->userdata('holes')?>',
            'player_count':1,
            'carts':0,
            'teed_off_time':'',
            'turn_time':''
        },
        'event_people':{},
        'customer_info':{},
        'price_classes':<?=json_encode(($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types()))?>,
        'image_status':'',
        'checkin_text':'Walk In',
        'save_text':'Reserve'
    },
    add_event_person:function(tee_time_id, data){
        var label = '';
        if (typeof data.label != 'undefined') {
            label = data.label;
        }

        if (data.person_id == 0) {
            data.name = label;
            data.phone = '';
            data.email = '';
        }
        else {

        }
        var result = event_person_row(data);
        var html = result.html;
        $('#event_people').append(html);
        $('h2 > span.count').text( parseInt($('h2 > span.count').text()) + 1);
        $('#event_person').val('');
        $.post('<?php echo site_url('teesheets/save_event_person/')?>/'+tee_time_id, {'person_id':data.person_id,'label':label},
            function(response){
                if (response.success){
                    //console.log('inside save_event_person success function -= -= -= - =- =- =- =- =- =- ');
                    //console.dir( $('#event_person_'+result.id+' input[name=event_person_id]'));
                    $('#event_person_'+result.id).attr('id','event_person_'+response.event_person_id);
                    $('#event_person_'+response.event_person_id+' input[name=event_person_id]').val(response.event_person_id);
                    //console.dir($('#event_person_'+data.id));
                    //console.dir($('#event_person_'+response.event_person_id));
                }
            },'json');

    },
    initialize_event_people:function(teetime_info) {
        $('#event_people span.delete').off('click').on('click', function(e){
            var button = $(this);
            var row = button.parents('li');
            var event_person_id = row.find('input[name=event_person_id]').val();
            row.remove();
            $('h2 > span.count').text( $('h2 > span.count').text() - 1);

            $.post('<?php echo site_url('teesheets/delete_event_person/')?>/'+teetime_info.TTID, {'event_person_id':event_person_id},
                function(response){

                },'json');

            e.preventDefault();
            return false;
        });
        $('#event_people a.checkin').off('click').on('click', function(e){
            if($(this).hasClass('done')){
                return false;
            }

            if(BackboneApp && !can_check_in_teetime( $('#teetime_teesheet_id').val() )){
                return false;
            }

            var event_person_id = $(this).parents('li').find('input[name=event_person_id]').val();
            var that = $(this);
            $.post('<?php echo site_url('teesheets/event_checkin/')?>/'+teetime_info.TTID, {'event_person_id':event_person_id},
                function(response){
                    if (!response.success) {
                        set_feedback(response.message,'error_message',false);
                    }
                    else {
                        that.addClass('done').text('✔ Checked In');
                    }
                },'json');
            e.preventDefault();
        });

        $('#event_people a.pay').off('click').on('click', function(e){
            if ($('#event_title').val() == '') {
                alert('Please enter a name/title or person name for this event');
                return false;
            }
            if($(this).hasClass('done')){
                return false;
            }

            <?php if($this->session->userdata('sales_v2') == '1' && $this->session->userdata('tee_sheet_speed_up')){ ?>
            if(window.parent.App.data.cart.get('payments').length > 0 && !window.parent.App.data.cart.get('suspended_name')){
                if(!confirm('An unfinished sale is still open that contains payments. Continuing will refund all payments.')){
                    return false;
                }
            }
            <?php } ?>

            if(BackboneApp && !can_check_in_teetime( $('#teetime_teesheet_id').val() )){
                return false;
            }

            var data = {};
            data.event_person_id = $(this).parents('li').find('input[name=event_person_id]').val();
            data.person_id = $(this).parents('li').find('input').eq(1).val();
            data.person_name = $(this).parents('li').find('span.name').text();
            data.price_class = $('#default_price_category').val();
            data.event_players = $('#event_players').val();
            data.purchase_quantity = 1;

            if($('#event_holes_9').attr('checked')){
                data.teetime_holes = 9;
            }else{
                data.teetime_holes = 18;
            }
            data.start = teetime_info.start;

            data.carts = 0;
            data.paid_carts = 0;
            if($('#default_cart_fee').attr('checked')){
                data.carts = 1;
            }
            if($('#use_event_rate').attr('checked')){
                data.use_event_rate = 1;
            }
            $('#teetime_form').submit();

            $.post('<?php echo site_url('teesheets/event_pay/')?>/'+teetime_info.TTID, data, function(response){
                if(response.success){
                    <?php if($this->session->userdata('sales_v2') == '1'){ ?>
                    <?php if ($this->session->userdata('tee_sheet_speed_up')) { ?>
                    window.parent.App.data.cart.set('cart_id', response.cart_id);
                    window.parent.App.data.cart.fetch({success: function () {parent.window.location = './home#sales'; $.colorbox.close();}});
                    if(window.parent.App.data.cart.get('suspended_name') && window.parent.App.data.cart.get('suspended_name') != ''){
                       window.parent.App.data.suspended_sales.fetch({data: {status: window.parent.App.data.suspended_sales.status}});
                    }
                    <?php } else { ?>
                    parent.window.location = './index.php/v2/home#sales';
                    $.colorbox.close();
                    <?php } ?>
                    <?php }else{ ?>
                    parent.window.location = './index.php/sales';
                    <?php } ?>
                }
                else {
                    set_feedback(response.message,'error_message',false);
                }
            },'json');
            e.preventDefault();
        });

    },
    populate:function(info) {
        <?php if ($this->config->item('current_day_checkins_only')) { ?>
            var tt_date = info.teetime_info.start;
            var year = tt_date.substr(0, 4);
            var month = tt_date.substr(4, 2);
            var day = tt_date.substr(6, 2);
            var today = new Date();
            if (year == today.getFullYear() && parseInt(month) == today.getMonth() && parseInt(day) == today.getDate()) {
                $('#checkin_buttons, #event_checkin, #checkin_txtbox, #shotgun_checkin, #event_people .status').show();
            }
            else {
                $('#checkin_buttons, #event_checkin, #checkin_txtbox, #shotgun_checkin, #event_people .status').hide();
            }
        <?php } ?>
        $('#existing_events').hide();
        if (typeof info.existing_events != 'undefined' && info.existing_events.length > 0) {
            var existing_events = info.existing_events;
            var html = '';
            for (var i in existing_events) {
                var ee = existing_events[i];
                if (ee.type == 'pending_reservation') {
                    html += "<div class='existing_pending_reservation'>Pending reservation</div>";
                }
                else if (ee.type == 'teetime') {
                    html += "<div>"+ee.player_count+' - '+ee.title+" (Tee time)</div>";
                }
                else {
                    html += "<div>"+ee.title+' ('+ee.type+")</div>";
                }
            }
            if (html != '') {
                $('#existing_events').show();
                $('#existing_events_list').html(html);
            }
        }

        var expand_players = (info.teetime_info.person_name_2 != '' || info.teetime_info.person_name_3 != '' || info.teetime_info.person_name_4 != '' || info.teetime_info.person_name_5 != '');
        $('#expand_players').attr('checked', (<?=$this->config->item('always_list_all')?> || expand_players) ? true : false);
        $('#saving_flag').val('');
        if (<?=$this->config->item('always_list_all')?> || expand_players) {
            $('.additional_player_row').show();
            initialize_additional_players();
        } else {
            $('.additional_player_row').hide();
        }
        //$('#squeeze_teetime').attr('checked', parseInt(info.teetime_info.squeeze));
        $("#teetime_id").val(info.teetime_info.TTID);
        $('#teetime_teesheet_id').val(info.teetime_info.teesheet_id);
        $('#teetime_form').attr('action', 'index.php/teesheets/save_teetime/'+info.teetime_info.TTID);
        // Don't forget to clear out data as well so that you don't accidentally get data carrying over from last time selected

        // Tee time data loading
        // TODO: fix _fci of undefined error
        // TODO: tee time title needs to be first guys name... not complete 5 names

        info.teetime_info.type = info.teetime_info.type == '' ? 'teetime' : info.teetime_info.type;
        if (info.teetime_info.type == 'teetime' && parseInt(info.teetime_info.player_count) == 0) {
            info.teetime_info.player_count = 1;
        }

        if (info.image_status != '') {
            $('#status_image').addClass(info.image_status);
        }
        if (info.teetime_info.person_id > 0) {
            $('#person_id').val(info.teetime_info.person_id);
            $('#email').val(info.customer_info[info.teetime_info.person_id].email);
            $('#phone').val(info.customer_info[info.teetime_info.person_id].phone_number);
            $('#zip').val(info.customer_info[info.teetime_info.person_id].zip);
        }
        $('input[name=booking_source]').val(info.teetime_info.booking_source);
        $('#assigned_carts_1').val(info.teetime_info.cart_num_1);
        $('#assigned_carts_2').val(info.teetime_info.cart_num_2);
        if (((info.teetime_info.send_confirmation == undefined || parseInt(info.teetime_info.send_confirmation) == 0) && !<?=$this->config->item('send_reservation_confirmations')?>) || parseInt(info.teetime_info.send_confirmation) == 2) {
            $('#send_confirmation_emails').attr('checked', false);
        }
        else {
            $('#send_confirmation_emails').attr('checked', true);
        }
        if (info.teetime_info.teed_off_time == '0000-00-00 00:00:00' || info.teetime_info.teed_off_time == '') {
            $('#remove_teed_off_time, #teed_off_label, #teed_off_time').hide();
        }
        else {
            $('#remove_teed_off_time, #teed_off_label, #teed_off_time').show();
            $('#teed_off_time').val(info.teetime_info.formatted_teed_off_time);
        }
        if (info.teetime_info.turn_time == '0000-00-00 00:00:00' || info.teetime_info.turn_time == '') {
            $('#remove_turn_time, #turn_label, #turn_time').hide();
        }
        else {
            $('#remove_turn_time, #turn_label, #turn_time').show();
            $('#turn_time').val(info.teetime_info.formatted_turn_time);
        }
        if (info.teetime_info.booked_info == '') {
            $('.employee_details').html('');
        }
        else {
            $('.employee_details').html(info.teetime_info.booked_info);
        }
        // Add tee time players
        for (var i = 2; i <= 5; i++) {
            if (info.teetime_info['person_id_'+i] > 0 || info.teetime_info['person_name_' + i] != '') {
                $("#person_id_" + i).val(info.teetime_info['person_id_' + i]);
                $("#teetime_title_" + i).val(info.teetime_info['person_name_' + i]);
                if (info.customer_info[info.teetime_info['person_id_' + i]] != undefined) {
                    $("#email_" + i).val(info.customer_info[info.teetime_info['person_id_' + i]].email);
                    $("#phone_" + i).val(info.customer_info[info.teetime_info['person_id_' + i]].phone_number);
                    $("#zip_" + i).val(info.customer_info[info.teetime_info['person_id_' + i]].zip);
                }
                else {
                    $("#email_" + i).val('');
                    $("#phone_" + i).val('');
                    $("#zip_" + i).val('');
                }
            }
            else {
                $("#person_id_" + i).val('');
                $("#teetime_title_" + i).val('');
            }
        }
        // Add credit card menu
        // Customer comment
        $('#players_'+info.teetime_info.player_count+'_label').click();
        $('#carts_'+info.teetime_info.carts+'_label').click();

        $('#teetime_save').html(info.save_text);
        $('input[name=start]').val(info.teetime_info.start);
        $('input[name=end]').val(info.teetime_info.end);
        $('input[name=teesheet_id]').val(info.teetime_info.teesheet_id);
        $('input[name=side]').val(info.teetime_info.side);
        $('input[name=paid_carts]').val(info.teetime_info.paid_carts);
        $('input[name=paid_player_count]').val(info.teetime_info.paid_player_count);
        $('input[name=aggregate_group_id]').val(info.teetime_info.aggregate_group_id);

        // Event data loading
        // Add event people
        // TODO: Info in customer_info, but not loading for event_people
        var event_people = info.event_people;
        var people_html = '';
        for (var a in event_people) {
            if (event_people[a].person_id) {
                people_html += '<li class="' + (event_people[a].paid == 1 ? "paid" : "") + '" id="event_person_' + event_people[a].event_person_id + '">';
                people_html += '<input type="hidden" name="event_person_id" value="' + event_people[a].event_person_id + '" />';
                people_html += '<input type="hidden" name="event_people_' + a + '" value="' + event_people[a].person_id + '" />';
                people_html += '<span class="clear_data delete" data-person-id="' + event_people[a].person_id + '">x</span>';
                people_html += '<span class="name">' + event_people[a].last_name + ', ' + event_people[a].first_name + '</span>';
                people_html += '<span class="phone">' + event_people[a].phone_number + '</span>';
                people_html += '<span class="email">' + event_people[a].email + '</span>';
                people_html += '<span class="status">';
                <?php if ($this->config->item('sales')) { ?>
                people_html += '<a href="#" class="pay ' + (event_people[a].paid == 1 ? "done" : "" ) + '">';
                people_html += '<?=$this->config->item('currency_symbol') ?> ' + (event_people[a].paid == 1 ? "Paid" : "Pay");
                people_html += '</a>';
                <?php } ?>
                people_html += '<a href="#" class="checkin ' + (event_people[a].checked_in == 1 ? "done" : "") + '">';
                people_html += '✔ ' + (event_people[a].checked_in == 1 ? "Checked In" : "Check In");
                people_html += '</a>';
                people_html += '</span>';
                people_html += '</li>';
            }
            else if (event_people[a].label) {
                people_html += '<li class="' + (event_people[a].paid == 1 ? "paid" : "") + '" id="event_person_' + event_people[a].event_person_id + '">';
                people_html += '<input type="hidden" name="event_person_id" value="' + event_people[a].event_person_id + '" />';
                people_html += '<input type="hidden" name="event_people_' + a + '" value="0" />';
                people_html += '<span class="clear_data delete" data-person-id="0">x</span>';
                people_html += '<span class="name">' + event_people[a].label + '</span>';
                people_html += '<span class="phone"></span>';
                people_html += '<span class="email"></span>';
                people_html += '<span class="status">';
                <?php if ($this->config->item('sales')) { ?>
                people_html += '<a href="#" class="pay ' + (event_people[a].paid == 1 ? "done" : "" ) + '">';
                people_html += '<?=$this->config->item('currency_symbol') ?> ' + (event_people[a].paid == 1 ? "Paid" : "Pay");
                people_html += '</a>';
                <?php } ?>
                people_html += '<a href="#" class="checkin ' + (event_people[a].checked_in == 1 ? "done" : "") + '">';
                people_html += '✔ ' + (event_people[a].checked_in == 1 ? "Checked In" : "Check In");
                people_html += '</a>';
                people_html += '</span>';
                people_html += '</li>';

            }
        }
        $('#event_people').html(people_html);


        // Shotgun data loading

        for (var h = 1; h <= 18; h++) {
            for (var p = 1; p <= 8; p++) {
                if (info.event_people[h] != undefined && info.event_people[h][p] != undefined) {
                    var p_info = info.event_people[h][p];
                    var sg_slot = $("#shotgun_players_"+h+"_"+p+"_name")
                    var sg_td = $("#shotgun_players_"+h+"_"+p)
                    // Mark as checked in
                    if (p_info.checked_in == '1') {
                        sg_td.addClass('checked_in');
                    }
                    // Mark as paid
                    if (p_info.paid == '1') {
                        sg_td.addClass('paid');
                    }
                    // Add player
                    var data = {};
                    data.name = p_info.first_name;
                    data.phone = p_info.phone_number;
                    data.email = p_info.email;
                    data.last_name = info.event_people[h][p].label || p_info.last_name;
                    data.person_id = p_info.person_id;
                    shotgun.player.add(sg_slot, data, false);
                }
            }
        }
        if (parseInt(info.teetime_info.default_cart_fee) == 1) {
            $('#default_cart_fee, input[name=sg_default_cart_fee]').attr('checked', true);
        }
        if (parseInt(info.teetime_info.use_event_rate) == 1) {
            $('#use_event_rate, #sg_use_event_rate').attr('checked', true);
        }

        // Joint data loading

        $('#closed_title, #event_title, #shotgun_title').val(info.teetime_info.title);
        $('#teetime_title').val(info.teetime_info['person_id'] > 0 || info.teetime_info['person_name'] != '' ? info.teetime_info['person_name'] : info.teetime_info.title)
        $('#closed_details, #event_details, #details, #shotgun_details').val(info.teetime_info.details);
        if (info.teetime_info.pc_menu_1 != '' && info.teetime_info.pc_menu_1 != undefined) {
            $('#price_class_1').replaceWith(info.teetime_info.pc_menu_1);
        }
        if (info.teetime_info.pc_menu_2 != '' && info.teetime_info.pc_menu_2 != undefined) {
            $('#price_class_2').replaceWith(info.teetime_info.pc_menu_2);
        }
        if (info.teetime_info.pc_menu_3 != '' && info.teetime_info.pc_menu_3 != undefined) {
            $('#price_class_3').replaceWith(info.teetime_info.pc_menu_3);
        }
        if (info.teetime_info.pc_menu_4 != '' && info.teetime_info.pc_menu_4 != undefined) {
            $('#price_class_4').replaceWith(info.teetime_info.pc_menu_4);
        }
        if (info.teetime_info.pc_menu_5 != '' && info.teetime_info.pc_menu_5 != undefined) {
            $('#price_class_5').replaceWith(info.teetime_info.pc_menu_5);
        }
        if (info.teetime_info.sgdpc_menu != '' && info.teetime_info.sgdpc_menu != undefined) {
            $('#sg_default_price_category').replaceWith(info.teetime_info.sgdpc_menu);
        }
        if (info.teetime_info.dpc_menu != '' && info.teetime_info.dpc_menu != undefined) {
            $('#default_price_category').replaceWith(info.teetime_info.dpc_menu);
        }

        inline_form.initialize(info.teetime_info);
        var event_type = info.teetime_info.type;
        event_type = event_type == 'pending_reservation' ? 'teetime' : event_type;
        $('#'+event_type+'_label').click();
        if ((info.teesheet_holes == '9' && info.teetime_info.holes != '18') || info.teetime_info.holes == '9') {
            $('#teetime_holes_9_label').click();
            $('#event_holes_9_label').click();
            $('#shotgun_holes_9_label').click();
        }
        else if ((info.teesheet_holes == '18' && info.teetime_info.holes != '9') || info.teetime_info.holes == '18') {
            $('#teetime_holes_18_label').click();
            $('#event_holes_18_label').click();
            $('#shotgun_holes_18_label').click();
        }
        $('#number_players, #event_players').val(info.teetime_info.player_count ? info.teetime_info.player_count : 1);
        $('#number_carts, #event_carts').val(info.teetime_info.carts);
        if (info.teetime_info.person_id != 0 && info.customer_info[info.teetime_info.person_id].person_id) {
            $('#customer_comment').html(info.customer_info[info.teetime_info.person_id].comments);
            $('#customer_button').attr('href', "index.php/customers/view/"+info.customer_info[info.teetime_info.person_id].person_id+"/width~1100");
            // Add credit card list
            $('#cc_dropdown_placeholder').html(info.credit_card_dropdown);
        }
        $('#sg_default_price_category, #default_price_category').val(info.teetime_info.default_price_category);
        $('#player_count').html(info.event_people.length);
        $('#cboxContent').unmask();
        $('.colbox').colorbox2();

        inline_form.mark_rows(info.teetime_info);
        var purchase_multi_number = info.teetime_info.player_count == 0 ? 5 : info.teetime_info.player_count - info.teetime_info.paid_player_count;
        if(purchase_multi_number < 0)
            purchase_multi_number = 0;
        $('#purchase_multi_number').val(purchase_multi_number);

    },
    // Since the HTML is never actually removed, we might need to do some click callback removal if it is stacking each time a tee time is opened up
    clear:function() {
        // Clear out old data
        $('#person_id, #person_id_2, #person_id_3, #person_id_4, #person_id_5').val('');
        $('#email, #email_2, #email_3, #email_4, #email_5').val('');
        $('#phone, #phone_2, #phone_3, #phone_4, #phone_5').val('');
        $('#zip, #zip_2, #zip_3, #zip_4, #zip_5').val('');
        $('#assigned_carts_1').val('');
        $('#assigned_carts_2').val('');
        $('#closed_title, #event_title, #teetime_title, #shotgun_title').val('');
        $('#closed_details, #event_details, #details, #shotgun_details').val('');
        $('#cc_dropdown_placeholder').html('');
        $('#customer_comment').val('');
        $('.marker_box span, .marker_box input').hide();
        $('#split_teetimes').attr('checked',<?=$this->config->item('auto_split_teetimes')?'true':'false';?>);
        $('#purchase_quantity').val('');
        $('#checkin').val('');
        $('#paid_carts').val('');
        $('#paid_player_count').val('');
        $('#delete_teetime').val('');
        $('#default_cart_fee, input[name=sg_default_cart_fee]').attr('checked', false);
        $('#use_event_rate, #sg_use_event_rate').attr('checked', false);
        $('#added_credit_card_id').val('');
        $('#remove_teed_off_time, #teed_off_label, #teed_off_time').hide();
        $('#remove_turn_time, #turn_label, #turn_time').hide();
        $('.customer_comment').html('');
	    $('#added_credit_card_id').val('');
        shotgun.clear();
    },
	trigger_dropdown_refresh: function (element) {
		var row = element.closest('tr').data('person-position');
		var person_id = $('#person_id' + (row > 1 ? '_' + row : '')).val();
		var filters = {
			start: $('input[name=start]').val(),
			holes: $('input[name=teetime_holes]:checked').val(),
			carts: $('#number_carts').val() > 0 ? 1 : 0,
			all_price_classes: BackboneApp.data.course.get_setting('limit_fee_dropdown_by_customer') == 0,
			person_id: person_id,
			tee_sheet_id: $('input[name=teesheet_id]').val()
		};
		var valid_price_classes = [];
		update_price_class_dropdown(element, valid_price_classes, element.val(), filters);

	},
	initialize:function(teetime_info){


		var self = this;

		_.each(BackboneApp.data.course.get("price_class_objects"),function(price_class){
			if(price_class.default == 1){
				self.defaultPriceClass = price_class.class_id;
			}
		});
		$('#event_person_import').off('click').on('click', function(e){
			$.colorbox2({href:'index.php/teesheets/excel_import/event',title:'Import Players',width:'800'});
        });
        $('#assigned_carts_1, #assigned_carts_2').on('blur', function(){
            // Allow alpha numeric only
            var cart_data = $(this).val();
            $(this).val(cart_data.replace(/[^a-z0-9\s]/gi,''));
        });

        if (teetime_info == undefined) {
            teetime_info = {
                type:'',
                TTID:''
            };
        }
        $.ui.autocomplete.prototype.options.autoSelect = true;
        if (teetime_info.type == 'shotgun') {
            shotgun.initialize();
        }

        $('.players_buttonset input').change(function(){
            var players = $(this).val();
            $('#number_players').val( players > 0 ? players : 1 );
            $('.tee_time_person_select').attr('checked', false);
            $('#teetime_people_row_1 .tee_time_person_select').attr('checked', true);
            if (players > 1) {
                $('#teetime_people_row_2 .tee_time_person_select').attr('checked', true);
            }
            if (players > 2) {
                $('#teetime_people_row_3 .tee_time_person_select').attr('checked', true);
            }
            if (players > 3) {
                $('#teetime_people_row_4 .tee_time_person_select').attr('checked', true);
            }
            if (players > 4) {
                $('#teetime_people_row_5 .tee_time_person_select').attr('checked', true);
            }
        });

        $('.carts_buttonset input').change(function(){
            $('#number_carts').val( $(this).val() );
        });

        $('input[name="promo_id"]').val(teetime_info.promo_id); 

        $('#number_players').blur(function(){
            var number = $(this).val();
            if(number <= 4){
                $('#players_'+number+'_label').trigger('click');
            }else{
                $('.players_buttonset input').attr('checked', null);
                $('.players_buttonset label').removeClass('ui-state-active');
            }
            $('.tee_time_person_select').attr('checked', false);
            $('#teetime_people_row_1 .tee_time_person_select').attr('checked', true);
            if (number > 1) {
                $('#teetime_people_row_2 .tee_time_person_select').attr('checked', true);
            }
            if (number > 2) {
                $('#teetime_people_row_3 .tee_time_person_select').attr('checked', true);
            }
            if (number > 3) {
                $('#teetime_people_row_4 .tee_time_person_select').attr('checked', true);
            }
            if (number > 4) {
                $('#teetime_people_row_5 .tee_time_person_select').attr('checked', true);
            }
        });

        $('#number_carts').blur(function(){
            var number = $(this).val();
            if(number <= 4){
                $('#carts_'+number+'_label').trigger('click');
            }else{
                $('.carts_buttonset input').attr('checked', null);
                $('.carts_buttonset label').removeClass('ui-state-active');
            }
        });

        $('#add_teetime_credit_card').off('click').on('click', function(e){
            e.preventDefault();
            var person_id = $("#person_id").val();
            $.colorbox2({href:'index.php/teesheets/open_add_credit_card_window/'+teetime_info.TTID+'/'+person_id, width:600})
        });
        $('#charge_for_tee_time').off('click').on('click', function (e){
            var card_to_charge = $('#credit_card_id').val();
            if (card_to_charge == 0)
            {
                alert('Please select a card to charge');
                return;
            }
            $.colorbox2({href:'index.php/teesheets/view_charge_card/'+teetime_info.TTID, width:500, onComplete:function(){
                $('#credit_card_label').html($('#credit_card_id option:selected').text());
                $('#card_to_charge').val(card_to_charge);
                $('#charge_person_id').val($('#person_id').val());
                $('#tee_time_id').val(teetime_info.TTID);
            }});
        });
        $('#teed_off_time').timepicker({'timeFormat':'h:mm tt', 'hour':teetime_info.teed_off_time_hour, 'minute':teetime_info.teed_off_time_minute});
        $('#turn_time').timepicker({'timeFormat':'h:mm tt', 'hour':teetime_info.turn_time_hour, 'minute':teetime_info.turn_time_minute});
        $('#remove_teed_off_time').off('click').on('click', function(){
            $.ajax({
                type: "POST",
                url: "index.php/teesheets/zero_teed_off/"+teetime_info.TTID+"/"+teetime_info.teesheet_id,
                data: "",
                success: function(response){
                    Calendar_actions.update_teesheet(response.teetimes);
                    $('#remove_teed_off_time, #teed_off_label, #teed_off_time').hide();
                },
                dataType:'json'
            });
        });
        $('#remove_turn_time').off('click').on('click', function(){
            $.ajax({
                type: "POST",
                url: "index.php/teesheets/zero_turn/"+teetime_info.TTID+"/"+teetime_info.teesheet_id,
                data: "",
                success: function(response){
                    Calendar_actions.update_teesheet(response.teetimes);
                    $('#remove_turn_time, #turn_label, #turn_time').hide();
                },
                dataType:'json'
            });
        });

        <?php if($this->config->item('currency_symbol') === '0' || $this->config->item('currency_symbol') == "$" ):?>
            $("#phone").mask2("(999) 999-9999");
        <?php endif; ?>

        var submitting = false;
        $('#teetime_form').bind('keypress', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) { //Enter keycode
                //Do something
                e.preventDefault();
                $('#teetime_save').click();
            }
        });
        $('#teetime_form').validate({
            submitHandler:function(form)
            {
                $('#saving_flag').val(1);
                var type = $('input[name=event_type]:checked').val();
                var proceed_with_save = true;
                if ($('#delete_teetime').val() == 1)
                {
                    //Implemented new confirmation box
                }
                else if ((type == 'teetime' && $('#teetime_title').val() == '') ||
                    ((type == 'tournament' || type == 'league' || type == 'event') && ($('#event_title').val() == '')) ||
                    (type == 'closed' && $('#closed_title').val() == '') ||
                    (type == 'shotgun' && $('#shotgun_title').val() == '')
                )
                {
                    alert('Please enter a name/title or person name for this event');
                    proceed_with_save = false;
                }
                
                <?php if($this->session->userdata('sales_v2') == '1' && $this->session->userdata('tee_sheet_speed_up')){ ?>
                if($('#purchase_quantity').val() && $('#purchase_quantity').val() > 0 && window.parent.App.data.cart.get('payments').length > 0){
                    if(!confirm('An unfinished sale is still open that contains payments. Continuing will refund all payments.')){
                        return false;
                    }
                }

                if(BackboneApp && 
                    $('#purchase_quantity').val() && 
                    $('#purchase_quantity').val() > 0 &&
                    !can_check_in_teetime( $('#teetime_teesheet_id').val() )
                ){
                    $('#purchase_quantity').val('')
                    return false;
                }
                <?php } ?>

                var errors = 0;
                for(var i = 1;i<=5;i++){
                    if($('#teetime_title_'+i).val() != ""){
                        var field_number = i;
                        if(field_number == 1){
                            field_number = "";
                        } else {
                            field_number = "_"+i;
                        }

                        if(field_settings['phone_number']['teesheet_required'] && $('#save_customer_'+i).is(":checked") && $('#phone'+field_number).val()==""){
                            errors++;
                            $('#phone'+field_number).addClass('error');
                        }
                        if(field_settings['phone_number']['teesheet_required'] && $('#save_customer_'+i).is(":checked") && $('#phone').val()==""){
                            errors++;
                            $('#phone'+field_number).addClass('error');
                        }
                        if(field_settings['email']['teesheet_required'] && $('#save_customer_'+i).is(":checked")  && $('#email'+field_number).val()==""){
                            errors++;
                            $('#email'+field_number).addClass('error');
                        }
                        if(field_settings['zip']['teesheet_required'] && $('#save_customer_'+i).is(":checked") && $('#zip'+field_number).val()==""){
                            errors++;
                            $('#zip'+field_number).addClass('error');
                        }
                    }
                }
                if(errors > 0){
                    alert("Missing required fields for customers. Either remove the customer or fill in fields.");
                    return false;
                }


                if (proceed_with_save)
                {
                    if (submitting) {
                        return;
                    }
                    submitting = true;
                    $(form).mask('<?php echo lang('common_wait'); ?>');

                    $(form).ajaxSubmit({
                        error: function(xhr, type, exception )
                        {
                            currently_editing = '';
                            submitting = false;
                            var res = jQuery.parseJSON(xhr.responseText);
                            alert(res.details);
                            $(form).unmask();
                        },
                        success:function(response)
                        {
                            $('#delete_teetime').val(0);
                            var overlap = true;
                            if (!response.second_time_available && $("#teetime_save").text() == "Reserve")
                                overlap = confirm('This reservation will re-round on an existing reservation or event. Do you want to continue booking it?');
                            if(overlap){
                                Calendar_actions.update_teesheet(response.teetimes);
                                submitting = false;
                                post_teetime_form_submit(response);
                                if (response.email_confirmation != ''){
                                    $.each(response.email_confirmation, function( index, value ) {
                                        Calendar_actions.send_confirmation(value);
                                    });
                                }else{
                                    Calendar_actions.send_confirmation(response.send_confirmation);
                                }
                                if (response.go_to_register){
                                    <?php if($this->session->userdata('sales_v2') == '1'){ ?>
                                    <?php if ($this->session->userdata('tee_sheet_speed_up')) { ?>
                                    window.parent.App.data.cart.set('cart_id', response.cart_id);
                                    window.parent.App.data.cart.fetch({success: function () {parent.window.location = './home#sales'; $.colorbox.close();}});
                                    if(window.parent.App.data.cart.get('suspended_name') && window.parent.App.data.cart.get('suspended_name') != ''){
                                        window.parent.App.data.suspended_sales.fetch({data: {status: window.parent.App.data.suspended_sales.status}});
                                    }
                                    <?php } else { ?>
                                    parent.window.location = './index.php/v2/home#sales';
                                    $.colorbox.close();
                                    <?php } ?>
                                    <?php }else{ ?>
                                    window.location = './index.php/sales';
                                    $.colorbox.close();
                                    $(".ui-autocomplete").hide();
                                    <?php } ?>
                                }
                                else {
                                    $.colorbox.close();
                                    $(".ui-autocomplete").hide();
                                }
                            } else {
                                // Delete the tee time
                                Calendar_actions.event.remove_empty_teetime(response.tee_time_id);
                                $.colorbox.close();
                                $(".ui-autocomplete").hide();
                            }

                            currently_editing = '';
                            submitting = false;

                            buildStats();
                        },
                        dataType:'json'
                    });
                }else{
                    $('#delete_teetime').val(0) ;
                    submitting = false;
                }
            },
            errorLabelContainer: '#error_message_box',
            wrapper: 'li'
        });

        $('#teetime_name').focus().select();
        $('#expand_players').off('click').on('click', function(e){
            var checked = $(e.target).attr('checked');
            if (checked)
            {
                $('.additional_player_row').show();
                $('#teetime_people_row_1 .tee_time_person_select').show();
            }
            else
            {
                $('.additional_player_row').hide();
                $('#teetime_people_row_1 .tee_time_person_select').hide();
            }
            initialize_additional_players();
            inline_form.set_available_rows();
            $.colorbox.resize();
        }).attr('checked') ? $('#teetime_people_row_1 .tee_time_person_select').show() : $('#teetime_people_row_1 .tee_time_person_select').hide();

        $('#purchase_1').off('click').on('click', function () {$('#purchase_quantity').val(1);});
        $('#purchase_2').off('click').on('click', function () {$('#purchase_quantity').val(2);});
        $('#purchase_3').off('click').on('click', function () {$('#purchase_quantity').val(3);});
        $('#purchase_4').off('click').on('click', function () {$('#purchase_quantity').val(4);});
        $('#purchase_5').off('click').on('click', function () {$('#purchase_quantity').val(5);});

    <?php if($this->session->userdata('sales') == '1'): ?>
        $('#purchase_multi').off('click').on('click', function () {$('#purchase_quantity').val( $('#purchase_multi_number').val() );});
    <?php else: ?>
        $('#purchase_multi').off('click').on('click', function () {$('#checkin').val( $('#purchase_multi_number').val() );});
    <?php endif; ?>

        $('#checkin_1').off('click').on('click', function () {$('#checkin').val(1);});
        $('#checkin_2').off('click').on('click', function () {$('#checkin').val(2);});
        $('#checkin_3').off('click').on('click', function () {$('#checkin').val(3);});
        $('#checkin_4').off('click').on('click', function () {$('#checkin').val(4);});
        $('#checkin_5').off('click').on('click', function () {$('#checkin').val(5);});

        $('#event_checkin').off('click').on('click', function () {
            var val = $('#checkin_txtbox').val();
            $('#purchase_quantity').val(val);
        });
        $("#checkin_txtbox").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $('#shotgun_checkin').off('click').on('click', function (e) {
            // Count checked boxes and send them through to pay for
            var checked_boxes = $('.person_select:checked');
            var checkin_val = $('#checkin_txtbox').val();
            var val = checked_boxes.length > checkin_val ? checked_boxes.length : checkin_val ;

            for (var i = 0; i < checked_boxes.length; i++) {
                var slot_id = $(checked_boxes[i]).parent().parent().attr('id');
                $(checked_boxes[i]).val(slot_id.replace('shotgun_players_', ''));
            }

            $('#purchase_quantity').val(val);
        });

        $("#shotgun_checkin_txtbox").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        $('#teetime_delete').off('click').on('click', function(e){
            e.preventDefault();
            var tee_time_id = $('#teetime_id').val();
            $.colorbox2({'href':'index.php/teesheets/confirm_event_delete/'+tee_time_id,width:'700','title':'Delete Event'});

            return false;
        });
        $('#event_delete').off('click').on('click', function(){$('#delete_teetime').val(1);});
        $('#shotgun_delete').off('click').on('click', function(){$('#delete_teetime').val(1);});
        $('#closed_delete').off('click').on('click', function(){$('#delete_teetime').val(1);});

        // Button icons and styles
        $('#paid_carts').button();
        $('#paid_players').button();
        $('#teetime_save').button();
        $('#event_save').button();
        $('#closed_save').button();
        $('#shotgun_save').button();
        $('#shotgun_delete').button();
        $('#teetime_delete').button();
        $('#event_delete').button();
        $('#closed_delete').button();
        $('.purchase_button').parent().buttonset();
        $('#teetime_checkin').button();
        $('#teetime_holes_9').button();
        $('#event_holes_9').button();
        $('#shotgun_holes_9').button();
        $('#teetime_confirm').button();
        $('#teetime_cancel').button();
        $('.teebuttons').buttonset();
        $('.holes_buttonset').buttonset();
        $('#players_0').button();
        $('#carts_0').button();
        $('.players_buttonset').buttonset();
        $('.carts_buttonset').buttonset();
        $('#players_0_label').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
        $('#teetime_purchase').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
        $('#players_0_label').children().css('color','#e9f4fe');

        // Teetime name settings
        $('#teetime_title').unbind().removeData();
        var auto_complete = $( '#teetime_title' ).autocomplete({
            source: '<?php echo site_url('teesheets/customer_search/'); ?>',
            delay: 10,
            autoFocus: true,
            minLength: 0,
            search: function( event, ui ) {

                $('#status_image').removeClass('status_red').removeClass('status_yellow').removeClass('status_green').removeClass('customer_status');
                var new_cust = "index.php/teesheets/view_customer/-1/width~1100";
                $('#customer_button').attr("href", new_cust);
                if ($('.autocomplete_loader').length < 1) {
                    $(this).after($('<span class="autocomplete_loader" style="margin-left:-26px; margin-bottom:-3px; background-image:url(\'/images/loading.gif\'); width:16px; display:inline-block; height:16px;"></span>'));
                    setTimeout(function(){
                        $('.autocomplete_loader').remove();
                    }, search_icon_time_limit);
                }
            },
            open: function(event, ui) {
                $('.autocomplete_loader').remove();
            },
            select: function(event, ui) {
                event.preventDefault();


                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                set_session(ui.item.status_flag, ui.item.comments);
                $('#customer_comment').html(ui.item.comments);
                if (ui.item.status_flag == 1) {
                    $('#status_image').addClass('status_red').addClass('customer_status');
                }
                else if (ui.item.status_flag == 2) {
                    $('#status_image').addClass('status_yellow').addClass('customer_status');
                }
                else if (ui.item.status_flag == 3) {
                    $('#status_image').addClass('status_green').addClass('customer_status');
                }
                $('#teetime_title').val(ui.item.label);
                $('#email').val(ui.item.email).removeClass('teetime_email');
                $('#phone').val(ui.item.phone_number).removeClass('teetime_phone');
                $('#person_id').val(ui.item.value);
                var cust_edit = "index.php/customers/view/"+ ui.item.value +"/width~1100";
                $('#customer_button').attr("href", cust_edit);
                $('#zip').val(ui.item.zip);

	            var exists = false;
	            $('#price_class_1 option').each(function(){
		            if (this.value == ui.item.price_class) {
			            exists = true;
			            return false;
		            }
	            });
	            if(ui.item.price_class && exists){
		            $('#price_class_1').val(ui.item.price_class);
	            } else if(self.defaultPriceClass){
		            $('#price_class_1').val(self.defaultPriceClass);
	            }

                if(BackboneApp.data.course.get_setting('limit_fee_dropdown_by_customer') == 1 && ui.item.valid_price_classes){
                    update_price_class_dropdown($('#price_class_1'), ui.item.valid_price_classes, parseInt(ui.item.price_class));
                }
            },
            focus: function(event, ui) {
                event.preventDefault();
                var card_swipe = $('#teetime_title').data('card_swipe');


                if (card_swipe != undefined && card_swipe == 1)
                {
                    var autocomplete = $( this ).data( "autocomplete" );


                    if ( !autocomplete.options.autoSelect || autocomplete.selectedItem ) { return; }

                    autocomplete.widget().children( ".ui-menu-item:first" ).each(function() {
                        var item = $( this ).data( "item.autocomplete" );
                        autocomplete.selectedItem = item;
                    });
                    if ( autocomplete.selectedItem ) {
                        autocomplete._trigger( "select", event, { item: autocomplete.selectedItem } );
                    }
                    $(this).autocomplete('close');
                    $('#teetime_title').data('card_swipe', 0);
                }

                //$('#teetime_title').val(ui.item.label);
            }
        }).data( "autocomplete" );
        if (auto_complete != undefined) {
            auto_complete._renderItem = function (ul, item) {
                if (item.phone_number == '() -') {
                    return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + item.label + " <span class='search_phone_number'>" + item.email + "</span></a>")
                        .appendTo(ul);
                } else {
                    return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + item.label + " <span class='search_phone_number'>" + item.phone_number + "</span> <span class='search_email'>" + item.email + "</span></a>")
                        .appendTo(ul);
                }

            };
        }
        $('#teetime_title').swipeable({allow_enter:false, auto_complete:'#teetime_title', character_limit:20, card_swipe:0, card_scan:0});

        $('#teetime_title').keyup(function(){
            var focus = $(this);
            if (focus.val() == '')
            {
                var index = focus.attr('id').replace('teetime_title_', '');
                index = (index == 'teetime_title' ? 1 :index);

                clear_player_data(index);
            }
        });

        $('#event_person').autocomplete({
            source: '<?php echo site_url('teesheets/customer_search'); ?>',
            delay: 150,
            autoFocus: false,
            minLength: 0,
            select: function(event, ui) {
                event.preventDefault();
                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                //set_session(ui.item.status_flag, ui.item.comments);
                var data = {};
                data.name = ui.item.label;
                data.phone = ui.item.phone_number;
                data.email = ui.item.email;
                data.person_id = ui.item.value;

                // If person is already in list, do nothing
                if($('#event_person_'+data.person_id).length > 0){
                    $('#event_person').val('');
                    return false;
                }

                inline_form.add_event_person(teetime_info.TTID, data);
                $.colorbox.resize();
                inline_form.initialize_event_people(teetime_info);
            },
            search: function(event, ui) {
                if ($('.autocomplete_loader').length < 1) {
                    $(this).after($('<span class="autocomplete_loader" style="margin-left:-26px; margin-bottom:-3px; background-image:url(\'/images/loading.gif\'); width:16px; display:inline-block; height:16px;"></span>'));
                    setTimeout(function(){
                        $('.autocomplete_loader').remove();
                    }, search_icon_time_limit);
                }
            },
            open: function(event, ui) {
                $('.autocomplete_loader').remove();
            },
            focus: function(event, ui) {
                event.preventDefault();
            }
        });
        $('#event_person').off('keypress').on('keypress', function(e){
            if(e.which == 13 && $('#event_person').val() != '') {
                e.preventDefault();
                inline_form.add_event_person(teetime_info.TTID, {person_id:0, label:$('#event_person').val()});
                $.colorbox.resize();
                inline_form.initialize_event_people(teetime_info);

                return false;
            }
        });
        $('#event_person').swipeable({allow_enter:false, auto_complete:'#event_person'});

        inline_form.initialize_event_people(teetime_info);

        // Phone number events
        $('#save_customer_1').attr('checked', false);
        $('#save_customer_1').prev().removeClass('checked');
        $('#phone, #zip, #email').keypress(function (e) {
            $('#save_customer_1').attr('checked', true);
            $('#save_customer_1').prev().addClass('checked');
        });
        $( '#phone' ).autocomplete({
            source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
            delay: 200,
            autoFocus: false,
            minLength: 1,
            select: function(event, ui)	{
                event.preventDefault();
                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                $('#teetime_title').val(ui.item.name).removeClass('teetime_name');
                $('#email').val(ui.item.email).removeClass('teetime_email');
                $('#phone').val(ui.item.label);
                $('#person_id').val(ui.item.value);

                if(BackboneApp.data.course.get_setting('limit_fee_dropdown_by_customer') == 1 && ui.item.valid_price_classes){
                    update_price_class_dropdown($('#price_class_1'), ui.item.valid_price_classes, parseInt(ui.item.price_class));
                }
            },
            search: function(event, ui) {
                if ($('.autocomplete_loader').length < 1) {
                    $(this).after($('<span class="autocomplete_loader" style="margin-left:-26px; margin-bottom:-3px; background-image:url(\'/images/loading.gif\'); width:16px; display:inline-block; height:16px;"></span>'));
                    setTimeout(function(){
                        $('.autocomplete_loader').remove();
                    }, search_icon_time_limit);
                }
            },
            open: function(event, ui) {
                $('.autocomplete_loader').remove();
            },
            focus: function(event, ui) {
                event.preventDefault();
            }

        });
        // Email
        $( '#email' ).autocomplete({
            source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
            delay: 200,
            autoFocus: false,
            minLength: 1,
            select: function(event, ui)	{
                event.preventDefault();
                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                $('#teetime_title').val(ui.item.name).removeClass('teetime_name');
                $('#phone').val(ui.item.phone_number).removeClass('teetime_phone');
                $('#email').val(ui.item.label);
                $('#person_id').val(ui.item.value);

                if(BackboneApp.data.course.get_setting('limit_fee_dropdown_by_customer') == 1 && ui.item.valid_price_classes){
                    update_price_class_dropdown($('#price_class_1'), ui.item.valid_price_classes, parseInt(ui.item.price_class));
                }
            },
            search: function(event, ui) {
                if ($('.autocomplete_loader').length < 1) {
                    $(this).after($('<span class="autocomplete_loader" style="margin-left:-26px; margin-bottom:-3px; background-image:url(\'/images/loading.gif\'); width:16px; display:inline-block; height:16px;"></span>'));
                    setTimeout(function(){
                        $('.autocomplete_loader').remove();
                    }, search_icon_time_limit);
                }
            },
            open: function(event, ui) {
                $('.autocomplete_loader').remove();
            },
            focus: function(event, ui) {
                event.preventDefault();
                //$('#email').val(ui.item.label);
            }

        });


        // CODE BELOW REQUIRES REVIEW
        $('#type_teetime').off('click').on('click', function(){
            $('#teetime_table').show();
            $('#closed_table').hide();
            $('#event_table').hide();
            $('#shotgun_table').hide();
            $.colorbox.resize();
            focus_on_title('teetime_title');
        });
        $('#type_tournament').off('click').on('click', function(){
            $('#teetime_table').hide();
            $('#closed_table').hide();
            $('#event_table').show();
            $('#shotgun_table').hide();
            $.colorbox.resize();
            focus_on_title('event_title');
        });
        $('#type_league').off('click').on('click', function(){
            $('#teetime_table').hide();
            $('#closed_table').hide();
            $('#event_table').show();
            $('#shotgun_table').hide();
            $.colorbox.resize();
            focus_on_title('event_title');
        });
        $('#type_event').off('click').on('click', function(){
            $('#teetime_table').hide();
            $('#closed_table').hide();
            $('#event_table').show();
            $('#shotgun_table').hide();
            $.colorbox.resize();
            focus_on_title('event_title');
        });
        $('#type_closed').off('click').on('click', function(){
            $('#teetime_table').hide();
            $('#closed_table').show();
            $('#event_table').hide();
            $('#shotgun_table').hide();
            $.colorbox.resize();
            focus_on_title('closed_title');
        });
        $('#type_shotgun').off('click').on('click', function(){
            $('#teetime_table').hide();
            $('#closed_table').hide();
            $('#event_table').hide();
            $('#shotgun_table').show();
            $.colorbox.resize();
            shotgun.initialize();
            focus_on_title('shotgun_title');
        });

        $('.teetime_name').each(function(index){
            if ($(this).val() != '')
                $(this).removeClass('teetime_name');
        });
        $('.teetime_phone').each(function(index){
            if ($(this).val() != '')
                $(this).removeClass('teetime_phone');
        });
        $('.teetime_email').each(function(index){
            if ($(this).val() != '')
                $(this).removeClass('teetime_email');
        });
        $('.teetime_details').each(function(index){
            if ($(this).val() != '')
                $(this).removeClass('teetime_details');
        });
        $('#feedback_bar').off('click').on('click', function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});

        // Initialize credit card buttons
        $('.credit_card_icon').off('click').on('click', function(e) {
            var person_position = $(e.target).parent().parent().parent().attr('data-person-position');
            var person_id = 0;
            if (person_position != undefined && person_position != '') {
                if (person_position == 1) {
                    person_id = $('#person_id').val();
                }
                else {
                    person_id = $('#person_id_'+person_position).val();
                }
            }
            if (person_id == undefined || person_id == '' || person_id < 1) {
                alert('Please select a customer before attempting to manage credit cards.');
            }
            else {
                $.colorbox2({'href':'index.php/customers/manage_credit_cards/'+person_id+'/1',width:'700','title':'Manage Credit Cards'});
            }
        });

        // Initialize edit customer links
        $('.customer_info_button').off('click').on('click', function(e) {
            e.preventDefault();
            var person_position = $(this).parent().parent().parent().attr('data-person-position');
            var person_id = 0;
            if (person_position != undefined && person_position != '') {
                if (person_position == 1) {
                    person_id = $('#person_id').val();
                }
                else {
                    person_id = $('#person_id_'+person_position).val();
                }
            }
            if (person_id == undefined || person_id == '' || person_id < 1) {
                $.colorbox2({'href':'index.php/teesheets/view_customer/-1/'+person_position,width:'1100','title':'Create New Customer'});
            }
            else {
                $.colorbox2({'href':'index.php/customers/view/'+person_id,width:'1100','title':'Edit Customers'});
            }
        });

        // Initialize check boxes
        var player_count = $('#number_players').val();
        $('.tee_time_person_select').attr('checked', false);
        $('#teetime_people_row_1 .tee_time_person_select').attr('checked', true);
        if (player_count > 1) {
            $('#teetime_people_row_2 .tee_time_person_select').attr('checked', true);
        }
        if (player_count > 2) {
            $('#teetime_people_row_3 .tee_time_person_select').attr('checked', true);
        }
        if (player_count > 3) {
            $('#teetime_people_row_4 .tee_time_person_select').attr('checked', true);
        }
        if (player_count > 4) {
            $('#teetime_people_row_5 .tee_time_person_select').attr('checked', true);
        }
        $('.tee_time_person_select').off('click').on('click', function(e){
            inline_form.set_multi_number();
        });

        // Initialize customer list progression (auto select player count)
        $('.tee_time_title').on('keyup', function(){
           inline_form.set_available_rows();
        });

        $('button.no-show').die('click').live('click', function(e){
            
            var row = $(this).parents('tr');
            var btn = $(this);
            var data = {
                teetime_id: teetime_info.TTID,
                customer_id: btn.data('customer-id'),
                position: btn.data('position')
            };

            $.post(SITE_URL + '/teesheets/no_show', data, function(){
                row.addClass('no-show');
                btn.replaceWith('<span>No-show</span>');
            },'json');
            return false;
        });
        $('#teetime_holes_9_label, #teetime_holes_18_label, #carts_0_label, #carts_1_label, #carts_2_label, #carts_3_label, #carts_4_label').off('click').on('click', function(ev){
            var holes = $(ev.currentTarget).attr('id').indexOf("9") > -1 ? 9 : 18;
            setTimeout(function() {
                $('#price_class_1, #price_class_2, #price_class_3, #price_class_4, #price_class_5').each(function (index,element) {
	                var element = $(element);
                    inline_form.trigger_dropdown_refresh(element);
                });
            }, 100);
        });
    },
    set_multi_number:function(){
        var count = $('.tee_time_person_select:checked:enabled').length;
        $("#purchase_multi_number").val(count);
    },
    set_available_rows:function(){
        // Set which rows are available to enter data
        var tee_time_titles = $('.tee_time_title');
        var player_count = 1;
        var last_row_populated = 0;
        for (var i = 0; i <= 4; i++) {
            var h = i+1;
            var content = $(tee_time_titles[i]).val();
            if (i > 0 && content == '' && !last_row_populated) {
                //$('#teetime_people_row_'+h+' input, #teetime_people_row_'+h+' select').attr('disabled', true);
                last_row_populated = 0;
            }
            else {
                if (!$('#teetime_people_row_'+i).hasClass('paid_row')) {
                    $('#teetime_people_row_' + i + ' .tee_time_person_select').attr('checked', true);
                }
                $('#teetime_people_row_'+h+' input, #teetime_people_row_'+h+' select').attr('disabled', false);
                if ($('#teetime_people_row_'+h).hasClass('paid_row') && $('#expand_players').attr('checked')) {
                    $('#teetime_people_row_'+h+" input.tee_time_person_select").attr('checked', false);
                }
                player_count = h;
                last_row_populated = 1;
                if (content == '') {
                    player_count = i;
                    last_row_populated = 0;
                }
            }
        }

        // Set the current player count
        var current_player_count = $('#number_players').val();
        if (current_player_count < player_count) {
            $('#number_players').val(player_count);
//            $('#number_players').blur();
        }
    },
    mark_rows:function(tee_time_info){
        var player_count = $('#number_players').val();
        for (var i = 1; i <= 5; i++) {
            
            var key = 'person_id';
            if(i < 1){
                key += '_'+i;
            }
            var customer_id = 0;
            if(tee_time_info[key]){
                customer_id = tee_time_info[key];
            }

            if (tee_time_info['person_paid_'+i] == '1') {
                // Color row green
                $('#teetime_people_row_'+i).addClass('paid_row');
                // Remove checkbox
                $('#teetime_people_row_'+i+' .tee_time_person_select').attr('checked', false);
                $('#teetime_people_row_'+i).find('button.no-show').hide();
            }
            else if(tee_time_info['person_no_show_'+i] == '1') {
                // Color row green
                $('#teetime_people_row_'+i).addClass('no-show');
                // Remove checkbox
                if (i <= player_count) {
                    $('#teetime_people_row_' + i + ' .tee_time_person_select').attr('checked', true);
                }
                $('#teetime_people_row_'+i).find('button.no-show').replaceWith('<span>No-show</span>');
            }        
            else {
                // Color row green
                $('#teetime_people_row_'+i).removeClass('paid_row');
                $('#teetime_people_row_'+i).removeClass('no-show');
                // Remove checkbox
                if (i <= player_count) {
                    $('#teetime_people_row_' + i + ' .tee_time_person_select').attr('checked', true);
                }
                $('#teetime_people_row_'+i).find('td.no-show').html('<button data-position="'+i+'" data-customer-id="'+customer_id+'" class="terminal_button no-show">No-show</button>');
            }
        }
    }
};
var shotgun = {
    initialize:function() {
        $('#shotgun_person_import').off('click').on('click', function(e){
            $.colorbox2({href:'index.php/teesheets/excel_import',title:'Import Players',width:'800'});
        });
        $('#view_cart_sign_options').off('click').on('click', function(e){
            e.preventDefault();
            shotgun.cart_signs.view();
        });
        $('#shotgun_grid span.person').contextMenu('shotgunPlayerMenu',{
            bindings:tbindings,
            onContextMenu:function(e, menu) {
                return true;
            },
            onShowMenu: function(e, menu) {
                return menu;
            }
        });
        $('input#shotgun_player_search').autocomplete({
            source: '<?php echo site_url('teesheets/customer_and_group_search/ln_and_pn/1'); ?>',
            delay: 150,
            autoFocus: false,
            minLength: 0,
            select: function(event, ui) {
                autocompleting = true;
                event.preventDefault();
                if (ui.item.is_group != undefined)
                {
                    shotgun.group.add($(this), ui.item.value);
                }
                else if (ui.item.add_customer != undefined)
                {
                    // Add customer
                    $.colorbox2({'href':'index.php/teesheets/view_customer_quick_add/'+ui.item.last_name+'/'+ui.item.first_name, 'title':'Add Customer', 'width':700});
                }
                else
                {
                    customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                    var data = {};
                    data[0] = {};
                    data[0].name = ui.item.name;
                    data[0].last_name = ui.item.last_name;
                    data[0].phone = ui.item.phone_number;
                    data[0].email = ui.item.email;
                    data[0].person_id = ui.item.value;
                    shotgun.populate($(this), data);
                }
                $(this).val('');
            },
            search: function(event, ui) {
                if ($('.autocomplete_loader').length < 1) {
                    $(this).after($('<span class="autocomplete_loader" style="margin-left:-26px; margin-bottom:-3px; background-image:url(\'/images/loading.gif\'); width:16px; display:inline-block; height:16px;"></span>'));
                    setTimeout(function(){
                        $('.autocomplete_loader').remove();
                    }, search_icon_time_limit);
                }
            },
            open: function(event, ui) {
                $('.autocomplete_loader').remove();
            },
            focus: function(event, ui) {
                event.preventDefault();
            }
        });
        $('input.shotgun-person').autocomplete({
            source: '<?php echo site_url('teesheets/customer_search/ln_and_pn/1'); ?>',
            delay: 150,
            autoFocus: false,
            minLength: 0,
            select: function(event, ui) {
                autocompleting = true;
                event.preventDefault();
                if (ui.item.is_group != undefined)
                {
                    shotgun.group.add($(this), ui.item.value);
                }
                else if (ui.item.add_customer != undefined)
                {
                    // Add customer
                    if (ui.item.last_name == '') {
                        ui.item.last_name = ui.item.first_name;
                    }
                    if (ui.item.first_name == '') {
                        ui.item.first_name = ui.item.last_name;
                    }
                    $.colorbox2({'href':'index.php/teesheets/view_customer_quick_add/'+ui.item.last_name+'/'+ui.item.first_name+'/'+$(this).attr('id'), 'title':'Add Customer', 'width':700});
                }
                else
                {
                    customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                    var data = {};
                    data.name = ui.item.name;
                    data.phone = ui.item.phone_number;
                    data.email = ui.item.email;
                    data.last_name = ui.item.last_name;
                    data.person_id = ui.item.value;
                    shotgun.player.add($(this), data);
                }
            },
            search: function(event, ui) {
                if ($('.autocomplete_loader').length < 1) {
                    $(this).after($('<span class="autocomplete_loader" style="margin-left:-26px; margin-bottom:-3px; background-image:url(\'/images/loading.gif\'); width:16px; display:inline-block; height:16px;"></span>'));
                    setTimeout(function(){
                        $('.autocomplete_loader').remove();
                    }, search_icon_time_limit);
                }
            },
            open: function(event, ui) {
                $('.autocomplete_loader').remove();
            },
            focus: function(event, ui) {
                event.preventDefault();
            }
        }).blur(function(){
            //alert('just blurred');

            if (!autocompleting)
            {
                shotgun.player.cancel_edit($(this));
            }
            autocompleting = false;
            var label = $(this).val();
            if (label != '') {
                var data = {};
                data.last_name = label;
                data.person_id = 0;
                shotgun.player.add($(this), data);
            }
        });
        $('input.shotgun-person').each(function(){
            var target = $(this);
            $(target).swipeable({allow_enter:false, auto_complete:target});
        });

        $('#shotgun_grid span.person span.person_label').off('click').on('click',  function(e){
            var target = $(this).parent();
            shotgun.player.edit(target);
            e.preventDefault();
        });

        $('#shotgun_grid span.person a.delete').off('click').on('click',  function(e){
            shotgun.player.remove($(this));
            e.preventDefault();
        });

        $('#shotgun_holes_9_label').off('click').on('click', function() {
            $(".eighteen_only").hide();
            $.colorbox.resize();
        });
        $('#shotgun_holes_18_label').off('click').on('click', function() {
            $(".eighteen_only").show();
            $.colorbox.resize();
        });
        // Show cart signs or not depending on if shotgun has been saved
        if ($('#shotgun_title').val() == '') {
            $('.cart_sign_options').hide();
        } else {
            $('.cart_sign_options').show();
        }

    },
    clear: function () {
        $('#shotgun_grid .shotgun-person:hidden').each(function(){
            var target = $(this);
            target.siblings('input[type="hidden"]').val('');
            target.siblings('span.person').remove();
            target.show().val('');
            target.parent().removeClass('checked_in').removeClass('paid');
        });
    },
    populate: function (starting_target, customers) {
        // Start on the selected field... populate sequentially.... if side A gets filled, add to side B. if side B gets filled, add to any open slot...
        // if all slots are filled, spit out a notification
        // add members one by one to shotgun into empty slots
        var primary_inputs = $('td.primary input.shotgun-person:visible');
        var secondary_inputs = $('td.secondary input.shotgun-person:visible');
        var switchover_index = 0;
        for (var i in customers)
        {
            if (primary_inputs.length > i)
            {
                shotgun.player.add($(primary_inputs[i]), customers[i]);
            }
            else if (secondary_inputs.length + primary_inputs.length > i)
            {
                shotgun.player.add($(secondary_inputs[i - primary_inputs.length]), customers[i]);
            }
            else
            {
                // notification that there are too many players
                alert('Not enough slots');
                return;
            }
        }

    },
    group: {
        add:function(starting_target, group_id) {
            // mask the popup
            $('#teetime_form').mask('Adding Group To Shotgun');
            // fetch members of the group
            $.ajax({
                type: "POST",
                url: "index.php/teesheets/get_group_members/"+group_id,
                data: "",
                success: function(response){
                    var customers = response.customers;
                    if (customers.length > 0)
                    {
                        shotgun.populate(starting_target, customers);
                    }
                    else
                    {
                        // Say that there were no customers in that group
                    }

                    $('#teetime_form').unmask();
                },
                dataType:'json'
            });
        }
    },
    cart_signs: {
        view:function(){
            var tee_time_id = $('#teetime_id').val();
            $.colorbox2({'href':'index.php/teesheets/view_print_cart/'+tee_time_id, 'title':'Print Cart Signs', 'width':1050});
        },
        refresh: function(){

        }
    },
    player: {
        add:function(target, data, save) {
            if (save == undefined) {
                save = true;
            }

            target.val(data.name);
            target.siblings('input[type="hidden"]').val(data.person_id);
            target.siblings('span.person').remove();
            target.hide().val('').after('<span class="person" title="'+data.name+'">' + '<input type="checkbox" value="'+data.person_id+'" name="checkin_box[]" class="person_select toggle" /><span class="person_label">' +data.last_name + '</span></span>');
            var input_name = target.attr('id');
            var name_parts = input_name.split('_');
            if (save) {
                shotgun.player.save(data.person_id, data.last_name, name_parts[3], name_parts[2]);
            }
            var person = target.siblings('span.person');
            person.contextMenu('shotgunPlayerMenu',{
                bindings:tbindings,
                onContextMenu:function(e, menu) {
                    return true;
                },
                onShowMenu: function(e, menu) {
                    return menu;
                }
            });
            $(target).siblings('span.person').find('span.person_label').off('click').on('click',  function(e){
                var target = $(this).parent();
                shotgun.player.edit(target);
                e.preventDefault();
            });
            //target.hide().val('').after('<span class="person">' + data.name + '<a href="#" class="delete" style="color: red;">Clear</a></span>');
        },
        save:function(person_id, name, player_position, hole) {
            $.ajax({
                type: "POST",
                url: "index.php/teesheets/save_shotgun_person/"+$("#teetime_id").val(),
                data: {'person_id':person_id, 'label':name, 'player_position':player_position, 'hole':hole},
                success: function(response){

                },
                dataType:'json'
            });
        },
        edit: function (target) {
            target.siblings('input[type="text"]').val('').show().focus();
            target.hide();
            //person.siblings('input[type="hidden"]').val('');
            //person.remove();
        },
        cancel_edit: function (target) {
            var person_id = target.siblings('input[type="hidden"]').val();

            if (person_id > 0)
            {
                var person = target.siblings('span.person');
                person.show();
                target.hide();
            }
            //person.siblings('input[type="hidden"]').val('');
            //person.remove();
        },
        remove: function (person) {
            //var person = target.parents('span.person');
            var target = person.siblings('input[type="text"]');
            var parent = person.parent();
            target.val('').show();
            person.siblings('input[type="hidden"]').val('');
            person.remove();
            parent.removeClass('paid');
            parent.removeClass('checked_id');
            // Update database too
            var input_name = target.attr('id');
            var name_parts = input_name.split('_');

            $.ajax({
                type: "POST",
                url: "index.php/teesheets/delete_shotgun_person/"+$("#teetime_id").val(),
                data: {'player_position':name_parts[3], 'hole':name_parts[2]},
                success: function(response){

                },
                dataType:'json'
            });
        },
        mark_paid: function (person) {
            //var person = target.parents('span.person');
            var target = person.siblings('input[type="text"]');
            // Update database too
            var input_name = target.attr('id');
            var name_parts = input_name.split('_');
            var parent = person.parent();
            var paid = (parent.hasClass('paid')) ? 0 : 1;
            if (paid)
            {
                parent.addClass('paid');
            }
            else
            {
                parent.removeClass('paid');
            }
            $.ajax({
                type: "POST",
                url: "index.php/teesheets/shotgun_person_pay/"+$("#teetime_id").val(),
                data: {'player_position':name_parts[3], 'hole':name_parts[2], 'paid':paid},
                success: function(response){

                },
                dataType:'json'
            });
        },
        mark_checked_in: function (person) {
            //var person = target.parents('span.person');
            var target = person.siblings('input[type="text"]');
            // Update database too
            var input_name = target.attr('id');
            var name_parts = input_name.split('_');
            var parent = person.parent();
            var checked_in = (parent.hasClass('checked_in')) ? 0 : 1;
            if (checked_in)
            {
                parent.addClass('checked_in');
            }
            else
            {
                parent.removeClass('checked_in');
            }

            $.ajax({
                type: "POST",
                url: "index.php/teesheets/shotgun_person_checkin/"+$("#teetime_id").val(),
                data: {'player_position':name_parts[3], 'hole':name_parts[2], 'checked_in':checked_in},
                success: function(response){

                },
                dataType:'json'
            });
        }
    }
};
var autocompleting = false;
var tbindings = {
    'check_in': function(t) {
        shotgun.player.mark_checked_in($(t));
    },
    'mark_paid': function(t) {
        shotgun.player.mark_paid($(t));
    },
    'delete': function(t) {
        shotgun.player.remove($(t));
    }
};
var tee_sheet_data = {
    pricing: {
        price_classes:{},
        prices:{
            // Date
                // Tee Sheet ID
                    // Price Class ID
                        // Start
        },
        get_price: function(tee_sheet_id, price_class_id, start, holes, carts){
            var prices = tee_sheet_data.pricing.prices;
            var date = '';// Derive from start
            if (typeof prices[date] == 'undefined' ||
                typeof prices[date][tee_sheet_id] == 'undefined' ||
                typeof prices[date][tee_sheet_id][price_class_id] == 'undefined'
            ){
                // Fetch Prices - Should never hit this
                console.log('******************************************* Problem: This code should not be getting hit. Prices should always be loaded prior to running get_price ***************************************************');
                tee_sheet_data.pricing.fetch_prices(date, tee_sheet_id);
            }
            else {
                // Return prices
                var relevant_prices = prices[date][tee_sheet_id][price_class_id]
                for (var i in relevant_prices) {
                    if (relevant_prices[i].start >= start && relevant_prices[i].end < start){
                        var total = 0;
                        if (holes == 18) {
                            total += relevant_prices[i].prices['price1']
                            if (carts){
                                total += relevant_prices[i].prices['price3']
                            }
                        }
                        else {
                            total += relevant_prices[i].prices['price2']
                            if (carts){
                                total += relevant_prices[i].prices['price4']
                            }

                        }
                        return total;
                    }
                }
            }
        },
        fetch_prices: function(date, tee_sheet_id, refresh) {
            if (typeof date == 'undefined') {
                date = '';// Today
            }
            if (typeof refresh == 'undefined') {
                refresh = false;
            }

            // AJAX prices for specific day
            var prices = tee_sheet_data.pricing.prices;
            if ((typeof prices[date] == 'undefined' ||
                typeof prices[date][tee_sheet_id] == 'undefined') ||
                refresh
            ) {
                $.ajax({
                    type: "POST",
                    url: "index.php/teesheets/fetch_prices/",
                    data: {'date': date, 'tee_sheet_id': tee_sheet_id},
                    success: function (response) {
                        // Save prices locally
                    },
                    dataType: 'json'
                });
            }
        }
    },
    customers:{
        // Person ID
            // Course IDs
    },
    events: {

    }
}
</script>
