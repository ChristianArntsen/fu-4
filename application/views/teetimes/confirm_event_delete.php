
<?php if (empty($linked_event_info['all_events']['count'])) {?>
    <div class="delete_message">
        Are you sure you want to delete this event?
    </div>
    <div id='delete_options' class="delete_button_holder">
        <button id="teetime_delete" class="teetime_delete_one deletebutton ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="float:left;" role="button" aria-disabled="false" data-delete-value="1"><span class="ui-button-text">Delete event</span></button>
    </div>
<?php } else { ?>
    <div class="delete_message">
        Would you like to delete only this event, all repeated events tied to this event, or this and all future repeated events?
    </div>
    <table id="delete_options" cellpadding="5">
        <tr>
            <td>
                <button id="teetime_delete" class="teetime_delete_one deletebutton ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="float:left;" role="button" aria-disabled="false" data-delete-value="1"><span class="ui-button-text">Only this Event</span></button>
            </td>
            <td>
                No other elements else will be deleted.
            </td>
        </tr>
        <tr>
            <td>
                <button id="teetime_delete" class="teetime_delete_future deletebutton ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="float:left;" role="button" aria-disabled="false" data-delete-value="2"><span class="ui-button-text">All future events</span></button>
            </td>
            <td>
                (<?=$linked_event_info['future_events']['count']?>) Delete this and all future repeated events.
            </td>
        </tr>
        <tr>
            <td>
                <button id="teetime_delete" class="teetime_delete_all deletebutton ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="float:left;" role="button" aria-disabled="false" data-delete-value="3"><span class="ui-button-text">All repeated events</span></button>
            </td>
            <td>
                (<?=$linked_event_info['all_events']['count']?>) Delete all repeated events.
            </td>
        </tr>
    </table>
    <table id="all_linked_events_table">
    <!--?php foreach ($linked_event_info['all_events']['events'] as $event) {
        echo "<tr>".
            "<td></td>";
    }?-->
    </table>
<?php } ?>
<style>
    .delete_message {
        text-align:center;
        padding:20px;
    }
    #delete_options #teetime_delete {
        width:210px;
        margin:5px 0px 5px 25px;
    }
    #delete_options {
        margin:0 0 20px;
    }
    .delete_button_holder {
        padding-left:200px;
        height:40px;
    }
</style>
<script>
    $(document).ready(function(){
        $('.teetime_delete_one, .teetime_delete_future, .teetime_delete_all').on('click', function(){
            var delete_value = $(this).data('delete-value');
            // Mark tee time to be deleted
            $('#delete_teetime').val(delete_value);
            // Submit form
            //alert (delete_value);
            $('#teetime_form').submit();
            $.colorbox2.close();
        });
    });
</script>

