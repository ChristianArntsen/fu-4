<?php $this->load->helper('date'); ?>
<link href="<?php echo base_url('css/jquery-ui-timepicker-addon.min.css'); ?>" type="text/css" rel="stylesheet" />
<script>
	function print_raincheck (response)
	{
		var rainchecks = response.data;

    	//Print raincheck
    	for (var i in rainchecks)
    	{
    		var data = rainchecks[i];
	        if ('<?php echo $this->config->item('webprnt')?>' == '1')
	        {
	            var builder = new StarWebPrintBuilder();
				var receipt_data = builder.createTextElement({data:'\nRaincheck Credit'});
	            // Items

	            // Totals
	            subtotal = (parseFloat(data.green_fee) + parseFloat(data.cart_fee))*parseInt(data.players);
	            if (data.customer != '')
	                receipt_data += builder.createTextElement({data:'\nCustomer: '+data.customer+'\n\n'});
	            receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('Green Fee: ','<?=$this->config->item('currency_symbol')?>'+parseFloat(data.green_fee).toFixed(2))+'\n'});
	            receipt_data += builder.createTextElement({data:add_white_space('Cart Fee: ','<?=$this->config->item('currency_symbol')?>'+parseFloat(data.cart_fee).toFixed(2))+'\n'});
	            receipt_data += builder.createTextElement({data:'\n'+add_white_space('Players: ','x '+(data.players))+'\n'});
	            receipt_data += builder.createTextElement({data:add_white_space('Subtotal: ','<?=$this->config->item('currency_symbol')?>'+parseFloat(subtotal).toFixed(2))+'\n'});
	            receipt_data += builder.createTextElement({data:'\n'+add_white_space('Tax: ','<?=$this->config->item('currency_symbol')?>'+parseFloat(data.tax).toFixed(2))+'\n'});
	            receipt_data += builder.createTextElement({data:'\n'+add_white_space('Total Credit: ','<?=$this->config->item('currency_symbol')?>'+parseFloat(data.total).toFixed(2))+'\n'});

	            // Sale id and barcode
	            //receipt_data += chr(27)+chr(97)+chr(49);
	            //receipt_data += "\x1Dh" +chr(80);  // ← Set height
		        receipt_data += builder.createTextElement({data:"\n\n"});// Some spacing at the bottom
		        var sale_num = String(data.raincheck_number);

		        // Sale id and barcode
	            receipt_data = webprnt.add_barcode(receipt_data, 'Raincheck ID', 'RID '+data.raincheck_number);

			    var header_data = {
			    	course_name:'<?php echo addslashes($this->config->item('name'))?>',
			    	address:'<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>',
			    	address_2:'<?php echo addslashes($this->config->item('city')).', '.$this->config->item('state').' '.$this->config->item('zip')?>',
			    	phone:'<?php echo $this->config->item('phone')?>',
			    	employee_name:'<?php echo addslashes($user_info->last_name).', '.addslashes($user_info->first_name); ?>',
			    	customer:''// Need to load this in from data
			    };
			    receipt_data = webprnt.add_receipt_header(receipt_data, header_data)
				console.log('about to print raincheck');
	            //print_receipt(receipt_data);
	            receipt_data = webprnt.add_paper_cut(receipt_data);

			    webprnt.print(receipt_data, window.location.protocol + "//" + "<?=$this->config->item('webprnt_ip')?>/StarWebPRNT/SendMessage");
	            //print_webprnt_receipt(receipt_data, false, data.raincheck_number, response.receipt_data);
	        }
	        else if ('<?php echo $this->config->item('print_after_sale')?>' == '1')
	        {
	        	if (<?php echo $controller_name == 'teesheets' ? 1:0?>)
					window.location = 'index.php/sales/raincheck/'+data.raincheck_number;

	        	var receipt_data = '\nRaincheck Credit';
	            // Items

	            // Totals
	            subtotal = (parseFloat(data.green_fee) + parseFloat(data.cart_fee))*parseInt(data.players);
	            if (data.customer != '')
	                receipt_data += '\nCustomer: '+data.customer+'\n\n';
	            receipt_data += '\n\n'+add_white_space('Green Fee: ','<?=$this->config->item('currency_symbol')?>'+parseFloat(data.green_fee).toFixed(2))+'\n';
	            receipt_data += add_white_space('Cart Fee: ','<?=$this->config->item('currency_symbol')?>'+parseFloat(data.cart_fee).toFixed(2))+'\n';
	            receipt_data += '\n'+add_white_space('Players: ','x '+(data.players))+'\n';
	            receipt_data += add_white_space('Subtotal: ','<?=$this->config->item('currency_symbol')?>'+parseFloat(subtotal).toFixed(2))+'\n';
	            receipt_data += '\n'+add_white_space('Tax: ','<?=$this->config->item('currency_symbol')?>'+parseFloat(data.tax*data.players).toFixed(2))+'\n';
	            receipt_data += '\n'+add_white_space('Total Credit: ','<?=$this->config->item('currency_symbol')?>'+parseFloat(data.total).toFixed(2))+'\n';

	            // Sale id and barcode
	            receipt_data += chr(27)+chr(97)+chr(49);
	            receipt_data += "\x1Dh" +chr(80);  // ← Set height
		        receipt_data += "\n\n";// Some spacing at the bottom
		        var sale_num = String(data.raincheck_number);
		        var len = sale_num.length;
				if (len == 2)
			        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x09\x7B\x41\x52\x49\x44\x20"+sale_num;
				else if (len == 3)
			        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0A\x7B\x41\x52\x49\x44\x20"+sale_num;
				else if (len == 4)
			        receipt_data += "\x1D\x77\x01\x1D\x6B\x49\x0B\x7B\x41\x52\x49\x44\x20"+sale_num;
		        else if (len == 5)
			        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0C\x7B\x41\x52\x49\x44\x20"+sale_num;
		        else if (len == 6)
			        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0D\x7B\x41\x52\x49\x44\x20"+sale_num;
		        else if (len == 7)
			        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0E\x7B\x41\x52\x49\x44\x20"+sale_num;
		        else if (len == 8)
			        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0F\x7B\x41\x52\x49\x44\x20"+sale_num;
		        else if (len == 9)
			        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x10\x7B\x41\x52\x49\x44\x20"+sale_num;
		        else if (len == 10)
			        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x11\x7B\x41\x52\x49\x44\x20"+sale_num;
			    console.log('adding barcode with '+sale_num+' stuff '+len);
	            // Sale id and barcode
	            receipt_data += '\n\nRaincheck ID: RID '+data.raincheck_number+'\n';
	            console.log('about to print raincheck');
	            print_receipt(receipt_data);
	        }
	        else
	        {
	            window.location = 'index.php/sales/raincheck/'+data.raincheck_number;
	        }
	    }
	    if ('<?php echo $this->config->item('webprnt')?>' == '1')
	    {
	    	webprnt.print_all(window.location.protocol + "//" + "<?=!empty($webprnt_ip) ? $webprnt_ip : ''?>/StarWebPRNT/SendMessage");
	    }

        $.colorbox.close();
        //post_campaign_form_submit(response);
	}
	function add_white_space(str_one, str_two)
	{
       var width = 42;
       var strlen_one = str_one.length;
       var strlen_two = str_two.length;
       var white_space = '';
       var white_space_length = 0;
       if (strlen_one + strlen_two >= width)
               return (str_one.substr(0, width - strlen_two - 4)+'... '+str_two); //truncated if text is longer than available space
       else
               white_space_length = width - (strlen_one + strlen_two);

       for (var i = 0; i < white_space_length; i++)
               white_space += ' ';
       return str_one+white_space+str_two;
	}

</script>
<?php $webprnt_ip = $this->config->item('webprnt_ip');?>
<?php
 if ($teetime_info && $teetime_info->paid_player_count == 0) {
 	?>
 	<div class='raincheck_error'>This tee time has not bee paid for yet. No raincheck can be issued at this time.</div>
 	<?php
 }
 else if ($teetime_info && $teetime_info->raincheck_players_issued > 0) {
 		$raincheck_data = $this->Sale->get_teetime_rainchecks($teetime_info->TTID)->result_array();
	 	foreach ($raincheck_data as $index => $rnck)
		{
	 		$customer = $this->Customer->get_info($rnck['customer_id']);
			$raincheck_data[$index]['customer'] = $customer->first_name.' '.$customer->last_name;
		}
		$raincheck = json_encode(array('data'=>$raincheck_data));
		//echo $raincheck;
	 	?>
	 	<script>
	 	var raincheck = $.parseJSON('<?=$raincheck?>');
	 	print_raincheck(raincheck);
	 	</script>
	 	<?php
 }
 else
 {

?>

<?php
echo form_open('/sales/save_raincheck/', array('id'=>'raincheck_form'));
//echo "teesheet holes: ".$teesheet_holes.'<br/>';
//print_r($teetime_info);
//print_r($green_fees);
$customer_name = '';
if ($teetime_info->person_id)
{
	$customer = $this->Customer->get_info($teetime_info->person_id);
	$customer_name = $customer->last_name.', '.$customer->first_name;
}
?>
<style>
	.totals_label {
		text-align:right;
	}
	#green_fee_total, #cart_fee_total, #tax_total, #total_credit_total, #player_total, #subtotal_total {
		text-align:right;
		width:100px;
		padding-right:32px;
	}
	fieldset div.field_row label {
		width:50px;
		float:none;
		height:25px;
	}
	fieldset div.field_row label.dropdown {
		width: 175px;
		padding: 5px;
		float: left;
	}
	fieldset div.field_row label span.ui-button-text{
		line-height:15px;
	}
	.player_buttons .ui-buttonset .ui-button .ui-button-text, .cart_buttons .ui-buttonset .ui-button .ui-button-text {
	    height: 12px;
	    line-height: 0.6em;
	    width: 22px;
	}
	.player_buttons .ui-buttonset .ui-button, .cart_buttons .ui-buttonset .ui-button {
	    height: 20px;
	    /*width:30px;*/
	}
	#raincheck_basic_info {
		padding: 5px;
		border:none;
	}
	fieldset {
		border: 1px solid #DDD;
	}
    .r_players_buttonset label.ui-state-active {
        border-right: none;
        box-shadow: inset -2px 2px 30px 3px black;
    }
</style>

<fieldset id="raincheck_basic_info">
<legend>Raincheck Info</legend>
	<div class="field_row clearfix">
		<?php echo form_label('Course:', 'teesheet_id',array('class'=>'wide dropdown')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('teesheet_id', $teesheet_options, $this->session->userdata('teesheet_id')); ?>
		</div>
	</div>

	<div class="field_row clearfix">
		<?php echo form_label('Green Fee:', 'green_fee',array('class'=>'wide dropdown')); ?>
		<div class='form_field'>
		<?php if($this->config->item('seasonal_pricing') == 1){
			echo form_dropdown('green_fee_dropdown', $green_fees, $pricing['green_fee_dropdown']);
		}else{
			echo $green_fee_dropdown;
		} ?>
		</div>
	</div>

	<div class="field_row clearfix">
		<?php echo form_label('Cart Fee:', 'cart_fee',array('class'=>'wide dropdown')); ?>
		<div class='form_field'>
		<?php if($this->config->item('seasonal_pricing') == 1){
			echo form_dropdown('cart_fee_dropdown', $cart_fees, $pricing['cart_fee_dropdown']);
		}else{
			echo $cart_fee_dropdown;
		} ?>
		</div>
	</div>

	<div class="field_row clearfix">
		<?php echo form_label('Customer:', 'customer_name',array('class'=>'wide dropdown')); ?>
		<div class='form_field'>
		<?php echo form_input(array('name' => 'customer_name', 'id' => 'customer_name', 'value' => $customer_name));?>
		<?php echo form_hidden('raincheck_customer_id', $teetime_info->person_id);?>
		<?php echo form_hidden('raincheck_teetime_id', $teetime_info->TTID);?>
		</div>
	</div>

	<div class="field_row clearfix">
	<?php echo form_label('Expiry Date:', 'expirydate',array('class'=>'wide dropdown'));?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'expiry_date', 'id' => 'expiry_date', 'value' => !empty($expiry_date) ? $expiry_date -> expirydate : ''));?>
	</div>
	</div>

	<?php if ($this->config->item('webprnt') || ($this->config->item('print_after_sale') && $controller_name != 'teesheets')) {?>
	<div class="field_row clearfix">
		<?php echo form_label('Split Raincheck:', 'split_raincheck',array('class'=>'wide dropdown')); ?>
		<div class='form_field'>
		<?php echo form_checkbox(array('name' => 'split_raincheck', 'id' => 'split_raincheck', 'value' => '1'));?>
		</div>
	</div>
	<?php } ?>

	<div id='players'>
		<div class="field_row clearfix">
		<div>Players: </div>
		</div>
		<div class="field_row clearfix">
		<span class='r_players_buttonset'>
			<input type='radio' id='players_1_r' name='players' value='1' <?php echo ($teetime_info->player_count == '1' || !$teetime_info->player_count)?'checked':''; ?>/>
			<label for='players_1_r' id='players_1_r_label'>1</label>
			<input type='radio' id='players_2_r' name='players' value='2' <?php echo ($teetime_info->player_count == '2')?'checked':''; ?>/>
			<label for='players_2_r' id='players_2_r_label'>2</label>
			<input type='radio' id='players_3_r' name='players' value='3' <?php echo ($teetime_info->player_count == '3')?'checked':''; ?>/>
			<label for='players_3_r' id='players_3_r_label'>3</label>
			<input type='radio' id='players_4_r' name='players' value='4' <?php echo ($teetime_info->player_count == '4')?'checked':''; ?>/>
			<label for='players_4_r' id='players_4_r_label'>4</label>
			<input type='radio' id='players_5_r' name='players' value='5' <?php echo ($teetime_info->player_count == '5')?'checked':''; ?>/>
			<label for='players_5_r' id='players_5_r_label'>5</label>
		</span>
		</div>
	</div>

	<div id='holes'>
		<div class="field_row clearfix">
		<div>Number of Holes Completed?</div>
		</div>
		<div class="field_row clearfix">
		<span class='holes_buttonset'>
			<input type='radio' id='holes_0' name='holes' value='0' checked />
			<label for='holes_0' id='holes_0_label'>0</label>
			<input type='radio' id='holes_1' name='holes' value='1' />
			<label for='holes_1' id='holes_1_label'>1</label>
			<input type='radio' id='holes_2' name='holes' value='2' />
			<label for='holes_2' id='holes_2_label'>2</label>
			<input type='radio' id='holes_3' name='holes' value='3' />
			<label for='holes_3' id='holes_3_label'>3</label>
			<input type='radio' id='holes_4' name='holes' value='4' />
			<label for='holes_4' id='holes_4_label'>4</label>
			<input type='radio' id='holes_5' name='holes' value='5' />
			<label for='holes_5' id='holes_5_label'>5</label>
			<input type='radio' id='holes_6' name='holes' value='6' />
			<label for='holes_6' id='holes_6_label'>6</label>
			<input type='radio' id='holes_7' name='holes' value='7' />
			<label for='holes_7' id='holes_7_label'>7</label>
			<input type='radio' id='holes_8' name='holes' value='8' />
			<label for='holes_8' id='holes_8_label'>8</label>
			<?php //if ($teetime_info->holes == 18) { ?>
				<br/>
			<span id='last_9'>
			<input type='radio' id='holes_9' name='holes' value='9' />
			<label for='holes_9' id='holes_9_label'>9</label>
			<input type='radio' id='holes_10' name='holes' value='10' />
			<label for='holes_10' id='holes_10_label'>10</label>
			<input type='radio' id='holes_11' name='holes' value='11' />
			<label for='holes_11' id='holes_11_label'>11</label>
			<input type='radio' id='holes_12' name='holes' value='12' />
			<label for='holes_12' id='holes_12_label'>12</label>
			<input type='radio' id='holes_13' name='holes' value='13' />
			<label for='holes_13' id='holes_13_label'>13</label>
			<input type='radio' id='holes_14' name='holes' value='14' />
			<label for='holes_14' id='holes_14_label'>14</label>
			<input type='radio' id='holes_15' name='holes' value='15' />
			<label for='holes_15' id='holes_15_label'>15</label>
			<input type='radio' id='holes_16' name='holes' value='16' />
			<label for='holes_16' id='holes_16_label'>16</label>
			<input type='radio' id='holes_17' name='holes' value='17' />
			<label for='holes_17' id='holes_17_label'>17</label>
			</span>
			<?php //} ?>
		</span>
		</div>
	</div>
        <table><tbody>
        <tr>
        	<td class='totals_label' colspan="2">Green Fee:</td>
        	<td id='green_fee_total'><?=$this->config->item('currency_symbol')?>0.00</td>
        </tr>
        <tr>
        	<td class='totals_label' colspan="2">Cart Fee:</td>
        	<td id='cart_fee_total'><?=$this->config->item('currency_symbol')?>0.00</td>
        </tr>
        <tr>
        	<td class='totals_label' colspan="2">Players:</td>
        	<td id='player_total'>x 1</td>
        </tr>
        <tr>
        	<td class='totals_label' colspan="2">Subtotal:</td>
        	<td id='subtotal_total'><?=$this->config->item('currency_symbol')?>0.00</td>
        </tr>
        <tr>
        	<td class='totals_label' colspan="2">Tax:</td>
        	<td id='tax_total'><?=$this->config->item('currency_symbol')?>0.00</td>
        </tr>
        <tr>
        	<td class='totals_label' colspan="2">Total Credit:</td>
        	<td id='total_credit_total'><?=$this->config->item('currency_symbol')?>0.00</td>
        </tr>
        <tr>
        	<td colspan=3>
        		<?php
        			echo form_hidden(array('green_fee'=>0,'cart_fee'=>0,'tax'=>0,'total_credit'=>0));
				?>
        		<?php
				echo form_submit(array(
					'name'=>'submitButton',
				 	'id'=>'submitButton',
					'value'=> 'Issue Raincheck',
					'class'=>'submit_button float_right')
				);
				?>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<?php form_close(); ?>
<script>
	var raincheck = {
		course_id:'<?= $this->session->userdata('course_id')?>',
		cart_fee: 0,
		green_fee: 0,
		cart_holes: 9,
		green_fee_holes: 9,

		fees:{},//$.parseJSON('<?= json_encode(!empty($fees) ? $fees : '')?>'),
		taxes:{},//$.parseJSON('<?= json_encode($taxes)?>'),

		get_raincheck_info: function () {
			var teesheet_id = $('#teesheet_id').val();
			$.ajax({
               type: "POST",
               url: "index.php/sales/get_raincheck_info/"+teesheet_id,
               data: "",
               success: function(response){
               		console.dir(response);
               		$('#green_fee_dropdown').replaceWith($(response.green_fee_dropdown));
               		$('#cart_fee_dropdown').replaceWith($(response.cart_fee_dropdown));
               		raincheck.initialize_dropdowns();
               		raincheck.fees = response.fees;
               		raincheck.update_totals();
               },
               dataType:'json'
            });
		},

		toggle_holes: function(){
			var gfv = $('#green_fee_dropdown').val();
			var holes_completed = $('input[name=holes]:checked').val();
			var parts = gfv.split('_');

			<?php if($this->config->item('seasonal_pricing') == 1){ ?>
			if (parts[2] == 2 || parts[2] == 4)
			{
				if (holes_completed > 8)
				{
					$('#holes_0_label').click();
					$('#holes_0').click();
				}
				$('#last_9').hide();
			}
			else
			{
				$('#last_9').show();
			}

			<?php }else{ ?>
			if (parts[1] < 5)
			{
				if (holes_completed > 8)
				{
					$('#holes_0_label').click();
					$('#holes_0').click();
				}
				$('#last_9').hide();
			}
			else
			{
				$('#last_9').show();
			}
			<?php } ?>
		},

		initialize_dropdowns: function (){
			$('#green_fee_dropdown, #cart_fee_dropdown').change(function(){
				raincheck.toggle_holes();
				raincheck.get_prices();
			});
		},
		get_prices: function(){

			var teesheet_id = $('#teesheet_id').val();
			var players = $('input[name=players]:checked').val();
			var holes_completed = $('input[name=holes]:checked').val();
			var gf_index = $('#green_fee_dropdown').val();
			var c_index = $('#cart_fee_dropdown').val();
			var gf_price = c_price = taxes = total = 0;

			<?php if($this->config->item('seasonal_pricing') == 1){ ?>
			var teetime_date = $('#teetime_date').val();
			var teetime_time = $('#teetime_time').val();
			var green_fee_parts = gf_index.split('_');
			var cart_fee_parts = c_index.split('_');

			var green_fee_class_id = green_fee_parts[0];
			var green_fee_timeframe = green_fee_parts[1];
			var green_fee_item_num = green_fee_parts[2];
			var cart_fee_class_id = cart_fee_parts[0];
			var cart_fee_timeframe = cart_fee_parts[1];
			var cart_fee_item_num = cart_fee_parts[2];

			if(green_fee_item_num == 1){
				raincheck.green_fee_holes = 18;
			}else if(green_fee_item_num == 2){
				raincheck.green_fee_holes = 9;
			}
			var params = {
				timeframe_id: green_fee_timeframe,
				holes: raincheck.green_fee_holes,
				cart: false
			};

			// Get price for green fee via ajax
			if(gf_index && gf_index != ''){
				$.get("<?php echo site_url('prices/get_price'); ?>", params, function(response){
					raincheck.green_fee = response.price;
					raincheck.update_totals();
				},'json');
			}

			if(cart_fee_item_num == 3){
				raincheck.cart_holes = 18;
			}else if(cart_fee_item_num == 4){
				raincheck.cart_holes = 9;
			}
			var params = {
				timeframe_id: cart_fee_timeframe,
				holes: raincheck.cart_holes,
				cart: true
			};

			// Get price for cart fee via ajax
			if(c_index && c_index != ''){
				$.get("<?php echo site_url('prices/get_price'); ?>", params, function(response){
					raincheck.cart_fee = response.price;
					raincheck.update_totals();
				},'json');
			}

			<?php }else{ ?>

			if (gf_index == '' && c_index == '')
			{

			}
			else
			{
				if (gf_index != '')
				{
					var gf_indexes = gf_index.split('_');
					var gf_price_cat = gf_indexes[0];
					var gf_item = gf_indexes[1];
					var gf_holes = (gf_item>=5)?18:9;
					raincheck.green_fee_holes = gf_holes;

					console.log(teesheet_id+' '+this.course_id+'_'+gf_item+' price_category_'+gf_price_cat);
					console.log(this.fees[teesheet_id][this.course_id+'_'+gf_item]['price_category_'+gf_price_cat]);
					console.log(gf_holes+' '+holes_completed+' '+(gf_holes - holes_completed)/gf_holes);
					raincheck.green_fee = this.fees[teesheet_id][this.course_id+'_'+gf_item]['price_category_'+gf_price_cat];
				}
				if (c_index != '')
				{
					var c_indexes = c_index.split('_');
					var c_price_cat = c_indexes[0];
					var c_item = c_indexes[1];
					var c_holes = (c_item>=5)?18:9;
					raincheck.cart_holes = c_holes;

					raincheck.cart_fee = this.fees[teesheet_id][this.course_id+'_'+c_item]['price_category_'+c_price_cat];
				}
			}
			raincheck.update_totals();
			<?php } ?>
		},
		update_totals: function() {
			var teesheet_id = $('#teesheet_id').val();
			var players = $('input[name=players]:checked').val();
			var holes_completed = $('input[name=holes]:checked').val();
			var gf_price = c_price = taxes = total = 0;

			gf_price = raincheck.green_fee * ((raincheck.green_fee_holes - holes_completed) / raincheck.green_fee_holes);
			c_price = raincheck.cart_fee * ((raincheck.cart_holes - holes_completed) / raincheck.cart_holes);

			if ("<?= $this->config->item('unit_price_includes_tax') ?>" == 1) {
				gf_subtotal = this.taxes.gf[0] ? (gf_price * players) / (1 + (this.taxes.gf[0].percent / 100)) : 0;
				c_subtotal =  this.taxes.c[0]  ? (c_price  * players) / (1 + (this.taxes.c[0].percent  / 100)) : 0;
				subtotal = parseFloat(gf_subtotal + c_subtotal).toFixed(2);
				total = (gf_price + c_price) * players;
				taxes = total - subtotal;
				console.log('erroring for some reason '+taxes+' total '+total+' subtotal '+subtotal);
			}
			else {
				c_total = c_price * players;
				gf_total = gf_price * players;

				if(<? echo isset($cart_fee_tax_included) && $cart_fee_tax_included == 1 ? "true" : "false"?> ){
					c_subtotal =  this.taxes.c[0]  ? (c_price  * players) / (1 + (this.taxes.c[0].percent  / 100)) : 0;
					c_taxes = c_total - c_subtotal;
				} else {
					c_subtotal = c_total;
					c_taxes = (this.taxes.c[0]?((c_price * players) * this.taxes.c[0].percent / 100):0);
				}

				if(<? echo isset($green_fee_tax_included) && $green_fee_tax_included == 1 ? "true" : "false"?> ){
					gf_subtotal = this.taxes.gf[0] ? (gf_price * players) / (1 + (this.taxes.gf[0].percent / 100)) : 0;
					gf_taxes = gf_total - gf_subtotal;
				}else {
					gf_subtotal = gf_total;
					gf_taxes = (this.taxes.gf[0]?((gf_price *players) * this.taxes.gf[0].percent / 100):0);
				}
				subtotal = parseFloat(gf_subtotal.toFixed(2)) + parseFloat(c_subtotal.toFixed(2));
				taxes = parseFloat(c_taxes.toFixed(2)) + parseFloat(gf_taxes.toFixed(2));
				total = parseFloat(subtotal+taxes);
			}
			//Update inputs
			$('#green_fee').val(gf_price.toFixed(2));
			$('#cart_fee').val(c_price.toFixed(2));
			$('#tax').val(taxes.toFixed(2));
			$('#total_credit').val(total.toFixed(2));

			//Update visible
			$('#green_fee_total').html('<?=$this->config->item('currency_symbol')?>'+gf_price.toFixed(2));
			$('#cart_fee_total').html('<?=$this->config->item('currency_symbol')?>'+c_price.toFixed(2));
			$('#subtotal_total').html('<?=$this->config->item('currency_symbol')?>'+subtotal.toFixed(2));
			$('#tax_total').html('<?=$this->config->item('currency_symbol')?>'+taxes.toFixed(2));
			$('#player_total').html('x '+players);
			$('#total_credit_total').html('<?=$this->config->item('currency_symbol')?>'+total.toFixed(2));
		}
	};

	$(document).ready(function(){
		raincheck.fees = <?= json_encode(!empty($fees) ? $fees : '')?>;
		raincheck.taxes = <?= json_encode($taxes)?>;
		raincheck.get_prices();
		raincheck.toggle_holes();

        $('#expiry_date').datetimepicker({
            defaultDate: new Date(),
            dateFormat: 'yy-mm-dd',
            timeFormat: 'HH:mm',
            defaultValue: '<?php echo date('Y-m-d'); ?> 12:00:00'
        });

		$( "#customer_name" ).autocomplete({
			source: '<?php echo site_url("sales/customer_search/ln_and_pn"); ?>',
			delay: 200,
			autoFocus: false,
			minLength: 0,
			select: function(event, ui)
			{
				event.preventDefault();
				customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
				$("#customer_name").val(ui.item.label);
				$('#raincheck_customer_id').val(ui.item.value);
			}
		});

		$('#raincheck_form #raincheck_basic_info .r_players_buttonset').buttonset();
		$('.holes_buttonset').buttonset();

		$('#teesheet_id').change(function(){
			raincheck.get_raincheck_info();
			raincheck.get_prices();
		});

		raincheck.initialize_dropdowns();

		$('input[name=players], input[name=holes]').change(function(){
			raincheck.update_totals();
		});

    	var submitting = false;
    	$('#raincheck_form').validate({
			submitHandler:function(form)
			{
	      		if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				$(form).ajaxSubmit({
			        success:function(response)
			        {
			        	print_raincheck(response);
					    submitting = false;
	    	        },
			        dataType:'json'
	  	        });
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules:
			{

		    },
			messages:
			{

			}
		});
        $('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	});
</script>
<?php } ?>
