<script>
    console.log('Starting Page load');
    console.log(new Date().getTime());
</script>

<div class='background'>
<?php
echo form_open('/teesheets/save_teetime/'.$teetime_info->TTID,array('id'=>'teetime_form'));
//echo 'teesheet holes: '.$teesheet_holes.'<br/>';
//print_r($customer_info);
?>
<div id='radioset' class='teebuttons'>
	<input type="hidden" name="booking_source" value="<?php echo $teetime_info->booking_source; ?>" />
	<input type="hidden" name="promo_id" value="<?php echo isset($teetime_info->promo_id) ? $teetime_info->promo_id : ''; ?>" />
	<input type='radio' value='teetime' id='type_teetime' name='event_type'  <?php echo ($teetime_info->type == 'teetime' || $teetime_info->type == '')?'checked':''; ?>><label id='teetime_label' for='type_teetime' >Tee Time</label>
    <input type='radio' value='tournament' id='type_tournament' name='event_type' <?php echo ($teetime_info->type == 'tournament')?'checked':''; ?>><label id='tournament_label' for='type_tournament'>Tournament</label>
    <input type='radio' value='league' id='type_league' name='event_type' <?php echo ($teetime_info->type == 'league')?'checked':''; ?>><label id='league_label' for='type_league'>League</label>
    <input type='radio' value='event' id='type_event' name='event_type' <?php echo ($teetime_info->type == 'event')?'checked':''; ?>><label id='event_label' for='type_event'>Event</label>
    <input type='radio' value='closed' id='type_closed' name='event_type' <?php echo ($teetime_info->type == 'closed')?'checked':''; ?>><label id='closed_label' for='type_closed'>Block</label>
    <input type='radio' value='shotgun' id='type_shotgun' name='event_type' <?php echo ($teetime_info->type == 'shotgun')?'checked':''; ?>><label id='shotgun_label' for='type_shotgun'>Shotgun</label>
    <div class='clear'></div>
</div>
<div id='closed_table' <?php echo ($teetime_info->type == 'closed')?'':"style='display:none'"?>>
	<?php $this->load->view('teetimes/closed'); ?>
</div>
<div id='event_table'  <?php echo ($teetime_info->type == 'tournament' || $teetime_info->type == 'league' || $teetime_info->type == 'event')?'':"style='display:none'"?>>
	<?php $this->load->view('teetimes/event'); ?>
</div>
<div id="shotgun_table" <?php echo ($teetime_info->type == 'shotgun')?'':"style='display:none'"?>>
	<?php $this->load->view('teetimes/shotgun'); ?>
</div>
<div id='teetime_table' <?php echo ($teetime_info->type == 'teetime' || $teetime_info->type == '')?'':"style='display:none'"?>>
	<?php $this->load->view('teetimes/teetime'); ?>
</div>

<?php $this->load->view('teetimes/styles'); ?>

<script type='text/javascript'>
var i = 0;
function event_person_row(data){
	var html = '<li id="event_person_'+data.person_id+'">' +
		'<input type="hidden" name="event_people_'+i+'" value="' + data.person_id + '" />' +
		'<span class="clear_data delete">x</span>' +
		'<span class="name">' + data.name + '</span>' +
		'<span class="phone">' + data.phone + '</span>' +
		'<span class="email">' + data.email + '</span>' +
		'<span class="status">' +
			'<a href="#" class="pay"><?=$this->config->item('currency_symbol') ?> Pay</a>' +
			'<a href="#" class="checkin">✔ Check In</a>' +
		'</span>' +
	'</li>';
	i++;
	return html;
}

function clear_player_data(row) {
	if (row == 1)
	{
		$('#teetime_title').val('');
		$('#person_id').val('');
		$('#email').val('');
		$('#phone').val('');
		$('#phone').val('');
		$('#credit_card_id').replaceWith('<input type="hidden" id="credit_card_id" value="0" />');
	}
	else
	{
		$('#teetime_title_'+row).val('');
		$('#person_id_'+row).val('');
		$('#email_'+row).val('');
		$('#phone_'+row).val('');
	}
}
function added_credit_card(cc_id)
{
	$('#added_credit_card_id').val(cc_id);
	update_credit_card_dropdown(0,cc_id);
}
function update_credit_card_dropdown(customer_id, cc_id)
{
	customer_id = !customer_id || customer_id == undefined ? $('#person_id').val() : customer_id;
	cc_id = !cc_id || cc_id == undefined ? $('#added_credit_card_id').val() : cc_id;
	$.ajax({
       type: "POST",
       url: "index.php/teesheets/credit_card_dropdown/"+customer_id+'/'+cc_id,
       data: '',
       success: function(response){
       		$('#credit_card_id').replaceWith(response);
	        $.colorbox2.close();
	    },
        dataType:'html'
     });
}
function focus_on_title(title) {
	<?php if ($teetime_info->type == 'teetime' || $teetime_info->type == ''){
			$focus = 'teetime_title';
		}else if ($teetime_info->type == 'closed'){
			$focus = 'closed_title';
		}else{
			$focus = 'event_title';
		} ?>
	title = (title == undefined)?'<?php echo $focus;?>':title;
    $('#'+title).focus().select();
}

function set_session(flag, comments){

}
function initialize_additional_players() {
    <?php if($this->config->item('currency_symbol') == '0' || $this->config->item('currency_symbol') == "$" ):?>
        $("#phone_2").mask2("(999) 999-9999");
        $("#phone_3").mask2("(999) 999-9999");
        $("#phone_4").mask2("(999) 999-9999");
        $("#phone_5").mask2("(999) 999-9999");
    <?php endif; ?>
    for (var i = 2; i < 6; i++) {
        $( '#teetime_title_'+i ).autocomplete({
            source: '<?php echo site_url('teesheets/customer_search/'); ?>',
            delay: <?=$this->config->item("search_delay") ?>,
            autoFocus: false,
            minLength: 1,
            select: function(event, ui) {
                event.preventDefault();
                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                var index = $(this).attr('id').replace('teetime_title_', '');
                $('#teetime_title_'+index).val(ui.item.label);
                $('#email_'+index).val(ui.item.email).removeClass('teetime_email');
                $('#phone_'+index).val(ui.item.phone_number).removeClass('teetime_phone');
                $('#person_id_'+index).val(ui.item.value);
                $('#price_class_'+index).val(ui.item.price_class);
            },
            focus: function(event, ui) {
                event.preventDefault();
            }
        });
    }
    $('#teetime_title_2').swipeable({allow_enter:false, auto_complete:'#teetime_title_2'});
    $('#teetime_title_3').swipeable({allow_enter:false, auto_complete:'#teetime_title_3'});
    $('#teetime_title_4').swipeable({allow_enter:false, auto_complete:'#teetime_title_4'});
    $('#teetime_title_5').swipeable({allow_enter:false, auto_complete:'#teetime_title_5'});
    $('#teetime_title_2, #teetime_title_3, #teetime_title_4, #teetime_title_5').keyup(function(){
        var focus = $(this);
        if (focus.val() == '')
        {
            var index = focus.attr('id').replace('teetime_title_', '');
            index = (index == 'teetime_title' ? 1 :index);

            clear_player_data(index);
        }
    });
    for (var i = 2; i < 6; i++) {
        $('#phone_'+i+', #zip_'+i+', #email_'+i).keypress(function (e) {
            var index = $(this).attr('id').replace('phone_', '').replace('zip_', '').replace('email_', '');
            $('#save_customer_'+index).attr('checked', true);
            $('#save_customer_'+index).prev().addClass('checked');
        });
        $( '#phone_'+i ).autocomplete({
            source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
            delay: 200,
            autoFocus: false,
            minLength: 1,
            select: function(event, ui)	{
                event.preventDefault();
                var index = $(this).attr('id').replace('phone_', '').replace('zip_', '').replace('email_', '');
                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                $('#teetime_title_'+index).val(ui.item.name).removeClass('teetime_name');
                $('#email_'+index).val(ui.item.email).removeClass('teetime_email');
                $('#phone_'+index).val(ui.item.label);
                $('#person_id_'+index).val(ui.item.value);
            },
            focus: function(event, ui) {
                event.preventDefault();
            }

        });
        $( '#email_'+i ).autocomplete({
            source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
            delay: 200,
            autoFocus: false,
            minLength: 1,
            select: function(event, ui)	{
                event.preventDefault();
                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
                var index = $(this).attr('id').replace('phone_', '').replace('zip_', '').replace('email_', '');
                $('#teetime_title_'+index).val(ui.item.name).removeClass('teetime_name');
                $('#phone_'+index).val(ui.item.phone_number).removeClass('teetime_phone');
                $('#email_'+index).val(ui.item.label);
                $('#person_id_'+index).val(ui.item.value);
            },
            focus: function(event, ui) {
                event.preventDefault();
            }

        });
    }
}
$.ui.autocomplete.prototype.options.autoSelect = true;
$(document).ready(function(){
    <?php if ($teetime_info->type == 'shotgun') { ?>
        shotgun.initialize();
    <?php } ?>
    console.log('Begin document ready');
    console.log(new Date().getTime());


    $('.players_buttonset input').change(function(){
		$('#number_players').val( $(this).val() );
	});
	
	$('.carts_buttonset input').change(function(){
		$('#number_carts').val( $(this).val() );
	});
	
	$('#number_players').blur(function(){
		var number = $(this).val();
		if(number <= 4){
			$('#players_'+number+'_label').trigger('click');	
		}else{
			$('.players_buttonset input').attr('checked', null);
			$('.players_buttonset label').removeClass('ui-state-active');
		}
	});
	
	$('#number_carts').blur(function(){
		var number = $(this).val();
		if(number <= 4){
			$('#carts_'+number+'_label').trigger('click');	
		}else{
			$('.carts_buttonset input').attr('checked', null);
			$('.carts_buttonset label').removeClass('ui-state-active');
		}
	});	
	
	$('#add_teetime_credit_card').click(function(e){
		e.preventDefault();
		var person_id = $("#person_id").val();
		$.colorbox2({href:'index.php/teesheets/open_add_credit_card_window/<?=$teetime_info->TTID?>/'+person_id, width:600})
	});
	$('#charge_for_tee_time').click(function (e){
		var card_to_charge = $('#credit_card_id').val();
		if (card_to_charge == 0)
		{
			alert('Please select a card to charge');
			return;
		}
		$.colorbox2({href:'index.php/teesheets/view_charge_card/<?=$teetime_info->TTID?>', width:500, onComplete:function(){
			$('#credit_card_label').html($('#credit_card_id option:selected').text());
			$('#card_to_charge').val(card_to_charge);
			$('#charge_person_id').val($('#person_id').val());
			$('#tee_time_id').val('<?=$teetime_info->TTID?>');
		}});
	});
	$('#teed_off_time').timepicker({'timeFormat':'h:mm tt', 'hour':'<?php echo date('h', strtotime($teetime_info->teed_off_time))?>', 'minute':'<?php echo date('i', strtotime($teetime_info->teed_off_time))?>'});
	$('#turn_time').timepicker({'timeFormat':'h:mm tt', 'hour':'<?php echo date('h', strtotime($teetime_info->turn_time))?>', 'minute':'<?php echo date('i', strtotime($teetime_info->turn_time))?>'});
	$('#remove_teed_off_time').click(function(){
		$.ajax({
	       type: "POST",
	       url: "index.php/teesheets/zero_teed_off/<?=$teetime_info->TTID?>",
	       data: "",
	       success: function(response){
		       Calendar_actions.update_teesheet(response.teetimes);
	           $('#remove_teed_off_time').remove();
	           $('#teed_off_label').remove();
	           $('#teed_off_time').remove();
		   },
		   dataType:'json'
	     });
	});
	$('#remove_turn_time').click(function(){
		$.ajax({
	       type: "POST",
	       url: "index.php/teesheets/zero_turn/<?=$teetime_info->TTID?>",
	       data: "",
	       success: function(response){
	           Calendar_actions.update_teesheet(response.teetimes);
           	   $('#remove_turn_time').remove();
	           $('#turn_label').remove();
	           $('#turn_time').remove();
		   },
		   dataType:'json'
	     });
	});
	$("#phone").mask2("(999) 999-9999");

	var submitting = false;
	$('#teetime_form').bind('keypress', function(e) {
    	var code = (e.keyCode ? e.keyCode : e.which);
		 if(code == 13) { //Enter keycode
             //Do something
             e.preventDefault();
             $('#teetime_save').click();
		 }
    });
    $('#teetime_form').validate({
		submitHandler:function(form)
		{
			var type = $('input[name=event_type]:checked').val();
			var proceed_with_save = true;
			if ($('#delete_teetime').val() == 1)
			{
				proceed_with_save = confirm('Are you sure you want to delete this event?');
			}
			else if ((type == 'teetime' && $('#teetime_title').val() == '') ||
				    ((type == 'tournament' || type == 'league' || type == 'event') && ($('#event_title').val() == '')) ||
				     (type == 'closed' && $('#closed_title').val() == '') ||
					(type == 'shotgun' && $('#shotgun_title').val() == '')
				     )
				 {
				     alert('Please enter a name/title or person name for this event');
				     proceed_with_save = false;
				 }
			if (proceed_with_save)
			{
				if (submitting) return;
				submitting = true;
				$(form).mask('<?php echo lang('common_wait'); ?>');

				$(form).ajaxSubmit({
					success:function(response)
					{
						var overlap = true;
						if (!response.second_time_available && $("#teetime_save").text() == "Reserve")
						 overlap = confirm('This reservation will re-round on an existing reservation or event. Do you want to continue booking it?');
						if(overlap){
							Calendar_actions.update_teesheet(response.teetimes);
			                submitting = false;
			                post_teetime_form_submit(response);
			                if (response.email_confirmation != ''){
			                	$.each(response.email_confirmation, function( index, value ) {
			                		Calendar_actions.send_confirmation(value);
			                	});
			                }else{
			                	Calendar_actions.send_confirmation(response.send_confirmation);
			                }
						}								

		                if (response.go_to_register){
		                	<?php if($this->session->userdata('sales_v2') == '1'){ ?>
                                <?php if ($this->session->userdata('tee_sheet_speed_up')) { ?>
                                    window.parent.App.data.cart.fetch({success: function () {parent.window.location = './home#sales'; $.colorbox.close();}});
                                <?php } else { ?>
                                    parent.window.location = './index.php/v2/home#sales';
                                $.colorbox.close();
                                <?php } ?>
							<?php }else{ ?>
		                	window.location = './index.php/sales';
                            $.colorbox.close();
                            <?php } ?>
						}
                        else {
                            $.colorbox.close();
                        }
		            	currently_editing = '';
		            },
					dataType:'json'
				});
			}else{
				$('#delete_teetime').val(0) ;
			}
		},
		errorLabelContainer: '#error_message_box',
 		wrapper: 'li'
	});

	$('#teetime_name').focus().select();
	$('#expand_players').click(function(e){
    	var checked = $(e.target).attr('checked');
    	if (checked)
    	{
	    	$('#teetime_people_table').show();
	    }
    	else
    	{
	    	$('#teetime_people_table').hide();
	    }
    	//var x = $('#teetime_form').height();
	    //var y = $('#teetime_form').width();
        initialize_additional_players();
	    $.colorbox.resize();
    });
//    for (var q = 1; q <= 5; q++) {
//		// Save customer event listeners
//	    $('#save_customer_'+q).click(function() {
//	    	if ($(this).attr('checked'))
//	        	$(this).prev().addClass('checked');
//	        else
//	   	    	$(this).prev().removeClass('checked');
//	    });
//	}
//    for (var i = 1; i < 6; i++) {
//        $('#purchase_'+i).click(function () {$('#purchase_quantity').val(i);});
//        $('#checkin_'+i).click(function () {$('#checkin').val(i);});
//    }
//    $('#purchase_multi').click(function () {$('#purchase_quantity').val( $('#purchase_multi_number').val() );});


       $('#purchase_1').click(function () {$('#purchase_quantity').val(1);});
       $('#purchase_2').click(function () {$('#purchase_quantity').val(2);});
       $('#purchase_3').click(function () {$('#purchase_quantity').val(3);});
       $('#purchase_4').click(function () {$('#purchase_quantity').val(4);});
       $('#purchase_5').click(function () {$('#purchase_quantity').val(5);});
       $('#purchase_multi').click(function () {$('#purchase_quantity').val( $('#purchase_multi_number').val() );});
       $('#checkin_1').click(function () {$('#checkin').val(1);});
       $('#checkin_2').click(function () {$('#checkin').val(2);});
       $('#checkin_3').click(function () {$('#checkin').val(3);});
       $('#checkin_4').click(function () {$('#checkin').val(4);});
       $('#checkin_5').click(function () {$('#checkin').val(5);});

    $('#event_checkin').click(function () {
		var val = $('#checkin_txtbox').val();
		$('#purchase_quantity').val(val);
	});
	$("#checkin_txtbox").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    	}
    });
	$('#shotgun_checkin').click(function (e) {
		// Count checked boxes and send them through to pay for
		var checked_boxes = $('.person_select:checked');
		var checkin_val = $('#checkin_txtbox').val();
		var val = checked_boxes.length > checkin_val ? checked_boxes.length : checkin_val ;
		//console.log(checked_boxes);
		//var shotgun_slots_checked = [];
		for (var i = 0; i < checked_boxes.length; i++) {
			var slot_id = $(checked_boxes[i]).parent().parent().attr('id');
			$(checked_boxes[i]).val(slot_id.replace('shotgun_players_', ''));
		}
		//console.log('shotgun_slots_checked +++++++++++++++++++++++');
		//console.dir(shotgun_slots_checked);
		//e.preventDefault();
		// Populate the larger of the two numbers for purchase quantity
		$('#purchase_quantity').val(val);
	});
	$("#shotgun_checkin_txtbox").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    	}
    });
	$('#teetime_delete').click(function(){$('#delete_teetime').val(1);});
	$('#event_delete').click(function(){$('#delete_teetime').val(1);});
	$('#shotgun_delete').click(function(){$('#delete_teetime').val(1);});
	$('#closed_delete').click(function(){$('#delete_teetime').val(1);});
	// Button icons and styles
	$('#paid_carts').button();
	$('#paid_players').button();
	$('#teetime_save').button();
    $('#event_save').button();
    $('#closed_save').button();
    $('#shotgun_save').button();
    $('#shotgun_delete').button();
    $('#teetime_delete').button();
    $('#event_delete').button();
    $('#closed_delete').button();
    $('.purchase_button').parent().buttonset();
    $('#teetime_checkin').button();
    $('#teetime_holes_9').button();
    $('#event_holes_9').button();
    $('#shotgun_holes_9').button();
    $('#teetime_confirm').button();
    $('#teetime_cancel').button();
    $('.teebuttons').buttonset();
    $('.holes_buttonset').buttonset();
    $('#players_0').button();
    $('#carts_0').button();
    $('.players_buttonset').buttonset();
    $('.carts_buttonset').buttonset();
    $('#players_0_label').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#teetime_purchase').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#players_0_label').children().css('color','#e9f4fe');

    // Teetime name settings
	$( '#teetime_title' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/'); ?>',
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: true,
		minLength: 0,
		search: function( event, ui ) {
			$('#status_image').removeClass('status_red').removeClass('status_yellow').removeClass('status_green').removeClass('customer_status');
			var new_cust = "index.php/teesheets/view_customer/-1/width~1100";
			$('#customer_button').attr("href", new_cust);
		},
		select: function(event, ui) {
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			set_session(ui.item.status_flag, ui.item.comments);
			$('#customer_comment').html(ui.item.comments);
			if (ui.item.status_flag == 1) {
				$('#status_image').addClass('status_red').addClass('customer_status');	
			}
			else if (ui.item.status_flag == 2) {
				$('#status_image').addClass('status_yellow').addClass('customer_status');	
			}
			else if (ui.item.status_flag == 3) {
				$('#status_image').addClass('status_green').addClass('customer_status');	
			}
			$('#teetime_title').val(ui.item.label);
			$('#email').val(ui.item.email).removeClass('teetime_email');
			$('#phone').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id').val(ui.item.value);
			var cust_edit = "index.php/customers/view/"+ ui.item.value +"/width~1100";
			$('#customer_button').attr("href", cust_edit);
			$('#zip').val(ui.item.zip);
			$('#price_class_1').val(ui.item.price_class);
			update_credit_card_dropdown(ui.item.value, $('#added_credit_card_id').val());
		},
		focus: function(event, ui) {
			event.preventDefault();
			var card_swipe = $('#teetime_title').data('card_swipe');

			console.log('focused!!!');
			console.log('card_swipe '+card_swipe);

			if (card_swipe != undefined && card_swipe == 1)
			{
				var autocomplete = $( this ).data( "autocomplete" );

				console.log('autocomplete--------------');

			    if ( !autocomplete.options.autoSelect || autocomplete.selectedItem ) { return; }

			    autocomplete.widget().children( ".ui-menu-item:first" ).each(function() {
			        var item = $( this ).data( "item.autocomplete" );
			        autocomplete.selectedItem = item;
			    });
			    if ( autocomplete.selectedItem ) {
			        autocomplete._trigger( "select", event, { item: autocomplete.selectedItem } );
			    }
			    $(this).autocomplete('close');
			    $('#teetime_title').data('card_swipe', 0);
			}

			//$('#teetime_title').val(ui.item.label);
		}
	}).data( "autocomplete" )._renderItem = function( ul, item ) {
		if (item.phone_number == '() -') {
			return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>"+item.label+" <span class='search_phone_number'>" + item.email + "</span></a>" )
        	.appendTo( ul );
		}else{
			return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>"+item.label+" <span class='search_phone_number'>" + item.phone_number + "</span> <span class='search_email'>" + item.email + "</span></a>" )
        	.appendTo( ul );
		}
        
	};
    $('#teetime_title').swipeable({allow_enter:false, auto_complete:'#teetime_title', character_limit:20});

    $('#teetime_title').keyup(function(){
		var focus = $(this);
		if (focus.val() == '')
		{
			var index = focus.attr('id').replace('teetime_title_', '');
			index = (index == 'teetime_title' ? 1 :index);

			clear_player_data(index);
		}
	});

	$('#event_person').autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/last_name'); ?>',
		delay: 150,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
			//set_session(ui.item.status_flag, ui.item.comments);
			var data = {};
			data.name = ui.item.label;
			data.phone = ui.item.phone_number;
			data.email = ui.item.email;
			data.person_id = ui.item.value;

			// If person is already in list, do nothing
			if($('#event_person_'+data.person_id).length > 0){
				$('#event_person').val('');
				return false;
			}

			$.post('<?php echo site_url('teesheets/save_event_person/'.$teetime_info->TTID); ?>', {'person_id':data.person_id},
				function(response){

			},'json');

			var html = event_person_row(data);
			$('#event_people').append(html);
			$('h2 > span.count').text( parseInt($('h2 > span.count').text()) + 1);
			$.colorbox.resize();
			$('#event_person').val('');
		},
		focus: function(event, ui) {
			event.preventDefault();
		}
	});

	$('#event_people').delegate('span.delete', 'click', function(e){
		var button = $(this);
		var person_id = button.data('person-id');
		var row = button.parents('li');
		row.remove();
		$('h2 > span.count').text( $('h2 > span.count').text() - 1);

		$.post('<?php echo site_url('teesheets/delete_event_person/'.$teetime_info->TTID); ?>', {'person_id':person_id},
			function(response){

		},'json');

		e.preventDefault();
		return false;
	});

	$('#event_people').delegate('a.checkin', 'click', function(e){
		if($(this).hasClass('done')){
			return false;
		}
		var person_id = $(this).parents('li').find('input').val();
		$.post('<?php echo site_url('teesheets/event_checkin/'.$teetime_info->TTID); ?>', {'person_id':person_id},
			function(response){

		},'json');
		$(this).addClass('done').text('✔ Checked In');
		e.preventDefault();
	});

	$('#event_people').delegate('a.pay', 'click', function(e){
		if($(this).hasClass('done')){
			return false;
		}
		var data = {};
		data.person_id = $(this).parents('li').find('input').val();
		data.person_name = $(this).parents('li').find('span.name').text();
		data.price_class = $('#default_price_category').val();
		data.event_players = $('#event_players').val();
		data.purchase_quantity = 1;

		if($('#event_holes_9').attr('checked')){
			data.teetime_holes = 9;
		}else{
			data.teetime_holes = 18;
		}
		data.start = '<? echo $teetime_info->start; ?>';

		data.carts = 0;
		data.paid_carts = 0;
		if($('#default_cart_fee:checked').attr('checked')){
			data.carts = 1;
		}

		$.post('<?php echo site_url('teesheets/event_pay/'.$teetime_info->TTID); ?>', data, function(response){
			if(response.success){
				<?php if($this->session->userdata('sales_v2') == '1'){ ?>
                    <?php if ($this->session->userdata('tee_sheet_speed_up')) { ?>
                        window.parent.App.data.cart.fetch({success: function () {parent.window.location = './home#sales'; $.colorbox.close();}});
                    <?php } else { ?>
                        parent.window.location = './index.php/v2/home#sales';
                        $.colorbox.close();
                    <?php } ?>
				<?php }else{ ?>	
				parent.window.location = './index.php/sales';
				<?php } ?>
			}
		},'json');
		e.preventDefault();
	});

	// Phone number events
 	$('#phone, #zip, #email').keypress(function (e) {
    	$('#save_customer_1').attr('checked', true);
    	$('#save_customer_1').prev().addClass('checked');
    });
    $( '#phone' ).autocomplete({
        source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
        delay: 200,
        autoFocus: false,
        minLength: 1,
        select: function(event, ui)	{
            event.preventDefault();
            customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
            $('#teetime_title').val(ui.item.name).removeClass('teetime_name');
            $('#email').val(ui.item.email).removeClass('teetime_email');
            $('#phone').val(ui.item.label);
            $('#person_id').val(ui.item.value);
        },
        focus: function(event, ui) {
            event.preventDefault();
        }

    });
    // Email
    $( '#email' ).autocomplete({
        source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
        delay: 200,
        autoFocus: false,
        minLength: 1,
        select: function(event, ui)	{
            event.preventDefault();
            customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
            $('#teetime_title').val(ui.item.name).removeClass('teetime_name');
            $('#phone').val(ui.item.phone_number).removeClass('teetime_phone');
            $('#email').val(ui.item.label);
            $('#person_id').val(ui.item.value);
        },
        focus: function(event, ui) {
            event.preventDefault();
            //$('#email').val(ui.item.label);
        }

    });


	// CODE BELOW REQUIRES REVIEW

    $('#type_teetime').click(function(){
    	$('#teetime_table').show();
    	$('#closed_table').hide();
    	$('#event_table').hide();
    	$('#shotgun_table').hide();
    	$.colorbox.resize();
    	focus_on_title('teetime_title');
    });
    $('#type_tournament').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$('#shotgun_table').hide();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_league').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$('#shotgun_table').hide();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_event').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$('#shotgun_table').hide();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_closed').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').show();
    	$('#event_table').hide();
    	$('#shotgun_table').hide();
    	$.colorbox.resize();
    	focus_on_title('closed_title');
    });
    $('#type_shotgun').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').hide();
    	$('#shotgun_table').show();
    	$.colorbox.resize();
        shotgun.initialize();
        focus_on_title('shotgun_title');
    });

	$('.teetime_name').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_name');
	});
    $('.teetime_phone').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_phone');
	});
	$('.teetime_email').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_email');
	});
    $('.teetime_details').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_details');
	});
    $('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});

    console.log('End document ready');
    console.log(new Date().getTime());

});

$('.colbox').colorbox2();

function post_person_form_submit(response)
{
	$('#teetime_title', window.parent.document).val($('#last_name').val()+","+$('#first_name').val());
	$("[name='email']", window.parent.document).val($("[name='email']").val());
	$('#phone', window.parent.document).val($('#phone_number').val());
	$("[name='zip']", window.parent.document).val($("[name='zip']").val());
	$('#price_class_1', window.parent.document).val($('#price_class').val()); 	
	var message = response.message;
	if (message.indexOf('added') != -1) {
		$("#teetime_title").autocomplete( "search", $('#teetime_title').val() );
	}
	$.colorbox2.close();
}
</script>
<?php //print_r($times); ?>
<script>
    console.log('Page loaded');
    console.log(new Date().getTime());
    </script>
