<div class="event_info">
	<input class='' id='shotgun_title' name='shotgun_title' placeholder='Title' value='<?php echo !empty($teetime_info) ? $teetime_info->title : ''; ?>' style='width:160px, margin-right:5px;'/>
	<span class='flag_icon'></span>
 	<span class='holes_buttonset'>
        <input type='radio' id='shotgun_holes_9' name='shotgun_holes' value='9' <?php echo !empty($teetime_info) && (($teesheet_holes == '9' && $teetime_info->holes != '18') || $teetime_info->holes == '9')?'checked':''; ?>/>
        <label for='shotgun_holes_9' id='shotgun_holes_9_label'>9</label>
        <input type='radio' id='shotgun_holes_18' name='shotgun_holes' value='18' <?php echo !empty($teetime_info) && (($teesheet_holes == '18' && $teetime_info->holes != '9') || $teetime_info->holes == '18')?'checked':''; ?>/>
        <label for='shotgun_holes_18' id='shotgun_holes_18_label'>18</label>
    </span>
 	<div class='player_buttons'>
        <span class='players_icon'></span>
        <div class='players'>
        	<span class='players_buttonset'>
    			<input class='' id='shotgun_player_search' name='shotgun_player_search' placeholder='Add players...' value=''/>
                <div id="shotgun_person_import"></div>
    		</span>
		</div>
		<?php echo form_dropdown('sg_default_price_category', !empty($price_classes) ? $price_classes : array(), !empty($teetime_info) ? $teetime_info->default_price_category : '', 'style="float: left; margin-top:4px;"'); ?>
        <input style='margin-left:15px;' name="sg_use_event_rate" id="sg_use_event_rate" value="1" type="checkbox" <?php if(!empty($teetime_info) && $teetime_info->use_event_rate > 0){ echo 'checked="checked"'; } ?> /> Use Event Rate
        <span class='carts_icon'></span>
        <div class='carts'>
        	<span class='carts_buttonset2'>
				<!-- <input class='teetime_carts' id='shotgun_carts' name='shotgun_carts' value='<?php echo !empty($teetime_info) ? $teetime_info->carts : 0; ?>'/> -->
				<label for="event_cart_fee" style="font-weight: normal; float: right; margin-right: 15px;">
					<!--input name="sg_default_cart_fee" type="hidden" value="0" /-->
					<input style='margin-top:9px;' name="sg_default_cart_fee" id="default_cart_fee" value="1" type="checkbox" <?php if(!empty($teetime_info) && $teetime_info->default_cart_fee > 0){ echo 'checked="checked"'; } ?> />
				</label>
			</span>
		</div>
   	</div>
    <div class="cart_sign_options">
        <a href="#" id="view_cart_sign_options">Cart Signs</a>
    </div>
   	<div class='clear'></div>
</div>
<div class='popup_divider'></div>
<table style="margin: 0px 0px 15px 15px;" id="shotgun_grid">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<?php for($player = 1; $player <= 8; $player++){ ?>
			<th>Player <?php echo $player; ?></th>
			<?php if ($player == 4) { ?>
				<td class="hole"></td>
			<?php } ?>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		<?php for($hole = 1; $hole <= 18; $hole++){ ?>
		<tr class="<?= ($hole > 9 ? "eighteen_only" : "");?>">
			<td class="hole"><?php echo $hole; ?>A</td>
			<?php for($player = 1; $player <= 8; $player++){ ?>
			<td  id="shotgun_players_<?php echo $hole; ?>_<?php echo $player; ?>" class="<?= ($player < 5 ? "primary" : "secondary");?> <?= (isset($event_people[$hole]) && isset($event_people[$hole][$player]) && $event_people[$hole][$player]['checked_in'] == 1 ? "checked_in" : "");?> <?= (isset($event_people[$hole]) && isset($event_people[$hole][$player]) && $event_people[$hole][$player]['paid'] == 1 ? "paid" : "");?>">
				<?php $slot_filled = isset($event_people[$hole]) && isset($event_people[$hole][$player]) && $event_people[$hole][$player]['label'] != '' ? true : false; ?>
				<input type="text" class="shotgun-person" value="" placeholder="Last, First name"  id="shotgun_players_<?php echo $hole; ?>_<?php echo $player; ?>_name" <?=$slot_filled ? "style='display:none;'" : '' ?>/>
				<?php if ($slot_filled) { ?>
				<span class="person" title="<?= isset($event_people[$hole]) && isset($event_people[$hole][$player]) ? $event_people[$hole][$player]['last_name'] : '';?>, <?= isset($event_people[$hole]) && isset($event_people[$hole][$player]) ? $event_people[$hole][$player]['first_name'] : '';?>">
					<input type="checkbox" value="1" name="checkin_box[]" class="person_select toggle" />
					<span class='person_label'><?=isset($event_people[$hole]) && isset($event_people[$hole][$player]) ? $event_people[$hole][$player]['label'] : '';?></span>
				</span>
				<?php } ?>
				<input type="hidden" name="shotgun_players[<?php echo $hole; ?>][<?php echo $player; ?>][person_id]" value="<?=isset($event_people[$hole]) && isset($event_people[$hole][$player]) ? $event_people[$hole][$player]['person_id'] : '';?>" id="shotgun_players_<?php echo $hole; ?>_<?php echo $player; ?>_person_id" />
			</td>
			<?php if ($player == 4) { ?>
				<td class="hole"><?php echo $hole; ?>B</td>
			<?php } ?>
			<?php } ?>
		<tr>
		<?php } ?>
	</tbody>
</table>
<div>
	<textarea tabindex=4 class='details_box' placeholder='Details' id='shotgun_details' name='shotgun_details' style='width:94%;height:50px;'><?php echo !empty($teetime_info->details) ? $teetime_info->details : ''; ?></textarea>
</div>
<div class='event_buttons_holder'>
	<span class='saveButtons'>
		<button id='shotgun_save'>Save</button>
	</span>
	<button id='shotgun_delete' class='deletebutton'>Delete Event</button>
	<span class='saveButtons'>
	   	<button id='shotgun_checkin' class='purchase_button'>Pay</button>
	</span>
	<input type="text" id="checkin_txtbox" placeholder="CheckIn" name="checkin_txtbox" value="1" maxlength="3">
    <div class='clear'></div> 
</div>
<div class='contextMenu' id='shotgunPlayerMenu' style='display:none'>
	<ul>
	    <li id="check_in">Check In</li>
	    <li id="mark_paid">Mark Paid</li>
	    <li id="delete">Remove</li>
	</ul>
</div>
<style>
	#shotgun_grid td input {
		width:120px;
	}
	.saveButtons input, .saveButtons button {
		float:left;
	}
	#shotgun_grid td input.person_select {
		width:auto;
		border:none;
	}
	#shotgun_grid td.hole {
		
	}
	table#shotgun_grid tr td span.person {
		color:rgb(94, 94, 94);
		border:none;
		font-size:12px;
	}
	table#shotgun_grid tr td span.person .delete{
		color:rgb(215, 0, 0);
		border:none;
	}
    #teetime_form #shotgun_table div.player_buttons  {
		width:650px;
	}
	#shotgun_grid tr td.checked_in {
		background-color:#CDD7E4;
	}
	#shotgun_grid tr td.paid {
		background-color:#D8EDD2;
	}
    .cart_sign_options {
        float: right;
    }
    .cart_sign_options a#view_cart_sign_options {
        background: #4f90d2;
        margin-right: 10px;
        font-style: normal;
        vertical-align: middle;
        padding: 5px 14px 5px 14px;
        border-radius: 10px;
        color: #ffffff;
        border: 1px solid #023a73;
        cursor: pointer;
        box-shadow: inset -2px -2px 5px -3px black;
    }
    #shotgun_person_import, #event_person_import {
        width:30px;
        height:30px;
        display: inline-block;
        background:url(../images/teesheet/excel.png) #bfbfbf no-repeat 3px 2px;
        cursor:pointer;
        border:1px solid #777;
        border-radius:5px;
        float:right;
        margin-right:10px;
    }
    #event_person_import {
        float:left;
    }
    #colorbox input#shotgun_player_search {
        margin-right:0px;
    }
</style>
