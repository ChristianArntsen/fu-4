<ul id="error_message_box"></ul>
<?php
echo form_open('food_and_beverage/process_payment');
?>
<fieldset id="card_info">
<legend><?php echo lang("sales_swipe_card"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('sales_card_number').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'card_number',
		'id'=>'card_number',
		'value'=>'')
	);?>
	</div>
</div>
</fieldset>
<?php
echo form_close();
?>