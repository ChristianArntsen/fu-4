<style>
#layout_editor {
	width: auto !important;
}

#new-layout {
	padding: 15px;
	width: 500px;
}

#new-layout .fnb_button {
	line-height: 30px !important;
	padding: 0px 5px 0px 5px !important;
	height: 30px !important;
	display: block;
	margin-top: 5px !important;
}

#new-layout label {
	margin-bottom: 2px;
}

#new-layout input {
    border: 1px solid #B1B1B1;
    border-radius: 5px;
    box-shadow: 0 6px 24px -12px #000000 inset;
    color: #666464;
    font-family: "Quicksand",Helvetica,sans-serif;
    font-size: 14px;
    font-weight: lighter;
    line-height: 20px;
    padding: 4px;
    width: 175px;
    display: block;
}

#layout_editor a.delete-layout {
	float: right;
	color: red;
	display: block;
	padding: 5px;
}
</style>
<script>
counter = 0;
function save_object(layout_id, x, y, type, object){

	var data = {
		layout_id: layout_id,
		pos_x: x,
		pos_y: y,
		type: type
	};

	$.post('<?php echo site_url('food_and_beverage/save_layout_object'); ?>', data,
		function(response){
			object.attr('data-object-id', response.object_id);
	},'json');
}

function save_object_pos(object_id, x, y){

	var data = {
		object_id: object_id,
		pos_x: x,
		pos_y: y
	};

	$.post('<?php echo site_url('food_and_beverage/save_layout_object'); ?>', data,
		function(response){

	},'json');
}

function save_object_dimensions(object_id, w, h){

	var data = {
		object_id: object_id,
		width: w,
		height: h
	};

	$.post('<?php echo site_url('food_and_beverage/save_layout_object'); ?>', data,
		function(response){

	},'json');
}

function initLayoutDrag(obj){

	$(obj).draggable({
		containment: 'parent',
		stop: function(e, ui){
			var obj = $(ui.helper);
			var pos = obj.position();
			var object_id = obj.attr('data-object-id');

			save_object_pos(object_id, pos.left, pos.top);
		}
	});

	$(obj).resizable({
		stop: function(e, ui){
			var object = $(ui.element);
			var object_id = object.attr('data-object-id');

			var width = ui.size.width;
			var height = ui.size.height;

			save_object_dimensions(object_id, width, height);
		}
	});
}

function initLayoutDrop(){
    $("div.floor_layout").droppable({
        drop: function (ev, ui) {
			if($(ui.draggable).hasClass('drag')){
                var newObject = $(ui.helper).clone();

				var pos = $(ui.helper).position();
				var type = newObject.attr('data-type');
				var left = pos.left;
				var top = pos.top + 25;

				var layoutId = $('div.floor_layout.active').attr('data-layout-id');

				newObject.css({
					"left": left,
					"top": top,
					"position": "absolute"
				});

                $(this).append(newObject);
				save_object(layoutId, left, top, type, newObject);

				newObject.removeClass("drag");
				initLayoutDrag(newObject);
			}
        }
    });
}

function save_table_number(){
	var field = $('input.object-label');
	var newLabel = field.val();
	var oldLabel = field.attr('data-orig');
	var objectId = field.parent('div').attr('data-object-id');
	var object = field.parent('div');

	if(newLabel != oldLabel){
		$.post('<?php echo site_url('food_and_beverage/save_layout_object'); ?>', {object_id:objectId, label:newLabel}, function(response){
			if(response.success){
				set_feedback('Changes saved successfully', 'success_message');
			}else{
				set_feedback(response.msg, 'error_message', false, 3000);
				object.find('span.label').text(oldLabel);				
			}
		},'json');
	}

	field.replaceWith('<span class="label">' + newLabel + '</span>');
}

function initLayoutEditor(){
	$.colorbox.resize({'width': 1172});
	$('body').off('click', 'div.object');

	$('div.floor_layout').find('div.object').draggable({
		stop: function(e, ui){
			var obj = $(ui.helper);
			var pos = obj.position();
			var object_id = obj.attr('data-object-id');

			save_object_pos(object_id, pos.left, pos.top);
		}
	});

	$('div.palette > div.drag').draggable({
        helper: 'clone',
        containment: 'layout_editor'
	});

	initLayoutDrop();

	$(document).bind('click', function(e){
		if(!$(e.target).hasClass('object-label') && $('input.object-label').length > 0){
			save_table_number();
		}
	});

    $('div.floor_layout > div.object').die('dblclick').live('dblclick', function(e){
		var table = $(this);
		var label = table.find('span.label').text();
		var input = $('<input name="label" data-orig="'+label+'" class="object-label" />');
		input.val(label);
		table.find('span.label').replaceWith(input);
		input.focus();

		$(table).on('keypress', function(e){
			if(e.key == "Enter" || e.keyCode == 13){
				save_table_number();
				return false;
			}
		});
		return false;
	});

	$('div.floor_layout > div.object').resizable({
		stop: function(e, ui){
			var object = $(ui.element);
			var object_id = object.attr('data-object-id');

			var width = ui.size.width;
			var height = ui.size.height;

			save_object_dimensions(object_id, width, height);
		}
	});

	$('#layout_editor ul.tabs').off('click', 'li.tab > a').on('click', 'li.tab > a', function(e){
		$(this).parents('ul.tabs').find('li').removeClass('active');
		$(this).parent('li').addClass('active');
		var target = $(this).attr('href');
		$(target).siblings('div').removeClass('active').hide();
		$(target).addClass('active').show();
		return false;
	});

	$('div.object a.delete-object').off('click').on('click', function(e){
		var object = $(this).parent('div.object');
		var object_id = object.attr('data-object-id');

		$.post('<?php echo site_url('food_and_beverage/delete_layout_object'); ?>', {object_id:object_id}, function(response){
			if(response.success){
				object.remove();
			}
		},'json');

		return false;
	});

	$('a.new-layout').colorbox2({
		html: $('#new-layout-html').html()
	});

	$('body').off('submit', '#add-new-layout').on('submit', '#add-new-layout', function(e){
		var params = {};
		params.name = $(this).find('input[name="name"]').val();
		params.layout_id = $(this).find('input[name="layout_id"]').val();

		var newLayout = false;
		if(!params.layout_id){
			newLayout = true;
		}

		if(!params.name){
			set_feedback('Layout name is required', 'error_message');
			return false;
		}

		$.post('<?php echo site_url('food_and_beverage/save_layout'); ?>', params, function(response){
			if(response.success){

				// If adding new layout
				if(newLayout){
					$('#layout_editor ul.tabs li.new').before('<li class="tab"><a class="tab" id="tab_'+response.layout_id+'" href="#layout_'+response.layout_id+'">'+params.name+'</a></li>');
					$('#layout_editor div.tab_content').append('<div id="layout_'+response.layout_id+'" class="floor_layout" data-layout-id="'+response.layout_id+'"><a class="delete-layout" href="#">Delete Layout</a></div>');
					initLayoutDrop();
					$('div.palette').show();
					$('div.tab_content').find('h1').remove();
					$('ul.tabs li.tab:last-child').trigger('click');
					$.colorbox2.close();

				// If just editing name of existing layout
				}else{
					$('#tab_'+params.layout_id).html(params.name);
					$.colorbox2.close();
				}
			}
		},'json');

		return false;
	});

	$('#layout_editor').off('click', 'a.delete-layout').on('click', 'a.delete-layout', function(e){
		var layout_id = $(this).parent('div.floor_layout').attr('data-layout-id');

		if(confirm('Are you sure you want to delete this layout?')){

			$.post('<?php echo site_url('food_and_beverage/delete_layout'); ?>', {layout_id:layout_id}, function(response){
				if(response.success){
					$('ul.tabs li.tab.active').remove();
					$('#layout_'+response.layout_id).remove();
					$('ul.tabs li.tab:last-child').trigger('click');

					if($('ul.tabs li.tab').length == 0){
						$('div.palette').hide();
					}
				}
			},'json');
		}
		return false;
	});

	// Close layout editor and re-open table layout window
	$('#save-layout-changes').off('click').on('click', function(e){
		$(document).one('cbox_closed', function(e){
			reload_tables(function(){
				fnb.show_tables();
				set_feedback("Layout saved successfully", "success_message");
			});
		});
		$.colorbox.close();

		return false;
	});

	$('ul.tabs').on('click', 'a.edit', function(e){
		var name = $(this).data('name');
		var layout_id = $(this).data('layout-id');

		$.colorbox2({
			title: 'Edit Layout',
			html: $('#new-layout-html').html(),
			onComplete: function(ui){
				$('#cbox2LoadedContent').find('input[name="name"]').val(name);
				$('#cbox2LoadedContent').find('input[name="layout_id"]').val(layout_id);
			}
		});
		e.preventDefault();
	});
}
</script>
<div style="display: none;" id="new-layout-html">
	<div id="new-layout">
		<form id="add-new-layout" action="post">
			<label>Layout Name</label>
			<input type="text" name="name" value="" />
			<input type="hidden" name="layout_id" value="" />
			<input type="submit" name="submit" class="fnb_button" value="Save" />
		</form>
	</div>
</div>
<div id="layout_editor">
	<ul class="tabs">
		<?php
		if(!empty($layouts)){
		$showPalette = '';
		$active = 'active';
		foreach($layouts as $layout){ ?>
		<li class="tab <?php echo $active; ?>">
			<a id="tab_<?php echo $layout['layout_id']; ?>" class="tab" href="#layout_<?php echo $layout['layout_id']; ?>">
				<?php echo $layout['name']; ?>
			</a>
			<a href="" data-layout-id="<?php echo $layout['layout_id']; ?>" data-name="<?php echo $layout['name']; ?>" class="edit">Edit</a>
		</li>
		<?php $active = ''; } } ?>
		<li class="new">
			<a href="#new-layout" title="New Layout" class="new-layout button new">+ New</a>
		</li>
		<li style="float: right;">
			<a href="#" id="save-layout-changes" title="Save Layout" style="float: right; margin: 15px;" class="button">Save</a>
		</li>
	</ul>

	<div class="tab_content">
	<?php if(!empty($layouts)){ ?>
	<?php
	$active = 'active';
	foreach($layouts as $layout){ ?>
		<div id="layout_<?php echo $layout['layout_id']; ?>" class="floor_layout <?php echo $active; ?>" data-layout-id="<?php echo $layout['layout_id']; ?>">
			<a class="delete-layout" href="#">Delete Layout</a>
		<?php foreach($layout['objects'] as $object){
		$objectSize = '';
		if(!empty($object['width'])){
			$objectSize .= 'width: '.$object['width'].'px; ';
		}
		if(!empty($object['height'])){
			$objectSize .= 'height: '.$object['height'].'px; ';
		}
		?>
			<div class="object" data-type="<?php echo $object['type']; ?>" data-object-id="<?php echo $object['object_id']; ?>" style="position: absolute; left: <?php echo $object['pos_x']; ?>px; top: <?php echo $object['pos_y']; ?>px; <?php echo $objectSize; ?>">
				<?php if($object['type'] == 'box'){ ?>
				<div class="box"></div>
				<?php }else{ ?>
				<?php include('images/restaurant_layout/'.$object['type'].'.svg'); ?>
				<?php } ?>
				<span class="label"><?php echo htmlspecialchars($object['label'], ENT_QUOTES); ?></span>
				<a href="#" class="delete-object">X</a>
			</div>
		<?php } ?>
		</div>
	<?php $active = ''; } }else{ ?>
	<h1 style="color: #AAA; text-align: center; margin-top: 25px;">Click "New" above to create a new layout</h1>
	<?php
	$showPalette = 'display: none;';
	} ?>
	</div>
	<div class="palette" style="<?php echo $showPalette; ?>">
		<div class="object drag" data-type="booth">
			<?php include('images/restaurant_layout/booth.svg'); ?>
			<span class="label"></span><a href="#" class="delete-object">X</a>
		</div>
		<div class="object drag" data-type="table_square_4">
			<?php include('images/restaurant_layout/table_square_4.svg'); ?>
			<span class="label"></span><a href="#" class="delete-object">X</a>
		</div>
		<div class="object drag" data-type="table_round_4">
			<?php include('images/restaurant_layout/table_round_4.svg'); ?>
			<span class="label"></span><a href="#" class="delete-object">X</a>
		</div>
		<div class="object drag misc" data-type="box">
			<div class="box"></div>
			<span class="label"></span><a href="#" class="delete-object">X</a>
		</div>
	</div>
</div>
