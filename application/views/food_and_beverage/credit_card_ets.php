<style type="text/css">
.etsFormGroup { margin: 10px 0; }
#ets_img { margin:40px auto; }

#ETSIFrame {
	width: 600px;
	display: block;
}

.etsButton {
	background: #349ac5 !important;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3');
	background: -webkit-linear-gradient(top, #349ac5, #4173b3);
	background: -moz-linear-gradient(top, #349ac5, #4173b3);
	color: white;
	display: block;
	color: white !important;
	border: 1px solid #232323 !important;
	display: block;
	font-size: 14px;
	font-weight: normal;
	height: 22px;
	padding: 5px 0 0 10px;
	text-align: center;
	text-shadow: 0px -1px 0px black !important;
	border-radius: 4px;
	margin-bottom: 8px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
}
</style>
<script>
var interval_id = '';
var count = 0;
$(document).ready(function(){
	interval_id = setInterval(add_ets_handlers, 200);
});

function add_ets_handlers()
{
	if ($('#ETSIFrame').length > 0)
	{
		$('#ETSIFrame').one('load', function(e){
			$('#cbox2LoadedContent').unmask();
			$.colorbox2.resize();
		});

		ETSPayment.addResponseHandler("success", function(e){
			
			if(e.status != 'success'){
				set_feedback('Card declined. '+e.message, 'error_message');
				return false;
			}
			
			e.type = "creditcard-ets";
			e.amount = e.transactions.amount;
			e.action = '<?php echo $action; ?>';
			
			if(e.transactions.type == 'gift card'){
				e.type = 'giftcard-ets';
			}
			
			// If refunding gift card, delete gift card payment
			var receipt = App.receipts.get(<?php echo $receipt_id; ?>);
			if(e.action == 'refund' && e.type == 'giftcard-ets'){
				receipt.get('payments').get('<?php echo $payment_type; ?>').destroy();
				set_feedback('Gift card refunded', 'success_message');
				$.colorbox2.close();
				return false;
			}			

			$.ajax({
				type: "POST",
				url: App.api_table + 'receipts/<?php echo $receipt_id; ?>/payments',
				data: e,
				success: function(response){
					
					if(response.sale_id){
						App.receipt.sale_id = "POS " + response.sale_id;
					}
					var receipt = App.receipts.get(<?php echo $receipt_id; ?>)
					
					// Add payment to receipt				
					if(receipt.get('payments').get(response.type)){
						receipt.get('payments').get(response.type).set(response);
					}else{
						receipt.get('payments').add(response);
					}
					
					var customer_name = false;
					if(receipt.get('customer')){
						customer_name = receipt.get('customer').get('last_name') + ', '+ receipt.get('customer').get('first_name');
					}	

					// PRINT CREDIT CARD RECEIPT
					<?php if ($this->config->item('print_credit_card_receipt')) { ?>
						var receipt_data = '';
			 			var header_data = {
					    	course_name:'<?php echo $this->config->item('name')?>',
					    	address:'<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>',
					    	address_2:'<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>',
					    	phone:'<?php echo $this->config->item('phone')?>',
					    	employee_name:App.receipt.header.employee_name,
					    	customer:customer_name
					    };
					    var card_data = {
					    	card_type:response.card_type,
					    	masked_account:response.card_number,
					    	auth_code:response.auth_code,
					    	auth_amount:response.amount.toFixed(2),
					    	print_tip_line:'<?php echo $this->config->item('print_tip_line'); ?>',
					    	cardholder_name:'<?php echo $cardholder_name; ?>',
					    	print_two_signature_slips:'<?php echo $this->config->item('print_two_signature_slips');?>'
					    };
			 			
			 			receipt_data = webprnt.build_credit_card_slip(receipt_data, card_data, header_data);
			 			console.log('webprnt.print credit card receipt *************************************************');
			 			console.log(receipt_data);
			 			webprnt.print(receipt_data, window.location.protocol + "//" + "<?=$this->config->item('webprnt_ip')?>/StarWebPRNT/SendMessage");
					<?php } ?>
					if(receipt.isPaid()){
						receipt.printReceipt('sale');
						$.colorbox2.close();
					}

					// Close table (if everything is paid)
					App.closeTable();
				}
			});	
		});

		clearInterval(interval_id);
	}
	else if (count > 50)
	{
		clearInterval(interval_id);
	}
	count ++;
}
</script>
<div style="padding: 10px; overflow: hidden; display: block;">
	<div class="wrapper" id="ets_payment_window">
		<a class="fnb_button show_payment_buttons" style="height: 30px; line-height: 30px; display: block; float: none; padding: 10px;" href="#">Back</a>
		<h1 style="margin-top: 0px;">Total: $<?=$amount?></h1>

		<!-- HTML5 Magic -->
		<div id='ets_session_id' data-ets-key="<?php echo trim($session->id); ?>">
			<img id='ets_img' src="http://www.loadinfo.net/main/download?spinner=3875&disposition=inline" alt="">
		</div>
	</div>
</div>
<div id='ets_script_box'></div>
<script>
if (typeof ETSPayment == 'undefined')
{
	var e = document.createElement('script');
	e.src = "<?php echo ECOM_ROOT_URL ?>/init";
	$('head')[0].appendChild(e);
}
else
{
	ETSPayment.createIFrame();
}
</script>
