<style>
#comp_window {
	padding: 15px;
	width: 500px;
}

#comp_window h1.total {
	text-align: center;
	font-size: 3em;
	padding: 10px 0px;
	margin: 0px;
	display: block;
}

#comp_window div.row {
	padding: 15px 0px;
	display: block;
	overflow: hidden;
}

#comp_window div.row label {
	line-height: 40px;
	height: 40px;
	padding: 0px 0px 0px 10px;
	text-align: left;
	display: block;
	width: 130px;
	font-size: 1.75em;
	float: left;
	clear: none;
}

#comp_window a.fnb_button.type {
	font-size: 2em; 
	font-weight: bold;
}

#comp_window input.amount {
	width: 100px;
	padding: 10px;
}

#comp_window div.row a.fnb_button {
	float: left;
	display: block;
	font-size: 1.5em;
}

#comp_window h2 {
	font-size: 3em;
	text-align: center;
	display: block;
}

#comp_window .fnb_button.selected {
	box-shadow: 0px 0px 15px rgba(0, 0, 0, 1) inset;
}

#comp_window div.row.total {
	border-top: 1px solid #D0D0D0;
}

#comp_window div.reasons > a.reason {
	display: block;
	padding: 0px 10px 0px 10px;
	text-align: left;
	height: 40px;
	line-height: 40px;
	margin: 0px 5px 5px 0px;
	width: auto;
	font-size: 1.4em;
	float: none;
	width: 220px;
	float: left;
}

a.reason.selected {
	box-shadow: 0px 0px 15px rgba(0, 0, 0, 1) inset;
}
</style>
<script type="text/html" id="template_comp_window">
	<h1 class="total"><%-accounting.formatMoney(subtotal)%></h1>
	<div class="row">
		<label>Amount</label>
		<input type="text" class="amount" name="amount" value="<%-accounting.formatMoney(comp.amount, '')%>" />
		<span style="font-size: 22px">%</span>
	</div>
	<div class="row reasons">
		<% _.each(reasons, function(reason){ %>
		<a class="fnb_button reason<% if(comp.description && reason == comp.description){ print(' selected') } %>"><%-reason%></a>
		<% }); %>
	</div>
	<div class="row total">
		<h2><%-accounting.formatMoney(subtotal)%></h2>
	</div>
	<div class="row" style="padding-bottom: 0px;">
		<a class="fnb_button delete_button no-comp" style="height: 30px !important; line-height: 14px !important;">No Comp</a>
		<a style="float: right;" class="fnb_button save">Save</a>
	</div>
</script>
