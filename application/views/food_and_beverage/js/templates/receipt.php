<script type="text/html" id="template_receipt">
<% if(status == 'complete'){ var paidClass = ' paid'; var btnClass = ' disabled'; } else { var paidClass = ''; var btnClass = ' blah'; } %>
<div class="receipt<%=paidClass%>">
	<div class="title-header">
		<% if(receipt_id != 1){ %>
		<a class="fnb_button delete-receipt">&times;</a>
		<% } %>		
		<span class="number">#<%-receipt_id%></span>
		<% if(customer){ %>
			<% if(status == 'complete'){ %>
			<%-customer.get('first_name') %> <%-customer.get('last_name') %>
			<% }else{ %>
			<a class="fnb_button customer"><%-customer.get('first_name') %> <%-customer.get('last_name') %></a>
			<% } %>
		<% }else{ %>
			<% if(status != 'complete'){ %>
			<a class="fnb_button customer">- No Customer -</a>
			<% } %>
		<% } %>
	</div>
	<div class="title">
		<% if(status == 'complete'){ %>
		<span>Gratuity: <%-accounting.formatNumber(auto_gratuity, 2)%>%</span>	
		<% }else{ %>
		<span>Gratuity</span>
		<input class="fnb_gratuity" value="<%-auto_gratuity%>"/>%
		<% } %>
		<span class="is-paid">PAID</span>
		<% if(taxable == 1){ %>
		<a class="tax fnb_button" data-tax="1">&#10004; Tax</a>
		<% }else{ %>
		<a class="tax fnb_button" data-tax="0">&#x25a2; Tax</a>	
		<% } %>
	</div>
	<div class="footer">
		<a class="up">&#x25B2;</a>
		<a class="down">&#x25BC;</a>
		<span class="total">Total: <%-accounting.formatMoney(total)%></span>
	</div>	
</div>

<% if(items.length == 0){ var btnClass = ' disabled'; } %>
<a class="fnb_button print<%=btnClass%>" href="#">Print</a>
<a class="fnb_button pay<%=btnClass%>" href="#">Pay</a>
</script>

<script type="text/html" id="template_receipt_item">
<span class="seat">SEAT <%-seat%></span>
<% if(comp_total > 0){ %>
<span class="item-name">(<%-accounting.formatNumber(quantity)%>) <%-_.shorten(name, 12)%></span>
<span class="price" style='width: 90px;'><strong>COMP</strong> <%-accounting.formatMoney(split_subtotal + split_sides_subtotal)%></span>
<% }else{ %>
<span class="item-name">(<%-accounting.formatNumber(quantity)%>) <%-_.shorten(name, 18)%></span>
<span class="price"><%-accounting.formatMoney(split_subtotal + split_sides_subtotal)%></span>	
<% } %>

</script>
