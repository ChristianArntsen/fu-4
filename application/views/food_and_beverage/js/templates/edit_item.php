<style>
.modifiers .scroll-buttons {
	display: block;
	width: 200px;
	height: 37px;
	margin: 0 auto;
	float: none;
	overflow: hidden;
}

.modifiers button.up, .modifiers button.down {
	width: 96px !important;
	height: 35px !important;
	line-height: 35px !important;
	text-align: center;
	float: left;
	display: block;
	background: #d6d6d6;
	background: -moz-linear-gradient(top,  #d6d6d6 0%, #c9c9c9 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d6d6d6), color-stop(100%,#c9c9c9));
	background: -webkit-linear-gradient(top,  #d6d6d6 0%,#c9c9c9 100%);
	background: -o-linear-gradient(top,  #d6d6d6 0%,#c9c9c9 100%);
	background: -ms-linear-gradient(top,  #d6d6d6 0%,#c9c9c9 100%);
	background: linear-gradient(to bottom,  #d6d6d6 0%,#c9c9c9 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d6d6d6', endColorstr='#c9c9c9',GradientType=0 );
	color: #666;
	border: 1px solid #AAA;
	margin: 0px;
	cursor: pointer;	
}

#modifiers_form {
	margin: 20px;
	font-size: 18px;
}

#modifiers_form input {
	padding: 5px;
	font-size: 20px;
	width: 100px;
}

#modifiers_form label {
	display: block;
	float: left;
	line-height: 35px;
	height: 35px;
	margin-right: 15px;
}

#modifiers_form div.form_field {
	margin-bottom: 10px;
	display: block;
	float: left;
}

#sales_item_modifiers {
	display: block;
	margin: 10px 0px 0px 0px;
	padding: 0px;
	overflow: hidden;
	width: auto;
}

#sales_item_modifiers li {
	display: block;
	padding: 0px 0px 5px 0px;
	margin: 0px;
	list-style-type: none;
	font-size: 18px;
	overflow: hidden;
	width: auto;
}

#sales_item_modifiers span {
	float: left;
	display: block;
}

#sales_item_modifiers span.value {
	float: none;
	display: inline;
	height: 42px;
	line-height: 42px;
}

#sales_item_modifiers span.name {
	width: 180px;
	height: 42px;
	line-height: 42px;
}

#sales_item_modifiers span.options {
	float: right;
	display: block;
}

#sales_item_modifiers span.options a {
	float: left;
}

#sales_item_modifiers span.price {
	width: 65px;
	text-align: right;
}

div.sides {
	padding: 15px;
}

.option_button,
.delete_button,
.submit_button {
	background: #349ac5;
	background: -moz-linear-gradient(top,  #349ac5 0%, #4173b3 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#349ac5), color-stop(100%,#4173b3));
	background: -webkit-linear-gradient(top,  #349ac5 0%,#4173b3 100%);
	background: -o-linear-gradient(top,  #349ac5 0%,#4173b3 100%);
	background: -ms-linear-gradient(top,  #349ac5 0%,#4173b3 100%);
	background: linear-gradient(to bottom,  #349ac5 0%,#4173b3 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#349ac5', endColorstr='#4173b3',GradientType=0 );
    border: 1px solid #232323;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 1px 1px 0 rgba(255, 255, 255, 0.5) inset, 0 3px 1px -2px rgba(255, 255, 255, 0.2);
    color: white;
    display: block;
	float: left;
    font-weight: normal;
    height: 40px !important;
    line-height: 40px !important;
    margin: 0px;
    padding: 0 20px;
    text-align: center;
    text-shadow: 0 -1px 0 black;
}

#edit_item .option_button,
#edit_item .delete_button,
#edit_item .submit_button {
    font-size: 16px;
}

#edit-item a.selected {
	box-shadow: 0 0px 20px 5px black inset !important;
}

#modifiers_form .submit_button {
	float: right !important;
	margin: 20px 0px 0px 0px !important;
	display: block !important;
}

#item_sides li {
	display: block;
	float: none;
	overflow: hidden;
	width: 300px;
}

#item_sides li a.choose_side {
	line-height: 25px;
	height: 25px;
	padding: 10px 0px;
}

ul.breadcrumb {
	display: block;
	width: auto;
	padding: 0px;
	margin: 0px;
	float: none;
	overflow: hidden;
}

ul.breadcrumb > li {
	display: block;
	float: left;
	padding: 0px;
	margin: 0px;
	height: 50px;
	width: 20%;
	position: relative;
	border-bottom: 1px solid #444;
	background: #54b2e5;
	background: -moz-linear-gradient(top,  #54b2e5 0%, #2474b4 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#54b2e5), color-stop(100%,#2474b4));
	background: -webkit-linear-gradient(top,  #54b2e5 0%,#2474b4 100%);
	background: -o-linear-gradient(top,  #54b2e5 0%,#2474b4 100%);
	background: -ms-linear-gradient(top,  #54b2e5 0%,#2474b4 100%);
	background: linear-gradient(to bottom,  #54b2e5 0%,#2474b4 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#54b2e5', endColorstr='#2474b4',GradientType=0 );
}

ul.breadcrumb > li.incomplete {
	background: #d14d4d;
	background: -moz-linear-gradient(top,  #d14d4d 0%, #ad2b2b 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d14d4d), color-stop(100%,#ad2b2b));
	background: -webkit-linear-gradient(top,  #d14d4d 0%,#ad2b2b 100%);
	background: -o-linear-gradient(top,  #d14d4d 0%,#ad2b2b 100%);
	background: -ms-linear-gradient(top,  #d14d4d 0%,#ad2b2b 100%);
	background: linear-gradient(to bottom,  #d14d4d 0%,#ad2b2b 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d14d4d', endColorstr='#ad2b2b',GradientType=0 );
}

ul.breadcrumb > li.active,
ul.breadcrumb > li.incomplete.active  {
	border-bottom: none;
	background: #EFEFEF;
}

ul.breadcrumb > li.active > a {
	color: #444;
	text-shadow: none;
}

ul.breadcrumb > li.incomplete.active > a {
	color: #d14d4d;
}

ul.breadcrumb > li > a {
	font-size: 16px;
	color: white;
	line-height: 50px;
	display: block;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.5);
	font-weight: bold;
	text-align: center;
	border-left: 1px solid rgba(0,0,0,0.2);
	border-right: 1px solid rgba(255,255,255,0.25);
}

#edit-item div.tab-pane {
	padding: 15px;
}

#edit-item div.tab-pane #sales_item_modifiers {
	height: 232px;
	overflow-y: auto;
	overflow-x: hidden;
}

div.tab-content {
	display: block;
	width: auto;
	overflow: hidden;
}

div.tab-content > div.tab-pane {
	display: none;
	overflow: auto;
	float: none;
}

div.tab-content > div.tab-pane.active {
	display: block;
}

#edit-item div.form_field {
	float: left;
	overflow: auto;
}

#edit-item div.row {
	display: block;
	overflow: hidden;
	padding: 15px;
}

#edit-item div.side {
	display: block;
	overflow: hidden;
	float: left;
	width: 425px;
}

#edit-item div.form_field input {
	width: 60px;
	float: left;
}

#edit-item div.form_field label {
	display: block;
	line-height: 18px;
	height: 18px;
	margin-bottom: 4px;
	text-align: center;
}

#edit-item div.form_field button.add, #edit-item div.form_field button.sub {
	display: block;
	width: 35px;
	float: left;
	height: 35px;
	margin: 0px;
	font-size: 18px;
	font-weight: bold;
}
</style>

<script type="text/html" id="template_edit_item">
<ul class="breadcrumb">
	<%
	if(typeof(default_section) == 'undefined'){
		default_section = 0;
	}

	_.each(sections, function(section, section_id){
	var incompleteClass = '';
	if(show_incomplete && section.incomplete){ incompleteClass = ' incomplete'; } %>
	<li class="tab<%-incompleteClass %><% if(section_id == default_section){ print(' active') }; %>" style="width: <%-_.round(100 / _.size(sections), 2)%>%">
		<a data-section-id="<%-section_id%>" href="#item-edit-<%-section_id%>"><%-section.name%></a>
	</li>
	<% }); %>
</ul>
<div class="tab-content">
	<% _.each(sections, function(section, section_id){ %>
	<div class="tab-pane <% if(section_id == default_section){ print('active') }; %>" id="item-edit-<%-section_id%>">
		<%-section.name%>
	</div>
	<% }); %>
</div>
<div class="row" style="position: relative; padding-right: 30px">
	<label for="edit_item_comments" style="margin-bottom: 5px; display: block; font-size: 16px;">Comments</label>
	<textarea style="display: block; width: 100%; height: 75px;" name="comments" id="edit_item_comments"><%-comments%></textarea>
</div>
<div class="row" style="position: relative; padding-bottom: 5px; height: 60px;">
	<a class="submit_button" href="#" style="float: right; margin-top: 15px;">Save Item</a>
	<div style="display: block; float: none; position: absolute; left: 140px; right: 140px; overflow: hidden;">
		<div class="form_field" style="margin-left: 15px;">
			<label>Price</label>
			<input type="text" name="item[price]" id="edit_item_price" value="<%=accounting.formatMoney(price,'')%>" />
		</div>
		<div class="form_field" style="margin-left: 50px;">
			<label>Qty</label>
			<button class="terminal_button sub">-</button><input type="text" name="item[quantity]" id="edit_item_quantity" value="<%=accounting.toFixed(quantity, 0)%>" style="width: 40px;" /><button class="terminal_button add">+</button>
		</div>
		<div class="form_field" style="margin-left: 50px;">
			<label>Discount (%)</label>
			<button class="terminal_button sub">-</button><input type="text" name="item[discount]" id="edit_item_discount" value="<%=discount%>" /><button class="terminal_button add">+</button>
		</div>
		<div class="form_field" style="margin-left: 50px;">
			<label>Seat</label>
			<button class="terminal_button sub">-</button><input type="text" name="item[seat]" id="edit_item_seat" value="<%=seat%>" style="width: 40px;" /><button class="terminal_button add">+</button>
		</div>
	</div>
	<a class="delete_button" href="#" style="margin-top: 15px;">Delete Item</a>
</div>
</script>
