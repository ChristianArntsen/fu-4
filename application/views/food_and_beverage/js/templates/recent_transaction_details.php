<style>
.recent_transaction_details {
	padding: 15px;
	display: block;
	overflow: hidden;
}

.recent_transaction_details div.row {
	padding: 0px;
	margin: 0px;
	display: block;
	overflow: hidden;
	margin-bottom: 5px;
}

.recent_transaction_details .title {
	display: block;
	padding-bottom: 5px;
	margin-bottom: 5px;
	border-bottom: 1px solid #BBB;
	overflow: hidden;
}

.recent_transaction_details .footer {
	display: block;
	padding-top: 5px;
	margin-top: 5px;
	border-top: 1px solid #BBB;
	overflow: hidden;
	font-size: 14px;
}

.recent_transaction_details .totals {
	float: right;
}

.recent_transaction_details .totals > span.value {
	width: 75px;
	text-align: left;
	float: right;
}

.recent_transaction_details .totals div.row {
	width: 200px;
}

.recent_transaction_details .totals span.field {
	float: left;
	text-align: right;
	width: 75px;
}

.recent_transaction_details .totals span.value {
	float: right;
	width: 65px;
	text-align: left;
	font-weight: bold;
}

.title h2 {
	float: left;
	height: 24px;
	line-height: 24px;
}

.recent_transaction_details span.person {
	float: right;
	font-size: 14px;
	height: 24px;
	line-height: 24px;
}

.recent_transaction_details span.table {
	float: right;
}

.recent_transaction_details ul.items {
	display: block;
	overflow: hidden;
	margin: 0px;
	padding: 0px;
}

.recent_transaction_details ul.items li {
	display: block;
	height: 22px;
	line-height: 22px;
	font-size: 14px;
	overflow: hidden;
}

.recent_transaction_details ul.items li span.name {
	width: 315px;
	float: left;
}

.recent_transaction_details ul.items li span.qty {
	width: 50px;
	text-align: center;
	float: left;
}

.recent_transaction_details ul.items li span.total {
	float: right;
	width: 65px;
	text-align: left;
}

ul.payments {
	padding: 0px;
	margin: 0px;
	display: block;
	overflow: hidden;
}

ul.payments li {
	display: block;
	overflow: hidden;
	list-style-type: none;
}

ul.payments li span.type {
	float: left;
	width: 175px;
}

ul.payments li span.amount {
	float: right;
	width: 60px;
	text-align: left;
}
</style>
<script type="text/html" id="template_recent_transaction_details">
<div class="recent_transaction_details">
	<div class="title">
		<div class="row">
			<h2 class="num">Sale #<%-sale_id%></h2>
			<span class="person">Server: <strong><%-employee_name%></strong></span>
		</div>
		<div class="row">
			<span class="date"><%-moment(sale_time).format('MM/DD/YYYY h:mma')%></span>
			<% if(customer_name) { %><span class="person">Customer: <strong><%-customer_name%></strong></span><% } %>
		</div>
	</div>
	<ul class="items">
	<% if(items.length > 0){ %>
	<% _.each(items, function(item){ %>
		<li>
			<span class="name"><%-item.name%></span>
			<span class="qty"><%-item.quantity%></span>
			<span class="total"><%-accounting.formatMoney(item.total)%></span>
		</li>
	<% }); %>
	<% } %>
	</ul>
	<div class="footer">
		<div class="totals">
			<div class="row">
				<span class="field">Subtotal</span>
				<span class="value"><%-accounting.formatMoney(subtotal) %></span>
			</div>
			<div class="row">
				<span class="field">Tax</span>
				<span class="value"><%-accounting.formatMoney(tax) %></span>
			</div>
			<div class="row">
				<span class="field">Total</span>
				<span class="value"><%-accounting.formatMoney(total) %></span>
			</div>
		</div>
		<% if(payments.length > 0){ %>
		<h4 style="margin-bottom: 5px;">Payments</h4>
		<ul class="payments">
		<% _.each(payments, function(payment){ %>
			<li>
				<span class="type"><%-payment.payment_type%></span>
				<span class="amount"><%-accounting.formatMoney(payment.amount)%></span>
			</li>
		<% }); %>
		</ul>
		<% } %>
	</div>
</div>
</script>
