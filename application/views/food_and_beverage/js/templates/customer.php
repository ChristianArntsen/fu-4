<script type="text/html" id="template_customer">
<img class="photo" src="<%-photo %>" />
<div class="customer-info">
	<h4><%-first_name %> <%-last_name %></h4>
	<span><%-email %></span>
	<span><%-phone %></span>
	
	<span style="width: 225px;">
		<strong>Acct #</strong> 
		<span class="balance">
			<% if(account_number){ 
			print(account_number);
		}else{ %>
			<span style="color: #AAA;">n/a</span>
		<% } %>
		</span>
	</span>	
	<span style="width: 225px;">
		<strong><%=App.member_account_name %></strong> 
		<span class="balance <% if(member_account_balance < 0){ print('negative'); } %>"><%-accounting.formatMoney(member_account_balance) %></span>
	</span>
	<span style="width: 225px;">
		<strong><%=App.customer_account_name %></strong> 
		<span class="balance <% if(account_balance < 0){ print('negative'); } %>"><%-accounting.formatMoney(account_balance) %></span>
	</span>
</div>
<a style="float: right;" class="small_button delete_button remove" href="">Remove</a>
</script>
