var ItemsView = Backbone.View.extend({
	tagName: "div",
	className: "item-list",
	
	events: {
		'click .edit-button-order': 'enableSorting',
		'click .save-button-order': 'saveSort',
		'click .up': 'scrollItemsUp',
		'click .down': 'scrollItemsDown'
	},
	
	render: function() {
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('<ul class="items" style="display: block;"></ul>'+
			'<button class="up">&#x25B2;</button>' +
			'<button class="down">&#x25BC;</button>'+
			'<a href="#" class="edit-button-order">Edit Button Order</a>');

		_.each(this.collection.models, this.addItem, this);
		return this;
	},

	addItem: function(item){
		var html = "<li style='display: block; overflow: hidden; float: left; padding: 0px; margin: 0px; width: auto;' data-order-name='"+Base64.encode(item.get('name'))+ "'" +
				"data-order-category='"+Base64.encode(item.get('category')) +"'" +
				"data-order-sub-category='"+Base64.encode(item.get('subcategory'))+"'>" +
					"<button style='float: left;' data-item-id='"+ item.get('item_id')+ "'"+
					"class='fnb_button menu_item'>" +item.get('name')+ "</button></li>";
		this.$el.find('ul').append(html);
	},
	
	enableSorting: function(e){
		this.$el.addClass('editing');
		this.$el.find('ul').sortable({
			handle: '.fnb_button',
			cancel: '',
			placeholder: 'sortable-button'
		});	
		this.$el.find('a.edit-button-order').replaceWith('<a href="#" class="save-button-order">Save Button Order</a>');	
		e.preventDefault();
	},
	
	saveSort: function(e){
		this.$el.removeClass('editing');
		e.preventDefault();
		$('div.item-list').removeClass('editing');
		
		var buttons = $.map( this.$el.find('ul.ui-sortable').children('li'), function(el){
			var element = $(el);
			return {
				'order': element.index(), 
				'name':element.data('order-name'), 
				'category':element.data('order-category'), 
				'sub_category': element.data('order-sub-category')
			}; 
		});

		$.post('<?php echo site_url('food_and_beverage/save_button_order'); ?>', {buttons: buttons}, function(response){
			
		},'json');
		
		this.$el.find('ul.ui-sortable').sortable("destroy");
		this.$el.find('a.save-button-order').replaceWith('<a href="#" class="edit-button-order">Edit Button Order</a>');		
	},
	
	scrollItemsUp: function(event){
		var list = this.$el.find('ul.items');
		var curPos = list.scrollTop();
		
		list.scrollTop(curPos - 65);
		return false;
	},
	
	scrollItemsDown: function(event){
		var list = this.$el.find('ul.items');
		var curPos = list.scrollTop();
		
		list.scrollTop(curPos + 65);	
		return false;	
	},	
});
