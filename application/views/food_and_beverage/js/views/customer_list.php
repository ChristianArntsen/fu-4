var CustomerListView = Backbone.View.extend({
	initialize: function() {
		this.listenTo(this.collection, "add", this.render);
		this.listenTo(this.collection, "remove", this.render);
		this.listenTo(this.collection, "reset", this.render);
	},

	render: function() {
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}

		if(this.collection.models.length > 0){
			this.$el.html('');
			_.each(this.collection.models, this.addCustomer, this);
		}else{
			this.$el.html('<li class="empty">No customers associated</li>');
		}
		return this;
	},

	addCustomer: function(customer){
		this.$el.append( new CustomerListItemView({model: customer}).render().el );
	}
});