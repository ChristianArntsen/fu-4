var ReceiptCustomerSearchView = Backbone.View.extend({
	
	id: 'customer_search_window',
	
	template: _.template( $('#template_receipt_customer_search').html() ),
	events: {
		"keyup input.search": "search",
		"click a.no-customer": "clearCustomer"		
	},
	
	initialize: function(options) {
		if(options && options.receipt){
			this.receipt = options.receipt;
		}
	},

	render: function() {
		var self = this;
		this.$el.html(this.template());
		return this;
	},
	
	search: function(event){
		var term = this.$el.find('input.search').val();
		var view = this;
		var receipt = this.receipt;
		
		if(term == ''){
			return false;
		}
		
		if(view.lastRequest){
			view.lastRequest.abort();
		}
		this.$el.find('img.search-loading').show();
		this.searchRequest(term, view);
	},
	
	searchRequest: _.debounce(function(term, view){
		var url = BASE_URL + 'index.php/food_and_beverage/customer_search';
	
		view.lastRequest = $.get(url, {term: term}, function(response){	
			var customers = new CustomerCollection(response);
			var customerSearchResults = new ReceiptCustomerListView({collection: customers, receipt: view.receipt});
			view.$el.find('div.customer-list').html( customerSearchResults.render().el );
			view.$el.find('img.search-loading').hide();
		}, 'json');
	}, 200),

	clearCustomer: function(event){
		event.preventDefault();
		this.receipt.set('customer', false);
		this.receipt.save();
		$.colorbox2.close();		
	}	
});
