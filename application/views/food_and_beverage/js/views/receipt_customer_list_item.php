var ReceiptCustomerListItemView = Backbone.View.extend({
	
	tagName: "li",
	className: "customer",
	template: _.template( $('#template_receipt_customer').html() ),

	events: {
		"click": "attach"
	},

	initialize: function(options) {
		
		if(options && options.receipt){
			this.receipt = options.receipt;
		}
		
		this.listenTo(this.model, "change", this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		return this;
	},
	
	attach: function(event){
		event.preventDefault();
		
		var receipt_data = {};
		receipt_data.customer = this.model;
		
		if(this.model.get('taxable') == '1'){
			receipt_data.taxable = true;
		}else{
			receipt_data.taxable = false;
		}
		
		this.receipt.set(receipt_data);
		this.receipt.save();
		$.colorbox2.close();
	}
});
