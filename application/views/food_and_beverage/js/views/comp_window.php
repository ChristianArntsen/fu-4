var CompWindowView = Backbone.View.extend({
	
	tagName: "div",
	id: "comp_window",
	template: _.template( $('#template_comp_window').html() ),

	initialize: function(options) {

	},

	events: {
		"click .save": "save",
		"click .no-comp": "clear",
		"click .type": "setType",
		"click .reason": "setReason",
		"blur input.amount": "calculateAmount"
	},

	render: function() {
		
		var attr = {};	
		attr.reasons = [
			'Did Not Like',
			'Long Wait',
			'Wrong Item',
			'Food Cold',
			'Manager Comp',
			'Employee Discount',
			'Birthday',
			'Anniversary',
			'Promotion',
			'Other'
		];
		
		attr.subtotal = this.model.get('subtotal') + this.model.get('comp_total');
		
		if(this.model.get('comp')){
			attr.comp = this.model.get('comp');
		}
		
		// Default comp values if no value is set
		if(!attr.comp){
			attr.comp = {
				type: 'percentage',
				amount: 100,
				description: 'Other'
			};
		}
		
		this.$el.html(this.template(attr));
		this.$el.find('input.amount').keypad({position: 'right'});
		this.calculateAmount();
		return this;
	},
	
	renderTotal: function(total){
		this.$el.find('h2').html( accounting.formatMoney(total) );
		return this;
	},
	
	setType: function(event){
		var button = $(event.currentTarget);
		button.addClass('selected').siblings().removeClass('selected');
		this.calculateAmount(event);
	},
	
	setReason: function(event){
		event.preventDefault();
		$(event.currentTarget).addClass('selected').siblings().removeClass('selected');
	},
	
	calculateAmount: function(event){

		var amount = _.round(this.$el.find('input.amount').val());
		var type = 'percentage';
		
		if(type == 'dollar' && amount > _.round(this.model.get('subtotal') + this.model.get('comp_total'))){
			amount = _.round(this.model.get('subtotal') + this.model.get('comp_total'));
		
		}else if(type == 'percentage' && _.round(amount) > 100){
			amount = 100;
		}
		
		this.$el.find('input.amount').val( accounting.formatNumber(amount, 2) );
		
		// If comp is being applied to a single item
		if(this.model instanceof CartItem){
			var subtotal = this.model.get('subtotal') + this.model.get('comp_total');
			var comp_amount = _.round(subtotal * (amount / 100), 2);
			subtotal -= comp_amount;			
		
		// If comp is being applied to an entire receipt
		}else if(this.model instanceof Receipt){
			
			var receipt = this.model;
			var subtotal = receipt.get('subtotal') + receipt.get('comp_total');
			var comp_amount = 0;
			
			// Loop through each item and calculate what the new comp total will be
			_.each(receipt.get('items').models, function(item){
				var item_subtotal = item.get('split_subtotal') + item.get('split_comp_total');
				comp_amount += _.round(item_subtotal * (amount / 100), 2);
			});
			
			subtotal = _.round(subtotal - comp_amount);
		}
		

		this.renderTotal(subtotal);
	},
	
	clear: function(event){
		event.preventDefault();
		
		if(this.model instanceof Receipt){
			this.model.clearComps();
			
		}else{
			this.model.set({'comp': false});
			this.model.save();				
		}		
			
		$.colorbox2.close();	
	},
	
	save: function(event){
		event.preventDefault();
		var amount = this.$el.find('input.amount').val();
		var type = 'percentage';
		var reason = this.$el.find('div.reasons > a.selected').text();
		
		if(this.model instanceof Receipt){
			this.model.compAllItems(type, reason, amount, App.admin_auth_id);
			
		}else{
			this.model.set({'comp': {'amount': amount, 'type': type, 'description': reason, 'employee_id': App.admin_auth_id}}, {'validate': true});
			this.model.save();			
		}

		$.colorbox2.close();
	}
});
