var CartItem = Backbone.Model.extend({
	idAttribute: "line",
	defaults: {
		"line": null,
		"name": "",
		"seat": 1,
		"item_id": "",
		"category": "",
		"sub_category": "",
		"department": "",
		"description": "",
		"max_discount": 0,
		"discount":0,
		"discount_amount": 0,
		"price": 0.00,
		"subtotal": 0.00,
		"total": 0.00,
		"tax": 0.00,
		"quantity": 1,
		"is_ordered": 0,
		"is_paid": 0,
		"splits": 0,
		"paid_splits": 0,
		"modifiers": [],
		"sides": [],
		"soups": [],
		"salads": [],
		"number_sides": 0,
		"number_soups": 0,
		"number_salads": 0,
		"printer_ip": "",
		"print_priority": 0,
		"comments":"",
		"is_new": false,
		"comp": false
	},
	
	initialize: function(){
		var line = this.get('line');
		var modifierCollection = new ModifierCollection(this.get('modifiers'));
		var sideCollection = new ItemSideCollection(this.get('sides'));
		var soupCollection = new ItemSideCollection(this.get('soups'));
		var saladCollection = new ItemSideCollection(this.get('salads'));
		
		// Keep all sides sorted by position
		sideCollection.comparator = 'position';
		soupCollection.comparator = 'position';
		saladCollection.comparator = 'position';

		// Create url to save sides to database
		sideCollection.url = App.api_table + "cart/" + this.get('line') + "/sides";
		soupCollection.url =  App.api_table + "cart/" + this.get('line') + "/soups";
		saladCollection.url =  App.api_table + "cart/" + this.get('line') + "/salads";

		this.set({
			modifiers: modifierCollection,
			sides: sideCollection,
			soups: soupCollection,
			salads: saladCollection
		});
		this.calculatePrice();

		// If any changes made to item, re-calcuate price (total, tax, etc)
		this.listenTo(this, "change", this.calculatePrice);
		this.listenTo(this, "invalid", displayError);
		this.listenTo(this.get('modifiers'), "change", this.calculatePrice);
		this.listenTo(this.get('sides'), "add remove change", this.calculatePrice);
		this.listenTo(this.get('soups'), "add remove change", this.calculatePrice);
		this.listenTo(this.get('salads'), "add remove change", this.calculatePrice);
	},
	
	// Returns a JSON copy of all the item data (including nested collections)
	copy: function(){
		
		var itemCopy = _.clone(this.attributes);		
		itemCopy.modifiers = itemCopy.modifiers.toJSON();	
		
		// Loop through all sides attached to item and copy their modifiers as well
		var sideTypes = ['sides', 'soups', 'salads'];
		_.each(sideTypes, function(type){
			
			itemCopy[type] = itemCopy[type].toJSON();
			
			// If sides exist, loop through each side
			if(itemCopy[type].length > 0){
				
				// Copy side modifiers
				_.each(itemCopy[type], function(side, index){
					itemCopy[type][index].modifiers = side.modifiers.toJSON();
				});
			}			
		});

		return itemCopy;		
	},

	set: function(attributes, options){
		if(!this.attributes.line){
			this.attributes.line = App.cart.getNextLine();
			this.id = this.attributes.line;
		}

		// If quantity, discount or comp is changed on cart item, 
		// apply same change to item sides (if any)
		var adjustSides = false;
		if(attributes.quantity || attributes.discount || attributes.comp){
			adjustSides = true
		}
			
		if(adjustSides){
			var cartItem = this;
			var sideTypes = ['sides', 'soups', 'salads'];
			var sideAttributes = _.pick(attributes, 'quantity', 'discount', 'comp');

			_.each(sideTypes, function(sideType){
				if(cartItem.get(sideType) && cartItem.get(sideType).length > 0){
					_.each(cartItem.get(sideType).models, function(side){
						side.set(sideAttributes, {silent: true});
						side.calculatePrice();
					});
				}
			});
		}

		Backbone.Model.prototype.set.call(this, attributes, options);
	},

	calculatePrice: function(){
		var cartItem = this;

		// Loop through all different sides and get their totals
		var sideTypes = ['sides', 'soups', 'salads'];
		var sidesTotal = 0;
		var sidesTax = 0;
		var sidesSubtotal = 0;
		var sidesBaseSubtotal = 0;
		var modifierTotal = 0;
		var sidesCompTotal = 0;

		_.each(sideTypes, function(sideType){
			if(cartItem.get(sideType) && cartItem.get(sideType).length > 0){
				_.each(cartItem.get(sideType).models, function(side){
					sidesTotal += side.get('total');
					sidesTax += side.get('tax');
					sidesSubtotal += side.get('subtotal');
					sidesCompTotal += _.round(side.get('comp_total'), 2);
					sidesBaseSubtotal += side.get('base_subtotal');
				});
			}
		});

		modifierTotal = _.round(this.get('modifiers').getTotalPrice(), 2);
		var price = _.round(parseFloat(this.get('price')), 2);
		
		var no_discount_subtotal = this.getSubtotal( _.round(price + modifierTotal, 2), this.get("quantity"), 0);
		var subtotal = this.getSubtotal( _.round(price + modifierTotal, 2), this.get("quantity"), this.get('discount'));
		var discount_amount = _.round(no_discount_subtotal - subtotal, 2);
		var comp_discount = this.getCompDiscount(subtotal);
		var base_subtotal = subtotal;
		subtotal -= comp_discount;
		var tax = this.getTax(subtotal);
		var total = this.getTotal(subtotal, tax);
		comp_discount += _.round(sidesCompTotal, 2);

		this.set({
			"comp_total": _.round(comp_discount, 2),
			"sides_total": _.round(sidesTotal, 2),
			"sides_tax": _.round(sidesTax, 2),
			"sides_subtotal": _.round(sidesSubtotal, 2),
			"sides_base_subtotal": _.round(sidesBaseSubtotal, 2),
			"discount_amount": _.round(discount_amount, 2),
			"base_subtotal": _.round(base_subtotal, 2),
			"subtotal": _.round(subtotal, 2),
			"subtotal_no_discount": _.round(no_discount_subtotal, 2),
			"tax": _.round(tax, 2),
			"total": _.round(total, 2)
		});
	},

	getSubtotal: function(price, qty, discount){
		var price = _.round(price, 2);
		var discount = _.round(discount, 2);
		if(isNaN(discount)){
			discount = 0;
		}
		
		var discount_ratio =  _.round( _.round(100 - discount, 2) / 100, 5);
		var discounted_price = _.round(price * discount_ratio, 2);
		return _.round(discounted_price * qty, 2);
	},

	getCompDiscount: function(subtotal){
		
		var comp_amount = 0;
		if(this.get('comp') && this.get('comp').amount > 0){
			comp_amount = this.get('comp').amount;
			
			if(this.get('comp').type == 'percentage'){
				comp_amount = _.round(subtotal * _.round(comp_amount / 100, 5), 2);
			}
		}

		return comp_amount; 
	},

	getTax: function(subtotal){
		var totalTax = 0;
		var subtotal = _.round(subtotal, 2);

		if(this.get('taxes') && this.get('taxes').length > 0){
			_.each(this.get('taxes'), function(tax){
				var percentage = parseFloat(tax.percent);
				if(isNaN(percentage) || percentage == 0){
					return true;
				}
				
				var taxAmount = 0;
				if(tax.cumulative == "1"){
					taxAmount = _.round( _.round(percentage / 100, 5) * _.round(subtotal + totalTax, 2), 5);
				}else{
					taxAmount = _.round( _.round(percentage / 100, 5) * subtotal, 5);
				}
				
				tax.amount = _.round(taxAmount, 2);
				totalTax += _.round(taxAmount, 2);
			});
		}

		return _.round(totalTax, 2);
	},

	getTotal: function(subtotal, total){
		return parseFloat(subtotal + total);
	},

	validate: function(attrs, options){
		var maxDiscount = this.get('max_discount');
		var minPrice = this.getSubtotal(this.get('base_price'), attrs.quantity,  maxDiscount);
		var setPrice = _.round( this.getSubtotal(attrs.price, attrs.quantity, attrs.discount), 2);

		if(attrs.quantity < 1){
			return "Quantity must be at least 1";
		}

		if(setPrice < minPrice){
			return "Maximum discount is " + accounting.formatMoney(this.get('max_discount'), '') + '% or ' + accounting.formatMoney(minPrice);
		}
	},

	isSoupsComplete: function(){
		if(this.get('soups').length < this.get('number_soups')){
			return false;
		}
		return true;
	},

	isSidesComplete: function(){
		if(this.get('sides').length < this.get('number_sides')){
			return false;
		}
		return true;
	},

	isSaladsComplete: function(){
		if(this.get('salads').length < this.get('number_salads')){
			return false;
		}
		return true;
	},

	isModifiersComplete: function(categoryId){
		if(!this.get('modifiers').isComplete(categoryId)){
			return false;
		}
		return true;
	},

	// Checks if all required fields, sides, and modifiers are set
	isComplete: function(){

		if(!this.isSoupsComplete()){ return false }
		if(!this.isSidesComplete()){ return false }
		if(!this.isSaladsComplete()){ return false }
		if(!this.isModifiersComplete()){ return false }

		return true;
	}
});
