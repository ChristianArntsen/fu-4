var Item = Backbone.Model.extend({
	idAttribute: "item_id",
	defaults: {
		"name": "",
		"category": "",
		"sub_category": "",
		"department": "",
		"description": "",
		"price": 0.00,
		"quantity": 1,
		"modifiers": [],
		"sides": [],
		"number_sides": 0,
		"number_soups": 0,
		"number_salads": 0,
		"sides_subtotal":0,
		"sides_total":0,
		"sides_tax":0,
		"printer_ip": "",
		"print_priority": 0,
		"inventory_unlimited": true,
		"inventory_level": 0,
		"is_new": false
	}
});
