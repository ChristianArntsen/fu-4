var ItemSide = Side.extend({
	idAttribute: "position",
	default: {
		'item_id': 0,
		'price': 0,
		'quantity': 1,
		'tax': 0,
		'total': 0,
		'subtotal': 0,
		'discount': 0,
		"printer_ip": "",
		"print_priority": 0,
		"comp_total": 0,
		"subtotal_no_discount": 0,
		"taxes": []
	},

	initialize: function(){
		var modifierCollection = new ModifierCollection(this.get('modifiers'));

		this.set({
			modifiers: modifierCollection
		});
		
		this.listenTo(this.get("modifiers"), "change", this.calculatePrice);
	}
});
