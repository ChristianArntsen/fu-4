var ReceiptItem = CartItem.extend({
	
	defaults: {
		"split_subtotal": 0,
		"split_total": 0,
		"split_tax": 0,
		"comp": false
	},
	
	initialize: function(){
		
		var line = this.get('line');
		var cartItem = App.cart.where({"line": line});

		var modifierCollection = new ModifierCollection(this.get('modifiers'));
		var sideCollection = new ItemSideCollection(this.get('sides'));
		var soupCollection = new ItemSideCollection(this.get('soups'));
		var saladCollection = new ItemSideCollection(this.get('salads'));

		// Keep all sides sorted by position
		sideCollection.comparator = 'position';
		soupCollection.comparator = 'position';
		saladCollection.comparator = 'position';
		
		// Check if the customer attached to the sale has a default discount
		var receipt = false;
		if(this.collection && this.collection.receipt){
			var receipt = this.collection.receipt;
		}
		
		if(this.get('discount') == 0 && receipt && receipt.get('customer')){
			
			var line = this.get('line');
			var cart_item = App.cart.get(line);
			
			// Apply default discount to item automatically
			var discount = receipt.get('customer').get('discount');
			var appliedDiscount = 0;
		    if(_.round(cart_item.get('max_discount')) < _.round(discount)){
				appliedDiscount = _.round(cart_item.get('max_discount'));
			}else{
				appliedDiscount = _.round(discount);
			}	

			cart_item.set('discount', appliedDiscount);
			cart_item.save();
		}		

		// Create url to save sides to database
		sideCollection.url = App.api_table + "cart/" + this.get('line') + "/sides";
		soupCollection.url =  App.api_table + "cart/" + this.get('line') + "/soups";
		saladCollection.url =  App.api_table + "cart/" + this.get('line') + "/salads";

		this.set({
			modifiers: modifierCollection,
			sides: sideCollection,
			soups: soupCollection,
			salads: saladCollection
		});

		// If cart item changes, automatically update receipt item
		if(cartItem[0]){
			this.listenTo(cartItem[0], "change", this.update, this);
			this.listenTo(cartItem[0], "remove", this.delete, this);
		}
	},

	update: function(data){
		this.set(data.attributes);
	},
	
	// Loops through all receipts and checks if this item is split
	// across multiple receipts
	getNumberSplits: function(){
				
		var receipts = App.receipts.models;
		var line = this.get('line');
		var divisor = 1;
		
		// Loop through each receipt
		_.each(receipts, function(receipt){
			
			if(receipt.get('items').length == 0){
				return false;
			}
			
			// Loop through each receipt's items
			_.each(receipt.get('items').models, function(item){
				if(item.get('line') == line){
					divisor++;
					return true;
				}
			});
		});
		
		return divisor;
	},
	
	// Calculate receipt item price (which may be split)
	calculatePrice: function(divisor, taxable){
		
		if(typeof(taxable) == 'undefined'){
			taxable = 1;
		}
		
		var receiptItem = this;
		if(!divisor){
			divisor = 1;
		}

		// Loop through all different sides and get their totals
		var sideTypes = ['soups', 'salads', 'sides'];
		var sidesSplitTotal = 0;
		var sidesSplitTax = 0;
		var sidesSplitSubtotal = 0;

		// Calculate and add up split total, tax, and subtotal of all sides
		_.each(sideTypes, function(sideType){
			if(receiptItem.get(sideType) && receiptItem.get(sideType).length > 0){
				_.each(receiptItem.get(sideType).models, function(side){
					
					receiptItem.calculateSplitPrices(divisor, side, taxable);	
					
					sidesSplitSubtotal += side.get('split_subtotal');
					sidesSplitTax += side.get('split_tax');
					sidesSplitTotal += _.round(side.get('split_tax') + side.get('split_subtotal'), 2);				
				});
			}
		});

		this.calculateSplitPrices(divisor, this, taxable);
		this.set({
			'split_sides_subtotal': _.round(sidesSplitSubtotal, 2),
			'split_sides_total': _.round(sidesSplitTotal, 2),
			'split_sides_tax': _.round(sidesSplitTax, 2) 
		});
		return this;
	},
	
	calculateSplitPrices: function(divisor, item, taxable){
		
		var data = {};
		data.total = item.get('total');
		data.subtotal = item.get('subtotal');
		data.subtotal_no_discount = item.get('subtotal_no_discount');
		data.tax = item.get('tax');
		data.comp_total = item.get('comp_total');
		
		data.price = item.get('price');
		data.discount_amount = item.get('discount_amount');
		data.number_splits = parseInt(divisor);
		
		data.split_price = _.round(data.price / divisor, 2);
		data.split_discount_amount = _.round(data.discount_amount / divisor, 2);
		data.split_subtotal_no_discount = _.round(data.subtotal_no_discount / divisor, 2);
		data.split_subtotal = _.round(data.subtotal / divisor, 2);
		data.split_comp_total = _.round(data.comp_total / divisor, 2);
		
		if(taxable == 1){ 
			data.split_tax = item.getTax(data.split_subtotal);
			data.split_total = _.round(data.split_subtotal + data.split_tax, 2);
		}else{
			data.split_tax = 0;
			data.split_total = data.split_subtotal;
		}
		
		item.set(data);
		return true;
	},

	delete: function(data){
		if(this.collection){
			this.collection.remove(this);
		}
	}
});
