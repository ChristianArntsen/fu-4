var Receipt = Backbone.Model.extend({
	
	idAttribute: "receipt_id",
	
	defaults: {
		"receipt_id": null,
		"items": [],
		"taxes": [],
		"date_paid": null,
		"status": "pending",
		"total": 0.00,
		"auto_gratuity": null,
		"auto_gratuity_amount": 0,
		"subtotal": 0.00,
		"tax": 0.00,
		"amount_due": 0.00,
		"taxable": 1,
		"customer": false
	},

	set: function(attributes, options){
		if(!attributes.receipt_id && !this.get('receipt_id')){
			attributes.receipt_id = App.receipts.getNextNumber();
			attributes.id = attributes.receipt_id;
		}
		Backbone.Model.prototype.set.call(this, attributes, options);
	},

	initialize: function(){
		var receipt_id = this.get('receipt_id');

		// Initialize item collection
		var itemCollection = new ReceiptCart(this.get('items'));
		itemCollection.url = App.api_table + 'receipts/' + receipt_id + '/items/';
		itemCollection.receipt = this;

		// Initialize payment collection
		var paymentCollection = new PaymentCollection(this.get('payments'));
		paymentCollection.url = App.api_table + 'receipts/' + receipt_id + '/payments/';
			
		if(this.get('customer')){
			this.set('customer', new Customer(this.get('customer')));
		}
			
		this.set({
			"items": itemCollection,
			"payments": paymentCollection,
			"auto_gratuity": this.get('auto_gratuity') != null ? this.get('auto_gratuity') : "<?=$this->config->item('auto_gratuity')?>"
		});

		this.listenTo(this.get('payments'), 'add remove reset', this.markPaid, this);
		this.listenTo(this.get('items'), 'add remove change:total change:sides_total change:comp', this.calculateTotals, this);
		this.on('change:taxable', this.calculateTotals);
		this.on('change:customer', this.applyCustomerDiscount);
		this.on('change:customer', this.render);
	},
	
	applyCustomerDiscount: function(model){
		
		var customer = this.get('customer');
		var items = this.get('items');
		
		if(!customer){
			return false;
		}
		var discount = customer.get('discount');
		
		if(!items || items.length == 0){
			return false;
		}
		
		// Loop through items currently on receipt and apply discount
		_.each(items.models, function(receipt_item){
			
			var line = receipt_item.get('line');
			var cart_item = App.cart.get(line)
			
			// If an item had a manually entered discount, do not modify it
		    if(_.round(cart_item.get('discount')) != 0){
				return false;
			}
		    
		    var appliedDiscount = 0;
		    if(_.round(cart_item.get('max_discount')) < _.round(discount)){
				appliedDiscount = _.round(cart_item.get('max_discount'));
			}else{
				appliedDiscount = _.round(discount);
			}
					
			cart_item.set('discount', appliedDiscount);
			cart_item.save();
		});		
	},
	
	calculateTotals: function(){
		
		var splitItems = this.collection.getSplitItems();
		var receipt = this;

		// Init receipt variables
		var taxable = this.get('taxable');
		var sideTypes = ['sides','soups','salads'];
		data = {};
		data.total = 0.00;
		data.subtotal = 0.00;
		data.tax = 0.00;
		data.comp_total = 0.00;
		data.auto_gratuity = _.round(this.get('auto_gratuity'));
		data.auto_gratuity_amount = _.round(this.get('auto_gratuity_amount'));
		this.set('taxes', []);
		
		// Add up all the items in receipt
		_.each(this.get('items').models, function(item){
			var line = item.get('line');
			var divisor = splitItems[line];

			item.calculatePrice(divisor, receipt.get('taxable'));
			
			if(taxable == 1){
				receipt.mergeTaxes(item.get('taxes'));
				
				_.each(sideTypes, function(sideType){
					if(item.get(sideType) && item.get(sideType).length > 0){
						_.each(item.get(sideType).models, function(side){
							receipt.mergeTaxes(side.get('taxes'));
						});
					}
				});
			}						
			
			data.total += item.get('split_total') + item.get('split_sides_total');
			data.subtotal += item.get('split_subtotal') + item.get('split_sides_subtotal');
			data.tax += item.get('split_tax') + item.get('split_sides_tax');
			data.comp_total += item.get('split_comp_total');
		});
		
		// Make sure receipt totals are rounded nicely
		data.subtotal = _.round(data.subtotal, 2);
		data.tax = _.round(data.tax, 2);
		data.comp_total = _.round(data.comp_total, 2);
		
		var auto_gratuity_adjustment = _.round(data.auto_gratuity / 100, 5);
		data.auto_gratuity_amount = _.round(_.round(data.subtotal * auto_gratuity_adjustment, 5));
		data.total = _.round(data.tax + data.subtotal + data.auto_gratuity_amount, 2);

		// Update the receipt with totals
		this.set(data);
		return data;
	},
	
	// Merges an item's taxes with the total receipt taxes (and amounts)
	mergeTaxes: function(itemTaxes){
		
		if(!itemTaxes || itemTaxes.length == 0){
			return false;
		}
		var receiptTaxes = this.get('taxes');

		_.each(itemTaxes, function(tax){
			var key = accounting.formatNumber(tax.percent, 2) + '' + tax.name;
			
			if(receiptTaxes[key]){
				receiptTaxes[key].amount += tax.amount;
				receiptTaxes[key].amount = _.round(receiptTaxes[key].amount, 2);
			}else{
				receiptTaxes[key] = _.clone(tax);
			}
		});
		
		this.set('taxes', receiptTaxes);
		return true;
	},

	getTotalDue: function(){
		// Figure out how much is due on the receipt
		this.calculateTotals();
		var totalPaid = _.round(this.get('payments').getTotal(), 2);
		totalDue = _.round(this.get('total') - totalPaid, 2);
		return totalDue;
	},

	isPaid: function(){
		if(this.getTotalDue() > 0){
			return false;
		}
		return true;
	},

	markPaid: function(){
		var isPaid = 1;
		if(this.get('payments').getTotal() == 0){
			var isPaid = 0;
		}

		// Mark each item in cart as paid or unpaid
		_.each(this.get('items').models, function(item){
			var line = item.get('line');
			App.cart.get(line).set({'is_paid': isPaid});
		});

		// Mark receipt as paid (if entire receipt has been paid)
		if(this.isPaid()){
			this.set({'status':'complete'});
		}
	},
	
	printReceipt: function(type){
		
		if ($('div#jquery_keypad').is(':visible')){
			alert('Please save gratuity in order to proceed.');
			return false;
		}
		if(!type){
			type = 'pre-sale';
		}
		this.calculateTotals();

		var receipt = this;
		var receipt_data = '';
		var builder = new StarWebPrintBuilder();
		
		// Add course information
		receipt_data += builder.createAlignmentElement({position:'center'});
		if (App.receipt.header.course_name){
			receipt_data += builder.createTextElement({width:1,data:App.receipt.header.course_name+"\n"});
		}
		if (App.receipt.header.address){
			receipt_data += builder.createTextElement({width:1,data:App.receipt.header.address+"\n"});
		}
		if (App.receipt.header.address_2){
			receipt_data += builder.createTextElement({width:1,data:App.receipt.header.address_2+"\n"});
		}
		if (App.receipt.header.phone){
			receipt_data += builder.createTextElement({width:1,data:App.receipt.header.phone+"\n\n"});
		}
		receipt_data += builder.createAlignmentElement({position:'left'});
		
		// Add sale information
		receipt_data += builder.createTextElement({width:1,data:'***********************************************\n'});
		receipt_data += builder.createTextElement({width:1,data:add_white_space('CHECK #'+ receipt.get('receipt_id'), add_white_space('DATE', webprnt.get_date(), 13))+'\n'});
		receipt_data += builder.createTextElement({width:1,data:add_white_space('TABLE #'+ App.table_num, add_white_space('TIME', webprnt.get_time(), 13))+'\n'});
		if(App.receipt.header.employee_name && App.receipt.header.employee_name != ''){
			receipt_data += builder.createTextElement({width:1,data:add_white_space('SERVER:', App.receipt.header.employee_name)+'\n'});
		}
		if(receipt.get('customer') && receipt.get('customer').get('person_id')){
			var customer_name = receipt.get('customer').get('last_name') +', '+ receipt.get('customer').get('first_name');
			receipt_data += builder.createTextElement({width:1,data:add_white_space('CUSTOMER:', customer_name)+'\n'});
		}
		receipt_data += builder.createTextElement({width:1,data:'***********************************************\n'});	
		
		// Begin item list
		receipt_data += builder.createTextElement({width:1,data:'\n' +add_white_space('ITEMS ORDERED', 'AMOUNT') + '\n\n'});	
		
		// Loop through items on receipt
		_.each(this.get('items').models, function(item){

			var split = '';
			if(item.get('number_splits') > 1){
				split = '1/' + item.get('number_splits')+ ' ';
			}
			receipt_data += builder.createTextElement({width:1,data: add_white_space('('+accounting.formatNumber(item.get('quantity'))+') ' +split+ item.get('name') +' ', accounting.formatMoney(item.get('split_subtotal_no_discount'), '')) +'\n'});
			
			// Loop through sides, soups, and salads (if any of each)
			if(item.get('sides')){
				_.each(item.get('sides').models, function(side){
					if (side.get('split_subtotal') > 0){
						receipt_data += builder.createTextElement({width:1,data: add_white_space('    ' +side.get('name')+ ' ', accounting.formatMoney(side.get('split_subtotal_no_discount'), ''))+'\n'});
					}
				});
			}
			if(item.get('soups')){
				_.each(item.get('soups').models, function(soup){
					if (soup.get('split_subtotal') > 0){
						receipt_data += builder.createTextElement({width:1,data: add_white_space('    ' +soup.get('name')+ ' ', accounting.formatMoney(soup.get('split_subtotal_no_discount'),''))+'\n'});
					}
				});
			}
			if(item.get('salads')){
				_.each(item.get('salads').models, function(salad){
					if (salad.get('split_subtotal') > 0){
						receipt_data += builder.createTextElement({width:1,data: add_white_space('    ' +salad.get('name')+ ' ', accounting.formatMoney(salad.get('split_subtotal_no_discount'), ''))+'\n'});
					}
				});
			}
			
			// If item has a discount, display it underneath the item name
			if(item.get('discount') && item.get('discount') > 0){
				receipt_data += builder.createTextElement({width:1,data: add_white_space('    '+ accounting.formatNumber(item.get('discount'), 2) + '% Discount', '-' + accounting.formatMoney(item.get('split_discount_amount'), '')) +'\n'});			
			}
			
			if(item.get('split_comp_total') && item.get('split_comp_total') > 0){
				receipt_data += builder.createTextElement({data: add_white_space('    Comp: ' + item.get('comp').description, '-' + accounting.formatMoney(item.get('split_comp_total'), '')) +'\n'});			
			}
		});
		
		// Receipt totals
		receipt_data += builder.createTextElement({data:'\n-----------------------------------------------\n'});
		receipt_data += builder.createTextElement({width: 2, data:'\n' + add_white_space('Subtotal:', accounting.formatMoney(receipt.get('subtotal')), 23) });
		
		if (receipt.get('auto_gratuity') && receipt.get('auto_gratuity') > 0){
			receipt_data += builder.createTextElement({width:2, data: '\n' + add_white_space(accounting.formatNumber(receipt.get('auto_gratuity'), 2) + '% Gratuity:', accounting.formatMoney(receipt.get('auto_gratuity_amount')), 23) });
		}
		
		if(!receipt.get('taxable') || receipt.get('taxable') == 0){
			receipt_data += builder.createTextElement({width: 2, data:'\n' + add_white_space('Tax:', accounting.formatMoney(0), 23) });
		}else{
			if(receipt.attributes.taxes){		
				for(var key in receipt.attributes.taxes){
					var tax = receipt.attributes.taxes[key];
					receipt_data += builder.createTextElement({width:2, data:'\n' + add_white_space(accounting.formatNumber(tax.percent, 3) +'% '+ tax.name, accounting.formatMoney(tax.amount), 23) });
				}
			}
		}
		
		receipt_data += builder.createTextElement({width: 2, data:'\n' + add_white_space('Total:', accounting.formatMoney(receipt.get('total')), 23) });
		receipt_data += builder.createTextElement({width: 1, data:'\n'});
		
		var show_tip_line = false;

		// Payments list
		if(type == 'sale'){
			receipt_data += builder.createTextElement({width:1, data:'\nPayments:\n'});
			_.each(this.get('payments').models, function(payment){
				
				if(payment.get('type').indexOf('Member') >= 0 || 
					payment.get('type').indexOf('Customer') >= 0 || 
					payment.get('type').indexOf('Gift') >= 0 ||
					payment.get('type').indexOf('Credit Card') >= 0){
					
					show_tip_line = true;
				}
			
				receipt_data += builder.createTextElement({width:1,data: add_white_space(payment.get('type') + ': ', accounting.formatMoney(payment.get('amount'))) + '\n'});
			});
		}
		
		if(type == 'sale'){
			
			if(show_tip_line && App.receipt.print_tip_line == '1'){
				// Add tip line
				receipt_data += builder.createTextElement({width: 1, data: '\n\n' + add_white_space('Tip:',  '$________.____') + '\n\n'});
				receipt_data += builder.createTextElement({width: 1, data: add_white_space('TOTAL CHARGE:', '$________.____') });	
			
				// Add signature line
				receipt_data += builder.createTextElement({width: 1, data: '\n\n\nX_____________________________________________\n'});
			}
					
			// Add sale ID barcode
			receipt_data += builder.createTextElement({width: 1, data:"\n"});
			receipt_data += builder.createAlignmentElement({position:'center'});
			receipt_data += builder.createTextElement({data:"\n\n"});
			receipt_data += builder.createBarcodeElement({symbology:'Code128', width: 'width2', height:40, hri:false, data: App.receipt.sale_id});
			receipt_data += builder.createTextElement({width:1,data:'\nSale ID: ' + App.receipt.sale_id + '\n'});
		}		
		
		// Add return policy
		if(App.receipt.return_policy != ''){
			receipt_data += builder.createAlignmentElement({position:'center'});
			receipt_data += builder.createTextElement({width: 1, data:"\n\n"});
			receipt_data += builder.createTextElement({width: 1, data: App.receipt.return_policy });
			receipt_data += builder.createTextElement({width: 1, data:"\n\n\n"});
		}
		
		// Add space at bottom and cut receipt
		receipt_data += builder.createTextElement({width: 1, data: '\n\n\n'});
		receipt_data += builder.createCutPaperElement({feed: true});
	   
	    if (App.receipt.print_two_receipts_other == '1' || App.receipt.print_two_receipts == '1'){
	    	receipt_data += receipt_data;
		}
	    
	    webprnt.print(receipt_data, window.location.protocol + "//" + App.receipt_ip+"/StarWebPRNT/SendMessage");
	    return false;
	},
	
	compAllItems: function(type, reason, amount, employee_id){
		var items = this.get('items');
		var receipt = this;
		
		if(items.length == 0){
			return false;
		}
		
		$.post(App.api_table + 'receipts/' + receipt.get('receipt_id') + '/comps', {'comp': {'type':type, 'description':reason, 'amount':amount, 'employee_id':employee_id}}, function(response){
			
		},'json');
		
		_.each(items.models, function(item){
			var line = item.get('line');
			var cart_item = App.cart.get(line);
			
			cart_item.set({'comp': {'amount': amount, 'type': type, 'description': reason, 'employee_id': App.admin_auth_id}}, {'validate': true});
		});	
		
		return true;	
	},
	
	clearComps: function(){
		var items = this.get('items');
		var receipt = this;
		
		if(items.length == 0){
			return false;
		}
		
		$.post(App.api_table + 'receipts/' + receipt.get('receipt_id') + '/comps', {'comp': false}, function(response){
			
		},'json');		
		
		_.each(items.models, function(item){
			var line = item.get('line');
			var cart_item = App.cart.get(line);
			
			cart_item.set({'comp': false});
		});
		
		return true;			
	}
});
