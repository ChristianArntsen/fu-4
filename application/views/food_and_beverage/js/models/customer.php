var Customer = Backbone.Model.extend({
	idAttribute: "person_id",
	defaults: {
		"first_name": "",
		"last_name": "",
		"email": "",
		"phone": "",
		"photo": "",
		"account_number": "",
		"member_account_balance": 0.00,
		"account_balance": 0.00,
		"member_account_balance_allow_negative": '0',
		"account_balance_allow_negative": '0',
		"discount":0.00,
		"taxable": 1
	},
	
	hasMemberBalance: function(){
		return this.get("member_account_balance") > 0 || this.get("member_account_balance_allow_negative") == '1';
	},
	
	hasCustomerBalance: function(){
		return this.get("account_balance") > 0 || this.get("account_balance_allow_negative") == '1';
	}	
});
