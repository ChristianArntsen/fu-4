var Side = CartItem.extend({
	idAttribute: "item_id",
	defaults: {
		"name": "",
		"category": "",
		"sub_category": "",
		"department": "",
		"description": "",
		"price": 0.00,
		"quantity": 1,
		"tax": 0,
		"total": 0,
		"subtotal": 0,
		"modifiers": [],
		"taxes": [],
		"view_category": 0,
		"printer_ip": "",
		"print_priority": 0,
		"add_on_price":0.00
	},

	// Reset "set" method back to default from CartItem
	set: function(attributes, options){
		Backbone.Model.prototype.set.call(this, attributes, options)
	}
});
