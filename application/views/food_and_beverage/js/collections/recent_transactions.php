var RecentTransactionCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		return new RecentTransaction(attrs, options);
	}
});