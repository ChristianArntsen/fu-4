var Cart = Backbone.Collection.extend({
	url: App.api_table + 'cart',

	model: function(attrs, options){
		return new CartItem(attrs, options);
	},

	initialize: function(){
		this.listenTo(this, 'error', this.undo);
	},

	// Undo quantity on item
	undo: function(item, response){
		if(!item.changed.quantity){
			this.remove(item);
		}else{
			item.set('quantity', item._previousAttributes.quantity);
		}
	},

	clearNew: function(){
		_.each(this.models, function(model){
			model.set('is_new', false);
		});
	},

	getNextSeat: function(){
		var seat = 0;
		_.each(this.models, function(cartItem){
			var curSeat = parseInt(cartItem.get('seat'));

			if(curSeat && curSeat > seat){
				seat = curSeat;
			}
		});

		return seat + 1;
	},

	getNextLine: function(){
		var line = 0;
		_.each(this.models, function(cartItem){
			var curLine = parseInt(cartItem.get('line'));

			if(curLine && curLine > line){
				line = curLine;
			}
		});

		return line + 1;
	},

	getTotals: function(){
		var total = 0.00;
		var subtotal = 0.00;
		var tax = 0.00;
		var num_items = 0;

		_.each(this.models, function(cartItem){
			total += _.round(cartItem.get('total') + cartItem.get('sides_total'), 2);
			subtotal += _.round(cartItem.get('subtotal') + cartItem.get('sides_subtotal'), 2);
			tax += _.round(cartItem.get('tax') + cartItem.get('sides_tax'), 2);
			num_items += parseInt(cartItem.get('quantity'));
		});

		return {
			"total": _.round(total, 2),
			"subtotal": _.round(subtotal, 2),
			"tax": _.round(tax, 2),
			"num_items": _.round(num_items, 2)
		};
	},

	unSelectItems: function(){
		var selectedItems = this.where({"selected":true});
		_.each(selectedItems, function(item){
			item.unset("selected");
		});

		return this;
	}
});
