var ReceiptCollection = Backbone.Collection.extend({
	url: App.api_table + "receipts",

	model: function(attrs, options){
		return new Receipt(attrs, options);
	},

	// Get all items selected within receipts
	getSelectedItems: function(){
		var selectedItems = [];

		// Loop through each receipt
		_.each(this.models, function(receipt){
			var items = receipt.get('items');
			if(items){
				selectedItems = selectedItems.concat(
					items.where({"selected":true})
				);
			}
		});

		return selectedItems;
	},

	unSelectItems: function(){
		// Loop through each receipt
		_.each(this.models, function(receipt){
			var items = receipt.get('items');
			if(items){
				var selectedItems = items.where({"selected":true});
				_.each(selectedItems, function(item){
					item.unset("selected");
				});
			}
		});

		return this;
	},

	getNextNumber: function(){
		var receiptNum = 0;
		_.each(this.models, function(receipt){
			var curNum = parseInt(receipt.get('receipt_id'));

			if(curNum && curNum > receiptNum){
				receiptNum = curNum;
			}
		});

		return receiptNum + 1;
	},

	getSplitItems: function(){
		var itemSplits = {};
		if(this.length == 0){
			return itemSplits;
		}

		// Loop through each receipt
		_.each(this.models, function(receipt){

			// Loop through each item in receipt
			var items = receipt.get('items');
			_.each(items.models, function(item){
				var line = item.get('line');

				// Store counts of split items in array
				if(itemSplits[line]){
					itemSplits[line]++;
				}else{
					itemSplits[line] = 1;
				}
			});
		});

		return itemSplits;
	},
	
	getCompTotals: function(line){
		var itemComps = {};
		if(this.length == 0){
			return itemComps;
		}

		// Loop through each receipt
		_.each(this.models, function(receipt){

			// Loop through each item in receipt
			if(line){
				var items = receipt.get('items').get(line);
			}else{
				var items = receipt.get('items');
			}
			
			_.each(items.models, function(item){
				
				var itemLine = item.get('line');
				if(item.get('comp_total') > 0){
					
					if(!itemComps[itemLine]){
						itemComps[itemLine] = 0;
					}	
					itemComps[itemLine] += item.get('comp_total');
				}
			});
		});
		
		if(line){
			return _.round(itemComps[line], 2);
		}
		
		return itemComps;
	},	

	allReceiptsPaid: function(){
		var complete = true;
		_.each(this.models, function(receipt){

			if(!receipt.isPaid() || (receipt.get('payments').length == 0 && receipt.get('items').length > 0)){
				complete = false;
			}
		});

		return complete;
	},

	paymentsMade: function(){
		var payments = false;
		_.each(this.models, function(receipt){
			if(receipt.get('payments').length > 0){
				payments = true;
			}
		});

		return payments;
	},
	
	calculateAllTotals: function(){
		_.each(this.models, function(receipt){
			receipt.calculateTotals();
		});
	}
});
