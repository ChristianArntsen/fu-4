var CustomerCollection = Backbone.Collection.extend({
	url: App.api_table + 'customers',
	model: function(attrs, options){
		return new Customer(attrs, options);
	},
	
	initialize: function(){
		this.on('add', this.applyDiscount);
	},

	byValidMemberBalance: function() {
		filtered = this.filter(function(customer) {
			return customer.get("member_account_balance") > 0 || customer.get("member_account_balance_allow_negative") == '1';
		});
		return new CustomerCollection(filtered);
	},

	byValidAccountBalance: function() {
		filtered = this.filter(function(customer) {
			return customer.get("account_balance") > 0 || customer.get("account_balance_allow_negative") == '1';
		});
		return new CustomerCollection(filtered);
	}
});
