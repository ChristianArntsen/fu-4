<?php $this->load->view("partial/course_header");
$holes_limit = $this->session->userdata('limit_holes');
$days_in_booking_window = $this->session->userdata('days_in_booking_window');
$teesheet_days_out = $this->session->userdata('days_out');
?>
<script>
	$(document).ready(function(){
		$('.colbox').colorbox({
			'width':650,
			'height':500,
			'onComplete':function() {
           		$.colorbox.resize();
           	}});
		/*$.ajax({
           type: "POST",
           url: "index.php/be/make_5_day_weather",
           data: '',
           success: function(response){
   			 	$('#weather').html(response.weather);
           },
           dataType:'json'
         }); */
         booking.get_available_teetimes();
         
         var tabLastWidth = 0;
         $('div.tab-container').on('click', 'div.tab-arrow', function(e){
			var left = parseInt($('div.tab-container ul.tabs').css('left').replace('px',''));
			var newWidth = parseInt($('div.tab-container ul.tabs').innerWidth());
		
			if($(this).hasClass('left')){
				if(left >= 0){
					return false;
				}
				left += 366;
				left = left + 'px';		
			}else{
				if(newWidth == tabLastWidth){
					return false;
				}				
				left -= 366;
				left = left + 'px';
				tabLastWidth = newWidth;
			}

			$('div.tab-container ul.tabs').css('left', left);
			e.preventDefault();
			return false;			
		 });
	});
    var booking = {
        teesheet_id:'<?php echo $teesheet_id ?>',
        time_range:'<?php echo $filters['filter_time_preset']?>',
        tab_index:'1',
        holes:'<?php echo $holes_limit == 9 ? 9 : 18;?>',
        available_spots:4,
        update_slider_hours:function(){
            var values = $( "#slider" ).slider( "option", "values" );
            var am_pm0 = 'a';
            var am_pm1 = 'a';
            if (values[0] > 11)
                am_pm0 = 'p';
            if (values[1] > 11)
                am_pm1 = 'p';
            if (values[0] > 12)
                values[0] = values[0]-12;
            if (values[1] > 12)
                values[1] = values[1]-12;

            $($('.ui-slider-handle')[0]).html(values[0]+am_pm0);
            $($('.ui-slider-handle')[1]).html(values[1]+am_pm1);

        },
        get_available_teetimes:function(){
            $('.tab_content').html("<div class='loading_golf_ball'><img src='<?php echo base_url(); ?>images/loading_golfball2.gif'/><div>Finding available teetimes...</div></div>");
            $.ajax({
               type: "POST",
               url: "index.php/be/get_available_teetimes",
               data: "time_range="+this.time_range+"&teesheet_id="+this.teesheet_id+"&holes="+this.holes+"&available_spots="+this.available_spots+"&tab_index="+this.tab_index,
               success: function(response){
                   booking.update_available_teetimes(response);
               }
            });
        },
        update_available_teetimes:function(ajax_response) {
            var data = eval('('+ajax_response+')');
            for (var i in data) {
                $('#tab'+(parseInt(i)+1)+'_label').html(data[i].day_label);
                var teetime_html = '';
                for (var j in data[i].teetimes) {
                    teetime_html += data[i].teetimes[j];
                }
                //console.log('#tab'+(parseInt(i)+1));
                $('#tab'+(parseInt(i)+1)).html(teetime_html);
            }
            this.add_reserve_button_styles();
            $('.colbox').colorbox({
            	'width':650,
            	'height':500,
            	'onComplete':function() {
               		$.colorbox.resize();
               	}});
        },
        open_reservation_popup:function() {

        },
        activate_filter_buttons:function() {
            //$("#"+this.time_range+'_range').click();
            $("input[name=time_button]").click(function(e){
                //console.dir($(e.target));
                var time_range = $(e.target)[0].id;
                time_range = time_range.slice(0, time_range.indexOf('_'));
                //console.log(time_range);
                booking.time_range = time_range;
                booking.get_available_teetimes();
            });
            $("#holes_"+this.holes).click();
            $("input[name=holes_button]").click(function(e){
                //console.dir($(e.target));
                var holes = $(e.target)[0].id;
                holes = holes.slice(holes.indexOf('_')+1);
                booking.holes = holes;
                booking.get_available_teetimes();
            });
            $("#player_"+this.available_spots).click();
            $("input[name=player_button]").click(function(e){
                //console.dir($(e.target));
                var players = $(e.target)[0].id;
                players = players.slice(players.indexOf('_')+1);
                //console.log(players);
                booking.available_spots = players;
                booking.get_available_teetimes();
            });
        },
        add_reserve_button_styles:function() {
            $('.reserve_button a').button();
        }
    };
    $(document).ready(function() {
        booking.activate_filter_buttons();
        //When page loads...
        $('#player_1').button();
        $('#player_2').button();
        $('#player_3').button();
        $('#player_4').button();
        $('#holes_9').button();
        $('#holes_18').button();
        booking.add_reserve_button_styles();
        //$('#reserve_button').button();
        $('#time_button_set').buttonset();
        $('#player_button_set').buttonset();
	$('#hole_button_set').buttonset();
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		booking.tab_index = activeTab.substring(4) - <?= $this->session->userdata('days_out')?>;
		booking.get_available_teetimes();
		return false;
	});

        //$('#slider').slider({min:5, max:19, range:true, values:[5,10], change:function(){booking.update_slider_hours()}});
        //booking.update_slider_hours();
});
function post_person_form_submit(response)
{
        if(!response.success)
        {
                set_feedback(response.message,'error_message',true);
        }
        else
        {
                //This is an update, just update one row
                if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
                {
                        //update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
                        set_feedback(response.message,'success_message',false);

                }
                else //refresh entire table
                {
                        //do_search(true,function()
                        //{
                                //highlight new row
                        //        highlight_row(response.person_id);
                                set_feedback(response.message,'success_message',false);
                        //});
                }
        }
}
</script>
<style>
html, table, div, .ui-widget {
		font-family:Quicksand, Helvetica,Arial, sans-serif;
	}

    html {
		background: url(<?php echo base_url(); ?>images/backgrounds/course.png)no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		font-family: Quicksand, Helvetica, Arial, sans-serif;
	}
   .ui-widget {
        font-size:.8em;
    }
    .ui-buttonset .ui-button {
        margin-right:-0.5em;
    }
    #slider {
        margin: 10px 10px 10px 20px;
        width:250px;
    }
    .ui-slider-handle {
        font-size:.8em;
        padding:1px 0 0 2px;
    }
    .tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	clear: both;
	float: left;
        width: 863px;
	background: #efefef;
        height:400px;
}
.tab_content {
	padding:0px 20px;
	font-size: 1.2em;
        height:330px;
        overflow: auto;
}
div.tab-container {
	background:url(<?php echo base_url(); ?>images/backgrounds/top_bar_bg_black.png) repeat;
	margin: 0;
	padding: 0px 0px 0px 4px;
	float: left;
	list-style: none;
	border-left: 1px solid #999;
	width: 860px;
	height: 35px;
	position: relative;	
	z-index: 0;
	border-bottom: 1px solid #999;
	overflow: hidden;	
}
div.tab-visible {
	<?php if($days_in_booking_window <= 14){ ?>
	width: 860px;		
	<?php }else{ ?>
	width: 796px;
	<?php } ?>
	height: 35px;
	display: block;
	overflow: hidden;
	float: left;
	position: relative;
	z-index: 2;
}
ul.tabs {
	display: block;
	overflow: hidden;
	height: 35px;
	margin: 0px !important;
	padding: 0px !important;
	position: absolute;
	left: 0px;
	max-width: 6000px;
	z-index: 2;
}
div.tab-container li, div.tab-container div.tab-arrow {
	float: left;
	margin: 0px 2px;
	padding: 0px;
	height: 30px; /*--Subtract 1px from the height of the unordered list--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	margin-top: 5px;
	position: relative !important;
	background: #d5d5d5;
	border-radius:5px 5px 0px 0px;
	width: 85px;
	list-style-type: none;
}
div.tab-arrow.right {
	float: right !important;
	margin-right: 4px !important;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: .9em;
	padding: 0 0px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
    text-align: center;
    line-height: 18px;
    width: auto;
	border-radius: 5px 5px 0px 0px;
}
div.tab-container div.tab-arrow {
	width: 25px;
	font-weight: bold;
	font-size: 16px;
	float: left;
	text-align: center;
	padding-top: 5px;
	cursor: pointer;
}
ul.tabs li a div {
    height:12px;
    font-size:12px;
    font-weight:bold;
}
ul.tabs li a span {
    font-size:.7em
}
ul.tabs li a:hover {
	background: #ccc;
}
label.ui-button {
	padding:0px 5px 0px;
}
.ui-button .ui-button-text {
	line-height: 15px;
}
.ui-button-text-only .ui-button-text {
	padding:5px 10px;
}
div label.ui-state-active {
	border-right: none;
	box-shadow: inset -2px 2px 30px 3px black;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#222222', endColorstr='#333333'); /* for IE */
}
.flag_icon, .time_icon, .player_icon {
	display: block;
	height: 32px;
	width: 43px;
	background: url(<?php echo base_url(); ?>images/icons/shadow_icons.png) no-repeat 0px -208px;
	float: left;
	margin: 6px 0px 0px 5px;
}
.player_icon {
	float:right;
	background-position:-4px -9px;
	margin-right:25px;
}
.flag_icon {
	background-position: 0px -162px;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #efefef;
	border-bottom: 1px solid #efefef; /*--Makes the active tab look like it's connected with its content--*/
}
#time_button_set, #hole_button_set {
    float:left;
    margin:10px 60px 0 15px;
}
#player_button_set {
    float:right;
    margin:10px 21px 0 0;
}
.search_controls {
    padding: 5px;
    border-bottom: 1px solid #ccc;
    box-shadow:0px 1px 0px 0px white;
}
.teetime {
    border:none;
    padding:8px 15px;
    background:transparent;
    width:48%;
    float:left;
    border-radius:0px;
    border-bottom:1px solid #ccc;
    border-right:1px solid #ccc;
    box-shadow:1px 1px 0px 0px white;
}
div.even {
	border-right:none 0px;
	box-shadow:0px 1px 0px 0px white;
	padding-right:0px;
}
div.odd {
	padding-left:0px;
}
.teetime .left {
    float:left;
    width:10%;
}
.teetime .center {
    float:left;
    width:75%;
    text-align: center;
}
.teetime .right {
    float:right;
    width:15%;
    margin-top:5px;
}
.teetime .left .players span {
    float: left;
}
.teetime .left .players span.ui-button-text {
    font-size: .6em;
    margin: 1px 0 0 2px;
}
.loading_golf_ball {
    font-size:14px;
    padding-top:94px;
    text-align:center;
}
.be_header {
    color:#336699;
    margin:15px auto 5px;
    text-align: center;
}
.be_form {
    width:65%;
    margin:auto;

}
.login_button {
	height:32px;
}
#employee_basic_info.login_info {
    margin-right:5px;
}
.register_form {
    width:85%;
}
.be_form td input {
    margin-top:3px;
    margin-left:5px;
}
.be_fine_print {
    color:#999999;
    font-size:11px;
    text-align:center;
}
.be_form td hr {
    color:#336699;
    margin:11px 0px 7px;
}
.be_reserve_buttonset {
    float:left;
    margin-right:22px;
}
.be_buttonset_label {
    line-height: 30px;
    color:#336699;
    margin-right:10px;
}
#cboxLoadedContent .teetime .course_name {
    font-size:1.4em;
    margin-top:6px;
}
#cboxLoadedContent .teetime .info {
    padding-left:10px;
    margin-top:7px;
}
#cboxLoadedContent .teetime .time, #cboxLoadedContent .teetime .holes, #cboxLoadedContent .teetime .date {
    color:#336699;
    font-size:1em;
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label, #content_area .teetime .spots {
    padding:2px 6px 2px 4px;
    float:left;
    line-height:17px;
    margin-right:14px;
    font-size:14px;
    font-weight:lighter;
    border:none;
   	background:transparent;
}
#content_area .teetime .price_label {
    margin-right:2px;
    padding-right:0px;
    line-height:19px;
}
#cboxLoadedContent .teetime .info .ui-icon {
    float:left;
    margin-right:5px;
}
#content_area .teetime .ui-icon {
    float:left;
    margin-right:5px;
}
#cboxLoadedContent .teetime .reservation_info {
    float:left;
    width:70%;
}
#cboxLoadedContent .teetime .price_box {
    float:right;
    text-align: right;
    width:27%;
    font-size:2.5em;
    line-height: 50px;
}
#content_area .teetime .price_details {
    padding-left: 90px;
}
#course_name {
    font-size:2em;
    text-align:center;
}
#teesheet_name {
	text-align: center;
}
.column {
	width:186px;
	margin:0 10px 0 0;
}
.booking_rules {
	width: 176px;
	padding:5px;
	font-size:11px;
}
.booking_rules p {
	margin-bottom:3px;
}
#home_module_list {
	clear:none;
	float:left;
	width:860px;
	margin-top:0px;
	margin-left:15px;
}
.wrapper {
	margin:20px auto;
	width:1100px;
}
.menu_item_home {
	height:62px;
	padding:10px 10px 0 0;
}
.dailyweather {
	width: 41px;
	float: left;
	text-align:center;
	margin-right:1px;
}
#weather {
padding: 5px;
}
div.ui-datepicker {
	width:14.4em;
	margin-bottom:10px;
}
div#booking_rules_content, div#available_deals_content {
	padding:5px;
}
.curtemp {
	font-size: 25px;
	float: right;
}
.wcity {
	font-size: 10px;
	margin-left: 20px;
	float: right;
	margin-top: -4px;
}
#hole_button_set {
	width:100px;
}
#player_button_set {
	width:140px;
}
.teetime .time {
	float:left;
	color:#303030;
}
.teetime a {
	color:#303030;
	float:right;
}
.teetime .bottom span {
	color:#303030;
}
.flag_icon_sm, .person_icon_sm, .cart_icon_sm, .no_cart_icon_sm {
	width:20px;
	height:16px;
	display:block;
	background:transparent url(<?php echo base_url(); ?>images/icons/icons.png) no-repeat -173px -10px;
	float:left;
}
.flag_icon_sm {
	width:16px;
}
.person_icon_sm {
	background-position:-173px -50px
}
.cart_icon_sm {
	background-position:-173px -92px;
}
.no_cart_icon_sm {
	background-position:-172px -131px;
}
.ui-state-default a, .ui-state-default a:link {
	background: #349ac5; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3'); /* for IE */
	background: -webkit-linear-gradient(top, #349AC5, #4173B3);
	background: -moz-linear-gradient(top,  #349AC5,  #4173B3); /* for firefox 3.6+ */
	color: white;
	display: block;
	font-size: 14px;
	font-weight: lighter;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
	padding:5px 20px;
}
#content_area {
	padding:10px;
}
#booking_rules {
	background:url(<?php echo base_url(); ?>images/backgrounds/bg3.png) repeat;
}
#booking_rules div.ui-datepicker-header {
	background:none;
	border:none;
	font-weight:lighter;
	font-size:14px;
	padding:6px 0px 8px;
}
#booking_rules_content {
	background:white;
	box-shadow:0px 1px 10px 0px #777, 0px 6px 0px -3px white, 0px 7px 10px -3px #777, 0px 11px 0px -6px white;
	margin-bottom:12px;
	padding:10px;
}
<?php if ($days_in_booking_window == 10) { ?>
ul.tabs li {
	width: 84px;
}
<?php } ?>
<?php if ($days_in_booking_window >= 14) { ?>
ul.tabs li {
	width: 56px;
}
<?php } ?>
<?php if ($this->session->userdata('iframe')) {
	// STYLES FOR AN IFRAME VERSION OF ONLINE BOOKING
	?>
#content_area {
	padding:0px;
}
#content_area_wrapper {
	min-height: 540px;
}
.column {
	float:none;
	width:auto;
	margin:0px;
}
.wrapper {
	width:758px;
	margin:0px auto;
}
ul.tabs {
	width:753px;
}
#home_module_list {
	width:758px;
	margin-left:0px;
	float:none;
}
.tab_container {
	width:756px;
}
.tab_content {
	padding:0px 10px;
}
.teetime {
	padding:8px 10px;
}
#time_button_set, #hole_button_set {
	margin: 10px 30px 0 15px;
}
div.dropdown-menu > a, #signin {
	font-size: 14px;
}
#course_name, span.customStyleSelectBox {
	font-size:12px;
}
.menu_item img {
	height: 30px;
}
#menubar_background, #menubar_full {
	padding:0px;
}
#teesheet_name, #course_name {
	line-height: 30px;
}
#navigation_menu {
	margin: 5px 15px 5px 10px;
}
#home_module_list {
	min-height: 475px;
}
#menubar_background, #menubar_full {
	display: none;
}
div.ui-datepicker {
	width: auto;
	margin-bottom: 0px;
}
<?php } ?>
<?php if ($this->session->userdata('mobile_browser') && false) {
	// STYLES FOR AN IFRAME VERSION OF ONLINE BOOKING
	?>
#content_area {
	padding:0px;
}
#content_area_wrapper {
	min-height: 540px;
}
.column {
	float:none;
	width:auto;
	margin:0px;
}
.wrapper {
	width:758px;
	margin:0px auto;
}
ul.tabs {
	width:753px;
}
#home_module_list {
	width:758px;
	margin-left:0px;
	float:none;
}
.tab_container {
	width:756px;
}
.tab_content {
	padding:0px 10px;
}
.teetime {
	padding:8px 10px;
}
#time_button_set, #hole_button_set {
	margin: 10px 30px 0 15px;
}
div.dropdown-menu > a, #signin {
	font-size: 14px;
}
#course_name, span.customStyleSelectBox {
	font-size:12px;
}
.menu_item img {
	height: 30px;
}
#menubar_background, #menubar_full {
	padding:0px;
}
#teesheet_name, #course_name {
	line-height: 30px;
}
#navigation_menu {
	margin: 5px 15px 5px 10px;
}
#home_module_list {
	min-height: 475px;
}
#menubar_background, #menubar_full {
	display: none;
}
div.ui-datepicker {
	width: auto;
	margin-bottom: 0px;
}
<?php } ?>
</style>
<!--div id="course_name"><?php echo $course_info->name?></div>
<div id="teesheet_name"><?php echo $teesheet_info->title;?></div-->
<div id="thank_you_box"></div>
<div class="wrapper">
	<div style="padding: 8px; font-size: 12px;"><a href="index.php/be/select_booking_class"><-- Back to Booking Classes</a></div>
    <?php if (!$this->session->userdata('iframe')) {
    	$this->load->view('be/booking_rules_column');
    }?>
	<div id="home_module_list">
	    <div class="tab-container">
			<?php if($days_in_booking_window > 14){ ?>
			<div class="tab-arrow left">
				&lt;				
			</div>
			<?php } ?>			
			<div class="tab-visible">
				<ul class="tabs">
					<?php for ($i = $teesheet_days_out+1; $i < $teesheet_days_out + $days_in_booking_window + 1; $i++) { ?>
					<li>
						<a href="#tab<?=$i?>" id="tab<?=$i?>_label"><?php echo $teetimes[0]['day_label'];?></a>
					</li>
					<?php } ?>        
				</ul>
			</div>
			<?php if($days_in_booking_window > 14){ ?>
			<div class="tab-arrow right">
				&gt;			
			</div>
			<?php } ?>
	    </div>

	    <div class="tab_container">
	        <div class="search_controls">
	        	<span class='time_icon'></span>
	            <div id="time_button_set">
	                <input type="radio" id="morning_range" name="time_button" <?php echo ($filters['filter_time_preset'] == 'morning')?'checked':'';?>/><label for="morning_range">Morning</label>
	                <input type="radio" id="midday_range" name="time_button" <?php echo ($filters['filter_time_preset'] == 'midday')?'checked':'';?>/><label for="midday_range">Midday</label>
	                <input type="radio" id="evening_range" name="time_button" <?php echo ($filters['filter_time_preset'] == 'evening')?'checked':'';?>/><label for="evening_range">Evening</label>
	            </div>
	            <!--div id="slider"></div-->
	            <span class='flag_icon'></span>
	            <div id="hole_button_set">
	            	<?php
	            	if ($holes_limit == 9 || $holes_limit == 0) { ?>
	                <input type="radio" id="holes_9" name="holes_button"/><label for="holes_9">9</label>
	                <?php }
					if ($holes_limit == 18 || $holes_limit == 0) { ?>
	                <input type="radio" id="holes_18" name="holes_button" checked/><label for="holes_18">18</label>
	                <?php } ?>
	            </div>
	            <div id="player_button_set">
	            	<?php
	            	$minimum_players = $this->session->userdata('minimum_players');
	            	if ($minimum_players == 1) { ?>
	                <input type="radio" id="player_1" name="player_button"/><label for="player_1">1</label>
	                <?php }
					if ($minimum_players < 3) { ?>
	                <input type="radio" id="player_2" name="player_button"/><label for="player_2">2</label>
	                <?php }
					if ($minimum_players < 4) { ?>
	                <input type="radio" id="player_3" name="player_button"/><label for="player_3">3</label>
	                <?php } ?>
	                <input type="radio" id="player_4" name="player_button" checked/><label for="player_4">4</label>
	            </div>
	            <span class='player_icon'></span>
	            <div class="clear"></div>
	        </div>
	        <?php for ($i = $teesheet_days_out+1; $i < $teesheet_days_out + $days_in_booking_window + 1; $i++) { ?>
	        <div id="tab<?=$i?>" class="tab_content">
	            <!--Content-->
	            <?php
	            	echo $teetimes[0]['deals'];
	            	if (count($teetimes[0]['teetimes']) > 0)
		                foreach ($teetimes[0]['teetimes'] as $teetime)
		                    echo $teetime;
					else
						echo 'No teetimes available. Please select a different day/time.';
	            ?>
	        </div>
	        <?php } ?>
	    </div>
	</div>
	<?php if ($this->session->userdata('iframe')) {
    	$this->load->view('be/booking_rules_column');
    }?>
</div>
<?php //$this->load->view("partial/course_footer"); ?>
