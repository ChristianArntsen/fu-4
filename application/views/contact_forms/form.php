<!DOCTYPE html>
<html>
<head>
	<title>Contact Form</title>
	<style>
		body {
			font-family: sans-serif;
			color: #333;
		}

		fieldset {
			position: relative;
			border: none;
		}

		legend {
			font-size: 2em;
			font-weight: bold;
			-webkit-padding-start: 0px;
		}

		p {
		}

		label {
			display: inline-block;
			width: 8em;
		}

		label::after {
			content: ":";
		}

		input {
			font-size: 1em;
		}

		select {
			font-size: 1em;
		}

		textarea {
			vertical-align: top;
			font-size: 1em;
		}

		[required]:after {
			content: "*";
			color: #800;
		}

		[required] {
			border: 1px solid #800;
		}

		#thanks {
			display: none;
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			background-color: rgba(255, 255, 255, 0.8);
			text-align: center;
			font-size: 2em;
			font-weight: bold;
			line-height: 8em;
		}
	</style>
	<script>
		/*! H5F
		 * https://github.com/ryanseddon/H5F/
		 * Copyright (c) Ryan Seddon | Licensed MIT */
		(function(e,t){"function"==typeof define&&define.amd?define(t):"object"==typeof module&&module.exports?module.exports=t():e.H5F=t()})(this,function(){var e,t,a,i,n,r,l,s,o,u,d,c,v,p,f,m,b,h,g,y,w,C,N,A,E,$,x=document,k=x.createElement("input"),q=/^[a-zA-Z0-9.!#$%&'*+-\/=?\^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,M=/[a-z][\-\.+a-z]*:\/\//i,L=/^(input|select|textarea)$/i;return r=function(e,t){var a=!e.nodeType||!1,i={validClass:"valid",invalidClass:"error",requiredClass:"required",placeholderClass:"placeholder",onSubmit:Function.prototype,onInvalid:Function.prototype};if("object"==typeof t)for(var r in i)t[r]===void 0&&(t[r]=i[r]);if(n=t||i,a)for(var s=0,o=e.length;o>s;s++)l(e[s]);else l(e)},l=function(a){var i,r=a.elements,l=r.length,c=!!a.attributes.novalidate;if(g(a,"invalid",o,!0),g(a,"blur",o,!0),g(a,"input",o,!0),g(a,"keyup",o,!0),g(a,"focus",o,!0),g(a,"change",o,!0),g(a,"click",u,!0),g(a,"submit",function(i){return e=!0,t||c||a.checkValidity()?(n.onSubmit.call(a,i),void 0):(w(i),void 0)},!1),!v())for(a.checkValidity=function(){return d(a)};l--;)i=!!r[l].attributes.required,"fieldset"!==r[l].nodeName.toLowerCase()&&s(r[l])},s=function(e){var t=e,a=h(t),n={type:t.getAttribute("type"),pattern:t.getAttribute("pattern"),placeholder:t.getAttribute("placeholder")},r=/^(email|url)$/i,l=/^(input|keyup)$/i,s=r.test(n.type)?n.type:n.pattern?n.pattern:!1,o=p(t,s),u=m(t,"step"),v=m(t,"min"),b=m(t,"max"),g=!(""===t.validationMessage||void 0===t.validationMessage);t.checkValidity=function(){return d.call(this,t)},t.setCustomValidity=function(e){c.call(t,e)},t.validity={valueMissing:a,patternMismatch:o,rangeUnderflow:v,rangeOverflow:b,stepMismatch:u,customError:g,valid:!(a||o||u||v||b||g)},n.placeholder&&!l.test(i)&&f(t)},o=function(e){var t=C(e)||e,a=/^(input|keyup|focusin|focus|change)$/i,r=/^(submit|image|button|reset)$/i,l=/^(checkbox|radio)$/i,u=!0;!L.test(t.nodeName)||r.test(t.type)||r.test(t.nodeName)||(i=e.type,v()||s(t),t.validity.valid&&(""!==t.value||l.test(t.type))||t.value!==t.getAttribute("placeholder")&&t.validity.valid?(A(t,[n.invalidClass,n.requiredClass]),N(t,n.validClass)):a.test(i)?t.validity.valueMissing&&A(t,[n.requiredClass,n.invalidClass,n.validClass]):t.validity.valueMissing?(A(t,[n.invalidClass,n.validClass]),N(t,n.requiredClass)):t.validity.valid||(A(t,[n.validClass,n.requiredClass]),N(t,n.invalidClass)),"input"===i&&u&&(y(t.form,"keyup",o,!0),u=!1))},d=function(t){var a,i,r,l,s,u=!1;if("form"===t.nodeName.toLowerCase()){a=t.elements;for(var d=0,c=a.length;c>d;d++)i=a[d],r=!!i.attributes.disabled,l=!!i.attributes.required,s=!!i.attributes.pattern,"fieldset"!==i.nodeName.toLowerCase()&&!r&&(l||s&&l)&&(o(i),i.validity.valid||u||(e&&i.focus(),u=!0,n.onInvalid.call(t,i)));return!u}return o(t),t.validity.valid},c=function(e){var t=this;t.validationMessage=e},u=function(e){var a=C(e);a.attributes.formnovalidate&&"submit"===a.type&&(t=!0)},v=function(){return E(k,"validity")&&E(k,"checkValidity")},p=function(e,t){if("email"===t)return!q.test(e.value);if("url"===t)return!M.test(e.value);if(t){var i=e.getAttribute("placeholder"),n=e.value;return a=RegExp("^(?:"+t+")$"),n===i?!1:""===n?!1:!a.test(e.value)}return!1},f=function(e){var t={placeholder:e.getAttribute("placeholder")},a=/^(focus|focusin|submit)$/i,r=/^(input|textarea)$/i,l=/^password$/i,s=!!("placeholder"in k);s||!r.test(e.nodeName)||l.test(e.type)||(""!==e.value||a.test(i)?e.value===t.placeholder&&a.test(i)&&(e.value="",A(e,n.placeholderClass)):(e.value=t.placeholder,g(e.form,"submit",function(){i="submit",f(e)},!0),N(e,n.placeholderClass)))},m=function(e,t){var a=parseInt(e.getAttribute("min"),10)||0,i=parseInt(e.getAttribute("max"),10)||!1,n=parseInt(e.getAttribute("step"),10)||1,r=parseInt(e.value,10),l=(r-a)%n;return h(e)||isNaN(r)?"number"===e.getAttribute("type")?!0:!1:"step"===t?e.getAttribute("step")?0!==l:!1:"min"===t?e.getAttribute("min")?a>r:!1:"max"===t?e.getAttribute("max")?r>i:!1:void 0},b=function(e){var t=!!e.attributes.required;return t?h(e):!1},h=function(e){var t=e.getAttribute("placeholder"),a=/^(checkbox|radio)$/i,i=!!e.attributes.required;return!(!i||""!==e.value&&e.value!==t&&(!a.test(e.type)||$(e)))},g=function(e,t,a,i){E(window,"addEventListener")?e.addEventListener(t,a,i):E(window,"attachEvent")&&window.event!==void 0&&("blur"===t?t="focusout":"focus"===t&&(t="focusin"),e.attachEvent("on"+t,a))},y=function(e,t,a,i){E(window,"removeEventListener")?e.removeEventListener(t,a,i):E(window,"detachEvent")&&window.event!==void 0&&e.detachEvent("on"+t,a)},w=function(e){e=e||window.event,e.stopPropagation&&e.preventDefault?(e.stopPropagation(),e.preventDefault()):(e.cancelBubble=!0,e.returnValue=!1)},C=function(e){return e=e||window.event,e.target||e.srcElement},N=function(e,t){var a;e.className?(a=RegExp("(^|\\s)"+t+"(\\s|$)"),a.test(e.className)||(e.className+=" "+t)):e.className=t},A=function(e,t){var a,i,n="object"==typeof t?t.length:1,r=n;if(e.className)if(e.className===t)e.className="";else for(;n--;)a=RegExp("(^|\\s)"+(r>1?t[n]:t)+"(\\s|$)"),i=e.className.match(a),i&&3===i.length&&(e.className=e.className.replace(a,i[1]&&i[2]?" ":""))},E=function(e,t){var a=typeof e[t],i=RegExp("^function|object$","i");return!!(i.test(a)&&e[t]||"unknown"===a)},$=function(e){for(var t=document.getElementsByName(e.name),a=0;t.length>a;a++)if(t[a].checked)return!0;return!1},{setup:r}});
	</script>
	<script>
		window.addEventListener('DOMContentLoaded', function () {
			var form = document.getElementById('contact_form');
      H5F.setup(form, {
        onSubmit: function (event) {
					var serialized = {}, element, i, xhr;

			event.preventDefault();

			for (i = 0; i < form.elements.length; i++) {
				element = form.elements[i];
				if (element.name) {
					serialized[element.name] = element.value;
				}
			}

			xhr = new XMLHttpRequest();
			xhr.open('POST', form.action);
			xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

			xhr.onreadystatechange = function () {
				var thanks;
				if (xhr.readyState == 4) {
					thanks = document.getElementById('thanks')
					thanks.setAttribute('style', 'display:block;');
					window.setTimeout(function () {
						thanks.setAttribute('style', '');
						form.reset();
					}, 5000);
				}
			};

			xhr.send(JSON.stringify(serialized));
        }
      });
		});
	</script>
</head>
<body>
<form action="<?php echo $form_url ?>" id="contact_form">
	<fieldset>
		<legend>Contact Us</legend>
		<?php
		if ($contact_form['has_first_name']) {
			echo '<p><label for="first_name">First Name</label><input type="text" id="first_name" name="first_name" '.$contact_form["req_first_name"].'></p>';
		}

		if ($contact_form['has_last_name']) {
			echo '<p><label for="last_name">Last Name</label><input type="text" id="last_name" name="last_name" '.$contact_form["req_last_name"].'></p>';
		}

		if ($contact_form['has_email']) {
			echo '<p><label for="email">Email</label><input type="email" id="email" name="email" '.$contact_form["req_email"].'></p>';
		}

		if ($contact_form['has_phone_number']) {
			echo '<p><label for="phone_number">Phone</label><input type="tel" id="phone_number" name="phone_number" pattern="\d{10}" '.$contact_form["req_phone_number"].'></p>';
		}

		if ($contact_form['has_birthday']) {
			echo '<p><label for="birthday">Birthday</label><input type="date" id="birthday" name="birthday" '.$contact_form["req_birthday"].'></p>';
		}

		if ($contact_form['has_address']) {
			echo '<p><label for="address">Address</label><input type="text" id="address" name="address" '.$contact_form["req_address"].'></p>';
		}

		if ($contact_form['has_city']) {
			echo '<p><label for="city">City</label><input type="text" id="city" name="city" '.$contact_form["req_city"].'></p>';
		}

		if ($contact_form['has_state']) {
			echo '		<p>
			<label for="state">State</label>
			<select id="state" name="state" '.$contact_form["req_state"].'>
				<option value="" selected>--</option>
				<option value="AL">Alabama</option>
				<option value="AK">Alaska</option>
				<option value="AZ">Arizona</option>
				<option value="AR">Arkansas</option>
				<option value="CA">California</option>
				<option value="CO">Colorado</option>
				<option value="CT">Connecticut</option>
				<option value="DE">Delaware</option>
				<option value="DC">District Of Columbia</option>
				<option value="FL">Florida</option>
				<option value="GA">Georgia</option>
				<option value="HI">Hawaii</option>
				<option value="ID">Idaho</option>
				<option value="IL">Illinois</option>
				<option value="IN">Indiana</option>
				<option value="IA">Iowa</option>
				<option value="KS">Kansas</option>
				<option value="KY">Kentucky</option>
				<option value="LA">Louisiana</option>
				<option value="ME">Maine</option>
				<option value="MD">Maryland</option>
				<option value="MA">Massachusetts</option>
				<option value="MI">Michigan</option>
				<option value="MN">Minnesota</option>
				<option value="MS">Mississippi</option>
				<option value="MO">Missouri</option>
				<option value="MT">Montana</option>
				<option value="NE">Nebraska</option>
				<option value="NV">Nevada</option>
				<option value="NH">New Hampshire</option>
				<option value="NJ">New Jersey</option>
				<option value="NM">New Mexico</option>
				<option value="NY">New York</option>
				<option value="NC">North Carolina</option>
				<option value="ND">North Dakota</option>
				<option value="OH">Ohio</option>
				<option value="OK">Oklahoma</option>
				<option value="OR">Oregon</option>
				<option value="PA">Pennsylvania</option>
				<option value="RI">Rhode Island</option>
				<option value="SC">South Carolina</option>
				<option value="SD">South Dakota</option>
				<option value="TN">Tennessee</option>
				<option value="TX">Texas</option>
				<option value="UT">Utah</option>
				<option value="VT">Vermont</option>
				<option value="VA">Virginia</option>
				<option value="WA">Washington</option>
				<option value="WV">West Virginia</option>
				<option value="WI">Wisconsin</option>
				<option value="WY">Wyoming</option>
			</select>
		</p>';
		}

		if ($contact_form['has_zip']) {
			echo '<p><label for="zip">Zip</label><input type="text" id="zip" name="zip" '.$contact_form["req_zip"].'></p>';
		}

		if ($contact_form['has_message']) {
			echo '<p><label for="message">Message</label><textarea id="message" name="message" maxlength="255" '.$contact_form["req_message"].'></textarea></p>';
		}
		?>

		<p><input type="submit"></p>
	</fieldset>
</form>
	<div id="thanks">
		<p>Thank You!</p>
	</div>
</body>
</html>
