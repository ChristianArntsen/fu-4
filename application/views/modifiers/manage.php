<style>
#modifiers, #modifiers td {
	background-color: white;
}

#modifiers td {
    background-color: white;
    border-bottom: 1px solid #B2B2B2;
    border-right: 1px solid #B2B2B2;
    color: #555555;
    font-size: 14px;
    height: 30px;
    padding: 0 6px;
    vertical-align: middle;
}

#modifiers th {
    background: url("<?php echo base_url(); ?>images/backgrounds/paper_edge_gray.png") repeat-x scroll 0 -2px #CCCCCC;
    border-right: 1px solid #BBBBBB;
    color: #333333;
    font-size: 16px;
    height: 30px;
    padding: 0 6px;
    text-align: left;
}

#modifiers tr.editing td {
	background-color: #F0F0F0 !important;
	padding: 5px !important;
}	

#add_modifier .submit_button {
	margin: 18px 0px 0px 0px !important;
	float: left;
}

#add_modifier label {
	display: block;
	line-height: 11px;
	padding: 4px 0px;
	font-size: 12px;
}

#add_modifier .form_field {
	float: left;
	margin-right: 10px;
}

#add_modifier input {
	display: block;
}	

#add_modifier {
	padding: 10px;
	overflow: hidden;
}

div.option_row {
	overflow: hidden;
	height: 35px;
	line-height: 35px;
	display: block;
}

div.option_row a.delete_option {
	line-height: 34px;
	font-size: 16px;
	margin-left: 2px;
}

div.option_row input {
	margin-bottom: 0px;
} 

.form_field.name input {
	width: 200px;
}

.form_field.options input {
	width: 150px;
	margin-right:5px;
	margin-bottom:5px;
	float:left;
}

.form_field.price input {
	width: 60px;
}
.add_option_button{
	cursor:pointer;
	clear:both;
}
</style>
<form method="post" id="add_modifier" action="<?php echo site_url('modifiers/save'); ?>">
	<div class='form_field'>
		<label>Name</label>
		<input style="width: 200px;" type="text" name="name" value="" />
	</div>
	<div class='form_field options'>
		<label>Options</label>
		<div class="option_row">
			<input placeholder="Option" style="width: 150px;" type="text" name="options_label[]" value="" />
			<input placeholder="Price (optional)" style="width: 150px;" type="text" name="options_price[]" value="" />
		</div>
		<div class="add_option_button">Add Option</div>
	</div>
	<div class='form_field'>
		<label for="multi_select">Multi-select</label>
		<?php echo form_checkbox('multi_select', 1, 0); ?>
	</div>	
	<div class='form_field'>
		<label>Category</label>
		<?php $category_array = array('1'=>'Customize', '2'=>'Condiments', '3'=>'Cook Temp');?>
		<?php echo form_dropdown('category_id', $category_array); ?>
	</div>
	<div class='form_field'>
		<label>Required</label>
		<?php echo form_dropdown('required', array('0'=>'No','1'=>'Yes')); ?>
	</div>	
	<input type="submit" class="submit_button" value="Add New" style="float: right;" />
</form>
<table id="modifiers">
	<thead class="fixedHeader">
		<tr>
			<th style="width: 225px;">Name</th>
			<th style="width: 375px;">Options</th>
			<th style="width: 75px;">Multi</th>
			<th style="width: 75px;">Category</th>
			<th style="width: 75px;">Required</th>
			<th style="width: 40px;">&nbsp;</th>
			<th style="width: 20px;">&nbsp;</th>
		</tr>
	</thead>
	<tbody>			
		<?php if(!empty($modifiers)){ ?>
		<?php foreach($modifiers as $key => $modifier){ ?>
		<tr data-modifier-id="<?php echo $modifier['modifier_id']; ?>">
			<td>
				<div class='form_field name'>
					<span class="data" data-name="name"><?php echo $modifier['name']; ?></span>
				</div>
			</td>
			<td>
				<div class='form_field options'>
					<span class="data" data-name="options" data-options="<?=json_encode($modifier['options']);?>">
						<?php if(!empty($modifier['options'])){ ?>
						<?php foreach ($modifier['options'] as $option){
							echo '<span class="option-label">'.$option['label'].'</span>'.($option['price'] != '<span class="option-price"></span>' ? ' ($<span class="option-price">'.$option['price'].'</span>)' : '').',  ';
						} } ?>
					</span>
				</div>
			</td>				
			<td>
				<div class='form_field multi-select'>
					<span class="data" data-name="multi_select" data-multi-select="<?=$modifier['multi_select']?>"><?php if($modifier['multi_select'] == 1){ ?>Yes<?php }else{ ?>No<?php } ?></span>
				</div>
			</td>			
			<td>
				<div class='form_field price'>
					<span class="data" data-name="category_id" data-category-id="<?=$modifier['category_id']?>"><?php echo $category_array[$modifier['category_id']]; ?></span>
				</div>
			</td>
			<td>
				<div class='form_field price'>
					<span class="data" data-name="required" data-required='<?=$modifier['required']?>'><?php echo $modifier['required'] ? 'Yes' : 'No'; ?></span>
				</div>
			</td>
			<td class="edit">
				<a href="#" title="Edit this modifier" class="edit_modifier">Edit</a>
			</td>			
			<td class="delete">
				<a href="#" class="delete_modifier" title="Delete this modifier" style="color: red;">X</a>
			</td>
		</tr>
		<?php } }else{ ?>
		
		<?php } ?>
	</tbody>
</table>
<script>
function initialize_add_option_button()
{
	$('.add_option_button').die('click').live('click', function (e){
		var new_row = '<div class="option_row"><input type="text" placeholder="Option" name="options_label[]" value="" />'+
			'<input type="text" placeholder="Price (optional)" name="options_price[]" value="" />'+
			'<a href="#" class="delete_option" title="Delete this option" style="color: red; line-height: 25px;">X</a></div>';
		$(this).before(new_row);
		$.colorbox.resize();
	});
}
$(function(){
	initialize_add_option_button();
	$('#add_modifier').submit(function(e){
		var url = $(this).attr('action');
		var data = $(this).serialize();
		
		$.post(url, data, function(response){
			$('#modifiers > tbody').append(response);
			$(this).find('input.extra_option').each(function(index, val){$(this).remove();});
			$.colorbox.resize();
		}, 'html');
		
		$(this)[0].reset();
		
		return false;
	});
	
	$('a.delete_modifier').die('click').live('click', function(e){
		var row = $(this).parents('tr');
		var modifierId = row.attr('data-modifier-id');
		var url = '<?php echo site_url('modifiers/delete'); ?>/' + modifierId;
		
		$.post(url, null, function(response){
			if(response.success){
				row.remove();
				$.colorbox.resize();
			}
		},'json');
		
		return false;
	});
	
	$('a.delete_option').die('click').live('click', function(e){
		$(this).parent('div').remove();
		return false;
	});

	$('a.edit_modifier').die('click').live('click', function(e){
		var row = $(this).parents('tr');
		row.addClass('editing');
		row.find('td.delete').remove();
		row.find('td.edit').attr('colspan',2);
		
		row.find('span.data').each(function(index, val){
			
			var fieldName = $(this).attr('data-name');
			var fieldData = $(this).text();
			
			if (fieldName == 'options'){
				var labels = $(this).find('span.option-label');
				var prices = $(this).find('span.option-price');
				var options_html = '';
				
				$.each(labels, function(index, el){
					var label = $(this).text();
					var price = prices.eq(index).text();
					
					options_html += '<div class="option_row"><input class="extra_option" type="text" placeholder="Option" name="options_label[]" value="'+label+'" />'+
						'<input class="extra_option" type="text" placeholder="Price (optional)" name="options_price[]" value="'+(price == undefined ? '' : price)+'" />'+
						'<a href="#" class="delete_option" title="Delete this option" style="color: red; line-height: 25px;">X</a></div>';
				});
				
				options_html += '<div class="add_option_button">Add Option</div>';
				var textField = $(options_html);			
			
			}else if (fieldName == 'category_id'){
				var category_id = $(this).attr('data-category-id');
				var textField = $('<select name="'+fieldName+'"><option value="1" '+(category_id == 1 ? 'selected' : '')+'>Customize</option><option value="2" '+(category_id == 2 ? 'selected' : '')+'>Condiments</option><option value="3" '+(category_id == 3 ? 'selected' : '')+'>Cook  Temp</option></select>');
			
			}else if (fieldName == 'required'){
				var required = $(this).attr('data-required');
				var textField = $('<select name="'+fieldName+'"><option value="0" '+(required == 0 ? 'selected' : '')+'>No</option><option value="1" '+(required == 1 ? 'selected' : '')+'>Yes</option></select>');
			
			}else if (fieldName == 'multi_select'){
				var checked = '';
				if(fieldData == 'Yes'){
					checked = 'checked';
				}
				var textField = $('<input type="hidden" name="'+fieldName+'" value="0"><input type="checkbox" name="'+fieldName+'" value="1" '+checked+'>');
			
			}else{
				var textField = $('<input type="text" name="'+fieldName+'" value="'+fieldData+'" />');
			}
			
			$(this).replaceWith(textField);
			initialize_add_option_button();
		});
		
		row.find('a.edit_modifier').replaceWith('<a href="#" class="save_modifier">Save</a>');
		$.colorbox.resize();
		return false;
	});
	
	$('a.save_modifier').die('click').live('click', function(e){
		var row = $(this).parents('tr');
		var data = row.find('input, select').serialize();
		
		$.post('<?php echo site_url('modifiers/save'); ?>/' + row.attr('data-modifier-id'), data, function(response){
			row.replaceWith(response);
			$.colorbox.resize();
		},'html');
		
		return false;
	});	
});
</script>
