<style>
#sales_item_modifiers {
	display: block;
	margin: 10px;
	padding: 0px;
	overflow: hidden;
	widthL auto;
}

#sales_item_modifiers li {
	display: block;
	padding: 5px;
	margin: 0px;
	list-style-type: none;
	font-size: 18px;
	overflow: hidden;
}

#sales_item_modifiers span {
	float: left;
	display: block;
	height: 34px;
	line-height: 34px;
}

#sales_item_modifiers span.value {
	float: none;
	display: inline;
}

#sales_item_modifiers span.name {
	width: 300px;
}

#sales_item_modifiers span.options {
	width: 450px;
	float: right;
}

#sales_item_modifiers span.options a {
	float: right;
}

#sales_item_modifiers span.price {
	width: 65px;
	text-align: right;
}

a.option_button {
    background: -moz-linear-gradient(center top , #349AC5, #4173B3) repeat scroll 0 0 transparent;
    background: -webkit-gradient(linear, center top, center bottom, color-stop(0%, #349AC5), color-stop(100%, #4173B3));
    border: 1px solid #232323;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 1px 1px 0 rgba(255, 255, 255, 0.5) inset, 0 3px 1px -2px rgba(255, 255, 255, 0.2);
    color: white;
    display: block;
    font-size: 16px;
	float: left;
    font-weight: normal;
    height: 30px;
    line-height: 30px;
    margin: 0px;
    padding: 0 20px;
    text-align: center;
    text-shadow: 0 -1px 0 black;
}

a.option_button.selected {
	box-shadow: 0 5px 20px -5px black inset;
}
</style>
<form id="sale_item_modifiers" action="<?php echo site_url('sales/save_item_modifiers'); ?>/<?php echo $cart_line; ?>" method="post" style="width: auto; display: block; overflow: hidden;">
	<ul id="sales_item_modifiers">
		<?php if(!empty($modifiers)){ ?>
		<?php foreach($modifiers as $modifier){
		if($modifier['selected_option'] == 'no'){
			$isSelected = false;
			$modifier['selected_price'] = '0.00';
		}else{
			$isSelected = true;
		} ?>
		<li>
			<span class="name"><?php echo $modifier['name']; ?></span>
			<span class="price">
				$<span class="value"><?php echo $modifier['selected_price']; ?></span>
			</span>
			<span class="options">
				<?php foreach(array_reverse($modifier['options']) as $key => $option){
					echo $modifier['selected'];
					if($modifier['selected_option'] == trim($option)){
						$btnClass = " selected";
					}else{
						$btnClass = "";
					} ?>
					<a data-price="<?php echo $modifier['price']; ?>" class="option_button<?php echo $btnClass; ?>" data-value="<?php echo trim($option); ?>" href="#">
						<?php echo ucfirst($option); ?>
					</a>
				<?php } ?>
			</span>
			<input class="selected_option" type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][selected_option]" value="<?php echo $modifier['selected_option']; ?>" />
			<input class="selected_price" type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][selected_price]" value="<?php echo $modifier['selected_price']; ?>" />
			<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][price]" value="<?php echo $modifier['price']; ?>" />
			<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][name]" value="<?php echo $modifier['name']; ?>" />
			<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][modifier_id]" value="<?php echo $modifier['modifier_id']; ?>" />
			<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][options]" value="<?php echo implode(',', $modifier['options']); ?>" />
			<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][item_id]" value="<?php echo implode(',', $modifier['item_id']); ?>" />
		</li>
	<?php } }else{ ?>
	<li>No modifiers available</li>
	<?php } ?>
	</ul>
<input type="submit" value="Save Modifiers" class="submit_button" />
</form>

<script>
$(function(){
	$('a.option_button').live('click', function(e){
		var price = $(this).attr('data-price');
		var selectedOption = $(this).attr('data-value');

		if(selectedOption == 'no'){
			price = '0.00';
		}

		$(this).addClass('selected');
		$(this).siblings().removeClass('selected');
		$(this).parents('span').siblings('span.price').find('span.value').text(price);
		$(this).parents('li').find('input.selected_option').val(selectedOption);
		$(this).parents('li').find('input.selected_price').val(price);
		return false;
	});

	$('#sale_item_modifiers').submit(function(e){
		var url = $(this).attr('action');
		var data = $(this).serialize();

		$.post(url, data, function(response){
			$.colorbox.close();
			sales.update_cart_info(<?php echo ($cart_line) ?>, response.item_info);
			sales.update_basket_totals(<?php echo ($cart_line) ?>, response.basket_info);
		},'json');

		return false;
	});
});
</script>