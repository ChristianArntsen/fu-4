<div class="wrapper superadmin">
  <?= form_open('support/save/',array('id'=>'help_form')); ?>
    <?= form_dropdown(
          'topic_type',
          array('question'=>'Question','tutorial'=>'Tutorial'),
          'question',
          'id="topic_type"');
    ?>
    <?= form_dropdown(
          'topic_module',
          $module_options,
          '',
          'id="topic_module"');
    ?>
    <?= form_input(array(
      'name'=>'topic_title',
      'size'=>'25',
      'id'=>'topic_title',
      'value'=> 'How do I issue a return?')
    );?>
    <br/><br/>
    <?= form_textarea(array(
      'name'=>'topic_content',
      'size'=>'25',
      'rows'  => 10,
      'cols'  => 25,
      'id'=>'topic_content',
      'value'=>'')
    );?>
    <br/>
    Keywords: <?= form_input(array(
      'name'=>'topic_keywords',
      'size'=>'10',
      'id'=>'topic_keywords',
      'value'=> '')
    );?>
    <br/>
    <?= form_input(array(
      'name'=>'topic_id',
      'id'=>'topic_id',
      'class'=>'hidden')
    );?>

    <br/>  
  <div id="relevant-article" class="hidden">
    Related Articles:<br/>
    <ul>
    <?php
      $num = 0;
      while($num < 5)
      {
        echo '<li>';
        echo '<div class="each-ra">';
        echo '<div id="ra-mask-'.$num.'" class="each-ra-mask"></div>';
        echo form_input(array(
          'name'=>'topic_related_articles[]',
          'size'=>'10',
          'id'=>'topic_related_articles-'.$num,
          'value'=> '',
          'class' => 'cu-related-articles')
        );
        echo '</div>';
        echo '</li>';
      $num++;
      }
    ?>
    </ul>
  </div>
  <br/><br/>
    <?= form_submit(array(
       'name'=>'articleSubmitButton',
       'id'=>'articleSubmitButton',
       'value'=> lang('common_submit'),
       'class'=>'submit_button float_right hidden')
    );
    ?>
  <?= form_close(); ?>
</div>
<br/>


<script type="text/javascript">
$(document).ready(function(){
  $('#topic_title').blur(function(){
     $('#relevant-article').removeClass('hidden');

     var title = $('#topic_title').val();
     title = $.trim(title);

     $.ajax({
          url: 'index.php/support/get_related_articles',
          type: "POST",
          data: {
            title: title
          },
          success: function( res ) {
            for(i=0;i<res.length;i++)
            {
              var input_id = '#topic_related_articles-' + i;
              var mask_id = '#ra-mask-' + i;
              $(input_id).val(res[i].title);
              $(mask_id).html(res[i].id);
            }
        },
        dataType:'json'
     });
  });
  $(".cu-related-articles").focus(function(){
    $(this).val('');
  });

  // attached auto search listener
  for(j=0;j<5;j++)
  {
    var input_id = '#topic_related_articles-' + j;

    $(input_id).autocomplete({
      source: 'index.php/support/auto_complete',
      delay: 200,
      minLength: 2,
      select: function(event, ui) {
        $(this).val(ui.item.value);
        var t = $(this).attr('id');
        var s = t.split('-');
        var mask_id = '#ra-mask-' + s[1];
        $(mask_id).html(ui.item.topic_id);
      },
      focus: function(event, ui) { return false; }
    }).data( "autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item.label + "</a>" )
        .appendTo( ul );
    };
  }

});

var obj_to_string = function(obj)
{
  var s = '{';

  for(i in obj)
  {
      s = s + '"'+i+'":"'+obj[i]+'",';
  }

  s = s.substring(0, s.length-1);
  s = s + '}';

  return s;
}

</script>