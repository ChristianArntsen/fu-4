<ul class="dropdown-menu app-header-list" role="menu">

    <?php
    $allowed_modules = ($this->Module->get_allowed_modules($this->session->userdata('person_id')));
    foreach($allowed_modules->result() as $module)
    {
        $module_list[] = $module->module_id;
    }
    ?>
    <li>
        <a id="timeclock_button">
            <div class="app-icon app-icon-time-clock"></div>
            <div class="app-content">
                <div class="app-name">
                    <?php echo lang("module_timeclock") ?>
                </div>
                <div class="app-description">
                    <?php echo lang('module_timeclock_desc');?>
                </div>
            </div>
        </a>
    </li>
    <script>
        $('#timeclock_button').colorbox({width:500,title:'Time Clock', href:'<?php echo site_url("timeclock");?>'});
    </script>
    <?php
    if ($this->permissions->is_super_admin() || ($this->permissions->course_has_module('employees') && in_array('config',$module_list)))
    {

        ?>
        <li>
            <a href="<?php echo site_url("employees");?>"
                <?php if(!empty($_GET['in_iframe'])){ ?>onClick="window.parent.location = this.getAttribute('href'); return false;"<?php } ?>>
                <div class="app-icon app-icon-employees"></div>
                <div class="app-content">
                    <div class="app-name">
                        <?php echo lang("module_employees") ?>
                    </div>
                    <div class="app-description">
                        <?php echo lang('module_employees_desc');?>
                    </div>
                </div>
            </a>
        </li>
    <?php
    }
    ?>
    <?php
    if ($this->permissions->is_super_admin() || ($this->permissions->course_has_module('config') && in_array('config',$module_list)))
    {

        ?>
        <li>
            <a href="<?php echo site_url("config");?>" 
                <?php if(!empty($_GET['in_iframe'])){ ?>onClick="window.parent.location = this.getAttribute('href'); return false;"<?php } ?>>
                <div class="app-icon app-icon-config"></div>
                <div class="app-content">
                    <div class="app-name">
                        <?php echo lang("module_config") ?>
                    </div>
                    <div class="app-description">
                        <?php echo lang('module_config_desc');?>
                    </div>
                </div>
            </a>
        </li>
    <?php
    }
    ?>
    <li>
        <a href="<?php echo site_url("support");?>" target='_blank'>
            <div class="app-icon app-icon-support"></div>
            <div class="app-content">
                <div class="app-name">Support</div>
                <div class="app-description">Help section</div>
            </div>
        </a>
    </li>
    <li>
        <a id="chat-support" onclick="olark('api.box.expand')">
            <div class="app-icon app-icon-support"></div>
            <div class="app-content">
                <div class="app-name">Chat</div>
                <div class="app-description">Chat support reps</div>
            </div>
        </a>
    </li>
    <li>
        <a id="uservoice-feedback" data-uv-trigger="smartvote">

            <div class="app-icon app-icon-support"></div>
            <div class="app-content">
                <div class="app-name">Feedback</div>
                <div class="app-description">Submit feedback</div>
            </div>
        </a>
    </li>


    <?php
    $has_sales_permissions = false;
    $has_sales_permission = false;
    $allowed_modules = $this->Module->get_allowed_modules($this->session->userdata('person_id'));
    foreach ($allowed_modules->result_array() as $allowed_module)
    {
        $has_sales_permission = ($has_sales_permission ? $has_sales_permission : $allowed_module['module_id'] == 'sales');
    }

    $logout_href = "";
    if (!$this->config->item('track_cash') || !$has_sales_permission) {
        $logout_href = 'href="'.site_url('home/logout').'"';
    } ?>
    <!-- START JULY REFERRAL PROGRAM LINK -->
<style>
    nav.navbar .app-header-list {
        width: 210px !important;
    }
</style>
    <li style="background-color:#d3ffbc;">
        <a target='_blank' href="http://foreupgolf.com/referrals/" id='referral_program_link'>
            <div class="app-icon app-icon-customers"></div>
            <div class="app-content">
                <div class="app-name">Referral Program</div>
                <div class="app-description">Make Some Extra Cash</div>
            </div>
        </a>
    </li>
    <!-- END JULY REFERRAL PROGRAM LINK -->

    <li>
        <a <?php echo $logout_href; ?> id='log_out_link'>
            <div class="app-icon app-icon-logout"></div>
            <div class="app-content">
                <div class="app-name">Logout</div>
                <div class="app-description">Sign out of foreUP</div>
            </div>
        </a>
    </li>

    <?php if ($this->config->item('track_cash') && $has_sales_permission) { ?>
        <script>
            $(document).ready(function(){
                $('#log_out_link').click(function(e){
                    e.preventDefault();
                    
                    <?php if($this->config->item('sales_v2') == 1 && $this->Sale->getUnfinishedRegisterLog()->row()){ ?>
                    parent.window.location = "<?php echo site_url('v2/home#sales/logout')?>";
                    return false;
                    <?php }else if($this->config->item('sales_v2') == 1){ ?>
                    parent.window.location = "<?=site_url('home/logout')?>";
                    return false;
                    <?php } ?>

                    $.ajax({
                        type: "POST",
                        url: "index.php/home/logout_options/",
                        data: "",
                        success: function(response){
   
                            if (response.open_log)
                            {
                                $.colorbox({html:response.html, 'width':350, 'title':'Log Out', onComplete:function(){
                                    $('#switch_user').click(function(e){
                                        e.preventDefault();
                                        system.switch_user();
                                    });
                                    $('#close_register').click(function (e) {
                                        e.preventDefault();
                                        parent.window.location = SITE_URL + '/sales/closeregister?continue=logout';
                                    });

//                                    $('#switch_user').click(function (e) {
//                                        e.preventDefault();
//                                        parent.window.location = SITE_URL + '/sales/logout';
//                                    });
                                    $('#bypass_cash_log').click(function (e) {
                                        e.preventDefault();
                                        parent.window.location = SITE_URL + '/home/logout/bypass';
                                    });
                                }});
                            }
                            else
                            {
                                parent.window.location = "index.php/home/logout";
                            }
                        },
                        dataType:'json'
                    });
                    $('#user_menu').hide();
                    return false;
                });
            });
            var system = {
                login: {
                    submit:function(controller){
                        controller = controller == undefined ? 'food_and_beverage' : controller;
                        $.ajax({
                            type: "POST",
                            url: "index.php/"+controller+"/login/",
                            data: "pin_or_card=" + encodeURIComponent($('#pin_or_card').val().replace('%','!')),
                            success: function(response){
                                if (response.success)
                                {
                                    //$('#software_menu_contents').replaceWith(response.software_menu_contents); TODO
                                    //$('#user_menu_contents').replaceWith(response.user_menu_contents); TODO
                                    $('#user_button .menu_title').html(response.employee.first_name + ' ' + response.employee.last_name);
                                    $('#user_button').show();

                                    App.receipt.header.employee_name = response.employee.last_name + ', ' + response.employee.first_name;
                                    App.employee_id = response.employee.person_id;
                                    App.recent_transactions.reset(response.recent_transactions);
                                    App.register_log = new RegisterLog(response.register_log);

                                    // Show register log window, if no log has been set yet
                                    if(App.track_cash != 0 && App.register_log && !App.register_log.get('register_log_id')){

                                        var registerLog = new RegisterLogView({model: App.register_log });
                                        $.colorbox({
                                            title: 'Open Register',
                                            html: registerLog.render().el,
                                            width: 600,
                                            height: 400
                                        });

                                        // Show table select window
                                    }else{
                                        reload_tables(function(response){
                                            fnb.show_tables();
                                        });
                                    }
                                }
                                else
                                {
                                    set_feedback(response.message,'warning_message',false, 5000);
                                }
                            },
                            dataType:'json'
                        });
                    },

                    show:function(){
                        $.colorbox({
                            transition: 'none',
                            title: 'Switch User',
                            escKey: false,
                            closeButton: false,
                            open: true,
                            opacity: 1,
                            overlayClose: false,
                            href: 'index.php/sales/soft_logout',
                            onLoad:function(){
                                $('#cboxClose').remove();
                            },
                            onComplete:function(){
                                $('#cboxClose').remove();
                                $('#pin').focus();
                                fnb.login.initialize();
                            },
                            width: 400,
                            height: 400
                        });
                    },

                    initialize:function(){
                        $('.number_one').click(function(){fnb.login.add_character(1)});
                        $('.number_two').click(function(){fnb.login.add_character(2)});
                        $('.number_three').click(function(){fnb.login.add_character(3)});
                        $('.number_four').click(function(){fnb.login.add_character(4)});
                        $('.number_five').click(function(){fnb.login.add_character(5)});
                        $('.number_six').click(function(){fnb.login.add_character(6)});
                        $('.number_seven').click(function(){fnb.login.add_character(7)});
                        $('.number_eight').click(function(){fnb.login.add_character(8)});
                        $('.number_nine').click(function(){fnb.login.add_character(9)});
                        $('.number_zero').click(function(){fnb.login.add_character(0)});
                        $('.number_backspace').click(function(){fnb.login.remove_character(true)});
                        $('.number_login').click(function(){fnb.login.submit()});

                        $('#pin').bind('keypress', function(e) {
                            e.preventDefault();
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if(code == 13) { //Enter keycode
                                //Do something
                                e.preventDefault();
                                fnb.login.submit();
                                return;
                            }
                            var c = String.fromCharCode(code);
                            console.log('character '+c);
                            fnb.login.add_character(c);
                        }).bind('keyup', function(e){
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if (code == 8 || code == 46) {
                                e.preventDefault();
                                console.log('backspace');
                                fnb.login.remove_character();
                            }
                        });

                        $('#pin').val('');
                        $('#pin_or_card').val('');
                    },
                    add_character:function(number){
                        console.log('adding_character');
                        $('#pin').val($('#pin').val()+'*')
                        $('#pin_or_card').val($('#pin_or_card').val()+''+number)

                        $('#pin').focus();
                    },
                    remove_character:function(both){
                        console.log('adding_character');
                        if (both === true)
                        {
                            var str = $('#pin').val();
                            $('#pin').val(str.substring(0, str.length - 1));
                        }
                        var str2 = $('#pin_or_card').val();
                        $('#pin_or_card').val(str2.substring(0, $('#pin').val().length));

                        $('#pin').focus();
                    }
                },
                logout:function() {

                },
                switch_user:function() {
                    system.login.show();
                    // $.ajax({
                    // type: "POST",
                    // url: "index.php/home/switch_user/",
                    // data: "",
                    // success: function(response){
                    // $('#menubar_stats').html('');
                    // $('#user_button').hide();
//
                    // },
                    // dataType:'json'
                    // });
                }
            };
        </script>
    <?php } else { ?>
        <script>
            $(document).ready(function(){
                $('#log_out_link').click(function(e){
                    e.preventDefault();
                    <?php if($this->config->item('sales_v2') == 1){ ?>
                    parent.window.location = "<?=site_url('v2/home#sales/logout')?>";
                    <?php }else{ ?>
                    parent.window.location = "<?=site_url('home/logout')?>";
                    <?php } ?>
                });
            });
        </script>
    <?php } ?>
</ul>