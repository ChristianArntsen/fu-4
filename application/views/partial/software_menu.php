<div id='software_menu_contents'>
	<div id='software_menu_header'>

	</div>
	<div id='allowed_modules'>
		<div id="home_module_list">
		<?php
			$allowed_modules = ($this->Module->get_allowed_modules($this->session->userdata('person_id')));
			
			$hide_sales = false;
			$hide_food_bev = false;
			if($this->permissions->course_has_module('sales_v2')){
				$hide_sales = true;
			}
			if($this->permissions->course_has_module('food_and_beverage_v2')){
				$hide_food_bev = true;
			}			
			
			foreach($allowed_modules->result() as $module)
			{
				if($hide_sales && $module->module_id == 'sales'){
					continue;
				}
				if($hide_food_bev && $module->module_id == 'food_and_beverage'){
					continue;
				}
		       
		        if ($this->permissions->is_super_admin() || $this->permissions->course_has_module($module->module_id))
		        {

				if(stripos($module->module_id, '_v2') !== false){
					$url = 'v2/home#'.str_replace('_v2', '', $module->module_id);
				}else if($module->module_id == 'marketing_campaigns'){
                    $url = "marketing_campaigns/campaign_builder#/dashboard";
                } else {
					$url = $module->module_id;
				}
			?>
			<div class="module_item">
				<a href="<?php echo site_url($url);?>">
					<span class="module_icon <?php echo str_replace('_v2', '', $module->module_id)?>_icon"></span>
					<div>
						<div><?php echo lang($module->name_lang_key); ?></div>
						<div class='module_desc'><?php echo lang($module->desc_lang_key);?></div>
					</div>
				</a>
			</div>
			<?php
			}
		        }
			?>
			<div class="module_item">
				<a href="<?php echo site_url("support");?>" target='_blank'>
					<span class="module_icon support_icon"></span>
					<div>
						<div>Support</div>
						<div class='module_desc'>Help section</div>
					</div>
				</a>
			</div>
			<div class='clear'></div>
		</div>
	</div>
</div>
