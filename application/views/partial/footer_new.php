<?php //$this->output->enable_profiler(TRUE); ?>

<div id='footer'>
	<table>
		<tr>
			<td id="footer_cred">
				<?php echo lang('common_please_visit_my').' &copy; '.date("Y");?> 
					<a href="http://www.foreup.com" target="_blank">
						<?php echo lang('common_website'); ?>
					</a> 
				<?php echo lang('common_learn_about_project'); ?>
			</td>
		</tr>
	</table>
</div>
<div id='contactBox'></div>


<script src="<?php echo base_url();?>js/bootstrap.js?<?php echo APPLICATION_VERSION; ?>"></script>
<script src="<?php echo base_url();?>js/global.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>


        <script>
        	var global_variables = {
        		simulator:<?php echo $this->config->item('simulator')?'true':'false'; ?>
        	};
        	<?php if (!($this->permissions->is_employee() && $controller_name == 'sales') && ($controller_name == 'teesheets' || $controller_name == 'reservations' || $controller_name == 'sales' || $controller_name == 'customers' || $controller_name == 'marketing_campaigns')) { ?>
	 			$(document).ready(function(){
		 			setTimeout(function(){load_stats_header('<?=$controller_name;?>'), 1000});
	 			});
			<?php } ?>
        </script>

<div class="modal fade bootstrap-modal" id="globalModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-load"></div>
        </div>
    </div>
</div>

</body>
</html>