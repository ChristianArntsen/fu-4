<script type="text/javascript">
    var key_map = {18:false};
    $(document).keydown(function(event)
    {
        if (event.keyCode in key_map) {
            key_map[event.keyCode] = true;
        }
        if (event.keyCode == 112)  // F1 - reservations
        {
            event.preventDefault();
            <?php if ($this->session->userdata('reservations')) { ?>
                window.parent.location = SITE_URL + "/reservations";
            <?php } else if ($this->config->item('tee_sheet_speed_up') == 1 && $this->config->item('sales_v2') == 1) { ?>
                window.parent.location = SITE_URL + "/v2/home#tee_sheet";
            <?php } else { ?>
                window.parent.location = SITE_URL + "/teesheets";
            <?php }?>
        }
        else if (event.keyCode == 113) // F2 - sales
        {
            <?php if ($this->config->item('sales_v2') == 1) { ?>
            window.parent.location = SITE_URL + "/v2/home#sales";
            <?php } else { ?>
            window.parent.location = SITE_URL + "/sales";
            <?php } ?>
        }else if (event.keyCode == 115) // F4 - food and beverage
        {
            <?php if ($this->config->item('food_and_beverage_v2') == 1) { ?>
            window.parent.location = SITE_URL + "/v2/home#food_and_beverage";
            <?php } else { ?>
            window.parent.location = SITE_URL + "/food_and_beverage";
            <?php } ?>
        }
        else if (event.keyCode == 117) // F6 - gift card lookup
        {
            if ($.colorbox)
                $.colorbox({
                    href: "index.php/home/view_giftcard_lookup",
                    title:'Giftcard/Punch Card Lookup',
                    width:500,
                    onComplete:function(e){
                        console.log('trying to select gcn');
                        //e.preventDefault();
                        console.dir($('#giftcard_number'));
                        $('#giftcard_number').focus();
                    }
                });
        }
        else if ((event.keyCode == 49 || event.keyCode == 97) && event.altKey) // Alt + 1 - reservations
        {
            event.preventDefault();
            <?php if ($this->session->userdata('reservations')) { ?>
                window.parent.location = SITE_URL + "/reservations";
            <?php } else if ($this->config->item('tee_sheet_speed_up') == 1 && $this->config->item('sales_v2') == 1) { ?>
                window.parent.location = SITE_URL + "/v2/home#tee_sheet";
            <?php } else { ?>
                window.parent.location = SITE_URL + "/teesheets";
            <?php }?>
        }
        else if ((event.keyCode == 50 || event.keyCode == 98) && event.altKey) // Alt + 2 - sales
        {
            event.preventDefault();
            <?php if ($this->config->item('sales_v2') == 1) { ?>
            window.parent.location = SITE_URL + "/v2/home#sales";
            <?php } else { ?>
            window.parent.location = SITE_URL + "/sales";
            <?php } ?>
        }
        else if ((event.keyCode == 51 || event.keyCode == 99) && event.altKey) // Alt + 3 - food & beverage
        {
            event.preventDefault();
            <?php if ($this->config->item('food_and_beverage_v2') == 1) { ?>
            window.parent.location = SITE_URL + "/v2/home#food_and_beverage";
            <?php } else { ?>
            window.parent.location = SITE_URL + "/food_and_beverage";
            <?php } ?>
        }
        else if ((event.keyCode == 48 || event.keyCode == 96) && event.altKey) // Alt + 0 - settings
        {
            event.preventDefault();
            window.parent.location = SITE_URL + "/config";
        }
        else if ((event.keyCode == 55 || event.keyCode == 103) && event.altKey) // Alt + 7 - inventory
        {
            event.preventDefault();
            window.parent.location = SITE_URL + "/items";
        }
        else if ((event.keyCode == 52 || event.keyCode == 100) && event.altKey) // Alt + 4 - employees
        {
            event.preventDefault();
            window.parent.location = SITE_URL + "/employees";
        }
        else if ((event.keyCode == 53 || event.keyCode == 101) && event.altKey) // Alt + 5 - customers
        {
            event.preventDefault();
            window.parent.location = SITE_URL + "/v2/home#customers";
        }
        else if ((event.keyCode == 54 || event.keyCode == 102) && event.altKey) // Alt + 6 -
        {
            event.preventDefault();
            if ($.colorbox)
                $.colorbox({
                    href:"index.php/home/view_print_queue",
                    title:'Print Queue',
                    width:500,
                    onComplete:function(e){
                        console.log('trying to select gcn');
                        //e.preventDefault();
                        console.dir($('#giftcard_number'));
                    }
                });
        }
        else if ((event.keyCode == 54 || event.keyCode == 102) && event.altKey) // Alt + 6
        {
            event.preventDefault();
            window.parent.location = SITE_URL + "/giftcards";
        }
        else if ((event.keyCode == 71 || event.keyCode == 103) && event.altKey) // Alt + g - gift card lookup
        {
            event.preventDefault();
            if ($.colorbox)
                $.colorbox({
                    href:"index.php/home/view_giftcard_lookup",
                    title:'Giftcard Lookup',
                    width:500,
                    onComplete:function(e){
                        console.log('trying to select gcn');
                        //e.preventDefault();
                        console.dir($('#giftcard_number'));
                        $('#giftcard_number').focus();
                    }
                });
        }
        else if (event.keyCode == 192 && event.altKey) // Alt + ` - quick key guide
        {
            event.preventDefault();
            if ($.colorbox)
                $.colorbox({
                    href:"index.php/home/view_key_guide",
                    title:'Quick Key Guide',
                    width:500,
                    onComplete:function(e){

                    }
                });
        }
        else if ((event.keyCode == 80 || event.keyCode == 112) && event.altKey)
        {
            event.preventDefault();
            if (window.parent['mercury'] != undefined)
                mercury.payments_window();
        }
        else if (event.keyCode == 84 && event.altKey) { // Alt + t - terminal select window
            event.preventDefault();
            if ($.colorbox)
                $.colorbox({'href':'index.php/home/terminal_window','title':'Select Terminal', 'width':400, 'overlayClose':false,onComplete : function() {$(this).colorbox.resize();}});
        }
    }).keyup(function(event) {
        if (event.keyCode in key_map) {
            key_map[event.keyCode] = false;
        }
    });
</script>
