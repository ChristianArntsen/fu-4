<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">

    <base href="<?php echo base_url();?>" />
    <title><?php echo $this->config->item('name').' -- '.lang('common_powered_by').' ForeUP' ?></title>
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto">
    <!-- Google Analytics -->

  	<script>
  	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  	  ga('create', 'UA-101056671-1', 'auto');
  	  ga('send', 'pageview');
  	</script>
    
    <style type="text/css">
        html {
            overflow: auto;
        }

        div.ibeacon {
            display: block;
            position: fixed;
            right: 10px;
            top: 10px;
            padding: 15px;
            background-color: #E9E9E9;
            border: 1px solid #AAA;
            border-radius: 5px;
            z-index: 100000;
            box-shadow: 2px 2px 10px rgba(0,0,0,0.2);
            overflow: hidden;
        }

        div.ibeacon span {
            display: block;
            padding: 2px 0px 2px 0px;
            font-size: 14px;
        }

        div.ibeacon span.balance span.amount {
            float: right;
            display: block;
            padding: 0px;
            font-weight: bold;
        }

        div.ibeacon span.balance {
            width: 225px;
        }

        div.ibeacon a.close {
            position: absolute;
            right: 10px;
            top: 0px;
            color: #666;
            font-weight: bold;
            font-size: 32px;
        }

        div.ibeacon a.close:hover {
            color: #398ABD;
            cursor: pointer;
        }

        div.ibeacon div.photo {
            width: 100px;
            float: left;
            min-height: 150px;
            margin-right: 10px;
            overflow: hidden;
        }

        div.ibeacon div.details {
            width: 300px;
            float: left;
            overflow: hidden;
        }

        div.ibeacon h3 {
            display: block;
            padding: 0px;
            margin: 0px 0px 5px 0px;
            font-size: 24px;
        }

        #page-header-content {
            color: #444;
            display: block;
            width: auto;
            left: 50px;
            right: 50px;
            top: 0px;
            float: none;
            overflow: hidden;
            text-align: center;
            margin: 5px 0px;
            position: absolute;
        }
        div.uv-icon svg{
            bottom:0px !important;
            right:-1000px !important;
        }
        .uv-icon svg {
            opacity:0;
            width:0px;
            height:0px;
            right:-1000px !important;
        }
        div.ue-button {
            background-color:#577da9;
        }
        div.ue-button:hover {
            background-color:#5ca2f4;
        }
        #page-header-content  {
            float:right;
            left:auto;
            right:auto;
            position:inherit;
        }
    </style>



    <?php if ( $this->config->item('load_3rd_party_tools') && !isset($_GET['in_iframe'] )){ ?>
        <!-- begin olark code -->
        <script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
                f[z]=function(){
                    (a.s=a.s||[]).push(arguments)};var a=f[z]._={
                },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
                    f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
                    0:+new Date};a.P=function(u){
                    a.p[u]=new Date-a.p[0]};function s(){
                    a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
                    hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
                    return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
                    b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
                    b.contentWindow[g].open()}catch(w){
                    c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
                    var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
                    b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
                loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
            /* custom configuration goes here (www.olark.com/documentation) */
            olark.identify('9651-903-10-1533');/*]]>*/</script>
        <script>
            olark('api.visitor.updateFullName', {
                fullName: "<?=$this->session->userdata('course_name') ?> - <?=$user_info->first_name?> <?=$user_info->last_name?>"
            });
            olark('api.visitor.updateEmailAddress', {
                emailAddress: '<?=$this->session->userdata('email') ?>'
            });
            olark('api.visitor.updatePhoneNumber', {
                phoneNumber: '<?=$user_info->phone_number ?>'
            });
        </script>

        <!-- end olark code -->
        <script type='text/javascript'>
            window.__lo_site_id = 59558;
            window.__wtw_custom_user_data = {};
            window.__wtw_custom_user_data.course = "<?=$this->session->userdata('course_id') ?>";
            window.__wtw_custom_user_data.terminal_id = "<?=$this->session->userdata('terminal_id') ?>";
            window.__wtw_custom_user_data.course_name = "<?=$this->session->userdata('course_name') ?>";
            window.__wtw_custom_user_data.email = "<?=$this->session->userdata('email') ?>";
            (function() {
                var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
                wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
            })();
        </script>

        <script>
            // Include the UserVoice JavaScript SDK (only needed once on a page)
            UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/uouQNcbVRYcnPbJP28TMAA.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();
            //
            // UserVoice Javascript SDK developer documentation:
            // https://www.uservoice.com/o/javascript-sdk
            //

            // Set colors
            UserVoice.push(['set', {
                accent_color: '#448dd6',
                trigger_color: 'white',
                trigger_background_color: '#448dd6'
            }]);
            // Identify the user and pass traits
            // To enable, replace sample data with actual user traits and uncomment the line
            UserVoice.push(['identify', {
                email:      '<?=$this->session->userdata('email') ?>', // User’s email address
                name:       "<?=$user_info->last_name !== null ? $user_info->first_name." ".$user_info->last_name: " " ?>", // User’s real name
                //created_at: 1364406966, // Unix timestamp for the date the user signed up
                id:         '<?=$this->session->userdata('emp_id') ?>', // Optional: Unique id of the user (if set, this should not change)
                type:       '<?=$this->session->userdata('user_level') ?>', // Optional: segment your users by type
                account: {
                    id:           '<?=$this->session->userdata('course_id') ?>', // Optional: associate multiple users with a single account
                    name:         '<?=addslashes($this->session->userdata('course_name')) ?>' // Account name
                    //created_at:   1364406966, // Unix timestamp for the date the account was created
                    //monthly_rate: 9.99, // Decimal; monthly rate of the account
                    //ltv:          1495.00, // Decimal; lifetime value of the account
                    //plan:         'Enhanced' // Plan name for the account
                }
            }]);

            // Add default trigger to the bottom-right corner of the window:
            UserVoice.push(['addTrigger', {
                trigger_position: 'bottom-right',
                mode:'smartvote'
            }]);
            UserVoice.push(['addTrigger', '#uservoice-feedback', {mode:"smartvote"}]);

            // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
            UserVoice.push(['autoprompt', {}]);
        </script>

    <?php } ?>
    <!-- PUSH NOTIFICATIONS VIA ROOST -->
    <link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>

    <!-- <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/phppos.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/menubar.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/general.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/popupbox.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/register.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/receipt.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/reports.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/tables.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/thickbox.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/colorbox.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/colorbox2.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/datepicker.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/editsale.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/footer.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/contactable.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/css3.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ui-lightness/jquery-ui-1.8.14.custom.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.loadmask.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/redmond.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/superfish.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/superfish-vertical.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.qtip.min2.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jHtmlArea.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.wysiwyg.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.checkbox.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.sth.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/new_general.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/dd.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/flags.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/skin2.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/sprite.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/style.css?<?php echo APPLICATION_VERSION; ?>" /> -->
    <!--link href='http://fonts.googleapis.com/css?family=Quicksand:400,700' rel='stylesheet' type='text/css'-->
    <?php if ($controller_name == 'teesheets' || $controller_name == 'reservations') { ?>
        <!-- <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css?<?php echo APPLICATION_VERSION; ?>1" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fullcalendar.print.css?<?php echo APPLICATION_VERSION; ?>" media="print"/>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/styles.css?<?php echo APPLICATION_VERSION; ?>" /> -->

        <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/all_tee_sheet_css.css?<?php echo APPLICATION_VERSION; ?>" />

        <link href="<?php echo base_url(); ?>css/weather/weather-icons.min.css?<?php echo APPLICATION_VERSION; ?>" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fullcalendar.print.css?<?php echo APPLICATION_VERSION; ?>" media="print"/>
    <?php } else { ?>
        <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/all_css.css?<?php echo APPLICATION_VERSION; ?>" />
    <?php } ?>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.timepicker.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/new_styles.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/phppos_print.css?<?php echo APPLICATION_VERSION; ?>"  media="print"/>


    <link rel="stylesheet" href="<?php echo base_url();?>css/sass/header-bootstrapless.css?<?php echo APPLICATION_VERSION; ?>">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300,300i,500,700' rel='stylesheet' type='text/css'>
    <script src="<?php echo base_url();?>js/jquery-1.7.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script type="text/javascript">
        var jQuery_1_7_2 = $.noConflict(true);
    </script>

    <script type="text/javascript">
        var lang_array = JSON.parse('<?php echo addslashes(json_encode($this->lang->all()));?>');
        function lang(key)
        {

            if (lang_array[key] != undefined)
            {
                return lang_array[key];
            }
        }
        var SITE_URL= "<?php echo site_url(); ?>";
    </script>

    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/ndcdpiikdnlagfjejimgpmaflngcemoo">
    <script type="text/javascript">
        var usePrinterExtension = <?php echo $this->config->item('use_printer_extension') ?>;
    </script>
    <script src="<?php echo base_url();?>js/underscore.min.js?<?php echo APPLICATION_VERSION; ?>"></script>

    <?php if ($controller_name == 'teesheets' || $controller_name == 'reservations') { ?>
        <!-- <script src="<?php echo base_url();?>js/jquery-1.5.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script> -->
        <script src="<?php echo base_url();?>js/all_tee_sheet_js.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script type='text/javascript' src="<?php echo base_url();?>js/highcharts/highcharts.js?<?php echo APPLICATION_VERSION; ?>" ></script>
        <script type='text/javascript' src="<?php echo base_url();?>js/teesheet_scrolling_summary.js?<?php echo APPLICATION_VERSION; ?>" ></script>
        <script src="<?php echo base_url('js/spectrum.js'); ?>"></script>
        <link href="<?php echo base_url('css/spectrum.css'); ?>" type="text/css" rel="stylesheet" />
        <script src="<?php echo base_url();?>js/jquery.timepicker.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>

    <?php } else if($controller_name == 'food_and_beverage'){ ?>

        <script src="<?php echo base_url();?>js/jquery-1.10.2.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery-migrate-1.2.1.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url('js/underscore.min.js?'.APPLICATION_VERSION); ?>"></script>
        <script src="<?php echo base_url('js/backbone.min.js?'.APPLICATION_VERSION); ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.colorbox.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.colorbox2.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery-ui.1.10.3.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/accounting.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/fastclick.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <!-- <script src="<?php echo base_url();?>js/bootstrap.min.js?<?php echo APPLICATION_VERSION; ?>"></script> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css?<?php echo APPLICATION_VERSION; ?>" /> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap-theme.min.css?<?php echo APPLICATION_VERSION; ?>" /> -->
        <script>
            window.addEventListener('load', function() {
                FastClick.attach(document.body);
            }, false);
        </script>

    <?php } else if($controller_name == 'config' || $controller_name == 'reports' || $controller_name == 'courses' || $controller_name == 'items' || $controller_name == 'receivings'){ ?>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>js/jquery.cleditor.css?<?php echo APPLICATION_VERSION; ?>" />
        <script src="<?php echo base_url();?>js/jquery-1.10.2.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery-migrate-1.2.1.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery-ui.1.10.3.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.colorbox.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.colorbox2.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/ui.expandable.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url('js/spectrum.js'); ?>"></script>
        <link href="<?php echo base_url('css/spectrum.css'); ?>" type="text/css" rel="stylesheet" />
        <script src="<?php echo base_url();?>js/jquery.tablesorter.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/common.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/manage_tables.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/jquery.cleditor.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/jquery.timepicker.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>

    <?php } else { ?>
        <!-- <script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script> -->
        <script src="<?php echo base_url();?>js/all_js.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <?php } ?>
    <script src="<?php echo base_url();?>js/moment.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.mask.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.contactable.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.customSelect.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.qtip.min2.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/StarWebPrintBuilder.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/StarWebPrintTrader.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/StarBarcodeEncoder.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/webprnt.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/taffy.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/db.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/swipeable.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.mtz.monthpicker.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.date-dropdown.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <!-- <script src="<?php echo base_url();?>js/jquery-ui.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.color.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.tablesorter.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.maskedinput.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/thickbox.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.colorbox.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.colorbox2.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/ui.expandable.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/common.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/manage_tables.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/date.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/datepicker.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/hoverIntent.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/superfish.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jHtmlArea-0.7.0.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.wysiwyg.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.checkbox.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.contextMenu.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.dd.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.timepicker.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.sth.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script> -->
    <!-- <script src="<?php echo base_url();?>js/sqlite.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/offline/db.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
     -->
    <script src="<?php echo base_url();?>js/header.js?<?php echo APPLICATION_VERSION; ?>"></script>

    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/text_size_reduced.css?<?php echo APPLICATION_VERSION; ?>" />
    <?php $this->load->view('partial/quickkeys'); ?>
    <script type="text/javascript">

        Date.format = '<?php echo get_js_date_format(); ?>';

        var old_messages = '';
        $(document).ready(function(){

            $(window).resize(function(){
                resize_table();
            });
            resize_table();
            <?php if($controller_name != "teesheets"): ?>
            setInterval(function(){webprnt.print_all(window.location.protocol + "//" + "<?=$this->config->item('webprnt_ip')?>/StarWebPRNT/SendMessage")}, 3000);
            <?php endif; ?>


            $('#menu_button').mouseenter(function(){
                $('#software_menu').show();
                $('#software_menu').unbind('mouseleave').mouseleave(function(){
                    $('#software_menu').hide();
                });

                setTimeout("$('body').one('click', function(e){ $('#software_menu').hide(); });", 500);
            });

            $('#user_button').mouseenter(function(){
                $('#user_menu').show();
                $('#user_menu').unbind('mouseleave').mouseleave(function(){
                    $('#user_menu').hide();
                });

                setTimeout("$('body').one('click', function(){ $('#user_menu').hide(); });", 500);
            });

            $('#stats_button').click(function() {
                $('#shadow_row').toggle();
                $('#stats_row').toggle();
                var sb = $('#stats_button');
                sb.hasClass('selected')?sb.removeClass('selected'):sb.addClass('selected')
                if($(".note_dialog").css('top') == '125px'){
                    $(".note_dialog").stop().animate({top:'285px'},10);
                };
                if($(".note_dialog").css('top') == '285px'){
                    $(".note_dialog").stop().animate({top:'125px'},10);
                };
            })
            <?php
                if ($this->session->userdata('display_message')) {
            ?>
            set_feedback("<?=$this->session->userdata('display_message')?>",'error_message',true);
            <?php
                    $this->session->unset_userdata('display_message');
                }

                 $hidden_style = '';
                 if (!($this->permissions->is_employee() && $controller_name == 'sales') && ($controller_name == 'teesheets' || $controller_name == 'reservations' || $controller_name == 'sales' || $controller_name == 'customers' || $controller_name == 'marketing_campaigns')) { ?>

            <?php } else {
                     $hidden_style = 'display:none';
                 }

                if ($controller_name != 'food_and_beverage' && $this->session->userdata('use_terminals') && $this->session->userdata('terminal_id') === false)
                {
            ?>
            $.colorbox({
                'href':'index.php/home/terminal_window',
                'title':'Select Terminal',
                'width':400,
                'overlayClose':false,
                'onComplete' : function() {
                    $(this).colorbox.resize();
                }
            });
            <?php
                }
            ?>
            // Watch for any messages from the server
            if (!!window.EventSource && false) {
                //var old = '';
                var source = new EventSource('index.php/home/get_course_messages/');

                source.onmessage = function(e)
                {
                    var message_html = '';
                    var nd = new Date();
                    //console.log('running '+nd.getTime());
                    if(old_messages!=e.data){
                        //console.log(e.data);
                        var  messages = eval("("+e.data+")");
                        console.log('course messages');
                        console.dir(messages);
                        for (var i in messages)
                        {
                            console.log('message '+i);
                            message_html += "<div id='course_message_"+messages[i].message_id+"' class='course_message'>"+
                            "<span class='date_posted'>"+messages[i].date_posted+"</span>"+
                            messages[i].message+
                            "<span class='mark_as_read' onclick='mark_message_as_read("+messages[i].message_id+")'>x</span>"+
                            "</div>";
                        }
                        $('#course_messages').html(message_html);
                        old_messages = e.data;
                    }
                };
            }
        });
        var controller_name = '<?=$controller_name?>';
        function mark_message_as_read(message_id)
        {
            $('#course_message_'+message_id).remove();
            $.ajax({
                type: "POST",
                url: "index.php/home/mark_message_as_read/"+message_id,
                data: '',
                success: function(response){
                },
                dataType:'json'
            });
        }
        function resize_table() {
            /*if (controller_name == 'reservations')
             {
             var sortable_table = $('#main');
             if (sortable_table.length > 0)
             {
             console.log('trying to grow the schedules');
             var win_height = $(window).height();
             var adj_win_height = (win_height - 200 > 250) ? win_height - 200 : 250;
             var table_height = (sortable_table.height() > 250) ? sortable_table.height() : 250;
             $('.calScroller, #track_calendar_holder .track_calendar').height((adj_win_height < table_height) ? adj_win_height : table_height);
             }
             }
             else*/
            {
                var sortable_table = $('#sortable_table');
                if (sortable_table.length > 0)
                {
                    var win_height = $(window).height();
                    if (controller_name == 'reports')
                    {}
                    else
                    {	var adj_win_height = (win_height - 200 > 250) ? win_height - 200 : 250;
                        var table_height = (sortable_table.height() > 250) ? sortable_table.height() : 250;
                        $('#table_holder').height((adj_win_height < table_height) ? adj_win_height : table_height);
                    }
                }
            }
        }

        load_stats_header = _.throttle(function(controller, start_date, end_date){

            <?php if (!($this->session->userdata('sales_stats') === '0' && $controller_name == 'sales')) { ?>
            console.log('initialize_stats_header'+controller+' sd: '+start_date+' ed: '+end_date);
            var date = new Date();
            var sd = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
            var ed = sd;
            if(window.parent){
                window.parent.App.vent.trigger("teesheet:datechanged",{start:start_date})
            }
            $.ajax({
                type: "POST",
                url: "index.php/"+controller+"/generate_stats/",
                data: 'start='+(start_date==undefined?sd:start_date)+'&end='+(end_date==undefined?ed:end_date),
                success: function(response){
                    var header_stats_html = '';
                    var more_stats_html = '';
                    var left_border_class = '';
                    var header = response.header;
                    var more = response.more;
                    for (var i in header)
                    {
                        header_stats_html += "<div class='bar_stat "+left_border_class+"'><div class='stat_value'>"+header[i]+"</div><div class='stat_label'>"+i.replace(/_/g, " ")+"</div></div>";
                        left_border_class = 'left_border';
                    }
                    left_border_class = '';
                    for (var i in more)
                    {
                        more_stats_html += "<div class='bar_stat "+left_border_class+"'><div class='stat_value'>"+more[i]+"</div><div class='stat_label'>"+i.replace(/_/g, " ")+"</div></div>";
                        left_border_class = 'left_border';
                    }
                    $('#menubar_stats').html(header_stats_html);

                    $('#stats_section').html(more_stats_html);
                    if (controller == 'Sales')
                        $('#item').focus();
                    if (controller == 'teesheets') {
                        teesheet_note();
                        update_standbys();
                    }
                    console.log('stats');
                    console.dir(response);
                },
                dataType:'json'
            });
            <?php } ?>
        }, 1000, {leading: false});

        <?php if(false && $this->config->item('ibeacon_enabled') == 1){ // DEACTIVATED UNTIL WE FINISH AND MAKE AVAILABLE ?>
        // Check for nearby ibeacons on this terminal for this course
        function get_beacons(){
            $.get('<?php echo site_url('ibeacon'); ?>', null, function(response){
                if(response){
                    $('body').prepend(response);
                }
            },'html');
        }

        $(function(){
            $('body div.ibeacon a.close').live('click', function(){
                $(this).parents('div.ibeacon').remove();
                return false;
            });

            // Poll for ibeacon messages every 5 seconds
            setInterval('get_beacons();', 5000);
        });
        <?php } ?>
    </script>
    <?php if ($controller_name == 'teesheets') { ?>
        <script src="<?php echo base_url();?>js/jquery.maskedinput.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/all_tee_sheet_2_js.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/javascript.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/fullcalendar.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/lscache.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/jquery-ajax-goodies.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>

    <?php } else if ($controller_name == 'reservations') { ?>
        <script src="<?php echo base_url();?>js/jquery.maskedinput.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/all_reservations_2_js.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <!-- <script src="<?php echo base_url();?>js/jquery.xml2json.pack.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.maskedinput.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.syncscroll.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/javascript<?php echo ($controller_name == 'reservations')?'_res':'';?>.js?<?php echo APPLICATION_VERSION.time();?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/fullcalendar.js?<?php echo APPLICATION_VERSION.time(); ?>" type="text/javascript" language="javascript" charset="UTF-8"></script> -->
    <?php } ?>

    <?php



    if(isset($this->config->config['roost_enable']) && $this->config->config['roost_enable'] === true)
    {
        ?>
        <!-- ENABLE PUSH NOTIFICATIONS VIA ROOST -->
        <script src='<?php echo $this->config->config['roost_api_js_src'] ?>' async></script>

        <!-- SET ALIAS AND SEGMENTS FOR NOTIFICATIONS -->
        <script type="application/javascript">
            var _roost = _roost || [];
            _roost.push(["alias", "<?php echo 'person_'.$this->session->userdata('person_id'); ?>"]);
            _roost.push(["segments", "<?php echo 'course_'.$this->session->userdata('course_id'); ?>"<?php if($this->session->userdata('terminal_id') > 0) echo ', "terminal_'.$this->session->userdata('terminal_id').'"'; ?>]);
        </script>
    <?php
    }
    ?>
    <!--script>
        // Include the UserVoice JavaScript SDK (only needed once on a page)
        UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/uouQNcbVRYcnPbJP28TMAA.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();

        //
        // UserVoice Javascript SDK developer documentation:
        // https://www.uservoice.com/o/javascript-sdk
        //

        // Set colors
        UserVoice.push(['set', {
            accent_color: '#448dd6',
            trigger_color: 'white',
            trigger_background_color: '#448dd6'
        }]);

        // Identify the user and pass traits
        // To enable, replace sample data with actual user traits and uncomment the line
        UserVoice.push(['identify', {
            //email:      'john.doe@example.com', // User’s email address
            //name:       'John Doe', // User’s real name
            //created_at: 1364406966, // Unix timestamp for the date the user signed up
            //id:         123, // Optional: Unique id of the user (if set, this should not change)
            //type:       'Owner', // Optional: segment your users by type
            //account: {
            //  id:           123, // Optional: associate multiple users with a single account
            //  name:         'Acme, Co.', // Account name
            //  created_at:   1364406966, // Unix timestamp for the date the account was created
            //  monthly_rate: 9.99, // Decimal; monthly rate of the account
            //  ltv:          1495.00, // Decimal; lifetime value of the account
            //  plan:         'Enhanced' // Plan name for the account
            //}
        }]);

        // Add default trigger to the bottom-right corner of the window:
        UserVoice.push(['addTrigger', {trigger_position: 'bottom-right' }]);

        // Or, use your own custom trigger:
        //UserVoice.push(['addTrigger', '#id']);

        // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
        UserVoice.push(['autoprompt', {}]);
    </script-->
    <!--script>
        // Include the UserVoice JavaScript SDK (only needed once on a page)
        UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/uouQNcbVRYcnPbJP28TMAA.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();

        //
        // UserVoice Javascript SDK developer documentation:
        // https://www.uservoice.com/o/javascript-sdk
        //

        // Set colors
        UserVoice.push(['set', {
            //Disables Contact Mode:
            contact_enabled: 'false',
            smartvote_enabled: true,
            post_suggestion_enabled: true,
            //
            accent_color: '#448dd6',
            trigger_color: 'white',
            trigger_background_color: 'rgba(67, 180, 255, 0.6)'
        }]);
        // Sets labels:
        UserVoice.push(['set', {
            strings: {
                // SmartVote Tab Label and Mode Title
                smartvote_menu_label: 'Help us decide what to add next',
                smartvote_title: 'What should we add next?',
                //  Post/Search Idea Tab Label and Mode Title and Placeholder
                post_suggestion_menu_label: 'Post your own idea',
                post_suggestion_title: 'Search Ideas or Post Your Own',
                post_suggestion_body: 'Others will be able to subscribe to and make comments on posted ideas.',
                post_suggestion_message_placeholder: 'Describe the feature you would like to see',

            }
        }]);


        // Identify the user and pass traits
        // To enable, replace sample data with actual user traits and uncomment the line
        UserVoice.push(['identify', {
            //email:      'john.doe@example.com', // User’s email address
            //name:       'John Doe', // User’s real name
            //created_at: 1364406966, // Unix timestamp for the date the user signed up
            //id:         123, // Optional: Unique id of the user (if set, this should not change)
            //type:       'Owner', // Optional: segment your users by type
            //account: {
            //  id:           123, // Optional: associate multiple users with a single account
            //  name:         'Acme, Co.', // Account name
            //  created_at:   1364406966, // Unix timestamp for the date the account was created
            //  monthly_rate: 9.99, // Decimal; monthly rate of the account
            //  ltv:          1495.00, // Decimal; lifetime value of the account
            //  plan:         'Enhanced' // Plan name for the account
            //}
        }]);

        // Add default trigger to the bottom-right corner of the window:
        UserVoice.push(['addTrigger', {mode: 'smartvote', trigger_position: 'bottom-right' }]);

        // Or, use your own custom trigger:
        //UserVoice.push(['addTrigger', '#id', { mode: 'contact' }]);

        // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
        UserVoice.push(['autoprompt', {}]);
    </script-->
    <!--script type='text/javascript'>

        var _ues = {
            host:'matson.userecho.com',
            forum:'48609',
            lang:'en',
            tab_corner_radius:5,
            tab_font_size:20,
            tab_image_hash:'ZmVlZGJhY2s%3D',
            tab_chat_hash:'Y2hhdA%3D%3D',
            tab_alignment:'right',
            tab_text_color:'#ffffff',
            tab_text_shadow_color:'#00000055',
            tab_bg_color:'#57a957',
            tab_hover_color:'#f45c5c'
        };

        (function() {
            var _ue = document.createElement('script'); _ue.type = 'text/javascript'; _ue.async = true;
            _ue.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.userecho.com/js/widget-1.4.gz.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(_ue, s);
        })();

    </script-->
</head>
<body id='<?=$controller_name?>'>
<?php //echo $this->session->userdata('terminal_id'); ?>
<div id='body'>
    <div id='course_messages'>

    </div>

    <nav class="navbar app-header">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed btn-default" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand app-header-logo">
                    <!--img src="/images/logo-white-small.png"-->
                    <img src="/images/foreUP-Holiday-2-small.png">
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle app-header-current app-header-btn" data-toggle="dropdown" role="button" aria-expanded="false">
                            <div class="app-icon app-icon-header app-icon-white app-icon-<?=$controller_name?>">

                            </div>
                            <?php echo lang('module_'.$controller_name);?> <span class="caret"></span>
                        </a>
                        <?php $this->load->view('partial/software_menu_new'); ?>
                    </li>
                    <li id="menubar_stats"></li>
                    <!--
                    <li id="stats_button" class="stats-button">
                        <i class="fa fa-dashboard"></i>
                        <div style="font-size: 12px;">
                            More Stats
                        </div>
                    </li>
                    -->
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle app-header-btn" data-toggle="dropdown" role="button" aria-expanded="false">
                            <?php echo img(array('src' => $this->Appconfig->get_logo_image())); ?>
                            <div class="logged-in-user" style="color: white; display: inline;"><?php echo $user_info->first_name.' '.$user_info->last_name ?></div>
                            <span class="caret"></span>
                        </a>
                        <?php $this->load->view('partial/user_menu_new'); ?>
                    </li>
                </ul>
                <?php if($this->session->userdata('terminal_id')){
                    $terminal = $this->Terminal->get_info($this->session->userdata('terminal_id'));
                    $terminal_name = '';

                    if(!empty($terminal[0])){
                        $terminal_name = $terminal[0]['label'];
                    } ?>
                    <div id="page-header-content">
                        <div>
                            <h3 style="margin: 4px 0px 0px 0px; padding: 0px; display: block; width: auto; text-align: center; color: white; font-size: 24px; font-weight: normal;">
                                <?php echo htmlentities($terminal_name); ?>
                            </h3>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </nav>
    <?php if ($controller_name == 'teesheets' && $this->config->item('tee_sheet_speed_up') == 1 && $this->config->item('sales_v2') == 1) {
        // This script is to handle navigation within the tee sheets module when it is in an iframe. We don't want to navigate within the iframe
        ?>
        <script>
            $(document).ready(function () {
                $('.dropdown-menu li a').on('click', function (e) {
                    var href = $(e.currentTarget).attr('href');
                    console.dir(e);
                    //alert('triggering navigation event listener '+href);
                    if (href != undefined) {
                        e.preventDefault();
                        parent.window.location = href;
                    }
                });
            });
        </script>
    <?php } ?>
    <div id='stats_row' style='display:none;'>
        <div id='stats_section'>

        </div>
    </div>


    <div id="content_area_wrapper">
        <div id="content_area">
