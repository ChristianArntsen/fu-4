<style>
#minimum_charges {
	background-color: white;
	border: 1px solid #E0E0E0;
}

#minimum_charges td {
	font-size: 14px;
	padding: 4px 8px;
}
</style>
<div style="padding: 15px; min-height: 500px;">
<h2 style="float: left">F&amp;B Minimum Charges</h2>
<button id="new_minimum_charge" class="terminal_button" style='float: right'>New Minimum</button>
<table id="minimum_charges" style="margin-top: 15px;">
	<thead>
		<tr>
			<th style="width: 75px;"><div class="header_cell header">Active</div></th>
			<th><div class="header_cell header">Name</div></th>
			<th style="width: 150px;"><div class="header_cell header">Frequency</div></th>
			<th style="width: 150px;"><div class="header_cell header">Amount</div></th>
			<th style="width: 40px;"><div class="header_cell header">&nbsp;</div></th>
			<th style="width: 20px;"><div class="header_cell header">&nbsp;</div></th>
		</tr>
	</thead>
	<tbody>	
		<?php if(!empty($minimum_charges)){ ?>
		<?php foreach($minimum_charges as $charge){ ?>
		<tr data-charge-id="<?php echo $charge['minimum_charge_id']; ?>">
			<td>
				<?php if($charge['is_active'] == 1){ ?>
				<strong style='color: green'>Yes</strong>
				<?php }else{ ?>
				<strong style='color: red'>No</strong>
				<?php } ?>
			</td>
			<td>
				<?php echo $charge['name']; ?>
			</td>				
			<td>
				Every <?php echo $charge['frequency']; ?> <?php echo $charge['frequency_period']; ?>
			</td>
			<td>
				<?php echo to_currency($charge['minimum_amount']); ?>
			</td>
			<td class="edit">
				<a href="" title="Edit this minimum charge" class="edit">Edit</a>
			</td>			
			<td class="delete">
				<a href="" class="delete" title="Delete this minimum charge" style="color: red;">Delete</a>
			</td>
		</tr>
		<?php } }else{ ?>
		<tr>
			<td colspan="7">No minimum charges set up</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
</div>

<script>
$(function(){
	$('#new_minimum_charge').click(function(){
		$.colorbox({
			href: "index.php/minimum_charges/view_charge", 
			width: 960,
			height: 700,
			title: 'Minimum Charge'
		});
	});

	$('a.delete').die().live('click', function(){
		var row = $(this).parents('tr');
		var charge_id = row.attr('data-charge-id');

		$.post('index.php/minimum_charges/delete/'+charge_id, null, function(data){
			row.remove();
		});

		return false;
	});

	$('a.edit').die().live('click', function(){
		var row = $(this).parents('tr');
		var charge_id = row.attr('data-charge-id');

		$.colorbox({
			href: "index.php/minimum_charges/view_charge/" + charge_id, 
			width: 830,
			height: 700,
			title: 'Minimum Charge'
		});

		return false;
	});
});
</script>