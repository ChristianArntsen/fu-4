<style>
	#cboxContent h1 {
		font-size:16px;
	}
	.section_text {
		padding-right:50px;
	}
	.label_checkbox {
		float:left;
		vertical-align:middle;
		line-height:30px;
		height:25px;
	}
	#teetimes_daily, #teetimes_weekly {
		width:15px;
	}
	#start_date {
		width:90px;
	}
	.annual_charge, .monthly_charge {
		width:60px;
	}
	#course_name, #contact_email {
		width:300px;
	}
</style>
<ul id="error_message_box"></ul>
<?php
echo form_open('billings/save/'.$billing_info->billing_id,array('id'=>'billing_form'));
?>
<fieldset id="billing_basic_info">
<legend>Billing Info</legend>
    <div class="field_row clearfix">
	<?php echo form_label(lang('courses_name').':<span class="required">*</span>', 'courses_name',array('class'=>'wide ')); ?>
		<div class='form_field'>
		<?php 
			$disabled = ($billing_info->billing_id == '')?'':'disabled';
		echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'course_name',
			"$disabled"=>"$disabled",
			'id'=>'course_name',
			'value'=>$course_name)
		);
		echo form_hidden('course_id', $billing_info->course_id);
		?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('billings_email').':', 'billings_email',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'contact_email',
			'id'=>'contact_email',
			'value'=>$billing_info->contact_email)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('billings_tax_name').':', 'tax_name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'tax_name',
			'id'=>'tax_name',
			'value'=>$billing_info->tax_name)
		);?>
		Tax Rate: 
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'tax_rate',
			'id'=>'tax_rate',
			'value'=>$billing_info->tax_rate)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('Product:', 'annual_charge',array('class'=>'wide'));?>
		<div class='form_field'>
			<?php
		echo form_dropdown('product', array('1'=>'Software','2'=>'Website','3'=>'Marketing'), $billing_info->product);
			?>
		</div>
	</div>
</fieldset>
<fieldset id="marketing_basic_info">
<legend>Payment method (<span class='red'>one required</span>)</legend>
<div id='payment_info'>
	<div class='field_row clearfix'>
		<?php echo form_label('Start date:<span class="required">*</span>', 'annual_charge',array('class'=>'wide '));?>
		<div class='form_field'>
		<?php 
			echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'start_date',
			'id'=>'start_date',
			'class'=>'start_date',
			'value'=>($billing_info->start_date)?$billing_info->start_date:date('Y-m-d'))
			);
		?>
		<?php echo '<span class="red">Open Season:</span>'; ?>
			<?php
		echo form_dropdown('period_start', $month_array, ($billing_info->period_start)?$billing_info->period_start:1);
			?>
			-
		<?php
		echo form_dropdown('period_end', $month_array, ($billing_info->period_end)?$billing_info->period_end:12);
			?>
		</div>
	</div>
	<div class='field_row clearfix'>
		<?php echo form_label('Credit Card:', 'credit_card_id', array('class'=>'wide'));?>
		<div class='form_field'>
			<?php
			echo form_dropdown('credit_card_id', $credit_card_list, $billing_info->credit_card_id);
			?>
		</div>
	</div>
	<div id='marketing_limits' style='<?= $billing_info->product == 3?'':'display:none'?>'>
	<div class="field_row clearfix">	
	<?php echo form_label(lang('billings_email_marketing_limit').':', 'email_limit',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'email_limit',
			'id'=>'email_limit',
			'value'=>$billing_info->email_limit)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">	
	<?php echo form_label(lang('billings_text_marketing_limit').':', 'text_limit',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'text_limit',
			'id'=>'text_limit',
			'value'=>$billing_info->text_limit)
		);?>
		</div>
	</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_checkbox(array('name'=>'annual','id'=>'annual','value'=>'1','checked'=>$billing_info->annual, 'class'=>'label_checkbox'));
		echo form_label(lang('billings_annual_charge').':', 'annual_amount',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'annual_amount',
			'id'=>'annual_amount',
			'class'=>'annual_charge',
			'value'=>$billing_info->annual_amount)
		);?>
		Billing month/day:
		<?php
		echo form_dropdown('annual_month', $month_array, $billing_info->annual_month);
			?>
		<?php
		echo form_dropdown('annual_day', $day_array, $billing_info->annual_day);
			?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_checkbox(array('name'=>'monthly','id'=>'monthly','value'=>'1','checked'=>$billing_info->monthly, 'class'=>'label_checkbox'));
		echo form_label(lang('billings_monthly_charge').':', 'monthly_amount',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'monthly_amount',
			'id'=>'monthly_amount',
			'class'=>'monthly_charge',
			'value'=>$billing_info->monthly_amount)
		);?>
		Billing day:
		<?php
			echo form_dropdown('monthly_day', $day_array, $billing_info->monthly_day);
		?>

		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_checkbox(array('name'=>'teetimes','id'=>'teetimes','value'=>'1','checked'=>$billing_info->teetimes, 'class'=>'label_checkbox'));
		echo form_label(lang('courses_teetimes_per_day').':', 'teetimes',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'teetimes_daily',
			'id'=>'teetimes_daily',
			'value'=>$billing_info->teetimes_daily)
		);?>
		Week: 
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'teetimes_weekly',
			'id'=>'teetimes_weekly',
			'value'=>$billing_info->teetimes_weekly)
		);?>
		Teesheet:
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'teesheet_id',
			'id'=>'teesheet_id',
			'value'=>$billing_info->teesheet_id)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_checkbox(array('name'=>'free','id'=>'free','value'=>'1','checked'=>$billing_info->free, 'class'=>'label_checkbox'));
		echo form_label(lang('billings_free'), 'free',array('class'=>'wide')); ?>
		<div class='form_field'>
		</div>
	</div>
</fieldset>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>

<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
var billing = {
	requires_payment_method:function(){
		//alert((!($('#annual').attr('checked') || $('#monthly').attr('checked') || $('#teetimes').attr('checked')))?'false':'true')
		return !($('#annual').attr('checked') || $('#monthly').attr('checked') || $('#teetimes').attr('checked') || $('#free').attr('checked'));
	},
	requires_teetimes:function(){
		return !((!$('#teetimes').attr('checked')) || ($('#teetimes').attr('checked') && ($('#teetimes_daily').val() > 0 || $('#teetimes_weekly').val() > 0)));
	},
	requires_credit_card:function(){
		return (($('#annual').attr('checked') || $('#monthly').attr('checked')) && $('#credit_card_id').val() == '');
	}
};
$(document).ready(function()
{
	$('#product').change(function(event){
		var selected = $(event.target).val();
		if (selected == 3){$('#marketing_limits').show();}
		else ($('#marketing_limits').hide());
		$.colorbox.resize();
	})
	$('#start_date').datepicker({dateFormat:'yy-mm-dd'});
	
	var submitting = false;
	$( "#teesheet_id" ).autocomplete({
		source: "<?php echo site_url('courses/suggest_teesheet');?>",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0
	});
	$( "#course_name" ).autocomplete({
		source: "<?php echo site_url('courses/course_search');?>",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$("#course_name").val(ui.item.label);
			$("#course_id").val(ui.item.value);
			credit_cards.get(true);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});

	$('#billing_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
					$.colorbox.close();
					post_item_form_submit(response);
				},
				dataType:'json'
			});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			course_id:"required",
			contact_email: {
				email:true
			},
			start_date:"required",
			credit_card_id:{
				required:function() {
					return billing.requires_credit_card();
				}
			},
			annual:{
				required:function() {
					return billing.requires_payment_method();
				}
			},
 			monthly:{
				required:function() {
					return billing.requires_payment_method();
				}
			},
 			teetimes:{
				required:function() {
					return billing.requires_payment_method();
				}
			},
			annual_amount:{
				required:'#annual:checked'
			},
			monthly_amount:{
				required:'#monthly:checked'
			},
			teetimes_daily:{
				required:function(){
					return billing.requires_teetimes();
				}
			},
			teetimes_weekly:{
				required:function(){
					return billing.requires_teetimes();
				}
			},
			teesheet_id:{
				required:"#teetimes:checked"
			}
 		},
		messages:
		{
			course_id:"<?php echo lang('billings_required_course');?>",
			contact_email: {
				email:"<?php echo lang('billings_required_email_format');?>"
			},
			start_date:"<?php echo lang('billings_required_start_date');?>",
			credit_card_id: {
				required:"<?php echo lang('billings_required_credit_card');?>"
			},
			annual:{
				required:"<?php echo lang('billings_required_payment_method_selected');?>"
			},
 			monthly:{
				required:"<?php echo lang('billings_required_payment_method_selected');?>"
			},
 			teetimes:{
				required:"<?php echo lang('billings_required_payment_method_selected');?>"
			},
			annual_amount:{
				required:"<?php echo lang('billings_required_annual_amount');?>"
			},
			monthly_amount:{
				required:"<?php echo lang('billings_required_monthly_amount');?>"
			},
			teetimes_daily:{
				required:"<?php echo lang('billings_required_teetimes_amount');?>"
			},
			teetimes_weekly:{
				required:"<?php echo lang('billings_required_teetimes_amount');?>"
			},
			teesheet_id:{
				required:"<?php echo lang('billings_required_teesheet_id');?>"
			}
 		}
	});
});
</script>