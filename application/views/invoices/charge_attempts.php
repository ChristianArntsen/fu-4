<style>
	#charge_attempts_body {
		min-height:300px;
		margin:0px 10px 20px;
	}
	#charge_attempts_date, #run_charge_attempts {
		width:100px;
		float:left;
		margin:10px;
	}
	#charge_attempts_header input#run_charge_attempts {
		background: #349ac5;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5',endColorstr='#4173b3');
		background: -webkit-linear-gradient(top,#349AC5,#4173B3);
		background: -moz-linear-gradient(top,#349AC5,#4173B3);
		color: white;
		display: block;
		font-size: 14px;
		font-weight: normal;
		height: 30px;
		text-align: center;
		text-shadow: 0px -1px 0px black;
		border-radius: 4px;
		margin-bottom: 10px;
		box-shadow: inset 0px 1px 1px 0px rgba(255,255,255,0.5),0px 3px 1px -2px rgba(255,255,255,.2);
		border: 1px solid #232323;
		width:140px;
	}
	#charge_attempts_body table thead td {
		font-weight:bold;
		text-decoration:underline;
	}
	#charge_attempts_body td.currency_alignment {
		text-align:right;
	}
	#charge_attempts_body td.center_alignment {
		text-align:center;
	}
	.charge_attempts_disclaimer {
		margin:20px 0px 0px 10px;
		float:left;
	}
</style>
<div id='charge_attemptss'>
	<div id='charge_attempts_header'>
		<!-- Date -->
		<input placeholder='Charge Date' type='text' value='' id='charge_attempts_date'/>
		<!-- Submit -->
		<input type='button' value='Run Report' id='run_charge_attempts'/>
		<span class='charge_attempts_disclaimer'>
		
		</span>
		<div class='clear'></div>
	</div>
	<div id='charge_attempts_body'>
		<table>
			<thead>
				<tr>
					<td class='center_alignment'>Date</td>
					<td class='center_alignment'>Customer</td>
					<td class='center_alignment'>Invoice #</td>
					<td class='center_alignment'>Card</td>
					<td class='currency_alignment'>Amount</td>
					<td class='center_alignment'>Successful</td>
					<td class='center_alignment'>Message</td>
					<td class='center_alignment'>Employee</td>
				</tr>
			</thead>
			<tbody id='charge_attempts'>
				
			</tbody>
		</table>
	</div>
	<!-- TODO: Add in disclaimer saying these are only charge_attempts -->
</div>
<script>
	$(document).ready(function(e){
		$('#charge_attempts_date').monthpicker();
		$('#run_charge_attempts').click(function(e){
			e.preventDefault();
			var date = $('#charge_attempts_date').monthpicker('getDate');
			if (date === null)
			{
				alert('Please select a date');
			}
			else
			{
				var date_string = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
			
				$.ajax({
		           type: "POST",
		           url: "index.php/invoices/run_charge_attempts/"+date_string,
		           data: '',
		           success: function(response){
		           		console.dir(response);
		           		$('#charge_attempts').html(response.return_html);
						$.colorbox.resize();
				    },
		            dataType:'json'
		        });
		    }
		});
	});
</script>