<?php
// OPTION 1 LOAD IN POS
echo "<div id='invoice_number'>Invoice #: $invoice_number</div>";
echo "<div id='invoice_customer'>Customer: {$customer->first_name} {$customer->last_name}</div>";
echo "<div id='invoice_amount'>Amount Due: $".($total - $paid)."</div>";
?>
<div id='payment_attempts_header'>Card Payment Attempts:</div>
<?php if (count($payment_attempts) > 0) { ?>
<div id='payment_attempts'>
	<table>
		<tbody>
			<tr id='header_row'>
				<td>Date</td>
				<td>Card</td>
				<td>Amount</td>
				<td>Result</td>
			</tr>
			<?php foreach ($payment_attempts as $payment_attempt) { ?>
			<tr>
				<td><?=date('Y-m-d', strtotime($payment_attempt['date']))?></td>
				<td><?=$payment_attempt['card_type']?> <?=str_replace('x', '', $payment_attempt['masked_account'])?></td>
				<td>$<?=$payment_attempt['amount']?></td>
				<td><?=$payment_attempt['success']?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<?php } ?>
<?php
echo "<div id='load_invoice_into_pos'>Load invoice into the Point of Sale</div>";
echo "<div id='spacer_text'>or pay with a card on file.</div>";
echo $this->Customer_credit_card->dropdown($person_id, $credit_card_id);
echo "<span id='manage_cards'>Manage Cards</span>";
echo "<div id='charge_card_on_file'>Charge Card</div>";
?>
<style>
	#invoice_number, #invoice_customer, #invoice_amount, #payment_attempts_header {
		margin:5px 10px;
		font-size:16px;
	}
	#spacer_text {
		margin:20px 10px 10px 10px;
	}
	#payment_attempts,	#credit_card_id {
		margin:0px 10px 0px 10px;
	}
	#load_invoice_into_pos, #charge_card_on_file, #manage_cards {
		text-align:center;
		margin:10px;
		padding:10px 10px;
		width:250px;
		background:#336699;
		color:white;
		font-weight:bold;
		cursor:pointer;
	}
	#charge_card_on_file, #manage_cards {
		width:150px;
	}
	#manage_cards {
		padding:5px 10px;
	}
	#payment_attempts {
		border-bottom:2px solid #838383;
		padding-bottom:10px;
	}
	#payment_attempts td {
		padding:8px 0px;
	}
	#header_row {
		border-bottom:1px #838383 solid;
	}
</style>
<script>
$(document).ready(function(){
    initialize_pay_window();
});
function initialize_pay_window() {
	$('#load_invoice_into_pos').click(function(e){
        e.preventDefault();
        var invoice_id, redirect;

        <?php if($this->session->userdata('sales_v2') == '1'){ ?>
        invoice_id =  "<?=$invoice_id?>";

        $.post('<?php echo site_url('customers/add_invoice_to_cart'); ?>', {invoice_id: invoice_id}, function(response){
            window.location = '<?php echo site_url('v2/home'); ?>#sales';
        },'json');

        <?php }else{ ?>
        invoice_id = "INV <?=$invoice_number?>";

        redirect = function(){
            window.location = '<?php echo site_url('sales'); ?>';
        };
        sales.add_item(invoice_id, redirect);
        <?php } ?>
        return false;
    });
	$('#manage_cards').click(function(e){
		$.colorbox2({href:'index.php/customers/manage_credit_cards/<?=$person_id?>', width:600});
	});
	$('#charge_card_on_file').click(function(e){
		var ccid = $('#credit_card_id').val();
		var cc = $('#credit_card_id option:selected').text();
		if (ccid > 0) {
			if (parseFloat('<?=($total - $paid)?>') > parseFloat(0) )
			{
				// CONFIRM CHARGING CARD
				if (confirm("You are about to charge $<?=($total - $paid)?> to "+cc+". Would you like to proceed?"))
				{
					// DISABLE BUTTON UNTIL FINISHED
					// VISUALLY INDICATE THAT WORK IS HAPPENING
					// CHARGE CARD AND SAVE SALE
					// Indicate that we're charging
					$('#cboxContent').mask();
					$.ajax({
			           type: "POST",
			           url: "index.php/invoices/pay_off_invoice/<?=$invoice_id?>/"+ccid,
			           data: '',
			           success: function(response){
			           		if (response.success) {
			           			set_feedback(response.message,'success_message',false);
								// Update row
								update_row(<?=$invoice_id?>,'<?php echo site_url("$controller_name/get_row")?>');
								highlight_row(<?=$invoice_id?>);
								// Close colorbox
								$.colorbox.close();
							}
							else {
								set_feedback(response.message,'error_message',false);
								var selected_card_id = $('#credit_card_id').val();
								$.colorbox({'href':'index.php/invoices/view_pay_options/<?=$invoice_id?>/'+selected_card_id, 'width':400, 'title':'Payment Options', onComplete:function(){initialize_pay_window();}});
							}
							
			           		$('#cboxContent').unmask();
					    },
			            dataType:'json'
			        });
				}
				else {
			   		alert('This invoice has already been paid');
				}
			}
		}
		else {
			alert('No card was selected');
		}
	});
}
function reload_customer_credit_cards(selected_card_id)
{	
	selected_card_id = selected_card_id == undefined ? '0' : selected_card_id ;
	$.colorbox2.close();
	$.colorbox({'href':'index.php/invoices/view_pay_options/<?=$invoice_id?>/'+selected_card_id, 'width':400, 'title':'Payment Options', onComplete:function(){initialize_pay_window();}});
}
</script>
<?php
// OPTION 2 TRY TO CHARGE CARD AGAIN

// OPTION 3 ADD/CHANGE CARD ON FILE AND TRY TO CHARGE AGAIN
