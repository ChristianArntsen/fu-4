<html>
<head>
    <title>Member Billing Daemon Test</title>
    <script>
        var root = 'http://development.foreupsoftware.com/index.php/daemon/';

        function getDate(){
            var date = document.getElementById('date').value;
            return date;
        }

        function iframeLoading(){
            var loadingEl = document.getElementById('iframe-loading');
            loadingEl.innerHTML = 'Loading...';
        }

        function iframeLoadingDone(){
            var loadingEl = document.getElementById('iframe-loading');
            loadingEl.innerHTML = 'Response';
        }

        function generateChargeJobs(){
            var iframe = document.getElementById('response');
            iframe.src = root + 'startMemberBillingCharges?current_time=' + getDate();
            iframeLoading();
        }

        function generateStatementJobs(){
            var iframe = document.getElementById('response');
            iframe.src = root + 'startMemberBillingStatements?current_time=' + getDate();
            iframeLoading();
        }

        function processChargeJobs(){
            var iframe = document.getElementById('response');
            iframe.src = root + 'processRecurringCharges?current_time=' + getDate();
            iframeLoading();
        }

        function processStatementJobs(){
            var iframe = document.getElementById('response');
            iframe.src = root + 'processStatementJobs?current_time=' + getDate();
            iframeLoading();
        }

        function processStatementPdfJobs(){
            var iframe = document.getElementById('response');
            iframe.src = root + 'processStatementPdfJobs?current_time=' + getDate();
            iframeLoading();
        }

        function reset(){
            var iframe = document.getElementById('response');
            iframe.src = root + 'testMemberBilling/1';
            iframeLoading();
        }
    </script>
    <style>
        button {
            padding: 5px 10px;
            margin: 5px 0px 5px 0px;
        }
    </style>
</head>
<body>
    <div>
        <h1>Member Billing Daemon Test</h1>
        <label>Date</label>
        <input type="text" name="date" id="date" value="2030-04-01" placeholder="YYYY-MM-DD">
    </div>
    <div style="width: 300px; float: left; height: 1200px;">
        <h2>Daemon Tests</h2>
        <button id="generate-charge-jobs" onclick="generateChargeJobs();">1. Generate Recurring Charge Jobs</button>
        <button id="generate-statement-jobs" onclick="generateStatementJobs();">2. Generate Statement Jobs</button>
        <button id="process-charge-jobs" onclick="processChargeJobs();">3. Process Recurring Charge Jobs</button>
        <button id="process-statement-jobs" onclick="processStatementJobs();">4. Process Statement Jobs</button>
        <button id="process-statement-pdf-jobs" onclick="processStatementPdfJobs();">5. Process Statement PDF Jobs</button>

        <button id="process-statement-pdf-jobs" style="margin-top: 200px; background-color: red; color: white;" onclick="reset();">Reset Member Billing Data</button>
    </div>
    <div style="width: 1000px; float: right; height: 1200px;">
        <div id="iframe-loading" style="width: 300px; height: 30px; line-height: 30px; background-color: #E0E0E0; padding: 0px 10px;">
            Response
        </div>
        <iframe id="response" style="width: 100%; height: 700px; display: block; border: 2px solid #E0E0E0; background: #F5F5F5" onload="iframeLoadingDone();"></iframe>
    </div>
</body>