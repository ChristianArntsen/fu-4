<ul id="error_message_box"></ul>
<?php echo form_open('teesheets/save/'.(isset($teesheet_info->teesheet_id) ? $teesheet_info->teesheet_id : '0'),array('id'=>'teesheet_form')); ?>
<fieldset id="reason_basic_info">
    <legend><?php echo lang("config_refund_reasons"); ?></legend>

    <div class="field_row clearfix">
        <?php echo form_label(lang('config_add_reason').':', 'name',array('class'=>'')); ?>
        <div class='form_field'>
            <?php echo form_input(array(
                    'name'=>'reason',
                    'size'=>'40',
                    'id'=>'reason',
                    'value'=>'')
            );?>
            <?php echo form_input(array(
                    'type'=>'button',
                    'value'=>'Add',
                    'id'=>'save_reason'
                ));
            ?>
        </div>
    </div>
    <?php foreach($reasons as $reason) { ?>
        <div class="field_row clearfix" id="reason_row_<?=$reason['reason_id']?>">
            <?php echo form_label('', '',array('class'=>'')); ?>
            <div class='form_field'>
                <a class="delete_reason" data-reason-id="<?=$reason['reason_id']?>" href="#">x</a>
                <?php echo $reason['label'];?>
            </div>
        </div>
    <?php } ?>
</fieldset>
</form>
<script>
    $(document).ready(function(){
        $('#save_reason').on('click', function(e){
            e.preventDefault();
            // Save reason
            var reason = $('#reason').val();
            if (reason != '') {
                $.ajax({
                    type: "POST",
                    url: "index.php/config/save_reason",
                    data: "label=" + reason,
                    success: function (response) {
                        if (response.success) {
                            // Add row
                            $('#reason_basic_info').append(
                                "<div class='field_row clearfix' id='reason_row_" + response.reason_id + "'>" +
                                '<?php echo form_label('', '',array('class'=>'')); ?>' +
                                "<div class='form_field'>" +
                                "<a class='delete_reason' data-reason-id='" + response.reason_id + "' href='#'>x</a>" +
                                reason +
                                "</div>" +
                                "</div>"
                            );
                            $.colorbox.resize();
                            // Clear reason box
                            $('#reason').val('');
                            // Re-initialize rows
                            initialize_reason_rows();
                        }
                        else {

                        }
                    },
                    dataType: 'json'
                });
            }
        });
        initialize_reason_rows();
    });
    function initialize_reason_rows() {
        $('.delete_reason').off('click').on('click', function(e){
            e.preventDefault();
            var reason_id = $(e.target).data('reason-id');
            // Delete reason
            $.ajax({
                type: "POST",
                url: "index.php/config/delete_reason/"+reason_id,
                data: "",
                success: function(response){
                    if (response.success) {
                        $('#reason_row_'+reason_id).remove();
                        $.colorbox.resize();
                    }
                    else {

                    }
                },
                dataType:'json'
            });
        });
    }
</script>
<style>
    fieldset#reason_basic_info div.field_row label {
        width:115px;
    }
</style>