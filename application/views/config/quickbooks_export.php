<style>
.quickbooks_map tbody {
	border-bottom: 25px solid white;
}

.quickbooks_map th {
	text-align: left;
	background-color: #F0F0F0;
	border-bottom: 2px solid #DDD;
	padding: 5px;
	font-size: 1.4em;
}

.quickbooks_map th.class {
	width: 75px;
}

.quickbooks_map tbody tr:first-child td {
	padding-top: 10px;
}

.quickbooks_map td.label {
	font-size: 17px;
	padding-right: 10px;
}

.quickbooks_map th.label {
	width: 250px;
}

.quickbooks_map .sub-heading th {
	font-size: 1em;
	font-weight: normal;
}

.quickbooks_map .sales th {
	text-align: center;
}

.quickbooks_map th.account {
	width: 200px;
}

#quickbooks_account_mapping p {
	font-size: 1.2em;
	padding: 10px 0px 0px 0px;
}

span.req {
	color: red;
	font-style: bold;
	font-size: 1.2em;
}

.quickbooks_map td {
	padding: 2px 25px 0px 0px;
}

.quickbooks_map td input {
	display: block !important;
	margin: 0px;
	width: 100% !important;
	float: none !important;
}

#quickbooks_export label.wide {
	width: 150px;
	float: left;
}

#quickbooks_export div.form_field {
	float: left;
	width: 500px;
	overflow: hidden;
}

#quickbooks_export div.form_field label {
	width: 300px;
	display: block;
	overflow: hidden;
	margin: 0px;
	padding: 0px;
	line-height: 30px;
	height: 30px;
	cursor: pointer;
	float: none;
}

#quickbooks_account_mapping .ui-widget-content {
	background-color: transparent;
}

#quickbooks_account_mapping .ui-expandable-title {
	padding: 5px;
}
</style>

<h2 style="padding-bottom: 5px; margin-bottom: 10px; border-bottom: 1px solid #BBBBBB;">QuickBooks Export</h2>
<form name="quickbooks_export" method="get" action="<?php echo site_url('quickbooks_export'); ?>" id="quickbooks_export">
	<div class="field_row clearfix">
		<label class="wide">Date Range</label>
		<div class="form_field">
			<input type="text" class="date-time-picker" name="start_date" value="<?php echo date('m/d/Y'); ?>" /> 
			to <input type="text" name="end_date" value="<?php echo date('m/d/Y'); ?>" class="date-time-picker" />		
		</div>
	</div>
	<div class="field_row clearfix">
		<label class="wide">Data</label>
		<div class="form_field">
			<label><input type="checkbox" name="data[sales]" value="1" checked /> Sales - Revenue/Payments</label>
			<label><input type="checkbox" name="data[inventory_cogs]" value="1" checked /> Sales - COGS/Inventory</label>
			<label><input type="checkbox" name="data[receivable_adjustments]" value="1" checked /> Manual A/R Adjustments</label>
			<label><input type="checkbox" name="data[receivings]" value="1" checked /> Receivings</label>
		</div>
	</div>

	<?php if($track_cash == 1){ ?>
	<div class="field_row clearfix">
		<label class="wide" for="quickbooks_register_log">Include Cash Log</label>
		<div class="form_field">
			<input type="checkbox" id="quickbooks_register_log" name="register_log" value="1" checked />
		</div>
	</div>	
	<?php  } ?>

	<div class="field_row clearfix">
		<label class="wide">Terminal</label>
		<div class="form_field">
			<select name="terminal">
				<option value="all" selected>- All -</option>
				<?php foreach($terminals as $terminal){ ?>
				<option value="<?php echo $terminal->terminal_id; ?>"><?php echo $terminal->label; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>	
	<input name="submit" type="submit" value="Export Data" style="float: none; display: block; margin: 10px 10px 10px 0px" />
</form>

<div id="quickbooks_account_mapping">
<p>
	<span class="req">*</span> Denotes required fields
</p>
<p style="padding-bottom: 10px;">
	Make sure Quickbooks account names or numbers are entered exactly.
	If using account names, sub accounts are denoted with a colon ':'. Eg. 'Sales' and 'Sales:Food', etc.
</p>
<form name="quickbooks_export_settings" method="post" id="form_quickbooks_export_settings">
	<table class="quickbooks_map">	
		<thead>
			<tr>
				<th class="label">Misc</th>
				<th class="account">Account <span class="req">*</span></th>
				<th class="class">Class</th>
				<th colspan="5">&nbsp;</th>
			</tr>
		</thead>	
		<tbody>	
			<tr>
				<td class="label">
					Cash Drawer Variance
				</td>
				<td>
					<?php echo form_input(array('name' => "cash_variance[account]", 'value'=>$accounts['cash_variance']['account'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => "cash_variance[class]", 'value'=>$accounts['cash_variance']['class'])); ?>
				</td>				
				<td colspan="4">
					If the cash drawer is off, the difference will be applied to this account (Usually a Quickbooks 'Expense' account).
				</td>		
			</tr>
			<?php 
			$category_name = 'Account Payments';
			$md5_category = md5($category_name);?>	
			<tr>
				<td class="label">
					Accounts Receivable Payments 
				</td>
				<td>
					<?php echo form_input(array('name' => 'categories['.$md5_category.'][income]', 'value'=>$accounts['categories'][$md5_category]['income'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => 'categories['.$md5_category.'][income_class]', 'value'=>$accounts['categories'][$md5_category]['income_class'])); ?>
				</td>					
				<td colspan="4">
					Payments by members to pay off their accounts (Quickbooks 'Other Current Asset' account). 
					Should be the same account as 'Member Account','Customer Credit' and 'Invoice Charge' payments below.
				</td>				
			</tr>
			<tr>
				<td class="label">
					Accounts Receivable Adjustments
				</td>
				<td>
					<?php echo form_input(array('name' => "receivable_adjustments[account]", 'value'=>$accounts['receivable_adjustments']['account'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => "receivable_adjustments[class]", 'value'=>$accounts['receivable_adjustments']['class'])); ?>
				</td>
				<td colspan="4">
					Manual adjustments made to member accounts. 
					Funds will move from this account into 'Accounts Receivable Payments' (above).
				</td>		
			</tr>			
			<tr>
				<td class="label">
					Accounts Payable (Suppliers) 
				</td>
				<td>
					<?php echo form_input(array('name' => 'accounts_payable[account]', 'value'=>$accounts['accounts_payable']['account'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => 'accounts_payable[class]', 'value'=>$accounts['accounts_payable']['class'])); ?>
				</td>
				<td colspan="4">
					Handles payments made to suppliers when receiving inventory (Quickbooks 'Accounts Payable' account).
				</td>					
			</tr>
			<tr>
				<td class="label">
					Rain Checks Issued (Debit)
				</td>
				<td>
					<?php echo form_input(array('name' => 'rain_checks_issued_debit[account]', 'value'=>$accounts['rain_checks_issued_debit']['account'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => 'rain_checks_issued_debit[class]', 'value'=>$accounts['rain_checks_issued_debit']['class'])); ?>
				</td>
				<td colspan="4">
					Account to debit when a rain check is issued
				</td>					
			</tr>
			<tr>
				<td class="label">
					Rain Checks Issued (Credit)
				</td>
				<td>
					<?php echo form_input(array('name' => 'rain_checks_issued_credit[account]', 'value'=>$accounts['rain_checks_issued_credit']['account'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => 'rain_checks_issued_credit[class]', 'value'=>$accounts['rain_checks_issued_credit']['class'])); ?>
				</td>
				<td colspan="4">
					Account to credit when a rain check is issued
				</td>					
			</tr>
			<tr>
				<td class="label">
					Gift Card Adjustments (Debit)
				</td>
				<td>
					<?php echo form_input(array('name' => 'gift_card_adjustments_debit[account]', 'value'=>$accounts['gift_card_adjustments_debit']['account'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => 'gift_card_adjustments_debit[class]', 'value'=>$accounts['gift_card_adjustments_debit']['class'])); ?>
				</td>
				<td colspan="4">
					Account to debit when the value of a gift card is edited manually
				</td>					
			</tr>
			<tr>
				<td class="label">
					Gift Card Adjustments (Credit)
				</td>
				<td>
					<?php echo form_input(array('name' => 'gift_card_adjustments_credit[account]', 'value'=>$accounts['gift_card_adjustments_credit']['account'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => 'gift_card_adjustments_credit[class]', 'value'=>$accounts['gift_card_adjustments_credit']['class'])); ?>
				</td>
				<td colspan="4">
					Account to credit when the value of a gift card is edited manually, usually the same as the Gift Card payment account (Liability/Payable)
				</td>					
			</tr>			
			<tr>
				<td class="label">
					Partial Refunds
				</td>
				<td>
					<?php echo form_input(array('name' => 'refunds[account]', 'value'=>$accounts['refunds']['account'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => 'refunds[class]', 'value'=>$accounts['refunds']['class'])); ?>
				</td>
				<td colspan="4">
					Usually an 'Expense' account to offset sales income
				</td>					
			</tr>					
		</tbody>
		
		<thead>
			<tr>
				<th class="label">Payments</th>
				<th>Account <span class="req">*</span></th>
				<th>Class</th>
				<th colspan="4">&nbsp;</th>
			</tr>
		</thead>
		<tbody>	
		<?php $payment_types = array();
		$payment_types[] = array('name' => 'Cash', 'key' => 'cash');
		$payment_types[] = array('name' => 'Check', 'key' => 'check');
		$payment_types[] = array('name' => 'Credit Card', 'key' => 'credit_card');
		$payment_types[] = array('name' => 'Credit Card - VISA', 'key' => 'credit_card_visa');
		$payment_types[] = array('name' => 'Credit Card - Discover', 'key' => 'credit_card_discover');
		$payment_types[] = array('name' => 'Credit Card - MasterCard', 'key' => 'credit_card_mastercard');
		$payment_types[] = array('name' => 'Credit Card - Amex', 'key' => 'credit_card_amex');
		$payment_types[] = array('name' => 'Credit Card - Diners', 'key' => 'credit_card_diners');
		$payment_types[] = array('name' => 'Credit Card - JCB', 'key' => 'credit_card_jcb');
		$payment_types[] = array('name' => 'Member Account (A/R)', 'key' => 'member_account');
		$payment_types[] = array('name' => 'Customer Credit (A/R)', 'key' => 'customer_credit');
		$payment_types[] = array('name' => 'Invoice Charge (A/R)', 'key' => 'invoice_charge');
		$payment_types[] = array('name' => 'Tournament Winnings (A/R)', 'key' => 'tournament_winnings');
		$payment_types[] = array('name' => 'Gift Cards', 'key' => 'gift_card');
		$payment_types[] = array('name' => 'Gift Card Discounts', 'key' => 'gift_card_discount');
		$payment_types[] = array('name' => 'Punch Cards', 'key' => 'punch_card');
		$payment_types[] = array('name' => 'Rain Checks', 'key' => 'rain_check');
		$payment_types[] = array('name' => 'Coupons', 'key' => 'coupons');
		$payment_types[] = array('name' => 'Loyalty Points', 'key' => 'loyalty_points');
		$payment_types[] = array('name' => 'Tips', 'key' => 'tips');
		
		foreach($payment_types as $index => $payment){ ?>
			<tr>
				<td class="label">
					<?php echo $payment['name']; ?> 
				</td>
				<td>
					<?php echo form_input(array('name' => "payments[{$payment['key']}][account]", 'value'=>$accounts['payments'][ $payment['key'] ]['account'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => "payments[{$payment['key']}][class]", 'value'=>$accounts['payments'][ $payment['key'] ]['class'])); ?>
				</td>				
			</tr>
		<?php } ?>
		</tbody>
		
		<?php if(!empty($custom_payments)){ ?>
		<thead>
			<tr>
				<th>Custom Payments</th>
				<th>Account <span class="req">*</span></th>
				<th>Class</th>
				<th colspan="5">&nbsp;</th>
			</tr>
		</thead>			
		<tbody>
		<?php foreach($custom_payments as $custom_payment){ 
			$custom_payment['key'] = $custom_payment['custom_payment_type']; ?>
			<tr>
				<td class="label">
					<?php echo $custom_payment['label']; ?> 
				</td>
				<td>
					<?php echo form_input(array('name' => "payments[{$custom_payment['key']}][account]", 'value'=>$accounts['payments'][ $custom_payment['key'] ]['account'])); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => "payments[{$custom_payment['key']}][class]", 'value'=>$accounts['payments'][ $custom_payment['key'] ]['class'])); ?>
				</td>				
			</tr>
     	<?php } ?>
		</tbody>
		<?php } ?>
		
		<thead>
			<tr>
				<th>Taxes</th>
				<th>Account <span class="req">*</span></th>
				<th>Class</th>
				<th>Vendor</th>
				<th colspan="4">&nbsp;</th>
			</tr>
		</thead>	
		<tbody>
		<?php 
		if(!empty($taxes)){
			foreach($taxes as $tax){ 
			$tax_key = md5($tax['key']); ?>
				<tr>
					<td class="label">
						<?php echo $tax['key']; ?>
					</td>
					<td>
						<?php echo form_input(array('name' => "taxes[{$tax_key}][account]", 'value'=>$accounts['taxes'][$tax_key]['account'])); ?>
					</td>
					<td>
						<?php echo form_input(array('name' => "taxes[{$tax_key}][class]", 'value'=>$accounts['taxes'][$tax_key]['class'])); ?>
					</td>					
					<td>
						<?php echo form_input(array('name' => "taxes[{$tax_key}][vendor]", 'value'=>$accounts['taxes'][$tax_key]['vendor'])); ?>
					</td>
				</tr>
		<?php } } else { ?>
				<tr>
					<td colspan="7">
						No taxes have been set up yet
					</td>
				</tr>			
		<?php } ?>
		</tbody>
		
		<thead>
			<tr class="sales">
				<th style="text-align: left;">Sales Categories</th>
				<th colspan="2">Income <span class="req">*</span></th>
				<th colspan="2">COGS</th>
				<th colspan="2">Inventory</th>
			</tr>
			<tr class="sub-heading">
				<th>&nbsp;</th>
				<th class="account">Account <span class="req">*</span></th>
				<th class="class">Class</th>
				<th class="account">Account</th>
				<th class="class">Class</th>
				<th class="account">Account</th>
				<th class="class">Class</th>			
			</tr>
		</thead>
		<tbody>
			<?php 
			if(!empty($categories)){
			foreach($categories as $category){ 
			$md5_category = md5($category['category']); 
			
			// Special categories to skip (displayed above in Misc accounts)
			if($category['category'] == 'Invoice Payments' || $category['category'] == 'Account Payments'){
				continue;
			} 
			
			$onlyIncome = false;
			if($category['category'] == 'Carts'  || $category['category'] == 'Green Fees'){
				$onlyIncome = true;
			} ?>
			<tr>
				<td class="label">
					<? if($category['category'] == ''){ ?>
					<em>- No Category -</em>
					<?php }else{ ?>
					<?php echo $category['category']; ?>
					<?php } ?>
				</td>
				<td class="account">
					<?php echo form_input(array('name' => "categories[{$md5_category}][income]", 'value'=>$accounts['categories'][$md5_category]['income'])); ?>				
				</td>
				<td>
					<?php echo form_input(array('name' => "categories[{$md5_category}][income_class]", 'value'=>$accounts['categories'][$md5_category]['income_class'])); ?>					
				</td>
				<?php if(!$onlyIncome){ ?>
				<td>
					<?php echo form_input(array('name' => "categories[{$md5_category}][cogs]", 'value'=>$accounts['categories'][$md5_category]['cogs'], 'class'=>'cogs_account')); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => "categories[{$md5_category}][cogs_class]", 'value'=>$accounts['categories'][$md5_category]['cogs_class'])); ?>					
				</td>				
				<td>
					<?php echo form_input(array('name' => "categories[{$md5_category}][inventory]", 'value'=>$accounts['categories'][$md5_category]['inventory'], 'class'=>'inventory_account')); ?>
				</td>
				<td>
					<?php echo form_input(array('name' => "categories[{$md5_category}][inventory_class]", 'value'=>$accounts['categories'][$md5_category]['inventory_class'])); ?>					
				</td>
				<?php } ?>					
			</tr>					
			<?php } } else { ?>
			<tr>
				<td colspan="7">
					No sales categories have been created yet
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>	
	<input type="submit" name="submit" value="Save" />
</form>
</div>
	
<script>
function validate_accounts(){
	var all_fields_mapped = true;
	var inventory_cogs_mapped = true;
	
	// Check if required fields are filled in (ignore class fields)
	$('#form_quickbooks_export_settings').find('input').each(function(){
		
		if($(this).val() == '' && (
			$(this).attr('name').indexOf('[class]') == -1 &&
			$(this).attr('name').indexOf('[income_class]') == -1 &&
			$(this).attr('name').indexOf('[cogs_class]') == -1 &&
			$(this).attr('name').indexOf('[inventory_class]') == -1 &&
			$(this).attr('name').indexOf('[vendor]') == -1 &&
			$(this).attr('name').indexOf('[cogs]') == -1 &&
			$(this).attr('name').indexOf('[inventory]') == -1
		)){
			all_fields_mapped = false;
		}
		
		if($(this).val() == '' && $(this).attr('name').indexOf('[cogs]') >= 0){
			var row = $(this).parents('tr').first();
			if(row.find('input.inventory_account').val() != ''){
				inventory_cogs_mapped = false;
			}
		
		}else if($(this).val() == '' && $(this).attr('name').indexOf('[inventory]') >= 0){
			var row = $(this).parents('tr').first();
			if(row.find('input.cogs_account').val() != ''){
				inventory_cogs_mapped = false;
			}
		}				
	});
	
	if(!inventory_cogs_mapped){
		set_feedback('Inventory and COGS accounts must both be filled in or both left blank for each category', 'error_message', true, 5000);
		$('#quickbooks_account_mapping').expandable('open');
		return false;
	}		
	
	if(!all_fields_mapped){
		set_feedback('All required Quickbooks accounts must be mapped before exporting', 'error_message');
		$('#quickbooks_account_mapping').expandable('open');
		return false;
	}
	
	return true;
}
	
$(function(){ 
	$('input.date-time-picker').datepicker();

	$('#quickbooks_export').on('submit', function(e){
		return validate_accounts();
	});

	$('#form_quickbooks_export_settings').on('submit', function(e){
		e.preventDefault();
		
		if(!validate_accounts()){
			return false;
		}

		var data = {};
		var a = $(this).serializeArray();
		
		$.each(a, function() {
			if (data[this.name] !== undefined) {
				if (!data[this.name].push) {
					data[this.name] = [data[this.name]];
				}
				data[this.name].push(this.value || '');
			} else {
				data[this.name] = this.value || '';
			}
		});
		
		$.post('<?php echo site_url('config/save/quickbooks_export'); ?>', data, function(response){
			if(response.success){
				set_feedback('Quickbooks account settings saved', 'success_message');
			}else{
				set_feedback('Error saving Quickbooks settings', 'error_message');
			}
		},'json');
		
		return false;
	});

	$('#quickbooks_account_mapping').expandable({title: 'Quickbooks Account Settings <em>(Click to expand)</em>'});			
});
</script>
