<form id="save_payment_types" action="index.php/config/save_custom_payments" method="POST">
    <table id="custom_payment_types_table">
        <tbody>
            <tr>
                <td colspan="2"><a href='#' id='add_payment_type'>Add Payment Type</a></td>
            </tr>
        <?php foreach($payment_types as $type) { ?>
            <tr>
                <td><input type='text' class='' value='<?=$type['label']?>' name="custom_payment_type[]" disabled/></td>
                <td><a href='#' class="remove_custom_payment_type" id="<?=$type['custom_payment_type']?>">Remove</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <input class='save_custom_payments' type="submit" value="Save"/>
</form>
<style>
    #custom_payment_types_table {
        margin:10px;
    }
    #custom_payment_types_table td {
        padding:4px;
    }
    #add_payment_type, .remove_custom_payment_type {
        background: #349AC5;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5',endColorstr='#4173b3');
        background: -webkit-linear-gradient(top,#349AC5,#4173B3);
        background: -moz-linear-gradient(top,#349AC5,#4173B3);
        color: white;
        display: block;
        font-size: 14px;
        font-weight: normal;
        height: 32px;
        padding: 0px;
        width: 140px;
        text-align: center;
        text-shadow: 0px -1px 0px black;
        border-radius: 4px;
        box-shadow: inset 0px 1px 1px 0px rgba(255,255,255,0.5),0px 3px 1px -2px rgba(255,255,255,.2);
        border: 1px solid #232323;
        margin: 0px 0px 6px 0px;
        line-height:32px;
    }
    .remove_custom_payment_type {
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#C53434',endColorstr='#B34141');
        background: -webkit-linear-gradient(top,#C53434,#B34141);
        background: -moz-linear-gradient(top,#C53434,#B34141);
    }
    input.save_custom_payments {
        margin:auto;
    }
</style>
<script>
    function initialize_rows() {
        $('.remove_custom_payment_type').on('click', function (e){
            e.preventDefault();
            console.dir($(this).parent().parent());
            // if we have a custom_payment_type, then we're going to ajax delete the payment type
            var custom_payment = $(this);
            var type = custom_payment.attr('id');
            if (type != undefined) {
                $.ajax({
                    type: "POST",
                    url: "index.php/config/delete_custom_payment/"+type,
                    data: '',
                    success: function(response){
                        custom_payment.parent().parent().remove();
                        $.colorbox2.resize();
                    },
                    dataType:'json'
                });
            }
            else {
                // else just remove and resize
                custom_payment.parent().parent().remove();
                $.colorbox2.resize();
            }
        });
    }
    $(document).ready(function() {
        var submitting = false;
        $('#save_payment_types').validate({
            submitHandler: function (form) {
                if (submitting) return;
                submitting = true;
                $(form).mask("<?php echo lang('common_wait'); ?>");
                $(form).ajaxSubmit({
                    success: function (response) {
                        if (response.success) {
                            $.colorbox2.close();
                        }
                        else {
                            // display error message
                        }
                        submitting = false;
                    },
                    dataType: 'json'
                });
            }
        });
        initialize_rows();
        $('#add_payment_type').on('click', function (e){
            e.preventDefault();
            $('#custom_payment_types_table tbody').append("<tr><td><input type='text' class='' value='' name='custom_payment_type[]'/></td><td><a href='#' class='remove_custom_payment_type'>Remove</a></td></tr>");
            $.colorbox2.resize();
            initialize_rows();
        });
    });
</script>