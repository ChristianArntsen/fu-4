<style>
	select.price_class_menu {
		padding: 5px;
		margin-right: 10px;
		float: left;
		display: block;
		margin: 6px 10px 10px 0px;
	}
</style>
<div class="teesheet_select">
	<div class="breadcrumbs">
		<span class="crumb_link"><a href="#" rel="crumb_teesheets">Teesheets</a></span>
		 > 
		<span class="crumb_link"><a href="#" rel="crumb_teesheet" data-teesheet-id="<?php echo (int) $teesheet->teesheet_id; ?>"><?=$teesheet->title?></a></span>
		 > 
		<span class="crumb_link"><?=$season->season_name?></span>
	</div>
</div>
<div class="columns_container">
	<div class="column_left">
		<div class="details">
			<h2><?=$season->season_name?></h2>
			<ul class="details">
				<li><h4><?=convert_to_short_date($season->start_date)?> - <?=convert_to_short_date($season->end_date)?></h4></li>
				<li>Holiday:  <span><?=$season->holiday ? "Yes" : "No"?></span></li>
			</ul>
			<a href="#" rel="edit_season" class="terminal_button inline_button" style="float: left;" title="Edit Season">Edit Season</a>
		</div>
	</div>
	<div class="column_main">
		<div class="list-header">
			<h2>Pricing</h2>
			<?php echo form_dropdown('class_id', $price_class_dropdown, null, "class='price_class_menu'"); ?>
			<a class="terminal_button" id="new_price_class" href="#" style="display: none;">Add Price Class</a>
		</div>
		<ul class="pricing">
			<?php foreach ($price_classes as $price){ ?>
			<li class="pricing" data-class-id="<?php echo (int) $price['price_class_id']; ?>">
				<h3>
					<? echo $price['price_class_name'] ?>
					<?php if($price['price_class_default'] == 1){ ?>
					<em style="font-weight: normal;">(Default)</em>
					<?php } ?>
				</h3>
				<span style="color: #666;"><?php echo $price['timeframe_names']; ?></span>
			</li>
			<?php } ?>
		</ul>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	<? if ($show !== false){ ?>console.log("We got the show variable in.");
	$.colorbox({href:"index.php/price_categories/view_price_category/<?=$show?>/<?=$season->season_id?>"});<? }?>
	$('a[rel=edit_season]').on('click',function(){
		$.colorbox({href:"index.php/seasons/view_season/<?=$season->season_id?>", width: 675});
		return false;
	});
	
	$('ul.pricing').on('click', 'li', function(e){
		var id = $(this).data('class-id');
		$.colorbox({href:"index.php/seasons/view_price/" + id + "/<?=$season->season_id?>", width: 600});
		e.preventDefault();
	});
	
	$('select.price_class_menu').on('change', function(e){
		if($(this).val() > 0){
			$('#new_price_class').show();
		}else{
			$('#new_price_class').hide();
		}
	});
	
	$('#new_price_class').on('click', function(e){
		var class_id = $('select.price_class_menu').val();
		var season_id = <?php echo (int) $season->season_id; ?>;
		var price_name = $('select.price_class_menu').find('option[value='+class_id+']').text();
		
		$.post('<?php echo site_url("seasons/add_price/{$season->season_id}"); ?>', {class_id: class_id}, function(response){
			
			if(response.success){
				set_feedback(response.message, 'success_message', false, 1500);
				$('select.price_class_menu').find('option[value='+class_id+']').remove();
				$('ul.pricing').prepend('<li class="pricing" data-class-id="'+class_id+'"><h3>'+price_name+'</h3></li>');
			}else{
				set_feedback(response.message, 'error_message', false, 1500);
			}
		},'json');
		
		e.preventDefault();			
	});
});
</script>
