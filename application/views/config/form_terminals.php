<ul id="error_message_box"></ul>
<?php
echo form_open('config/save_terminal_options/'.$terminal_info['terminal_id'], array('id'=>'terminal_form'));
?>
<fieldset id="terminal_basic_info">
<legend><?php echo lang("config_terminal_basic_information"); ?></legend>
<div class=''>
<div class="field_row clearfix">
<?php echo form_label(lang('config_terminal_name').':', 't_label',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_label',
		'id'=>'t_label',
		'value'=>$terminal_info['label'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_quickbutton_tab').':', 't_quickbutton_tab',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_quickbutton_tab', array("1"=>'1', '2'=>'2', '3'=>'3'), $terminal_info['quickbutton_tab']);?>
	</div>
</div>
<div id = 'custom_settings' >
<?php if ($this->config->item('mercury_id') && false) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('config_mercury_id').':', 't_mercury_id',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_mercury_id',
		'id'=>'t_mercury_id',
		'placeholder'=>$this->config->item('mercury_id') == $terminal_info['mercury_id'] ? '' : $this->config->item('mercury_id'),
		'value'=>$terminal_info['mercury_id'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_mercury_password').':', 't_mercury_password',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_mercury_password',
		'id'=>'t_mercury_password',
		'placeholder'=>$this->config->item('mercury_password') == $terminal_info['mercury_password'] ? '' : $this->config->item('mercury_password'),
		'value'=>$terminal_info['mercury_password'])
	);?>
	</div>
</div>
<?php } ?>
<?php if ($this->config->item('ets_key') && false) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('config_ets_key').':', 't_ets_key',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_ets_key',
		'id'=>'t_ets_key',
		'placeholder'=>$this->config->item('ets_key') == $terminal_info['ets_key'] ? '' : $this->config->item('ets_key'),
		'value'=>$terminal_info['ets_key'])
	);?>
	</div>
</div>
<?php } ?>
<div class="field_row clearfix">
    <?php echo form_label(lang('config_track_cash').':', 't_track_cash',array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_dropdown('t_track_cash', array('null'=>'Default', "0"=>'No', '1'=>'Yes'), $terminal_info['use_register_log']);?>
    </div>
</div>
<div class="field_row clearfix">
    <?php echo form_label('Multiple Cash Drawers', 'multi_cash_drawers', array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_dropdown('multi_cash_drawers', array("0"=>'No', '1'=>'Yes'), $terminal_info['multi_cash_drawers']);?>
    </div>
</div>
<div class="field_row clearfix cash_drawer_number">
    <?php echo form_label('Cash Drawer Number', 'cash_drawer_number', array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_dropdown('cash_drawer_number', array("1"=>'#1', '2'=>'#2'), $terminal_info['cash_drawer_number']);?>
    </div>
</div>
<div class="field_row clearfix">
    <?php echo form_label(lang('config_multi_user_cash_log').':', 'persistent_logs',array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_dropdown('persistent_logs', array("0"=>'No', '1'=>'Yes'), $terminal_info['persistent_logs']);?>
    </div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_auto_print_receipts').':', 't_auto_print_receipts',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_auto_print_receipts', array('null'=>'Default', "0"=>'No', '1'=>'Yes'), $terminal_info['auto_print_receipts']);?>
	</div>
</div>
	<div class="field_row clearfix">
		<?php echo form_label('Use Web Print:', 't_webprnt',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_dropdown('t_webprnt', array('null'=>'Default', "0"=>'No', '1'=>'Yes'), $terminal_info['webprnt']);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('Force HTTPS:', 't_https',array('class'=>'wide')); ?>
		<div class='form_field force-https'>
			<?php echo form_dropdown('t_https', array('null'=>'Default', "0"=>'No', '1'=>'Yes'), $terminal_info['https']);?>
		</div>
	</div>

<div id="terminal_printers">
	<div class="field_row clearfix">
		<?php echo form_label('Sales Receipt Printer:', 't_webprnt_ip',array('class'=>'wide', 'style' => 'width: 290px;')); ?>
		<div class='form_field'>
		<?php if($this->session->userdata('multiple_printers') == 0){ ?>
		<?php echo form_input(array(
			'name'=>'t_webprnt_ip',
			'id'=>'t_webprnt_ip',
			'placeholder'=>!empty($terminal_info['webprnt_ip']) && $this->config->item('webprnt_ip') == $terminal_info['webprnt_ip'] ? '' : $this->config->item('webprnt_ip'),
			'value'=>$terminal_info['receipt_ip'])
		);?>
		<?php }else{ ?>
		<?php 
		$sales_receipt_printers = $printer_menu;
		array_pop($sales_receipt_printers);
		echo form_dropdown('t_webprnt_ip', $sales_receipt_printers, $terminal_info['receipt_ip']);?>		
		<?php } ?>
		</div>
	</div>
	
	<?php if($this->session->userdata('multiple_printers') == 1){ ?>
	<div class="field_row clearfix">
	<?php echo form_label('Default Kitchen Printer:', 'default_kitchen_printer',array('class'=>'wide', 'style' => 'width: 290px;')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('t_default_kitchen_printer', $printer_menu, $terminal_info['default_kitchen_printer']);?>	
		</div>
	</div>		
	
	<?php foreach($printer_groups as $printer_group){ ?>
	<div class="field_row clearfix">
		<?php echo form_label($printer_group['printer_group_label'].':', null, array('class'=>'wide', 'style' => 'width: 290px;')); ?>
		<div class='form_field'>
			<?php echo form_dropdown('printer_groups['.$printer_group['printer_group_id'].']', $printer_menu, $printer_group['printer_id']);?>	
		</div>
	</div>
	<?php } ?>
	<?php } ?>

	<?php if($this->session->userdata('multiple_printers') == 0){ ?>
	<div class="field_row clearfix">
	<?php echo form_label(lang('config_webprnt_hot_ip').':', 't_webprnt_hot_ip',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'t_webprnt_hot_ip',
			'id'=>'t_webprnt_hot_ip',
			'placeholder'=>$this->config->item('webprnt_hot_ip') == $terminal_info['hot_webprnt_ip'] ? '' : $this->config->item('webprnt_hot_ip'),
			'value'=>$terminal_info['hot_webprnt_ip'])
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('config_webprnt_cold_ip').':', 't_webprnt_cold_ip',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'t_webprnt_cold_ip',
			'id'=>'t_webprnt_cold_ip',
			'placeholder'=>$this->config->item('webprnt_cold_ip') == $terminal_info['cold_webprnt_ip'] ? '' : $this->config->item('webprnt_cold_ip'),
			'value'=>$terminal_info['cold_webprnt_ip'])
		);?>
		</div>
	</div>
	<?php } ?>
</div>
<script type='text/javascript'>
	$('#terminal_printers').expandable({title:'Printer Mapping'});
</script>
			
<div class="field_row clearfix">
<?php echo form_label(lang('config_cash_register').':', 't_cash_register',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_cash_register', array('null'=>'Default', "0"=>'No', '1'=>'Yes'), $terminal_info['cash_register']);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_print_tip_line').':', 't_print_tip_line',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_print_tip_line', array('null'=>'Default', "0"=>'No', '1'=>'Yes'), $terminal_info['print_tip_line']);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_signature_slip_count').':', 't_signature_slip_count',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_signature_slip_count', array('null'=>'Default', "0"=>'0', '1'=>'1', '2'=>'2'), $terminal_info['signature_slip_count']);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_credit_card_receipt_count').':', 't_credit_card_receipt_count',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_credit_card_receipt_count', array('null'=>'Default', "0"=>'0', '1'=>'1', '2'=>'2'), $terminal_info['credit_card_receipt_count']);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_non_credit_card_receipt_count').':', 't_non_credit_card_receipt_count',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_non_credit_card_receipt_count', array('null'=>'Default', "0"=>'0', '1'=>'1', '2'=>'2'), $terminal_info['non_credit_card_receipt_count']);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Auto email receipt to customer:', 't_auto_email_receipt',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_auto_email_receipt', array('null'=>'Default', '0'=>'No', '1'=>'Yes'), $terminal_info['auto_email_receipt']);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Do not print receipt if emailed:', 't_auto_email_no_print',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_auto_email_no_print', array('null'=>'Default', "0"=>'No', '1'=>'Yes'), $terminal_info['auto_email_no_print']);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Require on-screen signature for member payments', 't_require_signature_member_payments',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_require_signature_member_payments', array('null'=>'Default', "0"=>'No', '1'=>'Yes'), $terminal_info['require_signature_member_payments']);?>
	</div>
</div>
<div class="field_row clearfix">	
	<?php echo form_label(lang('config_after_sale_load').':', 'after_sale_load',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_radio(array(
            'name'=>'after_sale_load',
            'id'=>'after_sale_load_null',
            'value'=>'null',
            'checked'=>$terminal_info['after_sale_load']==null));?>
            Default
    <?php echo form_radio(array(
            'name'=>'after_sale_load',
            'id'=>'after_sale_load_0',
            'value'=>'0',
            'checked'=>$terminal_info['after_sale_load']==='0'));?>
    <?php echo ($this->permissions->course_has_module('reservations') ? 'Reservations' : 'Tee Sheet');  ?>
    <?php echo form_radio(array(
            'name'=>'after_sale_load',
            'id'=>'after_sale_load_1',
            'value'=>'1',
            'checked'=>$terminal_info['after_sale_load']==1));?>
    Sales
    </div>

	<div class="field_row clearfix">	
	<?php echo form_label(lang('common_return_policy').':', 'return_policy',array('class'=>'wide')); ?>
	        <div class='form_field'>
	        <?php echo form_textarea(array(
	            'name'=>'return_policy',
	            'id'=>'return_policy',
	            'rows'=>'4',
	            'cols'=>'30',
	            'value'=>$terminal_info['return_policy']));?>
	        </div>
	</div>
</div>
</div>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script>
// function toggle_custom_settings()
// {
	// if ($('#t_use_custom_settings').attr('checked'))
		// $('#custom_settings').show();
	// else
		// $('#custom_settings').hide();
// 		
	// $.colorbox.resize();
// }
function toggle_multi_cash_drawers(){
	if($('#multi_cash_drawers').val() == 1){
		$('#persistent_logs').val('0').attr('disabled', 'disabled');
		$('.cash_drawer_number').show();
	}else{
		$('#persistent_logs').attr('disabled', null);
		$('.cash_drawer_number').hide();
	}
}

$(document).ready(function() {	
	// toggle_custom_settings();
	// $('#t_use_custom_settings').click(function(){
		// toggle_custom_settings();
	// });

	toggle_multi_cash_drawers();
	$('#multi_cash_drawers').change(function(){
		toggle_multi_cash_drawers();
	});

	var submitting = false;
    $('#terminal_form').validate({
		submitHandler:function(form)
		{
			if($('.force-https select :selected').attr('value') === "1"){
				if(!window.confirm('You have selected "Force HTTPS".\n\n'+
									'Please check that your printers are set up to use the https: protocol!\n\n'+
									'If they are not, THE TERMINAL WILL NOT BE ABLE TO PRINT TO THEM once HSTS is enabled, '+
									'and you will have to remove the HSTS setting from the browser manually.\n\n'+
						            'A link to enable will be provided on the terminal home page ' +
						            'to enable HTTP Strict Transport Security (HSTS) for browsers accessing this terminal.\n\n'))return;
			}
			if (submitting) return;
			submitting = true;
			$('#food_and_beverage').val($('#restaurant').attr('checked') ? 1 : 0);// IF $('#restautant') is checked, then we need to mark this item as food_and_beverage
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				submitting = false;
				if (response.success)
				{
					$.colorbox.close();
					$('#terminal_list').replaceWith($(response.terminal_table));
					add_edit_click();
					add_delete_click();
				}
				else
					$(form).unmask();
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
		},
		messages:
		{
		}
	});
});
</script>
