<?php $days_of_week = array(
	'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'
); ?>
<div class="specials_top">
	<div class="enabled_specials">	
		<?php echo form_label(lang('config_specials_enabled').' :', 'use',array('class'=>'wide specials_enabled')); ?>
	    <?php echo form_checkbox(array('name'=>'specials_enabled', 'value'=>$specials_enabled,'checked'=>$specials_enabled == '1', 'id'=>'specials_enabled')); ?>
    </div>
    <div class="terminal_button" id='add_daily_specials'><?=lang('config_add_daily_specials')?></div>	
</div>

<table class="special_table">
	<thead>
	<tr>
		<th class="leftmost"><div class="header_cell header"></div></th>
		<th><div class="header_cell header"></div></th>
		<th><div class="header_cell header">Active</div></th>
        <th><div class="header_cell header">Name</div></th>
		<th><div class="header_cell header">Booking&nbsp;Class</div></th>
        <th class="date"><div class="header_cell header">When</div></th>
		<th><div class="header_cell header special_time">Times</div></th>
		<th class="price"><div class="header_cell header">18 Holes</div></th>
		<th class="price"><div class="header_cell header">9 Holes</div></th>
		<th class="price"><div class="header_cell header">18 Cart</div></th>
		<th class="price"><div class="header_cell header">9 Cart</div></th>
	</tr>
	</thead>
	<tbody>
	<?php if(!empty($specials)){ ?>
	<?php foreach($specials as $special) { ?>
	<tr id='special_<?php echo $special['special_id'];?>'>
		<td><span id='delete_special_<?php echo $special['special_id'];?>' class='delete_specials'></span></td>
		<td><span id='edit_special_<?php echo $special['special_id'];?>' class='edit_specials'></span></td>
		<td>
			<?php if($special['active'] == 1){ ?>
			<span style="color: green">Active</span>
			<?php }else{ ?>
			<span style="color: red">Inactive</span>
			<?php } ?>
		</td>
        <td><?php echo $special['name'];?></td>
        <td><?php echo empty($special['booking_class']) ? 'All' : $special['booking_class'];?></td>
        <?php if($special['date'] == '0000-00-00'){ ?>
		<td><?php foreach($days_of_week as $day){
			if($special[$day] == 1){
			echo ucfirst($day).'<br>';
			}
		} ?></td>
		<?php }else{ ?>
		<td><?php echo $special['date'];?></td>
		<?php } ?>		
		<td class="special_time"><?php echo $special['special_times'];?></td>
		<td class="price"><?php echo $special['price_1'];?></td>
		<td class="price"><?php echo $special['price_2'];?></td>
		<td class="price"><?php echo $special['price_3'];?></td>
		<td class="price"><?php echo $special['price_4'];?></td>
	</tr>
	<?php } ?>
	<?php }else{ ?>
	<tr>
		<td colspan="10">No specials created</td>
	</tr>
	<?php } ?>
	</tbody>
</table>
<script type="text/javascript">
		var teesheet_id = <?php echo $teesheet_id;?>;
		$('.edit_specials').click(function(){
			var id = $(this).attr('id').replace('edit_special_','');	
				$.colorbox({
					href: "index.php/seasons/add_daily_specials/"+teesheet_id+"/"+id, 
					width: 830,
					height: 700,
					title: 'Create/Edit Daily Special'
				});
		});
		$('.delete_specials').click(function(){
			var id = $(this).attr('id').replace('delete_special_','');
			if (confirm('Are you sure you want to delete this specials? This action can not be undone.'))
			{
				$.ajax({
	               type: "POST",
	               url: "index.php/seasons/delete_specials/"+id,
	               data: "",
	               success: function(response){
	               		if(response == "success"){
	  						$('#special_'+id).hide();
	               		}
	               },
	               dataType:'json'
	            });
	        }
		});
		$('#specials_enabled').click(function(){
			var ischecked = $(this).is(":checked") ? 1:0; 
			{
				$.ajax({
	               type: "POST",
	               url: "index.php/seasons/specials_enabled/"+ischecked+"/"+teesheet_id,
	               data: "",
	               success: function(response){	
	               },
	               dataType:'json'
	            });
	        }
		});
		$('#add_daily_specials').click(function(){
			
			<?php if($is_aggregate){ ?>
			var url = "index.php/seasons/add_daily_specials/"+teesheet_id+"?aggregate=1";
			<?php }else{ ?>
			var url = "index.php/seasons/add_daily_specials/"+teesheet_id;
			<?php }?>

			$.colorbox({
				href: url, 
				width: 830,
				height: 700,
				title: 'Create/Edit Daily Special'
			});
		});	
</script>
