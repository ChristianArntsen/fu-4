<?php $this->load->view("partial/header_new"); ?>
<script type="text/javascript">
$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({'maxHeight':700, 'width':550});
    //init_table_sorting();
    //enable_select_all();
    //enable_checkboxes();
    //enable_row_selection();
    //enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
    //enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
    //enable_bulk_edit('<?php echo lang($controller_name."_none_selected")?>');
    //enable_cleanup('<?php echo lang("items_confirm_cleanup")?>');

	
});

function post_schedule_form_submit(response)
{
	console.log('inside post schedule------');
	console.dir(response);
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.teesheet_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.teesheet_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}
</script>
<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php echo 
					anchor("schedules/view/-1/width~1100",
					lang('schedules_new'),
					array('class'=>'colbox none new', 
						'title'=>lang('schedule_new')));
				?>
			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
			<div id='table_top'>
				<?php //echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
					<!--input type="text" name ='search' id='search' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form-->
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>