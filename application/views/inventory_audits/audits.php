<div class='audit_list_entry' data-audit-id=''>New Audit</div>
<?php
foreach($audits as $audit)
{
	echo "<div class='audit_list_entry' data-audit-id='{$audit['inventory_audit_id']}'>{$audit['date']} - {$audit['first_name']} {$audit['last_name']}</div>";
}
?>
<script>
	$(document).ready(function (){
		$('.audit_list_entry').click(function(){
         $('.update_count').hide();
			var audit_id = $(this).attr('data-audit-id');
           	$('#inventory_audit_items').mask("<?php echo lang('common_wait'); ?>");
			$.ajax({
               type: "POST",
               url: "index.php/items/get_audit_items/"+audit_id,
               data: "",
               success: function(response){
					$('#inventory_audit_items').html(response.items_html);
               		if (audit_id != '')
               		{
               			$('#submit_inventory_audit').hide();
               		}
               		else
               		{
               			$('#submit_inventory_audit').show();
               		}
	               	$('#inventory_audit_items').unmask();
               },
               dataType:'json'
            });
		});
	});
</script>