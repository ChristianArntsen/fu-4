
<style>
    <?= file_get_contents("css/dist/main.min.css") ?>
    @media print {
        /* styles go here */
    }
</style>


<div class="row static-header">
    <div class="col-sm-6"><img src="<?=$logo?>"/></div>
    <div class="col-sm-6 text-right"><?=$date?></div>
</div>
<div class="row">
    <div class="col-sm-6">
        <h4><?=$charge['name'] ?> Report </h4> <Br/>
        <b><?=$course_name?></b>
    </div>
    <div class="col-sm-6">
        <ul class="list-group-item">
            <li class="list-group-item">Start: <?=$charge_start ?></li>
            <li class="list-group-item">Closed: <?=$charge_end ?></li>
        </ul>


    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <h4>Customers</h4>
        <table>
            <thead>
            <tr >
                <th class="text-center">Customer Name</th>
                <th class="text-center">Total Spent</th>
                <th class="text-center">Total Left</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($customers as $customer): ?>
                <?php $total_charges += (integer)$customer['total']; ?>
                <?php
                    $customer_left = (integer)$charge['minimum_amount'] - (integer)$customer['total'];
                    if($customer_left < 0 ){
                        $customer_left = 0;
                    }
                    $total_left += $customer_left;

                ?>
                <tr>
                    <td ><?=$customer['customer_information']['first_name'] ?> <?=$customer['customer_information']['last_name'] ?></td>
                    <td class="text-right"><?=$customer['total'] ?></td>
                    <td class="text-right"><?=money_format('%n', $customer_left)?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
            <tfoot class="report-totals">
            <tr>
                <td>&nbsp;</td>
                <td class="text-right"><?=money_format('%n', $total_charges) ?></td>
                <td class="text-right"><?=money_format('%n', $total_left) ?></td>
            </tr>
            </tfoot>
        </table>
    </div>

</div>