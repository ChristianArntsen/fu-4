
<style>
    <?= file_get_contents("css/dist/main.min.css") ?>
    @media print {
        /* styles go here */
    }
</style>


<div class="row static-header">
    <div class="col-sm-6"><img src="<?=$logo?>"/></div>
    <div class="col-sm-6 text-right"><?=$date?></div>
</div>
<div class="row">
    <div class="col-sm-6">
        <h4>Payments and Sales</h4> <Br/>
        <b><?=$course_name?></b>
    </div>
    <div class="col-sm-6">
        <ul class="list-group-item">
            <li class="list-group-item">Start: <?=$start?> </li>
            <li class="list-group-item">End: <?=$end?> </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php $first = true; ?>
        <?php foreach($courses as $course): ?>
            <?= $course['name'] ?>
            <table class="table table-bordered">
                <?php if($first): ?>
                <?php $first = false; ?>
                <thead>
                <tr >
                    <th class="text-center">Grouping</th>
                    <th class="text-center">Course</th>
                    <th class="text-center">GL Code</th>
                    <th class="text-center">Debit</th>
                    <th class="text-center">Credit</th>
                </tr>
                </thead>

                <?php endif; ?>
                <tbody>
		        <?php foreach($course['table'] as $row):?>
                    <tr>
                        <td><?=$row[0] ?></td>
                        <td><?=$row[1] ?></td>
                        <td><?=$row[2] ?></td>
                        <td><?=$row[3] ?></td>
                        <td><?=$row[4] ?></td>
                    </tr>
		        <?php endforeach; ?>
                </tbody>
                <!--            <tfoot class="report-totals">-->
                <!--            <tr>-->
                <!--                <td >&nbsp;</td>-->
                <!--                <td class="text-right"  >--><?//= money_format('%n', $totals['new']) ?><!--</td>-->
                <!--                <td class="text-right" >--><?//=  money_format('%n', $totals['refunds']) ?><!--</td>-->
                <!--                <td class="text-right"  >--><?//= money_format('%n', $totals['voids']) ?><!--</td>-->
                <!--                <td class="text-right">--><?//=   money_format('%n', $totals['total']) ?><!--</td>-->
                <!--                <td class="text-right">--><?//=   money_format('%n', $totals['collected']) ?><!--</td>-->
                <!--                <td class="text-right">--><?//=   money_format('%n', $totals['difference']) ?><!--</td>-->
                <!--            </tr>-->
                <!--            </tfoot>-->
            </table>
            
            
        <?php endforeach; ?>
       
    </div>

</div>
<div class="spacer" style="height:25px;"></div>

