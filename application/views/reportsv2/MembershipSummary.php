
<style>
    <?= file_get_contents("css/dist/main.min.css") ?>
    @media print {
        /* styles go here */
    }
</style>


<div class="row static-header">
    <div class="col-sm-6"><img src="<?=$logo?>"/></div>
    <div class="col-sm-6 text-right"><?=$date?></div>
</div>
<div class="row">
    <div class="col-sm-6">
        <h4>Membership Summary Report </h4> <Br/>
        <b><?=$course_name?></b>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table>
            <thead>
            <tr >
                <th class="text-center">Customer Name</th>
                <th class="text-center">Groups</th>
                <th class="text-center">Payment Frequency</th>
                <th class="text-center">Total Invoiced</th>
                <th class="text-center">Total Paid</th>
                <th class="text-center">Spend in Proshop</th>
                <th class="text-center">Spend in F&B</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($customers as $customer): ?>
                <tr>
                    <td ><?=$customer['first_name'] ?> <?=$customer['last_name'] ?></td>
                    <td><?=$customer['label'] ?></td>
                    <td><?=$customer['groups'] ?></td>
                    <td class="text-right"><?=$customer['total_invoiced'] ?></td>
                    <td class="text-right"><?=$customer['total_paid'] ?></td>
                    <td class="text-right"><?=$customer['proshopspend'] ?></td>
                    <td class="text-right"><?=$customer['fbspend'] ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>

</div>