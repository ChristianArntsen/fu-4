<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
<head style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
	<!-- If you delete this meta tag, Half Life 3 will never be released. -->
	<meta name="viewport" content="width=device-width" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
	<title style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">ForeUp Import Complete</title>
</head>
 
<body bgcolor="#FFFFFF" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;width: 100%!important;">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#999999" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
	<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
		<td class="header container" style="margin: 0 auto!important;padding: 0;font-family: Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">
				<div class="content" style="margin: 0 auto;padding: 15px;font-family: Helvetica, Arial, sans-serif;max-width: 600px;display: block;">
				<table bgcolor="#999999" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
					<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
						<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"><h1 style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;line-height: 1.1;margin-bottom: 15px;color: #fff;font-weight: 500;font-size: 30px;">ForeUp Import Complete</h1></td>
						<td align="right" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
					</tr>
				</table>
				</div>	
		</td>
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
	</tr>
</table><!-- /HEADER -->

<!-- BODY -->
<table class="body-wrap" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
	<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
		<td class="container" bgcolor="#FFFFFF" style="margin: 0 auto!important;padding: 0;font-family: Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">
			<div class="content" style="margin: 0 auto;padding: 15px;font-family: Helvetica, Arial, sans-serif;max-width: 600px;display: block;">
				<h3 style="text-align: left;margin: 0;padding: 25px 0px 0px 0px;font-family: Helvetica, Arial, sans-serif;line-height: 1.1;margin-bottom: 15px;color: #000;font-weight: 500;font-size: 27px;">Results</h3>
				<table style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
					<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
						<td style="width: 33%; text-align: center; margin: 0; padding: 10px;font-family: Helvetica, Arial, sans-serif; background: #4da5d1; color: white;">
							<h2 style="margin: 10px 0px 10px 0px; font-family: Helvetica, Arial, sans-serif;line-height: 1.1; color: white; font-size: 24px"><?php echo number_format($records_imported); ?></h2>
							<h5 style="text-transform: uppercase; margin: 5px 0px 5px 0px; font-family: Helvetica, Arial, sans-serif;line-height: 1.1; color: white; font-size: 14px; font-weight: normal">Records Imported</h5>
						</td>
						<td style="width: 33%; text-align: center; margin: 0; padding: 10px;font-family: Helvetica, Arial, sans-serif;background: <?php if($records_failed > 0){ ?>#ce6161<?php }else{ ?>#4da5d1<?php } ?>; color: white;">
							<h2 style="margin: 10px 0px 10px 0px; font-family: Helvetica, Arial, sans-serif;line-height: 1.1; color: white; font-size: 24px">
								<?php echo number_format($records_failed); ?>
							</h2>
							<h5 style="text-transform: uppercase; margin: 5px 0px 5px 0px; font-family: Helvetica, Arial, sans-serif;line-height: 1.1; color: white; font-size: 14px; font-weight: normal">Records Failed</h5>
						</td>
						<td style="width: 33%; text-align: center; margin: 0;padding: 10px;font-family: Helvetica, Arial, sans-serif;background: #4da5d1; color: white;">
							<h2 style="margin: 10px 0px 10px 0px; font-family: Helvetica, Arial, sans-serif;line-height: 1.1; color: white; font-size: 24px">
								<?php
								$minutes = floor($total_duration / 60);
								$seconds = $total_duration % 60;
								echo sprintf('%2d:%2d', $minutes, $seconds) ; ?>
							</h2>
							<h5 style="text-transform: uppercase; margin: 5px 0px 5px 0px; font-family: Helvetica, Arial, sans-serif;line-height: 1.1; color: white; font-size: 14px; font-weight: normal">Time Elapsed</h5>
						</td>
					</tr>
				</table>

				<?php if(!empty($response['failed_csv_url'])){ ?>
					<a style="padding: 5px 0;font-family: Helvetica, Arial, sans-serif;" href="<?php echo $response['failed_csv_url']; ?>">Download Failed Records</a>
				<?php } ?>

				<?php if(!empty($response['errors'])){ ?>
					<h4 style="font-family: Helvetica, Arial, sans-serif; color: #c96e6c;">Errors</h4>
					<table style="margin: 25px 0px 25px 0px;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
						<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
							<td style="width: 15%; margin: 0;padding: 5px 0;font-family: Helvetica, Arial, sans-serif; border-bottom: 2px solid #E0E0E0;">
								Row
							</td>
							<td style="width: 85%; margin: 0;padding: 5px 0;font-family: Helvetica, Arial, sans-serif; border-bottom: 2px solid #E0E0E0;">
								Error
							</td>
						</tr>
						<?php foreach($response['errors'] as $row_number => $message){ ?>
						<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
							<td style="margin: 0;padding: 5px 0px;font-family: Helvetica, Arial, sans-serif;">
								<?php echo $row_number; ?>
							</td>
							<td style="margin: 0;padding: 5px 0px;font-family: Helvetica, Arial, sans-serif;">
								<?php echo $message; ?>
							</td>
						</tr>
						<?php } ?>
					</table>
				<?php } ?>

			</div><!-- /content -->
		</td>
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;clear: both!important;">
	<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
		<td class="container" style="margin: 0 auto!important;padding: 0;font-family: Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">
			
				<!-- content -->
				<div class="content" style="margin: 0 auto;padding: 15px;font-family: Helvetica, Arial, sans-serif;max-width: 600px;display: block;">
				<table style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
				<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
					<td align="center" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
						<p style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;">
							<!-- <a href="#" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;color: #2BA6CB;">Terms</a> |
							<a href="#" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;color: #2BA6CB;">Privacy</a> |
							<a href="#" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;color: #2BA6CB;"><unsubscribe style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">Unsubscribe</unsubscribe></a> -->
						</p>
					</td>
				</tr>
			</table>
				</div><!-- /content -->
				
		</td>
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
	</tr>
</table><!-- /FOOTER -->

</body>
</html>