<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
<head style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
	<!-- If you delete this meta tag, Half Life 3 will never be released. -->
	<meta name="viewport" content="width=device-width" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
	<title style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">ZURBemails</title>
</head>
 
<body bgcolor="#FFFFFF" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;width: 100%!important;">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#999999" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
	<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
		<td class="header container" style="margin: 0 auto!important;padding: 0;font-family: Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">
				<div class="content" style="margin: 0 auto;padding: 15px;font-family: Helvetica, Arial, sans-serif;max-width: 600px;display: block;">
				<table bgcolor="#999999" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
					<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
						<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"><h4 style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;line-height: 1.1;margin-bottom: 15px;color: #fff;font-weight: 500;font-size: 20px;"><?php echo $organization['name']; ?></h4></td>
						<td align="right" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
					</tr>
				</table>
				</div>	
		</td>
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
	</tr>
</table><!-- /HEADER -->

<!-- BODY -->
<table class="body-wrap" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
	<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
		<td class="container" bgcolor="#FFFFFF" style="margin: 0 auto!important;padding: 0;font-family: Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">

			<div class="content" style="margin: 0 auto;padding: 15px;font-family: Helvetica, Arial, sans-serif;max-width: 600px;display: block;">
			<table style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
				<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
					<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
						
						<h3 style="text-align: center;margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;line-height: 1.1;margin-bottom: 15px;color: #000;font-weight: 500;font-size: 27px;">A Gift for You</h3>
						<?php if(!empty($to)){ ?>
						<p class="lead" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 17px;line-height: 1.6;"><?php echo $from['first_name'].' '.$from['last_name']; ?> has sent you a gift card.</p>
						<p style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;"><?php echo $to['message']; ?></p>
						<?php }else{ ?>
						<p style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;">
							<strong>Sale Number:</strong> <?php echo $sale['number']; ?><br>
							<strong>Total:</strong> <?php echo to_currency($sale['total']); ?><br>
							<strong>Payment Method:</strong> <?php echo $sale['payment_method']; ?><br>
						</p>
						<?php } ?>

						<!-- Callout Panel -->
						<h3 class="callout" style="border: 1px solid #E0E0E0; margin: 0;padding: 15px;font-family: Helvetica, Arial, sans-serif;margin-bottom: 0px;font-weight: bold;font-size: 27px;line-height: 1.6;background-color: #ECF8FF; text-align: center;"><?php echo to_currency($gift_card['value']); ?>
						</h3><!-- /Callout Panel -->					
												
						<!-- social & contact -->
						<table class="social" width="100%" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;background-color: #fff;width: 100%;border: 1px solid #E0E0E0">
							<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
								<td style="margin: 0;padding: 15px;font-family: Helvetica, Arial, sans-serif; text-align: center">
									<img src="<?php echo site_url()."/barcode?scale=2&font_size=14&text=".$gift_card['giftcard_number']."&barcode=".$gift_card['giftcard_number']; ?>">
								</td>
							</tr>
						</table><!-- /social & contact -->
						
						<small style="padding-top: 15px;color: #444;font-family: Helvetica, Arial, sans-serif;font-weight: normal;font-size: 12px;line-height: 1.6;">To redeem, print this email or show it on your mobile device.</small>
					</td>
				</tr>
			</table>
			</div><!-- /content -->
									
		</td>
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;clear: both!important;">
	<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
		<td class="container" style="margin: 0 auto!important;padding: 0;font-family: Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">
			
				<!-- content -->
				<div class="content" style="margin: 0 auto;padding: 15px;font-family: Helvetica, Arial, sans-serif;max-width: 600px;display: block;">
				<table style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;width: 100%;">
				<tr style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
					<td align="center" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">
						<p style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;">
							<!-- <a href="#" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;color: #2BA6CB;">Terms</a> |
							<a href="#" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;color: #2BA6CB;">Privacy</a> |
							<a href="#" style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;color: #2BA6CB;"><unsubscribe style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;">Unsubscribe</unsubscribe></a> -->
						</p>
					</td>
				</tr>
			</table>
				</div><!-- /content -->
				
		</td>
		<td style="margin: 0;padding: 0;font-family: Helvetica, Arial, sans-serif;"></td>
	</tr>
</table><!-- /FOOTER -->

</body>
</html>