<?php ?>
<div id="aprivaPayBox">
    <div id="aprivaTotalBox">
        Total: $<?=$payment_data['trans_amount']?>
    </div>
    <div id="divIframe">
        <iframe id="iframe_HMP" name="iframe_HMP" src="about:blank" width="250" height="500"></iframe>
    </div>
</div>
<style>
    #aprivaTotalBox {
        font-size: 30px;
        font-weight: bold;
        text-align: center;
        padding: 15px;
    }
    #aprivaPayBox {
        text-align:center;
    }
    #iframe_HMP {
        border:none;
    }
</style>
<script>
    var apriva_transaction = {
        fullUrl: '',
        payment_purpose: '<?=$payment_purpose?>',
        loadIframe : function() {
            var fullUrl = "<?=$url?>".replace(/&amp;/g, '&');
            apriva_transaction.fullUrl = fullUrl;
            $("#iframe_HMP").attr('src', fullUrl);
            $.loadMask.hide();
        },
        processResponse : function (e) {
            var objData = $.parseJSON(e.data);

            apriva_transaction.recordResults(objData);
            apriva_transaction.handlePayment(objData);
        },
        packageTransactionData : function (type, data)
        {
            var InvoiceNo = '<?=$payment_data['ref_no']?>';
            var TranType = '<?=$payment_data['transaction_type']?>';

            if (type == 'API') {
                return {
                    'transaction_type' : TranType,
                    'invoice' : InvoiceNo,
                    'response' : data,
                    'request' : apriva_transaction.fullUrl,
                    'terminal_id' : '<?=$terminal_id?>'
                };
            }

            if (type == '') {
                if (data.Result.ResponseCode != 0) {
                    return {
                        'invoice_id': InvoiceNo,
                        'status': data.Result.ResponseCode == 0 ? 'Approved' : 'Declined',
                        'display_message': data.Result.ResponseText
                    }
                }
                else {
                    //var CardType = pax_transaction.getCardType(data.account_information.card_type);
                    return {
                        'tran_type': TranType,
                        //'card_type': CardType,
                        'invoice_id': InvoiceNo,
                        'payment_id': data.TransactionResultData.HostTransactionID,
                        'status': data.Result.ResponseCode == 0 ? 'Approved' : 'Declined',
                        'display_message': data.Result.ResponseText,
                        'acq_ref_data': data.TransactionResultData.RRN,
                        //'process_data': data.host_information.trace_number,
                        //'message' : data.response_message,
                        'amount': data.AmountsResp.RequestedTotalAmount,// / 100).toFixed(2),
                        'auth_amount': data.AmountsResp.ApprovedAmount,// / 100).toFixed(2),
                        //'params' : {},//params don't forget Fee
                        //'description' : CardType + ' ' + data.account_information.account,
                        //'verified' : (data.response_code == '000000') ? 1 : 0,
                        'auth_code': data.TransactionResultData.AuthCode,
                        //'cardholder_name': data.account_information.card_holder,
                        //'customer_note' : <?php echo !empty($customer_note) ? json_encode($customer_note) : 'null'; ?>,
                        //'masked_account': data.account_information.account
                    };
                }
            }
            else if (type == 'sale') {
                //var CardType = pax_transaction.getCardType(data.account_information.card_type);
                return {
                    'tran_type': TranType,
                    'type' : 'credit_card',
                    'record_id' : InvoiceNo,
                    'action' : TranType,
                    'merchant' : 'apriva',
                    'message' : data.Result.ResponseText,
                    'approved' : (data.Result.ResponseCode == 0) ? true : false,
                    'payment_amount' : data.AmountsResp.ApprovedAmount,
                    'auth_amount': data.AmountsResp.ApprovedAmount,
                    'amount': data.AmountsResp.ApprovedAmount,
                    'auth_code': data.TransactionResultData.AuthCode,
                    //'card_type': CardType,
                    'cardholder_name': '',
                    'card_number': '',
                    'description' : 'Credit Card'


                };
            }
            else if (type == 'online') {
                return {
                    'tran_type': TranType,
                    'type' : 'credit_card',
                    'record_id' : InvoiceNo,
                    'action' : TranType,
                    'merchant' : 'apriva',
                    'message' : data.Result.ResponseText,
                    'approved' : (data.Result.ResponseCode == 0) ? true : false,
                    'payment_amount' : data.AmountsResp.ApprovedAmount,
                    'auth_amount': data.AmountsResp.ApprovedAmount,
                    'amount': data.AmountsResp.ApprovedAmount,
                    'auth_code': data.TransactionResultData.AuthCode,
                    //'card_type': CardType,
                    'cardholder_name': '',
                    'card_number': '',
                    'description' : 'Credit Card'


                };
            }

        },
        recordResults : function (data) {
            var final_data = {
                'api_data': apriva_transaction.packageTransactionData('API', data),
                'transaction_data': apriva_transaction.packageTransactionData('', data)
            };

            $.ajax({
                type: "POST",
                url: SITE_URL + "record_apriva_transaction/",
                data: final_data,
                success: function (response) {
                    if (!response.success) {
                        console.log('failed saving pax transaction');
                    }
                    else {
                        console.log('success saving pax transaction');
                    }
                },
                dataType: 'json'
            });
        },
        handlePayment : function (data){
            if (data.Result.ResponseCode  != 0) {
                // Declined or cancelled
                var message = 'Payment Declined, please try again.';
                App.vent.trigger('notification', {'msg': message, 'type':'error'});
                // Close modal
                $('.modal').modal('hide');
            } else {
                // Approved
                // If in sales screen or F&B
                if (apriva_transaction.payment_purpose == 'sale') {
                    apriva_transaction.savePayment(data);
                }
                // Else if in online booking
                else if (apriva_transaction.payment_purpose == 'online') {
                    apriva_transaction.onlinePayment(data);
                }
            }
        },
        savePayment : function (data){
            var payment = apriva_transaction.packageTransactionData('sale', data);
            App.vent.trigger('credit-card', payment);
        },
        onlinePayment : function (data){
            var payment = apriva_transaction.packageTransactionData('online', data);
            //App.vent.trigger('credit-card', payment);
            $.post(SITE_URL + 'process_credit_card', payment, function(response){
                eval(response);
            }, 'html');
        },
        onlineAddCard : function (data) {
            // Add new credit card to user's profile
//            var params = {};
//            params.number = etsResponse.customers.cardNumber;
//            params.expiration = etsResponse.customers.cardExpiration;
//            params.type = etsResponse.customers.cardType;
//            params.ets_id = etsResponse.id;
//            params.ets_session_id = //";
//            params.ets_customer_id = etsResponse.customers.id;

//            App.data.user.get('credit_cards').create(params, {wait: true});
        }

    };
    function ProcessResponse(e) {
        $('#divIframe').hide();
        apriva_transaction.processResponse(e);
        window.removeEventListener('message', ProcessResponse);
    }
    $(document).ready(function(){
        apriva_transaction.loadIframe();
        window.addEventListener('message', ProcessResponse);
    });
</script>
<style>
    .apriva_error_box {
        margin:50px auto;
        font-size:30px;
        font-weight:bold;
        text-align:center;
    }
</style>