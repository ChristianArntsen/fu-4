<script src="<?php echo base_url('js/apriva.pax.js'); ?>"></script>
<script>
    pax_transaction.InvoiceNo = '<?=$payment_data['ref_no']?>';
    pax_transaction.TranType = '<?=$payment_data['transaction_type']?>';
    pax_transaction.CommandArray = '<?=$command_array?>';
    pax_transaction.CheckCommandArray = '<?=$check_command_array?>';
    pax_transaction.VoidCommandArray = '<?=$void_command_array?>';
    pax_transaction.CertURL = '<?=$certURL?>';
    pax_transaction.DevicePort = '<?=$devicePort?>';
    pax_transaction.TerminalID = '<?=$terminal_id?>';
</script>
</head>
<body>
    <script>
        pax_transaction.sendCreditSale();
    </script>
<div id="transaction_details">
    <?=ucfirst($payment_data['transaction_type'])?> <?='$'.number_format(str_replace(',','',$payment_data['trans_amount']), 2)?>
</div>
<div id="transaction_instructions">
    Please complete or cancel the transaction at the card terminal.
</div>
<style>
    #transaction_details, #transaction_instructions {
        font-size:24px;
        text-align:center;
        margin-top:20px;
    }
    #transaction_details {
        font-size:40px;
        font-weight: bold;
    }

</style>
</body>
</html>
