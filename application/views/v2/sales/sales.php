<?php $this->load->view('v2/header'); ?>
<script>
function find_jzebra_printer() {
	var applet = document.getElementById('qz');

	if(applet != null){
		if(!applet.isDoneFinding()){
			window.setTimeout('find_jzebra_printer()', 100);
		}else{
			var printer = applet.getPrinterName();
			console.error(printer == null ? "Printer not found" : "Printer \"" + printer + "\" found");
		}
	}else{
		console.log("Applet not loaded!");
	}
}
TERMINAL_ID = <?php echo (int) $this->session->userdata('terminal_id'); ?>;
COURSE_ID = <?php echo (int) $this->session->userdata('course_id'); ?>;
ENVIRONMENT = '<?php echo ENVIRONMENT; ?>';
VIRTUAL_NUMBER = '<?=$virtual_number ?>';
<?php 
$user_info->sales_stats = $this->session->userdata('sales_stats');
$user = elements(array(
	'person_id', 
	'user_level',
	'first_name', 
	'last_name', 
	'fnb_logged_in',
	'sales_stats',
	'permissions'
), (array) $user_info);

$user['skipped_register_log'] = (bool) $this->session->userdata('skipped_register_log');
$user['modules'] = $allowed_modules->result_array();
$user['fnb_logged_in'] = (bool) $this->session->userdata('fnb_logged_in');

$course_settings = elements(array(
	'allow_employee_register_log_bypass',
	'auto_email_receipt',
	'auto_email_no_print',
	'course_id',
	'name',
	'no_show_policy',
	'return_policy',
	'phone',
	'postal',
	'zip',
	'address',
	'after_sale_load',
	'area_code',
	'auto_gratuity',
	'auto_gratuity_threshold',
	'blind_close',
	'booking_rules',
	'cash_drawer_on_cash',
	'cash_drawer_on_sale',
	'cash_register',
	'city',
	'close_time',
	'country',
	'county',
	'credit_category',
	'credit_category_name',
	'credit_subtotal_only',
	'credit_department', 
	'credit_department_name',
	'currency_format',
	'currency_symbol',
	'currency_symbol_placement',
	'customer_credit_nickname',
	'customer_field_settings',
	'credit_card_fee',
	'date_format',
	'default_register_log_open',
	'default_taxes',
	'email',
	'fax',
	'food_bev_sort_by_seat',
	'food_bev_tip_line_first_receipt',
	'hide_back_nine',
	'hide_modifier_names_kitchen_receipts',
	'hide_taxable',
	'language',
	'limit_fee_dropdown_by_customer',
	'minimum_food_spend',
	'print_after_sale',
	'print_credit_card_receipt',
	'print_sales_receipt',
	'print_tee_time_details',
	'print_tip_line',
	'print_two_receipts',
	'print_two_receipts_other',
	'print_two_signature_slips',
	'print_suggested_tip',
	'require_customer_on_sale',
	'require_employee_pin',
	'require_signature_member_payments',
	'sales_v2',
	'service_fee_active',
	'tee_sheet_speed_up',
	'state',
	'timezone',
	'tip_line',
	'track_cash',
	'unit_price_includes_tax',
	'use_ets_giftcards',
	'use_loyalty',
	'loyalty_auto_enroll',
	'use_terminals',
	'webprnt',
	'webprnt_ip',
	'webprnt_label_ip',
	'website',
	'multiple_printers',
	'member_balance_nickname',
	'receipt_print_account_balance',
	'print_minimums_on_receipt',
	'require_guest_count',
	'use_course_firing',
	'course_firing_include_items',
	'hide_employee_last_name_receipt',
	'use_kitchen_buzzers',
	'use_new_permissions',
	'teesheet_caching_mode',
    'nonfiltered_giftcard_barcodes',
    'enable_captcha_online',
    'two_way_text'
), $course);

if(!empty($course['apriva_username'])){
    $course_settings['merchant'] = 'apriva';
}else if(!empty($course['ets_key'])){
	$course_settings['merchant'] = 'ets';
}else if(!empty($course['mercury_id']) && !empty($course['mercury_password'])){
	$course_settings['merchant'] = 'mercury';
}else if(!empty($course['element_account_id'])){
	$course_settings['merchant'] = 'element';
}else{
	$course_settings['merchant'] = false;
}

$course_settings['unfinished_register_log'] = (int) $this->session->userdata('unfinished_register_log');
$course_settings['skipped_register_log'] = (int) $this->session->userdata('skipped_register_log');
$course_settings['special_items'] = $special_items;
$course_settings['custom_payments'] = $custom_payments;
?>

SETTINGS = <?php echo json_encode($course_settings); ?>;
USER = <?php echo json_encode($user); ?>;

SETTINGS.price_classes = <?php echo json_encode($price_classes); ?>;
SETTINGS.price_class_objects = <?php echo json_encode($price_class_array); ?>;
SETTINGS.groups = <?php echo json_encode($customer_groups); ?>;
SETTINGS.fees = <?php echo json_encode($fees); ?>;
SETTINGS.teesheets = <?php echo json_encode($teesheets); ?>;
SETTINGS.pos_stats = <?php echo json_encode($pos_stats); ?>;

if(!SETTINGS.member_balance_nickname || SETTINGS.member_balance_nickname == ''){
	SETTINGS.member_balance_nickname = 'Member Balance';
}
if(!SETTINGS.customer_credit_nickname || SETTINGS.customer_credit_nickname == ''){
	SETTINGS.customer_credit_nickname = 'Customer Credit';
}

CART = <?php echo json_encode($cart); ?>;
RECENT_TRANSACTIONS = <?php echo json_encode($sales); ?>;
SUSPENDED_SALES = <?php echo json_encode($suspended_sales); ?>;
INCOMPLETE_SALES = <?php echo json_encode($incomplete_sales); ?>;
QUICK_BUTTONS = <?php echo json_encode($quick_buttons); ?>;
FOOD_BEV_ITEMS = <?php echo json_encode($food_bev_items); ?>;
FOOD_BEV_SIDES = <?php echo json_encode($food_bev_sides); ?>;
REGISTER_LOGS = <?php echo json_encode($register_logs); ?>;
PASSES = <?php echo json_encode($passes); ?>;
SUPPLIERS = <?php echo json_encode($suppliers); ?>;
SEASONS = <?php echo json_encode($seasons); ?>;
MINIMUM_CHARGES = <?php echo json_encode($minimum_charges); ?>;
DATA_IMPORT_JOBS = <?php echo json_encode($data_import_jobs); ?>;
ITEM_RECEIPT_CONTENT = <?php echo json_encode($item_receipt_content); ?>;

SETTINGS.employees = <?php echo json_encode($employees); ?>;
SETTINGS.modules = <?php echo json_encode($modules); ?>;
SETTINGS.terminals = <?php echo json_encode($terminals); ?>;
//SETTINGS.receipt_printers = new ReceiptPrinterCollection();
//SETTINGS.receipt_printers.fetch({async:false});
SETTINGS.meal_courses = <?php echo json_encode($meal_courses); ?>;
SETTINGS.nonfiltered_giftcard_barcodes = SETTINGS.nonfiltered_giftcard_barcodes == "1"
SETTINGS.enable_captcha_online = SETTINGS.enable_captcha_online == "1"

</script>
<div id="page-iframe-container"></div>
<div id="page" class="container-fluid"></div>

<?php if ($this->config->item('print_after_sale') && !$this->config->item('webprnt')) { ?>
<div style='height:1px; width:1px; overflow: hidden'>
	<applet id='qz' name="qz" code="qz.PrintApplet.class" archive="<?php echo base_url()?>/qz-print.jar" width="100" height="100">
		<param name="printer" value="foreup">
	</applet>
</div>
<?php } ?>

<?php $this->load->view('v2/footer'); ?>
