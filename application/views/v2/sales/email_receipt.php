<?php 
$this->load->model('Giftcard'); 
$show_member_balance = false;
$show_customer_balance = false;
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $receipt_title; ?></title>
</head>
<body>
<div id="receipt_wrapper">
	<div id="receipt_header" style="margin-bottom: 10px;">
		<div id="company_name"><?php echo $this->config->item('name'); ?></div>
		<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
		<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
		<div id="sale_time"><?php echo date('m/d/Y g:ia', strtotime($sale_time)); ?></div>
	</div>
	<div id="receipt_general_info" style="margin-bottom: 10px;">
		<?php if(!empty($customer)){ ?>
		<div id="customer"><strong><?php echo lang('customers_customer'); ?>:</strong> <?php echo $customer['last_name'].', '.$customer['first_name']; ?></div>
		<?php } ?>
		<div id="sale_id"><strong><?php echo lang('sales_id'); ?>:</strong> <?php echo $number; ?></div>
		<div id="employee"><strong><?php echo lang('employees_employee'); ?>:</strong> <?php echo $employee['last_name'].', '.$employee['first_name']; ?></div>
	</div>
    <?php if(!empty($teetime) && $this->config->item('print_tee_time_details') == 1) { ?>
    <div id="tee_time_general_info" style="margin-bottom: 10px;">
        <div id="tee_sheet_title"><?php echo 'Reservation for'; ?> <?php echo $teetime['teesheet_title']; ?></div>
        <div id="tee_sheet_info"><?php echo 'at'; ?> <?php echo date('m/d/Y g:ia', strtotime($teetime['start_date'])) ?></div>
    </div>
    <?php } ?>
	<table id="receipt_items" style="width: 100%;">
		<tr>
			<th style="width:33%; text-align: left; background-color: #F0F0F0; border-bottom: 1px solid #E0E0E0;"><?php echo lang('items_item'); ?></th>
			<th style="width:20%; text-align: left; background-color: #F0F0F0; border-bottom: 1px solid #E0E0E0;"><?php echo lang('common_price'); ?></th>
			<th style="width:16%; text-align:center; background-color: #F0F0F0; border-bottom: 1px solid #E0E0E0;"><?php echo lang('sales_quantity'); ?></th>
			<th style="width:15%; text-align:center; background-color: #F0F0F0; border-bottom: 1px solid #E0E0E0;"><?php echo lang('sales_discount'); ?></th>
			<th style="width:16%; text-align:right; background-color: #F0F0F0; border-bottom: 1px solid #E0E0E0;"><?php echo lang('sales_total'); ?></th>
		</tr>
		<?php foreach($items as $line => $item){ 
			if($item['item_type'] == 'member_account'){
				$show_member_balance = true;
			}
			if($item['item_type'] == 'customer_account'){
				$show_customer_balance = true;
			}		
		?>
		<tr>
			<td><span class='long_name'><?php echo $item['name']; ?></span></td>
			<td><?php echo to_currency($item['unit_price']); ?></td>
			<td style='text-align:center;'><?php echo $item['quantity_purchased']; ?></td>
			<td style='text-align:center;'><?php echo $item['discount_percent']; ?>%</td>
			<td style='text-align:right;'><?php echo to_currency($item['subtotal']); ?></td>
		</tr>
	    <tr>
			<td colspan="2" align="center"><?php echo '&nbsp;'; ?></td>
			<td colspan="2" ><?php echo isset($item['serialnumber']) ? $item['serialnumber'] : ''; ?></td>
			<td colspan="2"><?php echo '&nbsp;'; ?></td>
	    </tr>

	<?php
		foreach($item['modifiers'] as $modifier){
		?>
			<tr>
				<td> - <span class='long_name'><?php echo $modifier['name']; ?> : <?php echo is_array($modifier['selected_option'])?implode(', ',$modifier['selected_option']):$modifier['selected_option']; ?></span></td>
				<td><?php echo to_currency($modifier['selected_price']/$item['number_splits']); ?></td>
				<td style='text-align:center;'>&nbsp;</td>
				<td style='text-align:center;'>&nbsp;</td>
				<td style='text-align:right;'>&nbsp;</td>
			</tr>
		<?php
		}
		} ?>
	<tr>
		<td colspan="4" style='text-align:right;border-top:2px solid #000000;'><?php echo lang('sales_sub_total'); ?></td>
		<td colspan="2" style='text-align:right;border-top:2px solid #000000;'><?php echo to_currency($subtotal); ?></td>
	</tr>
	<?php foreach($taxes as $tax) {
		if(!$tax['amount'])continue;
		?>
		<tr>
			<td colspan="4" style='text-align:right;'><?php echo $tax['percent']; ?>% <?php echo $tax['name']; ?>:</td>
			<td colspan="2" style='text-align:right;'><?php echo to_currency($tax['amount']); ?></td>
		</tr>
	<?php }; ?>
	
	<tr>
		<td colspan="4" style='text-align:right;'><strong><?php echo lang('sales_total'); ?></strong></td>
		<td colspan="2" style='text-align:right'><strong><?php echo to_currency($total); ?></strong></td>
	</tr>
    <tr>
		<td colspan="6">&nbsp;</td>
	</tr>
	
	<tr>
		<td colspan="3">&nbsp;</td>
		<td colspan="3" style="border-bottom: 1px solid gray;">
			<strong>Payments</strong>
		</td>
	</tr>
	<?php
	$paymentTotal = 0;
    foreach($payments as $payment_id => $payment){

	    $paymentTotal +=  $payment['amount'];
		if($payment['type'] == 'member_account'){
			$show_member_balance = true;
		}
		if($payment['type'] == 'customer_account'){
			$show_customer_balance = true;
		}
	?>
        <?php
            if($payment['type'] == "gift_card_tip" || $payment['type'] == "gift_card"):
        ?>
                <tr>
                    <td colspan="4" style="text-align: right;"><?=$payment['description'] ?> </td>
                    <td colspan="2" style="text-align: right"><?php echo to_currency( $payment['amount']); ?>  </td>
                </tr>
        <?php else: ?>
            <tr>
                <td colspan="4" style="text-align: right;"><?php $splitpayment = explode(':',$payment['description']); echo $splitpayment[0]; ?> </td>
                <td colspan="2" style="text-align: right"><?php echo to_currency( $payment['amount']); ?>  </td>
            </tr>
	    <?php endif; ?>


	<?php } ?>
    <tr>
		<td colspan="6">&nbsp;</td>
	</tr>
	<tr>
        <td colspan="4" style="text-align: right;"><strong>Total Payments</strong></td>
        <td colspan="2" style="text-align: right"><strong><?=to_currency($paymentTotal )?></strong>  </td>
    </tr>
	<?php
    foreach($payments as $payment) {
        ?>
		<?php if($payment['type'] == 'gift_card' && !preg_match("/Auto Gratuity/",$payment['description'])){?>
		<tr>
			<td colspan="2" style="text-align:right;"><?php echo lang('sales_giftcard_balance'); ?></td>
			<td colspan="2" style="text-align:right;"><?php echo $payment['description'];?> </td>
			<td colspan="2" style="text-align:right"><?php echo to_currency($this->Giftcard->get_giftcard_value($payment['number'])); ?></td>
		</tr>
		<?php } ?>
	<?php } ?>
	</table>

	<?php if(isset($customer) && ($show_member_balance || $show_customer_balance)){ ?>
	<div style="margin-bottom: 25px">
		<?php if($show_member_balance){ ?>
		<?php echo $this->config->item('member_balance_nickname'); ?> ending balance: <?php echo to_currency($customer['member_account_balance']); ?>
		<?php } ?>
		<?php if($show_customer_balance){ ?>
		<?php echo $this->config->item('customer_credit_nickname'); ?> ending balance: <?php echo to_currency($customer['account_balance']); ?>
		<?php } ?>
	</div>
	<?php } ?>

	<?php if(!empty($customer_note)){ ?>
	<div id="customer_note" style="margin-bottom: 25px">
		<strong>Customer Note</strong><br>
		<?php echo $customer_note; ?>
	</div>
	<?php } ?>

	<?php $return_policy = nl2br($this->config->item('return_policy'));
	if(!empty($return_policy)){ ?>
	<div id="sale_return_policy">
	<?php echo htmlentities($return_policy); ?>
	</div>
	<?php } ?>
</div>
<?php if(isset($print)){ ?>
    <script type="text/javascript">
        window.print();
    </script>
<?php } ?>
</body>
</html>