<html>
<head>
    <script>
        function listener(event){
            //result processing logic goes here
            var jsonResponse = JSON.parse(event.data);
            jsonResponse.returnJSON = true;


            var url = 'home/blackline_payment';
            var App = window.parent.App;

            <?php if (strpos(current_url(),'v2') === false) {
            ?>
                if (window.parent.document.getElementById('tee_sheet_iframe') != null) {
                    var tee_time_id = window.parent.document.getElementById('tee_sheet_iframe').contentWindow.$('#teetime_id').val();
                    url = 'index.php/teesheets/card_captured/'+tee_time_id+'/<?=$person_id?>';
                } else {
                    url = 'index.php/customers/card_captured/<?=$person_id?>/<?=$billing_id?>';
                }
            <?php } ?>
            // Save data to foreup
            $.ajax({
                type: "POST",
                url: url,
                data:jsonResponse,
                success: function(response){
                    try {
                        if (jsonResponse.ResultCode == 'Cancelled') {
                            // Don't even try to add payment or capture card if it was cancelled
                            App.vent.trigger('notification', {'msg': 'The transaction was cancelled at the terminal.', 'type':'error'});
                            var payment = {};
                            App.vent.trigger('credit-card', payment);
                            return;
                        }
                        else if (jsonResponse.ResultCode == 'Declined') {
                            // Don't even try to add payment or capture card if it was cancelled
                            App.vent.trigger('notification', {'msg': 'The card was declined. Please try again or use a different card.', 'type':'error'});
                            var payment = {};
                            App.vent.trigger('credit-card', payment);
                            return;
                        }
                        <?php if (strpos(current_url(),'v2') === false) { ?>
                            <?php if ($controller_name == 'customers') { ?>
                                var current_window = window.parent.document.getElementById('tee_sheet_iframe') != null ? window.parent.document.getElementById('tee_sheet_iframe').contentWindow : window.parent;
                                var message = 'Adding credit card failed. Please try again.';
                                var message_type = 'error_message';
                                var persist = true;
                                if (response.success) {
                                    message = 'Credit card successfully added.';
                                    message_type = 'success_message';
                                    persist = false;
                                }
                                current_window.set_feedback(message, message_type, persist);
                                <?php if ($billing_id > 0) {?>
                                current_window.reload_customer(response.cc_dropdown);
                                current_window.$('#cc_dropdown').val(response.credit_card_id);
                                <?php } else {?>
                                current_window.$.colorbox2({
                                    'href': 'index.php/customers/manage_credit_cards/<?=$person_id?>',
                                    'width': 700
                                });
                                <?php }
                            }
                        } else { ?>
                            // Trigger action
                            var $ = window.parent.$;
                            $('#cancel-credit-card-payment').hide();

                            if (jsonResponse.TransType == 'refund') {
                                jsonResponse.Amount = parseFloat(-jsonResponse.Amount);
                            }

                            var payment = {};
                            if (jsonResponse.CardType == 'MasterCard') {
                                jsonResponse.CardType = 'M/C';
                            } else if (jsonResponse.CardType == 'Visa') {
                                jsonResponse.CardType = 'VISA';
                            } else if (jsonResponse.CardType == 'Amex') {
                                jsonResponse.CardType = 'AMEX';
                            } else if (jsonResponse.CardType == 'Discover') {
                                jsonResponse.CardType = 'DCVR';
                            } else if (jsonResponse.CardType == 'DinersClub') {
                                jsonResponse.CardType = 'DINERS';
                            }

                            payment.record_id = jsonResponse.RefID;
                            payment.invoice_id = jsonResponse.RefID;
                            var merchant_data = jsonResponse;
                            merchant_data.PaymendID = '';
                            merchant_data.ReturnCode = jsonResponse.ResultCode;
                            merchant_data.ReturnMessage = jsonResponse.Message;
                            merchant_data.payment_id = '';
                            payment.merchant_data = merchant_data; //{"PaymentID":"28855ec5-36be-46ea-9ecf-29320459d950","ReturnCode":"0","ReturnMessage":"Your transaction has been approved.","payment_id":"28855ec5-36be-46ea-9ecf-29320459d950"};
                            payment.merchant = 'mercury';
                            payment.approved = (jsonResponse.ResultCode == 'Approved') ? true : false;
                            payment.message = jsonResponse.Message;
                            payment.amount = jsonResponse.Amount;
                            payment.payment_amount = jsonResponse.Amount;

                            payment.params = response.params;
                            payment.params.fee = <?php echo (float) !empty($fee) ? $fee : 0; ?>;
                            payment.description = jsonResponse.CardType + ' ' + jsonResponse.Last4;
                            payment.verified = jsonResponse.ResultCode == 'Approved' ? 1 : 0;
                            payment.auth_code = jsonResponse.AuthCode;
                            payment.cardholder_name = jsonResponse.Name;
                            payment.card_number = jsonResponse.Last4;
                            payment.card_type = jsonResponse.CardType;
                            payment.auth_amount = jsonResponse.Amount;

                            <?php if(!empty($customer_note)){ ?>
                            payment.customer_note = <?php echo !empty($customer_note) ? json_encode($customer_note) : 'null'; ?>;
                            <?php } ?>

                            if (jsonResponse.TransType == 'auth') { //TODO: fix partial CC Refunds for Blackline
                                payment.type = 'credit_card_save';
                                payment.amount = 0;
                                payment.payment_amount = 0;
                                payment.auth_amount = 0;
                                //payment.credit_card_id = '<?$credit_card_id?>';
                            }
                            else if (jsonResponse.TransType == 'refund') { //TODO: fix partial CC Refunds for Blackline
                                payment.tran_type = 'credit_card_partial_refund';
                                payment.type = 'credit_card_partial_refund';
                                payment.payment_type = 'Partial CC Refund';
                            }
                            else {
                                payment.type = 'credit_card';
                            }

                            App.vent.trigger('credit-card', payment);
                        <?php } ?>
                    }
                    catch(err) {
                        // throw error and try and recover
                        if (typeof NREUM != 'undefined') {
                            NREUM.noticeError('EMV - ' + event.data);
                            NREUM.noticeError('EMV response - ' + JSON.stringify(response));
                            NREUM.noticeError('EMV - ' + url);
                            NREUM.noticeError('EMV - ' + err);
                        }
                        else {
                            console.log('EMV - ' + err);
                            console.log('EMV - ' + event.data);
                            console.log('EMV - ' + url);
                        }
                        App.vent.trigger('notification', {'msg': 'There was an error processing the credit card. Please try again.', 'type':'error'});
                        var payment = {};
                        App.vent.trigger('credit-card', payment);
                    }
                    // Close window
                },
                dataType:'json'
            });
        }
        if (window.addedEventListener == undefined || window.addedEventListener != 1) {
            if (window.addEventListener) {
                addEventListener("message", listener, false)
            } else {
                attachEvent("onmessage", listener)
            }
            window.addedEventListener = 1;
        }
    </script>
</head>

<p class="bg-info" style="padding:10px">
    Do not refresh this page or press the back button while processing a payment.
</p>
<iframe style="overflow:hidden" width="550px" height="650px" src="<?=$payment_url?>" id="mercury-emv-iframe"></iframe>

</html>