<style>
div.etsFormGroup {
	margin: 10px 0;
	display: block !important;
	overflow: hidden !important;
}

#ets_img {
	margin:40px auto;
}

.etsButton {
	background: #3D80B9;
	color: white !important;
	border: 1px solid #232323 !important;
	display: block;
	font-size: 14px;
	font-weight: normal;
	width: 150px;
	padding: 0.75em 1.25em;
	text-align: center;
	border-radius: 4px;
	margin-top: 1em;
	border: 1px solid #232323;
}

div.clearFix {
	display: block;
	overflow: hidden;
}

div.labelContainer {
	display: block;
	overflow: hidden;
}

input.etsInput {
	display: block;
	padding: 5px;
	margin: 0px 0px 10px 0px;
	float: right;
}

select.etsSelect {
	display: block;
	padding: 5px;
	margin: 0px 0px 5px 0px;
	float: right;
}

label.etsLabel {
	display: block;
	float: left;
	padding: 5px;
	margin: 0px 0px 10px 0px;
}

p.etsMessage {
	color: #c44b00;
	font-weight: bold;
	font-size: 20px;
}
</style>

<p class="bg-info" style="padding:10px">
    Do not refresh this page or press the back button while processing a payment.
</p>
<div id="ets_iframe_container">
	<?php if(!empty($amount)){ ?>
	<h2>
		$<?php echo number_format($amount, 2); ?>
		<?php if($action == 'refund'){ ?> (Credit)<?php } ?>
	</h2>
	<?php } ?>
	<div id='ets_iframe_replace' data-ets-key="<?php echo trim($session->id); ?>"></div>
</div>

<script>
var interval_id = '';
var count = 0;
$(document).ready(function(){
	interval_id = setInterval(add_ets_handlers, 50);
});

function add_ets_handlers(){

	if($('#ETSIFrame').length > 0){

		// Successful payment processed
		ETSPayment.addResponseHandler("success", function(etsResponse){
			
			<?php if($window == 'balance_check'){ ?>	
			if(etsResponse.status == 'success'){
				var giftcard = {};
				giftcard.date = etsResponse.created;
				giftcard.amount = etsResponse.giftCards.amount;
				giftcard.id = etsResponse.id;
				giftcard.type = 'ets-giftcard';
				giftcard.merchant = 'ets';
			
				App.vent.trigger('ets-giftcard-balance', giftcard);
			}
			
			<?php }else{ ?>
			
			var payment = {};
			payment = etsResponse;
			payment.type = '<?php echo $payment_type; ?>';
			payment.record_id = <?php echo (int) $invoice_id; ?>;
			payment.merchant_data = _.clone(etsResponse);
			payment.merchant_data.session_id = etsResponse.id;
			payment.receipt_id = <?php echo (int) $receipt_id; ?>;
			payment.action = '<?php echo $action; ?>';
			payment.merchant = 'ets';
			payment.message = etsResponse.message;
			payment.customer_note = <?php echo json_encode($customer_note); ?>;
			
			if(payment.type == 'credit_card_save'){
				payment.amount = 0;
				payment.merchant_data.transaction_id = etsResponse.customers.id;
			}else{
				payment.amount = etsResponse.transactions.amount;
				payment.merchant_data.transaction_id = etsResponse.transactions.id;
			}

			payment.params = {};
			payment.params.fee = <?php echo (float) $fee; ?>;
			
			if(etsResponse.status != 'success'){
				payment.approved = false;
			}else{
				payment.approved = true;
			}
			
			<?php if($action == 'refund'){ ?>
			payment.amount = _.round(0 - payment.amount); 
			<?php } ?>
			
			if(payment.type == 'gift_card'){
				payment.type = 'ets_gift_card';
				
				<?php if($giftcard_void){ ?>
				App.vent.trigger('ets-giftcard-void', payment);
				<?php }else{ ?>
				App.vent.trigger('gift-card', payment);
				<?php } ?>

			}else{
                $('#cancel-credit-card-payment').hide();
				App.vent.trigger('credit-card', payment);
			}
			<?php } ?>
		});
		
		clearInterval(interval_id);
		$.loadMask.hide();

	}else if (count > 5000){
		clearInterval(interval_id);
	}
	count++;
}

// Include ETS library into page
if (typeof ETSPayment == 'undefined'){
	var e = document.createElement('script');
	e.src = "<?php echo ECOM_ROOT_URL ?>/init";
	$('head')[0].appendChild(e);

// If library is included, create the iframe
}else{
	ETSPayment.createIFrame();
}
</script>
