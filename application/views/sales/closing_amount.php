<?php $this->load->view('partial/header_new.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
	apply_closing_callbacks();
});
function apply_closing_callbacks()
{
	$('#pennies, #nickels, #dimes, #quarters, #ones, #fives, #tens, #twenties, #fifties, #hundreds').keyup(function(){
		console.log('changing register counts');
		total_closing_amount();
	});
}
function total_closing_amount()
{
	var pennies = $('#pennies').val();
	var nickels = $('#nickels').val();
	var dimes = $('#dimes').val();
	var quarters = $('#quarters').val();
	var ones = $('#ones').val();
	var fives = $('#fives').val();
	var tens = $('#tens').val();
	var twenties = $('#twenties').val();
	var fifties = $('#fifties').val();
	var hundreds = $('#hundreds').val();
	
	var total = parseFloat(0);
	total += parseFloat(pennies != '' ? pennies : 0)* 0.01;
	total += parseFloat(nickels != '' ? nickels : 0) * 0.05;
	total += parseFloat(dimes != '' ? dimes : 0) * 0.10;
	total += parseFloat(quarters != '' ? quarters : 0) * 0.25;
	total += parseFloat(ones != '' ? ones : 0);
	total += parseFloat(fives != '' ? fives : 0) * 5;
	total += parseFloat(tens != '' ? tens : 0) * 10;
	total += parseFloat(twenties != '' ? twenties : 0) * 20;
	total += parseFloat(fifties != '' ? fifties : 0) * 50;
	total += parseFloat(hundreds != '' ? hundreds : 0) * 100;
	
	$('#closing_amount').val(total.toFixed(2));
};

</script>
<style type="text/css">
	.error { float: none !important; }
	.closing_labels {width:60px; text-align:right; display:inline-block;}
</style>
<div id="register_container" class="sales">
<?php
echo form_open('sales/closeregister' . $continue, array('id'=>'closing_amount_form'));
?>
<fieldset id="item_basic_info">
<legend><?php echo lang("sales_closing_amount_desc"); ?></legend>
<?php if (!$this->config->item('blind_close')) { ?>
<div class='closing_approximation'><?php echo sprintf(lang('sales_closing_amount_approx'), $closeout); ?></div>
<?php } ?>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>Pennies</span>
	<?php echo form_input(array(
		'name'=>'pennies',
		'id'=>'pennies',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_pennies'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>Nickels</span>
	<?php echo form_input(array(
		'name'=>'nickels',
		'id'=>'nickels',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_nickels'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>Dimes</span>
	<?php echo form_input(array(
		'name'=>'dimes',
		'id'=>'dimes',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_dimes'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>Quarter</span>
	<?php echo form_input(array(
		'name'=>'quarters',
		'id'=>'quarters',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_quarters'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$1</span>
	<?php echo form_input(array(
		'name'=>'ones',
		'id'=>'ones',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_ones'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$5</span>
	<?php echo form_input(array(
		'name'=>'fives',
		'id'=>'fives',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_fives'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$10</span>
		<?php echo form_input(array(
		'name'=>'tens',
		'id'=>'tens',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_tens'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$20</span>
		<?php echo form_input(array(
		'name'=>'twenties',
		'id'=>'twenties',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_twenties'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$50</span>
	<?php echo form_input(array(
		'name'=>'fifties',
		'id'=>'fifties',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_fifties'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$100</span>
	<?php echo form_input(array(
		'name'=>'hundreds',
		'id'=>'hundreds',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_hundreds'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
	<div class='form_field'>
		<span class='closing_labels'>Checks</span>
	<?php echo form_input(array(
		'name'=>'total_checks',
		'id'=>'total_checks',
		'style'=>'text-align:right;',
		'placeholder'=>'Total Checks',
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
	<div class='form_field'>
		<span class='closing_labels'>Total</span>
	<?php echo form_input(array(
		'name'=>'closing_amount',
		'id'=>'closing_amount',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_closing_amount'),
		'value'=>'')
	);?>
	</div>
</div>


<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
</div>
<?php $this->load->view('partial/footer.php'); ?>
<script type='text/javascript'>

	
//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#closing_amount_form').validate({
		rules:
		{
			closing_amount: {
				required: true,
				number: true
			}
   		},
		messages:
		{
			closing_amount: {
				required: "<?php echo lang('sales_amount_required'); ?>",
				number: "<?php echo lang('sales_amount_number'); ?>"
			}
		}
	});
});
</script>