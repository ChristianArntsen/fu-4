<?php
if(!isset($amount)){
    $amount = 0;
}
if (isset($mobile) && $mobile) {
	define("ECOM_ROOT_URL", "https://www.etsemoney.com/hp/v2");
	?>
<script src="<?php echo base_url();?>js/jquery-1.10.2.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
<?php } else {$mobile = false;} ?>
<style>
<?php if (isset($tee_time_card) && $tee_time_card) { ?>
	#cbox2Wrapper .wrapper {
		margin: 30px 20px 0px;
		width:515px;
	}
<?php } else { ?>
	.wrapper { margin: 30px 100px 0px; }
<?php } ?>
.etsFormGroup { margin: 10px 0; }
#ets_img { margin:40px auto;}

.etsButton {
	background: #3D80B9; /* for non-css3 browsers */
	color: white !important;
	border: 1px solid #232323 !important;
	display: block;
	font-size: 14px;
	font-weight: normal;
	height: 22px;
	padding: 5px 0 0 10px;
	text-align: center;
	text-shadow: 0px -1px 0px black !important;
	border-radius: 4px;
	margin-bottom: 8px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
}

div.pendingMessage p, .etsMessage {
	color: #31708f;
	padding: 5px;
	font-weight: bold;		
}
</style>
<script>
    var jQueryScriptOutputted = false;
    function initJQuery() {
        if (typeof(jQuery) == 'undefined') {
            if (! jQueryScriptOutputted) {
                jQueryScriptOutputted = true;
                document.write('<scr' + 'ipt type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></scr' + 'ipt>');
            }
            setTimeout("initJQuery()", 50);
        }
    }
    initJQuery();
</script>
<script>

var interval_id = '';
var poll_interval = '';
var count = 0;
$(document).ready(function(){
	interval_id = setInterval(add_ets_handlers, 200);
	<?php if (!$mobile) { ?>
	setTimeout(function(){$.colorbox2.resize()},3000);
	<?php } ?>
});

function add_ets_handlers()
{
	if ($('#ETSIFrame').length > 0)
	{
		ETSPayment.addResponseHandler("success", function(e){
			$.ajax({
				type: "POST",
				url: "<?php echo $url; ?>",
				data: 'response=' + JSON.stringify(e),
				success: function(response){
					$('#ets_payment_window').html(response);
				},
				dataType:'html'
			});	
		});
		clearInterval(interval_id);
	}
	else if (count > 50)
	{
		clearInterval(interval_id);
	}
	count++;
}
</script>
<div class="wrapper" id="ets_payment_window">
	<?php if (!isset($tee_time_card) || !$tee_time_card) { ?>
	<h1><?php if (isset($type) && $type == 'giftcard') {echo 'Giftcard ';}?>Total: $<?=$amount?></h1>
	<?php } ?>
	<!-- HTML5 Magic -->
	<div id='ets_session_id' data-ets-key="<?php echo trim($session->id); ?>">
		<img id='ets_img' src="http://www.loadinfo.net/main/download?spinner=3875&disposition=inline" alt="">
	</div>

</div>
<div id='ets_script_box'>
</div>
<script>
	if (typeof ETSPayment == 'undefined')
	{
		var e = document.createElement('script');
		e.src = "<?php echo ECOM_ROOT_URL ?>/init";
		//document.getElementById("ets_script_box").appendChild(e);
		$('head')[0].appendChild(e);
	}
	else
	{
		ETSPayment.createIFrame();
	}
</script>
