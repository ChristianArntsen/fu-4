<?php 
$disabled = '';
if(empty($customer_id)){ 
	$disabled = 'disabled';
}
?>
<style>
#return_credit_card:before {
	background-position: -126px -210px !important;
}

#return_cash:before  {
	background-position: -126px -170px !important;
}

#return_from_account:before  {
	background-position: -126px -50px !important;
}

#return_from_member_account:before  {
	background-position: -126px -50px !important;
}

div.amounts {
	display: block;
	width: auto;
	padding: 10px;
	background: #DDD;
	border-bottom: 1px solid #AAA;
	overflow: hidden;
	margin-bottom: 30px;
}

div.amounts input {
	box-shadow: inset 0px 6px 24px -12px black;
	border-radius: 5px;
	border: 1px solid #B1B1B1;
	padding: 4px;
	line-height: 20px;
	color: #666464;
	font-size: 14px;
	font-weight: lighter;
}

div.amounts div.field {
	display: block;
	float: left;
	margin: 0px;
	padding: 0px;
}

div.amounts div.field > div.field {
	height: 22px;
	line-height: 22px;
}

div.amounts div.field label {
	font-size: 14px; 
	font-weight: bold;
	display: block;
	float: left;
	margin-right: 10px;
	height: 30px;
	line-height: 30px;
}

div.amounts div.field span.value {
	font-size: 16px;
	height: 30px;
	line-height: 30px;
}

#partial_return_form {
	display: block;
	margin: 0px;
	padding: 0px;
	width: auto;
	overflow: hidden;
	width: 500px;
	height: 200px;
}
</style>
<div id="partial_return_window" style="display: none;">
	
	<form id="partial_return_form" name="partial_return" action="<?php echo site_url('sales/partial_return'); ?>" data-sale-total="<?php echo (float) ($sale_total + $refunded); ?>">
		<input id="partial_return_sale_id" type="hidden" name="sale_id" value="<?php echo $sale_id; ?>" />
		<input id="partial_return_payment_type" type="hidden" name="payment_type" value="" />
		<input id="partial_return_customer_id" type="hidden" name="customer_id" value="<?php echo (int) $customer_id; ?>" />
		
		<div class="amounts">
			<div class="field" style="width: 200px;">
				<div class="field">
					<label style="font-weight: normal; width: 80px;">Sale</label>
					<span class="value" id="partial_return_sale_amount"><?php echo to_currency($sale_total); ?></span>				
				</div>
				<div class="field">	
					<label style="font-weight: normal; width: 80px;">Refunded</label>
					<span class="value" id="partial_return_refunded"><?php echo to_currency($refunded); ?></span>			
				</div>
				<div class="field">	
					<label style="width: 80px;">Total</label>
					<span class="value" id="partial_return_refunded"><?php echo to_currency($sale_total + $refunded); ?></span>			
				</div>								
			</div>

			<div class="field" style="float: right">
				<label>Return Amount</label>
				<input id="return_amount" type="text" name="payment_amount" style="width: 50px;" value="<?php echo $sale_total + $refunded; ?>" />
			</div>
		</div>
		<div class='payment_column_1'>
			<span id='return_cash' class='payment_button'>Cash</span>
			<span id='return_credit_card' class='payment_button'>Credit Card</span>
		</div>
		<div class='payment_column_2'>
			<span id='return_from_account' class='payment_button payment_button_wide <?php echo $disabled; ?>'><?=$cab_name;?> Credit</span>
			<span id='return_from_member_account' class='payment_button payment_button_wide <?php echo $disabled; ?>'><?=$cmb_name;?> Credit</span>
		</div>
	</form>
</div>

<script>
function submit_partial_return(){
	var form = $('#partial_return_form');
	var url = form.attr('action');
	var params = form.serialize();
	
	$('cbox2LoadedContent').mask();
	
	$.post(url, params, function(response){
		$('cbox2LoadedContent').unmask();
		
		if(response.success){
			set_feedback('Refund applied successfully', 'success_message');
			$.colorbox2.close();
		}else{
			set_feedback(response.message, 'error_message');			
		}
	},'json');
}

$(document).ready(function(){
	
	if($('#feedback_bar').length <= 0){
		$('#body').append('<div id="feedback_bar"></div>');
	}
	
	$("#return_credit_card").die('click').live('click',function(e){
		
		var amount = $('#return_amount').val();
		
		<?php if($this->config->item('mercury_id') != '' || $this->config->item('ets_key') != ''){ ?>
		if(amount <= 0){
			set_feedback('Refund must be greater than $0.00', 'error_message');
			return false;
		}
		
		$.colorbox2({
			'href': '<?php echo site_url('sales/credit_card_refund_window'); ?>',
			'data': {'sale_id':<?php echo (int) $sale_id; ?>, 'amount':amount},
			'width': 600,
			'height': 550
		});
		<?php }else{ ?>
		$('#partial_return_payment_type').val('Credit Card');
		submit_partial_return();			
		<?php } ?>

		e.preventDefault();		
	});

	$('#return_cash').die('click').live('click',function(e){
		$('#partial_return_payment_type').val('Cash Refund');
		submit_partial_return();
		e.preventDefault();

		<?php if ($this->config->item('cash_drawer_on_cash') && $this->config->item('webprnt') != '1') { ?>
		if(typeof open_cash_drawer == 'function'){
			open_cash_drawer();
		}
		<?php } ?>
	});

	$('#return_from_account').die('click').live('click',function(e){
		$('#partial_return_payment_type').val('customer_account');
		submit_partial_return();
		e.preventDefault();		
	});

	$('#return_from_member_account').die('click').live('click',function(e){
		$('#partial_return_payment_type').val('member_account');
		submit_partial_return();
		e.preventDefault();	
	});
});
</script>
