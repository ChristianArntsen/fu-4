<form id="add_item_form">
	<?php echo form_input(array('name'=>'item','id'=>'item','size'=>'40', 'accesskey' => 'i', 'placeholder'=>lang('sales_start_typing_item_name')));?>
	<?php echo form_hidden('price_index','');?>
	<?php echo form_hidden('teetime_type','');?>
	<div id='mode_button' class='mode_<?php echo $mode ?>'><ul><li id='sales_button'>Sales</li><li id='return_button'>Return</li></ul></div>
</form>
<script type="text/javascript">
$( "#item" ).autocomplete({
	source: '<?php echo site_url("sales/item_search"); ?>',
	delay: 200,
	autoFocus: false,
	minLength: 1,
	select: function(event, ui)
	{
		$("input[name=price_index]").val(ui.item.price_index);
		$("input[name=teetime_type]").val(ui.item.teetime_type);
		
		var timeframe_id;
		if(ui.item.timeframe_id){
			timeframe_id = ui.item.timeframe_id
		}

		sales.add_item(ui.item.value, false, ui.item.price_index, ui.item.teetime_type, timeframe_id);
	}
});
$('#mode_button').click(function(){
	sales.change_sales_mode('<?php echo $mode ?>');
});
$("#add_item_form").submit(function(e) {
	e.preventDefault();
	$.ajax({
       type: "POST",
       url: "index.php/sales/add/"+$("#item").val(),
       data: "",
       success: function(response) {
	   			sales.update_page_sections(response);
	   },
       dataType:'json'
    });
});
</script>
