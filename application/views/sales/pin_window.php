<style>
	#individual-list {
		margin:10px;
	}
</style>
<ul id="error_message_box"></ul>
<?php
	echo form_open('sales/add_pin_to_sale/',array('id'=>'pin_form'));
?>
<fieldset id="pin_basic_info" style='padding-top:10px;'>
<div class="field_row clearfix">
<?php echo form_label('', 'employee_pin',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'name'=>'employee_pin',
		'size'=>'12',
		'maxlength'=>'16',
		'id'=>'employee_pin',
		'placeholder'=>'Employee Pin',
		'value'=>'')
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<style>
fieldset#pin_basic_info div.field_row label {
	width:113px;
}
#sales #colorbox #pin_basic_info input.submit_button {
	margin:15px 0 15px 144px;
}
</style>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
    $('#pin_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit(
			{
				success:function(response) {
					if (!response.success) {
						set_feedback('Invalid Pin', 'error_message',true);
						$(form).unmask();
						$('#employee_pin').focus();
					} else {
                		set_feedback('Pin accepted: '+response.name, 'success_message',false);
           				submitting = false;
				    	$("#finish_sale_form").submit();
						$.colorbox.close();
					}
		            submitting = false;
	                console.dir(response[0]);
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
	 	wrapper: "li",
		rules:{},
		messages:{}
	});
	setTimeout(function(){$('#employee_pin').focus();},200);
});
</script>