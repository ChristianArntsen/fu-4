<?php

$persisting_times = array();
$even_row = false;
?>
<div style="width:100%; margin-left:50px; ">
<table style="width:85%;" cellspacing="0">
    <tbody>
        <tr><th colspan="2" style="text-align:center;"><img src="<?= $this->Appconfig->get_logo_image() ?>" style="height:150px;" /></th></tr>
        <tr><th colspan="2" style="text-align:center;">Starter Sheet</th></tr>
        <tr><th colspan="2" style="text-align:center;"><?=$tee_sheet_info->title.' - '.date("F d, Y", strtotime($date))?></th></tr>
        <tr><th style="text-align:left; width:10%;">Time</th><th style="text-align:left; width:90%">Details</th></tr>
<?php
foreach ($slots as $mt => $slot) {
    // Generate the details
    $details = '';
    foreach($persisting_times as $pti => $persistant_time) {
        $persistant_time_start_time = (int) substr($persistant_time['start'],8);
        $persistant_time_end_time = (int) substr($persistant_time['end'],8);
        //echo 'pet '.$persistant_time_end_time.'<br/>';
        if ($persistant_time_end_time <= (int) $mt || $persistant_time['duration'] == 1) {
            unset($persisting_times[$pti]);
            continue;
        }

        $title = '';
        $title .= ($persistant_time['person_name'] == '') ? '' : ' - '.$persistant_time['person_name'];
        $title .= ($persistant_time['person_name_2'] == '') ? '' : ' - '.$persistant_time['person_name_2'];
        $title .= ($persistant_time['person_name_3'] == '') ? '' : ' - '.$persistant_time['person_name_3'];
        $title .= ($persistant_time['person_name_4'] == '') ? '' : ' - '.$persistant_time['person_name_4'];
        $title .= ($persistant_time['person_name_5'] == '') ? '' : ' - '.$persistant_time['person_name_5'];

        $title = $title == '' ? $persistant_time['title'] : $title;
        $details .= ((int)$persistant_time['reround'] == 1 ? '<span class="reround_icon" style="background:url('.base_url('images/icons/teesheet_icons.png').') -45px 1px no-repeat; width:20px; height:11px; display:inline-block; margin-right:5px; margin-top:5px;"></span>' : '')."{$title}<br/>";
    }
    foreach($tee_times as $tti => $tee_time) {
        // Take every time that applies and add it to details
        $tee_time_start_time = (int) substr($tee_time['start'],8);
        $tee_time_end_time = (int) substr($tee_time['end'],8);

        // Stop when we've reached the end of applicable times
        if ($tee_time_start_time > (int) $mt || ($tee_sheet_info->holes > 9 && $tee_time['side'] == 'back')) {
            // Tee times all start after now, so we're breaking
            continue;
        }
        $tt_details = $print_notes ? $tee_time['details'] : '';
        $title = '';
        $title .= ($tee_time['person_name'] == '') ? '' : ' - '.$tee_time['person_name'];
        $title .= ($tee_time['person_name_2'] == '') ? '' : ' - '.$tee_time['person_name_2'];
        $title .= ($tee_time['person_name_3'] == '') ? '' : ' - '.$tee_time['person_name_3'];
        $title .= ($tee_time['person_name_4'] == '') ? '' : ' - '.$tee_time['person_name_4'];
        $title .= ($tee_time['person_name_5'] == '') ? '' : ' - '.$tee_time['person_name_5'];

        $title = $title == '' ? $tee_time['title'] : $title;

        $details .= ((int)$tee_time['reround'] == 1 ? '<span class="reround_icon" style="background:url('.base_url('images/icons/teesheet_icons.png').') -45px 1px no-repeat; width:20px; height:11px; display:inline-block; margin-right:5px; margin-top:5px;"></span>' : '')."{$title} - {$tee_time['player_count']}: {$tt_details}<br/>";

        // Add any persisting times into the persisting array
        $persisting_times[] = $tee_time;
        unset($tee_times[$tti]);
    }
    //$details = $mt;
    $even_style = $even_row ? 'background-color:#efefef;' : '';
    echo "<tr><td style='border-bottom:1px solid #ccc; $even_style'>$slot</td><td style='border-bottom:1px solid #ccc; $even_style'>$details</td></tr>";
    $even_row = $even_row ? false : true;
}
if ($print_back_nine && $tee_sheet_info->holes > 9) {
    ?>
        </tbody>
    </table>
    <div style="page-break-before:always;"></div>
    <table style="width:85%;" cellspacing="0">
    <tbody>
    <tr><th colspan="2" style="height:40px;" ></th></tr>
    <tr><th colspan="2" ></th></tr>
    <tr><th colspan="2" ></th></tr>
    <tr><th colspan="2" style="text-align:center;"><?= $tee_sheet_info->title.' - '.date("F d, Y", strtotime($date))?> (Back 9)</th></tr>
    <tr><th style="text-align:left; width:10%">Time</th><th style="text-align:left">Details</th></tr>
    <?php
    foreach ($slots as $mt => $slot) {
        // Generate the details
        $details = '';
        foreach($persisting_times as $pti => $persistant_time) {
            $persistant_time_start_time = (int) substr($persistant_time['start'],8);
            $persistant_time_end_time = (int) substr($persistant_time['end'],8);
            //echo 'pet '.$persistant_time_end_time.'<br/>';
            if ($persistant_time_end_time <= (int) $mt || $persistant_time['duration'] == 1) {
                unset($persisting_times[$pti]);
                continue;
            }
            $title = '';
            $title .= ($persistant_time['person_name'] == '') ? '' : ' - '.$persistant_time['person_name'];
            $title .= ($persistant_time['person_name_2'] == '') ? '' : ' - '.$persistant_time['person_name_2'];
            $title .= ($persistant_time['person_name_3'] == '') ? '' : ' - '.$persistant_time['person_name_3'];
            $title .= ($persistant_time['person_name_4'] == '') ? '' : ' - '.$persistant_time['person_name_4'];
            $title .= ($persistant_time['person_name_5'] == '') ? '' : ' - '.$persistant_time['person_name_5'];

            $title = $title == '' ? $persistant_time['title'] : $title;
            $details .= ((int)$persistant_time['reround'] == 1 ? '<span class="reround_icon" style="background:url('.base_url('images/icons/teesheet_icons.png').') -45px 1px no-repeat; width:20px; height:11px; display:inline-block; margin-right:5px; margin-top:5px;"></span>' : '')."{$title}<br/>";
        }
        foreach($tee_times as $tti => $tee_time) {
            // Take every time that applies and add it to details
            $tee_time_start_time = (int) substr($tee_time['start'],8);
            $tee_time_end_time = (int) substr($tee_time['end'],8);

            // Stop when we've reached the end of applicable times
            if ($tee_time_start_time > (int) $mt || $tee_time['side'] == 'front') {
                // Tee times all start after now, so we're breaking
                continue;
            }
            $tt_details = $print_notes ? $tee_time['details'] : '';
            $title = '';
            $title .= ($tee_time['person_name'] == '') ? '' : ' - '.$tee_time['person_name'];
            $title .= ($tee_time['person_name_2'] == '') ? '' : ' - '.$tee_time['person_name_2'];
            $title .= ($tee_time['person_name_3'] == '') ? '' : ' - '.$tee_time['person_name_3'];
            $title .= ($tee_time['person_name_4'] == '') ? '' : ' - '.$tee_time['person_name_4'];
            $title .= ($tee_time['person_name_5'] == '') ? '' : ' - '.$tee_time['person_name_5'];

            $title = $title == '' ? $tee_time['title'] : $title;

            $details .= ((int)$tee_time['reround'] == 1 ? '<span class="reround_icon" style="background:url('.base_url('images/icons/teesheet_icons.png').') -45px 1px no-repeat; width:20px; height:11px; display:inline-block; margin-right:5px; margin-top:5px;"></span>' : '')."{$title} -  {$tee_time['player_count']}: {$tt_details}<br/>";

            // Add any persisting times into the persisting array
            $persisting_times[] = $tee_time;
            unset($tee_times[$tti]);
        }
        //$details = $mt;
        $even_style = $even_row ? 'background-color:#efefef;' : '';
        echo "<tr><td style='border-bottom:1px solid #ccc; $even_style'>$slot</td><td style='border-bottom:1px solid #ccc; $even_style'>$details</td></tr>";
        $even_row = $even_row ? false : true;
    }

}
?>
    </tbody>
</table>
</div>
