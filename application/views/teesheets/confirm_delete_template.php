<div id='confirm_delete_buttons' class='template_buttons'>
    <div id='cancel_delete_template' class='template_button apply_button'>Cancel</div>
    <div id='confirm_delete_template' class='template_button delete_button'>Delete</div>
</div>
<style>
    div#confirm_delete_buttons {
        margin: 10px auto;
        float: none;
        width: 170px;
    }
    div#cancel_delete_template {

    }
</style>
<script>
    $(document).ready(function(){
        var template_id = <?=$template_id?>;
        $('#confirm_delete_template').on('click',function(){
            $.ajax({
                type: "POST",
                url: "index.php/teesheets/delete_template/",
                data: "template_id=" + template_id,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        // Remove template from list
                        $('#template_' + template_id).remove();
                        $.colorbox2.close();
                    }
                }
            });
        });
        $('#cancel_delete_template').on('click',function() {
            $.colorbox2.close();
        });
    });
</script>