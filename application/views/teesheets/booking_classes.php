<?php 
	$price_categories = ($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types()); 
	$customer_group = $this->Customer->get_group_info(isset($customer_id) ? $customer_id : -1);
	foreach ($customer_group as $key => $value) {
		$customer_groups[$value['group_id']] = $value['label'];
	}
?>

<table class='booking_class_table'>
	<tbody>
		<tr>
			<th class='booking_class_cell'>Active</th>
			<th class='booking_class_cell'>Name</th>
			<th class='booking_class_cell'>List of Groups</th>
			<th class='booking_class_cell'>Price Class</th>
			<th class='booking_class_cell'>Login Required</th>
			<th></th>
			<th></th>
		</tr>
		<?php foreach ($obcs as $obc) { ?>
			<tr id='booking_class_<?=$obc['booking_class_id']?>'>
				<td class='booking_class_cell'><?=$obc['active'] ? 'Yes' : 'No'?></td>
				<td class='booking_class_cell'><?=$obc['name']?></td>
				<?php if ($obc['online_booking_protected'] == 1 && !empty($customer_groups[$obc['group_id']])) { ?>
					<td class='booking_class_cell'><?=$customer_groups[$obc['group_id']]?></td>
				<?php }else{ ?>
					<td class='booking_class_cell'></td>
				<?php } ?>
				<td class='booking_class_cell'><?=!empty($price_categories[$obc['price_class']]) ? $price_categories[$obc['price_class']] : ''?></td>
				<td class='booking_class_cell'><?=$obc['online_booking_protected'] ? 'Yes' : 'No'?></td>
				<td><a class='booking_class_edit' href='index.php/teesheets/view_booking_class/<?=$teesheet_info->teesheet_id?>/<?=$obc['booking_class_id']?>/width~800'>Edit</a></td>
				<td><a class='booking_class_delete' href='index.php/teesheets/delete_booking_class/<?=$teesheet_info->teesheet_id?>/<?=$obc['booking_class_id']?>'>Delete</a></span></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<style>
	.booking_class_table {
		background:#e4e4e4;
		font-size:12px;
		margin:0px 5px;	
		color:#181818;
	}
	.booking_class_cell {
		text-align:center;
		padding:3px;
	}
</style>
<script>
	$('.booking_class_edit').colorbox2();
	$('.booking_class_delete').click(function(e){
		e.preventDefault();
		var url = $(e.target).attr('href');
		if (confirm("Are you sure you want to delete this booking class?"))
		{
			$.ajax({
	           type: "POST",
	           url: url,
	           data: '',
	           success: function(response){
	           		console.dir(response);
	           		if (response.success)
		           		$('#booking_class_'+response.booking_class_id).remove();
			    },
	            dataType:'json'
	         });
	     }
	});
</script>
