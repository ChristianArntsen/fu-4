<?php
// Name the template
?>
<form id="template_form" action="/teesheets/save_template/">
    <div id="template_source_holder">
        <div id="template_tee_sheet_holder">
            Tee Sheet: <?php echo form_dropdown('template_tee_sheet_id', $teesheets, $tee_sheet_id);?>
        </div>
        <div id="template_date_holder">
            Date: <input type="date" placeholder="Date" name="template_date" value="<?=date('Y-m-d', strtotime($date))?>"/>
        </div>
    </div>
    <div id="template_name_holder">
        Name your new template: <input type="text" placeholder="Template Name" name="template_name"/>
    </div>
    <?php
    // Show events that are part of the template
    ?>
    <div id="template_events">
        <table id="template_events_table">
            <thead>
            <tr>
                <th>Event Name</th>
                <th>Event Start</th>
                <th>Event End</th>
                <!--th>Remove</th-->
            </tr>
            </thead>
            <tbody id="template_events_body">
                <?= $this->load->view('teesheets/template_events');?>
            </tbody>
        </table>
    </div>
    <div id="template_save_button_holder">
        <input type="submit" value="Save" name="template_save"/>
    </div>
</form>
<style>
    #template_name_holder, #template_date_holder, #template_tee_sheet_holder {
        padding:10px;
    }
    #template_name_holder input[name=template_name] {
        font-size:16px;
        margin-left:10px;
    }
    #template_events_table td, #template_events_table th {
        text-align:center;
        padding:5px;
    }
    #template_events_table th {
        font-size:20px;
    }
    #template_save_button_holder {
        padding:10px;
        text-align:center;
    }
    #template_save_button_holder input[name=template_save] {
        width:150px;
        height:30px;
        background-color: #287db9;
        color: white;
    }
    #template_save_button_holder input[name=template_save]:hover {
        background-color: #1672b9;
    }
</style>
<script>
    var submitting = false;

    $(document).ready(function() {
        $('input[name=template_date], #template_tee_sheet_id').on('change', function () {
            //alert('changing');
            var template_date = $('input[name=template_date]').val();
            var template_tee_sheet_id = $('#template_tee_sheet_id').val();
            if (template_date == '') {
                $('#template_events_body').html('');
            } else {
                //alert('getting template events');
                $.ajax({
                    type: "POST",
                    url: "index.php/teesheets/get_template_events/",
                    data: "tee_sheet_id=" + template_tee_sheet_id + "&date=" + template_date,
                    dataType: 'html',
                    success: function (response) {
                        $('#template_events_body').html(response);
                        return;
                    }
                });


            }
        });
        $('#template_form').validate({
            submitHandler: function (form) {
                if ($('input[name=template_name]').val() == '') {
                    set_feedback('Please name your template','error',false, 3000);
                    return false;
                }
                if ($('#template_events_body tr').length == 0) {
                    set_feedback('Please select a date that has events', 'error', false, 3000);
                    return false;
                }
                if (submitting) return;
                submitting = true;
                $(form).mask('<?php echo lang('common_wait'); ?>');
                //console.log( 'about to submit form');
                $(form).ajaxSubmit({
                    success: function (response) {
                        // Refresh data on template manager page
                        $.colorbox({'href':'index.php/teesheets/view_template_manager', 'title':'Save/Apply Template','width':'800'});
                        $.colorbox2.close();
                    },
                    dataType: 'json'
                });
            },
            errorLabelContainer: '#error_message_box',
            wrapper: 'li'
        });
    });
</script>