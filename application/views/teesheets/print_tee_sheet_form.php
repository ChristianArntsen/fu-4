<ul id="error_message_box"></ul>
<?php
echo form_open('teesheets/printable_tee_sheet/',array('id'=>'printable_tee_sheet_form'));
?>
<fieldset id="printable_tee_sheet_basic_info">
    <div id='printable_tee_sheet_basic_info' style=''>
        <div class="field_row clearfix">
            <?php echo form_label(lang('courses_date').':', 'name',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'print_date',
                        'id'=>'print_date',
                        'value'=>$date)
                );?>
            </div>
        </div>
        <div class="field_row clearfix">
            <?php echo form_label('Tee Sheet:', 'print_tee_sheet_id',array('class'=>'wide')); ?>

            <div class='form_field'>
                <?php echo form_dropdown('print_tee_sheet_id',
                    $tee_sheet_ids,
                    $selected_tee_sheet_id,
                    "id = 'print_tee_sheet_id' "
                );?>
            </div>
        </div>
        <div class="field_row clearfix">
            <?php echo form_label(lang('courses_show_back_nine').':', 'show_back_nine',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_checkbox('show_back_nine', '1', false);?>
            </div>
        </div>
        <div class="field_row clearfix">
            <?php echo form_label(lang('courses_show_notes').':', 'show_notes',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_checkbox('show_notes', '1', true);?>
            </div>
        </div>

        <div class="field_row clearfix">
		    <?php echo form_label("Print Full Week".':', 'week',array('class'=>'wide')); ?>
            <div class='form_field'>
			    <?php echo form_checkbox('week', '1', false);?>
            </div>
        </div>

    </div>
        <a target="_blank" class="submit_button float_right" id="print_submit" href="#"><?=lang('common_generate')?></a>
        <?php
//        echo anchor(array(
//                'name'=>'print_submit',
//                'id'=>'print_submit',
//                'value'=>lang('common_print'),
//                'class'=>'submit_button float_right')
//        );
        ?>
    <div class="or_holder"> OR download the old tee sheet printout.</div>
    <div class="legacy_tee_sheet_pdf">
        <a id="print_button" name="print_button" target="_blank" href="index.php/teesheets/print_teesheet/<?=date('Ymd', strtotime($date))-100?>">Tee Sheet Printout</a>
    </div>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
    //validation and submit handling
    $(document).ready(function()
    {
        // Apply Date Picker
        $('#print_date').datepicker({'dateFormat': 'yy-m-d'});

        // Apply Form Submit
        $('#printable_tee_sheet_form').on('submit', function(e) {
            e.preventDefault();
            // Loading screen
            // Fetch HTML and Print
            return false;
        });
        $("#print_submit").on('click', function(){
            // Change URL
            var print_date = $("#print_date").val();
            var tee_sheet_id = $('#print_tee_sheet_id').val();
            var show_back_nine = $("input[name=show_back_nine]").attr('checked') ? '1' : '0';
            var show_notes = $("input[name=show_notes]").attr('checked') ? '1' : '0';
            var week = $("input[name=week]").attr('checked') ? '1' : '0';
            var url = 'index.php/teesheets/pdf_tee_sheet/'+print_date+'/'+tee_sheet_id+'/'+show_back_nine+'/'+show_notes+'/'+week;
            $(this).attr('href',url);
        });
    });
</script>
<style>
    #teesheets #cboxContent fieldset#printable_tee_sheet_basic_info div.field_row label {
        width: 170px;
    }
    #printable_tee_sheet_basic_info a#print_submit {
        padding:5px 10px;
        margin:20px 43% 0px;
    }
    fieldset#printable_tee_sheet_basic_info {
        width:600px;
    }
    fieldset#printable_tee_sheet_basic_info, fieldset#printable_tee_sheet_basic_info .form_field, fieldset#printable_tee_sheet_basic_info .field_row  {
        width:560px;
    }
    .or_holder, .legacy_tee_sheet_pdf {
        font-weight: bold;
        text-align: center;
        padding: 30px 0px 20px;
    }
    .legacy_tee_sheet_pdf {
        padding:0px 0px 20px;
    }
</style>