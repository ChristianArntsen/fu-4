<?php

$persisting_times = array();
$even_row = false;
?>
<div style="width:100%">
    <?=$date ?>
<table cellspacing="0" style="table-layout: fixed;width:100%">
    <tbody style="font-size: 9px;">
    <?php

    if(isset($hide_times) && $hide_times == 1){
	    echo "<tr><th colspan='2' style=\"text-align:left; width:70%\">Details</th></tr>";
    } else {
	    echo "<tr><th style=\"text-align:left;width:30% \">Time</th><th style=\"text-align:left; width:80%\">Details</th></tr>";
    }
    ?>

<?php
foreach ($slots as $mt => $slot) {
    // Generate the details
    $details = '';
    foreach($persisting_times as $pti => $persistant_time) {
        $persistant_time_start_time = (int) substr($persistant_time['start'],8);
        $persistant_time_end_time = (int) substr($persistant_time['end'],8);
        //echo 'pet '.$persistant_time_end_time.'<br/>';
        if ($persistant_time_end_time <= (int) $mt || $persistant_time['duration'] == 1) {
            unset($persisting_times[$pti]);
            continue;
        }

	    $title = $persistant_time['type'];
	    $title .= " - ".$persistant_time['title'];
        $details .= ((int)$persistant_time['reround'] == 1 ? '<span class="reround_icon" style="background:url('.base_url('images/icons/teesheet_icons.png').') -45px 1px no-repeat; width:20px; height:11px; display:inline-block; margin-right:5px; margin-top:5px;"></span>' : '')."{$title}";
    }
    foreach($tee_times as $tti => $tee_time) {
        // Take every time that applies and add it to details
        $tee_time_start_time = (int) substr($tee_time['start'],8);
        $tee_time_end_time = (int) substr($tee_time['end'],8);

        // Stop when we've reached the end of applicable times
        if ($tee_time_start_time > (int) $mt || ($tee_sheet_info->holes > 9 && $tee_time['side'] == 'back')) {
            // Tee times all start after now, so we're breaking
            continue;
        }

	    $title = $tee_time['type'];
	    $title .= " - ".$tee_time['title'];

        $details .= ((int)$tee_time['reround'] == 1 ? '<span class="reround_icon" style="background:url('.base_url('images/icons/teesheet_icons.png').') -45px 1px no-repeat; width:20px; height:11px; display:inline-block; margin-right:5px; margin-top:5px;"></span>' : '')."{$title} - {$tee_time['player_count']}";

        // Add any persisting times into the persisting array
        $persisting_times[] = $tee_time;
        unset($tee_times[$tti]);
    }
    //$details = $mt;
    $even_style = $even_row ? 'background-color:#efefef;' : '';
    if(empty($details)){
        $details = "&nbsp;";
    }
	if(isset($hide_times) && $hide_times == 1){
		echo "<tr height='10px'><td colspan='2' style='white-space:nowrap;overflow:hidden;border-bottom:1px solid #ccc; $even_style'>$details</td></tr>";
	} else {
		echo "<tr height='10px'><td style='border-bottom:1px solid #ccc; $even_style' width:>$slot</td><td style='white-space:nowrap;overflow:hidden;border-bottom:1px solid #ccc; $even_style'>$details</td></tr>";
	}
    $even_row = $even_row ? false : true;
}
if ($print_back_nine && $tee_sheet_info->holes > 9) {

}
?>
    </tbody>
</table>
</div>
