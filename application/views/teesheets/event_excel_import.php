<?php
echo form_open_multipart('teesheets/do_excel_import/',array('id'=>'event_player_form'));
?>
<div id="required_fields_message"><?php echo lang('customers_mass_import_from_excel'); ?></div>
<ul id="error_message_box"></ul>
<b><a id='template_link' href="<?php echo site_url('teesheets/event_excel'); ?>"><?php echo lang('customers_download_excel_import_template'); ?></a></b>
<fieldset id="item_basic_info">
    <legend><?php echo lang('teesheets_import_players'); ?></legend>

    <div class="field_row clearfix">
        <?php echo form_label(lang('common_file_path').':', 'name',array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_upload(array(
                    'name'=>'file_path',
                    'id'=>'file_path',
                    'value'=>'')
            );?>
        </div>
    </div>
    <input type="hidden" name="import_type" value="event"/>

    <?php
    echo form_submit(array(
            'name'=>'submitf',
            'id'=>'submitf',
            'value'=>lang('common_submit'),
            'class'=>'submit_button float_right')
    );
    ?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
    jQuery.extend({
        handleError: function( s, xhr, status, e ) {
            // If a local callback was specified, fire it
            if ( s.error ) {
                s.error(xhr, status, e);
            }
            // If we have some XML response text (e.g. from an AJAX call) then log it in the console
            else if(xhr.responseText) {
                console.log('')
                console.log(xhr.responseText);
            }
        }
    });
    //validation and submit handling
    $(document).ready(function()
    {
        var submitting = false;
        $('#event_player_form').validate({
            submitHandler:function(form)
            {
                if (submitting) return;
                submitting = true;
                $(form).mask("<?php echo lang('common_wait'); ?>");
                var formData = new FormData($(form)[0]);
                console.log('about to ajax the data in');
                console.dir(formData);
                $.ajax({
                    url: 'index.php/teesheets/do_excel_import/',  //Server script to process data
                    type: 'POST',
                    xhr: function() {  // Custom XMLHttpRequest
                        var myXhr = $.ajaxSettings.xhr();
                        if(myXhr.upload){ // Check if upload property exists
                            myXhr.upload.addEventListener('progress',function(){}, false); // For handling the progress of the upload
                        }
                        return myXhr;
                    },
                    //Ajax events
                    beforeSend: function(){},
                    success: function(data){
                        var names = data.names;
                        for (var i in names) {
                            inline_form.add_event_person($("#teetime_id").val(), {person_id:0, label:names[i]});
                        }
                        $.colorbox.resize();
                        var teetime_info = {};
                        teetime_info.TTID = $("#teetime_id").val();
                        teetime_info.start = $('input[name=start]').val();
                        inline_form.initialize_event_people(teetime_info);
                        submitting = false;
                        $.colorbox2.close();
                    },
                    error: function(data){
                        set_feedback(data.message,'error_message',false);
                    },
                    // Form data
                    data: formData,
                    //Options to tell jQuery not to process data or worry about content-type.
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType:'json'
                });

            },
            errorLabelContainer: "#error_message_box",
            wrapper: "li",
            rules:
            {
                file_path:"required"
            },
            messages:
            {
                file_path:"<?php echo lang('customers_full_path_to_excel_required'); ?>"
            }
        });
    });
</script>