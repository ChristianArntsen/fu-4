<?php
$months = array(
    1 => 'January',
    2 => 'February',
    3 => 'March',
    4 => 'April',
    5 => 'May',
    6 => 'June',
    7 => 'July',
    8 => 'August',
    9 => 'September',
    10 => 'October',
    11 => 'November',
    12 => 'December'
);
$days = array(
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    5 => 5,
    6 => 6,
    7 => 7,
    8 => 8,
    9 => 9,
    10 => 10,
    11 => 11,
    12 => 12,
    13 => 13,
    14 => 14,
    15 => 15,
    16 => 16,
    17 => 17,
    18 => 18,
    19 => 19,
    20 => 20,
    21 => 21,
    22 => 22,
    23 => 23,
    24 => 24,
    25 => 25,
    26 => 26,
    27 => 27,
    28 => 28,
    29 => 29,
    30 => 30,
    31 => 31
);
$increments = array(
    '5.0'=>'5 min',
    '6.0'=>'6 min',
    '7.0'=>'7 min',
    '7.5'=>'7/8 min',
    '8.0'=>'8 min',
    '9.0'=>'9 min',
    '10.0'=>'10 min',
    '11.0'=>'11 min',
    '12.0'=>'12 min',
    '13.0'=>'13 min',
    '14.0'=>'14 min',
    '15.0'=>'15 min',
    '20.0'=>'20 min',
    '30.0'=>'30 min',
    '45.0'=>'45 min',
    '60.0'=>'60 min'
);
echo form_open('teesheets/save_increment_adjustments/',array('id'=>'increment_adjustments_form'));
?>
    <fieldset id="incrememnt_adjustments_info">
        <div id="increment_adjustments">
            <input type="hidden" name="teesheet_id" value="<?=$teesheet_id?>"/>
            <div class="severe_warning">
                <b>Warning:</b> <?php if ($using_api) {?>If you are currently integrated with Golf 18 Network or another 3rd party booking engine, customizing increments will create issues with that integration. We do not recommend editing these settings at this time. <?}?>
                Adding custom increments will cause issues with existing tee times. There are currently <?=$future_booking_count?> reservations scheduled for this tee sheet. To ensure the smoothest transition possible, <i>please</i> call in and work with a foreUP support agent before making changes. Thank you.
            </div>
            <table>
                <tbody>
            <?php foreach($increment_adjustments as $increment_adjustment) {
                $iaid = $increment_adjustment['increment_adjustment_id'];
                ?>
                <tr data-increment-adjustment-id="<?=$increment_adjustment['increment_adjustment_id']?>" class="increment_row">
                    <td>
                        <div class="increment_settings_label">Increment Label</div>
                        <input class="increment_settings_input" name="increment_label_<?=$iaid?>" value="<?=$increment_adjustment['label']?>"/>
                    </td>
                    <td>
                        <div class="increment_settings_label">Start Date</div>
                        <input type="hidden" name="increment_adjustment_id[]" value="<?=$iaid?>"/>
                        <input class="increment_settings_input datepicker" name="start_date_<?=$iaid?>" value="<?=date("M d", strtotime($increment_adjustment['start_date']))?>"/>
                        <?php //echo form_dropdown('start_month_'.$iaid, $months, (int) date('m', strtotime($increment_adjustment['start_date']))); ?>
                        <?php //echo form_dropdown('start_day_'.$iaid, $days, (int) date('d', strtotime($increment_adjustment['start_date']))); ?>
                    </td>
                    <td>
                        <div class="increment_settings_label">Start Time</div>
                        <input class="increment_settings_input timepicker" name="start_time_<?=$iaid?>" value="<?=$increment_adjustment['start_time']?>"/>
                    </td>
                    <td>
                        <div class="increment_settings_label">Days of the week</div>
                        <span class="day_of_week_buttonset">
                            <?php echo form_checkbox('monday_'.$iaid, '1', $increment_adjustment['monday'], 'id="increment_monday_box_'.$iaid.'"');?>
                            <label class='day_of_week_button' for="increment_monday_box_<?=$iaid?>" id="increment_monday_box_label_<?=$iaid?>">M</label>
                            <?php echo form_checkbox('tuesday_'.$iaid, '1', $increment_adjustment['tuesday'], 'id="increment_tuesday_box_'.$iaid.'"');?>
                            <label class='day_of_week_button' for="increment_tuesday_box_<?=$iaid?>" id="increment_tuesday_box_label_<?=$iaid?>">T</label>
                            <?php echo form_checkbox('wednesday_'.$iaid, '1', $increment_adjustment['wednesday'], 'id="increment_wednesday_box_'.$iaid.'"');?>
                            <label class='day_of_week_button' for="increment_wednesday_box_<?=$iaid?>" id="increment_wednesday_box_label_<?=$iaid?>">W</label>
                            <?php echo form_checkbox('thursday_'.$iaid, '1', $increment_adjustment['thursday'], 'id="increment_thursday_box_'.$iaid.'"');?>
                            <label class='day_of_week_button' for="increment_thursday_box_<?=$iaid?>" id="increment_thursday_box_label_<?=$iaid?>">Th</label>
                            <?php echo form_checkbox('friday_'.$iaid, '1', $increment_adjustment['friday'], 'id="increment_friday_box_'.$iaid.'"');?>
                            <label class='day_of_week_button' for="increment_friday_box_<?=$iaid?>" id="increment_friday_box_label_<?=$iaid?>">F</label>
                            <?php echo form_checkbox('saturday_'.$iaid, '1', $increment_adjustment['saturday'], 'id="increment_saturday_box_'.$iaid.'"');?>
                            <label class='day_of_week_button' for="increment_saturday_box_<?=$iaid?>" id="increment_saturday_box_label_<?=$iaid?>">Sa</label>
                            <?php echo form_checkbox('sunday_'.$iaid, '1', $increment_adjustment['sunday'], 'id="increment_sunday_box_'.$iaid.'"');?>
                            <label class='day_of_week_button' for="increment_sunday_box_<?=$iaid?>" id="increment_sunday_box_label_<?=$iaid?>">Su</label>
                        </span>
                    </td>
                    <td class="increment_vertical_separator_cell" rowspan="2">
                        <div class="increment_vertical_separator"></div>
                    </td>
                    <td class="final_column" rowspan="2">
                        <a href="#" class="remove_increment_adjustment">x</a>
                        <div class="increment_settings_label">Remove</div>
                    </td>
                </tr>
                <tr data-increment-adjustment-id="<?=$increment_adjustment['increment_adjustment_id']?>" class="increment_row second_increment_row">
                    <td>
                        <div class="increment_settings_label">Highlight Color</div>
                        <input class='increment_settings_input increment_color color-picker' name="increment_color_<?=$iaid?>" value="<?=$increment_adjustment['color']?>" type="text" />
                    </td>
                    <td>
                        <div class="increment_settings_label">End Date</div>
                        <input class="increment_settings_input datepicker" name="end_date_<?=$iaid?>" value="<?=date("M d", strtotime($increment_adjustment['end_date']))?>"/>
                        <?php //echo form_dropdown('end_month_'.$iaid, $months, (int) date('m', strtotime($increment_adjustment['end_date']))); ?>
                        <?php //echo form_dropdown('end_day_'.$iaid, $days, (int) date('d', strtotime($increment_adjustment['end_date']))); ?>
                    </td>
                    <td>
                        <div class="increment_settings_label">End Time</div>
                        <input class="increment_settings_input timepicker" name="end_time_<?=$iaid?>" value="<?=$increment_adjustment['end_time']?>"/>
                    </td>
                    <td>
                        <div class="increment_settings_label">Increment</div>
                        <?php echo form_dropdown('increment_'.$iaid, $increments, (string) $increment_adjustment['increment'], 'class="increment_dropdown"'); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div class="increment_settings_separator"></div>
                    </td>
                </tr>
            <?php } ?>
                </tbody>
            </table>
            <div id="increment_adjustment_button_holder">
                <?php
                echo form_submit(array(
                        'name'=>'add_increment_adjustment',
                        'id'=>'add_increment_adjustment',
                        'value'=>'+ Add New',
                        'class'=>'submit_button')
                );
                ?>
                <div class="clear"></div>
                <div class="increment_settings_separator"></div>
                <?php
                echo form_submit(array(
                        'name'=>'save_increment_adjustments',
                        'id'=>'save_increment_adjustments',
                        'value'=>'Save',
                        'class'=>'submit_button')
                );
                ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </fieldset>
</form>
<script>
    var increment_adjustments = {
        initialize : function () {
            jQuery_1_7_2('.timepicker').timepicker({});//{'timeFormat':'h:mm tt'});

            $('.datepicker').datepicker({'dateFormat': 'M d'});
            $('.day_of_week_buttonset').each(function(){
                if ($(this).hasClass('ui-buttonset')) {
                    $(this).buttonset('destroy').buttonset();
                } else {
                    $(this).buttonset();
                }

            });
            $('.remove_increment_adjustment').off('click').on('click', function(e){
                e.preventDefault();
                $(this).closest('tr').next('tr').next('tr').remove();
                $(this).closest('tr').next('tr').remove();
                $(this).closest('tr').remove();
                $.colorbox2.resize();
            });
            $('#add_increment_adjustment').off('click').on('click', function(e){
                e.preventDefault();
                increment_adjustments.save(true);
            });
            $('#save_increment_adjustments').off('click').on('click', function(e){
                e.preventDefault();
                increment_adjustments.save();
            });
            jQuery_1_7_2('input.increment_color').spectrum({

                preferredFormat: 'hex6',
                allowEmpty: true,
                showInput: true,
                clickoutFiresChange:true,
                showPreviewCode:true,
                change: function(color) {
                    var selectedColor = color.toHexString();
                    var element = $(this).next('.sp-preview');
                    $(element).after('<span class="sp-preview-code">'+selectedColor+'</span>')
                }
            });
        },
        save : function (add_row) {
            add_row = add_row == undefined ? 0 : 1;
            $.ajax({
                type: "POST",
                url: "index.php/teesheets/save_increment_adjustments/"+add_row,
                data: $('#increment_adjustments_form').serialize(),
                success: function (response) {
                    if (response.success) {
                        if (add_row) {
                            view_increment_settings();
                        }
                        else {
                            $.colorbox2.close();
                        }
                    }
                },
                dataType: 'json'
            });
        }
    }
    $(document).ready(function(){
        increment_adjustments.initialize();
    });
</script>
<style>
    #increment_adjustment_button_holder {
        padding:0px 0px 0px 20px;
        margin-bottom:30px;
    }
    #increment_adjustment_button_holder input[type=submit] {
        margin:10px;
        float:left;
    }
    #increment_adjustments table td {
        text-align:left;
        padding:0px 0px 0px 20px;
    }
    .timepicker {
        width:70px;
    }
    #increment_adjustments table a.remove_increment_adjustment {
        margin: 0px 0px 0px 0px;
        font-weight: bold;
        font-size: 20px;
        color: transparent;
        background: url(/images/teesheet/new/garbage-can.png) no-repeat transparent;
        width: 28px;
        height: 28px;
        display: inline-block;
    }
    .severe_warning {
        background: #c70d0d;
        color: white;
        margin: 10px;
        padding: 15px;
        font-size: 16px;
    }
    div#cbox2Title {
        height:60px;
        line-height:60px;
        font-weight:normal;
        font-size:20px;
        padding-left:20px;
        text-align:left;
        background:#369fcb;

    }
    div#cbox2Close {
        background: url(/images/teesheet/new/white-x.png) no-repeat 0px 0;
        right:10px;
        top:22px;
    }
    div#cbox2LoadedContent {
        margin-top:60px;
    }
    div.severe_warning {
        background: #f4d17e;
        color: #4c4c4b;
        margin: 0px 0px 30px;
        padding: 14px 26px;
        font-size: 12px;
        line-height: 30px;
    }
    .increment_settings_label {
        font-size:11px;
        color:gray;
        padding: 5px 0px;
    }
    .increment_settings_remove_label {

    }
    .increment_settings_input, #config input, .sp-replacer, .increment_dropdown {
        height: 18px;
        width: 140px;
        border: 1px solid gainsboro;
        padding: 5px;
        color: #5a5a5a;
        border-radius:3px;
        box-shadow:none;
        background:white;
    }
    #config input[type=checkbox] {
        width:auto;
    }
    .increment_row {

    }
    .second_increment_row {

    }
    .increment_settings_separator {
        border-bottom:1px solid gainsboro;
        display: block;
        margin:20px 20px 20px 0px;
    }
    .increment_vertical_separator {
        border-left:1px solid gainsboro;
        display: block;
        height:100px;
        width:1px;
    }
    td.increment_vertical_separator_cell {
        width:1px;
        padding:0px;
        margin:0px;
    }
    #increment_adjustments td.final_column {
        padding:0px 20px;
        text-align: center;
    }
    #increment_adjustments td.final_column .increment_settings_label {
        color: #d22b2b;
        font-size: 10px;
    }
    .sp-preview {
        height:16px;
        width:16px;
        border: 1px solid #dcdcdc;
    }
    .sp-dd {
        float: right;
    }
    .sp-preview-code {
        height: 20px;
        line-height: 20px;
    }
    .increment_dropdown {
        width: 218px;
        height: 30px;
    }
    #increment_adjustment_button_holder #add_increment_adjustment, #increment_adjustment_button_holder #save_increment_adjustments {
        float: right;
        margin: 0px 20px 0px 0px;
        text-shadow: none;
        border: none;
        width: auto;
        padding: 5px 20px;
        background:#369fcb;
        box-shadow:none;
        cursor:pointer;
    }
    #increment_adjustment_button_holder .increment_settings_separator {
        margin-top:50px;
    }
    .day_of_week_buttonset label.day_of_week_button {
        border: 1px solid gainsboro;
        background: white;
        height: 28px;
        margin: 0px;
        padding: 0px;
        width: 30px;
        color: #525252;
        text-shadow: none;
        font-size:12px;
        border-left:none;
    }
    .day_of_week_buttonset label.ui-corner-left {
        border-top-left-radius:3px;
        border-bottom-left-radius:3px;
        border-left: 1px solid gainsboro;
    }
    .day_of_week_buttonset label.ui-corner-right {
        border-top-right-radius:3px;
        border-bottom-right-radius:3px;
    }
    .day_of_week_buttonset label.ui-state-active {
        background:#cccccc;
        color:white;
    }
    .day_of_week_buttonset .ui-button-text-only .ui-button-text {
        padding: 0px;
        line-height:28px;
    }
</style>