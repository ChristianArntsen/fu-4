<style>
select.booking-class-multi {
	height: 100px;
	margin-bottom: 15px;
}

div.fee-item-placeholder {
	display: block;
	overflow: hidden;
	float: left;
	padding: 0px 5px 0px 5px;
	line-height: 30px;
	height: 30px;
	background: #E0E0E0;
	border: 1px solid #D0D0D0;
}

a.fee-item-remove {
	display: block;
	float: right;
	margin-left: 15px;
	color: red;
	font-weight: bold;
	cursor: pointer;
}

a.fee-item-remove:hover {
	color: blue;
}
</style>
<ul id="error_message_box"></ul>
<?php
echo form_open('teesheets/save_booking_class/'.$booking_class_info->booking_class_id,array('id'=>'booking_class_form'));

$title = lang("teesheets_booking_class_basic_info");
if($booking_class_info->is_aggregate == 1){
	$title = 'Aggregate Settings';
} ?>
<fieldset id="booking_class_basic_info">
<legend><?php echo $title?></legend>
	<?php if($booking_class_info->is_aggregate == 0){ ?>
	<div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_teesheet_title').':', 'bc_name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'bc_name',
			'size'=>'20',
			'id'=>'bc_name',
			'value'=>$booking_class_info->name)
		);?>
	</div>
	</div><div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_active').':', 'active',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_radio("active",0,$booking_class_info->active==1?FALSE:TRUE); ?>
		Off
		<?php echo form_radio("active",1,$booking_class_info->active==1?TRUE:FALSE); ?>
		On
		</div>
	</div>	
	<div class="field_row clearfix">	
    <?php echo form_label(lang('config_online_booking_protected').':', 'online_booking_protected',array('class'=>'wide')); ?>
        <div class='form_field'>
		<?php echo form_radio("online_booking_protected",0,$booking_class_info->online_booking_protected==1?FALSE:TRUE); ?>
		Off
		<?php echo form_radio("online_booking_protected",1,$booking_class_info->online_booking_protected==1?TRUE:FALSE); ?>
		On
		</div>
    </div>
    <?php
    $group_array = array('0'=>'Select Group');
    foreach($groups as $group)
	{
		$group_array[$group['group_id']] = $group['label'];
    }

 	$pass_array = [];
 	foreach($passes as $pass){
 		$pass_array[$pass['item_id']] = $pass['name'];
 	}
    ?>
    <div class="field_row clearfix" id="password_protect_settings">
		<div class="field_row clearfix">
		<?php echo form_label('Display filled times and player names', 'show_full_details',array('class'=>'wide')); ?>
			<div class='form_field'>
			<?php echo form_radio("show_full_details",0,!empty($booking_class_info->show_full_details) && $booking_class_info->show_full_details ==1?FALSE:TRUE); ?>
			No
			<?php echo form_radio("show_full_details",1,!empty($booking_class_info->show_full_details) && $booking_class_info->show_full_details ==1?TRUE:FALSE); ?>
			Yes
			</div>
		</div>		
		<?php echo form_label(lang('booking_class_groups').':', 'bc_class_groups'); ?>
		<div class='form_field'>
		<?php
			echo form_multiselect('bc_class_groups[]', $group_array, $class_groups, "class='booking-class-multi'");
		?>
		</div>
		<?php echo form_label('Passes:', 'bc_pass_item_ids'); ?>
		<div class='form_field'>
		<?php
			echo form_multiselect('bc_pass_item_ids[]', $pass_array, $booking_class_info->pass_item_ids, "class='booking-class-multi'");
		?>
		</div>
	    <div class="field_row clearfix" id="use_customer_pricing">
	        <div class="field_row clearfix">
	            <?php echo form_label('Allow player name entry', 'bc_allow_name_entry',array('class'=>'wide')); ?>
	            <div class='form_field'>
	                <?php echo form_radio("bc_allow_name_entry",0,$booking_class_info->allow_name_entry==1?FALSE:TRUE); ?>
	                No
	                <?php echo form_radio("bc_allow_name_entry",1,$booking_class_info->allow_name_entry==1?TRUE:FALSE); ?>
	                Yes
	            </div>
	        </div>
	    </div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('customers_price_class').':', 'bc_price_class'); ?>
		<div class='form_field'>
		<?php
			echo form_dropdown('bc_price_class', $price_classes, $booking_class_info->price_class);
		?>
		</div>
	</div>
    <div class="field_row clearfix" id="use_customer_pricing">
        <div class="field_row clearfix">
            <?php echo form_label('Use Customer Pricing', 'bc_use_customer_pricing',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_radio("bc_use_customer_pricing",0,$booking_class_info->use_customer_pricing==1?FALSE:TRUE); ?>
                No
                <?php echo form_radio("bc_use_customer_pricing",1,$booking_class_info->use_customer_pricing==1?TRUE:FALSE); ?>
                Yes
            </div>
        </div>
    </div>
    <div class="field_row clearfix" id="hide_online_prices">
        <div class="field_row clearfix">
            <?php echo form_label('Hide Online prices', 'bc_hide_online_prices',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_radio("bc_hide_online_prices",0,!empty($booking_class_info->hide_online_prices) && $booking_class_info->hide_online_prices==1?FALSE:TRUE); ?>
                No
                <?php echo form_radio("bc_hide_online_prices",1,!empty($booking_class_info->hide_online_prices) && $booking_class_info->hide_online_prices==1?TRUE:FALSE); ?>
                Yes
            </div>
        </div>
    </div>
    <?php }else{

		echo form_input(array(
			'name'=>'bc_name',
			'id'=>'bc_name',
			'type' => 'hidden',
			'value'=>$booking_class_info->name)
		);    	
    } ?>
	<div class="field_row clearfix">
	<?php echo form_label('Require Credit Card Online:', 'require_credit_card',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_radio("require_credit_card",0,$booking_class_info->require_credit_card==1?FALSE:TRUE); ?>
		No
		<?php echo form_radio("require_credit_card",1,$booking_class_info->require_credit_card==1?TRUE:FALSE); ?>
		Yes
		</div>
	</div>    
    <div class="field_row clearfix">
    <?php echo form_label(lang('config_online_open_time').':', 'booking_class_open_time',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_dropdown('booking_class_open_time', array(
                    '0000'=>'12:00am',
                    '0030'=>'12:30am',
                    '0100'=>'1:00am',
                    '0130'=>'1:30am',
                    '0200'=>'2:00am',
                    '0230'=>'2:30am',
                    '0300'=>'3:00am',
                    '0330'=>'3:30am',
                    '0400'=>'4:00am',
                    '0430'=>'4:30am',
                    '0500'=>'5:00am',
                    '0530'=>'5:30am',
                    '0600'=>'6:00am',
                    '0630'=>'6:30am',
                    '0700'=>'7:00am',
                    '0730'=>'7:30am',
                    '0800'=>'8:00am',
                    '0830'=>'8:30am',
                    '0900'=>'9:00am',
                    '0930'=>'9:30am',
                    '1000'=>'10:00am',
                    '1030'=>'10:30am',
					'1100'=>'11:00am',
					'1130'=>'11:30am',
					'1200'=>'12:00pm',
					'1230'=>'12:30pm',
					'1300'=>'1:00pm',
					'1330'=>'1:30pm',
					'1400'=>'2:00pm',
					'1430'=>'2:30pm',
					'1500'=>'3:00pm',
					'1530'=>'3:30pm',
					'1600'=>'4:00pm',
                    '1630'=>'4:30pm',
                    '1700'=>'5:00pm',
                    '1730'=>'5:30pm',
                    '1800'=>'6:00pm',
                    '1830'=>'6:30pm',
                    '1900'=>'7:00pm',
                    '1930'=>'7:30pm',
                    '2000'=>'8:00pm',
                    '2030'=>'8:30pm',
                    '2100'=>'9:00pm',
                    '2130'=>'9:30pm',
                    '2200'=>'10:00pm',
                    '2230'=>'10:30pm',
					'2300'=>'11:00pm',
					'2330'=>'11:30pm',
					'2400'=>'12:00am'),
                    $booking_class_info->online_open_time);
            ?>
            <span class="settings_note">
                (First avail. tee time)
            </span>
        </div>
    </div>
    <div class="field_row clearfix">	
    <?php echo form_label(lang('config_online_close_time').':', 'booking_class_close_time',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_dropdown('booking_class_close_time', array(
                    '0000'=>'12:00am',
                    '0030'=>'12:30am',
                    '0100'=>'1:00am',
                    '0130'=>'1:30am',
                    '0200'=>'2:00am',
                    '0230'=>'2:30am',
                    '0300'=>'3:00am',
                    '0330'=>'3:30am',
                    '0400'=>'4:00am',
                    '0430'=>'4:30am',
                    '0500'=>'5:00am',
                    '0530'=>'5:30am',
                    '0600'=>'6:00am',
                    '0630'=>'6:30am',
                    '0700'=>'7:00am',
                    '0730'=>'7:30am',
                    '0800'=>'8:00am',
                    '0830'=>'8:30am',
                    '0900'=>'9:00am',
                    '0930'=>'9:30am',
                    '1000'=>'10:00am',
                    '1030'=>'10:30am',
					'1100'=>'11:00am',
					'1130'=>'11:30am',
					'1200'=>'12:00pm',
					'1230'=>'12:30pm',
					'1300'=>'1:00pm',
					'1330'=>'1:30pm',
					'1400'=>'2:00pm',
					'1430'=>'2:30pm',
					'1500'=>'3:00pm',
					'1530'=>'3:30pm',
					'1600'=>'4:00pm',
                    '1630'=>'4:30pm',
                    '1700'=>'5:00pm',
                    '1730'=>'5:30pm',
                    '1800'=>'6:00pm',
                    '1830'=>'6:30pm',
                    '1900'=>'7:00pm',
                    '1930'=>'7:30pm',
                    '2000'=>'8:00pm',
                    '2030'=>'8:30pm',
                    '2100'=>'9:00pm',
                    '2130'=>'9:30pm',
                    '2200'=>'10:00pm',
                    '2230'=>'10:30pm',
					'2300'=>'11:00pm',
					'2330'=>'11:30pm',
					'2400'=>'12:00am'),
                    $booking_class_info->online_close_time);
            ?>
                <span class="settings_note">
                    (Last avail. tee time)
                </span>
            </div>
    </div>
        
	<div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_days_in_booking_window').':', 'bc_days_in_booking_window',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php
            $sixty_day_array = array();
            for ($i = 0; $i < 61; $i++)
            {
                $sixty_day_array[$i] = $i;
            }
            $sixty_day_array[90] = 90;
            $sixty_day_array[120] = 120;
            $sixty_day_array[365] = 365;
            
            echo form_dropdown('bc_days_in_booking_window', $sixty_day_array, $booking_class_info->days_in_booking_window, 'id="bc_days_in_booking_window"');
			?>
		</div>
	</div>  
	<div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_minimum_players').':', 'bc_minimum_players',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php 
				echo form_dropdown('bc_minimum_players', array('4'=>'4', '3'=>'3', '2'=>'2', '1'=>'1'), $booking_class_info->minimum_players, 'id="bc_minimum_players"'); 
			?>
		</div>
	</div>  
	<div class="field_row clearfix">
	<?php echo form_label(lang('teesheets_limit_holes').':', 'bc_limit_holes',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php 
				echo form_dropdown('bc_limit_holes', array('0'=>'No Limit', '9'=>'9 only', '18'=>'18 only'), $booking_class_info->limit_holes, 'id="bc_limit_holes"'); 
			?>
		</div>
	</div>  
	<div class="field_row clearfix">
	<?php echo form_label('Online rates allowed:', 'bc_booking_carts',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_dropdown(
				'bc_booking_carts', 
				array('0'=>'Walking', '2'=>'Riding', '1'=>'Both'), 
				isset($booking_class_info->booking_carts) ? $booking_class_info->booking_carts : 1, 
				'id="bc_booking_carts"'
			); ?>
		</div>
	</div>
	<div class="field_row clearfix">
	    <?php echo form_label('Allow pre-payment online', 'bc_pay_online', array('class'=>'wide')); ?>
	    <div class='form_field'>
			<?php echo form_dropdown('bc_pay_online', array(
				'0' => 'Not Allowed', 
				'1' => 'Optional', 
				'2' => 'Required'
			), isset($booking_class_info->pay_online) ? $booking_class_info->pay_online : 1, 'id="bc_pay_online"'); ?>
	    </div>
	</div>

	<div class="field_row clearfix">
		<?php echo form_label('Require Booking Fee', 'booking_fee_enabled',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_radio("booking_fee_enabled", 0, ($booking_class_info->booking_fee_enabled == 0)); ?>
			No
			<?php echo form_radio("booking_fee_enabled", 1, ($booking_class_info->booking_fee_enabled == 1)); ?>
			Yes
		</div>
	</div>

    <div id="booking_fee_settings">
		<div class="field_row clearfix">
			<?php echo form_label('Charge Booking Fee Per Person', 'booking_fee_enabled',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_radio("booking_fee_per_person", 0, ($booking_class_info->booking_fee_per_person == 0)); ?>
				No
				<?php echo form_radio("booking_fee_per_person", 1, ($booking_class_info->booking_fee_per_person == 1)); ?>
				Yes
			</div>
		</div>

		<div class="field_row clearfix">
		<?php echo form_label('Booking Fee Item', 'fee_item_search', array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
					'name'=>'fee_item_search',
					'size'=>'20',
					'id'=>'fee_item_search',
					'value'=> '',
					'style' => $booking_class_info->booking_fee_item ? 'display: none' : '',
					'placeholder' => 'Search items...')
				);?>
				<div class="fee-item-placeholder" <?php if(empty($booking_class_info->booking_fee_item)){ ?>style="display: none"<?php } ?>>
					<span class="fee-item-name">
					<?php if($booking_class_info->booking_fee_item){
						echo $booking_class_info->booking_fee_item['name'] .' - '. to_currency($booking_class_info->booking_fee_item['total']);
					} ?></span>
					<a class="fee-item-remove">X</a>
				</div>
			</div>
			<input type="hidden" id="booking_fee_item_id" name="booking_fee_item_id" value="<?php echo $booking_class_info->booking_fee_item_id; ?>" />
		</div>

        <div class="field_row clearfix" style="margin-top: 5px">
            <label for="booking_fee_terms" style="display: block; float: none; width: 500px">Booking Fee Terms (Displayed to customer before paying)</label>
            <div class='form_field' style="margin-left: 5px">
                <textarea id='booking_fee_terms' style="width: 500px; height: 100px" name='booking_fee_terms'><?php echo $booking_class_info->booking_fee_terms; ?></textarea>
            </div>
        </div>	
    </div>

	<?php echo form_hidden('teesheet_id', $teesheet_id)?>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_save'),
		'class'=>'submit_button float_right')
	);
	?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
function toggle_booking_fee_settings(){
	var field = $('input[name="booking_fee_enabled"]:checked');
	if(field.val() == 1){
		$('#booking_fee_settings').show();
	}else{
		$('#booking_fee_settings').hide();
	}
}

//validation and submit handling
$(document).ready(function()
{
	toggle_booking_fee_settings();
	$('input[name="booking_fee_enabled"]').change(function(){
		toggle_booking_fee_settings(); 
	});
	
	$("#fee_item_search").autocomplete({
		source: function(request, response){
	        $.ajax({
	            url: "<?php echo site_url('api/items');?>?item_type=item&api_key=no_limits",
	            dataType: "json",
	            data: {
	                search: request.term
	            },
	            success: function(data) {
	                response($.map(data.rows, function(item) {
	                    return {
	                        label: item.name + ' - $' + item.unit_price,
	                        value: item.item_id
	                    }
	                }))
	            }
	        })
    	},
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui){
			var placeholder = $('div.fee-item-placeholder');
			placeholder.find('span.fee-item-name').html(ui.item.label);
			placeholder.show();

			$('#fee_item_search').hide();
			$('#booking_fee_item_id').val(ui.item.value);
			
			return false;
		}
	});

	$('a.fee-item-remove').click(function(){
		$('div.fee-item-placeholder').hide();
		$('#fee_item_search').show();
		$('#booking_fee_item_id').val('');
	});

	var submitting = false;

    $('#booking_class_form').validate({
		submitHandler:function(form)
		{
			if($('input[name="booking_fee_enabled"]:checked').val() == 1 &&
				(!$('#booking_fee_item_id').val() || $('#booking_fee_item_id').val() == 0)
			){
				alert('Booking fee item is required');
				return false;
			}

			if ($.trim($('#bc_name').val()) != '')
			{
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				$(form).ajaxSubmit({
					success:function(response)
					{
						$('#online_booking_classes').html(response);
						$.colorbox2.close();
						submitting = false;
					},
					dataType:'html'
				});
			}
			else
				alert('Please enter a booking class name');

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
	if ($("input[type=radio][name='online_booking_protected']:checked").val() == 0) {
		$('#password_protect_settings').hide();
	}
	$('input[name=online_booking_protected]').click(function() {
      	if($(this).attr('value') == '1') {
            $('#password_protect_settings').show();           
       	}else {
           	$('#password_protect_settings').hide();
       	}
   	});
});
</script>
