<?php
// Save current template
// Apply current template
// Delete template
?>
<div id="template_list">
<?php
foreach ($templates as $template) {
    echo "<div class='template_container' id='template_{$template['template_id']}'><div class='template_name template_{$template['template_id']}'>
            <span class='template_name_label'>{$template['name']}</span>
            <div class='template_buttons'>
                <div data-template-id='{$template['template_id']}' class='template_button apply_button'>Apply</div>
                <div data-template-id='{$template['template_id']}' class='template_button delete_button'>Delete</div>
            </div>
            </div>";
    echo "<div class='template_description template_{$template['template_id']}'><table width='100%'><tbody>{$template['description']}</tbody></table></div></div>";
}
?>
</div>
<div id="template_footer_buttons">
    <div id="save_template_button" class="save_template_button">+ Create New Template</div>
    <div id="close_window_button" class="close_window_button">Close Window</div>
</div>
<style>
    #template_header_buttons {
        height:40px;
        border-bottom:1px solid #ccc;
    }
    #save_template_button {
        margin: 10px 0 0 10px;
        width: 200px;
        border: 1px solid #ccc;
        text-align: center;
        padding: 5px;
        cursor:pointer;
        float:left;
        background-color: #287db9;
        color: white;
    }
    #save_template_button:hover {

    }
    #template_footer_buttons {
        height:40px;
        margin-bottom:10px;
        border-top:1px solid #ccc;
    }
    #close_window_button {
        margin: 10px 10px 0 0;
        width: 200px;
        border: 1px solid #ccc;
        text-align: center;
        padding: 5px;
        cursor:pointer;
        float:right;
    }
    #template_list {
        height:400px;
        max-height:400px;
        overflow-y:scroll;
    }
    .template_container {
        border-bottom:1px solid #ccc;s
    }
    .template_container:hover {
        background-color:#a4b2cb;
    }
    .template_buttons {
        float: right;
    }
    .template_button {
        margin: 0px 0px 15px 10px;
        width: 60px;
        border: 1px solid #ccc;
        text-align: center;
        padding: 5px;
        cursor:pointer;
        float: left;
        font-size:14px;
        background-color:aliceblue;
    }
    .apply_button {

    }
    .delete_button {
        background-color:indianred;
    }
    .template_name {
        font-size: 20px;
        padding: 10px;
    }
    .template_description {
        padding: 0px 0px 20px 20px;
    }

</style>
<script>
    $(document).ready(function() {
        console.log('apply_button count '+($('.apply_button').length));
        $('.apply_button').on('click', function () {
            var template_id = $(this).data('template-id');
            var tee_sheet_id = $('#teesheetMenu').val();
            tee_sheet_id = tee_sheet_id ? tee_sheet_id : '<?=$this->session->userdata('teesheet_id')?>';
            var date = $('.calendar').fullCalendar('getDate');
            date = date.getFullYear() + '-' + (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1) + '-' + (date.getDate() < 10 ? '0' : '') + date.getDate();
            var template_name = $('.template_' + template_id + ' .template_name_label').html();
            $.colorbox2({'href':'index.php/teesheets/confirm_apply_template/'+template_id+'/'+tee_sheet_id+'/'+date, 'width':800, 'title':'Apply Template: '+template_name.substring(0,20)});
        });
        $('.delete_button').on('click', function () {
            var template_id = $(this).data('template-id');
            var template_name = $('.template_' + template_id + ' .template_name_label').html();
            $.colorbox2({'href':'index.php/teesheets/confirm_delete_template/'+template_id, 'width':400, 'title':'Delete Template: '+template_name.substring(0,20)+'?'});
        });
        $(".save_template_button").on('click', function () {
            var tee_sheet_id = $('#teesheetMenu').val();
            tee_sheet_id = tee_sheet_id ? tee_sheet_id : '<?=$this->session->userdata('teesheet_id')?>';
            var date = $('.calendar').fullCalendar('getDate');
            date = date.getFullYear() + '-' + (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1) + '-' + (date.getDate() < 10 ? '0' : '') + date.getDate();

            $.colorbox2({'href': 'index.php/teesheets/confirm_template_save/'+tee_sheet_id+'/'+date, 'width': 800, 'title': 'New Template'});
        });
        $('.close_window_button').on('click', function () {
            $.colorbox.close();
        });

    });
</script>