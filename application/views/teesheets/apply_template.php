<?php
// Name the template
?>
<form id="apply_template_form" action="/teesheets/apply_template/">
    <input type="hidden" name="template_id" value="<?=$template['template_id']?>"/>
    <div id="template_source_holder">
        <div id="template_tee_sheet_holder">
            Tee Sheet: <?php echo form_dropdown('template_tee_sheet_id', $teesheets, $tee_sheet_id);?>
        </div>
        <div id="template_date_holder">
            Date: <input type="date" placeholder="Date" name="template_date" value="<?=date('Y-m-d', strtotime($date))?>"/>
        </div>
    </div>
    <div id="template_name_holder">
        Applying Template: <?=$template['name']?>
    </div>
    <?php
    // Show events that are part of the template
    ?>
    <div id="template_events">
        <table id="template_events_table">
            <thead>
            <tr>
                <th>Event Name</th>
                <th>Event Start</th>
                <th>Event End</th>
                <!--th>Remove</th-->
            </tr>
            </thead>
            <tbody id="template_events_body">
            <?= $this->load->view('teesheets/template_events');?>
            </tbody>
        </table>
    </div>
    <div id="template_apply_button_holder">
        <input type="submit" value="Apply" name="template_apply"/>
    </div>
</form>
<style>
    #template_name_holder, #template_date_holder, #template_tee_sheet_holder {
        padding:10px;
    }
    #template_name_holder input[name=template_name] {
        font-size:16px;
        margin-left:10px;
    }
    #template_events_table td, #template_events_table th {
        text-align:center;
        padding:5px;
    }
    #template_events_table th {
        font-size:20px;
    }
    #template_apply_button_holder {
        padding:10px;
        text-align:center;
    }
    #template_apply_button_holder input[name=template_apply] {
        width:150px;
        height:30px;
        background-color: #287db9;
        color: white;
    }
    #template_apply_button_holder input[name=template_apply]:hover {
        background-color: #1672b9;
    }
</style>
<script>
    var submitting = false;

    $(document).ready(function() {
        $('#apply_template_form').validate({
            submitHandler: function (form) {
                if (submitting) return;
                submitting = true;
                $(form).mask('<?php echo lang('common_wait'); ?>');
                //console.log( 'about to submit form');
                $(form).ajaxSubmit({
                    success: function (response) {
                        if (!response.success) {
                            set_feedback('Sorry, the template could not be applied. Please try again.', 'error_message',false);
                        }
                        else {
                            // Update tee sheet
                            Calendar_actions.update_teesheet(response.json_events);
                        }
                        // Close window
                        $.colorbox2.close();
                    },
                    dataType: 'json'
                });
            },
            errorLabelContainer: '#error_message_box',
            wrapper: 'li'
        });
    });
</script>