<?php $this->load->view("partial/header_new"); ?>
<style>
td.rightmost a.edit-person {
    padding: 0px 10px 0px 10px;
}
a.billing-link {
    color: #4885B1 !important;
}
a.billing-link:hover {
    color: #68c0ff !important;
}
</style>
    <script type='text/javascript' src="js/customer.js"></script>
    <script type="text/javascript">
        function reload_customer(html)
        {
            $.colorbox2.close();
            $('#cc_dropdown').replaceWith(""+html+"");
        }
        function reload_customer_credit_cards(credit_card_id, person_id)
        {
            credit_card_id = credit_card_id == undefined ? 0 : credit_card_id;
            $.colorbox2({'href':'index.php/customers/manage_credit_cards/'+person_id, 'width':700});
        }

        function init_billing_links(){
            $('a.billing-link.invoices').colorbox();
            $('a.billing-link.history').colorbox2({'title':"Customer History"});
            $('a.billing-link.credit-cards').colorbox2({'title':'Manage Credit Cards', width:600});
        }

        $(document).ready(function()
        {
            <?php if($controller_name == 'customers' && $_GET['in_iframe'] == 1){ ?>
            init_billing_links();
            $('#import-customers').live('click', function(e){

                var import_job = _.find(window.parent.App.data.data_import_jobs.models, function(job){
                    if(job.get('type') == 'customers' && job.get('status') != 'pending'){
                        return job;
                    }
                });

                if(!import_job){
                    import_job = new window.parent.DataImportJob({
                        'type': 'customers'
                    });
                }

                var modal = new window.parent.DataImportView({model: import_job});
                modal.show();
                return false;
            });

            $('a.edit-person').live('click', function(e){
                e.preventDefault();

                var person_id = $(this).attr('data-person-id');
                var customer = new window.parent.Customer({
                    person_id: person_id
                });
                var modal = new window.parent.CustomerFormView({model: customer});
                modal.show();
                window.parent.$('.modal-content').loadMask();

                customer.fetch({
                    silent: true,
                    data: {
                        include_rainchecks: true
                    },
                    success: function(model){
                        modal.render();

                        // Delay the listener because a sync event is triggered
                        // AFTER this success callback (which we don't care about)
                        setTimeout(function(){
                            customer.on('sync', function(){
                                update_row(person_id, 'customers/get_row/true')
                            });
                        }, 10);
                    }
                });

                return false;
            });

            $('a.new').live('click', function(e){
                var customer = new window.parent.Customer();
                var modal = new window.parent.CustomerFormView({model: customer});
                modal.show();

                customer.on('sync', function(model){
                    // Refresh customer table
                    do_search(true, function(){
                        highlight_row(model.get('person_id'));
                        set_feedback('Customer created', 'success_message', false);
                    });
                });

                return false;
            });


            // Use backbone customer edit window
//            $('a.edit-person').live('click', function(e){
//                e.preventDefault();
//                var person_id = $(this).attr('data-person-id');
//                var customer = new window.parent.Customer({
//                    person_id: person_id
//                });
//
//                var customerEditModal = new window.parent.EditCustomerView({model: customer});
//                customerEditModal.show();
//                window.parent.$('.modal-content').loadMask();
//
//                customer.fetch({
//                    silent: true,
//                    data: {
//                        include_rainchecks: true,
//                        include_marketing: true
//                    },
//                    success: function(model){
//                        customerEditModal.render();
//
//                        // Delay the listener because a sync event is triggered
//                        // AFTER this success callback (which we don't care about)
//                        setTimeout(function(){
//                            customer.on('sync', function(){
//                                update_row(person_id, 'customers/get_row/true')
//                            });
//                        }, 10);
//                    }
//                });
//
//                return false;
//            });

//            $('a.new').live('click', function(e){
//                var customer = new window.parent.Customer();
//
//                var customerEditModal = new window.parent.EditCustomerView({model: customer});
//                customerEditModal.show();
//
//                customer.on('sync', function(model){
//                    // Refresh customer table
//                    do_search(true, function(){
//                        highlight_row(model.get('person_id'));
//                        set_feedback('Customer created', 'success_message', false);
//                    });
//                });
//                return false;
//            });
            <?php } ?>

            var base_export_url = $('#excel_export_button').attr('href');
            $('#excel_export_button').click(function(e){
                //e.preventDefault();
                var group_id = $('#customer_group').val();
                $(this).attr('href', base_export_url+'/'+group_id);
            });
            $('#merge_customers_button').colorbox({'width':1000, title:'Merge Customers'});
            $('#customer_group').msDropDown(/*{blur:function(){changeTeeSheet()},click:alert('stuff')}*/)
            $('#customer_pass').msDropDown(/*{blur:function(){changeTeeSheet()},click:alert('stuff')}*/)
            $('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
            $('.colbox').colorbox({'maxHeight':700, 'width':<?php echo $form_width?>});
            init_table_sorting();
            enable_select_all();
            enable_checkboxes();
            enable_row_selection();
            enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
            enable_email('<?php echo site_url("$controller_name/mailto")?>');
            <?php if ($controller_name == 'employees') { ?>
            enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
            <?php } else { ?>
            $('#delete').click(function(e){
                e.preventDefault();
                var selected_values = get_selected_values();
                console.dir(selected_values);
                //return;
                if($("#sortable_table tbody :checkbox:checked").length >0)
                {
                    $.post('index.php/customers/delete',
                        {
                            'ids[]':selected_values,
                            'confirm_message':'<?php echo lang($controller_name."_confirm_delete")?>'
                        },
                        function(data){
                            $.colorbox({
                                'html':data,
                                'width':500,
                                onComplete:function(){
                                    var checkboxes = $('input[name=customer_ids[]]');
                                    console.log('checkboxes');
                                    console.dir(checkboxes);
                                    checkboxes.click(function(){
                                        var id = $(this).attr('id').replace('delete_customer_', '');
                                        var checked = $(this).attr('checked');
                                        if (checked)
                                            $('#person_'+id).attr('checked','checked');
                                        else
                                            $('#person_'+id).removeAttr('checked');
                                    });
                                    var submitting = false;
                                    $('#delete_customers_form').validate({
                                        submitHandler:function(form)
                                        {
                                            if (submitting) return;
                                            submitting = true;
                                            $(form).mask("<?php echo lang('common_wait'); ?>");
                                            $(form).ajaxSubmit({
                                                success:function(response)
                                                {
                                                    if(response.success)
                                                    {
                                                        selected_rows = get_selected_rows();
                                                        set_feedback(response.message,'success_message',false);

                                                        $(selected_rows).each(function(index, dom)
                                                        {
                                                            $(this).find("td").animate({backgroundColor:"#FF0000"},1200,"linear")
                                                                .end().animate({opacity:0},1200,"linear",function()
                                                                {
                                                                    if(!$('#show_deleted_customers').is(":checked")) {

                                                                        $(this).remove();
                                                                        //Re-init sortable table as we removed a row
                                                                        update_sortable_table();
                                                                    }else{
                                                                        update_row($(this).find("input").attr("value"), 'customers/get_row/true')

                                                                    }


                                                                });
                                                        });
                                                    }
                                                    else
                                                    {
                                                        set_feedback(response.message,'error_message',true);
                                                    }
                                                    $.colorbox.close();
                                                    submitting = false;
                                                },
                                                dataType:'json'
                                            });

                                        },
                                        errorLabelContainer: "#error_message_box",
                                        wrapper: "li",
                                        rules:
                                        {
                                        },
                                        messages:
                                        {
                                        }
                                    });
                                }
                            });
                        }
                    )
                }
                else
                {
                    alert('<?php echo lang($controller_name."_none_selected")?>');
                }
            });
            <?php } ?>
            enable_bulk_edit('<?php echo lang($controller_name."_none_selected")?>');
            enable_cleanup('<?php echo lang("customers_confirm_cleanup")?>');
            $('#customer_group, #customer_pass').change(function(){
                $('#offset').val(0);
                $('#search_form').submit();
            });
            $("#course_name").autocomplete({
                source: 'index.php/courses/course_search',
                delay: <?=$this->config->item("search_delay") ?>,
                autoFocus: false,
                minLength: 0,
                select: function( event, ui )
                {
                    event.preventDefault();
                    $('#offset').val(0);
                    $("#course_name").val(ui.item.label);
                    $('#course_id').val(ui.item.value);
                    $('#search_form').submit();
                    //	do_search(true);
                },
                focus: function(event, ui) {
                    event.preventDefault();
                    //$("#teetime_title").val(ui.item.label);
                }
            });

            $('#load_member_billing').click(function(e){
                e.preventDefault();
                customer.billing.load_color_box(-1,'');
            })
        });

        function init_table_sorting()
        {
            //Only init if there is more than one row
            if($('.tablesorter tbody tr').length >1)
            {
                $("#sortable_table").tablesorter(
                    {
                        sortList: [[1,0]],
                        headers:
                        {
                            0: { sorter: false},
                            <?php if ($controller_name == 'employees' && $this->permissions->is_super_admin()) {?>
                            6: { sorter: false}
                            <?php }
                            else if ($controller_name == 'employees') { ?>
                            5: { sorter: false}
                            <?php }
                            else if ($controller_name == 'customers') { ?>
                            7: { sorter: false}
                            <?php } ?>


                        }

                    });
            }
        }

        function post_person_form_submit(response)
        {
            if(!response.success)
            {
                set_feedback(response.message,'error_message',true);
            }
            else
            {
                //This is an update, just update one row
                if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
                {
                    update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
                    set_feedback(response.message,'success_message',false);

                }
                else //refresh entire table
                {
                    do_search(true,function()
                    {
                        //highlight new row
                        highlight_row(response.person_id);
                        set_feedback(response.message,'success_message',false);
                    });
                }
            }
        }
        function post_bulk_form_submit(response)
        {
            if(!response.success)
            {
                set_feedback(response.message,'error_message',true);
            }
            else
            {
                set_feedback(response.message,'success_message',false);
                setTimeout(function(){window.location.reload();}, 2500);
            }
        }

        $(document).ready(function(){
            $("#show_deleted_customers").off("change").on("change", function() {
                enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
                do_search();
            })
        });
    </script>
    <style>
        #table_top #course_name {
            border:1px inset #ccc;
            background:white;
            margin: 3px 0px 0px 3px;
        }
        #sortable_table .rightmost .colbox, #sortable_table .override_login a {
            padding:0px;
            width:50px;
            height:100%;
            line-height:30px;
            display:block;
            font-size:12px;
        }
        #item_table td.override_login {
            padding:0px;
        }
    </style>

    <table id="contents">
        <tr>
            <td id="commands">
                <div id="new_button">
                    <?php
                    $new_button_class = 'colbox none new';
                    if($controller_name == 'customers' && $_GET['in_iframe'] == 1){
                        $new_button_class = 'none new';
                    }

                    if ($this->permissions->is_employee() && $controller_name == 'employees')
                    {
                    }
                    else
                    {
                        echo anchor("$controller_name/view/-1/width~1100",
                            lang($controller_name.'_new'),
                            array('class'=>$new_button_class, 'title'=>lang($controller_name.'_new')));
                    }
                    if ($controller_name == 'customers')
                    {
                        echo anchor("$controller_name/bulk_edit/",
                            lang("customers_bulk_edit"),
                            array('id'=>'bulk_edit',
                                'class' => 'bulk_edit_inactive',
                                'title'=>lang('customers_edit_multiple_customers')));

                        echo anchor("customers/manage_groups/width~720",
                            lang('customers_manage_groups'),
                            array('class'=>'colbox none', 'title'=>lang('customers_manage_groups')));

                        if(isset($old_passes_on) && $old_passes_on){
                            echo anchor("customers/manage_passes/width~720",
                                lang('customers_manage_passes'),
                                array('class'=>'colbox none', 'title'=>lang('customers_manage_passes')));
                        }

                        echo anchor("",
                            lang('customers_member_billing'),
                            array('class'=>'','id'=>'load_member_billing'));

                        echo anchor("minimum_charges/index/width~960",
                            'Minimum Charges',
                            array('class'=>'colbox none','id'=>'load_minimum_charges'));
                    }
                    ?>
                    <a class="email email_inactive" href="<?php echo current_url(). '#'; ?>" id="email"><?php echo lang("common_email");?></a>
                    <?php if ($controller_name =='customers') {?>
                        <a href="#" id="import-customers" class="import-customers">Import Customers</a>
                        <!--a href="#" id="billing-schedules">Billing Schedules</a-->
                    <?php } ?>
                    <?php
                    if ($controller_name == 'customers' || $controller_name == 'employees') {
                        echo anchor("$controller_name/excel_export",
                            lang($controller_name.'_export'),
                            array('class'=>'none import', 'id'=>'excel_export_button'));
                    }
                    ?>
                    <?php
                    if ($controller_name == 'customers' && !$this->permissions->is_employee()) {
                        echo anchor("$controller_name/view_merge",
                            lang($controller_name.'_merge'),
                            array('class'=>'none import', 'id'=>'merge_customers_button'));
                    }
                    ?>
                    <?php
                    if ($this->permissions->is_employee() && $controller_name == 'employees') {}
                    else
                        echo anchor("$controller_name/delete",lang("common_delete"),array('id'=>'delete', 'class'=>'delete_inactive'));
                    ?>
                </div>
            </td>
            <td style="width:10px;"></td>
            <td id="item_table">
                <div id='table_top'>
                    <?php
                    if ($controller_name == 'customers')
                    {
                        $groups['no_group'] = '-- No Group --';
                        echo form_dropdown('customer_group', $groups, 'all');
                        if (isset($old_passes_on) && $old_passes_on) {
                            $passes['no_pass'] = '-- No Pass --';
                            echo form_dropdown('customer_pass', $passes, 'all');
                        }


                    }
                    else if ($controller_name == 'employees' && $this->permissions->is_super_admin())
                    {
                        echo form_hidden('course_id', '');
                        echo form_input(array(
                            'name'=>'course_name',
                            'id'=>'course_name',
                            'placeholder'=>'Golf Course',
                            'value'=>''
                        ));
                    }
                    ?>
                    <?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
                    <input type="text" name ='search' id='search' placeholder="Search"/>
                    <img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
                    </form>

                    <?php
                    if($this->permissions->is_admin()) {
                        $deleted_customers = [
                            'name' => "show deleted customers",
                            'id' => "show_deleted_customers",
                            "style" => "width: 30px; margin-top: 0px;",
                            'checked' => false
                        ];
                        $label = [
                            'style' => "color: white;"
                        ];
                        echo "<div class = 'show_deleted_customers' style = 'display: inline-block; width: 180px;float: right;padding-top: 13px;'>";
                        echo form_checkbox($deleted_customers, "show_deleted_customers", true);
                        echo form_label("Show Deleted Customers", null, $label);
                        echo "</div>";
                    }
                    ?>
                </div>
                <div class='fixed_top_table'>
                    <div class='header-background'></div>
                    <div id="table_holder">
                        <?php echo $manage_table; ?>
                    </div>
                </div>
                <div id="pagination">
                    <?php echo $this->pagination->create_links();?>
                </div>
            </td>
        </tr>
    </table>
    <div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>