<?php $disabled = $timeframe->default ? "disabled" : "";?>
<div class="timeframe" id="timeframe_<?=$timeframe->timeframe_id?>">
	<?php echo form_hidden("timeframe[]", $timeframe->timeframe_id) ?>
	<?php if (!$timeframe->default){?><div class="close" id="close_timeframe_<?=$timeframe->timeframe_id?>">
		<strong>X</strong>
	</div><? } ?>

	<div class="row days">
		<div style="display: block; overflow:hidden;">
			<? echo form_input(array(
				'name' 			=> "timeframe_name[{$timeframe->timeframe_id}]",
				'id'			=> "timeframe_name_{$timeframe->timeframe_id}",
				'placeholder'	=> "Timeframe Label",
				'value'			=> $timeframe->timeframe_name,
				'style'			=> "width:250px; display:block; float: left; margin: 0px 0px 5px 0px",
				$disabled=>$disabled)); ?>
			<?php if($timeframe->default == 0){ ?>
			<label class="active" style="display: block; float: left; height: 35px; line-height: 35px; margin-left: 25px;">		
				<input type="hidden" name="active[<?php echo $timeframe->timeframe_id; ?>]" value="0" />
				<input class="activate-timeframe" type="checkbox" name="active[<?php echo $timeframe->timeframe_id; ?>]" value="1" <?php if($timeframe->active == 1){ echo 'checked'; } ?> /> Active		
			</label>
			<?php } ?>
		</div>
		<label style="font-weight:bold;">Days</label>
		
		<label><? echo form_checkbox(array(
			'name'      => "monday[{$timeframe->timeframe_id}]",
			'id'        => "monday_{$timeframe->timeframe_id}",
			'value'     => '1',
			'checked'	=> $timeframe->monday,
			$disabled	=> $disabled
		));?> Mon</label>
		
		<label><? echo form_checkbox(array(
			'name'		=> "tuesday[{$timeframe->timeframe_id}]",
			'id'		=> "tuesday_{$timeframe->timeframe_id}",
			'value'		=> '1',
			'checked'	=> $timeframe->tuesday,
			$disabled	=> $disabled
		));?> Tue</label>
		
		<label><? echo form_checkbox(array(
			'name'		=> "wednesday[{$timeframe->timeframe_id}]",
			'id'		=> "wednesday_{$timeframe->timeframe_id}",
			'value'		=> '1',
			'checked'	=> $timeframe->wednesday,
			$disabled	=> $disabled
		));?> Wed</label>
		
		<label><? echo form_checkbox(array(
			'name'		=> "thursday[{$timeframe->timeframe_id}]",
			'id'		=> "thursday_{$timeframe->timeframe_id}",
			'value'		=> '1',
			'checked'	=> $timeframe->thursday,
			$disabled	=> $disabled
		));?> Thu</label>
		
		<label><? echo form_checkbox(array(
			'name'		=> "friday[{$timeframe->timeframe_id}]",
			'id'		=> "friday_{$timeframe->timeframe_id}",
			'value'		=> '1',
			'checked'	=> $timeframe->friday,
			$disabled	=> $disabled
		));?> Fri</label>
		
		<label><? echo form_checkbox(array(
			'name'		=> "saturday[{$timeframe->timeframe_id}]",
			'id'		=> "saturday_{$timeframe->timeframe_id}",
			'value'		=> '1',
			'checked'	=> $timeframe->saturday,
			$disabled	=> $disabled
		));?> Sat</label>
		
		<label><? echo form_checkbox(array(
			'name'		=> "sunday[{$timeframe->timeframe_id}]",
			'id'		=> "sunday_{$timeframe->timeframe_id}",
			'value'		=> '1',
			'checked'	=> $timeframe->sunday,
			$disabled	=> $disabled
		));?> Sun</label>
	</div>
	<div class="row">
		<label>Hours</label>
		<?=form_dropdown("start_time[{$timeframe->timeframe_id}]", time_array(), sprintf('%1$04d', $timeframe->start_time), "$disabled");?>
		-
		<?=form_dropdown("end_time[{$timeframe->timeframe_id}]", time_array(), sprintf('%1$04d', $timeframe->end_time), "$disabled");?>
	</div>
	<div class="row label_span">
		<label class="price">9 Green</label>
		<?php if(isset($price_class['cart']) && $price_class['cart'] == 1){ ?><label class="price">9 Cart</label><?php } ?>
		<label class="price">18 Green</label>	
		<?php if(isset($price_class['cart']) && $price_class['cart'] == 1){ ?><label class="price">18 Cart</label><?php } ?>
	</div>
	<div class="row prices">
		<label>Prices</label>
		<? echo form_input(array(
			'name'=>"price2[{$timeframe->timeframe_id}]",
			'id'=>"price2_{$timeframe->timeframe_id}",
			'class'=>"price_input",
			'placeholder'=>"9 Green",
			'value'=>$timeframe->price2));
		if(isset($price_class['cart']) && $price_class['cart'] == 1){
			echo form_input(array(
			'name'=>"price4[{$timeframe->timeframe_id}]",
			'id'=>"price4_{$timeframe->timeframe_id}",
			'class'=>"price_input",
			'placeholder'=>"9 Cart",
			'value'=>$timeframe->price4));
		}
		echo form_input(array(
			'name'=>"price1[{$timeframe->timeframe_id}]",
			'id'=>"price1_{$timeframe->timeframe_id}",
			'class'=>"price_input",
			'placeholder'=>"18 Green",
			'value'=>$timeframe->price1));
		if(isset($price_class['cart']) && $price_class['cart'] == 1){
			echo form_input(array(
			'name'=>"price3[{$timeframe->timeframe_id}]",
			'id'=>"price3_{$timeframe->timeframe_id}",
			'class'=>"price_input",
			'placeholder'=>"18 Cart",
			'value'=>$timeframe->price3));
		}
		if ($timeframe->price5 != 0.00 && $timeframe->price6 != 0.00)
		{
			echo form_input(array(
				'name'=>"price5[{$timeframe->timeframe_id}]",
				'id'=>"price5_{$timeframe->timeframe_id}",
				'class'=>"price_input",
				'placeholder'=>"Price 5",
				'value'=>$timeframe->price5));
			echo form_input(array(
				'name'=>"price6[{$timeframe->timeframe_id}]",
				'id'=>"price6_{$timeframe->timeframe_id}",
				'class'=>"price_input",
				'placeholder'=>"Price 6",
				'value'=>$timeframe->price6));
		}?>
		<a href="#" class="add_prices" title="Add Prices">Add Prices</a>
	</div>
</div>
<script>
$(function(){
	toggle_timeframe_status( $('#timeframe_<?=$timeframe->timeframe_id?>') );
});
</script>
