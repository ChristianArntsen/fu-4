<?php
if(!isset($notes)){
    $notes = [];
}
if (count($notes) > 0) {

	foreach($notes as $note) {
	?>
	<div class='note_result' id="note_<?=$note['note_id']?>">
		<div class='delete_note' onclick="note.remove(<?=$note['note_id']?>)"></div>
    	<?=$note['message']?>
    	<div class='author_and_date'><?=date('m/d/y g:i',strtotime($note['date'])).' - '.$note['first_name'].' '.$note['last_name']?></div>
    </div>
<?php 
	}
} else { ?>
    <div class='note_result'>
    No Notes
    </div>
<?php } ?>
<style>
	.note_result:hover .delete_note {
		opacity:1;
		transition:opacity 0.2s linear;
	}
	.note_result:hover .author_and_date {
		visibility:visible;
		max-height:50px;
		transition:all 0.2s linear 0.2s;
	}
	.delete_note {
		display:block;
		width:10px;
		height:10px;
		float:right;
		background:url(../images/pieces/x_red2.png) no-repeat;
		opacity:0;
	}
	.author_and_date {
		font-size:10px;
		text-align:right;
		max-height:0px;
		visibility:hidden;
	}
</style>
<script>
	//$('.note_result').mouseover(function(){note.hover(this)}).mouseout(function(){note.out(this)});
	var note = {
		remove: function (note_id) {
			$.ajax({
	           type: "POST",
	           url: "index.php/teesheets/delete_note/"+note_id,
	           data: '',
	           success: function(response){
	           		console.dir(response);
	           		$('#note_'+note_id).remove();
			    },
	            dataType:'json'
	         });
		// },
		// hover: function (which) {
			// // SHOW DELETE
			// $(which).find('.delete_note').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0}, 200);
			// // SHOW AUTHOR AND DATE
// 			
		// },
		// out: function (which) {
			// $(which).find('.delete_note').css({opacity: 1.0, visibility: "hidden"}).animate({opacity: 0.0}, 200);
			
		}
	}
</script>