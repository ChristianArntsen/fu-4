<ul id="error_message_box"></ul>
<?php
echo form_open('courses/save_mobile_app_info/'.$course_info->course_id,array('id'=>'mobile_app_form'));
?>
<fieldset id="mobile_app_basic_info">
    <legend>Mobile App Information for <?=$course_info->name?></legend>
    <a href="#" id="edit_tee_sheet_holes">Edit Tee Sheet Holes</a>
    <div id='basic_mobile_app_info'>
        <div class="field_row clearfix">
            <?php echo form_label('Mobile App Active:', 'mobile_app_active',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_checkbox(array(
                        'autocomplete'=>'off',
                        'name'=>'mobile_app_active',
                        'id'=>'mobile_app_active',
                        'value'=>1,
                        'checked'=>$course_info->mobile_app_active)
                );?>
            </div>
        </div>
        <div class="field_row clearfix">
            <?php echo form_label('Base Color:', 'base_color',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'base_color',
                        'id'=>'base_color',
                        'value'=>$course_info->base_color)
                );?>
            </div>
        </div>
        <div class="field_row clearfix">
            <?php echo form_label('Mobile App Icon URL:', 'mobile_app_icon_url',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'mobile_app_icon_url',
                        'id'=>'mobile_app_icon_url',
                        'value'=>$course_info->mobile_app_icon_url)
                );?>
            </div>
        </div>

        <div class="field_row clearfix">
            <?php echo form_label('Mobile Testflight Email:', 'mobile_test_flight_email',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'mobile_test_flight_email',
                        'id'=>'mobile_test_flight_email',
                        'value'=>$course_info->mobile_test_flight_email)
                );?>
            </div>
        </div>
        <div class="field_row clearfix">
            <?php echo form_label('Mobile App Short Title:', 'mobile_app_short_title',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'mobile_app_short_title',
                        'id'=>'mobile_app_short_title',
                        'value'=>$course_info->mobile_app_short_title)
                );?>
            </div>
        </div>
        <div class="field_row clearfix">
            <?php echo form_label('Mobile App Summary:', 'mobile_app_summary',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_textarea(array(
                        'autocomplete'=>'off',
                        'name'=>'mobile_app_summary',
                        'id'=>'mobile_app_summary',
                        'value'=>$course_info->mobile_app_summary)
                );?>
            </div>
        </div>
        <div class="field_row clearfix">
            <?php echo form_label('Course Summary:', 'course_summary',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_textarea(array(
                        'autocomplete'=>'off',
                        'name'=>'course_summary',
                        'id'=>'course_summary',
                        'value'=>$course_info->course_summary)
                );?>
            </div>
        </div>

        <?php foreach ($mobile_app_modules as $mobile_app_module) { ?>
        <div class="field_row clearfix">
            <?php echo form_label($mobile_app_module['label'].':', 'mobile_app_module_permissions[]',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_checkbox(array(
                        'autocomplete'=>'off',
                        'name'=>'mobile_app_module_permissions[]',
                        'id'=>'mobile_app_module_permissions_'.$mobile_app_module['mobile_app_module_id'],
                        'value'=>$mobile_app_module['mobile_app_module_id'],
                        'checked'=>in_array($mobile_app_module['mobile_app_module_id'],$mobile_app_module_permissions))
                );?>
            </div>
        </div>
        <?php } ?>
    </div>

    <?php
    echo form_submit(array(
            'name'=>'submit',
            'id'=>'submit',
            'value'=>lang('common_save'),
            'class'=>'submit_button float_right')
    );
    ?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

    //validation and submit handling
    $(document).ready(function()
    {
        jQuery_1_7_2('#base_color').spectrum({
            preferredFormat: 'rgb',
            allowEmpty: true,
            clickoutFiresChange: true
        });

        $('#edit_tee_sheet_holes').colorbox2({'href':'index.php/courses/view_tee_sheet_holes/<?=$course_info->course_id?>', 'width':1050, 'title':'Tee Sheet Holes'});

        $('#mobile_app_form').validate({
            submitHandler:function(form)
            {
                $(form).ajaxSubmit({
                    success:function(response)
                    {
                        $.colorbox.close();
                        //post_item_form_submit(response);
                    },
                    dataType:'json'
                });

            },
            errorLabelContainer: "#error_message_box",
            wrapper: "li",
            rules:
            {},
            messages:
            {}
        });
    });
</script>

