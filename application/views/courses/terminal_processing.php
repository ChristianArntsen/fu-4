<?php
echo form_open('courses/save_terminal_processing',array('id'=>'terminal_processing_form'));
?>
<fieldset id="item_basic_info">
    <legend>Admin Course Information</legend>
    <div id='basic_terminal_processing_info'>
<?php
echo form_hidden('course_id',$course_id);
foreach( $terminals as $terminal ) {

    echo "<div>
        <div class='field_row clearfix'>
            <b>
            ".form_label($terminal->label, 'terminal_label',array('class'=>'wide'))."
            </b>
            <div class='form_field'>
                ".form_hidden('terminal_id[]',$terminal->terminal_id)."
            </div>
        </div>
        <div style='display:none'>
            <div class='field_row clearfix'>
                ".form_label(lang('courses_mercury_id').':', 'fake_mercury_id',array('class'=>'wide'))."
                <div class='form_field'>
                    ".form_input(array(
                            'autocomplete'=>'off',
                            'name'=>'fake_mercury_id',
                            'id'=>'fake_mercury_id',
                            'value'=>'')
                    )."
                </div>
            </div>
            <div class='field_row clearfix'>
                ".form_label(lang('courses_mercury_password').':', 'fake_mercury_password',array('class'=>'wide'))."
                <div class='form_field'>
                    ".form_password(array(
                            'autocomplete'=>'off',
                            'name'=>'fake_mercury_password',
                            'id'=>'fake_mercury_password',
                            'value'=>'')
                    )."
                </div>
            </div>
        </div>
        <div class='field_row clearfix mercury_box'>
            ".form_label(lang('courses_mercury_id').':', 'mercury_id',array('class'=>'wide'))."
            <div class='form_field'>
                ".form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'mercury_id[]',
                        'id'=>"mercury_id_{$terminal->terminal_id}",
                        'value'=>$terminal->mercury_id)
                )."
            </div>
        </div>
        <div class='field_row clearfix mercury_box'>
            ".form_label(lang('courses_mercury_password').':', 'mercury_password',array('class'=>'wide'))."
            <div class='form_field'>
                ".form_password(array(
                        'autocomplete'=>'off',
                        'name'=>'mercury_password[]',
                        'id'=>"mercury_password_{$terminal->terminal_id}",
                        'value'=>$terminal->mercury_password)
                )."
            </div>
        </div>
        <div class='field_row clearfix ets_box'>
            ".form_label(lang('courses_ets_key').':', 'ets_key',array('class'=>'wide'))."
            <div class='form_field'>
                ".form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'ets_key[]',
                        'id'=>"ets_key_{$terminal->terminal_id}",
                        'value'=>$terminal->ets_key)
                )."
            </div>
        </div>
        <div class='field_row clearfix element_box'>
            ".form_label('Element Account ID:', 'element_account_id', array('class'=>'wide'))."
            <div class='form_field'>
                ".form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'element_account_id[]',
                        'id'=>"element_account_id_{$terminal->terminal_id}",
                        'value'=>$terminal->element_account_id)
                )."
            </div>
        </div>
        <div class='field_row clearfix element_box'>
            ".form_label('Element Account Token:', 'element_account_token', array('class'=>'wide'))."
            <div class='form_field'>
                ".form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'element_account_token[]',
                        'id'=>"element_account_id_{$terminal->terminal_id}",
                        'value'=>$terminal->element_account_token)
                )."
            </div>
        </div>
        <div class='field_row clearfix element_box'>
            ".form_label('Element Application ID:', 'element_application_id', array('class'=>'wide'))."
            <div class='form_field'>
                ".form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'element_application_id[]',
                        'id'=>"element_application_id_{$terminal->terminal_id}",
                        'value'=>$terminal->element_application_id)
                )."
            </div>
        </div>
        <div class='field_row clearfix element_box'>
            ".form_label('Element Acceptor ID:', 'element_acceptor_id', array('class'=>'wide'))."
            <div class='form_field'>
                ".form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'element_acceptor_id[]',
                        'id'=>"element_acceptor_id_{$terminal->terminal_id}",
                        'value'=>$terminal->element_acceptor_id)
                )."
            </div>
        </div>
        <div class='field_row clearfix apriva_box'>
            ".form_label('Apriva Username:', 'apriva_username', array('class'=>'wide'))."
            <div class='form_field'>
                ".form_input(array(
                'autocomplete'=>'off',
                'name'=>'apriva_username[]',
                'id'=>"apriva_username_{$terminal->terminal_id}",
                'value'=>$terminal->apriva_username)
        )."
            </div>
        </div><div class='field_row clearfix apriva_box'>
            ".form_label('Apriva Password:', 'apriva_password', array('class'=>'wide'))."
            <div class='form_field'>
                ".form_input(array(
                'autocomplete'=>'off',
                'name'=>'apriva_password[]',
                'id'=>"apriva_password_{$terminal->terminal_id}",
                'value'=>$terminal->apriva_password)
        )."
            </div>
        </div><div class='field_row clearfix apriva_box'>
            ".form_label('Apriva Product:', 'apriva_product', array('class'=>'wide'))."
            <div class='form_field'>
                ".form_input(array(
                'autocomplete'=>'off',
                'name'=>'apriva_product[]',
                'id'=>"apriva_product_{$terminal->terminal_id}",
                'value'=>$terminal->apriva_product)
        )."
            </div>
        </div> <div class='field_row clearfix apriva_box'>
            ".form_label('Apriva Key:', 'apriva_key', array('class'=>'wide'))."
            <div class='form_field'>
                ".form_input(array(
                'autocomplete'=>'off',
                'name'=>'apriva_key[]',
                'id'=>"apriva_key_{$terminal->terminal_id}",
                'value'=>$terminal->apriva_key)
        )."
            </div>
        </div>
    </div>";
}

echo form_submit(array(
        'name'=>'submit',
        'id'=>'submit',
        'value'=>lang('common_save'),
        'class'=>'submit_button float_right')
);
?>
        </div>
    </fieldset>
</form>
<style>
    #terminal_processing_form fieldset div.field_row {
        font-size:14px;
    }
</style>
<script>
    $(document).ready(function() {
        // Hide credential types that are not being used
        if ($('#element_account_id').val() == '') {
            $('.element_box').hide();
        }
        if ($('#ets_key').val() == '') {
            $('.ets_box').hide();
        }
        if ($('#mercury_id').val() == '') {
            $('.mercury_box').hide();
        }
        if ($('#apriva_username').val() == '') {
            $('.apriva_box').hide();
        }

        var submitting = false;
        $('#terminal_processing_form').validate({
            submitHandler:function(form)
            {
                if (submitting) return;
                submitting = true;
                $(form).mask("<?php echo lang('common_wait'); ?>");
                $(form).ajaxSubmit({
                    success:function(response)
                    {
                        if (response.success){
                            $.colorbox2.close();
                        } else {
                            set_feedback("Save unsuccessful",'error_message',false);
                            $(form).unmask();
                        }

                    },
                    dataType:'json'
                });
            },
            errorLabelContainer: "#error_message_box",
            wrapper: "li",
            rules:{},
            messages:{}
        });
    });
</script>
