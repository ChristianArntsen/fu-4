<style>
	#cboxContent h1 {
		font-size:16px;
	}
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
        color: #39C8FC;
    }
</style>
<ul id="error_message_box"></ul>
<?php
echo form_open('courses/save/'.$item_info->course_id,array('id'=>'item_form'));
?>
<fieldset id="item_basic_info">
<legend>Admin Course Information</legend>
<div id='basic_course_info' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_active').':', 'active_course',array('class'=>'wide')); ?>
		<div class='form_field'>
	    <div  id="active_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->active_course == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'active_course',
	            'id'=>'active_course0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'active_course',
	            'id'=>'active_course1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="active_course0">No</label><?php
	            echo form_radio($data1);?><label for="active_course1">Yes</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_demo').':', 'demo',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="demo_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->demo == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'demo',
	            'id'=>'demo0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'demo',
	            'id'=>'demo1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="demo0">No</label><?php
	            echo form_radio($data1);?><label for="demo1">Yes</label><?php
	        ?>
	            </div>
		</div>
	</div>
    <div class="field_row clearfix">
        <?php echo form_label(lang('courses_clean_out_nightly').':', 'clean_out_nightly',array('class'=>'wide')); ?>
        <div class='form_field'>
            <div  id="clean_out_nightly_radio">
                <?php
                $zero = false;
                $one = false;
                if ($item_info->clean_out_nightly == 0)
                    $zero = true;
                else
                    $one = true;
                $data0 = array(
                    'name'=>'clean_out_nightly',
                    'id'=>'clean_out_nightly0',
                    'value'=>0,
                    'checked'=>$zero
                );
                $data1 = array(
                    'name'=>'clean_out_nightly',
                    'id'=>'clean_out_nightly1',
                    'value'=>1,
                    'checked'=>$one
                );
                echo form_radio($data0);?><label for="clean_out_nightly0">No</label><?php
                echo form_radio($data1);?><label for="clean_out_nightly1">Yes</label><?php
                ?>
            </div>
        </div>
    </div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_name').':', 'name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'name',
			'id'=>'name',
			'value'=>$item_info->name)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('config_address').':', 'address',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'address',
			'id'=>'address',
			'value'=>$item_info->address)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('common_city').':', 'city',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'city',
			'id'=>'city',
			'value'=>$item_info->city)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('common_state').':', 'state',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'state',
			'id'=>'state',
			'value'=>$item_info->state)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_zip').':', 'zip',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_input(array(
					'autocomplete'=>'off',
					'name'=>'zip',
					'id'=>'zip',
					'value'=>$item_info->zip)
			);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_latitude').':', 'zip',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_input(array(
					'autocomplete'=>'off',
					'name'=>'latitude_centroid',
					'id'=>'latitude_centroid',
					'value'=>$item_info->latitude_centroid)
			);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_longitude').':', 'zip',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_input(array(
					'autocomplete'=>'off',
					'name'=>'longitude_centroid',
					'id'=>'longitude_centroid',
					'value'=>$item_info->longitude_centroid)
			);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('common_phone').':', 'phone',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'phone',
			'id'=>'phone',
			'value'=>$item_info->phone)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('common_email').':', 'email',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'email',
			'id'=>'email',
			'value'=>$item_info->email)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('config_website').':', 'website',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'website',
			'id'=>'website',
			'value'=>$item_info->website)
		);?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#basic_course_info').expandable({title:'Basic Course Info:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>

<div id='booking_info' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_foreup_discount_percent').':', 'foreup_discount_percent',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'foreup_discount_percent',
			'id'=>'foreup_discount_percent',
			'value'=>$item_info->foreup_discount_percent)
		);?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#booking_info').expandable({title:'Online Booking Info:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<div id='mercury_info' style='display:none'>
	<div style='display:none'>
		<div class="field_row clearfix">
		<?php echo form_label(lang('courses_mercury_id').':', 'fake_mercury_id',array('class'=>'wide')); ?>
			<div class='form_field'>
			<?php echo form_input(array(
				'autocomplete'=>'off',
				'name'=>'fake_mercury_id',
				'id'=>'fake_mercury_id',
				'value'=>'')
			);?>
			</div>
		</div>
		<div class="field_row clearfix">
		<?php echo form_label(lang('courses_mercury_password').':', 'fake_mercury_password',array('class'=>'wide')); ?>
			<div class='form_field'>
			<?php echo form_password(array(
				'autocomplete'=>'off',
				'name'=>'fake_mercury_password',
				'id'=>'fake_mercury_password',
				'value'=>'')
			);?>
			</div>
		</div>
	</div>
    <div class="field_row clearfix" style='display:none'>
        <?php echo form_label(lang('employees_username').':<span class="required">*</span>', 'username',array('class'=>'')); ?>
        <div class='form_field'>
            <?php echo form_input(array(
                'autocomplete'=>'off',
                'name'=>'username_fake',
                'size'=>'30',
                'id'=>'username_fake',
                'value'=>$person_info->username));?>
        </div>
    </div>
    <div class="field_row clearfix" style='display:none'>
        <?php echo form_label(lang('employees_password').':'.$password_label_attributes, 'password',array()); ?>
        <div class='form_field'>
            <?php echo form_password(array(
                'autocomplete'=>'off',
                'size'=>'30',
                'name'=>'password_fake',
                'id'=>'password_fake'
            ));?>
        </div>
    </div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_mercury_id').':', 'mercury_id',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'mercury_id',
			'id'=>'mercury_id',
			'value'=>$item_info->mercury_id)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_mercury_password').':', 'mercury_password',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_password(array(
			'autocomplete'=>'off',
			'name'=>'mercury_password',
			'id'=>'mercury_password',
			'value'=>$item_info->mercury_password)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('courses_use_mercury_emv').':', 'use_mercury_emv',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_checkbox('use_mercury_emv', '1', $item_info->use_mercury_emv);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_ets_key').':', 'ets_key',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'ets_key',
			'id'=>'ets_key',
			'value'=>$item_info->ets_key)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Element Account ID:', 'element_account_id', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'element_account_id',
			'id'=>'element_account_id',
			'value'=>$item_info->element_account_id)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Element Account Token:', 'element_account_token', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'element_account_token',
			'id'=>'element_account_id',
			'value'=>$item_info->element_account_token)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Element Application ID:', 'element_application_id', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'element_application_id',
			'id'=>'element_application_id',
			'value'=>$item_info->element_application_id)
		);?>
		</div>
	</div>
    <div class="field_row clearfix">
        <?php echo form_label('Element Acceptor ID:', 'element_acceptor_id', array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array(
                    'autocomplete'=>'off',
                    'name'=>'element_acceptor_id',
                    'id'=>'element_acceptor_id',
                    'value'=>$item_info->element_acceptor_id)
            );?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Apriva Username:', 'apriva_username', array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array(
                    'autocomplete'=>'off',
                    'name'=>'apriva_username',
                    'id'=>'apriva_username',
                    'value'=>$item_info->apriva_username)
            );?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Apriva Password:', 'apriva_password', array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array(
                    'autocomplete'=>'off',
                    'name'=>'apriva_password',
                    'id'=>'apriva_password',
                    'value'=>$item_info->apriva_password)
            );?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Apriva Product:', 'apriva_product', array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array(
                    'autocomplete'=>'off',
                    'name'=>'apriva_product',
                    'id'=>'apriva_product',
                    'value'=>$item_info->apriva_product)
            );?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Apriva Key:', 'apriva_key', array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array(
                    'autocomplete'=>'off',
                    'name'=>'apriva_key',
                    'id'=>'apriva_key',
                    'value'=>$item_info->apriva_key)
            );?>
        </div>
    </div>


    <div style="text-align:center; padding:10px;">
		<a id="edit_terminal_settings" href="#" style="color:white;">Edit Terminal Settings</a>
	</div>
</div>
<script type='text/javascript'>
	$('#mercury_info').expandable({title:'Credit Card Processing Info:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<!--div id='payment_info' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_teetimes_per_day').':', 'teetimes_per_day',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'teetimes_per_day',
			'id'=>'teetimes_per_day',
			'value'=>$item_info->teetimes_per_day)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_teetimes_per_week').':', 'teetimes_per_week',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_password(array(
			'autocomplete'=>'off',
			'name'=>'teetimes_per_week',
			'id'=>'teetimes_per_week',
			'value'=>$item_info->teetimes_per_week)
		);?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#payment_info').expandable({title:'Payment Info:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script-->
<div id='course_simulator' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_simulator').':', 'simulator',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="simulator_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->simulator == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'simulator',
	            'id'=>'simulator0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'simulator',
	            'id'=>'simulator1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="simulator0">No</label><?php
	            echo form_radio($data1);?><label for="simulator1">Yes</label><?php
	        ?>
	            </div>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#course_simulator').expandable({title:'Course Simulator:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<div id='course_groups' style='display:none'>
	<?php
	$first_label = true;
	foreach($groups as $group)
	{
	?>
	<div class="field_row clearfix">
	<?php echo form_label(($first_label)?lang('customers_groups').':':'', 'groups'); ?>
		<div class='form_field'>
			<?	echo form_checkbox('groups[]', $group['group_id'], ($group['is_member']) ? true:FALSE).' '.$group['label'].' <span style="color:#ccc; font-size:smaller;">('.$group['type'].')</span>';?>
		</div>
	</div>
	<?php
	$first_label = false;
	} ?>
</div>
<div id='course_managed_groups' style='display:none'>
	<?php
	$first_label = true;
	foreach($managed_groups as $group)
	{
	?>
	<div class="field_row clearfix">
	<?php echo form_label(($first_label)?lang('customers_groups').':':'', 'managed_groups'); ?>
		<div class='form_field'>
			<?	echo form_checkbox('managed_groups[]', $group['group_id'], ($group['is_management']) ? true:FALSE).' '.$group['label'].' <span style="color:#ccc; font-size:smaller;">('.$group['type'].')</span>';?>
		</div>
	</div>
	<?php
	$first_label = false;
	} ?>
</div>
<script type='text/javascript'>
	$('#course_groups').expandable({title:'Course Groups:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
	$('#course_managed_groups').expandable({title:'Course Managed Groups:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<div id='course_permissions' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_teesheets').':', 'teesheets',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="teesheets_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->teesheets == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'teesheets',
	            'id'=>'teesheets0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'teesheets',
	            'id'=>'teesheets1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="teesheets0">Off</label><?php
	            echo form_radio($data1);?><label for="teesheets1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_reservations').':', 'reservations',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="reservations_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->reservations == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'reservations',
	            'id'=>'reservations0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'reservations',
	            'id'=>'reservations1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="reservations0">Off</label><?php
	            echo form_radio($data1);?><label for="reservations1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_schedules').':', 'schedules',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="schedules_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->schedules == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'schedules',
	            'id'=>'schedules0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'schedules',
	            'id'=>'schedules1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="schedules0">Off</label><?php
	            echo form_radio($data1);?><label for="schedules1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_sales').':', 'sales',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="sales_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->sales == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'sales',
	            'id'=>'sales0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'sales',
	            'id'=>'sales1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="sales0">Off</label><?php
	            echo form_radio($data1);?><label for="sales1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_food_and_beverage').':', 'food_and_beverage',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="food_and_beverage_radio">    
		<?php 
	        $zero = false;
	        $one = false;
	        if ($item_info->food_and_beverage == 0)
	            $zero = true;
	        else 
	            $one = true;
	        $data0 = array(
	            'name'=>'food_and_beverage',
	            'id'=>'food_and_beverage0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'food_and_beverage',
	            'id'=>'food_and_beverage1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="food_and_beverage0">Off</label><?php
	            echo form_radio($data1);?><label for="food_and_beverage1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_sales_v2').':', 'sales_v2',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="sales_v2_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->sales_v2 == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'sales_v2',
	            'id'=>'sales0_v2',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'sales_v2',
	            'id'=>'sales1_v2',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="sales0_v2">Off</label><?php
	            echo form_radio($data1);?><label for="sales1_v2">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_food_and_beverage_v2').':', 'food_and_beverage_v2',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="food_and_beverage_v2_radio">    
		<?php 
	        $zero = false;
	        $one = false;
	        if ($item_info->food_and_beverage_v2 == 0)
	            $zero = true;
	        else 
	            $one = true;
	        $data0 = array(
	            'name'=>'food_and_beverage_v2',
	            'id'=>'food_and_beverage0_v2',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'food_and_beverage_v2',
	            'id'=>'food_and_beverage1_v2',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="food_and_beverage0_v2">Off</label><?php
	            echo form_radio($data1);?><label for="food_and_beverage1_v2">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_items').':', 'items',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="items_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->items == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'items',
	            'id'=>'items0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'items',
	            'id'=>'items1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="items0">Off</label><?php
	            echo form_radio($data1);?><label for="items1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_item_kits').':', 'item_kits',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="item_kits_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->item_kits == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'item_kits',
	            'id'=>'item_kits0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'item_kits',
	            'id'=>'item_kits1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="item_kits0">Off</label><?php
	            echo form_radio($data1);?><label for="item_kits1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_employees').':', 'employees',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="employees_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->employees == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'employees',
	            'id'=>'employees0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'employees',
	            'id'=>'employees1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="employees0">Off</label><?php
	            echo form_radio($data1);?><label for="employees1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_customers').':', 'customers',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="customers_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->customers == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'customers',
	            'id'=>'customers0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'customers',
	            'id'=>'customers1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="customers0">Off</label><?php
	            echo form_radio($data1);?><label for="customers1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_invoices').':', 'invoices',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="invoices_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->invoices == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'invoices',
	            'id'=>'invoices0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'invoices',
	            'id'=>'invoices1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="invoices0">Off</label><?php
	            echo form_radio($data1);?><label for="invoices1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_reports').':', 'reports',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="reports_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->reports == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'reports',
	            'id'=>'reports0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'reports',
	            'id'=>'reports1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="reports0">Off</label><?php
	            echo form_radio($data1);?><label for="reports1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_suppliers').':', 'suppliers',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="suppliers_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->suppliers == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'suppliers',
	            'id'=>'suppliers0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'suppliers',
	            'id'=>'suppliers1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="suppliers0">Off</label><?php
	            echo form_radio($data1);?><label for="suppliers1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_receivings').':', 'receivings',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="receivings_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->receivings == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'receivings',
	            'id'=>'receivings0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'receivings',
	            'id'=>'receivings1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="receivings0">Off</label><?php
	            echo form_radio($data1);?><label for="receivings1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_config').':', 'config',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="config_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->config == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'config',
	            'id'=>'config0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'config',
	            'id'=>'config1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="config0">Off</label><?php
	            echo form_radio($data1);?><label for="config1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_giftcards').':', 'giftcards',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="giftcards_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->giftcards == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'giftcards',
	            'id'=>'giftcards0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'giftcards',
	            'id'=>'giftcards1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="giftcards0">Off</label><?php
	            echo form_radio($data1);?><label for="giftcards1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_passes').':', 'passes',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="passes_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->passes == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'passes',
	            'id'=>'passes0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'passes',
	            'id'=>'passes1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="passes0">Off</label><?php
	            echo form_radio($data1);?><label for="passes1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_marketing_campaigns').':', 'marketing_campaigns',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="marketing_campaigns_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->marketing_campaigns == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'marketing_campaigns',
	            'id'=>'marketing_campaigns0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'marketing_campaigns',
	            'id'=>'marketing_campaigns1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="marketing_campaigns0">Off</label><?php
	            echo form_radio($data1);?><label for="marketing_campaigns1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_tournaments').':', 'tournaments',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="tournaments_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->tournaments == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'tournaments',
	            'id'=>'tournaments0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'tournaments',
	            'id'=>'tournaments1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="tournaments0">Off</label><?php
	            echo form_radio($data1);?><label for="tournaments1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
    <div class="field_row clearfix">
        <?php echo form_label(lang('module_promotions').':', 'promotions',array('class'=>'wide')); ?>
        <div class='form_field'>
            <div  id="promotions_radio">
                <?php
                $zero = false;
                $one = false;
                if ($item_info->promotions == 0)
                    $zero = true;
                else
                    $one = true;
                $data0 = array(
                    'name'=>'promotions',
                    'id'=>'promotions0',
                    'value'=>0,
                    'checked'=>$zero
                );
                $data1 = array(
                    'name'=>'promotions',
                    'id'=>'promotions1',
                    'value'=>1,
                    'checked'=>$one
                );
                echo form_radio($data0);?><label for="promotions0">Off</label><?php
                echo form_radio($data1);?><label for="promotions1">On</label><?php
                ?>
            </div>
        </div>
    </div>
    <div class="field_row clearfix">
	<?php echo form_label(lang('module_quickbooks').':', 'quickbooks',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="quickbooks_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->quickbooks == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'quickbooks',
	            'id'=>'quickbooks0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'quickbooks',
	            'id'=>'quickbooks1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="quickbooks0">Off</label><?php
	            echo form_radio($data1);?><label for="quickbooks1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#course_permissions').expandable({title:'Course Permissions:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<div id='course_pricing' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label('Seasonal Pricing', 'pricing',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div id="pricing_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->seasonal_pricing == 0)
	            $zero = true;
	        else
	            $one = true;
	        
	        $data0 = array(
	            'name'=>'seasonal_pricing',
	            'id'=>'seasonal_pricing_off',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'seasonal_pricing',
	            'id'=>'seasonal_pricing_on',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="seasonal_pricing_off">Off</label><?php
	            echo form_radio($data1);?><label for="seasonal_pricing_on">On</label><?php
	        ?>
	            </div>
		</div>
	</div>	
</div>
<script type='text/javascript'>
	$('#course_pricing').expandable({title:'Course Pricing:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<div id='erange_credentials' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('config_erange_id').':', 'erange_id',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'erange_id',
			'id'=>'erange_id',
			'value'=>$item_info->erange_id)
		);?>
		</div>
	</div>	
	<div class="field_row clearfix">
	<?php echo form_label(lang('config_erange_password').':', 'erange_password',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'erange_password',
			'id'=>'erange_password',
			'value'=>$item_info->erange_password)
		);?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#erange_credentials').expandable({title:'E-Range Credentials :', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>



<div id="course_generic_information" style='display:none'>
	<div class="field_row clearfix">
		<?php echo form_label('Stage:', 'information[stage]',array('class'=>'wide')); ?>

		<div class='form_field'>
			<?php echo form_dropdown('information[stage]',
				[
					""=>'',
					"demo"=>'Demo',
					"setup"=>'Setup',
					"live"=>'Live',
					"cancelled"=>'Cancelled'
				],
				$course_information->stage,
				"id = 'information[stage]' "
			);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('type:', 'information[course_type]',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_dropdown('information[course_type]',
				[
					""=>'',
					"public"=>'Public',
					"semi"=>'Semi',
					"private"=>'Private',
					"simulator"=>'Simulator',
					"other"=>'Other'
				],
				$course_information->course_type,
				"id = 'information[course_type]' "
			);?>
		</div>
	</div>
    <div class="field_row clearfix">
		<?php echo form_label('region:', 'information[region]',array('class'=>'wide')); ?>
        <div class='form_field'>

			<?php echo form_dropdown('information[region]',
				[
					""=>'',
					"west"=>'West',
					"central"=>'Central',
					"east"=>'East'
				],
				$course_information->region,
				"id = 'information[region]' "
			);?>
        </div>
    </div>
	<div class="field_row clearfix">
		<?php echo form_label('health:', 'information[course_health]',array('class'=>'wide')); ?>
		<div class='form_field'>

			<?php echo form_dropdown('information[course_health]',
				[
					""=>'',
					"red"=>'Red',
					"yellow"=>'Yellow',
					"green"=>'Green'
				],
				$course_information->course_health,
				"id = 'information[course_health]' "
			);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('mobile_app:', 'information[mobile_app]',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_dropdown('information[mobile_app]',
				[
					""=>'',
					"Y"=>'Yes',
					"N"=>'No'
				],
				$course_information->mobile_app,
				"id = 'information[mobile_app]' "
			);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('sales_rep:', 'information[sales_rep]',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_input(array(
					'autocomplete'=>'off',
					'name'=>'information[sales_rep]',
					'id'=>'information[sales_rep]',
					'value'=>$course_information->sales_rep_name,
					'class'=>'employee-select',
					'data-value'=>$course_information->sales_rep)
			);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('course_value:', 'information[course_value]',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_dropdown('information[course_value]',
				[
					""=>'',
					"1"=>'1 Star',
					"2"=>'2 Star',
					"3"=>'3 Star',
					"4"=>'4 Star',
				],
				$course_information->course_value,
				"id = 'information[course_value]' "
			);?>

		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('sphere_influence:', 'information[sphere_influence]',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_dropdown('information[sphere_influence]',
				[
					""=>'',
					"1"=>'1',
					"2"=>'2',
					"3"=>'3'
				],
				$course_information->sphere_influence,
				"id = 'information[sphere_influence]' "
			);?>

		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('apr:', 'information[apr]',array('class'=>'wide')); ?>
		<div class='form_field'>

			<?php echo form_dropdown('information[apr]',
				[
					""=>'',
					"1999"=>'0-1999',
					"2999"=>'2000-2999',
					"4199"=>'3000-4199',
					"4200"=>'4200+'
				],
				$course_information->apr,
				"id = 'information[apr]' "
			);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('date_sold:', 'information[date_sold]',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_input(array(
					'autocomplete'=>'off',
					'name'=>'information[date_sold]',
					'id'=>'information[date_sold]',
					'class'=>'date-time-picker',
					'value'=>$course_information->date_sold)
			);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('date_contract_ends:', 'information[date_contract_ends]',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_input(array(
					'autocomplete'=>'off',
					'name'=>'information[date_contract_ends]',
					'id'=>'information[date_contract_ends]',
					'class'=>'date-time-picker',
					'value'=>$course_information->date_contract_ends)
			);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('date_cancelled:', 'information[date_cancelled]',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_input(array(
					'autocomplete'=>'off',
					'name'=>'information[date_cancelled]',
					'id'=>'information[date_cancelled]',
					'class'=>'date-time-picker',
					'value'=>$course_information->date_cancelled)
			);?>
		</div>
	</div>

</div>

<script type='text/javascript'>
	$('#course_generic_information').expandable({title:'Course Information :', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>


	<div id="course_mobile_information" style='display:none'>

		<div class="field_row clearfix">
			<?php echo form_label('Mobile Rep:', 'information[mobile_rep]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'information[mobile_rep]',
						'id'=>'information[mobile_rep]',
						'class'=>'employee-select',
						'data-value'=>$course_information->mobile_rep,
						'value'=>$course_information->mobile_rep_name)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('iOS:', 'information[ios]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_dropdown('information[ios]',
					[
						""=>'',
						"Y"=>'Yes',
						"N"=>'No'
					],
					$course_information->ios,
					"id = 'information[ios]' "
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<div class='form_field'>
				<?php echo form_label('iOS Stage:', 'course_website_information[ios_stage]',array('class'=>'wide')); ?>

				<?php echo form_dropdown('information[ios_stage]',
					[
						""=>'',
						"Not Started"=>'Not Started',
						"Building"=>'Building',
						"Submitted"=>'Submitted',
						'Live'=>'Live'
					],
					$course_information->ios_stage,
					"id = 'information[ios_stage]' "
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('iOS Live Date:', 'information[ios_live_date]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'information[ios_live_date]',
						'id'=>'information[ios_live_date]',
						'class'=>'date-time-picker',
						'value'=>$course_information->ios_live_date)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Android:', 'information[android]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_dropdown('information[android]',
					[
						""=>'',
						"Y"=>'Yes',
						"N"=>'No'
					],
					$course_information->ios,
					"id = 'information[android]' "
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<div class='form_field'>
				<?php echo form_label('Anroid Stage:', 'course_website_information[android_stage]',array('class'=>'wide')); ?>

				<?php echo form_dropdown('information[android_stage]',
					[
						""=>'',
						"Not Started"=>'Not Started',
						"Building"=>'Building',
						"Submitted"=>'Submitted',
						'Live'=>'Live'
					],
					$course_information->android_stage,
					"id = 'information[android_stage]' "
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Android Live Date:', 'information[android_live_date]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'information[android_live_date]',
						'id'=>'information[android_live_date]',
						'class'=>'date-time-picker',
						'value'=>$course_information->android_live_date)
				);?>
			</div>
		</div>
	</div>
	<script type='text/javascript'>
		$('#course_mobile_information').expandable({title:'Course Mobile Information :', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
	</script>
<!--


Start of Course Information Setup


-->

	<div id="course_setup_information" style='display:none'>


		<div class="field_row clearfix">
			<?php echo form_label('setup_rep:', 'course_setup_information[setup_rep]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_setup_information[setup_rep]',
						'id'=>'course_setup_information[setup_rep]',
						'value'=>$course_setup_information->setup_rep_name,
						'class'=>'employee-select',
						'data-value'=>$course_setup_information->setup_rep)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Go Live Goal:', 'course_setup_information[date_golive_goal]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_setup_information[date_golive_goal]',
						'id'=>'course_setup_information[date_golive_goal]',
						'class'=>'date-time-picker',
						'value'=>$course_setup_information->date_golive_goal)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Imports Completed:', 'course_setup_information[date_imports_completed]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_setup_information[date_imports_completed]',
						'id'=>'course_setup_information[date_imports_completed]',
						'class'=>'date-time-picker',
						'value'=>$course_setup_information->date_imports_completed)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Training Completed:', 'course_setup_information[date_training_completed]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_setup_information[date_training_completed]',
						'id'=>'course_setup_information[date_training_completed]',
						'class'=>'date-time-picker',
						'value'=>$course_setup_information->date_training_completed)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Hardward Setup:', 'course_setup_information[date_hardward_setup]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_setup_information[date_hardward_setup]',
						'id'=>'course_setup_information[date_hardward_setup]',
						'class'=>'date-time-picker',
						'value'=>$course_setup_information->date_hardward_setup)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Live :', 'course_setup_information[date_live]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_setup_information[date_live]',
						'id'=>'course_setup_information[date_live]',
						'class'=>'date-time-picker',
						'value'=>$course_setup_information->date_live)
				);?>
			</div>
		</div>
	</div>

	<script type='text/javascript'>
		$('#course_setup_information').expandable({title:'Course Setup Information :', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
	</script>


	<!--


	Start of Course Website Information


	-->

	<div id="course_website_information" style='display:none'>


		<div class="field_row clearfix">
			<?php echo form_label('Website Rep:', 'course_website_information[website_rep]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_website_information[website_rep]',
						'id'=>'course_website_information[website_rep]',
						'class'=>'employee-select',
						'data-value'=>$course_website_information->website_rep,
						'value'=>$course_website_information->website_rep_name)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Stage:', 'course_website_information[stage]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_dropdown('course_website_information[stage]',
					[
						""=>'',
						"Not Started"=>'Not Started',
						"Building"=>'Building',
						"Internal Review"=>'Internal Review',
						"Customer Review"=>'Customer Review',
						"Customer Requests"=>'Customer Requests',
						'Live'=>'Live'
					],
					$course_website_information->stage,
					"id = 'course_website_information[stage]' "
				);?>
			</div>
		</div>

        <div class="field_row clearfix">
            <?php echo form_label('Date Sold:', 'course_website_information[date_sold]',array('class'=>'wide')); ?>
            <div class='form_field'>
                <?php echo form_input(array(
                        'autocomplete'=>'off',
                        'name'=>'course_website_information[date_sold]',
                        'id'=>'course_website_information[date_sold]',
                        'class'=>'date-time-picker',
                        'value'=>$course_website_information->date_sold)
                );?>
            </div>
        </div>
		<div class="field_row clearfix">
			<?php echo form_label('Go Live Goal:', 'course_website_information[date_golive_goal]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_website_information[date_golive_goal]',
						'id'=>'course_website_information[date_golive_goal]',
						'class'=>'date-time-picker',
						'value'=>$course_website_information->date_golive_goal)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Started:', 'course_website_information[date_started]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_website_information[date_started]',
						'id'=>'course_website_information[date_started]',
						'class'=>'date-time-picker',
						'value'=>$course_website_information->date_started)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Sent for Review:', 'course_website_information[date_sent_for_review]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_website_information[date_sent_for_review]',
						'id'=>'course_website_information[date_sent_for_review]',
						'class'=>'date-time-picker',
						'value'=>$course_website_information->date_sent_for_review)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Live Date:', 'course_website_information[date_live]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_website_information[date_live]',
						'id'=>'course_website_information[date_live]',
						'class'=>'date-time-picker',
						'value'=>$course_website_information->date_live)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Username:', 'course_website_information[username]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_website_information[username]',
						'id'=>'course_website_information[username]',
						'class'=>'select',
						'data-value'=>$course_website_information->username,
						'value'=>$course_website_information->username)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Password:', 'course_website_information[password]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_website_information[password]',
						'id'=>'course_website_information[password]',
						'class'=>'select',
						'data-value'=>$course_website_information->password,
						'value'=>$course_website_information->password)
				);?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo form_label('Dev Url:', 'course_website_information[development_url]',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
						'autocomplete'=>'off',
						'name'=>'course_website_information[development_url]',
						'id'=>'course_website_information[development_url]',
						'class'=>'select',
						'data-value'=>$course_website_information->development_url,
						'value'=>$course_website_information->development_url)
				);?>
			</div>
		</div>
	</div>

	<script type='text/javascript'>
		$('#course_website_information').expandable({title:'Course Website Information :', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
	</script>






<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('#edit_terminal_settings').on('click', function (e){
		e.preventDefault();
		$.colorbox2({href:"index.php/courses/edit_terminal_processing/<?=$item_info->course_id?>", title:'Terminal Processing Settings', width:600})
	});
	$("#simulator_radio").buttonset();
    $("#active_radio").buttonset();
    $("#demo_radio").buttonset();
    $("#clean_out_nightly_radio").buttonset();
    $("#teesheets_radio").buttonset();
    $("#pricing_radio").buttonset();
    $("#reservations_radio").buttonset();
    $("#schedules_radio").buttonset();
    $("#sales_radio").buttonset();
    $("#food_and_beverage_radio").buttonset();
    $("#sales_v2_radio").buttonset();
    $("#food_and_beverage_v2_radio").buttonset();
    $("#items_radio").buttonset();
    $("#item_kits_radio").buttonset();
    $("#employees_radio").buttonset();
    $("#customers_radio").buttonset();
    $("#invoices_radio").buttonset();
    $("#reports_radio").buttonset();
    $("#suppliers_radio").buttonset();
    $("#receivings_radio").buttonset();
    $("#config_radio").buttonset();
    $("#giftcards_radio").buttonset();
    $("#passes_radio").buttonset();
	$("#marketing_campaigns_radio").buttonset();
    $("#tournaments_radio").buttonset();
    $("#promotions_radio").buttonset();
	$("#quickbooks_radio").buttonset();
	$( "#category" ).autocomplete({
		source: "<?php echo site_url('items/suggest_category');?>",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0
	});

	$('input.date-time-picker').datepicker({
		dateFormat: "yy-mm-dd"
	});

	$(".employee-select").autocomplete({
		source: "<?php echo site_url('employees/suggest');?>",
		delay: 200,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$(this).val(ui.item.label);
			$(this).attr("data-value",ui.item.value);
		}
	});

	$('#item_form').validate({
		submitHandler:function(form)
		{
			$(".employee-select").each(function(index){
				$(this).val($(this).attr("data-value"));
			})
			$(form).ajaxSubmit({
			success:function(response)
			{
				$.colorbox.close();
				post_item_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			name:"required",
			category:"required",
			cost_price:
			{
				required:true,
				number:true
			},

			unit_price:
			{
				required:true,
				number:true
			},
			tax_percent:
			{
				required:true,
				number:true
			},
			quantity:
			{
				required:true,
				number:true
			},
			reorder_level:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{
			name:"<?php echo lang('items_name_required'); ?>",
			category:"<?php echo lang('items_category_required'); ?>",
			cost_price:
			{
				required:"<?php echo lang('items_cost_price_required'); ?>",
				number:"<?php echo lang('items_cost_price_number'); ?>"
			},
			unit_price:
			{
				required:"<?php echo lang('items_unit_price_required'); ?>",
				number:"<?php echo lang('items_unit_price_number'); ?>"
			},
			tax_percent:
			{
				required:"<?php echo lang('items_tax_percent_required'); ?>",
				number:"<?php echo lang('items_tax_percent_number'); ?>"
			},
			quantity:
			{
				required:"<?php echo lang('items_quantity_required'); ?>",
				number:"<?php echo lang('items_quantity_number'); ?>"
			},
			reorder_level:
			{
				required:"<?php echo lang('items_reorder_level_required'); ?>",
				number:"<?php echo lang('items_reorder_level_number'); ?>"
			}

		}
	});
});
</script>
