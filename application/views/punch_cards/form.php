<ul id="error_message_box"></ul>
<?php
if ($cart_line)
	echo form_open('sales/save_punch_card_details/'.$cart_line,array('id'=>'punch_card_form'));
else
	echo form_open('giftcards/save_punch_card/'.$punch_card_info->punch_card_id,array('id'=>'punch_card_form'));
?>
<fieldset id="punch_card_basic_info">

<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_punch_card_number').':<span class="required">*</span>', 'punch_card_number',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo $view_only ? $punch_card_info->punch_card_number :
	form_input(array(
		'name'=>'punch_card_number',
		'size'=>'24',
		'maxlength'=>'20',
		'id'=>'punch_card_number',
		'value'=>$punch_card_info->punch_card_number)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_customer_name').':', 'customer',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo $view_only ? $customer : form_input(array('name'=>'customer','id'=>'customer','value'=>(isset($punch_card_info->customer_name) && $punch_card_info->customer_name != '' ? $punch_card_info->customer_name : ''), 'placeholder'=>'No Customer'/*lang('sales_start_typing_customer_name'),  'accesskey' => 'c'*/));?>
		<?php echo form_hidden('customer_id', $punch_card_info->customer_id);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_expiration_date').':', 'expiration_date',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		$expiration_date = ($punch_card_info->expiration_date != '0000-00-00')?$punch_card_info->expiration_date:'No expiration';
		echo $view_only ? $expiration_date : form_input(array('name'=>'expiration_date','id'=>'expiration_date','value'=>$expiration_date));?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_details').':', 'details',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
			echo form_hidden('item_kit_id', isset($punch_card_info->item_kit_id) ? $punch_card_info->item_kit_id : 0);
		?>
    </div>
</div>
    <div class="field_row clearfix">
        <?php //echo form_label('', 'details',array('class'=>'wide')); ?>
        <div class='form_field'>
            <table style="margin:0px 5px; width:95%;">
                <tbody>
                <tr>
                    <td>Item</td>
                    <td>Price Class</td>
                    <td>Punches</td>
                    <td>Used</td>
                </tr>
                <?php
		$items = $punch_card_info->value;
        if (count($items) > 0) {
            foreach ($items as $item) {
                //$qty = floor($item->quantity);
                //echo "{$item->name} (Qty $qty)";
                echo "<tr>";
                echo "<td><input type='text' class='item_name' value='{$item->name}'/></td>";
                echo "<td><input type='hidden' class='item_id' value='{$item->item_id}' name='item_id'/>";
                echo form_dropdown('price_class_id', $price_classes, $item->price_class_id);
                echo "</td><td><input type='text' class='item_quantity' value='{$item->punches}' name='punches'/></td>";
                echo "<td><input type='text' class='item_used' value='{$item->used}' name='used'/></td>";
                echo "</tr>";
            }
        }
        else {
            echo "<tr>";
            echo "<td><input type='text' class='item_name' value='{$item->name}'/></td>";
            echo "<td><input type='hidden' class='item_id' value='{$item->item_id}' name='item_id'/>";
            echo form_dropdown('price_class_id', $price_classes, $item->price_class_id);
            echo "</td><td><input type='text' class='item_quantity' value='{$item->punches}' name='punches'/></td>";
            echo "<td><input type='text' class='item_used' value='{$item->used}' name='used'/></td>";
            echo "</tr>";
        }
		?>
            </tbody>
            </table>
	</div>
</div>
<style>
    .item_quantity, .item_used {
      width:60px;
    }
    .
</style>
<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_notes').':', 'details',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		echo $view_only ? $punch_card_info->details : form_textarea(array('name'=>'details','id'=>'details','value'=>$punch_card_info->details,'rows'=>2));?>
	</div>
</div>

<?php
echo $view_only ? '' :
form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
if (!$view_only) {
?>
<style>
	#details {
		width:470px;
	}
</style>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('#expiration_date').datepicker({dateFormat:'yy-mm-dd'});
	$('#customer').click(function()
    {
    	$(this).attr('value','');
       	$('input[name=customer_id]').val('');
    });

    $('#punch_card_number').swipeable({allow_enter:false, character_limit:20});
    $( "#customer" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/last_name"); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)
		{
			event.preventDefault();
			$("#customer").val(ui.item.label);
			$("input[name=customer_id]").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
		}
	});
	var submitting = false;
    $( ".item_name" ).autocomplete({
        source: '<?php echo site_url("items/item_search"); ?>',
        delay: 200,
        autoFocus: false,
        minLength: 1,
        select: function( event, ui )
        {
            event.preventDefault();
            $('.item_id').val(ui.item.value);
            $(this).val(ui.item.label);


            return ;
        }
    });
    $('#punch_card_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			<?php if ($punch_card_info->punch_card_id) { ?>
				$(form).ajaxSubmit({
					success:function(response)
					{
						$.colorbox.close();
		                submitting = false;
		                <?php if ($cart_line) {?>
		                	console.dir(response);
						    //result = eval('('+response+')');
			                sales.update_cart_info(<?php echo ($cart_line) ?>, response.item_info);
			                sales.update_basket_totals(<?php echo ($cart_line) ?>, response.basket_info);

						<?php } else { ?>
							post_giftcard_form_submit(response);
						<?php } ?>
					},
					dataType:'json'
				});
			<?php } else {?>
				$.ajax({
		           type: "POST",
		           url: <?php if ($cart_line) {?>"index.php/sales/punch_card_exists/"+$('#punch_card_number').val()<?php } else { ?>"index.php/giftcards/punch_card_exists/"+$('#punch_card_number').val()<?php } ?>,
		           data: "",
		           success: function(response){
				   	   if (!response.exists)
				   	   {
	   	   					$(form).ajaxSubmit({
								success:function(response)
								{
									$.colorbox.close();
					                submitting = false;
					                <?php if ($cart_line) {?>
					                	console.dir(response);
									    //result = eval('('+response+')');
						                sales.update_cart_info(<?php echo ($cart_line) ?>, response.item_info);
						                sales.update_basket_totals(<?php echo ($cart_line) ?>, response.basket_info);

									<?php } else { ?>
										post_giftcard_form_submit(response);
									<?php } ?>
								},
								dataType:'json'
							});

				   	   }
				   	   else
				   	   {
				   	   		submitting = false;
				   	   		$(form).unmask();
							alert('That punch card number is already in use, please select a different one.');
				   	   }
				   },
		           dataType:'json'
		        });
		    <?php } ?>
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			punch_card_number:
			{
				required:true
			}
   		},
		messages:
		{
			punch_card_number:
			{
				required:"<?php echo lang('punch_card_number_required'); ?>",
				number:"<?php echo lang('punch_card_number'); ?>"
			}
		}
	});
});
</script>
<script>
	$(document).ready(function(){
		var gcn = $('#punch_card_number');
		gcn.keydown(function(event){
			// Allow: backspace, delete, tab, escape, and enter
	        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
	             // Allow: Ctrl+A
	            (event.keyCode == 65 && event.ctrlKey === true) ||
	             // Allow: home, end, left, right
	            (event.keyCode >= 35 && event.keyCode <= 39)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        else {
	            // Ensure that it is a number and stop the keypress
	            //if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	            	console.log('kc '+event.keyCode);
	            if (event.keyCode == 186 || event.keyCode == 187 || event.keyCode == 191 || (event.shiftKey && event.keyCode == 53)) {
	                event.preventDefault();
	            }
	        }
		});
		//gcn.focus();
	})
</script>
<?php } ?>
