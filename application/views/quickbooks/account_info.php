<?php $account_map = $quickbooks['account_map']; ?>
<script>
$(function(){
	$('#refresh-quickbooks-accounts').off().on('click', function(e){
		var url = $(this).attr('href');
		$.get(url, null, function(response){
			if(response.success){
				set_feedback(response.message,'success_message',false);
			}else{
				set_feedback(response.message,'error_message',true);
			}
		},'json');
		return false;
	});

	$('#disable_quickbooks').off().on('click', function(e){
		if(confirm('Are you sure you want to disable QuickBooks?')){
			var url = $(this).attr('href');
			$.get(url, null, function(response){
				if(response.success){
					set_feedback(response.message,'success_message',false);
					$('#quickbooks_account').html('');
					$('#enable_quickbooks').show();
				}else{
					set_feedback(response.message,'error_message',true);
				}
			},'json');
		}
		return false;
	});

	$('#sync_quickbooks').off().on('click', function(e){
		var url = '<?php echo site_url('qb/quickbooks_sync/'.$this->session->userdata('course_id')); ?>';
		$('#quickbooks_account').mask('Syncing Data...');
		$.get(url, null, function(response){
			if(response.success){
				set_feedback(response.message, 'success_message', false);
			}else{
				set_feedback(response.message, 'error_message', false, 5000);
			}
			$('#quickbooks_account').unmask();
		},'json');
		return false;
	});
});
</script>
<a title="Download QWC File" class="terminal_button" style="width: 155px; display: block; margin: 0px;" href="<?php echo site_url('qb/create_qwc'); ?>">Download QWC File</a>
<a href="<?php echo site_url('qb/refresh_accounts'); ?>" style="position: absolute; right: 10px; top: 10px;" id="refresh-quickbooks-accounts">Refresh Account List</a>
<div class="field_row clearfix">
	<?php echo form_label('QWC Username:', 'qb_username',array('class'=>'wide')); ?>
	<?php echo form_input(array(
		'name'=>'qb_username',
		'id'=>'qb_username',
		'placeholder'=>'',
		'value'=>$quickbooks['qb_username'],
		'disabled'=>true));?>
	<input type="hidden" name="qb_username" value="<?php echo $quickbooks['qb_username']; ?>" />
</div>
<div class="field_row clearfix">
	<?php echo form_label('QWC Password:', 'qb_password',array('class'=>'wide')); ?>
	<?php echo form_input(array(
		'name'=>'qb_password',
		'id'=>'qb_password',
		'placeholder'=>'',
		'value'=>$quickbooks['qb_password'],
		'disabled'=>true));?>
</div>
<div class="field_row clearfix">
	<?php echo form_label('Company File Location:', 'qb_company_file',array('class'=>'wide')); ?>
	<?php echo form_input(array(
		'name'=>'qb_company_file',
		'id'=>'qb_company_file_location',
		'placeholder'=>'',
		'style'=>'width: 400px;',
		'value'=>$quickbooks['qb_company_file']));?>
</div>

<h2 style="color: #888; margin-top: 15px; margin-bottom: 5px;">Account Mapping</h2>
<div class="field_row clearfix">
	<?php echo form_label('Income Account', 'qb_income_account', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('qb_income_account', $quickbooks['accounts'], $account_map['Income']['full_name']); ?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label('Green Fee Account', 'qb_greenfee_account', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('qb_greenfee_account', $quickbooks['accounts'], $account_map['GreenFees']['full_name']); ?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label('Cart Fee Account', 'qb_cartfee_account', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('qb_cartfee_account', $quickbooks['accounts'], $account_map['CartFees']['full_name']); ?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label('Inventory COGS Account', 'qb_cogs_account', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('qb_cogs_account', $quickbooks['accounts'], $account_map['COGS']['full_name']); ?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label('Inventory Asset Account', 'qb_assets_account', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('qb_assets_account', $quickbooks['accounts'], $account_map['Assets']['full_name']); ?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label('Redemption Account', 'qb_redemption_account', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('qb_redemption_account', $quickbooks['accounts'], $account_map['Redemption']['full_name']); ?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label('Tips Account', 'qb_tips_account', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('qb_tips_account', $quickbooks['accounts'], $account_map['Tips']['full_name']); ?>
	</div>
</div>
<?php
echo form_submit(array(
	'name'=>'submit_quickbooks',
	'id'=>'submit_quickbooks',
	'value'=>'Save Settings',
	'class'=>'submit_button',
	'style'=>'float: left')
);
?>
<button class="submit_button" id="sync_quickbooks">Sync Data</button>
<a id="disable_quickbooks" href="<?php echo site_url('qb/disable'); ?>" style="color: #AA0000; float: right;">Disable QuickBooks</a>