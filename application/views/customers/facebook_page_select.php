<div>	
	<ol id="selectable" class="facebook_pages"></ol>
</div>

<script>
		
$(document).ready(function(){
	if (!$('#selectable').hasClass('ui-selectable')){ 
	  	$('#selectable').selectable({
			selected: function(event, ui){				
				var URL, data;
								
				window.my_config.page_id = $($(ui)[0].selected).attr('data-page_id');
				window.my_config.access_token = $($(ui)[0].selected).attr('data-access_token')
				window.my_config.name = $($(ui)[0].selected).html();
					
				URL = '<?php echo site_url("teesheets/update_facebook_page_id");?>';
				data = {page_id: window.my_config.page_id, page_name : window.my_config.name};						
				
				$.post(URL, data, function(response) {				      
			      console.log('updated fb page id with the server')
			      console.log(response);
			    });	
			    
			    $.colorbox.close();
			    							
			}
		});
	}//end if

	FB.getLoginStatus(function(response) {
			  if (response.status === 'connected') {			    
			    FB.api('/me/applications/developer', function(response) {	
			    	window.my_config.user_accounts = response.data;	
			    	
			    	//delete current li items and then rebuild the list
			    	$('#selectable').children().remove();			    	  		
		  			$.each(response.data, function(index, value){		  				
		  				$('#selectable').append('<li class="ui-widget-content" data-page_id="' +value.id  + '" data-access_token="' + value.access_token + '">' + value.name + '</li>');		  				
		  			});		
		  			
		  			$.colorbox.resize();
		  			
		  			//this is defined in the view people/manage.php
		  			facebook_obj.logout();			  			
				});									
			  }
		 }, true);
});
</script>