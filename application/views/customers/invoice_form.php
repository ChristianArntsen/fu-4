
<style>
#invoice_details_table {
	margin: 10px;
	width: 350px;
}

#invoice_details_table td, #invoice_details_table td input {
	padding: 5px;
	font-size: 16px;
}

#invoice_details_table td input {
	width: 75px;
}
</style>
<ul id="error_message_box"></ul>
<?php echo form_open('sales/save_invoice_details/'.$cart_line, array('id'=>'invoice_form')); ?>
<table id="invoice_details_table">
<tbody>
	<tr>
		<td><strong>Total</strong></td>
		<td colspan="2"><?php echo to_currency($invoice_info['total']); ?></td>
	</tr>
	<tr>
		<td><strong>Paid</strong></td>
		<td colspan="2"><?php echo to_currency(-$invoice_info['paid']); ?></td>
	</tr>
	<tr>
		<td><strong>Total Due</strong></td>
		<td>$ <input name="invoice_payment_total" id="invoice_payment_total" value="<?php echo to_currency_no_money($invoice_info['price']); ?>" /></td>
		<td style="padding-left: 10px;">
			<?php if($overdue > 0){ ?>
			Outstanding: <span id="invoice_overdue" style="color: red;"><?php echo to_currency($overdue); ?></span>
			<?php } ?>
		</td>
	</tr>
</tbody>
</table>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
); ?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
//validation and submit handling
$(document).ready(function(){

	$('#invoice_payment_total').blur(function(e){
		var overdue = <?php echo (float) $overdue; ?>;
		var price = $('#invoice_payment_total').val();
		price = parseFloat(price.replace(/[^0-9.-]+/g, '')).toFixed(2);

		if(price > overdue){
			$('#invoice_payment_total').val( overdue.toFixed(2) );
		}
	});

	var submitting = false;
    $('#invoice_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
					var price = $('#invoice_payment_total').val();
					var context = '';
					price = parseFloat(price.replace(/[^0-9.-]+/g, '')).toFixed(2);
					context = '#reg_item_price_' + response.cart_line;
					$('div', context).html("$" + price + "");

					context = '#reg_item_total_' + response.cart_line;
					$('div', context).html("$" + price + "");

					$.colorbox.close();
	                submitting = false;
	                sales.update_basket_totals(null, response.basket_info);
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			giftcard_number:
			{
				required:true
			},
			value:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{
			giftcard_number:
			{
				required:"<?php echo lang('giftcards_number_required'); ?>",
				number:"<?php echo lang('giftcards_number'); ?>"
			},
			value:
			{
				required:"<?php echo lang('giftcards_value_required'); ?>",
				number:"<?php echo lang('giftcards_value'); ?>"
			}
		}
	});
});
</script>