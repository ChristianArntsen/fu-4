<?php if (!$hide_jquery) { ?>
<script src="<?php echo base_url();?>js/jquery-1.10.2.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
<?php } ?>
<div>
	<?php
		if ($card_captured)
			echo 'Card capture successful';
		else {
			echo 'Card capture did not occur. Closing window.';
		}
	//echo $test_value;
	//echo $payment_amount;
	//echo $payment_type;
	?>
</div>
<?php
if ($controller_name == 'teesheets')
{
	?>
	<script>
		if (window.parent.document.getElementById('tee_sheet_iframe').length > 0) {
            window.parent.document.getElementById('tee_sheet_iframe').contentWindow.added_credit_card("<?=$credit_card_id?>");
        } else {
            window.parent.added_credit_card("<?=$credit_card_id?>");
        }
	</script>
	<?php
}
else
{
	$cc_checkboxes = "";
	$cc_dropdown = "<select id='cc_dropdown' name='cc_dropdown'>";
	$cc_dropdown .= "<option value='invoice'>Only Generate Invoice</option>";

	foreach($credit_cards as $credit_card)
	{
		$cc_checkboxes .= "<div id='checkbox_holder_".$credit_card['credit_card_id']."'class='field_row clearfix ".(($credit_card['customer_id'] != '' && $credit_card['customer_id'] == $person_info->person_id)?'':'hidden')."'>".
				"<div class='form_field'>".
					form_checkbox('groups[]', $credit_card['credit_card_id'], ($credit_card['customer_id'] != '' && $credit_card['customer_id'] == $person_info->person_id) ? true:FALSE, "id='group_checkbox_{$credit_card['credit_card_id']}'")
					.' '.$credit_card['card_type'].' '.$credit_card['masked_account'].
					" - <span class='' onclick='customer.add_billing_row(\'".$credit_card['credit_card_id']."\')'>Add Billing</span>".
				"</div>
			</div>";
			$selected = '';
			if ($credit_card_id == $credit_card['credit_card_id'])
			{
				$selected = 'selected';
				$selected_card = "{$credit_card['card_type']} {$credit_card['masked_account']}";
			}
		$cc_dropdown .= "<option value='{$credit_card['credit_card_id']}' $selected>Charge to {$credit_card['card_type']} {$credit_card['masked_account']}</option>";

	}
	$cc_dropdown .= '</select>';
?>
<script>
<?php if ($billing_id > -1) { ?>
	$(document).ready(function(){
		window.parent.reload_customer("<?=$cc_dropdown ?>");
	});
<?php } else { ?>
	$(document).ready(function(){
        if (typeof window.parent.reload_customer_credit_cards == 'function') {
            window.parent.reload_customer_credit_cards(<?=$credit_card['credit_card_id']?>, <?=$customer_id?>);
        }
        else {
            var wpp = window.parent.parent;
            var credit_card_list = wpp.$('.credit-card-list');
            if (credit_card_list.length != 0) {
                <?php
                    $this->load->model('Customer_credit_card');
                    $card_type = $this->Customer_credit_card->get_simple_type(array('card_type'=>$credit_card['card_type']));
                ?>
                var new_card = '<tr data-credit-card-id="<?=$credit_card['credit_card_id']?>">' +
                    '<td><div class="col-md-12 credit-card <?=$card_type?>"></div></td>' +
                    '<td class="card-number"><?=str_replace(array('x', '*'), '', $credit_card['masked_account'])?></td>' +
                    '<td class="card-status"><div class="fa fa-check status-green"></div></td>' +
                    '<td class="card-success">0</td>' +
                    '<td class="card-failure">0</td>' +
                    '<td class="card-remove"><div class="remove-credit-card"><i class="fa fa-trash-o"></i></div></td>' +
                    '</tr>';
                credit_card_list.append(new_card);
                wpp.$('#modal-manage-credit-cards').modal('hide');
            }
        }
	});
<?php } ?>
</script>
<?php } ?>