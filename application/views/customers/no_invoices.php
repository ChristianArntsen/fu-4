<?php $this->load->view("partial/header_new"); ?>
<?php 
	if ($member_balance || $customer_credit) {
		echo "<div>No negative balances to invoice at this time</div>";
	}
	
	if ($recurring_billings) {
		echo "<div>No recurring billings to invoice between {$start_on} and {$end_on}</div>";
	}
?>
<?php $this->load->view("partial/footer"); ?>