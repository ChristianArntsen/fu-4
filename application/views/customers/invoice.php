<?php if (!empty($pdf)) { ?>
<!DOCTYPE html>
<html style="width:100%;">
<head>
	<title><?php echo $receipt_title; ?></title>
</head>
<body>
<?php }
if (base_url() == 'http://localhost:8080/') {
	$base_url = base_url();
}
else {
	$base_url = 'http://foreupsoftware.com/';
}

$bgc = '#c8c8c8';
$bc = '#606060';
$fc = '#606060';
$hfs = '10px';
$hfw = 'bold';
$cp = '4px 8px';
$amount_due_bg = '#EFEFEF';

$this_months_account_balance = 0;
$this_months_member_balance = 0;
$editable_area = 'editable_area';
$first_label = true;
$cc_checkboxes = '';
$cc_dropdown = '<select id="cc_dropdown" name="credit_card_id">';
$cc_dropdown .= '<option value="0">Only Generate Invoice</option>';
$selected_card = false;

if (!empty($credit_cards)) {
	foreach ($credit_cards as $credit_card) {
		$cc_checkboxes .= "<div id='checkbox_holder_" . $credit_card['credit_card_id'] . "'class='field_row clearfix " . (($credit_card['customer_id'] != '' && $credit_card['customer_id'] == $person_info->person_id) ? '' : 'hidden') . "'>" .
			"<div class='form_field'>" .
			form_checkbox('groups[]', $credit_card['credit_card_id'], ($credit_card['customer_id'] != '' && $credit_card['customer_id'] == $person_info->person_id) ? true : FALSE, "id='group_checkbox_{$credit_card['credit_card_id']}'")
			. ' ' . $credit_card['card_type'] . ' ' . $credit_card['masked_account'] .
			' - <span class="" onclick="customer.add_billing_row(\'' . $credit_card['credit_card_id'] . '\')">Add Billing</span>' .
			"</div>
			</div>";
		$selected = '';
		if (!empty($credit_card['credit_card_id']) && !empty($credit_card_id) && $credit_card_id == $credit_card['credit_card_id']) {
			$selected = 'selected';
			$selected_card = "{$credit_card['card_type']} {$credit_card['masked_account']}";
		}
		$cc_dropdown .= "<option value='{$credit_card['credit_card_id']}' $selected>Charge to {$credit_card['card_type']} {$credit_card['masked_account']}</option>";
		$first_label = false;
	}
}

$cc_dropdown .= '</select>';
$add_cc_link = isset($person_info) && ($this->config->item('mercury_id') || $this->config->item('ets_key')) ?
	anchor(
		site_url('customers/open_add_credit_card_window/' .
			$person_info->person_id.'/'.(!empty($billing_id) ? $billing_id : -1)),
		"<div id='add_card'>". lang('customers_add_credit_card')."</div>",
		array('id'=>'add_credit_card_link')
	) :
	'';

if (!empty($is_invoice))
{
	//$cc_dropdown = $selected_card ? $selected_card : '';
	$add_cc_link = '';
	$editable_area = '';
}
?>
	<?php if(empty($pdf)){ ?>
	<div id="receipt_wrapper">
	<style>
	#cc_dropdown {
		display: block;
		float: right;
		margin-top: 10px;
	}

	div.editable_area {
		padding: 4px 2px 4px 2px;
	}

	div.editable_area > label {
		margin: 5px;
		padding: 0px;
		line-height: 12px;
		height: 12px;
		display: block;
		font-size: 12px;
		position: relative;
		cursor: pointer;
	}

	div.editable_area > label input {
		top: 2px;
		position: relative;
	}

	#receipt_wrapper .line_item td {
		border: 1px solid #606060;
		padding: 2px;
		height: 32px;
	}

	#receipt_wrapper .line_item td > input {
		display: block;
		width: auto;
		float: left;
		height: 20px;
		margin: 0px;
	}

	#receipt_wrapper .line_item td > span.symbol {
		width: 10px;
		display: block;
		float: left;
		margin: 0px;
		padding: 0px;
		line-height: 28px;
		height: 28px;
		text-align: center;
	}

	#receipt_wrapper .line_item td.tax_col > span {
		float: right;
	}

	#receipt_wrapper td.name_col {
		width: 300px;
	}

	#receipt_wrapper td.name_col > input {
		width: 280px;
	}

	#receipt_wrapper td.qty_col  {
		width: 35px;
	}

	#receipt_wrapper td.qty_col > input  {
		width: 30px;
	}

	#receipt_wrapper td.price_col {
		width: 60px;
	}

	#receipt_wrapper td.price_col > input {
		width: 45px;
	}

	#receipt_wrapper td.tax_col  {
		width: 60px;
	}

	#receipt_wrapper td.tax_col > input  {
		width: 45px;
	}

	#receipt_wrapper td.row_total_col  {
		width: 60px;
		text-align: right;
		padding-right: 8px;
	}
	#receipt_wrapper #add_card {
		float: right;
		margin-top:10px;
	}
	</style>
    <input type='hidden' value='<?=$this->config->item('unit_price_includes_tax')?>' id='tax_included'/>
	<?php } else { ?>
	<!-- PDF ADDED STYLES -->
	<div id="receipt_wrapper" style="margin-left:10px; margin-top:15px;">
	<?php } ?>
	<!-- INVOICE HEADER -->
	<table cellspacing="0" style='width: <?=empty($pdf)?'580':'800'?>px; font-family:arial,sans-serif; font-size:12px; border-collapse:collapse; margin-bottom: 15px;'>
		<tbody>
			<tr>
				<td style="padding: 0px; text-align: left; width: 155px; vertical-align: top;">
					<!-- Logo -->
					<?php
					// Get course logo, if no logo is set, use ForeUp's logo
					$logo_id = (int) $course_info->company_logo;
					if(empty($logo_id)){
						//$logo = base_url('images/header/header_logo.png');
						$logo = $base_url.'images/header/header_logo.png';
					}else{
						//$logo = site_url('app_files/view').'/'.$logo_id;
						$logo = $base_url.'index.php/app_files/view/'.$logo_id;
					}
					echo img(array('src' => $logo, 'width' =>144));
					?>
				</td>
				<?php
					$phone_number = $course_info->phone;
					trim($phone_number);
					$len = strlen($phone_number);
					if($len == 7)
					$phone_number = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $phone_number);
					elseif($len == 10)
					$phone_number = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3',$phone_number);
				?>
				<td style='text-align: left; font-weight: normal; font-size: 12px; padding: 0px; vertical-align: top;'>
					<div id="company_name" style="font-size: 16px; font-weight: bold;"><?=$course_info->name;?></div>
					<div id="company_address"><?=$course_info->address;?></div>
					<div id="company_address"><?=$course_info->city;?>, <?=$course_info->state;?> <?=$course_info->zip;?></div>
					<div id="company_phone">p: <?=$phone_number;?></div>
					<div id="company_phone">e: <?=$course_info->email;?></div>
				</td>
				<td colspan="3" style="text-align: right; padding: 0px; vertical-align: top; font-weight: normal; padding-left: 50px;">
					<h3 style="font-size:24px; text-align: right;">Invoice</h3>
					<?php
					$invoice_number = !empty($invoice_number) ? $invoice_number : '';
					echo "<img style='margin-bottom: 5px;' src='{$base_url}index.php/barcode?barcode=INV%20$invoice_number&text=INV%20$invoice_number&.png' />";
					?><br />
					<?php if(isset($sent)&&$sent){ ?>
					<span style="float: left;">Invoice Date:</span> <span><?php echo $date; ?></span><br />
					<span style="float: left;">Due Date:</span> <span><?php echo $due_date; ?></span>
					<?php } ?>
				</td>
			</tr>
		</tbody>
	</table>
	<!-- END INVOICE HEADER -->

	<!-- CUSTOMER TOTAL DUE BOX -->
	<table cellspacing="0" style="width: <?=empty($pdf)?'580':'800'?>px; margin-bottom: 15px;">
		<tbody>
			<tr>
				<td style='width: 60%; background:<?=$amount_due_bg?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; font-size: 12px;'>
					<div>
					<table cellspacing="0" style="height: 175px;">
					<tbody>
						<tr>
							<td style="vertical-align: top; text-align: left; padding-top: 2px; width: 85px;"><strong>Customer</strong></td>
							<td colspan="2" style="vertical-align: top; text-align: left;">
								<?php
								if(isset($person_info)) {

									$customer_phone = $person_info->phone_number;
									trim($customer_phone);
									$len = strlen($customer_phone);
									if ($len == 7) {
										$customer_phone = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $customer_phone);
									} elseif ($len == 10) {
										$customer_phone = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $customer_phone);
									}
									?>
									<div id="company_name"
										 style="font-size: 20px; font-weight: bold;"><?= $person_info->last_name; ?>
										, <?= $person_info->first_name; ?></div>
									<?php if ($person_info->address_1 != '') { ?>
										<div id="company_address"><?= $person_info->address_1; ?></div>
										<div id="company_address"><?= $person_info->city; ?>
											, <?= $person_info->state; ?> <?= $person_info->zip; ?></div>
									<?php }
								}
								?>
							</td>
						</tr>
						<tr>
							<td style="height: 10px;" colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<?php $charge_style = ''; if(!empty($pdf)){ $charge_style = 'line-height: 16px; height: 16px;'; } ?>
							<?php $right_aligned = ''; if(empty($pdf)){ $right_aligned = 'text-align:right;'; } ?>
							<td style="vertical-align: top; text-align: left;"><strong>Charges</strong></td>
								<td style="<?php echo $charge_style; ?>">New Charges:</td>
							<td style="<?php echo $charge_style; ?>  <?=$right_aligned?> padding-right:10px;" id="customer_current_charges"><?php echo !empty($customer_current_charges) ? to_currency($customer_current_charges) : ''; ?></td>
						</tr>
						<tr>
							<td style="vertical-align: top; text-align: left;"><strong></strong></td>
							<td style="<?php echo $charge_style; ?>">Overdue Charges:</td>
							<td style="<?php echo $charge_style; ?>  <?=$right_aligned?> padding-right:10px;" id="customer_overdue_charges"><?php echo  !empty($customer_overdue_charges) ? to_currency($customer_overdue_charges) : ''; ?></td>
						</tr>
						<tr>
							<td style="vertical-align: top; text-align: left;" ><strong></strong></td>
							<td style="<?php echo $charge_style; ?>">Total Paid:</td>
							<td style="<?php echo $charge_style; ?>  <?=$right_aligned?> padding-right:10px;" id="customer_total_paid"><?php echo !empty($paid) ? to_currency($paid) : ''; ?></td>
						</tr>
						<tr>
							<td style="vertical-align: top; text-align: left;"><strong></strong></td>
							<td style="vertical-align: top; text-align: left;" colspan=2>
								<?=!empty($notes) ? $notes : ''?>
							</td>
						</tr>

						<?php if (!empty($editable)) { ?>
						<tr>
							<td style="vertical-align: top; text-align: left;"></td>
								<td style="<?php echo $charge_style; ?>"></td>
							<td style="<?php echo $charge_style; ?>  <?=$right_aligned?> padding-right:10px;" id="customer_current_charges">
								<a id='edit_invoice' href='#'>(Edit)</a>
								<script>$('#edit_invoice').colorbox2({'href':'index.php/invoices/edit_totals/<?=$invoice_id?>', 'width':550, 'title':'Edit Invoice'})</script>
							</td>
						</tr>
						<?php } ?>
						<tr>
							<td style="height: 5px;">&nbsp;</td>
						</tr>
						<tr>
							<td style="vertical-align: top; text-align: left; font-size: 14px;"><strong>Total Due</strong></td>
							<td style="vertical-align: top; text-align: left;">
								<em>as of <?php echo !empty($date) ? $date : ''; ?></em>
							</td>
							<td style="vertical-align: top; padding-right: 10px; <?=$right_aligned?>">
								<h2 id="customer_total_due" style="display: inline; line-height: 24px; font-size: 24px; padding: 0px; margin: 0px;"><?php echo !empty($customer_total_due) ? to_currency($customer_total_due) : ''; ?></h2>
							</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>

				<!-- INVOICE EDIT CONTROLS -->
				<td style="text-align: left; padding-left: 20px; vertical-align: top;">
					<?php if (!isset($sent) || !$sent) { ?>
					<div class='editable_area' style="float: right;">
						<?php if(isset($person_info) && empty($person_info->email) && $person_info->person_id != ''){ ?>
						<div data-href="index.php/customers/add_email/<?=$person_info->person_id?>/<?=($is_invoice?'invoice':'recurring_billing'); ?>/width~500" id='add_customer_email'>
							<?php echo lang('add_customer_email') ;?>
						</div>
						<?php } ?>
						<label>
							<input type='checkbox' name="email_invoice" value="1" <?php if(!empty($email_invoice)){ echo 'checked'; } ?> />
							Email Invoice to Customer
						</label>
						<label>
							<input type='checkbox' class='<?php echo (!empty($is_invoice)?'is_invoice':''); ?>' id='pay_member_balance' name='pay_member_balance' <?php echo (!empty($pay_member_balance)?'checked="checked"':'');?> />
							<?php $memberAccountName = ($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')); ?>
							Pay <?php echo $memberAccountName; ?>
						</label>
						<label>
							<input type='checkbox' class='<?php echo (!empty($is_invoice)?'is_invoice':''); ?>' id='pay_account_balance' name='pay_customer_balance' <?php echo (!empty($pay_account_balance)?'checked="checked"':'');?> />
							<?php $customerAccountName = ($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname')); ?>
							Pay <?php echo $customerAccountName; ?>
						</label>
						<label>
							<input type='checkbox' value="1" class='<?php echo (!empty($is_invoice)?'is_invoice':''); ?>' id='include_itemized_sales' name='show_account_transactions' <?php echo (!empty($show_account_transactions)?'checked="checked"':'');?> />
							Show Account Transactions
						</label>
					</div>
					<?php if (isset($person_info) && $person_info->person_id != '') { ?>
					<?php echo $cc_dropdown; ?>
					<?php echo $add_cc_link; ?>
					<?php } ?>
					<?php }else{ ?>&nbsp;<?php } ?>
				</td>
				<!-- END INVOICE EDIT CONTROLS -->
			</tr>
		</tbody>
		</table>
		<!-- END CUSTOMER TOTAL DUE BOX -->

		<!-- CURRENT CHARGES -->
		<table cellspacing="0" style="width: <?=empty($pdf)?'580':'800'?>px; margin-bottom: 15px; margin-top: 15px; font-size: 12px;">
			<tbody>
			<tr>
				<td colspan="5">
					<h2 style="color: #AAA; margin: 0px; font-weight: bold; font-size: 18px; line-height: 24px; height: 24px;">New Charges</h2>
				</td>
			</tr>
			<tr id="item_header">
				<td class="name_col" style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>ITEM</td>
				<td class="qty_col" style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>QTY</td>
				<td class="price_col" style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>PRICE</td>
				<td class="tax_col"  style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>TAX</td>
				<td class="row_total_col" style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>AMOUNT</td>
			</tr>
			<?php
			$service_fees = array();
			$service_fee_hash = array();
			$last_line = 0;

			if (!empty($current_items)) {
				$CI = get_instance();
				$CI->load->model('Item');
				foreach($current_items as $line => $item) {
					$item_info = $CI->Item->get_info($item['item_id']);
					?>

				<?php
					if(isset($item_info->is_service_fee) && $item_info->is_service_fee){
						if(!isset($service_fee_hash[$item['name']])){
							$service_fee_hash[$item['name']] = $item;
						}else{
							$service_fee_hash[$item['name']]['quantity'] = $item['quantity'];
							$service_fee_hash[$item['name']]['subtotal'] = $item['subtotal'];
							$service_fee_hash[$item['name']]['tax'] = $item['tax'];
							$service_fee_hash[$item['name']]['total'] = $item['total'];
						}
					}
					elseif ($sent) {
				?>
					<tr class="line_item">
						<td style='width: 400px; padding:<?=$cp?>; border:1px solid <?=$bc?>;'>
							<?php echo $item['name']; ?>
						</td>
						<td style='width: 40px; padding:<?=$cp?>; border:1px solid <?=$bc?>;'>
							<?php echo $item['quantity']; ?>
						</td>
						<td style='width: 40px; padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>
							<?php echo to_currency($item_info->unit_price_includes_tax ?
								$item['subtotal'] :
								$item['price']); ?>
						</td>
						<td style='width: 40px; padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>
							<?php echo to_currency($item_info->unit_price_includes_tax ?
                                $item['total'] - $item['subtotal'] :
                                $item['tax']); ?>
						</td>
						<td style='width: 40px; padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>
							<?php echo to_currency($item_info->unit_price_includes_tax ?
								$item['total'] :
								$item['subtotal']); ?>
						</td>
					</tr>

				<?php }else if ($item['item_type'] == 'member_balance' || $item['item_type'] == 'account_balance') { ?>
					<tr class="line_item" id="<?php echo $item['item_type']; ?>_row">
						<td class="name_col" style="padding: 4px 8px; border:1px solid #060606;">
							<?php echo $item['description']; ?>
							<input type="hidden" name="items[<?php echo $item['line']; ?>][description]" value="<?php echo $item['description']; ?>">
                            <input type="hidden" name="items[<?php echo $item['line']; ?>][item_type]" value="<?php echo $item['item_type']; ?>">
                        </td>
						<td class="qty_col" style="padding: 4px 8px; border:1px solid #060606;">1<input type="hidden" name="items[<?php echo $item['line']; ?>][quantity]" class="quantity" value="1"></td>
						<td class="price_col" style="padding: 4px 8px; border:1px solid #060606; text-align:right;">$0 <input type="hidden" name="items[<?php echo $item['line']; ?>][price]" class="price" value="0"></td>
						<td class="tax_col" style="padding: 4px 8px; border:1px solid #060606; text-align:right;">-- <input type="hidden" name="items[<?php echo $item['line']; ?>][tax_percentage]" class="tax" value="0"></td>
						<td class="row_total_col" style="padding: 4px 8px; border:1px solid #060606; text-align:right;">$0.00</td>
					</tr>

				<?php } else { ?>
					<tr class='line_item' data-taxes='<?php if(!empty($item['taxes'])){
						echo json_encode($item['taxes']);
					} ?>'>
						<td class="name_col">
							<span class='symbol delete_row'>x</span>
							<input class="item-search" name='items[<?php echo $item['line']; ?>][description]' placeholder='Description' value='<?php echo $item['description']; ?>' />
							<input class='item-id' type='hidden' value='<?php echo $item['item_id']; ?>' name='items[<?php echo $item['line']; ?>][item_id]' />
                            <input type="hidden" name="items[<?php echo $item['line']; ?>][item_type]" value="<?php echo $item['item_type']; ?>">
                            <input class='tax_included' type="hidden" name="items[<?php echo $item['line']; ?>][tax_included]" value="<?php echo $item['unit_price_includes_tax']; ?>">
                        </td>
						<td class="qty_col">
							<input class='quantity line_value' name='items[<?php echo $item['line']; ?>][quantity]' placeholder='Qty' value='<?php echo $item['quantity']; ?>'  />
						</td>
						<td class="price_col" style='border:1px solid #606060;'>
							<span class="symbol">$</span>
							<input class='price line_value' name='items[<?php echo $item['line']; ?>][price]' placeholder='0.00' value='<?php echo $item['price']; ?>' />
						</td>
						<td class="tax_col" style='border:1px solid #606060;'>
							<input <?php if(!empty($item['item_id'])){ echo 'disabled'; } ?> class='tax line_value' name='items[<?php echo $item['line']; ?>][tax_percentage]' placeholder='Tax' value='<?php echo ($item['tax_percentage']); ?>'/>
							<span class="symbol">%</span>
						</td>
						<td class='row_total_col'>
							<?php echo to_currency($item['subtotal']); ?>
						</td>
					</tr>
				<?php } ?>

				<?php $last_line = (int) $item['line'];
				}

				foreach($service_fee_hash as $item){
					?>
					<tr class="line_item">
						<td style='width: 400px; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $item['name']; ?></td>
						<td style='width: 40px; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $item['quantity']; ?></td>
						<td style='width: 40px; padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency($this->config->item('unit_price_includes_tax') ?
								$item['subtotal']
								: $item['price']); ?>
						</td>
						<td style='width: 40px; padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>
							<?php echo to_currency($item['tax']); ?>
						</td>
						<td style='width: 40px; padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>
							<?php echo to_currency($this->config->item('unit_price_includes_tax') ?
								$item['price'] : $item['subtotal']); ?>
						</td>
					</tr>

					<?php
				}
			}?>
			<?php if((!isset($sent)||!$sent) && empty($billing_id)){
			$last_line++; ?>
			<tr class="line_item">
				<td class="name_col">
					<span class='symbol delete_row'>x</span><input class="item-search" name='items[<?php echo $last_line; ?>][description]' placeholder='Description' value='' />
					<input class='item-id' type='hidden' value='0' name='items[<?php echo $last_line; ?>][item_id]' />
                    <input class='item-type' type='hidden' value='' name='items[<?php echo $last_line; ?>][item_type]' />
                    <input class='tax_included' type='hidden' value='' name='items[<?php echo $last_line; ?>][tax_included]' />
                </td>
				<td class="qty_col">
					<input class='quantity line_value' name='items[<?php echo $last_line; ?>][quantity]' placeholder='Qty' value='1'  />
				</td>
				<td class="price_col" style='border:1px solid #606060;'>
					<span class="symbol">$</span>
					<input class='price line_value' name='items[<?php echo $last_line; ?>][price]' placeholder='0.00' value='' />
				</td>
				<td class="tax_col" style='border:1px solid #606060;'>
					<input class='tax line_value' name='items[<?php echo $last_line; ?>][tax_percentage]' placeholder='0.0' value=''/>
					<span class="symbol">%</span>
				</td>
				<td class='row_total_col'>
					<?php echo !empty($item['subtotal']) ? to_currency($item['subtotal']) : ''; ?>
				</td>
			</tr>
			<?php } ?>
			<tr id="current_subtotal_row">
				<td colspan="1">
					<?php if(!isset($sent)||!$sent){ ?>
					<div id="add_item" style="bottom: 0px; top: 5px;">Add Item</div>
					<?php }else{ ?>
					&nbsp;
					<?php } ?>
				</td>
				<td colspan="2" style="font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>; text-align: left;">
					Subtotal
				</td>
				<td  id="current_subtotal" colspan="2" style="padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align: right;">
					<?php echo !empty($current_subtotal) ? to_currency($current_subtotal) : ''; ?>
				</td>
			</tr>
			<tr>
				<td colspan="1">&nbsp;</td>
				<td colspan="2" style="font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>; text-align: left;">
					Tax
				</td>
				<td id="current_tax" colspan="2" style="padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align: right;">
					<?php echo !empty($current_tax) ? to_currency($current_tax) : ''; ?>
				</td>
			</tr>
			<tr>
				<td colspan="1">&nbsp;</td>
				<td colspan="2" style="font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>; text-align: left;">
					New Charges
				</td>
				<td id="current_total_due" colspan="2" style="padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align: right;">
					<?php echo !empty($current_total) ? to_currency($current_total) : ''; ?>
				</td>
			</tr>
		</tbody>
	</table>
	<!-- END CURRENT CHARGES -->

	<?php if(!empty($show_account_transactions) && empty($editing)){ ?>
		
		<!-- INVOICE BALANCE ITEMS -->
		<table cellspacing="0" style="width: <?=empty($pdf)?'580':'800'?>px; margin-bottom: 15px; font-size: 10px;">
			<tbody>
				<tr>
					<td colspan="6">
						<h2 style="color: #AAA; font-weight: bold; font-size: 18px; margin: 0px;">
							Previous Open Invoice Balance (<?php echo date('m/d/Y', strtotime($bill_start)); ?> to <?php echo date('m/d/Y', strtotime($bill_end)); ?>)
						</h2>
					</td>
				</tr>
				<tr>
					<td style='width: 110px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>#</td>
					<td style='width: 45px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>DATE</td>
					<td style='width: 250px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>ITEM</td>
					<td style='width: 40px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>PAYMENT</td>
					<td style='width: 40px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>CHARGE</td>
					<td style='width: 40px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>BALANCE</td>
				</tr>
				<?php 
				if(!empty($account_transactions['invoice'])){
					foreach(array_reverse($account_transactions['invoice']) as $line => $item) {
					//$item['trans_description'] = trim($item['trans_description'], '<br/>');
					$cell_height = ((int) substr_count($item['trans_description'], '<br/>')-1) * 15;
					if($cell_height <= 0){
						$cell_height = 12;
					} 
						
					?>
					<tr style="height: <?php echo $cell_height; ?>px;">
						<td style='width: 110px; height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $item['trans_comment']; ?></td>
						<td style='width: 45px; height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $item['date']; ?></td>
						<?php
						$description = str_replace('<br/>', '<br>', $item['trans_description']);
						if(!empty($item['customer_note'])){
							$description = '<em>NOTE</em> '.$item['customer_note'].'<br>'.$description;
						}
						$description = strlen($description) > 3000 ? substr($description, 0, 3000).' ...' : $description;

                        ?>
						<td style='width: 250px; height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?=$description ?></td>
						<td style='width: 40px; height: <?php echo $cell_height; ?>px; vertical-align: top; text-align: right; padding:<?=$cp?>; border:1px solid <?=$bc?>;'> <?php if ($item['total'] > 0){echo to_currency($item['total']);} ?></td>
						<td style='width: 40px; height: <?php echo $cell_height; ?>px; vertical-align: top; text-align: right; padding:<?=$cp?>; border:1px solid <?=$bc?>;'> <?php if ($item['total'] <= 0){echo to_currency(-$item['total']);} ?></td>
						<td style='width: 40px; height: <?php echo $cell_height; ?>px; vertical-align: top; text-align: right; padding:<?=$cp?>; border:1px solid <?=$bc?>;'> <?php echo to_currency(-$item['running_balance']); ?></td>
					</tr>
					<?php } ?>
				<?php }else{ ?>
					<tr>
						<td colspan="6" style='height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'>
							<em>No transactions available</em>
						</td>
					</tr>				
				<? } ?>
			</tbody>
		</table>
		
		<!-- CUSTOMER ACCOUNT ITEMS -->
		<?php if ($pay_customer_account) { ?>
			<table cellspacing="0" style="width: <?=empty($pdf)?'580':'800'?>px; margin-bottom: 15px; font-size: 10px;">
				<tbody>
					<tr>
						<td colspan="6">
							<h2 style="color: #AAA; font-weight: bold; font-size: 18px; margin: 0px;">
								<?=$customer_credit_nickname?> Transactions (<?php echo date('m/d/Y', strtotime($bill_start)); ?> to <?php echo date('m/d/Y', strtotime($bill_end)); ?>)
							</h2>
						</td>
					</tr>
					<tr>
						<td style='width: 110px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>#</td>
						<td style='width: 45px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>DATE</td>
                        <?php if (!empty($sales_items)) { ?>
                            <td style='width: 200px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>ITEM</td>
                            <td style='width: 25px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>SUB</td>
                            <td style='width: 25px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>TAX</td>
                        <?php } else { ?>
                            <td style='width: 250px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>ITEM</td>
                        <?php } ?>
                        <td style='width: 40px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>PAYMENT</td>
						<td style='width: 40px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>CHARGE</td>
						<td style='width: 40px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>BALANCE</td>
					</tr>
					<?php 
					if(!empty($account_transactions['customer'])){
						foreach(array_reverse($account_transactions['customer']) as $line => $item) {
						//$item['trans_description'] = trim($item['trans_description'], '<br/>');
						$cell_height = ((int) substr_count($item['trans_description'], '<br/>')-1) * 15;
						if($cell_height <= 0){
							$cell_height = 12;
						} 
							
						?>
						<tr style="height: <?php echo $cell_height; ?>px;">
							<td style='width: 110px; height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $item['trans_comment']; ?></td>
							<td style='width: 45px; height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $item['date']; ?></td>
							<?php
							$description = str_replace('<br/>', '<br>', $item['trans_description']);
							if(!empty($item['customer_note'])){
								$description = '<em>NOTE</em> '.$item['customer_note'].'<br>'.$description;
							}
							$description = strlen($description) > 3000 ? substr($description, 0, 3000).' ...' : $description;
                            if (isset($sales_items[$item['sale_id']])) {
                                $description = "<table><tbody>";
                                foreach($sales_items[$item['sale_id']] as $sale_item) {
                                    $description .= "<tr>";
                                    $description .=
                                        "<td style='width: 200px; height: {$cell_height}px; vertical-align: top; padding:{$cp};'>({$sale_item['quantity_purchased']}) {$sale_item['name']}</td>".
                                        "<td style='width: 25px; height: {$cell_height}px; vertical-align: top; text-align: right; padding:{$cp};'>".to_currency($sale_item['subtotal'])."</td>".
                                        "<td style='width: 25px; height: {$cell_height}px; vertical-align: top; text-align: right; padding:{$cp};'>".to_currency($sale_item['tax'])."</td>";
                                    $description .= "</tr>";
                                }
                                $description .= "</tbody></table>";
                                echo "<td colspan=3 style='width: 250px; height: {$cell_height}px; vertical-align: top; border:1px solid {$bc};'>{$description}</td>";
                            } else {
                                ?>
                                <td style='width: 250px; height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?=$description?></td>
                            <?php } ?>
                            <td style='width: 40px; height: <?php echo $cell_height; ?>px; vertical-align: top; text-align: right; padding:<?=$cp?>; border:1px solid <?=$bc?>;'> <?php if ($item['total'] > 0){echo to_currency($item['total']);} ?></td>
							<td style='width: 40px; height: <?php echo $cell_height; ?>px; vertical-align: top; text-align: right; padding:<?=$cp?>; border:1px solid <?=$bc?>;'> <?php if ($item['total'] <= 0){echo to_currency(-$item['total']);} ?></td>
							<td style='width: 40px; height: <?php echo $cell_height; ?>px; vertical-align: top; text-align: right; padding:<?=$cp?>; border:1px solid <?=$bc?>;'> <?php echo to_currency(-$item['running_balance']); ?></td>
						</tr>
						<?php } ?>
					<?php }else{ ?>
						<tr>
							<td colspan="6" style='height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'>
								<em>No transactions available</em>
							</td>
						</tr>				
					<? } ?>
				</tbody>
			</table>
		<?php } ?>
		
		<!-- MEMBER ACCOUNT ITEMS -->
		<?php if ($pay_member_account) { ?>
			<table cellspacing="0" style="width: <?=empty($pdf)?'580':'800'?>px; margin-bottom: 15px; font-size: 10px;">
				<tbody>
					<tr>
						<td colspan="6">
							<h2 style="color: #AAA; font-weight: bold; font-size: 18px; margin: 0px;">
								<?=$member_nickname?> Transactions (<?php echo date('m/d/Y', strtotime($bill_start)); ?> to <?php echo date('m/d/Y', strtotime($bill_end)); ?>)
							</h2>
						</td>
					</tr>
					<tr>
						<td style='width: 110px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>#</td>
						<td style='width: 45px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>DATE</td>
                        <?php if (!empty($sales_items)) { ?>
                            <td style='width: 200px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>ITEM</td>
                            <td style='width: 25px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>SUB</td>
                            <td style='width: 25px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>TAX</td>
                        <?php } else { ?>
						<td style='width: 250px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>ITEM</td>
                        <?php } ?>
						<td style='width: 40px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>PAYMENT</td>
						<td style='width: 40px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>CHARGE</td>
						<td style='width: 40px; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>BALANCE</td>
					</tr>
					<?php 
					if(!empty($account_transactions['member'])){
						foreach(array_reverse($account_transactions['member']) as $line => $item) {
                        //$item['trans_description'] = trim($item['trans_description'], '<br/>');
						$cell_height = ((int) substr_count($item['trans_description'], '<br/>')-1) * 15;
						if($cell_height <= 0){
							$cell_height = 12;
						} 
							
						?>
						<tr style="height: <?php echo $cell_height; ?>px;">
							<td style='width: 110px; height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $item['trans_comment']; ?></td>
							<td style='width: 45px; height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $item['date']; ?></td>
							<?php
							$description = str_replace('<br/>', '<br>', $item['trans_description']);
							if(!empty($item['customer_note'])){
								$description = '<em>NOTE</em> '.$item['customer_note'].'<br>'.$description;
							}
							$description = strlen($description) > 3000 ? substr($description, 0, 3000).' ...' : $description;
                            if (isset($sales_items[$item['sale_id']])) {
                                $description = "<table><tbody>";
                                foreach($sales_items[$item['sale_id']] as $sale_item) {
                                    $description .= "<tr>";
                                    $description .=
                                        "<td style='width: 200px; height: {$cell_height}px; vertical-align: top; padding:{$cp};'>({$sale_item['quantity_purchased']}) {$sale_item['name']}</td>".
                                        "<td style='width: 25px; height: {$cell_height}px; vertical-align: top; text-align: right; padding:{$cp};'>".to_currency($sale_item['subtotal'])."</td>".
                                        "<td style='width: 25px; height: {$cell_height}px; vertical-align: top; text-align: right; padding:{$cp};'>".to_currency($sale_item['tax'])."</td>";
                                    $description .= "</tr>";
                                }
                                $description .= "</tbody></table>";
                            echo "<td colspan=3 style='width: 250px; height: {$cell_height}px; vertical-align: top; border:1px solid {$bc};'>{$description}</td>";
                            } else {
                            ?>
							<td style='width: 250px; height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?=$description?></td>
                            <?php } ?>
                            <td style='width: 40px; height: <?php echo $cell_height; ?>px; vertical-align: top; text-align: right; padding:<?=$cp?>; border:1px solid <?=$bc?>;'> <?php if ($item['total'] > 0){echo to_currency($item['total']);} ?></td>
							<td style='width: 40px; height: <?php echo $cell_height; ?>px; vertical-align: top; text-align: right; padding:<?=$cp?>; border:1px solid <?=$bc?>;'> <?php if ($item['total'] <= 0){echo to_currency(-$item['total']);} ?></td>
							<td style='width: 40px; height: <?php echo $cell_height; ?>px; vertical-align: top; text-align: right; padding:<?=$cp?>; border:1px solid <?=$bc?>;'> <?php echo to_currency(-$item['running_balance']); ?></td>
						</tr>
						<?php } ?>
					<?php }else{ ?>
						<tr>
							<td colspan="6" style='height: <?php echo $cell_height; ?>px; vertical-align: top; padding:<?=$cp?>; border:1px solid <?=$bc?>;'>
								<em>No transactions available</em>
							</td>
						</tr>				
					<? } ?>
				</tbody>
			</table>
		<?php } ?>
	
	<!-- END MEMBER ACCOUNT ITEMS -->
	<?php } ?>
</div>
<div style="page-break-before:always;"></div>
<?php if(!empty($pdf)) { ?>
</body>
</html>
<?php } ?>
