<style>
	#feedback_lightbox
	{
	    width:350px;
	    z-index:9999;
	    position:relative;
	    top:20%;
	    left:30%;
	}
	#feedback_overlay_box
	{
	 position:absolute;
	 top:0;
	 left:0;
	 background:#000000;
	 overflow:hidden;
	 z-index:9998;
	 width:100%;
	 display:none;
	 height:100%;
	}
	#feedback_close
	{
	display:block;
	height:30px;
	text-align:right;
	width:350px;
	}
	#feedback_close a
	{
	    display:block;
	    background:url(images/close_30.png) no-repeat top left;
	    width:30px;
	    float:right;
	    height:30px;
	}
	#feedback_form
	{
	}
	#feedback_form p
	{
	padding:0px 10px 10px 10px;
	margin:0px;
	display:block;
	}
	#feedback_form p label
	{
	display:block;
	font-size:18px;
	line-height:16px;
	padding:3px 0px 1px;
	color:#336699;
	}
	#feedback_form p .feedback_text,#feedback_form p .feedback_textarea
	{
	    border:1px solid #999999;
	 
	}
	#feedback_error
	{
	margin:10px 0px 0px 0px;
	border:1px solid red;
	padding:10px;
	overflow:hidden;
	display:none;
	}
	#feedback_response
	{
	margin:10px 0px 10px 0px;
	border:1px solid green;
	padding:10px;
	color:green;
	overflow:hidden;
	display:none;
	}
</style>
<?php
echo form_open('system/send_feedback',array('id'=>'feedback_form'));
?>
	<ul id="error_message_box"></ul>
	<fieldset id="feedback_info">
	<legend><?php echo lang("common_feedback"); ?></legend>
	<p>
		<label>Name :</label>
		<input type="text" name="name"  id="feedback_name" class="feedback_text" />
	</p>
	<p>
		<label>Email :</label>
		<input type="text" name="email" id="feedback_email" class="feedback_text" />
	</p>
	<p>
		<label>Subject :</label>
		<input type="text" name="subject" id="feedback_subject" class="feedback_text" />
	</p>
	<p>
		<label>Message :</label>
		<textarea name="message" class="feedback_textarea" id="feedback_message"></textarea>
	</p>
	<p>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit',
			'value'=>lang('common_submit'),
			'class'=>'submit_button float_right')
		);
		
		?>
	</p>
	</fieldset>
<?php 
echo form_close();
?>    
<script type='text/javascript'>        
$(document).ready(function()
{
	var submitting = false;
    $('#feedback_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				$.colorbox.close();
				post_person_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			name: "required",
			email: "email",
			subject: "required",
			message: "required"
   		},
		messages: 
		{
     		name: "<?php echo lang('common_name_required'); ?>",
     		email: "<?php echo lang('common_email_invalid_format'); ?>",
			subject: "<?php echo lang('common_subject_required'); ?>",
			message: "<?php echo lang('common_message_required'); ?>"
		}
	});
});
</script>    
 
    