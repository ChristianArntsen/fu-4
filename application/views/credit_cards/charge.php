<ul id="error_message_box"></ul>
<?php echo form_open('credit_cards/charge_credit_card/'.$credit_card_info->credit_card_id, array('id'=>'charge_card_form')); ?>
<fieldset id="credit_card_basic_info">
<legend>Charge Card</legend>
    <div class="field_row clearfix">
	<?php echo form_label(lang('courses_name').':', 'courses_name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php 
		echo $course_info->name;
		?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('billings_credit_card').':', 'courses_name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php 
		echo $credit_card_info->card_type.' - '.$credit_card_info->masked_account;
		?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('billings_email').':', 'contact_email',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'contact_email',
			'id'=>'contact_email',
			'value'=>'')
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('billings_add_charges').':', 'saved_cards',array('class'=>'wide')); ?>
		<div class='form_field'>
			
		</div>
	</div>
	<div class='field_row clearfix'>
		<!-- List all of the saved credit cards -->
		<table>
			<thead>
				<tr>
					<th>Description</th>
					<th>Amount</th>
					<th></th>
				</tr>
			</thead>
			<tbody id='charge_items'>
				<tr>
					<td>
						<?php
						echo form_input(array(
							'autocomplete'=>'off',
							'name'=>'description',
							'id'=>'description',
							'value'=>'')
						);
						?>
					</td>
					<td>
						<?php
						echo form_input(array(
							'autocomplete'=>'off',
							'name'=>'amount',
							'id'=>'amount',
							'value'=>'')
						);
						?>
					</td>
					<td><a href='javascript:credit_cards.add_item()'>Add Item</a></td>
				</tr>
			</tbody>
			<tbody id='totals'>
				<tr>
					<td>Subtotal:</td>
					<td id='subtotal'>$0.00</td>
					<td></td>
				</tr>
				<tr>
					<td>
						<?php echo form_label(lang('billings_tax_name').':', 'tax_name',array('class'=>'wide')); ?>
						<?php echo form_input(array(
							'autocomplete'=>'off',
							'name'=>'tax_name',
							'id'=>'tax_name',
							'value'=>'')
						);?>
						Tax Rate: 
						<?php echo form_input(array(
							'autocomplete'=>'off',
							'name'=>'tax_rate',
							'id'=>'tax_rate',
							'value'=>'')
						);?>
						Taxes:
					</td>
					<td id='taxes'>$0.00</td>
					<td></td>
				</tr>
				<tr>
					<td>Total:</td>
					<td id='total'>$0.00</td>
					<td></td>
				</tr>
			</tbody>
		</table>	
	</div>
	<!--div class="field_row clearfix">
	<?php echo form_label(lang('billings_totals').':', 'saved_cards',array('class'=>'wide')); ?>
		<div class='form_field'>
			
		</div>
	</div-->
	
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('billings_charge_card'),
		'class'=>'submit_button float_right')
	);
	?>
</fieldset>
</form>
<style>
	#course_name {
		width:250px;
	}
	#saved_credit_cards {
		min-height:50px;
	}
	.delete_credit_card {
		
	}
	.name_on_card {
		
	}
	.card_type {
		
	}
	.last_four {
		
	}
	.expiration {
		
	}
	#description {
		width:400px;
		margin-left:10px;
	}
	#amount {
		width:70px;
	}
	.description {
		padding-left:20px;
	}
	.amount {
		text-align:right;
	}
	#totals {
		text-align:right;
		border-top:1px solid #ccc;
	}
	#tax_name, #tax_rate {
		width:50px;
	}
</style>
<script>
	$(document).ready(function(){
		/*$('#submit').click(function(){
			if ($('#course_id').val() != '' && $('#course_id').val() != -1)
			{
				$.colorbox({
					href:'index.php/credit_cards/open_course_card_window/',
					'onComplete':function() {
	               		$.colorbox.resize({width:600, height:560});
	               	}
				});
			}
			else
				alert('You must select a course before you can add a credit card');
		});*/
		$( "#course_name" ).autocomplete({
			source: "<?php echo site_url('courses/course_search');?>",
			delay: <?=$this->config->item("search_delay") ?>,
			autoFocus: false,
			minLength: 0,
			select: function(event, ui) {
				event.preventDefault();
				$("#course_name").val(ui.item.label);
				$("#course_id").val(ui.item.value);
				credit_cards.get();
			},
			focus: function(event, ui) {
				event.preventDefault();
				//$("#teetime_title").val(ui.item.label);
			}
		});
		$('#tax_rate').keyup(function(){
			credit_cards.update_charge_taxes();
		});
		console.log('about to set validate');
		var submitting = false;
    	$('#charge_card_form').validate({
			submitHandler:function(form)
			{
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				$(form).ajaxSubmit({
					success:function(response)
					{
						$.colorbox.close();
						post_item_form_submit(response);
					},
					dataType:'json'
				});
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules:{	 		},
			messages:{	 		}
		});
		console.log('done setting validate');
	})
</script>