<ul id="error_message_box"></ul>
<fieldset id="credit_card_basic_info">
<legend>Credit Card Info</legend>
    <!--div class="field_row clearfix">
	<?php echo form_label(lang('billings_credit_card_').':', 'courses_name',array('class'=>'wide required')); ?>
		<div class='form_field'>
		<?php 
		echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'course_id',
			"$disabled"=>"$disabled",
			'id'=>'course_id',
			'value'=>$credit_card_info->course_id)
		);
		
		?>
		</div>
	</div-->
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_name').':', 'courses_name',array('class'=>'wide required')); ?>
		<div class='form_field'>
		<?php 
			$disabled = (empty($credit_card_info) || $credit_card_info->credit_card_id == '')?'':'disabled';
		echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'course_name',
			"$disabled"=>"$disabled",
			'id'=>'course_name',
			'value'=>$course_name)
		);
		echo form_hidden('course_id', $course_id);
		
		?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('billings_saved_cards').':', 'saved_cards',array('class'=>'wide')); ?>
		<div class='form_field'>
		</div>
	</div>
	<div class='field_row clearfix'>
		<!-- List all of the saved credit cards -->
		<table>
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th>Name on Card</th>
					<th>Card Type</th>
					<th>Last Four</th>
					<th>Token Expiration</th>
				</tr>
			</thead>
			<tbody id='saved_credit_cards'>
				
			</tbody>
		</table>	
	</div>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('billings_add_card'),
		'class'=>'submit_button float_right')
	);
	?>
</fieldset>
<style>
	#course_name {
		width:250px;
	}
	#saved_credit_cards {
		min-height:50px;
	}
	.delete_credit_card {
		
	}
	.name_on_card {
		
	}
	.card_type {
		
	}
	.last_four {
		
	}
	.expiration {
		
	}
</style>
<script>
	$(document).ready(function(){
		$('#submit').click(function(){
			if ($('#course_id').val() != '' && $('#course_id').val() != -1)
			{
				$.colorbox({
					href:'index.php/credit_cards/open_course_card_window/',
					'onComplete':function() {
	               		$.colorbox.resize({width:800, height:560});
	               	}
				});
			}
			else
				alert('You must select a course before you can add a credit card');
		});
		$( "#course_name" ).autocomplete({
			source: "<?php echo site_url('courses/course_search');?>",
			delay: <?=$this->config->item("search_delay") ?>,
			autoFocus: false,
			minLength: 0,
			select: function(event, ui) {
				event.preventDefault();
				$("#course_name").val(ui.item.label);
				$("#course_id").val(ui.item.value);
				credit_cards.get();
			},
			focus: function(event, ui) {
				event.preventDefault();
				//$("#teetime_title").val(ui.item.label);
			}
		});
	})
</script>