<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title><?php echo $page_name ?> - Password Reset</title>
	<link href="<?php echo base_url(); ?>css/dist/online-booking.min.css" type="text/css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>js/jquery-1.10.2.min.js"></script>
	<script src="<?php echo base_url(); ?>js/bootstrapValidator.0.5.3.js"></script>
	<style>
	body {
		background: url('<?php echo base_url(); ?>images/grid_noise.png');
	}
	
	.module {
		box-shadow: 1px 1px 1px 0px #CCC;
		border-top: 1px solid #DFDFDF; 
		border-left: 1px solid #DFDFDF;
		background-color: #FFFFFF;
		padding-bottom: 4em;
	}	
	
	#unavailable {
		background-color: white;
		border: 1px solid #E0E0E0;
		padding: 20px;
	}
	
	h1 {
		text-align: center;
		line-height: 1em;
		margin: 0.5em 0 0.5em 0;
		font-size: 2.5em;
	}
	
	label {
		display: block;
		float: left;
		width: auto;
	}
	
	div.field {
		float: left;
	}
	
	div.col-md-6 {
		overflow: hidden;
	}
	
	p {
		padding: 1em 0em;
	}
	</style>	
</head>
<body>
<nav class="navbar navbar-default" role="navigation" id="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><?php echo $page_name ?></a>
		</div>
	</div>
</nav>	
<div class="container">
	<?php if($valid_key){ ?>
	<div class="row">
		<div class="col-md-6 col-md-offset-3 module">
			<form class="form" method="post" id="password-reset" action="<?php echo site_url('api/booking/users/reset_password'); ?>">
				<h1>Password Reset</h1>
				<div id="server-response" class="alert" style="display: none;"></div>				
				<input id="api_key" type="hidden" name="api_key" value="no_limits" />
				<input id="key" type="hidden" name="key" value="<?php echo $reset_key; ?>" />
				<div class="form-group">
					<label for="password">New Password</label>
					<input id="password" class="form-control" type="password" name="password" data-bv-notempty-message="Field can not be blank" required />
				</div>
				<div class="form-group">
					<label for="confirm_password">Confirm New Password</label>
					<input id="confirm_password" class="form-control" type="password" name="confirm_password" data-bv-notempty-message="Field can not be blank" required />
				</div>
				<button class="btn btn-primary">Reset</button>					
			</form>
		</div>
	</div>
	<?php }else{ ?>
	
	<div class="row">
		<div class="col-md-12">
			<h1 class="text-danger">Password reset expired</h1>				
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<p class="alert alert-danger">The password reset link has expired or is invalid. 
			Please re-send the password reset email.</p>
		</div>
	</div>	
	<?php } ?>
</div>	
</div>
<script>
$(function(){
	
	$('#password-reset').bootstrapValidator({
		fields: {
            password: {
                validators: {
                    identical: {
                        field: 'confirm_password',
                        message: 'Passwords do not match'
                    },
                    stringLength: {
                        message: 'Password must be at least 6 characters',
                        min: 1
                    }
                }
            },
            confirm_password: {
                validators: {
                    identical: {
                        field: 'password',
                        message: 'Passwords do not match'
                    },
                    stringLength: {
                        message: 'Password must be at least 6 characters',
                        min: 1
                    }
                }
            }
        }		
	});
	
	$('#password-reset').on('submit', function(e){
		e.preventDefault();
		
		$(this).bootstrapValidator('validate');
		if(!$(this).data('bootstrapValidator').isValid()){
			return false;
		} 	
		
		var url = $(this).attr('action');
		var data = $(this).serialize();
		
		$.post(url, data, function(response){
			
			if(response.success){
				$('#server-response')
					.removeClass('alert-danger')
					.addClass('alert-success')
					.text('Password successfully reset. Redirecting to online booking')
					.show();
                $('#password-reset .form-group, #password-reset .btn-primary').hide();
                setTimeout(function (){
                    window.location = "<?=$booking_url?>";
                }, 3000);

			}else{
				$('#server-response')
					.removeClass('alert-success')
					.addClass('alert-danger')
					.text(response.msg)
					.show();
			}
			
		},'json').fail(function(response){
			$('#server-response')
				.removeClass('alert-success')
				.addClass('alert-danger')
				.text(response.responseJSON.msg)
				.show();			
		});
		 
		return true;
	});
});
</script>
</body>
</html>
