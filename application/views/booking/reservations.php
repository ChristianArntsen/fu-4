<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta charset="utf-8">
	<title><?php echo $page_name; ?> - Online Booking</title>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<link href="<?php echo base_url(); ?>css/dist/online-booking.min.css" type="text/css" rel="stylesheet" />

	<script>
	PAGE_NAME = '<?php echo addslashes($page_name); ?>';
	BASE_API_URL = '<?php echo site_url('api/booking'); ?>/';
	BASE_URL = '<?php echo site_url('booking'); ?>/index/';
	SITE_URL = '<?php echo site_url('booking'); ?>/';
	URL = '<?php echo base_url(); ?>';
	COURSE_ID = <?php echo (int) $this->session->userdata('course_id'); ?>;
	API_KEY = 'no_limits';
	
	<?php if(empty($group_id)){ ?>
	COURSES = [];
	COURSE = <?php echo json_encode($course); ?>;
	IS_GROUP = false;
	
	<?php }else{ ?>
	COURSES = <?php echo json_encode($courses); ?>;
	COURSE = {};
	IS_GROUP = true;
	<?php } ?>

	DEFAULT_FILTER = <?php echo json_encode($filter); ?>;
	SCHEDULES = <?php echo json_encode($schedules); ?>;
	USER = <?php echo json_encode($user); ?>;
	SETTINGS = <?php echo json_encode($settings); ?>;
    SETTINGS.enable_captcha_online = SETTINGS.enable_captcha_online == "1"
    <?php
    $credit_card_payment = $this->session->userdata('credit_card_payment');
    if (!empty($credit_card_payment['foreign_currency_type'])) { ?>
    FOREIGN_TOTAL = '<?=$credit_card_payment['foreign_total']?>';
    USD_TOTAL = '<?=$credit_card_payment['amount']?>';
    FOREIGN_EXCHANGE_RATE = '<?=$credit_card_payment['foreign_exchange_rate']?>';
    FOREIGN_CURRENCY_SYMBOL = '<?=$credit_card_payment['foreign_currency_symbol']?>';
    FOREIGN_CURRENCY_TYPE = '<?=$credit_card_payment['foreign_currency_type']?>';
    <?php } else { ?>
    FOREIGN_CURRENCY_TYPE = '';
    <?php } ?>
	FOREUP_DISCLAIMER = 'By selecting "Pay Online" you agree to the following terms: \
		The tee time is non-cancelable and non-refundable. \
		If the course is closed due to weather, you may be eligible for a rain check. \
		Booking fees are non-refundable. Fees are 100% due to ForeUP Inc. Charge will be in USD. \
		If local currency is not USD, exchange rates will be used to charge the USD equivalent. \
		If you cannot make your tee time, contact the course at least 24 hours \
		prior to your reservation and they may be able to reschedule your time. \
		Your billing statement will reflect this charge as coming from "ForeUP"';
	
	PAYMENT_WARNING = 'Clicking the "Submit" button will submit an active payment. \
		Clicking this button more than once may result in multiple charges. \
		You may be liable for additional fees incurred. If you have any issues, \
		do not click "Submit" again, but contact the facility. Thank you.';

    TERMS_AND_CONDITIONS = <?php echo json_encode($this->config->item('terms_and_conditions')); ?>;
	</script>
	<style>
	body {
		background: url('<?php echo base_url(); ?>images/grid_noise.png');
	}

	p.foreup-disclaimer {
		font-size: 12px;
		color: #999;
		margin-top: 10px;
	}

	#notification {
		margin: 1em;
		position: absolute;
		top: 1em;
		left: 1em;
		right: 1em;
		display: block;
		z-index: 10000;
	}
	
	nav.navbar {
		margin: 0px;
	}
	
	.module, .module-mobile {
		box-shadow: 1px 1px 1px 0px #CCC;
		border-top: 1px solid #DFDFDF; 
		border-left: 1px solid #DFDFDF;
		background-color: #FFFFFF;
		padding-bottom: 4em;
	}	
	
	@media (min-width: 768px) {
		.module-mobile {
			background-color: transparent;	
			border: none;
			box-shadow: none;
		}
	}
	
	div.booking-classes button.btn {
		padding: 0.5em;
		font-size: 1.2em;
		margin-top: 1em;
	}
	
	#booking_class {
		padding-top: 1em;
		padding-bottom: 1em;
		font-size: 1.2em;
	}
	
	#times .loading {
		position: absolute;
		top: 0px; 
		right: 0px;
		bottom: 0px;
		left: 0px;
		display: block;
		z-index: 10;
		text-align: center;
		background: white;
		padding-top: 5em;
		border: none;
	}

	#times .loading h1 {
		color: #CCC;
	}
	
	.time {
		display: block;
		list-style-type: none;
		margin: 0px;
		padding: 0px;
	}
	
	.time span.special {
		font-size: 0.9em;
		font-weight: normal;
		color: white;
		display: block;
		float: none;
		border-top-left-radius: 0px;
		border-top-right-radius: 0px;
		border: 1px solid #d68a44;
		position: absolute;
		bottom: -1px;
		left: -1px;
		right: -1px;
		margin: 0px;
		padding: 0px;
		text-align: center;
		line-height: 20px;
	}
	
	.time h6 {
		margin: 0px 0px 5px 0px;
		color: #777;
	}

	.time h4.start {
		margin: 0px 0px 5px 0px;
		font-size: 1.4em;
		font-weight: bold;
		display: block;		
	}
	
	.time.aggregate > div {
		height: 100px;
	}

	.time > div {
		height: 82px;
		display: block;
		list-style-type: none;		
		border: 1px solid #DFDFDF;
		border-bottom: 1px solid #CCC;
		display: block;
		float: none;
		width: auto;
		padding: 10px;
		overflow: hidden;
		margin: 5px 5px 0px 0px;
		position: relative;
		
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		border-radius: 5px;
		
		background: #fcfcfc;
		background: -moz-linear-gradient(top,  #fcfcfc 1%, #efefef 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#fcfcfc), color-stop(100%,#efefef));
		background: -webkit-linear-gradient(top,  #fcfcfc 1%,#efefef 100%);
		background: -o-linear-gradient(top,  #fcfcfc 1%,#efefef 100%);
		background: -ms-linear-gradient(top,  #fcfcfc 1%,#efefef 100%);
		background: linear-gradient(to bottom,  #fcfcfc 1%,#efefef 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfcfc', endColorstr='#efefef',GradientType=0 );
	}
	
	.time:hover > div {
		background: #eff9ff;
		background: -moz-linear-gradient(top,  #eff9ff 0%, #d3e3ed 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#eff9ff), color-stop(100%,#d3e3ed));
		background: -webkit-linear-gradient(top,  #eff9ff 0%,#d3e3ed 100%);
		background: -o-linear-gradient(top,  #eff9ff 0%,#d3e3ed 100%);
		background: -ms-linear-gradient(top,  #eff9ff 0%,#d3e3ed 100%);
		background: linear-gradient(to bottom,  #eff9ff 0%,#d3e3ed 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eff9ff', endColorstr='#d3e3ed',GradientType=0 );
		cursor: pointer;
	}
	
	.time.empty:hover {
		background-color: transparent !important;
	}		
	
	.time.empty {
		text-align: center;
		padding-top: 5em;
		color: #BBB;
	}
	
	.time.empty p {
		font-size: 1.5em;
	}
	
	.time.empty h1 {
		font-weight: bold;
		font-size: 3em;
		color: #888;
	}
	
	.time:last-child {
		border-bottom: none;
	}
		
	.time div.price {
		float: right;
		margin: 0;
		padding-bottom: 15px;
	}
	
	.time div.price > span {
		display: block;
		overflow: hidden;
		float: none;
		margin-top: 0;
		margin-bottom: 0.25em;
		color: #222;
	}
	
	.time div.pull-left, .time div.pull-right {
		margin: 0px;
		padding: 0px;
		float: left;
		overflow: hidden;	
	}
	
	.time span {
		float: left;
		display: block;
		font-size: 1.2em;
		line-height: 1.2em;
		margin-left: 0.25em;
		overflow: hidden;
		color: #666;
	}
	
	.time .holes, .time .spots {
		overflow: hidden;
		float: left;
	}
	
	.time .holes {
		margin-right: 0.75em;
	}
	
	.time i.glyphicon {
		font-size: 1em;
		float: left;
		display: block;
		color: #666 !important;
	}
	
	.time i.icon-golf-cart, .time i.icon-golf-ball-tee {
		margin-right: 5px;
	}	
	
	/* If times are table style (for private clubs) */
	table#times {
		padding-left: 15px;
		padding-right: 15px;
	}
	
	#times td.details {
		position: relative;
	}
	
	#times td.details > div.wrapper {
		min-height: 62px;
		display: block;
		padding: 0px;
		margin: 0px;
	}
	
	#times td.details .special {
		border-radius: 0px;
		position: absolute;
		display: block;
		padding: 0px;
		margin-top: 20px;
	}
	
	#times thead {
		background-color: #F0F0F0;
		border: none;
	}
	
	#times thead th {
		border: 1px solid #D9D9D9;
	}
	
	#times tr.time {
		width: auto;
		float: none;
		display: table-row;
	}
	
	#times tr.time td {
		border: 1px solid #D9D9D9;
		border-top: none;
	}
	
	#times div.price {
		padding-bottom: 0px;
	}
	
	#times .details {
		width: 115px;
	}
	
	#times .reserve {
		width: 5%;
		vertical-align: middle;
	}
	
	#times .reserve .btn {
		display: inline;
	}
	
	#times .player {
		width: 15%
		font-size: 1.1em;
	}
	
	#times h4.start {
		width: 100px;
	}
	/* End table view of tee times */
	
	#nav div.row > div {
		padding-bottom: 1em;
	}
	
	#nav > div.row {
		padding-top: 1em;
	}
	
	#date-menu {
		text-align: center;
	}
	
	#date {
		display: block;
		overflow: hidden;
		margin: 0px auto 0px auto;
		width: 230px;
	}
	
	#date > div.datepicker-inline {
		margin: 0px !important;
	}
	
	.time-details label, div.confirmation label {
		font-weight: bold;
		display: block;
		color: #AAA;
		margin-bottom: 2px;
	}
	
	div.confirmation label {
		color: #222;
	}
	
	div.confirmation span.field {
		margin-bottom: 1em;
		display: block;
	}
	
	div.confirmation div.well {
		margin-bottom: 0px;
	}
	
	@media (max-width: 767px) {
		div.confirmation .thanks {
			margin-top: 1em;
		}
	}
	
	.time-details span.field, div.confirmation span.field {
		font-size: 1.3em;
		line-height: 1.3em;
	}
	
	.time-details span.total {
		font-size: 1.8em;
	}
	
	.time-details.purchase span.total, .time-details.purchase span.subtotal, .time-details.purchase span.discount {
		font-size: 1.2em;
	}
	
	.time-details div.row > div {
		margin-bottom: 1em;
	}
	
	.time-details div.row.purchase > div {
		margin-bottom: 0.5em;
	}
	
	.time-details div.row.purchase label {
		color: #444;
		font-weight: normal;
		float: left;
		width: 100px;
		font-size: 1.2em;
	}	
		
	table.private-times .time .price {
		float: left;
		margin-top: 5px;
	}
	
	table.private-times .time .price span {
		margin-left: -5px;
	}
		
	@media (max-width: 768px) {	
		.time-details .modal-footer .btn {
			display: block;
			width: 100%;
		}
	}
	
	address {
		display: block;
		overflow: hidden;
	}
	
	#account-transactions {
		margin-top: 15px;
	}

	ul.nav span.glyphicon {
		margin-right: 0.5em;
	}
	
	ul.reservations, ul.credit-cards {
		display: block;
		overflow: hidden;
		margin: 0px;
		padding: 0px;
	}
	
	ul.reservations > li, ul.credit-cards > li {
		list-style-type: none;
		display: block;
		overflow: hidden;
		padding: 8px 0px 8px 0px;
		border-bottom: 1px solid #E0E0E0;
	}
	
	ul.credit-cards > li img.type {
		margin-right: 1em;
		display: block;
	}
	
	ul.reservations > li h4, ul.credit-cards > li h4 {
		display: block;
		margin-top: 0px;
		margin-bottom: 2px;
	}
	
	ul.reservations > li span.details {
		color: #888;
		margin-right: 25px;
	}
	
	ul.reservations > li .holes, ul.reservations > li .players {
		display: block;
		float: left;
		font-size: 1em;
		color: #666;
		margin-right: 25px;
	} 
	
	form.account label {
		font-weight: bold;
		color: #444;
		margin-bottom: 3px;
		margin-top: 6px;
	}
	
	form.account input {
		margin-bottom: 5px;
	}
	
	table.purchases .total {
		text-align: right;
		font-weight: bold;
	}
	
	#account-balances div.panel div.panel-body {
		padding: 5px 15px;
	}
	
	div.address {
		display: block;
		padding-left: 20px;
	}
	
	address > span, address > a {
		display: block;
		margin-bottom: 5px;
	}
	
	address span.glyphicon {
		margin-right: 0.5em;
	}
	
	div.address > span {
		display: block;
		margin-left: 0.5em;
	}
	
	button + span.success-msg {
		margin-left: 10px;
	}
	
	span.success-msg > span.glyphicon {
		margin-right: 5px;
	}
	
	#mercury_loader {
		display: block;
		z-index: 100000;
		position: absolute;
		text-align: center;
		padding-top: 75px;
		bottom: 0px;
		top: 0px;
		left: 0px;
		right: 0px;
		margin: 0px;
		background: rgba(255,255,255,0.25);
		padding: 50px;
	}
	
	#mercury_loader h2 {
		margin-bottom: 50px;
	}
	
	div.invoice-details div.dates label {
		float: left;
		display: block;
	}
	
	div.invoice-details div.dates span.line {
		display: block;
		overflow: hidden;
	}
	
	div.invoice-details div.dates span.date {
		float: right;
		display: block;
	}
	
	div.invoice-details table .price {
		text-align: right;
	}
	
	div.invoice-details table thead, div.invoice-details table.totals th {
		background-color: #F0F0F0;
	}
	
	div.invoice-details table.totals th {
		border-right: 1px solid #E0E0E0;
	}
	
	div.invoice-details table {
		border: 1px solid #E0E0E0;
	}
	
	div.invoice-details table.totals td {
		text-align: right;
	}
	
	#payment_methods {
		display: block;
		overflow: hidden;
		margin: 0px;
		padding: 0px 0px 0px 2px;
		width: auto;
	}
	
	#payment_methods > li {
		display: block;
		margin: 0px;
		padding: 0px;
		list-style-type: none;
		float: none;
	}
	
	#payment_methods > li > label {
		display: block;
		margin: 0px;
		padding: 10px;
		float: none;
		width: auto;
		cursor: pointer;
		overflow: hidden;
	}
	
	#payment_methods > li > label.selected {
		border-left: 2px solid #37B455;
		margin-left: -2px;
		background-color: #F6F6F6;
	}
	
	#payment_methods input {
		float: left;
		display: block;
		margin: 5px 10px 5px 0px;
		padding: 0px;
	}
	
	#payment_methods h4 {
		display: block;
		margin: 0px;
		float: left;
		clear: right;
		color: #222;
	}
	
	#payment_methods h4.price {
		float: right;
		clear: none;
		font-weight: bold;
	}
	
	#payment_methods span.details {
		display: block;
		float: left;
		margin-left: 23px;
		font-weight: normal;
	}
	
	#payment_methods span.discount {
		float: right;
		font-weight: normal;
		font-size: 1em;
	}
	
	#payment_methods div.method, #payment_methods div.details {
		overflow: hidden;
		display: block;
		margin: 0px;
		padding: 0px;
		height: auto;
		width: auto;
	}

	div.row.player {
		margin-bottom: 10px;
	}

	.input-group span.twitter-typeahead {
		width: 100%;
	}

	span.twitter-typeahead > input.form-control {
		float: none !important;
		border-top-left-radius: 0;
		border-bottom-left-radius: 0;
	}

	.tt-query {
	  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	     -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {
		position: absolute;
		top: 100%;
		left: 0;
		z-index: 100;
		padding: 5px 0;
		margin: 12px 0px 0px 0px;
		font-size: 14px;
		list-style: none;
		background-color: #fff;
		background-clip: padding-box;
		border: 1px solid #ccc;
		border: 1px solid rgba(0,0,0,.15);
		border-radius: 4px;
		-webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
		box-shadow: 0 6px 12px rgba(0,0,0,.175);
		width: 450px;
		color: #444;
		max-height: 400px;
		overflow-y: auto;
	}

	.tt-suggestion {
		padding: 3px 20px;
		font-size: 1em;
		line-height: 1.4em;
	}

	.tt-suggestion .details {
		color: #666;
		line-height: 0.8em;
		font-size: 0.8em;
	}

	.tt-suggestion:hover .details {
		color: white;
	}

	.tt-suggestion.tt-cursor, .tt-suggestion:hover{
		color: #fff;
		background-color: #0097cf;
		cursor: pointer;
	}

	.tt-suggestion p {
		margin: 0;
	}

	span.page-number {
		font-size: 14px;
		padding: 6px 12px;
	}

	.giftcard label.highlight {
		border: 1px solid blue;
		background-color: rgba(0,0,255, 0.1);
	}

	.giftcard.form-horizontal div.radio {
		line-height: 27px;
		padding-top: 0px;
	}

	.giftcard.form-horizontal .radio-inline {
		padding-top: 0px;
		line-height: 27px;
		margin-left: 15px;
		margin-top: -6px;
	}

	.giftcard.form-horizontal .radio-inline:first-child {
		margin-left: 0px;
	}

	.giftcard.form-horizontal .radio > input, 
	.giftcard.form-horizontal .radio-inline > input {
		top: 2px;
		position: relative;
	}

	.giftcard .amounts .radio-inline {
		font-size: 16px;
	}

	.e-giftcard {
		display: block;
		width: auto;
		overflow: hidden;
		border-radius: 3px;
		box-shadow: 0px 0px 5px rgba(0,0,0,0.2);
		padding: 0px;
		width: 300px;
		margin: 25px auto 0px auto;
	}

	.e-giftcard .course-name {
		font-size: 14px;
		text-shadow: 1px 1px 0px white;
		margin: 0px 0px 10px;
		padding: 0px;
	}

	.e-giftcard h3.value {
		float: left;
		padding: 10px;
		margin: 0px;
		font-size: 20px;
	}

	.e-giftcard div.details .description {
		float: right;
		padding: 10px;
		font-size: 16px;
		color: #888;
	}

	.e-giftcard div.barcode-container {
		min-height: 100px;
		padding: 15px;
		background-color: #F5F5F5;
	}

	.e-giftcard .barcode {
		display: block;
		margin: 0 auto;
		width: auto;
	}

	.e-giftcard div.details {
		display: block;
		float: none;
		overflow: hidden;
		border-top: 1px solid #E0E0E0;
	}

	#payment-warning {
		padding: 15px;
		display: block;
	}
	.btn-primary.active{
		background-color: #204d73
	}
    #aggregate_schedule_select button{
        width:100%;
        text-align:left;
        background:white;
    }
    #aggregate_schedule_select button span.dropdown-label {
        font-size:14px;
        overflow: hidden;
        text-overflow: ellipsis;
        -o-text-overflow: ellisis;
        white-space: nowrap;
        max-width: 225px;
        display: block;
        float: left;
    }
    #aggregate_schedule_select button span.caret {
        float:right;
        margin-top:7px;
    }
    #aggregate_schedule_select ul.dropdown-menu {
        margin-left:15px;
        margin-top:-15px;
    }
    #aggregate_schedule_select ul.dropdown-menu li a{
        font-size:14px;
    }
    #aggregate_schedule_select ul.dropdown-menu li a input {
        vertical-align:middle;
        margin-top:-2px;
    }
    div.update-button-row {
        text-align:center;
    }
        div.foreign_exchange_explanation {
            font-style: italic;
            font-size: 12px;
            border-bottom: 1px solid gray;
            margin-bottom: 15px;
            padding-bottom: 10px;
        }
        div.foreign_exchange_amounts {
            font-size: 20px;
            margin-bottom: 15px;
        }
	</style>	
</head>
<body>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-24484723-1', 'auto');
ga('send', 'pageview');
</script>

<script type="text/html" id="template_member_search">
<div>
	<%-first_name%> <%-last_name%>
	<% if(phone_number && phone_number != ''){ %>
	<br><small class="details"><%-_.formatPhoneNumber(phone_number)%></small>
	<% } %>
	<% if(email && email != ''){ %>
	<br><small class="details"><%-email%></small>
	<% } %>
</div>
</script>

<script type="text/html" id="template_welcome_message">
<div style="text-align: center; padding-top: 2em; padding-bottom: 4em;">	
	<% if(!online_booking_welcome_message || online_booking_welcome_message == ''){ %>
	<h3 class="center-block text-muted" style="text-align: center;">Welcome to</h3>
	<h2 class="center-block" style="text-align: center;"><%-name%></h2>
	<% }else{ %>
	<div style="display: block; overflow: hidden;"><%=online_booking_welcome_message%></div>
	<% } %>
	<p class="center-block" style="margin-top: 2em">
		<% if(logged_in){ %>
		<a href="#account" class="btn btn-lg btn-primary">View Account &#8594;</a>
		<% }else{ %>
		<button class="btn btn-lg btn-primary login">Log In</button>
		<% } %>
	</p>
</div>
</script>

<script type="text/html" id="template_tee_time_online_log_in">
<div style="text-align: center; padding-top: 2em; padding-bottom: 4em;">	
	<h3 class="center-block" style="text-align: center;">Please log in to see available tee times</h3>
	<p class="center-block" style="margin-top: 2em">
		<button class="btn btn-lg btn-primary login">Log In</button> 
	</p>
</div>
</script>

<script type="text/html" id="template_tee_time_log_in">
<div style="text-align: center; padding-top: 2em; padding-bottom: 4em;">	
	<h3 class="center-block" style="text-align: center;">Please log in to view tee times for <em><%-booking_class_name%></em></h3>
	<p class="center-block" style="margin-top: 2em">
		<button class="btn btn-lg btn-primary login">Log In</button>
	</p>
	<p style="margin-top: 2em;">
		<a href="" class="change-class">Change booking class</a>
	</p>
</div>
</script>	

<script type="text/html" id="template_online_giftcard">
<h2 style="margin-bottom: 15px">Purchase Gift Card</h2>
<div class="row">
	<div class='col-md-12' style="margin-bottom: 15px">
		<p>Gift card details will be emailed to you immediately. Details can also be viewed online.</p>
	</div>
</div>
<div class="row">
	<form class="form-horizontal col-md-12 giftcard" role="form">
		<div class="form-group amounts">
			<label class="col-md-1">Amount</label>
			<div class="col-md-6">
				<label class="radio-inline">
	  				<input type="radio" name="amount" value="25" checked> $25
				</label>
				<label class="radio-inline">
	  				<input type="radio" name="amount" value="50"> $50
				</label>
				<label class="radio-inline">
	  				<input type="radio" name="amount" value="100"> $100
				</label>
				<label class="radio-inline">
	  				<input type="radio" name="amount" value="custom" style="top: 5px">
	  				<div>
	  					<input type="text" class="form-control" name="custom_amount" placeholder="Other amt." value="" style="width: 100px" disabled>
	  				</div>
				</label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-3">
				<label class="checkbox" style="font-weight: normal">
				    <input id="giftcard-send" type="checkbox" name="send" value="1"> Send to someone else
				</label>
			</div>
		</div>
		<div class="form-group recipient-info" style="display: none">
			<label class="col-md-1">Name</label>
			<div class="col-md-4">
				<input type="text" class="form-control" name="recipient_name" value="" data-bv-notempty>
			</div>
		</div>
		<div class="form-group recipient-info" style="display: none">
			<label class="col-md-1">Email</label>
			<div class="col-md-4">
				<input type="text" class="form-control" name="recipient_email" value="" data-bv-notempty data-bv-emailaddress>
			</div>
		</div>
		<div class="form-group recipient-info" style="display: none">
			<label class="col-md-1">Message</label>
			<div class="col-md-4">
				<textarea name="recipient_message" class="form-control" style="height: 100px" data-bv-notempty></textarea>
			</div>
		</div>
		<div class="row recipient-info" style="display: none">
			<div class="col-md-12">
				<p class="text-muted">* A copy will also be delivered to your email</p>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<button class="btn btn-primary buy">Buy</button>
			</div>
		</div>
	</form>
</div>
</script>
	
<script type="text/html" id="template_course_info">
	<h2><%-name%></h2>
	<address class="well">
		<% if(phone){ %><a class="phone" href="tel:<%-phone%>"> <span class="glyphicon glyphicon-phone-alt"></span> <%-phone%></a><% } %>
		<% if(email){ %><a class="email" href="mailto:<%-email%>"> <span class="glyphicon glyphicon-envelope"></span> <%-email%></a><% } %>
		<% if(website){ %><a class="web" href="<%-website%>"> <span class="glyphicon glyphicon-globe"></span> <%-website%></a><% } %>
		<div class="address">
			<span class="line" style="margin-left: -20px;">
				<span class="glyphicon glyphicon-home"></span>
				<%-address%>
			</span>
			<span class="line"><%-city%>, <%-state%></span>
			<span class="line"><%-zip%> <%-country%></span>
		</div>
	</address>
	
	<div class="row">
		<% var addressStr = address +','+ city +','+ state +','+ zip;
		addressStr = addressStr.replace(' ', '+'); %>
		<iframe class="col-md-12 col-xs-12 col-sm-12" style="display: block; height: 400px; border: none; margin-bottom: 1em;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAeSdTtDQRykSrrC9KEzfZHUaljeHO_tCE&q=<%-addressStr%>" />
	</div>
</script>

<script type="text/html" id="template_user_settings">
	<h2>Account Information</h2>
	<form name="information" class="information">
		<div class="row">
			<div id="user-info-error" class="alert alert-danger" style="display: none; margin-left: 15px; margin-right: 15px;"></div>
			<div class="form-group col-md-3">
				<label for="account_first_name" class="control-label">First Name</label>
				<input type="text" name="first_name" placeholder="First Name" class="form-control" id="account_first_name" value="<%-first_name %>"
					data-bv-notempty data-bv-notempty-message="First name is required" />									
			</div>
			<div class="form-group col-md-3">
				<label for="account_last_name" class="control-label">Last Name</label>
				<input type="text" name="last_name" placeholder="Last Name" class="form-control" id="account_last_name" value="<%-last_name %>"
					data-bv-notempty data-bv-notempty-message="Last name is required" />									
			</div>			
			<div class="form-group col-md-3">
				<label for="account_email" class="control-label">Email</label>
				<input type="text" name="email" placeholder="Email" class="form-control" id="account_email" value="<%-email %>" 
					data-bv-notempty data-bv-notempty-message="Email address required" 
					data-bv-emailaddress data-bv-emailaddress-message="Email address is invalid" />									
			</div>				
			<div class="form-group col-md-2">
				<label for="account_phone" class="control-label">Phone Number</label>
				<input type="text" name="phone"  class="form-control" id="account_phone" value="<%-phone_number %>"
					data-bv-notempty data-bv-notempty-message="Phone number is required"
					<% if(SETTINGS.currency_symbol == 0 || SETTINGS.currency_symbol=="$" ){ %>
					data-bv-phone data-bv-phone-message="Phone number invalid" placeholder="xxx-xxx-xxxx"
					<% }  %>
				/>
			</div>							
		</div>
		<div class="row">
			<div class="form-group col-md-3">
				<label for="account_address" class="control-label">Address</label>
				<input type="text" name="address" placeholder="Address" class="form-control" id="account_address" value="<%-address_1 %>" />									
			</div>
			<div class="form-group col-md-3">
				<label for="account_city" class="control-label"><?=lang('common_city')?></label>
				<input type="text" name="city" placeholder="City" class="form-control" id="account_city" value="<%-city %>" />									
			</div>
			<div class="form-group col-md-3">
				<label for="account_state" class="control-label"><?=lang('common_state')?></label>
				<input type="text" name="state" placeholder="<?=lang('common_state')?>" class="form-control" id="account_state" value="<%-state %>" />
			</div>
			<div class="form-group col-md-2">
				<label for="account_zip" class="control-label"><?=lang('common_zip')?></label>
				<input type="text" name="zip" placeholder="<?=lang('common_zip')?>" class="form-control" id="account_zip" value="<%-zip %>" />
			</div>												
		</div>
		<div class="row">
			<div class="form-group col-md-2">
				<label for="account_birthday" class="control-label">Birthday</label>
				<input type="text" name="birthday" placeholder="mm/dd/yyyy" class="form-control" id="account_birthday" value="<% if(birthday != '0000-00-00'){ print(moment(birthday).format('MM/DD/YYYY')) } %>" />									
			</div>		
		</div>
		<div class="row" style="padding-top: 1em;">
			<div class="form-group col-md-12">
				<button class="btn btn-primary save-account" data-loading-text="Saving...">Save Account Info</button>
			</div>
		</div>
	</form>	
	<form name="account" class="account">			
		<div class="row">
			<fieldset class="col-md-12" style="margin-top: 2em;">	
				<legend>Login Information</legend>
				<div class="row">
					<div id="user-credentials-error" class="alert alert-danger" style="display: none; margin-left: 15px; margin-right: 15px;"></div>
					<div class="form-group col-md-3">
						<label for="account_username" class="control-label hidden-xs">Email</label>
						<input type="text" name="username" placeholder="Email" class="form-control" id="account_username" value="<%-username%>"
							data-bv-notempty data-bv-notempty-message="Username" />									
					</div>
					<div class="form-group col-md-3">
						<label for="account_current_password" class="control-label hidden-xs">Current Password</label>
						<input type="password" name="current_passsword" placeholder="Current Password" class="form-control" id="account_current_password" />									
					</div>
					<div class="form-group col-md-3">
						<label for="account_password" class="control-label hidden-xs">New Password</label>
						<input type="password" name="passsword" placeholder="New Password" class="form-control" id="account_password" />									
					</div>
				</div>
			</fieldset>	
		</div>
		<div class="row" style="padding-top: 1em;">
			<div class="col-md-12">
				<button class="btn btn-primary save-login" data-loading-text="Saving...">Save Login Info</button>
			</div>
		</div>
	</form>
</script>

<script type="text/html" id="template_reservation">
    <% if(!purchased){ %><button class="btn btn-danger pull-right cancel" data-loading-text="Cancelling...">Cancel</button><% } %>
    <% if(!purchased && moment().add(24, 'hours') < moment(time)){ %><button class="btn btn-info pull-right update" >Update</button><% } %>
    <div class="pull-left">
		<h4 style="margin-top: 0px; margin-bottom: 2px;"><%-moment(time).format('ddd, MMM Do @ h:mma')%></h4>
		<span class="details"><%-course_name%> - <%-teesheet_title%></span>
	</div>
	<div class="pull-left">
		<div class="holes">
			<span class="glyphicon glyphicon-flag"></span> <%-holes%> holes
		</div>
		<div class="players">
			<span class="glyphicon glyphicon-user"></span> <%-player_count%> players
		</div>		
	</div>
</script>

<script type="text/html" id="template_purchase">
	<td class="sale-num"><%-trans_comment%></td>
	<td class="date"><%-date%></td>
	<td class="items">
		<% if(customer_note){ %>
		<strong>NOTE</strong> <%-customer_note%><br>
		<% } %>
		<%=trans_description%>
	</td>	
	<% var style = '';
	if(total < 0){ 
		style = "style='color: #EE0000'";
	} %>
	<td class="total" <%=style%>><%-accounting.formatMoney(total)%></td>
	<% var style = '';
	if(running_balance < 0){ 
		style = "style='color: #EE0000'";
	} %>	
	<td class="total" <%=style%>><%-accounting.formatMoney(running_balance)%></td>
</script>

<script type="text/html" id="template_minimum_charge_transaction">
	<td class="sale-num"><%-sale_number%></td>
	<td class="date"><%-moment(sale_time, 'YYYY-MM-DD HH:mm:ss').format('M/D/YY h:ssa')%></td>
	<td class="item">(<%-quantity%>) <%-name%></td>
	<td><%-accounting.formatMoney(subtotal)%></td>
	<td><%-accounting.formatMoney(tax)%></td>
	<td class="total"><%-accounting.formatMoney(total)%></td>
</script>

<script type="text/html" id="template_invoice">
	<td class="invoice-num"><%-invoice_number%></td>
	<td class="date"><%-moment(date).format('MMM Do, YYYY')%></td>
	<td class="total"><%-accounting.formatMoney(total)%></td>
	<td class="due"><%-accounting.formatMoney(total - paid)%></td>
	<td class="status">
		<% if(_.round(total - paid) <= 0){ %>
		<span class="label label-success">PAID</span>
		<% }else if(moment(due_date).startOf('day').unix() < moment().startOf('day').unix()){ %>
		<span class="label label-danger">OVERDUE</span>
		<% }else{ %>
		<span class="label label-warning">UNPAID</span>
		<% } %>
	</td>
	<td style="text-align: right;">
		<% if(_.round(total - paid) > 0){ %><button class="btn btn-success pay" data-loading-text="Loading...">Pay</button><% } %>
		<button class="btn btn-primary view" data-loading-text="Loading...">View</button>	
	</td>
</script>

<script type="text/html" id="template_raincheck">
	<td class="number"><%-raincheck_number%></td>
	<td><%-moment(date_issued, 'YYYY-MM-DD HH:mm:ss').format('MMM Do, YYYY')%></td>
	<td class="expiration">
		<% if(expiry_date == '0000-00-00 00:00:00'){ %>
		<em>No Expiration</em>
		<% }else{ print(moment(expiry_date, 'YYYY-MM-DD HH:mm:ss').format('MMM Do, YYYY')) } %>
	</td>
	<td class="status">
		<% if(expiry_date == '0000-00-00 00:00:00' || moment(expiry_date).unix() > moment().unix() - 86400){ %>
			<span class="label label-success">Active</span>
		<% }else{ %>
			<span class="label label-danger">Expired</span>
		<% } %>
	</td>
	<td class="balance"><%-accounting.formatMoney(total)%></td>
	<td>
		<a href="#account/rainchecks/<%-raincheck_id%>" class="btn btn-sm btn-default">View Details</a>
	</td>
</script>

<script type="text/html" id="template_raincheck_details">
<div class="col-lg-12">
	<div class="row hidden-print">
		<div class="col-md-12">
			<a class='btn btn-sm btn-default back pull-left' href="#account/rainchecks" style="margin-top: 10px">
				<span class="glyphicon glyphicon-chevron-left"></span> Back
			</a>
			<button class='btn btn-sm btn-default print pull-right' style="margin-top: 10px">
				<span class="glyphicon glyphicon-print"></span> Print
			</button>
		</div>
	</div>
	<div class="e-giftcard">
		<div class="barcode-container">
			<h2 class="course-name"><%-course_name%></h2>
			<img class="barcode">
		</div>
		<div class="details">
			<h3 class="value">
				<%-accounting.formatMoney(total)%>
			</h3>
			<div class="description">
				Raincheck
			</div>
		</div>
	</div>
</div>
</script>

<script type="text/html" id="template_gift_card">
	<td class="number"><%-giftcard_number%></td>
	<td class="notes"><%-details%></td>
	<td class="expiration"><% if(expiration_date == '0000-00-00'){ %><em>No Expiration</em><% }else{ print(moment(expiration_date).format('MMM Do, YYYY')) } %></td>
	<td class="status">
		<% if(expiration_date == '0000-00-00' || moment(expiration_date).unix() > moment().unix() - 86400){ %>
			<span class="label label-success">Active</span>
		<% }else{ %>
			<span class="label label-danger">Expired</span>
		<% } %>
	</td>
	<td class="balance"><%-accounting.formatMoney(value)%></td>
	<td>
		<a href="#account/giftcards/<%-giftcard_id%>" class="btn btn-sm btn-default">View Details</a>
	</td>
</script>

<script type="text/html" id="template_gift_card_details">
<div class="col-lg-12">
	<div class="row hidden-print">
		<div class="col-md-12">
			<a class='btn btn-sm btn-default back pull-left' href="#account/giftcards" style="margin-top: 10px">
				<span class="glyphicon glyphicon-chevron-left"></span> Back
			</a>
			<button class='btn btn-sm btn-default print pull-right' style="margin-top: 10px">
				<span class="glyphicon glyphicon-print"></span> Print
			</button>
		</div>
	</div>
	<div class="e-giftcard">
		<div class="barcode-container">
			<h2 class="course-name"><%-course_name%></h2>
			<img class="barcode">
		</div>
		<div class="details">
			<h3 class="value">
				<%-accounting.formatMoney(value)%>
			</h3>
			<div class="description">
				E-Gift Card
			</div>
		</div>
	</div>
</div>
</script>

<script type="text/html" id="template_credit_card">
	<% var cc_images = {
		'M/C' : 'images/sales/mastercard.png',
		'AMEX' : 'images/sales/amex.png',
		'DISCOVER' : 'images/sales/discover.png',
		'DCVR' : 'images/sales/discover.png',
		'VISA' : 'images/sales/visa.png',
		'DINERS' : 'images/sales/diners.png',
		'JCB' : 'images/sales/jcb.png'
	}; %>
	<button class='btn btn-danger delete pull-right' data-loading-text="Deleting...">Delete</button>
	<div class="pull-left">
		<img class="type" src="<%-URL%>/<%=cc_images[card_type]%>" />
	</div>
	<div class="pull-left">
		<h4 class="number"><%-card_type.replace('M/C', 'MASTERCARD')%> - <%-masked_account.replace(/[^\d.]/g, "")%></h4>
		<span class="expiration">Expiration: <strong><% if(expiration == '0000-00-00'){ print('N/A') }else{ print(moment(expiration).format('MM/YY')) } %></strong></span>
	</div>
</script>

<script type="text/html" id="template_user_profile">
	<nav class="col-md-3">
		<ul class="nav nav-pills nav-stacked" style="margin-top: 25px;">
			<li class="active">
				<a href="#account-reservations" data-toggle="tab"><span class="glyphicon glyphicon-flag"></span> Reservations</a>
			</li>
			<% if(show_course_specific_info){ %>
			<li>
				<a id="rainchecks-tab" href="#account-rainchecks" data-toggle="tab"><span class="glyphicon glyphicon-barcode"></span> Rainchecks</a>
			</li>	
			<li>
				<a id="giftcards-tab" href="#account-giftcards" data-toggle="tab"><span class="glyphicon glyphicon-usd"></span> Gift Cards</a>
			</li>			
			<li>
				<a href="#account-purchases" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Account History</a>
			</li>
			<% if(minimum_charge_totals && minimum_charge_totals.length > 0){ %>
			<li>
				<a href="#account-fb-spending" data-toggle="tab"><span class="glyphicon glyphicon-screenshot"></span> Minimum Spending</a>
			</li>
			<% } %>
			<li>
				<a href="#account-invoices" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span> Invoices</a>
			</li>			
			<li>
				<a href="#account-creditcards" data-toggle="tab"><span class="glyphicon glyphicon-credit-card"></span> Credit Cards</a>
			</li>
			<% } %>
			<li>
				<a href="#account-settings" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Account Information</a>
			</li>			
		</ul>
	</nav>
	<section class="col-md-9 tab-content" style="min-height: 600px;">
		<div id="account-reservations" class="tab-pane active"></div>
		<div id="account-purchases" class="tab-pane">
			<h2>Account History</h2>
			<div class="row">
				<% if(member == 1){ %>
				<div class="col-md-3">
					<div class="panel <% if(member_account_balance < 0){ print('panel-danger') }else{ print('panel-default') } %>">
						<div class="panel-heading"><% if(App.data.course.get('member_balance_nickname')) { print(App.data.course.get('member_balance_nickname')) }else{ print('Member Balance') } %></div>
						<div class="panel-body <% if(member_account_balance < 0){ print('bg-danger') } %>"><h4><%-accounting.formatMoney(member_account_balance)%></h4></div>
					</div>
				</div>
				<% } %>
				<div class="col-md-3">
					<% 
					var invoice_total_due = 0;
					if(invoice_balance <= 0){
						invoice_total_due = invoice_balance;
					}

					var invoice_credit;
					if(invoice_balance > 0){
						invoice_credit = invoice_balance;
					} %>
					<div class="panel <% if(invoice_balance < 0){ print('panel-danger') }else{ print('panel-default') } %>">
						<div class="panel-heading">Invoices Due</div>
						<div class="panel-body <% if(invoice_balance < 0){ print('bg-danger') } %>" <% if(invoice_credit > 0){ print('style="padding-bottom: 0px;"') } %>>
							<h4 style="margin-bottom: 0px"><%-accounting.formatMoney(invoice_total_due)%></h4>
							<% if(invoice_credit > 0){%><h5 style="margin-top: 2px;">(<%-accounting.formatMoney(invoice_credit)%>) Credit</h5><%}%>
						</div>
					</div>
				</div>				
				<div class="col-md-3">
					<div class="panel <% if(account_balance < 0){ print('panel-danger') }else{ print('panel-default') } %>">
						<div class="panel-heading"><% if(App.data.course.get('customer_credit_nickname')) { print(App.data.course.get('customer_credit_nickname')) }else{ print('Customer Credit') } %></div>
						<div class="panel-body <% if(account_balance < 0){ print('bg-danger') } %>"><h4><%-accounting.formatMoney(account_balance)%></h4></div>
					</div>
				</div>
				<% if(use_loyalty == 1){ %>
				<div class="col-md-3">
					<div class="panel panel-default">
						<div class="panel-heading">Loyalty Points</div>
						<div class="panel-body"><h4><%-loyalty_points%></h4></div>
					</div>
				</div>
				<% } %>
			</div>
			<div class="row">
				<div class="col-md-3 form-group">
					<label>Period</label>
					<%=_.dropdown({
						'custom': 'Custom Range',
						'today': 'Today',
						'yesterday': 'Yesterday',
						'last_week': 'Past Week',
						'last_2_weeks': 'Past 2 Weeks',
						'last_month': 'Past Month',
						'last_6_months': 'Past 6 Months',
						'last_year': 'Past Year'
					}, {
						'id': 'transaction-period',
						'class': 'form-control'
					}) %>
				</div>
				<div class="col-md-3 form-group">
					<label>Start Date</label>
					<input type="text" id="transactions-start" placeholder="MM/DD/YYYY" class="datepicker form-control">		
				</div>
				<div class="col-md-3 form-group">
					<label>End Date</label>
					<input type="text" id="transactions-end" placeholder="MM/DD/YYYY" class="datepicker form-control">		
				</div>
				<div class="col-md-3 form-group">
					<label class="hidden-xs hidden-sm visible-md visible-lg">&nbsp;</label>
					<button data-loading-text="Filtering..." id="filter-transactions" class="btn btn-primary pull-right">Apply Filter</button>
				</div>
			</div>
			<div id="account-transactions" class="table-responsive"></div>		
		</div>
		<div id="account-invoices" class="tab-pane table-responsive"></div>
		<div id="account-creditcards" class="tab-pane"></div>
		<div id="account-settings" class="tab-pane"></div>
		<div id="account-rainchecks" class="tab-pane">
			<h2>Rainchecks</h2>
			<div id="raincheck-list" class="table-responsive"></div>					
		</div>	
		<div id="account-giftcards" class="tab-pane">
			<h2>Gift Cards</h2>
			<div id="giftcard-list" class="table-responsive"></div>					
		</div>
		<div id="account-fb-spending" class="tab-pane">
			<h2>Minimum Spending</h2>
			<div id="fb-totals"></div>
			<h3>Recent Transactions</h3>
			<div id="fb-transactions" class="table-responsive"></div>				
		</div>				
	</section>
</script>

<script type="text/html" id="template_minimum_charges">
<% 
if(minimum_charges){ %>
<% _.each(minimum_charges, function(charge){ %>
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 style="margin: 0px;"><%-charge.name%></h4>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-4">
				<strong>Current Period:</strong> <%-moment(charge.start_date, 'YYYY-MM-DD HH:ss:mm').format('M/D/YY')%> to 
				<%-moment(charge.end_date, 'YYYY-MM-DD HH:ss:mm').format('M/D/YY')%>
			</div>
			<div class="col-sm-3"><strong>Spent:</strong> <%-accounting.formatMoney(charge.total)%></div>
			<div class="col-sm-3"><strong>Minimum:</strong> <%-accounting.formatMoney(charge.minimum_amount)%></div>
			<div class="col-sm-2">
				<% if(parseFloat(charge.total) >= parseFloat(charge.minimum_amount)){ %>
				<label class="label label-success">Minimum Met</label>
				<% }else{ %>
				<label class="label label-danger">Not Met</label>
				<% } %>
			</div>
		</div>
	</div>
</div>
<% }) } %>
</script>

<script type="text/html" id="template_time">
<div class="reserve-time">	
	<% if(show_course_name){ %>
	<h6>
		<%-_.shorten(course_name+' '+schedule_name, 30)%>
	</h6>
	<% } %>
	<div class="pull-left">
		<h4 class="start"><%=moment(time).format('h:mma')%></h4>
		<div class="holes" style="margin-right: 0.5em;">
			<i class="glyphicon glyphicon-flag"></i> <span class="holes"><%=holes%></span>
		</div>
		<div class="spots">
			<i class="glyphicon glyphicon-user"></i> <span class="spots"><%=available_spots%></span>
		</div>
	</div>
	<div class="pull-right">
		<div class="price">
			<% if(!hide_prices){ %>
			<span title="Green Fee">
				<i class="icon-golf-ball-tee" style="font-size: 1em;"></i><% if(cart_fee !== false){ %>&nbsp;&nbsp;<% } %> 
				<%=accounting.formatMoney(green_fee + green_fee_tax)%>
			</span>
			<% if(cart_fee !== false){ %>
			<span title="Cart Fee"><i class="icon-golf-cart" style="font-size: 1em;"></i> +<%=accounting.formatMoney(cart_fee + cart_fee_tax)%></span>
			<% } } %>		
		</div>
	</div>
	<% if(has_special && !hide_prices){ %>
	<span class="label label-warning special"><%=special_discount_percentage%>% OFF</span>
	<% } %>		
</div>
</script>

<script type="text/html" id="template_time_private">
<td class="details">	
	<div class="wrapper">
		<div class="pull-left">
			<h4 class="start"><%=moment(time).format('h:mma')%></h4>
			<div class="holes" style="margin-right: 0.5em;">
				<i class="glyphicon glyphicon-flag"></i> <span class="holes"><%=holes%></span>
			</div>
			<div class="spots">
				<i class="glyphicon glyphicon-user"></i> <span class="spots"><%=available_spots%></span>
			</div>
		</div>
		<div class="price" <% if(has_special && !hide_prices){ %>style="padding-bottom: 15px;"<% } %>>
			<% if(!hide_prices){ %>
			<span title="Green Fee"><i class="icon-golf-ball-tee" style="font-size: 1em;"></i>  <% if(cart_fee !== false){ %>&nbsp;&nbsp;<% } %><%=accounting.formatMoney(green_fee)%></span>
			<% if(cart_fee !== false){ %><span title="Cart Fee"><i class="icon-golf-cart" style="font-size: 1em;"></i> +<%=accounting.formatMoney(cart_fee)%></span><% } %>		
			<% } %>
		</div>
		<% if(has_special && !hide_prices){ %>
		<span class="label label-warning special"><%=special_discount_percentage%>% OFF</span>
		<% } %>	
	</div>
</td>
<td class="player">
	<span class="hidden-xs">
	<% if(players && players[0] != undefined){
		print(players[0]['name']);
	}else{
		print('&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;')
	} %>
	</span>
	<span class="hidden-sm hidden-md hidden-lg">
	<% if(players){ _.each(players, function(player){ %>
		<%-player['name']%><br>	
	<% }); } %>
	</span>
</td>
<td class="player hidden-xs">
	<span><% if(players && players[1] != undefined){
		print(players[1]['name']);
	}else{
		print('&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;')
	} %></span>
</td>
<td class="player hidden-xs">
	<span><% if(players && players[2] != undefined){
		print(players[2]['name']);
	}else{
		print('&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;')
	} %></span>	
</td>
<td class="reserve">
	<% if(available_spots > 0 && available_spots >= min_players){ %>
	<button class="btn btn-success reserve-time">Reserve</button>
	<% }else{ %>
	<span class="hidden-xs">
	<% if(players && players[3] != undefined){
	print(players[3]['name']);
	} %>
	</span>			
	<% } %>
</td>
</script>

<script type="text/html" id="template_book_time">
<div class="modal-content time-details">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">
			Book Time
			<% if(has_special && !hide_prices){ %>
				<span style="margin-left: 5px" class="label label-warning">
			<%-special_discount_percentage%>% OFF</span><% } %>
		</h4>
	</div>
	<div class="modal-body container-fluid">
		<div id="booking-error" class="alert alert-danger" style="display: none;"></div>
		<div class="row">
			<div class="col-md-12">
				<h4 style="margin: 0px; color: #666;"><%-course_name%> - <%-schedule_name%></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-md-3">
				<label>Date</label>
				<span class="field"><%=reservation_day%></span>
			</div>
			<div class="col-xs-6 col-md-3">
				<label>Time</label>
				<span class="field"><%=reservation_time%></span>
			</div>
			<div class="col-xs-6 col-md-3">
				<label>Holes</label>
				<span class="field"><%=holes%></span>
			</div>																	
		</div>
		<div class="row">		
			<div class="col-sm-6 col-md-4">
				<label>Players</label>
				<div class="btn-group btn-group-justified players">
					<% for(var x = 1; x <= available_spots; x++){
					if(minimum_players <= x) { %>
					<a class="btn btn-primary <% if(players == x){ %>active<% } %>" data-value="<%-x%>"><%-x%></a>
					<% } } %>												
				</div>
			</div>
			<% if(rate_type == 'both'){ %>
			<div class="col-sm-6 col-md-4">
				<label>Carts</label>
				<div class="btn-group btn-group-justified carts">
					<a class="btn btn-primary <% if(!carts){ %>active<% } %>" data-value="no">No</a>
					<a class="btn btn-primary <% if(carts){ %>active<% } %>" data-value="yes">Yes</a>							
				</div>
			</div>
			<% } %>								
		</div>
		<% if(!has_special && SETTINGS.has_promo_codes){ %>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group has-feedback">	
					<label for="promo_code">Promo Code</label>
					<input name="promo_code" id="promo_code" value="" type="text" class="form-control" />			
				</div>
			</div>			
		</div>	
		<% } %>	
		<% if(!hide_prices){ %>
		<div class="row">
			<div class="col-md-2">
				<label>Total</label>
				<span class="field total"><%=accounting.formatMoney(total)%></span>
			</div>						
		</div>
		<% } %>
		<% if(show_course_info && course){ %>
		<div class="row">
			<div class="panel-group col-lg-12" id="accordion" role="tablist" aria-multiselectable="true">
			  	<div class="panel panel-default">
			   		 <div class="panel-heading" role="tab" id="course-info-heading">
			      		<h4 class="panel-title">
			        		<a data-toggle="collapse" data-parent="#accordion" href="#course-info" aria-expanded="true" aria-controls="course-info">
			         			Course Info
			        		</a>
			      		</h4>
			    	</div>
			    	<div id="course-info" class="panel-collapse collapse" role="tabpanel">
			      		<div class="panel-body">
							<address>
								<% if(course.get('phone')){ %><a class="phone" href="tel:<%-course.get('phone')%>"> <span class="glyphicon glyphicon-phone-alt"></span> <%-course.get('phone')%></a><% } %>
								<% if(course.get('email')){ %><a class="email" href="mailto:<%-course.get('email')%>"> <span class="glyphicon glyphicon-envelope"></span> <%-course.get('email')%></a><% } %>
								<% if(course.get('website')){ %><a class="web" href="<%-course.get('website')%>"> <span class="glyphicon glyphicon-globe"></span> <%-course.get('website')%></a><% } %>
								<div class="address">
									<span class="line" style="margin-left: -20px;">
										<span class="glyphicon glyphicon-home"></span>
										<%-course.get('address')%>
									</span>
									<span class="line"><%-course.get('city')%>, <%-course.get('state')%></span>
									<span class="line"><%-course.get('zip')%> <%-course.get('country')%></span>
								</div>
							</address>
			      		</div>
			    	</div>
			 	</div>
			  	<div class="panel panel-default">
			   		 <div class="panel-heading" role="tab" id="course-booking-rules-heading">
			      		<h4 class="panel-title">
			        		<a data-toggle="collapse" data-parent="#accordion" href="#course-booking-rules" aria-expanded="true" aria-controls="course-info">
			         			Booking Rules
			        		</a>
			      		</h4>
			    	</div>
			    	<div id="course-booking-rules" class="panel-collapse collapse" role="tabpanel">
			      		<div class="panel-body"><%=course.get('booking_rules')%></div>
			    	</div>
			 	</div>
			</div>
		</div>
		<% } %>

        <div class="row">
            <div class="g-recaptcha" id="recaptcha" style="float:right;"
                 data-sitekey="6LeQqCIUAAAAAI8nAaJahskkE6mNTI9cBBRseaFO"
                 data-callback="onSubmit"
                 data-size="invisible">
            </div>
        </div>
	</div>

	<div class="modal-footer">


		<button type="button" data-loading-text="Booking time..." class="btn btn-success book pull-left">Book Time</button>
		<button type="button" class="btn btn-default hidden-xs pull-left" data-dismiss="modal">Close</button>		
	</div>
</div>
</script>

<script type="text/html" id="template_invoice_details">
<div class="modal-content invoice-details">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">
			Invoice #<%-invoice_number%> 
			<% if(_.round(total - paid) <= 0){ %>
			<span class="label label-success">PAID</span>
			<% }else if(moment(due_date).startOf('day').unix() < moment().startOf('day').unix()){ %>
			<span class="label label-danger">OVERDUE</span>
			<% }else{ %>
			<span class="label label-warning">UNPAID</span>
			<% } %>
		</h4>
	</div>
	<div class="modal-body container-fluid">
		<div class="row">
			<div class="col-md-4" style="margin-bottom: 1em;">
				<h4 style="margin-top: 0px;">To</h4>
				<%-person_info.first_name%> <%-person_info.last_name%>
			</div>
			<div class="col-md-4" style="margin-bottom: 1em;">
				<h4 style="margin-top: 0px;">From</h4>
				<%-course_info.name%>
				<%-course_info.phone%>
			</div>
			<div class="col-md-4 dates" style="margin-bottom: 1em;">
				<% if(_.round(total - paid) > 0){ %>
				<span class="line">
					<button class="btn btn-success pay" style="float: right; margin-bottom: 10px;">Pay Online</button>
				</span>
				<% } %>	
				<span class="line">
					<label>Invoiced</label> <span class="date"><%-moment(date).format('MM/DD/YYYY') %></span>
				</span>
				<span class="line">
					<label>Due On</label> <span class="date"><%-moment(due_date).format('MM/DD/YYYY') %></span>
				</span>
			</div>						
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th class="name">Item</th>
								<th class="qty">Qty</th>
								<th class="price">Price</th>
							</tr>
						</thead>
						<tbody>
						<% _.each(current_items, function(item){ %>
							<tr>
								<td class="name"><%-item.name%></td>
								<td class="qty"><%-item.quantity%></td>
								<td class="price"><%-accounting.formatMoney(item.total)%></td>
							</tr>
						<% }); %>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-offset-6 col-md-6">
				<table class="table totals">
					<tbody>
						<tr>
							<th>Total</th>
							<td><%-accounting.formatMoney(total)%></td>
						</tr>
						<tr>
							<th>Paid</th>
							<td><%-accounting.formatMoney(paid)%></td>
						</tr>
						<tr>
							<th>Due</th>
							<td><strong><%-accounting.formatMoney(total - paid)%></strong></td>
						</tr>
					</tbody>												
				</table>
			</div>
		</div>
        <% if(show_account_transactions == 1){ %>
            <% if(pay_member_account == 1){ %>
            <div class="row">
                <div class="col-md-12">
                    <h4><%=member_nickname%> Transactions</h4>
                    <table class="table transactions">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th class="date">Date</th>
                            <th class="name">Item</th>
                            <th class="price">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <% if(account_transactions.member.length > 0){
                        _.each(account_transactions.member, function(transaction){ %>
                        <tr>
                            <td><%-transaction.trans_comment %></td>
                            <td><%-transaction.date %></td>
                            <td><%=transaction.trans_description %></td>
                            <td><%-accounting.formatMoney(transaction.total)%></td>
                        </tr>
                        <% }); }else{ %>
                        <tr>
                            <td colspan="4"><span class="muted">No transactions available</span></td>
                        </tr>
                        <% } %>
                        </tbody>
                    </table>
                </div>
            </div>
            <% } %>
            <% if(pay_customer_account == 1){ %>
            <div class="row">
                <div class="col-md-12">
                    <h4><%=customer_credit_nickname%> Transactions</h4>
                    <table class="table transactions">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th class="date">Date</th>
                            <th class="name">Item</th>
                            <th class="price">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <% if(account_transactions.customer.length > 0){
                        _.each(account_transactions.customer, function(transaction){ %>
                        <tr>
                            <td><%-transaction.trans_comment %></td>
                            <td><%-transaction.date %></td>
                            <td><%=transaction.trans_description %></td>
                            <td><%-accounting.formatMoney(transaction.total)%></td>
                        </tr>
                        <% }); }else{ %>
                        <tr>
                            <td colspan="4"><span class="muted">No transactions available</span></td>
                        </tr>
                        <% } %>
                        </tbody>
                    </table>
                </div>
            </div>
            <% } %>
        <% } %>
		<div class="row">
			<div class="col-md-12">
				<button class="btn btn-default close-invoice" data-dismiss="modal">Close</button>
			</div>
		</div>			
	</div>
</div>
</script>

<script type="text/html" id="template_credit_card_payment">
<div class="modal-content credit-card-payment">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Credit Card Payment</h4>
	</div>

	<div class="modal-body container-fluid" style="min-height: 200px;">
		<!-- Modal content will be replaced dynamically -->
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary ets-submit pull-left disabled" disabled="disabled" data-loading-text="Verifying...">Pay</button>
	</div>
</div>
</script>

<script type="text/html" id="template_payment_selection">
<div class="modal-content purchase">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Payment Method</h4>
	</div>
	<div class="modal-body container-fluid">
		<div class="row">
			<div class="col-md-9">
				<ul id="payment_methods">
					<%
					booking_fee = booking_fee_price;
					if(booking_fee_per_person){
						booking_fee = booking_fee_price * players;
					}

					if(pay_online == 'optional' || pay_online == 'no'){ %>
					<li>
						<label class="<% if(!foreup_discount){ %>selected<% } %>">
							<div class="method">
								<input type="radio" name="payment_method" value="course" <% if(!foreup_discount){ %>checked<% } %> />
								<h4>Pay at Course</h4>
								<h4 class="price"><%-accounting.formatMoney(total)%></h4>
							</div>
							<% if(booking_fee_required){ %>
							<div class="details">
								<span class="details text-danger">
									<strong><%-accounting.formatMoney(booking_fee)%></strong> fee required to book online
								</span>
							</div>
							<% }else if(!has_special && promo_discount <= 0){%>
							<div class="details">
								<span class="text-muted details">Regular Price</span>
							</div>
							<% } %>
						</label>
					</li>
					<% } %>
					<% if(foreup_discount){ %>
					<li>
						<label class="selected">
							<div class="method">
								<input type="radio" name="payment_method" value="online-foreup" checked />
								<h4>Pay Online <% if(pay_online == 'required'){ %><em>(Required)</em><%}%></h4><h4 class="price"><%-accounting.formatMoney(pay_total)%></h4>
							</div>
							<% if(!has_special && promo_discount <= 0){%>
							<div class="details">
								<span class="text-muted details"><%-accounting.formatNumber(discount_percent)%>% OFF - Limited Time!</span>
								<span class="label label-warning discount">Save <%-accounting.formatMoney(discount)%></span>
							</div>
							<% } %>
						</label>
					</li>
					<% }else if(pay_online == 'optional' || pay_online == 'required'){ %>
					<li>
						<label <% if(pay_online == 'required'){ %>class="selected"<% } %>>
							<div class="method">
								<input type="radio" name="payment_method" value="online" <% if(pay_online == 'required'){ %>checked<% } %> />
								<h4>Pay Online</h4>
								<h4 class="price"><%-accounting.formatMoney(total)%></h4>
							</div>
							<div class="details">
								<span class="text-muted details">
									<% if(pay_online == 'optional'){ %>
									Save time, pre-pay now
									<% }else{ %>
									Pre-payment required to book online
									<% } %>
									<% if(booking_fee_required){ %>
									<span class="text-danger">
										<strong><%-accounting.formatMoney(booking_fee)%></strong> fee required to book online
									</span>
									<% } %>
								</span>
							</div>
						</label>
					</li>							
					<% } %>
				</ul>
			</div>																	
		</div>
		<% if(booking_fee_required && booking_fee_terms){ %>
		<div class="row">
			<div class="col-md-12">
				<p class='foreup-disclaimer text-muted'>
					<% if(!foreup_discount && pay_online == 'optional'){ %>
					By selecting "Pay at Course"  or "Pay Online" 
					<% }else if(pay_online == 'required'){ %>
					By selecting "Pay Online"
					<% }else{ %>
				 	By selecting "Pay at Course"
				 	<% } %>
					you agree to the following terms: <%-booking_fee_terms%>
				</p>			
			</div>
		</div>		
		<% } %>
		<% if(foreup_discount){ %>
		<div class="row">
			<div class="col-md-12">
				<p class='foreup-disclaimer text-muted'><%=FOREUP_DISCLAIMER%></p>			
			</div>
		</div>
		<% } %>
		<% if(App.data.course.get('include_tax_online_booking') == "0" && App.data.course.get('unit_price_includes_tax') == "1"){ %>
		<div class="row">
			<div class="col-md-12">	
				<small class="text-muted">* Prices displayed may not include tax</small>	
			</div>						
		</div>
		<% } %>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-success continue">Continue</button>
		<button class="btn btn-success ets-submit col-xs-12 col-md-3 disabled" style="display: none" disabled="disabled" data-loading-text="Verifying Card...">Submit Payment</button>		
	</div>
</div>
</script>

<script type="text/html" id="template_purchase_reservation">
<div class="modal-content time-details purchase">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Purchase Your Reservation</h4>
	</div>
	<div class="modal-body container-fluid">
		<div class="row">
			<div class="col-xs-6 col-md-3">
				<label>Date</label>
				<span class="field"><%=reservation_day%></span>
			</div>
			<div class="col-xs-6 col-md-3">
				<label>Time</label>
				<span class="field"><%=reservation_time%></span>
			</div>
			<div class="col-xs-6 col-md-3">
				<label>Holes</label>
				<span class="field"><%=holes%></span>
			</div>																	
		</div>
		<div class="row">
			<% if(promo_discount > 0){
				var classes = 'col-xs-6 col-md-3';
			}else{
				var classes = 'col-sm-6 col-md-4';
			} %>
			<div class="<%-classes%>">
				<label>Players</label>
				<% if(promo_discount <= 0){ %>
				<div class="btn-group btn-group-justified players">
					<% for(var x = min_required_paying; x <= available_spots; x++){ %>
					<a class="btn btn-primary <% if(parseInt(pay_players) == x){ %>active<% } %>" data-value="<%-x%>"><%-x%></a>
					<% } %>
				</div>
				<% } else { %>
				<span class="field"><%-parseInt(pay_players)%></span>
				<% } %>														
			</div>
			
			<% if(can_book_carts){ %>
			<div class="<%-classes%>">
				<label>Carts</label>
				<% if(promo_discount <= 0){ %>
				<div class="btn-group btn-group-justified carts">
					<a class="btn btn-primary <% if(!carts || carts == 0){ %>active<% } %>" data-value="no">No</a>
					<a class="btn btn-primary <% if(carts || carts > 0){ %>active<% } %>" data-value="yes">Yes</a>							
				</div>
				<% } else { %>
				<span class="field">
				<% if(!carts || carts == 0){ 
					print('No') 
				}else{
					print('Yes')
				} %>
				</span>
				<% } %>						
			</div>	
			<% } %>						
		</div>
		<% if(!has_special && promo_discount <= 0){ %>
		<div class="row purchase" style="margin-top: 1em;">
			<div class="col-md-2 col-xs-4">
				<label>Subtotal</label>
			</div>
			<div class="col-md-2 col-xs-4">
				<span class="field subtotal"><%=accounting.formatMoney(pay_subtotal)%></span>
			</div>
		</div>
		<div class="row purchase">
			<div class="col-md-2 col-xs-4">
				<label>Discount</label>
			</div>
			<div class="col-md-2 col-xs-4">	
				<span class="field discount"><%=accounting.formatMoney(discount)%></span>
			</div>
		</div>
		<% } %>
		<div class="row purchase">
			<div class="col-md-2 col-xs-4">
				<label><strong>Total</strong></label>
			</div>
			<div class="col-md-2 col-xs-4">
				<span class="field total"><strong><%=accounting.formatMoney(pay_total)%></strong></span>
			</div>													
		</div>
		<div class="row purchase">
			<div class="col-md-12">
				<p class="foreup-disclaimer text-muted"><%=FOREUP_DISCLAIMER%></p>
			</div>
		</div>		
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-success purchase pull-left">Purchase</button>
		<button type="button" class="btn btn-default hidden-xs pull-left" data-dismiss="modal">Close</button>		
	</div>
</div>
</script>

<script type="text/html" id="template_log_in">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Log In<% if(!hide_signup){ %>/Register<% } %></h4>
	</div>
	<div class="modal-body container-fluid">
		<div id="login-error" class="alert alert-danger" style="display: none"></div>
		<form role="form" id="login_form" class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="form-group col-md-12">
						<label for="login_email" class="control-label hidden-xs">Email</label>
						<input type="text" name="email" placeholder="Email" class="form-control" id="login_email" data-bv-notempty="true" data-bv-notempty-message="Email is required" />
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<label for="login_password" class="control-label hidden-xs">Password</label>
						<input type="password" name="password" placeholder="Password" class="form-control" id="login_password" data-bv-notempty="true" data-bv-notempty-message="Password is required" />									
						<span class="help-block"><a class="forgot-password" href="#">Forgot Password</a></span>
					</div>
				</div>	
			</div>
			<% if(!hide_signup){ %>
			<div class="col-md-5 col-md-offset-1 hidden-xs hidden-sm">
				<p style="margin-top: 2em; text-align: center">Don&#39;t have an account yet?</p>
				<div class="form-group">
					<button class="btn btn-primary register center-block">Sign up for free</button>	
				</div>
			</div>	
			<% } %>
		</form>
	</div>
	<div class="modal-footer">
		<div class="form-group">
			<button class="btn btn-primary login col-xs-12 col-md-2" data-loading-text="Logging In...">Log In</button>
			<button type="button" class="btn btn-default hidden-xs hidden-sm pull-left" data-dismiss="modal">Close</button>											
		</div>		
		<% if(!hide_signup){ %>
		<div class="form-group hidden-md hidden-lg">
			<h4 style="text-align: center;" class="col-xs-12 text-muted">Or</h4>				
		</div>
		<div class="form-group">
			<button class="btn btn-primary register col-xs-12 col-md-3 hidden-md hidden-lg">Register</button>	
		</div>	
		<% } %>
	</div>
</div>
</script>

<script type="text/html" id="template_forgot_password">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Forgot Password</h4>
	</div>
	<div class="modal-body container-fluid">
		<div id="password-reset-error" class="alert alert-danger" style="display: none"></div>
		<div id="password-reset-success" class="alert alert-success" style="display: none"></div>
		<form role="form" id="reset_password_form" class="row">
			<div class="col-md-12">
				<p>Enter your Email below. An email will be sent with further instructions to
				reset your password.</p>
			</div>			
			<div class="col-md-12">
				<div class="row">
					<div class="form-group col-md-6">
						<label for="forgot_password_username" class="control-label hidden-xs">Email</label>
						<input type="text" name="username" placeholder="Email" class="form-control username" id="forgot_password_username" data-bv-notempty="true" data-bv-notempty-message="Email is required" />
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
		<div class="form-group">
			<button class="btn btn-primary reset-password col-xs-12 col-md-2" data-loading-text="Loading...">Reset</button>
			<button type="button" class="btn btn-default hidden-xs hidden-sm pull-left" data-dismiss="modal">Close</button>											
		</div>
	</div>
</div>
</script>

<script type="text/html" id="template_register">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Register</h4>
	</div>
	<div class="modal-body container-fluid" style="padding-bottom: 0px;">
		<form role="form" id="register_form">
			<div id="signup-error" class="alert alert-danger" style="display: none"></div>	
			<div class="row">
				<div class="form-group col-md-5">
					<label for="register_first_name" class="control-label hidden-xs">First Name</label>
					<input type="text" name="first_name" placeholder="First Name" class="form-control" id="register_first_name" 
						data-bv-notempty data-bv-notempty-message="First name is required" />											
				</div>
				<div class="form-group col-md-5">
					<label for="register_last_name" class="control-label hidden-xs">Last Name</label>
					<input type="text" name="last_name" placeholder="Last Name" class="form-control" id="register_last_name" 
						data-bv-notempty data-bv-notempty-message="Last name is required" />											
				</div>				
			</div>
			<div class="row">								
				<div class="form-group col-md-5">
					<label for="register_phone" class="control-label hidden-xs">Phone Number (w/ area code)</label>
					<input type="text" name="phone"  class="form-control" id="register_phone"
						data-bv-notempty data-bv-notempty-message="Phone number required"
					<% if(SETTINGS.currency_symbol == 0 || SETTINGS.currency_symbol=="$" ){ %>
					data-bv-phone data-bv-phone-message="Phone number invalid" placeholder="xxx-xxx-xxxx"
					<% } %>
					/>
				</div>
			</div>
			<div class="row">				
				<div class="form-group col-md-5">
					<label for="register_email" class="control-label hidden-xs">Email</label>
					<input type="text" name="email" placeholder="Email" class="form-control" id="register_email"
						data-bv-notempty data-bv-notempty-message="Email is required"
						data-bv-emailaddress data-bv-emailaddress-message="Email is invalid" />
				</div>
				<div class="form-group col-md-5">
					<label for="register_password" class="control-label hidden-xs">Password</label>
					<input type="password" name="password" placeholder="Password" class="form-control" id="register_password" 
						data-bv-notempty data-bv-notempty-message="Password required"
						data-bv-stringlength data-bv-stringlength-min="6" data-bv-stringlength-message="Must be at least 6 characters" />											
				</div>				
			</div>			
		</form>	
	</div>
	<div class="modal-footer">
		<div class="form-group">
			<button class="btn btn-success complete col-xs-12 col-md-3" data-loading-text="Creating account...">Register</button>												
		</div>	
	</div>

</div>
</script>

<script type="text/html" id="template_select_credit_card">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Select Credit Card</h4>
	</div>
	<div class="modal-body container-fluid">
		<div id="credit-card-error" class="alert alert-danger" style="display: none;"></div>
		<form role="form" id="register_form">
			<div class="alert alert-danger" style="display: none;" id="booking-error"></div>
			<div class="row">
				<div class="form-group col-md-12">
					<% if(App.data.course.get('no_show_policy')){ %>
					<h4 style="margin-top: 0px; padding-top: 0px;">No Show Policy</h4>
					<p><%=App.data.course.get('no_show_policy') %></p>
					<% } %>										
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<label for="booking_credit_card" class="control-label hidden-xs">Credit Card</label>
					<select id="booking_credit_card" class="form-control">
						<% if(!credit_cards || credit_cards.length == 0){ %>
						<option value="">- No Credit Cards -</option>
						<% }else{ %>
						<% _.each(credit_cards, function(credit_card){ %>
						<option value="<%-credit_card.get('credit_card_id')%>" <% if(credit_card.get('selected')){ %>selected<% } %>><%-credit_card.get('card_type')%> <%-credit_card.get('masked_account')%></option>
						<% }); } %>
					</select>											
				</div>
				<div class="col-md-2">
					<div style="height: 22px; display: block"></div>
					<button class="btn btn-primary add-card">Add Card</button>
				</div>			
			</div>		
		</form>		
	</div>
	<div class="modal-footer">
		<div class="form-group">
			<button class="btn btn-success set-card col-xs-12 col-md-3" data-loading-text="Booking tee time...">Book Time</button>												
		</div>	
	</div>
</div>
</script>

<script type="text/html" id="template_player_name_entry">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Players</h4>
	</div>
	<div class="modal-body container-fluid">
		<div class="row player">
	    	<div class="col-xs-12">
		    	<div class="input-group">
		      		<span class="input-group-addon">1</span>
		      		<input type="text" class="form-control disabled" name="player[1][name]" id="player_1" value="<%-user.first_name+' '+user.last_name%>" disabled>
		    	</div>
		    </div>
		</div>
		<% for(x = 2; x <= players; x++){ %>
		<div class="row player">
	    	<div class="col-xs-12">
		    	<div class="input-group">
		      		<span class="input-group-addon"><%-x%></span>
		      		<input type="text" data-member-id="" data-position="<%-x%>" class="form-control member-search" name="player[<%-x%>][name]" id="player_<%-x%>" placeholder="Search members...">
		      		<span class="input-group-btn">
		      			<button class="btn btn-default guest">Guest</button>
		      		</span>
		    	</div>
		    </div>
		</div>
		<% } %>
	</div>
	<div class="modal-footer">
		<div class="form-group">
			<button class="btn btn-success col-xs-12 col-md-3 continue" data-loading-text="Loading...">Continue</button>												
		</div>	
	</div>
</div>
</script>

<script type="text/html" id="template_invoice_select_credit_card">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Invoice #<%-invoice_number%> - Select Credit Card</h4>
	</div>
	<div class="modal-body">
		<form role="form" id="pay_invoice_form">
			<div id="invoice-payment-error" class="alert alert-danger" style="display: none;"></div>
			<div class="row" style="margin-bottom: 1em;">
				<div class="col-md-12">
					<h2 style="margin-top: 0px;"><%-accounting.formatMoney(total - paid) %></h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<label for="invoice_credit_card" class="control-label hidden-xs">Credit Card</label>
					<select id="invoice_credit_card" class="form-control">
						<% if(!credit_cards || credit_cards.length == 0){ %>
						<option value="">- No Credit Cards -</option>
						<% }else{ %>
						<% _.each(credit_cards.models, function(credit_card){ %>
						<option value="<%-credit_card.get('credit_card_id')%>" <% if(credit_card.get('selected')){ %>selected<% } %>><%-credit_card.get('card_type')%> <%-credit_card.get('masked_account')%></option>
						<% }); } %>
					</select>											
				</div>
				<div class="col-md-2">
					<div style="height: 22px; display: block"></div>
					<button class="btn btn-primary add-card">Add Card</button>
				</div>			
			</div>
            <div class="row" style="padding-top: 15px">
                <div class="col-md-12">
                    <small><%-PAYMENT_WARNING%></small>
                </div>
            </div>
            <div class="row" style="padding-top: 15px">
                <div class="col-md-12">
                    <small id="terms_and_conditions"></small>
                </div>
            </div>
        </form>
	</div>
	<div class="modal-footer">
		<div class="row">
			<div class="col-md-12">
				<button class="btn btn-success pay" style="float: left;" data-loading-text="Completing Payment...">Submit Payment</button>												
			</div>
		</div>	
	</div>
</div>
</script>

<script type="text/html" id="template_ets_credit_card_form">
    <p class="bg-info" style="padding:10px">
        Do not refresh this page or press the back button while processing a payment.
    </p>
<form role="form" id="add_credit_card_form">
	<% if(typeof amount != 'undefined'){ %>
	<div class="row">
		<div class="form-group col-md-12"><h2 style="margin: 0px;"><%-accounting.formatMoney(amount)%></h2></div>
	</div>
	<% } %>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="credit_card_first_name" class="control-label hidden-xs">First Name</label>
			<input type="text" class="form-control ETSFirstName" placeholder="First Name" name="first_name" id="credit_card_first_name"
				data-bv-notempty data-bv-notempty-message="First name is required" />											
		</div>
		<div class="form-group col-md-4">
			<label for="credit_card_last_name" class="control-label hidden-xs">Last Name</label>
			<input type="text" class="form-control ETSLastName" placeholder="Last Name"  name="last_name" id="credit_card_last_name" 
				data-bv-notempty data-bv-notempty-message="Last name is required"
			/>											
		</div>			
	</div>
	<div class="row">
		<div class="form-group col-md-6">
			<label for="credit_card_name" class="control-label hidden-xs">Name on Card</label>
			<input type="text" class="form-control ETSNameOnCard" placeholder="Name on card"  name="credit_card_name" id="credit_card_name"
				data-bv-notempty data-bv-notempty-message="Name on card is required"
			 />											
		</div>
		<div class="form-group col-md-2">
			<label for="credit_card_cvv" class="control-label hidden-xs">CVV</label>
			<input type="number" class="form-control ETSCVVCode" placeholder="CVV"  name="cvv" id="credit_card_cvv" style="padding-right: 0px;"
				data-bv-notempty data-bv-notempty-message="CVV required" />											
		</div>									
	</div>
	<div class="row">
		<div class="form-group col-md-5">
			<label for="booking_credit_card" class="control-label hidden-xs">Credit Card Number</label>
			<input type="number" class="form-control ETSCardNumber" placeholder="Number"  name="number" id="credit_card_number"
				data-bv-notempty data-bv-notempty-message="Credit card number is required" />											
		</div>
		<div class="form-group col-md-3">
			<label for="booking_credit_card" class="control-label hidden-xs">Expiration</label>
			<select name="expiration_month" class="form-control ETSExpirationMonth" style="width: 55px; float: left; padding: 0px 0px 0px 5px;">
				<option value="1">01</option>
				<option value="2">02</option>
				<option value="3">03</option>
				<option value="4">04</option>
				<option value="5">05</option>
				<option value="6">06</option>
				<option value="7">07</option>
				<option value="8">08</option>
				<option value="9">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>								
			</select>
			<select name="expiration_year" class="form-control ETSExpirationYear" style="width: 55px; float: left; padding: 0px 0px 0px 5px; margin-left: 5px;">
				<% var date = new Date();
				var year = date.getFullYear();
				for(var y = year + 20; year < y; year++){ %> 
				<option value="<%-year%>"><%-(year - 2000) %></option>
				<% } %>							
			</select>					
		</div>		
	</div>
	<% if(typeof amount != 'undefined'){ %>
    <div class="row">
        <div class="col-md-12"><small><%-PAYMENT_WARNING%></small></div>
    </div>
    <div class="row">
        <div class="col-md-12"><small id="terms_and_conditions"></small></div>
    </div>
   <% } %>
</form>
</script>

<script type="text/html" id="template_add_credit_card">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Add Credit Card</h4>
	</div>
	<div class="modal-body container-fluid" style="min-height: 250px;">
		<% if(App.data.course.get('credit_card_provider') == 'mercury'){ %>
			<div id="mercury_loader">
				<h2>Loading form...</h2>
				<div class="progress progress-striped active">
					<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
				</div>
			</div>

            <p class="bg-info" style="padding:10px">
                Do not refresh this page or press the back button while processing a payment.
            </p>
            <iframe id="mercury_iframe" src="<%=iframe_url%>?schedule_id=<%-schedule_id%>" onLoad="$('#mercury_loader').remove();" style="border: none; display: block; width: 100%; height: 540px; padding: 0px; margin: 0px;"></iframe>
		<% } %>
        <div class="row" style="padding:5px 40px">
            <div class="col-md-12"><small><%-PAYMENT_WARNING%></small></div>
        </div>
        <div class="row" style="padding:5px 40px 20px">
            <div class="col-md-12"><small id="terms_and_conditions"></small></div>
        </div>
    </div>
	<% if(App.data.course.get('credit_card_provider') == 'ets'){ %>
	<div class="modal-footer">
		<div class="form-group">
			<button class="btn btn-success add-card ets-submit col-xs-12 col-md-3 disabled" disabled="disabled" data-loading-text="Verifying Card...">Add Card</button>												
		</div>	
	</div>
	<% } %>
</div>
</script>

<script type="text/html" id="template_filters">
	<!-- Teesheet -->
	<div class="row" style="margin-top: 1em;">
		<div class="col-md-12">
			<label class="hidden-xs">Course</label>
            <% if (IS_GROUP && typeof schedule_ids != 'undefined') { %>
            <div class="button-group" id="aggregate_schedule_select">
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                    <span class="dropdown-label">
                        <%
                            var scheds = App.data.schedules;
                            if (schedule_ids.length == 0) { %>No Courses Selected<%}
                            else if (schedule_ids[0] == 0) {%>- All Courses -<%}
                            else if (schedule_ids.length == 1) {%>
                                <%=scheds.findWhere({teesheet_id: schedule_ids[0]}).get('course_name')%> - <%=scheds.findWhere({teesheet_id: schedule_ids[0]}).get('title')%>
                            <%}
                            else {%>Multiple Courses<%}
                        %>
                    </span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu schedules">
                <% _.each(App.data.schedules.models, function(schedule){ %>
                <li>
                    <a href="#" class="small" data-value="<%=schedule.get('teesheet_id') %>" tabIndex="-1">
                        <input type="checkbox" id="schedule_checkbox_<%=schedule.get('teesheet_id') %>" class="schedule_checkbox" value="<%=schedule.get('teesheet_id') %>" <%if($.inArray(''+schedule.get('teesheet_id'),schedule_ids) != -1 || schedule_ids[0] == 0){%>checked<%}%>/>&nbsp;
                        <% if(!IS_GROUP || schedule.get('teesheet_id') == 0){ %>
                        <%-schedule.get('title') %>
                        <% }else{ %>
                        <%-schedule.get('course_name') %> - <%=schedule.get('title') %>
                        <% } %>
                    </a>
                </li>
                <% }); %>
                </ul>
            </div>
            <% } else { %>
			<select id="schedule_select" name="schedules" class="form-control schedules">
				<% _.each(App.data.schedules.models, function(schedule){ %>
				<option value="<%=schedule.get('teesheet_id') %>" <%if(schedule_id == schedule.get('teesheet_id')){%>selected<%}%>>
                <% if(!IS_GROUP || schedule.get('teesheet_id') == 0){ %>
				<%-schedule.get('title') %>
				<% }else{ %>
				<%-schedule.get('course_name') %> - <%=schedule.get('title') %>	
				<% } %>
				</option>
				<% }); %>
			</select>
            <% } %>
		</div>
	</div>
	
	<!-- Date -->
	<div class="row">
		<div class="col-md-12">
			<div class="row hidden-xs hidden-sm">
				<div class="col-md-12">
					<label>Date</label>
					<input type="text" value="<%=date%>" placeholder="MM-DD-YYYY" name="date" class="form-control" id="date-field">
				</div>
			</div>
			<div class="row">
				<div id="date" class="datepicker hidden-xs hidden-sm input-append">
					<input value="" type="hidden" value="<%=date%>" />
				</div>	
			</div>
			<div class="row hidden-md hidden-lg">
				<div class="col-xs-3" style="padding-bottom: 0px;">
					<button class="btn btn-primary prevday" style="display: block; width: 100%;" type="button">&lt;</button>	
				</div>
				<div class="col-xs-6" style="padding: 0px;">
					<select name="date" id="date-menu" class="datepicker form-control"></select>
				</div>
				<div class="col-xs-3" style="padding-bottom: 0px;">
					<button class="btn btn-primary nextday" type="button" style="display: block; width: 100%;">&gt;</button>
				</div>					
			</div>
		</div>
	</div>

	<!-- Specials -->
	<div class="row">
		<div class="col-md-12">
			<label class="checkbox">
				<input type="checkbox" class="specials-only" name="specials_only" value="1" <%if(specials_only == 1){ print('checked') } %>>
				Show Specials Only
			</label>
		</div>
	</div>

	<!-- Players -->
	<div class="row">
		<div class="col-md-12 hidden-xs">
			<label>Players</label>
			<div class="btn-group btn-group-justified hidden-xs players">
				<% if(show_any_player_button){ %>
				<a class="btn btn-primary <% if(players == '0'){ %>active<% } %>" data-value="0">Any</a>	
				<% } %>
				<% if(minimum_players == 1){ %>
				<a class="btn btn-primary <% if(players == '1'){ %>active<% } %>" data-value="1">1</a>
				<% } if(minimum_players <= 2){ %>
				<a class="btn btn-primary <% if(players == '2'){ %>active<% } %>" data-value="2">2</a>
				<% } if(minimum_players <= 3){ %>
				<a class="btn btn-primary <% if(players == '3'){ %>active<% } %>" data-value="3">3</a>
				<% } if(minimum_players <= 4){ %>
				<a class="btn btn-primary <% if(players == '4'){ %>active<% } %>" data-value="4">4</a>	
				<% } %>
			</div>
		</div>
	</div>
	
	<div class="row">
		<!-- Time of day -->
		<div class="col-sm-6 col-md-12">
			<label class="hidden-xs">Time of Day</label>
			<div class="btn-group btn-group-justified time">
				<a class="btn btn-primary <% if(time == 'morning'){ %>active<% } %>" data-value="morning">Morning</a>
				<a class="btn btn-primary <% if(time == 'midday'){ %>active<% } %>" data-value="midday">Midday</a>
				<a class="btn btn-primary <% if(time == 'evening'){ %>active<% } %>" data-value="evening">Evening</a>
			</div>
		</div>
	
		<!-- Holes -->
		<div class="col-sm-6 col-md-6">
			<label class="hidden-xs">Holes</label>		
			<div class="btn-group btn-group-justified holes">
				<% if(limit_holes == 9 || limit_holes == '0'){ %>
				<a class="btn btn-primary <% if(holes == '9'){ %>active<% } %>" data-value="9">9</a>
				<% } if(limit_holes == 18 || limit_holes == '0'){ %>
				<a class="btn btn-primary <% if(holes == '18'){ %>active<% } %>" data-value="18">18</a>
				<% } %>
			</div>
		</div>
	</div>
	<% if(show_booking_rules){ %>
	<div class="row">
		<div class="col-md-12 hidden-xs hidden-sm">
			<h4>Booking Rules</h4>	
			<%=App.data.course.get('booking_rules')%>
		</div>
		<div style="padding-bottom: 0px;" class="col-md-12 hidden-md hidden-lg">
			<h4><a class="view-booking-rules" href="">View Booking Rules</a></h4>
		</div>		
	</div>
	<% } %>
</script>

<script type="text/html" id="template_booking_rules">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Booking Rules</h4>
	</div>
	<div class="modal-body">
		<%=booking_rules%>
	</div>
	<div class="modal-footer">
		<div class="row">
			<div class="col-md-12">
				<button class="btn btn-default" data-dismiss="modal">Close</button>												
			</div>
		</div>	
	</div>
</div>	
</script>

<script type="text/html" id="template_navigation">
<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>

	<% if(App.data.course.get('website')){
		var websiteUrl = App.data.course.get('website');
		if (!/^https?:\/\//i.test(websiteUrl)) {
			websiteUrl = 'http://' + websiteUrl;
		} 
	%>
		<a class="navbar-brand" href="<%-websiteUrl%>"><%-PAGE_NAME%></a>
		<% } else { %>
		<a class="navbar-brand" href=""#"><%-PAGE_NAME%></a>
	<% } %>
</div>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	<ul class="nav navbar-nav">
		<% if(show_teetimes){ %><li><a href="#teetimes"><span class="glyphicon glyphicon-flag"></span> Tee Times</a></li><% } %>
		<% if(show_course_info){ %><li><a href="#course"><span class="glyphicon glyphicon-map-marker"></span> Course Info</a></li><% } %>
		<% if(App.data.course && App.data.course.get('online_giftcard_purchases') == 1 && App.data.course.get('credit_card_provider')){ %>
		<li>
			<a href="#giftcard"><span class="glyphicon glyphicon-gift"></span> Buy Gift Card</a>
		</li>
		<% } %>
	</ul>

	<ul class="nav navbar-nav navbar-right">
		<% if(!user.logged_in){ %>
		<li><a class="login" href=""><span class="glyphicon glyphicon-log-in"></span> Log In</a></li>
		<% }else{ %>
		<li>
			<a href="#account"><span class="glyphicon glyphicon-briefcase"></span> My Account</a>
		</li>		
		<li>
			<a class="logout" href=""><span class="glyphicon glyphicon-log-out"></span> Logout</a>
		</li>	
		<% } %>
	</ul>
</div>
</script>

<script type="text/html" id="template_confirmation">
<h1 style="font-weight: bold; text-align: center;">Congratulations!</h1>
<h3 style="margin-top: 0px; text-align: center;">Your tee time has been booked</h3>
<div class="panel panel-default confirmation" style="margin-top: 15px;">
	<div class="panel-heading">
		<h3 class="panel-title">Reservation Details</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<h4><%-course_name%> - <%-teesheet_title%></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="well">
					<div class="row">
						<div class="col-xs-6 col-md-6">
							<label>Date</label>
							<span class="field"><%-moment(time).format('MMM Do')%></span>
						</div>
						<div class="col-xs-6 col-md-6">
							<label>Time</label>
							<span class="field"><%-moment(time).format('h:mma')%></span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6 col-md-6">
							<label>Players</label>
							<span class="field"><%-player_count%></span>
						</div>
						<div class="col-xs-6 col-md-6">
							<label>Holes</label>
							<span class="field"><%-holes%></span>
						</div>
					</div>
					<% if(!hide_prices){ %>
					<div class="row">
						<% if(promo_discount > 0){ %>
						<div class="col-xs-6 col-md-6">
							<div class="row">
								<div class="col-md-12">
									<strong style="width: 70px; float: left;">Subtotal</strong>
									<span><%-accounting.formatMoney(total + promo_discount)%></span>
								</div>
								<div class="col-md-12">
									<strong style="width: 70px; float: left;">Promo</strong>
									<span><%-accounting.formatMoney(-promo_discount)%></span>
								</div>
								<div class="col-md-12">
									<strong style="width: 70px; float: left;">Total</strong>
									<span><%-accounting.formatMoney(total)%></span>
								</div>
							</div>
						</div>	
						<% }else{ %>
						<div class="col-xs-6 col-md-6">
							<label>Total</label>
							<span class="field" style="margin-bottom: 0px;">
								<%-accounting.formatMoney(total)%>
							</span>
						</div>			
						<% } %>											
					</div>
					<% } %>							
				</div>
			</div>
			<div class="col-md-6 thanks">
				<h4>Thank you!</h4>
				<% if(foreup_discount && total > 0 && available_spots >= 2){ %>
				<p>
					<% if(!purchased){ %>
					<button class="btn btn-lg btn-success pay-now">
						<span class="glyphicon glyphicon-tag" style="top: 2px"></span> 
						<% if(!has_special && promo_discount <= 0){ %>
						Pay Now and Save <%-_.round(foreup_discount_percent,0) %>%
						<% }else{ %>
						Pay Online Now	
						<% } %>
					</button> 
					<% }else{ %>
					<h3>Your tee time has been purchased</h3>
					<% } %>
				</p>
				<% } %>
				<p>A confirmation email has been sent to <strong><%-user_email%></strong>.
				For questions, call <%-course_name%> at <a href="tel:<%-course_phone%>"><%-course_phone%></a></p>				
			</div>
		</div>
	</div>
</div>
</script>


<script type="text/html" id="template_two_col">
<div class="row hidden-xs">
	<div id="header" class="col-md-8"></div>
</div>
<div class="row">
	<div id="nav" class="col-xs-12 col-md-3"></div>
	<div class="col-xs-12 col-md-9">
		<div id="booking_class" class="row"></div>
		<div id="content" class="row"></div>
	</div>
</div>
</script>

<script type="text/html" id="template_one_col">
<div class="row hidden-xs">
	<div id="header" class="col-md-8"></div>
</div>
<div class="row">
	<div id="content" class="col-xs-12 col-md-12"></div>
</div>
</script>

<script type="text/html" id="template_update_reservation">
    <div class="modal-content time-details">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">
                Update Reservation
            </h4>
        </div>
        <div class="modal-body container-fluid">
            <form name="information" class="information">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Reservation Details</label>
                        <div id="user-info-error" class="alert alert-danger" style="display: none; margin-left: 15px; margin-right: 15px;"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 style="margin: 0px; color: #666;"><%-course_name%> - <%-teesheet_title%></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-md-3">
                                <label>Date</label>
                                <span class="field"><%-moment(time).format('ddd, MMM Do')%></span>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label>Time</label>
                                <span class="field"><%-moment(time).format('h:mma')%></span>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label>Holes</label>
                                <span class="field"><%=holes%></span>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label>Players</label>
                                <span class="field"><%=player_count%></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label>New Player Count</label>
                        <div class="btn-group btn-group-justified players">
                            <% for(var x = 1; x <= parseInt(player_count) + parseInt(available_spots); x++){
                            if(minimum_players <= x) { %>
                            <a class="btn btn-primary <% if(player_count == x){ %>active<% } %>" data-value="<%-x%>"><%-x%></a>
                            <% } } %>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-top: 1em;">
                    <div class="form-group col-md-12 update-button-row">
                        <button class="btn btn-primary update-reservation" data-loading-text="Updating...">Update Reservation</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<nav class="navbar navbar-default" role="navigation" id="navigation"></nav>
<div class="container module" id="page" style="margin-bottom: 2em;"></div>
<div class="modal" id="modal" role="dialog" aria-hidden="true"></div>
<div class="alert" style="display: none;" id="notification"></div>

<script src="<?php echo base_url(); ?>js/dist/online-booking.min.js?v=<? echo APPLICATION_VERSION;?>"></script>
</body>
</html>
