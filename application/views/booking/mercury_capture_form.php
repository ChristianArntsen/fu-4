<script>
    <?php
    $credit_card_payment = $this->session->userdata('credit_card_payment');
    ?>
    window.parent.FOREIGN_CURRENCY_TYPE = '<?=$credit_card_payment['foreign_currency_type']?>';
    window.parent.FOREIGN_CURRENCY_SYMBOL = '<?=$credit_card_payment['foreign_currency_symbol']?>';
    window.parent.FOREIGN_TOTAL = '<?=to_currency_no_money($credit_card_payment['foreign_total'])?>';
    window.parent.USD_TOTAL = '<?=to_currency_no_money($credit_card_payment['amount'])?>';
    var foreign_currency_info = '';
    if (window.parent.FOREIGN_CURRENCY_TYPE != '') {
        foreign_currency_info = "<div class='foreign_exchange_explanation'>This tee time is listed in ("+window.parent.FOREIGN_CURRENCY_SYMBOL+")<b>"+window.parent.FOREIGN_CURRENCY_TYPE+"</b> we must convert this tee time to ($)<b>USD</b> in order for our software to process this fee. The conversion of this price is represented below:</div>"+
            "<div class='foreign_exchange_amounts'>"+window.parent.FOREIGN_CURRENCY_SYMBOL+""+(window.parent.FOREIGN_TOTAL)+"("+window.parent.FOREIGN_CURRENCY_TYPE+") = <b>$"+(window.parent.USD_TOTAL)+" USD</b></div>";
    }
    window.parent.document.getElementById('foreign_currency_info').innerHTML = foreign_currency_info;
</script>
<form action='<?php echo $url; ?>' id='mercury_mobile_form' method='POST'>
	<input type='hidden' value='<? echo $payment_id?>' name='PaymentID' />
</form>
<script>document.getElementById('mercury_mobile_form').submit();</script>

