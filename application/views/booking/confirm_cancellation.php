<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title><?php echo $teetime_info['course_name']; ?> - Cancel Reservation</title>
	<link href="<?php echo base_url(); ?>/css/dist/online-booking.min.css" type="text/css" rel="stylesheet" />
	<style>
	body {
		background: url('<?php echo base_url(); ?>/images/grid_noise.png');
	}
	
	#unavailable {
		background-color: white;
		border: 1px solid #E0E0E0;
		padding: 20px;
	}
	
	h1 {
		text-align: center;
		line-height: 1em;
		margin: 0 0 0.5em 0;
	}
	
	div.well h3 {
		margin-top: 0px;
		margin-bottom: 1em;
	}
	
	label {
		display: block;
		float: left;
		width: 100px;
	}
	
	div.field {
		float: left;
	}
	
	div.col-md-6 {
		overflow: hidden;
	}
	</style>	
</head>
<body>
<nav class="navbar navbar-default" role="navigation" id="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo site_url('booking/'.$course_id); ?>"><?php echo $teetime_info['course_name']; ?></a>
		</div>
	</div>
</nav>	
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Confirm Cancellation</h1>				
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="well">
				<h3>Reservation Details</h3>
				<div class="row">
					<div class="col-md-12">
						<label>Course</label>
						<span class="field"><?php echo $teetime_info['course_name']; ?> - <?php echo $teetime_info['tee_sheet_title']; ?></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<label>Date</label>
						<span class="field"><?php echo $teetime_info['start_date']; ?></span>
					</div>
					<div class="col-md-6">
						<label>Time</label>
						<span class="field"><?php echo $teetime_info['start_time']; ?></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<label>Players</label>
						<span class="field"><?php echo $teetime_info['player_count']; ?></span>
					</div>
					<div class="col-md-6">
						<label>Holes</label>
						<span class="field"><?php echo $teetime_info['holes']; ?></span>
					</div>
				</div>							
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<?php if($teetime_info['status'] == 'deleted'){ ?>
			<p class="alert alert-info">This tee time was cancelled on <?php echo date('m/d/Y', strtotime($teetime_info['date_cancelled'])); ?></p>
			<?php }else if($teetime_info['paid_player_count'] > 0){ ?>
			<p class="alert alert-info">This tee time has been purchased. Cancelling online is no longer available.</p>
			<?php }else{ ?>
			<a href="<?php echo site_url('booking/cancel/'.$teetime_id.'/'.$person_id); ?>" style="margin-bottom: 1em;" class="btn btn-danger btn-lg">Cancel Reservation</a>
			<?php } ?>		
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<p>Questions? Call <?php echo $teetime_info['course_name']; ?> at <a href="tel:<?php echo $course_phone; ?>"><?php echo $course_phone; ?></a></p>			
		</div>
	</div>
</div>
</body>
</html>
