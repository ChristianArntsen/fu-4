<ul id="error_message_box"></ul>
<?php
echo form_open('home/giftcard_lookup/',array('id'=>'giftcard_form'));
?>
<span class='holes_buttonset' style="display: block; float: none; overflow: hidden; width: 200px; margin: 0px auto 10px auto;">
	<input type='radio' id='gc_lookup_giftcard' name='type' value='giftcard' checked />
	<label for='gc_lookup_giftcard'>Giftcard</label>
	<input type='radio' id='gc_lookup_punchcard' name='type' value='punchcard' />
	<label for='gc_lookup_punchcard'>Punch Card</label>
</span>
<fieldset id="giftcard_basic_info">
<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_giftcard_number').':<span class="required">*</span>', 'name', array('class'=>' wide','id'=>'giftcard_number_label')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'giftcard_number',
		'size'=>'24',
		'maxlength'=>'20',
		'id'=>'giftcard_number',
		'value'=>$giftcard_info->giftcard_number)
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>'Lookup',
	'class'=>'submit_button',
	'style' => 'margin-top: 15px;')
);
?>
</fieldset>
<?php echo form_close(); ?>
<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	$('.holes_buttonset').buttonset();
	$('#gc_lookup_giftcard').click(function(){
		$('#giftcard_number_label').html("Gift Card Number: <span class='required'>*</span>");
	});
	$('#gc_lookup_punchcard').click(function(){
		$('#giftcard_number_label').html("Punch Card Number: <span class='required'>*</span>");
	});
		
	var submitting = false;
    $('#giftcard_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
					if(response[0].giftcard_id){
						$.colorbox({href:'index.php/home/view_giftcard/'+response[0].giftcard_id+'/0/1/width~500', title:'Giftcard Information'});
					}else{
						$.colorbox({href:'index.php/home/view_punch_card/'+response[0].punch_card_id+'/0/1/width~500', title:'Punch Card Information'});
					}
					
	                submitting = false;
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
		},
		messages:
		{
		}
	});
});
</script>
<script>
$(document).ready(function(){
	$('#giftcard_number').swipeable({allow_enter:true, character_limit:20});
});
</script>
