<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php $this->load->view("partial/header_new"); ?>
<script src="/js/highcharts.src.js"></script>
<script src="/js/exporting.src.js"></script>
<script type="text/javascript" src="/js/gray.js"></script>
	
<?
$data = array();

$data['container_name'] = random_string('alpha', 10);
// $this->load->view('dashboard/filter_bar', $data);
?>

<?
$chart_ids = array();

$data['start'] = date('Y-m-d', strtotime('-1 year'));
$data['end'] = date('Y-m-d');
$data['type'] = 'month';
$data['data_source'] = 'revenue';
$data['graph_type'] = 'column';
$data['container_name'] = $chart_ids[] = random_string('alpha', 10);
$data['width'] = 900;
$data['height'] = 200;
$data['yAxis'] = 'Dollars ($)';
$this->load->view('dashboard/line_bar_area_charts', $data);

$data = array();

//DONUT CHART SHOWING MISSED TEE TIMES
$data['start'] = date('Y-m-d', strtotime('-1 year'));
$data['end'] = date('Y-m-d');
$data['type'] = 'month';
$data['data_source'] = 'missed_tee_time';
$data['graph_type'] = 'pie';
$data['container_name'] = $chart_ids[] = random_string('alpha', 10);
$data['width'] = 200;
$data['height'] = 200;
$data['yAxis'] = 'Dollars ($)';
$data['static'] = true;
$data['label'] = "Missed tee times";
$this->load->view('dashboard/small_widget_charts', $data);

$data = array();

//DONUT CHART SHOWING PLAYERS
$data['start'] = date('Y-m-d', strtotime('-1 year'));
$data['end'] = date('Y-m-d');
$data['type'] = 'month';
$data['data_source'] = 'players';
$data['graph_type'] = 'pie';
$data['container_name'] = $chart_ids[] = random_string('alpha', 10);
$data['width'] = 200;
$data['height'] = 200;
$data['yAxis'] = 'Dollars ($)';
$data['static'] = true;
$data['label'] = "players";
$this->load->view('dashboard/small_widget_charts', $data);
?>

<?
// $data = array();
// 
// $data['start'] = date('Y-m-d', strtotime('-1 year'));
// $data['end'] = date('Y-m-d');
// $data['type'] = 'month';
// $data['data_source'] = 'tee_time';
// $data['graph_type'] = 'line';
// $data['container_name'] = $chart_ids[] = random_string('alpha', 10);
// $data['width'] = 900;
// $data['height'] = 200;
// $data['yAxis'] = 'Count';
// $this->load->view('dashboard/line_bar_area_charts', $data);
?>


<?php 
	foreach ($chart_ids as $chart_id) {
		echo form_hidden('chart_ids', $chart_id);
	}
?>

<?php $this->load->view("partial/footer"); ?>