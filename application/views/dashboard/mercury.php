<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php $this->load->view("partial/header_new"); ?>
<style type="text/css">
  table.dashboard tbody td{
    font-weight: bold;
  }
  #placeholder{float:left;}
  #legend{
    float:left;
    margin-top: 50px;
    margin-left: 15px;
  }
  #legend td.legendLabel{
    padding-left: 5px;
  }
  #table_holder {
  	width:100%;
  }
  .money {
  	text-align:right;
  }
</style>
<table>
	<tbody>
	  <tr>
	    <td id="item_table">
	    <div id="table_holder">
	      <br/>
	    <h4>Mercury Totals</h4>
	    <table class="dashboard">
	      <thead>
	        <tr>
	          <th class="leftmost">C.C. Sales</th>
	          <th>Courses</th>
	          <th>Mercury Sales</th>
	          <th>Last Mo. Rev.</th>
	          <th>This Mo. Est. Rev.</th>
	          <th class="rightmost">Total Revenue</th>
	        </tr>
	      </thead>
	      <tbody>
	        <tr>
	          <td class='money' width="20%"><?= $mercury_stats['cc_sales']; ?></td>
	          <td class='money' width="15%"><?= $mercury_stats['courses']; ?></td>
	          <td class='money' width="20%"><?= $mercury_stats['mercury_sales']; ?></td>
	          <td class='money' width="15%"><?= $mercury_stats['last_month']; ?></td>
	          <td class='money' width="15%"><?= $mercury_stats['this_month']; ?></td>
	          <td class='money' width="15%"><?= $mercury_stats['total_revenue']; ?></td>
	        </tr>
	      </tbody>
	    </table>
	    </div>
	    </td>
	  </tr>
	  <tr>
	    <td id="item_table">
	    <div id="table_holder">
	      <br/>
	    <h4>Individual Courses</h4>
	    <table class="dashboard">
	      <thead>
	        <tr>
	          <th class="leftmost"></th>
	          <th colspan="2">Last Month</th>
	          <th colspan="2">This Month</th>
	          <th colspan='2' class="rightmost">Total</th>
	        </tr>
	        <tr>
	          <th>Course Name</th>
	          <th>CC Sales</th>
	          <th>Revenue</th>
	          <th>CC Sales</th>
	          <th>Revenue</th>
	          <th>CC Sales</th>
	          <th>Revenue</th>
	        </tr>
	      </thead>
	      <tbody>
	      	<?php while($cs = $mcs->fetch_assoc()) { ?>
	        <tr>
	          <td width="28%"><?= $cs['name']; ?></td>
	          <td class='money' width="12%"><?= to_currency($cs['last_month']); ?></td>
	          <td class='money' width="12%"><?= to_currency($cs['last_month_revenue']); ?></td>
	          <td class='money' width="12%"><?= to_currency($cs['this_month']); ?></td>
	          <td class='money' width="12%"><?= to_currency($cs['this_month_revenue']); ?></td>
	          <td class='money' width="12%"><?= to_currency($cs['total']); ?></td>
	          <td class='money' width="12%"><?= to_currency($cs['total_revenue']); ?></td>
	        </tr>
	        <?php } ?>
	      </tbody>
	    </table>
	    </div>
	    </td>
	  </tr>
	</tbody>
</table>


<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>