<?php if ($test) { ?>
<html>
  <head>
	<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery-ui.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/datepicker.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="/js/highcharts.src.js"></script>
	<script src="/js/exporting.src.js"></script>
	<script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.loadmask.css?<?php echo APPLICATION_VERSION; ?>" />
	<script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script type="text/javascript" src="/js/gray.js"></script>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/redmond.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/datepicker.css?<?php echo APPLICATION_VERSION; ?>" />
<?php } ?>
	
    <script type="text/javascript">
var <?=$container_name?>_chart;
    
$(function () {
    var type = '<?=$type?>';
    var graph_type = '<?=$graph_type?>';
    var start = '<?=$start?>';
    var end = '<?=$end?>';
    $(document).ready(function() {
    	$.ajax({
           type: "POST",
           url: "/index.php/dashboard/fetch_<?=$data_source?>_data/",
           data: 'type='+type+'&start='+start+'&end='+end,
           success: function(response){
           	console.log(response);
		        <?=$container_name?>_chart = new Highcharts.Chart({
		            chart: {
		                renderTo: '<?=$container_name?>',
		                type: graph_type,
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: response.title,
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: response.categories,
		                labels: response.labels
		            },
		            yAxis: {
		                title: {
		                    text: '<?=$yAxis?>'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		            	formatter: function() {
		                <?php if ($data_source == 'revenue') { ?>
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': $'+ (this.y).toFixed(2);
		                <?php } else { ?>
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ (this.y);
		                <?php } ?>
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: response.data
		        });
		    },
            dataType:'json'
        });
    });
    var submitting = false;
    $('#<?=$container_name?>_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				console.log(response);
				<?=$container_name?>_chart.changeData(response);
				submitting = false;
				$(form).unmask();
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
   		},
		messages: 
		{
		}
	});
    $('#<?=$container_name?>_start').datepicker({ dateFormat: "yy-mm-dd", defaultDate: '<?=$start?>'});
    $('#<?=$container_name?>_end').datepicker({ dateFormat: "yy-mm-dd", defaultDate: '<?=$end?>'});
 
});
		</script>

  </head>
  <body>
  	<style>
		.dashboard_filters {
			background:#555555;
			-webkit-border-radius:14px;
			border-radius:14px;
			margin-bottom:5px;
			padding:10px 0px 3px 0px;
		}
		.dashboard_widget:hover .dashboard_filters {
		}
		.dashboard_filters_inner {
			padding:0px 10px 0px 10px;
			float:right;
		}
		.filter_date {
			margin:0px 5px;
		}
	</style>
	<div class='dashboard_widget' style="width: <?=$width?>px;">
		<div id="<?=$container_name?>_filters" class='dashboard_filters' style="width: <?=$width?>px; height: 30px;">
			<div class='dashboard_filters_inner'>
				<?php
				echo form_open("dashboard/fetch_{$data_source}_data",array('id'=>$container_name.'_form'));
				echo form_input(array(
					'name'=>'start',
					'id'=>$container_name.'_start',
					'class'=>'filter_date',
					'placeholder'=>'Start date',
					'value'=>$start));
				echo form_input(array(
					'name'=>'end',
					'id'=>$container_name.'_end',
					'class'=>'filter_date',
					'placeholder'=>'End date',
					'value'=>$end));
				echo form_dropdown('type',array('hour'=>'Hourly','day'=>'Daily','dayname'=>'Day of Week','week'=>'Weekly','month'=>'Monthly','year'=>'Yearly'), $type, '', $container_name.'_type');
				echo form_submit(array(
					'name'=>'submit',
					'id'=>$container_name.'_submit',
					'value'=>lang('common_submit'),
					'class'=>'submit_button float_right')
				);
				?>
				</form>
			</div>
		</div>
		<div id="<?=$container_name?>" style="width: <?=$width?>px; height: <?=$height?>px;"></div>
	</div>
<?php if ($test) { ?>
  </body>
</html>
<?php } ?>