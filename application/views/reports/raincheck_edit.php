<ul id="error_message_box"></ul>
<?php
echo form_open('sales/update_raincheck/' . $raincheck_id, array('id' => 'raincheck_form'));
?>
<fieldset id="raincheck_basic_info">
    <legend>Raincheck Info</legend>
    <div class="field_row clearfix">
        <?php echo form_label('Customer:', 'customer_name', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array('name' => 'customer_name', 'id' => 'customer_name', 'value' => $customer)); ?>
            <?php echo form_hidden('raincheck_customer_id', $customer_id); ?>
        </div>
    </div>

    <div class="field_row clearfix">
        <?php echo form_label('Expiry Date:', 'expiry_date', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array('name' => 'expiry_date', 'id' => 'expiry_date', 'value' => $expiry_date)); ?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Green Fee:', 'green_fee', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array('name' => 'green_fee', 'id' => 'green_fee', 'value' => $green_fee, 'onkeyup' => 'javascript:update_total_value();')); ?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Cart Fee:', 'cart_fee', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array('name' => 'cart_fee', 'id' => 'cart_fee', 'value' => $cart_fee, 'onkeyup' => 'javascript:update_total_value();')); ?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Tax:', 'tax_val', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array('name' => 'tax_val', 'id' => 'tax_val', 'value' => $tax, 'onkeyup' => 'javascript:update_total_value();')); ?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Value:', 'value', array('class' => 'wide')); ?>
        <div class='form_field'>
            <?php echo form_label($total, 'total_value', array('class' => 'wide', 'id'=> 'total_label')); ?>
            <?php echo form_hidden('total_value', $total, false, 'total_value'); ?>
        </div>
    </div>
    <div class="field_row clearfix">
        <button class="submit_button delete_raincheck">Delete</button>
        <?php
        echo form_submit(array(
            'name' => 'submitButton',
            'id' => 'submitButton',
            'value' => 'Update Raincheck',
            'class' => 'submit_button float_right')
        );
        ?>
        <button class="submit_button print_raincheck">Print</button>
        <div class="clear"></div>
    </div>
</fieldset>
<?php form_close(); ?>
<style>
    .delete_raincheck, .print_raincheck {
        height:30px;
        margin:5px;
        cursor:pointer;
    }
    .delete_raincheck {
        float:left;
        background: #d14d4d;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#d14d4d',endColorstr='#c03939');
        background: -webkit-linear-gradient(top,#d14d4d,#c03939);
        background: -moz-linear-gradient(top,#d14d4d,#c03939);
    }
    .print_raincheck {
        float:right;
    }
    #raincheck_basic_info #submitButton {
        margin:5px;
        float:right;
        cursor:pointer;
    }
</style>
<script>
    $(document).ready(function() {
        $("#customer_name").autocomplete({
            source: '<?php echo site_url("sales/customer_search/ln_and_pn"); ?>',
            delay: 200,
            autoFocus: false,
            minLength: 0,
            select: function(event, ui)
            {
                event.preventDefault();
                $("#customer_name").val(ui.item.label);
                $('#raincheck_customer_id').val(ui.item.value);
            }
        });

        $('#expiry_date').datetimepicker({
            defaultDate: new Date(),
            dateFormat: 'yy-mm-dd',
            timeFormat: 'HH:mm',
            defaultValue: '<?php echo date('Y-m-d'); ?> 12:00:00'
        });
        $('#raincheck_form').validate({
            submitHandler: function(form) {
                $(form).mask("<?php echo lang('common_wait'); ?>");
                $(form).ajaxSubmit({
                    success: function(response) {
                        if (response.success) {
                            set_feedback(response.message, 'success_message', false);
                            reports.generate();
                            $.colorbox.close();
                        } else {
                            set_feedback(response.message, 'error_message', true);
                            $.colorbox.close();
                        }
                    },
                    dataType: 'json'
                });
            },
            errorLabelContainer: "#error_message_box",
            wrapper: "li",
            rules: {
            },
            messages: {
            }
        });
        $('.delete_raincheck').click(function(e){
            e.preventDefault();
            if (confirm('Are you sure you want to delete this rain check?')) {
                $.ajax({
                    type: "POST",
                    url: "index.php/sales/delete_raincheck/<?=$raincheck_id?>",
                    data: "",
                    success: function (response) {
                        // Reload report
                        reports.generate();
                        $.colorbox.close();
                    },
                    dataType: 'json'
                });
            }
        });
        $('.print_raincheck').click(function(e){
            e.preventDefault();
            // Print rain check
            $.ajax({
                type: "POST",
                url: "index.php/sales/raincheck/<?=$raincheck_number?>/1",
                data: "",
                success: function (response) {
                    // Reload report
                    console.dir(response);
                    var data = response;
                    data.id = response.raincheck_id;
                    if (data.raincheck_id) {
                        if ('<?php echo $this->config->item('print_after_sale')?>' == '1' && '<?php echo $this->config->item('webprnt')?>' == '1') {
                            var builder = new StarWebPrintBuilder();
                            var receipt_data = '\nRaincheck Credit';
                            // Items

                            // Totals
                            subtotal = (parseFloat(data.green_fee) + parseFloat(data.cart_fee)) * parseInt(data.players);
                            receipt_data += builder.createTextElement({data: '\n\n' + add_white_space('Green Fee: ', '<?=$this->config->item('currency_symbol')?>' + parseFloat(data.green_fee).toFixed(2)) + '\n'});
                            receipt_data += builder.createTextElement({data: add_white_space('Cart Fee: ', '<?=$this->config->item('currency_symbol')?>' + parseFloat(data.cart_fee).toFixed(2)) + '\n'});
                            receipt_data += builder.createTextElement({data: '\n' + add_white_space('Players: ', 'x ' + (data.players)) + '\n'});
                            receipt_data += builder.createTextElement({data: add_white_space('Subtotal: ', '<?=$this->config->item('currency_symbol')?>' + parseFloat(subtotal).toFixed(2)) + '\n'});
                            receipt_data += builder.createTextElement({data: '\n' + add_white_space('Tax: ', '<?=$this->config->item('currency_symbol')?>' + parseFloat(data.tax * data.players).toFixed(2)) + '\n'});
                            receipt_data += builder.createTextElement({data: '\n' + add_white_space('Total Credit: ', '<?=$this->config->item('currency_symbol')?>' + parseFloat(data.total).toFixed(2)) + '\n'});

                            receipt_data += builder.createTextElement({data: "\n\n"});// Some spacing at the bottom

                            receipt_data = webprnt.add_barcode(receipt_data, 'Raincheck ID', 'RID '+data.raincheck_number);
                            var header_data = {
                                course_name:'<?php echo addslashes($this->config->item('name'))?>',
                                address:'<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>',
                                address_2:'<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>',
                                phone:'<?php echo $this->config->item('phone')?>',
                                employee_name:data.employee,
                                raincheck_number:data.raincheck_number//   Need to load this in from data
                            };
                            if (data.customer.trim() != '') {
                                header_data.customer = data.customer;
                            }

                            receipt_data = webprnt.add_receipt_header(receipt_data, header_data);
                            receipt_data = webprnt.add_paper_cut(receipt_data);

                            webprnt.print(receipt_data, window.location.protocol + "//" + "<?=$this->config->item('webprnt_ip')?>/StarWebPRNT/SendMessage");
                            webprnt.print_all(window.location.protocol + "//" + "<?=$this->config->item('webprnt_ip')?>/StarWebPRNT/SendMessage");

                        }
                        else {
                            window.location = 'index.php/sales/raincheck/' + data.id;
                        }
                    }
                    $.colorbox.close();
                },
                dataType: 'json'
            });
            $.colorbox.close();
        });

    });
    
    function update_total_value() {
        var gf_fee = $('#green_fee').val();
        var cart_fee = $('#cart_fee').val();
        var tax_price = $('#tax_val').val();
        var total_price = parseFloat(gf_fee) + parseFloat(cart_fee) + parseFloat(tax_price);
        $('#total_value').val(total_price.toFixed(2));
        $('#total_label').text(total_price.toFixed(2));
    }
</script>