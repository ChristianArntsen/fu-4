<tr class="details">
<td class="innertable" colspan="12" style="display: table-cell">
	<table class="innertable">
		<thead>
			<tr>
				<th align="left">Item Number</th>
				<th align="left">Name</th>
				<th align="left">Category</th>
				<th align="left">Subcategory</th>
				<th align="left">Description</th>
				<th align="right">Qty</th>
				<th align="right">Subtotal</th>
				<th align="right">Total</th>
				<th align="right">Tax</th>
				<th align="right">Profit</th>
				<th align="right">Discount</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($details as $item){ ?>
			<tr>
				<td align="left"><?php echo $item['number']; ?></td>
				<td align="left"><?php echo $item['name']; ?></td>
				<td align="left"><?php echo $item['category']; ?></td>
				<td align="left"><?php echo $item['subcategory']; ?></td>
				<td align="left"><?php echo $item['description']; ?></td>
				<td align="right"><?php echo (float) $item['qty']; ?></td>
				<td align="right">$<?php echo $item['subtotal']; ?></td>
				<td align="right">$<?php echo $item['total']; ?></td>
				<td align="right">$<?php echo $item['tax']; ?></td>
				<td align="right">$<?php echo $item['profit']; ?></td>
				<td align="right"><?php echo $item['discount']; ?>%</td>
			</tr>
            <?php if ($item['receipt_only']) { ?>
                <tr>
                    <td align="left"></td>
                    <td align="left"><?=$item['name'];?> AR / AP Adjustment</td>
                    <td align="left"></td>
                    <td align="left"></td>
                    <td align="left"></td>
                    <td align="right"></td>
                    <td align="right"><?php echo to_currency(-$item['subtotal']); ?></td>
                    <td align="right"><?php echo to_currency(-$item['total']); ?></td>
                    <td align="right"><?php echo to_currency(-$item['tax']); ?></td>
                    <td align="right"><?php echo to_currency(-$item['profit']); ?></td>
                    <td align="right"></td>
                </tr>
            <?php }
            if(empty($item['modifiers']))$item['modifiers'] = array();
            foreach($item['modifiers'] as $modifier){ ?>
				<tr class="modifiers">
					<td align="left"><?php echo $item['number']; ?></td>
					<td align="right">- <?php echo $modifier['name']; ?></td>
					<td align="left"></td>
					<td align="left"></td>
					<td align="left"><?php echo is_array($modifier['selected_option'])?implode(', ',$modifier['selected_option']):$modifier['selected_option']; ?></td>
					<td align="right"></td>
					<td align="right">$<?php echo $modifier['selected_price']; ?></td>
					<td align="right"></td>
					<td align="right"></td>
					<td align="right"></td>
					<td align="right"></td>
				</tr>
			<?php } ?>
			<?php } ?>
		</tbody>
	</table>
</td>
</tr>