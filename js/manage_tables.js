function checkbox_click(event)
{
	event.stopPropagation();
	do_email(enable_email.url);
	if($(event.target).attr('checked'))
	{
		$(event.target).parent().parent().find("td").addClass('selected').css("backgroundColor","");
	}
	else
	{
		$(event.target).parent().parent().find("td").removeClass('selected');
	}

	determine_checkbox_status();
}

function enable_search(suggest_url,confirm_search_message,popup_width, get_params)
{

        //console.log('GET PARAMS ' + suggest_url + '?food_and_beverage=' + get_params);
	//Keep track of enable_email has been called
	if(!enable_search.enabled)
		enable_search.enabled=true;
	//if(popup_width === null || popup_width === undefined || popup_width === 'undefined')
		popup_width = 550;

	$('#search').click(function()
    {
    	$(this).attr('value','');
    	$('#search_id').val('');
	});

	$('#search').focus(function(){
		$('#offset').val(0);
	});
console.log('get params '+get_params);

	$( "#search" ).autocomplete({
 		source: suggest_url + '?show_deleted_customers='+($('#show_deleted_customers').is(":checked")?1:0)+'&food_and_beverage=' + get_params,
		delay: 200,
 		autoFocus: false,
 		minLength: 1,
 		select: function( event, ui )
 		{
			$('#offset').val(0);
			$( "#search" ).val(ui.item.label);
			if (ui.item.value != undefined)
				$( "#search_id" ).val(ui.item.value);
 			do_search(true, '', popup_width);
 		}
	});

	$('#search_form').submit(function(event)
	{
		event.preventDefault();
		if(get_selected_values().length > 0)
		{
			if(!confirm(confirm_search_message))
				return;
		}
		do_search(true, '', popup_width);
	});
}
enable_search.enabled=false;

function do_search(show_feedback,on_complete, popup_width)
{
	$('.fixed_top_table').mask('Loading Results');
	//If search is not enabled, don't do anything
	if(!enable_search.enabled)
		return;

	if(show_feedback)
		$('#spinner').show();

	var param = '';
	if ($('#customer_group').length)
		param = $('#customer_group').val()+'/'+$('#customer_pass').val();
	if ($('#course_id').length)
		param = $('#course_id').val();
	if ($('#paid_status').length) {
		var date = $('#month').monthpicker('getDate');
		var date_string = 'all';
		if (date != null)
			date_string = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
		param = date_string+'/'+$('#paid_status').val()+'/'+$('#search_id').val();
	}

	var offset = $('#offset').val();

	$.ajax({
           type: "POST",
           url: $('#search_form').attr('action')+'/'+offset+'/'+param,
           data: {'search':($('#search').val()), 'search_type': $('#search_type').val(), 'show_deleted_customers': $('#show_deleted_customers').is(":checked")?1:0},
           success: function(response){
           		if (response.data_rows) {
               		$('#sortable_table tbody').html(response.data_rows);
    	       		$('#pagination').html(response.pagination);
           		}
           		else
	           		$('#sortable_table tbody').html(response);

                $('.fixed_top_table').unmask();
				//$('#').html(response.pagination);
            	if(typeof on_complete=='function')
					on_complete();

				$('#spinner').hide();
			   	if (typeof init_email_links !== 'undefined') {
					init_email_links();
				}
				update_sortable_table();
				enable_row_selection();
			   	enable_undelete();
			   	
			   	if(typeof(init_billing_links) == 'function'){
			   		init_billing_links();
			   	}

			   	//console.log('about to init colorbox '+popup_width);
				$('a.colbox').colorbox({'width':popup_width});
				$('#sortable_table tbody :checkbox').click(checkbox_click);
				$("#select_all").attr('checked',false);
           },
            complete:function (){
                $('.fixed_top_table').unmask();
            },
            error:function(){
                $('.fixed_top_table').unmask();
            },
           dataType:'json'
        });

	/*$('#sortable_table tbody').load($('#search_form').attr('action')+'/'+offset+'/'+param,{'search':$('#search').val()},function()
	{
		console.log('here');
		if(typeof on_complete=='function')
			on_complete();

		$('#spinner').hide();
		//re-init elements in new table, as table tbody children were replaced
		//tb_init('#sortable_table a.thickbox');
		//$.colorbox({href:$(this).attr('href'),title:$(this).attr('title')});
		update_sortable_table();
		enable_row_selection();
		console.log('about to init colorbox '+popup_width);
		$('a.colbox').colorbox({'width':popup_width});
		$('#sortable_table tbody :checkbox').click(checkbox_click);
		$("#select_all").attr('checked',false);
	});*/
}

function enable_undelete(){
	$(".undelete").each(function(){
		$(this).off('click').on('click', function(e) {
			e.preventDefault();
			var url = $(this).attr('href');

			//give feedback that something is happening
			$(this).find("td").animate({backgroundColor:"#FF0000"},1200,"linear")
				.end().animate({opacity:0},1200,"linear",function(){
					$(this).remove();
					//Re-init sortable table as we removed a row
					update_sortable_table();
			});

			alert("This feature will NOT un-delete the following:\n\n Customers Account Number\n Customer Groups\n Customers Invoices and Invoice Balance\n Customers Member Billing Information\n Customers Online Booking Account\n");

			var person_id = $(this).attr('id');
			$.ajax({
				type: "POST",
				url: url,
				success: function($response){

					if($response['success']) {
						update_row(person_id, 'customers/get_row');
					}else{
						alert("An error occurred while un-deleting the customer.");
					}
				},
				dataType:'json'
			});
		});
	});
}

function do_card_type_switch(show_feedback,on_complete, popup_width, fnb)
{
	//If search is not enabled, don't do anything
	if(!enable_search.enabled)
		return;

	if(show_feedback)
		$('#spinner').show();

	var param = '';
	// if ($('#customer_group').length)
		// param = $('#customer_group').val()+'/'+$('#customer_pass').val();
	if ($('#course_id').length)
		param = $('#course_id').val();
	if ($('#paid_status').length)
		param = $('#month').val()+'/'+$('#paid_status').val();
	var offset = $('#offset').val();
	console.log('Search form attribute: ' + fnb);
	$.ajax({
           type: "POST",
           url: $('#switch_cards').attr('action')+'/'+offset+'/'+param,
           data: {'item_type':fnb},
           success: function(response){
           		if (response.data_rows) {
               		$('#sortable_table tbody').html(response.data_rows);
    	       		$('#pagination').html(response.pagination);
           		}
           		else
	           		$('#sortable_table tbody').html(response);
	           	if (fnb == 1)
	           	{
	           		var ths = $('#sortable_table thead th div');
	           		$(ths[1]).html("Punch Card Number<span class='sortArrow'>&nbsp;</span>");
					$(ths[2]).hide();
					$(ths[5]).hide();
					$(ths[6]).hide();
					$('#generate_barcode_labels').hide();
					$('#generate_barcodes').hide();
                    $('.import').hide();
					var buttons = $('#new_button a');
					$(buttons[0]).hide();
					$(buttons[0]).html('New Punch Card').attr('href', 'index.php/giftcards/view_punch_card/-1');
                    $('.punch_card_button').show();
                }
	           	else
	           	{
	           		var ths = $('#sortable_table thead th div');
	           		$(ths[1]).html("Giftcard Number<span class='sortArrow'>&nbsp;</span>");
					$(ths[2]).show();
					$(ths[5]).show();
					$(ths[6]).show();
					$('#generate_barcode_labels').show();
					$('#generate_barcodes').show();
					$('.import').show();
	           		var buttons = $('#new_button a');
					$(buttons[0]).show();
					$(buttons[0]).html('New Giftcard').attr('href', 'index.php/giftcards/view/-1');
                    $('.punch_card_button').hide();
                }
           		//$('#').html(response.pagination);
            	if(typeof on_complete=='function')
					on_complete();

				$('#spinner').hide();
				update_sortable_table();
				enable_row_selection();
				//console.log('about to init colorbox '+popup_width);
				$('a.colbox').colorbox({'width':popup_width});
				$('#sortable_table tbody :checkbox').click(checkbox_click);
				$("#select_all").attr('checked',false);
           },
           dataType:'json'
        });

	/*$('#sortable_table tbody').load($('#search_form').attr('action')+'/'+offset+'/'+param,{'search':$('#search').val()},function()
	{
		console.log('here');
		if(typeof on_complete=='function')
			on_complete();

		$('#spinner').hide();
		//re-init elements in new table, as table tbody children were replaced
		//tb_init('#sortable_table a.thickbox');
		//$.colorbox({href:$(this).attr('href'),title:$(this).attr('title')});
		update_sortable_table();
		enable_row_selection();
		console.log('about to init colorbox '+popup_width);
		$('a.colbox').colorbox({'width':popup_width});
		$('#sortable_table tbody :checkbox').click(checkbox_click);
		$("#select_all").attr('checked',false);
	});*/
}
function do_type_switch(show_feedback,on_complete, popup_width, fnb)
{
	//If search is not enabled, don't do anything
	if(!enable_search.enabled)
		return;

	if(show_feedback)
		$('#spinner').show();

	var param = '';
	if (fnb != undefined)
		param = fnb;
	if ($('#customer_group').length)
		param = $('#customer_group').val()+'/'+$('#customer_pass').val();
	if ($('#course_id').length)
		param = $('#course_id').val();
	if ($('#paid_status').length)
		param = $('#month').val()+'/'+$('#paid_status').val();
	var offset = $('#offset').val();
	console.log('Search form attribute: ' + param);
	$.ajax({
           type: "POST",
           url: $('#switch_items').attr('action')+'/'+offset+'/'+param,
           data: {'item_type':fnb},
           success: function(response){
           		if (response.data_rows) {
               		$('#sortable_table tbody').html(response.data_rows);
    	       		$('#pagination').html(response.pagination);
           		}
           		else
	           		$('#sortable_table tbody').html(response);
           		//$('#').html(response.pagination);
            	if(typeof on_complete=='function')
					on_complete();

				$('#spinner').hide();
				update_sortable_table();
				enable_row_selection();
				//console.log('about to init colorbox '+popup_width);
				$('a.colbox').colorbox({'width':popup_width});
				$('#sortable_table tbody :checkbox').click(checkbox_click);
				$("#select_all").attr('checked',false);
           },
           dataType:'json'
        });

	/*$('#sortable_table tbody').load($('#search_form').attr('action')+'/'+offset+'/'+param,{'search':$('#search').val()},function()
	{
		console.log('here');
		if(typeof on_complete=='function')
			on_complete();

		$('#spinner').hide();
		//re-init elements in new table, as table tbody children were replaced
		//tb_init('#sortable_table a.thickbox');
		//$.colorbox({href:$(this).attr('href'),title:$(this).attr('title')});
		update_sortable_table();
		enable_row_selection();
		console.log('about to init colorbox '+popup_width);
		$('a.colbox').colorbox({'width':popup_width});
		$('#sortable_table tbody :checkbox').click(checkbox_click);
		$("#select_all").attr('checked',false);
	});*/
}
function enable_email(email_url)
{
	//Keep track of enable_email has been called
	if(!enable_email.enabled)
		enable_email.enabled=true;

	//store url in function cache
	if(!enable_email.url)
	{
		enable_email.url=email_url;
	}

	$('#select_all, #sortable_table tbody :checkbox').click(checkbox_click);
}
enable_email.enabled=false;
enable_email.url=false;

function do_email(url)
{
	//If email is not enabled, don't do anything
	if(!enable_email.enabled)
		return;

	$.post(url, { 'ids[]': get_selected_values() },function(response)
	{
		$('#email').attr('href',response);
	});

}

function enable_checkboxes()
{
	$('#select_all, #sortable_table tbody :checkbox').click(checkbox_click);
}

function enable_delete(confirm_message,none_selected_message)
{
	//Keep track of enable_delete has been called
	if(!enable_delete.enabled)
		enable_delete.enabled=true;

	$('#delete').click(function(event)
	{
		event.preventDefault();
		if($("#sortable_table tbody :checkbox:checked").length >0)
		{
			if(confirm(confirm_message))
			{
				do_delete($("#delete").attr('href'));
			}
		}
		else
		{
			alert(none_selected_message);
		}
	});
}
enable_delete.enabled=false;

function do_delete(url)
{
	//If delete is not enabled, don't do anything
	if(!enable_delete.enabled)
		return;

	var row_ids = get_selected_values();
	var selected_rows = get_selected_rows();
	var type = $('input[name=billing_type]:checked').val() == 0 ? 'invoices' : 'billings' ;
	$.post(url, { 'ids[]': row_ids, invoice_type : type },function(response)
	{
		//delete was successful, remove checkbox rows
		if(response.success)
		{
			set_feedback(response.message,'success_message',false);

			$(selected_rows).each(function(index, dom)
			{
				$(this).find("td").animate({backgroundColor:"#FF0000"},1200,"linear")
				.end().animate({opacity:0},1200,"linear",function()
				{
					$(this).remove();
					//Re-init sortable table as we removed a row
					update_sortable_table();

				});
			});
		}
		else
		{
			set_feedback(response.message,'error_message',true);
		}


	},"json");
}

function enable_bulk_edit(none_selected_message)
{
	//Keep track of enable_bulk_edit has been called
	if(!enable_bulk_edit.enabled)
		enable_bulk_edit.enabled=true;

	$('#bulk_edit').click(function(event)
	{
		event.preventDefault();
		if($("#sortable_table tbody :checkbox:checked").length >0)
		{
			//tb_show($(this).attr('title'),$(this).attr('href'),false);
			$.colorbox({'maxHeight':700, 'width':550, href:$(this).attr('href'),title:$(this).attr('title')});
			$(this).blur();
		}
		else
		{
			alert(none_selected_message);
		}
	});
}
enable_bulk_edit.enabled=false;

function enable_select_all()
{
	//Keep track of enable_select_all has been called
	if(!enable_select_all.enabled)
		enable_select_all.enabled=true;

	$('#select_all').click(function()
	{
		if($(this).attr('checked'))
		{
			$("#sortable_table tbody :checkbox").each(function()
			{
				$(this).attr('checked',true);
				$(this).parent().parent().find("td").addClass('selected').css("backgroundColor","");

			});
		}
		else
		{
			$("#sortable_table tbody :checkbox").each(function()
			{
				$(this).attr('checked',false);
				$(this).parent().parent().find("td").removeClass('selected');
			});
		}
	 });
}
enable_select_all.enabled=false;

function enable_row_selection(rows)
{
	//Keep track of enable_row_selection has been called
	if(!enable_row_selection.enabled)
		enable_row_selection.enabled=true;

	if(typeof rows =="undefined")
		rows=$("#sortable_table tbody tr");

	rows.hover(
		function row_over()
		{
			$(this).find("td").addClass('over').css("backgroundColor","");
			$(this).css("cursor","pointer");
		},

		function row_out()
		{
			if(!$(this).find("td").hasClass("selected"))
			{
				$(this).find("td").removeClass('over');
			}
		}
	);

	rows.click(function row_click(event)
	{
		var checkbox = $(this).find(":checkbox");
		checkbox.attr('checked',!checkbox.attr('checked'));
		do_email(enable_email.url);

		if(checkbox.attr('checked'))
		{
			$(this).find("td").addClass('selected').css("backgroundColor","");
		}
		else
		{
			$(this).find("td").removeClass('selected');
		}

		determine_checkbox_status();
	});
}
enable_row_selection.enabled=false;

function update_sortable_table()
{
	//let tablesorter know we changed <tbody> and then triger a resort
	$("#sortable_table").trigger("update");

	if(typeof $("#sortable_table")[0].config!="undefined")
	{
		var sorting = $("#sortable_table")[0].config.sortList;
		if ($("#sortable_table .warning_message").length == 0)
			$("#sortable_table").trigger("sorton",[sorting]);
	}
}

function update_row(row_id,url,show_deleted)
{
	if(typeof show_deleted === 'undefined')
		show_deleted = false;

	$.post(url, { 'row_id': row_id, 'show_deleted': show_deleted },function(response)
	{
		//Replace previous row
		var row_to_update = $("#sortable_table tbody tr :checkbox[value="+row_id+"]").parent().parent();
		row_to_update.replaceWith(response);
		reinit_row(row_id);
		highlight_row(row_id);
		enable_undelete();
	});
}

function reinit_row(checkbox_id)
{
	var new_checkbox = $("#sortable_table tbody tr :checkbox[value="+checkbox_id+"]");
	var new_row = new_checkbox.parent().parent();
	enable_row_selection(new_row);
	//Re-init some stuff as we replaced row
	update_sortable_table();
	$(new_row.find('a.colbox, a.billing-link')).colorbox({'maxHeight':700, 'width':550});
	console.log('just reinited_row');
	//re-enable e-mail
	new_checkbox.click(checkbox_click);
}

function highlight_row(checkbox_id)
{
	var new_checkbox = $("#sortable_table tbody tr :checkbox[value="+checkbox_id+"]");
	var new_row = new_checkbox.parent().parent();

	new_row.find("td").animate({backgroundColor:"#e1ffdd"},"slow","linear")
		.animate({backgroundColor:"#e1ffdd"},5000)
		.animate({backgroundColor:"#e9e9e9"},"slow","linear");
}

function get_selected_values()
{
	var selected_values = new Array();
	$("#sortable_table tbody :checkbox:checked").each(function()
	{
		selected_values.push($(this).val());
	});
	return selected_values;
}

function get_selected_rows()
{
	var selected_rows = new Array();
	$("#sortable_table tbody :checkbox:checked").each(function()
	{
		selected_rows.push($(this).parent().parent());
	});
	return selected_rows;
}

function get_visible_checkbox_ids()
{
	var row_ids = new Array();
	$("#sortable_table tbody :checkbox").each(function()
	{
		row_ids.push($(this).val());
	});
	return row_ids;
}

function determine_checkbox_status()
{
	if ($("#sortable_table tbody :checkbox:checked").length > 0)
	{
		$("#email").removeClass("email_inactive");
		$("#delete").removeClass("delete_inactive");
		$("#generate_barcodes").removeClass("generate_barcodes_inactive");
		$("#generate_barcode_labels").removeClass("generate_barcodes_inactive");
		$("#bulk_edit").removeClass("bulk_edit_inactive");
	}
	else
	{
		$("#email").addClass("email_inactive");
		$("#delete").addClass("delete_inactive");
		$("#generate_barcodes").addClass("generate_barcodes_inactive");
		$("#generate_barcode_labels").addClass("generate_barcodes_inactive");
		$("#bulk_edit").addClass("bulk_edit_inactive");
	}
}

function enable_cleanup(confirm_message)
{
	if(!enable_cleanup.enabled)
		enable_cleanup.enabled=true;

	$('#cleanup').click(function(event)
	{
		do_cleanup(event, confirm_message);
	});
}
enable_cleanup.enabled=false;

function do_cleanup(event, confirm_message)
{
	event.preventDefault();

	if(!enable_cleanup.enabled)
		return;

	if (confirm(confirm_message))
	{
		$.post($(event.target).attr('href'), {},function(response)
		{
			set_feedback(response.message,'success_message',false);
		}, 'json');
	}
}