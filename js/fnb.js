var fnb = {

	login: {
		show:function(){
			$('#modal_buttons').remove();
			var login_window = new AuthorizeWindowView({action: 'Login'});
			
			$.colorbox2({
				html: login_window.render().el,
				transition: 'none',
				title: 'Food & Beverage Login',
				escKey: false,
				closeButton: false,
				overlayClose: false,
				open: true,
				opacity: 1,
				onLoad:function(){
					$('#cbox2Close').remove();
				},
				onComplete:function(){
					$('#cbox2Close').remove();
					$('input.pin').focus();
				}
			});
		}
	},
	logout:function() {
		$.ajax({
		   type: "POST",
		   url: "index.php/food_and_beverage/logout/",
		   data: "",
		   success: function(response){
				$('#menubar_stats').html('');
				$('#user_button').hide();
				App.receipt.header.employee_name = '';
				fnb.login.show();
		   },
		   dataType:'json'
		});
	},

	all_selected: false,

	override:function(){
		$.colorbox({href:'index.php/food_and_beverage/view_override/width~600',title:'Override Sale'})
	},

   	show_section: function(section_id) {
   		$('.menu_section_item_holder').hide();
   		$('#section_'+section_id+'_contents').show();
   		$('.selected_menu_section').removeClass('selected_menu_section');
   		$('#section_'+section_id).addClass('selected_menu_section');
   	},

    toggle_taxable: function(which) {
    	var checked = $(which).attr('checked');
        $.ajax({
           type: "POST",
           url: "index.php/food_and_beverage/change_taxable",
           data: "taxable="+checked,
           success: function(response){
               trace(response);
               fnb.update_basket_totals('all', response);
           },
           dataType:'json'
        });
    },

    change_item: function(line, which){
        trace('changing item');
        var item = $('input[name=item_id_'+line+']').val();
        var type = $('select[name=item_type_'+line+']').val();
        this.update_cart_ajax(line, 'item_number', type, true)
    },
    change_cart: function(line, which){
        trace('changing item');
        var item = $('select[name=item_id_'+line+']').val();
        var type = $('select[name=item_type_'+line+']').val();
        this.update_cart_ajax(line, 'item_number', type, true)
    },
    change_line_price: function (line, which, e){
        trace('changing line price');
            var price = (!isNaN(parseFloat($(which).val())))?parseFloat($(which).val()).toFixed(2):0;//).toFixed(2);
            console.log('UPDATING CART TO PRICE: ',price);
            this.update_cart_ajax(line, 'price', price);
       },

    change_line_discount: function (line, which){
        trace('changing line discount');
        if (!isNaN(parseFloat($(which).val()))) {
            var discount = $(which).val();
            this.update_cart_ajax(line, 'discount', discount);
        }
        else if ($(which).val() == '')
        {

        }
        else {
            $(which).val('');
            //alert('Please enter a numerical value.');
        }
    },
    update_basket_totals:function(line, basket_info){
        trace('getting inside update basket totals');
        //console.dir(basket_info);
        trace('after basket info');
        if (basket_info != undefined) {
            if (basket_info.items_in_basket != undefined)
                $('#items_in_basket').html(basket_info.items_in_basket);
            if (basket_info.subtotal != undefined)
            {
                $('#basket_subtotal').html('$'+basket_info.subtotal);
                $('#basket_total').html('$'+basket_info.subtotal);
            }
            if (basket_info.total != undefined)
                $('#basket_final_total').html('$'+basket_info.total);
            if (basket_info.amount_due != undefined) {
                $('.due_amount').html('$'+basket_info.amount_due);
                $('#amount_tendered').val(basket_info.amount_due);
            }
            //console.dir(basket_info.taxes);
            if (basket_info.taxes != undefined) {
                var tax_html = '';
                for (var index in basket_info.taxes) {
                    var tax = basket_info.taxes[index];
                    console.dir(tax);
                    tax_html += ' <div>'+
						              '<div class="right register_taxes">$'+tax.amount.toFixed(2)+'</div>'+
				                          '<div class="left register_taxes">'+tax.name+':</div>'+
				                          '<div class="clear"></div>'+
				                      '</div>';
                }
                //$('.register_taxes').remove();
                $('#taxes_holder').html(tax_html);
            }
        }

    },
    update_cart_info: function (line, item_info, update_price){

        // Need to update stock, price, qty, disc, and total
        var priceField = $('input[name=price_'+line+']');
    	if (parseFloat(priceField.val()) != parseFloat(item_info.price))
        {
        	priceField.val(item_info.price.toFixed(2));
        }
        priceField.val(item_info.price.toFixed(2));

        var discount = $('input[name=discount_'+line+']');
        if (parseFloat(discount.val()) != parseFloat(item_info.discount))
        {
        	set_feedback("Maximum discount on this item is "+item_info.discount+"% <a href='javascript:fnb.override();'>Override</a>",'success_message',true);
        }
        discount.val(item_info.discount);
        $('#splitpayment_item_discount_'+line+' div').text(item_info.discount);

        $('#reg_item_seat_'+line).text(item_info.seat);
        $('#splitpayment_item_seat_'+line).text(item_info.seat);

		var price_box = '<div class="total_price_box">$'+item_info.total.toFixed(2)+"</div>";

        $('#splitpayment_item_total_'+line).html(price_box);
        $('#splitpayment_item_top_'+line).attr('data-price', item_info.total.toFixed(2));

		var receipt_line = $('div.receipt').find('a.item[data-line="'+line+'"]');
        receipt_line.attr('data-price', item_info.total.toFixed(2)).find('span.seat').html(item_info.seat);
    },
    update_cart_ajax: function (line, attribute, value, update_price){
    	update_price = (update_price === undefined)?false:update_price;
        var checked = $("input:[name=select_"+line+"]").attr('checked');
        // delay(function(){
	      $.ajax({
	           type: "POST",
	           url: "index.php/food_and_beverage/update_cart",
	           data: "line="+line+"&"+attribute+"="+value+"&checked="+checked,
	           success: function(response){
	               result = eval('('+response+')');
	               fnb.update_cart_info(line, result.item_info, update_price);
	               fnb.update_basket_totals(line, result.basket_info);
	           }
	        });
	    // }, 1000 );
    },
    delete_item: function (lineNumber){
	    $.ajax({
	       type: "POST",
	       url: "index.php/food_and_beverage/delete_item/"+lineNumber,
	       data: "",
	       success: function(response) {
				fnb.update_page_sections(response);
   		   },
	       dataType:'json'
	    });
    },
    add_item: function (item_id, callback, price_index, teetime_type){
    	price_index = price_index == undefined ? '' : price_index;
    	teetime_type = teetime_type == undefined ? '' : teetime_type;

		/* JBDEBUG
	    $.ajax({
	       type: "POST",
	       url: "index.php/food_and_beverage/add/"+item_id,
	       data: {
	      		"price_index":price_index,
	      		"teetime_type":teetime_type
	      	},
	       success: function(response) {
	       		console.log('add_item js');
	       		fnb.update_page_sections(response);
	       		if (typeof callback === 'function'){
	       			callback();
	       		}
		   },
	       dataType:'json'
	    }); */
    },
    change_sales_mode: function (mode){
	    $.ajax({
	       type: "POST",
	       url: "index.php/food_and_beverage/change_mode/"+mode,
	       data: "",
	       success: function(response) {
				fnb.update_page_sections(response);
    	   },
	       dataType:'json'
	    });
    },
    suspend_sale: function (table){
	    $.ajax({
	       type: "POST",
	       url: "index.php/food_and_beverage/suspend/"+table,
	       data: "",
	       success: function(response) {
		   		if (fnb.update_page_sections(response))
		   		{
		   			fnb.setup_suspend_button('', response['suspend_button_title']);
		   			$.colorbox.close();
		   		}
		   },
	       dataType:'json'
	    });
    },
    unsuspend_sale: function (sale_id, table){
	    $.ajax({
	       type: "POST",
	       url: "index.php/food_and_beverage/unsuspend/"+sale_id+"/"+table,
	       data: "",
	       success: function(response) {
	       		$.colorbox.close();
		   		fnb.update_page_sections(response);
		   		fnb.setup_suspend_button(table);
		   },
	       dataType:'json'
	    });
    },
    update_page_sections:function(response) {
    	console.log('update_page_sections');
    	console.dir(response);
       	if (response) {
    		if (response['message']) {
    			set_feedback(response['message']['text'], response['message']['type'], response['message']['persist']);
    		}
    		if (response['register_box']) {
	   			$("#register_box").html(response['register_box']);
   			}
   			if (response['basket_info']) {
	   			fnb.update_basket_totals('all', response['basket_info']);
   			}
   			if (response['customer_info_filled']) {
	   			$("#customer_info_filled").html(response['customer_info_filled']);
	   		}
	   		if (response['payment_window']) {
		   		$("#make_payment").html(response['payment_window']);
	   		}
	   		if (response['payments']){
	   			console.log('are we even getting here');
	   			console.log(response.payments);
	   			$('.payments_and_tender').replaceWith(response.payments);
	   			$('.colbox').colorbox();
	   		}
	   		if (response['suspended_sales']) {
		   		$("#suspended_sales").html(response['suspended_sales']);
	   		}
			if (response['table_top']) {
	   			$("#table_top").html(response['table_top']);
   			}
	   		if (response['amount_due']){
	   			$('#amount_tendered').val(response.amount_due).select();
	   		}
			// if (response['recent_transactions']) {
		   		// $("#recent_transactions").html(response['recent_transactions']);
	   		// }
	   		if (response['suspend_button_title']) {
			   	fnb.setup_suspend_button('', response['suspend_button_title']);
		   	}

		   	if (response['is_cart_empty']) {
			   	// remove payment type values
			   	$(".payments_and_tender :form").each(function() {
					$(this).remove();
			   	});
		   	}
		   	if (response['table_top']) {
	   			if (response['mode'] == 'sale')
		   		{
			   		$("#register_items_container").removeClass('return');
			   		$('#mode').val('sale');
			   		$("#payments_button").html('Pay Now');
		   		}
		   		else
		   		{
			   		$('#mode').val('return');
			   		$("#register_items_container").addClass('return');
			   		$("#payments_button").html('Return Now');
		   		}
		   		$("#table_top").html(response['table_top']);
	   		}
	   		if (response['table_number'])
	   		{
	   			$('#menubar_stats').html('Table #'+response['table_number']);
	   		}
	   		return true;
   		} else {
   			document.location = document.location;
   		}
		return false;
    },
    setup_suspend_button: function (table, title){
    	$('#suspend_sale_button').unbind('click').click(function(){
    		fnb.show_tables();
    	});
    },
    show_tables:function(canClose) {

		if(canClose){
			canClose = true;
		}else{
			canClose = false;
		}

		$.colorbox({
			transition: 'none',
			title: 'Select a Table/Number',
			escKey: canClose,
			closeButton: canClose,
			overlayClose: canClose,
			opacity: 1,
			open: true,
			html:$('#table_layout').html(),
			onLoad:function(){
				if(!canClose){
					$('#cboxClose').hide();
				}else{
					$('#cboxClose').show();
				}
			},
			onComplete:function(){
				$.colorbox.resize();
				
				$('#cboxOverlay').append("<div id='modal_buttons'>" +
					"<a href='' class='terminal_button fnb_logout'>Logout</a>"+ 
					"<a href='' class='terminal_button fnb_recent_transactions'>Recent Transactions</a>" +
					"<a href='' class='terminal_button fnb_close_register'>Close Register</a>"+ 
				"</div>");
			},
			onCleanup: function(){
				$('#modal_buttons').remove();
			},
			width:1020,
			height:500
		});
    },
    cancel_sale: function (){
	    $.ajax({
	       type: "POST",
	       url: "index.php/food_and_beverage/cancel_sale",
	       data: "",
	       success: function(response) {
	       		fnb.update_page_sections(response);
	       },
	       dataType:'json'
	    });
    },
    load_return: function (sale_id, type, controller){
	    $.ajax({
	       type: "POST",
	       url: "index.php/food_and_beverage/load_return/"+sale_id+"/"+type,
	       data: "",
	       success: function(response) {
	       	   if (controller == 'food_and_beverage')
			       fnb.update_page_sections(response);
			   else
			       document.location = 'index.php/food_and_beverage';
		       $.colorbox.close();
		   },
	       dataType:'json'
	    });
    },
    email_invoice: function(invoice_id){
    	$.ajax({
	       type: "POST",
	       url: "index.php/customers/email_invoice/"+invoice_id,
	       data: "",
	       success: function(response) {

		   },
	       dataType:'json'
	    });
    }
}

$(document).ready(function(){
// THIS KILLS THE ABILITY TO OPEN DOWNLOADS FROM THE FOOD AND BEVERAGE PAGE... BARCODE SCANNERS WERE DOING IT ACCIDENTALLY, SO WE SUPPRESSED IT
	$(document).keydown(function(e){if (e.which == 74 && e.ctrlKey){e.preventDefault();}});
});
function customer_flag_fnb(flag, comment) { 
	$('#feedback_bar').hide();
	if(flag == 1) { 
		set_feedback(comment, 'error_message', true); 
	} else if (flag == 2) { 
		set_feedback(comment, 'warning_message', true);
	}
}
