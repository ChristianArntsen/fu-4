if(typeof BASE_URL === 'undefined'){
    BASE_URL = '/';
}
describe("The JsonRestModel",function(){
    var model = new Backbone.JsonRestModel();
    it('should be defined',function(){
        expect(Backbone.JsonRestModel).toBeDefined();
    });

    it('should construct a new instance',function(){
        expect(model).toBeDefined();
    });

    it('should be an instance of JsonRestModel',function(){
        expect(model instanceof Backbone.JsonRestModel).toBe(true)
    });

    it('should have an instance of the validator',function(){
        expect(model.validator instanceof JsonRestValidator).toBe(true);
    });
});

describe("The JsonRestCollection",function(){

    var collection = new Backbone.JsonRestCollection();
    url = BASE_URL +'api_rest/employee_audit_log';
    collection.url = url;
    collection.model = Backbone.JsonRestModel;
    it('should be defined',function(done){
        expect(Backbone.JsonRestCollection).toBeDefined();
        done();
    });

    it('should construct a new instance',function(done){
        expect(collection).toBeDefined();
        done();
    });

    it('should be an instance of JsonRestCollection',function(done){
        expect(collection instanceof Backbone.JsonRestCollection).toBe(true);
        done();
    });

    it('should have an instance of the validator',function(done){
        expect(collection.validator instanceof JsonRestValidator).toBe(true);
        done();
    });

    it('should fetch from the url',function(done){
        if(typeof App !== 'undefined' && App.data.user.get('user_level')*1<=3)
            pending('requires super-admin to test');
        var ob = {};
        ob.method = 'get';
        ob.reset = true;
        ob.success = function(res){
            expect(res).toBeDefined();
            expect(collection.parse).toHaveBeenCalled();
            expect(collection.validator.validate).toHaveBeenCalled();
            expect(collection.valid).toBe(true);
            expect(collection.get(1) instanceof Backbone.JsonRestModel).toBe(true);
            expect(collection.get(1).attributes.id).toBe('1');
            expect(collection.get(1).id).toBe('1');
            done();
        };
        collection.fetch(ob);
    },10000);

    it('should function correctly with the reset method',function(){
        var res = {data:[{id:"1",type:"employee_audit_log",attributes:{}},
            {id:"2",type:"employee_audit_log",attributes:{}}]};
        collection.reset(res,{parse:true});
        expect(collection.models.length).toBe(2);
        expect(collection.models[0].id).toBe("1");
        expect(collection.models[1].id).toBe("2");
    });

    describe('The get method',function(){
        it('should have populated correctly',function() {
            var res = {data:[{id:"6",type:"employee_audit_log",attributes:{}},
                {id:"5",type:"employee_audit_log",attributes:{}},
                {id:"4",type:"employee_audit_log",attributes:{}}
            ]};
            collection.reset(res,{parse:true});
            expect(collection.models.length).toBe(3);
        });
        it('should not find absent ids',function(){
            expect(collection.get(3)).toBeUndefined();
            expect(collection.get(2)).toBeUndefined();
            expect(collection.get(1)).toBeUndefined();
        });
        it('should find ids that are present from integers',function(){
            expect(collection.get(6).id).toBe("6");
            expect(collection.get(5).id).toBe("5");
            expect(collection.get(4).id).toBe("4");
        });
        it('should return when the argument is a string, as from the server',function() {
            expect(collection.get("6").id).toBe("6");
            expect(collection.get("5").id).toBe("5");
            expect(collection.get("4").id).toBe("4");
        });
        it('should not be stored as an integer',function() {
            expect(collection.get(6).id).not.toBe(6);
            expect(collection.get(5).id).not.toBe(5);
            expect(collection.get(4).id).not.toBe(4);
        });
        it('should be in order', function() {
            expect(collection.get(6).id).not.toBe(4);
            expect(collection.get(4).id).not.toBe(6);
            expect(collection.get(6).id).not.toBe("4");
            expect(collection.get(4).id).not.toBe("6");
        });
    });
});
