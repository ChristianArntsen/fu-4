function add_white_space(str_one, str_two)
{
       var width = 42;
       var strlen_one = str_one.length;
       var strlen_two = str_two.length;
       var white_space = '';
       var white_space_length = 0;
       if (strlen_one + strlen_two >= width)
               return (str_one.substr(0, width - strlen_two - 4)+'... '+str_two); //truncated if text is longer than available space
       else
               white_space_length = width - (strlen_one + strlen_two);

       for (var i = 0; i < white_space_length; i++)
               white_space += ' ';
       return str_one+white_space+str_two;
}

var webprnt = {
	extension_id: "ndcdpiikdnlagfjejimgpmaflngcemoo",
	nag_count:0,
	connection_errors:[],
	last_nagged: new Date(),
	// BARCODE LABELS
	blackmark_enable: function() {
		var builder = new StarWebPrintBuilder();
		return builder.createRawDataElement({data:'\x1B\x1D\x23\x2B\x31\x30\x33\x30\x30\x0A\x00\x1B\x1D\x23\x2D\x31\x30\x30\x30\x30\x0A\x00\x1B\x1D\x23\x57\x30\x30\x30\x30\x30\x0A\x00'});
	},
	blackmark_disable: function() {
		var builder = new StarWebPrintBuilder();
		return builder.createRawDataElement({data:'\x1B\x1D\x23\x2B\x31\x30\x30\x30\x30\x0A\x00\x1B\x1D\x23\x2D\x31\x30\x33\x30\x30\x0A\x00\x1B\x1D\x23\x57\x30\x30\x30\x30\x30\x0A\x00'});
	},
	build_barcode_label: function (barcode_data, title, upc, price, is_last, additional_price, barcode) {
		var builder = new StarWebPrintBuilder();
        barcode = typeof barcode == 'undefined' ? upc : barcode;
		// FIND NEXT LABEL
		barcode_data += builder.createAlignmentElement({position:'center'});
		barcode_data += builder.createTextElement({width:1,height:1,data:'\n'+title+' $'+price+'\n'});
        if (additional_price != '')
            barcode_data += builder.createTextElement({width:1,height:1,data:additional_price+'\n'});
        barcode_data += builder.createBarcodeElement({symbology:'Code128', width:'width4', height:60, hri:false, data:barcode});
	    barcode_data += builder.createTextElement({width:1,height:1,data:upc});
	    if (!is_last) {
		    barcode_data += builder.createRawDataElement({data:'\x0C'});
		} else {
			barcode_data += builder.createCutPaperElement({feed:true,type:'full'});
		}
	    return barcode_data;
	},
	// CREDIT CARDS
	credit_card_slips:'',
	credit_card_payments:0,
	show_tip_line: false,
	currently_printing:{},
	build_failed_slip: function (receipt_data) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createTextElement({data:'TRANSACTION FAILED \n'});
		receipt_data += builder.createTextElement({data:'IF THIS PROBLEM PERSISTS \n'});
		receipt_data += builder.createTextElement({data:'PLEASE CONTACT FOREUP \n'});
		receipt_data += builder.createTextElement({data:'WITH THE DETAILS OF THE SALE \n'});
		receipt_data += builder.createTextElement({data:'YOU ARE TRYING TO PROCESS \n'});
		receipt_data += builder.createTextElement({data:'THANK YOU \n'});

		return receipt_data;
	},
	build_credit_card_slip: function (receipt_data, params, header_data) {
		var builder = new StarWebPrintBuilder();
		if (params != undefined)
		{
		    console.log('inside build_credit_card_slip *********************');
		    console.dir(params);
			console.log('params build_credit_card_slip *********************');

			if (params.card_type != undefined) {
				receipt_data += builder.createTextElement({width:1,data:'Card Type: '+params.card_type+'\n'});
			}
			if (params.masked_account != undefined) {
		        receipt_data += builder.createTextElement({width:1,data:'Card No.: '+params.masked_account+'\n'});
		    }
		    if (params.auth_code != undefined) {
		        receipt_data += builder.createTextElement({width:1,data:'Auth: '+params.auth_code+'\n\n'});
		    }
		    if (params.auth_amount != undefined) {
		        receipt_data += builder.createTextElement({width:1,data:'Amount: $'+params.auth_amount+'\n\n'});
		    }
		    if (params.print_tip_line != undefined && params.print_tip_line === "1") {
		        receipt_data += builder.createTextElement({data:'\n'+add_white_space('Tip: ','$________.____')});
			    receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('TOTAL CHARGE: ','$________.____')});
		    }
			receipt_data += builder.createTextElement({width:1,data:'\n\nI agree to pay the above amount according to the card issuer agreement.\n\n\n'});
		    receipt_data += builder.createTextElement({width:1,data:'X_____________________________________________\n'});
		    if (params.cardholder_name != undefined) {
			    receipt_data += builder.createTextElement({data:'  '+params.cardholder_name});
		    }
		    receipt_data = this.add_receipt_header(receipt_data, header_data);
		    var final_receipt_data = '';
		    final_receipt_data += receipt_data;
		    if (params.print_two_signature_slips != undefined && params.print_two_signature_slips === "1") {
		    	final_receipt_data += builder.createTextElement({width:1,data:'\n\n**********************************************\nCustomer Copy\n**********************************************\n'});
		    	final_receipt_data = this.add_paper_cut(final_receipt_data);
		    	final_receipt_data += receipt_data;
		    	final_receipt_data += builder.createTextElement({width:1,data:'\n\n**********************************************\nMerchant Copy\n**********************************************\n'});
		    }
		    final_receipt_data = this.add_paper_cut(final_receipt_data);
		}
		return final_receipt_data;
	},
	add_credit_card_slips: function (receipt_data, card_data, header_data) {
		if (this.credit_card_slips != '')
		{
			receipt_data = this.credit_card_slips + receipt_data;
			this.credit_card_slips = '';
		}
		return receipt_data;
	},
	get_date: function() {
		var d = new Date();
	    var curr_date = d.getDate();
	    var curr_month = d.getMonth() + 1; //Months are zero based
	    var curr_year = d.getFullYear().toString();
	    curr_year = curr_year.substring(2);
	    return curr_month + "/" + curr_date + "/" + curr_year;
	},
	get_time: function() {
		var d = new Date();
	    var curr_hours = d.getHours();
	    curr_hours = curr_hours > 12 ? curr_hours - 12 : curr_hours;
	    var curr_min = d.getMinutes();
	    var am_pm = curr_hours > 11 ? 'AM' : 'PM';
	    return curr_hours + ":" + curr_min + am_pm;
	},
	add_itemized_header: function (receipt_data, params, check_number) {
		var builder = new StarWebPrintBuilder();
		var header_data = '';

       	if (params != undefined)
		{
			header_data += builder.createTextElement({width:1,data:'***********************************************\n'});
			header_data += builder.createTextElement({width:1,data:add_white_space('CHECK #'+check_number, add_white_space('DATE', this.get_date(), 13))+'\n'});
			header_data += builder.createTextElement({width:1,data:add_white_space('TABLE #'+params.table_number, add_white_space('TIME', this.get_time(), 13))+'\n'});
			if (params.employee_name != undefined)
				header_data += builder.createTextElement({width:1,data:add_white_space('SERVER:', params.employee_name)+'\n'});
	        if (params.customer != undefined && params.customer != '')
				header_data += builder.createTextElement({width:1,data:add_white_space('CUSTOMER:', params.customer)+'\n'});
			header_data += builder.createTextElement({width:1,data:'***********************************************\n'});

		}


        receipt_data = header_data + receipt_data;
        return receipt_data;
	},
	add_space: function (receipt_data) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createTextElement({data:'\n\n\n'});
		return receipt_data;
	},
	build_itemized_receipt: function (receipt_data, params) {
		var builder = new StarWebPrintBuilder();
		if (params != undefined)
		{
			if (params.cart != undefined)
			{
				var cart = params.cart;
				var erange_codes = params.erange_codes;
			    //Itemized
			    receipt_data += builder.createTextElement({width:1,data:'\n'+add_white_space('  ITEMS ORDERED','AMOUNT')+'\n\n'});
			    for (var line in cart)
			    {
			        receipt_data += builder.createTextElement({width:1,data:add_white_space('  '+cart[line].quantity+' '+cart[line].name+' ', (cart[line].price*cart[line].quantity).toFixed(2))+'\n'});
			    	if (parseInt(cart[line].discount) > 0)
			    		receipt_data += builder.createTextElement({width:1,data:add_white_space('     '+parseFloat(cart[line].discount).toFixed(2)+'% discount', '-$'+(cart[line].discount/100*cart[line].price*cart[line].quantity).toFixed(2))+'\n'});
			    	if (cart[line].sides)
			    	{
			    		var sides = cart[line].sides;
			    		for (var index in sides)
			    			receipt_data += builder.createTextElement({width:1,data:add_white_space('       '+sides[index].name+' ', (sides[index].price).toFixed(2))+'\n'});
			    	}
			    	if (cart[line].erange_code || (erange_codes != undefined && erange_codes[line])) {
			    		var erange_code = cart[line].erange_code ? cart[line].erange_code : erange_codes[line];
			       		receipt_data += builder.createTextElement({width:1,data:add_white_space('     E-Range Code: '+cart[line].erange_code, '')+'\n'});
			       	}
			    }
			}
			receipt_data += builder.createTextElement({width:1,data:'\n-----------------------------------------------\n'});

			if (params.subtotal != undefined)
			{
			    // Totals
			    receipt_data += builder.createTextElement({width:2, data:'\n'+add_white_space('Subtotal: ','$'+parseFloat(params.subtotal).toFixed(2), 23)+'\n'});
			}
			if (params.tax != undefined)
			{
			    //var taxes = params.taxes;
			    //for (var tax in taxes)
			           receipt_data += builder.createTextElement({width:2, data:add_white_space('Tax: ','$'+parseFloat(params.tax).toFixed(2), 23)+'\n'});
			}
			if (params.auto_gratuity != undefined && params.auto_gratuity > 0)
			{
			    //var taxes = params.taxes;
			    //for (var tax in taxes)
			    var gratuity = params.total - (params.subtotal + params.tax);
			           receipt_data += builder.createTextElement({width:2, data:add_white_space('Gratuity '+params.auto_gratuity+'%: ','$'+parseFloat(gratuity).toFixed(2), 23)+'\n'});
			}
			if (params.total != undefined)
			{
			       receipt_data += builder.createTextElement({width:2, data:add_white_space('Total: ','$'+parseFloat(params.total).toFixed(2), 23)+'\n'});
			}
			if (params.payments != undefined)
			{
			    // Payment Types
			    receipt_data += builder.createTextElement({width:1, data:'\nPayments:\n'});
			    var payments = params.payments;
			    for (var payment in payments)
			    {
				    var pay = payment + '';
				    if(pay.indexOf('Gift') >= 0 || pay.indexOf('xxxx') >= 0 || pay.indexOf('Customer') >= 0 || pay.indexOf('Member') >= 0){
						this.show_tip_line = true;
					}
				    this.credit_card_payments += (payments[payment].type+'').indexOf('xxxx') === -1 ? 0 : 1;
				    receipt_data += builder.createTextElement({width:1,data:add_white_space(payments[payment].type+': ','$'+parseFloat(payments[payment].amount).toFixed(2))+'\n'});
			    }
			}
			if (params.amount_change != undefined)
			{
		       // Change due SHOWS UP AS Change Issued under payments
		       //receipt_data += builder.createTextElement({data:'\n'+add_white_space('Change Due: ', params.amount_change)+'\n'});
		    }
		}
		if (params.print_tip_line)
	    {
		    receipt_data = this.add_tip_line(receipt_data);
		}

	    return receipt_data;
	},
	build_receipt_body: function (receipt_data, params) {
		var builder = new StarWebPrintBuilder();
		if (params != undefined)
		{
			if (params.cart != undefined)
			{
				var cart = params.cart;
				var erange_codes = params.erange_codes;
			    //Itemized
			    for (var line in cart)
			    {
			    	receipt_data += builder.createTextElement({width:1,data:add_white_space(cart[line].name+' ',cart[line].quantity+' @ '+parseFloat(cart[line].price).toFixed(2)+'    $'+(cart[line].price*cart[line].quantity).toFixed(2))+'\n'});
			    	if (cart[line]['giftcard_data'] != undefined && cart[line]['giftcard_data']['giftcard_number'] != '')
			    		receipt_data += builder.createTextElement({width:1,data:add_white_space('     #'+cart[line]['giftcard_data']['giftcard_number'],'')+'\n'});
			    	if (parseInt(cart[line].discount) > 0)
			    		receipt_data += builder.createTextElement({width:1,data:add_white_space('     '+parseFloat(cart[line].discount).toFixed(2)+'% discount', '-$'+(cart[line].discount/100*cart[line].price*cart[line].quantity).toFixed(2))+'\n'});
			    	if (cart[line].erange_code || (erange_codes != undefined && erange_codes[line])) {
			    		var erange_code = cart[line].erange_code ? cart[line].erange_code : erange_codes[line];
			       		receipt_data += builder.createTextElement({width:1,data:add_white_space('     E-Range Code: '+erange_code, '')+'\n'});
			       	}
				}
			}
			if (params.subtotal != undefined)
			{
			    // Totals
			    receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('Subtotal: ','$'+parseFloat(params.subtotal).toFixed(2))+'\n'});
			}
			if (params.taxes != undefined)
			{
			    var taxes = params.taxes;
			    for (var tax in taxes)
			           receipt_data += builder.createTextElement({data:add_white_space(tax+': ','$'+parseFloat(taxes[tax]).toFixed(2))+'\n'});
			}
			if (params.total != undefined)
			{
			       receipt_data += builder.createTextElement({data:add_white_space('Total: ','$'+parseFloat(params.total).toFixed(2))+'\n'});
			}
			if (params.payments != undefined)
			{
			    // Payment Types
			    receipt_data += builder.createTextElement({data:'\nPayments:\n'});
			    var payments = params.payments;
			    for (var payment in payments)
			    {
				    var pay = payment + '';
				    if(pay.indexOf('Gift') >= 0 || pay.indexOf('xxxx') >= 0 || pay.indexOf('Customer') >= 0 || pay.indexOf('Member') >= 0){
						this.show_tip_line = true;
					}
				    this.credit_card_payments += (payment+'').indexOf('xxxx') === -1 ? 0 : 1;
				    receipt_data += builder.createTextElement({data:add_white_space(payment+': ','$'+parseFloat(payments[payment].payment_amount).toFixed(2))+'\n'});
			    }
			}
			if (params.amount_change != undefined)
			{
		       // Change due SHOWS UP AS Change Issued UNDER PAYMENTS
		       //receipt_data += builder.createTextElement({data:'\n'+add_white_space('Change Due: ', params.amount_change)+'\n'});
		    }
		}

	    return receipt_data;
	},
	add_return_policy: function (receipt_data, return_policy) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createAlignmentElement({position:'center'});
		receipt_data += builder.createTextElement({data:"\n\n"});// Some spacing at the bottom
	    receipt_data += builder.createTextElement({width:1, data:return_policy});
	    receipt_data += builder.createTextElement({data:"\n\n"});// Some spacing at the bottom
	    receipt_data += builder.createTextElement({data:"\n"});// Some spacing at the bottom

	    return receipt_data;
	},
	add_signature_line: function (receipt_data) {
		var builder = new StarWebPrintBuilder();
	    receipt_data += builder.createTextElement({width:1,data:'\n\n\nX_____________________________________________\n'});

	    return receipt_data;
	},
	add_tip_line: function (receipt_data) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createTextElement({width:1, data:'\n\n'+add_white_space('Tip: ','$________.____')+'\n\n'});
	    receipt_data += builder.createTextElement({width:1, data:add_white_space('TOTAL CHARGE: ','$________.____')});

	    return receipt_data;
	},
	add_barcode: function (receipt_data, label, id) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createAlignmentElement({position:'center'});
	    receipt_data += builder.createTextElement({data:"\n\n"});
		receipt_data += builder.createBarcodeElement({symbology:'Code128', width:'width2', height:40, hri:false, data:id});
	    receipt_data += builder.createTextElement({width:1,data:'\n'+label+': '+id+'\n'});

	    return receipt_data;
	},
	add_receipt_header: function (receipt_data, params) {
		var builder = new StarWebPrintBuilder();
		var header_data = '';

       	if (params != undefined)
		{
			header_data += builder.createAlignmentElement({position:'center'});
	        if (params.course_name != undefined)
		    	header_data += builder.createTextElement({width:1,data:params.course_name+"\n"});

	        if (params.address != undefined)
		    	header_data += builder.createTextElement({width:1,data:params.address+"\n"});
	        if (params.address_2 != undefined)
		    	header_data += builder.createTextElement({width:1,data:params.address_2+"\n"});
            if (params.phone != undefined)
                header_data += builder.createTextElement({width:1,data:params.phone+"\n\n"});

            header_data += builder.createAlignmentElement({position:'left'});

	        if (params.no_date == undefined || !params.no_date)
	        {
	        	if (params.employee_name != undefined)
			    	header_data += builder.createTextElement({width:1,data:"Employee: "+params.employee_name+"\n"});
		        if (params.customer != undefined && params.customer != '')
			        header_data += builder.createTextElement({width:1,data:"Customer: "+params.customer+"\n"});
			}
            if (params.raincheck_number != undefined)
                header_data += builder.createTextElement({width:1,data:"Raincheck: RID "+params.raincheck_number+"\n"});
		}
        if (params.date != undefined) {
            var d = new Date(params.date);
        }
        else {
            var d = new Date();
        }
        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
        var ap = (d.getHours() < 12)?'am':'pm';
        if (params.no_date == undefined || !params.no_date)
        	header_data += builder.createTextElement({width:1,data:"Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n"});

        receipt_data = header_data + receipt_data;
        return receipt_data;
	},
	add_balance_info: function (receipt_data, params) {
		var builder = new StarWebPrintBuilder();
		var header_data = '';

       	if (params != undefined)
		{
			header_data += builder.createAlignmentElement({position:'left'});
	        if (params.giftcards != undefined)
		    {
		    	var giftcards = params.giftcards;
		    	for (var i in giftcards)
			    	header_data += builder.createTextElement({width:1,data:"Gift Card:"+giftcards[i].number+" Ending Balance $"+giftcards[i].new_balance+"\n"});
	       	}
	        if (params.punchcards != undefined)
		    {
		    	var punchcards = params.punchcards;
		    	for (var i in punchcards)
			    	header_data += builder.createTextElement({width:1,data:"Punch Card:"+punchcards[i].number+" Remaining Uses "+punchcards[i].punch_count+"\n"});
	       	}
	        if (params.accounts != undefined)
		    {
		    	var accounts = params.accounts;
		    	for (var i in accounts)
			    	header_data += builder.createTextElement({width:1,data:accounts[i].name+" Ending Balance "+accounts[i].new_balance+"\n"});
	       	}
	        if (params.loyalty != undefined && params.loyalty.balance != undefined)
		    {
		    	if (params.loyalty.earned > 0)
		    		header_data += builder.createTextElement({width:1,data:"You earned "+params.loyalty.earned+" Loyalty Points\n"});
				if  (params.loyalty.spent > 0)
		    		header_data += builder.createTextElement({width:1,data:"You spent "+params.loyalty.spent+" Loyalty Points\n"});
		    	header_data += builder.createTextElement({width:1,data:"Your Loyalty Point Balance is "+params.loyalty.balance+"\n"});
	        }
		}
        receipt_data += header_data;
        return receipt_data;
	},
	add_booking_reminder: function (receipt_data, website) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createAlignmentElement({position:'center'});
        receipt_data += builder.createTextElement({width:1,data:"\nAnd remember, you can book your\nnext tee time online at\n"+website});
    	return receipt_data;
	},
	add_paper_cut: function (receipt_data) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createCutPaperElement({feed:true});
		return receipt_data;
	},
	add_cash_drawer_open: function (receipt_data) {
		var builder = new StarWebPrintBuilder();
		//receipt_data += builder.createRawDataElement({data:chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r"});
		receipt_data = builder.createRawDataElement({data:chr(7)}) + receipt_data;
		return receipt_data;
	},

	old_print: function (receipt_data, url){
		console.log('adding receipt to url ' + url);
		_db.receipt.add(receipt_data, url);
	},

	print: function (receipt_data, url) {
		var self = this;
		if(usePrinterExtension == 1) {
			var ip_address = url.split("/", 3)[2];
			chrome.runtime.sendMessage(this.extension_id, {
					message: "add",
					ip_address: ip_address,
					data: receipt_data
				},
				function (reply) {
					var hasExtension = false;

					if (reply) {
						if (reply.version) {
							if (reply.version >= 0) {
								hasExtension = true;
							}
						}
					}

					if (!hasExtension) {
						self.old_print(receipt_data,url);
					}
				}
			);
		}else {
			this.old_print(receipt_data,url);
		}
	},

	old_print_all: function (url, supress_cash_drawer, callback, onErr) {
		if(webprnt.nag_count > 6) return false; // nobody is listening to you...
		if(webprnt.connection_errors[url] && webprnt.connection_errors[url] > 3) return false; // nobody is listening to you...
		if(!webprnt.connection_errors[url]){
			webprnt.connection_errors[url] = 0;
		}

		// I have not found anywhere that calls
		// this function with a supress_cash_drawer arg
		if (typeof supress_cash_drawer === 'undefined')
			supress_cash_drawer = false;

		var print_url = url.replace('_hot_', '').replace('_cold_', '');
		// CHECK IF WE'RE ALREADY PRINTING
		if (webprnt.currently_printing[print_url])
		{
			console.log('already printing');
			return;
		}

		// GET ALL THE RECEIPTS WAITING TO PRINT
		var receipts = _db.receipt.get(url);
		var receipt_data = '';

		for (var i in receipts)
		{
			receipt_data += receipts[i].receipt_data;
		}
		if (url.indexOf('_hot_') != -1) {
			var cold_receipts = _db.receipt.get(url.replace('_hot_', '_cold_'));

			for (var i in cold_receipts)
			{
				receipt_data += cold_receipts[i].receipt_data;
			}
		}
		if (receipt_data == '')
			return;

		// We will not open cash drawer if user does not want,
		// or if we are already opening the cash drawer
		if (supress_cash_drawer || receipt_data.indexOf('Bw==') > -1) {
			// DO NOTHING!!!!
		}
		else {
			// let us say that the print_all function is the wrong place to decide to open the drawer
			//receipt_data = this.add_cash_drawer_open(receipt_data);
		}

		// MARK IT AS PRINTING
		webprnt.currently_printing[print_url] = true;

		var trader = new StarWebPrintTrader({url:print_url, checkedblock:false});

		trader.onReceive = function (response) {
			currently_printing = false;
			var msg = '- onReceive -\n\n';

			msg += 'TraderSuccess : [ ' + response.traderSuccess + ' ]\n';
			msg += 'TraderStatus : [ ' + response.traderStatus + ',\n';

			if (trader.isCoverOpen            ({traderStatus:response.traderStatus})) {msg += '\tCoverOpen,\n';}
			if (trader.isOffLine              ({traderStatus:response.traderStatus})) {msg += '\tOffLine,\n';}
			if (trader.isCompulsionSwitchClose({traderStatus:response.traderStatus})) {msg += '\tCompulsionSwitchClose,\n';}
			if (trader.isEtbCommandExecute    ({traderStatus:response.traderStatus})) {msg += '\tEtbCommandExecute,\n';}
			if (trader.isHighTemperatureStop  ({traderStatus:response.traderStatus})) {msg += '\tHighTemperatureStop,\n';}
			if (trader.isNonRecoverableError  ({traderStatus:response.traderStatus})) {msg += '\tNonRecoverableError,\n';}
			if (trader.isAutoCutterError      ({traderStatus:response.traderStatus})) {msg += '\tAutoCutterError,\n';}
			if (trader.isBlackMarkError       ({traderStatus:response.traderStatus})) {msg += '\tBlackMarkError,\n';}
			if (trader.isPaperEnd             ({traderStatus:response.traderStatus})) {msg += '\tPaperEnd,\n';}
			if (trader.isPaperNearEnd         ({traderStatus:response.traderStatus})) {msg += '\tPaperNearEnd,\n';}

			msg += '\tEtbCounter = ' + trader.extractionEtbCounter({traderStatus:response.traderStatus}).toString() + ' ]\n';
			console.log('webPrnt onReceive msg - '+msg);
			// MARK IT AS DONE PRINTING
			webprnt.currently_printing[print_url] = false;
			webprnt.connection_errors[print_url] = 0;
			_db.receipt.remove(receipts);
			if (url.indexOf('_hot_') != -1) {
				_db.receipt.remove(cold_receipts);
			}
			if(typeof callback === 'function')callback(trader,response);
		}

		trader.onError = function (response) {
			currently_printing = false;
			var msg = '- onError -\n\n';
			msg += '\tStatus:' + response.status + '\n';
			msg += '\tResponseText:' + response.responseText;
			console.log('webPrnt onError msg - '+msg);
			// MARK IT AS DONE PRINTING
			webprnt.currently_printing[print_url] = false;
			if(typeof onErr === 'function')onErr(trader,response);
			var alert= '';
			if(response.status === 0 && response.responseText === ''){
				alert += 'Cannot find the printer.';
				alert += ' Check to see that it is powered on and connected to the network';
				if(window.location.protocol === 'https:') alert += ' and make sure it is configured to use https'
			}
			console.log(App,webprnt.nag_count,webprnt.last_nagged);
			if(typeof App !== 'undefined' && (webprnt.nag_count < 3 || new Date() - webprnt.last_nagged > 60000*10)) {
				// allow a visible message every 10 minutes
				webprnt.last_nagged = new Date();
				webprnt.nag_count++;
				App.vent.trigger('notification', {'type': 'error', 'msg': alert});
			}
			else {
				// otherwise, make it available in the console
				console.log(alert);
			}
			webprnt.connection_errors[print_url]++;
		}
		if(typeof window.smite_printing !== 'undefined' && window.smite_printing)
			return
		trader.sendMessage({request:receipt_data});
	},

	print_all: function (url, supress_cash_drawer, callback, onErr) {
		var self = this;
		if(typeof usePrinterExtension !== 'undefined' && usePrinterExtension == 1) {
			var ip_address = url.split("/", 3)[2];
			chrome.runtime.sendMessage(this.extension_id, {
					message: "print",
					ip_address: ip_address
				},
				function (reply) {
					var hasExtension = false;

					if (reply) {
						if (reply.version) {
							if (reply.version >= 0) {
								hasExtension = true;
							}
						}
					}

					if (!hasExtension) {
						self.old_print_all(url, supress_cash_drawer, callback, onErr);
					}
				}
			);
		}else {
			this.old_print_all(url, supress_cash_drawer, callback, onErr);
		}
	}
};

function chr(i) {
      return String.fromCharCode(i);
}
