/* 	Card swipe plugin
Author: 		Jordan Bush
Date authored: 	2/3/2015

Detects a card swipe based on typing speed
*/
(function($){
	
	var timestamp = 0, 
		is_human = true; 
		card_data = '';
		swipe_started = false;
        first_chars = '';
        last_character = '';
        lead_character = '';

    $.fn.cardSwipe = function(options){
		
		var settings = $.extend({
			speedThreshold: 75,
			lengthThreshold: 6,
			setInput: true,
			parseData: true
		}, options);
		
		var _TARGET = this;
		
		// Parse the raw card data
		var parse = function(raw_data){
			
			var data = '';
			
			// Check if there was an error swiping the card
			if(!raw_data || (raw_data[0] == '%' && raw_data[1] == 'E')){
				return false;
			}	
			
			if(settings.parseData){
				
				// Find ending of meta data at beginning of string
				for(var i = 0; i < raw_data.length; i++){
					if(!isNaN(parseInt(raw_data[i]))){
						break;
					}
				}
				// Trim off starting meta data
				if(i > 0){
					raw_data = raw_data.slice(i);	
				}

				// Loop through each character, when arriving at non-number, exit
				for(var x in raw_data){
					if(isNaN(parseInt(raw_data[x]))){
						break;
					}
					data += raw_data[x];
				}	
			
			}else{
				data = raw_data;
			}

			return data;
		};
		
		_TARGET.keydown(function(e){
			
			var cur_timestamp = Date.now();
			is_swipe = (cur_timestamp - timestamp < settings.speedThreshold);		
			timestamp = cur_timestamp;
            var character = String.fromCharCode(e.keyCode);

			// If typing is fast enough to be a card swipe, and the data is over the length threshold
			// to be detected
			if(is_swipe)
            {
                if (lead_character == '') {
                    // Capturing the first character compensates for the fact that we can't detect that its a swipe until character 2
                    lead_character = last_character;
                }
                if(_TARGET.val().length > settings.lengthThreshold || swipe_started)
                {
                    if (!swipe_started) {
                        _TARGET.trigger('onCardswipe');
                        swipe_started = true;
                        // This is updated to reset the field in case there were characters that need to be cleared out before the swipe started
                        card_data = lead_character+''+first_chars;//_TARGET.val();
                        _TARGET.val('');
                    }

                    card_data += character;

                    if ((e.keyCode == 13) && is_swipe) {
                        var parsed = parse(card_data);

                        card_data = '';
                        swipe_started = false;
                        first_chars = '';
                        lead_character = '';

                        if (!parsed) {
                            _TARGET.trigger('cardswipeError');

                        } else {
                            if (settings.setInput) {
                                _TARGET.val(parsed);
                            }
                            _TARGET.trigger('cardswipe', [parsed]);
                        }

                        _TARGET.trigger('afterCardswipe');
                        return true;
                    }
                    // Swipe is triggered if they hold down a key on the keyboard.... this will catch that after the effect and finish the 'swipe'
                    setTimeout(function(){
                        if (timestamp < Date.now() && is_swipe && swipe_started) {
                            var parsed = parse(card_data);

                            card_data = '';
                            swipe_started = false;
                            first_chars = '';
                            lead_character = '';

                            if (!parsed) {
                                _TARGET.trigger('cardswipeError');

                            } else {
                                if (settings.setInput) {
                                    _TARGET.val(parsed);
                                }
                                _TARGET.trigger('cardswipe', [parsed]);
                            }

                            _TARGET.trigger('afterCardswipe');
                        }
                    }, 400);
                    return false;
                }
                else if (_TARGET.val().length <= settings.lengthThreshold) {
                    first_chars += character;
                }
			}
            last_character = character;
		});
	}
}(jQuery));
