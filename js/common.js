function get_dimensions() 
{
	var dims = {width:0,height:0};
	
  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    dims.width = window.innerWidth;
    dims.height = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    dims.width = document.documentElement.clientWidth;
    dims.height = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    dims.width = document.body.clientWidth;
    dims.height = document.body.clientHeight;
  }
  
  return dims;
}
function pause(ms) {
	ms += new Date().getTime();
	while (new Date() < ms){}
} 
function customer_flag_teesheet(flag, comment) 
{ 
	$('#feedback_bar').hide(); 
	if(flag == 1) { 
		set_feedback('Customer Comment: '+comment, 'error_message', true); 
	} else if (flag == 2) { 
		set_feedback('Customer Comment: '+comment, 'warning_message', true);
	}
}

function set_feedback(text, classname, keep_displayed, time_override)
{
	if(text!='' && text!=undefined)
	{
		$('#feedback_bar').removeClass();
		$('#feedback_bar').addClass(classname);
		$('#feedback_bar').html(text+"<span class='message_close_box'></span>");
		$('#feedback_bar').slideDown(250);
		var text_length = text.length;
		var text_lengthx = 0;
		if (time_override != ''&& time_override != undefined)
			text_lengthx = time_override;
		else
			text_lengthx = text_length*50;
		
		if(!keep_displayed)
		{
			$('#feedback_bar').show();
			
			setTimeout(function()
			{
				$('#feedback_bar').slideUp(250, function()
				{
					$('#feedback_bar').removeClass();
				});
			},text_lengthx);
		}
	}
	else
	{
		$('#feedback_bar').hide();
	}
}
function trace(traced){
    var type = typeof traced;
    if (type == 'number' || type == 'string' || type == 'boolean') 
        if (window.console && console.log)
            console.log(traced);
    else if (type == 'object') 
        if (window.console && console.dir)
            console.dir(traced);
    else if (type == null || type == 'undefined') 
        if (window.console && console.log)
            console.log('null or undefined');
}
function updateClock(){
    var days_of_week = ['Sun','Mon','Tues','Wed','Thur','Fri','Sat'];
    var current_date_time = new Date();
    var hours = current_date_time.getHours();
    var minutes = current_date_time.getMinutes();
    var am_pm = 'am';
    var m_z = '';
    var day_of_week = days_of_week[current_date_time.getDay()];
    if (hours > 11)
        am_pm = 'pm';
    if (hours > 12)
        hours -= 12;
    if (parseInt(minutes) < 10)
        m_z =  '0';
    var cur_time = hours+':'+m_z+''+minutes;
    var cur_day = day_of_week+'<br/>'+am_pm;
    $('#menubar_date_time').html(cur_time);
    $('#menubar_dat_day').html(cur_day);
    
    setTimeout('updateClock()', 60000);
}
function addslashes(str) {
	str=str.replace(/\\/g,'\\\\');
	str=str.replace(/\'/g,'\\\'');
	str=str.replace(/\"/g,'\\"');
	str=str.replace(/\0/g,'\\0');
	return str;
}
function stripslashes(str) {
	str=str.replace(/\\'/g,'\'');
	str=str.replace(/\\"/g,'"');
	str=str.replace(/\\0/g,'\0');
	str=str.replace(/\\\\/g,'\\');
	return str;
}
var pagination = {
	override_links:function() {
		$('#pagination a').click(function(e){
			e.preventDefault();
			var href = $(e.target).attr('href');
			var start_pos = href.lastIndexOf('/')+1;
			var count = href.slice(start_pos);
			console.log('count '+count);
			count = count == ''? 0:count;
			$('#offset').val(count);
			//alert('you clicked a link! '+ count);
			$('#search_form').submit();
		})
	}
}
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();
$(document).ready(function(){
    //updateClock();
    //$('#feedback_button').colorbox({'href':'index.php/system/feedback','height':460,'width':420});
})

var sales = {
	all_selected:false,
	override:function(){
		$.colorbox({href:'index.php/sales/view_override/width~600',title:'Override Sale'});
	},
	open_pin_window:function(){
		$("body").unmask();
		$.colorbox({href:'index.php/sales/view_pin_window/width~400',title:'Pin Required to Finish Transaction'});
	},
	round: function(value, places) {

		if(typeof(value) != 'number'){
			value = parseFloat(value);
		}
		if(isNaN(value)){
			value = 0;
		}
		if(!places){
			places = 2;
		}

		return +(Math.round(value + "e+" + places)  + "e-" + places);
	},
    select_all_toggle: function(which) {
        var checked = $(which).attr('checked');
        if (checked == true) 
            $('input[name*=select]').attr('checked', 'checked');
        else 
            $('input[name*=select]').removeAttr('checked');

        $.ajax({
           type: "POST",
           url: "index.php/sales/update_basket",
           data: "line=all&checked="+checked,
           success: function(response){
               trace(response);
               result = eval('('+response+')');
               sales.update_basket_totals('all', result);
           }
        });
    },
    toggle_taxable: function(which) {
    	var checked = $(which).attr('checked');
        $.ajax({
           type: "POST",
           url: "index.php/sales/change_taxable",
           data: "taxable="+checked,
           success: function(response){
               trace(response);
               sales.update_basket_totals('all', response);
           },
           dataType:'json'
        });
    },
    select_cart_line: function(line, which) {
        trace('selecting: '+line+' '+$(which).attr('checked'));
        var checked = $(which).attr('checked');
        if (checked !== true)
            $('input[name=select_all]').attr('checked', '');
            
        $.ajax({
           type: "POST",
           url: "index.php/sales/update_basket",
           data: "line="+line+"&checked="+checked,
           success: function(response){
               sales.update_basket_totals(line, response);
           },
           dataType:'json'
        });
    },
    delete_checked_lines: function(){
        var deleted_lines = '';
        $("input:[name*=select]:checked").each(function(i,v){
            deleted_lines += '&line_array[]='+$(v).attr('name');
        });
        $.ajax({
           type: "POST",
           url: "index.php/sales/delete_cart_items",
           data: "line=none"+deleted_lines,
           success: function(response){
               //result = eval('('+response+')');
               window.location = SITE_URL + "/sales";
           }
        });
    },
    change_item: function(line, which){
        trace('changing item');
//        var item_id = $(which).val();
        var item = $('input[name=item_id_'+line+']').val();
        var type = $('select[name=item_type_'+line+']').val();
        this.update_cart_ajax(line, 'item_number', type, true)
    },
    change_cart: function(line, which){
        trace('changing item');
//        var item_id = $(which).val();
        var item = $('select[name=item_id_'+line+']').val();
        var type = $('select[name=item_type_'+line+']').val();
        this.update_cart_ajax(line, 'item_number', type, true)
    },
    change_line_price: function (line, which, e){
        trace('changing line price');
        //console.dir($(which));
        //console.log('keycode '+e.which);
        // var k = e.which;        
        // if (k==8||(k>45&&k<58)||(k>95&&k<106)||k==110||k==190)
        // {
        //if (!isNaN(parseFloat($(which).val()))) {
            var price = (!isNaN(parseFloat($(which).val())))?parseFloat($(which).val()).toFixed(2):0;//).toFixed(2);
            console.log('UPDATING CART TO PRICE: ',price);
            this.update_cart_ajax(line, 'price', price);
        // }
        //}
        //else if ($(which).val() == '')
        //{

        //}
        //else {
        //    $(which).val('');
            //alert('Please enter a numerical value.');
        //}
    },
    change_line_quantity: function (line, which){
        trace('changing line quantity');
        if (!isNaN(parseFloat($(which).val()))) {
            var quantity = parseFloat($(which).val());
            this.update_cart_ajax(line, 'quantity', quantity);
        }
        else if ($(which).val() == '')
        {

        }
        else {
            $(which).val('');
            //alert('Please enter a numerical value.');
        }
    },
    change_line_discount: function (line, which){
        trace('changing line discount');
        if (!isNaN(parseFloat($(which).val()))) {
            var discount = $(which).val();
            this.update_cart_ajax(line, 'discount', discount);
        }
        else if ($(which).val() == '')
        {

        }
        else {
            $(which).val('');
            //alert('Please enter a numerical value.');
        }
    },
    update_line_price: function (){
        
    },
    update_line_total: function (){
        
    },
    update_basket_totals:function(line, basket_info){
        trace('getting inside update basket totals');
        //console.dir(basket_info);
        trace('after basket info');
        if (basket_info != undefined) {
            if (basket_info.items_in_basket != undefined) 
                $('#items_in_basket').html(basket_info.items_in_basket);
            if (basket_info.subtotal != undefined) 
            {
                $('#basket_subtotal').html('$'+basket_info.subtotal);
                $('#basket_total').html('$'+basket_info.subtotal);
            }
            if (basket_info.total != undefined) 
                $('#basket_final_total').html('$'+basket_info.total);
            if (basket_info.amount_due != undefined) {
                $('.due_amount').html('$'+basket_info.amount_due);
                $('#amount_tendered').val(basket_info.amount_due);
            }
            //console.dir(basket_info.taxes);
            if (basket_info.taxes != undefined) {
                var tax_html = '';
                for (var index in basket_info.taxes) {
                    var tax = basket_info.taxes[index];
                    console.dir(tax);
                    tax_html += ' <div>'+
						              '<div class="right register_taxes">$'+tax.amount.toFixed(2)+'</div>'+
				                          '<div class="left register_taxes">'+tax.name+':</div>'+
				                          '<div class="clear"></div>'+
				                      '</div>';
                }
                //$('.register_taxes').remove();
                $('#taxes_holder').html(tax_html);
            }
        }
        
    },
    update_cart_info: function (line, item_info, update_price){
    	//console.log('update_cart_info');
    	//console.dir(item_info);
        // Need to update stock, price, qty, disc, and total
        //if (update_price)
        var price =	$('input:[name=price_'+line+']');
    	if (parseFloat(price.val()) != parseFloat(item_info.price))
        {
        	price.val(item_info.price);        	        	
        }
        //$('input:[name=quantity'+line+']').val(item_info.quantity);
        var discount = $('input:[name=discount_'+line+']');
        var discount_val = discount.val() == '' ? 0 : discount.val();
        //console.dir(discount);
        //console.log(line+': '+parseFloat(discount.val())+' <-  -> '+parseFloat(item_info.discount));
        if (parseFloat(discount_val) != parseFloat(item_info.discount) )
        {
        	discount.val(item_info.discount);
        	set_feedback("Maximum discount on this item is "+item_info.discount+"% <a href='javascript:sales.override();'>Override</a>",'success_message',true);
        }
		if(!item_info.modifier_total){
			item_info.modifier_total = 0;
		}
        $('#reg_item_total_'+line).html('<div class="total_price_box">$'+sales.round(((100-parseFloat(item_info.discount))/100)*(parseFloat(item_info.price)+parseFloat(item_info.modifier_total))*parseFloat(item_info.quantity),2)+"</div>");
    },
    update_cart_ajax: function (line, attribute, value, update_price){
    	update_price = (update_price === undefined)?false:update_price;
        var checked = $("input:[name=select_"+line+"]").attr('checked');
        // delay(function(){
	      $.ajax({
	           type: "POST",
	           url: "index.php/sales/update_cart",
	           data: "line="+line+"&"+attribute+"="+value+"&checked="+checked,
	           success: function(response){
	               result = eval('('+response+')');
	               sales.update_cart_info(line, result.item_info, update_price);
	               sales.update_basket_totals(line, result.basket_info);
	           }
	        });
	    // }, 1000 );  
    },
    delete_item: function (lineNumber){
	    $.ajax({
	       type: "POST",
	       url: "index.php/sales/delete_item/"+lineNumber,
	       data: "",
	       success: function(response) {
				sales.update_page_sections(response);
   		   },
	       dataType:'json'
	    });
    },
    add_item: function (item_id, callback, price_index, teetime_type, timeframe_id){    
    	price_index = price_index == undefined ? '' : price_index;
    	teetime_type = teetime_type == undefined ? '' : teetime_type;
    	timeframe_id = timeframe_id == undefined ? '' : timeframe_id;
	
	    $.ajax({
	       type: "POST",
	       url: "index.php/sales/add/"+item_id,
	       data: {
	      		"price_index":price_index,
	      		"teetime_type":teetime_type,
	      		"timeframe_id":timeframe_id
	      	},
	       success: function(response) {	
	       		console.log('add_item js');       		       		
	       		sales.update_page_sections(response);
	       		if (typeof callback === 'function'){	       			
	       			callback();
	       		}   	   					   			
		   },
	       dataType:'json'
	    });
    },
    update_register_box: function(response){
    },
    change_sales_mode: function (mode){
	    $.ajax({
	       type: "POST",
	       url: "index.php/sales/change_mode/"+mode,
	       data: "",
	       success: function(response) {
				sales.update_page_sections(response);
    	   },
	       dataType:'json'
	    });
    },
    suspend_sale: function (table){
	    $.ajax({
	       type: "POST",
	       url: "index.php/sales/suspend/"+table,
	       data: "",
	       success: function(response) {
		   		if (sales.update_page_sections(response))
		   		{
		   			sales.setup_suspend_button('', response['suspend_button_title']);
		   			$.colorbox.close();
		   		}
		   },
	       dataType:'json'
	    });
    },
    unsuspend_sale: function (sale_id, table){
	    $.ajax({
	       type: "POST",
	       url: "index.php/sales/unsuspend/"+sale_id,
	       data: "",
	       success: function(response) {
		   		sales.update_page_sections(response);
		   		sales.setup_suspend_button(table);
		   },
	       dataType:'json'
	    });
    },
    update_page_sections:function(response) {
    	console.log('update_page_sections');
    	console.dir(response);
       	if (response) {
    		if (response['message']) {
    			set_feedback(response['message']['text'], response['message']['type'], response['message']['persist']);
    		}
    		if (response['register_box']) {
	   			$("#register_box").html(response['register_box']);		   				   						   			 				   			
   			}
   			if (response['basket_info']) {
	   			sales.update_basket_totals('all', response['basket_info']);
   			}
   			if (response['customer_info_filled']) {
	   			$("#customer_info_filled").html(response['customer_info_filled']);
	   		}
	   		if (response['payment_window']) {
		   		$("#make_payment").html(response['payment_window']);
	   		}
	   		if (response['payments']){
	   			console.log('are we even getting here');
	   			console.log(response.payments);
	   			$('.payments_and_tender').replaceWith(response.payments);
	   			$('.colbox').colorbox();
	   		}
	   		if (response['suspended_sales']) {
		   		$("#suspended_sales").html(response['suspended_sales']);
	   		}
			if (response['table_top']) {
	   			$("#table_top").html(response['table_top']);
   			}
	   		if (response['amount_due']){
	   			$('#amount_tendered').val(response.amount_due).select();
	   		}			   					   										   	
			if (response['recent_transactions']) {
		   		$("#recent_transactions").html(response['recent_transactions']);
	   		}
	   		if (response['suspend_button_title']) {
			   	sales.setup_suspend_button('', response['suspend_button_title']);
		   	}
		   	
		   	if (response['is_cart_empty']) {
			   	// remove payment type values
			   	$(".payments_and_tender :form").each(function() {
					$(this).remove();
			   	});
		   	}
		   	if (response['table_top']) {
	   			if (response['mode'] == 'sale')
		   		{
			   		$("#register_items_container").removeClass('return');
			   		$('#mode').val('sale');
			   		$("#payments_button").html('Pay Now');
		   		}
		   		else
		   		{
			   		$('#mode').val('return');
			   		$("#register_items_container").addClass('return');
			   		$("#payments_button").html('Return Now');
		   		}
		   		$("#table_top").html(response['table_top']);
	   		}
	   		return true;
   		} else {
   			document.location = document.location;
   		}
		return false;   		
    },
    setup_suspend_button: function (table, title){
    	if (table != undefined && table != '') {
    		$('#suspend_sale_button').attr('href','javascript:void(0)');
    		$("#suspend_sale_button").colorbox.remove();
	    	$('#suspend_sale_button').click(function(e) {
	    		e.preventDefault();
		    	sales.suspend_sale(table);
	    	});
	    	$('#payment_suspend_sale').html('Update Sale #'+table);
    	} else {
    		$("#suspend_sale_button").unbind("click");
    		$("#suspend_sale_button").colorbox({width:548,height:340,title:'Assign a table/number',href:'index.php/sales/suspend_menu',onComplete:function(){$.colorbox.resize()}});
	    	//$('#suspend_sale_button').attr('href','index.php/sales/suspend_menu/');
	    	$('#payment_suspend_sale').html(title);
    	}
    },
    cancel_sale: function (){    	
	    $.ajax({
	       type: "POST",
	       url: "index.php/sales/cancel_sale",
	       data: "",
	       success: function(response) {
	       		sales.update_page_sections(response);	
	       },
	       dataType:'json'
	    });
    },
    load_return: function (sale_id, type, controller){
	    $.ajax({
	       type: "POST",
	       url: "index.php/sales/load_return/"+sale_id+"/"+type,
	       data: "",
	       success: function(response) {
	       	   if (controller == 'sales')
			       sales.update_page_sections(response);
			   else
			       document.location = 'index.php/sales';
		       $.colorbox.close();
		   },
	       dataType:'json'
	    });
    },
    email_invoice: function(invoice_id){    	
    	$.ajax({
	       type: "POST",
	       url: "index.php/customers/email_invoice/"+invoice_id,
	       data: "",
	       success: function(response) {		       
		       
		   },
	       dataType:'json'
	    });
    },
    edit_invoice: function(invoice_id){    
    	$.colorbox2({href:'index.php/customers/edit_invoice_totals/'+invoice_id,width:'500'});	
    },
    load_invoice_in_pos: function (invoice_id) {
    	redirect = function(){
			window.location = '/index.php/sales';
		};
		
		sales.add_item(invoice_id, redirect);
    }
}

//toggling positive negative sign and color for any plus/minus inputs
//handle making the number negative

