$(document).ready(function() {

    $('.dropdown-toggle').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        toggleDropdown($(this).parent());
    });

    $(document).on('click', function() {
        var dropdowns = $('.dropdown')
        if (dropdowns.hasClass('isActive')) {
            dropdowns.removeClass('isActive');
        }
    });

});


function toggleDropdown(element) {
    $('.dropdown').removeClass('isActive')
    element.addClass('isActive');
}