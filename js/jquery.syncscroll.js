jQuery.fn.synchronizeScroll = function(type) {
	if (type == undefined)
		type = 'hw';
    var elements = this;
    var hovered_element_id = '';
    if (elements.length <= 1) return;
   //console.log('elements-------------');
   //console.dir()
   	/*elements.mouseover(
   	function() {
   		hovered_element_id = $(this).parent().parent().parent().parent('.track_calendar').attr('id');
   		//console.log('hovered_element_id '+hovered_element_id);
   		//console.dir($(this).parent().parent().parent().parent('.track_calendar'));
   	});*/
    elements.scroll(
    function(e) {
    	e.preventDefault();
    	var left = $(this).scrollLeft();
        var top = $(this).scrollTop();
        elements.each(
            function() {
            	//var current_cal_id = $(this).parent().parent().parent().parent('.track_calendar').attr('id');
                //if (current_cal_id != hovered_element_id)
	        	{
	        		//console.log(current_cal_id+'  -----  '+hovered_element_id);
	        		if (type == 'hw' || type == 'h')
	        		if ($(this).scrollLeft() != left) $(this).scrollLeft(left);
	        		if (type == 'hw' || type == 'w')
	                if ($(this).scrollTop() != top) $(this).scrollTop(top);
	            }
            }
        );
    });
}