var App = window.parent.App;

var pax_transaction = {
    InvoiceNo : '',
    TranType : '',
    CheckCount: 0,
    CommandArray : '',
    CertURL : '',
    DevicePort : '',
    TerminalID : '',
    TransactionNumber : '',
    requestCommandArrString : '',
    response_keys : {
        0 : {'key' : 'status'},
        1 : {'key' : 'command'},
        2 : {'key' : 'version'},
        3 : {'key' : 'response_code'},
        4 : {'key' : 'response_message'},
        5 : {'key' : 'host_information',
            'data' : {
                0 : {'key' : 'host_response_code'},
                1 : {'key' : 'host_response_message'},
                2 : {'key' : 'auth_code'},
                3 : {'key' : 'host_reference_number'},
                4 : {'key' : 'trace_number'},
                5 : {'key' : 'batch_number'}
            }
        },
        6 : {'key' : 'transaction_type'},
        7 : {'key' : 'amount_information',
            'data' : {
                0 : {'key' : 'approve_amount'},
                1 : {'key' : 'amount_due'},
                2 : {'key' : 'tip_amount'},
                3 : {'key' : 'cash_back_amount'},
                4 : {'key' : 'merchant_fee'},
                5 : {'key' : 'tax_amount'},
                6 : {'key' : 'balance1'},
                7 : {'key' : 'balance2'}
            }
        },
        8 : {'key' : 'account_information',
            'data' : {
                0 : {'key' : 'account'},
                1 : {'key' : 'entry_mode'},
                2 : {'key' : 'expire_date'},
                3 : {'key' : 'ebt_type'},
                4 : {'key' : 'voucher_number'},
                5 : {'key' : 'new_account_no'},
                6 : {'key' : 'card_type'},
                7 : {'key' : 'card_holder'},
                8 : {'key' : 'cvd_approval_code'},
                9 : {'key' : 'cvd_message'},
                10 : {'key' : 'card_present_indicator'}
            }
        },
        9 : {'key' : 'trace_information',
            'data' : {
                0 : {'key' : 'transaction_number'},
                1 : {'key' : 'reference_number'},
                2 : {'key' : 'time_stamp'}
            }
        },
        10 : {'key' : 'avs_information',
            'data' : {
                0 : {'key' : 'avs_approval_code'},
                1 : {'key' : 'avs_message'}
            }
        },
        11 : {'key' : 'commercial_information',
            'data' : {
                0 : {'key' : 'po_number'},
                1 : {'key' : 'customer_code'},
                2 : {'key' : 'tax_exempt'},
                3 : {'key' : 'tax_exempt_id'}
            }
        },
        12 : {'key' : 'moto'},
        13 : {'key' : 'additional_information'}
    },
    trans_check_response_keys : {
        0 : {'key' : 'status'},
        1 : {'key' : 'command'},
        2 : {'key' : 'version'},
        3 : {'key' : 'response_code'},
        4 : {'key' : 'response_message'},
        5 : {'key' : 'total_record'},
        6 : {'key' : 'record_number'},
        7 : {'key' : 'host_information',
            'data' : {
                0 : {'key' : 'host_response_code'},
                1 : {'key' : 'host_response_message'},
                2 : {'key' : 'auth_code'},
                3 : {'key' : 'host_reference_number'},
                4 : {'key' : 'trace_number'},
                5 : {'key' : 'batch_number'}
            }
        },
        8 : {'key' : 'edc_type'},
        9 : {'key' : 'transaction_type'},
        10 : {'key' : 'orig_transaction_type'},
        11 : {'key' : 'amount_information',
            'data' : {
                0 : {'key' : 'approve_amount'},
                1 : {'key' : 'amount_due'},
                2 : {'key' : 'tip_amount'},
                3 : {'key' : 'cash_back_amount'},
                4 : {'key' : 'merchant_fee'},
                5 : {'key' : 'tax_amount'},
                6 : {'key' : 'balance1'},
                7 : {'key' : 'balance2'}
            }
        },
        12 : {'key' : 'account_information',
            'data' : {
                0 : {'key' : 'account'},
                1 : {'key' : 'entry_mode'},
                2 : {'key' : 'expire_date'},
                3 : {'key' : 'ebt_type'},
                4 : {'key' : 'voucher_number'},
                5 : {'key' : 'new_account_no'},
                6 : {'key' : 'card_type'},
                7 : {'key' : 'card_holder'},
                8 : {'key' : 'cvd_approval_code'},
                9 : {'key' : 'cvd_message'},
                10 : {'key' : 'card_present_indicator'}
            }
        },
        13 : {'key' : 'trace_information',
            'data' : {
                0 : {'key' : 'transaction_number'},
                1 : {'key' : 'reference_number'},
                2 : {'key' : 'time_stamp'}
            }
        },
        14 : {'key' : 'cashier_information',
            'data' : {
                0 : {'key' : 'clerk_id'},
                1 : {'key' : 'shift_id'}
            }
        },
        15 : {'key' : 'commercial_information',
            'data' : {
                0 : {'key' : 'po_number'},
                1 : {'key' : 'customer_code'},
                2 : {'key' : 'tax_exempt'},
                3 : {'key' : 'tax_exempt_id'}
            }
        },
        16 : {'key' : 'check_information',
            'data' : {
                0 : {'key' : 'sale_type'},
                1 : {'key' : 'routing_number'},
                2 : {'key' : 'account_number'},
                3 : {'key' : 'check_number'},
                4 : {'key' : 'check_type'},
                5 : {'key' : 'id_type'},
                6 : {'key' : 'id_value'},
                7 : {'key' : 'date_of_birth'},
                8 : {'key' : 'phone_number'},
                9 : {'key' : 'zip_code'}
            }
        },
        17 : {'key' : 'additional_information'}
    },
    responseCodes : {
        '000000' : 'Payment was Approved',
        '000100' : 'Payment was Declined',
        '100001' : 'Payment timed out',
        '100002' : 'Payment was aborted',
        '100003' : 'Payment error: ',
        '100004' : 'This transaction is not supported',
        '100005' : 'Unsupported EDC type',
        '100006' : 'Batch failed',
        '100007' : 'Connection error',
        '100008' : 'Error sending payment',
        '100009' : 'Error receiving payment response',
        '100010' : 'Communication error',
        '100011' : 'Duplicate Transaction',
        '100012' : 'GetVar error',
        '100013' : 'Missing Data',
        '100014' : 'Invalid Data',
        '100015' : 'CVV Error',
        '100016' : 'AVS Error',
        '100017' : 'Error: Halo Exceed',
        '100018' : 'Swipe only',
        '100019' : 'Swipe error',
        '100020' : 'Cannot adjust tip',
        '100021' : 'Transaction already voided',
        '100022' : 'Pin pad error',
        '100023' : 'Unknown error',
        '100024' : 'Error: No host app',
        '100025' : 'Please do settlement',
        '100027' : 'Unsupported command',
        '100028' : 'Tax amount larger than purchase amount',
        '199999' : 'Internal terminal error'
    },
    card_types : {
        '01' : 'VISA',
        '02' : 'M/C',
        '03' : 'AMEX',
        '04' : 'DCVR'
    },
    sendCreditSale : function() {
        // Build the Credit Sale Command <STX><COMMAND REQUEST PACKET><ETX><LRC> Example: [02]T00[1c]1.28[1c]01[1c]100[1c][1c]1[1c][1c][1c][1c][1c][03]C
        var CommandArray = new Uint8Array(JSON.parse(pax_transaction.CommandArray));
        var CommandString = pax_transaction.packageCommand(CommandArray);

        pax_transaction.sendTransaction(CommandString);
    },
    sendCreditSaleVoid : function() {
        // Build the Credit Sale Command <STX><COMMAND REQUEST PACKET><ETX><LRC> Example: [02]T00[1c]1.28[1c]01[1c]100[1c][1c]1[1c][1c][1c][1c][1c][03]C
        var CommandArray = new Uint8Array(JSON.parse(pax_transaction.CommandArray));
        var CommandString = pax_transaction.packageCommand(CommandArray);

        pax_transaction.sendTransaction(CommandString);
    },
    sendTransactionCheck : function() {
        // Build the Credit Sale Command <STX><COMMAND REQUEST PACKET><ETX><LRC> Example: [02]T00[1c]1.28[1c]01[1c]100[1c][1c]1[1c][1c][1c][1c][1c][03]C
        window.parent.$('#modal-payment_window').loadMask({'message': 'Credit card communication error. Canceling incomplete payments.'});
        var CommandArray = new Uint8Array(JSON.parse(pax_transaction.CheckCommandArray));
        var CommandString = pax_transaction.packageCommand(CommandArray);

        pax_transaction.sendTransaction(CommandString, 1, function(){pax_transaction.sendVoidTransaction();});
    },
    sendVoidTransaction : function() {
        var CharArray = JSON.parse(pax_transaction.VoidCommandArray);
        var AddedTransNumber = false;
        var FinalCharArray = [];
        for (var i in CharArray) {
            if (CharArray[i] == '88') {
                if (!AddedTransNumber){
                    for(var j in pax_transaction.TransactionNumber){
                        FinalCharArray.push(pax_transaction.TransactionNumber[j].charCodeAt(0));
                    }
                    AddedTransNumber = true;
                }
            }
            else {
                FinalCharArray.push(CharArray[i]);
            }
        }
        var CommandArray = new Uint8Array(FinalCharArray);
        var CommandString = pax_transaction.packageCommand(CommandArray);
        AddedTransNumber = false;
        var callback = function(){
            pax_transaction.closePaymentWindow();
            App.vent.trigger('notification', {'msg': 'Payment successfully cancelled. There was a communication error, please try again', 'type':'error', 'delay':30000});
        }
        pax_transaction.sendTransaction(CommandString, 1, callback);

    },
    closePaymentWindow:function(){
        window.parent.$.loadMask.hide();
        window.parent.$('#modal-payment_window').modal('hide');
    },
    sendTransaction : function (CommandString, TransCheck, Callback)
    {
        // Perform Asynchronus HTTP GET Call
        pax_transaction.httpGetAsync(window.location.protocol+'//'+pax_transaction.CertURL+':'+pax_transaction.DevicePort+'?' + CommandString, TransCheck, Callback);
    },
    packageCommand : function (CommandArray, Replacements)
    {
        // Extract the string to perform the LRC check. The check begins after the STX and proceeds and includes the ETX.
        var CommandArrString = new TextDecoder("utf-8").decode(CommandArray);
        if (typeof Replacements != 'undefined')
        {
            for(var i in Replacements)
            {
                CommandArrString = CommandArrString.replace(Replacements[i][0],Replacements[i][1]);
            }
        }
        console.log(CommandArrString);
        pax_transaction.requestCommandArrString = CommandArrString;
        var CommandArrayLength = CommandArrString.length;
        var LRCstring = CommandArrString.substring(1, CommandArrayLength );

        // Add the LRC to the ByteArray
        var LRC = pax_transaction.calculateLRC(LRCstring);
        var num = LRC.charCodeAt(0);
        CommandArray[CommandArrayLength - 1] = num;

        // Base64 Encode the String
        var CommandString = pax_transaction.arrayBufferToBase64(CommandArray);

        return CommandString;
    },
    parse : function(text)
    {
        var pieces = text.split('\u001c');
        for (var j in pieces) {
            if (pieces[j].indexOf('\u001f') != -1) {
                pieces[j] = pieces[j].split('\u001f');
            }
        }

        return pieces;
    },
    keyResponse : function (pieces, trans_check)
    {
        if (typeof trans_check == 'undefined') {
            var keys = pax_transaction.response_keys;
        } else {
            var keys = pax_transaction.trans_check_response_keys;
        }

        var keyed_array = {};
        for (var i in pieces) {
            if (Array.isArray(pieces[i])) {
                for(var j in pieces[i]) {

                    if (typeof keyed_array[keys[i]['key']] == 'undefined') {
                        keyed_array[keys[i]['key']] = {};
                    }

                    if (typeof keys[i]['data'] != 'undefined') {

                        // Keying 2nd level data
                        if (typeof keys[i]['data'][j] == 'undefined') {
                            var sub_key = 'key_'+j;
                        }
                        else {
                            var sub_key = keys[i]['data'][j]['key'];
                        }
                        keyed_array[keys[i]['key']][sub_key] = pieces[i][j];

                    } else {
                        // Keying additional data
                        var key_val = pieces[i][j].split('=');
                        keyed_array[keys[i]['key']][key_val[0]] = key_val[1];
                    }
                }
            } else {
                // Keying 1st level data
                keyed_array[keys[i]['key']] = pieces[i];
            }
        }

        return keyed_array;
    },
    // Asynchronus HTTP GET
    httpGetAsync : function(theUrl, check, callback)
    {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            {
                try {
                    var result = xmlHttp.responseText
                    console.log(result);
                    var pieces = pax_transaction.parse(result);
                    if (typeof check == 'undefined') {
                        pieces = pax_transaction.keyResponse(pieces);
                        pax_transaction.recordPAXResults(pieces);
                        pax_transaction.handlePayment(pieces);
                    }
                    else {
                        pieces = pax_transaction.keyResponse(pieces, check);
                        pax_transaction.recordPAXResults(pieces);
                        if (typeof pieces.response_code != 'undefined' && pieces.response_code == '000000' && pieces.response_code == '100011')
                        {
                            pax_transaction.TransactionNumber = pieces.trace_information.transaction_number;
                            callback();
                        } else {
                            pax_transaction.closePaymentWindow();
                        }
                    }
                    console.dir(pieces);

                }
                catch(err) {
                    if (typeof newrelic != 'undefined') {
                        newrelic.noticeError(err);
                        var err2 = new Error('APRIVA - bad response - '+JSON.stringify(xmlHttp));
                        newrelic.noticeError(err2);
                        var err3 = new Error('APRIVA - bad response - message - '+err.message);
                        newrelic.noticeError(err3);
                    }
                    //pax_transaction.checkPAXTransaction();
                }

            }
            else if (xmlHttp.readyState == 4)
            {
                var message = 'There was an error contacting the credit card terminal. Please check settings or contact support.';
                //pax_transaction.checkPAXTransaction();
                App.vent.trigger('notification', {'msg': message, 'type':'error', delay:30000});
                // Close modal
                //$('.modal').modal('hide');
                console.dir(xmlHttp);
                console.log('had an issue connecting');
            }
        }

        xmlHttp.open("GET", theUrl, true); // true for asynchronous
        xmlHttp.send(null);
    },
    getCardType : function (cardType)
    {
        return pax_transaction.card_types[cardType];
    },
    packageTransactionData : function (type, data)
    {
        var InvoiceNo = pax_transaction.InvoiceNo;
        var TranType = pax_transaction.TranType;

        if (type == 'API') {
            return {
                'transaction_type' : TranType,
                'invoice' : InvoiceNo,
                'response' : data,
                'request' : pax_transaction.requestCommandArrString,
                'terminal_id' : pax_transaction.TerminalID
            };
        }

        if (type == '') {
            if (data.response_code != '000000' && data.response_code != '100011') {
                return {
                    'response_code': data.response_code,
                    'status': data.response_message
                }
            }
            else {
                var CardType = pax_transaction.getCardType(data.account_information.card_type);
                var payment_id = typeof data.additional_information != 'undefined' ? data.additional_information.HRef : '';
                return {
                    'tran_type': TranType,
                    'card_type': CardType,
                    'invoice_id': InvoiceNo,
                    'payment_id': payment_id,
                    'status': data.response_message,
                    'display_message': data.host_information.host_response_message,
                    'acq_ref_data': data.host_information.host_reference_number,
                    'process_data': data.host_information.trace_number,
                    //'message' : data.response_message,
                    'amount': (data.amount_information.approve_amount / 100).toFixed(2),
                    'auth_amount': (data.amount_information.approve_amount / 100).toFixed(2),
                    //'params' : {},//params don't forget Fee
                    //'description' : CardType + ' ' + data.account_information.account,
                    //'verified' : (data.response_code == '000000') ? 1 : 0,
                    'auth_code': data.host_information.auth_code,
                    'cardholder_name': data.account_information.card_holder,
                    //'customer_note' : <?php echo !empty($customer_note) ? json_encode($customer_note) : 'null'; ?>,
                    'masked_account': data.account_information.account
                };
            }
        }
        else if (type == 'sale') {
            var CardType = pax_transaction.getCardType(data.account_information.card_type);
            return {
                'tran_type': TranType,
                'type' : 'credit_card',
                'record_id' : InvoiceNo,
                'invoice_id' : InvoiceNo,
                'action' : TranType,
                'merchant' : 'apriva',
                'message' : data.response_message,
                'approved' : (data.response_code == '000000' || (data.response_code == '100011' && data.amount_information.approve_amount > 0)) ? true : false,
                'payment_amount' : (data.amount_information.approve_amount/100).toFixed(2),
                'auth_amount': (data.amount_information.approve_amount / 100).toFixed(2),
                'amount': (data.amount_information.approve_amount / 100).toFixed(2),
                'auth_code': data.host_information.auth_code,
                'card_type': CardType,
                'cardholder_name': data.account_information.card_holder,
                'card_number': data.account_information.account,
                'description' : CardType + ' ' + data.account_information.account,
                'params' : {
                    'payment_id' : InvoiceNo,
                    'record_id' : InvoiceNo,
                    'cardholder_name' : data.account_information.card_holder,
                    'card_type' : CardType,
                    'card_number' : data.account_information.account,
                    'description' : CardType + ' ' + data.account_information.account,
                    'type' : TranType,
                    'amount' : (data.amount_information.approve_amount/100).toFixed(2),
                    'auth_code' : data.host_information.auth_code
                }
            };
        }
    },
    checkPAXTransaction : function (void_transaction)
    {
        void_transaction = typeof void_transaction == 'undefined' ? false : void_transaction;
        if (pax_transaction.CheckCount == 0)
        {
            pax_transaction.CheckCount++;
            this.sendTransactionCheck();
        }
        // $.ajax({
        //     type: "POST",
        //     url: "home/check_pax_transaction/"+pax_transaction.InvoiceNo,
        //     data: '',
        //     success: function (response) {
        //         if (!response.success) {
        //             console.log('failed checking pax transaction');
        //         }
        //         else {
        //             console.log('success checking pax transaction');
        //             // Record results in DB
        //             if (void_transaction) {
        //                 var voidData = {
        //
        //                 };
        //                 pax_transaction.voidTransaction(voidData);
        //             }
        //         }
        //     },
        //     dataType: 'json'
        // });

    },
    recordPAXResults : function (data)
    {
        var final_data = {
            'api_data': pax_transaction.packageTransactionData('API', data),
            'transaction_data': pax_transaction.packageTransactionData('', data)
        };

        $.ajax({
            type: "POST",
            url: "home/record_pax_transaction/",
            data: final_data,
            success: function (response) {
                if (!response.success) {
                    console.log('failed saving pax transaction');
                    if (typeof newrelic != 'undefined') {
                        var err = new Error('APRIVA - Did not record - '+JSON.stringify(data));
                        newrelic.noticeError(err);
                        //pax_transaction.checkPAXTransaction();
                    }
                }
                else {
                    console.log('success saving pax transaction');
                }
            },
            dataType: 'json'
        });
    },
    voidTransaction : function(voidData){

    },
    handlePayment : function (data) {
        if (data.response_code != '000000' && data.response_code != '100011') {
            // Declined or cancelled
            var message = pax_transaction.responseCodes[data.response_code];
            App.vent.trigger('notification', {'msg': message, 'type':'error', 'delay':30000});
            // Close modal
            $('.modal').modal('hide');
            // Void or refund the payment on the PAX

        } else {
            // Approved
            pax_transaction.savePayment(data);
        }
    },
    savePayment : function (data)
    {
        var payment = pax_transaction.packageTransactionData('sale', data);
        App.vent.trigger('credit-card', payment);
    },
    // Base64 Encoding
    arrayBufferToBase64 : function( buffer ) {
        var binary = '';
        var bytes = new Uint8Array( buffer );
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode( bytes[ i ] );
        }
        return btoa( binary );
    },
    // LRC Calculation Check
    calculateLRC : function(str) {
        var bytes = [];
        var lrc = 0;
        for (var i = 0; i < str.length; i++) {
            bytes.push(str.charCodeAt(i));
        }
        for (var i = 0; i < str.length; i++) {
            lrc ^= bytes[i];
        }
        return String.fromCharCode(lrc);
    }

}