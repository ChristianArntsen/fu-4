;(function ( $, window, document, undefined ) {

	// Create the defaults once
	var pluginName = "loadMask",

	defaults = {
		message: "Loading..."
	};

	// The actual plugin constructor
	function Plugin ( element, options ) {

		this.element = $(element);
		// jQuery has an extend method which merges the contents of two or
		// more objects, storing the result in the first object. The first object
		// is generally empty as we don't want to alter the default options for
		// future instances of the plugin
		this.settings = $.extend( {}, defaults, options );
		this._defaults = defaults;
		this._name = pluginName;
		
		this.init();
	}
	
	// Avoid Plugin.prototype conflicts
	$.extend(Plugin.prototype, {
		
		init: function () {
	
			var borderRadius = this.element.parent().css('-webkit-border-radius');
			var loadingHtml = '<h1>' + this.settings.message + '</h1>';
			
			loadingHtml += '<div class="progress progress-striped active">' +
				'<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>' +
				'</div></div>';
			
			if(this.element.find('div.loading-mask').length == 0){
				this.element.append('<div class="loading-mask">'+ loadingHtml +'</div>');	
			}
			this.show();
		},
		
		show: function () {
			if(this.element.find('div.loading-mask').length == 0){
				this.init();
			}else{
				this.element.find('div.loading-mask').addClass('visible');
			}
		},
		
		hide: function() {
			this.element.find('div.loading-mask').removeClass('visible');
		}
	});

	// A really lightweight plugin wrapper around the constructor,
	// preventing against multiple instantiations
	var publicMethod = $.fn[ pluginName ] = $[ pluginName ] = function ( options ) {
		
		this.each(function() {
			if (!$.data( this, "plugin_" + pluginName )){
				$.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
			}else{
				$(this).data("plugin_" + pluginName).show();
			}
		});

		// chain jQuery functions
		return this;
	};
	
	publicMethod.hide = function(){
		$('div.loading-mask').removeClass('visible');
	}
	
})( jQuery, window, document );
