ALTER TABLE  `foreup_item_kit_items` ADD  `unit_price` DECIMAL( 15, 2 ) NOT NULL DEFAULT  '0.00' AFTER  `quantity`;
ALTER TABLE  `foreup_sales_items` ADD  `parent_line` SMALLINT UNSIGNED NULL DEFAULT NULL;
ALTER TABLE  `foreup_sales_item_kits` ADD  `new` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0';