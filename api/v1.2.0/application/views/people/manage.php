<?php $this->load->view("partial/header"); ?>
<script type='text/javascript' src="js/customer.js"></script>
<script type="text/javascript">
function reload_customer(html)
{	
	$.colorbox2.close();
	$('#cc_dropdown').replaceWith(""+html+"");	
}
$(document).ready(function() 
{ 
	var base_export_url = $('#excel_export_button').attr('href');
	$('#excel_export_button').click(function(e){
		//e.preventDefault();
		var group_id = $('#customer_group').val();
		$(this).attr('href', base_export_url+'/'+group_id);
	});
	$('#customer_group').msDropDown(/*{blur:function(){changeTeeSheet()},click:alert('stuff')}*/)
	$('#customer_pass').msDropDown(/*{blur:function(){changeTeeSheet()},click:alert('stuff')}*/)
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({'maxHeight':700, 'width':<?php echo $form_width?>});
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
    enable_email('<?php echo site_url("$controller_name/mailto")?>');
    <?php if ($controller_name == 'employees') { ?>
    enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
    <?php } else { ?>
    $('#delete').click(function(e){
    	e.preventDefault();
    	var selected_values = get_selected_values();
    	console.dir(selected_values);
    	//return;
    	if($("#sortable_table tbody :checkbox:checked").length >0)
		{
			$.post('index.php/customers/delete',
	    		{
	    			'ids[]':selected_values,
	    			'confirm_message':'<?php echo lang($controller_name."_confirm_delete")?>'
	    		},
	    		function(data){
			    	$.colorbox({
			    		'html':data, 
			    		'width':500,
			    		onComplete:function(){
							var checkboxes = $('input[name=customer_ids[]]');
							console.log('checkboxes');
							console.dir(checkboxes);
							checkboxes.click(function(){
								var id = $(this).attr('id').replace('delete_customer_', '');
								var checked = $(this).attr('checked');
								if (checked)
									$('#person_'+id).attr('checked','checked');
								else
									$('#person_'+id).removeAttr('checked');
							});
							var submitting = false;
						    $('#delete_customers_form').validate({
								submitHandler:function(form)
								{
									if (submitting) return;
									submitting = true;
									$(form).mask("<?php echo lang('common_wait'); ?>");
									$(form).ajaxSubmit({
									success:function(response)
									{
										if(response.success)
										{
											selected_rows = get_selected_rows();
											set_feedback(response.message,'success_message',false);	
									
											$(selected_rows).each(function(index, dom)
											{
												$(this).find("td").animate({backgroundColor:"#FF0000"},1200,"linear")
												.end().animate({opacity:0},1200,"linear",function()
												{
													$(this).remove();
													//Re-init sortable table as we removed a row
													update_sortable_table();
													
												});
											});	
										}
										else
										{
											set_feedback(response.message,'error_message',true);	
										}
										$.colorbox.close();
									    submitting = false;
									},
									dataType:'json'
								});
						
								},
								errorLabelContainer: "#error_message_box",
						 		wrapper: "li",
								rules:
								{
						   		},
								messages:
								{
								}
							});
			    		}
			    	});
	    		}
	    	)
		}
		else
		{
			alert('<?php echo lang($controller_name."_none_selected")?>');
		}
    });
    <?php } ?>
	enable_bulk_edit('<?php echo lang($controller_name."_none_selected")?>');
    enable_cleanup('<?php echo lang("customers_confirm_cleanup")?>');
	$('#customer_group, #customer_pass').change(function(){
		$('#offset').val(0);
		$('#search_form').submit();
	});
	$("#course_name").autocomplete({
 		source: 'index.php/courses/course_search',
		delay: 10,
 		autoFocus: false,
 		minLength: 0,
 		select: function( event, ui ) 
 		{
			event.preventDefault();
			$('#offset').val(0);
			$("#course_name").val(ui.item.label);
 			$('#course_id').val(ui.item.value);
 			$('#search_form').submit();
 		//	do_search(true);
 		},
 		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});
	
	$('#load_member_billing').click(function(e){
		e.preventDefault();
		customer.billing.load_color_box(-1,'');
	})
}); 

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{ 
			sortList: [[1,0]], 
			headers: 
			{ 
				0: { sorter: false}, 
                                <?php if ($controller_name == 'employees' && $this->permissions->is_super_admin()) {?> 
				6: { sorter: false} 
                                <?php }
                                else if ($controller_name == 'employees') { ?>
				5: { sorter: false} 
                                <?php }
                                else if ($controller_name == 'customers') { ?>
				7: { sorter: false} 
                                <?php } ?>
                                    
                                    
			} 

		}); 
	}
}

function post_person_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);	
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}
function post_bulk_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		set_feedback(response.message,'success_message',false);
		setTimeout(function(){window.location.reload();}, 2500);
	}
}
</script>
<style>
	#title_bar #course_name {
		border:1px inset #ccc;
	}
</style>
<!--table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('common_list_of').' '.lang('module_'.$controller_name); ?>
		</td>
		<td id="groups">
			<?php
			if ($controller_name == 'customers') 	
			{
				$groups['no_group'] = '-- No Group --';
				echo form_dropdown('customer_group', $groups, 'all');
				$passes['no_pass'] = '-- No Pass --';
				echo form_dropdown('customer_pass', $passes, 'all');
			}
			else if ($controller_name == 'employees' && $this->permissions->is_super_admin())
			{
				echo form_hidden('course_id', '');
				echo form_input(array(
					'name'=>'course_name', 
					'id'=>'course_name',
					'value'=>''
				));
			}
			?>
		</td>
		<td id="title_search">
			<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
				<input type="text" name ='search' id='search'/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table-->
<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php 
				if ($this->permissions->is_employee() && $controller_name == 'employees')
				{
				}
				else 
				{
					echo anchor("$controller_name/view/-1/width~1100",
					lang($controller_name.'_new'),
					array('class'=>'colbox none new', 'title'=>lang($controller_name.'_new')));
				}
				if ($controller_name == 'customers')
				{
					echo anchor("$controller_name/bulk_edit/",
					lang("customers_bulk_edit"),
					array('id'=>'bulk_edit',
						'class' => 'bulk_edit_inactive',
						'title'=>lang('customers_edit_multiple_customers'))); 
				
					echo anchor("customers/manage_groups/width~720",
					lang('customers_manage_groups'),
					array('class'=>'colbox none new', 'title'=>lang('customers_manage_groups')));
					
					echo anchor("customers/manage_passes/width~720",
					lang('customers_manage_passes'),
					array('class'=>'colbox none new', 'title'=>lang('customers_manage_passes')));
					
					echo anchor("",
					lang('customers_member_billing'),
					array('class'=>'','id'=>'load_member_billing'));
				}
				?>
				<a class="email email_inactive" href="<?php echo current_url(). '#'; ?>" id="email"><?php echo lang("common_email");?></a>
				<?php if ($controller_name =='customers') {?>
				<?php echo anchor("$controller_name/excel_import/width~550",
				lang($controller_name.'_import'),
				array('class'=>'colbox none import','title'=>lang($controller_name.'_import')));
				} ?>
				<?php
				if ($controller_name == 'customers' || $controller_name == 'employees') {	
					echo anchor("$controller_name/excel_export",
					lang($controller_name.'_export'),
					array('class'=>'none import', 'id'=>'excel_export_button'));
				}
				?>
				<?php 
				if ($this->permissions->is_employee() && $controller_name == 'employees') {}
				else
					echo anchor("$controller_name/delete",lang("common_delete"),array('id'=>'delete', 'class'=>'delete_inactive')); 
				?>
			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
			<div id='table_top'>
				<?php
				if ($controller_name == 'customers') 	
				{
					$groups['no_group'] = '-- No Group --';
					echo form_dropdown('customer_group', $groups, 'all');
					$passes['no_pass'] = '-- No Pass --';
					echo form_dropdown('customer_pass', $passes, 'all');
				}
				else if ($controller_name == 'employees' && $this->permissions->is_super_admin())
				{
					echo form_hidden('course_id', '');
					echo form_input(array(
						'name'=>'course_name', 
						'id'=>'course_name',
						'value'=>''
					));
				}
				?>
				<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
					<input type="text" name ='search' id='search' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form>
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>