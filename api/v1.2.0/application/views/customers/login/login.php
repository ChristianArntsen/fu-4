<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/login.css?<?php echo APPLICATION_VERSION; ?>" />
<!--link href='http://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'-->
<title>foreUP <?php echo lang('login_login'); ?></title>
<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/common.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.loadmask.css?<?php echo APPLICATION_VERSION; ?>" />
<style>
	#feedback_bar{
		display: none;
	    font-weight: bold;
	    left: 50%;
	    padding: 10px;
	    position: fixed;
	    text-align: center;
	    top: 0;
	    width: 700px;
	    margin-left:-350px;
	    opacity:.9;
	    z-index:99999;
	}
	.warning_message,
	.error_message,
	.success_message {
		line-height: 30px;
		padding: 10px;
	}


	.warning_message
	{
	    background-color: #FBEA6E;
	    font-size: 0.9em;
	    font-weight: bold;
	    text-align: center;
	}

	.error_message
	{
	    background-color: #f68383;
	    border: 1px solid #da3232;
	}
	.message_close_box {
		background:transparent url(../css/ui-lightness/images/ui-icons_ffffff_256x240.png) no-repeat -97px -129px;
		width:15px;
		height:15px;
		display:block;
		position:absolute;
		right:2px;
		top:2px;
		cursor:pointer;
	}
	.success_message
	{
	    background-color: #E1FFDD;
	    border: 1px solid #2CA71C;
	}

</style>
<script type="text/javascript">
$(document).ready(function()
{
	$('#login_form').submit(function(e){
		e.preventDefault();
	})
	//$("#login_form input:first").focus();

	$('#login_form').validate({
		submitHandler:function(form)
		{
			$('#middle').mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
					console.log('were here');
					console.dir(response);
		            if (response.success) {
		                location.href = '<?php echo site_url('be/reservations/'.$this->session->userdata('course-id')); ?>';
		            }
		            else {
		               	set_feedback(response.message,'error_message',true);
                        $('#middle').unmask();
		            }
	            },
				dataType:'json'
			});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			password:
			{
				required:true,
				minlength: 8
			},
		    username: {
                required:true
            }
   		},
		messages:
		{
     		password:
			{
				required:"<?php echo lang('employees_password_required'); ?>",
				minlength: "<?php echo lang('employees_password_minlength'); ?>"
			},
			username: {
                required:"<?php echo lang('employees_email_required'); ?>"
            }
		}
	});
});
</script>
</head>
<body>
	<ul id='error_message_box'></ul>

	<!--div id="welcome_message" class="top_message">
		<?php echo lang($welcome_message); ?>
	</div-->
	<?php if (validation_errors()) {?>
		<div id="welcome_message" class="top_message_error">
			<?php echo validation_errors(); ?>
		</div>
	<?php } ?>
	<?php
	$this->load->helper('url');
	$this->load->library('user_agent');
	?>
<?php echo form_open('be/login',array('id'=>'login_form')) ?>
<div id="container">
	<div id="top">
		<?php //echo img(array('src' => '../images/login/login_logo.png'));?>
		<?php
			$logo = $this->Appconfig->get_logo_image('',$course_info->course_id);
			echo $logo ? img(array('src' => $logo,'width' =>144)) : '';
		?>
	</div>
	<div id='middle'>
		<div class='login_header'>
			Sign In
		</div>
		<div id="form_field_username">
			<span class='username_icon'></span>
			<?php echo form_input(array(
			'name'=>'username',
			'value'=>'',
			'placeholder'=>lang('login_username'),
			'size'=>'20')); ?>
		</div>
		<div id="form_field_password">
			<span class='password_icon'></span>
			<?php echo form_password(array(
			'name'=>'password',
			'value'=>'',
			'placeholder'=>lang('login_password'),
			'size'=>'20')); ?>
		</div>
		<div id="form_field_submit">
			<div id="submit_button">
				<?php echo form_submit('login_button',lang('login_continue')); ?>
			</div>
		</div>
	</div>
	<div id="bottom">
		<div id="right">
			<?php echo anchor('customer_login/reset_password', lang('login_forgot_password')); ?>
		</div>
	</div>
</div>
<div id="feedback_bar"></div>
<?php echo form_close(); ?>
</body>
</html>