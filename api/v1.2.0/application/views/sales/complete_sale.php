<div>

</div>
<div class='complete_sale'>
	<?php echo lang('sales_change_due').': '.$amount_change; ?><!--Complete sale now?-->
</div>
<!--table id="receipt_items">
	<tr>
	<th style="width:33%;text-align:left;"><?php echo lang('items_item'); ?></th>
	<th style="width:20%;text-align:left;"><?php echo lang('common_price'); ?></th>
	<th style="width:15%;text-align:center;"><?php echo lang('sales_quantity'); ?></th>
	<th style="width:16%;text-align:center;"><?php echo lang('sales_discount'); ?></th>
	<th style="width:16%;text-align:right;"><?php echo lang('sales_total'); ?></th>
	</tr>
	<?php
	foreach(array_reverse($cart, true) as $line=>$item)
	{
	?>
		<tr>
		<td><span class='long_name'><?php echo $item['name']; ?></span><span class='short_name'><?php echo character_limiter($item['name'],10); ?></span></td>
		<td><?php echo to_currency($item['price']); ?></td>
		<td style='text-align:center;'><?php echo $item['quantity']; ?></td>
		<td style='text-align:center;'><?php echo $item['discount']; ?></td>
		<td style='text-align:right;'><?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100); ?></td>
		</tr>

	    <tr>
	    <td colspan="2" align="center"><?php echo $item['description']; ?></td>
		<td colspan="2" ><?php echo isset($item['serialnumber']) ? $item['serialnumber'] : ''; ?></td>
		<td colspan="2"><?php echo '&nbsp;'; ?></td>
	    </tr>

	<?php
	}
	?>
	<tr>
	<td colspan="4" style='text-align:right;border-top:2px solid #000000;'><?php echo lang('sales_sub_total'); ?></td>
	<td colspan="2" style='text-align:right;border-top:2px solid #000000;'><?php echo to_currency($subtotal); ?></td>
	</tr>

	<?php foreach($taxes as $name=>$value) { ?>
		<tr>
			<td colspan="4" style='text-align:right;'><?php echo $name; ?>:</td>
			<td colspan="2" style='text-align:right;'><?php echo to_currency($value); ?></td>
		</tr>
	<?php }; ?>

	<tr>
	<td colspan="4" style='text-align:right;'><?php echo lang('sales_total'); ?></td>
	<td colspan="2" style='text-align:right'><?php echo to_currency($total); ?></td>
	</tr>

    <tr><td colspan="6">&nbsp;</td></tr>

	<?php
		foreach($payments as $payment_id=>$payment)
	{ ?>
		<tr>
		<td colspan="2" style="text-align:right;"><?php echo lang('sales_payment'); ?></td>
		<td colspan="2" style="text-align:right;"><?php $splitpayment=explode(':',$payment['payment_type']); echo $splitpayment[0]; ?> </td>
		<td colspan="2" style="text-align:right"><?php echo to_currency( $payment['payment_amount'] ); ?>  </td>
	    </tr>
	<?php
	}
	?>	
    <tr><td colspan="6">&nbsp;</td></tr>

	<?php foreach($payments as $payment) {?>
		<?php if (strpos($payment['payment_type'], lang('sales_giftcard'))!== FALSE) {?>
	<tr>
		<td colspan="2" style="text-align:right;"><?php echo lang('sales_giftcard_balance'); ?></td>
		<td colspan="2" style="text-align:right;"><?php echo $payment['payment_type'];?> </td>
		<td colspan="2" style="text-align:right"><?php echo to_currency($this->Giftcard->get_giftcard_value(end(explode(':', $payment['payment_type'])))); ?></td>
	</tr>
		<?php }?>
	<?php }?>
	-->

<div class='small_button' id='finish_sale_button_2' style='margin:5px auto;'><span>Ok<?php //echo lang('sales_complete_sale')?></span></div>
<!--div class='small_button' id='finish_sale_no_receipt_button_2' style='float:left;margin-top:5px;'><span><?php echo lang('sales_complete_sale_without_receipt')?></span></div>
<div class='small_button' id='do_not_finish_sale_button' style='float:left;margin-top:5px;'><span><?php echo lang('sales_do_not_complete_sale')?></span></div-->
<script>
$(document).ready(function(){
	var fsb2 = $('#finish_sale_button_2');
	fsb2.click(function()
    {
    	//if (confirm('<?php echo lang("sales_confirm_finish_sale"); ?>'))
    	//{
    		
		if (submitting_sale)
			console.log('already submitting');
		else
		{
			//submitting_sale = true;
			$('#finish_sale_form').submit();
    	}
    	//}
    });
    $(document).keypress(function(e) {
	  if(e.which == 13 && fsb2.length > 0) {
	    // enter pressed
	   	fsb2.click();
	  }
	  else
	  	console.log('pressing enter');
	});
    $('#finish_sale_button_2').dblclick(function(){});
    $("#finish_sale_no_receipt_button_2").click(function()
    {
    	//if (confirm('<?php echo lang("sales_confirm_finish_sale"); ?>'))
    	//{
        $('#no_receipt').val(1);
    	if (submitting_sale)
			console.log('already submitting');
		else
		{
			submitting_sale = true;
			$('#finish_sale_form').submit();
		}
    	//}
    });
    $('#finish_sale_no_receipt_button_2').dblclick(function(){});
    $("#do_not_finish_sale_button").click(function()
    {
    	$.colorbox.close();
    });
});
</script>						
