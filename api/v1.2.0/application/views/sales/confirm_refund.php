<?php

?>
<style>
	#issue_refund_wrapper {
		width:100%;
	}
</style>
<div id="issue_refund_wrapper">
	<fieldset>
	<?php echo form_open("sales/issue_refund/".$cc_payment['invoice'],array('id'=>'refund_form')); ?>
	<ul id="error_message_box"></ul>

	<div class="field_row clearfix">
	<?php echo form_label('Name on Card:', 'payment_name'); ?>
		<div class='form_field'>
			<?php echo $cc_payment['cardholder_name'] ?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Account #:', 'payment_account'); ?>
		<div class='form_field'>
			<?php echo $cc_payment['masked_account'] ?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Card Type:', 'payment_card_type'); ?>
		<div class='form_field'>
			<?php echo $cc_payment['card_type'] ?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Purchase Charge:', 'payment_purchase_charge'); ?>
		<div class='form_field'>
			<?php echo to_currency($cc_payment['amount']) ?>
		</div>
	</div>
	<?php if ($cc_payment['amount_refunded'] > 0) { ?>
		<div class="field_row clearfix">
			<?php echo lang('sales_return_already_issued').to_currency($cc_payment['amount_refunded']); ?>
		</div>
	<?php } else { ?>
	<div class="field_row clearfix">
	<?php echo form_label('Purchase Return:', 'payment_amount'); ?>
		<div class='form_field'>
			<?php echo form_input(array('name'=>'amount','value'=>$cc_payment['amount'], 'id'=>'amount'));?>
		</div>
	</div>
	<?php if ($cc_payment['gratuity_amount'] > 0) { ?>
	<div class="field_row clearfix">
	<?php echo form_label('Tip Charge:', 'payment_tip_charge'); ?>
		<div class='form_field'>
			<?php echo to_currency($cc_payment['gratuity_amount']) ?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Tip Return:', 'gratuity_amount'); ?>
		<div class='form_field'>
			<?php echo form_input(array('name'=>'gratuity_amount','value'=>$cc_payment['gratuity_amount'], 'id'=>'gratuity_amount'));?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Total Return:', 'payment_total_return'); ?>
		<div class='form_field' id='total_return'>
			<?php echo to_currency($cc_payment['gratuity_amount']+$cc_payment['amount']) ?>
		</div>
	</div>
	<?php } ?>
	<div class="field_row clearfix">
		By clicking submit, you are returning the stated amount to the credit card listed above.
	</div>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_submit'),
		'class'=>'submit_button float_left')
	);
	}
	?>
	</form>
	</fieldset>
	</div>
	<script>
	$(document).ready(function(){
		$('#amount, #gratuity_amount').keyup(function(){
			var amount = parseFloat($('#amount').val()).toFixed(2);
			var gratuity_amount = parseFloat($('#gratuity_amount').val()).toFixed(2);
			$('#total_return').html('$'+(parseFloat(amount) + parseFloat(gratuity_amount)).toFixed(2));
			
		})
		var submitting = false;
		$('#refund_form').validate({
			submitHandler:function(form)
			{
				if (submitting) return;
				if ($('#amount').val() > parseFloat('<?php echo $cc_payment['amount']?>'))
				{
					alert('You cannot refund more than $<?php echo $cc_payment['amount']?>');
					return;
				}
				if (parseFloat('<?php echo $cc_payment['gratuity_amount']?>') > 0 && $('#gratuity_amount').val() > parseFloat('<?php echo $cc_payment['gratuity_amount']?>'))
				{
					alert('You cannot refund more than $<?php echo $cc_payment['gratuity_amount']?> for the tip');
					return;
				}
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				//console.log( 'about to submit form');
				$(form).ajaxSubmit({
					success:function(response)
					{
						console.log(response);
						submitting = false;
						$.colorbox.close();

		                if (response.success)
			                window.location = '<?php echo site_url('sales'); ?>';
			            else
			            {
			            	if (response.message != undefined)
			            		set_feedback(response.message, 'error_message', true);
			            	else
								set_feedback('Credit card return error', 'error_message', true);
						}
					},
					dataType:'json'
				});
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li"
		});
	});
	</script>