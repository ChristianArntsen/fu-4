<?php
echo form_open('sales/add_payment',array('id'=>'giftcard_form'));
//print_r($price_classes);
?>
<ul id="error_message_box"></ul>
<fieldset id="giftcard_payment_info">
<legend><?php echo lang("giftcards_charge_giftcard"); ?></legend>
<div id='member_account_info' class="field_row clearfix">	
<?php echo form_label('Amount:<span class="required">*</span>', 'giftcard_amount',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'giftcard_amount',
		'id'=>'giftcard_amount',
		'size'=>'20',
		'value'=>"$amount",
		'style'=>'text-align:right;'
		)
	);?>
	</div>
</div>
<div id='member_account_info' class="field_row clearfix">	
<?php echo form_label('Giftcard #:<span class="required">*</span>', 'giftcard_number',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'giftcard_number',
		'id'=>'giftcard_number',
		'size'=>'20',
		'value'=>'',
		'maxlength'=>'16')
	);?>
	</div>
</div>
<div class='clear' style='text-align:center'>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</div>
</fieldset>

<?php 
echo form_close();
?>
<style>
	html[xmlns] div.hidden {
		display:none;
	}
</style>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
    $('#giftcard_form').validate({
		submitHandler:function(form)
		{
			giftcard_swipe = false;
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			submitting_payment = true;
			$("input:[name=payment_type]").val('Gift Card');
			$("input:[name=payment_gc_number]").val($('#giftcard_number').val());
			$("#amount_tendered").val($('#giftcard_amount').val());
			
			//$("#amount_tendered_label").html("<?php echo lang('sales_giftcard_number'); ?>");
			//$("#amount_tendered").val('');
			$("#amount_tendered").focus();
			mercury.add_payment();
			
			/*$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				$.colorbox.close();
				post_person_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});*/

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			giftcard_amount: "required",
			giftcard_number: "required"
   		},
		messages: 
		{
     		giftcard_amount: "<?php echo lang('common_first_name_required'); ?>",
     		giftcard_number: "<?php echo lang('common_last_name_required'); ?>"
		}
	});
});
</script>

<script>
	var giftcard_swipe = false;
	$(document).ready(function(){
		var gcn = $('#giftcard_number');
		gcn.keydown(function(event){
			// Allow: backspace, delete, tab, escape, and enter
	        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
	             // Allow: Ctrl+A
	            (event.keyCode == 65 && event.ctrlKey === true) || 
	             // Allow: home, end, left, right
	            (event.keyCode >= 35 && event.keyCode <= 39)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        else {
	            // Ensure that it is a number and stop the keypress
	            //if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	            	console.log('kc '+event.keyCode);
	            var kc = event.keyCode;
	            if (giftcard_swipe && !((kc >= 48 && kc <=57) || (kc >= 96 && kc <= 105) || kc == 13)) //Allow numbers only and enter
	            {
	            	console.log('numbers only');
            		event.preventDefault();
            	}
            	else if (kc == 186 /*semi-colon*/ || kc == 187 /*equal sign*/|| kc == 191 /*forward slash*/|| (event.shiftKey && kc == 53) /*percentage sign*/) 
	            {
	            	console.log('blocking special characters');
	            	giftcard_swipe = true;
	                event.preventDefault(); 
	            }   
	        }
		});
		//gcn.focus();
	})
</script>