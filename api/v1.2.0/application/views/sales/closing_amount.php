<?php $this->load->view('partial/header.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
	apply_closing_callbacks();
});
function apply_closing_callbacks()
{
	$('#change, #ones, #fives, #tens, #twenties, #fifties, #hundreds').keyup(function(){
		console.log('changing register counts');
		total_closing_amount();
	});
}
function total_closing_amount()
{
	var change = $('#change').val();
	var ones = $('#ones').val();
	var fives = $('#fives').val();
	var tens = $('#tens').val();
	var twenties = $('#twenties').val();
	var fifties = $('#fifties').val();
	var hundreds = $('#hundreds').val();
	
	var total = parseFloat(0);
	total += parseFloat(change != '' ? change : 0);
	total += parseFloat(ones != '' ? ones : 0);
	total += parseFloat(fives != '' ? fives : 0) * 5;
	total += parseFloat(tens != '' ? tens : 0) * 10;
	total += parseFloat(twenties != '' ? twenties : 0) * 20;
	total += parseFloat(fifties != '' ? fifties : 0) * 50;
	total += parseFloat(hundreds != '' ? hundreds : 0) * 100;
	
	$('#closing_amount').val(total.toFixed(2));
};

</script>
<style type="text/css">
	.error { float: none !important; }
	.closing_labels {width:50px; text-align:right; display:inline-block;}
</style>
<div id="register_container" class="sales">
<?php
echo form_open('sales/closeregister' . $continue, array('id'=>'closing_amount_form'));
?>
<fieldset id="item_basic_info">
<legend><?php echo lang("sales_closing_amount_desc"); ?></legend>
<?php if (!$this->config->item('blind_close')) { ?>
<div class='closing_approximation'><?php echo sprintf(lang('sales_closing_amount_approx'), $closeout); ?></div>
<?php } ?>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>&cent;</span>
	<?php echo form_input(array(
		'name'=>'change',
		'id'=>'change',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_change'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$1</span>
	<?php echo form_input(array(
		'name'=>'ones',
		'id'=>'ones',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_ones'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$5</span>
	<?php echo form_input(array(
		'name'=>'fives',
		'id'=>'fives',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_fives'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$10</span>
		<?php echo form_input(array(
		'name'=>'tens',
		'id'=>'tens',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_tens'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$20</span>
		<?php echo form_input(array(
		'name'=>'twenties',
		'id'=>'twenties',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_twenties'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$50</span>
	<?php echo form_input(array(
		'name'=>'fifties',
		'id'=>'fifties',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_fifties'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>$100</span>
	<?php echo form_input(array(
		'name'=>'hundreds',
		'id'=>'hundreds',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_hundreds'),
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php //echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
		<span class='closing_labels'>Total</span>
	<?php echo form_input(array(
		'name'=>'closing_amount',
		'id'=>'closing_amount',
		'style'=>'text-align:right;',
		'placeholder'=>lang('sales_closing_amount'),
		'value'=>'')
	);?>
	</div>
</div>


<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
</div>
<?php $this->load->view('partial/footer.php'); ?>
<script type='text/javascript'>

	
//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#closing_amount_form').validate({
		rules:
		{
			closing_amount: {
				required: true,
				number: true
			}
   		},
		messages:
		{
			closing_amount: {
				required: "<?php echo $lang('sales_amount_required'); ?>",
				number: "<?php echo $lang('sales_amount_number'); ?>"
			}
		}
	});
});
</script>