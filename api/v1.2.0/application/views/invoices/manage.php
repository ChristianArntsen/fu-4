<?php $this->load->view("partial/header"); ?>
<style>
	#table_top input#month {
		float:right;
		width:100px;
		background:url("../images/pieces/search2.png") no-repeat scroll left center transparent;
		margin-right:10px;
		border-radius: 0px 5px 5px 0px;
		border-right: 1px solid rgb(85, 85, 85);
	}
	#paid_status {
		float:right;
		margin-right:10px;
		margin-top:10px;
	}
</style>
<script type='text/javascript' src="js/customer.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
	$('#month').datepicker();
	$('#paid_status').msDropDown();
	$('#invoices .dd').css('width', '100px');
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({'maxHeight':700, 'width':650});
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
    enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo lang($controller_name."_none_selected")?>');

    $('.pay_invoice').click(function(e){		
		e.preventDefault();	
		var invoice_id, redirect;
		
		invoice_id = "INV " + $(this).attr('invoice-id');	
		
		redirect = function(){
			window.location = '/index.php/sales';
		};
		
		sales.add_item(invoice_id, redirect);
	});
	
	$('.email_invoice').click(function(e){
		e.preventDefault();
		var invoice_id = $(this).attr('invoice-id');		
		if(confirm('Email a copy of this Invoice?')){
			sales.email_invoice(invoice_id);	
		}
		
	})
});
function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			headers:
			{
				0: { sorter: false},
				7: { sorter: false},
				8: { sorter: false},
				9: { sorter: false},
				10: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}


</script>

<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php echo 
					anchor("$controller_name/view/-1/width~850",
					lang($controller_name.'_new'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_new')));
					echo 
					anchor("credit_cards/view/-1",
					lang($controller_name.'_new_recurring_billing'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_new_recurring_billing')));
				
					echo 
					anchor("$controller_name/view_batch_pdfs/",
					lang($controller_name.'_batch_pdfs'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_batch_pdfs')));
				
					/*echo 
					anchor("credit_cards/view/-1",
					lang($controller_name.'_templates'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_templates')));
				*/
					echo 
					anchor("invoices/view_settings/",
					lang($controller_name.'_settings'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_settings')));
				
					echo 
					anchor("$controller_name/delete",
					lang("common_delete"),
					array('id'=>'delete', 
						'class'=>'delete_inactive')); 
				?>

			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
			<div id='table_top'>
				<div id="billing_view_buttons">
		       		<ul>
		       			<li id="invoice_view" class='selected'>Invoices</li>
		       			<li id="recurring_billing_view" class="last">Recurring Billings</li>
		       		</ul>
		       </div>
		       <?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
					<input type="text" name ='search' id='search' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form>
				<select name ='paid_status' id='paid_status'>
					<option value='all'>Status</option>
					<option value='paid'>Paid</option>
					<option value='unpaid'>Unpaid</option>
				</select>
				<input type="text" name ='month' id='month' placeholder="Month"/>
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $invoice_manage_table; ?>
				</div>
				<div id="billing_table_holder" style='display:none'>
				<?php echo $billing_manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>