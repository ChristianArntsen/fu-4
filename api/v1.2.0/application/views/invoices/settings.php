<fieldset id="settings_info">
    <div class="field_row clearfix">
	<?php echo form_label(lang('invoices_generate_on'), 'generate_on',array('class'=>'wide ')); ?>
		<div class='form_field'>
		<?php 
		echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'generate_on',
			'id'=>'generate_on',
			'value'=>'')
		);
		?>
		<span style='font-size:10px;'>(<?=$generate_on_message?>)</span>
		</div>
	</div>
</fieldset>