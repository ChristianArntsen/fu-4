<?php $this->load->view("partial/course_header"); ?>
<div id="course_name"><?php echo $course_info->name?></div>
<div id="thank_you_box"></div>
<div id="home_module_list">
    <h2>You have successfully unsubscribed from further communications from <?=$course_bane?></h2>
    <br/><br/>
    <span>Online booking provided by</span><br/>
    <img src="images/header/header_logo.png" alt="ForeUP"/>
</div>
<?php $this->load->view("partial/course_footer"); ?>