<?php $this->load->view("partial/course_header"); ?>
<script>
	$(document).ready(function(){
		$('.colbox').colorbox({'width':600,'height':500});
	})
    var booking = {
        teesheet_id:'<?php echo $teesheet_id ?>',
        time_range:'morning',
        holes:'18',
        available_spots:1,
        update_slider_hours:function(){
            var values = $( "#slider" ).slider( "option", "values" );
            var am_pm0 = 'a';
            var am_pm1 = 'a';
            if (values[0] > 11)
                am_pm0 = 'p';
            if (values[1] > 11)
                am_pm1 = 'p';
            if (values[0] > 12)
                values[0] = values[0]-12;
            if (values[1] > 12)
                values[1] = values[1]-12;

            $($('.ui-slider-handle')[0]).html(values[0]+am_pm0);
            $($('.ui-slider-handle')[1]).html(values[1]+am_pm1);

        },
        get_available_teetimes:function(){
            $('.tab_content').html("<div class='loading_golf_ball'><img src='<?php echo base_url(); ?>images/loading_golfball2.gif'/><div>Finding available teetimes...</div></div>");
            $.ajax({
               type: "POST",
               url: "<?php echo site_url('be/get_available_teetimes'); ?>",
               data: "time_range="+this.time_range+"&teesheet_id="+this.teesheet_id+"&holes="+this.holes+"&available_spots="+this.available_spots,
               success: function(response){
                   booking.update_available_teetimes(response);
               }
            });
        },
        update_available_teetimes:function(ajax_response) {
            var data = eval('('+ajax_response+')');
            for (var i in data) {
                $('#tab'+(parseInt(i)+1)+'_label').html(data[i].day_label);
                var teetime_html = '';
                for (var j in data[i].teetimes) {
                    teetime_html += data[i].teetimes[j];
                }
                //console.log('#tab'+(parseInt(i)+1));
                $('#tab'+(parseInt(i)+1)).html(teetime_html);
            }
            this.add_reserve_button_styles();
            tb_init('a.thickbox');
            $('.colbox').colorbox({'width':600,'height':500});
        },
        open_reservation_popup:function() {

        },
        activate_filter_buttons:function() {
            $("#"+this.time_range+'_range').click();
            $("input:[name=time_button]").click(function(e){
                //console.dir($(e.target));
                var time_range = $(e.target)[0].id;
                time_range = time_range.slice(0, time_range.indexOf('_'));
                //console.log(time_range);
                booking.time_range = time_range;
                booking.get_available_teetimes();
            });
            $("#holes_"+this.holes).click();
            $("input:[name=holes_button]").click(function(e){
                //console.dir($(e.target));
                var holes = $(e.target)[0].id;
                holes = holes.slice(holes.indexOf('_')+1);
                booking.holes = holes;
                booking.get_available_teetimes();
            });
            $("#player_"+this.available_spots).click();
            $("input:[name=player_button]").click(function(e){
                //console.dir($(e.target));
                var players = $(e.target)[0].id;
                players = players.slice(players.indexOf('_')+1);
                //console.log(players);
                booking.available_spots = players;
                booking.get_available_teetimes();
            });
        },
        add_reserve_button_styles:function() {
            $('.reserve_button a').button({
                icons:{
                    primary:'ui-icon-check'
                }
            });
        }
    };
    $(document).ready(function() {
        booking.activate_filter_buttons();
        //When page loads...
        $('#player_1').button({
            icons:{
                primary:'ui-icon-person'
            }
        });
        $('#player_2').button({
            icons:{
                primary:'ui-icon-person'
            }
        });
        $('#player_3').button({
            icons:{
                primary:'ui-icon-person'
            }
        });
        $('#player_4').button({
            icons:{
                primary:'ui-icon-person'
            }
        });
        $('#holes_9').button({
            icons:{
                primary:'ui-icon-flag'
            }
        });
        $('#holes_18').button({
            icons:{
                primary:'ui-icon-flag'
            }
        });
        booking.add_reserve_button_styles();
        //$('#reserve_button').button();
        $('#time_button_set').buttonset();
        $('#player_button_set').buttonset();
	$('#hole_button_set').buttonset();
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

        //$('#slider').slider({min:5, max:19, range:true, values:[5,10], change:function(){booking.update_slider_hours()}});
        //booking.update_slider_hours();
});
function post_person_form_submit(response)
{
        if(!response.success)
        {
                set_feedback(response.message,'error_message',true);
        }
        else
        {
                //This is an update, just update one row
                if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
                {
                        //update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
                        set_feedback(response.message,'success_message',false);

                }
                else //refresh entire table
                {
                        //do_search(true,function()
                        //{
                                //highlight new row
                        //        highlight_row(response.person_id);
                                set_feedback(response.message,'success_message',false);
                        //});
                }
        }
}
</script>
<style>
	.ui-widget {
        font-size:.8em;
    }
    .ui-buttonset .ui-button {
        margin-right:-0.5em;
    }
    #slider {
        margin: 10px 10px 10px 20px;
        width:250px;
    }
    .ui-slider-handle {
        font-size:.8em;
        padding:1px 0 0 2px;
    }
    .tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	clear: both;
	float: left;
        width: 959px;
	background: #fff;
        height:400px;
}
.tab_content {
	padding:10px 20px;
	font-size: 1.2em;
        height:330px;
        overflow: auto;
        position:relative;
}
    ul.tabs {
	margin: 0;
	padding: 0;
	float: left;
	list-style: none;
	height: 46px; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 959px;
}
ul.tabs li {
	float: left;
	margin: 0;
	padding: 0;
	height: 45px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 45px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	overflow: hidden;
	position: relative;
	background: #e0e0e0;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: .9em;
	padding: 0 10px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
        width:114px;
        text-align: center;
        line-height: 31px;
}
ul.tabs li a div {
    height:12px;
}
ul.tabs li a span {
    font-size:.7em
}
ul.tabs li a:hover {
	background: #ccc;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #fff;
	border-bottom: 1px solid #fff; /*--Makes the active tab look like it's connected with its content--*/
}
#time_button_set, #hole_button_set {
    float:left;
    margin:10px 0 0 16px;
}
#player_button_set {
    float:right;
    margin:10px 21px 0 0;
}
.search_controls {
    padding:0px 5px 10px;
    border-bottom: 1px solid #ddd;
}
.teetime {
    border:1px solid #dedede;
    padding:8px 15px;
    margin-bottom:5px;
}
.teetime .left {
    float:left;
    width:10%;
}
.teetime .center {
    float:left;
    width:75%;
    text-align: center;
}
.teetime .right {
    float:right;
    width:11%;
    margin-top:5px;
}
.teetime .left .players span {
    float: left;
}
.teetime .left .players span.ui-button-text {
    font-size: .6em;
    margin: 1px 0 0 2px;
}
.loading_golf_ball {
    font-size:14px;
    padding-top:94px;
    text-align:center;
}
.be_header {
    color:#336699;
    margin:15px auto 5px;
    text-align: center;
}
.be_form {
    width:65%;
    margin:auto;

}
#employee_basic_info.login_info {
    margin-right:5px;
}
.register_form {
    width:85%;
}
.be_form td input {
    margin-top:3px;
    margin-left:5px;
}
.be_fine_print {
    color:#999999;
    font-size:11px;
    text-align:center;
}
.be_form td hr {
    color:#336699;
    margin:11px 0px 7px;
}
.be_reserve_buttonset {
    float:left;
    margin-right:22px;
}
.be_buttonset_label {
    line-height: 30px;
    color:#336699;
    margin-right:10px;
}
#cboxLoadedContent .teetime .course_name {
    font-size:1.4em;
    margin-top:6px;
}
#cboxLoadedContent .teetime .info {
    padding-left:10px;
    margin-top:7px;
}
#cboxLoadedContent .teetime .time, #cboxLoadedContent .teetime .holes, #cboxLoadedContent .teetime .date {
    color:#336699;
    font-size:1em;
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label, #content_area .teetime .spots {
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label {
    font-size:.6em;
}
#content_area .teetime .price_label {
    margin-right:2px;
    padding-right:0px;
    line-height:19px;
}
#cboxLoadedContent .teetime .info .ui-icon {
    float:left;
    margin-right:5px;
}
#content_area .teetime .ui-icon {
    float:left;
    margin-right:5px;
}
#cboxLoadedContent .teetime .reservation_info {
    float:left;
    width:70%;
}
#cboxLoadedContent .teetime .price_box {
    float:right;
    text-align: right;
    width:27%;
    font-size:2.5em;
    line-height: 50px;
}
#content_area .teetime .price_details {
    padding-left: 135px;
}
#course_name {
    font-size:2em;
    text-align:center;
}
#menubar_background {
	background-image:url('<?php echo base_url(); ?>images/header/header_piece.png');
}
#menubar_full {
	background-image:none;
}
#menubar_navigation .menu_item_home {
	padding:5px 10px 0 0 ;
}
</style>
<?php //print_r($teetimes);?>
<div id="course_name"><?php echo $course_info->name?></div>
<div id="thank_you_box"></div>
<div id="home_module_list">
    <ul class="tabs">
        <li><a href="#tab1" id="tab1_label">
                <?php echo $teetimes[0]['day_label'];?>
            </a></li>
        <li><a href="#tab2" id="tab2_label">
                <?php echo $teetimes[1]['day_label'];?>
            </a></li>
        <li><a href="#tab3" id="tab3_label">
                <?php echo $teetimes[2]['day_label'];?>
            </a></li>
        <li><a href="#tab4" id="tab4_label">
                <?php echo $teetimes[3]['day_label'];?>
            </a></li>
        <li><a href="#tab5" id="tab5_label">
                <?php echo $teetimes[4]['day_label'];?>
            </a></li>
        <li><a href="#tab6" id="tab6_label">
                <?php echo $teetimes[5]['day_label'];?>
            </a></li>
        <li><a href="#tab7" id="tab7_label">
                <?php echo $teetimes[6]['day_label'];?>
            </a></li>
    </ul>

    <div class="tab_container">
        <div class="search_controls">
            <div id="time_button_set">
                <input type="radio" id="morning_range" name="time_button" checked/><label for="morning_range">Morning</label>
                <input type="radio" id="midday_range" name="time_button"/><label for="midday_range">Midday</label>
                <input type="radio" id="evening_range" name="time_button"/><label for="evening_range">Evening</label>
            </div>
            <!--div id="slider"></div-->
            <div id="hole_button_set">
                <input type="radio" id="holes_9" name="holes_button"/><label for="holes_9">9</label>
                <input type="radio" id="holes_18" name="holes_button" checked/><label for="holes_18">18</label>
            </div>
            <div id="player_button_set">
                <input type="radio" id="player_1" name="player_button" checked/><label for="player_1">1</label>
                <input type="radio" id="player_2" name="player_button"/><label for="player_2">2</label>
                <input type="radio" id="player_3" name="player_button"/><label for="player_3">3</label>
                <input type="radio" id="player_4" name="player_button" checked/><label for="player_4">4</label>
            </div>
            <div class="clear"></div>
        </div>
        <div id="tab1" class="tab_content">
            <!--Content-->
            <?php
                foreach ($teetimes[0]['teetimes'] as $teetime)
                    echo $teetime;
            ?>
        </div>
        <div id="tab2" class="tab_content">
           <!--Content-->
            <?php
                foreach ($teetimes[1]['teetimes'] as $teetime)
                    echo $teetime;
            ?>
        </div>
        <div id="tab3" class="tab_content">
           <!--Content-->
            <?php
                foreach ($teetimes[2]['teetimes'] as $teetime)
                    echo $teetime;
            ?>
        </div>
        <div id="tab4" class="tab_content">
           <!--Content-->
            <?php
                foreach ($teetimes[3]['teetimes'] as $teetime)
                    echo $teetime;
            ?>
        </div>
        <div id="tab5" class="tab_content">
           <!--Content-->
            <?php
                foreach ($teetimes[4]['teetimes'] as $teetime)
                    echo $teetime;
            ?>
        </div>
        <div id="tab6" class="tab_content">
           <!--Content-->
            <?php
                foreach ($teetimes[5]['teetimes'] as $teetime)
                    echo $teetime;
            ?>
        </div>
        <div id="tab7" class="tab_content">
           <!--Content-->
            <?php
                foreach ($teetimes[6]['teetimes'] as $teetime)
                    echo $teetime;
            ?>
        </div>
    </div>
</div>
<?php $this->load->view("partial/course_footer"); ?>