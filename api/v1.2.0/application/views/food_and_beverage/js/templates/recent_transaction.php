<script type="text/html" id="template_recent_transaction">
<div class="info">
	<h3>Sale #<%-sale_id%></h3>
	<h5>Total: <%-accounting.formatMoney(total)%></h5>
	<% if(tips.length > 0){ %>
	<strong>Tips</strong>
	<ul>
	<% _.each(tips.models, function(tip){ %>
		<li><%-tip.get('type')%>: <%-accounting.formatMoney(tip.get('amount'))%></li>
	<% }) %>
	</ul>
	<% } %>
</div>
<a href="#" class="fnb_button add_tip" data-type="Cash Tip" style="float: right;">Cash Tip</a>
<% if(payments.length > 0){
_.each(payments, function(payment){
if(payment.invoice_id == 0){
	return true;
}
%>
<a href="#" class="fnb_button add_tip" data-type="<%-payment.type%> Tip" data-invoice-id="<%-payment.invoice_id%>" style="float: right;"><%-payment.type%> Tip</a>
<% }); } %>
</script>