var ReceiptView = Backbone.View.extend({
	tagName: "div",
	className: "receipt-container",
	template: _.template( $('#template_receipt').html() ),

	events: {
		"click a.pay": "payReceipt",
		"click a.print": "printReceipt",
		"click a.delete-receipt": "deleteReceipt",
		"click div.receipt": "moveItems"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model.get('items'), "add remove reset", this.render);
	},

	render: function() {
		this.$el.html( this.template(this.model.attributes) );
		this.$el.find('div.title').after( new ReceiptItemListView({collection:this.model.get('items')}).render().el );
		this.renderStatus();
		return this;
	},

	renderStatus: function(){
		if(this.model.get('items').length == 0){
			this.$el.find('a.pay, a.print').addClass('disabled');

		}else if(this.model.get('status') != 'complete'){
			this.$el.find('a.pay, a.print').removeClass('disabled');

		}else{
			this.$el.find('a.print').removeClass('disabled');
		}
	},

	payReceipt: function(event){
		if(this.model.get('items').length == 0 || this.model.get('status') == 'complete'){
			return false;
		}
		var receiptModel = this.model;
		this.model.calculateTotals();

		var payments = this.model.get('payments');
		var paymentWindow = new PaymentWindowView({collection: payments, model: receiptModel});

		$.colorbox2({
			title: 'Add Payment',
			html: paymentWindow.render().el,
			width: 625
		});

		return false;
	},

	printReceipt: function(event){
		this.model.calculateTotals();

		var receiptText = '';
		var EOL = '\r\n';
		var receipt = this.model;

		receiptText += 'Receipt #' + receipt.get('receipt_id') + EOL +
			'================================' + EOL +
			'ITEMS' + EOL;
		var cart = [];
		// Loop through items on receipt
		_.each(this.model.get('items').models, function(item){
			var sides = [];
			receiptText += item.get('name') + '........................' + accounting.formatMoney(item.get('split_subtotal')) + EOL;

			// Loop through sides, soups, and salads (if any of each)
			if(item.get('sides')){
				_.each(item.get('sides').models, function(side){
					receiptText += side.get('name') + '........................' + accounting.formatMoney(side.get('split_subtotal')) + EOL;
					if (side.get('split_subtotal') > 0)
						sides.push({name:side.get('name'), price:side.get('split_subtotal')});
				});
			}
			if(item.get('soups')){
				_.each(item.get('soups').models, function(soup){
					receiptText += soup.get('name') + '........................' + accounting.formatMoney(soup.get('split_subtotal')) + EOL;
					if (soup.get('split_subtotal') > 0)
						sides.push({name:soup.get('name'), price:soup.get('split_subtotal')});
				});
			}
			if(item.get('salads')){
				_.each(item.get('salads').models, function(salad){
					receiptText += salad.get('name') + '........................' + accounting.formatMoney(salad.get('split_subtotal')) + EOL;
					if (salad.get('split_subtotal') > 0)
						sides.push({name:salad.get('name'), price:salad.get('split_subtotal')});
				});
			}
			cart.push({name:item.get('name'), quantity:item.get('quantity'), discount:_.round(item.get('discount'), 2), price:item.get('split_subtotal'), sides:sides});
		});

		receiptText += '================================' + EOL +
		'Subtotal ........................' + accounting.formatMoney(receipt.get('subtotal')) + EOL +
		'Tax ........................' + accounting.formatMoney(receipt.get('tax')) + EOL +
		'TOTAL ........................' + accounting.formatMoney(receipt.get('total')) + EOL;

		//alert(receiptText);

		var data = {
			cart:cart,// Each object needs to include name, quantity, discount, and price
			//payments:{},
			subtotal:receipt.get('subtotal'),
			tax:receipt.get('tax'),
			total:receipt.get('total'),
			//change_due:''
		};

		console.debug(data); //JBDEBUG

		var receipt_data = '';
		receipt_data = webprnt.build_itemized_receipt(receipt_data, data);
		//if (App.receipt.print_tip_line)
		//{
		//	receipt_data = webprnt.add_tip_line(receipt_data);
		//	if (webprnt.credit_card_payments)
		//		receipt_data = webprnt.add_signature_line(receipt_data);
		//}
		if (App.receipt.return_policy != '')
		{
	    	receipt_data = webprnt.add_return_policy(receipt_data, App.receipt.return_policy);
	    }
	    //receipt_data = webprnt.add_barcode(receipt_data, 'Sale ID', data.sale_id);
	    receipt_data = webprnt.add_itemized_header(receipt_data, App.receipt.header, receipt.get('receipt_id'))
	    receipt_data = webprnt.add_receipt_header(receipt_data, App.receipt.header)
	    receipt_data = webprnt.add_paper_cut(receipt_data);
	    //if (App.receipt. != undefined && double_print == true)
	    //	receipt_data += receipt_data;
	    //receipt_data = webprnt.add_cash_drawer_open(receipt_data);
	    webprnt.add_space(receipt_data);
	    webprnt.print(receipt_data, window.location.protocol + "//" + App.receipt_ip+"/StarWebPrint/SendMessage");

		return false;
	},

	deleteReceipt: function(event){
		if(this.model.get('receipt_id') == 1){
			return false;
		}

		var splitItems = this.model.collection.getSplitItems();
		var items = this.model.get('items').models;
		var deleteReceipt = true;

		// Make sure items in receipt to be deleted exist on other receipts
		_.each(items, function(item){
			var line = item.get('line');
			if(splitItems[line] <= 1){
				deleteReceipt = false;
			}
		});

		if(deleteReceipt){
			this.model.destroy();
		}
		return false;
	},

	moveItems: function(event){
		// If this receipt has already been paid, do nothing
		if(this.model.get('status') == 'complete'){
			App.receipts.unSelectItems();
			return false;
		}

		var selectedItems = App.receipts.getSelectedItems();
		var items = [];
		var mode = $('#split_payments').find('div.btn-group').data('mode');

		if(selectedItems.length == 0){
			return false;
		}
		var targetReceiptItems = this.model.get('items');

		// Add selected items to target receipt.
		_.each(selectedItems, function(selectedItem){

			// Make sure items aren't being moved back onto same receipt
			if(selectedItem.collection != targetReceiptItems){

				// Copy the item including the nested collections
				var itemCopy = _.clone(selectedItem.attributes);
				itemCopy.sides = itemCopy.sides.toJSON();
				itemCopy.soups = itemCopy.soups.toJSON();
				itemCopy.salads = itemCopy.salads.toJSON();
				itemCopy.modifiers = itemCopy.modifiers.toJSON();

				targetReceiptItems.create(itemCopy, {complete: function(response){
					// If moving item, delete it from other receipt. Delete
					// call is made when move call has completed to prevent errors
					if(mode == 'move'){
						selectedItem.destroy();
					}
				}, isNew: true});

				// TODO: Hide the selected item so there is no delay
				// waiting for requests to complete
				if(mode == 'move'){

				}
			}
		});

		App.receipts.unSelectItems();
	}
});
