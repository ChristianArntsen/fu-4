var PaymentView = Backbone.View.extend({
	tagName: 'li',
	className: 'payment',
	template: _.template( $('#template_payment').html() ),

	events: {
		"click a.delete": "deletePayment",
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
	},

	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	},

	deletePayment: function(){
		this.model.destroy();
		return false;
	}
});