var PaymentWindowView = Backbone.View.extend({
	tagName: "div",
	id: "payment_window",
	template: _.template( $('#template_payment_window').html() ),

	initialize: function() {
		this.listenTo(this.collection, "add remove change", this.render);
		this.listenTo(this.collection, "invalid", this.displayError);
	},

	events: {
		"click #use_cash": "payCash",
		"click #use_check": "payCheck",
		"click a.show_payment_buttons": "showPaymentTypes",

		"click #use_gift_card": "showGiftCard",
		"click #use_punch_card": "showPunchCard",
		"click #use_credit_card": "showCreditCard",

		"click #pay_gift_card": "payGiftCard",
		"click a.close": "closeWindow"
	},

	render: function() {
		this.$el.html('');

		var data = {};
		data.amount_due = this.model.getTotalDue();

		this.$el.html(this.template(data));
		this.$el.find('#payment_amount_tendered').keypad({position: 'bottom'});
		this.renderPayments();
		return this;
	},

	displayError: function(model){
		set_feedback(model.validationError, 'error_message');
	},

	showPaymentTypes: function(){
		this.$el.find('#payment_buttons').show().siblings().hide();
		this.$el.find('div.left').removeClass('max');
		this.$el.find('div.payment-amount').show();
		return false;
	},

	renderPayments: function(){
		this.$el.find('ul.payments').html('');
		_.each(this.collection.models, this.addPayment, this);
		return this;
	},

	showChangeDue: function(change_due){
		var template = _.template( $('#template_change_issued').html() );
		var data = {};
		data.change_due = change_due;
		this.$el.html(template(data));
		return this;
	},

	addPayment: function(payment){
		this.$el.find('ul.payments').append( new PaymentView({model:payment}).render().el );
	},

	getPaymentData: function(){
		var amount = this.$el.find('#payment_amount_tendered').val();
		return {"amount":amount};
	},

	unmarkCartItems: function(){

	},

	showPunchCard: function(event){
		return false;
	},

	showGiftCard: function(event){
		this.$el.find('#payment_gift_card').show().siblings().hide();
		return false;
	},

	showCreditCard: function(event){
		// If shift key is pressed, perform basic Credit Card payment
		if(event.shiftKey){
			var data = this.getPaymentData();
			data.type = "Credit Card";
			var self = this;

			this.collection.create(data, {'success': function(){
				if(self.model.isPaid()){
					self.model.printReceipt();
					$.colorbox2.close();
					self.remove();
				}
				App.closeTable();
			}});
			return false;
		}

		var data = this.getPaymentData();
		data.receipt_id = this.model.get('receipt_id');

		var creditCardWindow = this.$el.find('#payment_credit_card');
		creditCardWindow.html('');
		creditCardWindow.show().siblings().hide();
		creditCardWindow.parent('div.left').addClass('max');
		this.$el.find('div.payment-amount').hide();

		// Open credit card processing window
		$('#cbox2LoadedContent').mask('<?php echo lang('common_wait'); ?>');
		$.post(SITE_URL + '/food_and_beverage/credit_card_window', data, function(response){
			creditCardWindow.html(response);
		},'html');

		return false;
	},

	payCash: function(event){
		var data = this.getPaymentData();
		data.type = "cash";
		var self = this;

		this.collection.create(data, {'success': function(){
			self.closePayments();
		}});
		return false;
	},

	payCheck: function(event){
		var data = this.getPaymentData();
		data.type = "check";
		var self = this;

		this.collection.create(data, {'success':function(){
			self.closePayments();
		}});
		return false;
	},

	payGiftCard: function(){
		var data = this.getPaymentData();
		data.type = "gift card";
		data.card_number = this.$el.find('#payment_giftcard_number').val();
		var self = this;

		this.collection.create(data, {'success':function(){
			self.closePayments();
		}});
		return false;
	},

	closePayments: function(){
		if(this.model.isPaid()){
			var totalDue = this.model.getTotalDue();

			// If change is due, show change due window
			if(totalDue < 0){
				this.model.printReceipt();
				this.showChangeDue(totalDue);
			}else{
				this.model.printReceipt();
				this.closeWindow();
			}
		}
	},

	closeWindow: function(event){
		$.colorbox2.close();
		this.remove();
		App.closeTable();
		return false;
	}
});
