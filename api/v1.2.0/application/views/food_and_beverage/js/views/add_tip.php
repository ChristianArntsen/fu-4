var AddTipView = Backbone.View.extend({
	tagName: "div",
	template: _.template( $('#template_add_tip').html() ),

	events: {
		"click a.save": "saveTip"
	},

	render: function() {
		this.$el.html(this.template(this.model));
		this.$el.find('#tip_amount').keypad({position: 'right'});
		return this;
	},

	saveTip: function(event){
		var data = {};
		data.type = $('#tip_type').val();
		data.amount = $('#tip_amount').val();
		data.invoice_id = $('#tip_invoice_id').val();

		$('#cbox2LoadedContent').mask('Applying Tip');
		this.collection.create(data, {'wait':true, 'merge':true, 'success':function(){
			$('#cbox2LoadedContent').unmask();
			$.colorbox2.close();
		}});

		event.preventDefault();
	}
});