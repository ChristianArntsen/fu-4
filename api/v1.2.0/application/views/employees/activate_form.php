<?php
echo form_open('employees/activate/'.$person_info->person_id,array('id'=>'employee_form'));
?>
<ul id="error_message_box"></ul>
<fieldset id="employee_activation_info">
<legend><?php echo lang("employees_activation_info"); ?></legend>
<?php if ($this->permissions->is_super_admin()){?>
<div class="field_row clearfix">	
<?php echo form_label(lang('common_golf_course').':<span class="required">*</span>', 'golf_course',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'golf_course',
		'id'=>'golf_course',
		'value'=>$person_info->course_id)
	);?>
	</div>
</div>
<script>
    $(document).ready(function(){
        $( "#golf_course" ).autocomplete({
 		source: 'index.php/courses/course_search',
		delay: 10,
 		autoFocus: false,
 		minLength: 0,
 		select: function( event, ui ) 
 		{
			$( "#golf_course" ).val(ui.item.label);
 			do_search(true);
 		}
	});
    })
</script>
<?php }?>
</fieldset>

<fieldset id="golf_course_permission_info">
<legend><?php echo lang("golf_course_permission_info"); ?></legend>
<p><?php echo lang("golf_course_permission_desc"); ?></p>

<ul id="permission_list">
<?php
if ($this->permissions->is_employee())
{
  ?>
    <span class="medium">Please contact your admin to change permissions.</span>
  <?php
}
else
{
    foreach($all_modules->result() as $module)
    {
        if ($module->module_id == 'courses')
            continue;
        else if ($module->module_id == 'teetimes' || $module->module_id == 'config') {
            $checked = true;
            $disabled = 'disabled';
        }
        else { 
            $checked = $this->Employee->has_permission($module->module_id,$person_info->person_id);
            $disabled = '';
        }
        
        ?>
        <li>	
        <?php 
        $checkbox_data = array('name'=>'permissions[]', 'value'=>$module->module_id, 'checked'=>$checked, $disabled=>$disabled);
        echo form_checkbox($checkbox_data); ?>
        <span class="medium"><?php echo lang('module_'.$module->module_id);?>:</span>
        <span class="small"><?php echo lang('module_'.$module->module_id.'_desc');?></span>
        </li>
        <?php
        
    }
}
?>
</ul>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_activate'),
	'class'=>'submit_button float_right')
);

?>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('#employee_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				$.colorbox.close();
				post_person_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			first_name: "required",
			last_name: "required",
			username:
			{
				required:true,
				minlength: 5
			},
			
			password:
			{
				<?php
				if($person_info->person_id == "")
				{
				?>
				required:true,
				<?php
				}
				?>
				minlength: 8
			},	
			repeat_password:
			{
 				equalTo: "#password"
			},
    		email: "email"
   		},
		messages: 
		{
     		first_name: "<?php echo lang('common_first_name_required'); ?>",
     		last_name: "<?php echo lang('common_last_name_required'); ?>",
     		username:
     		{
     			required: "<?php echo lang('employees_username_required'); ?>",
     			minlength: "<?php echo lang('employees_username_minlength'); ?>"
     		},
     		
			password:
			{
				<?php
				if($person_info->person_id == "")
				{
				?>
				required:"<?php echo lang('employees_password_required'); ?>",
				<?php
				}
				?>
				minlength: "<?php echo lang('employees_password_minlength'); ?>"
			},
			repeat_password:
			{
				equalTo: "<?php echo lang('employees_password_must_match'); ?>"
     		},
     		email: "<?php echo lang('common_email_invalid_format'); ?>"
		}
	});
});
</script>