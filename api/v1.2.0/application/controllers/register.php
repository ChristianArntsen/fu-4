<?php
class Register extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('encrypt');
	}
	
	function index()
	{
		if($this->Employee->is_logged_in())
		{
			redirect('home');
		}
		else
		{
        	$this->form_validation->set_rules('username', 'lang:login_undername', 'callback_register_check');
            $this->form_validation->set_rules('first_name', 'lang:common_first_name', 'required');
            $this->form_validation->set_rules('last_name', 'lang:common_last_name', 'required');
            $this->form_validation->set_rules('password', 'lang:common_password', 'required|match[password_confirm]');
            $this->form_validation->set_rules('password_confirm', 'lang:common_password_confirm', 'required');
            $this->form_validation->set_rules('email', 'lang:common_email', 'valid_email');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if($this->form_validation->run() == FALSE)
			{
				$this->load->view('register/register');
			}
			else
			{
				$data['welcome_message'] = 'register_thank_you_message';
				$this->load->view('login/login', $data);
			}
		}
	}
	
	function register_check($username)
	{
        $first_name = $this->input->post("first_name");
        $last_name = $this->input->post("last_name");
        $email = $this->input->post("email");
        $phone_number = $this->input->post("phone_number");
        $password_confirm = $this->input->post("password_confirm");
		$password = $this->input->post("password");	
            
        $person_data = array('first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'phone_number'=>$phone_number);
		$employee_data = array('username'=>$username,'password'=>md5($password));
        $permission_data = array();
        if(!$this->Employee->save($person_data, $employee_data, $permission_data,-1))
		{
        	$this->form_validation->set_message('register_check', lang('login_invalid_username_and_password'));
			return false;
		}
        return true;		
	}
	
	function reset_password()
	{
		$this->load->view('login/reset_password');
	}
	
	function do_reset_password_notify()
	{
		$employee = $this->Employee->get_employee_by_username_or_email($this->input->post('username_or_email'));
		if ($employee)
		{
			$data = array();
			$data['employee'] = $employee;
		    $data['reset_key'] = base64url_encode($this->encrypt->encode($employee->person_id.'|'.(time() + (2 * 24 * 60 * 60))));
			
			send_sendgrid(
				$employee->email,
				lang('login_reset_password'),
				$this->load->view("login/reset_password_email",$data, true),
				'support@foreup.com',
				'ForeUP Password Reset'
			);
		}
		
		$this->load->view('login/do_reset_password_notify');	
	}
	
	function reset_password_enter_password($key=false)
	{
		if ($key)
		{
			$data = array();
		    list($employee_id, $expire) = explode('|', $this->encrypt->decode(base64url_decode($key)));			
			if ($employee_id && $expire && $expire > time())
			{
				$employee = $this->Employee->get_info($employee_id);
				$data['username'] = $employee->username;
				$data['key'] = $key;
				$this->load->view('login/reset_password_enter_password', $data);			
			}
		}
	}
	
	function do_reset_password($key=false)
	{
		if ($key)
		{
	    	list($employee_id, $expire) = explode('|', $this->encrypt->decode(base64url_decode($key)));
			
			if ($employee_id && $expire && $expire > time())
			{
				$password = $this->input->post('password');
				$confirm_password = $this->input->post('confirm_password');
				
				if (($password == $confirm_password) && strlen($password) >=8)
				{
					if ($this->Employee->update_employee_password($employee_id, md5($password)))
					{
						$this->load->view('login/do_reset_password');	
					}
				}
				else
				{
					$data = array();
					$employee = $this->Employee->get_info($employee_id);
					$data['username'] = $employee->username;
					$data['key'] = $key;
					$data['error_message'] = lang('login_passwords_must_match_and_be_at_least_8_characters');
					$this->load->view('login/reset_password_enter_password', $data);
				}
			}
		}
	}
}
?>