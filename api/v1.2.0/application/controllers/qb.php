<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'libraries/QuickBooks.php');

class Qb extends CI_Controller {

	private static $dsn;
	private static $qbxmlHead = '<?xml version="1.0" encoding="utf-8"?><?qbxml version="6.0"?><QBXML><QBXMLMsgsRq onError="continueOnError">';
	private static $qbxmlFoot = '</QBXMLMsgsRq></QBXML>';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Quickbooks');

		define('QUICKBOOKS_LOADER_AUTOLOADER', false);

		// MySQL connection information
		self::$dsn = $this->db->dbdriver.'://'.$this->db->username.':'.$this->db->password.'@'.$this->db->hostname.'/'.$this->db->database;
	}

	function index(){
		return false;
	}

	function init(){
		$courseId = $this->session->userdata('course_id');
		if(empty($courseId)){
			return false;
		}

		$this->Quickbooks->init_quickbooks($courseId);

		$qbUser = $this->quickbooks->get_user($courseId);

		if($qbUser['is_initialized'] == 0){
			$this->Quickbooks->queue_items($courseId, 50);
			$this->Quickbooks->queue_item_kits($courseId, 50);
			$this->Quickbooks->queue_customers($courseId, 50);
			$this->Quickbooks->queue_sales($courseId, null, null, 25);
			$this->Quickbooks->queue_tips($courseId, 25);
			$this->Quickbooks->queue_returns($courseId, null, null, 25);
		}

		$this->Quickbooks->set_user_initialized($courseId);
	}

	/* Scans each course that is connected to QuickBooks
	 * and queues up any changes to be sent to QuickBooks
	 */
	function quickbooks_sync($key = ''){

		if($key != 'foreupcron'){
			return false;
		}

		$users = $this->Quickbooks->get_users();

		foreach($users as $user){
			$courseId = $user['qb_username'];
			if(empty($courseId)){
				continue;
			}

			$this->Quickbooks->queue_items($courseId, 50);
			$this->Quickbooks->queue_item_kits($courseId, 50);
			$this->Quickbooks->queue_customers($courseId, 50);
			$this->Quickbooks->queue_sales($courseId, null, null, 25);
			$this->Quickbooks->queue_tips($courseId, 25);
			$this->Quickbooks->queue_returns($courseId, null, null, 25);
		}
	}

	function generate_user(){

		$courseId = $this->session->userdata('course_id');
		if(empty($courseId)){
			return false;
		}

		// Generate password, use course ID as username
		$data['quickbooks']['qb_password'] = substr(md5($courseId), 0, 6);
		$data['quickbooks']['qb_username'] = $courseId;

		$data['quickbooks']['redemption_account'] = 0;
		$data['quickbooks']['sales_account'] = 0;
		$data['quickbooks']['tips_account'] = 0;
		$data['quickbooks']['qb_company_file'] = '';
		$data['quickbooks']['accounts'] = array(0 => '- No Accounts Available -');

		// Create QuickBooks user
		QuickBooks_Utilities::createUser(self::$dsn, $data['quickbooks']['qb_username'], $data['quickbooks']['qb_password']);

		// Insert account list request into queue
		$queue = new QuickBooks_Queue(self::$dsn);

		// Request all Quickbooks Accounts
		$queue->enqueue(QUICKBOOKS_QUERY_ACCOUNT, sha1('AllAccounts'), 5, $courseId, $courseId,
			'<AccountQueryRq></AccountQueryRq>',
		true);

		$this->load->view('quickbooks/account_info.php', $data);
	}

	function testinit(){
		$courseId = 6270;

		$this->Quickbooks->queue_items($courseId, 50);
		$this->Quickbooks->queue_item_kits($courseId, 50);
		$this->Quickbooks->queue_customers($courseId, 50);
		$this->Quickbooks->queue_sales($courseId, null, null, 25);
		$this->Quickbooks->queue_tips($courseId, 25);
		$this->Quickbooks->queue_returns($courseId, null, null, 25);

		echo (memory_get_peak_usage(true) / 1000000) . 'Mbs';
	}

	function edit_account(){

		if(!$this->session->userdata('course_id')){
			return false;
		}

		$params = $this->input->post();
		$courseId = $this->session->userdata('course_id');
		$this->load->model('quickbooks');
		$success = true;

		if(!empty($params['qb_username'])){
			$success = $this->quickbooks->update_company_file($params['qb_username'], $params['qb_company_file']);
		}

		if(!empty($params['qb_sales_account'])){
			$success = $this->quickbooks->set_sales_account($courseId, $params['qb_sales_account']);
		}

		if(!empty($params['qb_redemption_account'])){
			$success = $this->quickbooks->set_redemption_account($courseId, $params['qb_redemption_account']);
		}

		if(!empty($params['qb_tips_account'])){
			$success = $this->quickbooks->set_tips_account($courseId, $params['qb_tips_account']);
		}

		if(!empty($params['qb_sales_account']) && !empty($params['qb_redemption_account']) && !empty($params['qb_tips_account'])){
			$this->init();
		}

		if($success){
			$response = array('message'=>'QuickBooks settings saved', 'success'=>true);
		}else{
			$response = array('message'=>'Error saving QuickBooks settings', 'success'=>false);
		}

		echo json_encode($response);
	}

	function refresh_accounts(){

		if(!$this->session->userdata('course_id')){
			return false;
		}

		$courseId = $this->session->userdata('course_id');

		// Insert account list request into queue
		$queue = new QuickBooks_Queue(self::$dsn);

		// Request all Quickbooks Accounts
		$queue->enqueue(QUICKBOOKS_QUERY_ACCOUNT, sha1('AllAccounts'), 5, $courseId, $courseId,
			'<AccountQueryRq></AccountQueryRq>',
		true);

		echo json_encode( array('success'=>true, 'message'=>'QuickBooks accounts will be refreshed on next sync') );
	}

	function create_qwc(){

		if(!$this->session->userdata('course_id')){
			return false;
		}

		// Application name and description
		$name =  'ForeUp';
		$desc = 'Cloud Based Golf Course Software';

		// Both URLs *must* be httpS:// (path to your QuickBooks SOAP server)
		if(stripos(base_url(), '.com') !== false){
			$appurl = site_url('qb/push_data');
			$supporturl = site_url('support');
		}else{
			$appurl = site_url('qb/push_data');
			$supporturl = '';
		}

		// This is the username you stored in the 'quickbooks_user' table by using QuickBooks_Utilities::createUser()
		$username = $this->session->userdata('course_id');

		// Just make these up, but make sure it keeps the same format
		$fileid = '57F3B9B6-86F1-4FCC-B1FF-966DE1813D22';
		$ownerid = '57F3B9B6-86F1-4FCC-B1FF-166DE1813D22';

		// You can leave this as-is unless you're using QuickBooks POS
		$qbtype = QUICKBOOKS_TYPE_QBFS;

		// No, we want to write data to QuickBooks
		$readonly = false;

		// Run every 900 seconds (15 minutes)
		$run_every_n_seconds = 900;

		// Generate QWC file XML
		$QWC = new QuickBooks_QWC($name, $desc, $appurl, $supporturl, $username, $fileid, $ownerid, $qbtype, $readonly, $run_every_n_seconds);
		$xml = $QWC->generate();

		// Output QWQ file for download
		header('Content-type: text/xml');
		header('Content-Disposition: attachment; filename = "ForeUpConnection.qwc"');
		print($xml);
		exit;
	}

	function disable(){

		$courseId = $this->session->userdata('course_id');
		if(empty($courseId)){
			return false;
		}

		$this->Quickbooks->delete_accounts($courseId);
		$this->Quickbooks->clear_queue($courseId);
		$this->Quickbooks->clear_synced_records($courseId);
		$this->Quickbooks->delete_user($courseId);

		echo json_encode(array('success'=>true, 'message'=>'QuickBooks has been disconnected'));
	}

	function push_data(){

		ini_set('memory_limit', '512M');

		if (function_exists('date_default_timezone_set')){
			date_default_timezone_set('America/New_York');
		}

		// Map QuickBooks SOAP functions to controller functions
		$map = array(
			QUICKBOOKS_ADD_CUSTOMER => array(array('Qb', '_create_customer_request'), array('Qb', '_create_customer_response')),
			QUICKBOOKS_ADD_SALESRECEIPT => array(array('Qb', '_create_salesreceipt_request'), array('Qb', '_create_salesreceipt_response')),
			QUICKBOOKS_ADD_NONINVENTORYITEM => array(array('Qb', '_create_noninventoryitem_request'), array('Qb', '_create_noninventoryitem_response')),
			QUICKBOOKS_ADD_OTHERCHARGEITEM => array(array('Qb', '_create_otherchargeitem_request'), array('Qb', '_create_otherchargeitem_response')),
			QUICKBOOKS_ADD_PAYMENTITEM => array(array('Qb', '_create_paymentitem_request'), array('Qb', '_create_paymentitem_response')),
			QUICKBOOKS_ADD_CREDITMEMO => array(array('Qb', '_create_creditmemo_request'), array('Qb', '_create_creditmemo_response')),
			QUICKBOOKS_QUERY_CUSTOMER => array(array('Qb', '_get_customer_request'), array('Qb', '_get_customer_request')),
			QUICKBOOKS_QUERY_ACCOUNT => array(array('Qb', '_get_account_request'), array('Qb', '_get_account_response')),
			QUICKBOOKS_QUERY_TRANSACTION => array(array('Qb', '_get_transaction_request'), array('Qb', '_get_transaction_response'))
		);

		// Function for handling all errors
		$errmap = array(
			'*' => array('Qb', '_qb_error')
		);
		$handlers = array();

		// Create new SOAP listener
		$server = new QuickBooks_Server(
			self::$dsn,
			$map,
			$errmap,
			$hooks = array(),
			QUICKBOOKS_LOG_DEVELOP,
			QUICKBOOKS_SOAPSERVER_BUILTIN,
			QUICKBOOKS_WSDL,
			$soap_options = array(),
			$handlers,
			$driver_options = array(),
			$callback_options = array()
		);

		// Handle the QuickBooks SOAP interaction
		$response = $server->handle(true, true);
	}

	/*
	 * Handle any Quickbooks Errors
	 */
	public function _qb_error($requestID, $user, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg){
		// If error is because of duplicates, ignore it
		if($errnum == 3100 || $errnum == 3180){
			return true;
		}
		return false;
	}

	/*
	 * Create/update customer in QuickBooks
	 */
	public function _create_customer_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale, $config = array(), $qbxml = null){
		return self::$qbxmlHead . $qbxml . self::$qbxmlFoot;
	}

	public function _create_customer_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){

		if(empty($extra['record_id'])){
			return true;
		}

		$CI =& get_instance();
		$CI->load->model('quickbooks');

		// Parse response array XML
		$xmlArray = simplexml_load_string($xml);

		// Loop through response array and log each item request
		foreach($xmlArray->QBXMLMsgsRs->CustomerAddRs as $qbItem){
			$qbId = (string) $qbItem->CustomerRet->ListID;
			$qbFullName = (string) $qbItem->CustomerRet->FullName;

			foreach($extra['record_id'] as $recordId){
				if(stripos($qbFullName, $recordId) !== false){
					$CI->quickbooks->insert_sync_log($extra['course_id'], $recordId, $extra['record_type'], $qbId);
				}
			}
		}

		return true;
	}

	/*
	 * Create/update sales receipt in QuickBooks
	 */
	public function _create_salesreceipt_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale, $config = array(), $qbxml = null){
		return self::$qbxmlHead . $qbxml . self::$qbxmlFoot;
	}

	public function _create_salesreceipt_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){

		if(empty($extra['record_id'])){
			return true;
		}

		$CI =& get_instance();
		$CI->load->model('quickbooks');

		// Parse response array XML
		$xmlArray = simplexml_load_string($xml);

		// Loop through response array and log each item request
		foreach($xmlArray->QBXMLMsgsRs->SalesReceiptAddRs as $qbItem){
			$qbId = (string) $qbItem->SalesReceiptRet->TxnID;
			$qbRefNumber = (string) $qbItem->SalesReceiptRet->RefNumber;

			foreach($extra['record_id'] as $recordId){
				if(stripos($qbRefNumber, $recordId) !== false){
					$CI->quickbooks->insert_sync_log($extra['course_id'], $recordId, $extra['record_type'], $qbId);
				}
			}
		}

		return true;
	}

	/*
	 * Create/update credit memos (returns) in QuickBooks
	 */
	public function _create_creditmemo_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale, $config = array(), $qbxml = null){
		return self::$qbxmlHead . $qbxml . self::$qbxmlFoot;
	}

	public function _create_creditmemo_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){

		if(empty($extra['record_id'])){
			return true;
		}

		$CI =& get_instance();
		$CI->load->model('quickbooks');

		// Parse response array XML
		$xmlArray = simplexml_load_string($xml);

		// Loop through response array and log each item request
		foreach($xmlArray->QBXMLMsgsRs->CreditMemoAddRs as $qbItem){
			$qbId = (string) $qbItem->CreditMemoRet->TxnID;
			$qbRefNumber = (string) $qbItem->CreditMemoRet->RefNumber;

			foreach($extra['record_id'] as $recordId){
				if(stripos($qbRefNumber, $recordId) !== false){
					$CI->quickbooks->insert_sync_log($extra['course_id'], $recordId, $extra['record_type'], $qbId);
				}
			}
		}

		return true;
	}

	/*
	 * Get customer data from QuickBooks
	 */
	public function _get_customer_request(){
		return true;
	}

	public function _get_customer_response(){
		return true;
	}

	/*
	 * Get transaction data from QuickBooks
	 */
	public function _get_transaction_request(){
		return true;
	}

	public function _get_transaction_response(){
		return true;
	}

	/*
	 * Get account(s) from QuickBooks
	 */
	public function _get_account_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale, $config = array(), $qbxml = null){
		return self::$qbxmlHead . $qbxml . self::$qbxmlFoot;
	}

	public function _get_account_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){

		// Because this function is called staticly from QuickBooks
		// we must get the CodeIgniter app object manually
		$CI =& get_instance();
		$CI->load->model('quickbooks');
		$courseId = (int) $extra;

		// Parse QuickBooks XML that was passed to function
		$qbAccounts = simplexml_load_string($xml);
		$accounts = array();

		// Check if a sales account has already been selected
		$salesAccount = $CI->quickbooks->get_sales_account($courseId);

		$count = 0;
		// Loop through simplexml object, structure accounts into array
		foreach($qbAccounts->QBXMLMsgsRs->AccountQueryRs->AccountRet as $account){
			$qbAccountId = (string) $account->ListID;
			$metricId = 0;
			$accounts[$count]['course_id'] = $courseId;
			$accounts[$count]['quickbooks_id'] = $qbAccountId;
			$accounts[$count]['name'] = (string) $account->Name;
			$accounts[$count]['balance'] = (string) $account->TotalBalance;
			$accounts[$count]['type'] = (string) $account->AccountType;

			// If sales account has not been selected, auto-select based on name
			if(empty($salesAccount) && $accounts[$count]['name'] == 'Sales'){
				$CI->quickbooks->set_sales_account($courseId, $qbAccountId);
			}

			$count++;
		}
		$CI->quickbooks->save_accounts($accounts);

		return true;
	}

	/*
	 * Add inventory items to QuickBooks
	 */
	public function _create_noninventoryitem_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale, $config = array(), $qbxml = null){
		return self::$qbxmlHead.$qbxml.self::$qbxmlFoot;
	}

	public function _create_noninventoryitem_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){

		if(empty($extra['record_id'])){
			return true;
		}

		$CI =& get_instance();
		$CI->load->model('quickbooks');

		// Parse response array XML
		$xmlArray = simplexml_load_string($xml);

		// Loop through response array and log each item request
		foreach($xmlArray->QBXMLMsgsRs->ItemNonInventoryAddRs as $qbItem){
			$qbId = (string) $qbItem->ItemNonInventoryRet->ListID;
			$qbItemName = (string) $qbItem->ItemNonInventoryRet->FullName;

			foreach($extra['record_id'] as $recordId){
				if(stripos($qbItemName, $recordId) !== false){
					$CI->quickbooks->insert_sync_log($extra['course_id'], $recordId, $extra['record_type'], $qbId);
				}
			}
		}

		return true;
	}

	/*
	 * Add other charges to QuickBooks
	 */
	public function _create_otherchargeitem_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale, $config = array(), $qbxml = null){
		return self::$qbxmlHead.$qbxml.self::$qbxmlFoot;
	}

	public function _create_otherchargeitem_response(){
		return true;
	}

	/*
	 * Add other charges to QuickBooks
	 */
	public function _create_paymentitem_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale, $config = array(), $qbxml = null){
		return self::$qbxmlHead.$qbxml.self::$qbxmlFoot;
	}

	public function _create_paymentitem_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){
		return true;
	}
}