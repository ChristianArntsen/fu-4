<?php
require_once(APPPATH.'libraries/php-excel.class.php');
require_once ("secure_area.php");
class Reports extends Secure_area
{
	function __construct()
	{
		parent::__construct('reports');
		$this->load->helper('report');
	}

	//Initial report listing screen
	function index()
	{
		$this->load->model('Teesheet');
		$this->load->model('Schedule');
		$this->load->model('Terminal');
		$data = $this->_get_common_report_data();
		$terminals = $this->Terminal->get_all()->result_array();
		$data['terminals']['all'] = 'All';
		$data['terminals']['0'] = 'Other';
		foreach ($terminals as $terminal) {
			$data['terminals'][$terminal['terminal_id']] = $terminal['label'];
		}
		$departments = $this->Item->get_department_suggestions('',true);
		$data['departments']['all'] = 'All';
		foreach ($departments as $department) {
			$data['departments'][$department['label']] = $department['label'];
		}
		$categories = $this->Item->get_category_suggestions();
		$data['categories']['all'] = 'All';
		foreach ($categories as $category) {
			$data['categories'][$category['label']] = $category['label'];
		}
		$subcategories = $this->Item->get_subcategory_suggestions();
		$data['subcategories']['all'] = 'All';
		foreach ($subcategories as $subcategory) {
			$data['subcategories'][$subcategory['label']] = $subcategory['label'];
		}
		$data['report_teesheets'] = $this->permissions->course_has_module('reservations') ? $this->Schedule->get_tee_sheet_menu($this->Schedule->get_default(), false, 0) : $this->Teesheet->get_tee_sheet_menu($this->Teesheet->get_default(), false, 0);


        $result = $this->Module->get_allowed_modules($this->session->userdata('person_id'));
        foreach ($result->result() as $module)
        {
            $allowed_modules[$module->module_id] = 1 ;
        }
        $data['my_allowed_modules']= $allowed_modules;
		$data['controller_name']=strtolower(get_class());
		//$link = site_url('reports?report_type=specific_'.($controller_name == 'customers' ? 'customer' : 'employee').'&start='.$start_of_time.'&end='.$today.'&person_id='.$person->person_id.'&sale_type=all');
		$data['report_type'] = $this->input->get('report_type');
		$data['start'] = $this->input->get('start');
		$data['end'] = $this->input->get('end');
		$data['customer_id'] = $this->input->get('customers_id');
		$data['customer_name'] = $this->input->get('customers_name');
		$data['employee_id'] = $this->input->get('employees_id');
		$data['employee_name'] = $this->input->get('employees_name');
		$data['supplier_id'] = $this->input->get('suppliers_id');
		$data['supplier_name'] = $this->input->get('suppliers_name');
		$data['sale_type'] = $this->input->get('sale_type');
		$this->load->view("reports/sandbox",$data);
	}

	//Initial report listing screen
	function popup()
	{
		$this->load->model('Teesheet');
		$data = $this->_get_common_report_data();
		$departments = $this->Item->get_department_suggestions();
		$data['departments']['all'] = 'All';
		foreach ($departments as $department) {
			$data['departments'][$department['label']] = $department['label'];
		}
		$categories = $this->Item->get_category_suggestions();
		$data['categories']['all'] = 'All';
		foreach ($categories as $category) {
			$data['categories'][$category['label']] = $category['label'];
		}
		$subcategories = $this->Item->get_subcategory_suggestions();
		$data['subcategories']['all'] = 'All';
		foreach ($subcategories as $subcategory) {
			$data['subcategories'][$subcategory['label']] = $subcategory['label'];
		}
		$data['report_teesheets'] = $this->permissions->course_has_module('reservations') ? $this->schedule->get_tee_sheet_menu($this->schedule->get_default(), false, 0) : $this->Teesheet->get_tee_sheet_menu($this->Teesheet->get_default(), false, 0);


        //$result = $this->Module->get_allowed_modules($this->session->userdata('person_id'));
	    // foreach ($result->result() as $module)
	    // {
	        // $allowed_modules[$module->module_id] = 1 ;
	    // }
        // $data['my_allowed_modules']= $allowed_modules;
		// $data['controller_name']=strtolower(get_class());
		//$link = site_url('reports?report_type=specific_'.($controller_name == 'customers' ? 'customer' : 'employee').'&start='.$start_of_time.'&end='.$today.'&person_id='.$person->person_id.'&sale_type=all');
		$data['report_type'] = $this->input->get('report_type');
		$data['start'] = $this->input->get('start');
		$data['end'] = $this->input->get('end');
		$data['customer_id'] = $this->input->get('customers_id');
		$data['customer_name'] = $this->input->get('customers_name');
		$data['employee_id'] = $this->input->get('employees_id');
		$data['employee_name'] = $this->input->get('employees_name');
		$data['supplier_id'] = $this->input->get('suppliers_id');
		$data['supplier_name'] = $this->input->get('suppliers_name');
		$data['sale_type'] = $this->input->get('sale_type');
		$this->load->view("reports/popup",$data);
	}

	function _get_common_report_data()
	{
		$data = array();
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days'] = get_days();
		$data['years'] = get_years();
		$data['selected_month']=date('m');
		$data['selected_day']=date('d');
		$data['selected_year']=date('Y');

		return $data;
	}

	//Input for reports that require only a date range and an export to excel. (see routes.php to see that all summary reports route here)
	function date_input_excel_export($type='')
	{
		$data = $this->_get_common_report_data();
		$data['type'] = $type;
		$departments = $this->Item->get_department_suggestions();
		$data['departments']['all'] = 'All';
		foreach ($departments as $department) {
			$data['departments'][$department['label']] = $department['label'];
		}
		$categories = $this->Item->get_category_suggestions();
		$data['categories']['all'] = 'All';
		foreach ($categories as $category) {
			$data['categories'][$category['label']] = $category['label'];
		}
		$subcategories = $this->Item->get_subcategory_suggestions();
		$data['subcategory']['all'] = 'All';
		foreach ($subcategories as $subcategory) {
			$data['subcategories'][$subcategory['label']] = $subcategory['label'];
		}
		$this->load->view("reports/date_input_excel_export",$data);
	}

	//Input for reports that require only a date and a teesheet
	function date_input_teesheet()
	{
		$this->load->model('Teesheet');
		$data = $this->_get_common_report_data();
		$data['report_teesheets'] = $this->permissions->course_has_module('reservations') ? $this->schedule->get_tee_sheet_menu($this->schedule->get_default(), false, 0) : $this->Teesheet->get_tee_sheet_menu($this->Teesheet->get_default(), false, 0);
		$this->load->view("reports/date_input_teesheet",$data);
	}

	//Input for reports that require only a date range and an export to excel or pdf. (see routes.php to see that all summary reports route here)
	function date_input_excel_pdf_export()
	{
		$data = $this->_get_common_report_data();
		$departments = $this->Item->get_department_suggestions();
		$data['departments']['all'] = 'All';
		foreach ($departments as $department) {
			$data['departments'][$department['label']] = $department['label'];
		}
		$this->load->view("reports/date_input_excel_pdf_export",$data);
	}

	/** added for register log */
	function date_input_excel_export_register_log()
	{
		$data = $this->_get_common_report_data();
		$this->load->view("reports/date_input_excel_register_log.php",$data);
	}

	/** also added for register log */

	function detailed_register_log($start_date, $end_date, $export_excel=0, $terminal = 'all')
	{
		$this->load->model('reports/Detailed_register_log');
		$model = $this->Detailed_register_log;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'terminal'=>$terminal, 'export_excel'=>$export_excel));
		$headers = $model->getDataColumns();
		$report_data = $model->getData();
		$summary_data = array();
		$details_data = array();

		$overallSummaryData = array(
			'total_cash_sales'=>0,
			'total_shortages'=>0,
			'total_overages'=>0,
			'total_difference'=>0
		);
		if ($this->config->item('use_terminals'))
		{
			$this->load->model('Terminal');
			$terminal_array = array();
			$terminals = $this->Terminal->get_all()->result_array();
			foreach($terminals as $terminal)
			{
				$terminal_array[$terminal['terminal_id']] = $terminal['label'];
			}
		}
		foreach($report_data['summary'] as $row)
		{
			$summary_line = array();
			$cash_sales_total = $row['cash_sales_amount'];//$this->Sale->get_cash_sales_total_for_shift($row['shift_start'], $row['shift_end'], $row['employee_id']);
			if (!$this->permissions->is_employee() && $export_excel == 0)
				$summary_line[] = array('data'=>"<a id=\"register_edit_{$row['register_log_id']}\" href=\"javascript:$.colorbox({'href':'index.php/reports/edit_register_log/{$row['register_log_id']}/width~500'});\">Edit</a>", 'align'=>'left');

			$summary_line[] = array('data'=>$row['first_name'] . ' ' . $row['last_name'], 'align'=>'left');
			if ($this->config->item('use_terminals'))
				$summary_line[] = array('data'=>$terminal_array[$row['terminal_id']]);
			$summary_line[] = array('data'=>date(get_date_format().' '.get_time_format(), strtotime($row['shift_start'])), 'align'=>'left');
			$summary_line[] = array('data'=>date(get_date_format().' '.get_time_format(), strtotime($row['shift_end'])), 'align'=>'left');
			$summary_line[] = array('data'=>to_currency($row['open_amount']), 'align'=>'right');
			$summary_line[] = array('data'=>to_currency($row['close_amount']), 'align'=>'right');
			$summary_line[] = array('data'=>to_currency($cash_sales_total), 'align'=>'right');
			$summary_line[] = array('data'=>to_currency($cash_sales_total - ($row['close_amount'] - $row['open_amount'])), 'align'=>'right');

			$summary_data[] = $summary_line;
			$overallSummaryData['total_cash_sales'] += $row['cash_sales_amount'];
			if ($row['difference'] > 0) {
				$overallSummaryData['total_overages'] += $row['difference'];
			} else {
				$overallSummaryData['total_shortages'] += $row['difference'];
			}

			$overallSummaryData['total_difference'] += $row['difference'];
		}

		$data = array(
			"title" =>lang('reports_register_log_title'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $headers,
			"data" => $summary_data,
			"details_data" => array(),
			"summary_data" => $overallSummaryData,
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		//$text='whatever we want';
		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
			$this->load->view("reports/tabular", $data);
	}
	function edit_register_log($register_log_id)
	{
		if ($this->permissions->is_employee())
			return false;
		$data = array();
		$data['rl_data'] = $this->Sale->get_register_log_info($register_log_id)->row_array();
		$data['employee_data'] = (array)$this->Employee->get_info($data['rl_data']['employee_id']);
		//print_r($data);
		$this->load->view('sales/register_log_form.php', $data);
	}

	function save_register_log($register_log_id)
	{
		if ($this->permissions->is_employee())
			return false;

		$employee_name = $this->input->post('employee_name');
		$employee_id = $this->input->post('employee_id');
		$shift_start = date('Y-m-d H:i:s', strtotime($this->input->post('shift_start')));
		$shift_end = date('Y-m-d H:i:s', strtotime($this->input->post('shift_end')));
		$open_amount = $this->input->post('open_amount');
		$close_amount = $this->input->post('close_amount');
		$terminal_id = $this->input->post('terminal_id');
		$cash_sales_total = $this->Sale->get_cash_sales_total_for_shift($shift_start, $shift_end, $employee_id, $terminal_id);
		$data = array(
			'employee_id'=>$employee_id,
			'shift_start'=>$shift_start,
			'shift_end'=>$shift_end,
			'open_amount'=>$open_amount,
			'close_amount'=>$close_amount,
			'cash_sales_amount'=>$cash_sales_total
		);

		$success = $this->Sale->save_register_log($data, $register_log_id);
		//$row_info = $this->Sale->get_register_log_info($register_log_id)->row_array();
		$summary_data = array(array(
			array('data'=>"<a id=\"register_edit_{$register_log_id}\" href=\"javascript:$.colorbox({'href':'index.php/reports/edit_register_log/{$register_log_id}/width~500'});\">Edit</a>", 'align'=>'left'),
			array('data'=>$employee_name, 'align'=>'left'),
			array('data'=>date(get_date_format().' '.get_time_format(), strtotime($shift_start)), 'align'=>'left'),
			array('data'=>date(get_date_format().' '.get_time_format(), strtotime($shift_end)), 'align'=>'left'),
			array('data'=>to_currency($open_amount), 'align'=>'right'),
			array('data'=>to_currency($close_amount), 'align'=>'right'),
			array('data'=>to_currency($cash_sales_total), 'align'=>'right'),
			array('data'=>to_currency($cash_sales_total - ($close_amount - $open_amount)), 'align'=>'right')
		));

		if ($success)
			echo json_encode(array('success'=>true, 'register_log_id'=>$register_log_id, 'row_data'=>$this->load->view('reports/tabular_row.php', array('data'=>$summary_data), true)));
		else
			echo json_encode(array('success'=>false));
	}

	//Summary sales report
	function summary_sales($start_date, $end_date, $sale_type, $export_excel=0, $department = 'all')
	{
		$department = urldecode($department);
		$this->load->model('reports/Summary_sales');
		$model = $this->Summary_sales;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department' => $department));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department' => $department));
		$tabular_data = array();
		$report_data = $model->getData();
		foreach($report_data as $row)
		{
			$tabular_data[] = array(
                array('data'=>date(get_date_format(), strtotime($row['sale_date'])), 'align'=>'left'),
                array('data'=>to_currency($row['subtotal']), 'align'=>'right'),
                array('data'=>to_currency($row['total']), 'align'=>'right'),
                array('data'=>to_currency($row['tax']), 'align'=> 'right'),
                array('data'=>to_currency($row['profit']), 'align'=>'right'));
		}

		$data = array(
			"title" => lang('reports_sales_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			$this->load->library('Html2pdf');
			$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			$html2pdf->setDefaultFont('Arial');
			$html = $this->load->view("reports/tabular", $data, true);
			$html2pdf->writeHTML($html);
			$html2pdf->Output('summary_sales.pdf');
		}
		else
			$this->load->view("reports/tabular",$data);
	}

	function summary_z_out($start_date, $end_date, $export_excel=0)
	{
		$department = 'all';
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date));
		//echo '<br/><br/>'.$this->db->last_query().'<br/><br/>';
		$this->load->model('reports/Summary_z_out');
		$model = $this->Summary_z_out;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'department'=>$department));

		$debits = '';//$model->getDebits();
		$credits = '';//$model->getCredits();
		$summary_data = '';//$model->getSummaryData();
		$second_summary = '';//$model->getSecondSummaryData();

		// GET REGISTER COUNTS
		$drawer_counts = $model->getDrawerCounts();
		//echo $this->db->last_query();
		$total_drawer_count = 0;
		foreach ($drawer_counts['summary'] as $drawer_count) {
			$total_drawer_count += ($drawer_count['close_amount'] - $drawer_count['open_amount']);
		}

		// GET PAYMENTS INFO
		$this->load->model('reports/Summary_payments');
		$this->Summary_payments->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'department'=>$department, 'sale_type'=>'all', 'terminal'=>'all'));
		$payments_info = $this->Summary_payments->getData();

		// GET CATEGORIES SUMMARY INFO
		$this->load->model('reports/Summary_categories');
		$this->Summary_categories->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'department'=>$department));
		$sales_info = $this->Summary_categories->getData();

		// GET TAX COUNTS
		$this->load->model('reports/Summary_taxes');
		$this->Summary_taxes->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'department'=>$department));
		$tax_info = $this->Summary_taxes->getData();
		//print_r($tax_info);
		//echo 'here';

		$data = array(
			"title" => lang('reports_z_out_summery_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			//"headers" => $model->getDataColumns(),
			"drawer_count" => $drawer_counts,
			"drawer_total"=>$total_drawer_count,
			"payments"=>$payments_info,
			"revenue"=>$sales_info,
			"debits"=>$debits,
			"taxes"=>$tax_info,
			"credits"=>$credits,
			'start_date'=>$start_date,
			'end_date'=>$end_date,
			"summary_data" => $summary_data,
			"second_summary"=>$second_summary,
			"export_excel" => $export_excel
		);
		//print_r($data);
		if ($export_excel == 2)
		{
			$this->load->library('Html2pdf');
			$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			$html2pdf->setDefaultFont('Arial');
			$html = $this->load->view("reports/z_out", $data, true);
			$html2pdf->writeHTML($html);
			$html2pdf->Output('z_out.pdf');
		}
		else
			$this->load->view("reports/z_out", $data);
	}

	//Summary categories report
	function summary_categories($start_date, $end_date, $sale_type, $export_excel=0, $department = 'all')
	{
		$department = urldecode($department);
		$this->load->model('reports/Summary_categories');
		$model = $this->Summary_categories;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department' => $department));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department' => $department));
		$tabular_data = array();
		$report_data = $model->getData();

		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['category'], 'align' => 'left'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['total']), 'align' => 'right'), array('data'=>to_currency($row['tax']), 'align' => 'right'),array('data'=>to_currency($row['profit']), 'align' => 'right'));
		}

		$data = array(
			"title" => lang('reports_categories_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getShortSummaryData(),
			"export_excel" => $export_excel
		);
		if ($export_excel == 2)
		{
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		//$text='whatever we want';
		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
			$this->load->view("reports/tabular",$data);
	}

	//Summary customers report
	function summary_customers($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_customers');
		$model = $this->Summary_customers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$tabular_data = array();
		$report_data = $model->getData();

		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['customer'], 'align' => 'left'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['total']), 'align' => 'right'), array('data'=>to_currency($row['tax']), 'align' => 'right'),array('data'=>to_currency($row['profit']), 'align' => 'right'));
		}

		$data = array(
			"title" => lang('reports_customers_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			$this->load->library('Html2pdf');
			$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			$html2pdf->setDefaultFont('Arial');
			$html = $this->load->view("reports/tabular", $data, true);
			$html2pdf->writeHTML($html);
			$html2pdf->Output('summary_customers.pdf');
		}
		else
			$this->load->view("reports/tabular",$data);
	}

	//Summary suppliers report
	function summary_suppliers($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_suppliers');
		$model = $this->Summary_suppliers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$tabular_data = array();
		$report_data = $model->getData();

		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['supplier'], 'align' => 'left'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['total']), 'align' => 'right'), array('data'=>to_currency($row['tax']), 'align' => 'right'),array('data'=>to_currency($row['profit']), 'align' => 'right'));
		}

		$data = array(
			"title" => lang('reports_suppliers_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			$this->load->library('Html2pdf');
			$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			$html2pdf->setDefaultFont('Arial');
			$html = $this->load->view("reports/tabular", $data, true);
			$html2pdf->writeHTML($html);
			$html2pdf->Output('summary_suppliers.pdf');
		}
		else
			$this->load->view("reports/tabular",$data);
	}

	//Summary items report
	function summary_items($start_date, $end_date, $sale_type, $export_excel=0, $filter = 'Department', $value = 'all')
	{
		ini_set('memory_limit', '512M');
		set_time_limit(180);
		$this->load->model('reports/Summary_items');
		$model = $this->Summary_items;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'filter'=>$filter, 'value' =>addslashes(urldecode(urldecode($value)))));
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'filter'=>$filter, 'value'=>addslashes(urldecode(urldecode($value)))));
		//echo $this->db->last_query().'<br/>';
		$tabular_data = array();
		$report_data = $model->getData();
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['name'], 'align' => 'left'), array('data'=>floor($row['quantity_purchased']), 'align' => 'right'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['total']), 'align' => 'right'), array('data'=>to_currency($row['item_cost_price']), 'align' => 'right'), array('data'=>to_currency($row['tax']), 'align' => 'right'),array('data'=>to_currency($row['profit']), 'align' => 'right'), array('data'=>($row['is_unlimited'])?'Unlimited':($row['quantity']), 'align' => 'right'), array('data'=>($row['is_unlimited'])?'Unlimited':to_currency($row['potential_revenues']), 'align' => 'right'));
		}
		$data = array(
			"title" => lang('reports_items_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
		if ($export_excel == 2)
		{
			//echo '<br/>getting here';
			//$this->load->library('Html2pdf');
			//echo '<br/>getting here2';
			//$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			//echo '<br/>getting here3';
			//$html2pdf->setModeDebug();
			//$html2pdf->setDefaultFont('Arial');
			//echo '<br/>getting here4';
			//$html = $this->load->view("reports/tabular", $data, true);
			//echo str_replace(array('<','>'), array('&lt;','&gt;'), $html);
			//echo '<br/>getting here5';
			//$html2pdf->AddPage();
			//$html2pdf->writeHTML($html);
			//echo '<br/>getting here6';
			//$html2pdf->Output('summary_items.pdf');


			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		//$text='whatever we want';
		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
			$this->load->view("reports/tabular",$data);
	}
	//Summary item kits report
	function summary_item_kits($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_item_kits');
		$model = $this->Summary_item_kits;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$tabular_data = array();
		$report_data = $model->getData();

		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['name'], 'align' => 'left'), array('data'=>floor($row['quantity_purchased']), 'align' => 'right'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['total']), 'align' => 'right'), array('data'=>to_currency($row['tax']), 'align' => 'right'),array('data'=>to_currency($row['profit']), 'align' => 'right'));
		}

		$data = array(
			"title" => lang('reports_item_kits_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .' through '.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			// $this->load->library('Html2pdf');
			// $html2pdf = new Html2pdf('P','A4','fr');
			// $html2pdf->setModeDebug();
			// $html2pdf->setDefaultFont('Arial');
			// $html = $this->load->view("reports/tabular", $data, true);
			// $html2pdf->writeHTML($html);
			// $html2pdf->Output('summary_item_kits.pdf');
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		//$text='whatever we want';
		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
			$this->load->view("reports/tabular",$data);
	}

	//Summary employees report
	function summary_employees($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_employees');
		$model = $this->Summary_employees;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$tabular_data = array();
		$report_data = $model->getData();

		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['employee'], 'align'=>'left'), array('data'=>to_currency($row['subtotal']), 'align'=>'right'), array('data'=>to_currency($row['total']), 'align'=>'right'), array('data'=>to_currency($row['tax']), 'align'=>'right'),array('data'=>to_currency($row['profit']), 'align'=>'right'));
		}

		$data = array(
			"title" => lang('reports_employees_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getShortSummaryData(),
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			$this->load->library('Html2pdf');
			$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			$html2pdf->setDefaultFont('Arial');
			$html = $this->load->view("reports/tabular", $data, true);
			$html2pdf->writeHTML($html);
			$html2pdf->Output('summary_employees.pdf');
		}
		else
			$this->load->view("reports/tabular",$data);
	}

	//Summary taxes report
	function summary_taxes($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_taxes');
		$model = $this->Summary_taxes;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$tabular_data = array();
		$report_data = $model->getData();

		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['percent'], 'align'=>'left'), array('data'=>to_currency($row['tax']), 'align'=>'right'));
		}

		$data = array(
			"title" => lang('reports_taxes_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			$this->load->library('Html2pdf');
			$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			$html2pdf->setDefaultFont('Arial');
			$html = $this->load->view("reports/tabular", $data, true);
			$html2pdf->writeHTML($html);
			$html2pdf->Output('summary_taxes.pdf');
		}
		else
			$this->load->view("reports/tabular",$data);
	}

	//Summary discounts report
	function summary_discounts($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_discounts');
		$model = $this->Summary_discounts;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$tabular_data = array();
		$report_data = $model->getData();

		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['discount_percent'], 'align'=>'left'),array('data'=>$row['count'], 'align'=>'right'));
		}

		$data = array(
			"title" => lang('reports_discounts_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getShortSummaryData(),
			"export_excel" => $export_excel
		);
		if ($export_excel == 2)
		{
			//$this->load->library('Html2pdf');
			//$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			//$html2pdf->setDefaultFont('Arial');
			//$html = $this->load->view("reports/tabular", $data, true);
			//$html2pdf->writeHTML($html);
			//$html2pdf->Output('summary_discounts.pdf');
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		//$text='whatever we want';
		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
			$this->load->view("reports/tabular",$data);
	}

	function summary_payments($start_date, $end_date, $sale_type, $export_excel=0, $department = 'all', $terminal = 'all', $split_days = 0)
	{
		$department = urldecode($department);
		$this->load->model('reports/Summary_payments');
		$model = $this->Summary_payments;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department' => $department, 'terminal' => $terminal));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department' => $department, 'terminal' => $terminal));

		$tabular_data = $tabular_data_2 = array();
		$report_data = $model->getData(false, $split_days);
		//print_r($report_data);
		//$on_account = (isset($report_data['Account']['payment_amount']))?$report_data['Account']['payment_amount']:0;
		//$total_payments = (isset($report_data['Total']['payment_amount']))?$report_data['Total']['payment_amount']-$on_account:0;
		if ($split_days)
		{
			foreach($report_data as $day => $day_data)
			{
				$tabular_data[] = array(array('data'=>date('F d, Y', strtotime($day)), 'align'=>'center', 'colspan'=>2, 'background'=>'#ccc'));
				foreach($day_data as $row)
				{
					if ($row['payment_type'] == 'Total'){}
					else if ($row['payment_type'] == 'Account' || $row['payment_type'] == 'Gift Card')
						$tabular_data_2[] = array(array('data'=>$row['payment_type'], 'align'=>'left'),array('data'=>(($row['payment_amount'] < 0 && $export_excel == 0) ? '<span class="red">'.to_currency($row['payment_amount']).'</span>':to_currency($row['payment_amount'])), 'align'=>'right'));
					else
						$tabular_data[] = array(array('data'=>$row['payment_type'], 'align'=>'left'),array('data'=>(($row['payment_amount'] < 0 && $export_excel == 0) ? '<span class="red">'.to_currency($row['payment_amount']).'</span>':to_currency($row['payment_amount'])), 'align'=>'right'));
					if (isset($row['sub_payments']))
					{
						foreach($row['sub_payments'] as $sub_payment)
						{
							$tabular_data[] = array(array('data'=>$sub_payment['payment_type'], 'align'=>'center', 'font-size'=>10),array('data'=>(($sub_payment['payment_amount'] < 0 && $export_excel == 0) ? '<span class="red">'.to_currency($sub_payment['payment_amount']).'</span>':to_currency($sub_payment['payment_amount'])), 'align'=>'right', 'font-size'=>10));
							//$tabular_data[] = array(array('data'=>'<span style="font-size:10px; padding-left:40px;">'.$sub_payment['payment_type'].'</span>', 'align'=>'left'),array('data'=>(($sub_payment['payment_amount'] < 0 && $export_excel == 0) ? '<span style="font-size:10px;" class="red">'.to_currency($sub_payment['payment_amount']).'</span>':'<span style="font-size:10px;">'.to_currency($sub_payment['payment_amount']).'</span>'), 'align'=>'right'));
						}
					}
				}
			}
		}
		else
		{
			foreach($report_data as $row)
			{
				if ($row['payment_type'] == 'Total'){}
				else if ($row['payment_type'] == 'Account' || $row['payment_type'] == 'Gift Card')
					$tabular_data_2[] = array(array('data'=>$row['payment_type'], 'align'=>'left'),array('data'=>(($row['payment_amount'] < 0 && $export_excel == 0) ? '<span class="red">'.to_currency($row['payment_amount']).'</span>':to_currency($row['payment_amount'])), 'align'=>'right'));
				else
					$tabular_data[] = array(array('data'=>$row['payment_type'], 'align'=>'left'),array('data'=>(($row['payment_amount'] < 0 && $export_excel == 0) ? '<span class="red">'.to_currency($row['payment_amount']).'</span>':to_currency($row['payment_amount'])), 'align'=>'right'));
				if (isset($row['sub_payments']))
				{
					foreach($row['sub_payments'] as $sub_payment)
					{
						$tabular_data[] = array(array('data'=>$sub_payment['payment_type'], 'align'=>'center', 'font-size'=>10),array('data'=>(($sub_payment['payment_amount'] < 0 && $export_excel == 0) ? '<span class="red">'.to_currency($sub_payment['payment_amount']).'</span>':to_currency($sub_payment['payment_amount'])), 'align'=>'right', 'font-size'=>10));
				//		$tabular_data[] = array(array('data'=>'<span style="font-size:10px; padding-left:40px;">'.$sub_payment['payment_type'].'</span>', 'align'=>'left'),array('data'=>(($sub_payment['payment_amount'] < 0 && $export_excel == 0) ? '<span style="font-size:10px;" class="red">'.to_currency($sub_payment['payment_amount']).'</span>':'<span style="font-size:10px;">'.to_currency($sub_payment['payment_amount']).'</span>'), 'align'=>'right'));
					}
				}
			}
		}

		$data = array(
			"title" => lang('reports_payments_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .' through '.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"data_2" => $tabular_data_2,
//			"summary_data" => $model->getSummaryData($report_data),
			"export_excel" => $export_excel
		);
		//echo '<br/>getting summary data';
		$data['summary_data'] = $model->getSummaryData($report_data);
		//echo '<br/>getting all data';
		//$data['alldata'] = $model->getAllData();
		//echo '<br/>done getting all data';
		if ($export_excel == 2)
		{
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
		{
			//echo '<br/>viewing tabular';
			$this->load->view("reports/tabular",$data);
		}
	}

	//Input for reports that require only a date range. (see routes.php to see that all graphical summary reports route here)
	function date_input()
	{
		$data = $this->_get_common_report_data();
		$this->load->view("reports/date_input",$data);
	}

	//Graphical summary sales report
	function graphical_summary_sales($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_sales');
		$model = $this->Summary_sales;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_sales_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_sales_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}

	//The actual graph data
	function graphical_summary_sales_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_sales');
		$model = $this->Summary_sales;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();

		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[strtotime($row['sale_date'])]= $row['total'];
		}

		$data = array(
			"title" => lang('reports_sales_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/line",$data);

	}

	//Graphical summary items report
	function graphical_summary_items($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_items');
		$model = $this->Summary_items;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_items_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_items_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}

	//The actual graph data
	function graphical_summary_items_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_items');
		$model = $this->Summary_items;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();

		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['name']] = $row['total'];
		}

		$data = array(
			"title" => lang('reports_items_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/pie",$data);
	}

	//Graphical summary item kits report
	function graphical_summary_item_kits($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_item_kits');
		$model = $this->Summary_item_kits;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_item_kits_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_item_kits_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}

	//The actual graph data
	function graphical_summary_item_kits_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_item_kits');
		$model = $this->Summary_item_kits;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();

		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['name']] = $row['total'];
		}

		$data = array(
			"title" => lang('reports_item_kits_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/pie",$data);
	}

	//Graphical summary customers report
	function graphical_summary_categories($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_categories');
		$model = $this->Summary_categories;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_categories_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_categories_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}

	//The actual graph data
	function graphical_summary_categories_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_categories');
		$model = $this->Summary_categories;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$report_data = $model->getData();

		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['category']] = $row['total'];
		}

		$data = array(
			"title" => lang('reports_categories_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/pie",$data);
	}

	function graphical_summary_suppliers($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_suppliers');
		$model = $this->Summary_suppliers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_suppliers_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_suppliers_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}

	//The actual graph data
	function graphical_summary_suppliers_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_suppliers');
		$model = $this->Summary_suppliers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$report_data = $model->getData();

		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['supplier']] = $row['total'];
		}

		$data = array(
			"title" => lang('reports_suppliers_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/pie",$data);
	}

	function graphical_summary_employees($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_employees');
		$model = $this->Summary_employees;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_employees_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_employees_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}

	//The actual graph data
	function graphical_summary_employees_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_employees');
		$model = $this->Summary_employees;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();

		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['employee']] = $row['total'];
		}

		$data = array(
			"title" => lang('reports_employees_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/bar",$data);
	}

	function graphical_summary_taxes($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_taxes');
		$model = $this->Summary_taxes;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_taxes_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_taxes_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}

	//The actual graph data
	function graphical_summary_taxes_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_taxes');
		$model = $this->Summary_taxes;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();

		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['percent']] = $row['tax'];
		}

		$data = array(
			"title" => lang('reports_taxes_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/bar",$data);
	}

	//Graphical summary customers report
	function graphical_summary_customers($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_customers');
		$model = $this->Summary_customers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_customers_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_customers_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}

	//The actual graph data
	function graphical_summary_customers_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_customers');
		$model = $this->Summary_customers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$report_data = $model->getData();

		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['customer']] = $row['total'];
		}

		$data = array(
			"title" => lang('reports_customers_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/pie",$data);
	}

	//Graphical summary discounts report
	function graphical_summary_discounts($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_discounts');
		$model = $this->Summary_discounts;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_discounts_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_discounts_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}

	//The actual graph data
	function graphical_summary_discounts_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_discounts');
		$model = $this->Summary_discounts;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();

		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['discount_percent']] = $row['count'];
		}

		$data = array(
			"title" => lang('reports_discounts_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/bar",$data);
	}

	function graphical_summary_payments($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_payments');
		$model = $this->Summary_payments;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_payments_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_payments_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}

	//The actual graph data
	function graphical_summary_payments_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_payments');
		$model = $this->Summary_payments;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();

		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['payment_type']] = $row['payment_amount'];
		}

		$data = array(
			"title" => lang('reports_payments_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/bar",$data);
	}
	function specific_customer_input()
	{
		$data = $this->_get_common_report_data();
		$data['specific_input_name'] = lang('reports_customer');

		$customers = array();
		foreach($this->Customer->get_all()->result() as $customer)
		{
			$customers[$customer->person_id] = $customer->first_name .' '.$customer->last_name;
		}
		$data['specific_input_data'] = $customers;
		$data['report_type'] = 'Specific Customer';
		$this->load->view("reports/specific_input",$data);
	}

	function specific_customer($start_date, $end_date, $customer_id, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Specific_customer');
		$model = $this->Specific_customer;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'customer_id' =>$customer_id, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'customer_id' =>$customer_id, 'sale_type' => $sale_type));

		$headers = $model->getDataColumns();
		$report_data = $model->getData();

		$summary_data = array();
		$details_data = array();

		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format(), strtotime($row['sale_date'])), 'align'=> 'left'), array('data'=>floor($row['items_purchased']), 'align'=> 'left'), array('data'=>$row['employee_name'], 'align'=> 'left'), array('data'=>to_currency($row['subtotal']), 'align'=> 'right'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>to_currency($row['tax']), 'align'=> 'right'),array('data'=>to_currency($row['profit']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'left'));

			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['serialnumber'], 'align'=> 'left'), array('data'=>$drow['description'], 'align'=> 'left'), array('data'=>$drow['quantity_purchased'], 'align'=> 'left'), array('data'=>to_currency($drow['subtotal']), 'align'=> 'right'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>to_currency($drow['tax']), 'align'=> 'right'),array('data'=>to_currency($drow['profit']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
			}
		}

		$customer_info = $this->Customer->get_info($customer_id);
		$data = array(
			"title" => $customer_info->first_name .' '. $customer_info->last_name.' '.lang('reports_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		$this->load->view("reports/tabular_details",$data);
	}

	function specific_employee_input()
	{
		$data = $this->_get_common_report_data();
		$data['specific_input_name'] = lang('reports_employee');

		$employees = array();
		foreach($this->Employee->get_all()->result() as $employee)
		{
			$employees[$employee->person_id] = $employee->first_name .' '.$employee->last_name;
		}
		$data['specific_input_data'] = $employees;
		$this->load->view("reports/specific_input",$data);
	}

	function time_clock($start_date, $end_date, $employee_id, $sale_type, $export_excel=0)
	{
		$this->load->model('timeclock_entry');
		if ($employee_id)
		{
			$employee_info = $this->Employee->get_info($employee_id);
			$employees = array(array('person_id'=>$employee_info->person_id, 'first_name'=>$employee_info->first_name, 'last_name'=>$employee_info->last_name));
		}
		else
			$employees = $this->Employee->get_all()->result_array();
		$headers =  array(
            'summary' => array(
                array('data'=>lang('reports_employee'), 'align'=> 'left'),
                array('data'=>lang('reports_total_hours'), 'align'=> 'left')
                ),
            'details' => array(
                array('data'=>lang('reports_start_time'), 'align'=> 'left'),
                array('data'=>lang('reports_end_time'), 'align'=> 'left'),
                array('data'=>lang('reports_total'), 'align'=> 'left')
                )
            );
		$summary_data = array();
		$details_data = array();
		foreach ($employees as $key => $employee)
		{
			$total_time = 0;
			$time_entries = $this->timeclock_entry->get_all(10000, $employee['person_id'], $start_date, $end_date)->result_array();
			foreach ($time_entries as $time_entry)
			{
				$details_data[$key][] = array(
					array('data'=>anchor('timeclock/view/'.$employee['person_id'].'/'.str_replace(' ', 'T', $time_entry['shift_start']).'/width~500', $time_entry['shift_start'], array('class'=>'colbox', 'target'=>'_blank', 'title'=>'Edit Shift - '.$employee['last_name'].', '.$employee['first_name'])), 'align'=> 'left'),
					array('data'=>anchor('timeclock/view/'.$employee['person_id'].'/'.str_replace(' ', 'T', $time_entry['shift_start']).'/width~500', $time_entry['shift_end'], array('class'=>'colbox', 'target'=>'_blank', 'title'=>'Edit Shift - '.$employee['last_name'].', '.$employee['first_name'])), 'align'=> 'left'),
					array('data'=>anchor('timeclock/view/'.$employee['person_id'].'/'.str_replace(' ', 'T', $time_entry['shift_start']).'/width~500', $time_entry['total'], array('class'=>'colbox', 'target'=>'_blank', 'title'=>'Edit Shift - '.$employee['last_name'].', '.$employee['first_name'])), 'align'=> 'left')
				);
				$total_time += ($time_entry['shift_end'] != '0000-00-00 00:00:00' ? strtotime($time_entry['shift_end']) - strtotime($time_entry['shift_start']) : 0);
			}
			$summary_data[] = array(array('data'=>$employee['last_name'].', '.$employee['first_name'], 'align'=> 'left'), array('data'=>floor($total_time/(60*60)*100)/100, 'align'=> 'left'));

		}

		$data = array(
			"title" => $employee_info->first_name .' '. $employee_info->last_name.' '.lang('reports_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $headers,
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => '',
			"export_excel" => 0,
			'expanded' => true
		);
		$this->load->view("reports/tabular_details",$data);
	}
	function play_length($start_date, $end_date, $teesheet_id, $export_excel=0)
	{
		$this->load->model('teetime');
		$play_data = $this->teetime->get_play_length_data($start_date, $end_date, $teesheet_id);
		$headers =  array(
            'summary' => array(
                array('data'=>lang('reports_teed_off'), 'align'=> 'left'),
                array('data'=>lang('reports_turn'), 'align'=> 'left'),
                array('data'=>lang('reports_finish'), 'align'=> 'left'),
                array('data'=>lang('reports_half_time'), 'align'=> 'left'),
                array('data'=>lang('reports_total'), 'align'=> 'left')
            )
        );
		$summary_data = array();
		foreach ($play_data as $play)
		{
			$summary_data[] = array(
				array('data'=> $play['teed_off_time'], 'align'=> 'left'),
				array('data'=> $play['turn_time'], 'align'=> 'left'),
				array('data'=> $play['finish_time'], 'align'=> 'left'),
				array('data'=> $play['half_time'], 'align'=> 'left'),
				array('data'=> $play['total_time'], 'align'=> 'left')
			);
			$half_time += ($play['half_time'] > 0 ? $play['half_time'] : 0);
			$half_time_count += ($play['half_time'] > 0 ? 1 : 0);
			$total_time += ($play['total_time'] > 0 ? $play['total_time'] : 0);
			$total_time_count += ($play['total_time'] > 0 ? 1 : 0);
		}
		//$summary_data[] = array(array('data'=>$employee['last_name'].', '.$employee['first_name'], 'align'=> 'left'), array('data'=>floor($total_time/(60*60)*100)/100, 'align'=> 'left'));

/*		foreach ($employees as $key => $employee)
		{
			$total_time = 0;
			$time_entries = $this->timeclock_entry->get_all(10000, $employee['person_id'], $start_date, $end_date)->result_array();
			foreach ($time_entries as $time_entry)
			{
				$details_data[$key][] = array(
					array('data'=>anchor('timeclock/view/'.$employee['person_id'].'/'.str_replace(' ', 'T', $time_entry['shift_start']).'/width~500', $time_entry['shift_start'], array('class'=>'colbox', 'target'=>'_blank', 'title'=>'Edit Shift - '.$employee['last_name'].', '.$employee['first_name'])), 'align'=> 'left'),
					array('data'=>anchor('timeclock/view/'.$employee['person_id'].'/'.str_replace(' ', 'T', $time_entry['shift_start']).'/width~500', $time_entry['shift_end'], array('class'=>'colbox', 'target'=>'_blank', 'title'=>'Edit Shift - '.$employee['last_name'].', '.$employee['first_name'])), 'align'=> 'left'),
					array('data'=>anchor('timeclock/view/'.$employee['person_id'].'/'.str_replace(' ', 'T', $time_entry['shift_start']).'/width~500', $time_entry['total'], array('class'=>'colbox', 'target'=>'_blank', 'title'=>'Edit Shift - '.$employee['last_name'].', '.$employee['first_name'])), 'align'=> 'left')
				);
				$total_time += ($time_entry['shift_end'] != '0000-00-00 00:00:00' ? strtotime($time_entry['shift_end']) - strtotime($time_entry['shift_start']) : 0);
			}
			$summary_data[] = array(array('data'=>$employee['last_name'].', '.$employee['first_name'], 'align'=> 'left'), array('data'=>floor($total_time/(60*60)*100)/100, 'align'=> 'left'));

		}
*/		//print_r($data);
		$data = array(
			//"title" => $employee_info->first_name .' '. $employee_info->last_name.' '.lang('reports_report'),
			//"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $headers,
			"summary_data" => $summary_data,
			//"details_data" => $details_data,
			"overall_summary_data" => array('average_half'=>floor($half_time/$half_time_count*100)/100,'average_total'=>floor($total_time/$total_time_count*100)/100),
			"export_excel" => 0,
			'report'=>'play_length'
		);
		$this->load->view("reports/tabular_details",$data);
	}
	function specific_employee($start_date, $end_date, $employee_id, $sale_type, $export_excel=0)
	{
		ini_set('memory_limit', '512M');
		$this->load->model('reports/Specific_employee');
		$model = $this->Specific_employee;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'employee_id' =>$employee_id, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'employee_id' =>$employee_id, 'sale_type' => $sale_type));
		$headers = $model->getDataColumns();
		$report_data = $model->getData();

		$summary_data = array();
		$details_data = array();
		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format(), strtotime($row['sale_date'])), 'align'=> 'left'), array('data'=>$row['items_purchased'], 'align'=> 'left'), array('data'=>$row['customer_name'], 'align'=> 'left'), array('data'=>to_currency($row['subtotal']), 'align'=> 'right'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>to_currency($row['tax']), 'align'=> 'right'),array('data'=>to_currency($row['profit']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'left'));

			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['serialnumber'], 'align'=> 'left'), array('data'=>$drow['description'], 'align'=> 'left'), array('data'=>$drow['quantity_purchased'], 'align'=> 'left'), array('data'=>to_currency($drow['subtotal']), 'align'=> 'right'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>to_currency($drow['tax']), 'align'=> 'right'),array('data'=>to_currency($drow['profit']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
			}
		}

		$employee_info = $this->Employee->get_info($employee_id);
		$data = array(
			"title" => $employee_info->first_name .' '. $employee_info->last_name.' '.lang('reports_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		$this->load->view("reports/tabular_details",$data);
	}

	function detailed_sales($start_date, $end_date, $sale_type, $export_excel=0, $department = 'all', $sale_id = false, $terminal = 'all')
	{
		ini_set('memory_limit', '512M');
		set_time_limit(180);
		$department = urldecode($department);
		$this->load->model('reports/Detailed_sales');
		$model = $this->Detailed_sales;
		$params = array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department'=> $department, 'terminal'=>$terminal);
		if ($sale_id)
            $params['sale_id'] = trim(str_ireplace('pos', '', urldecode($sale_id)));
		// if ($sale_number)
			// $params['sale_id'] = $this->Sale->get_sale_id(trim(str_ireplace('pos', '', urldecode($sale_number))));
		//echo $params['sale_id'];
		$model->setParams($params);

		$this->Sale->create_sales_items_temp_table($params);
		//echo "<br/>created temp table";
		//echo $this->db->last_query();
		$headers = $model->getDataColumns();
		//echo '<br/>getting here';
		$report_data = $model->getData();
		//echo "<br/>got data";
		//print_r($report_data);
		$summary_data = array();
		$details_data = array();

		foreach($report_data['summary'] as $key=>$row)
		//while ($row = array_shift($report_data['summary']))
		{
			$summary_data[] = array(
				array('data'=>anchor('reports/sale_details/'.$row['sale_id'],'+',"class='get_details'"), 'align'=>'left'),
				array('data'=>anchor('sales/edit/'.$row['sale_id'].'/popup/width~720', lang('common_edit').' '.$row['sale_id'], array('target' => '_blank', 'class' => 'colbox', 'title' => 'Edit Sale - POS '.$row['sale_id'])), 'align'=>'left'),
				array('data'=>date('n/j g:ia', strtotime($row['sale_date'])), 'align'=>'left'),
				array('data'=>$row['items_purchased'], 'align'=>'right'),
				array('data'=>$row['employee_name'], 'align'=>'left'),
				array('data'=>$row['customer_name'], 'align'=>'left'),
				array('data'=>to_currency($row['subtotal']), 'align'=>'right'),
				array('data'=>to_currency($row['total']), 'align'=>'right'),
				array('data'=>to_currency($row['tax']), 'align'=>'right'),
				array('data'=>to_currency($row['profit']), 'align'=>'right'),
				array('data'=>$row['payment_type'], 'align'=>'left')
//				array('data'=>$row['comment'], 'align'=>'right')
			);
			$key = count($summary_data)-1;

			if(empty($report_data['details'][$key])){ continue; }

			foreach($report_data['details'][$key] as $drow)
			//while ($drow = array_shift($report_data['details'][$key]))
			{
				$details_data[$key][] = array(
					array('data'=>isset($drow['item_number']) ? $drow['item_number'] : $drow['item_kit_number'], 'align'=>'left'),
					array('data'=>isset($drow['item_name']) ? $drow['item_name'] : (isset($drow['item_kit_name']) ? $drow['item_kit_name'] : 'INV '.$drow['invoice_number']), 'align'=>'left'),
					array('data'=>$drow['category'], 'align'=>'left'), array('data'=>$drow['subcategory'], 'align'=>'left'),
					array('data'=>$drow['description'], 'align'=>'left'), array('data'=>floor($drow['quantity_purchased']), 'align'=>'right'),
					array('data'=>to_currency($drow['subtotal']), 'align'=>'right'), array('data'=>to_currency($drow['total']), 'align'=>'right'),
					array('data'=>to_currency($drow['tax']), 'align'=>'right'),array('data'=>to_currency($drow['profit']), 'align'=>'right'),
					array('data'=>$drow['discount_percent'].'%', 'align'=>'right'));
/*				$summary_data[] = array(
					array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=>'left'),
					array('data'=>date(get_date_format(), strtotime($row['sale_date'])), 'align'=>'left'),
					array('data'=>$row['items_purchased'], 'align'=>'left'),
					array('data'=>$row['employee_name'], 'align'=>'left'),
					array('data'=>$row['customer_name'], 'align'=>'left'),
					array('data'=>to_currency($row['subtotal']), 'align'=>'right'),
					array('data'=>to_currency($row['total']), 'align'=>'right'),
					array('data'=>to_currency($row['tax']), 'align'=>'right'),
					array('data'=>to_currency($row['profit']), 'align'=>'right'),
					array('data'=>$row['payment_type'], 'align'=>'right'),
					array('data'=>$row['comment'], 'align'=>'right')
				);

				foreach($report_data['details'][$key] as $drow)
				{
					$details_data[$key][] = array(array('data'=>isset($drow['item_number']) ? $drow['item_number'] : $drow['item_kit_number'], 'align'=>'left'), array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=>'left'), array('data'=>$drow['category'], 'align'=>'left'), array('data'=>$drow['subcategory'], 'align'=>'left'), array('data'=>$drow['description'], 'align'=>'left'), array('data'=>$drow['quantity_purchased'], 'align'=>'left'), array('data'=>to_currency($drow['subtotal']), 'align'=>'right'), array('data'=>to_currency($drow['total']), 'align'=>'right'), array('data'=>to_currency($drow['tax']), 'align'=>'right'),array('data'=>to_currency($drow['profit']), 'align'=>'right'), array('data'=>$drow['discount_percent'].'%', 'align'=>'left'));
				}*/
			}
		}

		//echo "<br/>foreached";
		$data = array(
			"report_type"=>'detailed_sales',
			"title" =>lang('reports_detailed_sales_report').': Department - '.ucfirst($department),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" =>$headers,
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
		//echo "<br/>summarydata";
		if ($export_excel == 2)
		{
			//$this->load->library('Html2pdf');
			//$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			//$html2pdf->setDefaultFont('Arial');
			//$html = $this->load->view("reports/tabular_details", $data, true);
			//$html2pdf->writeHTML($html);
			//$html2pdf->Output('deleted_sales.pdf');
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		//$text='whatever we want';
		    $html = $this->load->view("reports/tabular_details", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else{
			$this->load->view("reports/tabular_details",$data);
		}

	}

	function sale_details($sale_id = null){
		$this->load->model('Sale');
		$data['details'] = $this->Sale->get_sale_details($sale_id);
		$this->load->view('reports/sale_details', $data);
	}

	function detailed_teesheets($date, $teesheet_id)
	{
		//echo "loading teesheet model $date $teesheet_id<br/>";
		$this->load->model('teesheet');
		//echo 'loaded teesheet model<br/>';
		if ($this->permissions->course_has_module('reservations'))
		{
			$this->schedule->print_teesheet($date, $teesheet_id, true);
		}
		else
		{
			$this->teesheet->print_teesheet($date, $teesheet_id, true);
		}
		//echo 'printed teesheet <br/>';
	}

	function summary_tee_times($teesheet_id, $start_date, $end_date, $export_excel=0)
	{
		$this->load->model('reports/Summary_tee_times');
		$model = $this->Summary_tee_times;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date,  'teesheet_id' => $teesheet_id));
		$tabular_data = $tabular_data_2 = array();
		$report_datas = $model->getData();
		//print_r($report_datas);
		foreach ($report_datas as $report_data)
		{
			$tabular_data[] = array(
				array('data'=>$report_data['holes'].' Holes', 'align'=>'left'),
				array('data'=>'', 'align'=>'right')
			);
			$tabular_data[] = array(
				array('data'=>($export_excel==0?'&nbsp;&nbsp;&nbsp;&nbsp;':'').lang('reports_rounds_played'), 'align'=>'left'),
				array('data'=>$report_data['teetimes'], 'align'=>'right')
			);
			$tabular_data[] = array(
				array('data'=>($export_excel==0?'&nbsp;&nbsp;&nbsp;&nbsp;':'').lang('reports_players_booked'), 'align'=>'left'),
				array('data'=>$report_data['players'], 'align'=>'right')
			);
			$tabular_data[] = array(
				array('data'=>($export_excel==0?'&nbsp;&nbsp;&nbsp;&nbsp;':'').lang('reports_no_shows'), 'align'=>'left'),
				array('data'=>$report_data['no_show_tee_times'], 'align'=>'right')
			);
		}

		$data = array(
			"title" => lang('reports_payments_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"export_excel" => $export_excel
		);
		$data['summary_data'] = $model->getSummaryData($report_data);

		if ($export_excel == 2)
		{
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
		{
			$this->load->view("reports/tabular",$data);
		}
	}

	function detailed_taxes($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Detailed_taxes');
		$model = $this->Detailed_taxes;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		//echo $this->db->last_query();
		$tabular_data = array();
		$report_data = $model->getData();

		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['percent'], 'align'=>'left'), array('data'=>to_currency($row['tax']), 'align'=>'right'));
		}

		$data = array(
			"title" => lang('reports_taxes_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
			// $this->load->library('Html2pdf');
			// $html2pdf = new Html2pdf('P','A4','fr');
			// //$html2pdf->setModeDebug();
			// $html2pdf->setDefaultFont('Arial');
			// $html = $this->load->view("reports/tabular", $data, true);
			// $html2pdf->writeHTML($html);
			// $html2pdf->Output('summary_taxes.pdf');
		}
		else
			$this->load->view("reports/tabular",$data);
	}

	function detailed_categories($start_date, $end_date, $sale_type, $department='all', $export_excel=0, $terminal = 'all')
	{
		ini_set('memory_limit', '512M');
		set_time_limit(180);
		$department = urldecode(urldecode($department));
		//summary categories
		$this->load->model('reports/Detailed_categories');
		$model = $this->Detailed_categories;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department'=> $department, 'terminal'=>$terminal));
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department'=>$department,'terminal'=>$terminal));
		$this->benchmark->mark('temp_table_end');
		$tabular_data = array();
		$report_data = $model->getData();
		//print_r($report_data);
		$summary_data = array();
		$details_data = array();
		if ($export_excel == 2)
		{
			foreach($report_data['dept_summary'] as $key=>$row)
			{
				$d_name = ($row['department'] != '')?$row['department']:'Department not specified';
				$d_val = lang('reports_department').': '.$d_name;
				$department_data[] = array(array('data'=>$d_val, 'align'=>'right'), array('data'=>'', 'align' => 'left'), array('data'=>$row['department'].' Total:', 'align'=>'right'), array('data'=>$row['quantity'], 'align'=>'right'), array('data'=>to_currency($row['subtotal']), 'align'=>'right'), array('data'=>to_currency($row['tax']), 'align'=>'right'), array('data'=>to_currency($row['total']), 'align' => 'right'), array('data'=>to_currency($row['cost']), 'align' => 'right'),array('data'=>to_currency($row['profit']), 'align' => 'right'));
				foreach($report_data['summary'][$key] as $dkey=>$drow)
				{
					$category_data[$key][] = array(array('data'=>lang('reports_category').': '.$drow['category'], 'align'=>'right'), array('data'=>'', 'align' => 'left'), array('data'=>$drow['category'].' Total:', 'align'=>'right'), array('data'=>$drow['quantity'], 'align'=>'right'), array('data'=>to_currency($drow['subtotal']), 'align'=>'right'), array('data'=>to_currency($drow['tax']), 'align'=>'right'), array('data'=>to_currency($drow['total']), 'align' => 'right'), array('data'=>to_currency($drow['cost']), 'align' => 'right'),array('data'=>to_currency($drow['profit']), 'align' => 'right'));
					foreach($report_data['details'][$key][$dkey] as $ekey=>$erow)
					{
						$subcategory_data[$key][$dkey][] = array(array('data'=>lang('reports_subcategory').': '.$erow['subcategory'], 'align'=>'right'), array('data'=>'', 'align' => 'left'), array('data'=>$erow['subcategory'].' Total:', 'align'=>'right'), array('data'=>$erow['quantity'], 'align'=>'right'), array('data'=>to_currency($erow['subtotal']), 'align'=>'right'), array('data'=>to_currency($erow['tax']), 'align'=>'right'), array('data'=>to_currency($erow['total']), 'align' => 'right'), array('data'=>to_currency($erow['cost']), 'align' => 'right'),array('data'=>to_currency($erow['profit']), 'align' => 'right'));
						foreach($report_data['items'][$key][$dkey][$ekey] as $irow)
						{
							$item_data[$key][$dkey][$ekey][] = array(array('data'=>$irow['item_number'], 'align' => 'center'), array('data'=>$irow['name'], 'align' => 'left'), array('data'=>'', 'align'=>'right'), array('data'=>$irow['quantity'], 'align'=>'right'), array('data'=>to_currency($irow['subtotal']), 'align'=>'right'), array('data'=>to_currency($irow['tax']), 'align'=>'right'), array('data'=>to_currency($irow['total']), 'align' => 'right'), array('data'=>to_currency($irow['cost']), 'align' => 'right'),array('data'=>to_currency($irow['profit']), 'align' => 'right'));
						}
					}
				}
			}
		}
		else
		{
			foreach($report_data['dept_summary'] as $key=>$row)
			{
				$department_data[] = array(array('data'=>($row['department'] != '')?$row['department']:'Department not specified', 'align' => 'left'), array('data'=>$row['quantity'], 'align'=>'right'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['tax']), 'align' => 'right'), array('data'=>'<div class="payments_box"><div class="cash_box">*Cash '.to_currency($row['cash']).'</div><div class="credit_box">*CC '.to_currency($row['credit']).'</div></div><div class="total_box">'.to_currency($row['total']).'</div>', 'align' => 'right'), array('data'=>to_currency($row['cost']), 'align' => 'right'),array('data'=>to_currency($row['profit']), 'align' => 'right'));
				foreach($report_data['summary'][$key] as $dkey=>$drow)
				{
					$category_data[$key][] = array(array('data'=>$drow['category'], 'align' => 'left'), array('data'=>$drow['quantity'], 'align'=>'right'), array('data'=>to_currency($drow['subtotal']), 'align' => 'right'), array('data'=>to_currency($drow['tax']), 'align' => 'right'), array('data'=>'<div class="payments_box"><div class="cash_box">*Cash '.to_currency($drow['cash']).'</div><div class="credit_box">*CC '.to_currency($drow['credit']).'</div></div><div class="total_box">'.to_currency($drow['total']).'</div>', 'align' => 'right'), array('data'=>to_currency($drow['cost']), 'align' => 'right'),array('data'=>to_currency($drow['profit']), 'align' => 'right'));
					foreach($report_data['details'][$key][$dkey] as $ekey=>$erow)
					{
						$subcategory_data[$key][$dkey][] = array(array('data'=>$erow['subcategory'], 'align' => 'left'), array('data'=>$erow['quantity'], 'align'=>'right'), array('data'=>to_currency($erow['subtotal']), 'align' => 'right'), array('data'=>to_currency($erow['tax']), 'align' => 'right'), array('data'=>'<div class="payments_box"><div class="cash_box">*Cash '.to_currency($erow['cash']).'</div><div class="credit_box">*CC '.to_currency($erow['credit']).'</div></div><div class="total_box">'.to_currency($erow['total']).'</div>', 'align' => 'right'), array('data'=>to_currency($erow['cost']), 'align' => 'right'),array('data'=>to_currency($erow['profit']), 'align' => 'right'));
						foreach($report_data['items'][$key][$dkey][$ekey] as $irow)
						{
							$item_data[$key][$dkey][$ekey][] = array(array('data'=>($irow['name'] != '')?$irow['name']:'-----', 'align' => 'left'), array('data'=>$irow['quantity'], 'align'=>'right'), array('data'=>to_currency($irow['subtotal']), 'align' => 'right'), array('data'=>to_currency($irow['tax']), 'align' => 'right'), array('data'=>to_currency($irow['total']), 'align' => 'right'), array('data'=>to_currency($irow['cost']), 'align' => 'right'),array('data'=>to_currency($irow['profit']), 'align' => 'right'));
						}
					}
				}
			}
		}

		$data = array(
			"title" => lang('reports_categories_detailed_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"dates" => array('start_date'=>$start_date, 'end_date'=>$end_date),
			"headers" => $model->getDataColumns(),
			"data" => array('dep'=>$department_data,'cat'=>$category_data,'subcat'=>$subcategory_data,'items'=>$item_data),
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
		$this->load->view("reports/tabular_categories",$data);
	}
	function summary_gl_code($start_date, $end_date, $sale_type, $export_excel=0, $department = 'all')
	{
		$department = urldecode($department);
		$this->load->model('reports/summary_gl_code');
		$model = $this->summary_gl_code;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department' => $department));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department' => $department));
		$tabular_data = array();
		$report_data = $model->getData();

		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['gl_code'], 'align' => 'left'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['total']), 'align' => 'right'), array('data'=>to_currency($row['tax']), 'align' => 'right'),array('data'=>to_currency($row['profit']), 'align' => 'right'));
		}

		$data = array(
			"title" => 'GL Code Report',
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getShortSummaryData(),
			"export_excel" => $export_excel
		);
		if ($export_excel == 2)
		{
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		//$text='whatever we want';
		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
			$this->load->view("reports/tabular",$data);
	}
    function specific_supplier_input()
	{
		$data = $this->_get_common_report_data();
		$data['specific_input_name'] = lang('reports_supplier');

		$suppliers = array();
		foreach($this->Supplier->get_all()->result() as $supplier)
		{
			$suppliers[$supplier->person_id] = $supplier->company_name.' ('.$supplier->first_name .' '.$supplier->last_name.')';
		}
		$data['specific_input_data'] = $suppliers;
		$this->load->view("reports/specific_input",$data);
	}

	function specific_supplier($start_date, $end_date, $supplier_id, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Specific_supplier');
		$model = $this->Specific_supplier;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'supplier_id' =>$supplier_id, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'supplier_id' =>$supplier_id, 'sale_type' => $sale_type));
		$headers = $model->getDataColumns();
		$report_data = $model->getData();

		$summary_data = array();
		$details_data = array();

		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format(), strtotime($row['sale_date'])), 'align'=> 'left'), array('data'=>$row['items_purchased'], 'align'=> 'left'), array('data'=>$row['customer_name'], 'align'=> 'left'), array('data'=>to_currency($row['subtotal']), 'align'=> 'right'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>to_currency($row['tax']), 'align'=> 'right'),array('data'=>to_currency($row['profit']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'left'), array('data'=>$row['comment'], 'align'=> 'left'));

			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['serialnumber'], 'align'=> 'left'), array('data'=>$drow['description'], 'align'=> 'left'), array('data'=>$drow['quantity_purchased'], 'align'=> 'left'), array('data'=>to_currency($drow['subtotal']), 'align'=> 'right'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>to_currency($drow['tax']), 'align'=> 'right'),array('data'=>to_currency($drow['profit']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
			}
		}
		$supplier_info = $this->Supplier->get_info($supplier_id);

		$data = array(
					"title" => $supplier_info->company_name.' ('.$supplier_info->first_name .' '. $supplier_info->last_name.') '.lang('reports_report'),
					"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
					"headers" => $model->getDataColumns(),
					"summary_data" => $summary_data,
					"details_data" => $details_data,
					"overall_summary_data" => $model->getSummaryData(),
					"export_excel" => $export_excel
		);

		$this->load->view("reports/tabular_details",$data);
	}



	function deleted_sales($start_date, $end_date, $sale_type, $export_excel=0)
	{
        $this->load->model('reports/Deleted_sales');
		$model = $this->Deleted_sales;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$headers = $model->getDataColumns();
		$report_data = $model->getData();

		$summary_data = array();
		$details_data = array();

		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=>'left'), array('data'=>date(get_date_format(), strtotime($row['sale_date'])), 'align'=>'left'), array('data'=>floor($row['items_purchased']), 'align'=>'right'), array('data'=>$row['employee_name'], 'align'=>'left'), array('data'=>$row['customer_name'], 'align'=>'left'), array('data'=>to_currency($row['subtotal']), 'align'=>'right'), array('data'=>to_currency($row['total']), 'align'=>'right'), array('data'=>to_currency($row['tax']), 'align'=>'right'),array('data'=>to_currency($row['profit']), 'align'=>'right'), array('data'=>$row['payment_type'], 'align'=>'left'));

			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=>'left'), array('data'=>$drow['category'], 'align'=>'left'), array('data'=>$drow['serialnumber'], 'align'=>'left'), array('data'=>$drow['description'], 'align'=>'left'), array('data'=>floor($drow['quantity_purchased']), 'align'=>'right'), array('data'=>to_currency($drow['subtotal']), 'align'=>'right'), array('data'=>to_currency($drow['total']), 'align'=>'right'), array('data'=>to_currency($drow['tax']), 'align'=>'right'),array('data'=>to_currency($drow['profit']), 'align'=>'right'), array('data'=>$drow['discount_percent'].'%', 'align'=>'right'));
			}
		}

		$data = array(
			"title" =>lang('reports_deleted_sales_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			//$this->load->library('Html2pdf');
			//$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			//$html2pdf->setDefaultFont('Arial');
			//$html = $this->load->view("reports/tabular_details", $data, true);
			//$html2pdf->writeHTML($html);
			//$html2pdf->Output('deleted_sales.pdf');
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		//$text='whatever we want';
		    $html = $this->load->view("reports/tabular_details", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
			$this->load->view("reports/tabular_details",$data);
	}

	function detailed_receivings($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Detailed_receivings');
		$model = $this->Detailed_receivings;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Receiving->create_receivings_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$headers = $model->getDataColumns();
		$report_data = $model->getData();

		$summary_data = array();
		$details_data = array();

		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('receivings/edit/'.$row['receiving_id'], 'RECV '.$row['receiving_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format(), strtotime($row['receiving_date'])), 'align'=> 'left'), array('data'=>$row['items_purchased'], 'align'=> 'left'), array('data'=>$row['employee_name'], 'align'=> 'left'), array('data'=>$row['supplier_name'], 'align'=> 'left'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'left'), array('data'=>$row['comment'], 'align'=> 'left'));

			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>$drow['name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['quantity_purchased'], 'align'=> 'left'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
			}
		}

		$data = array(
			"title" =>lang('reports_detailed_receivings_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		$this->load->view("reports/tabular_details",$data);
	}
	function deleted_suspended_sales($start_date, $end_date, $sale_type, $export_excel=0)
	{
        $this->load->model('reports/Deleted_suspended_sales');
		$model = $this->Deleted_suspended_sales;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$headers = $model->getDataColumns();
		$report_data = $model->getData();

		$summary_data = array();
		$details_data = array();

		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=>'left'), array('data'=>date(get_date_format(), strtotime($row['sale_date'])), 'align'=>'left'), array('data'=>floor($row['items_purchased']), 'align'=>'right'), array('data'=>$row['employee_name'], 'align'=>'left'), array('data'=>$row['customer_name'], 'align'=>'left'), array('data'=>to_currency($row['subtotal']), 'align'=>'right'), array('data'=>to_currency($row['total']), 'align'=>'right'), array('data'=>to_currency($row['tax']), 'align'=>'right'),array('data'=>to_currency($row['profit']), 'align'=>'right'), array('data'=>$row['payment_type'], 'align'=>'left'));

			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=>'left'), array('data'=>$drow['category'], 'align'=>'left'), array('data'=>$drow['serialnumber'], 'align'=>'left'), array('data'=>$drow['description'], 'align'=>'left'), array('data'=>floor($drow['quantity_purchased']), 'align'=>'right'), array('data'=>to_currency($drow['subtotal']), 'align'=>'right'), array('data'=>to_currency($drow['total']), 'align'=>'right'), array('data'=>to_currency($drow['tax']), 'align'=>'right'),array('data'=>to_currency($drow['profit']), 'align'=>'right'), array('data'=>$drow['discount_percent'].'%', 'align'=>'right'));
			}
		}

		$data = array(
			"title" =>lang('reports_deleted_sales_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			//$this->load->library('Html2pdf');
			//$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			//$html2pdf->setDefaultFont('Arial');
			//$html = $this->load->view("reports/tabular_details", $data, true);
			//$html2pdf->writeHTML($html);
			//$html2pdf->Output('deleted_sales.pdf');
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);
		//$text='whatever we want';
		    $html = $this->load->view("reports/tabular_details", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
			$this->load->view("reports/tabular_details",$data);
	}
	function excel_export($type = '')
	{
		$data = array('type' => $type);
		$departments = $this->Item->get_department_suggestions();
		$data['departments']['all'] = 'All';
		foreach ($departments as $department) {
			$data['departments'][$department['label']] = $department['label'];
		}
		$categories = $this->Item->get_category_suggestions();
		$data['categories']['all'] = 'All';
		foreach ($categories as $category) {
			$data['categories'][$category['label']] = $category['label'];
		}
		$subcategories = $this->Item->get_subcategory_suggestions();
		$data['subcategory']['all'] = 'All';
		foreach ($subcategories as $subcategory) {
			$data['subcategories'][$subcategory['label']] = $subcategory['label'];
		}
		$suppliers = $this->Supplier->get_all()->result_array();
		$data['subcategory']['all'] = 'All';
		foreach ($suppliers as $supplier) {
			$data['suppliers'][$supplier['person_id']] = $supplier['company_name'].' ('.$supplier['last_name'].', '.$supplier['first_name'].')';
		}

		$this->load->view("reports/excel_export", $data);
	}

	function inventory_low($export_excel=0, $filter = 'Department', $value = 'value')
	{
		$this->load->model('reports/Inventory_low');
		$model = $this->Inventory_low;
		$model->setParams(array('filter'=>$filter,'value'=>$value));
		$tabular_data = array();
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['name'], 'align'=> 'left'), array('data'=>$row['item_number'], 'align'=> 'left'), array('data'=>$row['description'], 'align'=> 'left'),array('data'=>to_currency($row['cost_price']), 'align'=> 'right'),array('data'=>to_currency($row['unit_price']), 'align'=> 'right'), array('data'=>$row['quantity'], 'align'=> 'left'), array('data'=>$row['reorder_level'], 'align'=> 'left'));
		}

		$data = array(
			"title" => lang('reports_low_inventory_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel
		);

		$this->load->view("reports/tabular",$data);
	}
	function inventory_summary($export_excel=0, $filter='Department', $value='all')
	{
		$this->load->model('reports/Inventory_summary');
		$model = $this->Inventory_summary;
		$tabular_data = array();
		$model->setParams(array('filter'=>$filter,'value'=>$value));
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['name'], 'align'=> 'left'), array('data'=>$row['item_number'], 'align'=> 'left'), array('data'=>$row['description'], 'align'=> 'left'), array('data'=>to_currency($row['cost_price']), 'align'=> 'right'),array('data'=>to_currency($row['unit_price']), 'align' => 'right'), array('data'=>$row['quantity'], 'align'=> 'right'));
		}
		$data = array(
			"title" => lang('reports_inventory_summary_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel
		);
		$this->load->view("reports/tabular",$data);
	}
	function inventory_summary_2($export_excel=0, $filter='Department', $value='all', $supplier_id = false)//$start_date, $end_date, $sale_type, $department='all', $export_excel=0)
	{
		ini_set('memory_limit', '512M');
		set_time_limit(180);
		$department = urldecode(urldecode($department));
		//summary categories
		$tabular_data = array();
		$summary_data = array();
		$details_data = array();

		$this->load->model('reports/Inventory_summary');
		$model = $this->Inventory_summary;
		$tabular_data = array();
		$model->setParams(array('filter'=>$filter,'value'=>$value, 'supplier_id'=>$supplier_id));
		$report_data = $model->getData2(array());
		//print_r($report_data);
		//return;
		if ($export_excel == 2)
		{
			foreach($report_data['dept_summary'] as $key=>$row)
			{
				$d_name = ($row['department'] != '')?$row['department']:'Department not specified';
				$d_val = lang('reports_department').': '.$d_name;
				$department_data[] = array(
					array('data'=>$d_val, 'align'=>'left'),
					array('data'=>'-', 'align' => 'left'),
					array('data'=>$row['department'].' Total:', 'align'=>'right'),
					array('data'=>'-', 'align'=>'right'),
					//array('data'=>to_currency($row['subtotal']), 'align'=>'right'),
					//array('data'=>to_currency($row['tax']), 'align'=>'right'),
					array('data'=>to_currency($row['inventory_value']), 'align' => 'right'),
					array('data'=>to_currency($row['potential_value']), 'align' => 'right'));
				foreach($report_data['summary'][$key] as $dkey=>$drow)
				{
					$category_data[$key][] = array(
						array('data'=>lang('reports_category').': '.$drow['category'], 'align'=>'right'),
						array('data'=>'-', 'align' => 'left'),
						array('data'=>$drow['category'].' Total:', 'align'=>'right'),
						//array('data'=>'', 'align'=>'right'),
						//array('data'=>'', 'align'=>'right'),
						array('data'=>('-'), 'align'=>'right'),
						array('data'=>to_currency($drow['inventory_value']), 'align' => 'right'),
						array('data'=>to_currency($drow['potential_value']), 'align' => 'right'));
					foreach($report_data['details'][$key][$dkey] as $ekey=>$erow)
					{
						$subcategory_data[$key][$dkey][] = array(
							array('data'=>lang('reports_subcategory').': '.$erow['subcategory'], 'align'=>'right'),
							array('data'=>'-', 'align' => 'left'),
							array('data'=>$erow['subcategory'].' Total:', 'align'=>'right'),
							//array('data'=>'', 'align'=>'right'),
							//array('data'=>'', 'align'=>'right'),
							array('data'=>('-'), 'align'=>'right'),
							array('data'=>to_currency($erow['inventory_value']), 'align' => 'right'),
							array('data'=>to_currency($erow['potential_value']), 'align' => 'right'));
						foreach($report_data['items'][$key][$dkey][$ekey] as $irow)
						{
							$item_data[$key][$dkey][$ekey][] = array(
								array('data'=>$irow['name']!='' ? $irow['name'] : '-', 'align' => 'center'),
								array('data'=>$irow['item_number']!=''?$irow['item_number']:'-', 'align' => 'left'),
								//array('data'=>'', 'align'=>'right'), array('data'=>'', 'align'=>'left'),
								array('data'=>($irow['description']!=''?$irow['description'] : '-'), 'align'=>'left'),
								array('data'=>($irow['quantity']), 'align' => 'right'),
								array('data'=>to_currency($irow['cost_price']), 'align'=>'right'),
								array('data'=>to_currency($irow['unit_price']), 'align' => 'right'));
						}
					}
				}
			}
		}
		else
		{
			//echo 'here';
			foreach($report_data['dept_summary'] as $key=>$row)
			{
				$department_data[] = array(
					array('data'=>($row['department'] != '')?$row['department']:'Department not specified', 'align' => 'left'),
					array('data'=>'', 'align'=>'right'),
					array('data'=>''),
					array('data'=>''),
					array('data'=>to_currency($row['inventory_value']), 'align' => 'right'),
					array('data'=>to_currency($row['potential_value']), 'align' => 'right'));
				foreach($report_data['summary'][$key] as $dkey=>$drow)
				{
					$category_data[$key][] = array(
						array('data'=>$drow['category'], 'align' => 'left'),
						array('data'=>''),
						array('data'=>''),
						array('data'=>''),
						array('data'=>to_currency($drow['inventory_value']), 'align' => 'right'),
						array('data'=>to_currency($drow['potential_value']), 'align' => 'right'));
					foreach($report_data['details'][$key][$dkey] as $ekey=>$erow)
					{
						$subcategory_data[$key][$dkey][] = array(
							array('data'=>$erow['subcategory'], 'align' => 'left'),
							array('data'=>''),
							array('data'=>''),
							array('data'=>''),
							array('data'=>to_currency($erow['inventory_value']), 'align' => 'right'),
							array('data'=>to_currency($erow['potential_value']), 'align' => 'right'));
						foreach($report_data['items'][$key][$dkey][$ekey] as $irow)
						{
							$item_data[$key][$dkey][$ekey][] = array(
								array('data'=>($irow['name'] != '')?$irow['name']:'-----', 'align' => 'left'),
								array('data'=>$irow['item_number'], 'align'=>'left'),
								array('data'=>($irow['description']), 'align' => 'left'),
								array('data'=>($irow['quantity']), 'align' => 'right'),
								array('data'=>to_currency($irow['cost_price']), 'align' => 'right'),
								array('data'=>to_currency($irow['unit_price']), 'align' => 'right'));
						}
					}
				}
			}
		}

		/*$data = array(
			"title" => lang('reports_inventory_summary_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel
		);*/
		$data = array(
			"title" =>lang('reports_inventory_summary_report'),
			"subtitle" => date('Y-m-d'),
			"headers" => $model->getDataColumns(),
			"data" => array('dep'=>$department_data,'cat'=>$category_data,'subcat'=>$subcategory_data,'items'=>$item_data),
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel == 2 ? 3 : $export_excel
		);
		//$this->load->view("reports/tabular",$data);
		if ($export_excel == 2)
		{
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);

		    $html = $this->load->view("reports/tabular_inventory", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
		{
			$this->load->view("reports/tabular_inventory",$data);
		}
	}


	function invoices($date = false, $export_excel = 0)
	{
		$this->load->model('reports/Summary_invoices');

		$start_date = date('Y-m-01', strtotime($date));
		$end_date = date("Y-m-d", strtotime($start_date. "+ 1 month"));
		$model = $this->Summary_invoices;
		$tabular_data = array();

		$model->setParams(
			array(
				'start_date'=>$start_date,
				'end_date'=>$end_date,
				'export_excel'=>$export_excel
			)
		);

		$report_data = $model->getData();

		foreach($report_data as $row)
		{
			$tabular_data[] = array(
				array('data'=>($export_excel == 0 ? anchor("customers/load_sales_report_invoice/{$row['invoice_id']}/0/width~800", $row['invoice_number'], array('class'=>'colbox')) : $row['invoice_number']), 'align'=> 'left'),
				array('data'=>date('Y-m-d', strtotime($row['date'])), 'align'=> 'left'),
				array('data'=>$row['customer_name'], 'align'=> 'left'),
				array('data'=>to_currency($row['total']), 'align'=> 'right'),
				array('data'=>to_currency($row['paid']), 'align'=> 'right'),
				array('data'=>to_currency($row['due']), 'align'=> 'right')
			);
		}

		$data = array(
			"title" => lang('reports_invoice_summary_report'),
			"subtitle" => $start_date = date('m/Y', strtotime($date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel,
			"is_invoice_report"=>TRUE
		);
		if ($export_excel == 2)
		{
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);

		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		} else {
			$this->load->view("reports/tabular",$data);
		}
	}

	function summary_giftcards($export_excel = 0)
	{
		$this->load->model('reports/Summary_giftcards');
		$model = $this->Summary_giftcards;
		$tabular_data = array();
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['giftcard_number'], 'align'=> 'left'),array('data'=>to_currency($row['value']), 'align'=> 'left'), array('data'=>$row['customer_name'], 'align'=> 'left'));
		}

		$data = array(
			"title" => lang('reports_giftcard_summary_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel
		);

		if ($export_excel == 2)
		{
			$this->load->library('pdf_html');
			$pdf=new PDF_HTML();
		    $pdf->SetMargins(10, 10);
			$pdf->SetFont('Arial','',8);
		    $pdf->AddPage();
		    $pdf->SetAutoPageBreak('on', 10);

		    $html = $this->load->view("reports/tabular", $data, true);
		    $pdf->WriteHTML($html);
		    $pdf->Output();
		}
		else
		{
			$this->load->view("reports/tabular",$data);
		}
	}

	function summary_account_balances($export_excel = 0)
	{
		$this->load->model('reports/Summary_account_balances');
		$model = $this->Summary_account_balances;
		$tabular_data = array();
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			if (number_format(strip_tags(str_replace('$', '', $row['account_balance'])), 2) != number_format(0, 2) || number_format(strip_tags(str_replace('$', '', $row['member_balance'])), 2) != number_format(0,2))
				$tabular_data[] = array(array('data'=>$row['customer_name'], 'align'=> 'left'),array('data'=>$row['account_balance'], 'align'=>'right'),array('data'=>$row['member_balance'], 'align'=>'right'));
		}

		$data = array(
			"title" => lang('reports_account_balance_summary_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel
		);

		$this->load->view("reports/tabular",$data);
	}
	function detailed_account_transactions($date, $export_excel = 0)
	{
		$this->load->model('reports/Detailed_account_transactions');
		$model = $this->Detailed_account_transactions;
		$tabular_data = array();
		$model->setParams(
			array(
				'date'=>$date,
				'export_excel'=>$export_excel
			)
		);
		$report_data = $model->getData();

		// print_r($report_data);
		// return;

		$customer_credit_nickname = $this->config->item('customer_credit_nickname') ? $this->config->item('customer_credit_nickname') : 'Customer Credit';
		$member_balance_nickname = $this->config->item('member_balance_nickname') ? $this->config->item('member_balance_nickname') : 'Member Balance';
		$tabular_data[] = array(array('data'=>'<b>'.$this->config->item('customer_credit_nickname').' Transactions</b>', 'align'=> 'left'),array('data'=>'', 'align'=>'right'),array('data'=>'', 'align'=>'right'),array('data'=>'', 'align'=>'right'));
		foreach($report_data['account_transactions'] as $row)
		{
			$trans_amount = $row['trans_amount'] < 0 ? "<span class='red'>".to_currency($row['trans_amount']).'</span>' : to_currency($row['trans_amount']);
			$tabular_data[] = array(array('data'=>$row['last_name'].', '.$row['first_name'], 'align'=> 'left'),array('data'=>$trans_amount, 'align'=>'right'),array('data'=>date('Y-m-d g:ia', strtotime($row['trans_date'])), 'align'=>'right'),array('data'=>$row['trans_comment'], 'align'=>'right'));
		}

		$tabular_data[] = array(array('data'=>'<b>'.$member_balance_nickname.' Transactions</b>', 'align'=> 'left'),array('data'=>'', 'align'=>'right'),array('data'=>'', 'align'=>'right'),array('data'=>'', 'align'=>'right'));
		foreach($report_data['member_account_transactions'] as $row)
		{
			$trans_amount = $row['trans_amount'] < 0 ? "<span class='red'>".to_currency($row['trans_amount']).'</span>' : to_currency($row['trans_amount']);
			$tabular_data[] = array(array('data'=>$row['last_name'].', '.$row['first_name'], 'align'=> 'left'),array('data'=>$trans_amount, 'align'=>'right'),array('data'=>date('Y-m-d g:ia', strtotime($row['trans_date'])), 'align'=>'right'),array('data'=>$row['trans_comment'], 'align'=>'right'));
		}

		$data = array(
			"title" => lang('reports_account_balance_summary_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => array(),//$model->getSummaryData(array()),
			"export_excel" => $export_excel
		);

		$this->load->view("reports/tabular",$data);
	}

	function summary_rainchecks($export_excel = 0)
	{
		$this->load->model('reports/Summary_rainchecks');
		$model = $this->Summary_rainchecks;
		$tabular_data = array();
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>'RID '.$row['raincheck_number'], 'align'=> 'left'),array('data'=>$row['first_name'].' '.$row['last_name'], 'align'=> 'left'),array('data'=>$row['date_issued'], 'align'=> 'left'),array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>$row['status'], 'align'=> 'left'));
		}

		$data = array(
			"title" => lang('reports_raincheck_summary_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel
		);

		$this->load->view("reports/tabular",$data);
	}
	function summary_tips($start_date, $end_date, $employee_id, $export_excel=0)
	{
		$this->load->model('reports/Summary_tips');
		//cho 'hi there';
		$model = $this->Summary_tips;
		$model->setParams(
			array(
				'start_date'=>$start_date.' 00:00:00',
				'end_date'=>$end_date.' 23:59:59',
				'employee_id'=>$employee_id
			)
		);
		$tabular_data = array();
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>'POS '.$row['sale_id'], 'align'=> 'left'),array('data'=>$row['sale_time'], 'align'=> 'left'),array('data'=>to_currency($row['payment_amount']), 'align'=> 'right'), array('data'=>$row['last_name'].', '.$row['first_name'], 'align'=> 'left'));
		}

		$data = array(
			"title" => lang('reports_tips_summary_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel
		);

		$this->load->view("reports/tabular",$data);
	}

	function detailed_giftcards_input()
	{
		$data['specific_input_name'] = lang('reports_customer');

		$customers = array();
		foreach($this->Customer->get_all()->result() as $customer)
		{
			$customers[$customer->person_id] = $customer->first_name .' '.$customer->last_name;
		}
		$data['specific_input_data'] = $customers;
		$this->load->view("reports/detailed_giftcards_input",$data);
	}

	function detailed_giftcards($customer_id, $export_excel = 0)
	{
		$this->load->model('reports/Detailed_giftcards');
		$model = $this->Detailed_giftcards;
		//$model->setParams(array('customer_id' =>$customer_id));
		$model->setParams(array('giftcard_number' =>$customer_id));

		$this->Sale->create_sales_items_temp_table();

		$headers = $model->getDataColumns();
		$report_data = $model->getData();

		$summary_data = array();
		$details_data = array();

		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format(), strtotime($row['sale_date'])), 'align'=> 'left'), array('data'=>floor($row['items_purchased']), 'align'=> 'right'), array('data'=>$row['employee_name'], 'align'=> 'left'), array('data'=>to_currency($row['subtotal']), 'align'=> 'right'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>to_currency($row['tax']), 'align'=> 'right'),array('data'=>to_currency($row['profit']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'right'));

			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['serialnumber'], 'align'=> 'left'), array('data'=>$drow['description'], 'align'=> 'left'), array('data'=>floor($drow['quantity_purchased']), 'align'=> 'left'), array('data'=>to_currency($drow['subtotal']), 'align'=> 'right'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>to_currency($drow['tax']), 'align'=> 'right'),array('data'=>to_currency($drow['profit']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
			}
		}

		$customer_info = $this->Customer->get_info($customer_id);
		$data = array(
			"title" => $customer_info->first_name .' '. $customer_info->last_name.' '.lang('giftcards_giftcard'). ' '.lang('reports_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel,
			"empty_message" => 'No transactions have been made on this card.'
		);

		$this->load->view("reports/tabular_details",$data);
	}
	function detailed_punch_cards($customer_id, $export_excel = 0)
	{
		$this->load->model('reports/Detailed_punch_cards');
		$model = $this->Detailed_punch_cards;
		//$model->setParams(array('customer_id' =>$customer_id));
		$model->setParams(array('punch_card_number' =>$customer_id));

		$this->Sale->create_sales_items_temp_table();

		$headers = $model->getDataColumns();
		$report_data = $model->getData();

		$summary_data = array();
		$details_data = array();

		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format(), strtotime($row['sale_date'])), 'align'=> 'left'), array('data'=>floor($row['items_purchased']), 'align'=> 'right'), array('data'=>$row['employee_name'], 'align'=> 'left'), array('data'=>to_currency($row['subtotal']), 'align'=> 'right'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>to_currency($row['tax']), 'align'=> 'right'),array('data'=>to_currency($row['profit']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'right'));

			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['serialnumber'], 'align'=> 'left'), array('data'=>$drow['description'], 'align'=> 'left'), array('data'=>floor($drow['quantity_purchased']), 'align'=> 'left'), array('data'=>to_currency($drow['subtotal']), 'align'=> 'right'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>to_currency($drow['tax']), 'align'=> 'right'),array('data'=>to_currency($drow['profit']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
			}
		}

		$customer_info = $this->Customer->get_info($customer_id);
		$data = array(
			"title" => $customer_info->first_name .' '. $customer_info->last_name.' '.lang('giftcards_giftcard'). ' '.lang('reports_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel,
			"empty_message" => 'No transactions have been made on this card.'
		);

		$this->load->view("reports/tabular_details",$data);
	}

}
?>