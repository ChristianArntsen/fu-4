<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');

		$this->upload_path = FCPATH.'archives/images/'.$this->session->userdata('course_id').'/default';
		$this->upload_path_thumb = FCPATH.'archives/images/'.$this->session->userdata('course_id').'/thumbnails';
		$this->upload_path_preview = FCPATH.'archives/images/'.$this->session->userdata('course_id').'/previews';

		$this->upload_url = 'archives/images/'.$this->session->userdata('course_id').'/default';
		$this->upload_url_thumb = 'archives/images/'.$this->session->userdata('course_id').'/thumbnails';
		$this->upload_url_preview = 'archives/images/'.$this->session->userdata('course_id').'/previews';
	}

  	public function save_file($image_id = null, $data)
	{
		$data = elements(array('course_id', 'label', 'module', 'filename', 'width', 'height', 'filesize', 'date_created', 'saved'), $data);

		if(!empty($image_id)){
			foreach($data as $key => $val){
				if($val === false){ unset($data[$key]); }
			}
			$success = $this->db->update('images', $data, 'image_id = '.(int) $image_id);
		}else{
			$success = $this->db->insert('images', $data);
			$image_id = $this->db->insert_id();
		}

		if($success){
			return $image_id;
		}else{
			return false;
		}
	}

	public function get_files($module = '')
	{
		if ($module != ''){
			$this->db->where('module', $module);
		}

		return $this->db->select()
			->from('images')
			->where('course_id', $this->session->userdata('course_id'))
			->where('deleted', 0)
			->where('saved', 1)
			->get()
			->result_array();
	}

	public function search($query = '', $module = '')
	{
		if ($module != ''){
			$this->db->where('module', $module);
		}

		$words = explode(' ',$query);
		if(!empty($query)){
			$regex = implode('|', $words);
			$this->db->where("label REGEXP", $regex);
		}

		$results = $this->db->select()
			->from('images')
			->where('course_id', $this->session->userdata('course_id'))
			->where('deleted', 0)
			->where('saved', 1)
			->get()
			->result_array();

		return $results;
	}

	public function delete_file($file_id)
	{
		$file = $this->get_file($file_id);
		if (!$this->db->where('image_id', $file_id)->where('course_id', $this->session->userdata('course_id'))->delete('images'))
		{
			return false;
		}

		unlink($this->upload_path.'/'.$file['filename']);
		unlink($this->upload_path_thumb.'/'.$file['filename']);
		unlink($this->upload_path_preview.'/'.$file['filename']);
		return true;
	}

	public function get_file($file_id)
	{
		$query = $this->db->select()
			->from('images')
			->where('image_id', $file_id)
			->where('course_id', $this->session->userdata('course_id'))
			->where('deleted', 0)
			->get();

		if($query->num_rows() <= 0){
			$row['image_id'] = 0;
			$row['url'] = base_url().'images/no-image.png';
			$row['thumb_url'] = base_url().'images/no-image.png';
		}else{
			$row = $query->row_array();
			$row['url'] = $this->upload_url .'/'.$row['filename'].'?ts='.strtotime($row['date_updated']);
			$row['thumb_url'] = $this->upload_url_thumb .'/'.$row['filename'].'?ts='.strtotime($row['date_updated']);
		}

		return $row;
	}

	public function get_url($image_id = false){
		$url = $this->upload_url;

		if(!empty($image_id)){
			$this->db->select('filename, date_updated');
			$query = $this->db->get_where('images', array('course_id'=>$this->session->userdata('course_id'), 'image_id'=>$image_id, 'deleted'=>0,'saved'=>1));

			if($query->num_rows() <= 0){
				$url = base_url().'images/no-image.png';
			}else{
				$row = $query->row_array();
				$url .= '/'.$row['filename'].'?ts='.strtotime($row['date_updated']);
			}
		}else{
			$url = base_url().'images/no-image.png';
		}

		return $url;
	}

	public function get_thumb_url($image_id = false, $type = false){
		$url = $this->upload_url_thumb;

		if(!empty($image_id)){
			$this->db->select('filename, date_updated');
			$query = $this->db->get_where('images', array('course_id'=>$this->session->userdata('course_id'), 'image_id'=>$image_id, 'deleted'=>0, 'saved'=>1));

			if($query->num_rows() <= 0){
				$url = base_url().'images/no-image.png';
			}else{
				$row = $query->row_array();
				$url .= '/'.$row['filename'].'?ts='.strtotime($row['date_updated']);
			}
		}else if ($type == 'person'){
			$url = base_url().'images/profiles/profile_picture.png';
		}else{
			$url = base_url().'images/no-image.png';
		}

		return $url;
	}
}