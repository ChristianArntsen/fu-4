<?php
class Quickbutton extends CI_Model
{
	/*
	Determines if a given quickbutton_id is a quickbutton
	*/
	function exists($quickbutton_id)
	{
		$this->db->from('quickbuttons');
		$this->db->where("quickbutton_id", $quickbutton_id);
		$this->db->where("course_id", $this->session->userdata('course_id'));
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	
	/*
	Gets information about a particular quickbutton
	*/
	function get_info($quickbutton_id)
	{
		$this->db->from('quickbuttons');
		$this->db->where("quickbutton_id", $quickbutton_id);
		$this->db->where("course_id", $this->session->userdata('course_id'));
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('quickbuttons');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}

	/*
	Get the items associated with a quickbutton 
	*/
	function get_items($quickbutton_id)
	{
		$this->db->select('quickbutton_items.item_id, quickbutton_items.item_kit_id, items.name AS i_name, item_kits.name AS ik_name');
		$this->db->from("quickbutton_items");
		$this->db->join('items', 'quickbutton_items.item_id = items.item_id', 'left outer');
		$this->db->join('item_kits', 'quickbutton_items.item_kit_id = item_kits.item_kit_id', 'left outer');
		$this->db->where("quickbutton_id", $quickbutton_id);
		$this->db->order_by('order');
		return $this->db->get()->result_array();
	}
	/*
	Get all quickbuttons with associated items 
	*/
	function get_all($quickbutton_id = '')
	{
		$quickbuttons = array();
		$this->db->select('quickbuttons.display_name, quickbuttons.quickbutton_id, quickbuttons.tab, quickbuttons.position, quickbutton_items.item_id, quickbutton_items.item_kit_id, items.name as item_name, item_kits.name as item_kit_name');
		$this->db->from('quickbuttons');
		$this->db->where('quickbuttons.course_id', $this->session->userdata('course_id'));
		if ($quickbutton_id != '')
			$this->db->where('quickbuttons.quickbutton_id', $quickbutton_id);
		$this->db->join('quickbutton_items', 'quickbuttons.quickbutton_id = quickbutton_items.quickbutton_id');
		$this->db->join('items', 'quickbutton_items.item_id = items.item_id', 'left outer');
		$this->db->join('item_kits', 'quickbutton_items.item_kit_id = item_kits.item_kit_id', 'left outer');
		$this->db->order_by('tab, position, order');
		$results = $this->db->get();
		//echo $this->db->last_query();
		$results = $results->result_array();
		$green_fee_types = ($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types());
		//echo 'green fee type';
		//print_r($green_fee_types);
		foreach ($results as $result)
		{
			$item_id_pieces = explode('_', $result['item_id']);
			$green_fee_type = (count($item_id_pieces) > 1)?' '.$green_fee_types['price_category_'.$item_id_pieces[1]]:'';
			if (!isset($quickbuttons[$result['quickbutton_id']]['items']))
				$quickbuttons[$result['quickbutton_id']]['items'] = array();
			$quickbuttons[$result['quickbutton_id']]['name'] = $result['display_name'];
			$quickbuttons[$result['quickbutton_id']]['tab'] = $result['tab'];
			$quickbuttons[$result['quickbutton_id']]['position'] = $result['position'];
			if ($result['item_kit_id'])
				$quickbuttons[$result['quickbutton_id']]['items']["KIT ".$result['item_kit_id']] = $result['item_kit_name'];
			else 
				$quickbuttons[$result['quickbutton_id']]['items'][$result['item_id']] = $result['item_name'].$green_fee_type;
		}
		return $quickbuttons;
		//echo $this->db->last_query();
	}
/*
	Inserts or updates an item
	*/
	function save(&$quickbutton_data,$quickbutton_id=false)
	{
		if (!$quickbutton_id or !$this->exists($quickbutton_id))
		{
			if($this->db->insert('quickbuttons',$quickbutton_data))
			{
				$quickbutton_data['quickbutton_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('quickbutton_id', $quickbutton_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->update('quickbuttons',$quickbutton_data);
	}
	function save_items($data)
	{
		return $this->db->insert_batch('quickbutton_items', $data);
	}
	function save_positions($positions)
	{
		foreach ($positions as $position => $id)
		{
			$this->db->where('quickbutton_id', substr($id, 19));
			$this->db->update('quickbuttons', array('position'=>$position));
		}	
	}
	function delete_items($quickbutton_id)
	{
		$this->db->where("quickbutton_id", $quickbutton_id);
		return $this->db->delete('quickbutton_items');
	}
	function build($quickbutton_id, $quickbutton)
	{
		//print_r($quickbutton);
		$quickbutton_html = "<li id='quickbutton_holder_$quickbutton_id' class='ui-state-default'>";
		if (count($quickbutton['items']) > 1)
		{
			$quickbutton_html .= "<a class='quickbutton_menu new_quickbutton' href='index.php/sales' quickbutton_id='{$quickbutton_id}'>".character_limiter($quickbutton['name'],22)."</a>";
			$quickbutton_html .= "<div id='menu_{$quickbutton_id}' class='quickbutton_menu_box'>";
				foreach($quickbutton['items'] as $item_id => $item_name)
					$quickbutton_html .= "<a class='quickbutton new_quickbutton' href='index.php/sales' item_id='{$item_id}'>".character_limiter($item_name,22)."</a>";
			$quickbutton_html .= "</div>";
		}
		else
		{
			$quickbutton_html .= "<a class='quickbutton new_quickbutton' href='index.php/sales' item_id='";
			foreach($quickbutton['items'] as $item_id => $item_name)
				$quickbutton_html .= $item_id;
			$quickbutton_html .= "' quickbutton_id='{$quickbutton_id}'>".character_limiter($quickbutton['name'],22)."</a>";
		}
		$quickbutton_html .= "</li>";
		return $quickbutton_html;
	}
	/*
	Delete quickbutton and associated items
	*/
	function delete($quickbutton_id)
	{
		$tables = array('quickbuttons', 'quickbutton_items');
		$this->db->where('quickbutton_id', $quickbutton_id);
		$this->db->delete($tables);
	}
}
