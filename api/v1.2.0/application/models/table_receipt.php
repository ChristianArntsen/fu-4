<?php
class Table_Receipt extends CI_Model
{
	function __construct(){
		parent::__construct();
		$this->load->library('sale_lib');
	}

	// Check if current user has permission to access a sale
	function has_sale_access($sale_id)
	{
		$this->db->select('table_id');
		$result = $this->db->get_where('foreup_tables', array('sale_id'=>(int) $sale_id, 'course_id'=>$this->session->userdata('course_id')));

		if($result->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function receipt_exists($sale_id, $receipt_id){
		$this->db->select('receipt_id');
		$query = $this->db->get_where("table_receipts", array('receipt_id'=>$receipt_id, 'sale_id'=>$sale_id));

		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function is_on_receipt($sale_id, $line){
		$this->db->select('receipt_id');
		$query = $this->db->get_where("table_receipt_items", array('line'=>$line, 'sale_id'=>$sale_id));

		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function mark_paid($sale_id, $receipt_id){
		$query = $this->db->query("UPDATE foreup_table_receipts
			SET date_paid = NOW(),
				status = 'complete'
			WHERE sale_id = ".(int) $sale_id."
				AND receipt_id = ". (int) $receipt_id);
		return true;
	}

	function mark_unpaid($sale_id, $receipt_id){
		$query = $this->db->query("UPDATE foreup_table_receipts
			SET date_paid = NULL,
				status = 'pending'
			WHERE sale_id = ".(int) $sale_id."
				AND receipt_id = ". (int) $receipt_id);
		return true;
	}

	function complete($sale_id, $receipt_id){
		$success = $this->db->update('table_receipts',
			array('status' => 'complete', 'date_paid' => date('Y-m-d h:i:s')),
			array('sale_id' => $sale_id, 'receipt_id' => $receipt_id)
		);

		return $success;
	}

	function can_delete($sale_id, $receipt_id){

		if(empty($sale_id) || empty($receipt_id)){
			return false;
		}

		$query = $this->db->query("SELECT COUNT(receipt_items.line) AS count, receipt_items.line
			FROM ".$this->db->dbprefix('table_receipt_items')." AS receipt_items
			INNER JOIN (SELECT ri.line
				FROM ".$this->db->dbprefix('table_receipt_items')." AS ri
				WHERE ri.sale_id = ".(int) $sale_id."
				AND ri.receipt_id = ".(int) $receipt_id.") AS item_filter
				ON item_filter.line = receipt_items.line
			WHERE receipt_items.sale_id = ".(int) $sale_id."
			GROUP BY receipt_items.line");
		$rows = $query->result_array();

		// If receipt is empty, its ok to delete
		if($query->num_rows == 0){
			return true;
		}

		// If receipt contains items that do NOT belong on other receipts
		// disallow deletion
		foreach($rows as $row){
			if($row['count'] <= 1){
				return false;
			}
		}

		return true;
	}

	// Check if there is at least 1 open receipt to add new items to.
	// If there are no receipts avaialable, create new one
	function get_available_receipt($sale_id){
		$this->db->select('receipt_id');
		$this->db->where('status', 'pending');
		$this->db->where('sale_id', $sale_id);
		$this->db->from('table_receipts');
		$query = $this->db->get();

		if($query->num_rows == 0){
			$receipt_id = $this->save($sale_id);
		}else{
			$row = $query->row_array();
			$receipt_id = (int) $row['receipt_id'];
		}

		return $receipt_id;
	}

	function get_next_receipt_num($sale_id){
		$query = $this->db->query("SELECT MAX(receipt_id) AS receipt_id
			FROM ".$this->db->dbprefix('table_receipts')."
			WHERE sale_id = ".(int) $sale_id."
			GROUP BY sale_id");
		$row = $query->row_array();

		return (int) $row['receipt_id'] + 1;
	}

	// Saves a new receipt
	function save($sale_id, $receipt_id = null, $items = null){

		if(!$this->has_sale_access($sale_id)){
			return false;
		}

		if(empty($receipt_id)){
			$receipt_id = $this->get_next_receipt_num($sale_id);
		}

		if(!$this->receipt_exists($sale_id, $receipt_id)){
			$this->db->insert('table_receipts', array('sale_id'=>$sale_id, 'receipt_id'=>$receipt_id));
		}

		// If items were passed as well, insert those into database
		if(!empty($items)){
			$item_batch = array();

			// Conform the item array for DB insertion
			foreach($items as $item){
				$item_batch[] = array('receipt_id'=>$receipt_id, 'sale_id'=>$sale_id, 'item_id'=>$item['item_id'], 'line'=>$item['line']);
			}
			$this->db->insert_batch('table_receipt_items', $item_batch);
		}

		return $receipt_id;
	}

	// NEW Get function that utilizes logic from Table model to retrieve
	// needed items, instead of repeating total calcaulations, etc.
	function get($sale_id, $receipt_filter_id = false){

		$this->load->model('Table');
		$side_types = array('soups', 'salads', 'sides');

		// Retrieve all items from table (sale_id)
		$items = $this->Table->get_items($sale_id);
		$cart_items = array();

		// Organize table items by line
		foreach($items as $item){
			$cart_items[$item['line']] = $item;
		}
		unset($items);

		// Retrieve all payments made for this table
		$payment_rows = $this->Table->get_payments($sale_id, $receipt_filter_id);
		$payments = array();
		foreach($payment_rows as $row){
			$receipt_id = $row['receipt_id'];
			unset($row['sale_id'], $row['receipt_id']);
			$payments[$receipt_id][$row['payment_type']] = array(
				'type' => $row['payment_type'],
				'amount' => $row['payment_amount'],
				'credit_card_invoice_id' => $row['credit_card_invoice_id']
			);
		}
		unset($payment_rows);

		// Retrieve all receipts with items associated
		// Even if only retrieving single receipt, it is necessary to
		// retrieve all receipts to calculate which items are split
		$this->db->select('r.receipt_id, r.status, r.date_created AS receipt_date, r.date_paid,
			r.sale_id, ri.line AS item_line', false);
		$this->db->from('table_receipts AS r');
		$this->db->join('table_receipt_items AS ri', 'ri.receipt_id = r.receipt_id AND ri.sale_id = r.sale_id', 'left');
		$this->db->where('r.sale_id', $sale_id);
		$this->db->order_by('r.receipt_id ASC, ri.line ASC');
		$query = $this->db->get();

		$rows = $query->result_array();
		$receipts = array();
		$item_counts = array();

		// Loop through receipt items, create multi-dimensional array by receipt
		foreach($rows as $item){
			$receipt_id = $item['receipt_id'];
			$receipts[$receipt_id]['date_created'] = $item['receipt_date'];
			$receipts[$receipt_id]['sale_id'] = $item['sale_id'];
			$receipts[$receipt_id]['receipt_id'] = $receipt_id;
			$receipts[$receipt_id]['date_paid'] = $item['date_paid'];
			$receipts[$receipt_id]['status'] = $item['status'];

			if(isset($payments[$receipt_id])){
				$receipts[$receipt_id]['payments'] = array_values($payments[$receipt_id]);
			}

			if(!isset($receipts[$receipt_id]['total'])){
				$receipts[$receipt_id]['total'] = 0;
			}
			$receipts[$receipt_id]['total_due'] = 0;
			$receipts[$receipt_id]['tax'] = 0;

			// If row is an item, add the item to the proper receipt
			if(!empty($item['item_line'])){
				unset($item['receipt_id'], $item['receipt_date'], $item['sale_id']);

				if(empty($cart_items[$item['item_line']])){
					continue;
				}

				$cart_item = $cart_items[$item['item_line']];
				$item = $cart_item;
				$receipts[$receipt_id]['items'][] = $item;

				if(!isset($item_counts[$item['line']])){
					$item_counts[$item['line']] = 1;
				}else{
					$item_counts[$item['line']]++;
				}
			}
		}

		// Loop through each receipt and it's items, calculate actual prices
		// based on how receipt is split up
		foreach($receipts as $receipt_key => $receipt){

			foreach($receipt['items'] as $item_key => $item){

				$divisor = $item_counts[$item['line']];
				$item['num_splits'] = $divisor;
				$this->calculate_split_prices($divisor, $receipts[$receipt_key]['items'][$item_key]);
				$item = $receipts[$receipt_key]['items'][$item_key];

				// If item has sides, calculate the split prices of each side
				// Split prices will be used when receipt is inserted into sales table
				$sidesTotal = 0;
				$sidesSplitTotal = 0;
				$sidesSplitSubtotal = 0;
				$sidesSplitTax = 0;
				foreach($side_types as $side_type){
					if(!empty($item[$side_type])){
						foreach($item[$side_type] as $side_key => $side){
							$this->calculate_split_prices($divisor, $receipts[$receipt_key]['items'][$item_key][$side_type][$side_key]);
							$side = $receipts[$receipt_key]['items'][$item_key][$side_type][$side_key];
							$sidesTotal += $side['total'];
							$sidesSplitTotal += $side['split_total'];
							$sidesSplitSubTotal += $side['split_subtotal'];
							$sidesSplitTax += $side['split_tax'];
						}
					}
				}

				// Add item + sides total to final receipt total
				$receipts[$receipt_key]['subtotal'] += $item['split_subtotal'] + $sidesSplitSubtotal;
				$receipts[$receipt_key]['tax'] += $item['split_tax'] + $sidesSplitTax;
				$receipts[$receipt_key]['total'] += $item['split_total'] + $sidesSplitTotal;
			}
			// Round receipt totals for good measure
			$receipts[$receipt_key]['subtotal'] = round($receipts[$receipt_key]['subtotal'], 2);
			$receipts[$receipt_key]['tax'] = round($receipts[$receipt_key]['tax'], 2);
			$receipts[$receipt_key]['total'] = round($receipts[$receipt_key]['total'], 2);

			// Calculate the amount due on the receipt
			$receipts[$receipt_key]['total_paid'] = 0;
			if(!empty($payments[$receipt_key])){
				foreach($payments[$receipt_key] as $payment){
					$receipts[$receipt_key]['total_paid'] += (float) round($payment['amount'], 2);
				}
			}
			$receipts[$receipt_key]['total_due'] = round($receipts[$receipt_key]['total'] - $receipts[$receipt_key]['total_paid'], 2);
		}

		if(!empty($receipt_filter_id)){
			return $receipts[$receipt_filter_id];
		}

		return array_values($receipts);
	}

	function calculate_split_prices($divisor, &$item){
		$item['split_price'] = round($item['price'] / $divisor, 2);
		$item['split_subtotal'] = round($item['subtotal'] / $divisor, 2);
		$item['split_tax'] = round($item['tax'] / $divisor, 2);
		$item['split_total'] = round($item['total'] / $divisor, 2);
		$item['num_splits'] = $divisor;

		return $item;
	}

	function get_items($sale_id, $receipt_id = false){

		if(empty($receipt_id)){
			return false;
		}
		$this->load->model('Table');

		$this->db->select('line', false);
		$this->db->from('table_receipt_items');
		$this->db->where('sale_id', $sale_id);
		$this->db->where('receipt_id', $receipt_id);
		$query = $this->db->get();

		$rows = $query->result_array();
		$lines = array();

		if(empty($rows)){
			return array();
		}

		foreach($rows as $row){
			$lines[] = $row['line'];
		}

		// Retrieve specific items from cart
		$items = $this->Table->get_items($sale_id, $lines);
		return $items;
	}

	function can_delete_item($sale_id, $line){
		$this->db->select('line', false);
		$this->db->from('table_receipt_items');
		$this->db->where('sale_id', $sale_id);
		$this->db->where('line', $line);
		$query = $this->db->get();

		if($query->num_rows() > 1){
			return true;
		}else{
			return false;
		}
	}

	// Retrieve all receipts for a table including items connected to each
	function get_all($sale_id, $filter_receipt_id = false)
	{
		if(!$this->has_sale_access($sale_id)){
			return false;
		}
		$cart_items = $this->sale_lib->get_cart();
		$receipt_payments = $this->sale_lib->get_receipt_payments_total();

		$this->db->select('r.receipt_id, r.date_created AS receipt_date, r.date_paid,
			r.sale_id, ri.line AS item_line,i.item_id, i.name AS item_name', false);
		$this->db->from('table_receipts AS r');
		$this->db->join('table_receipt_items AS ri', 'ri.receipt_id = r.receipt_id AND ri.sale_id = r.sale_id', 'left');
		$this->db->join('items AS i', 'i.item_id = ri.item_id', 'left');
		$this->db->where('r.sale_id', $sale_id);
		$this->db->order_by('ri.receipt_id ASC, ri.line ASC');
		$query = $this->db->get();

		$rows = $query->result_array();
		$receipts = array();
		$item_counts = array();

		// Loop through receipt items, create multi-dimensional array by receipt
		foreach($rows as $item){
			$receipt_id = $item['receipt_id'];
			$receipts[$receipt_id]['date_created'] = $item['receipt_date'];
			$receipts[$receipt_id]['sale_id'] = $item['sale_id'];
			$receipts[$receipt_id]['receipt_id'] = $receipt_id;
			$receipts[$receipt_id]['date_paid'] = $item['date_paid'];

			if(isset($receipt_payments[$item['receipt_id']])){
				$receipts[$receipt_id]['total_paid'] = (float) $receipt_payments[$item['receipt_id']];
			}else{
				$receipts[$receipt_id]['total_paid'] = 0;
			}

			if(!isset($receipts[$receipt_id]['total'])){
				$receipts[$receipt_id]['total'] = 0;
			}
			$receipts[$receipt_id]['total_due'] = 0;
			$receipts[$receipt_id]['tax'] = 0;

			// If row is an item, add the item to the proper receipt
			if(!empty($item['item_id'])){
				unset($item['receipt_id'], $item['receipt_date'],$item['sale_id']);
				$cart_item = $cart_items[$item['item_line']];
				$item = $cart_item;
				$receipts[$receipt_id]['items'][] = $item;

				if(!isset($item_counts[$item['line'].'_'.$item['item_id']])){
					$item_counts[$item['line'].'_'.$item['item_id']] = 1;
				}else{
					$item_counts[$item['line'].'_'.$item['item_id']] += 1;
				}
			}
		}

		// Loop through each receipt and it's items, calculate actual prices
		// based on how receipt is split up
		foreach($receipts as $receipt_key => $receipt){

			foreach($receipt['items'] as $item_key => $item){

				$divisor = $item_counts[$item['line'].'_'.$item['item_id']];
				$subtotal = $this->sale_lib->calculate_item_subtotal($item['price'] + $item['modifier_total'], $item['quantity'], $item['discount']);

				$split_price = round($item['price'] / $divisor, 2);
				$split_subtotal = round($subtotal / $divisor, 2);
				$split_tax = round($item['tax'] / $divisor, 2);
				$split_total = $split_subtotal + $split_tax;

				$receipts[$receipt_key]['items'][$item_key]['price'] = $split_price;
				$receipts[$receipt_key]['items'][$item_key]['subtotal'] = $split_subtotal;
				$receipts[$receipt_key]['items'][$item_key]['tax'] = $split_tax;
				$receipts[$receipt_key]['items'][$item_key]['total'] = $split_total;
				$receipts[$receipt_key]['items'][$item_key]['unsplit_total'] = $subtotal + $item['tax'];
				$items_total += $split_total;

				$receipts[$receipt_key]['subtotal'] += $split_subtotal;
				$receipts[$receipt_key]['tax'] += $split_tax;
				$receipts[$receipt_key]['total'] += $split_subtotal + $split_tax;
			}
			$receipts[$receipt_key]['total_due'] = $receipts[$receipt_key]['total'] - $receipts[$receipt_key]['total_paid'];
		}

		if(!empty($filter_receipt_id)){
			return $receipts[$filter_receipt_id];
		}
		return array_values($receipts);
	}

	// Adds a new item to a receipt
	function add_item($item_id, $line, $sale_id, $receipt_id){
		if(!$this->receipt_exists($sale_id, $receipt_id)){
			$this->save($sale_id, $receipt_id);
		}
		$this->db->query("INSERT IGNORE INTO foreup_table_receipt_items (sale_id, receipt_id, item_id, line)
			VALUES (".(int) $sale_id.",".(int) $receipt_id.",".(int) $item_id.",".(int) $line.")");
		return true;
	}

	// Removes an item from a receipt
	function delete_item($item_id, $line, $sale_id, $receipt_id = null){

		if(!$this->has_sale_access($sale_id)){
			return false;
		}
		$where['line'] = $line;
		//$where['item_id'] = $item_id; not needed
		$where['sale_id'] = $sale_id;

		if(!empty($receipt_id)){
			$where['receipt_id'] = $receipt_id;
		}
		$results = $this->db->delete('table_receipt_items', $where);
		return $results;
	}

	// Delete entire receipt including receipt items associated with it
	function delete($sale_id, $receipt_id = false){

		if(!$this->has_sale_access($sale_id)){
			return false;
		}
		if(empty($sale_id) || $receipt_id === false){
			return false;
		}

		$result = $this->db->query("DELETE r.*, ri.*
			FROM foreup_table_receipts AS r
			LEFT JOIN foreup_table_receipt_items AS ri
				ON ri.sale_id = r.sale_id
				AND ri.receipt_id = r.receipt_id
			WHERE r.sale_id = ".(int) $sale_id."
				AND r.receipt_id = ".(int) $receipt_id);

		return $result;
	}

	function is_paid($sale_id, $receipt_id = false){
		if(empty($sale_id)){
			return false;
		}

		$where = "r.status = 'pending' AND r.sale_id = ".(int) $sale_id;
		if(!empty($receipt_id)){
			$where .= " AND r.receipt_id = ".(int) $receipt_id;
		}

		// Select any/all receipts that have not been paid
		// Ignore receipts with no items
		$query = $this->db->query("SELECT r.receipt_id
			FROM foreup_table_receipts AS r
			INNER JOIN foreup_table_receipt_items AS i
				ON i.receipt_id = r.receipt_id
				AND i.sale_id = r.sale_id
			WHERE {$where}
			GROUP BY r.receipt_id");

		if($query->num_rows() > 0){
			return false;
		}

		return true;
	}
}
?>
