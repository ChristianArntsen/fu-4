<?php
require_once("report.php");
class Detailed_taxes extends Report
{
	function __construct()
	{
		parent::__construct();
	}

	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_sales_activity'), 'align'=>'left'), array('data'=>lang('reports_sales'), 'align'=>'right'));
	}

	public function getData(){

		$this->db->select('SUM(IF(tax != 0.00, total, 0)) AS taxable,
			SUM(IF(tax = 0.00, total, 0)) AS non_taxable,
			SUM(total) AS total,
			SUM(subtotal) AS subtotal,
			SUM(tax) AS tax', false);
		$this->db->from('sales_items_temp');

		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);

		$query = $this->db->get();
		$taxes_array = $query->row_array();

		$totals_data = array(
			'Non Txbl:' => array('percent'=>'Non Taxable:','tax'=>$taxes_array['non_taxable']),
			'Txbl:' => array('percent'=>'Taxable:','tax'=>$taxes_array['taxable']),
			'Tax:' => array('percent'=>'Tax:','tax'=>$taxes_array['tax']),
			'Subtotal:' => array('percent'=>'Subtotal:','tax'=>$taxes_array['subtotal']),
			'Total:' => array('percent'=>'Total:','tax'=>$taxes_array['total'])
		);

		return $totals_data;
	}
	/*
	public function getData()
	{
		$sum_tax = $rounding_error = $nontax_total = $taxable_total = $raw_total = 0;

		$this->db->select('total, tax, sale_id, item_id, item_kit_id, line');
		$this->db->from('sales_items_temp');

		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}

		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);

		$taxes_data = array();
		$my_data = $this->db->get();
		//echo $this->db->last_query();
		//print_r($my_data);
		foreach($my_data->result_array() as $row)
		{
			if ($row['item_id'])
			{
				$this->getTaxesForItems($row['sale_id'], $row['item_id'], $row['line'], $taxes_data);
			}
			else if ($row['item_kit_id'])
			{
				$this->getTaxesForItemKits($row['sale_id'], $row['item_kit_id'], $row['line'], $taxes_data);
			}
			else if ($row['invoice_id'])
			{
				$this->getTaxesForInvoices($row['sale_id'], $row['invoice_id'], $row['line'], $taxes_data);
			}
			$already_totaled[$row['sale_id']] = $row['total'];
			$raw_total += $row['total'];
			if ($row['tax'] != '0.00')
				$taxable_total += $row['total'];
			else
				$nontax_total += $row['total'];
		}
		//echo 'getting here';

		foreach($taxes_data as $totals)
		{
			$sum_tax += $totals['tax'];
		}
		$totals_data = array(
			'Non Txbl:' => array('percent'=>'Non Txbl:','tax'=>$nontax_total),
			'Txbl:' => array('percent'=>'Txbl:','tax'=>$taxable_total),
			'Tax:' => array('percent'=>'Tax:','tax'=>$sum_tax),
			//'Rounding Error:'=> array('percent'=>'Rounding Error:', 'tax'=>),
			'Subtotal:' => array('percent'=>'Subtotal:','tax'=>$raw_total),
			'Total:' => array('percent'=>'Total:','tax'=>$raw_total)

		);
			//$this->db->select('sum(total) as total, sum(tax) as tax');
				//echo 'tax: '.$sum_tax.' nontax total:'.$nontax_total.' taxable total: '.$taxable_total.' raw total: '.$raw_total;

		return $totals_data;
	}

	function getTaxesForItems($sale_id, $item_id, $line, &$taxes_data)
	{
		$query = $this->db->query("SELECT percent, cumulative, item_unit_price, item_cost_price, quantity_purchased, discount_percent FROM ".$this->db->dbprefix('sales_items_taxes').'
		JOIN '.$this->db->dbprefix('sales_items'). ' USING(sale_id, item_id, line) WHERE '.
		$this->db->dbprefix('sales_items_taxes').'.sale_id = '.$sale_id.' and '.
		$this->db->dbprefix('sales_items_taxes').'.item_id = '.$item_id.' and '.
		$this->db->dbprefix('sales_items_taxes').'.line = '.$line. ' ORDER BY cumulative');

		$tax_result = $query->result_array();
		for($k=0;$k<count($tax_result);$k++)
		{
			$row = $tax_result[$k];
			if ($row['cumulative'])
			{
				$previous_tax = $tax;
				$subtotal = ($row['item_unit_price']*$row['quantity_purchased']-$row['item_unit_price']*$row['quantity_purchased']*$row['discount_percent']/100);
				$tax = ($subtotal + $tax) * ($row['percent'] / 100);
			}
			else
			{
				$subtotal = ($row['item_unit_price']*$row['quantity_purchased']-$row['item_unit_price']*$row['quantity_purchased']*$row['discount_percent']/100);
				$tax = $subtotal * ($row['percent'] / 100);
			}

			if (empty($taxes_data[$row['percent']]))
			{
				$taxes_data[$row['percent']] = array('percent' => $row['percent'] . ' %', 'tax' => 0);
			}

			$taxes_data[$row['percent']]['tax'] += $tax;
		}

	}

	function getTaxesForItemKits($sale_id, $item_kit_id, $line, &$taxes_data)
	{
		$query = $this->db->query("SELECT percent, cumulative, item_kit_unit_price, item_kit_cost_price, quantity_purchased, discount_percent FROM ".$this->db->dbprefix('sales_item_kits_taxes').'
		JOIN '.$this->db->dbprefix('sales_item_kits'). ' USING(sale_id, item_kit_id, line) WHERE '.
		$this->db->dbprefix('sales_item_kits_taxes').'.sale_id = '.$sale_id.' and '.
		$this->db->dbprefix('sales_item_kits_taxes').'.item_kit_id = '.$item_kit_id.' and '.
		$this->db->dbprefix('sales_item_kits_taxes').'.line = '.$line. ' ORDER BY cumulative');

		$tax_result = $query->result_array();
		for($k=0;$k<count($tax_result);$k++)
		{
			$row = $tax_result[$k];
			if ($row['cumulative'])
			{
				$previous_tax = $tax;
				$subtotal = ($row['item_kit_unit_price']*$row['quantity_purchased']-$row['item_kit_unit_price']*$row['quantity_purchased']*$row['discount_percent']/100);
				$tax = ($subtotal + $tax) * ($row['percent'] / 100);
			}
			else
			{
				$subtotal = ($row['item_kit_unit_price']*$row['quantity_purchased']-$row['item_kit_unit_price']*$row['quantity_purchased']*$row['discount_percent']/100);
				$tax = $subtotal * ($row['percent'] / 100);
			}

			if (empty($taxes_data[$row['percent']]))
			{
				$taxes_data[$row['percent']] = array('percent' => $row['percent'] . ' %', 'tax' => 0);
			}

			$taxes_data[$row['percent']]['tax'] += $tax;
		}
	}
	function getTaxesForInvoices($sale_id, $invoice_id, $line, &$taxes_data)
	{
		$query = $this->db->query("SELECT percent, cumulative, invoice_unit_price, invoice_cost_price, quantity_purchased, discount_percent FROM ".$this->db->dbprefix('sales_invoices_taxes').'
		JOIN '.$this->db->dbprefix('sales_invoices'). ' USING(sale_id, invoice_id, line) WHERE '.
		$this->db->dbprefix('sales_invoices_taxes').'.sale_id = '.$sale_id.' and '.
		$this->db->dbprefix('sales_invoices_taxes').'.invoice_id = '.$invoice_id.' and '.
		$this->db->dbprefix('sales_invoices_taxes').'.line = '.$line. ' ORDER BY cumulative');

		$tax_result = $query->result_array();
		for($k=0;$k<count($tax_result);$k++)
		{
			$row = $tax_result[$k];
			if ($row['cumulative'])
			{
				$previous_tax = $tax;
				$subtotal = ($row['invoice_unit_price']*$row['quantity_purchased']-$row['invoice_unit_price']*$row['quantity_purchased']*$row['discount_percent']/100);
				$tax = ($subtotal + $tax) * ($row['percent'] / 100);
			}
			else
			{
				$subtotal = ($row['invoice_unit_price']*$row['quantity_purchased']-$row['invoice_unit_price']*$row['quantity_purchased']*$row['discount_percent']/100);
				$tax = $subtotal * ($row['percent'] / 100);
			}

			if (empty($taxes_data[$row['percent']]))
			{
				$taxes_data[$row['percent']] = array('percent' => $row['percent'] . ' %', 'tax' => 0);
			}

			$taxes_data[$row['percent']]['tax'] += $tax;
		}
	}
*/
	public function getSummaryData()
	{
		return;
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		$this->db->from('sales_items_temp');

		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}

		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		return $this->db->get()->row_array();
	}

}
?>