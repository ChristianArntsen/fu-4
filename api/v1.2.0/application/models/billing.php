<?php
class Billing extends CI_Model
{
	/*
	Determines if a given item_id is an item
	*/
	function exists($billing_id)
	{
		$this->db->from('billing');
		$this->db->where("billing_id = '$billing_id'");
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the items
	*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('billing');
		//$this->db->order_by("name", "asc");
		$this->db->where('deleted !=', 1);
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('course_id');
		return $this->db->get();
	}
	function count_all()
	{
		$this->db->from('teetimes_bartered');
		return $this->db->count_all_results();
	}
	function get_course_payments($course_id)
	{
		$this->db->from('billing_charge_items');
		$this->db->join('billing_charges', 'billing_charge_items.charge_id = billing_charges.charge_id');
		$this->db->join('billing_credit_cards', 'billing_credit_cards.credit_card_id = billing_charges.credit_card_id');
		$this->db->where('billing_credit_cards.course_id', $course_id);
		$this->db->order_by('date DESC, billing_charge_items.charge_id');
		//$result = $this->db->get();
		//echo $this->db->last_query();
		return $this->db->get()->result_array();
	}
	function get_info_by_teesheet_id($teesheet_id)
	{
		$this->db->from('billing');
		$this->db->where("teesheet_id = '$teesheet_id'");
	 	$this->db->limit(1);	
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('courses');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	function get_monthly_limits($course_id = '')
	{
		$this->db->from('billing');
		$this->db->select('sum(email_limit) AS email_limit, sum(text_limit) AS text_limit, sum(annual_amount) AS annual_amount, sum(monthly_amount) AS monthly_amount');
		$this->db->where('deleted', 0);
		$this->db->where('(email_limit > 0 OR text_limit > 0)');
		if ($course_id != '')
			$this->db->where('course_id', $course_id);
		
		$results = $this->db->get()->result_array();
		
		$data = array(
			'email_limit'=>$results[0]['email_limit'],
			'text_limit'=>$results[0]['text_limit'],
			'annual_amount'=>to_currency($results[0]['annual_amount']),
			'monthly_amount'=>to_currency($results[0]['monthly_amount'])
		);
		return $data;
	}	
	
	function get_teetime_stats()
	{
		$this->db->from('billing');
		$this->db->select('COUNT(billing_id) AS courses');
		$this->db->where('teetimes', 1);
		$this->db->where('deleted', 0);
		
		$teetime_course_count = $this->db->get()->result_array();
		
		$this->db->from('billing');
		$this->db->select('teetimes_daily, teetimes_weekly, start_date');
		$this->db->where('teetimes', 1);
		$this->db->where('deleted', 0);
		
		//Teetime trade data
		$ttd = $this->db->get()->result_array();
		$potential_teetimes = 0;
		foreach ($ttd as $td)
		{
			$now = time(); // or your date as well
			$date = strtotime($td['start_date']);
			$date_diff = $now - $date;
			$days = floor($date_diff/(60*60*24));
			
			$potential_teetimes += $days * $td['teetimes_daily'];
			$potential_teetimes += floor($days/7 * floor($td['teetimes_weekly']));
		}
		
		
		$this->db->from('teetimes_bartered');
		$this->db->select('COUNT(teetime_id) AS sold, SUM(auth_amount) AS teetime_revenue');
		$this->db->join('sales_payments_credit_cards', 'sales_payments_credit_cards.invoice = teetimes_bartered.invoice_id');
		
		$sold_teetimes = $this->db->get()->result_array();
		
		$data = array(
			'courses'=>$teetime_course_count[0]['courses'],
			'potential_teetimes'=>$potential_teetimes,
			'sold_teetimes'=>$sold_teetimes[0]['sold'],
			'revenue'=>to_currency($sold_teetimes[0]['teetime_revenue'])
		);
		
		return $data;
	}
	function get_teetime_course_stats()
	{
		$this->load->model('Communication');
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-'.$this->Communication->days_in_month(date('m')));
		$pm_start_date = date('Y-m-01', strtotime($start_date.' -1 month'));
		$pm_end_date = date('Y-m-'.$this->Communication->days_in_month(date('m', strtotime($start_date.' -1 month'))), strtotime($start_date.' -1 month'));
		
		$this->db->from('billing');
		$this->db->select('course_id, teetimes_daily, teetimes_weekly, start_date');
		$this->db->where('teetimes', 1);
		$this->db->where('deleted', 0);
		
		//Teetime trade data
		$ttd = $this->db->get()->result_array();
		//echo '<br/><br/>'.$this->db->last_query();
		//print_r($ttd);
		$potential_teetimes = 0;
		$return_array = array();
		foreach ($ttd as $td)
		{
			if (!isset($return_array[$td['course_id']]))
				$return_array[$td['course_id']] = $td;
			$now = strtotime(date('Y-m-d')); // or your date as well
			$date = strtotime($td['start_date']);
			$date_diff = $now - $date;
			$days = floor($date_diff/(60*60*24));
			
			$return_array[$td['course_id']]['potential_teetimes'] += $days * $td['teetimes_daily'];
			$return_array[$td['course_id']]['potential_teetimes'] += floor($days/7 * floor($td['teetimes_weekly']));
			if ($date < strtotime($end_date))
			{
				$date_diff = ($date > strtotime($start_date))?strtotime($end_date) - $date:strtotime($end_date) - strtotime($start_date);
				$days = floor($date_diff/(60*60*24));
			
				$return_array[$td['course_id']]['potential_this_month'] += $days * $td['teetimes_daily'];
				$return_array[$td['course_id']]['potential_this_month'] += floor($days/7 * floor($td['teetimes_weekly']));
			}
			if ($date < strtotime($pm_end_date))
			{
				$date_diff = ($date > strtotime($pm_start_date))?strtotime($pm_end_date) - $date:strtotime($pm_end_date) - strtotime($pm_start_date);
				$days = floor($date_diff/(60*60*24));
			
				$return_array[$td['course_id']]['potential_last_month'] += $days * $td['teetimes_daily'];
				$return_array[$td['course_id']]['potential_last_month'] += floor($days/7 * floor($td['teetimes_weekly']));
			}
		}
		
		
//		$this->db->from('teetimes_bartered');
//		$this->db->select('course_id, COUNT(teetime_id) AS sold, SUM(auth_amount) AS teetime_revenue');
//		$this->db->join('sales_payments_credit_cards', 'sales_payments_credit_cards.invoice = teetimes_bartered.invoice_id');
		
		$query = $this->db->query("SELECT foreup_courses.course_id, COUNT(teetime_id) AS sold, SUM(auth_amount) AS teetime_revenue,
			sum(IF (date_booked >= '$start_date' AND date_booked <= '$end_date', 1, 0 )) AS sold_this_month, 
			sum(IF (date_booked >= '$pm_start_date' AND date_booked <= '$pm_end_date', 1, 0 )) AS sold_last_month,
			sum(IF (date_booked >= '$start_date' AND date_booked <= '$end_date', auth_amount, 0 )) AS revenue_this_month, 
			sum(IF (date_booked >= '$pm_start_date' AND date_booked <= '$pm_end_date', auth_amount, 0 )) AS revenue_last_month 
			FROM foreup_teetimes_bartered
			JOIN foreup_sales_payments_credit_cards ON foreup_sales_payments_credit_cards.invoice = foreup_teetimes_bartered.invoice_id
			JOIN foreup_courses ON foreup_courses.course_id = foreup_sales_payments_credit_cards.course_id
			GROUP BY course_id
			ORDER BY foreup_courses.name
		");
		//echo $this->db->last_query();
		//$sold_array = array();
		foreach($query->result_array() as $result)
			if ($result['course_id'] > 0)
				$return_array[$result['course_id']] = (isset($return_array[$result['course_id']]))? array_merge($return_array[$result['course_id']], $result): $result;
		//echo '<br/>Return Array<br/>';
		//print_r($return_array);
		//echo '<br/>Sold Array<br/>';
		//print_r($sold_array);
		//$data = count($sold_array) > 0 ? array_merge_recursive($return_array, $sold_array):$return_array;
		//echo '<br/>Last query<br/>'.$this->db->last_query();
		//print_r($data);
		return $return_array;
	}
	function mark_as_started($billing_id)
	{
		$data = array(
			'last_billing_attempt'=>date('Y-m-d 00:00:00'),
			'started'=>1,
			'charged'=>0,
			'emailed'=>0
		);
		$this->db->where("billing_id", $billing_id);
		$this->db->update("billing", $data);
	}
	function mark_as_charged($billing_id)
	{
		$data = array(
			'charged'=>1
		);
		$this->db->where("billing_id", $billing_id);
		$this->db->update("billing", $data);
	}
	function mark_as_sent($billing_id)
	{
		$data = array(
			'emailed'=>1
		);
		$this->db->where("billing_id", $billing_id);
		$this->db->update("billing", $data);
	}
	
	function have_sellable_teetimes($date)
	{
		$teesheet_id = $this->session->userdata('teesheet_id');
		$this->db->select('SUM(teetimes_daily) AS teetimes_daily');
		$this->db->from('billing');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->where('deleted', 0);
		$result = $this->db->get();
		$results = $result->result_array();	
		
		$sold_count = $this->get_daily_bartered_teetime_count($date);
		
		if ($result->num_rows() > 0)
			return $results[0]['teetimes_daily'] > $sold_count;
		else 
			return false;
	}
	function get_daily_bartered_teetime_count($date)
	{
		$teesheet_id = $this->session->userdata('teesheet_id');
		
		$this->db->from('teetimes_bartered');
		$this->db->like('start', substr($date, 0, 8), 'left');
		$this->db->where('teesheet_id', $teesheet_id);
		$query = $this->db->get();
		return $query->num_rows();
	}
	/*
	Gets information about a particular item
	*/
	function get_info($billing_id)
	{
		$this->db->from('billing');
		$this->db->where("billing_id = '$billing_id'");
		$this->db->limit(1);	
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('courses');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}

	/*
	Get an item id given an item number
	*/
	function get_item_id($item_number)
	{
		$this->db->from('courses');
		$this->db->where("course_id = '$item_number'");
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->course_id;
		}

		return false;
	}

	/*
	Gets information about multiple items
	*/
	function get_multiple_info($item_ids)
	{
		$this->db->from('courses');
                $this->db->where_in('course_id',$item_ids);
		$this->db->order_by("course_id", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates an item
	*/
	function save(&$billing_data,$billing_id=false)
	{
		if (!$billing_id or !$this->exists($billing_id))
		{
			if($this->db->insert('billing',$billing_data))
			{
				$billing_data['billing_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('billing_id', $billing_id);
        $val = $this->db->update('billing',$billing_data);
		//echo json_encode(array('sql'=>$this->db->last_query()));
		//return;
        
		return $val;
	}

	/*
	Updates multiple items at once
	*/
	function update_multiple($item_data,$item_ids)
	{
		$this->db->where_in('course_id',$item_ids);
		return $this->db->update('courses',$item_data);
	}

	/*
	Deletes one item
	*/
	function delete($item_id)
	{
		$this->db->where("course_id = '$item_id'");
		return $this->db->update('courses', array('deleted' => 1));
	}

	/*
	Deletes a list of items
	*/
	function delete_list($item_ids)
	{
		$this->db->where_in('billing_id',$item_ids);
		return $this->db->update('billing', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find items
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('billing');
		$this->db->join('courses', 'billing.course_id = courses.course_id');
		$this->db->like('name', $search);
		$this->db->where('deleted !=', 1);
		$this->db->order_by("name", "asc");
		$this->db->group_by('name');
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label' => $row->name);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	function get_course_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('courses');
		$this->db->like('name', $search);
		$this->db->order_by("name", "asc");
                $this->db->limit($limit);
		$by_name = $this->db->get();
		//echo json_encode(array('working'=>$by_name->num_rows()));
                foreach($by_name->result() as $row)
		{
			$suggestions[]=array('value' => $row->course_id, 'label' => $row->name.' - '.$row->city.', '.$row->state);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on items
	*/
	function search($search, $limit=20)
	{
		//return 'geebers';
		$this->db->from('billing');
		$this->db->join('courses', 'courses.course_id = billing.course_id');
		$this->db->like('name', $search);
		//$this->db->where("(name LIKE '%".$this->db->escape_like_str($search)."%' or 
		//city LIKE '%".$this->db->escape_like_str($search)."%' or 
		//state LIKE '%".$this->db->escape_like_str($search)."%')");
		$this->db->where('deleted != ', 1);
		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$results = $this->db->get();
		//return $this->db->last_query();
		return $results;	
	}
	function get_todays_billed()
	{
		$this->db->from('billing_charges');
		$this->db->where('date >=', date('Y-m-d '.'00:00:00'));
		$billings = $this->db->get()->result_array();
		$billing_array = array();
		foreach($billings as $billing)
			$billing_array[] = $billing['billing_id'];
		return $billing_array;
	}
	
	function get_todays_billings()
	{
		$month = date('n');
		$day = date('j');
		$today = date('Y-m-d');
		
		
		/*$this->db->from('billing');
		$this->db->join('billing_credit_cards', 'billing.credit_card_id = billing_credit_cards.credit_card_id');
		$this->db->where('billing.deleted !=', 1);
		$this->db->where('start_date <=', date('Y-m-d'));
		$this->db->where("((monthly = 1 AND monthly_day = ".date('j').") OR (annual = 1 AND annual_day = ".date('j')." AND annual_month = ".date('n')."))");
		$this->db->order_by('billing.credit_card_id');
		$this->db->where("((period_start <= period_end AND period_start <= $month AND period_end >= $month) OR (period_start > period_end AND (period_start <= $month OR period_end > $month)))");
		//$this->db->where('');
		return $this->db->get();
		*/
		return $this->db->query("SELECT
					token,
					cardholder_name,
					billing_id,
					foreup_billing.course_id AS course_id,
					foreup_billing.credit_card_id AS credit_card_id,
					SUM((CASE WHEN annual = 1 AND (annual = 1 AND annual_day = $day AND annual_month = $month) THEN annual_amount ELSE 0 END) * tax_rate / 100) AS annual_tax_amount, 
					SUM((CASE WHEN (monthly = 1 AND monthly_day = $day) THEN monthly_amount ELSE 0 END) * tax_rate / 100) AS monthly_tax_amount, 
					SUM((CASE WHEN annual = 1 AND (annual = 1 AND annual_day = $day AND annual_month = $month) THEN annual_amount ELSE 0 END)) AS total_annual_amount, 
					SUM((CASE WHEN (monthly = 1 AND monthly_day = $day) THEN monthly_amount ELSE 0 END)) AS total_monthly_amount, 
					GROUP_CONCAT(tax_name) AS tax_names, 
					GROUP_CONCAT(tax_rate) AS tax_rates, 
					GROUP_CONCAT(contact_email) AS contact_emails, 
					GROUP_CONCAT(product) AS products 
					FROM (`foreup_billing`) 
					JOIN `foreup_billing_credit_cards` ON `foreup_billing`.`credit_card_id` = `foreup_billing_credit_cards`.`credit_card_id` 
					WHERE `foreup_billing`.`deleted` != 1 
					AND ((monthly = 1 AND monthly_day = $day) OR (annual = 1 AND annual_day = $day AND annual_month = $month))  
					AND `start_date` <= '$today'
					AND ((period_start <= period_end AND period_start <= $month AND period_end >= $month) OR (period_start > period_end AND (period_start <= $month OR period_end >= $month))) 
					GROUP BY `foreup_billing`.`credit_card_id` 
					ORDER BY `foreup_billing`.`credit_card_id`");
		
	}
	
	function get_billing_products($course_id, $credit_card_id)
	{
		$month = date('n');
		$day = date('j');
		$today = date('Y-m-d');
		
		
		$this->db->from('billing');
		//$this->db->join('billing_credit_cards', 'billing.credit_card_id = billing_credit_cards.credit_card_id');
		$this->db->where('billing.deleted !=', 1);
		$this->db->where('start_date <=', $today);
		$this->db->where("(credit_card_id = $credit_card_id AND ((monthly = 1 AND monthly_day = $day) OR (annual = 1 AND annual_day = $day AND annual_month = $month)) OR free = 1)");
		$this->db->order_by('billing.credit_card_id');
		$this->db->where("((period_start <= period_end AND period_start <= $month AND period_end >= $month) OR (period_start > period_end AND (period_start <= $month OR period_end > $month)))");
		$this->db->where('course_id', $course_id);
//		$this->db->where("(free = 1 OR credit_card_id = $credit_card_id)");
		return $this->db->get();
	}
}
?>
