<?php
//template 5
$lang['email_tpl_live_preview_high_def']='Live Preview High Definition by PremiumStuff';
$lang['email_tpl_our_mac_ocx']='Our Mac OSX App now 50% off.';
$lang['email_tpl_rise_yourself']='Rise yourself to an Oreo. Get your app.';
$lang['email_tpl_interface']='Interface';
$lang['email_tpl_product_x']='Product X comes with a slick and slim design specially designed to work fast.';
$lang['email_tpl_lightweight']='Lightweight';
$lang['email_tpl_one_click_install']='One click install. We worked hard to let the App automate the installing process.';
$lang['email_tpl_experiment']='Experiment';
$lang['email_tpl_when_your_ready']='When your ready, experiment with the hundreds of elements we build in.';
$lang['email_tpl_app_name']='App Name';
$lang['email_tpl_one_time_email_sent']='This is a one-time email sent to';
$lang['email_tpl_if_you_wish_to_receive']='If you wish to receive more emails by Company Name, please take a few seconds to';
$lang['email_tpl_subscribe']='Subscribe';

//template 4
$lang['email_tpl_easy_box_email']='Easy Box Email Template';
$lang['email_tpl_having_trouble']='Having trouble viewing this email? ';
$lang['email_tpl_view_it_in_browser']='View it in your browser.';
$lang['email_tpl_all_right_reserved']='All rights reserved.';
$lang['email_tpl_your_company']='Your Company';
$lang['email_tpl_unsubscribe'] = 'unsubscribe';
$lang['email_tpl_signed_up_to_company']='You are currently signed up to Company\'s newsletters as';
$lang['company_address']='YourCompany, XXX Adress, XXX City, XX 123, XX Country';


//template 3
$lang['email_tpl_skyline_newsletter']='Skyline - Newsletter with Template Builder';
$lang['email_tpl_trouble_seeing_email']='Having trouble seeing this email?';
$lang['email_tpl_view_online']='view it online';
$lang['email_tpl_newsletter_template_builder'] = 'email_tpl_unsubscribe';
$lang['email_tpl_heading_goes_here']='Headline goes here';
$lang['email_tpl_received_email_subscribed']='You have received this email because you have subscribed to ';
$lang['email_tpl_skyline_media']='Skyline Media';
$lang['email_tpl_as']='as';
$lang['email_tpl_no_longer_wish']='If you no longer wish to receive emails please';
$lang['email_tpl_all_right_reserved']='�2012 Your Company here, All rights reserved';
$lang['email_tpl_scroll_to_top']='scroll to top';

//template 2
$lang['email_tpl_ruby_email']='Ruby E-mail Template - A Touch of Css3 and Magic perhaps.';
$lang['email_tpl_get_auto_update_icloud']='Get Auto Updates from iCloud.';
$lang['email_tpl_minimalistic_design']='Minimalistic Design.';
$lang['email_tpl_advance_switch_button']='Advanced Switch Button.';
$lang['email_tpl_valid_code']='Valid Code.';
$lang['email_tpl_psd_files']='PSD Files.';
$lang['email_tpl_youre_invited']='You\'re invited to download our beta.';
$lang['email_tpl_signed_up_online']='You are receiving this because you signed up online.';
$lang['email_tpl_dont_wish_to_hear_from_us']='if you do not wish to hear from us.';
$lang['email_tpl_cool_enough']='But we believe you think we are cool enough, aren\'t we?';

//template 1
$lang['email_tpl_america_template']='America Template';
$lang['email_tpl_we_are_friendly']='We are friendly.';
$lang['email_tpl_this_email_template']='This e-mail template is extremely user friendly. We have commented our Photoshop files &amp; code as possible as we can.';
$lang['email_tpl_we_push_forward']='We push forward.';
$lang['email_tpl_its_possible_to_convince_customers']='It\'s impossible to convince customers to buy your product with boring white e-mail templates. Not anymore with America!';
$lang['email_tpl_we_like_coffee']='We like coffee.';
$lang['email_tpl_and_we_also_like_great_design'] = 'And we also like great Design. We try to push email templates forward as far as possible. Just take a look at those pixels!';
$lang['email_tpl_save_time']='Save time.';
$lang['email_tpl_save_time_letting_us_do_your_work']='Save time by letting us do your work. We love what we do. You can';
$lang['email_tpl_contact_us_here']='contact us here';
$lang['email_tpl_what_are_you_waiting_for']='So champ, what are you waiting for?';
$lang['email_tpl_7_color_schemes']='7 Color Schemes, 2 Versions, Editable Photoshop files, Stretchable, Easy to Customize, Tightly Coded, Awesome Designed, Future Proof, Well Documented, Flawless Support. Ready to roll?';
$lang['email_tpl_one_time_email']='This is a one-time email sent to';
$lang['email_tpl_wish_to_received_more_emails']='If you wish to receive more emails by';
$lang['email_tpl_company_name']='Company Name';
$lang['email_tpl_please_take_a_few_second']='please take a few seconds to';

//reservation made
$lang['email_tpl_congratulations']='Congratulations';
$lang['email_tpl_reservation_booked']='your reservation has been booked.';
$lang['email_tpl_reservation_has_been']='Your reservation has been';
$lang['email_tpl_succcessfully_cancelled']='successfully cancelled';
$lang['email_tpl_reservation_details']='Reservation Details';
$lang['email_tpl_golf_course']='Golf Course';
$lang['email_tpl_details']='Details';
$lang['email_tpl_questions_or_concerns']='If you have any questions or concerns, you may contact';
$lang['email_tpl_at']='at';
$lang['email_tpl_cancel_your_reservation']='If you need to cancel your reservation for any reason, please';
$lang['email_tpl_click_here']='click here';

//purchase receipt
$lang['email_tpl_thank_you_purchase']='Thank you for your purchase.';
$lang['email_tpl_payment_details']='Payment Details';
$lang['email_tpl_billing_info']='Billing Info';
$lang['email_contact_foreup']='If you have any questions or concerns, you may contact ForeUP at support@foreup.com';
?>