var views = {
	login : {
		login : 
			'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'+
			'<html xmlns="http://www.w3.org/1999/xhtml">'+
			'<head>'+
			'<meta http-equiv="content-type" content="text/html; charset=utf-8" />'+
			'<link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>'+
			'<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/login.css?<?php echo APPLICATION_VERSION; ?>" />'+
			'<title>ForeUP '+lang('login_login')+'</title>'+
			'<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>'+
			'<script type="text/javascript">'+
			'$(document).ready(function()'+
			'{'+
			'	$("#login_form input:first").focus();'+
			'});'+
			'</script>'+
			'</head>'+
			'<body>'+
				'<div id="welcome_message" class="top_message_error">'+
				'</div>'+
			'<form action="login">'+
			'<div id="container">'+
				'<div id="top">'+
					'<img src="../images/login/login_logo.png"/>'+
				'</div>'+
				'<div id="middle">'+
					'<div class="login_header">'+
					'	Sign In'+
					'</div>'+
					'<div id="form_field_username">'+	
						'<span class="username_icon"></span>'+
						'<input name="username" value="" placeholder="'+lang('login_username')+' size=20 />'+
					'</div>'+
					'<div id="form_field_password">'+	
						'<span class="password_icon"></span>'+
						'<input type="password" name="password" value="" placeholder="'+lang('login_password')+'" size=20 />'+
					'</div>'+
					'<div id="form_field_submit">'+	
						'<div id="submit_button">'+
							'<input type="submit" name="login_button" value="'+lang('login_continue')+'" />'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div id="bottom">'+
					'<div id="right">'+
						'<a href="login/reset_password">'+lang('login_forgot_password')+'</a>'+ 
					'</div>'+
				'</div>'+
			'</div>'+
			'</form>'+
			'</body>'+
			'</html>'
	}
}
