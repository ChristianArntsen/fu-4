<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<style type="text/css">
<!--
.modfocustitle {color:#ffffff; font-size:100px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;}
.modfocuscon {color:#c3f5c4; text-decoration:none; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:18px;}
.modfulltitle {color:#000000; padding:19px 0px 13px 30px;font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:14px; font-weight:bold;}
.modfullcon {padding:0px 30px 16px 30px;font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#666666; line-height:18px;}
.mod12title {color:#000000; padding:19px 0px 13px 0px;font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:14px; font-weight:bold;}
.mod12con {padding:0px 0px 16px 0px;font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#666666; line-height:18px;}
.mod13title {color:#000000;font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:17px 0px 6px 0px;}
.mod13con {color:#02927c; font-family:Arial, Helvetica, sans-serif; font-size:11px;}
-->
</style>

<title>Easy Box Email Template</title>

</head>
<body style="margin:0 auto; padding: 0px; background-color:#333333;" bgcolor="#333333">
<!--
/* 
Project:		Easy Box
Version:		1.0
Last change:	2010/12/19 
Author: BWSM 
*/-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#333333"><tbody>
<tr>
<td>
<!-- Header bar start-->
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#000000">
<tbody>
  <tr>
    <td height="28" align="center"><span align="center" style="color:#666666; font-family:Arial, Helvetica, sans-serif; font-size:12px;">Having trouble viewing this email? <a href="#" target="_blank" style="color:#666666; text-decoration: underline;">View it in your
    browser.</a></span></td>
  </tr>
</tbody>
</table>
<!-- Header bar end-->
<table width="650" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#333333">  
<!-- Logo bar start -->
  <tbody><tr>
    <td>
	 <table width="600" height="70" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td width="422"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/logo.gif" alt="logo" width="173" height="70" border="0"></a></td>
          <td width="178" align="right"><span style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:11px;">August 8.2010</span></td>
        </tr>
      </tbody>
    </table>
	</td>
  </tr>
  <!-- Logo bar end-->
  <!--Promotion module start -->
  <tr>
    <td>
	<table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td height="21">&nbsp;</td>
          <td width="386" rowspan="2" bgcolor="#ffffff" style="line-height:2px; font-size:2px;"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/sampple-lyout1-focus.jpg" alt="pic" width="386" height="235" style="display:block;" border="0" vspace="0"></a></td>
        </tr>
        <tr>
          <td width="214" valign="top" bgcolor="#1a9481" style="padding-top:17px;"><table width="160" border="0" align="center" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td align="left" valign="top" class="modfocustitle">50</td>
                  <td><img src="http://www.uiueux.com/emailtheme/easybox/images/ico-off-blue.gif" alt="off" width="42" height="74"></td>
                </tr>
              </tbody>
            </table>
              <table width="160" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                  <tr>
                    <td class="modfocuscon">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ...</td>
                  </tr>
                </tbody>
            </table></td>
        </tr>
        <tr>
          <td colspan="2" style="line-height:2px; font-size:2px;"><img src="http://www.uiueux.com/emailtheme/easybox/images/linefull.gif" alt="line" height="2" width="600" style="display:block;" border="0" vspace="0"> </td>
        </tr>
      </tbody>
    </table>
	</td>
  </tr>
  <!--Promotion module end -->
  <!--- Full width image+text module start  -->
  <tr>
    <td>
	<table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td bgcolor="#FFFFFF"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/sample-picbig600px_01.jpg" alt="pic" width="600" height="270" style="display:block;" border="0" vspace="0"></a></td>
    </tr>
    <tr>
      <td><table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td width="570" colspan="3" bgcolor="#FFFFFF" class="modfulltitle">Lorem ipsum dolor</td>
          </tr>
          <tr>
            <td colspan="3" bgcolor="#FFFFFF" class="modfullcon">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, consectetur adipisicing elit</td>
          </tr>
          <tr>
            <td colspan="3" bgcolor="#FFFFFF" style="padding:0px 0px 20px 30px;"><a href="#" target="_blank" style="color:#0066cc; text-decoration:none; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"><img src="http://www.uiueux.com/emailtheme/easybox/images/btn-more-blue.gif" alt="more" width="79" height="18" border="0"></a></td>
          </tr>
          <tr>
            <td colspan="3" style="line-height:2px; font-size:2px;"><img src="http://www.uiueux.com/emailtheme/easybox/images/linefull.gif" alt="line" height="2" width="600" style="display:block;" border="0" vspace="0"></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
 	 </tbody>
	</table>
	</td>
  </tr>
  <!--- Full width image+text module end  -->
  <!--- 1/2 image+text module start-->
  <tr>
    <td><table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td bgcolor="#FFFFFF" style="padding:20px 0px 0px 30px;"><table width="259" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/sample-piclist259px.jpg" alt="pic" width="259" height="118" border="0"></a></td>
                </tr>
                <tr>
                  <td class="mod12title">Lorem ipsum dolor</td>
                </tr>
                <tr>
                  <td class="mod12con">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do  eiusmod tempor .... </td>
                </tr>
                <tr>
                  <td style="padding:0px 0px 20px 0px;"><a href="#" target="_blank" style="color:#0066cc; text-decoration:none; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"><img src="http://www.uiueux.com/emailtheme/easybox/images/btn-more-blue.gif" alt="more" width="79" height="18" border="0"></a></td>
                </tr>
              </tbody>
          </table></td>
          <td bgcolor="#FFFFFF" style="padding:20px 0px 0px 0px;"><table width="259" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/sample-piclist259px.jpg" alt="pic" width="259" height="118" border="0"></a></td>
                </tr>
                <tr>
                  <td class="mod12title">Lorem ipsum dolor</td>
                </tr>
                <tr>
                  <td class="mod12con">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do  eiusmod tempor .... </td>
                </tr>
                <tr>
                  <td style="padding:0px 0px 20px 0px;"><a href="#" target="_blank" style="color:#0066cc; text-decoration:none; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"><img src="http://www.uiueux.com/emailtheme/easybox/images/btn-more-blue.gif" alt="more" width="79" height="18" border="0"></a></td>
                </tr>
              </tbody>
          </table></td>
        </tr>
        <tr>
          <td colspan="2" style="line-height:2px; font-size:2px;"><img src="http://www.uiueux.com/emailtheme/easybox/images/linefull.gif" alt="line" height="2" width="600" style="display:block;" border="0" vspace="0"> </td>
        </tr>
      </tbody>
    </table></td>
  </tr>
  <!--- 1/2 image+text module end-->
  <!--- 1/3 image+text module start(all 6 units)-->
  <tr>
    <td><table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <!---3 units start-->
        <tr>
          <td width="30" valign="top" bgcolor="#FFFFFF" style="padding:20px 0px 20px 0px;">&nbsp;</td>
          <td width="190" valign="top" bgcolor="#FFFFFF" style="padding:20px 0px 18px 0px;"><table width="160" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/sample-piclist160px.jpg" alt="pic" width="160" height="160" border="0"></a></td>
                </tr>
                <tr>
                  <td class="mod13title">Lorem ipsum dolor</td>
                </tr>
                <tr>
                  <td class="mod13con">$82.00 USD</td>
                </tr>
              </tbody>
          </table></td>
          <td width="190" valign="top" bgcolor="#FFFFFF" style="padding:20px 0px 20px 0px;"><table width="160" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/sample-piclist160px.jpg" alt="pic" width="160" height="160" border="0"></a></td>
                </tr>
                <tr>
                  <td class="mod13title">Lorem ipsum dolor</td>
                </tr>
                <tr>
                  <td class="mod13con">$82.00 USD</td>
                </tr>
              </tbody>
          </table></td>
          <td width="190" valign="top" bgcolor="#FFFFFF" style="padding:20px 0px 0px 0px;"><table width="160" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/sample-piclist160px.jpg" alt="pic" width="160" height="160" border="0"></a></td>
                </tr>
                <tr>
                  <td class="mod13title">Lorem ipsum dolor</td>
                </tr>
                <tr>
                  <td class="mod13con">$82.00 USD</td>
                </tr>
              </tbody>
          </table></td>
        </tr>
        <!---3 units end-->
        <!---3 units start-->
        <tr>
          <td width="30" valign="top" bgcolor="#FFFFFF" style="padding:0px 0px 20px 0px;">&nbsp;</td>
          <td width="190" valign="top" bgcolor="#FFFFFF" style="padding:0px 0px 20px 0px;"><table width="160" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/sample-piclist160px.jpg" alt="pic" width="160" height="160" border="0"></a></td>
                </tr>
                <tr>
                  <td class="mod13title">Lorem ipsum dolor</td>
                </tr>
                <tr>
                  <td class="mod13con">$82.00 USD</td>
                </tr>
              </tbody>
          </table></td>
          <td width="190" valign="top" bgcolor="#FFFFFF" style="padding:0px 0px 20px 0px;"><table width="160" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/sample-piclist160px.jpg" alt="pic" width="160" height="160" border="0"></a></td>
                </tr>
                <tr>
                  <td class="mod13title">Lorem ipsum dolor</td>
                </tr>
                <tr>
                  <td class="mod13con">$82.00 USD</td>
                </tr>
              </tbody>
          </table></td>
          <td width="190" valign="top" bgcolor="#FFFFFF" style="padding:0px 0px 0px 0px;"><table width="160" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/sample-piclist160px.jpg" alt="pic" width="160" height="160" border="0"></a></td>
                </tr>
                <tr>
                  <td class="mod13title">Lorem ipsum dolor</td>
                </tr>
                <tr>
                  <td class="mod13con">$82.00 USD</td>
                </tr>
              </tbody>
          </table></td>
        </tr>
        <!---3 units end-->
        <tr>
          <td colspan="4" style="line-height:2px; font-size:2px;"><img src="http://www.uiueux.com/emailtheme/easybox/images/linefull.gif" alt="line" height="2" width="600" style="display:block;" border="0" vspace="0"> </td>
        </tr>
      </tbody>
    </table></td>
  </tr>
  <!--- 1/3 image+text module end-->
  <!---Share icons bar start -->
  <tr>
    <td>
	<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
  <tbody>
    <tr>
      <td>
	   <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td align="center" style=" padding:20px 0px 20px 0px;"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/ico_facebook_24.gif" alt="facebook" width="24" height="24" border="0"></a><img src="http://www.uiueux.com/emailtheme/easybox/images/spacer.gif" width="5"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/ico_twitter_24.gif" alt="twitter" width="24" height="24" border="0"></a><img src="http://www.uiueux.com/emailtheme/easybox/images/spacer.gif" width="5"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/ico_rss_24.gif" alt="rss" width="24" height="24" border="0"></a><img src="http://www.uiueux.com/emailtheme/easybox/images/spacer.gif" width="5"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/ico_youtube_24.gif" alt="youtube" width="24" height="24" border="0"></a><img src="http://www.uiueux.com/emailtheme/easybox/images/spacer.gif" width="5"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/ico_delicious_24.gif" alt="delicious" width="24" height="24" border="0"></a><img src="http://www.uiueux.com/emailtheme/easybox/images/spacer.gif" width="5"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/ico_digg_24.gif" alt="digg" width="24" height="24" border="0"></a><img src="http://www.uiueux.com/emailtheme/easybox/images/spacer.gif" width="5"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/ico_flickr_24.gif" alt="flickr" width="24" height="24" border="0"></a><img src="http://www.uiueux.com/emailtheme/easybox/images/spacer.gif" width="5"><a href="#"><img src="http://www.uiueux.com/emailtheme/easybox/images/ico_technorati_24.gif" alt="technorati" width="24" height="24" border="0"></a></td>
            </tr>
        </tbody>
      </table></td>
	    </tr>
	  </tbody>
	</table>
	</td>
  </tr>
  <!---Share icons bar end -->
  <!---Foot bar start -->
  <tr>
    <td>
	<table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
         <tr>
          <td valign="top" bgcolor="#000000" style=" padding:20px 0px 20px 30px;color:#999999; font-size:11px; line-height:18px; font-family:Arial, Helvetica, sans-serif;">YourCompany, XXX Adress, XXX City, XX 123, XX Country<br>
            E: <a href="mailto:youremailname@gmail.com" style="color:#999999; text-decoration:none;">youremailname@gmail.com</a><br>
            T: +86 1234567890<br>
            © 2010 <a href="#" style="color:#FFFFFF; text-decoration:none;">Your Company</a>. All rights reserved.</td>
          <td width="240" valign="top" bgcolor="#000000" style=" padding:20px 30px 0px 10px;color:#999999; font-size:11px; line-height:18px; font-family:Arial, Helvetica, sans-serif;">You are currently signed up to Company's newsletters as: <a href="mailto:email@email.com" style="color:#999999; text-decoration:none;">email@email.com</a>. <br>
            <a href="#" style="color:#FFFFFF; text-decoration: underline;">Unsubscribe</a></td>
        </tr>
        <tr>
          <td height="30" colspan="3">&nbsp;</td>
        </tr>
      </tbody>
    </table>
	</td>
  </tr>
   <!---Foot bar end -->
</tbody></table>
</td>
</tr>
</tbody>
</table>


</body></html>