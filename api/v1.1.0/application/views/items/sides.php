<ul id="error_message_box"></ul>
<?php
echo form_open('items/save_side_options/'.$item_id, array('id'=>'sides_form'));
?>
<fieldset id="item_basic_info">
<legend><?php echo lang("items_basic_sides_information"); ?></legend>
<table>
	<tbody>
		<tr>
			<th>Not Available</th>
			<th>Name</th>
			<th>Default</th>
			<th>Upgrade Price</th>
		</tr>
		<?php foreach($sides as $side) { ?>
			<?php //print_r($side);?>
			<tr>
				<td style='text-align: center;'>
					<?php echo form_hidden('side_id[]', $side->item_id);?>
					<?php echo form_checkbox(array(
						'name'=>'not_available[]',
						'id'=>'not_available',
						'value'=>$side->item_id,
						'checked'=>($side->not_available)? 1 : 0)
					);?>
				</td>
				<td><?=$side->name?></td>
				<td style='text-align: center;'>
					<?php echo form_checkbox(array(
						'name'=>'default[]',
						'id'=>'default',
						'value'=>$side->item_id,
						'checked'=>($side->default)? 1 : 0)
					);?>
				</td>
				<td style='text-align: center;'>
					<?php echo form_input(array(
						'name'=>'upgrade_price[]',
						'size'=>'8',
						'id'=>'upgrade_price',
						'placeholder'=>to_currency_no_money($side->add_on_price),
						'style'=>'text-align:right',
						'value'=>$side->upgrade_price > 0 ? to_currency_no_money($side->upgrade_price) : '')
					);?>
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
</fieldset>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
<?php
echo form_close();
?>
<script>
	$(document).ready(function(){
		console.log('applying sides validate');
		var submitting = false;
		$('#sides_form').validate({
			submitHandler:function(form)
			{
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				$(form).ajaxSubmit({
				success:function(response)
				{
					submitting = false;
					if (response.success)
						$.colorbox.close();
					else
						$(form).unmask();
				},
				dataType:'json'
			});
	
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules:
			{
	   		},
			messages:
			{
			}
		});
	});
</script>
