<div class="field_row clearfix">	
	<?php echo form_label(lang('config_use_terminals').':', 'use',array('class'=>'wide')); ?>
    <div class='form_field'>
    	<?php echo form_checkbox(array('name'=>'use_terminals', 'value'=>'1', 'checked'=>$use_terminals, 'id'=>'use_terminals')); ?>
    	<div id='add_terminal'><?=lang('config_add_terminal')?></div>
    </div>
</div>
<div class="field_row clearfix">	
	<?php echo form_label('', '',array('class'=>'wide')); ?>
    <div class='form_field' id='terminal_list'>
    	<?php foreach($terminals as $terminal) { ?>
    		<div id='terminal_<?=$terminal->terminal_id;?>'><span id='delete_terminal_<?=$terminal->terminal_id;?>' class='delete_terminal'></span><?php echo form_input(array('name' => 'terminal_label[]', 'value' => $terminal->label, 'size' => '50'));?><?php echo form_hidden('terminal_id[]',$terminal->terminal_id); ?><?php echo form_hidden('delete_terminal_id[]', 0); echo 'QB tab: '.form_dropdown("terminal_tab[]", array(1=>1,2=>2,3=>3), $terminal->quickbutton_tab); ?></div>
    	<?php } ?>
    </div>
</div>

<script>
	function add_delete_click() 
	{
		$('.delete_terminal').click(function(){
			var id = $(this).attr('id').replace('delete_terminal_','');
			$('#terminal_'+id+' input[name=delete_terminal_id[]]').val(1);
			$('#terminal_'+id).hide();
		});
	}
	$(document).ready(function(){
		$('#add_terminal').click(function(){
			var id=Math.floor(Math.random()*101);
			$('#terminal_list').append("<div id='terminal_"+id+"'><span id='delete_terminal_"+id+"' class='delete_terminal'></span><input name='terminal_label[]' value='' size='50'/><input type='hidden' name='terminal_id[]' value='0'/><input type='hidden' name='delete_terminal_id[]' value='0'/> QB tab: <select id='terminal_tab[]' name='terminal_tab[]'><option value='1' selected='selected'>1</option><option value='2'>2</option><option value='3'>3</option></select></div>");
			add_delete_click();
		});
		add_delete_click();
	});
</script>
