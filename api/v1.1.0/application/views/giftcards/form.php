<ul id="error_message_box"></ul>
<?php
if ($cart_line)
	echo form_open('sales/save_giftcard_details/'.$cart_line,array('id'=>'giftcard_form'));
else
	echo form_open('giftcards/save/'.$giftcard_info->giftcard_id,array('id'=>'giftcard_form'));
?>
<fieldset id="giftcard_basic_info">
<?php
if ($cart_line) { ?>
<div class="field_row clearfix">
	<span class='holes_buttonset'>
        <input type='radio' id='gc_action_purchase' name='gc_action' value='purchase' <?php echo ($giftcard_info->action != 'reload')?'checked':''; ?>/>
        <label for='gc_action_purchase' id='gc_action_purchase_label'>Purchase</label>
        <input type='radio' id='gc_action_reload' name='gc_action' value='reload'  <?php echo ($giftcard_info->action == 'reload')?'checked':''; ?>/>
        <label for='gc_action_reload' id='gc_action_reload_label'>Reload</label>
    </span>
	<style>
		.holes_buttonset label.ui-button {
		padding:0px 5px 6px;
	</style>	
	<script>
	    $('.holes_buttonset').buttonset();
	    $('#gc_action_reload_label').click(function(){
	    	$('.purchase_item').hide();
	    	$.colorbox.resize();
	    	$('#giftcard_value_label').html("Reload Amount: <span class='required'>*</span>");
	    });
	    $('#gc_action_purchase_label').click(function(){
	    	$('.purchase_item').show();
	    	$.colorbox.resize();
	    	$('#giftcard_value_label').html("Value: <span class='required'>*</span>");
	    });
	    $(document).ready(function(){
	    	<?php if ($giftcard_info->action == 'reload') { ?>
	    		$('#gc_action_reload_label').click();
	    	<?php } ?>
	    })
	</script>
</div>
<?php } ?>

<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_giftcard_number').':<span class="required">*</span>', 'name',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo $view_only ? $giftcard_info->giftcard_number :
	form_input(array(
		'name'=>'giftcard_number',
		'size'=>'24',
		'maxlength'=>'16',
		'id'=>'giftcard_number',
		'value'=>$giftcard_info->giftcard_number)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_card_value').':<span class="required">*</span>', 'name',array('class'=>' wide', 'id'=>'giftcard_value_label')); ?>
	<div class='form_field'>
	<?php echo $view_only ? $giftcard_info->value :
	form_input(array(
		'name'=>'value',
		'size'=>'8',
		'id'=>'value',
		'value'=>$giftcard_info->value)
	);?>
	</div>
</div>

<div class="field_row clearfix purchase_item">
<?php echo form_label(lang('giftcards_customer_name').':', 'customer',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo $view_only ? $customer : form_input(array('name'=>'customer','id'=>'customer','value'=>($customer != '')?$customer:'No Customer;'/*lang('sales_start_typing_customer_name'),  'accesskey' => 'c'*/));?>
		<?php echo form_hidden('customer_id', $giftcard_info->customer_id);?>
		<?php //echo form_dropdown('customer_id', $customers, $giftcard_info->customer_id, 'id="customer_id"');?>
	</div>
</div>
<div class="field_row clearfix purchase_item">
<?php echo form_label(lang('giftcards_expiration_date').':', 'expiration_date',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		$expiration_date = ($giftcard_info->expiration_date != '0000-00-00')?$giftcard_info->expiration_date:'No expiration'; 
		echo $view_only ? $expiration_date : form_input(array('name'=>'expiration_date','id'=>'expiration_date','value'=>$expiration_date));?>
	</div>
</div>
<div class="field_row clearfix purchase_item">
<?php echo form_label(lang('giftcards_department').':', 'department',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		echo $view_only ? '' :
			form_radio(array(
                    'name'=>'dept_cat',
                    'id'=>'dept_cat_1',
                    'value'=>'department',
                    'checked'=>$giftcard_info->category == ''));	
		echo $view_only ? $giftcard_info->department : form_input(array('name'=>'department','id'=>'department','value'=>$giftcard_info->department));?>
	</div>
</div>
<div class="field_row clearfix purchase_item">
<?php echo form_label(lang('giftcards_category').':', 'category',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		echo $view_only ? '' :
			form_radio(array(
                    'name'=>'dept_cat',
                    'id'=>'dept_cat_2',
                    'value'=>'category',
                    'checked'=>$giftcard_info->category != ''));	
		echo $view_only ? $giftcard_info->category : form_input(array('name'=>'category','id'=>'category','value'=>$giftcard_info->category, 'readonly'=>'readonly'));?>
	</div>
</div>
<div class="field_row clearfix purchase_item">
<?php echo form_label(lang('giftcards_notes').':', 'details',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		echo $view_only ? $giftcard_info->details : form_textarea(array('name'=>'details','id'=>'details','value'=>$giftcard_info->details,'rows'=>2));?>
	</div>
</div>

<?php
echo $view_only ? '' :
form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
if (!$view_only) {
?>
<style>
	#details {
		width:470px;
	}
</style>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('#dept_cat_1, #dept_cat_2').click(function(){
		var value = ($(this).val() == 'department' ? 'category' : 'department');
		$('#department').removeAttr('readonly')
		$('#category').removeAttr('readonly')
		$('#'+value).attr('readonly', 'readonly').val('').focus();
	});
	var dept = $( "#department" ); 
	dept.autocomplete({
		source: "<?php echo site_url('items/suggest_department');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
	//dept.click(function(){dept.autocomplete('search','')});
	
	var cat = $( "#category" );
	cat.autocomplete({
		source: "<?php echo site_url('items/suggest_category');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
	//cat.click(function(){cat.autocomplete('search','')});
	
	$('#expiration_date').datepicker({dateFormat:'yy-mm-dd'});
	$('#customer').click(function()
    {
    	$(this).attr('value','');
       	$('input[name=customer_id]').val('');
    });
    $('#giftcard_number').bind('keypress', function(e) {
    	var code = (e.keyCode ? e.keyCode : e.which);
		 if(code == 13) { //Enter keycode
		   //Do something
		   e.preventDefault();
		 }
    });
    $('#customer').blur(function(e)
    {
    	var text = $(e.target).val();
        if (text == '')
        	$(this).attr('value',"No Customer");
    });
	$( "#customer" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/last_name"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$("#customer").val(ui.item.label);
			$("input[name=customer_id]").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});
	var submitting = false;
    $('#giftcard_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			<?php if ($giftcard_info->giftcard_id) { ?>
				$(form).ajaxSubmit({
					success:function(response)
					{
						giftcard_swipe = false;
						$.colorbox.close();
		                submitting = false;
		                <?php if ($cart_line) {?>
		                	console.dir(response);
						    //result = eval('('+response+')');
			                sales.update_cart_info(<?php echo ($cart_line) ?>, response.item_info);
			                sales.update_basket_totals(<?php echo ($cart_line) ?>, response.basket_info);
		           	
						<?php } else { ?>
							post_giftcard_form_submit(response);
						<?php } ?>
					},
					dataType:'json'
				});
			<?php } else {?>
				var gc_action = $('input:[name=gc_action]:checked').val();
				
				if (gc_action == 'reload')
				{
					$.ajax({
			           type: "POST",
			           url: <?php if ($cart_line) {?>"index.php/sales/giftcard_exists/"+$('#giftcard_number').val()+'/'+<?=$cart_line?><?php } else { ?>"index.php/giftcards/exists/"+$('#giftcard_number').val()<?php } ?>,
			           data: "",
			           success: function(response){
					   	   if (response.exists)
					   	   {
		   	   					$(form).ajaxSubmit({
									success:function(response)
									{
										$.colorbox.close();
						                submitting = false;
						                <?php if ($cart_line) {?>
						                	console.dir(response);
										    //result = eval('('+response+')');
							                sales.update_cart_info(<?php echo ($cart_line) ?>, response.item_info);
							                sales.update_basket_totals(<?php echo ($cart_line) ?>, response.basket_info);
						           	
										<?php } else { ?>
											post_giftcard_form_submit(response);
										<?php } ?>
									},
									dataType:'json'
								});
		
					   	   }
					   	   else
					   	   {
					   	   		submitting = false;
					   	   		$(form).unmask();
								alert('Giftcard number does not exist, please select a different one.');
					   	   }
					   },
			           dataType:'json'
			        });
				}
				else
				{
					$.ajax({
			           type: "POST",
			           url: <?php if ($cart_line) {?>"index.php/sales/giftcard_exists/"+$('#giftcard_number').val()+'/'+<?=$cart_line?><?php } else { ?>"index.php/giftcards/exists/"+$('#giftcard_number').val()<?php } ?>,
			           data: "",
			           success: function(response){
					   	   if (!response.exists)
					   	   {
		   	   					$(form).ajaxSubmit({
									success:function(response)
									{
										$.colorbox.close();
						                submitting = false;
						                <?php if ($cart_line) {?>
						                	console.dir(response);
										    //result = eval('('+response+')');
							                sales.update_cart_info(<?php echo ($cart_line) ?>, response.item_info);
							                sales.update_basket_totals(<?php echo ($cart_line) ?>, response.basket_info);
						           	
										<?php } else { ?>
											post_giftcard_form_submit(response);
										<?php } ?>
									},
									dataType:'json'
								});
		
					   	   }
					   	   else
					   	   {
					   	   		submitting = false;
					   	   		$(form).unmask();
								alert('Giftcard number is already in use, please select a different one.');
					   	   }
					   },
			           dataType:'json'
			        });
			    }
		    <?php } ?>
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			giftcard_number:
			{
				required:true
			},
			value:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{
			giftcard_number:
			{
				required:"<?php echo lang('giftcards_number_required'); ?>",
				number:"<?php echo lang('giftcards_number'); ?>"
			},
			value:
			{
				required:"<?php echo lang('giftcards_value_required'); ?>",
				number:"<?php echo lang('giftcards_value'); ?>"
			}
		}
	});
});
</script>
<script>
var giftcard_swipe = false;
	$(document).ready(function(){
		var gcn = $('#giftcard_number');
		gcn.keydown(function(event){
			// Allow: backspace, delete, tab, escape, and enter
	        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
	             // Allow: Ctrl+A
	            (event.keyCode == 65 && event.ctrlKey === true) || 
	             // Allow: home, end, left, right
	            (event.keyCode >= 35 && event.keyCode <= 39)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        else {
	            // Ensure that it is a number and stop the keypress
	            //if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	            	console.log('kc '+event.keyCode);
	            var kc = event.keyCode;
	            if (giftcard_swipe && !((kc >= 48 && kc <=57) || (kc >= 96 && kc <= 105) || kc == 13)) //Allow numbers only and enter
	            {
	            	console.log('numbers only');
            		event.preventDefault();
            	}
            	else if (kc == 186 /*semi-colon*/ || kc == 187 /*equal sign*/|| kc == 191 /*forward slash*/|| (event.shiftKey && kc == 53) /*percentage sign*/) 
	            {
	            	console.log('blocking special characters');
	            	giftcard_swipe = true;
	                event.preventDefault(); 
	            }    
	        }
		});
		//gcn.focus();
	})
</script>
<?php } ?>