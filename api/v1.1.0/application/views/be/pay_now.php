<style>
	#terms_and_conditions {
		width:495px;
		height: 80px;
		border: 1px #EDEDED solid;
		color: #ADADAD;
		font-size: 12px;
		padding: 1px 4px;
		text-align:left;
	}
	#terms_and_conditions_accept {
		margin-top:12px;
	}
	.agree_text {
		font-size:12px;
	}
	.agree_text div {
		text-align:center;
	}
	.header {
		font-size:18px;
		margin-top:5px;
		margin-bottom:15px;
	}
	.player_table {
		text-align:center;
	}
	.first_col {
		text-align:left;
	}
	.centered_col {
		text-align:center;
	}
	.vertical_spacer {
		height:20px;
		width:100px;
		display:block;
	}
	button {
		margin:auto;
		width:60px;
		height:27px;
	}
	button.continue_button {
		margin:5px;
		width:135px;
		height:27px;
		background-color:green;
	}
	.continue_button span.ui-button-text {
		font-size:12px;
	}
	#cboxContent .left_column {
		width:265px;
		float:left;
	}
	#cboxContent .right_column {
		width:230px;
		float:right;
		padding:20px 5px 0px 0px;
		margin-top:20px;
	}
	.right_column .step_text {
		font-size:16px;
		color:orange''
	}
	.right_column .step_subtext {
		font-size:14px;
	}
	.left_column .small_note {
		font-size:10px;
		padding-left:10px;
	}
	div#player_button_set {
		float:left;
	}
	#player_button_set .ui-button-text-icon-primary .ui-button-text {
		padding:.4em .4em .4em 2em;
	}
	#cboxContent #booking_basic_info {
		width:335px;
	}
	#cboxContent .left_column {
		width:365px;
	}
	.ui-state-active {
		border-right: none;
		box-shadow: inset -2px 2px 30px 3px black;
	}
	#cboxContent button.continue_button {
		margin-bottom:10px;
	}
</style>
<?php 
if ($available_for_purchase) 
{
?>
<div class='left_column'>
	<fieldset id="booking_basic_info" class="ui-corner-all ui-state-default login_info">
				    <div class='reservation_info'>
				    	<table>
				    		<tbody>
				    			<tr>
				    				<td colspan=2 class='details_header'>Booking Details</td>
				    			</tr>
				    			<tr>
				    				<td class='detail_label'>Date:</td>
				    				<td><input type="hidden" value ="<?php echo $start?>" id="hidden_start"/>
				                <?php echo date('m/d/Y', strtotime($teetime_info->start+1000000))?></td>
				    			</tr>
				    			<tr>
				    				<td class='detail_label'>Time:</td>
				    				<td><?php echo date('g:i a', strtotime($teetime_info->start+1000000))?></td>
				    			</tr>
				    			<tr>
				    				<td class='detail_label'>Holes:</td>
				    				<td><input type="hidden" value ="<?php echo $teetime_info->holes?>" id="hidden_holes"/>
				                <?php echo $teetime_info->holes?></td>
				    			</tr>
				    			<tr>
				    				<td class='detail_label'>Players:</td>
				    				<td><div id="player_button_set" class="be_reserve_buttonset">
									    <?php 
						            	$minimum_players = $this->session->userdata('minimum_players');
						            	if ($minimum_players < 3) { ?>
						                <input type="radio" id="player_2" value="2" name="player_button" <?php echo ($player_count < 3)?'checked':'';?>/><label for="player_2">2</label>
						            	<?php }
						            	if ($minimum_players < 4) { ?>
									    <input type="radio" id="player_3" value="3" name="player_button" <?php echo ($player_count == 3)?'checked':'';?>/><label for="player_3">3</label>
										<?php } ?>
									    <input type="radio" id="player_4" value="4" name="player_button" <?php echo ($player_count == 4)?'checked':'';?>/><label for="player_4">4</label>
									    <?php //echo $teetime_info->player_count?>
									</div></td>
				    			</tr>
				    			<tr>
				    				<td class='detail_label'><div>Green Fee:</div>	<span class='small_note'>With Cart</span></td>
				    				<td><div class="price_box">$<?=$single_price?></div><span class='small_note'>$<?=number_format($single_price+$single_cart_price,2)?></span></td>
				    			</tr>
				    			<tr>
				    				<td class='detail_label'>Include Carts:</td>
				    				<td>
				    					<div id="cart_button_set" class="be_reserve_buttonset">
					    					<input type="radio" id="include_carts_1" value="1" name="include_carts" checked/><label for="include_carts_1">Yes</label>
											<input type="radio" id="include_carts_0" value="0" name="include_carts"/><label for="include_carts_0">No</label>
										</div>
									</td>
				    			</tr>
				    			<tr>
				    				<td class='detail_label'>Subtotal:</td>
				    				<td><div class="price_box">$<span class="subtotal_price"><?php echo number_format(($single_price+$single_cart_price)*$player_count, 2)?></span></div></td>
				    			</tr>
				    			<tr>
				    				<td class='detail_label'>Discount:</td>
				    				<td><div class="price_box">-$<span class="discount_price"><?php echo number_format(($single_price+$single_cart_price)*$player_count*$course_info->foreup_discount_percent/100, 2)?></span></div></td>
				    			</tr>
				    			<tr>
				    				<td class='detail_label'>Total:</td>
				    				<td><div class="price_box">$<span class="final_total_price"><?php echo number_format(($single_price+$single_cart_price)*$player_count*(100-$course_info->foreup_discount_percent)/100, 2)?></span></div></td>
				    			</tr>
				    		</tbody>
				    	</table>
				</fieldset>
	
	<div class="clear"></div>
	<div>
		<?php if ($logged_in) {
	    $js = 'onClick="reservations.book_tee_time();" class="login_button"';
	    echo form_button('login_button','Reserve', $js); 
	    
	    }?>
	</div>
</div>
<div class='right_column'>
	<span class='step_text'>1. Select how many players</span><br/>
	<span class='step_subtext'>(Minimum of 2 for discount)</span><br/><br/>
	<span class='step_text'>2. Select if you want carts</span><br/>
	<span class='step_subtext'>(Get a discount on carts now too!)</span><br/><br/>
	<span class='step_text'>3. Agree to the <a target='_blank' href='index.php/be/terms_and_conditions'>terms and conditions</a></span><br/><br/>
	<span class='step_text'>4. Click continue and pay!</span><br/>
</div>
<div class='clear'></div>
<div>
	<div>
		<table class='player_table'>
			<tbody>
				<tr>
					<td  class='first_col agree_text' colspan=4>
						<div>
							<input type=checkbox value=0 name='terms_and_conditions_accept' id='terms_and_conditions_accept'/> I agree to the <a target='_blank' href='index.php/be/terms_and_conditions'>terms and conditions</a>
						</div>
						<div>
							<button onclick="pay_now.continue_paying()" class='continue_button'>Continue & Pay</button>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<?php 
}
else {
	$this->load->view('be/sorry_not_available');
}
?>
<script>
	$(document).ready(function(){
		<?php if (!$available_for_purchase) { ?>
			booking.hide_pay_now_info();
		<?php } ?>
		$('.continue_button').button();
        $('input:radio[name=player_button], input:radio[name=include_carts]').click(function(){
	        pay_now.update_prices();
	    })
	    $('#player_2').button();
	    $('#player_3').button();
	    $('#player_4').button();
	    $('#player_button_set').buttonset();
        $('#cart_button_set').buttonset();
    
	})
</script>