<div class="column">
    <!--div id="weather_box" class="ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display: block;">
        <div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all">
             Weather
        </div>
        <div id='weather'>
    		Please wait for weather to load.
    	</div>
    </div-->
   <?php if ($available_deals != '') {?>
       <div id="available_deals" class="ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display: block;">
            <div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all">
                 Available Deals
            </div>
            <div id='available_deals_content'>
        		<?php echo $available_deals; ?>
        	</div>
        </div>
    <?php } ?>
    <div id="booking_rules" class="ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display: block;">
        <div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all">
             Booking Rules
        </div>
        <div id='booking_rules_content'>
    		<?php echo html_entity_decode($course_info->booking_rules); ?>
    	</div>
    </div>
</div>