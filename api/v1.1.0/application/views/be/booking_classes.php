<?php
$this->load->view("partial/course_header", array('booked'=>$booked));
?>
<script>
</script>
<style>
    html {
		background: url(<?php echo base_url(); ?>images/backgrounds/course.png)no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		font-family: Quicksand, Helvetica, Arial, sans-serif;
	}
	#content_area_wrapper {
		min-height:0;
	}
	.ui-widget {
        font-size:.8em;
    }
    .ui-buttonset .ui-button {
        margin-right:-0.5em;
        height:28px;
        padding:0px 5px 0px;
    }
    #slider {
        margin: 10px 10px 10px 20px;
        width:250px;
    }
    .ui-slider-handle {
        font-size:.8em;
        padding:1px 0 0 2px;
    }
    .tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	clear: both;
	float: left;
        width: 959px;
	background: #fff;
        height:400px;
}
.tab_content {
	padding:10px 20px;
	font-size: 1.2em;
        height:330px;
        overflow: auto;
}
    ul.tabs {
	margin: 0;
	padding: 0;
	float: left;
	list-style: none;
	height: 46px; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 959px;
}
ul.tabs li {
	float: left;
	margin: 0;
	padding: 0;
	height: 45px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 45px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	overflow: hidden;
	position: relative;
	background: #e0e0e0;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: .9em;
	padding: 0 10px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
        width:114px;
        text-align: center;
        line-height: 31px;
}
ul.tabs li a div {
    height:12px;
}
ul.tabs li a span {
    font-size:.7em
}
ul.tabs li a:hover {
	background: #ccc;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #fff;
	border-bottom: 1px solid #fff; /*--Makes the active tab look like it's connected with its content--*/
}
#time_button_set, #hole_button_set {
    float:left;
    margin:10px 0 0 16px;
}
#player_button_set {
    float:right;
    margin:10px 21px 0 0;
}
.search_controls {
    padding:0px 5px 10px;
    border-bottom: 1px solid #ddd;
}
.teetime {
    border:1px solid #dedede;
    padding:8px 15px;
    margin-bottom:5px;
}
.teetime .left {
    float:left;
    width:10%;
}
.teetime .center {
    float:left;
    width:75%;
    text-align: center;
}
.teetime .right {
    float:right;
    width:11%;
    margin-top:5px;
}
.teetime .left .players span {
    float: left;
}
.teetime .left .players span.ui-button-text {
    font-size: .6em;
    margin: 1px 0 0 2px;
}
.loading_golf_ball {
    font-size:14px;
    padding-top:94px;
    text-align:center;
}
.be_header {
    color:#336699;
    margin:15px auto 5px;
    text-align: center;
}
.be_form {
    width:65%;
    margin:auto;

}
#employee_basic_info.login_info {
    margin-right:5px;
}
.register_form {
    width:85%;
}
.be_form td input {
    margin-top:3px;
    margin-left:5px;
}
.be_fine_print {
    color:#333333;
    font-size:11px;
    text-align:center;
    margin-top:5px;
}
.be_form td hr {
    color:#336699;
    margin:11px 0px 7px;
}
.be_reserve_buttonset {
    float:left;
    margin-right:22px;
}
.be_buttonset_label {
    line-height: 30px;
    color:#336699;
    margin-right:10px;
}
#TB_ajaxContent .teetime .course_name {
    font-size:1.7em;
    margin-top:6px;
}
#TB_ajaxContent .teetime .info {
    padding-left:10px;
    margin-top:7px;
}
#TB_ajaxContent .teetime .time, #TB_ajaxContent .teetime .holes, #TB_ajaxContent .teetime .date {
    color:#336699;
    font-size:1.3em;
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label, #content_area .teetime .spots {
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label {
    font-size:.6em;
}
#content_area .teetime .price_label {
    margin-right:2px;
    padding-right:0px;
    line-height:19px;
}
#TB_ajaxContent .teetime .info .ui-icon {
    float:left;
    margin-right:5px;
}
#content_area .teetime .ui-icon {
    float:left;
    margin-right:5px;
}
#TB_ajaxContent .teetime .reservation_info {
    float:left;
    width:70%;
}
#TB_ajaxContent .teetime .price_box {
    float:right;
    text-align: right;
    width:27%;
    font-size:3.2em;
    line-height: 50px;
}
#content_area .teetime .price_details {
    padding-left: 135px;
}
#course_name {
    font-size:2em;
    text-align:center;
}
#home_module_list {
    text-align:left;
    width:900px;
    min-height:350px;
}
#content_area {
	width:900px;
}
#menubar_background, #menubar_full{
	background:none;
}
.booking_details {
	float:left;
	width:33%;
}
.reservation_info table {
	text-align:left;
}
.reservation_info table td {
	font-size:16px;
	padding:0px 0px 18px 7px;
}
.right_column {
	float:right;
	width:445px;
	font-size:14px;
	color:#5d5d5d;
	margin-right:5px;
}
button {
	width:70px;
	cursor:pointer;
}
.right_column button, button.continue_button {
	width:140px;
		background: #349ac5; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3'); /* for IE */
	background: -webkit-linear-gradient(top, #349AC5, #4173B3);
	background: -moz-linear-gradient(top,  #349AC5,  #4173B3); /* for firefox 3.6+ */
	color: white;
	display: block;
	font-size: 14px;
	font-weight: normal;
	height: 30px;
	line-height:30px;
	padding: 0px 0 0 10px;
	width: 180px;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	margin: 10px auto -25px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5),
				0px 3px 1px -2px rgba(255, 255, 255, .2),
				0px 0px 0px 10px white;
	border: 1px solid #232323;
}
.price_tag {
	background:url('../../images/pieces/price_tag.png') no-repeat 143px 0px transparent;
	height:64px;
	display:block;
	padding-left:16px;
	line-height:67px;
	font-size:28px;
	color:white;
}
.price_tag_pretext, .price_tag_posttext {
	font-size:20px;
	color:black;
}
.price_tag_pretext {
	padding-right:22px;
}
.price_tag_posttext {
	padding-left:13px;
}
#booking_basic_info legend {
	text-align:left;
}
.no_thanks {
	font-size:14px;
}
#pay_now_info legend {
	text-align: center;
	color: #3E6E97;
	font-weight: bold;
	font-size: 18px;
}
#pay_now_info {
	color: #5D5D5D;
	font-weight: normal;
	width: 400px;
	margin: 20px auto;
	padding-bottom:10px;
	background:transparent;
}
#pay_now_info span.no_thanks_link {
	font-size: 12px;
	color: #5D5D5D;
	pointer:cursor;
}
#booking_basic_info legend {
	font-size:20px;
}
.minimum_note {
	font-size:12px;
}
#confirmation_box_header {
	color: white;
	background: url(<?php echo base_url(); ?>images/backgrounds/top_bar_bg_black.png) repeat-x;
	height: 37px;
	line-height: 37px;
	font-size: 18px;
	text-align: left;
	text-indent: 15px;
	box-shadow:0px 1px 5px -1px black;
}
#confirmation_box {
	background: white;
	box-shadow: 0px 1px 13px -3px black;
}
#booking_basic_info {
	border:none;
	background:#ddd;
	padding:10px 10px 0px;
	margin-left:10px;
	margin-top:25px;
	margin-bottom:25px;
	width:385px;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
	font-weight: normal;
	color: #333;
}
.detail_label {
	font-weight:bold;
}
.details_header {
	font-size:16px;
	font-weight:bold;
}
.pay_now_box {
	text-align:center;
}
.thank_you {
	font-size:16px;
	font-weight:bold;
	margin:12px 0px 3px;
}
.facebook_share {
	margin: 10px 0px;
}
.booking_class_button {
	float:left;
	width:130px;
	font-size:20px;
	color:white;
	background:#336699;
	border-radius:5px;
	text-align:center;
	padding:30px 50px;
	margin:10px;
	cursor:pointer;
}
#home_module_list {
	width: 520px;
	margin: 80px auto;
}
<?php if ($this->session->userdata('iframe')) { 
	// STYLES FOR AN IFRAME VERSION OF ONLINE BOOKING
	?>
#content_area {
	padding:0px;
	width:760px;
}
#content_area_wrapper {
	min-height: 540px;
}
.column {
	float:none;
	width:auto;
	margin:0px;
}
.wrapper {
	width:758px;
	margin:0px auto;
}
ul.tabs {
	width:753px;
}
#home_module_list {
	width:758px;
	margin-left:0px;
	float:none;
	min-height:350px;
}
.tab_container {
	width:756px;
}
.tab_content {
	padding:0px 10px;
}
.teetime {
	padding:8px 10px;
}
#time_button_set, #hole_button_set {
	margin: 10px 30px 0 15px;
}
div.dropdown-menu > a, #signin {
	font-size: 14px;
}
#course_name, span.customStyleSelectBox {
	font-size:12px;
}
.menu_item img {
	height: 30px;
}
#menubar_background, #menubar_full {
	padding:0px;
}
#teesheet_name, #course_name {
	line-height: 30px;
	width:100%;
	float:none;
	margin-top:12px;
	font-size:24px;
}
#navigation_menu {
	margin: 5px 15px 5px 10px;
}
#menubar_background, #menubar_full {
	display: none;
}
div.ui-datepicker {
	width: auto;
	margin-bottom: 0px;
}
.congratulations {
	margin-top: 25px;
}
.right_column {
	width: 315px;
	text-align:center;
}
#pay_now_info {
	width: 265px;
	margin: 10px auto 30px;
}
.be_fine_print {
	padding: 0px 40px;
}
#confirmation_email {
	width: 200px;
	margin: auto;
}
#for_questions {
	width: 200px;
	margin: auto;
	margin-top: 10px;
}
<?php } ?>
</style>
<div id="course_name"><?php echo $course_info->name?></div>
<div id="thank_you_box"></div>
<div id="home_module_list">
	<div>Please select from one of the following reservation booking portals:</div>
 <?php foreach ($booking_classes as $booking_class) { ?>
 	<a href='index.php/be/select_booking_class/<?=$booking_class['booking_class_id']?>'><div class='booking_class_button'><?=$booking_class['name']?></div></a>
 <?php }?>
</div>
<?php //$this->load->view("partial/course_footer"); ?>