var ModifierCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		return new Modifier(attrs, options);
	},

	getTotalPrice: function(){
		var total = 0.00;

		if(this.models.length > 0){
			_.each(this.models, function(modifier){
				total += parseFloat(modifier.get('selected_price'));
			});
		}
		return total;
	},

	// Checks if all required modifiers are set
	isComplete: function(categoryId){
		var complete = true;

		_.each(this.models, function(modifier){
			if(categoryId && modifier.get('view_category') != categoryId){ return true; }
			if(modifier.get('required') && (!modifier.get('selected_option') || modifier.get('selected_option') == 'no')){
				complete = false;
			}
		});

		return complete;
	}
});