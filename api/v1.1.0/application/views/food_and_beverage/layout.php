<style>
#edit_layout {
	color: white;
	padding: 5px;
	display: block;
	text-shadow: 1px 1px 1px #333;
}
</style>
<script>
$(function(){
	// Switch tables
	$('body').off('click', 'div.object').on('click', 'div.object', function(e){

		$('#layout_editor').mask('Please wait...');
		var table = $(this);
		var url = 'index.php/api/food_and_beverage/service/' + table.data('table');

		// Load table data
		$.get(url, null, function(response){
			// Reset API url with new table number
			App.table_num = response.table_num;
			App.api_table = App.api_root + App.table_num + '/';
			App.cart.url = App.api_table + 'cart';
			App.receipts.url = App.api_table + 'receipts';

			// Apply new data to cart and receipts
			App.cart.reset(response.cart);
			App.receipts.reset(response.receipts);

			$('#menubar_stats').html('Table #' + App.table_num);

			// Close table select window
			$.colorbox.close();
			$(document).scrollTop(0);
			reload_tables();

		}, 'json').always(function(){
			$('#layout_editor').unmask();
		});

		return false;
	});

	if($('div.floor_layout > div.object').data('draggable')){
		$('div.floor_layout > div.object').draggable( "destroy" );
	}

	if($('div.floor_layout > div.object').data('resizable')){
		$('div.floor_layout > div.object').resizable( "destroy" );
	}

	$('#layout_editor ul.tabs li.tab').find('a').unbind('click').click(function(e){
		$(this).parents('ul.tabs').find('li').removeClass('active');
		$(this).parent('li').addClass('active');
		var target = $(this).attr('href');
		$(target).siblings().hide();
		$(target).show();
		return false;
	});

	$('#edit_layout').colorbox({
		onComplete: function(){
			initLayoutEditor();
		},
		onClosed: function(){
			if($('div.floor_layout > div.object').data('uiDraggable')){
				$('div.floor_layout > div.object').draggable( "destroy" );
			}

			if($('div.floor_layout > div.object').data('uiResizable')){
				$('div.floor_layout > div.object').resizable( "destroy" );
			}
		}
	});

	$('div.object').find('*').unbind('click');
});
</script>
<div id="layout_editor">
	<ul class="tabs">
		<?php
		$active = 'active';
		foreach($layouts as $layout){ ?>
		<li class="tab <?php echo $active; ?>">
			<a href="#layout_<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></a>
		</li>
		<?php $active = ''; } ?>
		<li style="float: right;"><a class="colbox" href="<?php echo site_url('food_and_beverage/edit_layout'); ?>" id="edit_layout">Edit Layouts</a></li>
	</ul>

	<div class="tab_content">
	<?php
	if(!empty($layouts)){
	$active = 'active';
	foreach($layouts as $layout){ ?>
		<div id="layout_<?php echo $layout['layout_id']; ?>" class="floor_layout <?php echo $active; ?>" data-layout-id="<?php echo $layout['layout_id']; ?>">
		<?php foreach($layout['objects'] as $object){

		$activeClass = '';
		$table_attr = 'data-table="'.$object['label'].'"';
		$employee_details = '';

		if(!empty($object['employee_id'])){
			if($object['employee_id'] == $employee_id){
				$activeClass = ' my-active';
			}else{
				$activeClass = ' other-active';
			}

			$employee_details = '<span class="employee">'.$object['employee_first_name'].' '.strtoupper($object['employee_last_name'][0]).'. </span>';
		}

		$objectSize = '';
		if(!empty($object['width'])){
			$objectSize .= 'width: '.$object['width'].'px; ';
		}
		if(!empty($object['height'])){
			$objectSize .= 'height: '.$object['height'].'px; ';
		}
		?>
			<div class="object <?php echo $activeClass; ?>" <?php echo $table_attr; ?> data-type="<?php echo $object['type']; ?>" data-object-id="<?php echo $object['object_id']; ?>" style="position: absolute; left: <?php echo $object['pos_x']; ?>px; top: <?php echo $object['pos_y']; ?>px; <?php echo $objectSize; ?>;">
				<?php if($object['type'] == 'box'){ ?>
				<div class="box"></div>
				<?php }else{ ?>
				<?php include('images/restaurant_layout/'.$object['type'].'.svg'); ?>
				<?php } ?>
				<span class="label"><?php echo $object['label']; ?></span>
				<?php echo $employee_details; ?>
			</div>
		<?php } ?>
		</div>
	<?php $active = ''; }
	}else{ ?>
		<h1 style="text-align: center; display: block; color: #AAA; margin-top: 75px;">No tables available</h1>
	<?php } ?>
	</div>
</div>
