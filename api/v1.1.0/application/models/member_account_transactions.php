<?php
class Member_account_transactions extends CI_Model 
{
	function save($person_id = -1, $trans_description='', $add_subtract='', $trans_details='', $employee_id = false)
	{
		$employee_id=$employee_id?$employee_id:$this->Employee->get_logged_in_employee_info()->person_id;
		if ($add_subtract == '')
			$add_subtract = $this->input->post('add_subtract');
		if ($trans_description == '')
			$trans_description = $this->input->post('trans_comment');
		$manual_edit = $this->input->post('manual_edit');
		
		// CHECK TO SEE IF CUSTOMER IS PART OF A HOUSEHOLD. IF SO, CHARGE TO  CARD
        $this->load->model('Household');
        if (!$manual_edit && $this->Household->is_member($person_id))
        {
            $head = $this->Household->get_head($person_id);
            $cur_customer_info = $this->Customer->get_info($head['person_id']);
            $trans_household = $head['person_id'];
        }
        else
        {
            $cur_customer_info = $this->Customer->get_info($person_id);
            $trans_household  = $person_id;
        }
		
		$trans_data = array
		(
			'trans_date'=>date('Y-m-d H:i:s'),
			'trans_customer'=>$person_id,
			'trans_household'=>$trans_household,
            'trans_user'=>$employee_id,
			'trans_comment'=>$trans_description,
			'trans_description'=>$trans_details,
			'trans_amount'=>$add_subtract
		);
		
		//Update account balance
		$customer_data = array(
			'member_account_balance'=>$cur_customer_info->member_account_balance + $add_subtract
		);
		
		$this->db->where('person_id', $trans_household);
		$result = $this->db->update('customers', $customer_data);
		
		if ($result) 
		{
			$this->insert($trans_data);
			return true;
		}
		else 
			return false;
	}	
	
	function insert($member_account_transaction_data)
	{
		if ($member_account_transaction_data['trans_customer'] !== null)
			return $this->db->insert('member_account_transactions',$member_account_transaction_data);
		else 
			return false;
	}
	function get_member_account_transaction_data_for_customer($customer_id = false, $month = false, $date = false)
	{
		$month_sql = "";
		$table_join = '';
		$customer_sql = '';
		if ($customer_id)
		{
			$customer_sql = " AND (`trans_customer` = '$customer_id' OR `trans_household` = '$customer_id') ";
		}
		else
		{
			$table_join = ' LEFT JOIN foreup_customers ON foreup_customers.person_id = foreup_member_account_transactions.trans_customer LEFT JOIN foreup_people ON foreup_people.person_id = foreup_customers.person_id ';
			$customer_sql = " AND foreup_customers.`course_id` = '{$this->session->userdata('course_id')}' ";
			$date = $date ? $date : date('Y-m-d');
			$month_sql = " AND trans_date > '".date('Y-m-d 00:00:00', strtotime($date))."' AND trans_date < '".date('Y-m-d 23:59:59', strtotime($date))."' ";
		}
		if ($month)
			$month_sql = " AND trans_date > '".date('Y-m-01 00:00:00', strtotime($month.' -1 month'))."' AND trans_date < '".date('Y-m-01 00:00:00', strtotime($month))."' ";
		$result = $this->db->query("SELECT *, DATE_FORMAT(trans_date, '%c-%e-%y %l:%i %p') AS date FROM (`foreup_member_account_transactions`) $table_join WHERE trans_amount != 0 $customer_sql $month_sql ORDER BY `trans_date` desc");
		return $result->result_array();		
	}
	function get_month_total($customer_id = false, $month = false)
	{
		if (!$customer_id || ! $month)
			return 0;
		
		$result = $this->db->query("
			SELECT SUM(trans_amount) AS total 
			FROM foreup_member_account_transactions 
			WHERE (`trans_household` = '$customer_id')  
				AND trans_date > '".date('Y-m-01 00:00:00', strtotime($month.' -1 month'))."' 
				AND trans_date < '".date('Y-m-01 00:00:00', strtotime($month))."' 
				AND  trans_amount < 0")->row_array();
				
		return $result['total'];
	}
}

?>