<?php
class Supplier extends Person
{	
	/*
	Determines if a given person_id is a customer
	*/
	function exists($person_id)
	{
		$this->db->from('suppliers');	
		$this->db->join('people', 'people.person_id = suppliers.person_id');
		$this->db->where('suppliers.person_id',$person_id);
		$this->db->limit(1);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}
	
	/*
	Returns all the suppliers
	*/
	function get_all($limit=10000, $offset=0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND foreup_suppliers.course_id = '{$this->session->userdata('course_id')}'";
        $this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');			
		$this->db->where("deleted = 0 $course_id");
		//$this->db->order_by("last_name", "asc");
		$this->db->order_by("company_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();		
	}
	
	function count_all()
	{
		$course_id = '';
                if (!$this->permissions->is_super_admin())
                    $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
                $this->db->from('suppliers');
		$this->db->where("deleted = 0 $course_id");
		return $this->db->count_all_results();
	}
	
	/*
	Gets information about a particular supplier
	*/
	function get_info($supplier_id)
	{
		$course_id = '';
                if (!$this->permissions->is_super_admin())
                    $course_id = "AND foreup_suppliers.course_id = '{$this->session->userdata('course_id')}'";
                $this->db->from('suppliers');	
		$this->db->join('people', 'people.person_id = suppliers.person_id');
		$this->db->where("foreup_suppliers.person_id = '$supplier_id' $course_id");
		$this->db->limit(1);
		$query = $this->db->get();
		
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $supplier_id is NOT an supplier
			$person_obj=parent::get_info(-1);
			
			//Get all the fields from supplier table
			$fields = $this->db->list_fields('suppliers');
			
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			
			return $person_obj;
		}
	}
	
	/*
	Gets information about multiple suppliers
	*/
	function get_multiple_info($suppliers_ids)
	{
		$this->db->from('suppliers');
		$this->db->join('people', 'people.person_id = suppliers.person_id');		
		$this->db->where_in('suppliers.person_id',$suppliers_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();		
	}
	
	/*
	Inserts or updates a suppliers
	*/
	function save(&$person_data, &$supplier_data,$supplier_id=false)
	{
		$success=false;
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		
		if(parent::save($person_data,$supplier_id))
		{
			if (!$supplier_id or !$this->exists($supplier_id))
			{
				$supplier_data['person_id'] = $person_data['person_id'];
				$success = $this->db->insert('suppliers',$supplier_data);				
			}
			else
			{
				$this->db->where('person_id', $supplier_id);
				$success = $this->db->update('suppliers',$supplier_data);
			}
			
			
		}
		
		$this->db->trans_complete();		
		return $success;
	}
	
	/*
	Deletes one supplier
	*/
	function delete($supplier_id)
	{
		$course_id = '';
                if (!$this->permissions->is_super_admin())
                    $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
                $this->db->where("person_id = '$supplier_id' $course_id");
		return $this->db->update('suppliers', array('deleted' => 1,'account_number'=>null));
	}
	
	/*
	Deletes a list of suppliers
	*/
	function delete_list($supplier_ids)
	{
		if (!$this->permissions->is_super_admin())
                    $this->db->where('course_id', $this->session->userdata('course_id'));
                $this->db->where_in('person_id',$supplier_ids);
		return $this->db->update('suppliers', array('deleted' => 1,'account_number'=>null));
 	}
 	
 	/*
	Get search suggestions to find suppliers
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND foreup_suppliers.course_id = '{$this->session->userdata('course_id')}'";
        $suggestions = array();
		
		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');	
		$this->db->where("deleted = 0 $course_id");
		$this->db->like("company_name",$search);
		$this->db->order_by("company_name", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)
		{
			$suggestions[]=array('label' => $row->company_name, 'value'=>$row->person_id);		
		}

		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');	
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label' => $row->first_name.' '.$row->last_name, 'value'=>$row->person_id);		
		}
		
		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');	
		$this->db->where("deleted = 0 $course_id");
		$this->db->like("email",$search);
		$this->db->order_by("email", "asc");		
		$by_email = $this->db->get();
		foreach($by_email->result() as $row)
		{
			$suggestions[]=array('label' => $row->email, 'value'=>$row->person_id);		
		}

		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');	
		$this->db->where("deleted = 0 $course_id");
		$this->db->like("phone_number",$search);
		$this->db->order_by("phone_number", "asc");		
		$by_phone = $this->db->get();
		foreach($by_phone->result() as $row)
		{
			$suggestions[]=array('label' => $row->phone_number, 'value'=>$row->person_id);		
		}
		
		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');	
		$this->db->where("deleted = 0 $course_id");
		$this->db->like("account_number",$search);
		$this->db->order_by("account_number", "asc");		
		$by_account_number = $this->db->get();
		foreach($by_account_number->result() as $row)
		{
			$suggestions[]=array('label' => $row->account_number, 'value'=>$row->person_id);		
		}
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	
	}
	
	/*
	Get search suggestions to find suppliers
	*/
	function get_suppliers_search_suggestions($search,$limit=25)
	{
		$course_id = '';
	    if (!$this->permissions->is_super_admin())
	        $course_id = "AND foreup_suppliers.course_id = '{$this->session->userdata('course_id')}'";
	    $suggestions = array();
		
		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');	
		$this->db->where("deleted = 0 $course_id");
		$this->db->like("company_name",$search);
		$this->db->order_by("company_name", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)
		{
			$suggestions[]=array('value' => $row->person_id, 'label' => $row->company_name);	
		}


		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');	
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('value' => $row->person_id, 'label' => $row->first_name.' '.$row->last_name);	
		}
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	/*
	Perform a search on suppliers
	*/
	function search($search, $limit=20, $offset = 0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND foreup_suppliers.course_id = '{$this->session->userdata('course_id')}'";
        $this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		company_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		email LIKE '%".$this->db->escape_like_str($search)."%' or 
		phone_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		account_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");		
		$this->db->order_by("last_name", "asc");
        // Just return a count of all search results
        if ($limit == 0)
            return $this->db->get()->num_rows();
        // Return results
        $this->db->offset($offset);
		$this->db->limit($limit);
		return $this->db->get();	
	}
    function find_supplier_id($search)
	{
		$course_id = '';
        //if (!$this->permissions->is_super_admin())
        //   $course_id = "AND foreup_suppliers.course_id = '{$this->session->userdata('course_id')}'";
        $this->db->select("suppliers.person_id");
		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		company_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		email LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");		
		$this->db->order_by("last_name", "asc");
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			return $query->row()->person_id;
		}
		
		return null;
	}


}
?>
