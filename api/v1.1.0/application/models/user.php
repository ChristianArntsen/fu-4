<?php
class User extends Person
{
	/*
		Determines if a user exists
	*/
	function exists( $person_id )
	{
		$this->db->from('users');
		$this->db->where('person_id',$person_id);
		$this->db->where('deleted != 1');
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
		Gets information about a user base on username
	*/
	function get_by_username($username)
	{
		$query = $this->db->limit(1)->get_where('users', array('email' => $username, 'deleted' => 0), 1);
		return $query->row();
	}

	/*
		Returns all Users
	*/
	function get_all($limit=10000, $offset=0)
	{
//
        // $this->db->from('marketing_sendhub_accounts');
		// $this->db->where("deleted = 0 $course_id");
		// $this->db->order_by("send_date", "desc");
		// $this->db->limit($limit);
		// $this->db->offset($offset);
		// return $this->db->get();
	}

	function count_all()
	{
		// $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        // $this->db->from('marketing_sendhub_accounts');
		// $this->db->where("deleted = 0 $course_id");
		// return $this->db->count_all_results();
	}

	/*
		Gets information about a particular user
	*/
	function get_info($person_id)
	{
		$this->db->from('people AS p');
		$this->db->join('users AS u', 'p.person_id = u.person_id');
		$this->db->where('p.person_id', $person_id);
		$result = $this->db->get();

        $person_info = $result->row_array();

		if($person_info['birthday'] != '0000-00-00'){
			$person_info['birthday'] = date('m/d/Y', strtotime($person_info['birthday']));
		}

        return $person_info;
	}

	/*
	Gets information about multiple sendhub_accounts
	*/
	function get_multiple_info($sendhub_account_ids)
	{
		// $this->db->from('marketing_sendhub_accounts');
		// $this->db->where_in('sendhub_account_id',$sendhub_account_ids);
		// $this->db->where('deleted',0);
		// $this->db->order_by("send_date", "desc");
		// return $this->db->get();
	}

    function login($username, $password)
	{
        if ($this->permissions->is_super_admin()){
            $user_password = $password;
        }else{
            $user_password = md5($password);
        }

		$this->db->select('p.person_id, p.first_name, p.last_name, p.phone_number, p.email');
		$this->db->from('users AS u');
        $this->db->join('people AS p', 'u.person_id = p.person_id');
        $this->db->where('u.email', $username);
        $this->db->where('u.password', $user_password);
        $this->db->limit(1);
        $query = $this->db->get();

		// If credentials were valid
        if ($query->num_rows() == 1)
		{
			// Load user info into session
			$row = $query->row();
			$this->session->set_userdata('customer_id', $row->person_id);
			$this->session->set_userdata('first_name', $row->first_name);
			$this->session->set_userdata('last_name', $row->last_name);
			$this->session->set_userdata('phone_number', $row->phone_number);
			$this->session->set_userdata('customer_email', $row->email);
			$this->session->set_userdata('type', 'customer');

            return true;
		}

		return false;
	}

	/*
	function login($login_data)
	{
		$password = $login_data['password'];
		$facebook_id = $login_data['facebook_id'];
		$twitter_id = $login_data['twitter_id'];
		$email = $login_data['email'];

		$email_authenticated = false;

		$this->db->from('users');
		$this->db->where('email', $email);
		if($password != ''){
			$password = md5($password);
			$this->db->where("password = '$password'");
			$email_authenticated = true;
		}

		$response = $this->db->get();

		$success = false;

		if ($facebook_id != '' || $twitter_id != ''){
			$email_authenticated = true;

			//save the facebook or twitter id to the user table
			if ($response->num_rows > 0)
			{
				$user_data = $response->row_array();
				$person_id = $user_data['person_id'];
				$person_data = array(
					'person_id'=>$person_id
				);

				$user_data = array(
					'facebook_id'=> $facebook_id != '' ? $facebook_id : $user_data['facebook_id'],
					'twitter_id'=>$twitter_id != '' ? $twitter_id : $user_data['twitter_id']
				);
				$this->save($person_data, $user_data, $person_id);
			}

		}

		if ($response->num_rows > 0 && $email_authenticated) {
			$login_data['user_data'] = $response->row_array();
			if ($facebook_id != '') $login_data['user_data']['facebook_id'] = $facebook_id;
			if ($twitter_id != '') $login_data['user_data']['twitter_id'] = $twitter_id;
			$success = true;
		}

		return $success;
	}
	*/
	/*
		Determines if a given username exists in database
	*/
	function username_exists($username)
	{
		$this->db->from('users');
		$this->db->where('email', $username);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows() == 1);
	}

	function save($person_id, $user_data)
	{
		$success = false;
		$this->load->helper('array');

		if(empty($user_data)){
			return false;
		}

		$user_data = elements(array('email', 'password'), $user_data);

		if(empty($user_data['email'])){
			unset($user_data['email']);
		}
		if(empty($user_data['password'])){
			unset($user_data['password']);
		}

		if(empty($user_data)){
			return false;
		}

		// If user already exists, update record
		if($this->exists($person_id)){
			$this->db->where('person_id', $person_id);
			$success = $this->db->update('users', $user_data);

		// If no user yet, insert record
		}else{
			$user_data['person_id'] = $person_id;
			$success = $this->db->insert('users', $user_data);
		}

		return $success;
	}

	function add_to_course($personId, $courseId){

		$this->load->model('customer');

		if(empty($courseId) || empty($personId)){
			return false;
		}

		// Check if user is already a customer of course
		if(!$this->customer->exists($personId)){
			$this->db->insert('customers', array('person_id' => $personId, 'course_id'=>$courseId));
		}

		return true;
	}
	function update_password($person_id, $password)
	{
		$user_data = array('password' => $password);
		$this->db->where('person_id', $person_id);
		$success = $this->db->update('users',$user_data);

		return $success;
	}
	
	/*
	Updates multiple sendhub_accounts at once
	*/
	function update_multiple($sendhub_account_data,$sendhub_account_ids)
	{
		// $this->db->where_in('sendhub_account_id',$sendhub_account_ids);
		// return $this->db->update('marketing_sendhub_accounts',$sendhub_account_data);
	}

	/*
	Deletes one sendhub_account
	*/
	function delete($phone_number)
	{
        // $this->db->where("phone_number = $phone_number");
		// return $this->db->update('sendhub_accounts', array('deleted' => 1));
	}

	/*
	Deletes a list of sendhub_accounts
	*/
	function delete_list($sendhub_account_ids)
	{
		// $this->db->where('course_id', $this->session->userdata('course_id'));
        // $this->db->where_in('sendhub_account_id',$sendhub_account_ids);
		// return $this->db->update('marketing_sendhub_accounts', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find sendhub_accounts
	*/
	function get_search_suggestions($search,$limit=25)
	{
	}

	/*
	Preform a search on sendhub_accounts
	*/
	function search($search, $limit=20, $offset = 0)
	{
	}

	/*
		Check if a user is logged in
	*/
	function is_logged_in()
	{
		return ($this->session->userdata('customer_id') != false);// && $this->session->userdata('type') == 'customer');
	}

	/*
		Logs out a user by destorying all session data and redirect to login
	*/
	function logout()
	{
        $course_id = $this->session->userdata('course_id');
		$this->session->sess_destroy();
		redirect("be/reservations/$course_id");
	}

	/*
		Retrieves all course customer accounts that are linked to the user
	*/
	function get_customer_accounts($personId, $courseId = null){

		if(empty($personId)){
			return false;
		}

		$this->db->select('course.name AS course_name, course.course_id, course.address AS course_address, course.city AS course_city,
			course.state AS course_state, course.postal AS course_postal, cust.account_number, cust.account_balance,
			cust.member_account_balance, cust.loyalty_points, cust.opt_out_email, cust.opt_out_text,
			cust.course_news_announcements, cust.teetime_reminders');
	    $this->db->from('customers AS cust');
		$this->db->join('courses AS course', 'course.course_id = cust.course_id', 'inner');
		$this->db->join('people AS p', 'p.person_id = cust.person_id', 'inner');
		$this->db->where('cust.person_id', $personId);
		$this->db->where('cust.deleted', 0);
		$this->db->where('course.active', 1);

		if(!empty($courseId)){
			$this->db->where('cust.course_id', $courseId);
		}

		$this->db->order_by("course.name ASC");
		$customer_accounts = $this->db->get();

        return $customer_accounts->result_array();
	}

	/*
		Retrieves all teetimes the user has made
	*/
	function get_teetimes($personId, $type = 'upcoming', $courseId = null){

		if(empty($personId)){
			return false;
		}
		$personId = (int) $personId;

		$this->db->select('time.TTID, time.type, time.start, time.end, time.allDay, time.holes, time.carts, time.player_count,
			time.paid_player_count, time.paid_carts, time.clubs, time.title, time.cplayer_count, time.choles,
			time.ccarts, time.cpayment, time.phone, time.email, time.details, time.side,
			time.person_id, time.person_name,
			time.person_id_2, time.person_name_2,
			time.person_id_3, time.person_name_3,
			time.person_id_4, time.person_name_4,
			time.person_id_5, time.person_name_5,
			time.last_updated, time.date_booked, time.date_cancelled,
			time.booking_source, time.booker_id, time.canceller_id, time.teetime_id, time.teesheet_id,
			time.teed_off_time, time.turn_time, time.finish_time, course.name AS course_name,
			course.course_id AS course_id');
		$this->db->from('teetime AS time');
		$this->db->join('teesheet AS sheet', 'sheet.teesheet_id = time.teesheet_id', 'inner');
		$this->db->join('courses AS course', 'course.course_id = sheet.course_id', 'inner');
		$this->db->where("(time.booker_id = {$personId} || time.person_id = {$personId}
			|| time.person_id_2 = {$personId} || time.person_id_3 = {$personId}
			|| time.person_id_4 = {$personId} || time.person_id_5 = {$personId})");
		$this->db->where('sheet.deleted', 0);
		$this->db->where('time.status !=', 'deleted');

		if($type == 'upcoming'){
			$this->db->where("(time.start > ".time()." AND date_cancelled = '0000-00-00 00:00:00')");
		}

		if(!empty($courseId)){
			$this->db->where('sheet.course_id', $courseId);
		}

        $this->db->order_by('time.start ASC');
		$result = $this->db->get();
		$teetimes = $result->result_array();

		$lastId = '';
		// Loop through tee times
		foreach($teetimes as $key => $teetime){
			$players = array();

			// Filter out the back nine reservation when the tee time is for 18 holes
			if($teetime['holes'] == '18' && substr($teetime['TTID'], 0, 20) == substr($lastId, 0, 20)){
				unset($teetimes[$key]);
				continue;
			}

			// Organize peope into a sub array on each tee time
			if(!empty($teetime['person_id'])){
				$players[] = array('person_id' => $teetime['person_id'], 'person_name' => $teetime['person_name']);
			}
			if(!empty($teetime['person_id_2'])){
				$players[] = array('person_id' => $teetime['person_id_2'], 'person_name' => $teetime['person_name_2']);
			}
			if(!empty($teetime['person_id_3'])){
				$players[] = array('person_id' => $teetime['person_id_3'], 'person_name' => $teetime['person_name_3']);
			}
			if(!empty($teetime['person_id_4'])){
				$players[] = array('person_id' => $teetime['person_id_4'], 'person_name' => $teetime['person_name_4']);
			}
			if(!empty($teetime['person_id_5'])){
				$players[] = array('person_id' => $teetime['person_id_5'], 'person_name' => $teetime['person_name_5']);
			}
			$teetimes[$key]['players'] = $players;

			// Make unix timestamp from teetime start and end fields
			$teetimes[$key]['start_timestamp'] = strtotime($teetime['start'] + 1000000);
			$teetimes[$key]['end_timestamp'] = strtotime($teetime['end'] + 1000000);

			$lastId = $teetime['TTID'];
		}

		return $teetimes;
	}

	/*
		Retrieves all customer billing for user
	*/
	function get_customer_billing($personId, $courseId = null){

		if(empty($personId)){
			return false;
		}

		$this->db->select('course.name AS course_name, course.course_id, bill.billing_id, bill.title,
			bill.start_date, bill.pay_account_balance, bill.pay_member_balance, bill.email_invoice,
			bill.total, bill.description, bill.frequency');
	    $this->db->from('customer_billing AS bill');
		$this->db->join('courses AS course', 'course.course_id = bill.course_id', 'inner');
		$this->db->where('course.active', 1);

		if(!empty($courseId)){
			$this->db->where('bill.course_id', $courseId);
		}

		$this->db->order_by("course.name ASC");
		$customer_accounts = $this->db->get();

        return $customer_accounts->result_array();
	}

	/*
		Retrieves all user's giftcards
	*/
	function get_giftcards($personId, $courseId = null){

		if(empty($personId)){
			return false;
		}

		$this->db->select('gc.giftcard_id, gc.course_id, gc.giftcard_number, gc.value,
			gc.details, gc.expiration_date');
	    $this->db->from('foreup_giftcards AS gc');
		$this->db->where('gc.deleted', 0);
		$this->db->where('gc.customer_id', $personId);

		if(!empty($courseId)){
			$this->db->where('gc.course_id', $courseId);
		}

		$this->db->order_by("gc.expiration_date ASC");
		$giftcards = $this->db->get();

        return $giftcards->result_array();
	}

	/*
		Retrieves all purchases made by user
	*/
	function get_purchases($personId, $courseId = null){
		if(empty($personId)){
			return false;
		}

		$this->load->model('sale');
		$this->load->model('reports/detailed_sales');

		$this->sale->create_sales_items_temp_table(array('customer_id'=>$personId));
		$this->detailed_sales->params = array('department'=>'all', 'terminal'=>'all');
		$sales = $this->detailed_sales->getData();

        return $sales;
	}
}
