<?php
class Booking_class extends CI_Model
{
	/*
	Determines if a given person_id is a household head
	*/
	function get_all($teesheet_id)
	{
		$this->db->from('booking_classes');
		//$this->db->join('household_members', 'household_members.household_id = households.household_id', 'left');
		//$this->db->where("(household_head_id = {$person_id} OR household_member_id = {$person_id})");
		$this->db->where("(teesheet_id = {$teesheet_id})");
		$this->db->where('deleted', 0);
		//$this->db->limit(1);
        $result = $this->db->get()->result_array();
	//echo $this->db->last_query();
		return $result;
	}
	/*
	Gets information about a particular customer
	*/
	function get_info($booking_class_id, $teesheet_id)
	{
		$this->db->from('booking_classes');
		$this->db->where('booking_class_id',$booking_class_id);
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get all the fields from booking_classes table
			$fields = $this->db->list_fields('booking_classes');

			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$booking_class_obj->$field='';
			}

			return $booking_class_obj;
		}
	}
	function load_info($booking_class_id, $teesheet_id)
	{
		$booking_info = $this->get_info($booking_class_id, $teesheet_id);
		if ($booking_info->booking_class_id != '')
		{
			$this->session->set_userdata('booking_class_id', $booking_info->booking_class_id);
			$this->session->set_userdata('booking_class_info', $booking_info);
			
			return true;
		}
		else {
			return false;
		}
	}
	/*
	Save booking class
	*/
	function save($data, $booking_class_id = false)
	{
		if ($booking_class_id)
		{
			// UPDATE BOOKING CLASS
			$this->db->where('booking_class_id', $booking_class_id);
			$this->db->update("booking_classes", $data);
		}
		else 
		{
			// INSERT BOOKING CLASS
			$this->db->insert("booking_classes", $data);
		}
	}
	function delete($teesheet_id, $booking_class_id)
	{
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->where('booking_class_id', $booking_class_id);
		return $this->db->update('booking_classes', array('deleted'=>1));
	}
	
}
?>
