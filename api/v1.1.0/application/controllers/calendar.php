<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    // TO DO: to verify if this module should be secured or not

    $this->load->model('Event');
  }

  public function index()
  {
    $data['allowed_modules']=$this->Module->get_allowed_modules(0);

    $ui = new stdClass();
    $ui->first_name = 'John';
    $ui->last_name  = 'Doe';
    $data['user_info'] = $ui;
    $am = array('reservations', 'directories', 'calendar', 'account',
        'newsletter', 'settings', 'information');
    $data['modules'] = $am;
    $data['controller_name'] = __CLASS__;
    $data['contents'] = $this->load->view('user/calendar/manage', $data, true);
    $this->load->view("user/home", $data);
  }

  public function get_all()
  {
    $data = array();

    $events = $this->Event->get_all();

    if($events->num_rows() > 0)
    {
      foreach($events->result() as $event)
      {
        $data[] = array(
            'id' => $event->id,
            'title'  => $event->title,
            'start'  => $event->start_date,
            'end'  => $event->end_date,
            'details'  => nl2br($event->details)
        );
      }
    }

    echo json_encode($data);
  }
}