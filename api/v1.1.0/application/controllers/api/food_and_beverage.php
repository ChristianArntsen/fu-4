<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Food_and_beverage extends REST_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('table_receipt');
		$this->load->model('table_ticket');
		$this->load->library('sale_lib');
		$this->load->model('Employee');

		$this->suspended_sale_id = false;
		$this->table_num = false;
	}

	// Retrieves suspended sale ID for table number and checks if user
	// can access table
    private function _get_table($table_num){

		$suspended_sale_id = $this->Table->get_id_by_table_number($table_num);
		$employee_id = $this->session->userdata('person_id');

		// If table service (suspended sale) doesn't exist, create a new one
		if(empty($suspended_sale_id) && !empty($table_num)){
			$suspended_sale_id = $this->Table->save(array(), 0, $employee_id, '', array(), null, $table_num);
		}

		if(!$suspended_sale_id){
			$this->response(array('success'=>false, 'msg' => 'Table does not exist'), 404);
			return false;
		}

		$table_info = $this->Table->get_info($suspended_sale_id);
		$table_info = $table_info->row_array();

		// If employee isn't allowed to access table, return error
		if($table_info['employee_id'] != $this->session->userdata('person_id')){
			$this->response(array('success'=>false, 'msg' => 'You don\'t have permission to access that table'), 403);
			return false;
		}

		$this->suspended_sale_id = $suspended_sale_id;
		$this->table_num = $table_num;

		return true;
	}

    function index_get()
    {
        echo 'Food and beverage root';
    }

    /*******************************************************************
     * Service - Manages listing/opening/suspending sales on tables
	 ******************************************************************/
	function service_get($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		// If retrieving data for single table
		if(!empty($table_num)){

			$this->_get_table($table_num);

			// Route receipts
			if($method == 'receipts'){
				$this->receipts_get($recordId, $method2, $recordId2);
				return true;
			}

			// Route cart
			if($method == 'cart'){
				$this->cart_get($recordId);
				return true;
			}

			// Route kitchen orders
			if($method == 'orders'){
				$this->orders_get($recordId);
				return true;
			}

			// Retrieve table data along with all items, receipts and
			// payments associated with it
			$table_info = $this->Table->get_info($this->suspended_sale_id);
			$table_info = $table_info->row_array();

			$data['table_num'] = $table_info['table_id'];
			$data['employee_id'] = $table_info['employee_id'];
			$data['cart'] = $this->Table->get_items($this->suspended_sale_id);
			$data['receipts'] = $this->table_receipt->get($this->suspended_sale_id);
			$data['receipt_payments'] = $this->sale_lib->get_receipt_payments_total();

			$this->session->set_userdata('table_number', $data['table_num']);

			$this->response($data);
			return true;
		}

		// If getting list of available tables
		$tables = $this->Table->get_all(true)->result_array();
		$this->response($tables);
	}

	function service_post($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$this->_get_table($table_num);

		// Route receipts
		if($method == 'receipts'){
			$this->receipts_post($recordId, $method2, $recordId2);
			return true;
		}

		// Route cart
		if($method == 'cart'){
			$this->cart_post($recordId);
			return true;
		}

		// Route kitchen orders
		if($method == 'orders'){
			$this->orders_post($recordId);
			return true;
		}

		$this->response(array('success'=>true,'msg'=>'TODO: Open new ticket for table'));
	}

	function service_put($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$this->_get_table($table_num);

		// Route receipts
		if($method == 'receipts'){
			$this->receipts_put($recordId, $method2, $recordId2);
			return true;
		}

		// Route cart
		if($method == 'cart'){
			$this->cart_put($recordId);
			return true;
		}

		// Route kitchen orders
		if($method == 'orders'){
			$this->orders_put($recordId);
			return true;
		}

		$requestData = $this->request->Body;
		$this->response(array('success' => true,'msg' => 'TODO: Update table'));
	}

	function service_delete($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$this->_get_table($table_num);

		// Route receipts
		if($method == 'receipts'){
			$this->receipts_delete($recordId, $method2, $recordId2);
			return true;
		}

		// Route cart
		if($method == 'cart'){
			$this->cart_delete($recordId);
			return true;
		}

		// Route kitchen orders
		if($method == 'orders'){
			$this->orders_delete($recordId);
			return true;
		}

		// Check if some receipts still need to be paid
		if(!$this->table_receipt->is_paid($this->suspended_sale_id)){
			$this->response(array('success' => false, 'msg' => 'Error closing table service, all receipts must be paid first'), 400);
			return false;
		}

		$success = $this->Table->delete($this->suspended_sale_id);

		if(empty($success)){
			$this->response(array('success' => $success, 'msg' => 'Error closing table service'), 400);

		}else{
			$this->session->unset_userdata('table_number');
			$this->session->unset_userdata('suspended_sale_id');
			$this->response(array('success' => $success, 'msg' => 'Table service closed'));
		}
	}

    /*******************************************************************
     * Receipts
	 ******************************************************************/
	function receipts_get($receipt_id = false, $method = null, $record_id = null)
	{
		if($method == 'items'){
			$this->receipt_items_get($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_get($receipt_id, $record_id);
			return true;
		}

		$receipts = $this->table_receipt->get($this->suspended_sale_id, $receipt_id);
		$this->response($receipts);
	}

	function receipts_post($receipt_id = false, $method = null, $record_id = null)
	{
		if($method == 'items'){
			$this->receipt_items_post($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_post($receipt_id, $record_id);
			return true;
		}

		$success = $this->table_receipt->save($this->suspended_sale_id, null);
		$this->response(array('success'=>true,'msg'=>'All receipts for table'));
	}

	function receipts_put($receipt_id = false, $method = null, $record_id = null)
	{
		if($method == 'items'){
			$this->receipt_items_put($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_put($receipt_id, $record_id);
			return true;
		}

		$receiptData = $this->request->body;

		$success = $this->table_receipt->save($this->suspended_sale_id, $receipt_id);
		$this->response(array('success'=>true,'msg'=>'Receipt updated'));
	}

	function receipts_delete($receipt_id = false, $method = null, $record_id = null)
	{
		if($method == 'items'){
			$this->receipt_items_delete($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_delete($receipt_id, $record_id);
			return true;
		}

		if($receipt_id === false){
			$this->response(array('msg'=>'Can\'t delete all receipts at once.'), 400);
			return false;
		}

		if((int) $receipt_id == 1){
			$this->response(array('msg'=>'Can\'t delete receipt #1'), 400);
			return false;
		}

		if(!$this->table_receipt->can_delete($this->suspended_sale_id, $receipt_id)){
			$this->response(array('msg'=>'Receipt must be empty to delete'), 400);
			return false;
		}

		$success = $this->table_receipt->delete($this->suspended_sale_id, $receipt_id);
		$this->response(array('success' => $success, 'msg'=>'Receipt deleted'));
	}

    /*******************************************************************
     * Receipt items
	 ******************************************************************/
	function receipt_items_get($receiptId, $line = null)
	{
		$items = $this->table_receipt->get_items($this->suspended_sale_id, $receiptId);
		if(is_array($items)){
			$this->response($items, 200);
		}
		$this->response($items);
	}

	function receipt_items_post($receiptId, $line = null)
	{
		$itemData = $this->post();
		$success = $this->table_receipt->add_item($itemData['item_id'], $itemData['line'], $this->suspended_sale_id, $receiptId);
		$this->response(array('success'=>$success,'msg'=>'Receipt item added'));
	}

	function receipt_items_put($receiptId, $line = null)
	{
		$itemData = $this->request->body;
		$success = $this->table_receipt->add_item($itemData['item_id'], $itemData['line'], $this->suspended_sale_id, $receiptId);
		$this->response(array('success'=>$success,'msg'=>'Receipt item updated'));
	}

	function receipt_items_delete($receiptId, $line = null)
	{
		$itemData = $this->request->body;

		if(!$this->table_receipt->can_delete_item($this->suspended_sale_id, $line)){
			$this->response(array('success'=>false, 'msg'=>'Item must belong to at least 1 receipt'), 400);
			return false;
		}

		$success = $this->table_receipt->delete_item(0, $line, $this->suspended_sale_id, $receiptId);
		$this->response(array('success'=>true, 'msg'=>'Receipt item deleted'));
	}

    /*******************************************************************
     * Receipt payments
	 ******************************************************************/
	function receipt_payments_get($receiptId, $line = null)
	{
		$items = $this->Table->get_payments($this->suspended_sale_id, $receiptId);
		if(is_array($items)){
			$this->response($items, 200);
		}
		$this->response($items);
	}

	function receipt_payments_post($receiptId, $line = null)
	{
		$payment = $this->request->body;
		$msg = null;

		if(empty($payment)){
			$payment = $this->post();
		}

		// Make sure payment type is passed
		if(empty($payment['type'])){
			$this->response(array('success'=>false, 'msg'=>'Payment type is required'), 400);
			return false;
		}
		// Make sure payment amount is over 0
		if(empty($payment['amount']) || $payment['amount'] <= 0){
			$this->response(array('success'=>false, 'msg'=>'Amount must be greater than 0'), 400);
			return false;
		}
		$payment['amount'] = (float) round($payment['amount'], 2);

		// Process payment depending on type
		switch(strtolower($payment['type'])){
			case 'cash':
			case 'check':
			break;
			case 'gift card':
				$this->load->model('Giftcard');

				if(empty($payment['card_number'])){
					$this->response(array('success'=>false, 'msg'=>'Gift card number is required'), 400);
					return false;
				}
				$giftcard_id = $this->Giftcard->get_giftcard_id($payment['card_number']);

				// If gift card doesn't exist, or is expired, return error
				if(empty($giftcard_id) || $this->Giftcard->is_expired($giftcard_id)){
					$this->response(array('success'=> false, 'msg'=>lang('sales_giftcard_does_not_exist')), 400);
					return false;
				}

				$giftcard_info = $this->Giftcard->get_info($giftcard_id);
				$valid_amount = $this->sale_lib->get_valid_amount($giftcard_info->department, $giftcard_info->category);

				$cur_giftcard_value =  $giftcard_value - $current_payments_with_giftcard;

				if ($cur_giftcard_value <= 0 && $amount_tendered > 0){
					$this->response(array('success'=>false, 'msg'=>lang('sales_giftcard_balance_is').' '.to_currency($giftcard_value)), 400);
					return false;
				}

				if (($cur_giftcard_value - $amount_tendered) > 0){
					$msg = lang('sales_giftcard_balance_is').' '.to_currency($cur_giftcard_value);
				}
				$payment['type'] = 'Gift Card:'.$payment['card_number'];

			break;
			case 'creditcard':

				if(empty($payment['id'])){
					$this->response(array('success'=>false, 'msg'=>'Credit card session ID is required'), 400);
					return false;
				}
				if(empty($payment['created'])){
					$this->response(array('success'=>false, 'msg'=>'Created date is required'), 400);
					return false;
				}
				if(empty($payment['transactions'])){
					$this->response(array('success'=>false, 'msg'=>'Credit card transaction data required'), 400);
					return false;
				}
				$this->load->model('sale');

				// Verify ETS transaction
				$this->load->library('Hosted_payments');
				$hosted_payment = new Hosted_payments();
				$hosted_payment->initialize($this->config->item('ets_key'));
				$session_id = $hosted_payment->get("session_id");

				$transaction_id = $payment['transactions']['id'];

				$hosted_payment->set("action", "verify")
					->set("sessionID", $paymment['id'])
					->set("transactionID", $transaction_id);

				$account_id = $payment['customers']['id'];
				if ($account_id){
					$hosted_payment->set('accountID', $account_id);
				}

				$verify = $hosted_payment->send();
				$transaction_time = date('Y-m-d H:i:s', strtotime($payment['created']));

				// Convert card type to match mercury card types
				if ((string) $payment['transactions']['type'] == 'credit card') {
					$ets_card_type = $payment['transactions']['cardType'];
					$card_type = '';
					switch($ets_card_type){
						case 'MasterCard':
							$card_type = 'M/C';
						break;
						case 'Visa':
							$card_type = 'VISA';
						break;
						case 'Discover':
							$card_type = 'DCVR';
						break;
						case 'AmericanExpress':
							$card_type = 'AMEX';
						break;
						case 'Diners':
							$card_type = 'DINERS';
						break;
						case 'JCB':
							$card_type = 'JCB';
						break;
					}
					$masked_account = (string) $payment['transactions']['cardNumber'];
					$auth_code = (string) $payment['transactions']['approvalCode'];
				}
				else
				{
					$card_type = 'Bank Acct';
					$masked_account = (string) $payment['transactions']['accountNumber'];
					$auth_code = '';
				}

				$credit_card_data = array(
					'course_id' => $this->session->userdata('selected_course'),
					'card_type' => $card_type,
					'masked_account' => $masked_account,
					'cardholder_name' => ''
				);

				//Update credit card payment data
				$trans_message = (string) $payment['transactions']['message'];
				$payment_data = array (
					'course_id' 		=> $this->session->userdata('course_id'),
					'masked_account' 	=> $masked_account,
					'trans_post_time' 	=> (string) $transaction_time,
					'ets_id' 			=> $this->config->item('ets_key'),
					'tran_type' 		=> 'Sale',
					'amount' 			=> (string) $payment['transactions']['amount'],
					'auth_amount'		=> (string)	$payment['transactions']['amount'],
					'auth_code'			=> $auth_code,
					'card_type' 		=> $card_type,
					'frequency' 		=> 'OneTime',
					'payment_id' 		=> (string) $payment['transactions']['id'],
					'process_data'		=> (string) $payment['id'],
					'status'			=> (string) $payment['transactions']['status'],
					'status_message'	=> (string) $payment['transactions']['message'],
					'display_message'	=> (string) $payment['message'],
					'operator_id'		=> (string) $this->session->userdata('person_id')
				);

				$primary_store = (string) $payment['store']['primary'];
				$this->sale->update_credit_card_payment($primary_store, $payment_data);
				$this->session->set_userdata('invoice_id', $primary_store);
				$payment_data['payment_type'] = $payment_data['card_type'].' '.$payment_data['masked_account'];

				// Data to return
				$payment['type'] = $payment_data['payment_type'];
				$payment['card_type'] = $card_type;
				$payment['card_number'] = $masked_account;
				$payment['credit_card_invoice_id'] = $primary_store;
			break;
			/*
			case 'punchcard':
				$this->load->model('Punch_card');
				$punch_card_id = $this->Punch_card->get_punch_card_id($payment['card_number']);

				if($this->Punch_card->is_expired($punch_card_id)){
					$this->response(array('success'=>false, 'msg'=>lang('sales_punch_card_does_not_exist')), 400);
					return false;
				}
				$punch_card_info = $this->Punch_card->get_info($punch_card_id);
				$punch_card_value = $this->Punch_card->get_punch_card_value($punch_card_id);

				if(!$punch_card_value['success']){
					$this->response(array('success'=>false, 'msg'=>$punch_card_value['message']), 400);
					return false;
				}
				$payment['type'] = 'punchcard:'.$payment['card_number'];

			break; */
			default:
				$this->response(array('success'=>false, 'msg'=>'Payment type is invalid'), 400);
				return false;
			break;
		}
		$this->session->set_userdata('taxable', true);
		$success = $this->Table->add_payment($this->suspended_sale_id, $receiptId, $payment['type'], $payment['amount'], $payment['credit_card_invoice_id']);

		// If payment was successfully added
		if($success){
			$newPayment = $this->Table->get_payments($this->suspended_sale_id, $receiptId, $payment['type']);
			$returnData = array('type' => $payment['type'], 'amount' => (float) $newPayment['payment_amount']);

			if(!empty($payment['card_type'])){
				$returnData['card_type'] = $payment['card_type'];
			}
			if(!empty($payment['card_number'])){
				$returnData['card_number'] = $payment['card_number'];
			}

			// Retrieve all data for receipt (items, payments, etc)
			$receiptData = $this->table_receipt->get($this->suspended_sale_id, $receiptId);

			// If receipt is now fully paid, complete the sale
			if($receiptData['total_due'] <= 0){

				$this->load->model('Sale');
				$this->table_receipt->mark_paid($this->suspended_sale_id, $receiptId);

				// Gather up receipt data to complete the sale
				$customer_id = -1; // TODO
				$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
				$comment = '';
				$payments = array();
				$items = array();
				$sideTypes = array('soups', 'salads', 'sides');

				// Loop through items, add split prices as the default price
				// Split off side dishes as their own items
				$lineCount = 1;
				foreach($receiptData['items'] as $key => $itemData){
					$item = $itemData;

					// Overwrite regular price with split price on all items
					$item['total'] = $item['split_total'];
					$item['price'] = $item['split_price'];
					$item['subtotal'] = $item['split_subtotal'];
					$item['tax'] = $item['split_tax'];
					$item['line'] = $lineCount;

					$items[] = $item;
					$lineCount++;

					// Loop through the various side dishes associated with each item
					foreach($sideTypes as $sideType){

						if(!empty($item[$sideType])){

							foreach($item[$sideType] as $side){
								// If side is 'none' skip it
								if((int) $side['item_id'] == 0){ continue; }

								$side['line'] = $lineCount;
								$side['total'] = $side['split_total'];
								$side['price'] = $side['split_price'];
								$side['subtotal'] = $side['split_subtotal'];
								$side['tax'] = $side['split_tax'];
								$items[] = $side;

								unset($items[$lineCount - 1][$sideType]);
								$lineCount++;
							}
						}
					}
				}

				// Conform payments to play nice with sale->save method
				foreach($receiptData['payments'] as $key => $payment){
					$payments[$key]['payment_type'] = ucfirst($payment['type']);
					$payments[$key]['payment_amount'] = $payment['amount'];
					$payments[$key]['invoice_id'] = $payment['credit_card_invoice_id'];
				}

				// Complete sale (places all data in sales table)
				$sale_id = $this->Sale->save($items, $customer_id, $employee_id, $comment, $payments, false, false, false, null, false, $this->suspended_sale_id);
			}

			// Format list of items from receipt as array
			$items = array();
			foreach($receiptData['items'] as $item){
				$items[] = (int) $item['line'];
			}

			// Mark all items in cart (that are in receipt) as paid
			$this->Table->mark_items_paid($this->suspended_sale_id, $items);
			$this->response($returnData);

		}else{
			$this->response(array('success'=>$success));
		}
	}

	function receipt_payments_put($receiptId, $paymentType = null)
	{
		return $this->receipt_payments_post($receiptId);
	}

	function receipt_payments_delete($receiptId, $paymentType = null)
	{
		$itemData = $this->request->body;
		$paymentType = urldecode($paymentType);

		// Get payment data
		$paymentData = $this->Table->get_payments($this->suspended_sale_id, $receiptId, $paymentType);
		$refundSuccess = true;

		// If payment being deleted is from credit card, refund the payment back to card
		if($paymentData['credit_card_invoice_id'] != 0 && $this->config->item('ets_key')){
			$refundSuccess = false;
			$this->load->model('sale');
			$cc_payment = $this->sale->get_credit_card_payment($paymentData['credit_card_invoice_id']);
			$cc_payment = $cc_payment[0];

			// If payment hasn't been refunded already
			if ($cc_payment['amount_refunded'] <= 0){

				$amount = $cc_payment['amount'] + $cc_payment['gratuity_amount'];
				if($cc_payment['process_data'] != ''){
					$this->load->library('Hosted_payments');
					$hosted_payment = new Hosted_payments();
					$hosted_payment->initialize($this->config->item('ets_key'));

					$ets_response = $hosted_payment->set('action', 'void')
								->set('transactionID', $cc_payment['payment_id'])
								->set('sessionID', $cc_payment['process_data'])
								->send();

					if($ets_response->status == 'success'){
						$refundSuccess = true;
						$sql = $this->sale->add_ets_refund_payment($paymentData['credit_card_invoice_id'], $amount);
					}
				}

			}else{
				$refundSuccess = true;
			}
		}

		if(!$refundSuccess){
			$this->response(array('success'=>false, 'msg'=>'Error refunding credit card payment'), 500);
			return false;
		}

		$success = $this->Table->delete_payment($this->suspended_sale_id, $receiptId, $paymentType);

		if($success){
			$receiptData = $this->table_receipt->get($this->suspended_sale_id, $receiptId);

			// If all payments were deleted from receipt, mark items in cart as UNPAID (that belong to receipt)
			if($receiptData['amount_paid'] <= 0){
				$items = array();
				foreach($receiptData['items'] as $item){
					$items[] = (int) $item['line'];
				}
				// Mark all items in cart (that are in receipt) as unpaid
				$this->Table->mark_items_unpaid($this->suspended_sale_id, $items);
			}
		}

		$this->response(array('success'=>true, 'msg'=>'Payment deleted'));
	}

    /*******************************************************************
     * Cart
	 ******************************************************************/
	function cart_get($line)
	{
		// If getting all items in cart
		if(empty($line)){
			$result = $this->Table->get_sale_items($this->suspended_sale_id);
			$rows = $result->result_array();
			$items = array();

			// Loop through rows and conform them to proper item structure
			foreach($rows as $row){
				$item = array(
					'item_id' => $row['item_id'],
					'line' => $row['line'],
					'description' => $row['description'],
					'serialnumber' => $row['serialnumber'],
					'seat' => $row['seat'],
					'quantity' => $row['quantity_purchased'],
					'cost' => $row['item_cost_price'],
					'price' => $row['item_unit_price'],
					'discount' => $row['discount_percent']
				);

				$items[] = $item;
			}
			$this->response($items);

		// If getting just one item
		}else{
			$this->response(array('TODO: data here'));
		}
	}

	function cart_post($line)
	{
		$itemData = $this->post();
		$result = $this->Table->save_item($this->suspended_sale_id, $itemData);
		$this->response(array('msg'=>'Post cart'));
	}

	function cart_put($line)
	{
		$itemData = $this->request->body;
		$result = $this->Table->save_item($this->suspended_sale_id, $itemData);

		// Automatically place item on next available receipt
		$receipt_id = $this->table_receipt->get_available_receipt($this->suspended_sale_id);
		$this->table_receipt->add_item($itemData['item_id'], $itemData['line'], $this->suspended_sale_id, $receipt_id);

		$this->response(array('success'=>$success, 'msg'=>'Item added to cart'));
	}

	function cart_delete($line)
	{
		$success = $this->Table->delete_item($this->suspended_sale_id, $line);
		$this->response(array('success'=>$success, 'msg'=>'Item removed from cart'));
	}

    /*******************************************************************
     * Kitchen Order - Post item line numbers to have them sent to kitchen
	 ******************************************************************/
	function orders_get($order_id)
	{
		$this->response(array('msg'=>'TODO: get order data'));
	}

	function orders_post($order_id)
	{
		$orderData = $this->request->body;
		if(empty($orderData['items'])){
			$this->response(array('msg'=>'Items can not be empty'), 400);
			return false;
		}
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		$ticket_id = $this->table_ticket->save($this->suspended_sale_id, $this->table_num, $employee_id, $orderData['items']);

		$lines = array();
		foreach($orderData['items'] as $item){
			$lines[] = $item['line'];
		}
		$this->Table->mark_items_ordered($this->suspended_sale_id, $lines);

		$this->response(array('msg'=>'TODO: order post'));
	}

	function orders_put($order_id)
	{
		$itemData = $this->request->body;
		$this->response(array('msg'=>'TODO: edit order'));
	}

	function orders_delete($order_id)
	{
		$success = $this->Table->delete_item($this->suspended_sale_id, $line);
		$this->response(array('success'=>$success, 'msg'=>'Item removed from cart'));
	}
}
