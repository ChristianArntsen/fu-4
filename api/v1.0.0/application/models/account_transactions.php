<?php
class Account_transactions extends CI_Model 
{
	function save($person_id = -1, $trans_description='', $add_subtract='', $trans_details='', $employee_id)
	{		
		$employee_id=$employee_id?$employee_id:$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_customer_info = $this->Customer->get_info($person_id);
		if ($add_subtract == '')
			$add_subtract = $this->input->post('add_subtract');
		if ($trans_description == '')
			$trans_description = $this->input->post('trans_comment');
		$trans_data = array
		(
			'trans_date'=>date('Y-m-d H:i:s'),
			'trans_customer'=>$person_id,
			'trans_user'=>$employee_id,
			'trans_comment'=>$trans_description,
			'trans_description'=>$trans_details,
			'trans_amount'=>$add_subtract
		);
		
		//Update account balance
		$customer_data = array(
		'account_balance'=>$cur_customer_info->account_balance + $add_subtract
		);
		$person_data = array(
		'address_2'=>$cur_customer_info->address_2
		);

		$giftcards = array();
		$groups = array();
		$passes = array();
		if ($this->Customer->save($person_data, $customer_data,$person_id, $giftcards, $groups, $passes)) 
		{
			$this->insert($trans_data);
			return true;
		}
		else 
		{			
			return false;
		}
	}	
	
	function insert($account_transaction_data)
	{
		return $this->db->insert('account_transactions',$account_transaction_data);
	}
	
	function get_account_transaction_data_for_customer($customer_id = false, $month = false, $date = false)
	{
		$month_sql = "";
		$table_join = '';
		$customer_sql = '';
		if ($customer_id)
		{
			$customer_sql = " AND `trans_customer` = '$customer_id' ";
		}
		else
		{
			$table_join = ' LEFT JOIN foreup_customers ON foreup_customers.person_id = foreup_account_transactions.trans_customer LEFT JOIN foreup_people ON foreup_people.person_id = foreup_customers.person_id ';
			$customer_sql = " AND foreup_customers.`course_id` = '{$this->session->userdata('course_id')}' ";
			$date = $date ? $date : date('Y-m-d');
			$month_sql = " AND trans_date > '".date('Y-m-d 00:00:00', strtotime($date))."' AND trans_date < '".date('Y-m-d 23:59:59', strtotime($date))."' ";
		}
		if ($month)
			$month_sql = " AND trans_date > '".date('Y-m-01 00:00:00', strtotime($month.' -1 month'))."' AND trans_date < '".date('Y-m-01 00:00:00', strtotime($month))."' ";
		$result = $this->db->query("SELECT *, DATE_FORMAT(trans_date, '%c-%e-%y %l:%i %p') AS date FROM (`foreup_account_transactions`) $table_join WHERE trans_amount != 0 $customer_sql $month_sql ORDER BY `trans_date` desc");
		return $result->result_array();		
	}
}

?>