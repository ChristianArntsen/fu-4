<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Guest extends REST_Controller
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model('Api_data');
		$this->load->helper('email_helper');
	}
	
    function create_get()
    {
    	$first_name = trim($this->input->get('first_name'));
		$last_name = trim($this->input->get('last_name'));
		$email = $this->input->get('email');
		$phone_number = $this->input->get('phone_number');
		$zip = $this->input->get('zip');
		$course_id = $this->input->get('course_id');							
		
		$errors = array();
		if (!$first_name || $first_name == '')
			$errors[] = 'Missing parameter: first_name';
		if (!$last_name || $last_name == '')
			$errors[] = 'Missing parameter: last_name';
		if (!$email || $email == '')
			$errors[] = 'Missing parameter: email';
		else if (!valid_email($email))
			$errors[] = 'Invalid parameter: email';
		if (!$phone_number || $phone_number == '')
			$errors[] = 'Missing parameter: phone_number';
		else if (!preg_match('/^\(?[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}$/', trim($phone_number)))
			$errors[] = 'Invalid parameter: phone_number';
		if (!$zip || $zip == '')
			$errors[] = 'Missing parameter: zip';
		if (!$course_id || $course_id == '')
			$errors[] = 'Missing parameter: course_id';
		if (count($errors)>0)
			$this->response(array('error', implode(', ', $errors)));
		
        $person_data = array(
        //Required - start
		'first_name'=>$first_name,
		'last_name'=>$last_name,
		'email'=> $email,
		'phone_number'=>$phone_number,
		'zip'=>$zip,
		//Required - end
		'birthday'=>$this->input->get('birthday'),
		'address_1'=>$this->input->get('address'),
		'city'=>$this->input->get('city'),
		'state'=>$this->input->get('state'),
		'country'=>$this->input->get('country')
		);
		$customer_data=array(
			'api_id' => $this->rest->api_id,
			'course_id' => $course_id
		); 		

		$success = $this->Api_data->save_customer_info($person_data, $customer_data);
		
		if ($success) {
			$response = array('success'=>true,'guest_id'=>$customer_data['person_id']);
			$this->response($response);
		}
		else 
		{
			$this->response(array('success'=>true,'guest_id'=>'','error'=>'Customer could not be save'));
		}
    }
	
	function list_get()
	{		
		$course_id = $this->input->get('golf_course_id');
		if (!$course_id)
			$this->response(array("error", 'Missing parameter: golf_course_id'));
		
		$customer_list = $this->Api_data->get_customer_list($course_id, $this->rest->api_id);		
		if (count($customer_list) > 0)
			$this->response(array('success'=>true,'guests'=>$customer_list));
		else
			$this->response(array('error','No results'));
	}
	
	function info_get()
	{
		$person_id = $this->input->get('guest_id');
		if (!$person_id)
			$this->response(array("error", 'Missing parameter: guest_id'));
		
		$customer_info = $this->Api_data->get_customer_info($person_id, $this->rest->api_id);
		if ($customer_info->num_rows() > 0)
		{
			$customer_info = $customer_info->row_array();
			$customer_info['golf_course_id'] = $customer_info['course_id'];
			unset($customer_info['course_id']);
			$customer_info['guest_id'] = $customer_info['person_id'];
			unset($customer_info['person_id']);
			$this->response(array('success'=>true,'guest_info'=>$customer_info));
		}
		else {
			$this->response(array('succes'=>true, 'guest_info'=>array(),'message'=>'No results'));			
		}
		
	}
	
	function update_get()
	{
		$course_id = $this->input->get('golf_course_id');
		$person_id = $this->input->get('guest_id');
		
		$errors = array();
		
		if (!$course_id || $course_id == '')
			$errors[] = 'Missing parameter: golf_course_id';
		if (!$person_id || $person_id == '')
			$errors[] = 'Missing parameter: guest_id';
		
		if (count($errors)>0)
			$this->response(array('error', implode(', ', $errors)));
		
		
    	$first_name = trim($this->input->get('first_name'));
		$last_name = trim($this->input->get('last_name'));
		$email = $this->input->get('email');
		$phone_number = $this->input->get('phone_number');
		$zip = $this->input->get('zip');
		$birthday = $this->input->get('birthday');
		$address = $this->input->get('address');
		$city = $this->input->get('city');
		$state = $this->input->get('state');
		
		$customer_data=array(
			'api_id' => $this->rest->api_id,
			'course_id' => $course_id
		); 
		
		$person_data = array();
		
		if ($person_id) $person_data['person_id'] = $person_id;
		if ($first_name) $person_data['first_name'] = $first_name;	
		if ($last_name) $person_data['last_name'] = $last_name;
		if ($email) $person_data['email'] = $email;
		if ($phone_number) $person_data['phone_number'] = $phone_number;
		if ($zip) $person_data['zip'] = $zip;
		if ($birthday) $person_data['birthday'] = $birthday;
		if ($address) $person_data['address'] = $address;
		if ($city) $person_data['city'] = $city;
		if ($state) $person_data['state'] = $state;				
		
		$success = $this->Api_data->save_customer_info($person_data, $customer_data, $person_id);
		
		if ($success) {
			$response = array('success'=>true,'guest_id'=>$person_id);
			$this->response($response);
		}
		else 
		{
			$this->response(array('success'=>true,'error'=>'Customer could not be save'));
		}
	}



}
