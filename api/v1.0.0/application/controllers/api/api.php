<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Api extends REST_Controller
{
	function __construct() 
	{
		parent::__construct();
		
		
	}
    function index_get()
    {
        echo 'adding item';
    }
	
	function other_get()
	{
		$this->response(array('success'=>false,'error'=>'The reservation could not be canceled'));
	}
}


// CRUD for Tee Times
// R Green Fees
// --> CRU LOGIN Users

// R Golf Courses

// CR Reviews
// CR Favorites
// CR Checkins
// CR Photos
// CR Photo Likes
// CR Photo Flags

// R Items (Menu)
// CR Orders (Suspended Sales?)

// R Promotions ?