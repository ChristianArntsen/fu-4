<?php
class Credit_cards extends CI_Controller
{
	function __construct()
	{
          	parent::__construct('credit_cards');
			$this->load->model('Credit_card');
  	
	}

	function index()
	{
		
	}
	
	function get()
	{
		$course_id = $this->input->post('course_id');
		$this->session->set_userdata('selected_course', $course_id);
		echo json_encode($this->Credit_card->get($course_id));
	}
	function delete()
	{
		$credit_card_id = $this->input->post('credit_card_id');
		echo json_encode(array('success'=>$this->Credit_card->delete($credit_card_id)));
	}
	
	function view($course_id = -1) 
	{
		$course_info = $this->Course->get_info($course_id);
		$data = array('course_id'=>$course_id, 'course_name'=>$course_info->name);
        $this->load->view("credit_cards/form",$data);
	}
	function charge($credit_card_id = -1)
	{
		$this->load->model('Credit_card');
		$this->Credit_card->clear_charge_items();
		$data = array();
		$data['credit_card_info'] = $this->Credit_card->get_info($credit_card_id);
		$data['course_info'] = $this->Course->get_info($data['credit_card_info']->course_id);
		$this->load->view('credit_cards/charge', $data);
	}
	function add_charge_item() {
		$this->load->model('Credit_card');
		$description = $this->input->post('description');
		$amount = $this->input->post('amount');
		echo json_encode($this->Credit_card->add_charge_item($description, $amount));
	}
	function delete_charge_item() {
		$this->load->model('Credit_card');
		$line_number = $this->input->post('line_number');
		echo json_encode($this->Credit_card->delete_charge_item($line_number));
	}
	function update_tax_rate() {
		$this->load->model('Credit_card');
		$tax_rate = $this->input->post('tax_rate');
		echo json_encode($this->Credit_card->update_tax_rate($tax_rate));
	}
	function charge_credit_card($credit_card_id)
	{
		$this->load->library('Hosted_checkout_2');
		$HC = new Hosted_checkout_2();
		
		$totals = $this->Credit_card->get_charge_totals();
		$items = $this->Credit_card->get_charge_items();
		$credit_card_info = $this->Credit_card->get_info($credit_card_id);
		$course_info = $this->Course->get_info($credit_card_info->course_id);
		$invoice = $this->Sale->add_credit_card_payment(array('mercury_id'=>config_item('foreup_mercury_id'),'tran_type'=>'CreditSaleToken','frequency'=>'Recurring'));
		$emp_info=$this->Employee->get_info($this->session->userdata('person_id'));
		$employee = $emp_info->first_name.' '.$emp_info->last_name;
		$billing_email = $this->input->post('contact_email');
		$tax_name = $this->input->post('tax_name');
		$tax_rate = $this->input->post('tax_rate');
		$totals['taxes'] = floor($totals['subtotal'] * $tax_rate)/100;
		$totals['total'] = floor(($totals['subtotal'] + $totals['taxes'])*100)/100;
		//echo 'here';
		$HC->set_merchant_credentials(config_item('foreup_mercury_id'),config_item('foreup_mercury_password'));//foreUP's Credentials
		$HC->set_frequency('Recurring');
		$HC->set_token($credit_card_info->token);
		$HC->set_cardholder_name($credit_card_info->cardholder_name);
		$HC->set_invoice($invoice);
	
		//echo json_encode(array('success'=>true));
		$transaction_results = $HC->token_transaction('Sale',$totals['total'], '0.00', $totals['taxes']);
		//print_r($transaction_results);
		$payment_data = array(
			'acq_ref_data'=>(string)$transaction_results->AcqRefData,
			'auth_code'=>(string)$transaction_results->AuthCode,
			'auth_amount'=>(string)$transaction_results->AuthorizeAmount,
			'avs_result'=>(string)$transaction_results->AVSResult,
			'batch_no'=>(string)$transaction_results->BatchNo,
			'card_type'=>(string)$transaction_results->CardType,
			'cvv_result'=>(string)$transaction_results->CVVResult,
			'gratuity_amount'=>(string)$transaction_results->GratuityAmount,
			'masked_account'=>(string)$transaction_results->Account,
			'status_message'=>(string)$transaction_results->Message,
			'amount'=>(string)$transaction_results->PurchaseAmount,
			'ref_no'=>(string)$transaction_results->RefNo,
			'status'=>(string)$transaction_results->Status,
			'token'=>(string)$transaction_results->Token,
			'process_data'=>(string)$transaction_results->ProcessData
		);
		$this->Sale->update_credit_card_payment($invoice, $payment_data);
		//echo $this->db->last_query();
		$credit_card_data = array(
			'token'=>$payment_data['token'],
			'token_expiration'=>date('Y-m-d', strtotime('+2 years'))
		);
		if ($payment_data['status'] == 'Approved')
		{
			$this->Credit_card->save($credit_card_data, $credit_card_id);
			$this->Credit_card->record_charges($credit_card_id);
		}
		$data = array(
			'receipt_title'=>'Billing Receipt',
			'transaction_time'=>date('Y-m-d'),
			'customer'=>$course_info->name,
			'sale_id'=>$invoice,
			'employee'=>$employee,
			'items'=>$items,
			'totals'=>$totals,
			'payment_data'=>$payment_data,
			'billing_email'=>$billing_email,
			'tax_name'=>$tax_name,
			'tax_rate'=>$tax_rate,
			'course_info'=>$course_info
		);
		$subject = ($payment_data['status'] == 'Approved'?'':'Declined ');
		$subject .=  $course_info->name.' Charge Made';
		$to_array = array('billing@foreup.com','jhopkins@foreup.com');
		if ($billing_email != '' && $payment_data['status'] == 'Approved')
			$to_array[] = $billing_email;
		send_sendgrid($to_array, $subject, $this->load->view("billing/receipt_email",$data, true), 'billing@foreup.com', 'ForeUP Billing');
		echo json_encode(array('success'=>$payment_data['status']=='Approved'?true:false, 'message'=>"A charge of {$payment_data['amount']} was {$payment_data['status']} for card {$payment_data['masked_account']}", 'item_id'=>0));
	}
	function open_course_card_window()
	{
		$course_id = $this->session->userdata('course_id');
        
		//$this->open_payment_window('FeCOM', 'Sale', 'Recurring', '1.00', '0.00');
		
		$this->load->library('Hosted_checkout_2');
		
		$HC = new Hosted_checkout_2();
		$HC->set_merchant_credentials(config_item('foreup_mercury_id'),config_item('foreup_mercury_password'));//foreUP's Credentials
		$HC->set_response_urls('credit_cards/card_captured', 'credit_cards/process_cancelled');
		$HC->set_default_swipe('Manual');
		$initialize_results = $HC->initialize_payment('1.00','0.00','PreAuth','POS','Recurring');
		//print_r($initialize_results);
		if ((int)$initialize_results->ResponseCode == 0)
		{
			//Set invoice number to save in the database
			$invoice = $this->sale->add_credit_card_payment(array('tran_type'=>'PreAuth','frequency'=>'Recurring'));
			$this->session->set_userdata('invoice', $invoice);
			
			$user_message = (string)$initialize_results->Message;
			$return_code = (int)$initialize_results->ResponseCode;
			$this->session->set_userdata('payment_id', (string)$initialize_results->PaymentID);
			$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
			$data = array('user_message'=>$user_message, 'return_code'=>$return_code, 'url'=>$url);
			$this->load->view('sales/hc_pos_iframe.php', $data);
		}
	}
	function card_captured() {
		$this->load->library('Hosted_checkout_2');
		$HC = new Hosted_checkout_2();
		$HC->set_merchant_credentials(config_item('foreup_mercury_id'),config_item('foreup_mercury_password'));//foreUP's Credentials
		//$data = $HC->payment_made();
		$payment_id = $this->session->userdata('payment_id');
		$this->session->unset_userdata('payment_id');
		$HC->set_payment_id($payment_id);
		$verify_results = $HC->verify_payment();
		//echo 'about to complete<br/>';
		$HC->complete_payment();
		//echo 'completed <br/>';
		$invoice = $this->session->userdata('invoice');
		$this->session->unset_userdata('invoice');
		
		$this->load->model('Credit_card');
		//Add card to billing_credit_cards
		$credit_card_data = array(
			'course_id'=>$this->session->userdata('selected_course'),
			'token'=>(string)$verify_results->Token,
			'token_expiration'=>date('Y-m-d', strtotime('+2 years')),
			'card_type'=>(string)$verify_results->CardType,
			'masked_account'=>(string)$verify_results->MaskedAccount,
			'cardholder_name'=>(string)$verify_results->CardholderName
		);
		$this->session->unset_userdata('selected_course');
		$this->Credit_card->save($credit_card_data);
		//echo 'saved credit card data';
		//Update credit card payment data
		$payment_data = array (
			'course_id'=>$this->session->userdata('course_id'),
			'mercury_id'=>config_item('foreup_mercury_id'),
			'tran_type'=>(string)$verify_results->TranType,
			'amount'=>(string)$verify_results->Amount,
			'auth_amount'=>(string)$verify_results->AuthAmount,
			'card_type'=>(string)$verify_results->CardType,
			'frequency'=>'Recurring',
			'masked_account'=>(string)$verify_results->MaskedAccount,
			'cardholder_name'=>(string)$verify_results->CardholderName,
			'ref_no'=>(string)$verify_results->RefNo,
			'operator_id'=>(string)$verify_results->OperatorID,
			'terminal_name'=>(string)$verify_results->TerminalName,
			'trans_post_time'=>(string)$verify_results->TransPostTime,
			'auth_code'=>(string)$verify_results->AuthCode,
			'voice_auth_code'=>(string)$verify_results->VoiceAuthCode,
			'payment_id'=>$payment_id,
			'acq_ref_data'=>(string)$verify_results->AcqRefData,
			'process_data'=>(string)$verify_results->ProcessData,
			'token'=>(string)$verify_results->Token,
			'response_code'=>(int)$verify_results->ResponseCode,
			'status'=>(string)$verify_results->Status,
			'status_message'=>(string)$verify_results->StatusMessage,
			'display_message'=>(string)$verify_results->DisplayMessage,
			'avs_result'=>(string)$verify_results->AvsResult,
			'cvv_result'=>(string)$verify_results->CvvResult,
			'tax_amount'=>(string)$verify_results->TaxAmount,
			'avs_address'=>(string)$verify_results->AVSAddress,
			'avs_zip'=>(string)$verify_results->AVSZip,
			'payment_id_expired'=>(string)$verify_results->PaymendIDExpired,
			'customer_code'=>(string)$verify_results->CustomerCode,
			'memo'=>(string)$verify_results->Memo
		);
		$this->sale->update_credit_card_payment($invoice, $payment_data);
		//echo 'saved credit card payment data';
		$data = array ('card_captured'=>true, 'course_id'=>$credit_card_data['course_id']);
		//print_r($payment_data);
		if ($payment_data['response_code'] === 0 && $payment_data['status'] != "Declined")
			$this->load->view('credit_cards/card_captured.php', $data);
	}
	function cancel_authorization($invoice)
	{
		$this->load->library('Hosted_checkout_2');
		$HC = new Hosted_checkout_2();
		
		$payment_data = $this->Sale->get_credit_card_payment($invoice);
		
		//set values
		$HC->set_merchant_credentials(config_item('foreup_mercury_id'),config_item('foreup_mercury_password'));//foreUP's Credentials
		$HC->set_invoice($invoice);
		$HC->set_token($payment_data[0]['token']);
		$HC->set_cardholder_name($payment_data[0]['cardholder_name']);
		$HC->set_ref_no($payment_data[0]['ref_no']);
		$HC->set_auth_amount($payment_data[0]['auth_amount']);
		$HC->set_frequency($payment_data[0]['frequency']);
		$HC->set_auth_code($payment_data[0]['auth_code']);
		$HC->set_acq_ref_data($payment_data[0]['acq_ref_data']);
		$HC->set_process_data($payment_data[0]['process_data']);		
		
		//run reversal
		$transaction_response = $HC->token_transaction('Reversal');
		//print_r($transaction_response);
	}
	function process_cancelled() {
		
	}
	function save($credit_card_id = -1) 
	{
		
	}
	
}