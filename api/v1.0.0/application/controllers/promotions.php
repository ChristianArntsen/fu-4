<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");

class Promotions extends Secure_area implements iData_controller
{
  function __construct()
	{
		parent::__construct('promotions');

    $this->load->model('Promotion');
	}
  
  public function index()
  {
		$config['base_url'] = site_url('promotions/index');
		$config['total_rows'] = $this->Promotion->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_promotions_manage_table($this->Promotion->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('promotions/manage',$data);

  }
	public function search($offset = 0)
  {
    	$search=$this->input->post('search');
		$data_rows=get_promotions_manage_table_data_rows($this->Promotion->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
        $config['base_url'] = site_url('promotions/index');
        $config['total_rows'] = $this->Promotion->search($search, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);

  }
	public function suggest()
  {
    $suggestions = $this->Promotion->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
  }

	public function get_row()
	{
		$promotion_id = $this->input->post('row_id');
		$data_row=get_promotions_data_row($this->Promotion->get_info($promotion_id),$this);
		echo $data_row;
	}
  
	public function view($promotion_id=-1)
  {
    $data = array();

    $obj = $this->Promotion->get_info($promotion_id);
    $obj->limit = ($obj->limit == 0) ? '' : $obj->limit;
    $obj->buy_quantity = ($obj->buy_quantity == 0) ? '' : $obj->buy_quantity;
    $obj->get_quantity = ($obj->get_quantity == 0) ? '' : $obj->get_quantity;
    $obj->min_purchase = ($obj->min_purchase == 0) ? '' : $obj->min_purchase;
    $data['promotion_info'] = $obj;

    $data['days'] = array(
        ''        => '',
        'monday'  => 'Monday',
        'tuesday'  => 'Tuesday',
        'wednesday'  => 'Wednesday',
        'thursday'  => 'Thursday',
        'friday'  => 'Friday',
        'saturday'  => 'Staurday',
        'sunday'  => 'Sunday'
    );

    $range = array();
    $range[''] = '';
    $range['anytime'] = 'Anytime';

    
    
    $count = 1;
    while($count < 12)
    {
      $r = sprintf("%02d",$count);
      $r = round($r);
      $range[ $r . ':00am'] = $r . ':00am';
      $count++;
    }
    $range['12:00pm'] = '12:00pm';
    $count2 = 1;
    while($count2 < 12)
    {
      $r = sprintf("%02d",$count);
      $r = round($r);
      $range[ $r . ':00pm'] = $r . ':00pm';
      $count2++;
    }
    $range['00:00am'] = '00:00am';
    $data['range'] = $range;

    $course  = $this->Course->get_info($this->session->userdata('course_id'));
    
    $data['course'] = $course->name;

    $this->load->view("promotions/form",$data);
  }

	public function save($promotion_id=-1)
  {
    $promotion_data = array(
        'course_id' => $this->session->userdata('course_id'),
        'number_available' => $this->input->post('promotion_limit'),
        'expiration_date' => date('Y-m-d', strtotime($this->input->post('promotion_expiration_date'))),
        'name'=> $this->input->post('promotion_name'),
        'amount_type'=> $this->input->post('promotion_amount_type'),
        'amount'=> $this->input->post('promotion_amount'),
        'valid_for'=> $this->input->post('promotion_valid_for'),
        'valid_day_start'=> $this->input->post('promotions_valid_on_start'),
        'valid_day_end'=> $this->input->post('promotions_valid_on_end'),
        'valid_between_from'=> $this->input->post('promotions_valid_between_start'),
        'valid_between_to'=> $this->input->post('promotions_valid_between_end'),
        'limit'=> $this->input->post('promotion_limit'),
        'buy_quantity'=> $this->input->post('promotion_buy'),
        'get_quantity'=> $this->input->post('promotion_get'),
        'min_purchase'=> $this->input->post('promotion_min_purchase'),
        'additional_details'=> $this->input->post('promotion_additional_details')
    );

    $rules = '';
    if(!empty($promotion_data['min_purchase']))
    {
      $rules .= 'With minimum  purchase of ' . $promotion_data['min_purchase'];
    }

    if(!empty($promotion_data['limit'])) $rules .= " Limit {$promotion_data['limit']}. ";

    if(!empty($promotion_data['valid_day_start']) && !empty($promotion_data['valid_day_end']))
    {
      $s = ucfirst($promotion_data['valid_day_start']);
      $e = ucfirst($promotion_data['valid_day_end']);
      if(!empty($promotion_data['valid_between_from']) && !empty($promotion_data['valid_between_to']))
      {
        $f = $promotion_data['valid_between_from'];
        if($f=='anytime') $f == 'close';
        $t = $promotion_data['valid_between_to'];
        if($t=='anytime') $t == 'close';
        
        $rules .= "Valid {$s} thru {$e} between {$f} and {$t}. ";
      }
      else
      {
        $rules .= "Valid {$s} thru {$e}. ";
      }
    }

    if(!empty($promotion_data['additional_details']))
    {
      $rules .= $promotion_data['additional_details'];
    }

    $promotion_data['rules'] = trim($rules);
    
    if($this->Promotion->save($promotion_data, $promotion_id))
    {
      //New promotion
			if($promotion_id==-1)
			{
        echo json_encode(array('success'=>true,'message'=>lang('promotions_successful_adding').' '.
				strip_tags($promotion_data['name']),'id'=>$promotion_data['id']));
				$promotion_id = $promotion_data['id'];
			}
			else //previous promotion
			{
				echo json_encode(array('success'=>true,'message'=>lang('promotions_successful_updating').' '.
				strip_tags($promotion_data['name']),'id'=>$promotion_id));
			}
    }
    else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('promotions_error_adding_updating').' '.
			$promotion_data['name'],'id'=>-1));
		}
  }
  
	public function delete()
  {
    $promotions_to_delete=$this->input->post('ids');

		if($this->Promotion->delete_list($promotions_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('promotions_successful_deleted').' '.
			count($promotions_to_delete).' '.lang('promotions_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('promotions_cannot_be_deleted')));
		}
  }

	public function get_form_width()
  {
    return 1024;
  }
}