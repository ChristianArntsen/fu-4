<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
   <script src="<?php echo base_url()?>js/site.js"></script>
   <script src="<?php echo base_url()?>js/ajaxfileupload.js"></script>
   <script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
   <script src="<?php echo base_url()?>js/jquery.Jcrop.min.js"></script>
   <link href="<?php echo base_url()?>css/style.css" rel="stylesheet" />
   <link href="<?php echo base_url()?>css/jquery.Jcrop.min.css" rel="stylesheet" />
</head>
<body>
   <a href='#' id='upload_header' style='text-decoration: none; color:#336699'>Upload</a>
   <div id='upload_box' style='display:none'>
	   <form method="post" action="" id="upload_file">
	      <label for="title">Title</label>
	      <input type="text" name="title" id="title" value="" />
	      <label for="userfile">File</label>
	      <input type="file" name="userfile" id="userfile" size="20" />
	      <input type="submit" name="submit" id="submit" value ='Upload' />
	   </form>
	</div>
   <h2>Library</h2>
   <div id="files" style='max-height: 400px;'></div>
</body>
</html>
<script>
	$(function() {
		$('#upload_header').click(function(e){e.preventDefault(); $('#upload_box').toggle();})
   $('#upload_file').submit(function(e) {
      e.preventDefault();
      $.ajaxFileUpload({
         url         :'<?php echo base_url()?>index.php/upload/upload_file/',
         secureuri      :false,
         fileElementId  :'userfile',
         dataType    : 'json',
         data        : {
            'title' : $('#title').val()
         },
         success  : function (data, status)
         {
            if(data.status != 'error')
            {
               $('#files').html('<p>Reloading files...</p>');
               load_and_crop(data.image_id);
               $('#title').val('');
            }
            console.log(data);
         }
      });
      return false;
   });
   $('.delete_file_link').live('click', function(e) {
   e.preventDefault();
   if (confirm('Are you sure you want to delete this file?'))
   {
      var link = $(this);
      $.ajax({
         url         : '<?php echo base_url()?>index.php/upload/delete_file/' + link.data('file_id'),
         dataType : 'json',
         success     : function (data)
         {
            files = $('#files');
            if (data.status === "success")
            {
               link.parents('li').fadeOut('fast', function() {
                  $(this).remove();
                  if (files.find('li').length == 0)
                  {
                     files.html('<p>No Files Uploaded</p>');
                  }
               });
            }
            else
            {
               alert(data.msg);
            }
         }
      });
   }
});
	if ('<?=$file->image_id?>' != '')
   		load_and_crop(<?=$file->image_id?>);
});
function refresh_files()
{
   $.get('<?php echo base_url()?>index.php/upload/files/')
   .success(function (data){
      $('#files').html(data);
   });
}
function load_and_crop(image_id)
{
   $.get('<?php echo base_url()?>index.php/upload/crop/'+image_id)
   .success(function (data){
      $('#files').html(data);
      $('#crop_image').Jcrop({
      		aspectRatio:1,
      		onSelect:updateCoords
      });
   });				
}
function updateCoords(c)
{
	$('#x').val(c.x);
	$('#y').val(c.y);
	$('#w').val(c.w);
	$('#h').val(c.h);
};

function checkCoords()
{
	if (parseInt($('#w').val())) return true;
	alert('Please select a crop region then press submit.');
	return false;
};
</script>