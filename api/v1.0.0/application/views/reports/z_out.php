	<?php if ($export_excel == 1) { 
	//$excelXml = new Excel_XML();
	//$excelXml->setWorksheetTitle($title);
	$rows = array();
	$row= array($this->session->userdata('course_name'),'Z Out Report','','','','');
	$rows[] = $row;
	$row= array('for '.date('m/d/Y', strtotime($start_date)).' - '.date('m/d/Y', strtotime($end_date)),'','','','','');
	$rows[] = $row;
	$row= array('Debits','','','Credits','','');
	$rows[] = $row;
	$row= array('Drawer Count','','','Revenue','','');
	$rows[] = $row;
	$row= array('Description','System','Count','Description','Count','Amount');
	$rows[] = $row;
	$row = array();
	$total_revenue = 0;
	$total_payments = $payments['Total']['payment_amount'];
	$total_count = 0;
	$over_under = $cash_over = $cash_under = 0;
	unset($payments['Total']);
	$count = (count($revenue) > count($payments) ? count($revenue) : count($payments));
	$pk = $pay_keys = array_keys($payments);
	$tip_total = 0;
	for ($i = 0; $i < $count; $i++) {
		$row = array(); 
		$total_revenue += $revenue[$i]['subtotal'];
		$payment_amount = ($payments[$pk[$i]]['payment_type'] == '') ? '' : $payments[$pk[$i]]['payment_amount'];
		if ($payments[$pk[$i]]['payment_type'] == 'Tips')
			$tip_total = $payment_amount;
		$row[] = $payments[$pk[$i]]['payment_type'];
		$row[] = number_format($payment_amount,2);
		if ($pk[$i] == 'Cash' && $this->config->item('track_cash'))
		{
			$over_under = $payment_amount-$drawer_total;
			$cash_over = ($over_under > 0 ? '' : -$over_under);
			$cash_under = ($over_under < 0 ? '' : -$over_under);
			$total_count += $drawer_total;
			$row[] = number_format($drawer_total,2);
		}
		else {
			$total_count += $payment_amount;
			$row[] = number_format($payment_amount,2);
		}
		$row[] = $revenue[$i]['category'];
		$row[] = $revenue[$i]['count'];
		$row[] = number_format($revenue[$i]['subtotal'],2);
		$rows[] = $row;
	}	
	$row= array('Total',number_format($total_payments,2),number_format($total_count,2),'Revenue','',number_format($total_revenue,2));
	$rows[] = $row;
	$row= array('Difference','',number_format($over_under,2),'','','');
	$rows[] = $row;
	$row= array('','','','','','');
	$rows[] = $row;
	$row= array('Miscellaneous Debits','','','Miscellaneous Credits','','');
	$rows[] = $row;
	$row= array('Description','System','','Description','','Amount');
	$rows[] = $row;
	
	$tax_total = $debit_total = 0;
	$tk = $tax_keys = array_keys($taxes);
	for ($i = 0; $i < count($taxes); $i++) {
		$tax_total += $taxes[$tk[$i]]['tax'];
		if ($taxes[$tk[$i]]['tax'] != 0)
		{
			$row= array('','','',$taxes[$tk[$i]]['percent'],'',number_format($taxes[$tk[$i]]['tax'],2));
			$rows[] = $row;
		}
	} 
	if ($tip_total > 0)
    {
        $tax_total += $tip_total;
        $row= array('','','','Tips','',number_format($tip_total,2));
        $rows[] = $row;
    }
	$row= array('Total',number_format($debit_total,2),'','Total','',number_format($tax_total,2));
	$rows[] = $row;
	$row= array('','','','','','');
	$rows[] = $row;
	$row= array('Drawer Count','',number_format($total_count,2),'Revenue','',number_format($total_count,2));
	$rows[] = $row;
	$row= array('Miscellaneous Debits','',number_format($debit_total,2),'Miscellanous Credits','',number_format($tax_total,2));
	$rows[] = $row;
	$row= array('Cash Short','',number_format(-$cash_under,2),'Cash Over','',number_format($cash_over,2));
	$rows[] = $row;
	$row= array('Total Debits','',number_format($total_count+$debit_total-$cash_under,2),'Total Credits','',number_format($total_revenue+$tax_total+$cash_over,2));
	$rows[] = $row;
	
	//$excelXml->addArray($rows);
	//$excelXml->generateXML($title);
	$content = array_to_csv($rows);
	
	force_download(strip_tags('Z Out Report') . '.csv', $content);
	exit;
	}

// PDF OR HTML VERSIONS FOLLOW
?>

<table id="report_contents" cellpadding="0" cellspacing="0"><!-- id="item_table" -->
	<?php if ($export_excel == 2) { ?>
		<tr>
			<td colspan=6 style='text-align:center;'><?=$this->session->userdata('course_name')?></td>
		</tr>
		<tr>
			<td colspan=6 style='text-align:center;'>Z Out Report</td>
		</tr>
		<tr>
			<td colspan=6 style='text-align:center;'>for <?=date('m/d/Y', strtotime($start_date))?> - <?=date('m/d/Y', strtotime($end_date))?></td>
		</tr>
		<tr>
			<td colspan=6 style='text-align:center;'>Generated</td>
		</tr>
		<tr>
			<td colspan=6 style='text-align:center;'><?=date('m/d/Y h:ia')?></td>
		</tr>
		<tr>
			<td colspan=6><div style='width:750px'>&nbsp;</div></td>
		</tr>
	<?php } ?>
	<tr style='text-align:center; font-weight:bold;'>
		<td colspan=3 style='padding:8px'>
			Debits
		</td>
		<td colspan=3 style='padding:8px'>
			Credits
		</td>
	</tr>
	<tr style='text-align:center; font-weight:bold;'>
		<td colspan=3 style='border-right:1px solid black; border-top:1px solid black;  border-left:1px solid black; padding:8px'>
			Drawer Count
		</td>
		<td colspan=3 style='border-right:1px solid black; border-top:1px solid black; padding:8px'>
			Revenue
		</td>
	</tr>
	<tr style='border-bottom:1px solid black;'>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Description
		</td>
		<td style=' padding:2px 5px; text-align:right;'>
			System
		</td>
		<td style='border-right:1px solid black; padding:2px 5px; text-align:right;'>
			Count
		</td>
		<td style='padding:2px 5px;'>
			Description
		</td>
		<td style=' padding:2px 5px; text-align:right;'>
			Count
		</td>
		<td style='border-right:1px solid black; padding:2px 5px; text-align:right;'>
			Amount
		</td>
	</tr>
	<tr><td colspan=6 style='border-top:1px solid black'></td></tr>
	<?php 
	//print_r($payments);
	$total_revenue = 0;
	$total_payments = $payments['Total']['payment_amount'];
	$total_count = 0;
	$over_under = $cash_over = $cash_under = 0;
	unset($payments['Total']);
	$count = (count($revenue) > count($payments) ? count($revenue) : count($payments));
	// SORTING PAYMENTS INTO REVENUE AND NON-REVENUE BEFORE DISPLAYING
	$revenue_payments = array();
	$non_revenue_payments = array();
	$tip_total = 0;
	foreach ($payments as $index => $payment)
	{
		if ($payment['payment_type'] == 'Tips')
            $tip_total = $payment['payment_amount'];
		if ($payment['payment_type'] == 'Cash' || $payment['payment_type'] == 'Check' || $payment['payment_type'] == 'Credit Card')
		{
			$revenue_payments[$index] = $payment;
			$revenue_payment_total += $payment['payment_amount'];
		}
		else 
			$non_revenue_payments[$index] = $payment;
		
	}
	$revenue_payments = count($revenue_payments) > 0 ? $revenue_payments : array();
	$non_revenue_payments = count($non_revenue_payments) > 0 ? $non_revenue_payments : array();
	$payments = array_merge($revenue_payments,$non_revenue_payments);
	$pk = $pay_keys = array_keys($payments);
	$revenue_payment_total = 0;
	$non_revenue_payment_total = 0;
	$revenue_payment_count_total = 0;
	$non_revenue_payment_count_total = 0;
	for ($i = 0; $i < $count; $i++) { 
			$total_revenue += $revenue[$i]['subtotal'];
			$payment_amount = ($payments[$pk[$i]]['payment_type'] == '') ? '' : $payments[$pk[$i]]['payment_amount'];
			if ($payments[$pk[$i]]['payment_type'] == 'Cash' || $payments[$pk[$i]]['payment_type'] == 'Check' || $payments[$pk[$i]]['payment_type'] == 'Credit Card')
			{
				$revenue_payment_total += $payments[$pk[$i]]['payment_amount'];
				$revenue_payment_count_total += ($pk[$i] == 'Cash' && $this->config->item('track_cash')) ? $drawer_total : $payment_amount;
			}
			else
			{
				$non_revenue_payment_total += $payments[$pk[$i]]['payment_amount'];
				$non_revenue_payment_count_total += $payment_amount;
			}
		?>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			<?=$payments[$pk[$i]]['payment_type'];?>
		</td>
		<td style='text-align:right; padding:2px 5px;'>
			<?=number_format($payment_amount,2);?>
		</td>
		<td style='border-right:1px solid black; text-align: right; padding:2px 5px;'>
			<?php
			if ($pk[$i] == 'Cash' && $this->config->item('track_cash'))
			{
				$over_under = $payment_amount-$drawer_total;
				$cash_over = ($over_under > 0 ? '' : -$over_under);
				$cash_under = ($over_under < 0 ? '' : -$over_under);
				$total_count += $drawer_total;
				echo number_format($drawer_total,2);
			}
			else {
				$total_count += $payment_amount;
				echo number_format($payment_amount,2);
			}
			?>
		</td>
		<td style='padding:2px 5px;'>
			<?=$revenue[$i]['category'];?>
		</td>
		<td style='text-align:right; padding:2px 5px'>
			<?=number_format($revenue[$i]['count'],2);?>
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px'>
			<?=number_format($revenue[$i]['subtotal'],2);?>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Revenue Payments Total
		</td>
		<td style='text-align:right; border-top:1px solid black; padding:2px 5px;'>
			<?=number_format($revenue_payment_total,2)?>
		</td>
		<td style='border-right:1px solid black; text-align:right; border-top:1px solid black; padding:2px 5px;'>
			<?=number_format($revenue_payment_count_total,2)?>
		</td>
		<td style='padding:2px 5px;'>
		</td>
		<td>
	
		</td>
		<td style='border-right:1px solid black; text-align:right; border-top:1px solid black; padding:2px 5px;'>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Non Revenue Payments Total
		</td>
		<td style='text-align:right;  padding:2px 5px;'>
			<?=number_format($non_revenue_payment_total,2)?>
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?=number_format($non_revenue_payment_count_total,2)?>
		</td>
		<td style='padding:2px 5px;'>
		</td>
		<td>
	
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Total
		</td>
		<td style='text-align:right; border-top:1px solid black; padding:2px 5px;'>
			<?=number_format($total_payments,2)?>
		</td>
		<td style='border-right:1px solid black; text-align:right; border-top:1px solid black; padding:2px 5px;'>
			<?=number_format($total_count,2)?>
		</td>
		<td style='padding:2px 5px;'>
			Revenue
		</td>
		<td>
	
		</td>
		<td style='border-right:1px solid black; text-align:right; border-top:1px solid black; padding:2px 5px;'>
			<?=number_format($total_revenue,2)?>		
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Difference
		</td>
		<td style='text-align:right; padding:2px 5px;'>
		</td>
		<td style='border-right:1px solid black;  text-align:right; padding:2px 5px;'>
			<?=number_format($over_under,2)?>
		</td>
		<td>
			
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black;'>
			
		</td>
	</tr>
	<tr><td colspan=6 style='border-top:1px solid black'></td></tr>
	<tr style='text-align:center; font-weight:bold; border-top:2px solid black;'>
		<td colspan=3 style='border-right:1px solid black; padding:8px; border-left:1px solid black;'>
			Miscellaneous Debits
		</td>
		<td colspan=3 style='border-right:1px solid black; padding:8px'>
			Miscellaneous Credits
		</td>
	</tr>
	<tr style='border-bottom:1px solid black;'>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Description
		</td>
		<td style=' padding:2px 5px; text-align:right;'>
			System
		</td>
		<td style='border-right:1px solid black;  padding:2px 5px;'>
			
		</td>
		<td style='padding:2px 5px;'>
			Description
		</td>
		<td style=' text-align:right; padding:2px 5px;'>
			
		</td>
		<td style='border-right:1px solid black; padding:2px 5px; text-align:right;'>
			Amount
		</td>
	</tr>
	<tr><td colspan=6 style='border-top:1px solid black'></td></tr>
	<?php 
	$tax_total = $debit_total = 0;
	$tk = $tax_keys = array_keys($taxes);
	for ($i = 0; $i < count($taxes); $i++) {
		$tax_total += $taxes[$tk[$i]]['tax'];
	?>
	<tr>
		<td style='border-left:1px solid black;'>
			
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black;'>
			
		</td>
		<td style='padding:2px 5px;'>
			<?=$taxes[$tk[$i]]['percent']?>
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?=number_format($taxes[$tk[$i]]['tax'],2)?>
		</td>
	</tr>
	<?php } ?>
	<?php 
	if ($tip_total > 0) {
		$tax_total += $tip_total;
	?>
	<tr>
		<td style='border-left:1px solid black;'>
			
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black;'>
			
		</td>
		<td style='padding:2px 5px;'>
			Tips
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?=number_format($tip_total,2)?>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Total
		</td>
		<td style='text-align:right; border-top:1px solid black; text-align:right; padding:2px 5px;'>
			<?=number_format($debit_total,2)?>
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
		
		</td>
		<td style='padding:2px 5px;'>
			Total
		</td>
		<td>
	
		</td>
		<td style='border-right:1px solid black; text-align:right; border-top:1px solid black; padding:2px 5px;'>
			<?=number_format($tax_total,2)?>		
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			&nbsp;
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black; '>
		</td>
		<td style='padding:2px 5px;'>
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black;'>
		</td>
	</tr>
	<tr><td colspan=6 style='border-top:1px solid black'></td></tr>
	<tr style='border-top:1px solid black;'>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Drawer Count
		</td>
		<td>
			
		</td>
		<td style=' text-align:right; padding:2px 5px;'>
			<?= number_format($total_count,2)?>
		</td>
		<td style='padding:2px 20px;'>
			Revenue
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?= number_format($total_revenue,2); ?>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			+ Miscellaneous Debits
		</td>
		<td>
			
		</td>
		<td style=' text-align:right; padding:2px 5px;'>
			<?= number_format($debit_total,2);?>
		</td>
		<td style='padding:2px 20px;'>
			+ Miscellanous Credits
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?= number_format($tax_total,2);?>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			+ Cash Short
		</td>
		<td>
			
		</td>
		<td style=' text-align:right; padding:2px 5px;'>
			<?=number_format(-$cash_under,2)?>
		</td>
		<td style='padding:2px 20px;'>
			+ Cash Over
		</td>
		<td>
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?=number_format($cash_over,2)?>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			<!--- Cash Open-->
		</td>
		<td>
			
		</td>
		<td style=' text-align:right; padding:2px 5px;'>
		</td>
		<td>
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Total Debits
		</td>
		<td>
			
		</td>
		<td style='text-align:right; padding:2px 5px;'>
			<?=number_format($total_count+$debit_total-$cash_under,2);?>
		</td>
		<td style='padding:2px 20px;'>
			Total Credits
		</td>
		<td>
			
		</td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?=number_format($total_revenue+$tax_total+$cash_over,2);?>
		</td>
	</tr>
	<tr><td colspan=6 style='border:1px solid black'><div style='text-align: center; padding:10px;'><?php echo lang('reports_z_out_note');?></div></td></tr>
	<tr><td colspan=6 style='border-top:1px solid black'></td></tr>
	
</table>
<div id="feedback_bar"></div>
