<table id="contents">
	<tr>
		<!--td id="commands" rowspan=2>
			<div id="new_button">
        <?php if (isset($my_allowed_modules['sales'])) {?>
			<a href="<?php echo site_url('reports/detailed_sales');?>" id='sales' report_type='sales_report' class='selected_report_type'><?php echo lang('reports_sales'); ?></a>			
			<?php if ($this->config->item('track_cash')) { ?>
				<a href="<?php echo site_url('reports/detailed_register_log');?>" id='register_log_report' report_type='register_log'><?php echo lang('reports_register_log_title'); ?></a>			
			<?php } ?>
				<a href="<?php echo site_url('reports/detailed_categories');?>" id='categories_report' report_type='categories'><?php echo lang('reports_categories'); ?></a>			
			<?php if (isset($my_allowed_modules['teesheets']) && false) { ?>
				<a href="<?php echo site_url('reports/detailed_teetimes');?>" id='teetimes_report' report_type='teetimes'><?php echo lang('reports_teetimes'); ?></a>			
			<?php } ?>
			<?php if (isset($my_allowed_modules['teesheets'])) { ?>
				<a href="<?php echo site_url('reports/detailed_teesheets');?>" id='teesheets_report' report_type='teesheets'><?php echo lang('reports_teesheets'); ?></a>			
			<?php } ?>
			<a href="<?php echo site_url('reports/summary_discounts');?>" id='discounts_report' report_type='discounts'><?php echo lang('reports_discounts'); ?></a>			
			<a href="<?php echo site_url('reports/summary_payments');?>" id='payments_report' report_type='payments'><?php echo lang('reports_payments'); ?></a>	
			<a href="<?php echo site_url('reports/detailed_taxes');?>" id='taxes_report' report_type='taxes'><?php echo lang('reports_taxes'); ?></a>			
			<a href="<?php echo site_url('reports/deleted_sales');?>" id='deleted_sales_report' report_type='deleted_sales'><?php echo lang('reports_deleted_sales'); ?></a>
		<?php } ?>
	    <?php if (isset($my_allowed_modules['customers'])) {?>
			<a href="<?php echo site_url('reports/specific_customer');?>" id='customers_report' report_type='customers'><?php echo lang('reports_customers'); ?></a>		
        <?php } ?>
        <?php if (isset($my_allowed_modules['employees'])) {?>
			<a href="<?php echo site_url('reports/specific_employee');?>" id='employees_report' report_type='employees'><?php echo lang('reports_employees'); ?></a>
	    <?php } ?>
        <?php if (isset($my_allowed_modules['items'])) {?>
			<a href="<?php echo site_url('reports/summary_items');?>" id='items_report' report_type='items'><?php echo lang('reports_items'); ?></a>			
			<a href="<?php echo site_url('reports/inventory_summary');?>" id='inventory_report' report_type='inventory'><?php echo lang('reports_inventory_reports'); ?></a>	
		<?php } ?>
        <?php if (isset($my_allowed_modules['item_kits'])) {?>
			<a href="<?php echo site_url('reports/summary_item_kits');?>" id='item_kits_report' report_type='item_kits'><?php echo lang('module_item_kits'); ?></a>			
		<?php } ?>
        <?php if (isset($my_allowed_modules['suppliers'])) {?>
			<a href="<?php echo site_url('reports/specific_supplier');?>" is='suppliers_report' report_type='suppliers'><?php echo lang('reports_suppliers'); ?></a>					
		<?php } ?>
        <?php if (isset($my_allowed_modules['receivings'])) {?>
			<a href="<?php echo site_url('reports/detailed_receivings');?>" id='receivings_report' report_type='receivings'><?php echo lang('reports_receivings'); ?></a>			
		<?php } ?>
        <?php if (isset($my_allowed_modules['giftcards'])) {?>
			<a href="<?php echo site_url('reports/detailed_giftcards');?>" id='giftcards_report' report_type='giftcards'><?php echo lang('reports_giftcards'); ?></a>			
	    <?php } ?>
	    <?php if (isset($my_allowed_modules['giftcards'])) {?>
			<a href="<?php echo site_url('reports/summary_account_balances');?>" id='account_balances_report' report_type='account_balances'><?php echo lang('reports_account_balances'); ?></a>			
	    <?php } ?>
	    <?php if (isset($my_allowed_modules['sales'])) {?>
			<a href="<?php echo site_url('reports/summary_rainchecks');?>" id='rainchecks_report' report_type='rainchecks'><?php echo lang('reports_rainchecks'); ?></a>			
	    <?php } ?>
	    </div>
	    </td-->
	    <td id='filters'>
	    	<div id='filters_holder'>
	    		<?php $this->load->view("reports/filters"); ?>
	    	</div>
	    </td>
    </tr>
    <tr id='report'>
    	<td>
    		<div id='report_holder'>
    			<div id='default1'></div>
    		</div>
    	</td>
    </tr>
</table>

<script>
	var reports = {
		initialize:function() {
			$('#new_button a').click(function(e){
				e.preventDefault();
				reports.select_report_type(e.target);
				reports.change_filters();
			})
		},
		select_report_type:function(anchor) {
			console.log('selecting_report_type');
			$('.selected_report_type').removeClass('selected_report_type');
			$(anchor).addClass('selected_report_type');
			//$('#report_holder').html("<div id='default2'></div>");
		},
		change_filters:function() {
			var filter_array = [];
			var report_types = [];
			var selected_report_type = $('.selected_report_type').attr('report_type');
			switch(selected_report_type) {
				case 'customers':
					filter_array = ['date_range','customer','sale_type'];
					report_types = ['html','csv'];
					break;
				case 'employees':
					filter_array = ['date_range','employee','sale_type'];
					report_types = ['html','csv'];
					break;
				case 'sales':
					filter_array = ['sale_id','date_range','department','sale_type'];
					report_types = ['html','csv'];
					break;
				case 'categories':
					filter_array = ['date_range','department','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'teesheets':
					filter_array = ['date','teesheet'];
					report_types = ['pdf'];
					break;
				case 'discounts':
					filter_array = ['date_range','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'payments':
					filter_array = ['date_range','department','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'taxes':
					filter_array = ['date_range','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'deleted_sales':
					filter_array = ['date_range','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'items':
					filter_array = ['date_range','department','category','subcategory','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'inventory':
					filter_array = ['department','category','subcategory','supplier'];
					report_types = ['html','csv'];
					break;
				case 'item_kits':
					filter_array = ['date_range','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'suppliers':
					filter_array = ['date_range','supplier','sale_type'];
					report_types = ['html','csv'];
					break;
				case 'receivings':
					filter_array = ['date_range','sale_type'];
					report_types = ['html','csv'];
					break;
				case 'giftcards':
					filter_array = ['giftcard'];
					report_types = ['html','csv'];
					break;
				case 'account_balances':
					filter_array = [];
					report_types = ['html','csv'];
					break;
				case 'rainchecks':
					filter_array = [];
					report_types = ['html','csv'];
					break;
			}
			$('.filter_col').hide();
			for (var i in filter_array)
			{
				$('.'+filter_array[i]+'_col').show();
			}
			$('.report_type').addClass('disabled');
			for (var i in report_types)
			{
				var selector = '';
				if (report_types[i] == 'html')
					selector = '#generate_report';
				else if (report_types[i] == 'csv')
					selector = '#csv a';
				else if (report_types[i] == 'pdf')
					selector = '#pdf a';
				$(selector).removeClass('disabled');
			}
		},
		generate:function(type) {
			$('#report_holder').mask('Generating Report');
			
			$.ajax({
	           type: "POST",
	           url: this.create_url(0),
	           data: '',
	           success: function(response){
					$('#report_holder').html(response);
					$('#report_holder').unmask();
					resize_table()
	          	},
	            dataType:'html'
	         }); 
		},
		create_url:function(type){
			var dept_cat_subcat = 'department';
			var val = 'all';
			var url = 'index.php/reports/';
			var simple_date = $('#simple_radio').attr('checked');
			var complex_date = $('#complex_radio').attr('checked');
			var range_simple = $('#report_date_range_simple option:selected').val().split('/');
			var sale_id = $('#sale_id').val();
			var sd = new Date((complex_date ? $('#start_date').val() : range_simple[0]));
			var start_date = sd.getFullYear()+'-'+(sd.getMonth() < 9 ? '0' : '')+(sd.getMonth()+1)+'-'+(sd.getDate() < 10 ? '0' : '')+sd.getDate();
			var ed = new Date((complex_date ? $('#end_date').val() : range_simple[1]));
			var end_date = ed.getFullYear()+'-'+(ed.getMonth() < 9 ? '0' : '')+(ed.getMonth()+1)+'-'+(ed.getDate() < 10 ? '0' : '')+ed.getDate();
			var rd = new Date($('#report_date').val()); 
			var report_date = rd.getFullYear()+(rd.getMonth() < 10 ? '0' : '')+(rd.getMonth())+(rd.getDate() < 10 ? '0' : '')+rd.getDate();
			var teesheet_id = $('#teesheetMenu').val();
			var customer_id = $('#customer_id').val();
			var employee_id = $('#employee_id').val(); 
			var supplier_id = $('#supplier_id').val();
			var sale_department = $('#sale_department').val();
			var sale_category = $('#sale_category').val();
			var sale_subcategory = $('#sale_subcategory').val();
			var sale_type = $('#sale_type').val();
			var selected_report_type = $('.selected_report_type').attr('report_type');
			if (sale_department != 'all')
			{
				dept_cat_subcat = 'department';
				val = sale_department;
			}
			else if (sale_category != 'all')
			{
				dept_cat_subcat = 'category';
				val = sale_category;
			}
			else if (sale_subcategory != 'all')
			{
				dept_cat_subcat = 'subcategory';
				val = sale_subcategory;
			}
			
			switch(selected_report_type) {
				case 'customers':
					url += 'specific_customer/'+start_date+'/'+end_date+'/'+customer_id+'/'+sale_type+'/'+type;
					break;
				case 'employees':
					url += 'specific_employee/'+start_date+'/'+end_date+'/'+employee_id+'/'+sale_type+'/'+type;
					break;
				case 'sales':
					if (sale_id != '')
						url += 'detailed_sales/0000-00-00/3000-01-01/sales/'+type+'/all/'+encodeURIComponent(sale_id);
					else
						url += 'detailed_sales/'+start_date+'/'+end_date+'/'+sale_type+'/'+type+'/'+sale_department+'/0';
					break;
				case 'categories':
					url += 'detailed_categories/'+start_date+'/'+end_date+'/'+sale_type+'/'+sale_department+'/'+type;
					break;
				case 'teesheets':
					url += 'detailed_teesheets/'+report_date+'/'+teesheet_id;
					break;
				case 'discounts':
					url += 'summary_discounts/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					break;
				case 'payments':
					url += 'detailed_categories/'+start_date+'/'+end_date+'/'+sale_type+'/'+type+'/'+sale_department;
					break;
				case 'taxes':
					url += 'detailed_taxes/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					break;
				case 'deleted_sales':
					url += 'detailed_taxes/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					break;
				case 'items':
					url += 'summary_items/'+start_date+'/'+end_date+'/'+sale_type+'/'+type+'/'+dept_cat_subcat+'/'+encodeURIComponent(encodeURIComponent(val));
					break;
				case 'inventory':
					url += 'inventory_summary/'+type+'/'+dept_cat_subcat+'/'+val;
					break;
				case 'item_kits':
					url += 'summary_item_kits/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					break;
				case 'suppliers':
					url += 'specific_supplier/'+start_date+'/'+end_date+'/'+supplier_id+'/'+sale_type+'/'+type;
					break;
				case 'receivings':
					url += 'detailed_receivings/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					break;
				case 'giftcards':
					url += 'detailed_giftcards/'+customer_id+'/'+type;
					break;
				case 'account_balances':
					url += 'summary_account_balances/'+type;
					break;
				case 'rainchecks':
					url += 'summary_rainchecks/'+type;
					break;
			}
			return url;
		}
	};
	$(document).ready(function(){
		reports.initialize();
		if ('<?=$report_type?>' != '')
		{
			$('#<?=$report_type?>_report').click();
			reports.generate();
		}
	});
</script>