<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
?>
	<table id='reports_generating_table'>
		<tr>
			<th class='sale_id_col filter_col'><?php echo form_label(lang('reports_sale_id'), 'report_sale_id', array('class'=>'')); ?></th>
			<th class='date_range_col filter_col'><?php echo form_label(lang('reports_date_range'), 'report_date_range_label', array('class'=>'')); ?></th>
			<th class='date_col filter_col'><?php echo form_label(lang('reports_date'), 'report_date_label', array('class'=>'')); ?></th>
			<th class='day_split_col filter_col'><?php echo form_label(lang('reports_day_split'), 'report_day_split_label', array('class'=>'')); ?></th>
			<th class='teesheet_col filter_col'><?php echo form_label(lang('reports_teesheet'), 'report_teesheet_label', array('class'=>'')); ?></th>
			<th class='customer_col filter_col'><?php echo form_label(lang('reports_customer'), 'report_customer_label', array('class'=>'')); ?></th>
			<th class='giftcard_col filter_col'><?php echo form_label(lang('reports_giftcard'), 'report_giftcard_label', array('class'=>'')); ?></th>
			<th class='employee_col filter_col'><?php echo form_label(lang('reports_employee'), 'report_employee_label', array('class'=>'')); ?></th>
			<th class='supplier_col filter_col'><?php echo form_label(lang('reports_supplier'), 'report_supplier_label', array('class'=>'')); ?></th>
			<th class='terminal_col filter_col'><?php echo form_label(lang('reports_terminal'), 'reports_terminal_label', array('class'=>'')); ?></th>
			<th class='department_col filter_col'><?php echo form_label(lang('reports_department'), 'reports_department_label', array('class'=>'')); ?></th>
			<th class='category_col filter_col'><?php echo form_label(lang('reports_category'), 'reports_category_label', array('class'=>'')); ?></th>
			<th class='subcategory_col filter_col'><?php echo form_label(lang('reports_subcategory'), 'reports_subcategory_label', array('class'=>'')); ?></th>
			<th class='sale_type_col filter_col'><?php echo form_label(lang('reports_sale_type'), 'reports_sale_type_label', array('class'=>'')); ?></th>
			<th class='invoice_col filter_col'><?php echo form_label(lang('reports_invoice_month'), 'reports_sale_type_label', array('class'=>'')); ?></th>
			<th></th>
			<th class='buttons_col'>
				<div id="teesheet_download_button">
					<div class='download_options' id='download_options'>
						<ul>
						    <li id="csv"><a class='report_type' href='' target='_blank'>CSV</a></li>
						    <li id="pdf"><a class='report_type' href='' target='_blank'>PDF</a></li>
						</ul>
					</div>	    
				</div>
			</th>
		</tr>
		<tr>
			<td class='sale_id_col filter_col'>
				<?php echo form_input(array(
	            'name'=>'sale_id', 
	            'id'=>'sale_id',
	            'placeholder'=>'Sale ID', 
	            'value'=>'',
	            'size'=>'20')); ?>
	            -OR-
			</td>
			<td class='date_range_col filter_col'>
				<div id='report_date_range_simple'>
					<input type="radio" name="report_type" id="simple_radio" value='simple' <?php if (!$start) {?>checked='checked'<?php } ?> />
					<?php echo form_dropdown('report_date_range_simple',$report_date_range_simple, '', 'id="report_date_range_simple"'); ?>
				</div>
				
				<div id='report_date_range_complex'>
					<input type="radio" name="report_type" id="complex_radio" value='complex' <?php if ($start) {?>checked='checked'<?php } ?> />
					<input type='text' name='start_date' id='start_date' value='<?=$start?>' placeholder='From'/>
					-
					<input type='text' name='end_date' id='end_date' value='<?=$end?>' placeholder='To'/>
					<?php //echo form_dropdown('start_month',$months, $selected_month, 'id="start_month"'); ?>
					<?php //echo form_dropdown('start_day',$days, $selected_day, 'id="start_day"'); ?>
					<?php //echo form_dropdown('start_year',$years, $selected_year, 'id="start_year"'); ?>
					
					<?php //echo form_dropdown('end_month',$months, $selected_month, 'id="end_month"'); ?>
					<?php //echo form_dropdown('end_day',$days, $selected_day, 'id="end_day"'); ?>
					<?php //echo form_dropdown('end_year',$years, $selected_year, 'id="end_year"'); ?>
				</div>
			</td>
			<td class='date_col filter_col'>		
				<div id='report_date_simple'>
					<?php echo form_input(array('name'=>'report_date', 'id'=>'report_date', 'value'=>date('m/d/Y'))); ?>
				</div>
			</td>
			<td class='day_split_col filter_col'>
				<div id='report_day_split'>
					<input type="checkbox" name="report_day_split" id="day_split_checkbox" value='1'  />
				</div>
			</td>
			<td class='teesheet_col filter_col'>
				<div id='report_teesheet_simple'>
					<?php echo $report_teesheets; ?>
				</div>
			</td>
			<td class='customer_col filter_col'>
				<div id='report_customer'>
					<?php echo form_input(array('name'=>'customer','id'=>'customer','placeholder'=>'No Customer', 'value'=>$customer_name));
					echo form_hidden('customer_id',"$customer_id");?>
				</div>
			</td>
			<td class='giftcard_col filter_col'>
				<div id='report_giftcard'>
					<?php echo form_input(array('name'=>'giftcard','id'=>'giftcard','placeholder'=>'No Giftcard', 'value'=>$giftcard_number));
					echo form_hidden('giftcard_id',"$giftcard_id");?>
				</div>
			</td>
			<td class='employee_col filter_col'>
				<div id='report_employee'>
					<?php echo form_input(array('name'=>'employee','id'=>'employee','placeholder'=>'No Employee', 'value'=>$employee_name));
					echo form_hidden('employee_id',"$employee_id");?>
				</div>
			</td>
			<td class='supplier_col filter_col'>
				<div id='report_supplier'>
					<?php echo form_input(array('name'=>'supplier','id'=>'supplier','placeholder'=>'No Supplier', 'value'=>$supplier_name));
					echo form_hidden('supplier_id',"$supplier_id");?>
				</div>
			</td>
			<td class='terminal_col filter_col'>
				<div id='report_terminal'>
					<?php echo form_dropdown('terminal', $terminals, 'all', 'id="terminal"'); ?>
				</div>
			</td>
			<td class='department_col filter_col'>
				<div id='report_sale_department'>
					<?php echo form_dropdown('sale_department', $departments, 'all', 'id="sale_department"'); ?>
				</div>
			</td>
			<td class='category_col filter_col'>
				<div id='report_sale_category'>
					<?php echo form_dropdown('sale_category', $categories, 'all', 'id="sale_category"'); ?>
				</div>
			</td>
			<td class='subcategory_col filter_col'>
				<div id='report_sale_subcategory'>
					<?php echo form_dropdown('sale_subcategory', $subcategories, 'all', 'id="sale_subcategory"'); ?>
				</div>
			</td>
			<td class='sale_type_col filter_col'>
				<div id='report_sale_type'>
					<?php echo form_dropdown('sale_type',array('all' => lang('reports_all'), 'sales' => lang('reports_sales'), 'returns' => lang('reports_returns')), ($sale_type ? $sale_type : 'all'), 'id="sale_type"'); ?>
				</div>
			</td>
			<td class='invoice_col filter_col'>
				<div id='report_sale_invoice'>
					<?php echo form_input(array('name'=>'report_month', 'id'=>'report_month', 'value'=>date('m/d/Y'))); ?>
				</div>
			</td>
			<td></td>
			<td class='buttons_col'>
				<?php
				echo form_button(array(
					'name'=>'generate_report',
					'id'=>'generate_report',
					'content'=>'Generate Report',//lang('common_submit'),
					'class'=>'submit_button report_type')
				);
				?>
			</td>
		</tr>
	</table>
	
	
	
	
	




<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$('#generate_report').click(function(e){e.preventDefault;if ($('#generate_report').hasClass('disabled')) return; else reports.generate(0);})
	$('#start_date').datepicker({defaultDate:new Date(),dateFormat:'M d, yy', onSelect:function(){$('#complex_radio').attr('checked','checked')}});
	$('#end_date').datepicker({defaultDate:new Date(),dateFormat:'M d, yy', onSelect:function(){$('#complex_radio').attr('checked','checked')}});
	$('#report_date').datepicker({defaultDate:new Date(),dateFormat:'M d, yy'});	
	$('#report_month').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        defaultDate:new Date(),
        dateFormat: 'MM yy',        
        onClose: function(dateText, inst) { 
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        }
    }).focus(function() {
  		$(".ui-datepicker-calendar").remove();
	});	
		
	$('#report_month').datepicker('setDate', new Date());	

	var csv = $('#csv a');
	csv.click(function(e){
		//e.preventDefault();
		if (csv.hasClass('disabled'))
		{
			e.preventDefault();
			return;
		}
		console.dir(reports.create_url(1));
		console.dir(reports.create_url(2));
		csv.attr('href', reports.create_url(1));
	});
	var pdf = $('#pdf a');
	pdf.click(function(e){
		//e.preventDefault();
		if (pdf.hasClass('disabled'))
		{
			e.preventDefault();
			return;
		}
		pdf.attr('href', reports.create_url(2));
	});
	$('#sale_department').change(function(){
		$('#sale_category').val('all');
		$('#sale_subcategory').val('all');
	});
	$('#sale_category').change(function(){
		$('#sale_department').val('all');
		$('#sale_subcategory').val('all');
	});
	$('#sale_subcategory').change(function(){
		$('#sale_category').val('all');
		$('#sale_department').val('all');
	});
	$( "#customer" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/last_name"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$("#customer").val(ui.item.label);
			$("#customer_id").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});
	$( "#giftcard" ).autocomplete({
		source: '<?php echo site_url("giftcards/suggest"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$("#giftcard").val(ui.item.label);
			$("#giftcard_id").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});
	$( "#employee" ).autocomplete({
		source: '<?php echo site_url("employees/suggest/"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$("#employee").val(ui.item.label);
			$("#employee_id").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});
	$( "#supplier" ).autocomplete({
		source: '<?php echo site_url("suppliers/suggest/"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$("#supplier").val(ui.item.label);
			$("#supplier_id").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});
	$("#start_month, #start_day, #start_year, #end_month, #end_day, #end_year").click(function()
	{
		$("#complex_radio").attr('checked', 'checked');
	});
	
	$("#report_date_range_simple").click(function()
	{
		$("#simple_radio").attr('checked', 'checked');
	});
	
});
</script>