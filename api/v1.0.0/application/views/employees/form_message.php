<ul id="error_message_box"></ul>
<?php
echo form_open('teesheets/save_note/'.$message_info->message_id,array('id'=>'note_form'));
?>
<style>
	#teesheets #cboxContent fieldset div.field_row label, #reservations #cboxContent fieldset div.field_row label {
		width: 120px;
	}
</style>
<fieldset id="item_basic_info">
<div id='course_message_info'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('employees_recipient').':', 'name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'employee_name',
			'id'=>'employee_name',
			'value'=>$message_info->employee_name)
		);?> (optional)
		</div>
		<?php 
			echo form_hidden('recipient_id', $message_info->recipient);
		?>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_message').':', 'name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_textarea(array(
			'name'=>'message',
			'id'=>'message',
			'value'=>$message_info->message)
		);?>
		</div>
	</div>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_save'),
		'class'=>'submit_button float_right')
	);
	?>
</div>
</fieldset>
</form>
<script>
$(document).ready(function(){
	$( "#employee_name" ).autocomplete({
		source: "<?php echo site_url('employees/suggest');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$('#employee_name').val(ui.item.label);
			$('#recipient_id').val(ui.item.value);
		}
	});
	$('#note_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
				success:function(response)
				{
					// var message = $('#message').val();
					// var now = new Date();
					// var strDateTime = [[AddZero(now.getDate()), AddZero(now.getMonth() + 1), now.getFullYear()].join("/"), [AddZero(now.getHours()), AddZero(now.getMinutes())].join(":"), now.getHours() >= 12 ? "PM" : "AM"].join(" ");
// 					
					// //Pad given value to the left with "0"
					// function AddZero(num) {
					    // return (num >= 0 && num < 10) ? "0" + num : num + "";
					// }
					// var message_html = '';
					// message_html = "<div class='message'>"+message+"</div>";
					// message_html += "<div class='date_posted'>"+strDateTime+"</div>";
					$('#note_list').html(response.note_html);
					//console.log()
					$.colorbox.close();
					//ADD MESSAGE TO SIDE COLUMN
					
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{

   		},
		messages:
		{

		}
	});
});
</script>