<form class="support-search-big" id="support-search" method="get" action="<?= site_url('support/search') ?>">

	<div class="outer">
		<div class="inner">
			<input type="text" value="" maxlength="100" name="q" id="q" class="ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true"/>

			<div class="question-big" id="question-mask">Have a Question?  Ask or enter a search term here.</div>
			<input type="submit" value="Search" id="support-search-submit"/>
		</div>
	</div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $( "#q" ).bind("autocompleteopen", function(event, ui) {
      $('.ui-autocomplete').css({'margin':'0 0 0 -5px', 'width':'845px'});
    });

    $("#q").autocomplete({
      source: '<?= site_url('support/auto_complete'); ?>' + '?rand=' + Math.round(Math.random() * 100000000000).toString(),
      delay: 200,
      minLength: 2,
      select: function(event, ui) {
        $(location).attr('href', ui.item.id);
      },
      focus: function(event, ui) { return false; }
    }).data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.label + "</a>" )
				.appendTo( ul );
		};

		$('#q').focus(function(){
			$('#question-mask').hide();
		});

		$('#q').blur(function(){
			if ($(this).val().length == 0) {
				$('#question-mask').show();
			}
		});

		$('#question-mask').click(function() {
			$('#q').focus();
		});

		$('form').submit(function(){
	    	$('input[type=text]').each(function(){
				$(this).val($.trim($(this).val()))
			});
		});

      // Default Focus
    $( "#q" ).bind("autocompleteopen", function(event, ui) {
        $('.ui-autocomplete').css({'margin':'0 0 0 -5px', 'width':'845px'});
    });
});
</script>