<?= $superadmin ?>

<div class="wrapper">
  <?= $search_form; ?>
  <div id="breadcrumbs">
    <a href="<?= site_url('support'); ?>">Home</a> › <?= $topic; ?>
  </div>  
  <div id="support-main">    
      <div class="support-body">
        <div class="content articles">
          <div class="title">
          	<?php 
          		switch($topic)
			      {
			        case 'Tee Sheets': $module_id = 'teesheets'; break;
			        case 'Inventory': $module_id = 'items'; break;
			        case 'Settings': $module_id = 'config'; break;
			        case 'Marketing': $module_id = 'marketing_campaigns'; break;
			        default:
			          $module_id = strtolower(str_replace(' ', '_', $topic));
			        break;
			      }
			          		
          		$lc_topic = strtolower(str_replace(' ', '_', $module_id));
          	?>
            <h3><img style='vertical-align:middle; margin-right:8px;' src='<?php echo base_url("images/menubar/{$lc_topic}.png")?>' /><span><?= $topic; ?></span></h3>
          </div>
          <?php if(count($topics) > 0): ?>
          <ul>
            <?php foreach($topics as $topic): ?>
            <li class="article">
              <h4>
                <a href="<?= site_url("support/get_topic/{$topic['id']}"); ?>">
                  <span id="post-title-<?= $topic['id'] ?>"><?= $topic['title'] ?></span>
                </a>
              </h4>
              <div id="post-content-<?= $topic['id'] ?>"><?= $topic['content'] ?></div>
              <div class="meta"><?= $topic['date_modified']; ?></div>
              <p><?= $topic['admin_opts']; ?></p>
              <span id="post-type-<?= $topic['id'] ?>" class="hidden"><?= $topic['type'] ?></span>
              <span id="post-topic-<?= $topic['id'] ?>" class="hidden"><?= $topic['topic_id'] ?></span>
              <span id="post-keywords-<?= $topic['id'] ?>" class="hidden"><?= $topic['keywords'] ?></span>
            </li>
            <?php endforeach; ?>
          </ul>
          <?php else: ?>
          <div><center><p><i>No topics to display.</i></p></center></div>
          <?php endif; ?>          
        </div>
    </div>
  </div>
  <?= $side_bar ?>
</div>

<div id="dialog" title="Confirmation Required" class="hidden">
  Are you sure you want to delete the selected article?
</div>


<script type="text/javascript">
$(document).ready(function()
{
  // reset all values
var reset = function(){
    $('#topic_module').attr('disabled', '');
    $('#articleSubmitButton').removeClass('hidden');
    $('#topic_type').val('');
    $('#topic_module').val('');
    $('#topic_title').val('');
    $('#topic_content').val('');
    $('#topic_id').val('');
    $('#topic_keywords').val('');
    $('#topic_module').val('<?= $topic_id; ?>');
  }

  reset();

  $('#topic_content').wysiwyg({
    initialContent: '',
    maxHeight:25,
    height:25,
    width: 239,
    maxWidth:239,
    controls: {
      html: {visible : true }
    },
    iFrameClass: 'wysiwyg-title-iframe'
  });

  $('.article-edit').click(function(){
      reset();
      var id = $(this)[0].attributes[0].value;
      var id_parts = id.split('-');

      var id = id_parts[0];

      var type = $('#post-type-'+id).html();
      var topic = $('#post-topic-'+id).html();
      var title = $('#post-title-'+id).html();
      var content = $('#post-content-'+id).html();
      var keywords = $('#post-keywords-'+id).html();
      
      $('#topic_type').val(type);     
      $('#topic_title').val(title);
      $('#topic_content').val(content);
      $('#topic_id').val(id);
      $('#topic_keywords').val(keywords);      
      
      $('#topic_content-wysiwyg-iframe').contents().find('body').html(content);
      $('#topic_title').trigger('blur');
  });

  $('#articleSubmitButton').click(function(e){
    e.preventDefault();

    var content = $('#topic_content').val();
    content = $.trim(content);
    var title = $('#topic_title').val();
    title = $.trim(title);
    if(content.length < 1 || title.length < 1)
    {
       alert('Error: Title or Content must not be empty.');
    }
    else
    {
      var ra = '{"list":[';
      for(j=0;j<5;j++)
      {
        var input_id = '#topic_related_articles-' + j;
        var mask_id = '#ra-mask-' + j;
        var id = $(mask_id).html();
        if(id.length > 0) ra = ra + obj_to_string({id:id,title:$(input_id).val()}) + ',';
      }

      ra = ra.substring(0, ra.length-1);
      ra = ra + ']}';

      $.ajax({
          url: "<?= site_url('support/save'); ?>",
          type: "POST",
          data: {
              id: $('#topic_id').val(),
              type: $('#topic_type').val(),
              title: title,
              content: content,
              topic_id: $('#topic_module').val(),
              keywords: $('#topic_keywords').val(),
              related_articles: ra
          },
          success: function( res ) {
            alert(res);
            window.location.reload();
          },
          dataType:'text'
        });
      }
  });

  $('.article-delete').click(function(){
      var id = $(this)[0].attributes[0].value;
      var id_parts = id.split('-');
      
      $("#dialog").dialog({
      buttons : {
        "Confirm" : function() {
          //Some ajax here
          $.ajax({
            url: "<?= site_url('support/delete_post/'); ?>",
            type: "POST",
            data: {
                id: id_parts[0]
            },
            success: function( res ) {
              alert(res);
              window.location.reload();
//              window.location.href=window.location.href
            },
            dataType:'text'
          });
          $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $("#dialog").dialog("open");

  });
});
</script>