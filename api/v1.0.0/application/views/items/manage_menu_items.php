<?php
echo form_open('items/add_take_out_menu_items/',array('id'=>'menu_items_form'));
?>
<ul id="error_message_box"></ul>
<fieldset id="customer_menu">
    <legend><?php echo $menu_items['menu_name']; ?></legend>


<div id='menu_container'>
	<div class="menu_title">	

</div>
<div class='menu_subtitle'>

</div>
	
<?php 
$count = 1;
foreach($menu_items['menu_data'] as $category=>$menu)
{
    
    
    echo $category . '</br>';
    foreach($menu['item_name'] as $key=>$item_name)
    {
       // log_message('error', 'CATEGORY?: ' . $key . ' ' . $menu_items['item_checks'][$menu['item_id'][$key]]);
?>   
        
	<div class='form_field' id='row_<?php echo $menu['item_id'][$key]; ?>'>
		
            <div><input type='checkbox' name='<?php echo 'item_' . $count++;?>' value='<?php echo $menu['item_id'][$key]; ?>' <?php echo ($menu_items['item_checks'][$menu['item_id'][$key]]?'checked':'');?>><span class="items_label"><?php echo $item_name; ?></span></div>
        
	</div>
<? 

} 
}?>
    <input type="hidden" name="menu_id" value='<?php echo $menu_items['menu_id']; ?>'/>
    <input type="hidden" name="max_num_items" value='<?php echo $count; ?>'/>
    <div class='' id='add_items_button'><?php echo 'Select Items into your menu';?></div>
<div id='clear' class='clear'></div>
</div>

</fieldset>
<?php 
echo form_close();
?>
<script>
    
    $(document).ready(function(){
        $('#add_items_button').click(function(){
		$(this).submit();
	});
    
   		//$.colorbox.resize({width:850, maxHeight:700});
    
 
        console.dir('applying menu_form ajax handler');
    var submitting = false;
    $('#menu_items_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
                    $(form).ajaxSubmit({
			success:function(response)
			{
				
				$.colorbox.close();
				
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
   		},
		messages:
		{
		}
	});
       })
</script>