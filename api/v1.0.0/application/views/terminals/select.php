<div id='terminal_selector'>
	<h2>Please select your current station:</h2>
	<?php 
	foreach($terminals as $terminal)
	{
		echo "<div id='tid_$terminal->terminal_id' class='terminal_button'>$terminal->label</div>";
	}
	?>
	<div id='tid_0' class='terminal_button'>Other</div>
</div>
<script>
	$(document).ready(function(){
		$.colorbox.resize();
		$('.terminal_button').click(function(){
			var terminal_id = $(this).attr('id').replace('tid_', '');
			$.ajax({
	           type: "POST",
	           url: "index.php/home/set_terminal/"+terminal_id,
	           data: '',
	           success: function(response){
	           		console.dir(response);
	           		$.colorbox.close();
			    },
	            dataType:'json'
	        });
		});
	})
</script>