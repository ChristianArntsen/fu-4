<?php

	//Revision from/to on PRODUCTION CODE
	// Date 1/4/12
	rev. unknown/120
	
	// Date 1/12/12
	// note: upgraded the sales sheet, and added customer account transactions, etc.
	rev. 120/136
	
	// Date 1/13/12
	// note: quick fix on payment and added extra data collection for cc payments
	rev. 136/137/138
	
	// Date 1/18/12
	rev. 138/148 //Note, NOT FULL UPDATE (only views/sales/register.php and css/receipt.css) had to revert views/sales/register.php back to 138, make a quick correction to commit all the way up, and then reverting back to rev 147 for continuing development
	
	// Date 1/19/12-1/20/12
	rev. 151/152 //Note: NOT FULL UPDATE (only models/reports/summary_payments.php) couple of bug fixes and summarized credit card data

	// Date 1/31/12
	rev. 138/164
	
	// Dave 2/14/12
	rev. 177/192
	U    css/register.css
	U    css/fullcalendar.css
	A    css/jquery.checkbox.css
	U    css/styles.css
	U    application/language/english/item_kits_lang.php
	U    application/language/english/employees_lang.php
	U    application/language/english/giftcards_lang.php
	U    application/language/english/sales_lang.php
	U    application/language/english/items_lang.php
	U    application/language/english/customers_lang.php
	U    application/language/english/suppliers_lang.php
	U    application/models/teesheet.php
	U    application/models/teetime.php
	U    application/models/item.php
	U    application/models/item_kit.php
	U    application/models/green_fee.php
	U    application/models/sale.php
	U    application/controllers/reports.php
	U    application/controllers/teesheets.php
	U    application/controllers/giftcards.php
	U    application/controllers/sales.php
	U    application/controllers/items.php
	U    application/views/teetimes/form_simulator.php
	U    application/views/teetimes/form.php
	U    application/views/items/manage.php
	U    application/views/people/manage.php
	U    application/views/suppliers/manage.php
	U    application/views/be/be.php
	U    application/views/item_kits/manage.php
	U    application/views/teesheets/form.php
	U    application/views/partial/footer.php
	U    application/views/partial/header.php
	U    application/views/reports/specific_input.php
	U    application/views/config/green_fees.php
	U    application/views/giftcards/form.php
	U    application/views/giftcards/manage.php
	U    application/views/sales/register.php
	A    images/sales/payments_medium.png
	A    images/pieces/empty.png
	A    images/pieces/checkbox_final.png
	A    images/pieces/x3.png
	A    images/pieces/complete_sale2.png
	A    images/pieces/x_red3.png
	A    js/jquery.checkbox.js
	U    database/database_change_log.php
	Updated to revision 192.
	