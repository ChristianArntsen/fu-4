INSERT INTO `foreup_modules` VALUES('module_food_and_beverage', 'module_food_and_beverage_desc', 11, 'food_and_beverage');

ALTER TABLE  `foreup_courses` ADD  `food_and_beverage` TINYINT NOT NULL AFTER  `events`;

ALTER TABLE  `foreup_employees` ADD  `pin` MEDIUMINT NULL AFTER  `password` ,
ADD  `card` VARCHAR( 255 ) NULL AFTER  `pin`;

ALTER TABLE  `ejacketa_pos`.`foreup_employees` ADD UNIQUE  `pin` (  `course_id` ,  `pin` );

ALTER TABLE  `ejacketa_pos`.`foreup_employees` ADD UNIQUE  `card` (  `course_id` ,  `card` );

ALTER TABLE  `foreup_courses` ADD  `fnb_login` TINYINT NOT NULL AFTER  `after_sale_load`;

ALTER TABLE `foreup_items` ADD `food_and_beverage` BOOLEAN NOT NULL DEFAULT '0' AFTER `reorder_level`;

// SUSPENDED SALES FOR FNB ARE NOW TABLES

CREATE TABLE `foreup_tables` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `course_id` int(11) NOT NULL,
  `table_id` tinyint(4) NOT NULL,
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `table_id` (`course_id`,`table_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `foreup_table_items`
--

CREATE TABLE `foreup_table_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `foreup_table_items_modifiers`
--

CREATE TABLE `foreup_table_items_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `option` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `foreup_table_items_taxes`
--

CREATE TABLE `foreup_table_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `foreup_table_item_kits`
--

CREATE TABLE `foreup_table_item_kits` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_kit_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_kit_cost_price` decimal(15,2) NOT NULL,
  `item_kit_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`),
  KEY `item_kit_id` (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `foreup_table_item_kits_taxes`
--

CREATE TABLE `foreup_table_item_kits_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `foreup_table_payments`
--

CREATE TABLE `foreup_table_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `foreup_tables`
--
ALTER TABLE `foreup_tables`
  ADD CONSTRAINT `foreup_tables_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `foreup_employees` (`person_id`),
  ADD CONSTRAINT `foreup_tables_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `foreup_customers` (`person_id`);

--
-- Constraints for table `foreup_table_items`
--
ALTER TABLE `foreup_table_items`
  ADD CONSTRAINT `foreup_table_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`),
  ADD CONSTRAINT `foreup_table_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_tables` (`sale_id`);

--
-- Constraints for table `foreup_table_items_taxes`
--
ALTER TABLE `foreup_table_items_taxes`
  ADD CONSTRAINT `foreup_table_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_table_items` (`sale_id`),
  ADD CONSTRAINT `foreup_table_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`);

--
-- Constraints for table `foreup_table_item_kits`
--
ALTER TABLE `foreup_table_item_kits`
  ADD CONSTRAINT `foreup_table_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`),
  ADD CONSTRAINT `foreup_table_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_tables` (`sale_id`);

--
-- Constraints for table `foreup_table_item_kits_taxes`
--
ALTER TABLE `foreup_table_item_kits_taxes`
  ADD CONSTRAINT `foreup_table_item_kits_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_table_item_kits` (`sale_id`),
  ADD CONSTRAINT `foreup_table_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`);

--
-- Constraints for table `foreup_table_payments`
--
ALTER TABLE `foreup_table_payments`
  ADD CONSTRAINT `foreup_table_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_tables` (`sale_id`);