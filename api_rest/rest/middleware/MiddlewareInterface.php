<?php
namespace foreup\rest\middleware;

use Silex\Application;

interface MiddlewareInterface
{
	public static function register(Application &$application);
}