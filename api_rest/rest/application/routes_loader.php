<?php
namespace foreup\rest\application;

use Silex\Application;
use foreup\rest\controllers;

class routes_loader
{
	protected $app;

	public function __construct(Application &$app)
	{
		$this->app = $app;
		
		$this->instantiateControllers();
	}

	protected function instantiateControllers()
	{
		$this->app['courses.controller'] = $this->app->share(function () {
			return new controllers\courses($this->app['orm.em'],$this->app['user'],$this->app['auth.userObj']);
		});
		$this->app['sales.controller'] = $this->app->share(function () {
			return new controllers\sales($this->app['orm.em'],$this->app['user'],$this->app['auth.userObj'],$this->app['legacySaleService'] );
		});
		$this->app['teesheets.controller'] = $this->app->share(function () {
			return new controllers\teesheets($this->app['orm.em']);
		});
		$this->app['modules.controller'] = $this->app->share(function () {
			return new controllers\modules($this->app['orm.em']);
		});
		$this->app['marketing_campaigns.controller'] = $this->app->share(function () {
			return new controllers\marketing_campaigns($this->app['orm.em'],$this->app['user'],$this->app['auth.user']);
		});
		$this->app['tokens.controller'] = $this->app->share(function () {
			return new controllers\tokens($this->app['orm.em'],$this->app['jwt_token']);
		});
		$this->app['customer_images.controller'] = $this->app->share(function () {
			return new controllers\customer_images();
		});

		$this->app['tokensValidation.controller'] = $this->app->share(function () {
			return new controllers\tokens($this->app['orm.em'],$this->app['jwt_token'],$this->app['auth.userObj']);
		});
        $this->app['scores.controller'] = $this->app->share(function () {
            return new controllers\scores($this->app['orm.em'],$this->app['auth.user']);
        });
        $this->app['employee_audit_log.controller'] = $this->app->share(function () {
            return new controllers\employee_audit_log($this->app['orm.em'],$this->app['auth.user']);
        });
		$this->app['person_altered.controller'] = $this->app->share(function () {
            return new controllers\person_altered($this->app['orm.em'],$this->app['auth.user']);
        });
		$this->app['account_ledger.controller'] = $this->app->share(function () {
			return new controllers\account_ledger($this->app['orm.em'],$this->app);
		});

		$this->app['accounts.controller'] = $this->app->share(function () {
			return new controllers\accounts($this->app['orm.em'],$this->app['employee']);
		});

		$this->app['account_types.controller'] = $this->app->share(function () {
			return new controllers\account_types($this->app['orm.em'],$this->app['employee']);
		});

		$this->app['account_type_statements.controller'] = $this->app->share(function () {
			return new controllers\account_type_statements($this->app['orm.em'],$this->app['employee']);
		});

        $this->app['customers.controller'] = $this->app->share(function () {
            return new controllers\courses\customers($this->app['orm.em'],$this->app['auth.userObj']);
        });
        $this->app['customerGroups.controller'] = $this->app->share(function () {
            return new controllers\courses\customers\groups($this->app['orm.em'],$this->app['auth.userObj']);
        });
        $this->app['customerGroupMembers.controller'] = $this->app->share(function () {
            return new controllers\courses\customers\group_members($this->app['orm.em'],$this->app['auth.userObj']);
        });
        $this->app['customerSales.controller'] = $this->app->share(function () {
			return new controllers\courses\customers\sales($this->app['orm.em'],$this->app['auth.userObj']);
		});
		$this->app['items.controller'] = $this->app->share(function () {
			return new controllers\courses\items($this->app['orm.em'],$this->app['auth.userObj']);
		});
        $this->app['inventory.controller'] = $this->app->share(function () {
            return new controllers\courses\inventory($this->app['orm.em'],$this->app['auth.userObj']);
        });
        $this->app['inventoryAudits.controller'] = $this->app->share(function () {
            return new controllers\courses\inventory_audits($this->app['orm.em'],$this->app['auth.userObj']);
        });
		$this->app['terminals.controller'] = $this->app->share(function () {
			return new controllers\courses\terminals($this->app['orm.em'],$this->app['auth.userObj']);
		});
		$this->app['printers.controller'] = $this->app->share(function () {
			return new controllers\courses\printers($this->app['orm.em'],$this->app['auth.userObj']);
		});

		$this->app['priceClasses.controller'] = $this->app->share(function () {
			return new controllers\courses\priceClasses($this->app['orm.em'],$this->app['auth.userObj']);
		});

		$this->app['printerGroups.controller'] = $this->app->share(function () {
			return new controllers\courses\printerGroups($this->app['orm.em'],$this->app['auth.userObj']);
		});
		$this->app['refundReasons.controller'] = $this->app->share(function () {
			return new controllers\courses\refundReasons($this->app['orm.em'],$this->app['auth.userObj']);
		});
		$this->app['customPaymentTypes.controller'] = $this->app->share(function () {
			return new controllers\courses\customPaymentTypes($this->app['orm.em'],$this->app['auth.userObj']);
		});
        $this->app['receiptAgreements.controller'] = $this->app->share(function () {
            return new controllers\courses\receiptAgreements($this->app['orm.em'],$this->app['auth.userObj']);
        });
        $this->app['loyaltyRates.controller'] = $this->app->share(function () {
            return new controllers\courses\loyaltyRates($this->app['orm.em'],$this->app['auth.userObj']);
        });
        $this->app['onlineHours.controller'] = $this->app->share(function () {
            return new controllers\courses\onlineHours($this->app['orm.em'],$this->app['auth.userObj']);
        });


		$this->app['employees.controller'] = $this->app->share(function () {
			return new controllers\courses\employees($this->app['orm.em'],$this->app['auth.userObj']);
		});
		$this->app['courseTeesheets.controller'] = $this->app->share(function () {
			return new controllers\courses\teesheets($this->app['orm.em'],$this->app['auth.userObj']);
		});
		$this->app['coursePageSettings.controller'] = $this->app->share(function () {
			return new controllers\courses\page_settings($this->app['orm.em'],$this->app['auth.userObj']);
		});
		$this->app['courseSettings.controller'] = $this->app->share(function () {
			return new controllers\courses\settings($this->app['orm.em'],$this->app['auth.userObj']);
		});

		$this->app['hooks.controller'] = $this->app->share(function () {
			return new controllers\hooks($this->app['orm.em'],$this->app['auth.userObj']);
		});

		$this->app['teesheetBookings.controller'] = $this->app->share(function () {
			return new controllers\courses\teesheets\bookings($this->app['orm.em'],$this->app['auth.userObj'],$this->app['legacyTeetimeService']);
		});
		$this->app['bookedPlayer.controller'] = $this->app->share(function () {
			return new controllers\courses\teesheets\bookedPlayers($this->app['orm.em'],$this->app['auth.userObj']);
		});
		$this->app['teesheetTeetimes.controller'] = $this->app->share(function () {
			return new controllers\courses\teesheets\teetimes(
				$this->app['orm.em'],
				$this->app['auth.userObj'],
				$this->app['legacyTeetimeService'],
				$this->app['courseSettings'],
				$this->app['legacyCartService']
			);
		});
		$this->app['carts.controller'] = $this->app->share(function () {
			return new controllers\courses\carts($this->app['orm.em'],$this->app['auth.userObj'],$this->app['legacyCartService']);
		});
		$this->app['cartsItems.controller'] = $this->app->share(function () {
			return new controllers\courses\carts\items($this->app['orm.em'],$this->app['auth.userObj'],$this->app['legacyCartService']);
		});
		$this->app['teesheetStats.controller'] = $this->app->share(function () {
			return new controllers\courses\teesheets\stats($this->app['orm.em'],$this->app['auth.userObj'],$this->app['teesheetStats']);
		});
		$this->app['teesheetSeasons.controller'] = $this->app->share(function () {
			return new controllers\courses\teesheets\seasons($this->app['orm.em'],$this->app['auth.userObj']);
		});
        $this->app['seasonPriceClasses.controller'] = $this->app->share(function () {
            return new controllers\courses\teesheets\seasons\priceClasses($this->app['orm.em'], $this->app['auth.userObj']);
        });
        $this->app['seasonTimeframes.controller'] = $this->app->share(function () {
            return new controllers\courses\teesheets\seasons\timeframes($this->app['orm.em'], $this->app['auth.userObj']);
        });
        $this->app['reservationGroups.controller'] = $this->app->share(function () {
            return new controllers\courses\teesheets\reservationGroups($this->app['orm.em'], $this->app['auth.userObj']);
        });
        $this->app['specials.controller'] = $this->app->share(function () {
            return new controllers\courses\teesheets\specials($this->app['orm.em'], $this->app['auth.userObj']);
        });
        $this->app['accountRecurringCharges.controller'] = $this->app->share(function () {
			return new controllers\account_recurring\accountRecurringCharges($this->app['orm.em'],$this->app,$this->app['user'],$this->app['auth.userObj']);
		});
		$this->app['accountRecurringChargeItems.controller'] = $this->app->share(function () {
			return new controllers\account_recurring\accountRecurringChargeItems($this->app['orm.em'],$this->app,$this->app['user'],$this->app['auth.userObj']);

		});
		$this->app['accountRecurringChargeGroups.controller'] = $this->app->share(function () {
			return new controllers\account_recurring\accountRecurringChargeGroups($this->app['orm.em'],$this->app,$this->app['user'],$this->app['auth.userObj']);
		});
		$this->app['accountRecurringChargeCustomers.controller'] = $this->app->share(function () {
			return new controllers\account_recurring\accountRecurringChargeCustomers($this->app['orm.em'],$this->app,$this->app['user'],$this->app['auth.userObj']);
		});
		$this->app['accountRecurringStatements.controller'] = $this->app->share(function () {
			return new controllers\account_recurring\accountRecurringStatements($this->app['orm.em'],$this->app,$this->app['user'],$this->app['auth.userObj']);
		});
		$this->app['repeatable.controller'] = $this->app->share(function () {
			return new controllers\account_recurring\repeatable($this->app['orm.em'],$this->app,$this->app['user'],$this->app['auth.userObj']);
		});
		$this->app['accountStatements.controller'] = $this->app->share(function () {
			return new controllers\accountStatements($this->app['orm.em'],$this->app,$this->app['user'],$this->app['auth.userObj']);
		});
		$this->app['bulkEditJobs.controller'] = $this->app->share(function () {
			return new controllers\bulk_edit_jobs($this->app['orm.em'],$this->app,$this->app['user'],$this->app['auth.userObj']);
		});
	}

	public function bindRoutesToControllers()
	{
		$api = $this->app["controllers_factory"];

		$api->get('/customerImages/{name}', "customer_images.controller:get");
		// Resources
		$api->get('/resources', "resources.controller:getAll");
		$api->post('/resources', "resources.controller:save");
		$api->get('/resources/{id}', "resources.controller:get");
		$api->delete('/resources/{id}', "resources.controller:delete");

		$api->get('/hooks', "hooks.controller:getAll");
		$api->post('/hooks', "hooks.controller:create");
		$api->delete('/hooks/{id}', "hooks.controller:delete");

		$api->get("/tokens/validate/{token}","tokensValidation.controller:validate");


		// Courses
		$api->get('/courses', "courses.controller:getAll");
        $api->get('/courses/all', "courses.controller:reallyGetAll");
        $api->get('/courses/suggestions', "courses.controller:getSuggestions");
        $api->get('/courses/{id}', "courses.controller:get");
        $api->patch('/courses/{id}', "courses.controller:patch");
        $api->get('/courses/{courseId}/settings', "courseSettings.controller:getAll");
        $api->put('/courses/{courseId}/settings/{courseId2}', "courseSettings.controller:update");
        $api->post('/courses/{id}/mobile_token', "courses.controller:mobileToken");

		$api->get('/courses/{courseId}/customers', "customers.controller:getAll");
		$api->post('/courses/{courseId}/customers', "customers.controller:create");
		$api->get('/courses/{courseId}/customers/fields', "customers.controller:fields");
		$api->get('/courses/{courseId}/page_settings/{page}', "coursePageSettings.controller:get");
		$api->put('/courses/{courseId}/page_settings/{page}', "coursePageSettings.controller:update");
		$api->get('/courses/{courseId}/customers/{id}', "customers.controller:get");
		$api->delete('/courses/{courseId}/customers/{id}', "customers.controller:delete");
		$api->put('/courses/{courseId}/customers/{id}', "customers.controller:update");
		$api->get('/courses/{courseId}/customers/{personId}/sales', "customerSales.controller:getAll");
		$api->get('/courses/{courseId}/items', "items.controller:getAll");
		$api->post('/courses/{courseId}/items', "items.controller:create");
		$api->get('/courses/{courseId}/items/{itemId}', "items.controller:get");
		$api->get('/courses/{courseId}/inventory_audits', "inventoryAudits.controller:getAll");
        $api->get('/courses/{courseId}/inventory_audits/{inventoryAuditId}', "inventoryAudits.controller:get");
        $api->post('/courses/{courseId}/inventory_audits', "inventoryAudits.controller:create");
        $api->put('/courses/{courseId}/inventory_audits/{inventoryAuditId}', "inventoryAudits.controller:update");
        $api->get('/courses/{courseId}/inventory', "inventory.controller:getAll");
		$api->get('/courses/{courseId}/terminals', "terminals.controller:getAll");
		$api->get('/courses/{courseId}/terminals/{terminalId}', "terminals.controller:get");
		$api->put('/courses/{courseId}/terminals/{terminalId}', "terminals.controller:update");
		$api->put('/courses/{courseId}/terminals/{terminalId}', "terminals.controller:delete");
		$api->put('/courses/{courseId}/terminals', "terminals.controller:create");


		$api->get('/courses/{courseId}/printers', "printers.controller:getAll");
		$api->get('/courses/{courseId}/printers/{printerId}', "printers.controller:get");
		$api->put('/courses/{courseId}/printers/{printerId}', "printers.controller:update");
        $api->delete('/courses/{courseId}/printers/{printerId}', "printers.controller:delete");
        $api->post('/courses/{courseId}/printers', "printers.controller:create");

		$api->get('/courses/{courseId}/priceClasses', "priceClasses.controller:getAll");
		$api->get('/courses/{courseId}/priceClasses/{priceClassId}', "priceClasses.controller:get");
		$api->put('/courses/{courseId}/priceClasses/{priceClassId}', "priceClasses.controller:update");
		$api->delete('/courses/{courseId}/priceClasses/{priceClassId}', "priceClasses.controller:delete");
		$api->post('/courses/{courseId}/priceClasses', "priceClasses.controller:create");

		$api->get('/courses/{courseId}/printerGroups', "printerGroups.controller:getAll");
		$api->get('/courses/{courseId}/printerGroups/{priNnterGroupId}', "printerGroups.controller:get");
		$api->put('/courses/{courseId}/printerGroups/{printerGroupId}', "printerGroups.controller:update");
        $api->delete('/courses/{courseId}/printerGroups/{printerGroupId}', "printerGroups.controller:delete");
        $api->post('/courses/{courseId}/printerGroups', "printerGroups.controller:create");

		$api->get('/courses/{courseId}/refundReasons', "refundReasons.controller:getAll");
		$api->get('/courses/{courseId}/refundReasons/{reasonId}', "refundReasons.controller:get");
		$api->put('/courses/{courseId}/refundReasons/{reasonId}', "refundReasons.controller:update");
        $api->delete('/courses/{courseId}/refundReasons/{reasonId}', "refundReasons.controller:delete");
        $api->post('/courses/{courseId}/refundReasons', "refundReasons.controller:create");

		$api->get('/courses/{courseId}/customPaymentTypes', "customPaymentTypes.controller:getAll");
		$api->get('/courses/{courseId}/customPaymentTypes/{paymentId}', "customPaymentTypes.controller:get");
		$api->put('/courses/{courseId}/customPaymentTypes/{paymentId}', "customPaymentTypes.controller:update");
        $api->delete('/courses/{courseId}/customPaymentTypes/{paymentId}', "customPaymentTypes.controller:delete");
		$api->post('/courses/{courseId}/customPaymentTypes', "customPaymentTypes.controller:create");

		$api->get('/courses/{courseId}/receiptAgreements', "receiptAgreements.controller:getAll");
		$api->get('/courses/{courseId}/receiptAgreements/{agreementId}', "receiptAgreements.controller:get");
		$api->put('/courses/{courseId}/receiptAgreements/{agreementId}', "receiptAgreements.controller:update");
		$api->post('/courses/{courseId}/receiptAgreements', "receiptAgreements.controller:create");

        $api->get('/courses/{courseId}/loyaltyRates', "loyaltyRates.controller:getAll");
        $api->get('/courses/{courseId}/loyaltyRates/{loyaltyRateId}', "loyaltyRates.controller:get");
        $api->put('/courses/{courseId}/loyaltyRates/{loyaltyRateId}', "loyaltyRates.controller:update");
        $api->delete('/courses/{courseId}/loyaltyRates/{loyaltyRateId}', "loyaltyRates.controller:delete");
        $api->post('/courses/{courseId}/loyaltyRates', "loyaltyRates.controller:create");

		$api->get('/courses/{courseId}/employees', "employees.controller:getAll");
		$api->get('/courses/{courseId}/teesheets', "courseTeesheets.controller:getAll");
        $api->post('/courses/{courseId}/teesheets', "courseTeesheets.controller:create");
        $api->get('/courses/{courseId}/teesheets/{teesheetId}', "courseTeesheets.controller:getOne");
        $api->put('/courses/{courseId}/teesheets/{teesheetId}', "courseTeesheets.controller:update");
        $api->delete('/courses/{courseId}/teesheets/{teesheetId}', "courseTeesheets.controller:delete");

        $api->get('/courses/{courseId}/teesheets/{teesheetId}/bookings', "teesheetBookings.controller:getAll");
		$api->post('/courses/{courseId}/teesheets/{teesheetId}/bookings', "teesheetBookings.controller:create");
		$api->get('/courses/{courseId}/teesheets/{teesheetId}/bookings/{bookingId}', "teesheetBookings.controller:getOne");
		$api->put('/courses/{courseId}/teesheets/{teesheetId}/bookings/{bookingId}', "teesheetBookings.controller:update");
		$api->delete('/courses/{courseId}/teesheets/{teesheetId}/bookings/{bookingId}', "teesheetBookings.controller:delete");
		$api->get('/courses/{courseId}/teesheets/{teesheetId}/teetimes', "teesheetTeetimes.controller:getAll");
		$api->get('/courses/{courseId}/teesheets/{teesheetId}/teetimes/{teetimeId}', "teesheetTeetimes.controller:getOne");
		$api->get('/courses/{courseId}/teesheets/{teesheetId}/bookings/{teetimeId}/pricing', "teesheetTeetimes.controller:getPricing");

		$api->post('/courses/{courseId}/teesheets/{teesheetId}/bookings/{teetimeId}/checkIn', "teesheetTeetimes.controller:checkIn");
		$api->put('/courses/{courseId}/carts/{cartId}', "carts.controller:update");
		$api->get('/courses/{courseId}/carts/{cartId}', "carts.controller:getOne");
		$api->post('/courses/{courseId}/carts/{cartId}/payments', "carts.controller:addPayment");
		$api->post('/courses/{courseId}/carts/{cartId}/items', "cartsItems.controller:create");
		$api->put('/courses/{courseId}/carts/{cartId}/items/{line}', "cartsItems.controller:update");
		$api->delete('/courses/{courseId}/carts/{cartId}/items/{line}', "cartsItems.controller:delete");


		$api->get('/courses/{courseId}/teesheets/{teesheetId}/stats', "teesheetStats.controller:getAll");
		$api->patch('/courses/{courseId}/teesheets/{teesheetId}/bookings/{bookingId}/bookedPlayers/{bookedPlayerId}', "bookedPlayer.controller:update");
		$api->put('/courses/{courseId}/teesheets/{teesheetId}/bookings/{bookingId}/bookedPlayers/{bookedPlayerId}', "bookedPlayer.controller:update");

        /** Reservation Groups */
        $api->get('/courses/{courseId}/teesheets/{teesheetId}/reservationGroups', "reservationGroups.controller:getAll");
        $api->get('/courses/{courseId}/teesheets/{teesheetId}/reservationGroups/{reservationGroupId}', "reservationGroups.controller:get");
        $api->put('/courses/{courseId}/teesheets/{teesheetId}/reservationGroups/{reservationGroupId}', "reservationGroups.controller:update");
        $api->post('/courses/{courseId}/teesheets/{teesheetId}/reservationGroups', "reservationGroups.controller:create");
        $api->delete('/courses/{courseId}/teesheets/{teesheetId}/reservationGroups/{reservationGroupId}', "reservationGroups.controller:delete");


        /** Specials */
        $api->get('/courses/{courseId}/teesheets/{teesheetId}/specials', "specials.controller:getAll");
        $api->get('/courses/{courseId}/teesheets/{teesheetId}/specials/{seasonId}', "specials.controller:get");
        $api->patch('/courses/{courseId}/teesheets/{teesheetId}/specials/{seasonId}', "specials.controller:update");
        $api->post('/courses/{courseId}/teesheets/{teesheetId}/specials', "specials.controller:create");
        $api->delete('/courses/{courseId}/teesheets/{teesheetId}/specials/{seasonId}', "specials.controller:delete");


        /** Online Hours */
        //$api->get('/courses/{courseId}/onlineHours', "onlineHours.controller:getAll");
        $api->get('/courses/{courseId}/onlineHours/{teesheetId}', "onlineHours.controller:get");
        $api->patch('/courses/{courseId}/onlineHours/{teesheetId}', "onlineHours.controller:update");
        //$api->post('/courses/{courseId}/onlineHours', "onlineHours.controller:create");
        //$api->delete('/courses/{courseId}/onlineHours/{onlineHoursId}', "onlineHours.controller:delete");



        /** Teesheet Seasons */
		$api->get('/courses/{courseId}/teesheets/{teesheetId}/seasons', "teesheetSeasons.controller:getAll");
		$api->get('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}', "teesheetSeasons.controller:get");
		$api->patch('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}', "teesheetSeasons.controller:update");
		$api->post('/courses/{courseId}/teesheets/{teesheetId}/seasons', "teesheetSeasons.controller:create");
		$api->delete('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}', "teesheetSeasons.controller:delete");



        $api->get('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}/priceClasses', "seasonPriceClasses.controller:getAll");
        $api->get('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}/priceClasses/{priceClassId}', "seasonPriceClasses.controller:get");
        $api->patch('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}/priceClasses/{priceClassId}', "seasonPriceClasses.controller:update");
        $api->post('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}/priceClasses', "seasonPriceClasses.controller:create");
        $api->delete('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}/priceClasses/{priceClassId}', "seasonPriceClasses.controller:delete");

        $api->get('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}/timeframes', "seasonTimeframes.controller:getAll");
        $api->get('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}/timeframes/{timeframeId}', "seasonTimeframes.controller:get");
        $api->patch('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}/timeframes/{timeframeId}', "seasonTimeframes.controller:update");
        $api->post('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}/timeframes', "seasonTimeframes.controller:create");
        $api->delete('/courses/{courseId}/teesheets/{teesheetId}/seasons/{seasonId}/timeframes/{timeframeId}', "seasonTimeframes.controller:delete");

        // Customer Groups
        $api->get('/courses/{courseId}/customer_groups', "customerGroups.controller:getAll");
        $api->post('/courses/{courseId}/customer_groups', "customerGroups.controller:create");
        $api->put('/courses/{courseId}/customer_groups/{id}', "customerGroups.controller:update");
        $api->delete('/courses/{courseId}/customer_groups/{id}', "customerGroups.controller:delete");
        
		// Employees
        //$api->get('/employees', "employees.controller:getAll");
        //$api->get('/employees/suggestions', "employees.controller:getSuggestions");
        // TODO: implement get and patch for employees
        //$api->get('/employees/{id}', "employees.controller:get");
        //$api->patch('/employees/{id}', "employees.controller:patch");

		// Tee Sheets
		$api->get('/teesheets', "teesheets.controller:getAll");
		$api->post('/teesheets', "teesheets.controller:save");
		$api->get('/teesheets/{id}', "teesheets.controller:get");
		$api->delete('/teesheets/{id}', "teesheets.controller:delete");
		$api->get('/teesheets/{id}/teetimes', "teesheets.controller:getAllTeetimes");



		// Modules
		$api->get('/modules', "modules.controller:getAll");
		$api->post('/modules', "modules.controller:save");
		$api->get('/modules/{id}', "modules.controller:get");
		$api->delete('/modules/{id}', "modules.controller:delete");

		// Marketing Campaigns
		$api->get('/marketing_campaigns', "marketing_campaigns.controller:getAll");
		$api->post('/marketing_campaigns', "marketing_campaigns.controller:save");
		$api->put('/marketing_campaigns/{id}', "marketing_campaigns.controller:update");
		$api->get('/marketing_campaigns/{id}', "marketing_campaigns.controller:get");
		$api->delete('/marketing_campaigns/{id}', "marketing_campaigns.controller:delete");
		$api->patch('/marketing_campaigns/{id}', "marketing_campaigns.controller:patch");
		$api->post('/marketing_campaigns/{id}/promote_to_sent', "marketing_campaigns.controller:promoteToSent");
		$api->post('/marketing_campaigns/{id}/demote_to_draft', "marketing_campaigns.controller:demoteToDraft");

        $api->get('/employee_audit_log','employee_audit_log.controller:getAll');
        $api->post('/employee_audit_log','employee_audit_log.controller:save');
        $api->get('/courses/{id}/employee_audit_log','employee_audit_log.controller:get_course_log_entries');
        $api->get('/employee/{id}/employee_audit_log','employee_audit_log.controller:get_employee_log_entries');
        $api->post('/employee_audit_log/{id}/undo','employee_audit_log.controller:undo_logged_action');
        $api->post('/employee_audit_log/{id}/redo','employee_audit_log.controller:redo_logged_action');

        $api->post('/person_altered','person_altered.controller:save');
        $api->get('/courses/{id}/person_altered','person_altered.controller:get_course_log_entries');
        $api->get('/employee/{id}/person_altered','person_altered.controller:get_employee_log_entries');

		$api->post('/tokens', "tokens.controller:save");

		// Account ledger
		$api->post('/accounts/{account_id}/debit',"accounts.controller:debit");
		$api->post('/accounts/{account_id}/credit',"accounts.controller:credit");
		$api->post('/accounts/{account_id}/payment',"accounts.controller:payment");
		$api->post('/accounts/',"accounts.controller:create");

		$api->post('/account_types/',"account_types.controller:create");

		$api->post('/account_types/{account_type_id}/statement', "account_type_statements.controller:create");

		$api->get('/courses/{courseId}/accountRecurringCharges/{id}','accountRecurringCharges.controller:get');
		$api->get('/courses/{courseId}/accountRecurringCharges','accountRecurringCharges.controller:getAll');
		$api->post('/courses/{courseId}/accountRecurringCharges','accountRecurringCharges.controller:create');
		$api->patch('/courses/{courseId}/accountRecurringCharges/{id}','accountRecurringCharges.controller:update');
		$api->delete('/courses/{courseId}/accountRecurringCharges/{id}','accountRecurringCharges.controller:delete');
		$api->put('/courses/{courseId}/accountRecurringCharges/{id}','accountRecurringCharges.controller:put');
		$api->put('/courses/{courseId}/accountRecurringCharges','accountRecurringCharges.controller:put');

		$api->get('/courses/{courseId}/accountRecurringStatements/{id}','accountRecurringStatements.controller:get');
		$api->get('/courses/{courseId}/accountRecurringStatements','accountRecurringStatements.controller:getAll');
		$api->post('/courses/{courseId}/accountRecurringStatements','accountRecurringStatements.controller:create');
		//$api->patch('/courses/{courseId}/accountRecurringStatements/{id}','accountRecurringStatements.controller:update');
		$api->delete('/courses/{courseId}/accountRecurringStatements/{id}','accountRecurringStatements.controller:delete');
		$api->put('/courses/{courseId}/accountRecurringStatements/{id}','accountRecurringStatements.controller:put');
		$api->put('/courses/{courseId}/accountRecurringStatements','accountRecurringStatements.controller:put');

		$api->get('/courses/{courseId}/bulkEditJobs/{id}','bulkEditJobs.controller:get');
		$api->get('/courses/{courseId}/bulkEditJobs','bulkEditJobs.controller:getAll');
        $api->post('/courses/{courseId}/bulkEditJobs','bulkEditJobs.controller:create');
        $api->put('/courses/{courseId}/bulkEditJobs/{id}','bulkEditJobs.controller:update');
		$api->delete('/courses/{courseId}/bulkEditJobs/{id}','bulkEditJobs.controller:delete');

		$api->get('/courses/{courseId}/accountRecurringChargeCustomers/{id}','accountRecurringChargeCustomers.controller:get');
		$api->get('/courses/{courseId}/accountRecurringCharges/{rc_id}/accountRecurringChargeCustomers','accountRecurringChargeCustomers.controller:getAll');
		$api->post('/courses/{courseId}/accountRecurringChargeCustomers','accountRecurringChargeCustomers.controller:create');
		$api->patch('/courses/{courseId}/accountRecurringChargeCustomers/{id}','accountRecurringChargeCustomers.controller:update');
		$api->delete('/courses/{courseId}/accountRecurringChargeCustomers/{id}','accountRecurringChargeCustomers.controller:delete');

		$api->get('/courses/{courseId}/accountRecurringChargeGroups/{id}','accountRecurringChargeGroups.controller:get');
		$api->get('/courses/{courseId}/accountRecurringCharges/{rc_id}/accountRecurringChargeGroups','accountRecurringChargeGroups.controller:getAll');
		$api->post('/courses/{courseId}/accountRecurringChargeGroups','accountRecurringChargeGroups.controller:create');
		$api->patch('/courses/{courseId}/accountRecurringChargeGroups/{id}','accountRecurringChargeGroups.controller:update');
		$api->delete('/courses/{courseId}/accountRecurringChargeGroups/{id}','accountRecurringChargeGroups.controller:delete');

		$api->get('/accountRecurringChargeItems/{id}','accountRecurringChargeItems.controller:get');
		$api->get('/courses/{courseId}/accountRecurringChargeItems','accountRecurringChargeItems.controller:getAll');


		$api->delete('/courses/{courseId}/accountStatements/{id}','accountStatements.controller:delete');
        $api->get('/courses/{courseId}/accountStatements/fields','accountStatements.controller:fields');
        $api->get('/courses/{courseId}/accountStatements/{id}','accountStatements.controller:get');
		$api->get('/courses/{courseId}/accountStatements','accountStatements.controller:getAll');
		$api->get('/courses/{courseId}/accountStatements/{id}/pdf','accountStatements.controller:getStatementPdf');
        $api->get('/courses/{courseId}/accountStatements/{id}/html','accountStatements.controller:getStatementHtml');

		$api->post('/scores', "scores.controller:save");

		$api->get('/courses/{id}/sales','sales.controller:getAll');
		$api->get('/courses/{id}/sales/{saleId}', "sales.controller:getOne");
		$api->post('/courses/{courseId}/sales','sales.controller:create');
		// Look for prepended API Endpoint
		$this->app->mount($this->app["config.api.endpoint"], $api);
	}
}