<?php
namespace foreup\rest\application;

use foreup\rest\middleware;
use Silex\Application;

class middleware_loader
{
	protected $app;

	public function __construct(Application &$app)
	{
		$this->app = $app;
	}

	public function load_middleware()
	{
		middleware\cors::register($this->app);
		middleware\authentication::register($this->app);
		middleware\json_only::register($this->app);
	}
}
