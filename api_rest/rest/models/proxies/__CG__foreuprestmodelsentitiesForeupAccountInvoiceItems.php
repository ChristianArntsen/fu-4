<?php

namespace DoctrineProxies\__CG__\foreup\rest\models\entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ForeupAccountInvoiceItems extends \foreup\rest\models\entities\ForeupAccountInvoiceItems implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'id', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'lineNumber', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'itemId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'itemType', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'unitPrice', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'discountPercent', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'quantity', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'subtotal', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'tax', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'total', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'taxable', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'invoice'];
        }

        return ['__isInitialized__', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'id', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'lineNumber', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'itemId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'itemType', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'unitPrice', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'discountPercent', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'quantity', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'subtotal', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'tax', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'total', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'taxable', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupAccountInvoiceItems' . "\0" . 'invoice'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ForeupAccountInvoiceItems $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setLineNumber($lineNumber)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLineNumber', [$lineNumber]);

        return parent::setLineNumber($lineNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function getLineNumber()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLineNumber', []);

        return parent::getLineNumber();
    }

    /**
     * {@inheritDoc}
     */
    public function setItemId($itemId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setItemId', [$itemId]);

        return parent::setItemId($itemId);
    }

    /**
     * {@inheritDoc}
     */
    public function getItemId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItemId', []);

        return parent::getItemId();
    }

    /**
     * {@inheritDoc}
     */
    public function setItemType($itemType)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setItemType', [$itemType]);

        return parent::setItemType($itemType);
    }

    /**
     * {@inheritDoc}
     */
    public function getItemType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getItemType', []);

        return parent::getItemType();
    }

    /**
     * {@inheritDoc}
     */
    public function setUnitPrice($unitPrice)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUnitPrice', [$unitPrice]);

        return parent::setUnitPrice($unitPrice);
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitPrice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUnitPrice', []);

        return parent::getUnitPrice();
    }

    /**
     * {@inheritDoc}
     */
    public function setDiscountPercent($discountPercent)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDiscountPercent', [$discountPercent]);

        return parent::setDiscountPercent($discountPercent);
    }

    /**
     * {@inheritDoc}
     */
    public function getDiscountPercent()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDiscountPercent', []);

        return parent::getDiscountPercent();
    }

    /**
     * {@inheritDoc}
     */
    public function setQuantity($quantity)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setQuantity', [$quantity]);

        return parent::setQuantity($quantity);
    }

    /**
     * {@inheritDoc}
     */
    public function getQuantity()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getQuantity', []);

        return parent::getQuantity();
    }

    /**
     * {@inheritDoc}
     */
    public function setSubtotal($subtotal)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSubtotal', [$subtotal]);

        return parent::setSubtotal($subtotal);
    }

    /**
     * {@inheritDoc}
     */
    public function getSubtotal()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSubtotal', []);

        return parent::getSubtotal();
    }

    /**
     * {@inheritDoc}
     */
    public function setTax($tax)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTax', [$tax]);

        return parent::setTax($tax);
    }

    /**
     * {@inheritDoc}
     */
    public function getTax()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTax', []);

        return parent::getTax();
    }

    /**
     * {@inheritDoc}
     */
    public function setTotal($total)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTotal', [$total]);

        return parent::setTotal($total);
    }

    /**
     * {@inheritDoc}
     */
    public function getTotal()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTotal', []);

        return parent::getTotal();
    }

    /**
     * {@inheritDoc}
     */
    public function setTaxable($taxable)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTaxable', [$taxable]);

        return parent::setTaxable($taxable);
    }

    /**
     * {@inheritDoc}
     */
    public function getTaxable()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTaxable', []);

        return parent::getTaxable();
    }

    /**
     * {@inheritDoc}
     */
    public function setInvoice(\foreup\rest\models\entities\ForeupAccountInvoices $invoice = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setInvoice', [$invoice]);

        return parent::setInvoice($invoice);
    }

    /**
     * {@inheritDoc}
     */
    public function getInvoice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getInvoice', []);

        return parent::getInvoice();
    }

}
