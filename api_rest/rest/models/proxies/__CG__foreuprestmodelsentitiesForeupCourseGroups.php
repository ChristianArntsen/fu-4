<?php

namespace DoctrineProxies\__CG__\foreup\rest\models\entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ForeupCourseGroups extends \foreup\rest\models\entities\ForeupCourseGroups implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'groupId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'label', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'type', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'sharedTeeSheet', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'sharedCustomers', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'sharedGiftcards', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'sharedPriceClasses', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'aggregateBooking', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'course'];
        }

        return ['__isInitialized__', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'groupId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'label', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'type', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'sharedTeeSheet', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'sharedCustomers', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'sharedGiftcards', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'sharedPriceClasses', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'aggregateBooking', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupCourseGroups' . "\0" . 'course'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ForeupCourseGroups $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function hasPermission($permission)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasPermission', [$permission]);

        return parent::hasPermission($permission);
    }

    /**
     * {@inheritDoc}
     */
    public function getGroupId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getGroupId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getGroupId', []);

        return parent::getGroupId();
    }

    /**
     * {@inheritDoc}
     */
    public function setLabel($label)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLabel', [$label]);

        return parent::setLabel($label);
    }

    /**
     * {@inheritDoc}
     */
    public function getLabel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLabel', []);

        return parent::getLabel();
    }

    /**
     * {@inheritDoc}
     */
    public function setType($type)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setType', [$type]);

        return parent::setType($type);
    }

    /**
     * {@inheritDoc}
     */
    public function getType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getType', []);

        return parent::getType();
    }

    /**
     * {@inheritDoc}
     */
    public function setSharedTeeSheet($sharedTeeSheet)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSharedTeeSheet', [$sharedTeeSheet]);

        return parent::setSharedTeeSheet($sharedTeeSheet);
    }

    /**
     * {@inheritDoc}
     */
    public function getSharedTeeSheet()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSharedTeeSheet', []);

        return parent::getSharedTeeSheet();
    }

    /**
     * {@inheritDoc}
     */
    public function setSharedCustomers($sharedCustomers)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSharedCustomers', [$sharedCustomers]);

        return parent::setSharedCustomers($sharedCustomers);
    }

    /**
     * {@inheritDoc}
     */
    public function getSharedCustomers()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSharedCustomers', []);

        return parent::getSharedCustomers();
    }

    /**
     * {@inheritDoc}
     */
    public function setSharedGiftcards($sharedGiftcards)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSharedGiftcards', [$sharedGiftcards]);

        return parent::setSharedGiftcards($sharedGiftcards);
    }

    /**
     * {@inheritDoc}
     */
    public function getSharedGiftcards()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSharedGiftcards', []);

        return parent::getSharedGiftcards();
    }

    /**
     * {@inheritDoc}
     */
    public function setSharedPriceClasses($sharedPriceClasses)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSharedPriceClasses', [$sharedPriceClasses]);

        return parent::setSharedPriceClasses($sharedPriceClasses);
    }

    /**
     * {@inheritDoc}
     */
    public function getSharedPriceClasses()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSharedPriceClasses', []);

        return parent::getSharedPriceClasses();
    }

    /**
     * {@inheritDoc}
     */
    public function setAggregateBooking($aggregateBooking)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAggregateBooking', [$aggregateBooking]);

        return parent::setAggregateBooking($aggregateBooking);
    }

    /**
     * {@inheritDoc}
     */
    public function getAggregateBooking()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAggregateBooking', []);

        return parent::getAggregateBooking();
    }

    /**
     * {@inheritDoc}
     */
    public function addCourse(\foreup\rest\models\entities\ForeupCourses $course)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addCourse', [$course]);

        return parent::addCourse($course);
    }

    /**
     * {@inheritDoc}
     */
    public function removeCourse(\foreup\rest\models\entities\ForeupCourses $course)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeCourse', [$course]);

        return parent::removeCourse($course);
    }

    /**
     * {@inheritDoc}
     */
    public function getCourses()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCourses', []);

        return parent::getCourses();
    }

}
