<?php

namespace DoctrineProxies\__CG__\foreup\rest\models\entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ForeupBookingClasses extends \foreup\rest\models\entities\ForeupBookingClasses implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingClassId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'active', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'name', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'priceClass', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'onlineBookingProtected', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'requireCreditCard', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'onlineOpenTime', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'onlineCloseTime', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'daysInBookingWindow', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'minimumPlayers', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'limitHoles', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingCarts', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'deleted', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'useCustomerPricing', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'isAggregate', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'payOnline', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'hideOnlinePrices', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'onlineRateSetting', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'showFullDetails', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'passItemIds', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingFeeItemId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingFeeTerms', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingFeeEnabled', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingFeePerPerson', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'allowNameEntry', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'teesheet'];
        }

        return ['__isInitialized__', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingClassId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'active', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'name', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'priceClass', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'onlineBookingProtected', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'requireCreditCard', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'onlineOpenTime', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'onlineCloseTime', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'daysInBookingWindow', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'minimumPlayers', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'limitHoles', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingCarts', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'deleted', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'useCustomerPricing', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'isAggregate', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'payOnline', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'hideOnlinePrices', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'onlineRateSetting', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'showFullDetails', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'passItemIds', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingFeeItemId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingFeeTerms', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingFeeEnabled', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'bookingFeePerPerson', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'allowNameEntry', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupBookingClasses' . "\0" . 'teesheet'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ForeupBookingClasses $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getBookingClassId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getBookingClassId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBookingClassId', []);

        return parent::getBookingClassId();
    }

    /**
     * {@inheritDoc}
     */
    public function setActive($active)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setActive', [$active]);

        return parent::setActive($active);
    }

    /**
     * {@inheritDoc}
     */
    public function getActive()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getActive', []);

        return parent::getActive();
    }

    /**
     * {@inheritDoc}
     */
    public function setName($name)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setName', [$name]);

        return parent::setName($name);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getName', []);

        return parent::getName();
    }

    /**
     * {@inheritDoc}
     */
    public function setPriceClass($priceClass)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPriceClass', [$priceClass]);

        return parent::setPriceClass($priceClass);
    }

    /**
     * {@inheritDoc}
     */
    public function getPriceClass()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPriceClass', []);

        return parent::getPriceClass();
    }

    /**
     * {@inheritDoc}
     */
    public function setOnlineBookingProtected($onlineBookingProtected)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOnlineBookingProtected', [$onlineBookingProtected]);

        return parent::setOnlineBookingProtected($onlineBookingProtected);
    }

    /**
     * {@inheritDoc}
     */
    public function getOnlineBookingProtected()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOnlineBookingProtected', []);

        return parent::getOnlineBookingProtected();
    }

    /**
     * {@inheritDoc}
     */
    public function setRequireCreditCard($requireCreditCard)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRequireCreditCard', [$requireCreditCard]);

        return parent::setRequireCreditCard($requireCreditCard);
    }

    /**
     * {@inheritDoc}
     */
    public function getRequireCreditCard()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRequireCreditCard', []);

        return parent::getRequireCreditCard();
    }

    /**
     * {@inheritDoc}
     */
    public function setOnlineOpenTime($onlineOpenTime)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOnlineOpenTime', [$onlineOpenTime]);

        return parent::setOnlineOpenTime($onlineOpenTime);
    }

    /**
     * {@inheritDoc}
     */
    public function getOnlineOpenTime()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOnlineOpenTime', []);

        return parent::getOnlineOpenTime();
    }

    /**
     * {@inheritDoc}
     */
    public function setOnlineCloseTime($onlineCloseTime)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOnlineCloseTime', [$onlineCloseTime]);

        return parent::setOnlineCloseTime($onlineCloseTime);
    }

    /**
     * {@inheritDoc}
     */
    public function getOnlineCloseTime()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOnlineCloseTime', []);

        return parent::getOnlineCloseTime();
    }

    /**
     * {@inheritDoc}
     */
    public function setDaysInBookingWindow($daysInBookingWindow)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDaysInBookingWindow', [$daysInBookingWindow]);

        return parent::setDaysInBookingWindow($daysInBookingWindow);
    }

    /**
     * {@inheritDoc}
     */
    public function getDaysInBookingWindow()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDaysInBookingWindow', []);

        return parent::getDaysInBookingWindow();
    }

    /**
     * {@inheritDoc}
     */
    public function setMinimumPlayers($minimumPlayers)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMinimumPlayers', [$minimumPlayers]);

        return parent::setMinimumPlayers($minimumPlayers);
    }

    /**
     * {@inheritDoc}
     */
    public function getMinimumPlayers()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMinimumPlayers', []);

        return parent::getMinimumPlayers();
    }

    /**
     * {@inheritDoc}
     */
    public function setLimitHoles($limitHoles)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLimitHoles', [$limitHoles]);

        return parent::setLimitHoles($limitHoles);
    }

    /**
     * {@inheritDoc}
     */
    public function getLimitHoles()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLimitHoles', []);

        return parent::getLimitHoles();
    }

    /**
     * {@inheritDoc}
     */
    public function setBookingCarts($bookingCarts)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setBookingCarts', [$bookingCarts]);

        return parent::setBookingCarts($bookingCarts);
    }

    /**
     * {@inheritDoc}
     */
    public function getBookingCarts()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBookingCarts', []);

        return parent::getBookingCarts();
    }

    /**
     * {@inheritDoc}
     */
    public function setDeleted($deleted)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDeleted', [$deleted]);

        return parent::setDeleted($deleted);
    }

    /**
     * {@inheritDoc}
     */
    public function getDeleted()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDeleted', []);

        return parent::getDeleted();
    }

    /**
     * {@inheritDoc}
     */
    public function setUseCustomerPricing($useCustomerPricing)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUseCustomerPricing', [$useCustomerPricing]);

        return parent::setUseCustomerPricing($useCustomerPricing);
    }

    /**
     * {@inheritDoc}
     */
    public function getUseCustomerPricing()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUseCustomerPricing', []);

        return parent::getUseCustomerPricing();
    }

    /**
     * {@inheritDoc}
     */
    public function setIsAggregate($isAggregate)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIsAggregate', [$isAggregate]);

        return parent::setIsAggregate($isAggregate);
    }

    /**
     * {@inheritDoc}
     */
    public function getIsAggregate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIsAggregate', []);

        return parent::getIsAggregate();
    }

    /**
     * {@inheritDoc}
     */
    public function setPayOnline($payOnline)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPayOnline', [$payOnline]);

        return parent::setPayOnline($payOnline);
    }

    /**
     * {@inheritDoc}
     */
    public function getPayOnline()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPayOnline', []);

        return parent::getPayOnline();
    }

    /**
     * {@inheritDoc}
     */
    public function setHideOnlinePrices($hideOnlinePrices)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setHideOnlinePrices', [$hideOnlinePrices]);

        return parent::setHideOnlinePrices($hideOnlinePrices);
    }

    /**
     * {@inheritDoc}
     */
    public function getHideOnlinePrices()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHideOnlinePrices', []);

        return parent::getHideOnlinePrices();
    }

    /**
     * {@inheritDoc}
     */
    public function setOnlineRateSetting($onlineRateSetting)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOnlineRateSetting', [$onlineRateSetting]);

        return parent::setOnlineRateSetting($onlineRateSetting);
    }

    /**
     * {@inheritDoc}
     */
    public function getOnlineRateSetting()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOnlineRateSetting', []);

        return parent::getOnlineRateSetting();
    }

    /**
     * {@inheritDoc}
     */
    public function setShowFullDetails($showFullDetails)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setShowFullDetails', [$showFullDetails]);

        return parent::setShowFullDetails($showFullDetails);
    }

    /**
     * {@inheritDoc}
     */
    public function getShowFullDetails()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getShowFullDetails', []);

        return parent::getShowFullDetails();
    }

    /**
     * {@inheritDoc}
     */
    public function setPassItemIds($passItemIds)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPassItemIds', [$passItemIds]);

        return parent::setPassItemIds($passItemIds);
    }

    /**
     * {@inheritDoc}
     */
    public function getPassItemIds()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPassItemIds', []);

        return parent::getPassItemIds();
    }

    /**
     * {@inheritDoc}
     */
    public function setBookingFeeItemId($bookingFeeItemId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setBookingFeeItemId', [$bookingFeeItemId]);

        return parent::setBookingFeeItemId($bookingFeeItemId);
    }

    /**
     * {@inheritDoc}
     */
    public function getBookingFeeItemId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBookingFeeItemId', []);

        return parent::getBookingFeeItemId();
    }

    /**
     * {@inheritDoc}
     */
    public function setBookingFeeTerms($bookingFeeTerms)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setBookingFeeTerms', [$bookingFeeTerms]);

        return parent::setBookingFeeTerms($bookingFeeTerms);
    }

    /**
     * {@inheritDoc}
     */
    public function getBookingFeeTerms()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBookingFeeTerms', []);

        return parent::getBookingFeeTerms();
    }

    /**
     * {@inheritDoc}
     */
    public function setBookingFeeEnabled($bookingFeeEnabled)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setBookingFeeEnabled', [$bookingFeeEnabled]);

        return parent::setBookingFeeEnabled($bookingFeeEnabled);
    }

    /**
     * {@inheritDoc}
     */
    public function getBookingFeeEnabled()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBookingFeeEnabled', []);

        return parent::getBookingFeeEnabled();
    }

    /**
     * {@inheritDoc}
     */
    public function setBookingFeePerPerson($bookingFeePerPerson)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setBookingFeePerPerson', [$bookingFeePerPerson]);

        return parent::setBookingFeePerPerson($bookingFeePerPerson);
    }

    /**
     * {@inheritDoc}
     */
    public function getBookingFeePerPerson()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBookingFeePerPerson', []);

        return parent::getBookingFeePerPerson();
    }

    /**
     * {@inheritDoc}
     */
    public function setAllowNameEntry($allowNameEntry)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAllowNameEntry', [$allowNameEntry]);

        return parent::setAllowNameEntry($allowNameEntry);
    }

    /**
     * {@inheritDoc}
     */
    public function getAllowNameEntry()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAllowNameEntry', []);

        return parent::getAllowNameEntry();
    }

    /**
     * {@inheritDoc}
     */
    public function setTeesheet(\foreup\rest\models\entities\ForeupTeesheet $teesheet = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTeesheet', [$teesheet]);

        return parent::setTeesheet($teesheet);
    }

    /**
     * {@inheritDoc}
     */
    public function getTeesheet()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTeesheet', []);

        return parent::getTeesheet();
    }

}
