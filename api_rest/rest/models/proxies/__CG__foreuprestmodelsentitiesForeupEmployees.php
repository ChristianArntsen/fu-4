<?php

namespace DoctrineProxies\__CG__\foreup\rest\models\entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ForeupEmployees extends \foreup\rest\models\entities\ForeupEmployees implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'courseId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'teesheetId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'cid', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'tsid', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'userLevel', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'position', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'username', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'password', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'pin', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'card', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'imageId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'activated', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'deleted', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'lastLogin', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'zendesk', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'deskMultipass', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'deskSignature', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'person', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'course', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'permissions'];
        }

        return ['__isInitialized__', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'courseId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'teesheetId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'cid', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'tsid', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'userLevel', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'position', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'username', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'password', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'pin', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'card', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'imageId', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'activated', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'deleted', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'lastLogin', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'zendesk', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'deskMultipass', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'deskSignature', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'person', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'course', '' . "\0" . 'foreup\\rest\\models\\entities\\ForeupEmployees' . "\0" . 'permissions'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ForeupEmployees $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getPermissions()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPermissions', []);

        return parent::getPermissions();
    }

    /**
     * {@inheritDoc}
     */
    public function addPermissions(\foreup\rest\models\entities\ForeupPermissions $permission)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addPermissions', [$permission]);

        return parent::addPermissions($permission);
    }

    /**
     * {@inheritDoc}
     */
    public function setCourseId($courseId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCourseId', [$courseId]);

        return parent::setCourseId($courseId);
    }

    /**
     * {@inheritDoc}
     */
    public function getCourseId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCourseId', []);

        return parent::getCourseId();
    }

    /**
     * {@inheritDoc}
     */
    public function getCourse(): \foreup\rest\models\entities\ForeupCourses
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCourse', []);

        return parent::getCourse();
    }

    /**
     * {@inheritDoc}
     */
    public function setCourse(\foreup\rest\models\entities\ForeupCourses $course)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCourse', [$course]);

        return parent::setCourse($course);
    }

    /**
     * {@inheritDoc}
     */
    public function setTeesheetId($teesheetId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTeesheetId', [$teesheetId]);

        return parent::setTeesheetId($teesheetId);
    }

    /**
     * {@inheritDoc}
     */
    public function getTeesheetId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTeesheetId', []);

        return parent::getTeesheetId();
    }

    /**
     * {@inheritDoc}
     */
    public function setCid($cid)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCid', [$cid]);

        return parent::setCid($cid);
    }

    /**
     * {@inheritDoc}
     */
    public function getCid()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCid', []);

        return parent::getCid();
    }

    /**
     * {@inheritDoc}
     */
    public function setTsid($tsid)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTsid', [$tsid]);

        return parent::setTsid($tsid);
    }

    /**
     * {@inheritDoc}
     */
    public function getTsid()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTsid', []);

        return parent::getTsid();
    }

    /**
     * {@inheritDoc}
     */
    public function setUserLevel($userLevel)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUserLevel', [$userLevel]);

        return parent::setUserLevel($userLevel);
    }

    /**
     * {@inheritDoc}
     */
    public function getUserLevel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUserLevel', []);

        return parent::getUserLevel();
    }

    /**
     * {@inheritDoc}
     */
    public function setPosition($position)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPosition', [$position]);

        return parent::setPosition($position);
    }

    /**
     * {@inheritDoc}
     */
    public function getPosition()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPosition', []);

        return parent::getPosition();
    }

    /**
     * {@inheritDoc}
     */
    public function setUsername($username)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUsername', [$username]);

        return parent::setUsername($username);
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUsername', []);

        return parent::getUsername();
    }

    /**
     * {@inheritDoc}
     */
    public function setPassword($password)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPassword', [$password]);

        return parent::setPassword($password);
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPassword', []);

        return parent::getPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function setPin($pin)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPin', [$pin]);

        return parent::setPin($pin);
    }

    /**
     * {@inheritDoc}
     */
    public function getPin()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPin', []);

        return parent::getPin();
    }

    /**
     * {@inheritDoc}
     */
    public function setCard($card)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCard', [$card]);

        return parent::setCard($card);
    }

    /**
     * {@inheritDoc}
     */
    public function getCard()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCard', []);

        return parent::getCard();
    }

    /**
     * {@inheritDoc}
     */
    public function setImageId($imageId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setImageId', [$imageId]);

        return parent::setImageId($imageId);
    }

    /**
     * {@inheritDoc}
     */
    public function getImageId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getImageId', []);

        return parent::getImageId();
    }

    /**
     * {@inheritDoc}
     */
    public function setActivated($activated)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setActivated', [$activated]);

        return parent::setActivated($activated);
    }

    /**
     * {@inheritDoc}
     */
    public function getActivated()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getActivated', []);

        return parent::getActivated();
    }

    /**
     * {@inheritDoc}
     */
    public function setDeleted($deleted)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDeleted', [$deleted]);

        return parent::setDeleted($deleted);
    }

    /**
     * {@inheritDoc}
     */
    public function getDeleted()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDeleted', []);

        return parent::getDeleted();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateCreated(\DateTime $lastLogin)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateCreated', [$lastLogin]);

        return parent::setDateCreated($lastLogin);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateCreated()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateCreated', []);

        return parent::getDateCreated();
    }

    /**
     * {@inheritDoc}
     */
    public function setZendesk($zendesk)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setZendesk', [$zendesk]);

        return parent::setZendesk($zendesk);
    }

    /**
     * {@inheritDoc}
     */
    public function getZendesk()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getZendesk', []);

        return parent::getZendesk();
    }

    /**
     * {@inheritDoc}
     */
    public function setDeskMultipass($deskMultipass)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDeskMultipass', [$deskMultipass]);

        return parent::setDeskMultipass($deskMultipass);
    }

    /**
     * {@inheritDoc}
     */
    public function getDeskMultipass()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDeskMultipass', []);

        return parent::getDeskMultipass();
    }

    /**
     * {@inheritDoc}
     */
    public function setDeskSignature($deskSignature)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDeskSignature', [$deskSignature]);

        return parent::setDeskSignature($deskSignature);
    }

    /**
     * {@inheritDoc}
     */
    public function getDeskSignature()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDeskSignature', []);

        return parent::getDeskSignature();
    }

    /**
     * {@inheritDoc}
     */
    public function setPerson(\foreup\rest\models\entities\ForeupPeople $person = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPerson', [$person]);

        return parent::setPerson($person);
    }

    /**
     * {@inheritDoc}
     */
    public function getPerson()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPerson', []);

        return parent::getPerson();
    }

}
