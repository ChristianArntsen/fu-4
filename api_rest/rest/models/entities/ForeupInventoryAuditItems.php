<?php

namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupInventoryAuditItems
 *
 * @ORM\Table(name="foreup_inventory_audit_items", indexes={@ORM\Index(name="inventory_audit_id", columns={"inventory_audit_id"})})
 * @ORM\Entity
 */
class ForeupInventoryAuditItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \foreup\rest\models\entities\ForeupItems
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="item_id", referencedColumnName="item_id", nullable=true)
     * })
     */
    private $item;

    /**
     * @var integer
     *
     * @ORM\Column(name="current_count", type="smallint", nullable=false)
     */
    private $currentCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="manual_count", type="smallint", nullable=false)
     */
    private $manualCount;

    /**
     * @var \foreup\rest\models\entities\ForeupInventoryAudits
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupInventoryAudits")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inventory_audit_id", referencedColumnName="inventory_audit_id")
     * })
     */
    private $inventoryAudit;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \foreup\rest\models\entities\ForeupItems
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param \foreup\rest\models\entities\ForeupItems $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return int
     */
    public function getCurrentCount()
    {
        return $this->currentCount;
    }

    /**
     * @param int $currentCount
     */
    public function setCurrentCount($currentCount)
    {
        $this->currentCount = $currentCount;
    }

    /**
     * @return int
     */
    public function getManualCount()
    {
        return $this->manualCount;
    }

    /**
     * @param int $manualCount
     */
    public function setManualCount($manualCount)
    {
        $this->manualCount = $manualCount;
    }

    /**
     * @return \foreup\rest\models\entities\ForeupInventoryAudits
     */
    public function getInventoryAudit()
    {
        return $this->inventoryAudit;
    }

    /**
     * @param \foreup\rest\models\entities\ForeupInventoryAudits $inventoryAudit
     */
    public function setInventoryAudit($inventoryAudit)
    {
        $this->inventoryAudit = $inventoryAudit;
    }


}

