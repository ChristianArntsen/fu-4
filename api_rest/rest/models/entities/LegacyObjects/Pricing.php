<?php
namespace foreup\rest\models\entities\LegacyObjects;
use Carbon\Carbon;

class Pricing
{
	private $hole18Price;
	private $hole18Cart;
	private $hole9Price;
	private $hole9Cart;
	private $cartFeeTaxIncluded;
	private $greenFeeTaxIncluded;
	private $hole18Tax;
	private $cart18Tax;
	private $hole9Tax;
	private $cart9Tax;


	public function __construct($apiResponse)
	{
		$this->setHole9Cart($apiResponse->{"9_hole_price"});
		$this->setHole9Price($apiResponse->{"9_cart_price"});
		$this->setHole18Price($apiResponse->{"18_hole_price"});
		$this->setHole18Cart($apiResponse->{"18_cart_price"});

		$this->setCartFeeTaxIncluded($apiResponse->{"cart_fee_tax_included"});
		$this->setGreenFeeTaxIncluded($apiResponse->{"green_fee_tax_included"});
		$this->setHole18Tax($apiResponse->{"18_hole_tax"});
		$this->setCart18Tax($apiResponse->{"18_cart_tax"});
		$this->setHole9Tax($apiResponse->{"9_hole_tax"});
		$this->setCart9Tax($apiResponse->{"9_cart_tax"});
	}

	/**
	 * @return mixed
	 */
	public function getCartFeeTaxIncluded()
	{
		return $this->cartFeeTaxIncluded;
	}

	/**
	 * @param mixed $cartFeeTaxIncluded
	 */
	public function setCartFeeTaxIncluded($cartFeeTaxIncluded)
	{
		$this->cartFeeTaxIncluded = $cartFeeTaxIncluded;
	}

	/**
	 * @return mixed
	 */
	public function getGreenFeeTaxIncluded()
	{
		return $this->greenFeeTaxIncluded;
	}

	/**
	 * @param mixed $greenFeeTaxIncluded
	 */
	public function setGreenFeeTaxIncluded($greenFeeTaxIncluded)
	{
		$this->greenFeeTaxIncluded = $greenFeeTaxIncluded;
	}

	/**
	 * @return mixed
	 */
	public function getHole18Tax()
	{
		return $this->hole18Tax;
	}

	/**
	 * @param mixed $hole18Tax
	 */
	public function setHole18Tax($hole18Tax)
	{
		$this->hole18Tax = $hole18Tax;
	}

	/**
	 * @return mixed
	 */
	public function getCart18Tax()
	{
		return $this->cart18Tax;
	}

	/**
	 * @param mixed $cart18Tax
	 */
	public function setCart18Tax($cart18Tax)
	{
		$this->cart18Tax = $cart18Tax;
	}

	/**
	 * @return mixed
	 */
	public function getHole9Tax()
	{
		return $this->hole9Tax;
	}

	/**
	 * @param mixed $hole9Tax
	 */
	public function setHole9Tax($hole9Tax)
	{
		$this->hole9Tax = $hole9Tax;
	}

	/**
	 * @return mixed
	 */
	public function getCart9Tax()
	{
		return $this->cart9Tax;
	}

	/**
	 * @param mixed $cart9Tax
	 */
	public function setCart9Tax($cart9Tax)
	{
		$this->cart9Tax = $cart9Tax;
	}

	/**
	 * @return mixed
	 */
	public function getHole18Price()
	{
		return $this->hole18Price;
	}

	/**
	 * @param mixed $hole18Price
	 */
	public function setHole18Price($hole18Price)
	{
		$this->hole18Price = (double)$hole18Price;
	}

	/**
	 * @return mixed
	 */
	public function getHole18Cart()
	{
		return $this->hole18Cart;
	}

	/**
	 * @param mixed $hole18Cart
	 */
	public function setHole18Cart($hole18Cart)
	{
		$this->hole18Cart = (double)$hole18Cart;
	}

	/**
	 * @return mixed
	 */
	public function getHole9Price()
	{
		return $this->hole9Price;
	}

	/**
	 * @param mixed $hole9Price
	 */
	public function setHole9Price($hole9Price)
	{
		$this->hole9Price = (double)$hole9Price;
	}

	/**
	 * @return mixed
	 */
	public function getHole9Cart()
	{
		return $this->hole9Cart;
	}

	/**
	 * @param mixed $hole9Cart
	 */
	public function setHole9Cart($hole9Cart)
	{
		$this->hole9Cart = (double)$hole9Cart;
	}


}