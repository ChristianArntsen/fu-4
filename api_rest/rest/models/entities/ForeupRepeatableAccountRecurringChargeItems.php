<?php
namespace foreup\rest\models\entities;
use Doctrine\ORM\Mapping as ORM;
use \foreup\rest\models\entities\ForeupAccountRecurringChargeItems;
use \foreup\rest\models\entities\ForeupRepeatable;

/**
 * Class ForeupRepeatable
 * @package foreup\rest\models\entities
 * @ORM\Entity
 */
class ForeupRepeatableAccountRecurringChargeItems extends ForeupRepeatable
{
	protected $type = 'recurring_charge_item';

	/**
	 * @var ForeupAccountRecurringChargeItems
	 *
	 * @ORM\ManyToOne(targetEntity="ForeupAccountRecurringChargeItems")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="resource_id", referencedColumnName="id")
	 * })
	 */
	private $item;

	/**
	 * @return ForeupAccountRecurringChargeItems
	 */
	public function getItem()
	{
		return $this->item;
	}

	/**
	 * @return ForeupAccountRecurringChargeItems
	 */
	public function getResource()
	{
		return $this->getItem();
	}

	/**
	 * @param ForeupAccountRecurringChargeItems $item
	 * @return ForeupRepeatableAccountRecurringChargeItems
	 */
	public function setItem($item)
	{
		$this->item = $item;
		$type = 'recurring_charge_item';
		$resource_id = $item->getId();
		$this->setType($type);
		$this->setResourceId($resource_id);
		return $this;
	}

	/**
	 * @param ForeupAccountRecurringChargeItems $item
	 * @return ForeupRepeatableAccountRecurringChargeItems
	 */
	public function setResource($item){
		return $this->setItem(($item));
	}
}
?>