<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupGroups
 *
 * @ORM\Table(name="foreup_groups")
 * @ORM\Entity
 * ORM\HasLifecycleCallbacks
 */
class ForeupGroups
{
	private $last_error;

	public function getLastError(){
		return $this->last_error;
	}

	public function resetLastError(){
		$this->last_error = null;
	}

	private function invalid($throw = true){
		if($throw)throw new \InvalidArgumentException($this->last_error);
		else return $this->last_error;
	}

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{
		$course = $this->course;
		if(!is_a($course, '\foreup\rest\models\entities\ForeupCourses')){
			$this->last_error = 'ForeupGroups->validate Error: course not found';
			return $this->invalid($throw);
		}

		$label = $this->label;
		if(!is_string($label)){
			$this->last_error = 'ForeupGroups->validate Error: label must be a string';
			return $this->invalid($throw);
		}

		$cid = $this->cid;
		if(!is_numeric($cid)||!is_int($cid*1)){
			$this->last_error = 'ForeupGroups->validate Error: label must be a string';
			return $this->invalid($throw);
		}
		return true;
	}
    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $groupId;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var integer
     *
     * @ORM\Column(name="CID", type="integer", nullable=false)
     */
    private $cid;

	/**
	 * @var \foreup\rest\models\entities\ForeupCourses
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $course;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->groupId;
	}

	/**
	 * Get label
	 *
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * Set label
	 *
	 * @param string $label
	 *
	 * @return ForeupGroups
	 */
	public function setItemType($label)
	{
		$this->label = $label;

		return $this;
	}

	/**
	 * Get cid
	 *
	 * @return integer
	 */
	public function getCid()
	{
		return $this->cid;
	}

	/**
	 * Set cid
	 *
	 * @param integer $cid
	 *
	 * @return ForeupGroups
	 */
	public function setCid($cid)
	{
		$this->cid = $cid;

		return $this;
	}

	/**
	 * Get course
	 *
	 * @return \foreup\rest\models\entities\ForeupCourses
	 */
	public function getCourse()
	{
		return $this->course;
	}

	/**
	 * Set course
	 *
	 * @param \foreup\rest\models\entities\ForeupCourses $course
	 *
	 * @return ForeupGroups
	 */
	public function setCourse(\foreup\rest\models\entities\ForeupCourses $course = null)
	{
		$this->course = $course;

		return $this;
	}
}

