<?php
namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupEmployees
 *
 * @ORM\Table(name="foreup_employees", uniqueConstraints={@ORM\UniqueConstraint(name="username", columns={"username"}), @ORM\UniqueConstraint(name="pin", columns={"course_id", "pin"}), @ORM\UniqueConstraint(name="card", columns={"course_id", "card"})}, indexes={@ORM\Index(name="person_id", columns={"person_id"}), @ORM\Index(name="deleted", columns={"deleted"})})
 * @ORM\Entity (repositoryClass="foreup\rest\models\repositories\EmployeesRepository")
 */
class ForeupEmployees
{
    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;


    /**
     * @var integer
     *
     * @ORM\Column(name="teesheet_id", type="integer", nullable=false)
     */
    private $teesheetId = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="CID", type="integer", nullable=false)
     */
    private $cid = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="TSID", type="string", length=20, nullable=false)
     */
    private $tsid = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_level", type="integer", nullable=false)
     */
    private $userLevel = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255, nullable=false)
     */
    private $position = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="pin", type="integer", nullable=true)
     */
    private $pin;

    /**
     * @var string
     *
     * @ORM\Column(name="card", type="string", length=255, nullable=true)
     */
    private $card;

    /**
     * @var integer
     *
     * @ORM\Column(name="image_id", type="integer", nullable=false)
     */
    private $imageId = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="activated", type="integer", nullable=false)
     */
    private $activated = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted = '0';

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="last_login", type="datetime", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $lastLogin;


    /**
     * @var boolean
     *
     * @ORM\Column(name="zendesk", type="boolean", nullable=false)
     */
    private $zendesk = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="desk_multipass", type="string", length=255, nullable=true)
     */
    private $deskMultipass;

    /**
     * @var string
     *
     * @ORM\Column(name="desk_signature", type="string", length=255, nullable=true)
     */
    private $deskSignature;

    /**
     * @var \foreup\rest\models\entities\ForeupPeople
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
     * })
     */
    private $person;

	/**
	 * @var \foreup\rest\models\entities\ForeupCourses
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $course;

	/**
	 * @var \foreup\rest\models\entities\ForeupPermissions $permissions
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupPermissions", mappedBy="person")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
	 * })
	 */
	private $permissions;

	/**
	 * ForeupEmployees constructor.
	 */
	public function __construct()
	{
		$this->permissions = new \Doctrine\Common\Collections\ArrayCollection();
	}


	/**
	 * @return ForeupPermissions
	 */
	public function getPermissions()
	{
		return $this->permissions;
	}

	public function addPermissions(ForeupPermissions $permission)
	{
		$this->permissions[] = $permission;
	}

	/**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupEmployees
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;

        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

	/**
	 * @return ForeupCourses
	 */
	public function getCourse(): ForeupCourses
	{
		return $this->course;
	}

	/**
	 * @param ForeupCourses $course
	 */
	public function setCourse(ForeupCourses $course)
	{
		$this->course = $course;
	}

    /**
     * Set teesheetId
     *
     * @param integer $teesheetId
     *
     * @return ForeupEmployees
     */
    public function setTeesheetId($teesheetId)
    {
        $this->teesheetId = $teesheetId;

        return $this;
    }

    /**
     * Get teesheetId
     *
     * @return integer
     */
    public function getTeesheetId()
    {
        return $this->teesheetId;
    }

    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return ForeupEmployees
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set tsid
     *
     * @param string $tsid
     *
     * @return ForeupEmployees
     */
    public function setTsid($tsid)
    {
        $this->tsid = $tsid;

        return $this;
    }

    /**
     * Get tsid
     *
     * @return string
     */
    public function getTsid()
    {
        return $this->tsid;
    }

    /**
     * Set userLevel
     *
     * @param boolean $userLevel
     *
     * @return ForeupEmployees
     */
    public function setUserLevel($userLevel)
    {
        $this->userLevel = $userLevel;

        return $this;
    }

    /**
     * Get userLevel
     *
     * @return boolean
     */
    public function getUserLevel()
    {
        return $this->userLevel;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return ForeupEmployees
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return ForeupEmployees
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return ForeupEmployees
     */
    public function setPassword($password)
    {
        $this->password = md5($password);

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set pin
     *
     * @param integer $pin
     *
     * @return ForeupEmployees
     */
    public function setPin($pin)
    {
        $this->pin = $pin;

        return $this;
    }

    /**
     * Get pin
     *
     * @return integer
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * Set card
     *
     * @param string $card
     *
     * @return ForeupEmployees
     */
    public function setCard($card)
    {
        $this->card = $card;

        return $this;
    }

    /**
     * Get card
     *
     * @return string
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * Set imageId
     *
     * @param integer $imageId
     *
     * @return ForeupEmployees
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;

        return $this;
    }

    /**
     * Get imageId
     *
     * @return integer
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * Set activated
     *
     * @param integer $activated
     *
     * @return ForeupEmployees
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;

        return $this;
    }

    /**
     * Get activated
     *
     * @return integer
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     *
     * @return ForeupEmployees
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

	/**
	 * Set lastLogin
	 *
	 * @param \DateTime $lastLogin
	 *
	 * @return ForeupEmployees
	 */
	public function setDateCreated(\DateTime $lastLogin)
	{
		$this->lastLogin = $lastLogin;

		return $this;
	}

	/**
	 * Get lastLogin
	 *
	 * @return \DateTime
	 */
	public function getDateCreated()
	{
		return $this->lastLogin;
	}

    /**
     * Set zendesk
     *
     * @param boolean $zendesk
     *
     * @return ForeupEmployees
     */
    public function setZendesk($zendesk)
    {
        $this->zendesk = $zendesk;

        return $this;
    }

    /**
     * Get zendesk
     *
     * @return boolean
     */
    public function getZendesk()
    {
        return $this->zendesk;
    }

    /**
     * Set deskMultipass
     *
     * @param string $deskMultipass
     *
     * @return ForeupEmployees
     */
    public function setDeskMultipass($deskMultipass)
    {
        $this->deskMultipass = $deskMultipass;

        return $this;
    }

    /**
     * Get deskMultipass
     *
     * @return string
     */
    public function getDeskMultipass()
    {
        return $this->deskMultipass;
    }

    /**
     * Set deskSignature
     *
     * @param string $deskSignature
     *
     * @return ForeupEmployees
     */
    public function setDeskSignature($deskSignature)
    {
        $this->deskSignature = $deskSignature;

        return $this;
    }

    /**
     * Get deskSignature
     *
     * @return string
     */
    public function getDeskSignature()
    {
        return $this->deskSignature;
    }

    /**
     * Set person
     *
     * @param \foreup\rest\models\entities\ForeupPeople $person
     *
     * @return ForeupEmployees
     */
    public function setPerson(\foreup\rest\models\entities\ForeupPeople $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \foreup\rest\models\entities\ForeupPeople
     */
    public function getPerson()
    {
        return $this->person;
    }
}
