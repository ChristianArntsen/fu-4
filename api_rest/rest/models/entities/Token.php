<?php

namespace foreup\rest\models\entities;

class Token
{
    public function __construct()
    {
        $this->id = 0;
    }

    private $token,$id;


    public function getToken()
    {
        return $this->token;
    }
    public function getId()
    {
        return $this->id;
    }

    public function setToken($token)
    {
        $this->id = $token;
    }


}

