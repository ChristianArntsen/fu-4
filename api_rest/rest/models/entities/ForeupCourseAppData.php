<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupCourseAppData
 *
 * @ORM\Table(name="foreup_course_app_data")
 * @ORM\Entity
 */
class ForeupCourseAppData
{
    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $courseId;

    /**
     * @var string
     *
     * @ORM\Column(name="course_latitude", type="decimal", precision=10, scale=6, nullable=false)
     */
    private $courseLatitude;

    /**
     * @var string
     *
     * @ORM\Column(name="course_longitude", type="decimal", precision=10, scale=6, nullable=false)
     */
    private $courseLongitude;

    /**
     * @var float
     *
     * @ORM\Column(name="red", type="float", precision=10, scale=0, nullable=false)
     */
    private $red;

    /**
     * @var float
     *
     * @ORM\Column(name="green", type="float", precision=10, scale=0, nullable=false)
     */
    private $green;

    /**
     * @var float
     *
     * @ORM\Column(name="blue", type="float", precision=10, scale=0, nullable=false)
     */
    private $blue;

    /**
     * @var float
     *
     * @ORM\Column(name="alpha", type="float", precision=10, scale=0, nullable=false)
     */
    private $alpha;

    /**
     * @var integer
     *
     * @ORM\Column(name="home_view", type="integer", nullable=false)
     */
    private $homeView;

    /**
     * @var integer
     *
     * @ORM\Column(name="gps_view", type="integer", nullable=false)
     */
    private $gpsView;

    /**
     * @var integer
     *
     * @ORM\Column(name="scorecard_view", type="integer", nullable=false)
     */
    private $scorecardView;

    /**
     * @var integer
     *
     * @ORM\Column(name="teetime_view", type="integer", nullable=false)
     */
    private $teetimeView;

    /**
     * @var integer
     *
     * @ORM\Column(name="food_and_beverage_view", type="integer", nullable=false)
     */
    private $foodAndBeverageView;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_updated", type="date", nullable=false)
     */
    private $lastUpdated;


    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set courseLatitude
     *
     * @param string $courseLatitude
     *
     * @return ForeupCourseAppData
     */
    public function setCourseLatitude($courseLatitude)
    {
        $this->courseLatitude = $courseLatitude;

        return $this;
    }

    /**
     * Get courseLatitude
     *
     * @return string
     */
    public function getCourseLatitude()
    {
        return $this->courseLatitude;
    }

    /**
     * Set courseLongitude
     *
     * @param string $courseLongitude
     *
     * @return ForeupCourseAppData
     */
    public function setCourseLongitude($courseLongitude)
    {
        $this->courseLongitude = $courseLongitude;

        return $this;
    }

    /**
     * Get courseLongitude
     *
     * @return string
     */
    public function getCourseLongitude()
    {
        return $this->courseLongitude;
    }

    /**
     * Set red
     *
     * @param float $red
     *
     * @return ForeupCourseAppData
     */
    public function setRed($red)
    {
        $this->red = $red;

        return $this;
    }

    /**
     * Get red
     *
     * @return float
     */
    public function getRed()
    {
        return $this->red;
    }

    /**
     * Set green
     *
     * @param float $green
     *
     * @return ForeupCourseAppData
     */
    public function setGreen($green)
    {
        $this->green = $green;

        return $this;
    }

    /**
     * Get green
     *
     * @return float
     */
    public function getGreen()
    {
        return $this->green;
    }

    /**
     * Set blue
     *
     * @param float $blue
     *
     * @return ForeupCourseAppData
     */
    public function setBlue($blue)
    {
        $this->blue = $blue;

        return $this;
    }

    /**
     * Get blue
     *
     * @return float
     */
    public function getBlue()
    {
        return $this->blue;
    }

    /**
     * Set alpha
     *
     * @param float $alpha
     *
     * @return ForeupCourseAppData
     */
    public function setAlpha($alpha)
    {
        $this->alpha = $alpha;

        return $this;
    }

    /**
     * Get alpha
     *
     * @return float
     */
    public function getAlpha()
    {
        return $this->alpha;
    }

    /**
     * Set homeView
     *
     * @param integer $homeView
     *
     * @return ForeupCourseAppData
     */
    public function setHomeView($homeView)
    {
        $this->homeView = $homeView;

        return $this;
    }

    /**
     * Get homeView
     *
     * @return integer
     */
    public function getHomeView()
    {
        return $this->homeView;
    }

    /**
     * Set gpsView
     *
     * @param integer $gpsView
     *
     * @return ForeupCourseAppData
     */
    public function setGpsView($gpsView)
    {
        $this->gpsView = $gpsView;

        return $this;
    }

    /**
     * Get gpsView
     *
     * @return integer
     */
    public function getGpsView()
    {
        return $this->gpsView;
    }

    /**
     * Set scorecardView
     *
     * @param integer $scorecardView
     *
     * @return ForeupCourseAppData
     */
    public function setScorecardView($scorecardView)
    {
        $this->scorecardView = $scorecardView;

        return $this;
    }

    /**
     * Get scorecardView
     *
     * @return integer
     */
    public function getScorecardView()
    {
        return $this->scorecardView;
    }

    /**
     * Set teetimeView
     *
     * @param integer $teetimeView
     *
     * @return ForeupCourseAppData
     */
    public function setTeetimeView($teetimeView)
    {
        $this->teetimeView = $teetimeView;

        return $this;
    }

    /**
     * Get teetimeView
     *
     * @return integer
     */
    public function getTeetimeView()
    {
        return $this->teetimeView;
    }

    /**
     * Set foodAndBeverageView
     *
     * @param integer $foodAndBeverageView
     *
     * @return ForeupCourseAppData
     */
    public function setFoodAndBeverageView($foodAndBeverageView)
    {
        $this->foodAndBeverageView = $foodAndBeverageView;

        return $this;
    }

    /**
     * Get foodAndBeverageView
     *
     * @return integer
     */
    public function getFoodAndBeverageView()
    {
        return $this->foodAndBeverageView;
    }

    /**
     * Set lastUpdated
     *
     * @param \DateTime $lastUpdated
     *
     * @return ForeupCourseAppData
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * Get lastUpdated
     *
     * @return \DateTime
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }
}

