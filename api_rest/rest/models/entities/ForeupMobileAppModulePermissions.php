<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupMobileAppModulePermissions
 *
 * @ORM\Table(name="foreup_mobile_app_module_permissions", indexes={@ORM\Index(name="course_id", columns={"course_id"})})
 * @ORM\Entity
 */
class ForeupMobileAppModulePermissions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mobile_app_module_permission_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $mobileAppModulePermissionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="mobile_app_module_id", type="integer", nullable=false)
     */
    private $mobileAppModuleId;


    /**
     * Get mobileAppModulePermissionId
     *
     * @return integer
     */
    public function getMobileAppModulePermissionId()
    {
        return $this->mobileAppModulePermissionId;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupMobileAppModulePermissions
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;

        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set mobileAppModuleId
     *
     * @param integer $mobileAppModuleId
     *
     * @return ForeupMobileAppModulePermissions
     */
    public function setMobileAppModuleId($mobileAppModuleId)
    {
        $this->mobileAppModuleId = $mobileAppModuleId;

        return $this;
    }

    /**
     * Get mobileAppModuleId
     *
     * @return integer
     */
    public function getMobileAppModuleId()
    {
        return $this->mobileAppModuleId;
    }
}
