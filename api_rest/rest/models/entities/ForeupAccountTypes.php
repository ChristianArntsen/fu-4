<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountTypes
 *
 * @ORM\Table(name="foreup_account_types", indexes={@ORM\Index(name="organization_id", columns={"organization_id"})})
 * @ORM\Entity
 */
class ForeupAccountTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="`limit`", type="decimal", precision=15, scale=2, nullable=true, unique=false)
     */
    private $limit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_negative", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $allowNegative;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=1024, precision=0, scale=0, nullable=true, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $organizationId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set limit
     *
     * @param string $limit
     *
     * @return ForeupAccountTypes
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    
        return $this;
    }

    /**
     * Get limit
     *
     * @return string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set allowNegative
     *
     * @param boolean $allowNegative
     *
     * @return ForeupAccountTypes
     */
    public function setAllowNegative($allowNegative)
    {
        $this->allowNegative = $allowNegative;
    
        return $this;
    }

    /**
     * Get allowNegative
     *
     * @return boolean
     */
    public function getAllowNegative()
    {
        return $this->allowNegative;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupAccountTypes
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ForeupAccountTypes
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     *
     * @return ForeupAccountTypes
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }
}

