<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupPosCartItems
 *
 * @ORM\Table(name="foreup_pos_cart_items", indexes={@ORM\Index(name="line", columns={"line"}), @ORM\Index(name="FK_foreup_pos_cart_items_foreup_items", columns={"item_id"}), @ORM\Index(name="IDX_3BADBA9A1AD5CDBF", columns={"cart_id"})})
 * @ORM\Entity
 */
class ForeupPosCartItems
{

    /**
     * @var integer
     *
     * @ORM\Column(name="line", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $line;

    /**
     * @var string
     *
     * @ORM\Column(name="quantity", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_price", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $unitPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="discount_percent", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $discountPercent;

    /**
     * @var string
     *
     * @ORM\Column(name="item_type", type="string", length=16, nullable=false)
     */
    private $itemType;

    /**
     * @var string
     *
     * @ORM\Column(name="item_number", type="string", length=255, nullable=true)
     */
    private $itemNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="price_class_id", type="integer", nullable=true)
     */
    private $priceClassId;

    /**
     * @var integer
     *
     * @ORM\Column(name="timeframe_id", type="integer", nullable=true)
     */
    private $timeframeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="special_id", type="integer", nullable=true)
     */
    private $specialId;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="string", length=2048, nullable=true)
     */
    private $params;

    /**
     * @var integer
     *
     * @ORM\Column(name="punch_card_id", type="integer", nullable=true)
     */
    private $punchCardId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tournament_id", type="integer", nullable=false)
     */
    private $tournamentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_id", type="integer", nullable=true)
     */
    private $invoiceId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="selected", type="boolean", nullable=false)
     */
    private $selected = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="unit_price_includes_tax", type="boolean", nullable=false)
     */
    private $unitPriceIncludesTax = false;

    /**
     * @var \foreup\rest\models\entities\ForeupItems
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="item_id", referencedColumnName="item_id")
     * })
     */
    private $item;

    /**
     * @var \foreup\rest\models\entities\ForeupPosCart
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupPosCart")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cart_id", referencedColumnName="cart_id")
     * })
     */
    private $cart;

	public function __construct(ForeupPosCart $cart, $data = [])
	{

		$this->setCart($cart);
		if(!empty($data)){
			$this->setUnitPrice($data->item_unit_price);
			$this->setQuantity($data->quantity);
			$this->setDiscountPercent($data->discount_percent);
			$this->setLine($data->line);
			$this->setUnitPriceIncludesTax($data->unit_price_includes_tax);
			$this->setItemType($data->item_type);
		}
	}

	/**
	 * @return int
	 */
	public function getLine()
	{
		return $this->line;
	}

	/**
	 * @param int $line
	 */
	public function setLine($line)
	{
		$this->line = $line;
	}

	/**
	 * @return string
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * @param string $quantity
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	}

	/**
	 * @return string
	 */
	public function getUnitPrice()
	{
		return $this->unitPrice;
	}

	/**
	 * @param string $unitPrice
	 */
	public function setUnitPrice($unitPrice)
	{
		$this->unitPrice = $unitPrice;
	}

	/**
	 * @return string
	 */
	public function getDiscountPercent()
	{
		return $this->discountPercent;
	}

	/**
	 * @param string $discountPercent
	 */
	public function setDiscountPercent($discountPercent)
	{
		$this->discountPercent = $discountPercent;
	}

	/**
	 * @return string
	 */
	public function getItemType()
	{
		return $this->itemType;
	}

	/**
	 * @param string $itemType
	 */
	public function setItemType($itemType)
	{
		$this->itemType = $itemType;
	}

	/**
	 * @return string
	 */
	public function getItemNumber()
	{
		return $this->itemNumber;
	}

	/**
	 * @param string $itemNumber
	 */
	public function setItemNumber($itemNumber)
	{
		$this->itemNumber = $itemNumber;
	}

	/**
	 * @return int
	 */
	public function getPriceClassId()
	{
		return $this->priceClassId;
	}

	/**
	 * @param int $priceClassId
	 */
	public function setPriceClassId($priceClassId)
	{
		$this->priceClassId = $priceClassId;
	}

	/**
	 * @return int
	 */
	public function getTimeframeId()
	{
		return $this->timeframeId;
	}

	/**
	 * @param int $timeframeId
	 */
	public function setTimeframeId($timeframeId)
	{
		$this->timeframeId = $timeframeId;
	}

	/**
	 * @return int
	 */
	public function getSpecialId()
	{
		return $this->specialId;
	}

	/**
	 * @param int $specialId
	 */
	public function setSpecialId($specialId)
	{
		$this->specialId = $specialId;
	}

	/**
	 * @return string
	 */
	public function getParams()
	{
		return $this->params;
	}

	/**
	 * @param string $params
	 */
	public function setParams($params)
	{
		$this->params = $params;
	}

	/**
	 * @return int
	 */
	public function getPunchCardId()
	{
		return $this->punchCardId;
	}

	/**
	 * @param int $punchCardId
	 */
	public function setPunchCardId($punchCardId)
	{
		$this->punchCardId = $punchCardId;
	}

	/**
	 * @return int
	 */
	public function getTournamentId()
	{
		return $this->tournamentId;
	}

	/**
	 * @param int $tournamentId
	 */
	public function setTournamentId($tournamentId)
	{
		$this->tournamentId = $tournamentId;
	}

	/**
	 * @return int
	 */
	public function getInvoiceId()
	{
		return $this->invoiceId;
	}

	/**
	 * @param int $invoiceId
	 */
	public function setInvoiceId($invoiceId)
	{
		$this->invoiceId = $invoiceId;
	}

	/**
	 * @return bool
	 */
	public function isSelected()
	{
		return $this->selected;
	}

	/**
	 * @param bool $selected
	 */
	public function setSelected($selected)
	{
		$this->selected = $selected;
	}

	/**
	 * @return bool
	 */
	public function isUnitPriceIncludesTax()
	{
		return $this->unitPriceIncludesTax;
	}

	/**
	 * @param bool $unitPriceIncludesTax
	 */
	public function setUnitPriceIncludesTax($unitPriceIncludesTax)
	{
		$this->unitPriceIncludesTax = (bool) $unitPriceIncludesTax;
	}

	/**
	 * @return ForeupItems
	 */
	public function getItem()
	{
		return $this->item;
	}

	/**
	 * @param ForeupItems $item
	 */
	public function setItem($item)
	{
		$this->item = $item;
	}

	/**
	 * @return ForeupPosCart
	 */
	public function getCart()
	{
		return $this->cart;
	}

	/**
	 * @param ForeupPosCart $cart
	 */
	public function setCart($cart)
	{
		$this->cart = $cart;
	}


}

