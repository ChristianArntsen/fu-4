<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupSeasonPriceClasses
 *
 * @ORM\Table(name="foreup_season_price_classes")
 * @ORM\Entity
 */
class ForeupSeasonPriceClasses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="season_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $seasonId;

    /**
     * @var integer
     *
     * @ORM\Column(name="class_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $classId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dateCreated;


    /**
     * Set seasonId
     *
     * @param integer $seasonId
     *
     * @return ForeupSeasonPriceClasses
     */
    public function setSeasonId($seasonId)
    {
        $this->seasonId = $seasonId;

        return $this;
    }

    /**
     * Get seasonId
     *
     * @return integer
     */
    public function getSeasonId()
    {
        return $this->seasonId;
    }

    /**
     * Set classId
     *
     * @param integer $classId
     *
     * @return ForeupSeasonPriceClasses
     */
    public function setClassId($classId)
    {
        $this->classId = $classId;

        return $this;
    }

    /**
     * Get classId
     *
     * @return integer
     */
    public function getClassId()
    {
        return $this->classId;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupSeasonPriceClasses
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }
}

