<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountPaymentTerms
 *
 * @ORM\Table(name="foreup_account_payment_terms", indexes={@ORM\Index(name="organization_id", columns={"organization_id"})})
 * @ORM\Entity
 */
class ForeupAccountPaymentTerms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=1024, precision=0, scale=0, nullable=true, unique=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="past_due_days", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pastDueDays;

    /**
     * @var string
     *
     * @ORM\Column(name="past_due_fee_percentage", type="decimal", precision=15, scale=2, nullable=true, unique=false)
     */
    private $pastDueFeePercentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $organizationId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateCreated;

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="string", length=4096, precision=0, scale=0, nullable=true, unique=false)
     */
    private $memo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupAccountPaymentTerms
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set pastDueDays
     *
     * @param integer $pastDueDays
     *
     * @return ForeupAccountPaymentTerms
     */
    public function setPastDueDays($pastDueDays)
    {
        $this->pastDueDays = $pastDueDays;
    
        return $this;
    }

    /**
     * Get pastDueDays
     *
     * @return integer
     */
    public function getPastDueDays()
    {
        return $this->pastDueDays;
    }

    /**
     * Set pastDueFeePercentage
     *
     * @param string $pastDueFeePercentage
     *
     * @return ForeupAccountPaymentTerms
     */
    public function setPastDueFeePercentage($pastDueFeePercentage)
    {
        $this->pastDueFeePercentage = $pastDueFeePercentage;
    
        return $this;
    }

    /**
     * Get pastDueFeePercentage
     *
     * @return string
     */
    public function getPastDueFeePercentage()
    {
        return $this->pastDueFeePercentage;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     *
     * @return ForeupAccountPaymentTerms
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupAccountPaymentTerms
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set memo
     *
     * @param string $memo
     *
     * @return ForeupAccountPaymentTerms
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
    
        return $this;
    }

    /**
     * Get memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }
}

