<?php

namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupItems
 *
 * @ORM\Table(name="foreup_items", uniqueConstraints={@ORM\UniqueConstraint(name="item_number2", columns={"course_id", "item_number"})}, indexes={@ORM\Index(name="foreup_items_ibfk_1", columns={"supplier_id"}), @ORM\Index(name="name", columns={"name"}), @ORM\Index(name="category", columns={"category"}), @ORM\Index(name="deleted", columns={"deleted"}), @ORM\Index(name="course_id", columns={"course_id"}), @ORM\Index(name="receipt_content_id", columns={"receipt_content_id"})})
 * @ORM\Entity
 */
class ForeupItems
{

	public function __construct()
	{
		$this->taxes = new \Doctrine\Common\Collections\ArrayCollection();
		$this->isGiftcard = false;
		$this->isPass= false;
	}

	use \foreup\rest\models\entities\EntityValidator;
    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $itemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CID", type="integer", nullable=false)
     */
    private $cid = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="department", type="string", length=255, nullable=false)
     */
    private $department;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=255, nullable=false)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="subcategory", type="string", length=255, nullable=false)
     */
    private $subcategory;

    /**
     * @var string
     *
     * @ORM\Column(name="item_number", type="string", length=255, nullable=true)
     */
    private $itemNumber = null;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="image_id", type="integer", nullable=false)
     */
    private $imageId = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="cost_price", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $costPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_price", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $unitPrice;

    /**
     * @var boolean
     *
     * @ORM\Column(name="unit_price_includes_tax", type="boolean", nullable=false)
     */
    private $unitPriceIncludesTax;

    /**
     * @var double
     *
     * @ORM\Column(name="max_discount", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $maxDiscount = 100;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $quantity = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_unlimited", type="boolean", nullable=false)
     */
    private $isUnlimited;

    /**
     * @var integer
     *
     * @ORM\Column(name="reorder_level", type="integer", nullable=false)
     */
    private $reorderLevel = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=false)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="gl_code", type="string", length=20, nullable=true)
     */
    private $glCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_alt_description", type="boolean", nullable=false)
     */
    private $allowAltDescription = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_serialized", type="boolean", nullable=false)
     */
    private $isSerialized = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_giftcard", type="boolean", nullable=false)
     */
    private $isGiftcard = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="invisible", type="boolean", nullable=false)
     */
    private $invisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="food_and_beverage", type="boolean", nullable=false)
     */
    private $foodAndBeverage = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="soup_or_salad", type="string", nullable=false)
     */
    private $soupOrSalad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="number_of_sides", type="boolean", nullable=false)
     */
    private $numberOfSides = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_side", type="boolean", nullable=false)
     */
    private $isSide;

    /**
     * @var float
     *
     * @ORM\Column(name="add_on_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $addOnPrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="print_priority", type="integer", nullable=false)
     */
    private $printPriority = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="kitchen_printer", type="boolean", nullable=false)
     */
    private $kitchenPrinter = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="quickbooks_income", type="string", length=1024, nullable=false)
     */
    private $quickbooksIncome= "";

    /**
     * @var string
     *
     * @ORM\Column(name="quickbooks_cogs", type="string", length=1024, nullable=false)
     */
    private $quickbooksCogs = "";

    /**
     * @var string
     *
     * @ORM\Column(name="quickbooks_assets", type="string", length=1024, nullable=false)
     */
    private $quickbooksAssets= "";

    /**
     * @var boolean
     *
     * @ORM\Column(name="inactive", type="boolean", nullable=false)
     */
    private $inactive = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="erange_size", type="boolean", nullable=false)
     */
    private $erangeSize = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_fee", type="boolean", nullable=false)
     */
    private $isFee = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="meal_course_id", type="integer", nullable=false)
     */
    private $mealCourseId = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="do_not_print", type="boolean", nullable=false)
     */
    private $doNotPrint = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prompt_meal_course", type="boolean", nullable=false)
     */
    private $promptMealCourse = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="do_not_print_customer_receipt", type="boolean", nullable=false)
     */
    private $doNotPrintCustomerReceipt = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_pass", type="boolean", nullable=false)
     */
    private $isPass = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="receipt_content_id", type="integer", nullable=true)
     */
    private $receiptContentId;

	/**
	 * @var \foreup\rest\models\entities\ForeupSuppliers
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupSuppliers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="supplier_id", referencedColumnName="person_id")
	 * })
	 */

	//private $supplier;

	/**
	 * @var \foreup\rest\models\entities\ForeupItemsTaxes
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupItemsTaxes",mappedBy="item")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="item_id", referencedColumnName="item_id")
	 * })
	 */
	private $taxes;

	public function setItemId($id)
	{
		$this->itemId = $id;
		return $this;
	}


    /**
     * Get itemId
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupItems
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;

        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return ForeupItems
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupItems
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set department
     *
     * @param string $department
     *
     * @return ForeupItems
     */
    public function setDepartment($department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return ForeupItems
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set subcategory
     *
     * @param string $subcategory
     *
     * @return ForeupItems
     */
    public function setSubcategory($subcategory)
    {
        $this->subcategory = $subcategory;

        return $this;
    }

    /**
     * Get subcategory
     *
     * @return string
     */
    public function getSubcategory()
    {
        return $this->subcategory;
    }

    /**
     * Set itemNumber
     *
     * @param string $itemNumber
     *
     * @return ForeupItems
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;

        return $this;
    }

    /**
     * Get itemNumber
     *
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ForeupItems
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set imageId
     *
     * @param integer $imageId
     *
     * @return ForeupItems
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;

        return $this;
    }

    /**
     * Get imageId
     *
     * @return integer
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * Set costPrice
     *
     * @param string $costPrice
     *
     * @return ForeupItems
     */
    public function setCostPrice($costPrice)
    {
        $this->costPrice = $costPrice;

        return $this;
    }

    /**
     * Get costPrice
     *
     * @return string
     */
    public function getCostPrice()
    {
        return $this->costPrice;
    }

    /**
     * Set unitPrice
     *
     * @param string $unitPrice
     *
     * @return ForeupItems
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    /**
     * Get unitPrice
     *
     * @return string
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Set unitPriceIncludesTax
     *
     * @param boolean $unitPriceIncludesTax
     *
     * @return ForeupItems
     */
    public function setUnitPriceIncludesTax($unitPriceIncludesTax)
    {
        $this->unitPriceIncludesTax = $unitPriceIncludesTax;

        return $this;
    }

    /**
     * Get unitPriceIncludesTax
     *
     * @return boolean
     */
    public function getUnitPriceIncludesTax()
    {
        return $this->unitPriceIncludesTax;
    }

    /**
     * Set maxDiscount
     *
     * @param string $maxDiscount
     *
     * @return ForeupItems
     */
    public function setMaxDiscount($maxDiscount)
    {
        $this->maxDiscount = $maxDiscount;

        return $this;
    }

    /**
     * Get maxDiscount
     *
     * @return string
     */
    public function getMaxDiscount()
    {
        return $this->maxDiscount;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return ForeupItems
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set isUnlimited
     *
     * @param boolean $isUnlimited
     *
     * @return ForeupItems
     */
    public function setIsUnlimited($isUnlimited)
    {
        $this->isUnlimited = $isUnlimited;

        return $this;
    }

    /**
     * Get isUnlimited
     *
     * @return boolean
     */
    public function getIsUnlimited()
    {
        return $this->isUnlimited;
    }

    /**
     * Set reorderLevel
     *
     * @param integer $reorderLevel
     *
     * @return ForeupItems
     */
    public function setReorderLevel($reorderLevel)
    {
        $this->reorderLevel = $reorderLevel;

        return $this;
    }

    /**
     * Get reorderLevel
     *
     * @return integer
     */
    public function getReorderLevel()
    {
        return $this->reorderLevel;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return ForeupItems
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set glCode
     *
     * @param string $glCode
     *
     * @return ForeupItems
     */
    public function setGlCode($glCode)
    {
        $this->glCode = $glCode;

        return $this;
    }

    /**
     * Get glCode
     *
     * @return string
     */
    public function getGlCode()
    {
        return $this->glCode;
    }

    /**
     * Set allowAltDescription
     *
     * @param boolean $allowAltDescription
     *
     * @return ForeupItems
     */
    public function setAllowAltDescription($allowAltDescription)
    {
        $this->allowAltDescription = $allowAltDescription;

        return $this;
    }

    /**
     * Get allowAltDescription
     *
     * @return boolean
     */
    public function getAllowAltDescription()
    {
        return $this->allowAltDescription;
    }

    /**
     * Set isSerialized
     *
     * @param boolean $isSerialized
     *
     * @return ForeupItems
     */
    public function setIsSerialized($isSerialized)
    {
        $this->isSerialized = $isSerialized;

        return $this;
    }

    /**
     * Get isSerialized
     *
     * @return boolean
     */
    public function getIsSerialized()
    {
        return $this->isSerialized;
    }

    /**
     * Set isGiftcard
     *
     * @param boolean $isGiftcard
     *
     * @return ForeupItems
     */
    public function setIsGiftcard($isGiftcard)
    {
        $this->isGiftcard = $isGiftcard;

        return $this;
    }

    /**
     * Get isGiftcard
     *
     * @return boolean
     */
    public function getIsGiftcard()
    {
        return $this->isGiftcard;
    }

    /**
     * Set invisible
     *
     * @param boolean $invisible
     *
     * @return ForeupItems
     */
    public function setInvisible($invisible)
    {
        $this->invisible = $invisible;

        return $this;
    }

    /**
     * Get invisible
     *
     * @return boolean
     */
    public function getInvisible()
    {
        return $this->invisible;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     *
     * @return ForeupItems
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set foodAndBeverage
     *
     * @param boolean $foodAndBeverage
     *
     * @return ForeupItems
     */
    public function setFoodAndBeverage($foodAndBeverage)
    {
        $this->foodAndBeverage = $foodAndBeverage;

        return $this;
    }

    /**
     * Get foodAndBeverage
     *
     * @return boolean
     */
    public function getFoodAndBeverage()
    {
        return $this->foodAndBeverage;
    }

    /**
     * Set soupOrSalad
     *
     * @param string $soupOrSalad
     *
     * @return ForeupItems
     */
    public function setSoupOrSalad($soupOrSalad)
    {
        $this->soupOrSalad = $soupOrSalad;

        return $this;
    }

    /**
     * Get soupOrSalad
     *
     * @return string
     */
    public function getSoupOrSalad()
    {
        return $this->soupOrSalad;
    }

    /**
     * Set numberOfSides
     *
     * @param boolean $numberOfSides
     *
     * @return ForeupItems
     */
    public function setNumberOfSides($numberOfSides)
    {
        $this->numberOfSides = $numberOfSides;

        return $this;
    }

    /**
     * Get numberOfSides
     *
     * @return boolean
     */
    public function getNumberOfSides()
    {
        return $this->numberOfSides;
    }

    /**
     * Set isSide
     *
     * @param boolean $isSide
     *
     * @return ForeupItems
     */
    public function setIsSide($isSide)
    {
        $this->isSide = $isSide;

        return $this;
    }

    /**
     * Get isSide
     *
     * @return boolean
     */
    public function getIsSide()
    {
        return $this->isSide;
    }

    /**
     * Set addOnPrice
     *
     * @param float $addOnPrice
     *
     * @return ForeupItems
     */
    public function setAddOnPrice($addOnPrice)
    {
        $this->addOnPrice = $addOnPrice;

        return $this;
    }

    /**
     * Get addOnPrice
     *
     * @return float
     */
    public function getAddOnPrice()
    {
        return $this->addOnPrice;
    }

    /**
     * Set printPriority
     *
     * @param boolean $printPriority
     *
     * @return ForeupItems
     */
    public function setPrintPriority($printPriority)
    {
        $this->printPriority = $printPriority;

        return $this;
    }

    /**
     * Get printPriority
     *
     * @return boolean
     */
    public function getPrintPriority()
    {
        return $this->printPriority;
    }

    /**
     * Set kitchenPrinter
     *
     * @param boolean $kitchenPrinter
     *
     * @return ForeupItems
     */
    public function setKitchenPrinter($kitchenPrinter)
    {
        $this->kitchenPrinter = $kitchenPrinter;

        return $this;
    }

    /**
     * Get kitchenPrinter
     *
     * @return boolean
     */
    public function getKitchenPrinter()
    {
        return $this->kitchenPrinter;
    }

    /**
     * Set quickbooksIncome
     *
     * @param string $quickbooksIncome
     *
     * @return ForeupItems
     */
    public function setQuickbooksIncome($quickbooksIncome)
    {
        $this->quickbooksIncome = $quickbooksIncome;

        return $this;
    }

    /**
     * Get quickbooksIncome
     *
     * @return string
     */
    public function getQuickbooksIncome()
    {
        return $this->quickbooksIncome;
    }

    /**
     * Set quickbooksCogs
     *
     * @param string $quickbooksCogs
     *
     * @return ForeupItems
     */
    public function setQuickbooksCogs($quickbooksCogs)
    {
        $this->quickbooksCogs = $quickbooksCogs;

        return $this;
    }

    /**
     * Get quickbooksCogs
     *
     * @return string
     */
    public function getQuickbooksCogs()
    {
        return $this->quickbooksCogs;
    }

    /**
     * Set quickbooksAssets
     *
     * @param string $quickbooksAssets
     *
     * @return ForeupItems
     */
    public function setQuickbooksAssets($quickbooksAssets)
    {
        $this->quickbooksAssets = $quickbooksAssets;

        return $this;
    }

    /**
     * Get quickbooksAssets
     *
     * @return string
     */
    public function getQuickbooksAssets()
    {
        return $this->quickbooksAssets;
    }

    /**
     * Set inactive
     *
     * @param boolean $inactive
     *
     * @return ForeupItems
     */
    public function setInactive($inactive)
    {
        $this->inactive = $inactive;

        return $this;
    }

    /**
     * Get inactive
     *
     * @return boolean
     */
    public function getInactive()
    {
        return $this->inactive;
    }

    /**
     * Set erangeSize
     *
     * @param boolean $erangeSize
     *
     * @return ForeupItems
     */
    public function setErangeSize($erangeSize)
    {
        $this->erangeSize = $erangeSize;

        return $this;
    }

    /**
     * Get erangeSize
     *
     * @return boolean
     */
    public function getErangeSize()
    {
        return $this->erangeSize;
    }

    /**
     * Set isFee
     *
     * @param boolean $isFee
     *
     * @return ForeupItems
     */
    public function setIsFee($isFee)
    {
        $this->isFee = $isFee;

        return $this;
    }

    /**
     * Get isFee
     *
     * @return boolean
     */
    public function getIsFee()
    {
        return $this->isFee;
    }

    /**
     * Set mealCourseId
     *
     * @param integer $mealCourseId
     *
     * @return ForeupItems
     */
    public function setMealCourseId($mealCourseId)
    {
        $this->mealCourseId = $mealCourseId;

        return $this;
    }

    /**
     * Get mealCourseId
     *
     * @return integer
     */
    public function getMealCourseId()
    {
        return $this->mealCourseId;
    }

    /**
     * Set doNotPrint
     *
     * @param boolean $doNotPrint
     *
     * @return ForeupItems
     */
    public function setDoNotPrint($doNotPrint)
    {
        $this->doNotPrint = $doNotPrint;

        return $this;
    }

    /**
     * Get doNotPrint
     *
     * @return boolean
     */
    public function getDoNotPrint()
    {
        return $this->doNotPrint;
    }

    /**
     * Set promptMealCourse
     *
     * @param boolean $promptMealCourse
     *
     * @return ForeupItems
     */
    public function setPromptMealCourse($promptMealCourse)
    {
        $this->promptMealCourse = $promptMealCourse;

        return $this;
    }

    /**
     * Get promptMealCourse
     *
     * @return boolean
     */
    public function getPromptMealCourse()
    {
        return $this->promptMealCourse;
    }

    /**
     * Set doNotPrintCustomerReceipt
     *
     * @param boolean $doNotPrintCustomerReceipt
     *
     * @return ForeupItems
     */
    public function setDoNotPrintCustomerReceipt($doNotPrintCustomerReceipt)
    {
        $this->doNotPrintCustomerReceipt = $doNotPrintCustomerReceipt;

        return $this;
    }

    /**
     * Get doNotPrintCustomerReceipt
     *
     * @return boolean
     */
    public function getDoNotPrintCustomerReceipt()
    {
        return $this->doNotPrintCustomerReceipt;
    }

    /**
     * Set isPass
     *
     * @param boolean $isPass
     *
     * @return ForeupItems
     */
    public function setIsPass($isPass)
    {
        $this->isPass = $isPass;

        return $this;
    }

    /**
     * Get isPass
     *
     * @return boolean
     */
    public function getIsPass()
    {
        return $this->isPass;
    }

    /**
     * Set receiptContentId
     *
     * @param integer $receiptContentId
     *
     * @return ForeupItems
     */
    public function setReceiptContentId($receiptContentId)
    {
        $this->receiptContentId = $receiptContentId;

        return $this;
    }

    /**
     * Get receiptContentId
     *
     * @return integer
     */
    public function getReceiptContentId()
    {
        return $this->receiptContentId;
    }

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection|ForeupItemsTaxes[]
	 */
	public function getTaxes ()
	{
		return $this->taxes;
	}



	/**
	 * @param ForeupItemsTaxes $tax
	 */
	public function addTaxes(\foreup\rest\models\entities\ForeupItemsTaxes $tax)
	{
		$tax->setItem($this);
		$this->taxes[] = $tax;

		return $this;
	}

}
