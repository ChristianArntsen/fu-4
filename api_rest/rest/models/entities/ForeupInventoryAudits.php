<?php
namespace foreup\rest\models\entities;



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupInventoryAudits
 *
 * @ORM\Table(name="foreup_inventory_audits", indexes={@ORM\Index(name="course_id", columns={"course_id"})})
 * @ORM\Entity
 */
class ForeupInventoryAudits
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inventory_audit_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inventoryAuditId;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="employee_id", type="integer", nullable=false)
     */
    private $employeeId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupInventoryAuditItems", mappedBy="inventoryAudit")
     */
    private $inventoryAuditItems;

    /**
     * @return int
     */
    public function getInventoryAuditId()
    {
        return $this->inventoryAuditId;
    }

    /**
     * @param int $inventoryAuditId
     */
    public function setInventoryAuditId($inventoryAuditId)
    {
        $this->inventoryAuditId = $inventoryAuditId;
    }

    /**
     * @return int
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * @param int $courseId
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    }

    /**
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * @param int $employeeId
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryAuditItems()
    {
        return $this->inventoryAuditItems;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $inventoryAuditItems
     */
    public function setInventoryAuditItems($inventoryAuditItems)
    {
        $this->inventoryAuditItems = $inventoryAuditItems;
    }

}

