<?php
namespace foreup\rest\models\entities;

if(\file_exists( realpath('./').'/application/libraries/fu/rrule/RRule.php'))
  include_once realpath('./').'/application/libraries/fu/rrule/RRule.php';
else
  include_once '../application/libraries/fu/rrule/RRule.php';

if(\file_exists( realpath('./').'/application/models/Repeatable/Row.php'))
	include_once realpath('./').'/application/models/Repeatable/Row.php';
else
  include_once '../application/models/Repeatable/Row.php';

use \models\Repeatable\Row as RepeatableObject;
use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupRepeatable
 *
 * @ORM\Table(name="foreup_repeatable", uniqueConstraints={@ORM\UniqueConstraint(name="type_resource_id", columns={"type", "resource_id"})})
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"recurring_charge_item" = "ForeupRepeatableAccountRecurringChargeItems", "marketing_campaign" = "ForeupRepeatableMarketingCampaigns","recurring_statement" = "ForeupRepeatableAccountRecurringStatements"})
 * @ORM\HasLifecycleCallbacks
 */
abstract class ForeupRepeatable
{
	protected $last_error;

    public static $frequencies = [
        0 => 'YEARLY',
        1 => 'MONTHLY',
        2 => 'WEEKLY',
        3 => 'DAILY',
        4 => 'HOURLY',
        5 => 'MINUTELY',
        6 => 'SECONDLY'
    ];

    public static $bydayEnum = array('MO','TU','WE','TH','FR','SA','SU');

	public function getLastError(){
		return $this->last_error;
	}

	public function resetLastError(){
		$this->last_error = null;
	}

	protected function invalid($throw = true){
		if($throw)throw new \InvalidArgumentException($this->last_error);
		else return $this->last_error;
	}

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true) {
		$type = $this->getType();
		$typeEnum = array('marketing_campaign','recurring_charge_item','recurring_statement');
		if(!in_array($type,$typeEnum)){
			$this->last_error = 'ForeupRepeatable->validate Error: type "'.$type.'" not found in enumerated values: '.json_encode($typeEnum);
			return $this->invalid($throw);
		}

		$resourceId = $this->getResourceId();
		if(!isset($resourceId)){
			$this->last_error = 'ForeupRepeatable->isActive Error: resourceId cannot be NULL';
			return $this->invalid($throw);
		}
		elseif(!is_numeric($resourceId) || !is_int($resourceId*1)){
			$this->last_error = 'ForeupRepeatable->validate Error: resourceId must be an integer: '.$resourceId;
			return $this->invalid($throw);
		}

		$lastRan = $this->getLastRan();
		if(is_string($lastRan) && strtotime($lastRan)) {
			$lastRan = new \DateTime($lastRan);
			$this->setLastRan($lastRan);
		}
		if(isset($lastRan) && !is_a($lastRan, '\DateTime')){
			$this->last_error = 'ForeupRepeatable->validate Error: invalid lastRan: '.$lastRan;
			return $this->invalid($throw);
		}

		$nextOccurence = $this->getNextOccurence();
		if(isset($nextOccurence) && !is_a($nextOccurence, '\DateTime')){
			$this->last_error = 'ForeupRepeatable->validate Error: invalid nextOccurence: '.$nextOccurence;
			return $this->invalid($throw);
		}

		$freq = $this->getFreq();
		$freqEnum = array('SECONDLY','MINUTELY','HOURLY','DAILY','WEEKLY','MONTHLY','YEARLY');
		if(isset($freq) && !in_array($freq,$freqEnum)){
			$this->last_error = 'ForeupRepeatable->validate Error: freq "'.$freq.'" not found in enumerated values: '.json_encode($freqEnum);
			return $this->invalid($throw);
		}

		$dstart = $this->getDtstart();
		if(isset($dstart) && $dstart !== '' && !is_a($dstart, '\DateTime')){
			$this->last_error = 'ForeupRepeatable->validate Error: invalid dtstart: '.$dstart;
			return $this->invalid($throw);
		}

		$until = $this->getUntil();
		if(isset($until) && $until !== '' && !is_a($until, '\DateTime')){
			$this->last_error = 'ForeupRepeatable->validate Error: invalid until: '.$until;
			return $this->invalid($throw);
		}

		$interval = $this->getInterval();
		if(isset($interval) && $interval !== '' && (!is_numeric($interval) || !is_int($interval*1))){
			$this->last_error = 'ForeupRepeatable->validate Error: interval must be an integer: '.$interval;
			return $this->invalid($throw);
		}

		$count = $this->getCount();
		if(isset($count) && $count !== '' && (!is_numeric($count) || !is_int($count*1))){
		    $this->last_error = 'ForeupRepeatable->validate Error: count must be an integer: '.$count;
			return $this->invalid($throw);
		}

		$bymonth = $this->getBymonth();
		if(is_array($bymonth)){
			foreach ($bymonth as $val) {
				if (!is_numeric($val) || !is_int($val * 1) || $val < 1 || $val > 12) {
					$this->last_error = 'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): ' . $val;
					return $this->invalid($throw);
				}
			}
		}else if($bymonth !== '' && $bymonth !== NULL){
            $this->last_error = 'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): ' . $bymonth;
            return $this->invalid($throw);
        }

		$byweekno = $this->getByweekno();
		if(is_array($byweekno)) {
			foreach ($byweekno as $val) {
				if (!is_numeric($val) || !is_int($val * 1) || $val < -53 || $val == 0 || $val > 53) {
					$this->last_error = 'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): ' . $val;
					return $this->invalid($throw);
				}
			}
		}else if($byweekno !== '' && $byweekno !== NULL){
            $this->last_error = 'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): ' . $byweekno;
            return $this->invalid($throw);
        }

		$byyearday = $this->getByyearday();
        if(is_array($byyearday)) {
			foreach ($byyearday as $val){
				if (!is_numeric($val) || !is_int($val * 1)  || $val < -366 || $val == 0 || $val > 366) {
					$this->last_error = 'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): ' . $val;
					return $this->invalid($throw);
				}
			}
		}else if($byyearday !== '' && $byyearday !== NULL){
            $this->last_error = 'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): ' . $byyearday;
            return $this->invalid($throw);
        }

        $bymonthday = $this->getBymonthday();
		if(is_array($bymonthday)) {
			foreach ($bymonthday as $val) {
				if (!is_numeric($val) || !is_int($val * 1) || $val < -31 || $val == 0 || $val > 31) {
					$this->last_error = 'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): ' . $val;
					return $this->invalid($throw);
				}
			}
		}else if($bymonthday !== '' && $bymonthday !== NULL){
            $this->last_error = 'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): ' . $bymonthday;
            return $this->invalid($throw);
        }

		$byday = $this->getByday();
		$bydayEnum = self::$bydayEnum;
		$bydayErr = 'ForeupRepeatable->validate Error: byday must be an array of integers (0..6, 0 being MO) or the following enumerated values: '.json_encode($bydayEnum);
        if(is_array($byday)) {
			foreach ($byday as $key=>$val) {
				if (!in_array($val,$bydayEnum) && !is_numeric($val)) {
					$this->last_error = $bydayErr.': '.$val;
					return $this->invalid($throw);
				}
				elseif(is_numeric($val) && (!is_int($val * 1) || ($val > 6 || $val < 0)) ){
					$this->last_error = $bydayErr.': '.$val;
					return $this->invalid($throw);
				}
			}
		}else if($byday !== '' && $byday !== NULL){
            $this->last_error = $bydayErr.': '.$byday;
            return $this->invalid($throw);
        }

		$wkst = $this->getWkst();
		$wkstErr = 'ForeupRepeatable->validate Error: byday must be a single integer (0..6, 0 being MO) or one of the following enumerated values: '.json_encode($bydayEnum);
		if(isset($wkst) && $wkst !== ''){
			if(is_string($wkst) && !in_array($wkst,$bydayEnum)){
				$this->last_error = $wkstErr;
				return $this->invalid($throw);
			}elseif (!is_string($wkst) && (!is_numeric($wkst) || !is_int($wkst*1) || $wkst < 1 || $wkst > 6)){
				$this->last_error = $wkstErr;

			}
		}

		$byhour = $this->getByhour();
		if(is_array($byhour)) {
			foreach ($byhour as $val) {
				if (!is_numeric($val) || !is_int($val * 1)) {
					$this->last_error = 'ForeupRepeatable->validate Error: byhour must be an array of integers: ' . $val;
					return $this->invalid($throw);
				}
			}
		}else if($byhour !== '' && $byhour !== NULL){
            $this->last_error = 'ForeupRepeatable->validate Error: byhour must be an array of integers: ' . $byhour;
            return $this->invalid($throw);
        }

		$byminute = $this->getByminute();
		if(is_array($byminute)) {
			foreach ($byminute as $val) {
				if (!is_numeric($val) || !is_int($val * 1)) {
					$this->last_error = 'ForeupRepeatable->validate Error: byminute must be an array of integers: ' . $val;
					return $this->invalid($throw);
				}
			}
		}else if($byminute !== '' && $byminute !== NULL){
            $this->last_error = 'ForeupRepeatable->validate Error: byminute must be an array of integers: ' . $byminute;
            return $this->invalid($throw);
        }

		$bysecond = $this->getBysecond();
		if(is_array($bysecond)) {
			foreach ($bysecond as $val) {
				if (!is_numeric($val) || !is_int($val * 1)) {
					$this->last_error = 'ForeupRepeatable->validate Error: bysecond must be an array of integers: ' . $val;
					return $this->invalid($throw);
				}
			}
		}else if($bysecond !== '' && $bysecond !== NULL){
            $this->last_error = 'ForeupRepeatable->validate Error: bysecond must be an array of integers: ' . $bysecond;
            return $this->invalid($throw);
        }

		$bysetpos = $this->getBysetpos();
		if(is_array($bysetpos)){
			foreach ($bysetpos as $val) {
				if (!is_numeric($val) || !is_int($val * 1)) {
					$this->last_error = 'ForeupRepeatable->validate Error: bysetpos must be an array of integers: ' . $val;
					return $this->invalid($throw);
				}
			}
		}else if($bysetpos !== '' && $bysetpos !== NULL){
            $this->last_error = 'ForeupRepeatable->validate Error: bysetpos must be an array of integers: ' . $bysetpos;
            return $this->invalid($throw);
        }

		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    protected $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="resource_id", type="integer", nullable=false)
     */
    protected $resourceId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_ran", type="datetime", nullable=true)
     */
    protected $lastRan = NULL;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="next_occurence", type="datetime", nullable=true)
     */
    protected $nextOccurence;

    /**
     * @var string
     *
     * @ORM\Column(name="FREQ", type="string", nullable=true)
     */
    protected $freq;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DTSTART", type="datetime", nullable=true)
     */
    protected $dtstart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UNTIL", type="datetime", nullable=true)
     */
    protected $until;

    /**
     * @var integer
     *
     * @ORM\Column(name="`INTERVAL`", type="integer", nullable=true)
     */
    protected $interval;

    /**
     * @var integer
     *
     * @ORM\Column(name="COUNT", type="integer", nullable=true)
     */
    protected $count;

    /**
     * @var simple_array
     *
     * @ORM\Column(name="BYMONTH", type="simple_array", nullable=true)
     */
    protected $bymonth;

    /**
     * @var simple_array
     *
     * @ORM\Column(name="BYWEEKNO", type="simple_array", nullable=true)
     */
    protected $byweekno;

    /**
     * @var simple_array
     *
     * @ORM\Column(name="BYYEARDAY", type="simple_array", nullable=true)
     */
    protected $byyearday;

    /**
     * @var simple_array
     *
     * @ORM\Column(name="BYMONTHDAY", type="simple_array", nullable=true)
     */
    protected $bymonthday;

    /**
     * @var simple_array
     *
     * @ORM\Column(name="BYDAY", type="simple_array", nullable=true)
     */
    protected $byday;

    /**
     * @var string
     *
     * @ORM\Column(name="WKST", type="string", nullable=true)
     */
    protected $wkst;

    /**
     * @var simple_array
     *
     * @ORM\Column(name="BYHOUR", type="simple_array", nullable=true)
     */
    protected $byhour;

    /**
     * @var simple_array
     *
     * @ORM\Column(name="BYMINUTE", type="simple_array", nullable=true)
     */
    protected $byminute;

    /**
     * @var simple_array
     *
     * @ORM\Column(name="BYSECOND", type="simple_array", nullable=true)
     */
    protected $bysecond;

    /**
     * @var simple_array
     *
     * @ORM\Column(name="BYSETPOS", type="simple_array", nullable=true)
     */
    protected $bysetpos;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


	/**
	 * @return ForeupAccountRecurringChargeItems|ForeupMarketingCampaigns||null
	 */
    public function getResource()
    {
    	$resource = null;
    	return $resource;
    }

	/**
	 * @param ForeupMarketingCampaigns|ForeupAccountRecurringChargeItems $resource
	 * @return ForeupRepeatable
	 */
    public function setResource($resource){
    	return $this;
    }


    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupRepeatable
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set resourceId
     *
     * @param integer $resourceId
     *
     * @return ForeupRepeatable
     */
    public function setResourceId($resourceId)
    {
        $this->resourceId = $resourceId;
    
        return $this;
    }

    /**
     * Get resourceId
     *
     * @return integer
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * Set lastRan
     *
     * @param \DateTime $lastRan
     *
     * @return ForeupRepeatable
     */
    public function setLastRan($lastRan)
    {
        $this->lastRan = $lastRan;
    
        return $this;
    }

    /**
     * Get lastRan
     *
     * @return \DateTime
     */
    public function getLastRan()
    {
        return $this->lastRan;
    }

    /**
     * Set nextOccurence
     *
     * @param \DateTime $nextOccurence
     *
     * @return ForeupRepeatable
     */
    public function setNextOccurence($nextOccurence)
    {
    	if(is_string($nextOccurence))$nextOccurence=new \DateTime($nextOccurence);
        $this->nextOccurence = $nextOccurence;
    
        return $this;
    }

    public function calculateNextOccurence(\DateTime $currentTime = null)
    {
	    $ro = new RepeatableObject();
    	$array = $this->toRuleArray();
	    $ro->createFromArray($array);
	    $strTime = $currentTime->format(\DATE_ISO8601);
	    $nextTime = $ro->getNextCalculatedOccurrence($strTime);

	    return $nextTime;

    }

    public function calculateLastOccurrence(\DateTime $currentTime = null)
    {
    	$ro = new RepeatableObject();
    	$array = $this->toRuleArray();
    	$ro->createFromArray($array);
	    $strTime = $currentTime->format(\DATE_ISO8601);
	    $lastTime = $ro->getLastCalculatedOccurrence($strTime);

	    return $lastTime;

    }

    /**
     * Get nextOccurence
     *
     * @return \DateTime
     */
    public function getNextOccurence()
    {
        return $this->nextOccurence;
    }

    /**
     * Set freq
     *
     * @param string $freq
     *
     * @return ForeupRepeatable
     */
    public function setFreq($freq)
    {
        if(is_numeric($freq) && isset(self::$frequencies[$freq])){
            $freq = self::$frequencies[$freq];
        }
        $this->freq = $freq;
    
        return $this;
    }

    /**
     * Get freq
     *
     * @return string
     */
    public function getFreq()
    {
        return $this->freq;
    }

    /**
     * Set dtstart
     *
     * @param \DateTime $dtstart
     *
     * @return ForeupRepeatable
     */
    public function setDtstart($dtstart)
    {
	    if(is_string($dtstart))$dtstart=new \DateTime($dtstart);
        $this->dtstart = $dtstart;
    
        return $this;
    }

    /**
     * Get dtstart
     *
     * @return \DateTime
     */
    public function getDtstart()
    {
        return $this->dtstart;
    }

    /**
     * Set until
     *
     * @param \DateTime $until
     *
     * @return ForeupRepeatable
     */
    public function setUntil($until)
    {
	    if(is_string($until))$until=new \DateTime($until);
        $this->until = $until;
    
        return $this;
    }

    /**
     * Get until
     *
     * @return \DateTime
     */
    public function getUntil()
    {
        return $this->until;
    }

    /**
     * Set interval
     *
     * @param integer $interval
     *
     * @return ForeupRepeatable
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;
    
        return $this;
    }

    /**
     * Get interval
     *
     * @return integer
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return ForeupRepeatable
     */
    public function setCount($count)
    {
        $this->count = $count;
    
        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set bymonth
     *
     * @param integer $bymonth
     *
     * @return ForeupRepeatable
     */
    public function setBymonth($bymonth)
    {
        $this->bymonth = $bymonth;
    
        return $this;
    }

    /**
     * Get bymonth
     *
     * @return integer
     */
    public function getBymonth()
    {
        return $this->bymonth;
    }

    /**
     * Set byweekno
     *
     * @param integer $byweekno
     *
     * @return ForeupRepeatable
     */
    public function setByweekno($byweekno)
    {
        $this->byweekno = $byweekno;
    
        return $this;
    }

    /**
     * Get byweekno
     *
     * @return integer
     */
    public function getByweekno()
    {
        return $this->byweekno;
    }

    /**
     * Set byyearday
     *
     * @param integer $byyearday
     *
     * @return ForeupRepeatable
     */
    public function setByyearday($byyearday)
    {
        $this->byyearday = $byyearday;
    
        return $this;
    }

    /**
     * Get byyearday
     *
     * @return integer
     */
    public function getByyearday()
    {
        return $this->byyearday;
    }

    /**
     * Set bymonthday
     *
     * @param integer $bymonthday
     *
     * @return ForeupRepeatable
     */
    public function setBymonthday($bymonthday)
    {
        $this->bymonthday = $bymonthday;
    
        return $this;
    }

    /**
     * Get bymonthday
     *
     * @return integer
     */
    public function getBymonthday()
    {
        return $this->bymonthday;
    }

    /**
     * Set byday
     *
     * @param string $byday
     *
     * @return ForeupRepeatable
     */
    public function setByday($byday)
    {
        $this->byday = $byday;
    
        return $this;
    }

    /**
     * Get byday
     *
     * @return string
     */
    public function getByday($getStr = false)
    {
        $data = $this->byday;

        if($getStr){
            if(is_array($this->byday)){
                $data = [];
                foreach($this->byday as $key => $val){
                	if(is_numeric($val)) {
		                $data[] = self::$bydayEnum[$val];
	                }else{
                		$data[] = $val;
	                }
                }
            }else{
                if($data !== null){
                    $data = self::$bydayEnum[$data];
                }
            }
        }
        return $data;
    }

    /**
     * Set wkst
     *
     * @param string|integer $wkst
     *
     * @return ForeupRepeatable
     */
    public function setWkst($wkst)
    {
    	$enum = array('MO','TU','WE','TH','FR','SA');
    	if(is_numeric($wkst) && is_int($wkst*1) && isset($enum[$wkst])){
    		$wkst = $enum[$wkst];
	    }
        $this->wkst = $wkst;
    
        return $this;
    }

    /**
     * Get wkst
     *
     * @return string
     */
    public function getWkst()
    {
        return $this->wkst;
    }

    /**
     * Set byhour
     *
     * @param integer $byhour
     *
     * @return ForeupRepeatable
     */
    public function setByhour($byhour)
    {
        $this->byhour = ($byhour);
    
        return $this;
    }

    /**
     * Get byhour
     *
     * @return integer
     */
    public function getByhour()
    {
        return $this->byhour;
    }

    /**
     * Set byminute
     *
     * @param integer $byminute
     *
     * @return ForeupRepeatable
     */
    public function setByminute($byminute)
    {
        $this->byminute = $byminute;
    
        return $this;
    }

    /**
     * Get byminute
     *
     * @return integer
     */
    public function getByminute()
    {
        if($this->byminute === null){
            return [];
        }
        return $this->byminute;
    }

    /**
     * Set bysecond
     *
     * @param integer $bysecond
     *
     * @return ForeupRepeatable
     */
    public function setBysecond($bysecond)
    {
        $this->bysecond = $bysecond;
    
        return $this;
    }

    /**
     * Get bysecond
     *
     * @return integer
     */
    public function getBysecond()
    {
        if($this->bysecond === null){
            return [];
        }
        return $this->bysecond;
    }

    /**
     * Set bysetpos
     *
     * @param integer $bysetpos
     *
     * @return ForeupRepeatable
     */
    public function setBysetpos($bysetpos)
    {
        $this->bysetpos = $bysetpos;
    
        return $this;
    }

    /**
     * Get bysetpos
     *
     * @return integer
     */
    public function getBysetpos()
    {
        return $this->bysetpos;
    }

    /**
     * toRuleArray
     *
     * @return array
     */
    public function toRuleArray(){

        $array = [
            'FREQ' => $this->getFreq(),
            'DTSTART' => !empty($this->getDtstart())?$this->getDtstart()->format(\DateTime::ISO8601):null,
            'UNTIL' => !empty($this->getUntil())?$this->getUntil()->format(\DateTime::ISO8601):null,
            'INTERVAL' => $this->getInterval(),
            'COUNT' => $this->getCount(),
            'BYMONTH' => $this->getBymonth(),
            'BYWEEKNO' => $this->getByweekno(),
            'BYYEARDAY' => $this->getByyearday(),
            'BYMONTHDAY' => $this->getBymonthday(),
            'BYDAY' => $this->getByday(true),
            'WKST' => $this->getWkst(),
            'BYHOUR' => $this->getByhour(),
            'BYMINUTE' => $this->getByminute(),
            'BYSECOND' => $this->getBysecond(),
            'BYSETPOS' => $this->getBysetpos() 
        ];      

        $commaSeparated = [
            'BYMONTH',
            'BYWEEKNO',
            'BYYEARDAY',
            'BYMONTHDAY',
            'BYDAY',
            'BYHOUR',
            'BYMINUTE',
            'BYSECOND',
            'BYSETPOS'
        ];

        foreach($array as $key => &$val){
            
            if(is_array($val) && in_array($key, $commaSeparated)){
                $val = implode(',', $val);
            }

            if($val === ''){
                $val = null;
            }
        }

        if(empty($array['BYDAY'])){
            $array['BYDAY'] = null;
        }

        return $array;
    }
}
