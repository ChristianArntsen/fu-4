<?php
namespace foreup\rest\models\entities;
use Doctrine\ORM\Mapping as ORM;
use \foreup\rest\models\entities\ForeupAccountRecurringStatements;
use \foreup\rest\models\entities\ForeupRepeatable;

/**
 * Class ForeupRepeatable
 * @package foreup\rest\models\entities
 * @ORM\Entity
 */
class ForeupRepeatableAccountRecurringStatements extends ForeupRepeatable
{
	protected $type = 'recurring_statement';

	/**
	 * @var ForeupAccountRecurringStatements
	 *
	 * @ORM\ManyToOne(targetEntity="ForeupAccountRecurringStatements")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="resource_id", referencedColumnName="id")
	 * })
	 */
	private $statement;

	/**
	 * @return ForeupAccountRecurringStatements
	 */
	public function getStatement()
	{
		return $this->statement;
	}

	/**
	 * @return ForeupAccountRecurringStatements
	 */
	public function getResource()
	{
		return $this->getStatement();
	}

	/**
	 * @param ForeupAccountRecurringStatements $statement
	 * @return ForeupRepeatableAccountRecurringStatements
	 */
	public function setStatement($statement)
	{
		$this->statement = $statement;
		$type = 'recurring_statement';
		$resource_id = $statement->getId();
		$this->setType($type);
		$this->setResourceId($resource_id);
		return $this;
	}

	/**
	 * @param ForeupAccountRecurringStatements $statement
	 * @return ForeupRepeatableAccountRecurringStatements
	 */
	public function setResource($statement){
		return $this->setStatement(($statement));
	}
}