<?php

namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupPrinters
 *
 * @ORM\Table(name="foreup_printers")
 * @ORM\Entity
 */
class ForeupPrinters
{
    /**
     * @var integer
     *
     * @ORM\Column(name="printer_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $printerId;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ip_address", type="string", length=255, nullable=false)
	 */
	private $ipAddress;

	/**
	 * @var \foreup\rest\models\entities\ForeupCourses
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $course;

	/**
	 * @return int
	 */
	public function getPrinterId()
	{
		return $this->printerId;
	}

	/**
	 * @param int $printerId
	 */
	public function setPrinterId(int $printerId)
	{
		$this->printerId = $printerId;
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @param string $label
	 */
	public function setLabel(string $label)
	{
		$this->label = $label;
	}

	/**
	 * @return string
	 */
	public function getIpAddress()
	{
		return $this->ipAddress;
	}

	/**
	 * @param string $ipAddress
	 */
	public function setIpAddress(string $ipAddress)
	{
		$this->ipAddress = $ipAddress;
	}

	/**
	 * @return ForeupCourses
	 */
	public function getCourse(): ForeupCourses
	{
		return $this->course;
	}

	/**
	 * @param ForeupCourses $course
	 */
	public function setCourse(ForeupCourses $course)
	{
		$this->course = $course;
	}



}
