<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountLedgerPayments
 *
 * @ORM\Table(name="foreup_account_ledger_payments", uniqueConstraints={@ORM\UniqueConstraint(name="ledger_id_payment_id", columns={"ledger_id", "payment_id"})}, indexes={@ORM\Index(name="fk_account_ledger_payments_payment_id_idx", columns={"payment_id"}), @ORM\Index(name="IDX_BD403205A7B913DD", columns={"ledger_id"})})
 * @ORM\Entity
 */
class ForeupAccountLedgerPayments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $amount = '0.00';

    /**
     * @var \foreup\rest\models\entities\ForeupAccountLedger
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountLedger")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ledger_id", referencedColumnName="id")
     * })
     */
    private $ledger;

    /**
     * @var \foreup\rest\models\entities\ForeupAccountPayments
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountPayments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     * })
     */
    private $payment;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return ForeupAccountLedgerPayments
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set ledger
     *
     * @param \foreup\rest\models\entities\ForeupAccountLedger $ledger
     *
     * @return ForeupAccountLedgerPayments
     */
    public function setLedger(\foreup\rest\models\entities\ForeupAccountLedger $ledger = null)
    {
        $this->ledger = $ledger;
    
        return $this;
    }

    /**
     * Get ledger
     *
     * @return \foreup\rest\models\entities\ForeupAccountLedger
     */
    public function getLedger()
    {
        return $this->ledger;
    }

    /**
     * Set payment
     *
     * @param \foreup\rest\models\entities\ForeupAccountPayments $payment
     *
     * @return ForeupAccountLedgerPayments
     */
    public function setPayment(\foreup\rest\models\entities\ForeupAccountPayments $payment = null)
    {
        $this->payment = $payment;
    
        return $this;
    }

    /**
     * Get payment
     *
     * @return \foreup\rest\models\entities\ForeupAccountPayments
     */
    public function getPayment()
    {
        return $this->payment;
    }
}
