<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ForeupAccountRecurringCharges
 *
 * @ORM\Table(name="foreup_account_recurring_charges", uniqueConstraints={@ORM\UniqueConstraint(name="number", columns={"organization_id"})}, indexes={@ORM\Index(name="payment_terms_id", columns={"payment_terms_id"}), @ORM\Index(name="organization_id", columns={"organization_id"}), @ORM\Index(name="created_by", columns={"created_by"})})
 * @ORM\Entity (repositoryClass="foreup\rest\models\repositories\AccountRecurringChargesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ForeupAccountRecurringCharges
{
	use \foreup\rest\models\entities\EntityValidator;

	public function __construct()
	{
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
		$this->invoices = new \Doctrine\Common\Collections\ArrayCollection();
		$this->accountRecurringChargeCustomers = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true) {
		$location = 'ForeupAccountRecurringCharges->validate';
		$this->resetLastError();
		$dateCreated = $this->getDateCreated();

		$name = $this->getName();
		$v = $this->validate_string($location,'name',$name,false,$throw);
		if($v!==true)return $v;

		$description = $this->getDescription();
		$v = $this->validate_string($location,'description',$description,false,$throw);
		if($v!==true)return $v;

		if(isset($dateCreated) && !is_a($dateCreated, '\DateTime')){
			$this->last_error = 'ForeupaccountRecurringCharges->validate Error: invalid dateCreated: '.$dateCreated;
			return $this->invalid($throw);
		}

		$createdBy = $this->getCreatedBy();
		if(!isset($createdBy)){
			$this->last_error = 'ForeupaccountRecurringCharges->validate Error: createdBy cannot be NULL';
			return $this->invalid($throw);
		}
		if(!is_numeric($createdBy)||!is_int($createdBy*1)){
			$this->last_error = 'ForeupaccountRecurringCharges->validate Error: createdBy must be an integer: '.$createdBy;
			return $this->invalid($throw);
		}

		$organizationId = $this->getOrganizationId();
		if(!isset($organizationId)){
			$this->last_error = 'ForeupaccountRecurringCharges->validate Error: setOrganizationId cannot be NULL';
			return $this->invalid($throw);
		}
		if(!is_numeric($organizationId)||!is_int($organizationId*1)){
			$this->last_error = 'ForeupaccountRecurringCharges->validate Error: organizationId must be an integer: '.$organizationId;
			return $this->invalid($throw);
		}

		$isActive = $this->getIsActive();
		$v = $this->validate_boolean($location,'isActive',$isActive,true,$throw);
		if($v!==true)return $v;

		$dateDeleted = $this->getDateDeleted();
		if(isset($dateDeleted) && !is_a($dateDeleted, '\DateTime')){
			$this->last_error = 'ForeupaccountRecurringCharges->validate Error: invalid dateDeleted: '.$endDate;
			return $this->invalid($throw);
		}

		$deletedBy = $this->getDeletedBy();
		if(isset($deletedBy) && (!is_numeric($deletedBy)||!is_int($deletedBy*1))){
			$this->last_error = 'ForeupaccountRecurringCharges->validate Error: deletedBy must be an integer: '.$deletedBy;
			return $this->invalid($throw);
		}

		$recurringStatement = $this->getRecurringStatement();
		if(!is_a($recurringStatement, '\foreup\rest\models\entities\ForeupAccountRecurringStatements')){
			$this->last_error = 'ForeupaccountRecurringCharges->validate Error: invalid recurringStatement: '.$recurringStatement;
			return $this->invalid($throw);
		}

		$prorateCharges = $this->getProrateCharges();
		$v = $this->validate_boolean($location,'prorateCharges',$prorateCharges,true,$throw);
		if($v!==true)return $v;

		return true;
	}

	private function invalid($throw = true){
		if($throw)throw new \InvalidArgumentException($this->last_error);
		else return $this->last_error;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=256, precision=0, scale=0, nullable=true, unique=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=256, precision=0, scale=0, nullable=true, unique=false)
	 */
	private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dateCreated;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $organizationId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isActive = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_deleted", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateDeleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted_by", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $deletedBy;

    /**
     * @var \foreup\rest\models\entities\ForeupAccountPaymentTerms
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountPaymentTerms")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_terms_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $paymentTerms;


	/**
	 * @var \foreup\rest\models\entities\ForeupAccountRecurringChargeItems
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupAccountRecurringChargeItems", mappedBy="recurringCharge")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="id", referencedColumnName="recurring_charge_id")
	 * })
	 */
	private $items;


	/**
	 * @var \foreup\rest\models\entities\ForeupInvoices
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupInvoices", mappedBy="recurringCharge")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="id", referencedColumnName="account_recurring_charge_id")
	 * })
	 */
	private $invoices;

	/**
	 * @var \foreup\rest\models\entities\ForeupAccountRecurringStatements
	 *
	 * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupAccountRecurringStatements")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="recurring_statement_id", referencedColumnName="id", nullable=false)
	 * })
	 */
	private $recurringStatement;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="prorate_charges", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $prorateCharges = 0;

	/**
	 * @var ForeupAccountRecurringChargeCustomers
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupAccountRecurringChargeCustomers", mappedBy="recurringCharge")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="id", referencedColumnName="recurring_charge_id")
	 * })
	 */
	private $accountRecurringChargeCustomers;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection|ForeupAccountRecurringChargeCustomers[]
	 */
	public function getAccountRecurringChargeCustomers()
	{
		return $this->accountRecurringChargeCustomers;
	}

	/**
	 * @param ForeupAccountRecurringChargeCustomers $customer
	 */
	public function addAccountRecurringChargeCustomers($customer)
	{
		$customer->setRecurringCharge($this);
		$this->accountRecurringChargeCustomers[] = $customer;

		return $this;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection|ForeupAccountRecurringChargeItems[]
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @param ForeupAccountRecurringChargeItems $item
	 */
	public function addItems($item)
	{
		$item->setRecurringCharge($this);
		$this->items[] = $item;

		return $this;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection|ForeupInvoices[]
	 */
	public function getInvoices()
	{
		return $this->invoices;
	}

	/**
	 * @param ForeupInvoices $item
	 */
	public function addInvoice(ForeupInvoices $item)
	{
		$item->setRecurringCharge($this);
		$this->invoices[] = $item;

		return $this;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return ForeupAccountRecurringCharges
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $name
	 *
	 * @return ForeupAccountRecurringCharges
	 */
	public function setDescription($description)
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupAccountRecurringCharges
     */
	public function setDateCreated(\DateTime $dateCreated)
	{
		$this->dateCreated = $dateCreated;

		return $this;
	}

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return ForeupAccountRecurringCharges
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     *
     * @return ForeupAccountRecurringCharges
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

	/**
	 * Get organizationId
	 *
	 * @return integer
	 */
	public function getOrganizationId()
	{
		return $this->organizationId;
	}

	/**
	 * Get courseId
	 *
	 * @return integer
	 */
	public function getCourseId()
	{
		return $this->organizationId;
	}

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return ForeupAccountRecurringCharges
     * @throws \InvalidArgumentException
     *
     */
    public function setIsActive($isActive)
    {
    	if(is_string($isActive)){
    		$tmp = json_decode(strtolower($isActive));
    		if(isset($tmp))$isActive = $tmp;
	    }
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set dateDeleted
     *
     * @param \DateTime $dateDeleted
     *
     * @return ForeupAccountRecurringCharges
     */
    public function setDateDeleted(\DateTime $dateDeleted = null)
    {
        $this->dateDeleted = $dateDeleted;
    
        return $this;
    }

    /**
     * Get dateDeleted
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->dateDeleted;
    }

    /**
     * Set deletedBy
     *
     * @param integer $deletedBy
     *
     * @return ForeupAccountRecurringCharges
     */
    public function setDeletedBy($deletedBy)
    {
        $this->deletedBy = $deletedBy;
    
        return $this;
    }

    /**
     * Get deletedBy
     *
     * @return integer
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

	/**
	 * Set paymentTerms
	 *
	 * @param \foreup\rest\models\entities\ForeupAccountPaymentTerms|null $paymentTerms
	 *
	 * @return ForeupAccountRecurringCharges
	 */
	public function setPaymentTerms(\foreup\rest\models\entities\ForeupAccountPaymentTerms $paymentTerms = null)
	{
		$this->paymentTerms = $paymentTerms;

		return $this;
	}

	/**
	 * Get paymentTerms
	 *
	 * @return \foreup\rest\models\entities\ForeupAccountPaymentTerms|null
	 */
	public function getPaymentTerms()
	{
		return $this->paymentTerms;
	}

	/**
	 * Set recurringStatement
	 *
	 * @param \foreup\rest\models\entities\ForeupAccountPaymentTerms|null $paymentTerms
	 *
	 * @return ForeupAccountRecurringCharges
	 */
	public function setRecurringStatement(\foreup\rest\models\entities\ForeupAccountRecurringStatements $recurringStatement = null)
	{
		$this->recurringStatement = $recurringStatement;

		return $this;
	}

	/**
	 * Get recurringStatement
	 *
	 * @return \foreup\rest\models\entities\ForeupAccountRecurringStatements|null
	 */
	public function getRecurringStatement()
	{
		return $this->recurringStatement;
	}

	/**
	 * Set prorateCharges
	 *
	 * @param boolean $prorate
	 *
	 * @return ForeupAccountRecurringCharges
	 * @throws \InvalidArgumentException
	 *
	 */
	public function setProrateCharges($prorate)
	{
		if(is_string($prorate)){
			$tmp = json_decode(strtolower($prorate));
			if(isset($tmp))$prorate = $tmp;
		}
		$this->prorateCharges = $prorate;

		return $this;
	}

	/**
	 * Get prorateCharges
	 *
	 * @return boolean
	 */
	public function getProrateCharges()
	{
		return $this->prorateCharges;
	}
}

