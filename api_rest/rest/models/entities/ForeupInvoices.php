<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupInvoices
 *
 * @ORM\Table(name="foreup_invoices", indexes={@ORM\Index(name="credit_card_payment_id", columns={"credit_card_payment_id"}), @ORM\Index(name="course_id", columns={"course_id"}), @ORM\Index(name="person_id", columns={"person_id"}), @ORM\Index(name="billing_id", columns={"billing_id"}), @ORM\Index(name="date", columns={"date"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupInvoices
{
	use \foreup\rest\models\entities\EntityValidator;

	public function __construct()
	{
		$this->invoiceItems = new \Doctrine\Common\Collections\ArrayCollection();

	}

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function mungeSaleId($something) {
		$something;
	}

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{
		$this->resetLastError();
		$location = 'ForeupInvoices->validate';

		$invoiceNumber = $this->getInvoiceNumber();
		$v = $this->validate_integer($location,'invoiceNumber',$invoiceNumber,true,$throw);
		if($v!==true)return $v;

		$name = $this->getName();
		$v = $this->validate_string($location,'name',$name,true,$throw);
		if($v!==true)return $v;

		$department = $this->getDepartment();
		$v = $this->validate_string($location,'department',$department,true,$throw);
		if($v!==true)return $v;

		$category = $this->getCategory();
		$v = $this->validate_string($location,'category',$category,true,$throw);
		if($v!==true)return $v;

		$subcategory = $this->getSubcategory();
		$v = $this->validate_string($location,'subcategory',$subcategory,true,$throw);
		if($v!==true)return $v;

		$day = $this->getDay();
		$v = $this->validate_boolean($location,'day',$day,true,$throw);
		if($v!==true)return $v;

		$courseId = $this->getCourseId();###################
		$v = $this->validate_integer($location,'courseId',$courseId,true,$throw);
		if($v!==true)return $v;

		$creditCard = $this->getCreditCard(); ########## foreup customer credit cards

		$billingId = $this->getBillingId();
		$v = $this->validate_integer($location,'billingId',$billingId,true,$throw);
		if($v!==true)return $v;

		$person = $this->getPerson();

		$employee = $this->getEmployee();

		$date = $this->getDate();
		if($date==='CURRENT_TIMESTAMP')$date = new \DateTime();
		$this->setDate($date);
		$v = $this->validate_object($location,'date',$date,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$billStart = $this->getBillStart();
		$v = $this->validate_object($location,'billStart',$billStart,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$billEnd = $this->getBillEnd();
		$v = $this->validate_object($location,'billEnd',$billEnd,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$total = $this->getTotal();
		$v = $this->validate_numeric($location,'total',$total,true,$throw);
		if($v!==true)return $v;

		$overdueTotal = $this->getOverdueTotal();
		$v = $this->validate_numeric($location,'overdueTotal',$overdueTotal,true,$throw);
		if($v!==true)return $v;

		$previousPayments = $this->getPreviousPayments();
		$v = $this->validate_numeric($location,'previousPayments',$previousPayments,true,$throw);
		if($v!==true)return $v;

		$paid = $this->getPaid();
		$v = $this->validate_numeric($location,'paid',$paid,true,$throw);
		if($v!==true)return $v;

		$overdue = $this->getOverdue();
		$v = $this->validate_numeric($location,'overdue',$overdue,true,$throw);
		if($v!==true)return $v;

		$deleted = $this->getDeleted();
		$v = $this->validate_boolean($location,'deleted',$deleted,true,$throw);
		if($v!==true)return $v;

		$creditCardPaymentId = $this->getCreditCardPaymentId();####### foreup sales payments credit cards
		$creditCardPayment = $this->getCreditCardPayment();

		$emailInvoice = $this->getEmailInvoice();
		$v = $this->validate_boolean($location,'emailInvoice',$emailInvoice,true,$throw);
		if($v!==true)return $v;

		$lastBillingAttempt = $this->getLastBillingAttempt();
		$v = $this->validate_object($location,'lastBillingAttempt',$lastBillingAttempt,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$started = $this->getStarted();
		$v = $this->validate_boolean($location,'started',$started,true,$throw);
		if($v!==true)return $v;

		$chargedNoSale = $this->getChargedNoSale();
		$v = $this->validate_boolean($location,'chargedNoSale',$chargedNoSale,true,$throw);
		if($v!==true)return $v;

		$charged = $this->getCharged();
		$v = $this->validate_boolean($location,'charged',$charged,true,$throw);
		if($v!==true)return $v;

		$emailed = $this->getEmailed();
		$v = $this->validate_boolean($location,'emailed',$emailed,true,$throw);
		if($v!==true)return $v;

		$sendDate = $this->getSendDate();
		$v = $this->validate_object($location,'sendDate',$sendDate,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$dueDate = $this->getDueDate();
		$v = $this->validate_object($location,'dueDate',$dueDate,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$autoBillDate = $this->getAutoBillDate();
		$v = $this->validate_object($location,'autoBillDate',$autoBillDate,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$payCustomerAccount = $this->getPayCustomerAccount();
		$v = $this->validate_boolean($location,'payCustomerAccount',$payCustomerAccount,true,$throw);
		if($v!==true)return $v;

		$payMemberAccount = $this->getPayMemberAccount();
		$v = $this->validate_boolean($location,'payMemberAccount',$payMemberAccount,true,$throw);
		if($v!==true)return $v;

		$showAccountTransactions = $this->getShowAccountTransactions();
		$v = $this->validate_boolean($location,'showAccountTransactions',$showAccountTransactions,true,$throw);
		if($v!==true)return $v;

		$sale = $this->getSale();###########################

		$notes = $this->getNotes();
		$v = $this->validate_string($location,'notes',$notes,true,$throw);
		if($v!==true)return $v;

		$attemptLimit = $this->getAttemptLimit();
		$v = $this->validate_integer($location,'attemptLimit',$attemptLimit,true,$throw);
		if($v!==true)return $v;

		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $invoiceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_number", type="integer", nullable=false)
     */
    private $invoiceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="department", type="string", length=255, nullable=false)
     */
    private $department = 'Invoice';

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=255, nullable=false)
     */
    private $category = 'Invoice';

    /**
     * @var string
     *
     * @ORM\Column(name="subcategory", type="string", length=255, nullable=false)
     */
    private $subcategory = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="day", type="boolean", nullable=false)
     */
    private $day = 0;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="course_id", type="integer", nullable=false)
	 */
	private $courseId;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="person_id", type="integer", nullable=false)
	 */
	private $personId;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="credit_card_id", type="integer", nullable=false)
	 */
	private $creditCardId = 0;

	/**
	 * @var \foreup\rest\models\entities\ForeupCustomerCreditCards
	 */
    private $creditCard;

    /**
     * @var integer
     *
     * @ORM\Column(name="billing_id", type="integer", nullable=false)
     */
    private $billingId = 0;

	/**
	 * @var \foreup\rest\models\entities\ForeupPeople
	 */
	private $person;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="employee_id", type="integer", nullable=false)
	 */
	private $employeeId = 0;

	/**
	 * @var \foreup\rest\models\entities\ForeupEmployees
	 */
	private $employee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bill_start", type="datetime", nullable=false)
     */
    private $billStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bill_end", type="datetime", nullable=false)
     */
    private $billEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="overdue_total", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $overdueTotal = 0.00;

    /**
     * @var string
     *
     * @ORM\Column(name="previous_payments", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $previousPayments = 0.00;

    /**
     * @var string
     *
     * @ORM\Column(name="paid", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $paid = 0.00;

    /**
     * @var string
     *
     * @ORM\Column(name="overdue", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $overdue = 0.00;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="credit_card_payment_id", type="integer", nullable=false)
     */
    private $creditCardPaymentId = 0;

	/**
	 * @var \foreup\rest\models\entities\ForeupSalesPaymentsCreditCards
	 */
	private $creditCardPayment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="email_invoice", type="boolean", nullable=false)
     */
    private $emailInvoice = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_billing_attempt", type="date", nullable=true)
     */
    private $lastBillingAttempt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="started", type="boolean", nullable=false)
     */
    private $started = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="charged_no_sale", type="boolean", nullable=false)
     */
    private $chargedNoSale = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="charged", type="boolean", nullable=false)
     */
    private $charged = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="emailed", type="boolean", nullable=false)
     */
    private $emailed = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="send_date", type="date", nullable=true)
     */
    private $sendDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="date", nullable=true)
     */
    private $dueDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="auto_bill_date", type="date", nullable=true)
     */
    private $autoBillDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pay_customer_account", type="boolean", nullable=false)
     */
    private $payCustomerAccount = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="pay_member_account", type="boolean", nullable=false)
     */
    private $payMemberAccount = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_account_transactions", type="boolean", nullable=false)
     */
    private $showAccountTransactions = '0';


	/**
	 * @var \foreup\rest\models\entities\ForeupSales
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupSales")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="sale_id", referencedColumnName="sale_id")
	 * })
	 */
	private $sale;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="sale_id", type="integer", nullable=false)
	 */
	private $saleId = 0;

	/**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=255, nullable=false)
     */
    private $notes = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="attempt_limit", type="integer", nullable=false)
     */
    private $attemptLimit = 1;

	/**
	 * @var \foreup\rest\models\entities\ForeupAccountRecurringCharges|null
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountRecurringCharges")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="account_recurring_charge_id", referencedColumnName="id")
	 * })
	 */
	private $recurringCharge;


	/**
	 * @var \foreup\rest\models\entities\ForeupInvoiceItems
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupInvoiceItems", mappedBy="invoice",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="invoice_id")
	 * })
	 */
	private $invoiceItems;



	/**
	 * Get invoiceId
	 *
	 * @return integer
	 */
	public function getStatementId()
	{
		return $this->invoiceId;
	}



	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->invoiceId;
	}

    /**
     * Set invoiceNumber
     *
     * @param integer $invoiceNumber
     *
     * @return ForeupInvoices
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;
    
        return $this;
    }

    /**
     * Get invoiceNumber
     *
     * @return integer
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupInvoices
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set department
     *
     * @param string $department
     *
     * @return ForeupInvoices
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    
        return $this;
    }

    /**
     * Get department
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return ForeupInvoices
     */
    public function setCategory($category)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set subcategory
     *
     * @param string $subcategory
     *
     * @return ForeupInvoices
     */
    public function setSubcategory($subcategory)
    {
        $this->subcategory = $subcategory;
    
        return $this;
    }

    /**
     * Get subcategory
     *
     * @return string
     */
    public function getSubcategory()
    {
        return $this->subcategory;
    }

    /**
     * Set day
     *
     * @param boolean $day
     *
     * @return ForeupInvoices
     */
    public function setDay($day)
    {
	    if(is_string($day)){
			$day = strtolower($day);
		    $ac = json_decode($day,true);
		    if(isset($ac))$day = $ac;
	    }

	    $this->day = $day;
    
        return $this;
    }

    /**
     * Get day
     *
     * @return boolean
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupInvoices
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    
        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

	/**
	 * Set creditCard
	 *
	 * @param \foreup\rest\models\entities\ForeupCustomerCreditCards|null $card
	 *
	 * @return ForeupInvoices
	 */
	public function setCreditCard(\foreup\rest\models\entities\ForeupCustomerCreditCards $card = null)
	{
		if(!$card){
			$this->creditCardId = 0;
			$this->creditCard = null;

			return $this;
		}

		if(!method_exists($card,'getCreditCardId')){
			throw new \Exception('ForeupInvoiceItems->setCreditCard: Invalid ForeupCustomerCreditCard');
		}
		$this->creditCardId = $card->getCreditCardId();

		$this->creditCard = $card;

		return $this;
	}

	/**
	 * Get creditCard
	 *
	 * @return \foreup\rest\models\entities\ForeupCustomerCreditCards
	 */
	public function getCreditCard()
	{
		if($this->creditCardId == 0)return null;
		return $this->creditCard;
	}

    /**
     * Set billingId
     *
     * @param integer $billingId
     *
     * @return ForeupInvoices
     */
    public function setBillingId($billingId)
    {
        $this->billingId = $billingId;
    
        return $this;
    }

    /**
     * Get billingId
     *
     * @return integer
     */
    public function getBillingId()
    {
        return $this->billingId;
    }

	/**
	 * Set person
	 *
	 * @param \foreup\rest\models\entities\ForeupCustomers $person
	 *
	 * @return ForeupInvoices
	 */
	public function setPerson(\foreup\rest\models\entities\ForeupPeople $person = null)
	{
		if(!isset($person)){
			$this->person_id = 0;
		}else{
			$this->personId = $person->getPersonId();
		}

		$this->person = $person;
		// does not always belong to course...
		//$this->courseId = $person->getCourseId();

		return $this;
	}

	/**
	 * Get person
	 *
	 * @return \foreup\rest\models\entities\ForeupCustomers
	 */
	public function getPerson()
	{
		return $this->person;
	}

	/**
	 * Set employee
	 *
	 * @param \foreup\rest\models\entities\ForeupEmployees|null $employee
	 *
	 * @return ForeupInvoices
	 */
	public function setEmployee(\foreup\rest\models\entities\ForeupEmployees $employee = null)
	{
		if(!isset($employee)){
			$this->employeeId = 0;
			$this->employee = null;
			return $this;
		}

		if(!method_exists($employee,'getPerson')){
			throw \Exception('ForeupInvoiceItems->setEmployee: Invalid ForeupEmployees');
		}
		$this->employeeId = $employee->getPerson()->getPersonId();

		$this->employee = $employee;

		return $this;
	}

	/**
	 * Get employee
	 *
	 * @return \foreup\rest\models\entities\ForeupEmployees|null
	 */
	public function getEmployee()
	{
		if($this->employeeId == 0) return null;
		return $this->employee;
	}

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ForeupInvoices
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set billStart
     *
     * @param \DateTime $billStart
     *
     * @return ForeupInvoices
     */
    public function setBillStart($billStart)
    {
        $this->billStart = $billStart;
    
        return $this;
    }

    /**
     * Get billStart
     *
     * @return \DateTime
     */
    public function getBillStart()
    {
        return $this->billStart;
    }

    /**
     * Set billEnd
     *
     * @param \DateTime $billEnd
     *
     * @return ForeupInvoices
     */
    public function setBillEnd($billEnd)
    {
        $this->billEnd = $billEnd;
    
        return $this;
    }

    /**
     * Get billEnd
     *
     * @return \DateTime
     */
    public function getBillEnd()
    {
        return $this->billEnd;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ForeupInvoices
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set overdueTotal
     *
     * @param string $overdueTotal
     *
     * @return ForeupInvoices
     */
    public function setOverdueTotal($overdueTotal)
    {
        $this->overdueTotal = $overdueTotal;
    
        return $this;
    }

    /**
     * Get overdueTotal
     *
     * @return string
     */
    public function getOverdueTotal()
    {
        return $this->overdueTotal;
    }

    /**
     * Set previousPayments
     *
     * @param string $previousPayments
     *
     * @return ForeupInvoices
     */
    public function setPreviousPayments($previousPayments)
    {
        $this->previousPayments = $previousPayments;
    
        return $this;
    }

    /**
     * Get previousPayments
     *
     * @return string
     */
    public function getPreviousPayments()
    {
        return $this->previousPayments;
    }

    /**
     * Set paid
     *
     * @param string $paid
     *
     * @return ForeupInvoices
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    
        return $this;
    }

    /**
     * Get paid
     *
     * @return string
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set overdue
     *
     * @param string $overdue
     *
     * @return ForeupInvoices
     */
    public function setOverdue($overdue)
    {
        $this->overdue = $overdue;
    
        return $this;
    }

    /**
     * Get overdue
     *
     * @return string
     */
    public function getOverdue()
    {
        return $this->overdue;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ForeupInvoices
     */
    public function setDeleted($deleted)
    {
	    if(is_string($deleted)){
			$deleted = strtolower($deleted);
		    $ac = json_decode($deleted,true);
		    if(isset($ac))$deleted = $ac;
	    }

        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Get creditCardPaymentId
     *
     * @return integer
     */
    public function getCreditCardPaymentId()
    {
        return $this->creditCardPaymentId;
    }

	/**
	 * Set creditCardPayment
	 *
	 * @param \foreup\rest\models\entities\ForeupSalesPaymentsCreditCards|null $payment
	 *
	 * @return ForeupInvoices
	 */
	public function setCreditCardPayment($payment = null)
	{
		if(!isset($payment)){
			$this->creditCardPaymentId = 0;
			$this->creditCardPayment = null;
			return $this;
		}
		else{
			if(!method_exists($payment,'getInvoice')){
				throw Exception('ForeupInvoiceItems->setCreditCardPayment: Invalid ForeupSalesPaymentsCreditCards');
			}
			$this->creditCardPaymentId = $payment->getInvoice();
			$this->creditCardPayment = $payment;
		}

		return $this;
	}

	/**
	 * Get creditCard
	 *
	 * @return \foreup\rest\models\entities\ForeupSalesPaymentsCreditCards|null
	 */
	public function getCreditCardPayment()
	{
		if($this->creditCardPaymentId == 0)return null;
		return $this->creditCardPayment;

	}

    /**
     * Set emailInvoice
     *
     * @param boolean $emailInvoice
     *
     * @return ForeupInvoices
     */
    public function setEmailInvoice($emailInvoice)
    {
	    if(is_string($emailInvoice)){
			$emailInvoice = strtolower($emailInvoice);
		    $ac = json_decode($emailInvoice,true);
		    if(isset($ac))$emailInvoice = $ac;
	    }

	    $this->emailInvoice = $emailInvoice;
    
        return $this;
    }

    /**
     * Get emailInvoice
     *
     * @return boolean
     */
    public function getEmailInvoice()
    {
        return $this->emailInvoice;
    }

    /**
     * Set lastBillingAttempt
     *
     * @param \DateTime $lastBillingAttempt
     *
     * @return ForeupInvoices
     */
    public function setLastBillingAttempt($lastBillingAttempt)
    {
	    if(is_string($this->lastBillingAttempt) && strtotime($lastBillingAttempt)){
		    $lastBillingAttempt = new \DateTime($lastBillingAttempt);
	    }

	    $this->lastBillingAttempt = $lastBillingAttempt;
    
        return $this;
    }

    /**
     * Get lastBillingAttempt
     *
     * @return \DateTime
     */
    public function getLastBillingAttempt()
    {
    	if(!isset($this->lastBillingAttempt)){
    		$this->lastBillingAttempt = new \DateTime('0000-00-00 00:00:00');
	    }
	    if(isset($this->lastBillingAttempt) && method_exists($this->lastBillingAttempt,'getTimestamp') && $this->lastBillingAttempt->getTimestamp() < 0)
		    return null;
        return $this->lastBillingAttempt;
    }

    /**
     * Set started
     *
     * @param boolean $started
     *
     * @return ForeupInvoices
     */
    public function setStarted($started)
    {
	    if(is_string($started)){
			$started = strtolower($started);
		    $ac = json_decode($started,true);
		    if(isset($ac))$started = $ac;
	    }

	    $this->started = $started;
    
        return $this;
    }

    /**
     * Get started
     *
     * @return boolean
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * Set chargedNoSale
     *
     * @param boolean $chargedNoSale
     *
     * @return ForeupInvoices
     */
    public function setChargedNoSale($chargedNoSale)
    {
	    if(is_string($chargedNoSale)){
			$chargedNoSale = strtolower($chargedNoSale);
		    $ac = json_decode($chargedNoSale,true);
		    if(isset($ac))$chargedNoSale = $ac;
	    }

	    $this->chargedNoSale = $chargedNoSale;
    
        return $this;
    }

    /**
     * Get chargedNoSale
     *
     * @return boolean
     */
    public function getChargedNoSale()
    {
        return $this->chargedNoSale;
    }

    /**
     * Set charged
     *
     * @param boolean $charged
     *
     * @return ForeupInvoices
     */
    public function setCharged($charged)
    {
	    if(is_string($charged)){
			$charged = strtolower($charged);
		    $ac = json_decode($charged,true);
		    if(isset($ac))$charged = $ac;
	    }

	    $this->charged = $charged;
    
        return $this;
    }

    /**
     * Get charged
     *
     * @return boolean
     */
    public function getCharged()
    {
        return $this->charged;
    }

    /**
     * Set emailed
     *
     * @param boolean $emailed
     *
     * @return ForeupInvoices
     */
    public function setEmailed($emailed)
    {
	    if(is_string($emailed)){
			$emailed = strtolower($emailed);
		    $ac = json_decode($emailed,true);
		    if(isset($ac))$emailed = $ac;
	    }

	    $this->emailed = $emailed;
    
        return $this;
    }

    /**
     * Get emailed
     *
     * @return boolean
     */
    public function getEmailed()
    {
        return $this->emailed;
    }

    /**
     * Set sendDate
     *
     * @param \DateTime $sendDate
     *
     * @return ForeupInvoices
     */
    public function setSendDate($sendDate)
    {
	    if(is_string($this->sendDate) && strtotime($sendDate)){
		    $sendDate = new \DateTime($sendDate);
	    }

	    $this->sendDate = $sendDate;
    
        return $this;
    }

    /**
     * Get sendDate
     *
     * @return \DateTime
     */
    public function getSendDate()
    {
	    if(!isset($this->sendDate)){
		    $this->sendDate = new \DateTime('0000-00-00 00:00:00');
	    }
	    if(isset($this->sendDate) && method_exists($this->sendDate,'getTimestamp') && $this->sendDate->getTimestamp() < 0)
		    return null;
        return $this->sendDate;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     *
     * @return ForeupInvoices
     */
    public function setDueDate($dueDate)
    {
	    if(is_string($this->dueDate) && strtotime($dueDate)){
		    $dueDate = new \DateTime($dueDate);
	    }

	    $this->dueDate = $dueDate;
    
        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
	    if(!isset($this->dueDate)){
		    $this->dueDate = new \DateTime('0000-00-00 00:00:00');
	    }
	    if(isset($this->dueDate) && method_exists($this->dueDate,'getTimestamp') && $this->dueDate->getTimestamp() < 0)
		    return null;
        return $this->dueDate;
    }

    /**
     * Set autoBillDate
     *
     * @param \DateTime $autoBillDate
     *
     * @return ForeupInvoices
     */
    public function setAutoBillDate($autoBillDate)
    {
	    if(is_string($this->autoBillDate) && strtotime($autoBillDate)){
		    $autoBillDate = new \DateTime($autoBillDate);
	    }

        $this->autoBillDate = $autoBillDate;
    
        return $this;
    }

    /**
     * Get autoBillDate
     *
     * @return \DateTime
     */
    public function getAutoBillDate()
    {
	    if(!isset($this->autoBillDate)){
		    $this->autoBillDate = new \DateTime('0000-00-00 00:00:00');
	    }
	    if(isset($this->autoBillDate) && method_exists($this->autoBillDate,'getTimestamp') && $this->autoBillDate->getTimestamp() < 0)
		    return null;
        return $this->autoBillDate;
    }

    /**
     * Set payCustomerAccount
     *
     * @param boolean $payCustomerAccount
     *
     * @return ForeupInvoices
     */
    public function setPayCustomerAccount($payCustomerAccount)
    {
	    if(is_string($payCustomerAccount)){
			$payCustomerAccount = strtolower($payCustomerAccount);
		    $ac = json_decode($payCustomerAccount,true);
		    if(isset($ac))$payCustomerAccount = $ac;
	    }

	    $this->payCustomerAccount = $payCustomerAccount;
    
        return $this;
    }

    /**
     * Get payCustomerAccount
     *
     * @return boolean
     */
    public function getPayCustomerAccount()
    {
        return $this->payCustomerAccount;
    }

    /**
     * Set payMemberAccount
     *
     * @param boolean $payMemberAccount
     *
     * @return ForeupInvoices
     */
    public function setPayMemberAccount($payMemberAccount)
    {
	    if(is_string($payMemberAccount)){
			$payMemberAccount = strtolower($payMemberAccount);
		    $ac = json_decode($payMemberAccount,true);
		    if(isset($ac))$payMemberAccount = $ac;
	    }

	    $this->payMemberAccount = $payMemberAccount;
    
        return $this;
    }

    /**
     * Get payMemberAccount
     *
     * @return boolean
     */
    public function getPayMemberAccount()
    {
        return $this->payMemberAccount;
    }

    /**
     * Set showAccountTransactions
     *
     * @param boolean $showAccountTransactions
     *
     * @return ForeupInvoices
     */
    public function setShowAccountTransactions($showAccountTransactions)
    {
	    if(is_string($showAccountTransactions)){
			$showAccountTransactions = strtolower($showAccountTransactions);
		    $ac = json_decode($showAccountTransactions,true);
		    if(isset($ac))$showAccountTransactions = $ac;
	    }

	    $this->showAccountTransactions = $showAccountTransactions;
    
        return $this;
    }

    /**
     * Get showAccountTransactions
     *
     * @return boolean
     */
    public function getShowAccountTransactions()
    {
        return $this->showAccountTransactions;
    }

	/**
	 * Set sale
	 *
	 * @param \foreup\rest\models\entities\ForeupEmployees $sale
	 *
	 * @return ForeupInvoices
	 */
	public function setSale(\foreup\rest\models\entities\ForeupSales $sale = null)
	{
		if(empty($sale)){
			$this->saleId = 0;
		}else {
			$this->saleId = $sale->getSaleId();
		}
		$this->sale = $sale;

		return $this;
	}

	/**
	 * Get sale
	 *
	 * @return \foreup\rest\models\entities\ForeupSales
	 */
	public function getSale()
	{
		return $this->sale;
	}

	/**
	 * Set recurringCharge
	 *
	 * @param \foreup\rest\models\entities\ForeupAccountRecurringCharges|null $recurringCharge
	 *
	 * @return ForeupInvoices
	 */
	public function setRecurringCharge(\foreup\rest\models\entities\ForeupAccountRecurringCharges $recurringCharge = null)
	{
		$this->recurringCharge = $recurringCharge;

		return $this;
	}

	/**
	 * Get recurringCharge
	 *
	 * @return \foreup\rest\models\entities\ForeupAccountRecurringCharges|null
	 */
	public function getRecurringCharge()
	{
		return $this->recurringCharge;
	}

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return ForeupInvoices
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set attemptLimit
     *
     * @param integer $attemptLimit
     *
     * @return ForeupInvoices
     */
    public function setAttemptLimit($attemptLimit)
    {
        $this->attemptLimit = $attemptLimit;
    
        return $this;
    }

    /**
     * Get attemptLimit
     *
     * @return integer
     */
    public function getAttemptLimit()
    {
        return $this->attemptLimit;
    }

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection|ForeupInvoiceItems[]
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @param ForeupInvoiceItems $item
	 */
	public function addItems($item)
	{
		$item->setInvoice($this);
		$this->items[] = $item;

		return $this;
	}
}
