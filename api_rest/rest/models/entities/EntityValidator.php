<?php
namespace foreup\rest\models\entities;

trait EntityValidator
{

	protected $last_error;

	public function testing_override($key,$value){
		$this->{$key} = $value;
	}

	public function getLastError(){
		return $this->last_error;
	}

	public function resetLastError(){
		$this->last_error = null;
	}

	protected function invalid($throw = true){
		if($throw)throw new \InvalidArgumentException($this->last_error);
		else return $this->last_error;
	}

	protected function validate_boolean($location,$name,$value,$required = false,$throw){
		if(is_bool($value)){
			// all good
		}
		elseif($required && !isset($value)){
			$this->last_error = $location.' Error: "'.$name.'" cannot be NULL';
			return $this->invalid($throw);
		}
		elseif (is_numeric($value)){
			if(!is_int($value*1) || $value > 1 || $value < 0){
				$this->last_error = $location.' Error: ambiguous use of numeric in boolean context: '.$value;
				return $this->invalid($throw);
			}
		}
		elseif (is_string($value)){
			$test = strtolower($value);
			if(!in_array($test,array('true','false'))){
				$this->last_error = $location.' Error: ambiguous use of string in boolean context: '.$value;
				return $this->invalid($throw);
			}
		}elseif(isset($value)){
			$this->last_error = $location.' Error: bad argument in boolean context: '.$value;
			return $this->invalid($throw);
		}
		return true;
	}

	protected function validate_integer($location,$name,$value,$required = false,$throw = true){
		if($required && !isset($value)){
			$this->last_error = $location.' Error: "'.$name.'" cannot be NULL';
			return $this->invalid($throw);
		}
		if(isset($value) && (!is_numeric($value) || !is_int($value*1))){
			$this->last_error = $location.' Error: "'.$name.'" must be an integer: '.$value;
			return $this->invalid($throw);
		}

		return true;
	}

	protected function validate_numeric($location,$name,$value,$required = false,$throw){
		if($required && !isset($value)){
			$this->last_error = $location.' Error: "'.$name.'" cannot be NULL';
			return $this->invalid($throw);
		}
		if(isset($value) && !is_numeric($value)){
			$this->last_error = $location.' Error: "'.$name.'" must be numeric: '.$value;
			return $this->invalid($throw);
		}

		return true;
	}

	protected function validate_string($location,$name,$value,$required = false,$throw){
		if($required && !isset($value)){
			$this->last_error = $location.' Error: "'.$name.'" cannot be NULL';
			return $this->invalid($throw);
		}
		if(isset($value) && !is_string($value)){
			$this->last_error = $location.' Error: "'.$name.'" must be a string: '.$value;
			return $this->invalid($throw);
		}

		return true;
	}

	protected function validate_enum($location,$name,$value,$options,$required = false,$throw){
		$valid = $this->validate_string($location,$name,$value,$required,$throw);
		if($valid!==true)return $valid;
		if(!isset($value)&&!$required)return true;
		if(!in_array($value,$options)) {
			$this->last_error = $location.' Error: "'.$name.'" value not found in enum values: '.$value;
			return $this->invalid($throw);
		}

		return true;
	}

	protected function validate_object($location,$name,$value,$type,$required = false, $throw){
		if($required && !isset($value)){
			$this->last_error = $location.' Error: "'.$name.'" cannot be NULL';
			return $this->invalid($throw);
		}
		if(isset($value) && !is_a($value, $type)){
			$this->last_error = $location.' Error: "'.$name.'" must be of type "'.$type.'": '.$value;
			return $this->invalid($throw);
		}
		return true;
	}

    protected function validate_array($location,$name,$value,$required = false, $throw){
        if($required && !isset($value)){
            $this->last_error = $location.' Error: "'.$name.'" cannot be NULL';
            return $this->invalid($throw);
        }
        if(isset($value) && !is_array($value)){
            $this->last_error = $location.' Error: "'.$name.'" must be an array: '.$value;
            return $this->invalid($throw);
        }
        return true;
    }
}