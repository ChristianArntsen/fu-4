<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupLoyaltyRates
 *
 * @ORM\Table(name="foreup_loyalty_rates", indexes={@ORM\Index(name="loyalty_package_id_fk", columns={"loyalty_package_id"})})
 * @ORM\Entity
 */
class ForeupLoyaltyRates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="loyalty_rate_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $courseId;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="value_label", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $valueLabel;

    /**
     * @var float
     *
     * @ORM\Column(name="points_per_dollar", type="float", precision=15, scale=2, nullable=false, unique=false)
     */
    private $pointsPerDollar;

    /**
     * @var float
     *
     * @ORM\Column(name="dollars_per_point", type="float", precision=15, scale=5, nullable=false, unique=false)
     */
    private $dollarsPerPoint;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tee_time_index", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $teeTimeIndex;

    /**
     * @var boolean
     *
     * @ORM\Column(name="price_category", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $priceCategory;

    /**
     * @var \foreup\rest\models\entities\ForeupLoyaltyPackages
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupLoyaltyPackages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="loyalty_package_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $loyaltyPackage;


    /**
     * Get loyaltyRateId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupLoyaltyRates
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;

        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return ForeupLoyaltyRates
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupLoyaltyRates
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ForeupLoyaltyRates
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set valueLabel
     *
     * @param string $valueLabel
     *
     * @return ForeupLoyaltyRates
     */
    public function setValueLabel($valueLabel)
    {
        $this->valueLabel = $valueLabel;

        return $this;
    }

    /**
     * Get valueLabel
     *
     * @return string
     */
    public function getValueLabel()
    {
        return $this->valueLabel;
    }

    /**
     * Set pointsPerDollar
     *
     * @param float $pointsPerDollar
     *
     * @return ForeupLoyaltyRates
     */
    public function setPointsPerDollar($pointsPerDollar)
    {
        $this->pointsPerDollar = $pointsPerDollar;

        return $this;
    }

    /**
     * Get pointsPerDollar
     *
     * @return float
     */
    public function getPointsPerDollar()
    {
        return $this->pointsPerDollar;
    }

    /**
     * Set dollarsPerPoint
     *
     * @param float $dollarsPerPoint
     *
     * @return ForeupLoyaltyRates
     */
    public function setDollarsPerPoint($dollarsPerPoint)
    {
        $this->dollarsPerPoint = $dollarsPerPoint;

        return $this;
    }

    /**
     * Get dollarsPerPoint
     *
     * @return float
     */
    public function getDollarsPerPoint()
    {
        return $this->dollarsPerPoint;
    }

    /**
     * Set teeTimeIndex
     *
     * @param boolean $teeTimeIndex
     *
     * @return ForeupLoyaltyRates
     */
    public function setTeeTimeIndex($teeTimeIndex)
    {
        $this->teeTimeIndex = $teeTimeIndex;

        return $this;
    }

    /**
     * Get teeTimeIndex
     *
     * @return boolean
     */
    public function getTeeTimeIndex()
    {
        return $this->teeTimeIndex;
    }

    /**
     * Set priceCategory
     *
     * @param boolean $priceCategory
     *
     * @return ForeupLoyaltyRates
     */
    public function setPriceCategory($priceCategory)
    {
        $this->priceCategory = $priceCategory;

        return $this;
    }

    /**
     * Get priceCategory
     *
     * @return boolean
     */
    public function getPriceCategory()
    {
        return $this->priceCategory;
    }

    /**
     * Set loyaltyPackage
     *
     * @param \foreup\rest\models\entities\ForeupLoyaltyPackages $loyaltyPackage
     *
     * @return ForeupLoyaltyRates
     */
    public function setLoyaltyPackage(\foreup\rest\models\entities\ForeupLoyaltyPackages $loyaltyPackage = null)
    {
        $this->loyaltyPackage = $loyaltyPackage;

        return $this;
    }

    /**
     * Get loyaltyPackage
     *
     * @return \foreup\rest\models\entities\ForeupLoyaltyPackages
     */
    public function getLoyaltyPackage()
    {
        return $this->loyaltyPackage;
    }
}

