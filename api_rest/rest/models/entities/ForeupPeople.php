<?php


namespace foreup\rest\models\entities;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupPeople
 *
 * @ORM\Table(name="foreup_people", indexes={@ORM\Index(name="email", columns={"email"}), @ORM\Index(name="person_orderby", columns={"last_name", "first_name", "person_id"}), @ORM\Index(name="first_name", columns={"first_name"}), @ORM\Index(name="last_name", columns={"last_name"})})
 * @ORM\Entity
 */
class ForeupPeople
{
	use \foreup\rest\models\entities\EntityValidator;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personId;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=255, nullable=false)
     */
    private $phoneNumber = "";

    /**
     * @var string
     *
     * @ORM\Column(name="cell_phone_number", type="string", length=255, nullable=false)
     */
    private $cellPhoneNumber = "";

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email = "";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="address_1", type="string", length=255, nullable=false)
     */
    private $address1 = "";

    /**
     * @var string
     *
     * @ORM\Column(name="address_2", type="string", length=255, nullable=false)
     */
    private $address2 = "";

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city = "";

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=false)
     */
    private $state = "";

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=255, nullable=false)
     */
    private $zip = "";

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=false)
     */
    private $country = "";

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", length=65535, nullable=false)
     */
    private $comments = "";

    /**
     * @var boolean
     *
     * @ORM\Column(name="foreup_news_announcements_unsubscribe", type="boolean", nullable=false)
     */
    private $foreupNewsAnnouncementsUnsubscribe = 0;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="handicap_account_number", type="string", length=50, nullable=true)
	 */
	private $handicapAccountNumber = "";

	/**
	 * @var string
	 *
	 * @ORM\Column(name="handicap_score", type="decimal", nullable=true)
	 */
	private $handicapScore = "";


    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * @param int $personId
     *
     * @return ForeupPersonAltered
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return ForeupPersonAltered
     */
    public function setFirstName($firstName)
    {
        $this->firstName =  trim($firstName);
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return ForeupPersonAltered
     */
    public function setLastName($lastName)
    {
        $this->lastName = trim($lastName);
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     *
     * @return ForeupPersonAltered
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = trim($phoneNumber);
        return $this;
    }

    /**
     * @return string
     */
    public function getCellPhoneNumber()
    {
        return $this->cellPhoneNumber;
    }

    /**
     * @param string $cellPhoneNumber
     *
     * @return ForeupPersonAltered
     */
    public function setCellPhoneNumber($cellPhoneNumber)
    {
        $this->cellPhoneNumber = trim($cellPhoneNumber);
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return ForeupPersonAltered
     */
    public function setEmail($email)
    {
        $this->email = trim($email);
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getBirthday()
    {
    	if(isset($this->birthday) && $this->birthday->getTimestamp() < 0)
    		return null;
        return $this->birthday;
    }

    /**
     * @param Carbon $birthday
     *
     * @return ForeupPersonAltered
     */
    public function setBirthday($birthday)
    {
    	if(is_string($birthday)){
    		$birthday = Carbon::parse($birthday);
	    }
        $this->birthday = $birthday;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param string $address1
     *
     * @return ForeupPersonAltered
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param string $address2
     *
     * @return ForeupPersonAltered
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return ForeupPersonAltered
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     *
     * @return ForeupPersonAltered
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     *
     * @return ForeupPersonAltered
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return ForeupPersonAltered
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     *
     * @return ForeupPersonAltered
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isForeupNewsAnnouncementsUnsubscribe()
    {
        return $this->foreupNewsAnnouncementsUnsubscribe;
    }

    /**
     * @param boolean $foreupNewsAnnouncementsUnsubscribe
     *
     * @return ForeupPersonAltered
     */
    public function setForeupNewsAnnouncementsUnsubscribe($foreupNewsAnnouncementsUnsubscribe)
    {
        $this->foreupNewsAnnouncementsUnsubscribe = $foreupNewsAnnouncementsUnsubscribe;
        return $this;
    }

	/**
	 * @return string
	 */
	public function getHandicapAccountNumber()
	{
		return $this->handicapAccountNumber;
	}

	/**
	 * @param string $handicapAccountNumber
	 */
	public function setHandicapAccountNumber(string $handicapAccountNumber)
	{
		$this->handicapAccountNumber = $handicapAccountNumber;
	}

	/**
	 * @return string
	 */
	public function getHandicapScore()
	{
		return $this->handicapScore;
	}

	/**
	 * @param string $handicapScore
	 */
	public function setHandicapScore(string $handicapScore)
	{
		$this->handicapScore = $handicapScore;
	}



}

