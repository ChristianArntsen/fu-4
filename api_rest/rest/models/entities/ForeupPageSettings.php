<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupPageSettings
 *
 * @ORM\Table(name="foreup_page_settings", indexes={@ORM\Index(name="FK_foreup_page_settings_foreup_courses", columns={"course_id"})})
 * @ORM\Entity
 */
class ForeupPageSettings
{
    private $validPageTypes = ['customers', 'billing', 'employees'];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="page", type="string", nullable=true)
     */
    private $page;

    /**
     * @var integer
     *
     * @ORM\Column(name="page_size", type="integer", nullable=true)
     */
    private $pageSize;

    /**
     * @var string
     *
     * @ORM\Column(name="columns", type="string", length=1000, nullable=true)
     */
    private $columns;

    /**
     * @var \foreup\rest\models\entities\ForeupCourses
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $course;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set page
     *
     * @param string $page
     *
     * @return ForeupPageSettings
     */
    public function setPage($page)
    {
    	if(!in_array($page, $this->validPageTypes)){
    		throw new \InvalidArgumentException("Invalid page type.");
	    }
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set pageSize
     *
     * @param integer $pageSize
     *
     * @return ForeupPageSettings
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    /**
     * Get pageSize
     *
     * @return integer
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * Set columns
     *
     * @param string $columns
     *
     * @return ForeupPageSettings
     */
    public function setColumns($columns)
    {
        $this->columns = json_encode($columns);

        return $this;
    }

    /**
     * Get columns
     *
     * @return string
     */
    public function getColumns()
    {
        return json_decode($this->columns);
    }

    /**
     * Set course
     *
     * @param \foreup\rest\models\entities\ForeupCourses $course
     *
     * @return ForeupPageSettings
     */
    public function setCourse(\foreup\rest\models\entities\ForeupCourses $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \foreup\rest\models\entities\ForeupCourses
     */
    public function getCourse()
    {
        return $this->course;
    }
}
