<?php


use foreup\rest\models\entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupCoursesInformationSetup
 *
 * @ORM\Table(name="foreup_courses_information_setup", indexes={@ORM\Index(name="sales_rep", columns={"setup_rep"})})
 * @ORM\Entity
 */
class ForeupCoursesInformationSetup
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_golive_goal", type="date", nullable=false)
     */
    private $dateGoliveGoal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_imports_completed", type="date", nullable=false)
     */
    private $dateImportsCompleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_training_completed", type="date", nullable=false)
     */
    private $dateTrainingCompleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_hardward_setup", type="date", nullable=false)
     */
    private $dateHardwardSetup;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_live", type="date", nullable=false)
     */
    private $dateLive;

    /**
     * @var \ForeupCourses
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $course;

    /**
     * @var \foreup\rest\models\entities\ForeupEmployees
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="setup_rep", referencedColumnName="person_id")
     * })
     */
    private $setupRep;


    /**
     * Set dateGoliveGoal
     *
     * @param \DateTime $dateGoliveGoal
     *
     * @return ForeupCoursesInformationSetup
     */
    public function setDateGoliveGoal($dateGoliveGoal)
    {
        $this->dateGoliveGoal = $dateGoliveGoal;

        return $this;
    }

    /**
     * Get dateGoliveGoal
     *
     * @return \DateTime
     */
    public function getDateGoliveGoal()
    {
        return $this->dateGoliveGoal;
    }

    /**
     * Set dateImportsCompleted
     *
     * @param \DateTime $dateImportsCompleted
     *
     * @return ForeupCoursesInformationSetup
     */
    public function setDateImportsCompleted($dateImportsCompleted)
    {
        $this->dateImportsCompleted = $dateImportsCompleted;

        return $this;
    }

    /**
     * Get dateImportsCompleted
     *
     * @return \DateTime
     */
    public function getDateImportsCompleted()
    {
        return $this->dateImportsCompleted;
    }

    /**
     * Set dateTrainingCompleted
     *
     * @param \DateTime $dateTrainingCompleted
     *
     * @return ForeupCoursesInformationSetup
     */
    public function setDateTrainingCompleted($dateTrainingCompleted)
    {
        $this->dateTrainingCompleted = $dateTrainingCompleted;

        return $this;
    }

    /**
     * Get dateTrainingCompleted
     *
     * @return \DateTime
     */
    public function getDateTrainingCompleted()
    {
        return $this->dateTrainingCompleted;
    }

    /**
     * Set dateHardwardSetup
     *
     * @param \DateTime $dateHardwardSetup
     *
     * @return ForeupCoursesInformationSetup
     */
    public function setDateHardwardSetup($dateHardwardSetup)
    {
        $this->dateHardwardSetup = $dateHardwardSetup;

        return $this;
    }

    /**
     * Get dateHardwardSetup
     *
     * @return \DateTime
     */
    public function getDateHardwardSetup()
    {
        return $this->dateHardwardSetup;
    }

    /**
     * Set dateLive
     *
     * @param \DateTime $dateLive
     *
     * @return ForeupCoursesInformationSetup
     */
    public function setDateLive($dateLive)
    {
        $this->dateLive = $dateLive;

        return $this;
    }

    /**
     * Get dateLive
     *
     * @return \DateTime
     */
    public function getDateLive()
    {
        return $this->dateLive;
    }

    /**
     * Set course
     *
     * @param \ForeupCourses $course
     *
     * @return ForeupCoursesInformationSetup
     */
    public function setCourse(\foreup\rest\models\entities\ForeupCourses $course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \ForeupCourses
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set setupRep
     *
     * @param \ForeupEmployees $setupRep
     *
     * @return ForeupCoursesInformationSetup
     */
    public function setSetupRep(\foreup\rest\models\entities\ForeupEmployees $setupRep = null)
    {
        $this->setupRep = $setupRep;

        return $this;
    }

    /**
     * Get setupRep
     *
     * @return \ForeupEmployees
     */
    public function getSetupRep()
    {
        return $this->setupRep;
    }
}

