<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountStatementCharges
 *
 * @ORM\Table(name="foreup_account_statement_charges", uniqueConstraints={@ORM\UniqueConstraint(name="statement_id_line", columns={"statement_id", "line"})}, indexes={@ORM\Index(name="statement_id", columns={"statement_id"}), @ORM\Index(name="item_id", columns={"item_id"}), @ORM\Index(name="sale_id", columns={"sale_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupAccountStatementCharges
{
	use \foreup\rest\models\entities\EntityValidator;

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{

		$this->resetLastError();
		$location = 'ForeupAccountStatementCharges->validate';

		$value = $this->getDateCharged();
		$v = $this->validate_object($location,'dateCharged',$value,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$value = $this->line;
		$v = $this->validate_integer($location,'line',$value,true,$throw);
		if($v!==true)return $v;

		$value = $this->name;
		$v = $this->validate_string($location,'name',$value,false,$throw);
		if($v!==true)return $v;

		$value = $this->getitem();
		$v = $this->validate_object($location,'item',$value,'foreup\rest\models\entities\ForeupItems',false,$throw);
		if($v !== true) return $v;

		$value = $this->getTotal();
		$v = $this->validate_numeric($location,'total',$value,true,$throw);
		if($v !== true) return $v;

		$value = $this->getSubtotal();
		$v = $this->validate_numeric($location,'subtotal',$value,true,$throw);
		if($v !== true) return $v;

		$value = $this->getTax();
		$v = $this->validate_numeric($location,'tax',$value,true,$throw);
		if($v !== true) return $v;

		$value = $this->getQty();
		$v = $this->validate_numeric($location,'qty',$value,true,$throw);
		if($v !== true) return $v;

		$value = $this->getUnitPrice();
		$v = $this->validate_numeric($location,'unitPrice',$value,true,$throw);
		if($v !== true) return $v;

		$value = $this->getTaxPercentage();
		$v = $this->validate_numeric($location,'taxPercentage',$value,true,$throw);
		if($v !== true) return $v;

		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

	/**
	 * @var \foreup\rest\models\entities\ForeupAccountStatements
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountStatements")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="statement_id", referencedColumnName="id")
	 * })
	 */
	private $statement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_charged", type="datetime", nullable=false)
     */
    private $dateCharged;

    /**
     * @var integer
     *
     * @ORM\Column(name="line", type="integer", nullable=false)
     */
    private $line;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=1024, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false)
     */
    private $itemId;

	/**
	 * @var \foreup\rest\models\entities\ForeupItems
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupItems")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="item_id", referencedColumnName="item_id")
	 * })
	 */
	private $item;

    /**
     * @var integer
     *
     * @ORM\Column(name="sale_id", type="integer", nullable=false)
     */
    private $saleId;

	/**
	 * @var \foreup\rest\models\entities\ForeupSales
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupSales")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="sale_id", referencedColumnName="sale_id")
	 * })
	 */
	private $sale;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $total = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="subtotal", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $subtotal = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="tax", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $tax = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="qty", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $qty = '1.00';

    /**
     * @var string
     *
     * @ORM\Column(name="unit_price", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $unitPrice = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="tax_percentage", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $taxPercentage = '0.00';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statement
     *
     * @param ForeupAccountStatements $statement
     *
     * @return ForeupAccountStatementCharges
     */
    public function setStatement($statement)
    {
        $this->statement = $statement;
    
        return $this;
    }

    /**
     * Get statement
     *
     * @return ForeupAccountStatements
     */
    public function getStatement()
    {
        return $this->statement;
    }

    /**
     * Set dateCharged
     *
     * @param \DateTime $dateCharged
     *
     * @return ForeupAccountStatementCharges
     */
    public function setDateCharged($dateCharged)
    {
        $this->dateCharged = $dateCharged;
    
        return $this;
    }

    /**
     * Get dateCharged
     *
     * @return \DateTime
     */
    public function getDateCharged()
    {
        return $this->dateCharged;
    }

    /**
     * Set line
     *
     * @param integer $line
     *
     * @return ForeupAccountStatementCharges
     */
    public function setLine($line)
    {
        $this->line = $line;
    
        return $this;
    }

    /**
     * Get line
     *
     * @return integer
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupAccountStatementCharges
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get itemId
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->itemId;
    }

	/**
	 * Set invoice
	 *
	 * @param \foreup\rest\models\entities\ForeupItems|null $item
	 *
	 * @return ForeupAccountStatementCharges
	 */
	public function setItem(\foreup\rest\models\entities\ForeupItems $item = null)
	{
		$this->item = $item;
		if(method_exists($item,'getItemId')){
			$itemId = $item->getItemId();
			$this->itemId = isset($itemId)?$itemId:0;
		}

		return $this;
	}

	/**
	 * Get invoice
	 *
	 * @return \foreup\rest\models\entities\ForeupItems
	 */
	public function getItem()
	{
		return $this->item;
	}

    /**
     * Get saleId
     *
     * @return integer
     */
    public function getSaleId()
    {
        return $this->saleId;
    }

	/**
	 * Set sale
	 *
	 * @param \foreup\rest\models\entities\ForeupEmployees $sale
	 *
	 * @return ForeupAccountStatementCharges
	 */
	public function setSale(\foreup\rest\models\entities\ForeupSales $sale = null)
	{
		$this->sale = $sale;
		if(method_exists($sale,'getSaleId')){
			$saleId = $sale->getSaleId();
			$this->saleId = isset($saleId)?$saleId:0;
		}

		return $this;
	}

	/**
	 * Get sale
	 *
	 * @return \foreup\rest\models\entities\ForeupSales
	 */
	public function getSale()
	{
		return $this->sale;
	}

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ForeupAccountStatementCharges
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set subtotal
     *
     * @param string $subtotal
     *
     * @return ForeupAccountStatementCharges
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    
        return $this;
    }

    /**
     * Get subtotal
     *
     * @return string
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * Set tax
     *
     * @param string $tax
     *
     * @return ForeupAccountStatementCharges
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    
        return $this;
    }

    /**
     * Get tax
     *
     * @return string
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set qty
     *
     * @param string $qty
     *
     * @return ForeupAccountStatementCharges
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
    
        return $this;
    }

    /**
     * Get qty
     *
     * @return string
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set unitPrice
     *
     * @param string $unitPrice
     *
     * @return ForeupAccountStatementCharges
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    
        return $this;
    }

    /**
     * Get unitPrice
     *
     * @return string
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Set taxPercentage
     *
     * @param string $taxPercentage
     *
     * @return ForeupAccountStatementCharges
     */
    public function setTaxPercentage($taxPercentage)
    {
        $this->taxPercentage = $taxPercentage;
    
        return $this;
    }

    /**
     * Get taxPercentage
     *
     * @return string
     */
    public function getTaxPercentage()
    {
        return $this->taxPercentage;
    }
}
