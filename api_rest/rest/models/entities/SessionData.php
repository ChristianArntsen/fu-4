<?php


use Doctrine\ORM\Mapping as ORM;

/**
 * SessionData
 *
 * @ORM\Table(name="session_data")
 * @ORM\Entity
 */
class SessionData
{
    /**
     * @var string
     *
     * @ORM\Column(name="session_id", type="string", length=32, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sessionId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=32, nullable=false)
     */
    private $hash = '';

    /**
     * @var string
     *
     * @ORM\Column(name="session_data", type="blob", length=65535, nullable=false)
     */
    private $sessionData;

    /**
     * @var integer
     *
     * @ORM\Column(name="session_expire", type="integer", nullable=false)
     */
    private $sessionExpire = '0';


    /**
     * Get sessionId
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return SessionData
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set sessionData
     *
     * @param string $sessionData
     *
     * @return SessionData
     */
    public function setSessionData($sessionData)
    {
        $this->sessionData = $sessionData;

        return $this;
    }

    /**
     * Get sessionData
     *
     * @return string
     */
    public function getSessionData()
    {
        return $this->sessionData;
    }

    /**
     * Set sessionExpire
     *
     * @param integer $sessionExpire
     *
     * @return SessionData
     */
    public function setSessionExpire($sessionExpire)
    {
        $this->sessionExpire = $sessionExpire;

        return $this;
    }

    /**
     * Get sessionExpire
     *
     * @return integer
     */
    public function getSessionExpire()
    {
        return $this->sessionExpire;
    }
}

