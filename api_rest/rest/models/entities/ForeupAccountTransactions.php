<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountTransactions
 *
 * @ORM\Table(name="foreup_account_transactions", indexes={@ORM\Index(name="foreup_account_transactions_ibfk_1", columns={"trans_customer"}), @ORM\Index(name="foreup_account_transactions_ibfk_2", columns={"trans_user"}), @ORM\Index(name="sale_id", columns={"sale_id"}), @ORM\Index(name="invoice_id", columns={"invoice_id"}), @ORM\Index(name="minimum_charge_id", columns={"minimum_charge_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupAccountTransactions
{
	use \foreup\rest\models\entities\EntityValidator;

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{
		$this->resetLastError();

		$location = 'ForeupAccountTransactions->validate';

		$accountType = $this->getAccountType();
		$v = $this->validate_string($location,'accountType',$accountType,true,$throw);
		if($v!==true)return $v;

		$saleId = $this->getSaleId();
		$v = $this->validate_integer($location,'saleId',$saleId,true,$throw);
		if($v!==true)return $v;

		$invoiceId = $this->getInvoiceId();
		$v = $this->validate_integer($location,'invoiceId',$invoiceId,true,$throw);
		if($v!==true)return $v;

		$courseId = $this->getCourseId();
		$v = $this->validate_integer($location,'courseId',$courseId,true,$throw);
		if($v!==true)return $v;

		$transHousehold = $this->getTransHousehold();
		$v = $this->validate_integer($location,'transHousehold',$transHousehold,true,$throw);
		if($v!==true)return $v;

		$transUser = $this->getTransUser();
		$v = $this->validate_integer($location,'transUser',$transUser,false,$throw);
		if($v!==true)return $v;

		$transDate = $this->getTransDate(); // defaults to current timestamp
		$v = $this->validate_object($location,'transDate',$transDate,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$transComment = $this->getTransComment();
		$v = $this->validate_string($location,'transComment',$transComment,true,$throw);
		if($v!==true)return $v;

		$transDescription = $this->getTransDescription();
		$v = $this->validate_string($location,'transDescription',$transDescription,true,$throw);
		if($v!==true)return $v;

		$transAmount = $this->getTransAmount(); // defaults to 0.00
		$v = $this->validate_numeric($location,'transAmount',$transAmount,false,$throw);
		if($v!==true)return $v;

		$runningBalance = $this->getRunningBalance();
		$v = $this->validate_numeric($location,'runningBalance',$runningBalance,false,$throw);
		if($v!==true)return $v;

		$hideOnInvoices = $this->getHideOnInvoices();
		$v = $this->validate_boolean($location,'hideOnInvoices',$hideOnInvoices,true,$throw);
		if($v!==true)return $v;

		$minimumChargeId = $this->getMinimumChargeId();
		$v = $this->validate_integer($location,'minimumChargeId',$minimumChargeId,false,$throw);
		if($v!==true)return $v;

		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="trans_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $transId;

    /**
     * @var string
     *
     * @ORM\Column(name="account_type", type="string", length=24, precision=0, scale=0, nullable=true, unique=false)
     */
    private $accountType;

    /**
     * @var integer
     *
     * @ORM\Column(name="sale_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $saleId;

	/**
	 * @var \foreup\rest\models\entities\ForeupSales
	 *
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupSales", inversedBy="items")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="sale_id", referencedColumnName="sale_id")
	 * })
	 */
	private $sale;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $invoiceId = '0';

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="course_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $courseId;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="trans_customer", type="integer", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $transCustomer;

	/**
	 * @var \foreup\rest\models\entities\ForeupCustomers
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCustomers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id"),
     *   @ORM\JoinColumn(name="trans_customer", referencedColumnName="person_id")
	 * })
	 */
	private $customer;

    /**
     * @var integer
     *
     * @ORM\Column(name="trans_household", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $transHousehold;

    /**
     * @var integer
     *
     * @ORM\Column(name="trans_user", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $transUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="trans_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $transDate;

    /**
     * @var string
     *
     * @ORM\Column(name="trans_comment", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $transComment;

    /**
     * @var string
     *
     * @ORM\Column(name="trans_description", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $transDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="trans_amount", type="decimal", precision=15, scale=2, nullable=false, unique=false)
     */
    private $transAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="running_balance", type="decimal", precision=15, scale=2, nullable=true, unique=false)
     */
    private $runningBalance;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide_on_invoices", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $hideOnInvoices;

    /**
     * @var integer
     *
     * @ORM\Column(name="minimum_charge_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $minimumChargeId;

    /**
     * Get transId
     *
     * @return integer
     */
    public function getTransId()
    {
        return $this->transId;
    }

    /**
     * Set accountType
     *
     * @param string $accountType
     *
     * @return ForeupAccountTransactions
     */
    public function setAccountType($accountType)
    {
        $this->accountType = $accountType;
    
        return $this;
    }

    /**
     * Get accountType
     *
     * @return string
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * Get saleId
     *
     * @return integer
     */
    public function getSaleId()
    {
        return $this->saleId;
    }

    public function setSale(ForeupSales $sale)
	{
		if($sale->getSaleId())$this->saleId = $sale->getSaleId();

		$this->sale = $sale;
	}

    public function getSale()
    {
    	return $this->sale;
    }

    /**
     * Set invoiceId
     *
     * @param integer $invoiceId
     *
     * @return ForeupAccountTransactions
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
    
        return $this;
    }

    /**
     * Get invoiceId
     *
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupAccountTransactions
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    
        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

	/**
	 * Set customer
	 *
	 * @param \foreup\rest\models\entities\ForeupCustomers $customer
	 *
	 * @return ForeupAccountTransactions
	 */
	public function setCustomer(ForeupCustomers $customer = null)
	{
		$this->customer = $customer;
		$this->courseId = $customer->getCourseId();
		$this->transCustomer = $customer->getPersonId();

		return $this;
	}

	/**
	 * Get customer
	 *
	 * @return \foreup\rest\models\entities\ForeupCustomers
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

    /**
     * Set transHousehold
     *
     * @param integer $transHousehold
     *
     * @return ForeupAccountTransactions
     */
    public function setTransHousehold($transHousehold)
    {
        $this->transHousehold = $transHousehold;
    
        return $this;
    }

    /**
     * Get transHousehold
     *
     * @return integer
     */
    public function getTransHousehold()
    {
        return $this->transHousehold;
    }

    /**
     * Set transUser
     *
     * @param integer $transUser
     *
     * @return ForeupAccountTransactions
     */
    public function setTransUser($transUser)
    {
        $this->transUser = $transUser;
    
        return $this;
    }

    /**
     * Get transUser
     *
     * @return integer
     */
    public function getTransUser()
    {
        return $this->transUser;
    }

    /**
     * Set transDate
     *
     * @param \DateTime $transDate
     *
     * @return ForeupAccountTransactions
     */
    public function setTransDate($transDate)
    {
        $this->transDate = $transDate;
    
        return $this;
    }

    /**
     * Get transDate
     *
     * @return \DateTime
     */
    public function getTransDate()
    {
        return $this->transDate;
    }

    /**
     * Set transComment
     *
     * @param string $transComment
     *
     * @return ForeupAccountTransactions
     */
    public function setTransComment($transComment)
    {
        $this->transComment = $transComment;
    
        return $this;
    }

    /**
     * Get transComment
     *
     * @return string
     */
    public function getTransComment()
    {
        return $this->transComment;
    }

    /**
     * Set transDescription
     *
     * @param string $transDescription
     *
     * @return ForeupAccountTransactions
     */
    public function setTransDescription($transDescription)
    {
        $this->transDescription = $transDescription;
    
        return $this;
    }

    /**
     * Get transDescription
     *
     * @return string
     */
    public function getTransDescription()
    {
        return $this->transDescription;
    }

    /**
     * Set transAmount
     *
     * @param string $transAmount
     *
     * @return ForeupAccountTransactions
     */
    public function setTransAmount($transAmount)
    {
        $this->transAmount = $transAmount;
    
        return $this;
    }

    /**
     * Get transAmount
     *
     * @return string
     */
    public function getTransAmount()
    {
        return $this->transAmount;
    }

    /**
     * Set runningBalance
     *
     * @param string $runningBalance
     *
     * @return ForeupAccountTransactions
     */
    public function setRunningBalance($runningBalance)
    {
        $this->runningBalance = $runningBalance;
    
        return $this;
    }

    /**
     * Get runningBalance
     *
     * @return string
     */
    public function getRunningBalance()
    {
        return $this->runningBalance;
    }

    /**
     * Set hideOnInvoices
     *
     * @param boolean $hideOnInvoices
     *
     * @return ForeupAccountTransactions
     */
    public function setHideOnInvoices($hideOnInvoices)
    {
	    if(is_string($hideOnInvoices)){
            $hideOnInvoices = strtolower($hideOnInvoices);
		    $ac = json_decode($hideOnInvoices,true);
		    if(isset($ac))$hideOnInvoices = $ac;
	    }
        $this->hideOnInvoices = $hideOnInvoices;
    
        return $this;
    }

    /**
     * Get hideOnInvoices
     *
     * @return boolean
     */
    public function getHideOnInvoices()
    {
        return $this->hideOnInvoices;
    }

    /**
     * Set minimumChargeId
     *
     * @param integer $minimumChargeId
     *
     * @return ForeupAccountTransactions
     */
    public function setMinimumChargeId($minimumChargeId)
    {
        $this->minimumChargeId = $minimumChargeId;
    
        return $this;
    }

    /**
     * Get minimumChargeId
     *
     * @return integer
     */
    public function getMinimumChargeId()
    {
        return $this->minimumChargeId;
    }
}

