<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupTeesheet
 *
 * @ORM\Table(name="foreup_teesheet")
 * @ORM\Entity
 */
class ForeupTeesheet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="teesheet_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CID", type="integer", nullable=false)
     */
    private $cid;

    /**
     * @var string
     *
     * @ORM\Column(name="TSID", type="string", length=20, nullable=false)
     */
    private $tsid;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="associated_courses", type="text", length=65535, nullable=false)
     */
    private $associatedCourses;

    /**
     * @var boolean
     *
     * @ORM\Column(name="holes", type="boolean", nullable=false)
     */
    private $holes;

    /**
     * @var string
     *
     * @ORM\Column(name="online_open_time", type="string", length=10, nullable=false)
     */
    private $onlineOpenTime = '0900';

    /**
     * @var string
     *
     * @ORM\Column(name="online_close_time", type="string", length=10, nullable=false)
     */
    private $onlineCloseTime = '1800';

    /**
     * @var integer
     *
     * @ORM\Column(name="days_out", type="smallint", nullable=false)
     */
    private $daysOut;

    /**
     * @var integer
     *
     * @ORM\Column(name="days_in_booking_window", type="smallint", nullable=false)
     */
    private $daysInBookingWindow;

    /**
     * @var boolean
     *
     * @ORM\Column(name="minimum_players", type="boolean", nullable=false)
     */
    private $minimumPlayers = '2';

    /**
     * @var boolean
     *
     * @ORM\Column(name="limit_holes", type="boolean", nullable=false)
     */
    private $limitHoles = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="booking_carts", type="boolean", nullable=false)
     */
    private $bookingCarts;

    /**
     * @var string
     *
     * @ORM\Column(name="increment", type="decimal", precision=4, scale=1, nullable=false)
     */
    private $increment = '8.0';

    /**
     * @var integer
     *
     * @ORM\Column(name="frontnine", type="smallint", nullable=false)
     */
    private $frontnine = '200';

    /**
     * @var integer
     *
     * @ORM\Column(name="fntime", type="smallint", nullable=false)
     */
    private $fntime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="default", type="boolean", nullable=false)
     */
    private $default;

    /**
     * @var string
     *
     * @ORM\Column(name="default_price_class", type="string", length=30, nullable=false)
     */
    private $defaultPriceClass;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=7, nullable=false)
     */
    private $color;

    /**
     * @var boolean
     *
     * @ORM\Column(name="online_booking", type="boolean", nullable=false)
     */
    private $onlineBooking;

    /**
     * @var boolean
     *
     * @ORM\Column(name="online_rate_setting", type="boolean", nullable=false)
     */
    private $onlineRateSetting;

    /**
     * @var boolean
     *
     * @ORM\Column(name="require_credit_card", type="boolean", nullable=false)
     */
    private $requireCreditCard;

    /**
     * @var boolean
     *
     * @ORM\Column(name="send_thank_you", type="boolean", nullable=false)
     */
    private $sendThankYou;

    /**
     * @var integer
     *
     * @ORM\Column(name="thank_you_campaign_id", type="integer", nullable=false)
     */
    private $thankYouCampaignId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="thank_you_next_send", type="datetime", nullable=false)
     */
    private $thankYouNextSend;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="specials_enabled", type="boolean", nullable=false)
     */
    private $specialsEnabled;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide_online_prices", type="boolean", nullable=false)
     */
    private $hideOnlinePrices = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_on_aggregate", type="boolean", nullable=false)
     */
    private $showOnAggregate = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="pay_online", type="boolean", nullable=false)
     */
    private $payOnline = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="block_online_booking", type="boolean", nullable=false)
     */
    private $blockOnlineBooking = '0';

    /**
     * @ORM\ManyToOne(targetEntity="ForeupCourses", inversedBy="teeSheets")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     */
    private $course;

    /**
     * @ORM\OneToMany(targetEntity="ForeupTeesheetHoles", mappedBy="teeSheet")
     *
     */
    private $teeSheetHoles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teeSheetHoles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get teeSheetHoles
     *
     * @return collection
     */
    public function getHoleList()
    {
        return $this->teeSheetHoles;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupTeesheet
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;

        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

	/**
	 * @return mixed
	 */
	public function getCourse()
	{
		return $this->course;
	}

	/**
	 * @param mixed $course
	 */
	public function setCourse($course)
	{
		$this->course = $course;
	}



    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return ForeupTeesheet
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set tsid
     *
     * @param string $tsid
     *
     * @return ForeupTeesheet
     */
    public function setTsid($tsid)
    {
        $this->tsid = $tsid;

        return $this;
    }

    /**
     * Get tsid
     *
     * @return string
     */
    public function getTsid()
    {
        return $this->tsid;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ForeupTeesheet
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set associatedCourses
     *
     * @param string $associatedCourses
     *
     * @return ForeupTeesheet
     */
    public function setAssociatedCourses($associatedCourses)
    {
        $this->associatedCourses = $associatedCourses;

        return $this;
    }

    /**
     * Get associatedCourses
     *
     * @return string
     */
    public function getAssociatedCourses()
    {
        return $this->associatedCourses;
    }

    /**
     * Set holes
     *
     * @param boolean $holes
     *
     * @return ForeupTeesheet
     */
    public function setHoles($holes)
    {
        $this->holes = $holes;

        return $this;
    }

    /**
     * Get holes
     *
     * @return boolean
     */
    public function getHoles()
    {
        return $this->holes;
    }

    /**
     * Set onlineOpenTime
     *
     * @param string $onlineOpenTime
     *
     * @return ForeupTeesheet
     */
    public function setOnlineOpenTime($onlineOpenTime)
    {
        $this->onlineOpenTime = $onlineOpenTime;

        return $this;
    }

    /**
     * Get onlineOpenTime
     *
     * @return string
     */
    public function getOnlineOpenTime()
    {
        return $this->onlineOpenTime;
    }

    /**
     * Set onlineCloseTime
     *
     * @param string $onlineCloseTime
     *
     * @return ForeupTeesheet
     */
    public function setOnlineCloseTime($onlineCloseTime)
    {
        $this->onlineCloseTime = $onlineCloseTime;

        return $this;
    }

    /**
     * Get onlineCloseTime
     *
     * @return string
     */
    public function getOnlineCloseTime()
    {
        return $this->onlineCloseTime;
    }

    /**
     * Set daysOut
     *
     * @param integer $daysOut
     *
     * @return ForeupTeesheet
     */
    public function setDaysOut($daysOut)
    {
        $this->daysOut = $daysOut;

        return $this;
    }

    /**
     * Get daysOut
     *
     * @return integer
     */
    public function getDaysOut()
    {
        return $this->daysOut;
    }

    /**
     * Set daysInBookingWindow
     *
     * @param integer $daysInBookingWindow
     *
     * @return ForeupTeesheet
     */
    public function setDaysInBookingWindow($daysInBookingWindow)
    {
        $this->daysInBookingWindow = $daysInBookingWindow;

        return $this;
    }

    /**
     * Get daysInBookingWindow
     *
     * @return integer
     */
    public function getDaysInBookingWindow()
    {
        return $this->daysInBookingWindow;
    }

    /**
     * Set minimumPlayers
     *
     * @param boolean $minimumPlayers
     *
     * @return ForeupTeesheet
     */
    public function setMinimumPlayers($minimumPlayers)
    {
        $this->minimumPlayers = $minimumPlayers;

        return $this;
    }

    /**
     * Get minimumPlayers
     *
     * @return boolean
     */
    public function getMinimumPlayers()
    {
        return $this->minimumPlayers;
    }

    /**
     * Set limitHoles
     *
     * @param boolean $limitHoles
     *
     * @return ForeupTeesheet
     */
    public function setLimitHoles($limitHoles)
    {
        $this->limitHoles = $limitHoles;

        return $this;
    }

    /**
     * Get limitHoles
     *
     * @return boolean
     */
    public function getLimitHoles()
    {
        return $this->limitHoles;
    }

    /**
     * Set bookingCarts
     *
     * @param boolean $bookingCarts
     *
     * @return ForeupTeesheet
     */
    public function setBookingCarts($bookingCarts)
    {
        $this->bookingCarts = $bookingCarts;

        return $this;
    }

    /**
     * Get bookingCarts
     *
     * @return boolean
     */
    public function getBookingCarts()
    {
        return $this->bookingCarts;
    }

    /**
     * Set increment
     *
     * @param string $increment
     *
     * @return ForeupTeesheet
     */
    public function setIncrement($increment)
    {
        $this->increment = $increment;

        return $this;
    }

    /**
     * Get increment
     *
     * @return string
     */
    public function getIncrement()
    {
        return $this->increment;
    }

    /**
     * Set frontnine
     *
     * @param integer $frontnine
     *
     * @return ForeupTeesheet
     */
    public function setFrontnine($frontnine)
    {
        $this->frontnine = $frontnine;

        return $this;
    }

    /**
     * Get frontnine
     *
     * @return integer
     */
    public function getFrontnine()
    {
        return $this->frontnine;
    }

    /**
     * Set fntime
     *
     * @param integer $fntime
     *
     * @return ForeupTeesheet
     */
    public function setFntime($fntime)
    {
        $this->fntime = $fntime;

        return $this;
    }

    /**
     * Get fntime
     *
     * @return integer
     */
    public function getFntime()
    {
        return $this->fntime;
    }

    /**
     * Set default
     *
     * @param boolean $default
     *
     * @return ForeupTeesheet
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return boolean
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set defaultPriceClass
     *
     * @param string $defaultPriceClass
     *
     * @return ForeupTeesheet
     */
    public function setDefaultPriceClass($defaultPriceClass)
    {
        $this->defaultPriceClass = $defaultPriceClass;

        return $this;
    }

    /**
     * Get defaultPriceClass
     *
     * @return string
     */
    public function getDefaultPriceClass()
    {
        return $this->defaultPriceClass;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return ForeupTeesheet
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set onlineBooking
     *
     * @param boolean $onlineBooking
     *
     * @return ForeupTeesheet
     */
    public function setOnlineBooking($onlineBooking)
    {
        $this->onlineBooking = $onlineBooking;

        return $this;
    }

    /**
     * Get onlineBooking
     *
     * @return boolean
     */
    public function getOnlineBooking()
    {
        return $this->onlineBooking;
    }

    /**
     * Set onlineRateSetting
     *
     * @param boolean $onlineRateSetting
     *
     * @return ForeupTeesheet
     */
    public function setOnlineRateSetting($onlineRateSetting)
    {
        $this->onlineRateSetting = $onlineRateSetting;

        return $this;
    }

    /**
     * Get onlineRateSetting
     *
     * @return boolean
     */
    public function getOnlineRateSetting()
    {
        return $this->onlineRateSetting;
    }

    /**
     * Set requireCreditCard
     *
     * @param boolean $requireCreditCard
     *
     * @return ForeupTeesheet
     */
    public function setRequireCreditCard($requireCreditCard)
    {
        $this->requireCreditCard = $requireCreditCard;

        return $this;
    }

    /**
     * Get requireCreditCard
     *
     * @return boolean
     */
    public function getRequireCreditCard()
    {
        return $this->requireCreditCard;
    }

    /**
     * Set sendThankYou
     *
     * @param boolean $sendThankYou
     *
     * @return ForeupTeesheet
     */
    public function setSendThankYou($sendThankYou)
    {
        $this->sendThankYou = $sendThankYou;

        return $this;
    }

    /**
     * Get sendThankYou
     *
     * @return boolean
     */
    public function getSendThankYou()
    {
        return $this->sendThankYou;
    }

    /**
     * Set thankYouCampaignId
     *
     * @param integer $thankYouCampaignId
     *
     * @return ForeupTeesheet
     */
    public function setThankYouCampaignId($thankYouCampaignId)
    {
        $this->thankYouCampaignId = $thankYouCampaignId;

        return $this;
    }

    /**
     * Get thankYouCampaignId
     *
     * @return integer
     */
    public function getThankYouCampaignId()
    {
        return $this->thankYouCampaignId;
    }

    /**
     * Set thankYouNextSend
     *
     * @param \DateTime $thankYouNextSend
     *
     * @return ForeupTeesheet
     */
    public function setThankYouNextSend($thankYouNextSend)
    {
        $this->thankYouNextSend = $thankYouNextSend;

        return $this;
    }

    /**
     * Get thankYouNextSend
     *
     * @return \DateTime
     */
    public function getThankYouNextSend()
    {
        return $this->thankYouNextSend;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ForeupTeesheet
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set specialsEnabled
     *
     * @param boolean $specialsEnabled
     *
     * @return ForeupTeesheet
     */
    public function setSpecialsEnabled($specialsEnabled)
    {
        $this->specialsEnabled = $specialsEnabled;

        return $this;
    }

    /**
     * Get specialsEnabled
     *
     * @return boolean
     */
    public function getSpecialsEnabled()
    {
        return $this->specialsEnabled;
    }

    /**
     * Set hideOnlinePrices
     *
     * @param boolean $hideOnlinePrices
     *
     * @return ForeupTeesheet
     */
    public function setHideOnlinePrices($hideOnlinePrices)
    {
        $this->hideOnlinePrices = $hideOnlinePrices;

        return $this;
    }

    /**
     * Get hideOnlinePrices
     *
     * @return boolean
     */
    public function getHideOnlinePrices()
    {
        return $this->hideOnlinePrices;
    }

    /**
     * Set showOnAggregate
     *
     * @param boolean $showOnAggregate
     *
     * @return ForeupTeesheet
     */
    public function setShowOnAggregate($showOnAggregate)
    {
        $this->showOnAggregate = $showOnAggregate;

        return $this;
    }

    /**
     * Get showOnAggregate
     *
     * @return boolean
     */
    public function getShowOnAggregate()
    {
        return $this->showOnAggregate;
    }

    /**
     * Set payOnline
     *
     * @param boolean $payOnline
     *
     * @return ForeupTeesheet
     */
    public function setPayOnline($payOnline)
    {
        $this->payOnline = $payOnline;

        return $this;
    }

    /**
     * Get payOnline
     *
     * @return boolean
     */
    public function getPayOnline()
    {
        return $this->payOnline;
    }

    /**
     * Set blockOnlineBooking
     *
     * @param boolean $blockOnlineBooking
     *
     * @return ForeupTeesheet
     */
    public function setBlockOnlineBooking($blockOnlineBooking)
    {
        $this->blockOnlineBooking = $blockOnlineBooking;

        return $this;
    }

    /**
     * Get blockOnlineBooking
     *
     * @return boolean
     */
    public function getBlockOnlineBooking()
    {
        return $this->blockOnlineBooking;
    }
}
