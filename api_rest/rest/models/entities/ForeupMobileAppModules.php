<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupMobileAppModules
 *
 * @ORM\Table(name="foreup_mobile_app_modules")
 * @ORM\Entity
 */
class ForeupMobileAppModules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mobile_app_module_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $mobileAppModuleId;

    /**
     * @ORM\ManyToMany(targetEntity="ForeupCourses", mappedBy="modules")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     */
    private $course;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var smallint
     *
     * @ORM\Column(name="position", type="smallint", nullable=false)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="icon_url", type="string", length=255, nullable=false)
     */
    private $iconUrl;

    private $parentCourse;

    /**
     * Get mobileAppModuleId
     *
     * @return integer
     */
    public function getMobileAppModuleId()
    {
        return $this->mobileAppModuleId;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return ForeupMobileAppModules
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return ForeupMobileAppModules
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set position
     *
     * @param smallint $position
     *
     * @return ForeupMobileAppModules
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return smallint
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set iconUrl
     *
     * @param string $iconUrl
     *
     * @return ForeupMobileAppModules
     */
    public function setIconUrl($iconUrl)
    {
        $this->iconUrl = $iconUrl;

        return $this;
    }

    /**
     * Get iconUrl
     *
     * @return string
     */
    public function getIconUrl()
    {
        return $this->iconUrl;
    }

    public function setParentCourse($course)
    {
        $this->parentCourse = $course;
        return $this;
    }
    public function getParentCourse()
    {
        return $this->parentCourse;
    }

    public function getRenderedUrl()
    {
        $url = $this->getUrl();
        if($this->getParentCourse() != null){
            $url = preg_replace('/\{\{course_id\}\}/',$this->getParentCourse()->getCourseId(),$url);
        }


        return $url;
    }

}
