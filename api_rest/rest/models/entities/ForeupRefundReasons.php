<?php
namespace foreup\rest\models\entities;



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupRefundReasons
 *
 * @ORM\Table(name="foreup_refund_reasons", indexes={@ORM\Index(name="course_id", columns={"course_id"})})
 * @ORM\Entity
 */
class ForeupRefundReasons
{


	/**
	 * ForeupRefundReasons constructor.
	 * @param ForeupCourses $course
	 */
	public function __construct(ForeupCourses $course)
	{
		$this->course = $course;
		$this->label = "";
	}


	/**
     * @var integer
     *
     * @ORM\Column(name="reason_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

	/**
	 * @var \foreup\rest\models\entities\ForeupCourses
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $course;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id)
	{
		$this->id = $id;
	}

	/**
	 * @return ForeupCourses
	 */
	public function getCourse(): ForeupCourses
	{
		return $this->course;
	}

	/**
	 * @param ForeupCourses $course
	 */
	public function setCourse(ForeupCourses $course)
	{
		$this->course = $course;
	}

	/**
	 * @return string
	 */
	public function getLabel(): string
	{
		return $this->label;
	}

	/**
	 * @param string $label
	 */
	public function setLabel(string $label)
	{
		$this->label = $label;
	}



}

