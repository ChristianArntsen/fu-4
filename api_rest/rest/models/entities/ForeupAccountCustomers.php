<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountCustomers
 *
 * @ORM\Table(name="foreup_account_customers", indexes={@ORM\Index(name="organization_id", columns={"organization_id"}), @ORM\Index(name="fk_account_customers_account_id_idx", columns={"account_id"})})
 * @ORM\Entity
 */
class ForeupAccountCustomers
{


    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $personId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_added", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dateAdded;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_owner", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isOwner;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $organizationId;

    /**
     * @var string
     *
     * @ORM\Column(name="limit", type="decimal", precision=15, scale=2, nullable=true, unique=false)
     */
    private $limit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_negative", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $allowNegative;

    /**
     * @var \foreup\rest\models\entities\ForeupAccounts
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupAccounts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $account;


    /**
     * Set personId
     *
     * @param integer $personId
     *
     * @return ForeupAccountCustomers
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    
        return $this;
    }

    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return ForeupAccountCustomers
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
    
        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set isOwner
     *
     * @param boolean $isOwner
     *
     * @return ForeupAccountCustomers
     */
    public function setIsOwner($isOwner)
    {
        $this->isOwner = $isOwner;
    
        return $this;
    }

    /**
     * Get isOwner
     *
     * @return boolean
     */
    public function getIsOwner()
    {
        return $this->isOwner;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     *
     * @return ForeupAccountCustomers
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set limit
     *
     * @param string $limit
     *
     * @return ForeupAccountCustomers
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    
        return $this;
    }

    /**
     * Get limit
     *
     * @return string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set allowNegative
     *
     * @param boolean $allowNegative
     *
     * @return ForeupAccountCustomers
     */
    public function setAllowNegative($allowNegative)
    {
        $this->allowNegative = $allowNegative;
    
        return $this;
    }

    /**
     * Get allowNegative
     *
     * @return boolean
     */
    public function getAllowNegative()
    {
        return $this->allowNegative;
    }

    /**
     * Set account
     *
     * @param \foreup\rest\models\entities\ForeupAccounts $account
     *
     * @return ForeupAccountCustomers
     */
    public function setAccount(\foreup\rest\models\entities\ForeupAccounts $account)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \foreup\rest\models\entities\ForeupAccounts
     */
    public function getAccount()
    {
        return $this->account;
    }
}

