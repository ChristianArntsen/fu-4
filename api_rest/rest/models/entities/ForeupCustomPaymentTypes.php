<?php
namespace foreup\rest\models\entities;



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupCustomPaymentTypes
 *
 * @ORM\Table(name="foreup_custom_payment_types", indexes={@ORM\Index(name="course_id", columns={"course_id"})})
 * @ORM\Entity
 */
class ForeupCustomPaymentTypes
{


    /**
     * @var integer
     *
     * @ORM\Column(name="payment_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

	/**
	 * @var \foreup\rest\models\entities\ForeupCourses
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $course;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_id", type="string", length=255, nullable=true)
     */
    private $paymentId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=true)
     */
    private $deleted = false;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_payment_type", type="string", length=255, nullable=true)
     */
    private $customPaymentType = '0';

	/**
	 * ForeupCustomPaymentTypes constructor.
	 * @param ForeupCourses $course
	 */
	public function __construct(ForeupCourses $course)
	{
		$this->course = $course;
		$this->paymentId = "";
		$this->label = "";
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $paymentTypeId)
	{
		$this->id = $paymentTypeId;
	}

	/**
	 * @return ForeupCourses
	 */
	public function getCourse(): ForeupCourses
	{
		return $this->course;
	}

	/**
	 * @param ForeupCourses $course
	 */
	public function setCourse(ForeupCourses $course)
	{
		$this->course = $course;
	}

	/**
	 * @return string
	 */
	public function getLabel(): string
	{
		return $this->label;
	}

	/**
	 * @param string $label
	 */
	public function setLabel(string $label)
	{
		$this->label = $label;
		$this->customPaymentType = $this->clean($label);
	}

	/**
	 * @return string
	 */
	public function getPaymentId()
	{
		return $this->paymentId;
	}

	/**
	 * @param string $paymentId
	 */
	public function setPaymentId(string $paymentId)
	{
		$this->paymentId = $paymentId;
	}

	/**
	 * @return bool
	 */
	public function isDeleted(): bool
	{
		return $this->deleted;
	}

	/**
	 * @param bool $deleted
	 */
	public function setDeleted(bool $deleted)
	{
		$this->deleted = $deleted;
	}

	/**
	 * @return string
	 */
	public function getCustomPaymentType(): string
	{
		return $this->customPaymentType;
	}


	private function clean($string) {
		$string = str_replace(' ', '-', strtolower($string)); // Replaces all spaces with hyphens.

		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
}

