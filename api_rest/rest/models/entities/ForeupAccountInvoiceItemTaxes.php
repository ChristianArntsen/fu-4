<?php
namespace foreup\rest\models\entities;



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountInvoiceItemTaxes
 *
 * @ORM\Table(name="foreup_account_invoice_item_taxes", indexes={@ORM\Index(name="fk_account_invoice_item_taxes_invoice_item_id_idx", columns={"invoice_item_id"})})
 * @ORM\Entity
 */
class ForeupAccountInvoiceItemTaxes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="percentage", type="decimal", precision=15, scale=4, nullable=false)
     */
    private $percentage;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $amount = '0.00';

    /**
     * @var \foreup\rest\models\entities\ForeupAccountInvoiceItems
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountInvoiceItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_item_id", referencedColumnName="id")
     * })
     */
    private $invoiceItem;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupAccountInvoiceItemTaxes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set percentage
     *
     * @param string $percentage
     *
     * @return ForeupAccountInvoiceItemTaxes
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return ForeupAccountInvoiceItemTaxes
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set invoiceItem
     *
     * @param \foreup\rest\models\entities\ForeupAccountInvoiceItems $invoiceItem
     *
     * @return ForeupAccountInvoiceItemTaxes
     */
    public function setInvoiceItem(\foreup\rest\models\entities\ForeupAccountInvoiceItems $invoiceItem = null)
    {
        $this->invoiceItem = $invoiceItem;

        return $this;
    }

    /**
     * Get invoiceItem
     *
     * @return \foreup\rest\models\entities\ForeupAccountInvoiceItems
     */
    public function getInvoiceItem()
    {
        return $this->invoiceItem;
    }
}
