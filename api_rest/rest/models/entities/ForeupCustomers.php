<?php
namespace foreup\rest\models\entities;


use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Debug\Exception\ContextErrorException;

/**
 * ForeupCustomers
 *
 * @ORM\Table(name="foreup_customers", uniqueConstraints={@ORM\UniqueConstraint(name="course customer", columns={"course_id", "person_id"}), @ORM\UniqueConstraint(name="account_number2", columns={"course_id", "account_number"})}, indexes={@ORM\Index(name="person_id", columns={"person_id"}), @ORM\Index(name="deleted", columns={"deleted"}), @ORM\Index(name="username", columns={"username"}), @ORM\Index(name="course_id", columns={"course_id"})})
 * @ORM\Entity (repositoryClass="foreup\rest\models\repositories\CustomersRepository")
 */
class ForeupCustomers
{
    use EntityValidator;

    public function __toString()
    {
	    return 'ForeupCustomer: '.$this->getPerson()->getPersonId();
    }

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->sales = new ArrayCollection();
		$this->groups = new ArrayCollection();
		$this->invoices = new ArrayCollection();
		$this->billing = new ArrayCollection();
		$this->accountTransactions = new ArrayCollection();
	}


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="CID", type="integer", nullable=false)
	 */
	private $cid;

	/**
	 * @var integer
	 *
	 * @ORM\Id
	 * @ORM\Column(name="course_id", type="integer", nullable=false)
	 */
	private $courseId;

	/**
	 * @var integer
	 *
	 * @ORM\Id
	 * @ORM\Column(name="person_id", type="integer", nullable=false)
	 */
	private $personId;

	/**
	 * @var \foreup\rest\models\entities\ForeupCourses
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $course;

	/**
	 * @var \foreup\rest\models\entities\ForeupPeople
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPeople",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
	 * })
	 */
	private $person;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="price_class", type="string", length=255, nullable=false)
	 */
	private $priceClass = "";

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="api_id", type="integer", nullable=false)
	 */
	private $apiId = 0;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="username", type="string", length=255, nullable=false)
	 */
	private $username = "";

	/**
	 * @var string
	 *
	 * @ORM\Column(name="password", type="string", length=255, nullable=false)
	 */
	private $password = "";

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="member", type="smallint", nullable=false)
	 */
	private $member = 0;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="image_id", type="integer", nullable=false)
	 */
    private $imageId = '0';

	/**
	 * @var \foreup\rest\models\entities\ForeupImages
	 *
	 * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupImages",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="image_id", referencedColumnName="image_id")
	 * })
	 */
	private $photo = null;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="account_number", type="string", length=255, nullable=true)
	 */
	private $accountNumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="account_balance", type="decimal", precision=15, scale=2, nullable=false)
	 */
	private $accountBalance = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="account_balance_allow_negative", type="boolean", nullable=false)
	 */
	private $accountBalanceAllowNegative = false;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="account_limit", type="decimal", precision=15, scale=2, nullable=false)
	 */
	private $accountLimit = 0;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="member_account_balance", type="decimal", precision=15, scale=2, nullable=false)
	 */
	private $memberAccountBalance = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="member_account_balance_allow_negative", type="boolean", nullable=false)
	 */
	private $memberAccountBalanceAllowNegative = '1';

	/**
	 * @var string
	 *
	 * @ORM\Column(name="member_account_limit", type="decimal", precision=15, scale=2, nullable=false)
	 */
	private $memberAccountLimit = 0;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="invoice_balance", type="decimal", precision=15, scale=2, nullable=false)
	 */
	private $invoiceBalance = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="invoice_balance_allow_negative", type="boolean", nullable=false)
	 */
	private $invoiceBalanceAllowNegative = '0';

	/**
	 * @var string
	 *
	 * @ORM\Column(name="invoice_email", type="string", length=255, nullable=false)
	 */
	private $invoiceEmail = "";

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="use_loyalty", type="boolean", nullable=false)
	 */
	private $useLoyalty = '1';

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="loyalty_points", type="integer", nullable=false)
	 */
	private $loyaltyPoints = 0;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="company_name", type="string", length=255, nullable=false)
	 */
	private $companyName = "";

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="taxable", type="integer", nullable=false)
	 */
	private $taxable = '1';

	/**
	 * @var float
	 *
	 * @ORM\Column(name="discount", type="float", precision=5, scale=2, nullable=false)
	 */
	private $discount = 0;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted = '0';

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="opt_out_email", type="boolean", nullable=false)
	 */
	private $optOutEmail = '0';

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="opt_out_text", type="boolean", nullable=false)
	 */
	private $optOutText = '0';

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="unsubscribe_all", type="boolean", nullable=false)
	 */
	private $unsubscribeAll = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="course_news_announcements", type="boolean", nullable=false)
	 */
	private $courseNewsAnnouncements = true;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="teetime_reminders", type="boolean", nullable=false)
	 */
	private $teetimeReminders = true;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="status_flag", type="boolean", nullable=false)
	 */
	private $statusFlag = '0';

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="require_food_minimum", type="boolean", nullable=false)
	 */
	private $requireFoodMinimum = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="hide_from_search", type="boolean", nullable=false)
	 */
	private $hideFromSearch = false;


	/**
	 * @var \foreup\rest\models\entities\ForeupSales
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupSales",mappedBy="customer")
	 */
	private $sales;

	/**
	* @ORM\ManyToMany(targetEntity="foreup\rest\models\entities\ForeupCustomerGroups")
	* @ORM\JoinTable(name="foreup_customer_group_members",
	*      joinColumns={@ORM\JoinColumn(name="person_id", referencedColumnName="person_id")},
	*      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="group_id")}
	*      )
	* @var ForeupCustomerGroups[]
	*/
	private $groups;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_created", type="datetime", nullable=true)
	 */
	private $dateCreated;



	/**
	 * @var \foreup\rest\models\entities\ForeupCustomerBilling[]
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupCustomerBilling", mappedBy="customer")
	 */
	private $billing;

	/**
	 * @var \foreup\rest\models\entities\ForeupAccountTransactions
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupAccountTransactions",mappedBy="customer")
	 */
	private $accountTransactions;






	/**
	 * @return mixed
	 */
	public function getGroups()
	{
		$courseIds = $this->course->getGroupMemberIds();
		$groupsToReturn = [];
		foreach($this->groups as $group)
		{
			if(array_search($group->getCourse()->getCourseId(),$courseIds) !== false){
				$groupsToReturn[] = $group;
			}
		}
		return $groupsToReturn;
	}

	/**
	 * @param mixed $groups
	 */
	public function addGroups($group)
	{
		$this->groups[] = $group;
	}

	/**
	 * @return ForeupSales
	 * @return \Doctrine\Common\Collections\ArrayCollection|ForeupSales[]
	 */
	public function getSales()
	{
		return $this->sales;
	}

	/**
	 * @param ForeupSales $sales
	 */
	public function setSales($sales)
	{
		$this->sales = $sales;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateCreated()
	{
		return $this->dateCreated;
	}

	/**
	 * @param \DateTime $dateCreated
	 */
	public function setDateCreated($dateCreated)
	{
		if(is_string($dateCreated)){
			$dateCreated = Carbon::parse($dateCreated);
		}
		$this->dateCreated = $dateCreated;
		return $this;
	}

	/**
	 * @return ForeupImages
	 */
	public function getPhoto()
	{
		$firstName = $this->getPerson()->getFirstName();
		$lastName = $this->getPerson()->getLastName();
		$firstName = str_replace("/","",$firstName);
		$lastName = str_replace("/","",$lastName);
		//$gravatarUrl = 'https://www.gravatar.com/avatar/'.md5(strtolower(trim($this->getPerson()->getEmail()))).'?r=pg&d='. urlencode("images/profiles/Customer_photo_icon.png");
		$defaultUrl = "https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/" . $firstName . "_" . $lastName . ".png";
		$gravatarUrl = 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($this->getPerson()->getEmail()))) . '?r=pg&d=' . $defaultUrl;
		if ($this->imageId) {
			try {
				$fileName = $this->photo->getFilename();
				return "https://s3-us-west-2.amazonaws.com/foreup.application.files/" . $this->courseId . "/thumbnails/" . $fileName;
			} catch (\Exception $e) {
				return $gravatarUrl;
			}

		} else {
			return $gravatarUrl;
		}
	}

	/**
	 * @param ForeupImages $photo
	 */
	public function setPhoto(ForeupImages $photo)
	{
		$this->photo = $photo;
	}


	/**
	 * Set cid
	 *
	 * @param integer $cid
	 *
	 * @return ForeupCustomers
	 */
	public function setCid($cid)
	{
		$this->cid = $cid;

		return $this;
	}

	/**
	 * Get cid
	 *
	 * @return integer
	 */
	public function getCid()
	{
		return $this->cid;
	}

	/**
	 * Set courseId
	 *
	 * @param integer $courseId
	 *
	 * @return ForeupCustomers
	 */
	public function setCourseId($courseId)
	{
		$this->courseId = $courseId;

		return $this;
	}

	/**
	 * Get courseId
	 *
	 * @return integer
	 */
	public function getCourseId()
	{
		if(!isset($this->courseId) && isset($this->course)){
			$this->courseId = $this->course->getCourseId();
		}
		return $this->courseId;
	}

	/**
	 * Get courseId
	 *
	 * @return integer
	 */
	public function getPersonId()
	{
		if(!isset($this->personId) && isset($this->person)){
			$this->personId = $this->person->getPersonId();
		}
		return $this->personId;
	}

	/**
	 * Set apiId
	 *
	 * @param integer $apiId
	 *
	 * @return ForeupCustomers
	 */
	public function setApiId($apiId)
	{
		$this->apiId = $apiId;

		return $this;
	}

	/**
	 * Get apiId
	 *
	 * @return integer
	 */
	public function getApiId()
	{
		return $this->apiId;
	}

	/**
	 * Set username
	 *
	 * @param string $username
	 *
	 * @return ForeupCustomers
	 */
	public function setUsername($username)
	{
		$this->username = trim($username);

		return $this;
	}

	/**
	 * Get username
	 *
	 * @return string
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * Set password
	 *
	 * @param string $password
	 *
	 * @return ForeupCustomers
	 */
	public function setPassword($password)
	{
		$this->password = $password;

		return $this;
	}

	/**
	 * Get password
	 *
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * Set member
	 *
	 * @param integer $member
	 *
	 * @return ForeupCustomers
	 */
	public function setMember($member)
	{
		$this->member = $member;

		return $this;
	}

	/**
	 * Get member
	 *
	 * @return integer
	 */
	public function getMember()
	{
		return $this->member;
	}

	/**
	 * Set priceClass
	 *
	 * @param string $priceClass
	 *
	 * @return ForeupCustomers
	 */
	public function setPriceClass($priceClass)
	{
		$this->priceClass = $priceClass;

		return $this;
	}

	/**
	 * Get priceClass
	 *
	 * @return ForeupPriceClass
	 */
	public function getPriceClass()
	{
		return $this->priceClass;
	}

	/**
	 * Set imageId
	 *
	 * @param integer $imageId
	 *
	 * @return ForeupCustomers
	 */
	public function setImageId($imageId)
	{
		$this->imageId = $imageId;

		return $this;
	}

	/**
	 * Get imageId
	 *
	 * @return integer
	 */
	public function getImageId()
	{
		return $this->imageId;
	}


	/**
	 * Set accountNumber
	 *
	 * @param string $accountNumber
	 *
	 * @return ForeupCustomers
	 */
	public function setAccountNumber($accountNumber)
	{
		$this->accountNumber = $accountNumber;

		return $this;
	}

	/**
	 * Get accountNumber
	 *
	 * @return string
	 */
	public function getAccountNumber()
	{
		return $this->accountNumber;
	}

	/**
	 * Set accountBalance
	 *
	 * @param string $accountBalance
	 *
	 * @return ForeupCustomers
	 */
	public function setAccountBalance($accountBalance)
	{
		$this->accountBalance = $accountBalance;

		return $this;
	}

	/**
	 * Get accountBalance
	 *
	 * @return string
	 */
	public function getAccountBalance()
	{
		return $this->accountBalance;
	}

	/**
	 * Set accountBalanceAllowNegative
	 *
	 * @param boolean $accountBalanceAllowNegative
	 *
	 * @return ForeupCustomers
	 */
	public function setAccountBalanceAllowNegative($accountBalanceAllowNegative)
	{
		$this->accountBalanceAllowNegative = $accountBalanceAllowNegative;

		return $this;
	}

	/**
	 * Get accountBalanceAllowNegative
	 *
	 * @return boolean
	 */
	public function getAccountBalanceAllowNegative()
	{
		return $this->accountBalanceAllowNegative;
	}

	/**
	 * Set accountLimit
	 *
	 * @param string $accountLimit
	 *
	 * @return ForeupCustomers
	 */
	public function setAccountLimit($accountLimit)
	{
		$this->accountLimit = $accountLimit;

		return $this;
	}

	/**
	 * Get accountLimit
	 *
	 * @return string
	 */
	public function getAccountLimit()
	{
		return $this->accountLimit;
	}

	/**
	 * Set memberAccountBalance
	 *
	 * @param string $memberAccountBalance
	 *
	 * @return ForeupCustomers
	 */
	public function setMemberAccountBalance($memberAccountBalance)
	{
		$this->memberAccountBalance = $memberAccountBalance;

		return $this;
	}

	/**
	 * Get memberAccountBalance
	 *
	 * @return string
	 */
	public function getMemberAccountBalance()
	{
		return $this->memberAccountBalance;
	}

	/**
	 * Set memberAccountBalanceAllowNegative
	 *
	 * @param boolean $memberAccountBalanceAllowNegative
	 *
	 * @return ForeupCustomers
	 */
	public function setMemberAccountBalanceAllowNegative($memberAccountBalanceAllowNegative)
	{
		$this->memberAccountBalanceAllowNegative = $memberAccountBalanceAllowNegative;

		return $this;
	}

	/**
	 * Get memberAccountBalanceAllowNegative
	 *
	 * @return boolean
	 */
	public function getMemberAccountBalanceAllowNegative()
	{
		return $this->memberAccountBalanceAllowNegative;
	}

	/**
	 * Set memberAccountLimit
	 *
	 * @param string $memberAccountLimit
	 *
	 * @return ForeupCustomers
	 */
	public function setMemberAccountLimit($memberAccountLimit)
	{
		$this->memberAccountLimit = $memberAccountLimit;

		return $this;
	}

	/**
	 * Get memberAccountLimit
	 *
	 * @return string
	 */
	public function getMemberAccountLimit()
	{
		return $this->memberAccountLimit;
	}

	/**
	 * Set invoiceBalance
	 *
	 * @param string $invoiceBalance
	 *
	 * @return ForeupCustomers
	 */
	public function setInvoiceBalance($invoiceBalance)
	{
		$this->invoiceBalance = $invoiceBalance;

		return $this;
	}

	/**
	 * Get invoiceBalance
	 *
	 * @return string
	 */
	public function getInvoiceBalance()
	{
		return $this->invoiceBalance;
	}

	/**
	 * Set invoiceBalanceAllowNegative
	 *
	 * @param boolean $invoiceBalanceAllowNegative
	 *
	 * @return ForeupCustomers
	 */
	public function setInvoiceBalanceAllowNegative($invoiceBalanceAllowNegative)
	{
		$this->invoiceBalanceAllowNegative = $invoiceBalanceAllowNegative;

		return $this;
	}

	/**
	 * Get invoiceBalanceAllowNegative
	 *
	 * @return boolean
	 */
	public function getInvoiceBalanceAllowNegative()
	{
		return $this->invoiceBalanceAllowNegative;
	}

	/**
	 * Set invoiceEmail
	 *
	 * @param string $invoiceEmail
	 *
	 * @return ForeupCustomers
	 */
	public function setInvoiceEmail($invoiceEmail)
	{
		$this->invoiceEmail = $invoiceEmail;

		return $this;
	}

	/**
	 * Get invoiceEmail
	 *
	 * @return string
	 */
	public function getInvoiceEmail()
	{
		return $this->invoiceEmail;
	}

	/**
	 * Set useLoyalty
	 *
	 * @param boolean $useLoyalty
	 *
	 * @return ForeupCustomers
	 */
	public function setUseLoyalty($useLoyalty)
	{
		$this->useLoyalty = $useLoyalty;

		return $this;
	}

	/**
	 * Get useLoyalty
	 *
	 * @return boolean
	 */
	public function getUseLoyalty()
	{
		return $this->useLoyalty;
	}

	/**
	 * Set loyaltyPoints
	 *
	 * @param integer $loyaltyPoints
	 *
	 * @return ForeupCustomers
	 */
	public function setLoyaltyPoints($loyaltyPoints)
	{
		$this->loyaltyPoints = $loyaltyPoints;

		return $this;
	}

	/**
	 * Get loyaltyPoints
	 *
	 * @return integer
	 */
	public function getLoyaltyPoints()
	{
		return $this->loyaltyPoints;
	}

	/**
	 * Set companyName
	 *
	 * @param string $companyName
	 *
	 * @return ForeupCustomers
	 */
	public function setCompanyName($companyName)
	{
		$this->companyName = $companyName;

		return $this;
	}

	/**
	 * Get companyName
	 *
	 * @return string
	 */
	public function getCompanyName()
	{
		if ($this->companyName === 0)
			return null;
		return $this->companyName;
	}

	/**
	 * Set taxable
	 *
	 * @param integer $taxable
	 *
	 * @return ForeupCustomers
	 */
	public function setTaxable($taxable)
	{
		$this->taxable = $taxable;

		return $this;
	}

	/**
	 * Get taxable
	 *
	 * @return integer
	 */
	public function getTaxable()
	{
		return $this->taxable;
	}

	/**
	 * Set discount
	 *
	 * @param float $discount
	 *
	 * @return ForeupCustomers
	 */
	public function setDiscount($discount)
	{
		$this->discount = $discount;

		return $this;
	}

	/**
	 * Get discount
	 *
	 * @return float
	 */
	public function getDiscount()
	{
		return $this->discount;
	}

	/**
	 * mark customer as deleted
	 * @param /EntityManager $db
	 * @return ForeupCustomers
	 */
	public function deleteCustomer($db)
	{
		foreach($this->groups as $group){
			$this->groups->removeElement($group);
		}

		foreach($this->billing as $bill){
			$bill->setDeleted(true);
		}



		//When customer belongs to only a single course, delete online credentials
		$qb = $db->createQueryBuilder();
		$qb->select($qb->expr()->countDistinct('c.courseId'))
			->from('e:ForeupCustomers', 'c')
			->where( $qb->expr()->andX(
				$qb->expr()->eq('c.person', ':person'),
				$qb->expr()->eq('c.deleted', 0)))
				->setParameter('person', $this->person);
		$results = $qb->getQuery()->getResult();

		if($results[0][1] === 1){
			$qb = $db->createQueryBuilder();
			$qb->delete('e:ForeupUsers', 'u')
				->where($qb->expr()->eq('u.person', ':person'))
				->setParameter('person', $this->person);
			$qb->getQuery()->getResult();
		}

		$this->deleted = true;
		$this->accountNumber = null;
		$this->invoiceBalance = null;

		return $this;
	}

	/**
	 * mark customer as deleted
	 *
	 * @return ForeupCustomers
	 */
	public function undeleteCustomer()
	{
		$this->deleted = false;

		return $this;
	}

	/**
	 * Get deleted
	 *
	 * @return integer
	 */
	public function getDeleted()
	{
		return $this->deleted;
	}

	public function setEmailSubscribed(bool $subscribed)
	{
		return $this->setOptOutEmail(!$subscribed);
	}

    /**
     * Set optOutEmail
     *
     * @param boolean $optOutEmail
     *
     * @return ForeupCustomers
     */
    public function setOptOutEmail($optOutEmail)
    {
        $this->optOutEmail = $optOutEmail;
    
        return $this;
    }

    /**
     * Get optOutEmail
     *
     * @return boolean
     */
    public function getOptOutEmail()
    {
        return $this->optOutEmail;
    }

    /**
     * Set optOutText
     *
     * @param boolean $optOutText
     *
     * @return ForeupCustomers
     */
    public function setOptOutText($optOutText)
    {
        $this->optOutText = $optOutText;
    
        return $this;
    }

    /**
     * Get optOutText
     *
     * @return boolean
     */
    public function getOptOutText()
    {
        return $this->optOutText;
    }

    /**
     * Set unsubscribeAll
     *
     * @param boolean $unsubscribeAll
     *
     * @return ForeupCustomers
     */
    public function setUnsubscribeAll($unsubscribeAll)
    {
        $this->unsubscribeAll = $unsubscribeAll;
    
        return $this;
    }

    /**
     * Get unsubscribeAll
     *
     * @return boolean
     */
    public function getUnsubscribeAll()
    {
        return $this->unsubscribeAll;
    }

    /**
     * Set courseNewsAnnouncements
     *
     * @param boolean $courseNewsAnnouncements
     *
     * @return ForeupCustomers
     */
    public function setCourseNewsAnnouncements($courseNewsAnnouncements)
    {
        $this->courseNewsAnnouncements = $courseNewsAnnouncements;
    
        return $this;
    }

    /**
     * Get courseNewsAnnouncements
     *
     * @return boolean
     */
    public function getCourseNewsAnnouncements()
    {
        return $this->courseNewsAnnouncements;
    }

    /**
     * Set teetimeReminders
     *
     * @param boolean $teetimeReminders
     *
     * @return ForeupCustomers
     */
    public function setTeetimeReminders($teetimeReminders)
    {
        $this->teetimeReminders = $teetimeReminders;
    
        return $this;
    }

    /**
     * Get teetimeReminders
     *
     * @return boolean
     */
    public function getTeetimeReminders()
    {
        return $this->teetimeReminders;
    }

    /**
     * Set statusFlag
     *
     * @param boolean $statusFlag
     *
     * @return ForeupCustomers
     */
    public function setStatusFlag($statusFlag)
    {
        $this->statusFlag = $statusFlag;
    
        return $this;
    }

    /**
     * Get statusFlag
     *
     * @return boolean
     */
    public function getStatusFlag()
    {
        return $this->statusFlag;
    }

    /**
     * Set requireFoodMinimum
     *
     * @param integer $requireFoodMinimum
     *
     * @return ForeupCustomers
     */
    public function setRequireFoodMinimum($requireFoodMinimum)
    {
        $this->requireFoodMinimum = $requireFoodMinimum;
    
        return $this;
    }

    /**
     * Get requireFoodMinimum
     *
     * @return integer
     */
    public function getRequireFoodMinimum()
    {
        return $this->requireFoodMinimum;
    }

    /**
     * Set hideFromSearch
     *
     * @param boolean $hideFromSearch
     *
     * @return ForeupCustomers
     */
    public function setHideFromSearch($hideFromSearch)
    {
        $this->hideFromSearch = $hideFromSearch;
    
        return $this;
    }

    /**
     * Get hideFromSearch
     *
     * @return boolean
     */
    public function getHideFromSearch()
    {
        return $this->hideFromSearch;
    }

    /**
     * Set person
     *
     * @param \foreup\rest\models\entities\ForeupPeople $person
     *
     * @return ForeupCustomers
     */
    public function setPerson(\foreup\rest\models\entities\ForeupPeople $person = null)
    {
        $this->person = $person;
        $this->personId = $person->getPersonId();
    
        return $this;
    }

    /**
     * Get person
     *
     * @return \foreup\rest\models\entities\ForeupPeople
     */
    public function getPerson()
    {
        return $this->person;
    }

	/**
	 * @return ForeupCourses
	 */
	public function getCourse()
	{
		return $this->course;
	}

	/**
	 * @param ForeupCourses $course
	 */
	public function setCourse(ForeupCourses $course)
	{
		$this->course = $course;
		$this->courseId = $course->getCourseId();
		$this->cid = $course->getCourseId();
	}

	/**
	 * @return ForeupAccountTransactions
	 * @return \Doctrine\Common\Collections\ArrayCollection|ForeupAccountTransactions[]
	 */
	public function getAccountTransactions()
	{
		return $this->accountTransactions;
	}

	public function addAccountTransaction(ForeupAccountTransactions $entity)
	{
		$this->accountTransactions[] = $entity;
	}


}
