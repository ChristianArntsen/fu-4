<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;
use fu\Exceptions\InvalidArgument;

/**
 * ForeupAccountRecurringStatements
 *
 * @ORM\Table(name="foreup_account_recurring_statements", uniqueConstraints={@ORM\UniqueConstraint(name="number", columns={"organization_id"})}, indexes={@ORM\Index(name="payment_terms_id", columns={"payment_terms_id"}), @ORM\Index(name="organization_id", columns={"organization_id"}), @ORM\Index(name="created_by", columns={"created_by"}), @ORM\Index(name="is_default", columns={"is_default"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupAccountRecurringStatements
{
	use \foreup\rest\models\entities\EntityValidator;
	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true) {
		$this->resetLastError();

		$location = 'ForeupAccountRecurringStatements->validate';

		$dateCreated = $this->getDateCreated();
		$v = $this->validate_object($location,'dateCreated',$dateCreated,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$createdBy = $this->createdBy;
		$v = $this->validate_object($location,'createdBy',$createdBy,'\foreup\rest\models\entities\ForeupEmployees',true,$throw);
		if($v!==true)return $v;

		$organizationId = $this->organizationId;
		$v = $this->validate_integer($location,'organizationId',$organizationId,true,$throw);
		if($v!==true)return $v;

		$paymentTerms = $this->paymentTerms;
		$v = $this->validate_integer($location,'paymentTerms',$paymentTerms,false,$throw);
		if($v!==true)return $v;

		$name = $this->name;
		$v = $this->validate_string($location,'name',$name,false,$throw);
		if($v!==true)return $v;

		$customerMessage = $this->customerMessage;
		$v = $this->validate_string($location,'customerMessage',$customerMessage,false,$throw);
		if($v!==true)return $v;

		$isActive = $this->isActive;
		$v = $this->validate_boolean($location,'isActive',$isActive,true,$throw);
		if($v!==true)return $v;

		$sendEmptyStatements = $this->sendEmptyStatements;
		$v = $this->validate_boolean($location,'sendEmptyStatements',$sendEmptyStatements,true,$throw);
		if($v!==true)return $v;

		$dateDeleted = $this->getDateDeleted();
		$v = $this->validate_object($location,'dateDeleted',$dateDeleted,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$deletedBy = $this->getDeletedBy();
		$v = $this->validate_integer($location,'deletedBy',$deletedBy,false,$throw);
		if($v!==true)return $v;

		$includeMemberTransactions = $this->getIncludeMemberTransactions();
		$v = $this->validate_boolean($location,'includeMemberTransactions',$includeMemberTransactions,true,$throw);
		if($v!==true)return $v;

		$includeCustomerTransactions = $this->getIncludeCustomerTransactions();
		$v = $this->validate_boolean($location,'includeCustomerTransactions',$includeCustomerTransactions,true,$throw);
		if($v!==true)return $v;

		$sendEmail = $this->getSendEmail();
		$v = $this->validate_boolean($location,'sendEmail',$sendEmail,true,$throw);
		if($v!==true)return $v;

		$sendMail = $this->getSendMail();
		$v = $this->validate_boolean($location,'sendMail',$sendMail,true,$throw);
		if($v!==true)return $v;

		$includePastDue = $this->getIncludePastDue();
		$v = $this->validate_boolean($location,'includePastDue',$includePastDue,true,$throw);
		if($v!==true)return $v;

		$dueAfterDays = $this->getDueAfterDays();
		$v = $this->validate_integer($location,'dueAfterDays',$dueAfterDays,true,$throw);
		if($v!==true)return $v;

		$autopayEnabled = $this->getAutopayEnabled();
		$v = $this->validate_boolean($location,'autopayEnabled',$autopayEnabled,true,$throw);
		if($v!==true)return $v;

		$autopayAfterDays = $this->getAutopayAfterDays();
		$v = $this->validate_integer($location,'autopayAfterDays',$autopayAfterDays,true,$throw);
		if($v!==true)return $v;

		$autopayAttempts = $this->getAutopayAttempts();
		$v = $this->validate_integer($location,'autopayAttempts',$autopayAttempts,true,$throw);
		if($v!==true)return $v;

		$autopayOverdueBalance = $this->getAutopayOverdueBalance();
		$v = $this->validate_boolean($location,'autopayOverdueBalance',$autopayOverdueBalance,true,$throw);
		if($v!==true)return $v;

		$payMemberBalance = $this->payMemberBalance;
		$v = $this->validate_boolean($location,'payMemberBalance',$payMemberBalance,true,$throw);
		if($v!==true)return $v;

		$payCustomerBalance = $this->payCustomerBalance;
		$v = $this->validate_boolean($location,'payCustomerBalance',$payCustomerBalance,true,$throw);
		if($v!==true)return $v;

		$value = $this->sendZeroChargeStatement;
		$v = $this->validate_boolean($location,'sendZeroChargeStatement',$value,true,$throw);
		if($v!==true)return $v;

		$value = $this->isDefault;
		$v = $this->validate_boolean($location,'isDefault',$value,true,$throw);
		if($v!==true)return $v;

		$value = $this->footerText;
		$v = $this->validate_string($location,'footerText',$value,false,$throw);
		if($v!==true)return $v;

		$value = $this->messageText;
		$v = $this->validate_string($location,'messageText',$value,false,$throw);
		if($v!==true)return $v;

		$value = $this->termsAndConditionsText;
		$v = $this->validate_string($location,'termsAndConditionsText',$value,false,$throw);
		if($v!==true)return $v;

		$value = $this->financeChargeEnabled;
		$v = $this->validate_boolean($location,'financeChargeEnabled',$value,true,$throw);
		if($v!==true)return $v;

		$value = $this->financeChargeAmount;
		$v = $this->validate_numeric($location,'financeChargeAmount',$value,true,$throw);
		if($v!==true)return $v;

		$value = $this->financeChargeType;
		$v = $this->validate_enum($location,'financeChargeType',$value,['percent','fixed'],true,$throw);
		if($v!==true)return $v;

		$value = $this->financeChargeAfterDays;
		$v = $this->validate_integer($location,'financeChargeAfterDays',$value,true,$throw);
		if($v!==true)return $v;

		return true;
	}
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=256, precision=0, scale=0, nullable=true, unique=false)
     */
    private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customer_message", type="text", length=65535, nullable=true)
	 */
	private $customerMessage = '';

	/**
	 * @var string
	 *
	 * @ORM\Column(name="footer_text", type="text", length=65535, nullable=true)
	 */
	private $footerText;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="message_text", type="text", length=65535, nullable=true)
	 */
	private $messageText;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="terms_and_conditions_text", type="text", length=65535, nullable=true)
	 */
	private $termsAndConditionsText;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateCreated;


    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="person_id", nullable=true)
     * })
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $organizationId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isActive = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_deleted", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateDeleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted_by", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $deletedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="send_empty_statements", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $sendEmptyStatements = 1;

    /**
     * @var \foreup\rest\models\entities\ForeupAccountPaymentTerms
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountPaymentTerms")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_terms_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $paymentTerms;

	/**
	 * @var \foreup\rest\models\entities\ForeupRepeatable
	 *
	 * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupRepeatable",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="repeated", referencedColumnName="id")
	 * })
	 */
	private $repeated;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="include_member_transactions", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $includeMemberTransactions = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="include_customer_transactions", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $includeCustomerTransactions = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="send_email", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $sendEmail = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="send_mail", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $sendMail = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="include_past_due", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $includePastDue = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="due_after_days", type="integer", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $dueAfterDays = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="autopay_enabled", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $autopayEnabled = 0;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="autopay_after_days", type="integer", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $autopayAfterDays = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="autopay_attempts", type="integer", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $autopayAttempts= 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="autopay_overdue_balance", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $autopayOverdueBalance= 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="pay_member_balance", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $payMemberBalance = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="pay_customer_balance", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $payCustomerBalance = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="send_zero_charge_statement", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $sendZeroChargeStatement = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_default", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $isDefault = 0;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="finance_charge_enabled", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $financeChargeEnabled = 0;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="finance_charge_amount", type="decimal", precision=15, scale=2, nullable=false)
	 */
	private $financeChargeAmount = '0.00';

	/**
	 * @var string
	 *
	 * @ORM\Column(name="finance_charge_type", type="text", length=65535, nullable=false)
	 */
	private $financeChargeType = 'percent';

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="finance_charge_after_days", type="integer", nullable=false)
	 */
	private $financeChargeAfterDays = 0;




	/**
	 * @var ForeupAccountRecurringCharges
	 *
	 * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupAccountRecurringCharges", mappedBy="recurringStatement",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="id", referencedColumnName="recurring_statement_id")
	 * })
	 */
	private $recurringCharge;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


	/**
	 * @return \foreup\rest\models\entities\ForeupRepeatable
	 */
	public function getRepeated()
	{
		return $this->repeated;
	}


	/**
	 * @return \foreup\rest\models\entities\ForeupAccountRecurringCharges
	 */
	public function getRecurringCharge()
	{
		return $this->recurringCharge;
	}

	/**
	 * @param \foreup\rest\models\entities\ForeupRepeatable $repeated
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setRepeated(\foreup\rest\models\entities\ForeupRepeatable $repeated = null)
	{
		$this->repeated = $repeated;
		return $this;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set footerText
	 *
	 * @param string $text
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setFooterText($text)
	{
		$this->footerText = $text;

		return $this;
	}

	/**
	 * Get footerText
	 *
	 * @return string
	 */
	public function getFooterText()
	{
		return $this->footerText;
	}

	/**
	 * Set messageText
	 *
	 * @param string $text
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setMessageText($text)
	{
		$this->messageText = $text;

		return $this;
	}

	/**
	 * Get messageText
	 *
	 * @return string
	 */
	public function getMessageText()
	{
		return $this->messageText;
	}

	/**
	 * Set termsAndConditionsText
	 *
	 * @param string $text
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setTermsAndConditionsText($text)
	{
		$this->termsAndConditionsText = $text;

		return $this;
	}

	/**
	 * Get termsAndConditionsText
	 *
	 * @return string
	 */
	public function getTermsAndConditionsText()
	{
		return $this->termsAndConditionsText;
	}

	/**
	 * Set customerMessage
	 *
	 * @param string $name
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setCustomerMessage($name)
	{
		$this->customerMessage = $name;

		return $this;
	}

	/**
	 * Get customerMessage
	 *
	 * @return string
	 */
	public function getCustomerMessage()
	{
		return $this->customerMessage;
	}

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupAccountRecurringStatements
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set createdBy
     *
     * @param ForeupEmployees $createdBy
     *
     * @return ForeupAccountRecurringStatements
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return ForeupEmployees
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     *
     * @return ForeupAccountRecurringStatements
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

	/**
	 * Set isActive
	 *
	 * @param boolean $isActive
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setIsActive($isActive)
	{
		if(is_string($isActive)){
			$isActive = strtolower($isActive);
			$ac = json_decode($isActive,true);
			if(isset($ac))$isActive = $ac;
		}

		$this->isActive = $isActive;

		return $this;
	}

	/**
	 * Get isActive
	 *
	 * @return boolean
	 */
	public function getIsActive()
	{
		return $this->isActive;
	}

	/**
	 * Set sendZeroChargeStatement
	 *
	 * @param boolean $send
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setSendZeroChargeStatement($send)
	{
		if(is_string($send)){
			$send = strtolower($send);
			$ac = json_decode($send,true);
			if(isset($ac))$send = $ac;
		}

		$this->sendZeroChargeStatement = $send;

		return $this;
	}

	/**
	 * Get sendZeroChargeStatement
	 *
	 * @return boolean
	 */
	public function getSendZeroChargeStatement()
	{
		return $this->sendZeroChargeStatement;
	}

	/**
	 * Set isDefault
	 *
	 * @param boolean $default
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setIsDefault($default)
	{
		if(is_string($default)){
			$default = strtolower($default);
			$ac = json_decode($default,true);
			if(isset($ac))$default = $ac;
		}

		$this->isDefault = $default;

		return $this;
	}

	/**
	 * Get isDefault
	 *
	 * @return boolean
	 */
	public function getIsDefault()
	{
		return $this->isDefault;
	}

    /**
     * Set dateDeleted
     *
     * @param \DateTime $dateDeleted
     *
     * @return ForeupAccountRecurringStatements
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->dateDeleted = $dateDeleted;
    
        return $this;
    }

    /**
     * Get dateDeleted
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->dateDeleted;
    }

    /**
     * Set deletedBy
     *
     * @param integer $deletedBy
     *
     * @return ForeupAccountRecurringStatements
     */
    public function setDeletedBy($deletedBy)
    {
        $this->deletedBy = $deletedBy;
    
        return $this;
    }

    /**
     * Get deletedBy
     *
     * @return integer
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

    /**
     * Set sendEmptyStatements
     *
     * @param boolean $sendEmptyStatements
     *
     * @return ForeupAccountRecurringStatements
     */
    public function setSendEmptyStatements($sendEmptyStatements)
    {
	    if(is_string($sendEmptyStatements)){
			$sendEmptyStatements = strtolower($sendEmptyStatements);
		    $ac = json_decode($sendEmptyStatements,true);
		    if(isset($ac))$sendEmptyStatements = $ac;
	    }
        $this->sendEmptyStatements = $sendEmptyStatements;
    
        return $this;
    }

    /**
     * Get sendEmptyStatements
     *
     * @return boolean
     */
    public function getSendEmptyStatements()
    {
        return $this->sendEmptyStatements;
    }

    /**
     * Set paymentTerms
     *
     * @param \foreup\rest\models\entities\ForeupAccountPaymentTerms $paymentTerms
     *
     * @return ForeupAccountRecurringStatements
     */
    public function setPaymentTerms(\foreup\rest\models\entities\ForeupAccountPaymentTerms $paymentTerms = null)
    {
        $this->paymentTerms = $paymentTerms;
    
        return $this;
    }

    /**
     * Get paymentTerms
     *
     * @return \foreup\rest\models\entities\ForeupAccountPaymentTerms
     */
    public function getPaymentTerms()
    {
        return $this->paymentTerms;
    }

	/**
	 * Set includeMemberTransactions
	 *
	 * @param boolean $include
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setIncludeMemberTransactions($include)
	{
		if(is_string($include)){
			$include = strtolower($include);
			$ac = json_decode($include,true);
			if(isset($ac))$include = $ac;
		}

		$this->includeMemberTransactions = $include;

		return $this;
	}

	/**
	 * Get includeMemberTransactions
	 *
	 * @return boolean
	 */
	public function getIncludeMemberTransactions()
	{
		return $this->includeMemberTransactions;
	}

	/**
	 * Set includeCustomerTransactions
	 *
	 * @param boolean $include
	 *
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setIncludeCustomerTransactions($include)
	{
		if(is_string($include)){
			$include = strtolower($include);
			$ac = json_decode($include,true);
			if(isset($ac))$include = $ac;
		}

		$this->includeCustomerTransactions = $include;

		return $this;
	}

	/**
	 * Get includeCustomerTransactions
	 *
	 * @return boolean
	 */
	public function getIncludeCustomerTransactions()
	{
		return $this->includeCustomerTransactions;
	}

	/**
	 * Set sendEmail
	 *
	 * @param boolean $send
	 *
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setSendEmail($send)
	{
		if(is_string($send)){
			$send = strtolower($send);
			$ac = json_decode($send,true);
			if(isset($ac))$send = $ac;
		}

		$this->sendEmail = $send;

		return $this;
	}

	/**
	 * Get sendEmail
	 *
	 * @return boolean
	 */
	public function getSendEmail()
	{
		return $this->sendEmail;
	}

	/**
	 * Set sendMail
	 *
	 * @param boolean $send
	 *
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setSendMail($send)
	{
		if(is_string($send)){
			$send = strtolower($send);
			$ac = json_decode($send,true);
			if(isset($ac))$send = $ac;
		}

		$this->sendMail = $send;

		return $this;
	}

	/**
	 * Get sendMail
	 *
	 * @return boolean
	 */
	public function getSendMail()
	{
		return $this->sendMail;
	}

	/**
	 * Set includePastDue
	 *
	 * @param boolean $include
	 *
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setIncludePastDue($include)
	{
		if(is_string($include)){
			$include = strtolower($include);
			$ac = json_decode($include,true);
			if(isset($ac))$include = $ac;
		}

		$this->includePastDue = $include;

		return $this;
	}

	/**
	 * Get includePastDue
	 *
	 * @return boolean
	 */
	public function getIncludePastDue()
	{
		return $this->includePastDue;
	}

	/**
	 * Set dueAfterDays
	 *
	 * @param integer $dueAfterDays
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setDueAfterDays($dueAfterDays)
	{
		$this->dueAfterDays = $dueAfterDays;

		return $this;
	}

	/**
	 * Get dueAfterDays
	 *
	 * @return integer
	 */
	public function getDueAfterDays()
	{
		return $this->dueAfterDays;
	}

	/**
	 * Set autopayEnabled
	 *
	 * @param boolean $autopayEnabled
	 *
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setAutopayEnabled($autopayEnabled)
	{
		if(is_string($autopayEnabled)){
			$autopayEnabled = strtolower($autopayEnabled);
			$ac = json_decode($autopayEnabled,true);
			if(isset($ac))$autopayEnabled = $ac;
		}

		$this->autopayEnabled = $autopayEnabled;

		return $this;
	}

	/**
	 * Get autopayEnabled
	 *
	 * @return boolean
	 */
	public function getAutopayEnabled()
	{
		return $this->autopayEnabled;
	}

	/**
	 * Set autopayAfterDays
	 *
	 * @param integer $autopayAfterDays
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setAutopayAfterDays($autopayAfterDays)
	{
		$this->autopayAfterDays = $autopayAfterDays;

		return $this;
	}

	/**
	 * Get autopayAfterDays
	 *
	 * @return integer
	 */
	public function getAutopayAfterDays()
	{
		return $this->autopayAfterDays;
	}


	/**
	 * Set autopayAttempts
	 *
	 * @param integer $autopayAttempts
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setAutopayAttempts($autopayAttempts)
	{
		$this->autopayAttempts = $autopayAttempts;

		return $this;
	}

	/**
	 * Get autopayAttempts
	 *
	 * @return integer
	 */
	public function getAutopayAttempts()
	{
		return $this->autopayAttempts;
	}

	/**
	 * Set autopayOverdueBalance
	 *
	 * @param boolean $autopayOverdueBalance
	 *
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setAutopayOverdueBalance($autopayOverdueBalance)
	{
		if(is_string($autopayOverdueBalance)){
			$autopayOverdueBalance = strtolower($autopayOverdueBalance);
			$ac = json_decode($autopayOverdueBalance,true);
			if(isset($ac))$autopayOverdueBalance = $ac;
		}

		$this->autopayOverdueBalance = $autopayOverdueBalance;

		return $this;
	}

	/**
	 * Get autopayOverdueBalance
	 *
	 * @return boolean
	 */
	public function getAutopayOverdueBalance()
	{
		return $this->autopayOverdueBalance;
	}

	/**
	 * Set payMemberBalance
	 *
	 * @param boolean $payMemberBalance
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setPayMemberBalance($payMemberBalance)
	{
		if(is_string($payMemberBalance)){
			$payMemberBalance = strtolower($payMemberBalance);
			$is=json_decode($payMemberBalance,true);
			if(isset($is))$payMemberBalance=$is;
		}
		$this->payMemberBalance = $payMemberBalance;

		return $this;
	}

	/**
	 * Get payMemberBalance
	 *
	 * @return boolean
	 */
	public function getPayMemberBalance()
	{
		return $this->payMemberBalance;
	}

	/**
	 * Set payCustomerBalance
	 *
	 * @param boolean $payCustomerBalance
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setPayCustomerBalance($payCustomerBalance)
	{
		if(is_string($payCustomerBalance)){
			$payCustomerBalance = strtolower($payCustomerBalance);
			$is=json_decode($payCustomerBalance,true);
			if(isset($is))$payCustomerBalance=$is;
		}
		$this->payCustomerBalance = $payCustomerBalance;

		return $this;
	}

	/**
	 * Get payCustomerBalance
	 *
	 * @return boolean
	 */
	public function getPayCustomerBalance()
	{
		return $this->payCustomerBalance;
	}

	/**
	 * Set financeChargeEnabled
	 *
	 * @param boolean $financeChargeEnabled
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setFinanceChargeEnabled($financeChargeEnabled)
	{
		if(is_string($financeChargeEnabled)){
			$financeChargeEnabled = strtolower($financeChargeEnabled);
			$is=json_decode($financeChargeEnabled,true);
			if(isset($is))$financeChargeEnabled=$is;
		}
		$this->financeChargeEnabled = $financeChargeEnabled;

		return $this;
	}

	/**
	 * Get financeChargeEnabled
	 *
	 * @return boolean
	 */
	public function getFinanceChargeEnabled()
	{
		return $this->financeChargeEnabled;
	}

	/**
	 * Set financeChargeAmount
	 *
	 * @param string $amount
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setFinanceChargeAmount($amount)
	{
		$this->financeChargeAmount = $amount;

		return $this;
	}

	/**
	 * Get financeChargeAmount
	 *
	 * @return string
	 */
	public function getFinanceChargeAmount()
	{
		return $this->financeChargeAmount;
	}

	/**
	 * Set financeChargeType
	 *
	 * @param string $text
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setFinanceChargeType($text)
	{

		$this->financeChargeType = $text;

		return $this;
	}

	/**
	 * Get financechargeType
	 *
	 * @return string
	 */
	public function getFinanceChargeType()
	{
		return $this->financeChargeType;
	}


	/**
	 * Set financeChargeAfterDays
	 *
	 * @param integer $days
	 *
	 * @return ForeupAccountRecurringStatements
	 */
	public function setFinanceChargeAfterDays($days)
	{
		$this->financeChargeAfterDays = $days;

		return $this;
	}

	/**
	 * Get financeChargeAfterDays
	 *
	 * @return integer
	 */
	public function getFinanceChargeAfterDays()
	{
		return $this->financeChargeAfterDays;
	}

}


