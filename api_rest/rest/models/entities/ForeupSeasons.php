<?php

namespace foreup\rest\models\entities;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupSeasons
 *
 * @ORM\Table(name="foreup_seasons", indexes={@ORM\Index(name="start_date", columns={"start_date", "end_date"}), @ORM\Index(name="holiday", columns={"holiday"}), @ORM\Index(name="course_id", columns={"course_id"}), @ORM\Index(name="online_open_time", columns={"online_open_time", "online_close_time"}), @ORM\Index(name="FK_foreup_seasons_foreup_teesheet", columns={"teesheet_id"})})
 * @ORM\Entity
 */
class ForeupSeasons
{
    /**
     * @var integer
     *
     * @ORM\Column(name="season_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $seasonId;


    /**
     * @var string
     *
     * @ORM\Column(name="season_name", type="string", length=255, nullable=false)
     */
    private $seasonName;

    /**
     * @var \Carbon\Carbon
     *
     * @ORM\Column(name="start_date", type="date", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date", nullable=false)
     */
    private $endDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="holiday", type="boolean", nullable=false)
     */
    private $holiday = false;

    /**
     * @var string
     *
     * @ORM\Column(name="increment", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $increment = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="front_nine", type="string", length=255, nullable=false)
     */
    private $frontNine = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="online_booking", type="boolean", nullable=false)
     */
    private $onlineBooking = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="online_open_time", type="smallint", nullable=false)
     */
    private $onlineOpenTime = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="online_close_time", type="smallint", nullable=false)
     */
    private $onlineCloseTime = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="`default`", type="boolean", nullable=false)
     */
    private $default = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted;

    /**
     * @var \foreup\rest\models\entities\ForeupTeesheet
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupTeesheet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="teesheet_id", referencedColumnName="teesheet_id")
     * })
     */
    private $teesheet;

    /**
     * @var ForeupPriceClass[]
     *
     * @ORM\ManyToMany(targetEntity="foreup\rest\models\entities\ForeupPriceClass", inversedBy="season")
     * @ORM\JoinTable(name="foreup_season_price_classes",
     *   joinColumns={
     *     @ORM\JoinColumn(name="season_id", referencedColumnName="season_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="class_id", referencedColumnName="class_id")
     *   }
     * )
     */
    private $class;


    /**
     * @var \foreup\rest\models\entities\ForeupCourses
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $course;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->class = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get seasonId
     *
     * @return integer
     */
    public function getSeasonId()
    {
        return $this->seasonId;
    }

    /**
     * Set seasonName
     *
     * @param string $seasonName
     *
     * @return ForeupSeasons
     */
    public function setSeasonName($seasonName)
    {
        $this->seasonName = $seasonName;
    
        return $this;
    }

    /**
     * Get seasonName
     *
     * @return string
     */
    public function getSeasonName()
    {
        return $this->seasonName;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return ForeupSeasons
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \Carbon\Carbon
     */
    public function getStartDate()
    {
        return Carbon::instance($this->startDate);
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return ForeupSeasons
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \Carbon\Carbon
     */
    public function getEndDate()
    {
        return Carbon::instance($this->endDate);
    }

    /**
     * Set holiday
     *
     * @param boolean $holiday
     *
     * @return ForeupSeasons
     */
    public function setHoliday($holiday)
    {
        $this->holiday = $holiday;
    
        return $this;
    }

    /**
     * Get holiday
     *
     * @return boolean
     */
    public function getHoliday()
    {
        return $this->holiday;
    }

    /**
     * Set increment
     *
     * @param string $increment
     *
     * @return ForeupSeasons
     */
    public function setIncrement($increment)
    {
        $this->increment = $increment;
    
        return $this;
    }

    /**
     * Get increment
     *
     * @return string
     */
    public function getIncrement()
    {
        return $this->increment;
    }

    /**
     * Set frontNine
     *
     * @param string $frontNine
     *
     * @return ForeupSeasons
     */
    public function setFrontNine($frontNine)
    {
        $this->frontNine = $frontNine;
    
        return $this;
    }

    /**
     * Get frontNine
     *
     * @return string
     */
    public function getFrontNine()
    {
        return $this->frontNine;
    }

    /**
     * Set onlineBooking
     *
     * @param boolean $onlineBooking
     *
     * @return ForeupSeasons
     */
    public function setOnlineBooking($onlineBooking)
    {
        $this->onlineBooking = $onlineBooking;
    
        return $this;
    }

    /**
     * Get onlineBooking
     *
     * @return boolean
     */
    public function getOnlineBooking()
    {
        return $this->onlineBooking;
    }

    /**
     * Set onlineOpenTime
     *
     * @param integer $onlineOpenTime
     *
     * @return ForeupSeasons
     */
    public function setOnlineOpenTime($onlineOpenTime)
    {
        $this->onlineOpenTime = $onlineOpenTime;
    
        return $this;
    }

    /**
     * Get onlineOpenTime
     *
     * @return integer
     */
    public function getOnlineOpenTime()
    {
        return $this->onlineOpenTime;
    }

    /**
     * Set onlineCloseTime
     *
     * @param integer $onlineCloseTime
     *
     * @return ForeupSeasons
     */
    public function setOnlineCloseTime($onlineCloseTime)
    {
        $this->onlineCloseTime = $onlineCloseTime;
    
        return $this;
    }

    /**
     * Get onlineCloseTime
     *
     * @return integer
     */
    public function getOnlineCloseTime()
    {
        return $this->onlineCloseTime;
    }

    /**
     * Set default
     *
     * @param boolean $default
     *
     * @return ForeupSeasons
     */
    public function setDefault($default)
    {
        $this->default = $default;
    
        return $this;
    }

    /**
     * Get default
     *
     * @return boolean
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ForeupSeasons
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @return ForeupCourses
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param ForeupCourses $course
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }



    /**
     * Set teesheet
     *
     * @param \foreup\rest\models\entities\ForeupTeesheet $teesheet
     *
     * @return ForeupSeasons
     */
    public function setTeesheet(\foreup\rest\models\entities\ForeupTeesheet $teesheet = null)
    {
        $this->teesheet = $teesheet;
    
        return $this;
    }

    /**
     * Get teesheet
     *
     * @return \foreup\rest\models\entities\ForeupTeesheet
     */
    public function getTeesheet()
    {
        return $this->teesheet;
    }

    public function hasClass(\foreup\rest\models\entities\ForeupPriceClass $newPriceClass)
    {
	    foreach($this->class as $priceClass){
		    if($priceClass->getId() == $newPriceClass->getId())
			    return true;
	    }
	    return false;
    }

    /**
     * Add class
     *
     * @param \foreup\rest\models\entities\ForeupPriceClass $class
     *
     * @return ForeupSeasons
     */
    public function addClass(\foreup\rest\models\entities\ForeupPriceClass $newPriceClass)
    {
    	foreach($this->class as $priceClass){
    		if($priceClass->getId() == $newPriceClass->getId())
    			return $this;
	    }
        $this->class[] = $newPriceClass;
    
        return $this;
    }

    /**
     * Remove class
     *
     * @param \foreup\rest\models\entities\ForeupPriceClasse$class
     */
    public function removeClass(\foreup\rest\models\entities\ForeupPriceClass $class)
    {
        $this->class->removeElement($class);
    }

    /**
     * Get class
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClass()
    {
        return $this->class;
    }

	public function isValid()
	{
		return true;
	}
}
