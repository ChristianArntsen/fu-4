<?php

namespace foreup\rest\models\entities;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use foreup\rest\models\entities\Partial\BookingPlayer;

/**
 * ForeupTeetime
 *
 * @ORM\Table(name="foreup_teetime", indexes={@ORM\Index(name="teesheet_id", columns={"teesheet_id"}), @ORM\Index(name="start", columns={"start"}), @ORM\Index(name="end", columns={"end"}), @ORM\Index(name="status", columns={"status"}), @ORM\Index(name="teesheet_id_start", columns={"teesheet_id", "start"})})
 * @ORM\Entity
 */
class ForeupTeetime
{

	public function __construct()
	{
		$this->dateBooked = Carbon::now();
	}

	private $bookedPlayers;

    /**
     * @var string
     *
     * @ORM\Column(name="TTID", type="string", length=21, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ttid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="reround", type="boolean", nullable=false)
     */
    private $reround = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="start", type="bigint", nullable=false)
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_datetime", type="datetime", nullable=false)
     */
    private $startDatetime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="duration", type="boolean", nullable=true)
     */
    private $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="end", type="bigint", nullable=false)
     */
    private $end;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_datetime", type="datetime", nullable=false)
     */
    private $endDatetime;

    /**
     * @var string
     *
     * @ORM\Column(name="allDay", type="string", length=5, nullable=false)
     */
    private $allday;

    /**
     * @var integer
     *
     * @ORM\Column(name="player_count", type="smallint", nullable=false)
     */
    private $playerCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="holes", type="integer", nullable=false)
     */
    private $holes;

    /**
     * @var float
     *
     * @ORM\Column(name="carts", type="float", precision=10, scale=0, nullable=false)
     */
    private $carts;

    /**
     * @var integer
     *
     * @ORM\Column(name="paid_player_count", type="integer", nullable=false)
     */
    private $paidPlayerCount = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="paid_carts", type="integer", nullable=false)
     */
    private $paidCarts = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="clubs", type="boolean", nullable=false)
     */
    private $clubs;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=false)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="cplayer_count", type="integer", nullable=false)
     */
    private $cplayerCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="choles", type="integer", nullable=false)
     */
    private $choles;

    /**
     * @var float
     *
     * @ORM\Column(name="ccarts", type="float", precision=10, scale=0, nullable=false)
     */
    private $ccarts;

    /**
     * @var string
     *
     * @ORM\Column(name="cpayment", type="string", length=10, nullable=false)
     */
    private $cpayment;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="details", type="text", length=65535, nullable=false)
     */
    private $details;

    /**
     * @var string
     *
     * @ORM\Column(name="side", type="string", length=10, nullable=false)
     */
    private $side;

    /**
     * @var string
     *
     * @ORM\Column(name="className", type="string", length=40, nullable=false)
     */
    private $classname;

    /**
     * @var integer
     *
     * @ORM\Column(name="credit_card_id", type="integer", nullable=false)
     */
    private $creditCardId;

	/**
	 * @var \foreup\rest\models\entities\ForeupSales
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupSales",mappedBy="teetime")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="TTID", referencedColumnName="teetime_id")
	 * })
	 */
	private $sales;

	/**
	 * @var \foreup\rest\models\entities\ForeupPeople
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
	 * })
	 */
	private $person1;

	/**
	 * @var \foreup\rest\models\entities\ForeupPeople
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="person_id_2", referencedColumnName="person_id")
	 * })
	 */
	private $person2;

	/**
	 * @var \foreup\rest\models\entities\ForeupPeople
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="person_id_3", referencedColumnName="person_id")
	 * })
	 */
	private $person3;

	/**
	 * @var \foreup\rest\models\entities\ForeupPeople
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="person_id_4", referencedColumnName="person_id")
	 * })
	 */
	private $person4;

	/**
	 * @var \foreup\rest\models\entities\ForeupPeople
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="person_id_5", referencedColumnName="person_id")
	 * })
	 */
	private $person5;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer", nullable=false)
     */
    private $personId;

    /**
     * @var ForeupPriceClass
     *
     * @ORM\ManyToOne(targetEntity="ForeupPriceClass",fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="price_class_1", referencedColumnName="class_id")
     * })
     */
    private $priceClass1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="person_paid_1", type="boolean", nullable=false)
     */
    private $personPaid1;




    /**
     * @var boolean
     *
     * @ORM\Column(name="cart_paid_1", type="boolean", nullable=false)
     */
    private $cartPaid1;

    /**
     * @var string
     *
     * @ORM\Column(name="person_name", type="string", length=255, nullable=false)
     */
    private $personName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="person_no_show_1", type="boolean", nullable=false)
     */
    private $personNoShow1;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id_2", type="integer", nullable=false)
     */
    private $personId2;

	/**
	 * @var ForeupPriceClass
	 *
	 * @ORM\ManyToOne(targetEntity="ForeupPriceClass",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="price_class_2", referencedColumnName="class_id")
	 * })
	 */
    private $priceClass2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="person_paid_2", type="boolean", nullable=false)
     */
    private $personPaid2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cart_paid_2", type="boolean", nullable=false)
     */
    private $cartPaid2;

    /**
     * @var string
     *
     * @ORM\Column(name="person_name_2", type="string", length=255, nullable=false)
     */
    private $personName2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="person_no_show_2", type="boolean", nullable=false)
     */
    private $personNoShow2;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id_3", type="integer", nullable=false)
     */
    private $personId3;

	/**
	 * @var ForeupPriceClass
	 *
	 * @ORM\ManyToOne(targetEntity="ForeupPriceClass",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="price_class_3", referencedColumnName="class_id")
	 * })
	 */
    private $priceClass3;

    /**
     * @var boolean
     *
     * @ORM\Column(name="person_paid_3", type="boolean", nullable=false)
     */
    private $personPaid3;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cart_paid_3", type="boolean", nullable=false)
     */
    private $cartPaid3;

    /**
     * @var string
     *
     * @ORM\Column(name="person_name_3", type="string", length=255, nullable=false)
     */
    private $personName3;

    /**
     * @var boolean
     *
     * @ORM\Column(name="person_no_show_3", type="boolean", nullable=false)
     */
    private $personNoShow3;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id_4", type="integer", nullable=false)
     */
    private $personId4;

	/**
	 * @var ForeupPriceClass
	 *
	 * @ORM\ManyToOne(targetEntity="ForeupPriceClass",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="price_class_4", referencedColumnName="class_id")
	 * })
	 */
    private $priceClass4;

	/**
	 * @var ForeupPriceClass
	 *
	 * @ORM\ManyToOne(targetEntity="ForeupPriceClass",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="price_class_5", referencedColumnName="class_id")
	 * })
	 */
	private $priceClass5;

    /**
     * @var boolean
     *
     * @ORM\Column(name="person_paid_4", type="boolean", nullable=false)
     */
    private $personPaid4;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cart_paid_4", type="boolean", nullable=false)
     */
    private $cartPaid4;

    /**
     * @var string
     *
     * @ORM\Column(name="person_name_4", type="string", length=255, nullable=false)
     */
    private $personName4;

    /**
     * @var boolean
     *
     * @ORM\Column(name="person_no_show_4", type="boolean", nullable=false)
     */
    private $personNoShow4;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id_5", type="integer", nullable=false)
     */
    private $personId5;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="price_class_1", type="integer", nullable=false)
	 */
	private $priceClassId1;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="price_class_2", type="integer", nullable=false)
	 */
	private $priceClassId2;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="price_class_3", type="integer", nullable=false)
	 */
	private $priceClassId3;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="price_class_4", type="integer", nullable=false)
	 */
	private $priceClassId4;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="price_class_5", type="integer", nullable=false)
	 */
	private $priceClassId5;


    /**
     * @var boolean
     *
     * @ORM\Column(name="person_paid_5", type="boolean", nullable=false)
     */
    private $personPaid5;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cart_paid_5", type="boolean", nullable=false)
     */
    private $cartPaid5;

    /**
     * @var string
     *
     * @ORM\Column(name="person_name_5", type="string", length=255, nullable=false)
     */
    private $personName5;

    /**
     * @var boolean
     *
     * @ORM\Column(name="person_no_show_5", type="boolean", nullable=false)
     */
    private $personNoShow5;

    /**
     * @var integer
     *
     * @ORM\Column(name="no_show_count", type="integer", nullable=false)
     */
    private $noShowCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_cart_fee", type="smallint", nullable=false)
     */
    private $defaultCartFee;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_price_category", type="smallint", nullable=false)
     */
    private $defaultPriceCategory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_updated", type="datetime", nullable=false)
     */
    private $lastUpdated = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_booked", type="datetime", nullable=false)
     */
    private $dateBooked;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_cancelled", type="datetime", nullable=false)
     */
    private $dateCancelled;

    /**
     * @var boolean
     *
     * @ORM\Column(name="send_confirmation", type="boolean", nullable=false)
     */
    private $sendConfirmation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="confirmation_emailed", type="boolean", nullable=false)
     */
    private $confirmationEmailed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="thank_you_emailed", type="boolean", nullable=false)
     */
    private $thankYouEmailed;

    /**
     * @var string
     *
     * @ORM\Column(name="booking_source", type="string", length=255, nullable=false)
     */
    private $bookingSource;

    /**
     * @var string
     *
     * @ORM\Column(name="booker_id", type="string", length=255, nullable=false)
     */
    private $bookerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="canceller_id", type="integer", nullable=false)
     */
    private $cancellerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="teetime_id", type="integer", nullable=false)
     */
    private $teetimeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="teesheet_id", type="integer", nullable=false)
     */
    private $teesheetId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="teed_off_time", type="datetime", nullable=false)
     */
    private $teedOffTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="turn_time", type="datetime", nullable=false)
     */
    private $turnTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish_time", type="datetime", nullable=false)
     */
    private $finishTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="api_id", type="integer", nullable=false)
     */
    private $apiId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="raincheck_players_issued", type="boolean", nullable=false)
     */
    private $raincheckPlayersIssued;

    /**
     * @var boolean
     *
     * @ORM\Column(name="front_paid", type="boolean", nullable=false)
     */
    private $frontPaid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="back_paid", type="boolean", nullable=false)
     */
    private $backPaid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="front_player_count", type="boolean", nullable=false)
     */
    private $frontPlayerCount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="back_player_count", type="boolean", nullable=false)
     */
    private $backPlayerCount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="front_carts", type="boolean", nullable=false)
     */
    private $frontCarts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="back_carts", type="boolean", nullable=false)
     */
    private $backCarts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="front_paid_carts", type="boolean", nullable=false)
     */
    private $frontPaidCarts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="back_paid_carts", type="boolean", nullable=false)
     */
    private $backPaidCarts;

    /**
     * @var string
     *
     * @ORM\Column(name="cart_num_1", type="string", length=20, nullable=false)
     */
    private $cartNum1;

    /**
     * @var string
     *
     * @ORM\Column(name="cart_num_2", type="string", length=20, nullable=false)
     */
    private $cartNum2;

    /**
     * @var integer
     *
     * @ORM\Column(name="promo_id", type="integer", nullable=true)
     */
    private $promoId;

    /**
     * @var integer
     *
     * @ORM\Column(name="aggregate_group_id", type="integer", nullable=true)
     */
    private $aggregateGroupId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_event_rate", type="boolean", nullable=false)
     */
    private $useEventRate;

	/**
	 * @return int
	 */
	public function getPriceClassId1()
	{
		return $this->priceClassId1;
	}

	/**
	 * @return int
	 */
	public function getPriceClassId2()
	{
		return $this->priceClassId2;
	}

	/**
	 * @return int
	 */
	public function getPriceClassId3()
	{
		return $this->priceClassId3;
	}

	/**
	 * @return int
	 */
	public function getPriceClassId4()
	{
		return $this->priceClassId4;
	}

	/**
	 * @return int
	 */
	public function getPriceClassId5()
	{
		return $this->priceClassId5;
	}
    /**
     * Get ttid
     *
     * @return string
     */
    public function getTtid()
    {
        return $this->ttid;
    }

    /**
     * Set reround
     *
     * @param boolean $reround
     *
     * @return ForeupTeetime
     */
    public function setReround($reround)
    {
        $this->reround = $reround;

        return $this;
    }

    /**
     * Get reround
     *
     * @return boolean
     */
    public function getReround()
    {
        return $this->reround;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupTeetime
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ForeupTeetime
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set start
     *
     * @param integer $start
     *
     * @return ForeupTeetime
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return integer
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set startDatetime
     *
     * @param \DateTime $startDatetime
     *
     * @return ForeupTeetime
     */
    public function setStartDatetime($startDatetime)
    {
        $this->startDatetime = $startDatetime;

        return $this;
    }

    /**
     * Get startDatetime
     *
     * @return \DateTime
     */
    public function getStartDatetime()
    {
        return Carbon::instance($this->startDatetime);
    }

    /**
     * Set duration
     *
     * @param boolean $duration
     *
     * @return ForeupTeetime
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return boolean
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set end
     *
     * @param integer $end
     *
     * @return ForeupTeetime
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return integer
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set endDatetime
     *
     * @param \DateTime $endDatetime
     *
     * @return ForeupTeetime
     */
    public function setEndDatetime($endDatetime)
    {
        $this->endDatetime = $endDatetime;

        return $this;
    }

    /**
     * Get endDatetime
     *
     * @return \DateTime
     */
    public function getEndDatetime()
    {
        return  Carbon::instance($this->endDatetime);
    }

    /**
     * Set allday
     *
     * @param string $allday
     *
     * @return ForeupTeetime
     */
    public function setAllday($allday)
    {
        $this->allday = $allday;

        return $this;
    }

    /**
     * Get allday
     *
     * @return string
     */
    public function getAllday()
    {
        return $this->allday;
    }

    /**
     * Set playerCount
     *
     * @param integer $playerCount
     *
     * @return ForeupTeetime
     */
    public function setPlayerCount($playerCount)
    {
        $this->playerCount = $playerCount;

        return $this;
    }

    /**
     * Get playerCount
     *
     * @return integer
     */
    public function getPlayerCount()
    {
        return $this->playerCount;
    }

    /**
     * Set holes
     *
     * @param boolean $holes
     *
     * @return ForeupTeetime
     */
    public function setHoles($holes)
    {
        $this->holes = $holes;

        return $this;
    }

    /**
     * Get holes
     *
     * @return integer
     */
    public function getHoles()
    {
        return $this->holes;
    }

    /**
     * Set carts
     *
     * @param float $carts
     *
     * @return ForeupTeetime
     */
    public function setCarts($carts)
    {
        $this->carts = $carts;

        return $this;
    }

    /**
     * Get carts
     *
     * @return float
     */
    public function getCarts()
    {
        return $this->carts;
    }

    /**
     * Set paidPlayerCount
     *
     * @param boolean $paidPlayerCount
     *
     * @return ForeupTeetime
     */
    public function setPaidPlayerCount($paidPlayerCount)
    {
        $this->paidPlayerCount = $paidPlayerCount;

        return $this;
    }

    /**
     * Get paidPlayerCount
     *
     * @return boolean
     */
    public function getPaidPlayerCount()
    {
        return $this->paidPlayerCount;
    }

    /**
     * Set paidCarts
     *
     * @param boolean $paidCarts
     *
     * @return ForeupTeetime
     */
    public function setPaidCarts($paidCarts)
    {
        $this->paidCarts = $paidCarts;

        return $this;
    }

    /**
     * Get paidCarts
     *
     * @return boolean
     */
    public function getPaidCarts()
    {
        return $this->paidCarts;
    }

    /**
     * Set clubs
     *
     * @param boolean $clubs
     *
     * @return ForeupTeetime
     */
    public function setClubs($clubs)
    {
        $this->clubs = $clubs;

        return $this;
    }

    /**
     * Get clubs
     *
     * @return boolean
     */
    public function getClubs()
    {
        return $this->clubs;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ForeupTeetime
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set cplayerCount
     *
     * @param boolean $cplayerCount
     *
     * @return ForeupTeetime
     */
    public function setCplayerCount($cplayerCount)
    {
        $this->cplayerCount = $cplayerCount;

        return $this;
    }

    /**
     * Get cplayerCount
     *
     * @return boolean
     */
    public function getCplayerCount()
    {
        return $this->cplayerCount;
    }

    /**
     * Set choles
     *
     * @param boolean $choles
     *
     * @return ForeupTeetime
     */
    public function setCholes($choles)
    {
        $this->choles = $choles;

        return $this;
    }

    /**
     * Get choles
     *
     * @return boolean
     */
    public function getCholes()
    {
        return $this->choles;
    }

    /**
     * Set ccarts
     *
     * @param float $ccarts
     *
     * @return ForeupTeetime
     */
    public function setCcarts($ccarts)
    {
        $this->ccarts = $ccarts;

        return $this;
    }

    /**
     * Get ccarts
     *
     * @return float
     */
    public function getCcarts()
    {
        return $this->ccarts;
    }

    /**
     * Set cpayment
     *
     * @param string $cpayment
     *
     * @return ForeupTeetime
     */
    public function setCpayment($cpayment)
    {
        $this->cpayment = $cpayment;

        return $this;
    }

    /**
     * Get cpayment
     *
     * @return string
     */
    public function getCpayment()
    {
        return $this->cpayment;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return ForeupTeetime
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ForeupTeetime
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set details
     *
     * @param string $details
     *
     * @return ForeupTeetime
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
	    $stripped_of_invalid_utf8_chars_string = iconv('UTF-8', 'UTF-8//IGNORE', $this->details);
        return $stripped_of_invalid_utf8_chars_string;
    }

    /**
     * Set side
     *
     * @param string $side
     *
     * @return ForeupTeetime
     */
    public function setSide($side)
    {
        $this->side = $side;

        return $this;
    }

    /**
     * Get side
     *
     * @return string
     */
    public function getSide()
    {
        return $this->side;
    }

    /**
     * Set classname
     *
     * @param string $classname
     *
     * @return ForeupTeetime
     */
    public function setClassname($classname)
    {
        $this->classname = $classname;

        return $this;
    }

    /**
     * Get classname
     *
     * @return string
     */
    public function getClassname()
    {
        return $this->classname;
    }

    /**
     * Set creditCardId
     *
     * @param integer $creditCardId
     *
     * @return ForeupTeetime
     */
    public function setCreditCardId($creditCardId)
    {
        $this->creditCardId = $creditCardId;

        return $this;
    }

    /**
     * Get creditCardId
     *
     * @return integer
     */
    public function getCreditCardId()
    {
        return $this->creditCardId;
    }

    /**
     * Set personId
     *
     * @param integer $personId
     *
     * @return ForeupTeetime
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;

        return $this;
    }

    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set priceClass1
     *
     * @param integer $priceClass1
     *
     * @return ForeupTeetime
     */
    public function setPriceClass1(ForeupPriceClass $priceClass)
    {
	    $this->priceClassId1 = $priceClass->getId();
	    $this->priceClass1 = $priceClass;

        return $this;
    }

    /**
     * Get priceClass1
     *
     * @return integer
     */
    public function getPriceClass1()
    {
	    if($this->priceClassId1 == 0)
	    	return null;

        return $this->priceClass1;
    }

    /**
     * Set personPaid1
     *
     * @param boolean $personPaid1
     *
     * @return ForeupTeetime
     */
    public function setPersonPaid1($personPaid1)
    {
        if($this->personPaid1 != $personPaid1){
	        $this->adjustUpPaidPersonCount($personPaid1);
        }
	    $this->personPaid1 = $personPaid1;


	    return $this;
    }

    public function adjustUpPaidPersonCount($up)
    {
    	$currentCount = $this->getPaidPlayerCount();
    	if($up){
		    $this->setPaidPlayerCount(++$currentCount);
	    } else {
    		$this->setPaidPlayerCount(--$currentCount);
	    }
    }

    /**
     * Get personPaid1
     *
     * @return boolean
     */
    public function getPersonPaid1()
    {
        return $this->personPaid1;
    }

    /**
     * Set cartPaid1
     *
     * @param boolean $cartPaid1
     *
     * @return ForeupTeetime
     */
    public function setCartPaid1($cartPaid1)
    {
        $this->cartPaid1 = $cartPaid1;

        return $this;
    }

    /**
     * Get cartPaid1
     *
     * @return boolean
     */
    public function getCartPaid1()
    {
        return $this->cartPaid1;
    }

    /**
     * Set personName
     *
     * @param string $personName
     *
     * @return ForeupTeetime
     */
    public function setPersonName($personName)
    {
        $this->personName = $personName;
        $this->setTitle($this->generateTitle());

        return $this;
    }

    /**
     * Get personName
     *
     * @return string
     */
    public function getPersonName()
    {
        return $this->personName;
    }

    /**
     * Set personNoShow1
     *
     * @param boolean $personNoShow1
     *
     * @return ForeupTeetime
     */
    public function setPersonNoShow1($personNoShow1)
    {
        $this->personNoShow1 = $personNoShow1;

        return $this;
    }

    /**
     * Get personNoShow1
     *
     * @return boolean
     */
    public function getPersonNoShow1()
    {
        return $this->personNoShow1;
    }

    /**
     * Set personId2
     *
     * @param integer $personId2
     *
     * @return ForeupTeetime
     */
    public function setPersonId2($personId2)
    {
        $this->personId2 = $personId2;

        return $this;
    }

    /**
     * Get personId2
     *
     * @return integer
     */
    public function getPersonId2()
    {
        return $this->personId2;
    }

    /**
     * Set priceClass2
     *
     * @param boolean $priceClass2
     *
     * @return ForeupTeetime
     */
    public function setPriceClass2(ForeupPriceClass $priceClass)
    {
	    $this->priceClassId2 = $priceClass->getId();
        $this->priceClass2 = $priceClass;

        return $this;
    }

    /**
     * Get priceClass2
     *
     * @return boolean
     */
    public function getPriceClass2()
    {
	    if($this->priceClassId2 == 0)
		    return null;
        return $this->priceClass2;
    }

    /**
     * Set personPaid2
     *
     * @param boolean $personPaid2
     *
     * @return ForeupTeetime
     */
    public function setPersonPaid2($personPaid2)
    {
	    if($this->personPaid2 != $personPaid2){
		    $this->adjustUpPaidPersonCount($personPaid2);
	    }
        $this->personPaid2 = $personPaid2;

        return $this;
    }

    /**
     * Get personPaid2
     *
     * @return boolean
     */
    public function getPersonPaid2()
    {
        return $this->personPaid2;
    }

    /**
     * Set cartPaid2
     *
     * @param boolean $cartPaid2
     *
     * @return ForeupTeetime
     */
    public function setCartPaid2($cartPaid2)
    {
        $this->cartPaid2 = $cartPaid2;

        return $this;
    }

    /**
     * Get cartPaid2
     *
     * @return boolean
     */
    public function getCartPaid2()
    {
        return $this->cartPaid2;
    }

    /**
     * Set personName2
     *
     * @param string $personName2
     *
     * @return ForeupTeetime
     */
    public function setPersonName2($personName2)
    {
        $this->personName2 = $personName2;
	    $this->setTitle($this->generateTitle());

        return $this;
    }

    /**
     * Get personName2
     *
     * @return string
     */
    public function getPersonName2()
    {
        return $this->personName2;
    }

    /**
     * Set personNoShow2
     *
     * @param boolean $personNoShow2
     *
     * @return ForeupTeetime
     */
    public function setPersonNoShow2($personNoShow2)
    {
        $this->personNoShow2 = $personNoShow2;

        return $this;
    }

    /**
     * Get personNoShow2
     *
     * @return boolean
     */
    public function getPersonNoShow2()
    {
        return $this->personNoShow2;
    }

    /**
     * Set personId3
     *
     * @param integer $personId3
     *
     * @return ForeupTeetime
     */
    public function setPersonId3($personId3)
    {
        $this->personId3 = $personId3;

        return $this;
    }

    /**
     * Get personId3
     *
     * @return integer
     */
    public function getPersonId3()
    {
        return $this->personId3;
    }

    /**
     * Set priceClass3
     *
     * @param boolean $priceClass3
     *
     * @return ForeupTeetime
     */
    public function setPriceClass3(ForeupPriceClass $priceClass)
    {
	    $this->priceClassId3 = $priceClass->getId();
        $this->priceClass3 = $priceClass;

        return $this;
    }

    /**
     * Get priceClass3
     *
     * @return boolean
     */
    public function getPriceClass3()
    {
	    if($this->priceClassId3 == 0)
		    return null;
        return $this->priceClass3;
    }

    /**
     * Set personPaid3
     *
     * @param boolean $personPaid3
     *
     * @return ForeupTeetime
     */
    public function setPersonPaid3($personPaid3)
    {
	    if($this->personPaid3 != $personPaid3){
		    $this->adjustUpPaidPersonCount($personPaid3);
	    }
        $this->personPaid3 = $personPaid3;

        return $this;
    }

    /**
     * Get personPaid3
     *
     * @return boolean
     */
    public function getPersonPaid3()
    {
        return $this->personPaid3;
    }

    /**
     * Set cartPaid3
     *
     * @param boolean $cartPaid3
     *
     * @return ForeupTeetime
     */
    public function setCartPaid3($cartPaid3)
    {
        $this->cartPaid3 = $cartPaid3;

        return $this;
    }

    /**
     * Get cartPaid3
     *
     * @return boolean
     */
    public function getCartPaid3()
    {
        return $this->cartPaid3;
    }

    /**
     * Set personName3
     *
     * @param string $personName3
     *
     * @return ForeupTeetime
     */
    public function setPersonName3($personName3)
    {
        $this->personName3 = $personName3;
	    $this->setTitle($this->generateTitle());

        return $this;
    }

    /**
     * Get personName3
     *
     * @return string
     */
    public function getPersonName3()
    {
        return $this->personName3;
    }

    /**
     * Set personNoShow3
     *
     * @param boolean $personNoShow3
     *
     * @return ForeupTeetime
     */
    public function setPersonNoShow3($personNoShow3)
    {
        $this->personNoShow3 = $personNoShow3;

        return $this;
    }

    /**
     * Get personNoShow3
     *
     * @return boolean
     */
    public function getPersonNoShow3()
    {
        return $this->personNoShow3;
    }

    /**
     * Set personId4
     *
     * @param integer $personId4
     *
     * @return ForeupTeetime
     */
    public function setPersonId4($personId4)
    {
        $this->personId4 = $personId4;

        return $this;
    }

    /**
     * Get personId4
     *
     * @return integer
     */
    public function getPersonId4()
    {
        return $this->personId4;
    }

    /**
     * Set priceClass4
     *
     * @param boolean $priceClass4
     *
     * @return ForeupTeetime
     */
    public function setPriceClass4(ForeupPriceClass $priceClass)
    {
	    $this->priceClassId4 = $priceClass->getId();
        $this->priceClass4 = $priceClass;

        return $this;
    }

    /**
     * Get priceClass4
     *
     * @return boolean
     */
    public function getPriceClass4()
    {
	    if($this->priceClassId4 == 0)
		    return null;
        return $this->priceClass4;
    }

    /**
     * Set personPaid4
     *
     * @param boolean $personPaid4
     *
     * @return ForeupTeetime
     */
    public function setPersonPaid4($personPaid4)
    {
	    if($this->personPaid4 != $personPaid4){
		    $this->adjustUpPaidPersonCount($personPaid4);
	    }
        $this->personPaid4 = $personPaid4;

        return $this;
    }

    /**
     * Get personPaid4
     *
     * @return boolean
     */
    public function getPersonPaid4()
    {
        return $this->personPaid4;
    }

    /**
     * Set cartPaid4
     *
     * @param boolean $cartPaid4
     *
     * @return ForeupTeetime
     */
    public function setCartPaid4($cartPaid4)
    {
        $this->cartPaid4 = $cartPaid4;

        return $this;
    }

    /**
     * Get cartPaid4
     *
     * @return boolean
     */
    public function getCartPaid4()
    {
        return $this->cartPaid4;
    }

    /**
     * Set personName4
     *
     * @param string $personName4
     *
     * @return ForeupTeetime
     */
    public function setPersonName4($personName4)
    {
        $this->personName4 = $personName4;
	    $this->setTitle($this->generateTitle());

        return $this;
    }

    /**
     * Get personName4
     *
     * @return string
     */
    public function getPersonName4()
    {
        return $this->personName4;
    }

    /**
     * Set personNoShow4
     *
     * @param boolean $personNoShow4
     *
     * @return ForeupTeetime
     */
    public function setPersonNoShow4($personNoShow4)
    {
        $this->personNoShow4 = $personNoShow4;

        return $this;
    }

    /**
     * Get personNoShow4
     *
     * @return boolean
     */
    public function getPersonNoShow4()
    {
        return $this->personNoShow4;
    }

    /**
     * Set personId5
     *
     * @param integer $personId5
     *
     * @return ForeupTeetime
     */
    public function setPersonId5($personId5)
    {
        $this->personId5 = $personId5;

        return $this;
    }

    /**
     * Get personId5
     *
     * @return integer
     */
    public function getPersonId5()
    {
        return $this->personId5;
    }

    /**
     * Set priceClass5
     *
     * @param boolean $priceClass5
     *
     * @return ForeupTeetime
     */
    public function setPriceClass5(ForeupPriceClass $priceClass)
    {
	    $this->priceClassId5 = $priceClass->getId();
        $this->priceClass5 = $priceClass;

        return $this;
    }

    /**
     * Get priceClass5
     *
     * @return boolean
     */
    public function getPriceClass5()
    {
	    if($this->priceClassId5 == 0)
		    return null;
        return $this->priceClass5;
    }
    /**
     * Set personPaid5
     *
     * @param boolean $personPaid5
     *
     * @return ForeupTeetime
     */
    public function setPersonPaid5($personPaid5)
    {
	    if($this->personPaid5 != $personPaid5){
		    $this->adjustUpPaidPersonCount($personPaid5);
	    }
        $this->personPaid5 = $personPaid5;

        return $this;
    }

    /**
     * Get personPaid5
     *
     * @return boolean
     */
    public function getPersonPaid5()
    {
        return $this->personPaid5;
    }

    /**
     * Set cartPaid5
     *
     * @param boolean $cartPaid5
     *
     * @return ForeupTeetime
     */
    public function setCartPaid5($cartPaid5)
    {
        $this->cartPaid5 = $cartPaid5;

        return $this;
    }

    /**
     * Get cartPaid5
     *
     * @return boolean
     */
    public function getCartPaid5()
    {
        return $this->cartPaid5;
    }

    /**
     * Set personName5
     *
     * @param string $personName5
     *
     * @return ForeupTeetime
     */
    public function setPersonName5($personName5)
    {
        $this->personName5 = $personName5;
	    $this->setTitle($this->generateTitle());

        return $this;
    }

    /**
     * Get personName5
     *
     * @return string
     */
    public function getPersonName5()
    {
        return $this->personName5;
    }

    /**
     * Set personNoShow5
     *
     * @param boolean $personNoShow5
     *
     * @return ForeupTeetime
     */
    public function setPersonNoShow5($personNoShow5)
    {
        $this->personNoShow5 = $personNoShow5;

        return $this;
    }

    /**
     * Get personNoShow5
     *
     * @return boolean
     */
    public function getPersonNoShow5()
    {
        return $this->personNoShow5;
    }

    /**
     * Set noShowCount
     *
     * @param integer $noShowCount
     *
     * @return ForeupTeetime
     */
    public function setNoShowCount($noShowCount)
    {
        $this->noShowCount = $noShowCount;

        return $this;
    }

    /**
     * Get noShowCount
     *
     * @return integer
     */
    public function getNoShowCount()
    {
        return $this->noShowCount;
    }

    /**
     * Set defaultCartFee
     *
     * @param integer $defaultCartFee
     *
     * @return ForeupTeetime
     */
    public function setDefaultCartFee($defaultCartFee)
    {
        $this->defaultCartFee = $defaultCartFee;

        return $this;
    }

    /**
     * Get defaultCartFee
     *
     * @return integer
     */
    public function getDefaultCartFee()
    {
        return $this->defaultCartFee;
    }

    /**
     * Set defaultPriceCategory
     *
     * @param integer $defaultPriceCategory
     *
     * @return ForeupTeetime
     */
    public function setDefaultPriceCategory($defaultPriceCategory)
    {
        $this->defaultPriceCategory = $defaultPriceCategory;

        return $this;
    }

    /**
     * Get defaultPriceCategory
     *
     * @return integer
     */
    public function getDefaultPriceCategory()
    {
        return $this->defaultPriceCategory;
    }

    /**
     * Set lastUpdated
     *
     * @param \DateTime $lastUpdated
     *
     * @return ForeupTeetime
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * Get lastUpdated
     *
     * @return \DateTime
     */
    public function getLastUpdated()
    {
        return  Carbon::instance($this->lastUpdated);
    }

    /**
     * Set dateBooked
     *
     * @param \DateTime $dateBooked
     *
     * @return ForeupTeetime
     */
    public function setDateBooked($dateBooked)
    {
        $this->dateBooked = $dateBooked;

        return $this;
    }

    /**
     * Get dateBooked
     *
     * @return \DateTime
     */
    public function getDateBooked()
    {
	    if(isset($this->dateBooked) && $this->dateBooked->getTimestamp() < 0)
		    return null;
        return  Carbon::instance($this->dateBooked);
    }

    /**
     * Set dateCancelled
     *
     * @param \DateTime $dateCancelled
     *
     * @return ForeupTeetime
     */
    public function setDateCancelled($dateCancelled)
    {
        $this->dateCancelled = $dateCancelled;

        return $this;
    }

    /**
     * Get dateCancelled
     *
     * @return \DateTime
     */
    public function getDateCancelled()
    {
	    if(isset($this->dateCancelled) && $this->dateCancelled->getTimestamp() < 0)
		    return null;
        return  Carbon::instance($this->dateCancelled);
    }

    /**
     * Set sendConfirmation
     *
     * @param boolean $sendConfirmation
     *
     * @return ForeupTeetime
     */
    public function setSendConfirmation($sendConfirmation)
    {
        $this->sendConfirmation = $sendConfirmation;

        return $this;
    }

    /**
     * Get sendConfirmation
     *
     * @return boolean
     */
    public function getSendConfirmation()
    {
        return $this->sendConfirmation;
    }

    /**
     * Set confirmationEmailed
     *
     * @param boolean $confirmationEmailed
     *
     * @return ForeupTeetime
     */
    public function setConfirmationEmailed($confirmationEmailed)
    {
        $this->confirmationEmailed = $confirmationEmailed;

        return $this;
    }

    /**
     * Get confirmationEmailed
     *
     * @return boolean
     */
    public function getConfirmationEmailed()
    {
        return $this->confirmationEmailed;
    }

    /**
     * Set thankYouEmailed
     *
     * @param boolean $thankYouEmailed
     *
     * @return ForeupTeetime
     */
    public function setThankYouEmailed($thankYouEmailed)
    {
        $this->thankYouEmailed = $thankYouEmailed;

        return $this;
    }

    /**
     * Get thankYouEmailed
     *
     * @return boolean
     */
    public function getThankYouEmailed()
    {
        return $this->thankYouEmailed;
    }

    /**
     * Set bookingSource
     *
     * @param string $bookingSource
     *
     * @return ForeupTeetime
     */
    public function setBookingSource($bookingSource)
    {
        $this->bookingSource = $bookingSource;

        return $this;
    }

    /**
     * Get bookingSource
     *
     * @return string
     */
    public function getBookingSource()
    {
        return $this->bookingSource;
    }

    /**
     * Set bookerId
     *
     * @param string $bookerId
     *
     * @return ForeupTeetime
     */
    public function setBookerId($bookerId)
    {
        $this->bookerId = $bookerId;

        return $this;
    }

    /**
     * Get bookerId
     *
     * @return string
     */
    public function getBookerId()
    {
        return $this->bookerId;
    }

    /**
     * Set cancellerId
     *
     * @param integer $cancellerId
     *
     * @return ForeupTeetime
     */
    public function setCancellerId($cancellerId)
    {
        $this->cancellerId = $cancellerId;

        return $this;
    }

    /**
     * Get cancellerId
     *
     * @return integer
     */
    public function getCancellerId()
    {
        return $this->cancellerId;
    }

    /**
     * Set teetimeId
     *
     * @param integer $teetimeId
     *
     * @return ForeupTeetime
     */
    public function setTeetimeId($teetimeId)
    {
        $this->teetimeId = $teetimeId;

        return $this;
    }

    /**
     * Get teetimeId
     *
     * @return integer
     */
    public function getTeetimeId()
    {
        return $this->teetimeId;
    }

    /**
     * Set teesheetId
     *
     * @param integer $teesheetId
     *
     * @return ForeupTeetime
     */
    public function setTeesheetId($teesheetId)
    {
        $this->teesheetId = $teesheetId;

        return $this;
    }

    /**
     * Get teesheetId
     *
     * @return integer
     */
    public function getTeesheetId()
    {
        return $this->teesheetId;
    }

    /**
     * Set teedOffTime
     *
     * @param \DateTime $teedOffTime
     *
     * @return ForeupTeetime
     */
    public function setTeedOffTime($teedOffTime)
    {
    	if(is_string($teedOffTime)){
    		$teedOffTime = Carbon::parse($teedOffTime);
	    }
        $this->teedOffTime = $teedOffTime;

        return $this;
    }

    /**
     * Get teedOffTime
     *
     * @return \DateTime
     */
    public function getTeedOffTime()
    {
    	if(!isset($this->teedOffTime) || $this->teedOffTime->getTimestamp() <= 0)
    		return null;
        return  Carbon::instance($this->teedOffTime);
    }

    /**
     * Set turnTime
     *
     * @param \DateTime $turnTime
     *
     * @return ForeupTeetime
     */
    public function setTurnTime($turnTime)
    {
        $this->turnTime = $turnTime;

        return $this;
    }

    /**
     * Get turnTime
     *
     * @return \DateTime
     */
    public function getTurnTime()
    {
	    if(isset($this->turnTime) && $this->turnTime->getTimestamp() < 0)
		    return null;
        return  Carbon::instance($this->turnTime);
    }

    /**
     * Set finishTime
     *
     * @param \DateTime $finishTime
     *
     * @return ForeupTeetime
     */
    public function setFinishTime($finishTime)
    {
        $this->finishTime = $finishTime;

        return $this;
    }

    /**
     * Get finishTime
     *
     * @return \DateTime
     */
    public function getFinishTime()
    {
	    if(isset($this->finishTime) && $this->finishTime->getTimestamp() < 0)
		    return null;
        return  Carbon::instance($this->finishTime);
    }

    /**
     * Set apiId
     *
     * @param integer $apiId
     *
     * @return ForeupTeetime
     */
    public function setApiId($apiId)
    {
        $this->apiId = $apiId;

        return $this;
    }

    /**
     * Get apiId
     *
     * @return integer
     */
    public function getApiId()
    {
        return $this->apiId;
    }

    /**
     * Set raincheckPlayersIssued
     *
     * @param boolean $raincheckPlayersIssued
     *
     * @return ForeupTeetime
     */
    public function setRaincheckPlayersIssued($raincheckPlayersIssued)
    {
        $this->raincheckPlayersIssued = $raincheckPlayersIssued;

        return $this;
    }

    /**
     * Get raincheckPlayersIssued
     *
     * @return boolean
     */
    public function getRaincheckPlayersIssued()
    {
        return $this->raincheckPlayersIssued;
    }

    /**
     * Set frontPaid
     *
     * @param boolean $frontPaid
     *
     * @return ForeupTeetime
     */
    public function setFrontPaid($frontPaid)
    {
        $this->frontPaid = $frontPaid;

        return $this;
    }

    /**
     * Get frontPaid
     *
     * @return boolean
     */
    public function getFrontPaid()
    {
        return $this->frontPaid;
    }

    /**
     * Set backPaid
     *
     * @param boolean $backPaid
     *
     * @return ForeupTeetime
     */
    public function setBackPaid($backPaid)
    {
        $this->backPaid = $backPaid;

        return $this;
    }

    /**
     * Get backPaid
     *
     * @return boolean
     */
    public function getBackPaid()
    {
        return $this->backPaid;
    }

    /**
     * Set frontPlayerCount
     *
     * @param boolean $frontPlayerCount
     *
     * @return ForeupTeetime
     */
    public function setFrontPlayerCount($frontPlayerCount)
    {
        $this->frontPlayerCount = $frontPlayerCount;

        return $this;
    }

    /**
     * Get frontPlayerCount
     *
     * @return boolean
     */
    public function getFrontPlayerCount()
    {
        return $this->frontPlayerCount;
    }

    /**
     * Set backPlayerCount
     *
     * @param boolean $backPlayerCount
     *
     * @return ForeupTeetime
     */
    public function setBackPlayerCount($backPlayerCount)
    {
        $this->backPlayerCount = $backPlayerCount;

        return $this;
    }

    /**
     * Get backPlayerCount
     *
     * @return boolean
     */
    public function getBackPlayerCount()
    {
        return $this->backPlayerCount;
    }

    /**
     * Set frontCarts
     *
     * @param boolean $frontCarts
     *
     * @return ForeupTeetime
     */
    public function setFrontCarts($frontCarts)
    {
        $this->frontCarts = $frontCarts;

        return $this;
    }

    /**
     * Get frontCarts
     *
     * @return boolean
     */
    public function getFrontCarts()
    {
        return $this->frontCarts;
    }

    /**
     * Set backCarts
     *
     * @param boolean $backCarts
     *
     * @return ForeupTeetime
     */
    public function setBackCarts($backCarts)
    {
        $this->backCarts = $backCarts;

        return $this;
    }

    /**
     * Get backCarts
     *
     * @return boolean
     */
    public function getBackCarts()
    {
        return $this->backCarts;
    }

    /**
     * Set frontPaidCarts
     *
     * @param boolean $frontPaidCarts
     *
     * @return ForeupTeetime
     */
    public function setFrontPaidCarts($frontPaidCarts)
    {
        $this->frontPaidCarts = $frontPaidCarts;

        return $this;
    }

    /**
     * Get frontPaidCarts
     *
     * @return boolean
     */
    public function getFrontPaidCarts()
    {
        return $this->frontPaidCarts;
    }

    /**
     * Set backPaidCarts
     *
     * @param boolean $backPaidCarts
     *
     * @return ForeupTeetime
     */
    public function setBackPaidCarts($backPaidCarts)
    {
        $this->backPaidCarts = $backPaidCarts;

        return $this;
    }

    /**
     * Get backPaidCarts
     *
     * @return boolean
     */
    public function getBackPaidCarts()
    {
        return $this->backPaidCarts;
    }

    /**
     * Set cartNum1
     *
     * @param string $cartNum1
     *
     * @return ForeupTeetime
     */
    public function setCartNum1($cartNum1)
    {
        $this->cartNum1 = $cartNum1;

        return $this;
    }

    /**
     * Get cartNum1
     *
     * @return string
     */
    public function getCartNum1()
    {
        return $this->cartNum1;
    }

    /**
     * Set cartNum2
     *
     * @param string $cartNum2
     *
     * @return ForeupTeetime
     */
    public function setCartNum2($cartNum2)
    {
        $this->cartNum2 = $cartNum2;

        return $this;
    }

    public function setCart1($carNum1)
    {
    	return $this->setCartNum1($carNum1);
    }
	public function setCart2($carNum2)
	{
		return $this->setCartNum2($carNum2);
	}
    /**
     * Get cartNum2
     *
     * @return string
     */
    public function getCartNum2()
    {
        return $this->cartNum2;
    }

    /**
     * Set promoId
     *
     * @param integer $promoId
     *
     * @return ForeupTeetime
     */
    public function setPromoId($promoId)
    {
        $this->promoId = $promoId;

        return $this;
    }

    /**
     * Get promoId
     *
     * @return integer
     */
    public function getPromoId()
    {
        return $this->promoId;
    }

    /**
     * Set aggregateGroupId
     *
     * @param integer $aggregateGroupId
     *
     * @return ForeupTeetime
     */
    public function setAggregateGroupId($aggregateGroupId)
    {
        $this->aggregateGroupId = $aggregateGroupId;

        return $this;
    }

    /**
     * Get aggregateGroupId
     *
     * @return integer
     */
    public function getAggregateGroupId()
    {
        return $this->aggregateGroupId;
    }

    /**
     * Set useEventRate
     *
     * @param boolean $useEventRate
     *
     * @return ForeupTeetime
     */
    public function setUseEventRate($useEventRate)
    {
        $this->useEventRate = $useEventRate;

        return $this;
    }

    /**
     * Get useEventRate
     *
     * @return boolean
     */
    public function getUseEventRate()
    {
        return $this->useEventRate;
    }

	/**
	 * @return ForeupPeople
	 */
	public function getPerson1()
	{
		if($this->personId == 0)
			return null;
		return $this->person1;
	}

	public function generateTitle()
	{
		$players = [];
		foreach($this->getBookedPlayers() as $player)
		{
			if($player->getPerson()){
				if($player->getPerson()->getLastName() != ""){
					$players[] = $player->getPerson()->getLastName();
				} else if($player->getPerson()->getFirstName()){
					$players[] = $player->getPerson()->getFirstName();
				}
			} else if($player->getName()){
				$players[] = $player->getName();
			}
		}

		$title = "";
		if(!empty($players)){
			$title = join(" - ",$players);
		} else {
			$title = "Tee time";
		}

		return $title;
	}

	/**
	 * @param ForeupPeople $person1
	 */
	public function setPerson1($person)
	{
		if(empty($person)){
			$this->person1 = null;
			$this->personId = 0;
		} else {
			$this->personId = $person->getPersonId();
			$this->person1 = $person;
		}
		$this->setTitle($this->generateTitle());
	}



	/**
	 * @return ForeupPeople
	 */
	public function getPerson2()
	{
		if($this->personId2 == 0)
			return null;
		return $this->person2;
	}

	/**
	 * @param ForeupPeople $person2
	 */
	public function setPerson2($person)
	{
		if(empty($person)){
			$this->person2 = null;
			$this->personId2 = 0;
		} else {
			$this->personId2 = $person->getPersonId();
			$this->person2 = $person;
		}
		$this->setTitle($this->generateTitle());
	}

	/**
	 * @return ForeupPeople
	 */
	public function getPerson3()
	{
		if($this->personId3 == 0)
			return null;
		return $this->person3;
	}

	/**
	 * @param ForeupPeople $person3
	 */
	public function setPerson3($person)
	{
		if(empty($person)){
			$this->person3 = null;
			$this->personId3 = 0;
		} else {
			$this->personId3 = $person->getPersonId();
			$this->person3 = $person;
		}
		$this->setTitle($this->generateTitle());
	}

	/**
	 * @return ForeupPeople
	 */
	public function getPerson4()
	{
		if($this->personId4 == 0)
			return null;
		return $this->person4;
	}

	/**
	 * @param ForeupPeople $person4
	 */
	public function setPerson4($person)
	{
		if(empty($person)){
			$this->person4 = null;
			$this->personId4 = 0;
		} else {
			$this->personId4 = $person->getPersonId();
			$this->person4 = $person;
		}
		$this->setTitle($this->generateTitle());
	}


	/**
	 * @return ForeupPeople
	 */
	public function getPerson5()
	{
		if($this->personId5 == 0)
			return null;
		return $this->person5;
	}

	/**
	 * @param ForeupPeople $person4
	 */
	public function setPerson5($person)
	{
		if(empty($person)){
			$this->person5 = null;
			$this->personId5 = 0;
		} else {
			$this->personId5 = $person->getPersonId();
			$this->person5 = $person;
		}
		$this->setTitle($this->generateTitle());
	}


	/**
	 * @param BookingPlayer $players
	 */
	public function setBookedPlayers(array $players )
	{
		$currentPlayers = $this->getBookedPlayers();
		foreach($currentPlayers as $key => $currentPlayer){
			$currentPlayer->setCartPaid($players[$key]->getCartPaid());
			$currentPlayer->setName($players[$key]->getName());
			$currentPlayer->setNoShow($players[$key]->getNoShow());
			$currentPlayer->setPaid($players[$key]->getPaid());
			$currentPlayer->setPriceClass($players[$key]->getPriceClass());
		}
	}


	public function getBookedPlayerById($id)
	{
		$exploded = explode("-",$id);
		if(count($exploded)!=2 || $exploded[1]>5 || $exploded < 1){
			throw new \Exception("Invalid booked playerId, $id");
		}

		return $this->getBookedPlayerByPosition($exploded[1]);
	}
	public function getBookedPlayerByPosition($position)
	{
		return $this->getBookedPlayers()[$position-1];
	}

	/**
	 * @return BookingPlayer[]
	 */
	public function getBookedPlayers()
	{
		$this->bookedPlayers = [];
		for($i=1;$i<=5;$i++){
			$bookingPlayer = new BookingPlayer($i,$this);
			$this->bookedPlayers[] = $bookingPlayer;
		}

		return $this->bookedPlayers;
	}

	/**
	 * @return ForeupPeople[]
	 */
    public function getPlayers()
    {
    	$players = [];
    	if($this->getPersonId() != 0){
			$players [] = $this->getPerson1();
	    }
	    if($this->getPersonId2() != 0){
		    $players [] = $this->getPerson2();
	    }
	    if($this->getPersonId3() != 0){
		    $players [] = $this->getPerson3();
	    }
	    if($this->getPersonId4() != 0){
		    $players [] = $this->getPerson4();
	    }
	    if($this->getPersonId5() != 0){
		    $players [] = $this->getPerson5();
	    }
	    return $players;
    }


    public function getSales()
    {
    	return $this->sales;
    }
}

