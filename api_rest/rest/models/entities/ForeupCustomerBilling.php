<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupCustomerBilling
 *
 * @ORM\Table(name="foreup_customer_billing", indexes={@ORM\Index(name="course_id", columns={"course_id"}), @ORM\Index(name="person_id", columns={"person_id"}), @ORM\Index(name="start_date", columns={"start_date", "end_date"}), @ORM\Index(name="batch_id", columns={"batch_id"})})
 * @ORM\Entity
 */
class ForeupCustomerBilling
{
    /**
     * @var \foreup\rest\models\entities\ForeupCustomers
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCustomers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id"),
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $customer;

    /**
     * @var integer
     *
     * @ORM\Column(name="billing_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $billingId;

    /**
     * @var integer
     *
     * @ORM\Column(name="batch_id", type="integer", nullable=false)
     */
    private $batchId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="employee_id", type="integer", nullable=false)
     */
    private $employeeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer", nullable=false)
     */
    private $personId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date", nullable=false)
     */
    private $endDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="email_invoice", type="boolean", nullable=false)
     */
    private $emailInvoice;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="frequency", type="smallint", nullable=false)
     */
    private $frequency;

    /**
     * @var string
     *
     * @ORM\Column(name="frequency_period", type="string", length=24, nullable=false)
     */
    private $frequencyPeriod;

    /**
     * @var string
     *
     * @ORM\Column(name="frequency_on", type="string", length=24, nullable=false)
     */
    private $frequencyOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="frequency_on_date", type="date", nullable=false)
     */
    private $frequencyOnDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="due_days", type="smallint", nullable=false)
     */
    private $dueDays;

    /**
     * @var integer
     *
     * @ORM\Column(name="stop_after", type="smallint", nullable=true)
     */
    private $stopAfter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_invoice_generation_attempt", type="date", nullable=false)
     */
    private $lastInvoiceGenerationAttempt;

    /**
     * @var integer
     *
     * @ORM\Column(name="credit_card_id", type="integer", nullable=false)
     */
    private $creditCardId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_bill_delay", type="boolean", nullable=false)
     */
    private $autoBillDelay;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_billing_attempt", type="date", nullable=false)
     */
    private $lastBillingAttempt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="started", type="boolean", nullable=false)
     */
    private $started;

    /**
     * @var boolean
     *
     * @ORM\Column(name="charged", type="boolean", nullable=false)
     */
    private $charged;

    /**
     * @var boolean
     *
     * @ORM\Column(name="emailed", type="boolean", nullable=false)
     */
    private $emailed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_account_transactions", type="boolean", nullable=false)
     */
    private $showAccountTransactions = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="attempt_limit", type="boolean", nullable=false)
     */
    private $attemptLimit = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_pay_overdue", type="boolean", nullable=false)
     */
    private $autoPayOverdue;



    /**
     * Get billingId
     *
     * @return integer
     */
    public function getBillingId()
    {
        return $this->billingId;
    }

    /**
     * Set batchId
     *
     * @param integer $batchId
     *
     * @return ForeupCustomerBilling
     */
    public function setBatchId($batchId)
    {
        $this->batchId = $batchId;
    
        return $this;
    }

    /**
     * Get batchId
     *
     * @return integer
     */
    public function getBatchId()
    {
        return $this->batchId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ForeupCustomerBilling
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupCustomerBilling
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    
        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set employeeId
     *
     * @param integer $employeeId
     *
     * @return ForeupCustomerBilling
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;
    
        return $this;
    }

    /**
     * Get employeeId
     *
     * @return integer
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * Set personId
     *
     * @param integer $personId
     *
     * @return ForeupCustomerBilling
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    
        return $this;
    }

    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return ForeupCustomerBilling
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return ForeupCustomerBilling
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set emailInvoice
     *
     * @param boolean $emailInvoice
     *
     * @return ForeupCustomerBilling
     */
    public function setEmailInvoice($emailInvoice)
    {
        $this->emailInvoice = $emailInvoice;
    
        return $this;
    }

    /**
     * Get emailInvoice
     *
     * @return boolean
     */
    public function getEmailInvoice()
    {
        return $this->emailInvoice;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ForeupCustomerBilling
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ForeupCustomerBilling
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set frequency
     *
     * @param integer $frequency
     *
     * @return ForeupCustomerBilling
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    
        return $this;
    }

    /**
     * Get frequency
     *
     * @return integer
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set frequencyPeriod
     *
     * @param string $frequencyPeriod
     *
     * @return ForeupCustomerBilling
     */
    public function setFrequencyPeriod($frequencyPeriod)
    {
        $this->frequencyPeriod = $frequencyPeriod;
    
        return $this;
    }

    /**
     * Get frequencyPeriod
     *
     * @return string
     */
    public function getFrequencyPeriod()
    {
        return $this->frequencyPeriod;
    }

    /**
     * Set frequencyOn
     *
     * @param string $frequencyOn
     *
     * @return ForeupCustomerBilling
     */
    public function setFrequencyOn($frequencyOn)
    {
        $this->frequencyOn = $frequencyOn;
    
        return $this;
    }

    /**
     * Get frequencyOn
     *
     * @return string
     */
    public function getFrequencyOn()
    {
        return $this->frequencyOn;
    }

    /**
     * Set frequencyOnDate
     *
     * @param \DateTime $frequencyOnDate
     *
     * @return ForeupCustomerBilling
     */
    public function setFrequencyOnDate($frequencyOnDate)
    {
        $this->frequencyOnDate = $frequencyOnDate;
    
        return $this;
    }

    /**
     * Get frequencyOnDate
     *
     * @return \DateTime
     */
    public function getFrequencyOnDate()
    {
        return $this->frequencyOnDate;
    }

    /**
     * Set dueDays
     *
     * @param integer $dueDays
     *
     * @return ForeupCustomerBilling
     */
    public function setDueDays($dueDays)
    {
        $this->dueDays = $dueDays;
    
        return $this;
    }

    /**
     * Get dueDays
     *
     * @return integer
     */
    public function getDueDays()
    {
        return $this->dueDays;
    }

    /**
     * Set stopAfter
     *
     * @param integer $stopAfter
     *
     * @return ForeupCustomerBilling
     */
    public function setStopAfter($stopAfter)
    {
        $this->stopAfter = $stopAfter;
    
        return $this;
    }

    /**
     * Get stopAfter
     *
     * @return integer
     */
    public function getStopAfter()
    {
        return $this->stopAfter;
    }

    /**
     * Set lastInvoiceGenerationAttempt
     *
     * @param \DateTime $lastInvoiceGenerationAttempt
     *
     * @return ForeupCustomerBilling
     */
    public function setLastInvoiceGenerationAttempt($lastInvoiceGenerationAttempt)
    {
        $this->lastInvoiceGenerationAttempt = $lastInvoiceGenerationAttempt;
    
        return $this;
    }

    /**
     * Get lastInvoiceGenerationAttempt
     *
     * @return \DateTime
     */
    public function getLastInvoiceGenerationAttempt()
    {
        return $this->lastInvoiceGenerationAttempt;
    }

    /**
     * Set creditCardId
     *
     * @param integer $creditCardId
     *
     * @return ForeupCustomerBilling
     */
    public function setCreditCardId($creditCardId)
    {
        $this->creditCardId = $creditCardId;
    
        return $this;
    }

    /**
     * Get creditCardId
     *
     * @return integer
     */
    public function getCreditCardId()
    {
        return $this->creditCardId;
    }

    /**
     * Set autoBillDelay
     *
     * @param boolean $autoBillDelay
     *
     * @return ForeupCustomerBilling
     */
    public function setAutoBillDelay($autoBillDelay)
    {
        $this->autoBillDelay = $autoBillDelay;
    
        return $this;
    }

    /**
     * Get autoBillDelay
     *
     * @return boolean
     */
    public function getAutoBillDelay()
    {
        return $this->autoBillDelay;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ForeupCustomerBilling
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set lastBillingAttempt
     *
     * @param \DateTime $lastBillingAttempt
     *
     * @return ForeupCustomerBilling
     */
    public function setLastBillingAttempt($lastBillingAttempt)
    {
        $this->lastBillingAttempt = $lastBillingAttempt;
    
        return $this;
    }

    /**
     * Get lastBillingAttempt
     *
     * @return \DateTime
     */
    public function getLastBillingAttempt()
    {
        return $this->lastBillingAttempt;
    }

    /**
     * Set started
     *
     * @param boolean $started
     *
     * @return ForeupCustomerBilling
     */
    public function setStarted($started)
    {
        $this->started = $started;
    
        return $this;
    }

    /**
     * Get started
     *
     * @return boolean
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * Set charged
     *
     * @param boolean $charged
     *
     * @return ForeupCustomerBilling
     */
    public function setCharged($charged)
    {
        $this->charged = $charged;
    
        return $this;
    }

    /**
     * Get charged
     *
     * @return boolean
     */
    public function getCharged()
    {
        return $this->charged;
    }

    /**
     * Set emailed
     *
     * @param boolean $emailed
     *
     * @return ForeupCustomerBilling
     */
    public function setEmailed($emailed)
    {
        $this->emailed = $emailed;
    
        return $this;
    }

    /**
     * Get emailed
     *
     * @return boolean
     */
    public function getEmailed()
    {
        return $this->emailed;
    }

    /**
     * Set showAccountTransactions
     *
     * @param boolean $showAccountTransactions
     *
     * @return ForeupCustomerBilling
     */
    public function setShowAccountTransactions($showAccountTransactions)
    {
        $this->showAccountTransactions = $showAccountTransactions;
    
        return $this;
    }

    /**
     * Get showAccountTransactions
     *
     * @return boolean
     */
    public function getShowAccountTransactions()
    {
        return $this->showAccountTransactions;
    }

    /**
     * Set attemptLimit
     *
     * @param boolean $attemptLimit
     *
     * @return ForeupCustomerBilling
     */
    public function setAttemptLimit($attemptLimit)
    {
        $this->attemptLimit = $attemptLimit;
    
        return $this;
    }

    /**
     * Get attemptLimit
     *
     * @return boolean
     */
    public function getAttemptLimit()
    {
        return $this->attemptLimit;
    }

    /**
     * Set autoPayOverdue
     *
     * @param boolean $autoPayOverdue
     *
     * @return ForeupCustomerBilling
     */
    public function setAutoPayOverdue($autoPayOverdue)
    {
        $this->autoPayOverdue = $autoPayOverdue;
    
        return $this;
    }

    /**
     * Get autoPayOverdue
     *
     * @return boolean
     */
    public function getAutoPayOverdue()
    {
        return $this->autoPayOverdue;
    }
}
