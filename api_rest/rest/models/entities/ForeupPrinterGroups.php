<?php
namespace foreup\rest\models\entities;



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupPrinterGroups
 *
 * @ORM\Table(name="foreup_printer_groups", indexes={@ORM\Index(name="course_id", columns={"course_id"}), @ORM\Index(name="default_printer_id", columns={"default_printer_id"})})
 * @ORM\Entity
 */
class ForeupPrinterGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="printer_group_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

	/**
	 * @var \foreup\rest\models\entities\ForeupCourses
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $course;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label = "Default Label";

    /**
     * @var integer
     *
     * @ORM\Column(name="default_printer_id", type="integer", nullable=false)
     */
    private $defaultPrinterId;

	/**
	 * @return int
	 */
	public function getId(): int
	{
		if(empty($this->id)){
			return 0;
		}
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id)
	{
		$this->id = $id;
	}



	/**
	 * @return ForeupCourses
	 */
	public function getCourse(): ForeupCourses
	{
		return $this->course;
	}

	/**
	 * @param ForeupCourses $course
	 */
	public function setCourse(ForeupCourses $course)
	{
		$this->course = $course;
	}

	/**
	 * @return string
	 */
	public function getLabel(): string
	{
		return $this->label;
	}

	/**
	 * @param string $label
	 */
	public function setLabel(string $label)
	{
		$this->label = $label;
	}

	/**
	 * @return ForeupPrinters
	 */
	public function getDefaultPrinterId()
	{
		return $this->defaultPrinterId;
	}

	/**
	 * @param ForeupPrinters $defaultPrinterId
	 */
	public function setDefaultPrinterId(int $defaultPrinterId)
	{
		$this->defaultPrinterId = $defaultPrinterId;
	}




}

