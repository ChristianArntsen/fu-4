<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupApiUserCourses
 *
 * @ORM\Table(name="foreup_api_user_courses", indexes={@ORM\Index(name="FK_foreup_people_courses_foreup_courses", columns={"course_id"}), @ORM\Index(name="FK_foreup_people_courses_foreup_users", columns={"user_id"})})
 * @ORM\Entity
 */
class ForeupApiUserCourses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \foreup\rest\models\entities\ForeupCourses
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses",fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $course;

	/**
	 * @var \foreup\rest\models\entities\ForeupUsers
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupUsers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="actual_user_id", referencedColumnName="id")
	 * })
	 */
	private $actualUser;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="user_id", type="integer", nullable=false)
	 */
	private $userId;


	/**
	 * @var \foreup\rest\models\entities\ForeupEmployees
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="employee_id", referencedColumnName="person_id")
	 * })
	 */
	private $employee;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="price_class_id", type="integer", nullable=false)
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $priceClassId;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="is_module", type="boolean", nullable=false)
	 */
	private $isModel;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="module_name", type="string", length=255, nullable=true)
	 */
	private $moduleName;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="module_subheading", type="string", length=255, nullable=true)
	 */
	private $moduleSubheading;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="module_url", type="string", length=1500, nullable=true)
	 */
	private $moduleUrl;

	/**
	 * @return int
	 */
	public function getIsModel()
	{
		return $this->isModel;
	}

	/**
	 * @param int $isModel
	 */
	public function setIsModel($isModel)
	{
		$this->isModel = $isModel;
	}

	/**
	 * @return int
	 */
	public function getModuleName()
	{
		return $this->moduleName;
	}

	/**
	 * @param int $moduleName
	 */
	public function setModuleName($moduleName)
	{
		$this->moduleName = $moduleName;
	}

	/**
	 * @return int
	 */
	public function getModuleSubheading()
	{
		return $this->moduleSubheading;
	}

	/**
	 * @param int $moduleSubheading
	 */
	public function setModuleSubheading($moduleSubheading)
	{
		$this->moduleSubheading = $moduleSubheading;
	}

	/**
	 * @return int
	 */
	public function getModuleUrl()
	{
		return $this->moduleUrl;
	}

	/**
	 * @param int $moduleUrl
	 */
	public function setModuleUrl($moduleUrl)
	{
		$this->moduleUrl = $moduleUrl;
	}


	/**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set course
     *
     * @param \foreup\rest\models\entities\ForeupCourses $course
     *
     * @return ForeupApiUserCourses
     */
    public function setCourse(\foreup\rest\models\entities\ForeupCourses $course = null)
    {
        $this->course = $course;
    
        return $this;
    }

    /**
     * Get course
     *
     * @return \foreup\rest\models\entities\ForeupCourses
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set user
     *
     * @param \foreup\rest\models\entities\ForeupUsers $user
     *
     * @return ForeupApiUserCourses
     */
    public function setUser(\foreup\rest\models\entities\ForeupUsers $user = null)
    {
        $this->userId = $user->getId();
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \foreup\rest\models\entities\ForeupUsers
     */
    public function getUser()
    {
        return $this->userId;
    }

	/**
	 * @return ForeupEmployees
	 */
	public function getEmployee()
	{
		return $this->employee;
	}

	/**
	 * @param ForeupEmployees $employee
	 */
	public function setEmployee($employee)
	{
		$this->employee = $employee;

		return $this->employee;
	}

	/**
	 * @return int
	 */
	public function getPriceClassId()
	{
		return $this->priceClassId;
	}

	/**
	 * @param int $priceClassId
	 */
	public function setPriceClassId($priceClassId)
	{
		$this->priceClassId = $priceClassId;
	}

	/**
	 * @return ForeupUsers
	 */
	public function getActualUser()
	{
		return $this->actualUser;
	}

	/**
	 * @param ForeupUsers $actualUser
	 */
	public function setActualUser($actualUser)
	{
		$this->actualUser = $actualUser;
	}


}
