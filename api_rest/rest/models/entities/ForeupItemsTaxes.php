<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupItemsTaxes
 *
 * @ORM\Table(name="foreup_items_taxes", indexes={@ORM\Index(name="IDX_25B60A2F126F525E", columns={"item_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupItemsTaxes
{
	use \foreup\rest\models\entities\EntityValidator;

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{
		$this->resetLastError();
		$location = 'ForeupItemsTaxes->validate';

		$courseId = $this->getCourseId();
		$v = $this->validate_integer($location,'courseId',$courseId,true,$throw);
		if($v!==true)return $v;

		$cid = $this->getCid();
		$v = $this->validate_integer($location,'cid',$cid,true,$throw);
		if($v!==true)return $v;

		$name = $this->getName();
		$v = $this->validate_string($location,'name',$name,true,$throw);
		if($v!==true)return $v;

		$percent = $this->getPercent();
		$v = $this->validate_numeric($location,'percent',$percent,true,$throw);
		if($v!==true)return $v;

		$cumulative = $this->getCumulative();
		$v = $this->validate_boolean($location,'cumulative',$cumulative,true,$throw);
		if($v!==true)return $v;

		return true;

	}

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="percent", type="decimal", precision=15, scale=3, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $percent;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CID", type="integer", nullable=false)
     */
    private $cid;

    /**
     * @var integer
     *
     * @ORM\Column(name="cumulative", type="integer", nullable=false)
     */
    private $cumulative = '0';

    /**
     * @var \foreup\rest\models\entities\ForeupItems
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="item_id", referencedColumnName="item_id")
     * })
     */
    private $item;



    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupItemsTaxes
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set percent
     *
     * @param string $percent
     *
     * @return ForeupItemsTaxes
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    
        return $this;
    }

    /**
     * Get percent
     *
     * @return string
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupItemsTaxes
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    
        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return ForeupItemsTaxes
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    
        return $this;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set cumulative
     *
     * @param boolean $cumulative
     *
     * @return ForeupItemsTaxes
     */
    public function setCumulative($cumulative)
    {
	    if(is_string($cumulative)){
            $cumulative = strtolower($cumulative);
		    $ac = json_decode($cumulative,true);
		    if(isset($ac))$cumulative = $ac;
	    }

	    $this->cumulative = $cumulative;

        return $this;
    }

    /**
     * Get cumulative
     *
     * @return integer
     */
    public function getCumulative()
    {
        return $this->cumulative;
    }

    /**
     * Set item
     *
     * @param \foreup\rest\models\entities\ForeupItems $item
     *
     * @return ForeupItemsTaxes
     */
    public function setItem(\foreup\rest\models\entities\ForeupItems $item)
    {
        $this->item = $item;
    
        return $this;
    }

    /**
     * Get item
     *
     * @return \foreup\rest\models\entities\ForeupItems
     */
    public function getItem()
    {
        return $this->item;
    }
}
