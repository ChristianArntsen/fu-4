<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupSalesItems
 *
 * @ORM\Table(name="foreup_sales_items", indexes={@ORM\Index(name="item_id", columns={"item_id"}), @ORM\Index(name="sale_id", columns={"sale_id"}), @ORM\Index(name="pass_id", columns={"pass_id"})})
 * @ORM\Entity
 */
class ForeupSalesItems
{
	public function __construct()
	{
		$this->priceCategory = "";
		$this->type = "item";
		$this->profit = 0;
	}

    /**
     * @var \foreup\rest\models\entities\ForeupItems
     *
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupItems",fetch="EAGER")
     * @ORM\Id
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="item_id", referencedColumnName="item_id", nullable=false)
     * })
     */
    private $item;

    /**
     * @var integer
     *
     * @ORM\Column(name="line", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $line = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_id", type="integer", nullable=false)
     */
    private $invoiceId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="teesheet", type="string", length=255, nullable=false)
     */
    private $teesheet;

    /**
     * @var string
     *
     * @ORM\Column(name="price_category", type="string", length=40, nullable=false)
     */
    private $priceCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="serialnumber", type="string", length=255, nullable=true)
     */
    private $serialnumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="timeframe_id", type="integer", nullable=true)
     */
    private $timeframeId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="special_id", type="integer", nullable=true)
     */
    private $specialId = '0';

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="price_class_id", type="integer", nullable=true)
	 */
	private $priceClassId;

	/**
	 * @var \foreup\rest\models\entities\ForeupPriceClass
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPriceClass")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="price_class_id", referencedColumnName="class_id", nullable=false)
	 * })
	 */
    private $priceClass;

    /**
     * @var string
     *
     * @ORM\Column(name="quantity_purchased", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $quantityPurchased = '0.00';

    /**
     * @var integer
     *
     * @ORM\Column(name="num_splits", type="smallint", nullable=false)
     */
    private $numSplits = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_side", type="boolean", nullable=false)
     */
    private $isSide = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="item_cost_price", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $itemCostPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="item_unit_price", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $itemUnitPrice;

    /**
     * @var boolean
     *
     * @ORM\Column(name="unit_price_includes_tax", type="boolean", nullable=false)
     */
    private $unitPriceIncludesTax;

    /**
     * @var string
     *
     * @ORM\Column(name="discount_percent", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $discountPercent = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="subtotal", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $subtotal;

    /**
     * @var string
     *
     * @ORM\Column(name="tax", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $tax;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="profit", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $profit;

    /**
     * @var string
     *
     * @ORM\Column(name="total_cost", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $totalCost;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receipt_only", type="boolean", nullable=false)
     */
    private $receiptOnly;

    /**
     * @var integer
     *
     * @ORM\Column(name="erange_code", type="integer", nullable=false)
     */
    private $erangeCode = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="pass_id", type="integer", nullable=true)
     */
    private $passId;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_line", type="smallint", nullable=true)
     */
    private $parentLine;

    /**
     * @var \foreup\rest\models\entities\ForeupSales
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupSales", inversedBy="items")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sale_id", referencedColumnName="sale_id")
     * })
     */
    private $sale;


	private $validationMessage;

	/**
	 * @return mixed
	 */
	public function getValidationMessage()
	{
		return $this->validationMessage;
	}

	/**
	 * @param mixed $validationMessage
	 */
	public function setValidationMessage($validationMessage)
	{
		$this->validationMessage = $validationMessage;
	}


	/**
     * @return \foreup\rest\models\entities\ForeupItems
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param ForeupItems $item
     */
    public function setItem($item)
    {
        $this->item = $item;
	    $this->setItemCostPrice($item->getCostPrice());
	    $this->setTotalCost($item->getCostPrice() * $this->getQuantityPurchased());
	    $this->setUnitPriceIncludesTax($item->getUnitPriceIncludesTax());
	    $this->setProfit($this->itemUnitPrice - $this->itemCostPrice);

        return $this;
    }



    /**
     * Set line
     *
     * @param integer $line
     *
     * @return ForeupSalesItems
     */
    public function setLine($line)
    {
        $this->line = $line;

        return $this;
    }

    /**
     * Get line
     *
     * @return integer
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Set invoiceId
     *
     * @param integer $invoiceId
     *
     * @return ForeupSalesItems
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * Get invoiceId
     *
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ForeupSalesItems
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set teesheet
     *
     * @param string $teesheet
     *
     * @return ForeupSalesItems
     */
    public function setTeesheet($teesheet)
    {
        $this->teesheet = $teesheet;

        return $this;
    }

    /**
     * Get teesheet
     *
     * @return string
     */
    public function getTeesheet()
    {
        return $this->teesheet;
    }

    /**
     * Set priceCategory
     *
     * @param string $priceCategory
     *
     * @return ForeupSalesItems
     */
    public function setPriceCategory($priceCategory)
    {
        $this->priceCategory = $priceCategory;

        return $this;
    }

    /**
     * Get priceCategory
     *
     * @return string
     */
    public function getPriceCategory()
    {
        return $this->priceCategory;
    }

    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return ForeupSalesItems
     */
    public function setSerialnumber($serialnumber)
    {
        $this->serialnumber = $serialnumber;

        return $this;
    }

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber()
    {
        return $this->serialnumber;
    }

    /**
     * Set timeframeId
     *
     * @param integer $timeframeId
     *
     * @return ForeupSalesItems
     */
    public function setTimeframeId($timeframeId)
    {
        $this->timeframeId = $timeframeId;

        return $this;
    }

    /**
     * Get timeframeId
     *
     * @return integer
     */
    public function getTimeframeId()
    {
        return $this->timeframeId;
    }

    /**
     * Set specialId
     *
     * @param integer $specialId
     *
     * @return ForeupSalesItems
     */
    public function setSpecialId($specialId)
    {
        $this->specialId = $specialId;

        return $this;
    }

    /**
     * Get specialId
     *
     * @return integer
     */
    public function getSpecialId()
    {
        return $this->specialId;
    }

	/**
	 * @return ForeupPriceClass
	 */
	public function getPriceClass()
	{
		if($this->priceClassId == 0)
			return null;
        $priceClass = null;
        try {
            $priceClass = $this->priceClass;
            $priceClass->getId();
        } catch (EntityNotFoundException $e)
        {
            $priceClass = null;
        }
		return $priceClass;
	}

	/**
	 * @param ForeupPriceClass $priceClass
	 */
	public function setPriceClass($priceClass)
	{
		$this->priceClass = $priceClass;
		return $this;
	}



    /**
     * Set quantityPurchased
     *
     * @param string $quantityPurchased
     *
     * @return ForeupSalesItems
     */
    public function setQuantityPurchased($quantityPurchased)
    {
        $this->quantityPurchased = $quantityPurchased;
	    if(!empty($this->item)){
		    $this->setTotalCost($this->getItem()->getCostPrice() * $this->getQuantityPurchased());
	    }
        return $this;
    }

    /**
     * Get quantityPurchased
     *
     * @return string
     */
    public function getQuantityPurchased()
    {
        return $this->quantityPurchased;
    }

    /**
     * Set numSplits
     *
     * @param integer $numSplits
     *
     * @return ForeupSalesItems
     */
    public function setNumSplits($numSplits)
    {
        $this->numSplits = $numSplits;

        return $this;
    }

    /**
     * Get numSplits
     *
     * @return integer
     */
    public function getNumSplits()
    {
        return $this->numSplits;
    }

    /**
     * Set isSide
     *
     * @param boolean $isSide
     *
     * @return ForeupSalesItems
     */
    public function setIsSide($isSide)
    {
        $this->isSide = $isSide;

        return $this;
    }

    /**
     * Get isSide
     *
     * @return boolean
     */
    public function getIsSide()
    {
        return $this->isSide;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupSalesItems
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set itemCostPrice
     *
     * @param string $itemCostPrice
     *
     * @return ForeupSalesItems
     */
    public function setItemCostPrice($itemCostPrice)
    {
        $this->itemCostPrice = $itemCostPrice;
	    $this->setProfit($this->itemUnitPrice - $this->itemCostPrice);
        return $this;
    }

    /**
     * Get itemCostPrice
     *
     * @return string
     */
    public function getItemCostPrice()
    {
        return $this->itemCostPrice;
    }

    /**
     * Set itemUnitPrice
     *
     * @param string $itemUnitPrice
     *
     * @return ForeupSalesItems
     */
    public function setItemUnitPrice($itemUnitPrice)
    {
        $this->itemUnitPrice = $itemUnitPrice;
	    $this->setProfit($this->itemUnitPrice - $this->itemCostPrice);
        return $this;
    }

    /**
     * Get itemUnitPrice
     *
     * @return string
     */
    public function getItemUnitPrice()
    {
        return $this->itemUnitPrice;
    }

    /**
     * Set unitPriceIncludesTax
     *
     * @param boolean $unitPriceIncludesTax
     *
     * @return ForeupSalesItems
     */
    public function setUnitPriceIncludesTax($unitPriceIncludesTax)
    {
        $this->unitPriceIncludesTax = $unitPriceIncludesTax;

        return $this;
    }

    /**
     * Get unitPriceIncludesTax
     *
     * @return boolean
     */
    public function getUnitPriceIncludesTax()
    {
        return $this->unitPriceIncludesTax;
    }

    /**
     * Set discountPercent
     *
     * @param string $discountPercent
     *
     * @return ForeupSalesItems
     */
    public function setDiscountPercent($discountPercent)
    {
        $this->discountPercent = $discountPercent;

        return $this;
    }

    /**
     * Get discountPercent
     *
     * @return string
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    /**
     * Set subtotal
     *
     * @param string $subtotal
     *
     * @return ForeupSalesItems
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * Get subtotal
     *
     * @return string
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * Set tax
     *
     * @param string $tax
     *
     * @return ForeupSalesItems
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return string
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ForeupSalesItems
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
    	if(!isset($this->total) && isset($this->quantityPurchased) && isset($this->subtotal) && isset($this->tax)) {
            $this->total = (float)$this->getQuantityPurchased() * (float)$this->getSubtotal();

            if(!$this->unitPriceIncludesTax){
                $this->total += (float)$this->getQuantityPurchased() * (float)$this->getTax();
            }
        }

    	return $this->total;
    }

    /**
     * Set profit
     *
     * @param string $profit
     *
     * @return ForeupSalesItems
     */
    public function setProfit($profit)
    {
        $this->profit = $profit;

        return $this;
    }

    /**
     * Get profit
     *
     * @return string
     */
    public function getProfit()
    {
        return $this->profit;
    }

    /**
     * Set totalCost
     *
     * @param string $totalCost
     *
     * @return ForeupSalesItems
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = $totalCost;

        return $this;
    }

    /**
     * Get totalCost
     *
     * @return string
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * Set receiptOnly
     *
     * @param boolean $receiptOnly
     *
     * @return ForeupSalesItems
     */
    public function setReceiptOnly($receiptOnly)
    {
        $this->receiptOnly = $receiptOnly;

        return $this;
    }

    /**
     * Get receiptOnly
     *
     * @return boolean
     */
    public function getReceiptOnly()
    {
        return $this->receiptOnly;
    }

    /**
     * Set erangeCode
     *
     * @param integer $erangeCode
     *
     * @return ForeupSalesItems
     */
    public function setErangeCode($erangeCode)
    {
        $this->erangeCode = $erangeCode;

        return $this;
    }

    /**
     * Get erangeCode
     *
     * @return integer
     */
    public function getErangeCode()
    {
        return $this->erangeCode;
    }

    /**
     * Set passId
     *
     * @param integer $passId
     *
     * @return ForeupSalesItems
     */
    public function setPassId($passId)
    {
        $this->passId = $passId;

        return $this;
    }

    /**
     * Get passId
     *
     * @return integer
     */
    public function getPassId()
    {
        return $this->passId;
    }

    /**
     * Set parentLine
     *
     * @param integer $parentLine
     *
     * @return ForeupSalesItems
     */
    public function setParentLine($parentLine)
    {
        $this->parentLine = $parentLine;

        return $this;
    }

    /**
     * Get parentLine
     *
     * @return integer
     */
    public function getParentLine()
    {
        return $this->parentLine;
    }

    /**
     * Set sale
     *
     * @param \foreup\rest\models\entities\ForeupSales $sale
     *
     * @return ForeupSalesItems
     */
    public function setSale(\foreup\rest\models\entities\ForeupSales $sale)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * Get sale
     *
     * @return \foreup\rest\models\entities\ForeupSales
     */
    public function getSale()
    {
        return $this->sale;
    }

    public function isValid()
    {
    	if($this->getItem() == null){
    		$this->validationMessage = "The item id is required. ";
    		return false;
	    }
    	return true;
    }
}
