<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;
use foreup\rest\exceptions\ValidateException;

/**
 * ForeupAccountPayments
 *
 * @ORM\Table(name="foreup_account_payments", indexes={@ORM\Index(name="person_id", columns={"person_id"}), @ORM\Index(name="organization_id", columns={"organization_id"}), @ORM\Index(name="account_id", columns={"account_id"}), @ORM\Index(name="payment_type_id", columns={"payment_type_id"}), @ORM\Index(name="record_id", columns={"record_id"})})
 * @ORM\Entity (repositoryClass="foreup\rest\models\repositories\PaymentsRepository")
 */
class ForeupAccountPayments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=2, nullable=false, unique=false)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="amount_open", type="decimal", precision=15, scale=2, nullable=false, unique=false)
     */
    private $amountOpen;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateCreated;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $personId;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $organizationId;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=128, precision=0, scale=0, nullable=true, unique=false)
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="record_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="meta", type="string", length=1024, precision=0, scale=0, nullable=true, unique=false)
     */
    private $meta;

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="string", length=4096, precision=0, scale=0, nullable=true, unique=false)
     */
    private $memo;

    /**
     * @var \foreup\rest\models\entities\ForeupAccounts
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccounts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $account;

    /**
     * @var \foreup\rest\models\entities\ForeupAccountLedger
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountLedger")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ledger_item_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $ledgerItem;

    /**
     * @return ForeupAccountLedger
     */
    public function getLedgerItem()
    {
        return $this->ledgerItem;
    }

    /**
     * @param ForeupAccountLedger $ledgerItem
     */
    public function setLedgerItem($ledgerItem)
    {
        $this->ledgerItem = $ledgerItem;
    }


    /**
     * @var \foreup\rest\models\entities\ForeupPaymentTypes
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPaymentTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $paymentType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="foreup\rest\models\entities\ForeupAccountLedger", mappedBy="payment")
     */
    private $ledger;

    /**
     * Constructor
     */
    public function __construct(ForeupAccountLedger $ledgerItem)
    {
        $this->ledgerItem = $ledgerItem;
        $this->ledger = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return ForeupAccountPayments
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return ForeupAccountPayments
     */
    public function setAmountOpen($amount)
    {
        $this->amountOpen = $amount;
        if($this->ledgerItem === null){
            throw new ValidateException("Payments require a ledger item before anything else can happen.");
        }
        $this->getLedgerItem()->setAmountOpen($amount*-1);
        $this->getLedgerItem()->setAmountPaid($this->getLedgerItem()->getAmountPaid() - $amount);

        return $this;
    }


    public function subAmountOpen($amount)
    {
        $this->amountOpen = $this->amountOpen - $amount;
        $this->getLedgerItem()->setAmountOpen($this->amountOpen * -1);
        $this->getLedgerItem()->setAmountPaid(-1 * ($this->amount - $this->amountOpen));

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmountOpen()
    {
        return $this->amountOpen;
    }


    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupAccountPayments
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set personId
     *
     * @param integer $personId
     *
     * @return ForeupAccountPayments
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    
        return $this;
    }

    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     *
     * @return ForeupAccountPayments
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return ForeupAccountPayments
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set recordId
     *
     * @param integer $recordId
     *
     * @return ForeupAccountPayments
     */
    public function setRecordId($recordId)
    {
        $this->recordId = $recordId;
    
        return $this;
    }

    /**
     * Get recordId
     *
     * @return integer
     */
    public function getRecordId()
    {
        return $this->recordId;
    }

    /**
     * Set meta
     *
     * @param string $meta
     *
     * @return ForeupAccountPayments
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
    
        return $this;
    }

    /**
     * Get meta
     *
     * @return string
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set memo
     *
     * @param string $memo
     *
     * @return ForeupAccountPayments
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
    
        return $this;
    }

    /**
     * Get memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set account
     *
     * @param \foreup\rest\models\entities\ForeupAccounts $account
     *
     * @return ForeupAccountPayments
     */
    public function setAccount(\foreup\rest\models\entities\ForeupAccounts $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \foreup\rest\models\entities\ForeupAccounts
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set paymentType
     *
     * @param \foreup\rest\models\entities\ForeupPaymentTypes $paymentType
     *
     * @return ForeupAccountPayments
     */
    public function setPaymentType(\foreup\rest\models\entities\ForeupPaymentTypes $paymentType = null)
    {
        $this->paymentType = $paymentType;
    
        return $this;
    }

    /**
     * Get paymentType
     *
     * @return \foreup\rest\models\entities\ForeupPaymentTypes
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Add ledger
     *
     * @param \foreup\rest\models\entities\ForeupAccountLedger $ledger
     *
     * @return ForeupAccountPayments
     */
    public function addLedger(\foreup\rest\models\entities\ForeupAccountLedger $ledger)
    {
        $this->ledger[] = $ledger;
    
        return $this;
    }

    /**
     * Remove ledger
     *
     * @param \foreup\rest\models\entities\ForeupAccountLedger $ledger
     */
    public function removeLedger(\foreup\rest\models\entities\ForeupAccountLedger $ledger)
    {
        $this->ledger->removeElement($ledger);
    }

    /**
     * Get ledger
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLedger()
    {
        return $this->ledger;
    }

    public function hasBalance()
    {
        return $this->getAmountOpen() > 0;
    }
}

