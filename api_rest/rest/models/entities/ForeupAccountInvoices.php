<?php
namespace foreup\rest\models\entities;



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountInvoices
 *
 * @ORM\Table(name="foreup_account_invoices", uniqueConstraints={@ORM\UniqueConstraint(name="number", columns={"number", "organization_id"})}, indexes={@ORM\Index(name="organization_id", columns={"organization_id"}), @ORM\Index(name="created_by", columns={"created_by"}), @ORM\Index(name="person_id", columns={"person_id"}), @ORM\Index(name="ledger_id", columns={"ledger_id"}), @ORM\Index(name="fk_account_invoices_recurring_charge_id_idx", columns={"recurring_charge_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupAccountInvoices
{
	private $last_error;

	public function getLastError(){
		return $this->last_error;
	}

	public function resetLastError(){
		$this->last_error = null;
	}

	private function invalid($throw = true){
		if($throw)throw new \InvalidArgumentException($this->last_error);
		else return $this->last_error;
	}

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true) {


		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetime", nullable=true)
     */
    private $dueDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true)
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_id", type="integer", nullable=false)
     */
    private $organizationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer", nullable=false)
     */
    private $personId;

    /**
     * @var integer
     *
     * @ORM\Column(name="recurring_charge_id", type="integer", nullable=true)
     */
    private $recurringChargeId;

    /**
     * @var string
     *
     * @ORM\Column(name="subtotal", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $subtotal = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="tax", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $tax = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $total = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="total_paid", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $totalPaid = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="total_open", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $totalOpen = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    private $type = 'invoice';

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="string", length=4096, nullable=true)
     */
    private $memo;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number = '1';

    /**
     * @var \foreup\rest\models\entities\ForeupAccountLedger
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountLedger")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ledger_id", referencedColumnName="id")
     * })
     */
    private $ledger;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupAccountInvoices
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     *
     * @return ForeupAccountInvoices
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return ForeupAccountInvoices
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     *
     * @return ForeupAccountInvoices
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;

        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set personId
     *
     * @param integer $personId
     *
     * @return ForeupAccountInvoices
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;

        return $this;
    }

    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set recurringChargeId
     *
     * @param integer $recurringChargeId
     *
     * @return ForeupAccountInvoices
     */
    public function setRecurringChargeId($recurringChargeId)
    {
        $this->recurringChargeId = $recurringChargeId;

        return $this;
    }

    /**
     * Get recurringChargeId
     *
     * @return integer
     */
    public function getRecurringChargeId()
    {
        return $this->recurringChargeId;
    }

    /**
     * Set subtotal
     *
     * @param string $subtotal
     *
     * @return ForeupAccountInvoices
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * Get subtotal
     *
     * @return string
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * Set tax
     *
     * @param string $tax
     *
     * @return ForeupAccountInvoices
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return string
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ForeupAccountInvoices
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set totalPaid
     *
     * @param string $totalPaid
     *
     * @return ForeupAccountInvoices
     */
    public function setTotalPaid($totalPaid)
    {
        $this->totalPaid = $totalPaid;

        return $this;
    }

    /**
     * Get totalPaid
     *
     * @return string
     */
    public function getTotalPaid()
    {
        return $this->totalPaid;
    }

    /**
     * Set totalOpen
     *
     * @param string $totalOpen
     *
     * @return ForeupAccountInvoices
     */
    public function setTotalOpen($totalOpen)
    {
        $this->totalOpen = $totalOpen;

        return $this;
    }

    /**
     * Get totalOpen
     *
     * @return string
     */
    public function getTotalOpen()
    {
        return $this->totalOpen;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupAccountInvoices
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set memo
     *
     * @param string $memo
     *
     * @return ForeupAccountInvoices
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;

        return $this;
    }

    /**
     * Get memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return ForeupAccountInvoices
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set ledger
     *
     * @param \foreup\rest\models\entities\ForeupAccountLedger $ledger
     *
     * @return ForeupAccountInvoices
     */
    public function setLedger(\foreup\rest\models\entities\ForeupAccountLedger $ledger = null)
    {
        $this->ledger = $ledger;

        return $this;
    }

    /**
     * Get ledger
     *
     * @return \foreup\rest\models\entities\ForeupAccountLedger
     */
    public function getLedger()
    {
        return $this->ledger;
    }
}
