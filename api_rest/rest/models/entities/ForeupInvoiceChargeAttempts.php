<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupStatementChargeAttempts
 *
 * @ORM\Table(name="foreup_invoice_charge_attempts")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupInvoiceChargeAttempts
{
	use \foreup\rest\models\entities\EntityValidator;

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{

		$this->resetLastError();
		$location = 'ForeupStatementChargeAttempts->validate';

		$creditCardCharge = $this->getCreditCardCharge();
		$v = $this->validate_object($location,'creditCardCharge',$creditCardCharge,'foreup\rest\models\entities\ForeupSalesPaymentsCreditCards',true,$throw);
		if($v!==true)return $v;

		$invoice = $this->getStatement();
		$v = $this->validate_object($location,'invoice',$invoice, 'foreup\rest\models\entities\ForeupInvoices',true,$throw);
		if($v!==true)return $v;

		$card = $this->getCreditCard();
		$v = $this->validate_object($location,'creditCard',$card,'foreup\rest\models\entities\ForeupCustomerCreditCards',true,$throw);
		if($v!==true)return $v;

		$amount = $this->getAmount();
		$v = $this->validate_numeric($location,'amount',$amount,true,$throw);
		if($v!==true)return $v;

		$date = $this->getDate();
		$v = $this->validate_object($location,'date',$date,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$success = $this->getSuccess();
		$v = $this->validate_string($location,'success',$success,true,$throw);
		if($v!==true)return $v;

		$status = $this->getStatusMessage();
		$v = $this->validate_string($location,'statusMessage',$status,true,$throw);
		if($v!==true)return $v;

		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="charge_attempt_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $chargeAttemptId;

    /**
     * @var integer
     *
     * @ORM\Column(name="credit_card_charge_id", type="integer", nullable=false)
     */
    private $creditCardChargeId = 0;

	/**
	 * @var \foreup\rest\models\entities\ForeupSalesPaymentsCreditCards
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupSalesPaymentsCreditCards")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="credit_card_charge_id", referencedColumnName="invoice")
	 * })
	 */
	private $creditCardCharge;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_id", type="integer", nullable=false)
     */
    private $invoiceId = 0;

	/**
	 * @var \foreup\rest\models\entities\ForeupInvoices
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupInvoices")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="invoice_id")
	 * })
	 */
	private $invoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="credit_card_id", type="integer", nullable=false)
     */
    private $creditCardId = 0;

	/**
	 * @var \foreup\rest\models\entities\ForeupCustomerCreditCards
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCustomerCreditCards")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="credit_card_id", referencedColumnName="credit_card_id")
	 * })
	 */
	private $creditCard;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $amount = 0.00;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="success", type="string", length=255, nullable=false)
     */
    private $success = '';

    /**
     * @var string
     *
     * @ORM\Column(name="status_message", type="string", length=255, nullable=false)
     */
    private $statusMessage = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="employee_id", type="integer", nullable=false)
     */
    private $employeeId;



	/**
	 * Get chargeAttemptId
	 *
	 * @return integer
	 */
	public function getChargeAttemptId()
	{
		return $this->chargeAttemptId;
	}

	/**
	 * Get chargeAttemptId
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->chargeAttemptId;
	}

	/**
	 * Set creditCardCharge
	 *
	 * @param \foreup\rest\models\entities\ForeupSalesPaymentsCreditCards|null $invoice
	 *
	 * @return ForeupInvoiceChargeAttempts
	 */
	public function setCreditCardCharge(\foreup\rest\models\entities\ForeupSalesPaymentsCreditCards $charge = null)
	{
		$this->creditCardCharge = $charge;

		return $this;
	}

	/**
	 * Get $creditCardCharge
	 *
	 * @return \foreup\rest\models\entities\ForeupSalesPaymentsCreditCards
	 */
	public function getCreditCardCharge()
	{
		return $this->creditCardCharge;
	}

	/**
	 * Set invoice
	 *
	 * @param \foreup\rest\models\entities\ForeupInvoices|null $invoice
	 *
	 * @return ForeupInvoiceChargeAttempts
	 */
	public function setStatement(\foreup\rest\models\entities\ForeupInvoices $invoice = null)
	{
		$this->invoice = $invoice;
		if(method_exists($invoice,'getStatementId')){
			$invoiceId = $invoice->getStatementId();
			$this->invoiceId = isset($invoiceId)?$invoiceId:0;
		}

		return $this;
	}

	/**
	 * Get invoice
	 *
	 * @return \foreup\rest\models\entities\ForeupInvoices
	 */
	public function getStatement()
	{
		return $this->invoice;
	}

    /**
     * Get invoiceId
     *
     * @return integer
     */
    public function getStatementId()
    {
        return $this->invoiceId;
    }

	/**
	 * Set creditCard
	 *
	 * @param \foreup\rest\models\entities\ForeupCustomerCreditCards $card
	 *
	 * @return ForeupInvoiceChargeAttempts
	 */
	public function setCreditCard(\foreup\rest\models\entities\ForeupCustomerCreditCards $card = null)
	{
		$this->creditCard = $card;

		return $this;
	}

	/**
	 * Get creditCard
	 *
	 * @return \foreup\rest\models\entities\ForeupCustomerCreditCards
	 */
	public function getCreditCard()
	{
		return $this->creditCard;
	}


	/**
     * Set amount
     *
     * @param string $amount
     *
     * @return ForeupInvoiceChargeAttempts
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ForeupInvoiceChargeAttempts
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set success
     *
     * @param string $success
     *
     * @return ForeupInvoiceChargeAttempts
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    
        return $this;
    }

    /**
     * Get success
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * Set statusMessage
     *
     * @param string $statusMessage
     *
     * @return ForeupInvoiceChargeAttempts
     */
    public function setStatusMessage($statusMessage)
    {
        $this->statusMessage = $statusMessage;
    
        return $this;
    }

    /**
     * Get statusMessage
     *
     * @return string
     */
    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

	/**
	 * Set employee
	 *
	 * @param \foreup\rest\models\entities\ForeupEmployees $employee
	 *
	 * @return ForeupInvoiceChargeAttempts
	 */
	public function setEmployee(\foreup\rest\models\entities\ForeupEmployees $employee = null)
	{
		if(!isset($employee))$this->employeeId = 0;
		$this->employee = $employee;

		return $this;
	}

	/**
	 * Get employee
	 *
	 * @return \foreup\rest\models\entities\ForeupEmployees|null
	 */
	public function getEmployee()
	{
		if($this->employeeId == 0)return null;
		return $this->employee;
	}
}
