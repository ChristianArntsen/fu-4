<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupPersonAltered
 *
 * @ORM\Table(name="foreup_person_altered", uniqueConstraints={@ORM\UniqueConstraint(name="UID", columns={"employee_audit_log_id", "person_edited"})}, indexes={@ORM\Index(name="foreup_person_altered_ibfk_2", columns={"person_edited"}), @ORM\Index(name="IDX_2499CA82E78C5A1D", columns={"employee_audit_log_id"})})
 * @ORM\Entity (repositoryClass="foreup\rest\models\repositories\PersonAlteredRepository")
 */
class ForeupPersonAltered
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tables_updated", type="string", length=256, nullable=true)
     */
    private $tablesUpdated;

    /**
     * @var \foreup\rest\models\entities\ForeupEmployeeAuditLog
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployeeAuditLog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="employee_audit_log_id", referencedColumnName="id")
     * })
     */
    private $employeeAuditLog;

    /**
     * @var \foreup\rest\models\entities\ForeupPeople
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_edited", referencedColumnName="person_id")
     * })
     */
    private $personEdited;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return ForeupPersonAltered
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTablesUpdated()
    {
        return $this->tablesUpdated;
    }

    /**
     * @param string $tablesUpdated
     *
     * @return ForeupPersonAltered
     */
    public function setTablesUpdated($tablesUpdated)
    {
        $this->tablesUpdated = $tablesUpdated;
        return $this;
    }

    /**
     * @return \foreup\rest\models\entities\ForeupEmployeeAuditLog
     */
    public function getEmployeeAuditLog()
    {
        return $this->employeeAuditLog;
    }

    /**
     * @param \foreup\rest\models\entities\ForeupEmployeeAuditLog $employeeAuditLog
     *
     * @return ForeupPersonAltered
     */
    public function setEmployeeAuditLog($employeeAuditLog)
    {
        $this->employeeAuditLog = $employeeAuditLog;
        return $this;
    }

    /**
     * @return \foreup\rest\models\entities\ForeupPeople
     */
    public function getPersonEdited()
    {
        return $this->personEdited;
    }

    /**
     * @param \foreup\rest\models\entities\ForeupPeople $personEdited
     *
     * @return ForeupPersonAltered
     */
    public function setPersonEdited($personEdited)
    {
        $this->personEdited = $personEdited;
        return $this;
    }


}

