<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupCoursesInformationWebsiteSteps
 *
 * @ORM\Table(name="foreup_courses_information_website_steps")
 * @ORM\Entity
 */
class ForeupCoursesInformationWebsiteSteps
{
    /**
     * @var integer
     *
     * @ORM\Column(name="step_number", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $stepNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="step_name", type="string", length=255, nullable=false)
     */
    private $stepName;


    /**
     * Get stepNumber
     *
     * @return integer
     */
    public function getStepNumber()
    {
        return $this->stepNumber;
    }

    /**
     * Set stepName
     *
     * @param string $stepName
     *
     * @return ForeupCoursesInformationWebsiteSteps
     */
    public function setStepName($stepName)
    {
        $this->stepName = $stepName;

        return $this;
    }

    /**
     * Get stepName
     *
     * @return string
     */
    public function getStepName()
    {
        return $this->stepName;
    }
}

