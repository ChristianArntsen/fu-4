<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupStatementChanges
 *
 * @ORM\Table(name="foreup_invoice_changes", indexes={@ORM\Index(name="invoice_id", columns={"invoice_id", "course_id", "person_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupInvoiceChanges
{
	use \foreup\rest\models\entities\EntityValidator;

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{
		$this->resetLastError();
		$location = 'ForeupStatementChanges->validate';

		$invoiceId = $this->getStatementId();
		$v = $this->validate_integer($location,'statementId',$invoiceId,true,$throw);
		if($v!==true)return $v;

		$invoice = $this->getStatement();
		$v = $this->validate_object($location,'statement',$invoice, '\foreup\rest\models\entities\ForeupInvoices',true,$throw);
		if($v!==true)return $v;

		$courseId = $this->getCourseId();
		$v = $this->validate_integer($location,'courseId',$courseId,true,$throw);
		if($v!==true)return $v;

		$person = $this->getPerson();
		$v = $this->validate_object($location,'person',$person,'\foreup\rest\models\entities\ForeupCustomers',true,$throw);
		if($v!==true)return $v;

		$personId = $this->getPersonId();
		$v = $this->validate_integer($location,'personId',$personId,true,$throw);
		if($v!==true)return $v;

		$date = $this->getDate();
		$v = $this->validate_object($location,'date',$date,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$previousPaid = $this->getPreviousPaid();
		$v = $this->validate_numeric($location,'previousPaid',$previousPaid,true,$throw);
		if($v!==true)return $v;

		$paid = $this->getPaid();
		$v = $this->validate_numeric($location,'paid',$paid,true,$throw);
		if($v!==true)return $v;

		$previousOverdue = $this->getPreviousOverdue();
		$v = $this->validate_numeric($location,'previousOverdue',$previousOverdue,true,$throw);
		if($v!==true)return $v;

		$overdue = $this->getOverdue();
		$v = $this->validate_numeric($location,'overdue',$overdue,true,$throw);
		if($v!==true)return $v;

		$notes = $this->getNotes();
		$v = $this->validate_string($location,'notes',$notes,true,$throw);
		if($v!==true)return $v;

		$previousDate = $this->getPreviousDate();
		$v = $this->validate_object($location,'previousDate',$previousDate,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$newDate = $this->getNewDate();
		$v = $this->validate_object($location,'newDate',$newDate,'\DateTime',false,$throw);
		if($v!==true)return $v;


		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_id", type="integer", nullable=false)
     */
    private $invoiceId;

	/**
	 * @var \foreup\rest\models\entities\ForeupInvoices
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupInvoices")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="invoice_id")
	 * })
	 */
	private $invoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer", nullable=false)
     */
    private $personId;

	/**
	 * @var \foreup\rest\models\entities\ForeupCustomers
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCustomers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
	 * })
	 */
	private $person;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date = 'CURRENT_TIMESTAMP';

    /**
     * @var float
     *
     * @ORM\Column(name="previous_paid", type="float", precision=15, scale=2, nullable=false)
     */
    private $previousPaid = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="paid", type="float", precision=15, scale=2, nullable=false)
     */
    private $paid = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="previous_overdue", type="float", precision=15, scale=2, nullable=false)
     */
    private $previousOverdue = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="overdue", type="float", precision=15, scale=2, nullable=false)
     */
    private $overdue = 0.00;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=255, nullable=false)
     */
    private $notes = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="previous_date", type="datetime", nullable=true)
     */
    private $previousDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="new_date", type="datetime", nullable=true)
     */
    private $newDate;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get invoiceId
     *
     * @return integer
     */
    public function getStatementId()
    {
        return $this->invoiceId;
    }

	/**
	 * Set invoice
	 *
	 * @param \foreup\rest\models\entities\ForeupInvoices|null $invoice
	 *
	 * @return ForeupInvoiceChanges
	 */
	public function setStatement(\foreup\rest\models\entities\ForeupInvoices $invoice)
	{
		$this->invoice = $invoice;
		if(method_exists($invoice,'getStatementId')){
			$invoiceId = $invoice->getStatementId();
			$this->invoiceId = isset($invoiceId)?$invoiceId:0;
		}

		return $this;
	}

	/**
	 * Get invoice
	 *
	 * @return \foreup\rest\models\entities\ForeupInvoices
	 */
	public function getStatement()
	{
		if($this->invoiceId == 0) return null;
		return $this->invoice;
	}

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupInvoiceChanges
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    
        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

	/**
	 * Set person
	 *
	 * @param \foreup\rest\models\entities\ForeupCustomers $person
	 *
	 * @return ForeupInvoiceChanges
	 */
	public function setPerson(\foreup\rest\models\entities\ForeupCustomers $person)
	{
		$this->personId = $person->getPerson()->getPersonId();
		$this->courseId = $person->getCourseId();
		$this->person = $person;

		return $this;
	}

	/**
	 * Get person
	 *
	 * @return \foreup\rest\models\entities\ForeupCustomers
	 */
	public function getPerson()
	{
		return $this->person;
	}

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ForeupInvoiceChanges
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
    	if($this->date==='CURRENT_TIMESTAMP')
    		$this->date = new \DateTime();
        return $this->date;
    }

    /**
     * Set previousPaid
     *
     * @param float $previousPaid
     *
     * @return ForeupInvoiceChanges
     */
    public function setPreviousPaid($previousPaid)
    {
        $this->previousPaid = $previousPaid;
    
        return $this;
    }

    /**
     * Get previousPaid
     *
     * @return float
     */
    public function getPreviousPaid()
    {
        return $this->previousPaid;
    }

    /**
     * Set paid
     *
     * @param float $paid
     *
     * @return ForeupInvoiceChanges
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    
        return $this;
    }

    /**
     * Get paid
     *
     * @return float
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set previousOverdue
     *
     * @param float $previousOverdue
     *
     * @return ForeupInvoiceChanges
     */
    public function setPreviousOverdue($previousOverdue)
    {
        $this->previousOverdue = $previousOverdue;
    
        return $this;
    }

    /**
     * Get previousOverdue
     *
     * @return float
     */
    public function getPreviousOverdue()
    {
        return $this->previousOverdue;
    }

    /**
     * Set overdue
     *
     * @param float $overdue
     *
     * @return ForeupInvoiceChanges
     */
    public function setOverdue($overdue)
    {
        $this->overdue = $overdue;
    
        return $this;
    }

    /**
     * Get overdue
     *
     * @return float
     */
    public function getOverdue()
    {
        return $this->overdue;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return ForeupInvoiceChanges
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set previousDate
     *
     * @param \DateTime $previousDate
     *
     * @return ForeupInvoiceChanges
     */
    public function setPreviousDate($previousDate)
    {
        $this->previousDate = $previousDate;
    
        return $this;
    }

    /**
     * Get previousDate
     *
     * @return \DateTime
     */
    public function getPreviousDate()
    {
        return $this->previousDate;
    }

    /**
     * Set newDate
     *
     * @param \DateTime $newDate
     *
     * @return ForeupInvoiceChanges
     */
    public function setNewDate($newDate)
    {
        $this->newDate = $newDate;
    
        return $this;
    }

    /**
     * Get newDate
     *
     * @return \DateTime
     */
    public function getNewDate()
    {
        return $this->newDate;
    }
}
