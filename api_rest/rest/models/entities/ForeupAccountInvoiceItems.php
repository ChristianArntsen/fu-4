<?php
namespace foreup\rest\models\entities;



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountInvoiceItems
 *
 * @ORM\Table(name="foreup_account_invoice_items", indexes={@ORM\Index(name="item_id", columns={"item_id"}), @ORM\Index(name="invoice_id", columns={"invoice_id"})})
 * @ORM\Entity
 */
class ForeupAccountInvoiceItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="line_number", type="integer", nullable=false)
     */
    private $lineNumber = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false)
     */
    private $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="item_type", type="string", nullable=true)
     */
    private $itemType = 'item';

    /**
     * @var string
     *
     * @ORM\Column(name="unit_price", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $unitPrice = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="discount_percent", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $discountPercent = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="quantity", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $quantity = '1.00';

    /**
     * @var string
     *
     * @ORM\Column(name="subtotal", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $subtotal = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="tax", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $tax = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $total = '0.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="taxable", type="boolean", nullable=false)
     */
    private $taxable = '1';

    /**
     * @var \foreup\rest\models\entities\ForeupAccountInvoices
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountInvoices")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     * })
     */
    private $invoice;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lineNumber
     *
     * @param integer $lineNumber
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setLineNumber($lineNumber)
    {
        $this->lineNumber = $lineNumber;

        return $this;
    }

    /**
     * Get lineNumber
     *
     * @return integer
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set itemType
     *
     * @param string $itemType
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setItemType($itemType)
    {
        $this->itemType = $itemType;

        return $this;
    }

    /**
     * Get itemType
     *
     * @return string
     */
    public function getItemType()
    {
        return $this->itemType;
    }

    /**
     * Set unitPrice
     *
     * @param string $unitPrice
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    /**
     * Get unitPrice
     *
     * @return string
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Set discountPercent
     *
     * @param string $discountPercent
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setDiscountPercent($discountPercent)
    {
        $this->discountPercent = $discountPercent;

        return $this;
    }

    /**
     * Get discountPercent
     *
     * @return string
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set subtotal
     *
     * @param string $subtotal
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * Get subtotal
     *
     * @return string
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * Set tax
     *
     * @param string $tax
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return string
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set taxable
     *
     * @param boolean $taxable
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setTaxable($taxable)
    {
        $this->taxable = $taxable;

        return $this;
    }

    /**
     * Get taxable
     *
     * @return boolean
     */
    public function getTaxable()
    {
        return $this->taxable;
    }

    /**
     * Set invoice
     *
     * @param \foreup\rest\models\entities\ForeupAccountInvoices $invoice
     *
     * @return ForeupAccountInvoiceItems
     */
    public function setInvoice(\foreup\rest\models\entities\ForeupAccountInvoices $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \foreup\rest\models\entities\ForeupAccountInvoices
     */
    public function getInvoice()
    {
        return $this->invoice;
    }
}
