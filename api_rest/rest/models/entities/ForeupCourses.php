<?php
namespace foreup\rest\models\entities;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
/**
 * ForeupCourses
 *
 * @ORM\Table(name="foreup_courses")
 * @ORM\Entity (repositoryClass="foreup\rest\models\repositories\CoursesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ForeupCourses
{
	use EntityValidator;

	public $boolean_is = [['boolean','ActiveCourse',true],['boolean','Demo',true],['boolean','MaintenanceMode',true],
		['boolean','AutoMailers',true],['boolean','Events',true],['boolean','FoodAndBeverage',true],
		['boolean','FoodAndBeverageV2',false],['boolean','Passes',true],['boolean','Invoices',true],
		['boolean','Quickbooks',true],['boolean','Recipients',true],['boolean','Sales',true],
		['boolean','Schedules',true],['boolean','Tournaments',true],
		['boolean','UnitPriceIncludesTax',true],['boolean','DisplayEmailAds',true],
		['boolean','HideBackNine',true],['boolean','PrintAfterSale',true],
		['boolean','Webprnt',true],['boolean','PrintTwoReceipts',true],['boolean','PrintTwoSignatureSlips',true],
		['boolean','PrintTwoReceiptsOther',true],['boolean','TipLine',true],
		['boolean','UpdatedPrinting',true],['boolean','AfterSaleLoad',true],['boolean','FnbLogin',true],
		['boolean','TeesheetUpdatesAutomatically',true],['boolean','AutoSplitTeetimes',true],
		['boolean','SendReservationConfirmations',true],
		['boolean','PrintCreditCardReceipt',true],['boolean','PrintSalesReceipt',true],
		['boolean','PrintTipLine',true],['boolean','CashDrawerOnCash',true],['boolean','TrackCash',true],
		['boolean','BlindClose',true],['boolean','SeparateCourses',true],['boolean','DeductTips',true],
		['boolean','UseTerminals',true],['boolean','UseLoyalty',true],['boolean','LoyaltyAutoEnroll',true],['boolean','OpenSun',true],
		['boolean','OpenMon',true],['boolean','OpenTue',true],['boolean','OpenWed',true],
		['boolean','OpenThu',true],['boolean','OpenFri',true],['boolean','OpenSat',true],
		['boolean','WeekendFri',true],['boolean','WeekendSat',true],['boolean','WeekendSun',true],
		['boolean','Simulator',true],['boolean','TestCourse',true],['boolean','PaymentRequired',true],
		['boolean','SendEmailReminder',true],
		['boolean','SendTextReminder',true],['boolean','SeasonalPricing',true],
		['boolean','IncludeTaxOnlineBooking',true],['boolean','HideEmployeeLastNameReceipt',true],
		['boolean','SalesV2',false],['boolean','RequireEmployeePin',true],['boolean','CleanOutNightly',true],
		['boolean','UseCourseFiring',true],['boolean','CourseFiringIncludeItems',true],
		['boolean','TeeSheetSpeedUp',true],['boolean','UseKitchenBuzzers',true],
		['boolean','HideModifierNamesKitchenReceipts',true],['boolean','FoodBevSortBySeat',false],
		['boolean','ServiceFeeActive',true],['boolean','PrintSuggestedTip',true],['boolean','MarketingTexting',true],
		['boolean','StackTeeSheets',true],['boolean','AlwaysListAll',true],
		['boolean','FoodBevTipLineFirstReceipt',true],['boolean','HideRegistration',true],
		['boolean','MobileAppActive',true],['boolean','AutoEmailReceipt',true],
		['boolean','AutoEmailNoPrint',true],
		['boolean','RequireSignatureMemberPayments',true],['boolean','HideTaxable',true],
		['boolean','AllowEmployeeRegisterLogBypass',true],['boolean','TeetimeDefaultToPlayer1',true],
		['boolean','PrintTeeTimeDetails',true],['boolean','ReceiptPrintAccountBalance',true],
		['boolean','PrintMinimumsOnReceipt',true],['boolean','LimitFeeDropdownByCustomer',true],
		['boolean','RequireCustomerOnSale',true],['boolean','CashDrawerOnSale',true],
		['boolean','UseNewPermissions',true],['boolean','SideBySideTeeSheets',true],
		['boolean','CurrentDayCheckinsOnly',true],
		['boolean','OnlineGiftcardPurchases',true],['boolean','ShowInvoiceSaleItems',false]];

	public $mixed_get_set = [['integer','CourseId',true],
		['datetime','MobileAppRefreshTimestamp',false],['string','MobileAppAndroidPublishedVersion',true],
		['string','MobileAppIosPublishedVersion',true],['string','CourseSummary',true],
		['string','MobileAppSummary',true],['string','MobileAppShortTitle',true],
		['string','MobileTestFlightEmail',true],['string','MobileAppIconUrl',true],
		['string','BaseColor',true],['integer','AreaId',true],['integer','Cid',true],
		['integer','CourseAppDataId',true],['string','Name',true],
		['string','Address',true],['string','City',true],['string','State',true],
		['string','StateName',true],['string','Postal',true],['string','Woeid',true],['string','Zip',true],
		['string','Country',true],['string','AreaCode',true],['string','Timezone',true],['string','Dst',true],
		['numeric','LatitudeCentroid',true],['string','LatitudePoly',true],['numeric','LongitudeCentroid',true],
		['string','LongitudePoly',true],['string','Phone',true],['string','Holes',true],['string','Type',true],
		['string','County',true],
		['string','OpenTime',true],['string','CloseTime',true],['string','Increment',true],
		['string','EarlyBirdHoursBegin',true],['string','EarlyBirdHoursEnd',true],
		['string','MorningHoursBegin',true],
		['string','MorningHoursEnd',true],['string','AfternoonHoursBegin',true],
		['string','AfternoonHoursEnd',true],['string','TwilightHour',true],
		['string','SuperTwilightHour',true],
		['boolean','Holidays',true],['boolean','OnlineBooking',true],
		['boolean','OnlineBookingProtected',true],['integer','MinRequiredPlayers',true],
		['integer','MinRequiredHoles',true],
		['string','BookingRules',true],['boolean','Config',true],['boolean','Courses',true],
		['boolean','Customers',true],['boolean','Dashboards',true],['boolean','Employees',true],
		['boolean','Giftcards',true],['boolean','ItemKits',true],
		['boolean','Items',true],['boolean','MarketingCampaigns',true],['boolean','Promotions',true],
		['boolean','Receivings',true],['boolean','Reservations',true],['boolean','Suppliers',true],
		['string','Frontnine',true],
		/*['boolean','Teesheets',true],*/['string','CurrencySymbol',true],
		['string','CurrencySymbolPlacement',true],['string','CurrencyFormat',true],
		['string','CompanyLogo',true],
		['string','DateFormat',true],['string','DefaultTax1Name',true],
		['numeric','DefaultTax1Rate',true],
		['boolean','DefaultTax2Cumulative',true],['string','DefaultTax2Name',true],
		['numeric','DefaultTax2Rate',true],['string','CustomerCreditNickname',true],
		['boolean','CreditDepartment',true],['string','CreditDepartmentName',true],
		['boolean','CreditCategory',true],['string','CreditCategoryName',true],
		['boolean','CreditLimitToSubtotal',true],
		['string','MemberBalanceNickname',true],['string','Email',true],
		['string','ReservationEmail',true],['string','BillingEmail',true],['datetime','LastInvoicesSent',true],
		['string','Fax',true],['string','Language',true],['string','MailchimpApiKey',true],
		['string','NumberOfItemsPerPage',true],
		['string','WebprntIp',true],['string','WebprntHotIp',true],['string','WebprntColdIp',true],
		['string','WebprntLabelIp',true],['integer','TeesheetRefreshRate',true],
		['integer','OnlinePurchaseTerminalId',true],
		['integer','OnlineInvoiceTerminalId',true],['string','ReturnPolicy',true],['string','TimeFormat',true],
		['string','Website',true],['string','AtLogin',true],['string','AtPassword',true],
		['boolean','AtTest',true],
		['string','MercuryId',true],['string','MercuryPassword',true],['string','MercuryE2eId',true],
		['string','MercuryE2ePassword',true],['string','EtsKey',true],['string','E2eAccountId',true],
		['string','E2eAccountKey',true],
		['boolean','UseEtsGiftcards',true],['string','FacebookPageId',true],['string','FacebookPageName',true],
		['string','FacebookExtendedAccessToken',true],['boolean','AllowFriendsToInvite',true],
		['numeric','ForeupDiscountPercent',true],['string','NoShowPolicy',true],
		['string','ReservationEmailText',true],['integer','ReservationEmailPhoto',true],
		['integer','IbeaconMajorId',true],
		['boolean','IbeaconEnabled',true], ['numeric','AutoGratuity',true],['numeric','MinimumFoodSpend',true],
		['string','QuickbooksExportSettings',true],['string','ErangeId',true],['string','ErangePassword',true],
		['numeric','CreditCardFee',true],['boolean','MultiplePrinters',true],
		['boolean','RequireGuestCount',true],['integer','DefaultKitchenPrinter',true],
		['string','ElementAccountId',false],
		['string','ElementAccountToken',false],['string','ElementApplicationId',false],
		['string','ElementAcceptorId',false],['boolean','DisableFacebookWidget',true],
		['string','ServiceFeeTax1Name',true],['numeric','ServiceFeeTax1Rate',true],
		['string','ServiceFeeTax2Name',true],['numeric','ServiceFeeTax2Rate',true],
		['integer','AutoGratuityThreshold',true],
		['numeric','DefaultRegisterLogOpen',true],['string','CustomerFieldSettings',true],
		['string','TermsAndConditions',true],['string','OnlineBookingWelcomeMessage',true],
		/*'Category','Department',
		'Modules','ApiUserCourses',*/['string','TeedOffColor',false],['string','TeeTimeCompletedColor',false],['string','ReceiptPrinter',true]];

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{
		$location = 'ForeupCourses->validate';

		foreach ($this->boolean_is as $field) {
			$value = $this->{'is'.$field[1]}();
			$v = $this->validate_boolean($location, lcfirst($field[1]), $value, $field[2], $throw);
			if ($v !== true) return $v;
		}

		foreach($this->mixed_get_set as $field){
			$value = $this->{'get'.$field[1]}();
			if($field[0]==='datetime'){
				$v = $this->validate_object($location,lcfirst($field[1]),$value,'\DateTime',$field[2],$throw);
			}
			else{
				$v = $this->{'validate_'.$field[0]}($location, lcfirst($field[1]), $value, $field[2], $throw);
			}
			if ($v !== true) return $v;
		}

		return true;

	}

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $courseId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active_course", type="boolean", nullable=false)
     */
    private $activeCourse = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="demo", type="boolean", nullable=false)
     */
    private $demo;


    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var integer
     *
     * @ORM\Column(name="area_id", type="integer", nullable=false)
     */
    private $areaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CID", type="integer", nullable=false)
     */
    private $cid;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_app_data_id", type="integer", nullable=false)
     */
    private $courseAppDataId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=40, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=40, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=20, nullable=false)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="state_name", type="string", length=50, nullable=false)
     */
    private $stateName;

    /**
     * @var string
     *
     * @ORM\Column(name="postal", type="string", length=10, nullable=false)
     */
    private $postal;

    /**
     * @var string
     *
     * @ORM\Column(name="woeid", type="string", length=25, nullable=false)
     */
    private $woeid;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=10, nullable=false)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=40, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="area_code", type="string", length=10, nullable=false)
     */
    private $areaCode;

    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string", length=20, nullable=false)
     */
    private $timezone;

    /**
     * @var string
     *
     * @ORM\Column(name="DST", type="string", length=20, nullable=false)
     */
    private $dst;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude_centroid", type="float", precision=15, scale=2, nullable=false)
     */
    private $latitudeCentroid;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude_poly", type="string", length=30, nullable=false)
     */
    private $latitudePoly;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude_centroid", type="float", precision=15, scale=6, nullable=false)
     */
    private $longitudeCentroid;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude_poly", type="string", length=30, nullable=false)
     */
    private $longitudePoly;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="holes", type="string", length=10, nullable=false)
     */
    private $holes;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="county", type="string", length=40, nullable=false)
     */
    private $county;

    /**
     * @var string
     *
     * @ORM\Column(name="open_time", type="string", length=10, nullable=false)
     */
    private $openTime = '0600';

    /**
     * @var string
     *
     * @ORM\Column(name="close_time", type="string", length=10, nullable=false)
     */
    private $closeTime = '1900';

    /**
     * @var string
     *
     * @ORM\Column(name="increment", type="string", length=10, nullable=false)
     */
    private $increment = '8';

    /**
     * @var string
     *
     * @ORM\Column(name="frontnine", type="string", length=10, nullable=false)
     */
    private $frontnine = '200';

    /**
     * @var string
     *
     * @ORM\Column(name="early_bird_hours_begin", type="string", length=10, nullable=false)
     */
    private $earlyBirdHoursBegin;

    /**
     * @var string
     *
     * @ORM\Column(name="early_bird_hours_end", type="string", length=10, nullable=false)
     */
    private $earlyBirdHoursEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="morning_hours_begin", type="string", length=10, nullable=false)
     */
    private $morningHoursBegin;

    /**
     * @var string
     *
     * @ORM\Column(name="morning_hours_end", type="string", length=10, nullable=false)
     */
    private $morningHoursEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="afternoon_hours_begin", type="string", length=10, nullable=false)
     */
    private $afternoonHoursBegin;

    /**
     * @var string
     *
     * @ORM\Column(name="afternoon_hours_end", type="string", length=10, nullable=false)
     */
    private $afternoonHoursEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="twilight_hour", type="string", length=10, nullable=false)
     */
    private $twilightHour = '1400';

    /**
     * @var string
     *
     * @ORM\Column(name="super_twilight_hour", type="string", length=10, nullable=false)
     */
    private $superTwilightHour = '2399';

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="holidays", type="boolean", nullable=false)
	 */
	private $holidays;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="maintenance_mode", type="boolean", nullable=false)
	 */
	private $maintenanceMode=false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="recipients", type="boolean", nullable=false)
	 */
	private $recipients=false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="display_email_ads", type="boolean", nullable=false)
	 */
	private $displayEmailAds=false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="online_booking", type="boolean", nullable=false)
     */
    private $onlineBooking = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="online_booking_protected", type="boolean", nullable=false)
     */
    private $onlineBookingProtected;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_required_players", type="integer", nullable=false)
     */
    private $minRequiredPlayers = '2';

    /**
     * @var integer
     *
     * @ORM\Column(name="min_required_carts", type="integer", nullable=false)
     */
    private $minRequiredCarts;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_required_holes", type="integer", nullable=false)
     */
    private $minRequiredHoles;

    /**
     * @var string
     *
     * @ORM\Column(name="booking_rules", type="text", length=65535, nullable=false)
     */
    private $bookingRules;

    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_mailers", type="boolean", nullable=false)
     */
    private $autoMailers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="config", type="boolean", nullable=false)
     */
    private $config = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="courses", type="boolean", nullable=false)
     */
    private $courses = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="customers", type="boolean", nullable=false)
     */
    private $customers = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="dashboards", type="boolean", nullable=false)
     */
    private $dashboards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="employees", type="boolean", nullable=false)
     */
    private $employees = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="events", type="boolean", nullable=false)
     */
    private $events;

    /**
     * @var boolean
     *
     * @ORM\Column(name="food_and_beverage", type="boolean", nullable=false)
     */
    private $foodAndBeverage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="giftcards", type="boolean", nullable=false)
     */
    private $giftcards = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="passes", type="boolean", nullable=false)
     */
    private $passes = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="invoices", type="boolean", nullable=false)
     */
    private $invoices;

    /**
     * @var boolean
     *
     * @ORM\Column(name="item_kits", type="boolean", nullable=false)
     */
    private $itemKits = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="items", type="boolean", nullable=false)
     */
    private $items = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="marketing_campaigns", type="boolean", nullable=false)
     */
    private $marketingCampaigns = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="promotions", type="boolean", nullable=false)
     */
    private $promotions = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="quickbooks", type="boolean", nullable=false)
     */
    private $quickbooks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receivings", type="boolean", nullable=false)
     */
    private $receivings = '0';



    /**
     * @var boolean
     *
     * @ORM\Column(name="reports", type="boolean", nullable=false)
     */
    private $reports = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="reservations", type="boolean", nullable=false)
     */
    private $reservations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sales", type="boolean", nullable=false)
     */
    private $sales = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="schedules", type="boolean", nullable=false)
     */
    private $schedules;

    /**
     * @var boolean
     *
     * @ORM\Column(name="suppliers", type="boolean", nullable=false)
     */
    private $suppliers = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="teesheets", type="boolean", nullable=false)
     */
    private $teesheets = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="tournaments", type="boolean", nullable=false)
     */
    private $tournaments = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="currency_symbol", type="string", length=3, nullable=false)
     */
    private $currencySymbol = '$';

    /**
     * @var string
     *
     * @ORM\Column(name="currency_symbol_placement", type="string", length=20, nullable=false)
     */
    private $currencySymbolPlacement;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_format", type="string", length=20, nullable=false)
     */
    private $currencyFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="company_logo", type="string", length=255, nullable=false)
     */
    private $companyLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="date_format", type="string", length=20, nullable=false)
     */
    private $dateFormat = 'middle_endian';

    /**
     * @var string
     *
     * @ORM\Column(name="default_tax_1_name", type="string", length=255, nullable=false)
     */
    private $defaultTax1Name = 'Sales Tax';

    /**
     * @var string
     *
     * @ORM\Column(name="default_tax_1_rate", type="string", length=20, nullable=false)
     */
    private $defaultTax1Rate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="default_tax_2_cumulative", type="string", length=20, nullable=false)
     */
    private $defaultTax2Cumulative = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="default_tax_2_name", type="string", length=255, nullable=false)
     */
    private $defaultTax2Name = 'Sales Tax 2';

    /**
     * @var string
     *
     * @ORM\Column(name="default_tax_2_rate", type="string", length=20, nullable=false)
     */
    private $defaultTax2Rate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="unit_price_includes_tax", type="boolean", nullable=false)
     */
    private $unitPriceIncludesTax;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_credit_nickname", type="string", length=255, nullable=false)
     */
    private $customerCreditNickname;

    /**
     * @var boolean
     *
     * @ORM\Column(name="credit_department", type="boolean", nullable=false)
     */
    private $creditDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="credit_department_name", type="string", length=255, nullable=false)
     */
    private $creditDepartmentName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="credit_category", type="boolean", nullable=false)
     */
    private $creditCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="credit_category_name", type="string", length=255, nullable=false)
     */
    private $creditCategoryName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="credit_subtotal_only", type="boolean", nullable=false)
     */
    private $creditLimitToSubtotal;

    /**
     * @var string
     *
     * @ORM\Column(name="member_balance_nickname", type="string", length=255, nullable=false)
     */
    private $memberBalanceNickname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="reservation_email", type="string", length=255, nullable=false)
     */
    private $reservationEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_email", type="string", length=255, nullable=false)
     */
    private $billingEmail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_invoices_sent", type="date", nullable=false)
     */
    private $lastInvoicesSent;


    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=40, nullable=false)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=20, nullable=false)
     */
    private $language = 'english';

    /**
     * @var string
     *
     * @ORM\Column(name="mailchimp_api_key", type="string", length=255, nullable=false)
     */
    private $mailchimpApiKey;

    /**
     * @var string
     *
     * @ORM\Column(name="number_of_items_per_page", type="string", length=10, nullable=false)
     */
    private $numberOfItemsPerPage = '20';

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide_back_nine", type="boolean", nullable=false)
     */
    private $hideBackNine;

    /**
     * @var string
     *
     * @ORM\Column(name="print_after_sale", type="string", length=10, nullable=false)
     */
    private $printAfterSale = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="webprnt", type="boolean", nullable=false)
     */
    private $webprnt;

    /**
     * @var string
     *
     * @ORM\Column(name="webprnt_ip", type="string", length=20, nullable=false)
     */
    private $webprntIp;

    /**
     * @var string
     *
     * @ORM\Column(name="webprnt_hot_ip", type="string", length=30, nullable=false)
     */
    private $webprntHotIp;

    /**
     * @var string
     *
     * @ORM\Column(name="webprnt_cold_ip", type="string", length=30, nullable=false)
     */
    private $webprntColdIp;

    /**
     * @var string
     *
     * @ORM\Column(name="webprnt_label_ip", type="string", length=30, nullable=false)
     */
    private $webprntLabelIp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="print_two_receipts", type="boolean", nullable=false)
     */
    private $printTwoReceipts = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="print_two_signature_slips", type="boolean", nullable=false)
     */
    private $printTwoSignatureSlips;

    /**
     * @var boolean
     *
     * @ORM\Column(name="print_two_receipts_other", type="boolean", nullable=false)
     */
    private $printTwoReceiptsOther = '0';


    /**
     * @var boolean
     *
     * @ORM\Column(name="updated_printing", type="boolean", nullable=false)
     */
    private $updatedPrinting;

    /**
     * @var boolean
     *
     * @ORM\Column(name="after_sale_load", type="boolean", nullable=false)
     */
    private $afterSaleLoad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fnb_login", type="boolean", nullable=false)
     */
    private $fnbLogin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="teesheet_updates_automatically", type="boolean", nullable=false)
     */
    private $teesheetUpdatesAutomatically;


    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_split_teetimes", type="boolean", nullable=false)
     */
    private $autoSplitTeetimes;

    /**
     * @var integer
     *
     * @ORM\Column(name="teesheet_refresh_rate", type="integer", nullable=false)
     */
    private $teesheetRefreshRate = '90';

    /**
     * @var boolean
     *
     * @ORM\Column(name="send_reservation_confirmations", type="boolean", nullable=false)
     */
    private $sendReservationConfirmations = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="receipt_printer", type="string", length=255, nullable=false)
     */
    private $receiptPrinter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="print_credit_card_receipt", type="boolean", nullable=false)
     */
    private $printCreditCardReceipt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="print_sales_receipt", type="boolean", nullable=false)
     */
    private $printSalesReceipt = '1';

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="print_tip_line", type="boolean", nullable=false)
	 */
	private $printTipLine = '1';

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="tip_line", type="boolean", nullable=false)
	 */
	private $tipLine = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="cash_drawer_on_cash", type="boolean", nullable=false)
     */
    private $cashDrawerOnCash;

    /**
     * @var boolean
     *
     * @ORM\Column(name="track_cash", type="boolean", length=10, nullable=false)
     */
    private $trackCash;

    /**
     * @var boolean
     *
     * @ORM\Column(name="blind_close", type="boolean", nullable=false)
     */
    private $blindClose;

    /**
     * @var boolean
     *
     * @ORM\Column(name="separate_courses", type="boolean", nullable=false)
     */
    private $separateCourses = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="deduct_tips", type="boolean", nullable=false)
     */
    private $deductTips;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_terminals", type="boolean", nullable=false)
     */
    private $useTerminals;

    /**
     * @var integer
     *
     * @ORM\Column(name="online_purchase_terminal_id", type="integer", nullable=false)
     */
    private $onlinePurchaseTerminalId;

    /**
     * @var integer
     *
     * @ORM\Column(name="online_invoice_terminal_id", type="integer", nullable=false)
     */
    private $onlineInvoiceTerminalId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_loyalty", type="boolean", nullable=false)
     */
    private $useLoyalty = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="loyalty_auto_enroll", type="boolean", nullable=false)
     */
    private $loyaltyAutoEnroll = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="return_policy", type="text", length=65535, nullable=false)
     */
    private $returnPolicy;

    /**
     * @var string
     *
     * @ORM\Column(name="time_format", type="string", length=20, nullable=false)
     */
    private $timeFormat = '12_hour';

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=false)
     */
    private $website;

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_sun", type="boolean", nullable=false)
     */
    private $openSun = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_mon", type="boolean", nullable=false)
     */
    private $openMon = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_tue", type="boolean", nullable=false)
     */
    private $openTue = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_wed", type="boolean", nullable=false)
     */
    private $openWed = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_thu", type="boolean", nullable=false)
     */
    private $openThu = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_fri", type="boolean", nullable=false)
     */
    private $openFri = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_sat", type="boolean", nullable=false)
     */
    private $openSat = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="weekend_fri", type="boolean", nullable=false)
     */
    private $weekendFri = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="weekend_sat", type="boolean", nullable=false)
     */
    private $weekendSat = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="weekend_sun", type="boolean", nullable=false)
     */
    private $weekendSun = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="simulator", type="boolean", nullable=false)
     */
    private $simulator;

    /**
     * @var boolean
     *
     * @ORM\Column(name="test_course", type="boolean", nullable=false)
     */
    private $testCourse = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="at_login", type="string", length=20, nullable=false)
     */
    private $atLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="at_password", type="string", length=20, nullable=false)
     */
    private $atPassword;

    /**
     * @var boolean
     *
     * @ORM\Column(name="at_test", type="boolean", nullable=false)
     */
    private $atTest = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="mercury_id", type="string", length=255, nullable=false)
     */
    private $mercuryId;

    /**
     * @var string
     *
     * @ORM\Column(name="mercury_password", type="string", length=255, nullable=false)
     */
    private $mercuryPassword;


    /**
     * @var string
     *
     * @ORM\Column(name="ets_key", type="string", length=255, nullable=false)
     */
    private $etsKey;

    /**
     * @var string
     *
     * @ORM\Column(name="e2e_account_id", type="string", length=255, nullable=false)
     */
    private $e2eAccountId;

    /**
     * @var string
     *
     * @ORM\Column(name="e2e_account_key", type="string", length=255, nullable=false)
     */
    private $e2eAccountKey;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_ets_giftcards", type="boolean", nullable=false)
     */
    private $useEtsGiftcards;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_page_id", type="string", length=255, nullable=false)
     */
    private $facebookPageId;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_page_name", type="string", length=255, nullable=false)
     */
    private $facebookPageName;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_extended_access_token", type="string", length=255, nullable=false)
     */
    private $facebookExtendedAccessToken;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_friends_to_invite", type="boolean", nullable=false)
     */
    private $allowFriendsToInvite;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payment_required", type="boolean", nullable=false)
     */
    private $paymentRequired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="send_email_reminder", type="boolean", nullable=false)
     */
    private $sendEmailReminder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="send_text_reminder", type="boolean", nullable=false)
     */
    private $sendTextReminder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="seasonal_pricing", type="boolean", nullable=false)
     */
    private $seasonalPricing = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="include_tax_online_booking", type="boolean", nullable=false)
     */
    private $includeTaxOnlineBooking = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="foreup_discount_percent", type="float", precision=15, scale=2, nullable=false)
     */
    private $foreupDiscountPercent = '25.00';

    /**
     * @var string
     *
     * @ORM\Column(name="no_show_policy", type="string", length=2048, nullable=false)
     */
    private $noShowPolicy;

    /**
     * @var string
     *
     * @ORM\Column(name="reservation_email_text", type="text", length=65535, nullable=false)
     */
    private $reservationEmailText;

    /**
     * @var integer
     *
     * @ORM\Column(name="reservation_email_photo", type="integer", nullable=false)
     */
    private $reservationEmailPhoto;

    /**
     * @var integer
     *
     * @ORM\Column(name="ibeacon_major_id", type="integer", nullable=false)
     */
    private $ibeaconMajorId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="ibeacon_enabled", type="boolean", nullable=false)
     */
    private $ibeaconEnabled = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="auto_gratuity", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $autoGratuity;

    /**
     * @var float
     *
     * @ORM\Column(name="minimum_food_spend", type="float", precision=10, scale=0, nullable=false)
     */
    private $minimumFoodSpend;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide_employee_last_name_receipt", type="boolean", nullable=false)
     */
    private $hideEmployeeLastNameReceipt = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="quickbooks_export_settings", type="text", length=65535, nullable=false)
     */
    private $quickbooksExportSettings;

    /**
     * @var boolean
     *
     * @ORM\Column(name="food_and_beverage_v2", type="boolean", nullable=true)
     */
    private $foodAndBeverageV2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sales_v2", type="boolean", nullable=true)
     */
    private $salesV2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="require_employee_pin", type="boolean", nullable=false)
     */
    private $requireEmployeePin;

    /**
     * @var string
     *
     * @ORM\Column(name="erange_id", type="string", length=255, nullable=false)
     */
    private $erangeId;

    /**
     * @var string
     *
     * @ORM\Column(name="erange_password", type="string", length=255, nullable=false)
     */
    private $erangePassword;

    /**
     * @var float
     *
     * @ORM\Column(name="credit_card_fee", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $creditCardFee = '0.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="multiple_printers", type="boolean", nullable=false)
     */
    private $multiplePrinters = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="require_guest_count", type="boolean", nullable=false)
     */
    private $requireGuestCount = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="default_kitchen_printer", type="integer", nullable=false)
     */
    private $defaultKitchenPrinter = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="clean_out_nightly", type="boolean", nullable=false)
     */
    private $cleanOutNightly = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="element_account_id", type="string", length=255, nullable=true)
     */
    private $elementAccountId;

    /**
     * @var string
     *
     * @ORM\Column(name="element_account_token", type="string", length=255, nullable=true)
     */
    private $elementAccountToken;

    /**
     * @var string
     *
     * @ORM\Column(name="element_application_id", type="string", length=255, nullable=true)
     */
    private $elementApplicationId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="element_acceptor_id", type="string", length=255, nullable=true)
	 */
	private $elementAcceptorId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mercury_e2e_id", type="string", length=255, nullable=true)
	 */
	private $mercuryE2eId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mercury_e2e_password", type="string", length=255, nullable=true)
	 */
	private $mercuryE2ePassword;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_course_firing", type="boolean", nullable=false)
     */
    private $useCourseFiring = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="course_firing_include_items", type="boolean", nullable=false)
     */
    private $courseFiringIncludeItems = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="tee_sheet_speed_up", type="boolean", nullable=false)
     */
    private $teeSheetSpeedUp = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_kitchen_buzzers", type="boolean", nullable=false)
     */
    private $useKitchenBuzzers = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide_modifier_names_kitchen_receipts", type="boolean", nullable=false)
     */
    private $hideModifierNamesKitchenReceipts = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="food_bev_sort_by_seat", type="boolean", nullable=false)
     */
    private $foodBevSortBySeat = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="disable_facebook_widget", type="boolean", nullable=false)
     */
    private $disableFacebookWidget = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="service_fee_active", type="boolean", nullable=false)
     */
    private $serviceFeeActive = '0';

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="use_printer_extension", type="boolean", nullable=false)
	 */
	private $usePrinterExtension = false;

    /**
     * @var string
     *
     * @ORM\Column(name="service_fee_tax_1_name", type="string", length=255, nullable=false)
     */
    private $serviceFeeTax1Name = 'Sales Tax 1';

    /**
     * @var float
     *
     * @ORM\Column(name="service_fee_tax_1_rate", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $serviceFeeTax1Rate = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="service_fee_tax_2_name", type="string", length=255, nullable=false)
     */
    private $serviceFeeTax2Name = 'Sales Tax 2';

    /**
     * @var float
     *
     * @ORM\Column(name="service_fee_tax_2_rate", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $serviceFeeTax2Rate = '0.00';

    /**
     * @var integer
     *
     * @ORM\Column(name="auto_gratuity_threshold", type="smallint", nullable=false)
     */
    private $autoGratuityThreshold = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="print_suggested_tip", type="boolean", nullable=false)
     */
    private $printSuggestedTip = false;

    /**
     * @var float
     *
     * @ORM\Column(name="default_register_log_open", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $defaultRegisterLogOpen = '100.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="marketing_texting", type="boolean", nullable=false)
     */
    private $marketingTexting = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="customer_field_settings", type="text", length=65535, nullable=false)
     */
    private $customerFieldSettings;

    /**
     * @var boolean
     *
     * @ORM\Column(name="stack_tee_sheets", type="boolean", nullable=false)
     */
    private $stackTeeSheets;

    /**
     * @var boolean
     *
     * @ORM\Column(name="always_list_all", type="boolean", nullable=false)
     */
    private $alwaysListAll;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="cash_drawer_on_sale", type="boolean", nullable=false)
	 */
	private $cashDrawerOnSale = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="food_bev_tip_line_first_receipt", type="boolean", nullable=false)
     */
    private $foodBevTipLineFirstReceipt = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mobile_app_refresh_timestamp", type="datetime", nullable=true)
     */
    private $mobileAppRefreshTimestamp = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_app_ios_published_version", type="text", nullable=false)
     */
    private $mobileAppIosPublishedVersion = '0';
    /**
     * @var string
     *
     * @ORM\Column(name="mobile_app_android_published_version", type="text", nullable=false)
     */
    private $mobileAppAndroidPublishedVersion = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_app_short_title", type="text", length=15, nullable=true)
     */
    private $mobileAppShortTitle = '';


    /**
     * @var string
     *
     * @ORM\Column(name="mobile_test_flight_email", type="text", length=500, nullable=true)
     */
    private $mobileTestFlightEmail = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_app_summary", type="text", length=65535, nullable=false)
     */
    private $mobileAppSummary = '';


    /**
     * @var string
     *
     * @ORM\Column(name="course_summary", type="text", length=65535, nullable=false)
     */
    private $courseSummary = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_app_icon_url", type="string", length=255, nullable=false)
     */
    private $mobileAppIconUrl = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="mobile_app_active", type="boolean", nullable=true)
     */
    private $mobileAppActive = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide_registration", type="boolean", nullable=true)
     */
    private $hideRegistration = '';

	/**
	 * @var string
	 *
	 * @ORM\Column(name="terms_And_conditions", type="string", nullable=true)
	 */
	private $termsAndConditions= '';

	/**
	 * @var string
	 *
	 * @ORM\Column(name="online_booking_welcome_message", type="string", nullable=true)
	 */
	private $onlineBookingWelcomeMessage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="base_color", type="string", length=50, nullable=false)
     */
    private $baseColor;


    /**
     * @var ForeupTeesheet
     *
     * @ORM\OneToMany(targetEntity="ForeupTeesheet", mappedBy="course")
     */
    private $teeSheets;

    /**
     * @ORM\OneToMany(targetEntity="ForeupMobileCourseImages", mappedBy="course")
     */
    private $mobileCourseImages;

    /**
     * @ORM\OneToMany(targetEntity="ForeupMobileTokens", mappedBy="course")
     */
    private $mobileTokens;


	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="auto_email_receipt", type="boolean", nullable=false)
	 */
	private $autoEmailReceipt = false;
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="auto_email_no_print", type="boolean", nullable=false)
	 */
	private $autoEmailNoPrint= false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="require_signature_member_payments", type="boolean", nullable=false)
	 */
	private $requireSignatureMemberPayments = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="hide_taxable", type="boolean", nullable=false)
	 */
	private $hideTaxable = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="allow_employee_register_log_bypass", type="boolean", nullable=false)
	 */
	private $allowEmployeeRegisterLogBypass= false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="teetime_default_to_player1", type="boolean", nullable=false)
	 */
	private $teetimeDefaultToPlayer1 = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="print_tee_time_details", type="boolean", nullable=false)
	 */
	private $printTeeTimeDetails = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="receipt_print_account_balance", type="boolean", nullable=false)
	 */
	private $receiptPrintAccountBalance = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="print_minimums_on_receipt", type="boolean", nullable=false)
	 */
	private $printMinimumsOnReceipt = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="limit_fee_dropdown_by_customer", type="boolean", nullable=false)
	 */
	private $limitFeeDropdownByCustomer = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="require_customer_on_sale", type="boolean", nullable=false)
	 */
	private $requireCustomerOnSale = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="use_new_permissions", type="boolean", nullable=false)
	 */
	private $useNewPermissions = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="side_by_side_tee_sheets", type="boolean", nullable=false)
	 */
	private $sideBySideTeeSheets = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="current_day_checkins_only", type="boolean", nullable=false)
	 */
	private $currentDayCheckinsOnly = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="online_giftcard_purchases", type="boolean", nullable=false)
	 */
	private $onlineGiftcardPurchases= false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="show_invoice_sale_items", type="boolean", nullable=false)
	 */
	private $showInvoiceSaleItems = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="teed_off_color", type="string", nullable=false)
     */
    private $teedOffColor = '';


    /**
     * @var boolean
     *
     * @ORM\Column(name="tee_time_completed_color", type="string", nullable=false)
     */
    private $teeTimeCompletedColor = '';


    /**
     * @ORM\ManyToMany(targetEntity="ForeupMobileAppModules")
     * @ORM\JoinTable(name="foreup_mobile_app_module_permissions",
     *      joinColumns={@ORM\JoinColumn(name="course_id", referencedColumnName="course_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="mobile_app_module_id", referencedColumnName="mobile_app_module_id")}
     *      )
     * @var ForeupMobileAppModules
     */
    private $modules;

    /**
     * @var \foreup\rest\models\entities\ForeupCoursesInformation
     *
     * @ORM\OneToOne(targetEntity="ForeupCoursesInformation")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     */
    private $courseInformation;
	/**
	 * @var \foreup\rest\models\entities\ForeupApiUserCourses
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupApiUserCourses",mappedBy="course")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $apiUserCourses;


	/**
	 * @var ForeupCourseGroups[]
	 *
	 * @ORM\ManyToMany(targetEntity="foreup\rest\models\entities\ForeupCourseGroups", inversedBy="course",fetch="EAGER")
	 * @ORM\JoinTable(name="foreup_course_group_members",
	 *   joinColumns={
	 *     @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 *   },
	 *   inverseJoinColumns={
	 *     @ORM\JoinColumn(name="group_id", referencedColumnName="group_id")
	 *   }
	 * )
	 */
	private $courseGroups;

    /**
     * @return mixed
     */
    public function getMobileCourseImages()
    {
        return $this->mobileCourseImages;
    }

    /**
     * @param mixed $mobileCourseImages
     *
     * @return ForeupCourses
     */
    public function setMobileCourseImages($mobileCourseImages)
    {
        $this->mobileCourseImages = $mobileCourseImages;

        return $this;
    }

    public function getMobileTokens()
    {
        return $this->mobileTokens;
    }

    /**
     * @param mixed $mobileTokens
     */
    public function addMobileTokens($mobileTokens)
    {
        $this->mobileTokens[] = $mobileTokens;

        return $this;
    }

    /**
     * @return ForeupCourseInformation
     */
    public function getCourseInformation()
    {
        return $this->courseInformation;
    }

    /**
     * @param ForeupCourseInformation $courseInformation
     *
     * @return ForeupCourses
     */
    public function setCourseInformation($courseInformation)
    {
        $this->courseInformation = $courseInformation;

        return $this;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teeSheets = new ArrayCollection();
        $this->modules = new ArrayCollection();
        $this->mobileTokens = new ArrayCollection();
        $this->courseGroups = new ArrayCollection();
    }

	/**
	 * @return ForeupCourseGroups[]
	 */
	public function getCourseGroups()
	{
		return $this->courseGroups;
	}

	/**
	 * @param \Doctrine\Common\Collections\Collection $courseGroups
	 *
	 * @return ForeupCourses
	 */
	public function setCourseGroups(\Doctrine\Common\Collections\Collection $courseGroups)
	{
		$this->courseGroups = $courseGroups;

		return $this;
	}

    /**
     * Get myTeeSheets
     *
     * @return ForeupTeesheet[]
     */
    public function getMyTeeSheets()
    {
    	$teesheets = [];
    	foreach($this->teeSheets as $teesheet){
    		/** @var ForeupTeesheet $teesheet */
			if($teesheet->getDeleted() == 0){
				$teesheets[] = $teesheet;
			}
	    }
        return $teesheets;
    }


    /**
     * Get myModules
     *
     * @return ForeupMobileAppModules[]
     */
    public function getMyModules()
    {
        foreach($this->modules as $module){
            $module->setParentCourse($this);
        }
        return $this->modules;
    }


    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupCourses
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;

        return $this;
    }

    /**
     * Set activeCourse
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setActiveCourse($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->activeCourse = $value;

        return $this;
    }

    /**
     * Get activeCourse
     *
     * @return boolean
     */
    public function isActiveCourse()
    {
        return $this->activeCourse;
    }

    /**
     * Set mobileAppRefreshTimestamp
     *
     * @param datetime mobileAppRefreshTimestamp
     *
     * @return ForeupCourses
     */
    public function setMobileAppRefreshTimestamp($mobileAppRefreshTimestamp)
    {
        $this->mobileAppRefreshTimestamp = $mobileAppRefreshTimestamp;

        return $this;
    }

    /**
     * @return text
     */
    public function getMobileAppAndroidPublishedVersion()
    {
        return $this->mobileAppAndroidPublishedVersion;
    }

    /**
     * @param text $mobileAppAndroidPublishedVersion
     *
     * @return ForeupCourses
     */
    public function setMobileAppAndroidPublishedVersion($mobileAppAndroidPublishedVersion)
    {
        $this->mobileAppAndroidPublishedVersion = $mobileAppAndroidPublishedVersion;

        return $this;
    }



    /**
     * @return datetime
     */
    public function getMobileAppIosPublishedVersion()
    {
        return $this->mobileAppIosPublishedVersion;
    }

    /**
     * @param datetime $mobileAppPublishedDate
     *
     * @return ForeupCourses
     */
    public function setMobileAppIosPublishedVersion($mobileAppPublishedDate)
    {
        $this->mobileAppIosPublishedVersion = $mobileAppPublishedDate;


        return $this;
    }

    /**
     * Get mobileAppRefreshTimestamp
     *
     * @return datetime
     */
    public function getMobileAppRefreshTimestamp()
    {
        return $this->mobileAppRefreshTimestamp;
    }

    /**
     * @return text
     */
    public function getCourseSummary()
    {
        return $this->courseSummary;
    }

    /**
     * @param text $course_summary
     */
    public function setCourseSummary($course_summary)
    {
        $this->courseSummary = $course_summary;

	    return $this;
    }

    /**
     * Set mobileAppSummary
     *
     * @param text mobileAppSummary
     *
     * @return ForeupCourses
     */
    public function setMobileAppSummary($mobileAppSummary)
    {
        $this->mobileAppSummary = $mobileAppSummary;

        return $this;
    }

    /**
     * Get mobileAppSummary
     *
     * @return text
     */
    public function getMobileAppSummary()
    {
        return $this->mobileAppSummary;
    }

    /**
     * @return text
     */
    public function getMobileAppShortTitle()
    {
        return $this->mobileAppShortTitle;
    }

    /**
     * @param text $mobileAppShortTitle
     * @return ForeupCourses
     */
    public function setMobileAppShortTitle($mobileAppShortTitle)
    {
        $this->mobileAppShortTitle = $mobileAppShortTitle;

        return $this;
    }

    /**
     * @return text
     */
    public function getMobileTestFlightEmail()
    {
        return $this->mobileTestFlightEmail;
    }

    /**
     * @param text $mobileTestFlightEmail
     * @return ForeupCourses
     */
    public function setMobileTestFlightEmail($mobileTestFlightEmail)
    {
        $this->mobileTestFlightEmail = $mobileTestFlightEmail;
        return $this;
    }

    /**
     * Set mobileAppIconUrl
     *
     * @param string mobileAppIconUrl
     *
     * @return ForeupCourses
     */
    public function setMobileAppIconUrl($mobileAppIconUrl)
    {
        $this->mobileAppIconUrl = $mobileAppIconUrl;

        return $this;
    }

    /**
     * Get mobileAppIconUrl
     *
     * @return string
     */
    public function getMobileAppIconUrl()
    {
        return $this->mobileAppIconUrl;
    }

    /**
     * Set baseColor
     *
     * @param string baseColor
     *
     * @return ForeupCourses
     */
    public function setBaseColor($baseColor)
    {
        $this->baseColor = $baseColor;

        return $this;
    }

    /**
     * Get baseColor
     *
     * @return string
     */
    public function getBaseColor()
    {
        return $this->baseColor;
    }

    /**
     * Set demo
     *
     * @param boolean $demo
     *
     * @return ForeupCourses
     */
    public function setDemo($demo)
    {
	    if(is_string($demo)){
		    $tmp = json_decode(strtolower($demo));
		    if(isset($tmp))$demo = $tmp;
	    }
        $this->demo = $demo;

        return $this;
    }

    /**
     * Get demo
     *
     * @return boolean
     */
    public function isDemo()
    {
        return $this->demo;
    }

    /**
     * Set maintenanceMode
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setMaintenanceMode($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->maintenanceMode = $value;

        return $this;
    }

    /**
     * Get maintenanceMode
     *
     * @return boolean
     */
    public function isMaintenanceMode()
    {
        return $this->maintenanceMode;
    }

    /**
     * Set active
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setActive($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->active = $value;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set areaId
     *
     * @param integer $areaId
     *
     * @return ForeupCourses
     */
    public function setAreaId($areaId)
    {
        $this->areaId = $areaId;

        return $this;
    }

    /**
     * Get areaId
     *
     * @return integer
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return ForeupCourses
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set courseAppDataId
     *
     * @param integer $courseAppDataId
     *
     * @return ForeupCourses
     */
    public function setCourseAppDataId($courseAppDataId)
    {
        $this->courseAppDataId = $courseAppDataId;

        return $this;
    }

    /**
     * Get courseAppDataId
     *
     * @return integer
     */
    public function getCourseAppDataId()
    {
        return $this->courseAppDataId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupCourses
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return ForeupCourses
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return ForeupCourses
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return ForeupCourses
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set stateName
     *
     * @param string $stateName
     *
     * @return ForeupCourses
     */
    public function setStateName($stateName)
    {
        $this->stateName = $stateName;

        return $this;
    }

    /**
     * Get stateName
     *
     * @return string
     */
    public function getStateName()
    {
        return $this->stateName;
    }

    /**
     * Set postal
     *
     * @param string $postal
     *
     * @return ForeupCourses
     */
    public function setPostal($postal)
    {
        $this->postal = $postal;

        return $this;
    }

    /**
     * Get postal
     *
     * @return string
     */
    public function getPostal()
    {
        return $this->postal;
    }

    /**
     * Set woeid
     *
     * @param string $woeid
     *
     * @return ForeupCourses
     */
    public function setWoeid($woeid)
    {
        $this->woeid = $woeid;

        return $this;
    }

    /**
     * Get woeid
     *
     * @return string
     */
    public function getWoeid()
    {
        return $this->woeid;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return ForeupCourses
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return ForeupCourses
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set areaCode
     *
     * @param string $areaCode
     *
     * @return ForeupCourses
     */
    public function setAreaCode($areaCode)
    {
        $this->areaCode = $areaCode;

        return $this;
    }

    /**
     * Get areaCode
     *
     * @return string
     */
    public function getAreaCode()
    {
        return $this->areaCode;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     *
     * @return ForeupCourses
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set dst
     *
     * @param string $dst
     *
     * @return ForeupCourses
     */
    public function setDst($dst)
    {
        $this->dst = $dst;

        return $this;
    }

    /**
     * Get dst
     *
     * @return string
     */
    public function getDst()
    {
        return $this->dst;
    }

    /**
     * Set latitudeCentroid
     *
     * @param float $latitudeCentroid
     *
     * @return ForeupCourses
     */
    public function setLatitudeCentroid($latitudeCentroid)
    {
        $this->latitudeCentroid = $latitudeCentroid;

        return $this;
    }

    /**
     * Get latitudeCentroid
     *
     * @return float
     */
    public function getLatitudeCentroid()
    {
        return $this->latitudeCentroid;
    }

    /**
     * Set latitudePoly
     *
     * @param string $latitudePoly
     *
     * @return ForeupCourses
     */
    public function setLatitudePoly($latitudePoly)
    {
        $this->latitudePoly = $latitudePoly;

        return $this;
    }

    /**
     * Get latitudePoly
     *
     * @return string
     */
    public function getLatitudePoly()
    {
        return $this->latitudePoly;
    }

    /**
     * Set longitudeCentroid
     *
     * @param float $longitudeCentroid
     *
     * @return ForeupCourses
     */
    public function setLongitudeCentroid($longitudeCentroid)
    {
        $this->longitudeCentroid = $longitudeCentroid;

        return $this;
    }

    /**
     * Get longitudeCentroid
     *
     * @return float
     */
    public function getLongitudeCentroid()
    {
        return $this->longitudeCentroid;
    }

    /**
     * Set longitudePoly
     *
     * @param string $longitudePoly
     *
     * @return ForeupCourses
     */
    public function setLongitudePoly($longitudePoly)
    {
        $this->longitudePoly = $longitudePoly;

        return $this;
    }

    /**
     * Get longitudePoly
     *
     * @return string
     */
    public function getLongitudePoly()
    {
        return $this->longitudePoly;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return ForeupCourses
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set holes
     *
     * @param string $holes
     *
     * @return ForeupCourses
     */
    public function setHoles($holes)
    {
        $this->holes = $holes;

        return $this;
    }

    /**
     * Get holes
     *
     * @return string
     */
    public function getHoles()
    {
        return $this->holes;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupCourses
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set county
     *
     * @param string $county
     *
     * @return ForeupCourses
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Get county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Set openTime
     *
     * @param string $openTime
     *
     * @return ForeupCourses
     */
    public function setOpenTime($openTime)
    {
        $this->openTime = $openTime;

        return $this;
    }

    /**
     * Get openTime
     *
     * @return string
     */
    public function getOpenTime()
    {
        return $this->openTime;
    }

    /**
     * Set closeTime
     *
     * @param string $closeTime
     *
     * @return ForeupCourses
     */
    public function setCloseTime($closeTime)
    {
        $this->closeTime = $closeTime;

        return $this;
    }

    /**
     * Get closeTime
     *
     * @return string
     */
    public function getCloseTime()
    {
        return $this->closeTime;
    }

    /**
     * Set increment
     *
     * @param string $increment
     *
     * @return ForeupCourses
     */
    public function setIncrement($increment)
    {
        $this->increment = $increment;

        return $this;
    }

    /**
     * Get increment
     *
     * @return string
     */
    public function getIncrement()
    {
        return $this->increment;
    }

    /**
     * Set frontnine
     *
     * @param string $frontnine
     *
     * @return ForeupCourses
     */
    public function setFrontnine($frontnine)
    {
        $this->frontnine = $frontnine;

        return $this;
    }

    /**
     * Get frontnine
     *
     * @return string
     */
    public function getFrontnine()
    {
        return $this->frontnine;
    }

    /**
     * Set earlyBirdHoursBegin
     *
     * @param string $earlyBirdHoursBegin
     *
     * @return ForeupCourses
     */
    public function setEarlyBirdHoursBegin($earlyBirdHoursBegin)
    {
        $this->earlyBirdHoursBegin = $earlyBirdHoursBegin;

        return $this;
    }

    /**
     * Get earlyBirdHoursBegin
     *
     * @return string
     */
    public function getEarlyBirdHoursBegin()
    {
        return $this->earlyBirdHoursBegin;
    }

    /**
     * Set earlyBirdHoursEnd
     *
     * @param string $earlyBirdHoursEnd
     *
     * @return ForeupCourses
     */
    public function setEarlyBirdHoursEnd($earlyBirdHoursEnd)
    {
        $this->earlyBirdHoursEnd = $earlyBirdHoursEnd;

        return $this;
    }

    /**
     * Get earlyBirdHoursEnd
     *
     * @return string
     */
    public function getEarlyBirdHoursEnd()
    {
        return $this->earlyBirdHoursEnd;
    }

    /**
     * Set morningHoursBegin
     *
     * @param string $morningHoursBegin
     *
     * @return ForeupCourses
     */
    public function setMorningHoursBegin($morningHoursBegin)
    {
        $this->morningHoursBegin = $morningHoursBegin;

        return $this;
    }

    /**
     * Get morningHoursBegin
     *
     * @return string
     */
    public function getMorningHoursBegin()
    {
        return $this->morningHoursBegin;
    }

    /**
     * Set morningHoursEnd
     *
     * @param string $morningHoursEnd
     *
     * @return ForeupCourses
     */
    public function setMorningHoursEnd($morningHoursEnd)
    {
        $this->morningHoursEnd = $morningHoursEnd;

        return $this;
    }

    /**
     * Get morningHoursEnd
     *
     * @return string
     */
    public function getMorningHoursEnd()
    {
        return $this->morningHoursEnd;
    }

    /**
     * Set afternoonHoursBegin
     *
     * @param string $afternoonHoursBegin
     *
     * @return ForeupCourses
     */
    public function setAfternoonHoursBegin($afternoonHoursBegin)
    {
        $this->afternoonHoursBegin = $afternoonHoursBegin;

        return $this;
    }

    /**
     * Get afternoonHoursBegin
     *
     * @return string
     */
    public function getAfternoonHoursBegin()
    {
        return $this->afternoonHoursBegin;
    }

    /**
     * Set afternoonHoursEnd
     *
     * @param string $afternoonHoursEnd
     *
     * @return ForeupCourses
     */
    public function setAfternoonHoursEnd($afternoonHoursEnd)
    {
        $this->afternoonHoursEnd = $afternoonHoursEnd;

        return $this;
    }

    /**
     * Get afternoonHoursEnd
     *
     * @return string
     */
    public function getAfternoonHoursEnd()
    {
        return $this->afternoonHoursEnd;
    }

    /**
     * Set twilightHour
     *
     * @param string $twilightHour
     *
     * @return ForeupCourses
     */
    public function setTwilightHour($twilightHour)
    {
        $this->twilightHour = $twilightHour;

        return $this;
    }

    /**
     * Get twilightHour
     *
     * @return string
     */
    public function getTwilightHour()
    {
        return $this->twilightHour;
    }

    /**
     * Set superTwilightHour
     *
     * @param string $superTwilightHour
     *
     * @return ForeupCourses
     */
    public function setSuperTwilightHour($superTwilightHour)
    {
        $this->superTwilightHour = $superTwilightHour;

        return $this;
    }

    /**
     * Get superTwilightHour
     *
     * @return string
     */
    public function getSuperTwilightHour()
    {
        return $this->superTwilightHour;
    }

    /**
     * Set holidays
     *
     * @param boolean $holidays
     *
     * @return ForeupCourses
     */
    public function setHolidays($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->holidays = $value;

        return $this;
    }

    /**
     * Get holidays
     *
     * @return boolean
     */
    public function getHolidays()
    {
        return $this->holidays;
    }

    /**
     * Set onlineBooking
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setOnlineBooking($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->onlineBooking = $value;

        return $this;
    }

    /**
     * Get onlineBooking
     *
     * @return boolean
     */
    public function getOnlineBooking()
    {
        return $this->onlineBooking;
    }

    /**
     * Set onlineBookingProtected
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setOnlineBookingProtected($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->onlineBookingProtected = $value;

        return $this;
    }

    /**
     * Get onlineBookingProtected
     *
     * @return boolean
     */
    public function getOnlineBookingProtected()
    {
        return $this->onlineBookingProtected;
    }

    /**
     * Set minRequiredPlayers
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setMinRequiredPlayers($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->minRequiredPlayers = $value;

        return $this;
    }

    /**
     * Get minRequiredPlayers
     *
     * @return boolean
     */
    public function getMinRequiredPlayers()
    {
        return $this->minRequiredPlayers;
    }

    /**
     * Set minRequiredCarts
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setMinRequiredCarts($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->minRequiredCarts = $value;

        return $this;
    }

    /**
     * Get minRequiredCarts
     *
     * @return boolean
     */
    public function getMinRequiredCarts()
    {
        return $this->minRequiredCarts;
    }

    /**
     * Set minRequiredHoles
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setMinRequiredHoles($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->minRequiredHoles = $value;

        return $this;
    }

    /**
     * Get minRequiredHoles
     *
     * @return boolean
     */
    public function getMinRequiredHoles()
    {
        return $this->minRequiredHoles;
    }

    /**
     * Set bookingRules
     *
     * @param string $bookingRules
     *
     * @return ForeupCourses
     */
    public function setBookingRules($bookingRules)
    {
        $this->bookingRules = $bookingRules;

        return $this;
    }

    /**
     * Get bookingRules
     *
     * @return string
     */
    public function getBookingRules()
    {
        return $this->bookingRules;
    }

    /**
     * Set autoMailers
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setAutoMailers($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->autoMailers = $value;

        return $this;
    }

    /**
     * Get autoMailers
     *
     * @return boolean
     */
    public function isAutoMailers()
    {
        return $this->autoMailers;
    }

    /**
     * Set config
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setConfig($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->config = $value;

        return $this;
    }

    /**
     * Get config
     *
     * @return boolean
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set courses
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setCourses($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->courses = $value;

        return $this;
    }

    /**
     * Get courses
     *
     * @return boolean
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * Set customers
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setCustomers($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->customers = $value;

        return $this;
    }

    /**
     * Get customers
     *
     * @return boolean
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Set dashboards
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setDashboards($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->dashboards = $value;

        return $this;
    }

    /**
     * Get dashboards
     *
     * @return boolean
     */
    public function getDashboards()
    {
        return $this->dashboards;
    }

    /**
     * Set employees
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setEmployees($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->employees = $value;

        return $this;
    }

    /**
     * Get employees
     *
     * @return boolean
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * Set events
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setEvents($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->events = $value;

        return $this;
    }

    /**
     * Get events
     *
     * @return boolean
     */
    public function isEvents()
    {
        return $this->events;
    }

    /**
     * Set foodAndBeverage
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setFoodAndBeverage($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->foodAndBeverage = $value;

        return $this;
    }

    /**
     * Get foodAndBeverage
     *
     * @return boolean
     */
    public function isFoodAndBeverage()
    {
        return $this->foodAndBeverage;
    }

    /**
     * Set giftcards
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setGiftcards($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->giftcards = $value;

        return $this;
    }

    /**
     * Get giftcards
     *
     * @return boolean
     */
    public function getGiftcards()
    {
        return $this->giftcards;
    }

    /**
     * Set passes
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setPasses($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->passes = $value;

        return $this;
    }

    /**
     * Get passes
     *
     * @return boolean
     */
    public function isPasses()
    {
        return $this->passes;
    }

    /**
     * Set invoices
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setInvoices($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->invoices = $value;

        return $this;
    }

    /**
     * Get invoices
     *
     * @return boolean
     */
    public function isInvoices()
    {
        return $this->invoices;
    }

    /**
     * Set itemKits
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setItemKits($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->itemKits = $value;

        return $this;
    }

    /**
     * Get itemKits
     *
     * @return boolean
     */
    public function getItemKits()
    {
        return $this->itemKits;
    }

    /**
     * Set items
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setItems($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->items = $value;

        return $this;
    }

    /**
     * Get items
     *
     * @return boolean
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set marketingCampaigns
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setMarketingCampaigns($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->marketingCampaigns = $value;

        return $this;
    }

    /**
     * Get marketingCampaigns
     *
     * @return boolean
     */
    public function getMarketingCampaigns()
    {
        return $this->marketingCampaigns;
    }

    /**
     * Set promotions
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setPromotions($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->promotions = $value;

        return $this;
    }

    /**
     * Get promotions
     *
     * @return boolean
     */
    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * Set quickbooks
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setQuickbooks($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->quickbooks = $value;

        return $this;
    }

    /**
     * Get quickbooks
     *
     * @return boolean
     */
    public function isQuickbooks()
    {
        return $this->quickbooks;
    }

    /**
     * Set receivings
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setReceivings($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->receivings = $value;

        return $this;
    }

    /**
     * Get receivings
     *
     * @return boolean
     */
    public function getReceivings()
    {
        return $this->receivings;
    }

    /**
     * Set recipients
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setRecipients($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->recipients = $value;

        return $this;
    }

    /**
     * Get recipients
     *
     * @return boolean
     */
    public function isRecipients()
    {
        return $this->recipients;
    }

    /**
     * Set reports
     *
     * @param integer $reports
     *
     * @return ForeupCourses
     */
    public function setReports($reports)
    {
        $this->reports = $reports;

        return $this;
    }

    /**
     * Get reports
     *
     * @return integer
     */
    public function isReports()
    {
        return $this->reports;
    }

    /**
     * Set reservations
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setReservations($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->reservations = $value;

        return $this;
    }

    /**
     * Get reservations
     *
     * @return boolean
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * Set sales
     *
     * @param bool $value
     *
     * @return ForeupCourses
     */
    public function setSales($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->sales = $value;

        return $this;
    }

    /**
     * Get sales
     *
     * @return bool
     */
    public function isSales()
    {
        return $this->sales;
    }

    /**
     * Set schedules
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setSchedules($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->schedules = $value;

        return $this;
    }

    /**
     * Get schedules
     *
     * @return boolean
     */
    public function isSchedules()
    {
        return $this->schedules;
    }

    /**
     * Set suppliers
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setSuppliers($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->suppliers = $value;

        return $this;
    }

    /**
     * Get suppliers
     *
     * @return boolean
     */
    public function getSuppliers()
    {
        return $this->suppliers;
    }

    /**
     * Set teesheets
     *
     * @param integer $teesheets
     *
     * @return ForeupCourses
     */
    public function setTeesheets($teesheets)
    {
        $this->teesheets = $teesheets;

        return $this;
    }

    /**
     * Get teesheets
     *
     * @return integer
     */
    public function getTeesheets()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('deleted',0));
        return $this->teeSheets->matching($criteria);
    }

    /**
     * Set tournaments
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setTournaments($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->tournaments = $value;

        return $this;
    }

    /**
     * Get tournaments
     *
     * @return boolean
     */
    public function isTournaments()
    {
        return $this->tournaments;
    }

    /**
     * Set currencySymbol
     *
     * @param string $currencySymbol
     *
     * @return ForeupCourses
     */
    public function setCurrencySymbol($currencySymbol)
    {
        $this->currencySymbol = $currencySymbol;

        return $this;
    }

    /**
     * Get currencySymbol
     *
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->currencySymbol;
    }

    /**
     * Set currencySymbolPlacement
     *
     * @param string $currencySymbolPlacement
     *
     * @return ForeupCourses
     */
    public function setCurrencySymbolPlacement($currencySymbolPlacement)
    {
        $this->currencySymbolPlacement = $currencySymbolPlacement;

        return $this;
    }

    /**
     * Get currencySymbolPlacement
     *
     * @return string
     */
    public function getCurrencySymbolPlacement()
    {
        return $this->currencySymbolPlacement;
    }

    /**
     * Set currencyFormat
     *
     * @param string $currencyFormat
     *
     * @return ForeupCourses
     */
    public function setCurrencyFormat($currencyFormat)
    {
        $this->currencyFormat = $currencyFormat;

        return $this;
    }

    /**
     * Get currencyFormat
     *
     * @return string
     */
    public function getCurrencyFormat()
    {
        return $this->currencyFormat;
    }

    /**
     * Set companyLogo
     *
     * @param string $companyLogo
     *
     * @return ForeupCourses
     */
    public function setCompanyLogo($companyLogo)
    {
        $this->companyLogo = $companyLogo;

        return $this;
    }

    /**
     * Get companyLogo
     *
     * @return string
     */
    public function getCompanyLogo()
    {
        return $this->companyLogo;
    }

    /**
     * Set dateFormat
     *
     * @param string $dateFormat
     *
     * @return ForeupCourses
     */
    public function setDateFormat($dateFormat)
    {
        $this->dateFormat = $dateFormat;

        return $this;
    }

    /**
     * Get dateFormat
     *
     * @return string
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * Set defaultTax1Name
     *
     * @param string $defaultTax1Name
     *
     * @return ForeupCourses
     */
    public function setDefaultTax1Name($defaultTax1Name)
    {
        $this->defaultTax1Name = $defaultTax1Name;

        return $this;
    }

    /**
     * Get defaultTax1Name
     *
     * @return string
     */
    public function getDefaultTax1Name()
    {
        return $this->defaultTax1Name;
    }

    /**
     * Set defaultTax1Rate
     *
     * @param string $defaultTax1Rate
     *
     * @return ForeupCourses
     */
    public function setDefaultTax1Rate($defaultTax1Rate)
    {
        $this->defaultTax1Rate = $defaultTax1Rate;

        return $this;
    }

    /**
     * Get defaultTax1Rate
     *
     * @return string
     */
    public function getDefaultTax1Rate()
    {
        return $this->defaultTax1Rate;
    }

    /**
     * Set defaultTax2Cumulative
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setDefaultTax2Cumulative($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->defaultTax2Cumulative = $value;

        return $this;
    }

    /**
     * Get defaultTax2Cumulative
     *
     * @return string
     */
    public function getDefaultTax2Cumulative()
    {
        return $this->defaultTax2Cumulative;
    }

    /**
     * Set defaultTax2Name
     *
     * @param string $defaultTax2Name
     *
     * @return ForeupCourses
     */
    public function setDefaultTax2Name($defaultTax2Name)
    {
        $this->defaultTax2Name = $defaultTax2Name;

        return $this;
    }

    /**
     * Get defaultTax2Name
     *
     * @return string
     */
    public function getDefaultTax2Name()
    {
        return $this->defaultTax2Name;
    }

    /**
     * Set defaultTax2Rate
     *
     * @param string $defaultTax2Rate
     *
     * @return ForeupCourses
     */
    public function setDefaultTax2Rate($defaultTax2Rate)
    {
        $this->defaultTax2Rate = $defaultTax2Rate;

        return $this;
    }

    /**
     * Get defaultTax2Rate
     *
     * @return string
     */
    public function getDefaultTax2Rate()
    {
        return $this->defaultTax2Rate;
    }

    /**
     * Set unitPriceIncludesTax
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setUnitPriceIncludesTax($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->unitPriceIncludesTax = $value;

        return $this;
    }

    /**
     * Get unitPriceIncludesTax
     *
     * @return boolean
     */
    public function isUnitPriceIncludesTax()
    {
        return $this->unitPriceIncludesTax;
    }

    /**
     * Set customerCreditNickname
     *
     * @param string $customerCreditNickname
     *
     * @return ForeupCourses
     */
    public function setCustomerCreditNickname($customerCreditNickname)
    {
        $this->customerCreditNickname = $customerCreditNickname;

        return $this;
    }

    /**
     * Get customerCreditNickname
     *
     * @return string
     */
    public function getCustomerCreditNickname()
    {
        return $this->customerCreditNickname;
    }

    /**
     * Set creditDepartment
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setCreditDepartment($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->creditDepartment = $value;

        return $this;
    }

    /**
     * Get creditDepartment
     *
     * @return boolean
     */
    public function getCreditDepartment()
    {
        return $this->creditDepartment;
    }

    /**
     * Set creditDepartmentName
     *
     * @param string $creditDepartmentName
     *
     * @return ForeupCourses
     */
    public function setCreditDepartmentName($creditDepartmentName)
    {
        $this->creditDepartmentName = $creditDepartmentName;

        return $this;
    }

    /**
     * Get creditDepartmentName
     *
     * @return string
     */
    public function getCreditDepartmentName()
    {
        return $this->creditDepartmentName;
    }

    /**
     * Set creditCategory
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setCreditCategory($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->creditCategory = $value;

        return $this;
    }

    /**
     * Get creditCategory
     *
     * @return boolean
     */
    public function getCreditCategory()
    {
        return $this->creditCategory;
    }

    /**
     * Set creditCategoryName
     *
     * @param string $creditCategoryName
     *
     * @return ForeupCourses
     */
    public function setCreditCategoryName($creditCategoryName)
    {
        $this->creditCategoryName = $creditCategoryName;

        return $this;
    }

    /**
     * Get creditCategoryName
     *
     * @return string
     */
    public function getCreditCategoryName()
    {
        return $this->creditCategoryName;
    }

    /**
     * Set creditLimitToSubtotal
     *
     * @param bool $value
     *
     * @return ForeupCourses
     */
    public function setCreditLimitToSubtotal($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->creditLimitToSubtotal = $value;

        return $this;
    }

    /**
     * Get creditLimitToSubtotal
     *
     * @return bool
     */
    public function getCreditLimitToSubtotal()
    {
        return $this->creditLimitToSubtotal;
    }

    /**
     * Set memberBalanceNickname
     *
     * @param string $memberBalanceNickname
     *
     * @return ForeupCourses
     */
    public function setMemberBalanceNickname($memberBalanceNickname)
    {
        $this->memberBalanceNickname = $memberBalanceNickname;

        return $this;
    }

    /**
     * Get memberBalanceNickname
     *
     * @return string
     */
    public function getMemberBalanceNickname()
    {
        return $this->memberBalanceNickname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ForeupCourses
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set reservationEmail
     *
     * @param string $reservationEmail
     *
     * @return ForeupCourses
     */
    public function setReservationEmail($reservationEmail)
    {
        $this->reservationEmail = $reservationEmail;

        return $this;
    }

    /**
     * Get reservationEmail
     *
     * @return string
     */
    public function getReservationEmail()
    {
        return $this->reservationEmail;
    }

    /**
     * Set billingEmail
     *
     * @param string $billingEmail
     *
     * @return ForeupCourses
     */
    public function setBillingEmail($billingEmail)
    {
        $this->billingEmail = $billingEmail;

        return $this;
    }

    /**
     * Get billingEmail
     *
     * @return string
     */
    public function getBillingEmail()
    {
        return $this->billingEmail;
    }

    /**
     * Set lastInvoicesSent
     *
     * @param \DateTime $lastInvoicesSent
     *
     * @return ForeupCourses
     */
    public function setLastInvoicesSent($lastInvoicesSent)
    {
        $this->lastInvoicesSent = $lastInvoicesSent;

        return $this;
    }

    /**
     * Get lastInvoicesSent
     *
     * @return \DateTime
     */
    public function getLastInvoicesSent()
    {
        return $this->lastInvoicesSent;
    }

    /**
     * Set displayEmailAds
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setDisplayEmailAds($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->displayEmailAds = $value;

        return $this;
    }

    /**
     * Get displayEmailAds
     *
     * @return boolean
     */
    public function isDisplayEmailAds()
    {
        return $this->displayEmailAds;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return ForeupCourses
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return ForeupCourses
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set mailchimpApiKey
     *
     * @param string $mailchimpApiKey
     *
     * @return ForeupCourses
     */
    public function setMailchimpApiKey($mailchimpApiKey)
    {
        $this->mailchimpApiKey = $mailchimpApiKey;

        return $this;
    }

    /**
     * Get mailchimpApiKey
     *
     * @return string
     */
    public function getMailchimpApiKey()
    {
        return $this->mailchimpApiKey;
    }

    /**
     * Set numberOfItemsPerPage
     *
     * @param string $numberOfItemsPerPage
     *
     * @return ForeupCourses
     */
    public function setNumberOfItemsPerPage($numberOfItemsPerPage)
    {
        $this->numberOfItemsPerPage = $numberOfItemsPerPage;

        return $this;
    }

    /**
     * Get numberOfItemsPerPage
     *
     * @return string
     */
    public function getNumberOfItemsPerPage()
    {
        return $this->numberOfItemsPerPage;
    }

    /**
     * Set hideBackNine
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setHideBackNine($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->hideBackNine = $value;

        return $this;
    }

    /**
     * Get hideBackNine
     *
     * @return boolean
     */
    public function isHideBackNine()
    {
        return $this->hideBackNine;
    }

    /**
     * Set printAfterSale
     *
     * @param bool $value
     *
     * @return ForeupCourses
     */
    public function setPrintAfterSale($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->printAfterSale = $value;

        return $this;
    }

    /**
     * Get printAfterSale
     *
     * @return bool
     */
    public function isPrintAfterSale()
    {
        return $this->printAfterSale;
    }

    /**
     * Set webprnt
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setWebprnt($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->webprnt = $value;

        return $this;
    }

    /**
     * Get webprnt
     *
     * @return boolean
     */
    public function isWebprnt()
    {
        return $this->webprnt;
    }

    /**
     * Set webprntIp
     *
     * @param string $webprntIp
     *
     * @return ForeupCourses
     */
    public function setWebprntIp($webprntIp)
    {
        $this->webprntIp = $webprntIp;

        return $this;
    }

    /**
     * Get webprntIp
     *
     * @return string
     */
    public function getWebprntIp()
    {
        return $this->webprntIp;
    }

    /**
     * Set webprntHotIp
     *
     * @param string $webprntHotIp
     *
     * @return ForeupCourses
     */
    public function setWebprntHotIp($webprntHotIp)
    {
        $this->webprntHotIp = $webprntHotIp;

        return $this;
    }

    /**
     * Get webprntHotIp
     *
     * @return string
     */
    public function getWebprntHotIp()
    {
        return $this->webprntHotIp;
    }

    /**
     * Set webprntColdIp
     *
     * @param string $webprntColdIp
     *
     * @return ForeupCourses
     */
    public function setWebprntColdIp($webprntColdIp)
    {
        $this->webprntColdIp = $webprntColdIp;

        return $this;
    }

    /**
     * Get webprntColdIp
     *
     * @return string
     */
    public function getWebprntColdIp()
    {
        return $this->webprntColdIp;
    }

    /**
     * Set webprntLabelIp
     *
     * @param string $webprntLabelIp
     *
     * @return ForeupCourses
     */
    public function setWebprntLabelIp($webprntLabelIp)
    {
        $this->webprntLabelIp = $webprntLabelIp;

        return $this;
    }

    /**
     * Get webprntLabelIp
     *
     * @return string
     */
    public function getWebprntLabelIp()
    {
        return $this->webprntLabelIp;
    }

    /**
     * Set printTwoReceipts
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setPrintTwoReceipts($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->printTwoReceipts = $value;

        return $this;
    }

    /**
     * Get printTwoReceipts
     *
     * @return boolean
     */
    public function isPrintTwoReceipts()
    {
        return $this->printTwoReceipts;
    }

    /**
     * Set printTwoSignatureSlips
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setPrintTwoSignatureSlips($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->printTwoSignatureSlips = $value;

        return $this;
    }

    /**
     * Get printTwoSignatureSlips
     *
     * @return boolean
     */
    public function isPrintTwoSignatureSlips()
    {
        return $this->printTwoSignatureSlips;
    }

    /**
     * Set printTwoReceiptsOther
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setPrintTwoReceiptsOther($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->printTwoReceiptsOther = $value;

        return $this;
    }

    /**
     * Get printTwoReceiptsOther
     *
     * @return boolean
     */
    public function isPrintTwoReceiptsOther()
    {
        return $this->printTwoReceiptsOther;
    }

    /**
     * Set tipLine
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setTipLine($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->tipLine = $value;

        return $this;
    }

    /**
     * Get tipLine
     *
     * @return boolean
     */
    public function isTipLine()
    {
        return $this->tipLine;
    }

    /**
     * Set updatedPrinting
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setUpdatedPrinting($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->updatedPrinting = $value;

        return $this;
    }

    /**
     * Get updatedPrinting
     *
     * @return boolean
     */
    public function isUpdatedPrinting()
    {
        return $this->updatedPrinting;
    }

    /**
     * Set afterSaleLoad
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setAfterSaleLoad($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->afterSaleLoad = $value;

        return $this;
    }

    /**
     * Get afterSaleLoad
     *
     * @return boolean
     */
    public function isAfterSaleLoad()
    {
        return $this->afterSaleLoad;
    }

    /**
     * Set fnbLogin
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setFnbLogin($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->fnbLogin = $value;

        return $this;
    }

    /**
     * Get fnbLogin
     *
     * @return boolean
     */
    public function isFnbLogin()
    {
        return $this->fnbLogin;
    }

    /**
     * Set teesheetUpdatesAutomatically
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setTeesheetUpdatesAutomatically($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->teesheetUpdatesAutomatically = $value;

        return $this;
    }

    /**
     * Get teesheetUpdatesAutomatically
     *
     * @return boolean
     */
    public function isTeesheetUpdatesAutomatically()
    {
        return $this->teesheetUpdatesAutomatically;
    }



    /**
     * Set autoSplitTeetimes
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setAutoSplitTeetimes($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->autoSplitTeetimes = $value;

        return $this;
    }

    /**
     * Get autoSplitTeetimes
     *
     * @return boolean
     */
    public function isAutoSplitTeetimes()
    {
        return $this->autoSplitTeetimes;
    }

    /**
     * Set teesheetRefreshRate
     *
     * @param integer $teesheetRefreshRate
     *
     * @return ForeupCourses
     */
    public function setTeesheetRefreshRate($teesheetRefreshRate)
    {
        $this->teesheetRefreshRate = $teesheetRefreshRate;

        return $this;
    }

    /**
     * Get teesheetRefreshRate
     *
     * @return integer
     */
    public function getTeesheetRefreshRate()
    {
        return $this->teesheetRefreshRate;
    }

    /**
     * Set sendReservationConfirmations
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setSendReservationConfirmations($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->sendReservationConfirmations = $value;

        return $this;
    }

    /**
     * Get sendReservationConfirmations
     *
     * @return boolean
     */
    public function isSendReservationConfirmations()
    {
        return $this->sendReservationConfirmations;
    }

    /**
     * Set receiptPrinter
     *
     * @param string $receiptPrinter
     *
     * @return ForeupCourses
     */
    public function setReceiptPrinter($receiptPrinter)
    {
        $this->receiptPrinter = $receiptPrinter;

        return $this;
    }

    /**
     * Get receiptPrinter
     *
     * @return string
     */
    public function getReceiptPrinter()
    {
        return $this->receiptPrinter;
    }

    /**
     * Set printCreditCardReceipt
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setPrintCreditCardReceipt($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->printCreditCardReceipt = $value;

        return $this;
    }

    /**
     * Get printCreditCardReceipt
     *
     * @return boolean
     */
    public function isPrintCreditCardReceipt()
    {
        return $this->printCreditCardReceipt;
    }

    /**
     * Set printSalesReceipt
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setPrintSalesReceipt($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->printSalesReceipt = $value;

        return $this;
    }

    /**
     * Get printSalesReceipt
     *
     * @return boolean
     */
    public function isPrintSalesReceipt()
    {
        return $this->printSalesReceipt;
    }

    /**
     * Set printTipLine
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setPrintTipLine($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->printTipLine = $value;

        return $this;
    }

    /**
     * Get printTipLine
     *
     * @return boolean
     */
    public function isPrintTipLine()
    {
        return $this->printTipLine;
    }

    /**
     * Set cashDrawerOnCash
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setCashDrawerOnCash($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->cashDrawerOnCash = $value;

        return $this;
    }

    /**
     * Get cashDrawerOnCash
     *
     * @return boolean
     */
    public function isCashDrawerOnCash()
    {
        return $this->cashDrawerOnCash;
    }

    /**
     * Set trackCash
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setTrackCash($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->trackCash = $value;

        return $this;
    }

    /**
     * Get trackCash
     *
     * @return boolean
     */
    public function isTrackCash()
    {
        return $this->trackCash;
    }

    /**
     * Set blindClose
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setBlindClose($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->blindClose = $value;

        return $this;
    }

    /**
     * Get blindClose
     *
     * @return boolean
     */
    public function isBlindClose()
    {
        return $this->blindClose;
    }

    /**
     * Set separateCourses
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setSeparateCourses($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->separateCourses = $value;

        return $this;
    }

    /**
     * Get separateCourses
     *
     * @return boolean
     */
    public function isSeparateCourses()
    {
        return $this->separateCourses;
    }

    /**
     * Set deductTips
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setDeductTips($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->deductTips = $value;

        return $this;
    }

    /**
     * Get deductTips
     *
     * @return boolean
     */
    public function isDeductTips()
    {
        return $this->deductTips;
    }

    /**
     * Set useTerminals
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setUseTerminals($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->useTerminals = $value;

        return $this;
    }

    /**
     * Get useTerminals
     *
     * @return boolean
     */
    public function isUseTerminals()
    {
        return $this->useTerminals;
    }

    /**
     * Get onlinePurchaseTerminalId
     *
     * @return integer
     */
    public function getOnlinePurchaseTerminalId()
    {
        return $this->onlinePurchaseTerminalId;
    }

    /**
     * Set onlinePurchaseTerminalId
     *
     * @param integer $onlinePurchaseTerminalId
     *
     * @return ForeupCourses
     */
    public function setOnlinePurchaseTerminalId($onlinePurchaseTerminalId)
    {
        $this->onlinePurchaseTerminalId = $onlinePurchaseTerminalId;

        return $this;
    }

    /**
     * Get onlineInvoiceTerminalId
     *
     * @return integer
     */
    public function getOnlineInvoiceTerminalId()
    {
        return $this->onlineInvoiceTerminalId;
    }

    /**
     * Set onlineInvoiceTerminalId
     *
     * @param integer $onlineInvoiceTerminalId
     *
     * @return ForeupCourses
     */
    public function setOnlineInvoiceTerminalId($onlineInvoiceTerminalId)
    {
        $this->onlineInvoiceTerminalId = $onlineInvoiceTerminalId;

        return $this;
    }

    /**
     * Set useLoyalty
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setUseLoyalty($value)
    {
        if(is_string($value)){
            $tmp = json_decode(strtolower($value));
            if(isset($tmp))$value = $tmp;
        }
        $this->useLoyalty = $value;

        return $this;
    }

    /**
     * Get useLoyalty
     *
     * @return boolean
     */
    public function isUseLoyalty()
    {
        return $this->useLoyalty;
    }

    /**
     * Set loyaltyAutoEnroll
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setLoyaltyAutoEnroll($value)
    {
        if(is_string($value)){
            $tmp = json_decode(strtolower($value));
            if(isset($tmp))$value = $tmp;
        }
        $this->loyaltyAutoEnroll = $value;

        return $this;
    }

    /**
     * Get loyaltyAutoEnroll
     *
     * @return boolean
     */
    public function isLoyaltyAutoEnroll()
    {
        return $this->loyaltyAutoEnroll;
    }

    /**
     * Set returnPolicy
     *
     * @param string $returnPolicy
     *
     * @return ForeupCourses
     */
    public function setReturnPolicy($returnPolicy)
    {
        $this->returnPolicy = $returnPolicy;

        return $this;
    }

    /**
     * Get returnPolicy
     *
     * @return string
     */
    public function getReturnPolicy()
    {
        return $this->returnPolicy;
    }

    /**
     * Set timeFormat
     *
     * @param string $timeFormat
     *
     * @return ForeupCourses
     */
    public function setTimeFormat($timeFormat)
    {
        $this->timeFormat = $timeFormat;

        return $this;
    }

    /**
     * Get timeFormat
     *
     * @return string
     */
    public function getTimeFormat()
    {
        return $this->timeFormat;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return ForeupCourses
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set openSun
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setOpenSun($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->openSun = $value;

        return $this;
    }

    /**
     * Get openSun
     *
     * @return boolean
     */
    public function isOpenSun()
    {
        return $this->openSun;
    }

    /**
     * Set openMon
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setOpenMon($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->openMon = $value;

        return $this;
    }

    /**
     * Get openMon
     *
     * @return boolean
     */
    public function isOpenMon()
    {
        return $this->openMon;
    }

    /**
     * Set openTue
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setOpenTue($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->openTue = $value;

        return $this;
    }

    /**
     * Get openTue
     *
     * @return boolean
     */
    public function isOpenTue()
    {
        return $this->openTue;
    }

    /**
     * Set openWed
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setOpenWed($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->openWed = $value;

        return $this;
    }

    /**
     * Get openWed
     *
     * @return boolean
     */
    public function isOpenWed()
    {
        return $this->openWed;
    }

    /**
     * Set openThu
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setOpenThu($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->openThu = $value;

        return $this;
    }

    /**
     * Get openThu
     *
     * @return boolean
     */
    public function isOpenThu()
    {
        return $this->openThu;
    }

    /**
     * Set openFri
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setOpenFri($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->openFri = $value;

        return $this;
    }

    /**
     * Get openFri
     *
     * @return boolean
     */
    public function isOpenFri()
    {
        return $this->openFri;
    }

    /**
     * Set openSat
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setOpenSat($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->openSat = $value;

        return $this;
    }

    /**
     * Get openSat
     *
     * @return boolean
     */
    public function isOpenSat()
    {
        return $this->openSat;
    }

    /**
     * Set weekendFri
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setWeekendFri($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->weekendFri = $value;

        return $this;
    }

    /**
     * Get weekendFri
     *
     * @return boolean
     */
    public function isWeekendFri()
    {
        return $this->weekendFri;
    }

    /**
     * Set weekendSat
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setWeekendSat($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->weekendSat = $value;

        return $this;
    }

    /**
     * Get weekendSat
     *
     * @return boolean
     */
    public function isWeekendSat()
    {
        return $this->weekendSat;
    }

    /**
     * Set weekendSun
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setWeekendSun($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->weekendSun = $value;

        return $this;
    }

    /**
     * Get weekendSun
     *
     * @return boolean
     */
    public function isWeekendSun()
    {
        return $this->weekendSun;
    }

    /**
     * Set simulator
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setSimulator($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->simulator = $value;

        return $this;
    }

    /**
     * Get simulator
     *
     * @return boolean
     */
    public function isSimulator()
    {
        return $this->simulator;
    }

    /**
     * Set testCourse
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setTestCourse($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->testCourse = $value;

        return $this;
    }

    /**
     * Get testCourse
     *
     * @return boolean
     */
    public function isTestCourse()
    {
        return $this->testCourse;
    }

    /**
     * Set atLogin
     *
     * @param string $atLogin
     *
     * @return ForeupCourses
     */
    public function setAtLogin($atLogin)
    {
        $this->atLogin = $atLogin;

        return $this;
    }

    /**
     * Get atLogin
     *
     * @return string
     */
    public function getAtLogin()
    {
        return $this->atLogin;
    }

    /**
     * Set atPassword
     *
     * @param string $atPassword
     *
     * @return ForeupCourses
     */
    public function setAtPassword($atPassword)
    {
        $this->atPassword = $atPassword;

        return $this;
    }

    /**
     * Get atPassword
     *
     * @return string
     */
    public function getAtPassword()
    {
        return $this->atPassword;
    }

    /**
     * Set atTest
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setAtTest($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->atTest = $value;

        return $this;
    }

    /**
     * Get atTest
     *
     * @return boolean
     */
    public function getAtTest()
    {
        return $this->atTest;
    }

    /**
     * Set mercuryId
     *
     * @param string $mercuryId
     *
     * @return ForeupCourses
     */
    public function setMercuryId($mercuryId)
    {
        $this->mercuryId = $mercuryId;

        return $this;
    }

    /**
     * Get mercuryId
     *
     * @return string
     */
    public function getMercuryId()
    {
        return $this->mercuryId;
    }

    /**
     * Set mercuryPassword
     *
     * @param string $mercuryPassword
     *
     * @return ForeupCourses
     */
    public function setMercuryPassword($mercuryPassword)
    {
        $this->mercuryPassword = $mercuryPassword;

        return $this;
    }

    /**
     * Get mercuryPassword
     *
     * @return string
     */
    public function getMercuryPassword()
    {
        return $this->mercuryPassword;
    }

    /**
     * Set mercuryE2eId
     *
     * @param string $mercuryE2eId
     *
     * @return ForeupCourses
     */
    public function setMercuryE2eId($mercuryE2eId)
    {
        $this->mercuryE2eId = $mercuryE2eId;

        return $this;
    }

    /**
     * Get mercuryE2eId
     *
     * @return string
     */
    public function getMercuryE2eId()
    {
        return $this->mercuryE2eId;
    }

    /**
     * Set mercuryE2ePassword
     *
     * @param string $mercuryE2ePassword
     *
     * @return ForeupCourses
     */
    public function setMercuryE2ePassword($mercuryE2ePassword)
    {
        $this->mercuryE2ePassword = $mercuryE2ePassword;

        return $this;
    }

    /**
     * Get mercuryE2ePassword
     *
     * @return string
     */
    public function getMercuryE2ePassword()
    {
        return $this->mercuryE2ePassword;
    }

    /**
     * Set etsKey
     *
     * @param string $etsKey
     *
     * @return ForeupCourses
     */
    public function setEtsKey($etsKey)
    {
        $this->etsKey = $etsKey;

        return $this;
    }

    /**
     * Get etsKey
     *
     * @return string
     */
    public function getEtsKey()
    {
        return $this->etsKey;
    }

    /**
     * Set e2eAccountId
     *
     * @param string $e2eAccountId
     *
     * @return ForeupCourses
     */
    public function setE2eAccountId($e2eAccountId)
    {
        $this->e2eAccountId = $e2eAccountId;

        return $this;
    }

    /**
     * Get e2eAccountId
     *
     * @return string
     */
    public function getE2eAccountId()
    {
        return $this->e2eAccountId;
    }

    /**
     * Set e2eAccountKey
     *
     * @param string $e2eAccountKey
     *
     * @return ForeupCourses
     */
    public function setE2eAccountKey($e2eAccountKey)
    {
        $this->e2eAccountKey = $e2eAccountKey;

        return $this;
    }

    /**
     * Get e2eAccountKey
     *
     * @return string
     */
    public function getE2eAccountKey()
    {
        return $this->e2eAccountKey;
    }

    /**
     * Set useEtsGiftcards
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setUseEtsGiftcards($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->useEtsGiftcards = $value;

        return $this;
    }

    /**
     * Get useEtsGiftcards
     *
     * @return boolean
     */
    public function getUseEtsGiftcards()
    {
        return $this->useEtsGiftcards;
    }

    /**
     * Set facebookPageId
     *
     * @param string $facebookPageId
     *
     * @return ForeupCourses
     */
    public function setFacebookPageId($facebookPageId)
    {
        $this->facebookPageId = $facebookPageId;

        return $this;
    }

    /**
     * Get facebookPageId
     *
     * @return string
     */
    public function getFacebookPageId()
    {
        return $this->facebookPageId;
    }

    /**
     * Set facebookPageName
     *
     * @param string $facebookPageName
     *
     * @return ForeupCourses
     */
    public function setFacebookPageName($facebookPageName)
    {
        $this->facebookPageName = $facebookPageName;

        return $this;
    }

    /**
     * Get facebookPageName
     *
     * @return string
     */
    public function getFacebookPageName()
    {
        return $this->facebookPageName;
    }

    /**
     * Set facebookExtendedAccessToken
     *
     * @param string $facebookExtendedAccessToken
     *
     * @return ForeupCourses
     */
    public function setFacebookExtendedAccessToken($facebookExtendedAccessToken)
    {
        $this->facebookExtendedAccessToken = $facebookExtendedAccessToken;

        return $this;
    }

    /**
     * Get facebookExtendedAccessToken
     *
     * @return string
     */
    public function getFacebookExtendedAccessToken()
    {
        return $this->facebookExtendedAccessToken;
    }

    /**
     * Set allowFriendsToInvite
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setAllowFriendsToInvite($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->allowFriendsToInvite = $value;

        return $this;
    }

    /**
     * Get allowFriendsToInvite
     *
     * @return boolean
     */
    public function getAllowFriendsToInvite()
    {
        return $this->allowFriendsToInvite;
    }

    /**
     * Set paymentRequired
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setPaymentRequired($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->paymentRequired = $value;

        return $this;
    }

    /**
     * Get paymentRequired
     *
     * @return boolean
     */
    public function isPaymentRequired()
    {
        return $this->paymentRequired;
    }

    /**
     * Set sendEmailReminder
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setSendEmailReminder($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->sendEmailReminder = $value;

        return $this;
    }

    /**
     * Get sendEmailReminder
     *
     * @return boolean
     */
    public function isSendEmailReminder()
    {
        return $this->sendEmailReminder;
    }

    /**
     * Set sendTextReminder
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setSendTextReminder($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->sendTextReminder = $value;

        return $this;
    }

    /**
     * Get sendTextReminder
     *
     * @return boolean
     */
    public function isSendTextReminder()
    {
        return $this->sendTextReminder;
    }

    /**
     * Set seasonalPricing
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setSeasonalPricing($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->seasonalPricing = $value;

        return $this;
    }

    /**
     * Get seasonalPricing
     *
     * @return boolean
     */
    public function isSeasonalPricing()
    {
        return $this->seasonalPricing;
    }

    /**
     * Set includeTaxOnlineBooking
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setIncludeTaxOnlineBooking($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->includeTaxOnlineBooking = $value;

        return $this;
    }

    /**
     * Get includeTaxOnlineBooking
     *
     * @return boolean
     */
    public function isIncludeTaxOnlineBooking()
    {
        return $this->includeTaxOnlineBooking;
    }

    /**
     * Set foreupDiscountPercent
     *
     * @param float $foreupDiscountPercent
     *
     * @return ForeupCourses
     */
    public function setForeupDiscountPercent($foreupDiscountPercent)
    {
        $this->foreupDiscountPercent = $foreupDiscountPercent;

        return $this;
    }

    /**
     * Get foreupDiscountPercent
     *
     * @return float
     */
    public function getForeupDiscountPercent()
    {
        return $this->foreupDiscountPercent;
    }

    /**
     * Set noShowPolicy
     *
     * @param string $noShowPolicy
     *
     * @return ForeupCourses
     */
    public function setNoShowPolicy($noShowPolicy)
    {
        $this->noShowPolicy = $noShowPolicy;

        return $this;
    }

    /**
     * Get noShowPolicy
     *
     * @return string
     */
    public function getNoShowPolicy()
    {
        return $this->noShowPolicy;
    }

    /**
     * Set reservationEmailText
     *
     * @param string $reservationEmailText
     *
     * @return ForeupCourses
     */
    public function setReservationEmailText($reservationEmailText)
    {
        $this->reservationEmailText = $reservationEmailText;

        return $this;
    }

    /**
     * Get reservationEmailText
     *
     * @return string
     */
    public function getReservationEmailText()
    {
        return $this->reservationEmailText;
    }

    /**
     * Set reservationEmailPhoto
     *
     * @param integer $reservationEmailPhoto
     *
     * @return ForeupCourses
     */
    public function setReservationEmailPhoto($reservationEmailPhoto)
    {
        $this->reservationEmailPhoto = $reservationEmailPhoto;

        return $this;
    }

    /**
     * Get reservationEmailPhoto
     *
     * @return integer
     */
    public function getReservationEmailPhoto()
    {
        return $this->reservationEmailPhoto;
    }

    /**
     * Set ibeaconMajorId
     *
     * @param integer $ibeaconMajorId
     *
     * @return ForeupCourses
     */
    public function setIbeaconMajorId($ibeaconMajorId)
    {
        $this->ibeaconMajorId = $ibeaconMajorId;

        return $this;
    }

    /**
     * Get ibeaconMajorId
     *
     * @return integer
     */
    public function getIbeaconMajorId()
    {
        return $this->ibeaconMajorId;
    }

    /**
     * Set ibeaconEnabled
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setIbeaconEnabled($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->ibeaconEnabled = $value;

        return $this;
    }

    /**
     * Get ibeaconEnabled
     *
     * @return boolean
     */
    public function getIbeaconEnabled()
    {
        return $this->ibeaconEnabled;
    }

    /**
     * Set autoGratuity
     *
     * @param float $autoGratuity
     *
     * @return ForeupCourses
     */
    public function setAutoGratuity($autoGratuity)
    {
        $this->autoGratuity = $autoGratuity;

        return $this;
    }

    /**
     * Get autoGratuity
     *
     * @return float
     */
    public function getAutoGratuity()
    {
        return $this->autoGratuity;
    }

    /**
     * Set minimumFoodSpend
     *
     * @param float $minimumFoodSpend
     *
     * @return ForeupCourses
     */
    public function setMinimumFoodSpend($minimumFoodSpend)
    {
        $this->minimumFoodSpend = $minimumFoodSpend;

        return $this;
    }

    /**
     * Get minimumFoodSpend
     *
     * @return float
     */
    public function getMinimumFoodSpend()
    {
        return $this->minimumFoodSpend;
    }

    /**
     * Set hideEmployeeLastNameReceipt
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setHideEmployeeLastNameReceipt($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->hideEmployeeLastNameReceipt = $value;

        return $this;
    }

    /**
     * Get hideEmployeeLastNameReceipt
     *
     * @return boolean
     */
    public function isHideEmployeeLastNameReceipt()
    {
        return $this->hideEmployeeLastNameReceipt;
    }

    /**
     * Set quickbooksExportSettings
     *
     * @param string $quickbooksExportSettings
     *
     * @return ForeupCourses
     */
    public function setQuickbooksExportSettings($quickbooksExportSettings)
    {
        $this->quickbooksExportSettings = $quickbooksExportSettings;

        return $this;
    }

    /**
     * Get quickbooksExportSettings
     *
     * @return string
     */
    public function getQuickbooksExportSettings()
    {
        return $this->quickbooksExportSettings;
    }

    /**
     * Set foodAndBeverageV2
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setFoodAndBeverageV2($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->foodAndBeverageV2 = $value;

        return $this;
    }

    /**
     * Get foodAndBeverageV2
     *
     * @return boolean
     */
    public function isFoodAndBeverageV2()
    {
        return $this->foodAndBeverageV2;
    }

    /**
     * Set salesV2
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setSalesV2($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->salesV2 = $value;

        return $this;
    }

    /**
     * Get salesV2
     *
     * @return boolean
     */
    public function isSalesV2()
    {
        return $this->salesV2;
    }

    /**
     * Set requireEmployeePin
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setRequireEmployeePin($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->requireEmployeePin = $value;

        return $this;
    }

    /**
     * Get requireEmployeePin
     *
     * @return boolean
     */
    public function isRequireEmployeePin()
    {
        return $this->requireEmployeePin;
    }

    /**
     * Set erangeId
     *
     * @param string $erangeId
     *
     * @return ForeupCourses
     */
    public function setErangeId($erangeId)
    {
        $this->erangeId = $erangeId;

        return $this;
    }

    /**
     * Get erangeId
     *
     * @return string
     */
    public function getErangeId()
    {
        return $this->erangeId;
    }

    /**
     * Set erangePassword
     *
     * @param string $erangePassword
     *
     * @return ForeupCourses
     */
    public function setErangePassword($erangePassword)
    {
        $this->erangePassword = $erangePassword;

        return $this;
    }

    /**
     * Get erangePassword
     *
     * @return string
     */
    public function getErangePassword()
    {
        return $this->erangePassword;
    }

    /**
     * Set creditCardFee
     *
     * @param string $creditCardFee
     *
     * @return ForeupCourses
     */
    public function setCreditCardFee($creditCardFee)
    {
        $this->creditCardFee = $creditCardFee;

        return $this;
    }

    /**
     * Get creditCardFee
     *
     * @return string
     */
    public function getCreditCardFee()
    {
        return $this->creditCardFee;
    }

    /**
     * Set multiplePrinters
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setMultiplePrinters($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->multiplePrinters = $value;

        return $this;
    }

    /**
     * Get multiplePrinters
     *
     * @return boolean
     */
    public function getMultiplePrinters()
    {
        return $this->multiplePrinters;
    }

    /**
     * Set requireGuestCount
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setRequireGuestCount($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->requireGuestCount = $value;

        return $this;
    }

    /**
     * Get requireGuestCount
     *
     * @return boolean
     */
    public function getRequireGuestCount()
    {
        return $this->requireGuestCount;
    }

    /**
     * Set defaultKitchenPrinter
     *
     * @param integer $defaultKitchenPrinter
     *
     * @return ForeupCourses
     */
    public function setDefaultKitchenPrinter($defaultKitchenPrinter)
    {
        $this->defaultKitchenPrinter = $defaultKitchenPrinter;

        return $this;
    }

    /**
     * Get defaultKitchenPrinter
     *
     * @return integer
     */
    public function getDefaultKitchenPrinter()
    {
        return $this->defaultKitchenPrinter;
    }

    /**
     * Set cleanOutNightly
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setCleanOutNightly($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->cleanOutNightly = $value;

        return $this;
    }

    /**
     * Get cleanOutNightly
     *
     * @return boolean
     */
    public function isCleanOutNightly()
    {
        return $this->cleanOutNightly;
    }

    /**
     * Set elementAccountId
     *
     * @param string $elementAccountId
     *
     * @return ForeupCourses
     */
    public function setElementAccountId($elementAccountId)
    {
        $this->elementAccountId = $elementAccountId;

        return $this;
    }

    /**
     * Get elementAccountId
     *
     * @return string
     */
    public function getElementAccountId()
    {
        return $this->elementAccountId;
    }

    /**
     * Set elementAccountToken
     *
     * @param string $elementAccountToken
     *
     * @return ForeupCourses
     */
    public function setElementAccountToken($elementAccountToken)
    {
        $this->elementAccountToken = $elementAccountToken;

        return $this;
    }

    /**
     * Get elementAccountToken
     *
     * @return string
     */
    public function getElementAccountToken()
    {
        return $this->elementAccountToken;
    }

    /**
     * Set elementApplicationId
     *
     * @param string $elementApplicationId
     *
     * @return ForeupCourses
     */
    public function setElementApplicationId($elementApplicationId)
    {
        $this->elementApplicationId = $elementApplicationId;

        return $this;
    }

    /**
     * Get elementApplicationId
     *
     * @return string
     */
    public function getElementApplicationId()
    {
        return $this->elementApplicationId;
    }

    /**
     * Set elementAcceptorId
     *
     * @param string $elementAcceptorId
     *
     * @return ForeupCourses
     */
    public function setElementAcceptorId($elementAcceptorId)
    {
        $this->elementAcceptorId = $elementAcceptorId;

        return $this;
    }

    /**
     * Get elementAcceptorId
     *
     * @return string
     */
    public function getElementAcceptorId()
    {
        return $this->elementAcceptorId;
    }

    /**
     * Set useCourseFiring
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setUseCourseFiring($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->useCourseFiring = $value;

        return $this;
    }

    /**
     * Get useCourseFiring
     *
     * @return boolean
     */
    public function isUseCourseFiring()
    {
        return $this->useCourseFiring;
    }

    /**
     * Set courseFiringIncludeItems
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setCourseFiringIncludeItems($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->courseFiringIncludeItems = $value;

        return $this;
    }

    /**
     * Get courseFiringIncludeItems
     *
     * @return boolean
     */
    public function isCourseFiringIncludeItems()
    {
        return $this->courseFiringIncludeItems;
    }

    /**
     * Set teeSheetSpeedUp
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setTeeSheetSpeedUp($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->teeSheetSpeedUp = $value;

        return $this;
    }

    /**
     * Get teeSheetSpeedUp
     *
     * @return boolean
     */
    public function isTeeSheetSpeedUp()
    {
        return $this->teeSheetSpeedUp;
    }

    /**
     * Set useKitchenBuzzers
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setUseKitchenBuzzers($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->useKitchenBuzzers = $value;

        return $this;
    }

    /**
     * Get useKitchenBuzzers
     *
     * @return boolean
     */
    public function isUseKitchenBuzzers()
    {
        return $this->useKitchenBuzzers;
    }

    /**
     * Set hideModifierNamesKitchenReceipts
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setHideModifierNamesKitchenReceipts($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->hideModifierNamesKitchenReceipts = $value;

        return $this;
    }

    /**
     * Get hideModifierNamesKitchenReceipts
     *
     * @return boolean
     */
    public function isHideModifierNamesKitchenReceipts()
    {
        return $this->hideModifierNamesKitchenReceipts;
    }

    /**
     * Set foodBevSortBySeat
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setFoodBevSortBySeat($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->foodBevSortBySeat = $value;

        return $this;
    }

    /**
     * Get foodBevSortBySeat
     *
     * @return boolean
     */
    public function isFoodBevSortBySeat()
    {
        return $this->foodBevSortBySeat;
    }

    /**
     * Set disableFacebookWidget
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setDisableFacebookWidget($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->disableFacebookWidget = $value;

        return $this;
    }

    /**
     * Get disableFacebookWidget
     *
     * @return boolean
     */
    public function getDisableFacebookWidget()
    {
        return $this->disableFacebookWidget;
    }

    /**
     * Set serviceFeeActive
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setServiceFeeActive($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->serviceFeeActive = $value;

        return $this;
    }

    /**
     * Get serviceFeeActive
     *
     * @return boolean
     */
    public function isServiceFeeActive()
    {
        return $this->serviceFeeActive;
    }

	/**
	 * Set usePrinterExtension
	 *
	 * @param boolean $value
	 *
	 * @return ForeupCourses
	 */
	public function setUsePrinterExtension($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->usePrinterExtension = $value;

		return $this;
	}

	/**
	 * Get usePrinterExtension
	 *
	 * @return boolean
	 */
	public function getUsePrinterExtension()
	{
		return $this->usePrinterExtension;
	}

    /**
     * Set serviceFeeTax1Name
     *
     * @param string $serviceFeeTax1Name
     *
     * @return ForeupCourses
     */
    public function setServiceFeeTax1Name($serviceFeeTax1Name)
    {
        $this->serviceFeeTax1Name = $serviceFeeTax1Name;

        return $this;
    }

    /**
     * Get serviceFeeTax1Name
     *
     * @return string
     */
    public function getServiceFeeTax1Name()
    {
        return $this->serviceFeeTax1Name;
    }

    /**
     * Set serviceFeeTax1Rate
     *
     * @param string $serviceFeeTax1Rate
     *
     * @return ForeupCourses
     */
    public function setServiceFeeTax1Rate($serviceFeeTax1Rate)
    {
        $this->serviceFeeTax1Rate = $serviceFeeTax1Rate;

        return $this;
    }

    /**
     * Get serviceFeeTax1Rate
     *
     * @return string
     */
    public function getServiceFeeTax1Rate()
    {
        return $this->serviceFeeTax1Rate;
    }

    /**
     * Set serviceFeeTax2Name
     *
     * @param string $serviceFeeTax2Name
     *
     * @return ForeupCourses
     */
    public function setServiceFeeTax2Name($serviceFeeTax2Name)
    {
        $this->serviceFeeTax2Name = $serviceFeeTax2Name;

        return $this;
    }

    /**
     * Get serviceFeeTax2Name
     *
     * @return string
     */
    public function getServiceFeeTax2Name()
    {
        return $this->serviceFeeTax2Name;
    }

    /**
     * Set serviceFeeTax2Rate
     *
     * @param string $serviceFeeTax2Rate
     *
     * @return ForeupCourses
     */
    public function setServiceFeeTax2Rate($serviceFeeTax2Rate)
    {
        $this->serviceFeeTax2Rate = $serviceFeeTax2Rate;

        return $this;
    }

    /**
     * Get serviceFeeTax2Rate
     *
     * @return string
     */
    public function getServiceFeeTax2Rate()
    {
        return $this->serviceFeeTax2Rate;
    }

    /**
     * Set autoGratuityThreshold
     *
     * @param integer $autoGratuityThreshold
     *
     * @return ForeupCourses
     */
    public function setAutoGratuityThreshold($autoGratuityThreshold)
    {
        $this->autoGratuityThreshold = $autoGratuityThreshold;

        return $this;
    }

    /**
     * Get autoGratuityThreshold
     *
     * @return integer
     */
    public function getAutoGratuityThreshold()
    {
        return $this->autoGratuityThreshold;
    }

    /**
     * Set printSuggestedTip
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setPrintSuggestedTip($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->printSuggestedTip = $value;

        return $this;
    }

    /**
     * Get printSuggestedTip
     *
     * @return boolean
     */
    public function isPrintSuggestedTip()
    {
        return $this->printSuggestedTip;
    }

    /**
     * Set defaultRegisterLogOpen
     *
     * @param string $defaultRegisterLogOpen
     *
     * @return ForeupCourses
     */
    public function setDefaultRegisterLogOpen($defaultRegisterLogOpen)
    {
        $this->defaultRegisterLogOpen = $defaultRegisterLogOpen;

        return $this;
    }

    /**
     * Get defaultRegisterLogOpen
     *
     * @return string
     */
    public function getDefaultRegisterLogOpen()
    {
        return $this->defaultRegisterLogOpen;
    }

    /**
     * Set marketingTexting
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setMarketingTexting($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->marketingTexting = $value;

        return $this;
    }

    /**
     * Get marketingTexting
     *
     * @return boolean
     */
    public function isMarketingTexting()
    {
        return $this->marketingTexting;
    }

    /**
     * Set customerFieldSettings
     *
     * @param string $customerFieldSettings
     *
     * @return ForeupCourses
     */
    public function setCustomerFieldSettings($customerFieldSettings)
    {
        $this->customerFieldSettings = $customerFieldSettings;

        return $this;
    }

    /**
     * Get customerFieldSettings
     *
     * @return string
     */
    public function getCustomerFieldSettings()
    {
        return $this->customerFieldSettings;
    }

    /**
     * Set stackTeeSheets
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setStackTeeSheets($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->stackTeeSheets = $value;

        return $this;
    }

    /**
     * Get stackTeeSheets
     *
     * @return boolean
     */
    public function isStackTeeSheets()
    {
        return $this->stackTeeSheets;
    }

    /**
     * Set alwaysListAll
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setAlwaysListAll($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->alwaysListAll = $value;

        return $this;
    }

    /**
     * Get alwaysListAll
     *
     * @return boolean
     */
    public function isAlwaysListAll()
    {
        return $this->alwaysListAll;
    }

    /**
     * Set foodBevTipLineFirstReceipt
     *
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setFoodBevTipLineFirstReceipt($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->foodBevTipLineFirstReceipt = $value;

        return $this;
    }

    /**
     * Get foodBevTipLineFirstReceipt
     *
     * @return boolean
     */
    public function isFoodBevTipLineFirstReceipt()
    {
        return $this->foodBevTipLineFirstReceipt;
    }



    /**
     * Get person
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return boolean
     */
    public function isHideRegistration()
    {
        return $this->hideRegistration;
    }

    /**
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setHideRegistration($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->hideRegistration = $value;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isMobileAppActive()
    {
        return $this->mobileAppActive;
    }

    /**
     * @param boolean $value
     *
     * @return ForeupCourses
     */
    public function setMobileAppActive($value)
    {
	    if(is_string($value)){
		    $tmp = json_decode(strtolower($value));
		    if(isset($tmp))$value = $tmp;
	    }
        $this->mobileAppActive = $value;

	    return $this;
    }


    public function getGroupSharedTeesheets()
    {
    	$courses = $this->getGroupMemberCourses("tee_sheet");
    	$teesheets = [];
    	foreach($courses as $course){
		    foreach($course->getMyTeeSheets() as $teesheet){
		    	$teesheets[] = $teesheet;
		    }
	    }
	    return $teesheets;
    }

    public function getGroupSharedTeesheetIds()
    {
    	$teesheets = $this->getGroupSharedTeesheets();
    	$teesheet_ids = [];
    	foreach($teesheets as $teesheet){
    		/** @var ForeupTeesheet $teesheet */
    		$teesheet_ids[] = $teesheet->getId();
	    }
	    return $teesheet_ids;
    }
    /*
     * @return ForeupCourses
     */
    public function getGroupMemberCourses($permission = "customers",$type = "linked")
    {
	    $courses = new ArrayCollection([$this]);
	    if($this->getCourseGroups()){
		    foreach($this->getCourseGroups() as $group){
			    if($group->getType() == $type && $group->hasPermission($permission)){
				    $courses = new ArrayCollection(
					    array_merge($group->getCourses()->toArray(), $courses->toArray())
				    );
			    }

		    }
	    }
	    return $courses;
    }

    public function getGroupMemberIds($permission = "customers",$type = "linked")
    {
		$courses = $this->getGroupMemberCourses($permission,$type);
	    $courseIds = [];
	    foreach($courses as $course){
		    $courseIds[] = $course->getCourseId();
	    }

	    return $courseIds;
    }

	/**
	 * @return string
	 */
	public function getTermsAndConditions()
	{
		return $this->termsAndConditions;
	}

	/**
	 * @param string $termsAndConditions
	 *
	 * @return ForeupCourses
	 */
	public function setTermsAndConditions($termsAndConditions)
	{
		$this->termsAndConditions = $termsAndConditions;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getOnlineBookingWelcomeMessage()
	{
		return $this->onlineBookingWelcomeMessage;
	}

	/**
	 * @param string $onlineBookingWelcomeMessage
	 *
	 * @return ForeupCourses
	 */
	public function setOnlineBookingWelcomeMessage($onlineBookingWelcomeMessage)
	{
		$this->onlineBookingWelcomeMessage = $onlineBookingWelcomeMessage;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isAutoEmailReceipt()
	{
		return $this->autoEmailReceipt;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setAutoEmailReceipt($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->autoEmailReceipt = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isAutoEmailNoPrint()
	{
		return $this->autoEmailNoPrint;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setAutoEmailNoPrint($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->autoEmailNoPrint = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isRequireSignatureMemberPayments()
	{
		return $this->requireSignatureMemberPayments;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setRequireSignatureMemberPayments($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->requireSignatureMemberPayments = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isHideTaxable()
	{
		return $this->hideTaxable;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setHideTaxable($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->hideTaxable = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isAllowEmployeeRegisterLogBypass()
	{
		return $this->allowEmployeeRegisterLogBypass;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setAllowEmployeeRegisterLogBypass($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->allowEmployeeRegisterLogBypass = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isTeetimeDefaultToPlayer1()
	{
		return $this->teetimeDefaultToPlayer1;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setTeetimeDefaultToPlayer1($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->teetimeDefaultToPlayer1 = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isPrintTeeTimeDetails()
	{
		return $this->printTeeTimeDetails;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setPrintTeeTimeDetails($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->printTeeTimeDetails = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isReceiptPrintAccountBalance()
	{
		return $this->receiptPrintAccountBalance;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setReceiptPrintAccountBalance($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->receiptPrintAccountBalance = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isPrintMinimumsOnReceipt()
	{
		return $this->printMinimumsOnReceipt;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setPrintMinimumsOnReceipt($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->printMinimumsOnReceipt = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isLimitFeeDropdownByCustomer()
	{
		return $this->limitFeeDropdownByCustomer;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setLimitFeeDropdownByCustomer($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->limitFeeDropdownByCustomer = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isRequireCustomerOnSale()
	{
		return $this->requireCustomerOnSale;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setRequireCustomerOnSale($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->requireCustomerOnSale = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isCashDrawerOnSale()
	{
		return $this->cashDrawerOnSale;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setCashDrawerOnSale($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->cashDrawerOnSale = $value;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param string $category
	 *
	 * @return ForeupCourses
	 */
	public function setCategory($category)
	{
		$this->category = $category;

		return $this;
	}



	/**
	 * @return string
	 */
	public function getDepartment()
	{
		return $this->department;
	}

	/**
	 * @param string $department
	 *
	 * @return ForeupCourses
	 */
	public function setDepartment($department)
	{
		$this->department = $department;

		return $this;
	}

	/**
	 * @return ForeupMobileAppModules
	 */
	public function getModules()
	{
		return $this->modules;
	}

	/**
	 * @param ForeupMobileAppModules $modules
	 *
	 * @return ForeupCourses
	 */
	public function setModules($modules)
	{
		$this->modules = $modules;

		return $this;
	}

	/**
	 * @return ForeupApiUserCourses
	 */
	public function getApiUserCourses()
	{
		return $this->apiUserCourses;
	}

	/**
	 * @param ForeupApiUserCourses $apiUserCourses
	 *
	 * @return ForeupCourses
	 */
	public function setApiUserCourses($apiUserCourses)
	{
		$this->apiUserCourses = $apiUserCourses;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isUseNewPermissions()
	{
		return $this->useNewPermissions;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setUseNewPermissions($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->useNewPermissions = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isSideBySideTeeSheets()
	{
		return $this->sideBySideTeeSheets;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setSideBySideTeeSheets($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->sideBySideTeeSheets = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isCurrentDayCheckinsOnly()
	{
		return $this->currentDayCheckinsOnly;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setCurrentDayCheckinsOnly($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->currentDayCheckinsOnly = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isOnlineGiftcardPurchases()
	{
		return $this->onlineGiftcardPurchases;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setOnlineGiftcardPurchases($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->onlineGiftcardPurchases = $value;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isShowInvoiceSaleItems()
	{
		return $this->showInvoiceSaleItems;
	}

	/**
	 * @param bool $value
	 *
	 * @return ForeupCourses
	 */
	public function setShowInvoiceSaleItems($value)
	{
		if(is_string($value)){
			$tmp = json_decode(strtolower($value));
			if(isset($tmp))$value = $tmp;
		}
		$this->showInvoiceSaleItems = $value;

		return $this;
	}

    /**
     * @return string
     */
    public function getTeedOffColor()
    {
        return $this->teedOffColor;
    }

    /**
     * @param string $teedOffColor
     *
     * @return ForeupCourses
     */
    public function setTeedOffColor($teedOffColor)
    {
        $this->teedOffColor = $teedOffColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getTeeTimeCompletedColor()
    {
        return $this->teeTimeCompletedColor;
    }

    /**
     * @param string $teeTimeCompletedColor
     *
     * @return ForeupCourses
     */
    public function setTeeTimeCompletedColor($teeTimeCompletedColor)
    {
        $this->teeTimeCompletedColor = $teeTimeCompletedColor;

        return $this;
    }


}

