<?php

namespace foreup\rest\models\repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use foreup\rest\models\entities\ForeupAccountCustomers;


class AccountLedgerRepository extends EntityRepository
{


	public function create($ledger_item)
	{
		$max_number = $this->getEntityManager()->createQueryBuilder('al')
			->select('MAX(al.number)')
			->from('e:ForeupAccountLedger', 'al')
			->where("al.account = :account_id")
			->setParameter("account_id",(int)$ledger_item->getAccount()->getId())
			->getQuery()
			->getSingleScalarResult();
		$max_number++;
		$ledger_item->setNumber($max_number);
		$this->getEntityManager()->persist($ledger_item);
	}


	public function getUsersPendingCharges(ForeupAccountCustomers $userAccount)
	{
		$usersBalance = $this->getEntityManager()->createQueryBuilder('al')
				->select('SUM(al.amountOpen)')
				->from('e:ForeupAccountLedger', 'al')
				->andWhere("al.account = :account")
				->andWhere("al.personId = :user")
				->andWhere("al.type IN ('sale','return','credit','charge','payment')")
				->setParameter("account",$userAccount->getAccount())
				->setParameter("user",$userAccount->getPersonId())
				->getQuery()
				->getSingleScalarResult();
		if($usersBalance == null)
			$usersBalance = 0;
		return $usersBalance;
	}
	/**
	 * @param \foreup\rest\models\entities\ForeupAccounts $account
	 * @return \foreup\rest\models\entities\ForeupAccountLedger[]
	 */
	public function getAllOpenItems($account)
	{
		$transactions = $this->getEntityManager()->createQueryBuilder('al')
			->select('al')
			->from('e:ForeupAccountLedger', 'al')
			->andWhere("al.account = :account")
			->andWhere("al.amountOpen > 0")
			->setParameter("account",$account)
			->getQuery()
			->getResult();

		return $transactions;
	}

}