<?php

namespace foreup\rest\models\repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;


class CoursesRepository extends EntityRepository
{

    /**
     * @param $email
     * @param $password
     * @return \foreup\rest\models\entities\ForeupUsers
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get_all_mobile_courses()
    {
        $qb = $this->createQueryBuilder('courses');
        $dql = $qb
            ->select('partial courses.{courseId,name,address,city,state,zip,phone,mobileAppSummary,baseColor,
                                       mobileAppIconUrl,mobileAppRefreshTimestamp,courseSummary,mobileAppShortTitle,
                                       mobileTestFlightEmail,hideRegistration,mobileAppIosPublishedVersion} ')
            ->leftJoin('courses.courseInformation','course_info')
            ->where("course_info.ios = :ios")
            ->andWhere("courses.mobileAppActive = 1")
            ->getDQL();
        $em =  $this->getEntityManager();
        return $em->createQuery($dql)
            ->setParameter("ios",'Y')
            ->getResult();
    }

    public function get_all()
    {
        /*
        $qb = $this->createQueryBuilder('courses');
        $dql = $qb
            ->select('partial courses.{courseId,name} ')
            ->getDQL();
        $em =  $this->getEntityManager();
        return $em->createQuery($dql)
            ->getResult();
        */
        $em =  $this->getEntityManager();

        $q = $em->createQuery('SELECT c.courseId, c.name, c.address, c.city, c.state, c.zip, c.phone from foreup\rest\models\entities\ForeupCourses c');
        $res = $q->getResult(); // gets an array...
        return $res;
    }

    public function get_suggestions($search, $limit = 25)
    {
        $em =  $this->getEntityManager();
        $limit *= 1;
        $q = $em->createQuery('SELECT c.courseId, c.name, c.address, c.city, c.state, c.zip, c.phone from foreup\rest\models\entities\ForeupCourses c where c.courseId = :eq or c.name like :lk');
        $q->setMaxResults($limit);
        $q->setParameter('lk',"%$search%");
        $q->setParameter('eq',$search);
        $res = $q->getResult(); // gets an array...
        return $res;
    }
}