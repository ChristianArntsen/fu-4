<?php

namespace foreup\rest\models\repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use foreup\rest\models\entities\ForeupMarketingCampaigns;


class MarketingCampaignsRepository extends EntityRepository
{

    /**
     * @return \foreup\rest\models\repositories\MarketingCampaignsRepository
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAll()
    {
        $qb = $this->createQueryBuilder('marketing_campaigns');
        $dql = $qb
            ->select('marketing_campaigns')
            ->getDQL();
        $em =  $this->getEntityManager();
        $q = $em->createQuery($dql);
        return $q->getResult();
    }

    public function get($id) {
        $em = $this->getEntityManager();
        return $em->find('foreup\rest\models\entities\ForeupMarketingCampaigns',$id);
    }


    public function unstick($id, $promote)
    {
        $results = array('type'=>'marketing_campaigns','id'=> (string) $id,
            'attributes'=>array(),
            'meta'=>array()
        );
        $entity = $this->get($id);

        $qb = $this->createQueryBuilder('log_entry');
        $tp = $promote?'sent':'draft';

        $results['meta']['action']=$tp;

        $entity->setAttempts($promote);
        $entity->setQueued($promote);
        $entity->setIsSent($promote);
        $entity->setStatus($tp);
        $this->getEntityManager()->flush();

        $results['meta']['success'] = $tp === $entity->getStatus();
        $results['attributes']['status'] = $entity->getStatus();
        return array('data'=>$results);
    }

}

?>