<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 1/19/2017
 * Time: 4:43 PM
 */

namespace foreup\rest\models\repositories;


use Doctrine\ORM\Query\Expr;

class Filter
{
	private $expression,$value;

	/**
	 * Filter constructor.
	 * @param Expr\Comparison $expression
	 * @param $value
	 */
	public function __construct($expression, $value,$boundParameter = null)
	{
		$this->expression = $expression;
		$this->value = $value;
		$this->boundParamter = $boundParameter;
	}

	/**
	 * @return Expr\Comparison
	 */
	public function getExpression()
	{
		return $this->expression;
	}

	/**
	 * @param Expr\Base $expression
	 */
	public function setExpression($expression)
	{
		$this->expression = $expression;
	}

	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param mixed $value
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}

	public function getBoundParamter()
	{
		return $this->boundParamter;
	}


}