<?php

namespace foreup\rest\models\repositories;

use Carbon\Carbon;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;


class AccountsRepository extends EntityRepository
{
	/**
	 * @param \foreup\rest\models\entities\ForeupAccounts $account
	 * @return \foreup\rest\models\entities\ForeupAccountLedger[]
	 */
	public function create($account)
	{
		$account->setDateCreated(Carbon::now()->toDateTimeString());
		$account->setOrganizationId($this->user->getCourseId());
		$this->getEntityManager()->persist($account);
	}
}