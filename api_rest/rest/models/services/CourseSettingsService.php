<?php
namespace foreup\rest\models\services;


use Doctrine\ORM\EntityManager;
use foreup\rest\models\entities\ForeupCourses;

class CourseSettingsService
{

	/**
	 * @var
	 */
	/**
	 * @var AuthenticatedUser
	 */
	private $coursesRepo,$authUser;

	/**
	 * CourseSettingsService constructor.
	 * @param EntityManager $em
	 * @param AuthenticatedUser $authUser
	 */
	public function __construct(EntityManager $em, AuthenticatedUser $authUser)
	{
		$this->coursesRepo = $em->getRepository("e:ForeupCourses");
		$this->authUser = $authUser;
		$this->loadSettings();
	}


	/**
	 * @return null|ForeupCourses
	 */
	public function loadSettings($courseId = null)
	{
		if(isset($courseId)){
			$course = $this->coursesRepo->find($courseId);
		} else {
			$course = $this->coursesRepo->find($this->authUser->getCid());
		}
		return $course;
	}
}