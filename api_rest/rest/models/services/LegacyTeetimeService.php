<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 9/23/2016
 * Time: 3:36 PM
 */

namespace foreup\rest\models\services;


use Carbon\Carbon;
use foreup\auth\json_web_token;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupSales;
use foreup\rest\models\entities\ForeupTeetime;
use foreup\rest\models\entities\LegacyObjects\PriceClass;
use foreup\rest\models\entities\LegacyObjects\TeetimeSlot;
use GuzzleHttp\Client;
use JsonSchema\Exception\InvalidArgumentException;

class LegacyTeetimeService
{


	/**
	 * @var ForeupCustomers $customer
	 */
	private $timezone,$date,$startTime,$endTime,$holes,$customerId,$priceClassId;

	public function __construct($url)
	{
		$this->timezone = "America/Denver";
		$this->client = new Client(['base_uri' => $url.'/index.php/']);
		$this->headers = $headers = [
			'x-authorization' => 'Bearer ',
			"Api_key"=>"no_limits",
			"Content-Type"=>"application/json"
		];
		$this->date = Carbon::now()->toDateString();
		$this->startTime = 0000;
		$this->endTime = 2359;
		$this->holes = 18;
	}

	public function setToken($token)
	{
		$this->headers['x-authorization'] = "Bearer ".$token;
	}

	/**
	 * @return mixed
	 */
	public function getTimezone()
	{
		return $this->timezone;
	}

	/**
	 * @param mixed $timezone
	 */
	public function setTimezone($timezone)
	{
		$this->timezone = $timezone;
	}

	public function bookTime(ForeupTeetime $booking,$teesheet_id = "")
	{
		$url = '/teesheets/save_simple';
		if(!empty($teesheet_id)){
			$url .= "/".$teesheet_id;
		}
		$headers = $this->headers;
		$headers['Content-Type'] = "application/x-www-form-urlencoded";
		$response = $this->client->request('POST', $url, [
			"headers"=>$headers,
			'form_params' => [
				"start"=>$booking->getStart(),
				"side"=>$booking->getSide(),
				"event_type"=>"teetime",
				"teetime_details"=>$booking->getDetails(),
				"teetime_holes"=>$booking->getHoles(),
				"players"=>$booking->getPlayerCount(),
				"carts"=>$booking->getCarts(),
				"teetime_title"=>$booking->getTitle(),
				"teesheet_id"=>$booking->getTeesheetId(),
				"booking_source"=>"api",
			]
		]);
		$responseBody = json_decode($response->getBody()->getContents(),true);
		if($responseBody['success']){
			return $responseBody['data']['teetime_id'];
		} else {
			if(isset($responseBody['msg'])){
				throw new \Exception($responseBody['msg']);
			} else {
				throw new \Exception("Tee time slot isn't available.");
			}
		}

		return false;
	}

	public function getPriceClasses($token,$teetimeId)
	{
		$url = '/teesheets/get_price_classes';
		$url .= "/".$teetimeId;

		$headers = $this->headers;
		$headers['Content-Type'] = "application/x-www-form-urlencoded";
		$this->setToken($token);
		$response = $this->client->request('GET', $url, [
			"headers"=>$this->headers,
			"query"=> [
				"include_tax"=>true
			]
		]);
		$bodyContent = $response->getBody()->getContents();
		$responseBody = json_decode($bodyContent);
		$priceClasses = [];
		if($responseBody){
			foreach($responseBody as $priceClass){
				$priceClasses[] = new PriceClass($priceClass);
			}
		}

		return $priceClasses;
	}

	public function getTimes($token,$scheduleId)
	{
		$timezone =  $this->timezone;
		$data = [
			"date"=>$this->date,
			"start_time"=>$this->startTime,
			"end_time"=>$this->endTime,
			"holes"=>$this->holes,
			"schedule_id"=>$scheduleId,
			"api_key"=>"no_limits"
		];
		if(isset($this->customerId)){
			$data['customer_id']=$this->customerId;
		}
		if(isset($this->priceClassId)){
			$data['price_class_id']=$this->priceClassId;
		}
		$this->setToken($token);
			$response = $this->client->request('GET', '/index.php/api/booking/times_advanced', [
				"headers"=>$this->headers,
				"query"=> $data
			]);



		$responseBody = json_decode($response->getBody()->getContents());
		$teetimes = [];
		foreach($responseBody as $teetimeSlot)
		{
			$teetimeSlot->timezone = $timezone;
			$teetimes[] = new TeetimeSlot($teetimeSlot);
		}


		return $teetimes;
	}

	public function createCart($bookingId,Array $positions)
	{
		$data = [
			"positions"=>$positions
		];
		$response = $this->client->request('POST', '/index.php/teesheets/create_cart_from_teetime/'.$bookingId, [
			"headers"=>$this->headers,
			"body"=> json_encode($data)
		]);



		$responseBody = json_decode($response->getBody()->getContents());

		if(empty($responseBody->cart_id)){
			return false;
		}


		return $responseBody->cart_id;
	}




	/**
	 * @return string
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param string $date
	 */
	public function setDate($date)
	{
		$date = Carbon::parse($date);
		$this->date = $date->toDateString();
	}

	/**
	 * @return int
	 */
	public function getStartTime()
	{
		return $this->startTime;
	}

	/**
	 * @param int $startTime
	 */
	public function setStartTime($startTime)
	{
		if($startTime < 0 || $startTime > 2359){
			throw new \Exception("Invalid format for startTime, must be between 0000 and 2359");
		}

		$this->startTime = $startTime;
	}

	/**
	 * @return mixed
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

	/**
	 * @param mixed $customer
	 */
	public function setCustomer($customerId)
	{
		$this->customerId = $customerId;
	}

	/**
	 * @return int
	 */
	public function getEndTime()
	{
		return $this->endTime;
	}

	/**
	 * @param int $endTime
	 */
	public function setEndTime($endTime)
	{
		if($endTime < 0 || $endTime > 2359){
			throw new \Exception("Invalid format for endTime, must be between 0000 and 2359");
		}
		$this->endTime = $endTime;
	}

	/**
	 * @return int
	 */
	public function getHoles()
	{
		return $this->holes;
	}

	/**
	 * @param int $holes
	 */
	public function setHoles($holes)
	{
		$this->holes = $holes;
	}


	public function getPriceClassId()
	{
		return $this->priceClassId;
	}

	/**
	 * @param int $priceClassId
	 */
	public function setPriceClassId(int $priceClassId)
	{
		$this->priceClassId = $priceClassId;
	}


}