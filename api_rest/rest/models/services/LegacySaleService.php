<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 9/23/2016
 * Time: 3:36 PM
 */

namespace foreup\rest\models\services;


use Carbon\Carbon;
use foreup\rest\models\entities\ForeupSales;
use foreup\rest\models\entities\ForeupSalesPayments;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class LegacySaleService
{

	private $teesheetId,$token;

	public function __construct($url)
	{
		$this->client = new Client(['base_uri' => $url.'/index.php/']);
		$this->headers = $headers = [
			'x-authorization' => 'Bearer ',
			"api-key"=>"no_limits",
			"Content-Type"=>"application/json"
		];
	}

	private function setToken($token)
	{
		$this->headers['x-authorization'] = "Bearer ".$token;
	}

	private function initializeCart($sale)
	{
		$request = new \GuzzleHttp\Psr7\Request('POST', '/api/cart/initialize',$this->headers);
		$response = $this->client->send($request);
		$responseBody = \GuzzleHttp\json_decode($response->getBody()->getContents());
		return $responseBody->cart_id;
	}

	private function savePayments($sale,$cartId)
	{
		/** @var ForeupSalesPayments $payment */
		foreach($sale->getPayments() as $payment)
		{
			$request = new \GuzzleHttp\Psr7\Request('POST', '/api/cart/'.$cartId.'/payments',$this->headers,json_encode([
				"type"=>$payment->getType(),
				"description"=>$payment->getPaymentType(),
				"record_id"=>$payment->getInvoiceId(),
				"amount"=>$payment->getPaymentAmount()
			]));



			$response = $this->client->send($request);
			$responseBody = json_decode($response->getBody()->getContents());
			if(!$responseBody->success){
				return $responseBody;
			}
		}
		return true;
	}

	private function saveSale($sale,$cartId)
	{

		$requestBody = [
			"status"=>"complete",
			"total"=>$sale->getTotal(),
			"subtotal"=>$sale->getSubTotal(),
			"tax"=>$sale->getTax(),
			"customer_note"=>$sale->getCustomerNote(),
			"items"=>[]
		];
		foreach($sale->getItems() as $item)
		{
			$requestBody["items"][] =
				[
					"line"=>$item->getLine(),
					"name"=>$item->getDescription(),
					"item_type"=>$item->getType(),
					"unit_price_includes_tax"=>true,
					"item_cost_price"=>$item->getItemCostPrice(),
					"total_cost"=>$item->getTotalCost(),
					"item_id"=>$item->getItem()->getItemId(),
					"total"=>$item->getTotal(),
					"subtotal"=>$item->getSubtotal(),
					"tax"=>$item->getTax(),
					"quantity"=>$item->getQuantityPurchased(),
					"unit_price"=>$item->getItemUnitPrice(),
					"discount_percent"=>$item->getDiscountPercent(),
					"description"=>$item->getDescription(),
					"selected"=>true
				];
		}


		$request = new \GuzzleHttp\Psr7\Request('POST', '/api/cart/'.$cartId,$this->headers,json_encode($requestBody));
		$response = $this->client->send($request);
		return json_decode($response->getBody()->getContents());
	}

	public function completeSale(ForeupSales $sale,$token)
	{
		$this->setToken($token);
		$cartId = $this->initializeCart($sale);
		$savePaymentsResults = $this->savePayments($sale,$cartId);
		if($savePaymentsResults !== true){
			return $savePaymentsResults;
		}
		try{
			$response = $this->saveSale($sale,$cartId);
		} catch(\Exception $e){
			$responseBody = (string)$e->getResponse()->getBody(true);
			preg_match("/(\{.*\})$/",$responseBody,$matches);
			$response = json_decode($matches[1]);
		}


		return $response;
	}
}