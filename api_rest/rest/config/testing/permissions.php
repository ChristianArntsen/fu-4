<?php
unset($config);
/*
 * This configuration file is used to firewall requests
 * based on authenticated user_level and requested controller.
 *
 * Rules are executed in order.
 * Default Allow -> All users with level 5 to all controllers.
 * Default Deny -> All unmatched requests.
 *
 * Rules can be of the following types:
 *    exact
 *    pcre
 *
 * Minimum Levels of -1 is a special case, which will allow non-authenticated requests
 *
 * Rules are added to $config['permissions'] array
 * ex. $config['permissions'][] = array( "type"=>"pcre", "min_level" => 3, "endpoint" => "/^(login\.controller:).*$");
 *     ^ will match all **routed** login endpoints where user is equal to or greater than 3
 *
 * ex. $config['permissions'][] = array( "type"=>"exact", "min_level" => 3, "endpoint" => "login.controller:get"); //will match all **routed** login endpoints
 *     ^ will match the **routed** endpoint login.controller:get where user is equal to or greater than 3
 *
 */

//$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(courses\.controller:).*$/');
//$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(marketing_campaigns\.controller:).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(courses\.controller:).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(employee_audit_log\.controller:).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(marketing_campaigns\.controller:).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(courses\.controller:).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(marketing_campaigns\.controller:).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(scores\.controller:).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(accounts\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(customers\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(customerSales\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(teesheetStats\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(sales\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(terminals\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(employees\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(hooks\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(teesheetTeetimes\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(courseTeesheets\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(teesheetBookings\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(teesheetSeasons\.controller).*$/');

$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(carts\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(inventory\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(items\.controller).*$/');
$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(coursePageSettings\.controller).*$/');
$config['permissions'][] = array( 'type'=>"pcre", 'min_level' => -1, 'endpoint' => '/^(tokens\.controller:).*$/'); // Allow all requests to login endpoint(s)

