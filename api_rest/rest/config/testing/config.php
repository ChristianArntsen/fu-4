<?php
$config['orm.auto_generate_proxies'] = Doctrine\Common\Proxy\AbstractProxyFactory::AUTOGENERATE_NEVER;
$config['legacy_url'] = "http://development.foreupsoftware.com";
$config['cache'] = [
    "driver"=>"array",
    "path"=>sys_get_temp_dir()
];