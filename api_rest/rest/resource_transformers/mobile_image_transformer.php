<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupMobileCourseImages;
use League\Fractal;

class mobile_image_transformer extends Fractal\TransformerAbstract
{
	public function transform(ForeupMobileCourseImages $image)
	{
		return [
			'id' => (int)$image->getId(),
			'url' => $image->getUrl(),
			'description' => $image->getDescription(),
			'title' => $image->getTitle()
		];
	}
}