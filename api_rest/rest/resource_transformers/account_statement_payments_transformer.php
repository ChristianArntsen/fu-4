<?php

namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupAccountStatementPayments;
use League\Fractal;
class account_statement_payments_transformer extends Fractal\TransformerAbstract
{
	/**
	 * @var array
	 */
	protected $defaultIncludes = [
		'statement',
		'chargeInvoice',
		'customer',
		'sale'
	];

	public function transform(ForeupAccountStatementPayments $entity)
	{
		return [
			'id' => $entity->getId(),
			'amount' => $entity->getAmount(),
			'type' => $entity->getType(),
			'subType' => $entity->getSubType(),
			'accountNumber' => $entity->getAccountNumber(),
            'saleId' => $entity->getSale()->getSaleId(),
			'name' => $entity->getName(),
			'isAutopayment' => $entity->getIsAutopayment(),
			'dateCreated' => !empty($entity->getDateCreated())?$entity->getDateCreated()->format(\DateTime::ISO8601):null,
		];
	}

	/**
	 * @param ForeupAccountStatementPayments|null $charge
	 * @return Fractal\Resource\Item
	 */
	public function includeStatement(ForeupAccountStatementPayments $entity = null)
	{
		$referred = $entity->getStatement();
		if(!isset($referred)) return null;

		return $this->item($referred, new account_statements_transformer(), 'customer');
	}

	/**
	 * @param ForeupAccountStatementPayments|null $charge
	 * @return Fractal\Resource\Item
	 */
	public function includeChargeInvoice(ForeupAccountStatementPayments $entity = null)
	{
		$referred = $entity->getChargeInvoice();
		if(!isset($referred)) return null;

		return $this->item($referred, new sales_credit_card_payment_transformer(), 'chargeInvoice');
	}

	/**
	 * @param ForeupAccountStatementPayments|null $charge
	 * @return Fractal\Resource\Item
	 */
	public function includeCustomer(ForeupAccountStatementPayments $entity = null)
	{
		$referred = $entity->getCustomer();
		if(!isset($referred)) return null;

		return $this->item($referred, new customer_transformer(), 'customer');
	}

	/**
	 * @param ForeupAccountStatementPayments|null $charge
	 * @return Fractal\Resource\Item
	 */
	public function includeSale(ForeupAccountStatementPayments $entity = null)
	{
		$referred = $entity->getSale();
		if(!isset($referred)) return null;

		return $this->item($referred, new sales_transformer(), 'sale');
	}
}
?>