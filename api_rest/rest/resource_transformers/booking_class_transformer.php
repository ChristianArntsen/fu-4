<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupBookingClasses;
use League\Fractal;

class booking_class_transformer extends Fractal\TransformerAbstract
{
    public function transform(ForeupBookingClasses $class)
    {
    	$formattedObject = [];
		$formattedObject['id']=$class->getBookingClassId();
		$formattedObject['isActive']=$class->getActive();
		$formattedObject['name']=$class->getName();
		$formattedObject['priceClass']=$class->getPriceClass();
		$formattedObject['onlineBookingProtected']=$class->getOnlineBookingProtected();
		$formattedObject['requireCreditCard']=$class->getRequireCreditCard();
		$formattedObject['onlineOpenTime']=$class->getOnlineOpenTime();
		$formattedObject['onlineCloseTime']=$class->getOnlineCloseTime();
		$formattedObject['daysInBookingWindow']=$class->getDaysInBookingWindow();
		$formattedObject['minimumPlayers']=$class->getMinimumPlayers();
		$formattedObject['limitHoles']=$class->getLimitHoles();
		$formattedObject['bookingCarts']=$class->getBookingCarts();
		$formattedObject['deleted']=$class->getDeleted();
		$formattedObject['useCustomerPricing']=$class->getUseCustomerPricing();
		$formattedObject['isAggregate']=$class->getIsAggregate();
		$formattedObject['payOnline']=$class->getPayOnline();
		$formattedObject['hideOnlinePrices']=$class->getHideOnlinePrices();
		$formattedObject['onlineRateSetting']=$class->getOnlineRateSetting();
		$formattedObject['showFullDetails']=$class->getShowFullDetails();
		$formattedObject['passItemIds']=$class->getPassItemIds();
		$formattedObject['bookingFeeItemId']=$class->getBookingFeeItemId();
		$formattedObject['bookingFeeTerms']=$class->getBookingFeeTerms();
		$formattedObject['bookingFeePerPerson']=$class->getBookingFeePerPerson();
		$formattedObject['allowNameEntry']=$class->getAllowNameEntry();
	    return $formattedObject;
    }
}