<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupAccountLedger;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupAccounts;
use League\Fractal;
class account_type_statement_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'created_by','deleted_by'
	];

	public function transform(ForeupAccountRecurringStatements $statement)
	{
		return [
			'id' => (int)$statement->getId(),
			'name' => $statement->getName(),
			'is_active' => (bool)$statement->getIsActive(),
			'date_created' => $statement->getDateCreated(),
			'date_deleted' => $statement->getDateDeleted() ,
			'start_date' => $statement->getStartDate(),
			'include_balance_forward' => (bool)$statement->getIncludeBalanceForward(),
			'send_empty_statements' => (bool)$statement->getSendEmptyStatements(),
			'itemize_sales' => (bool)$statement->getItemizeSales(),
			'itemize_invoices' => (bool)$statement->getItemizeInvoices(),
			'itemize_charges' => (bool)$statement->getItemizeCharges(),
			'include_sales' => (bool)$statement->getIncludeSales(),
			'include_invoices' => (bool)$statement->getIncludeInvoices(),
			'include_charges' => (bool)$statement->getIncludeCharges()
		];
	}

	public function includeDeletedBy(ForeupAccountRecurringStatements $statement)
	{
		return $this->item($statement->getDeletedBy(),new employees_transformer(),"employees");
	}
	public function includeCreatedBy(ForeupAccountRecurringStatements $statement)
	{
		return $this->item($statement->getCreatedBy(),new employees_transformer(),"employees");
	}
}