<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupInventory;
use foreup\rest\models\entities\ForeupTerminals;
use League\Fractal;

class terminals_transformer extends Fractal\TransformerAbstract
{
    public function transform(ForeupTerminals $terminal)
    {
	    return [
		    'id'=>$terminal->getId(),
		    'label'=>$terminal->getLabel(),
		    'autoPrintReceipts'=>$terminal->isAutoPrintReceipts(),
		    'webprnt'=>$terminal->isWebprnt(),
		    'UseRegisterLog'=>$terminal->isUseRegisterLog(),
		    'cashRegister'=>$terminal->isCashRegister(),
		    'printTipLine'=>$terminal->isPrintTipLine(),
		    'signatureSlipCount'=>$terminal->getSignatureSlipCount(),
		    'creditCardReceiptCount'=>$terminal->getCreditCardReceiptCount(),
		    'nonCreditCardReceiptCount'=>$terminal->getNonCreditCardReceiptCount(),
		    'persistentLogs'=>$terminal->isPersistentLogs(),
		    'afterSaleLoad'=>$terminal->isAfterSaleLoad(),
		    'autoEmailReceipt'=>$terminal->isAutoEmailReceipt(),
		    'autoEmailNoPrint'=>$terminal->isAutoEmailNoPrint(),
		    'requireSignatureMemberPayments'=>$terminal->isRequireSignatureMemberPayments(),
		    'multiCashDrawers'=>$terminal->isMultiCashDrawers(),
		    'cashDrawerNumber'=>$terminal->getCashDrawerNumber(),
		    'Https'=>$terminal->isHttps(),
            'quickbuttonTab'=>$terminal->getQuickbuttonTab(),

	    ];
    }

}