<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupAccountRecurringChargeItems;
use foreup\rest\models\entities\ForeupRepeatable;
use League\Fractal;
class account_recurring_charge_items_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'repeated','recurring_charge'
	];

	protected $defaultIncludes = [
		'repeated','item'
	];

	public function transform(ForeupAccountRecurringChargeItems $item)
	{
		return [
			'id' => (int)$item->getId(),
			'item_id' => $item->getItemId(),
			'item_type' => $item->getItemType(),
			'line_number' => $item->getLineNumber(),
			'override_price' => $item->getOverridePrice(),
			'discount_percent' => $item->getDiscountPercent(),
			'quantity' => $item->getQuantity(),
			'recurring_charge_id' => $item->getRecurringCharge()->getId()
		];
	}


	/**
	 * includeRepeated
	 *
	 * @param ForeupAccountRecurringChargeItems|null $item
	 * @return Fractal\Resource\Item
	 */
	public function includeRepeated(ForeupAccountRecurringChargeItems $item = null)
	{
		if(!isset($item))return null;
		$rep = $item->getRepeated();

		return $this->item($rep, new repeatable_transformer(), 'repeated');
	}


	/**
	 * includeRecurringCharge
	 *
	 * @param ForeupAccountRecurringChargeItems|null $item
	 * @return Fractal\Resource\Item
	 */
	public function includeRecurringCharge(ForeupAccountRecurringChargeItems $item = null)
	{
		if(!isset($item))return null;
		$rep = $item->getRecurringCharge();

		return $this->item($rep, new account_recurring_charges_transformer(), 'accountRecurringCharges');
	}


	/**
	 * includeItem
	 *
	 * @param ForeupAccountRecurringChargeItems|null $item
	 * @return Fractal\Resource\Item
	 */
	public function includeItem(ForeupAccountRecurringChargeItems $item = null)
	{
		if(!isset($item))return null;
		$rep = $item->getItem();

		return $this->item($rep, new items_transformer(), 'item');
	}
}
?>