<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupSpecials;
use League\Fractal;

class specials_transformer extends Fractal\TransformerAbstract
{
    public function transform(ForeupSpecials $class)
    {
        $formattedObject = [];
        $formattedObject['id']=$class->getId();
        $formattedObject['name']=$class->getName();
        $formattedObject['date']=$class->getDate();
        $formattedObject['price1']=$class->getPrice1();
        $formattedObject['price2']=$class->getPrice2();
        $formattedObject['price3']=$class->getPrice3();
        $formattedObject['price4']=$class->getPrice4();
        $formattedObject['price5']=$class->getPrice5();
        $formattedObject['price6']=$class->getPrice6();
        $formattedObject['monday']=$class->getMonday();
        $formattedObject['tuesday']=$class->getTuesday();
        $formattedObject['wednesday']=$class->getWednesday();
        $formattedObject['thursday']=$class->getThursday();
        $formattedObject['friday']=$class->getFriday();
        $formattedObject['saturday']=$class->getSaturday();
        $formattedObject['sunday']=$class->getSunday();
        $formattedObject['isRecurring']=$class->getIsRecurring();
        $formattedObject['isAggregate']=$class->getIsAggregate();
        $formattedObject['active']=$class->getActive();
        $formattedObject['bookingClassId']=$class->getBookingClassId();
        return $formattedObject;
    }
}