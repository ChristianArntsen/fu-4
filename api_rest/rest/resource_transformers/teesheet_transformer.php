<?php
namespace foreup\rest\resource_transformers;

use League\Fractal;
use foreup\rest\models\entities\ForeupTeesheet;

class teesheet_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'holes'
	];

	public function transform(ForeupTeesheet $teesheet)
	{
		return [
			'id' => (int)$teesheet->getId(),
			'title' => $teesheet->getTitle(),
			'courseId' => $teesheet->getCourseId(),
            'holes' => $teesheet->getHoles(),
            'onlineOpenTime' => $teesheet->getOnlineOpenTime(),
            'onlineCloseTime' => $teesheet->getOnlineCloseTime(),
            'daysOut' => $teesheet->getDaysOut(),
            'daysInBookingWindow' => $teesheet->getDaysInBookingWindow(),
            'minimumPlayers' => $teesheet->getMinimumPlayers(),
            'limitHoles' => $teesheet->getLimitHoles(),
            'bookingCarts' => $teesheet->getBookingCarts(),
            'increment' => $teesheet->getIncrement(),
            'frontnine' => $teesheet->getFrontnine(),
            'default' => $teesheet->getDefault(),
            'defaultPriceClass' => $teesheet->getDefaultPriceClass(),
            'color' => $teesheet->getColor(),
            'onlineBooking' => $teesheet->getOnlineBooking(),
            'onlineRateSetting' => $teesheet->getOnlineRateSetting(),
            'requireCreditCard' => $teesheet->getRequireCreditCard(),
            'sendThankYou' => $teesheet->getSendThankYou(),
            'thankYouCampaignId' => $teesheet->getThankYouCampaignId(),
            'specialsEnabled' => $teesheet->getSpecialsEnabled(),
            ''
		];
	}

	public function includeHoles(ForeupTeesheet $teesheet)
	{
		$holes = $teesheet->getHoleList();

		return $this->collection($holes, new teesheet_hole_transformer(), 'holes');
	}
}