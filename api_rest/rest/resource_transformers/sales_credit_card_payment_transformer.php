<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupCustomerCreditCards;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\models\entities\ForeupSalesPaymentsCreditCards;
use League\Fractal;
use models\payment_lock;

class sales_credit_card_payment_transformer extends Fractal\TransformerAbstract
{
	public function transform(ForeupSalesPaymentsCreditCards $payment)
	{
		return [
			'id' => $payment->getInvoice(),
			'cid' => $payment->getCid(),
			'courseId' => $payment->getCourseId(),
			'tranType' => $payment->getTranType(),
			'amount' => $payment->getAmount(),
			'authAmount' => $payment->getAuthAmount(),
			'cardType' => $payment->getCardType(),
			'maskedAccount' => $payment->getMaskedAccount(),
			'cardholder_name' => $payment->getCardholderName(),
			'refNo' => $payment->getRefNo(),
			'operatorId' => $payment->getOperatorId(),
			'terminalName' => $payment->getTerminalName(),
			'transPostTime' => $payment->getTransPostTime()->format(DATE_ISO8601),
			// do we need/want to share these?
			//'authCode' => $payment->getAuthCode(),
			//'voiceAuthCode' => $payment->getVoiceAuthCode(),
			//'paymentId' => $payment->getPaymentId(),
			//'acqRefData' => $payment->getAcqRefData(),
			//'processData' => $payment->getProcessData(),
			//'token' => $payment->getToken(),
			'tokenUsed' => $payment->getTokenUsed(),
			'amountRefunded' => $payment->getAmountRefunded(),
			'frequency' => $payment->getFrequency(),
			'responseCode' => $payment->getResponseCode(),
			'status' => $payment->getStatus(),
			'statusMessage' => $payment->getStatusMessage(),
			'displayMessage' => $payment->getDisplayMessage(),
			'avsResult' => $payment->getAvsResult(),
			'cvvResult' => $payment->getCvvResult(),
			'taxAmount' => $payment->getTaxAmount(),
			'avsAddress' => $payment->getAvsAddress(),
			'avsZip' => $payment->getAvsZip(),
			'paymentIdExpired' => $payment->getPaymentIdExpired(),
			'customerCode' => $payment->getCustomerCode(),
			'memo' => $payment->getMemo(),
			'batchNo' => $payment->getBatchNo(),
			'gratuityAmount' => $payment->getGratuityAmount(),
			'voided' => $payment->getVoided(),
			'initiationTime' => $payment->getInitiationTime()->format(DATE_ISO8601),
			'primaryInvoice' => $payment->getPrimaryInvoice(),
			'cartId' => $payment->getCartId(),
			//'mobileGuid' => $payment->getMobileGuid(),
			'successCheckCount' => $payment->getSuccessCheckCount(),
			'cancelTime' => $payment->getCancelTime()->format(DATE_ISO8601),
			//'signatureData' => $payment->getSignatureData(),
			'emvPayment' => $payment->getEmvPayment(),
			'toBeVoided' => $payment->getToBeVoided()
		];
	}
}

?>