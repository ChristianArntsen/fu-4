<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupSeasons;
use League\Fractal;
use foreup\rest\models\entities\ForeupTeesheet;

class teesheet_seasons_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'classes'
	];

	public function transform(ForeupSeasons $season)
	{
		return [
			'id' => (int)$season->getSeasonId(),
			'name' => $season->getSeasonName(),
			'startDate' => $season->getStartDate()->toDateString(),
			'endDate' => $season->getEndDate()->toDateString(),
			'isDefault'=>$season->getDefault()
		];
	}

	public function includeClasses(ForeupSeasons $seasons)
	{
		$classes = $seasons->getClass();

		return $this->collection($classes, new teesheet_seasons_classes_transformer(), 'classes');
	}
}