<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupAccountLedger;
use foreup\rest\models\entities\ForeupAccounts;
use foreup\rest\models\entities\ForeupAccountTypes;
use League\Fractal;
class account_types_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		"organization"
	];

	public function transform(ForeupAccountTypes $account)
	{
		return [
			'id' => $account->getId(),
			'limit' => $account->getLimit(),
			'allow_negative' => (float)$account->getAllowNegative(),
			'name' => $account->getName(),
			'description' => $account->getDescription(),
		];
	}

}