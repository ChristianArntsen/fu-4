<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupOnlineHours;
use League\Fractal;

class online_hours_transformer extends Fractal\TransformerAbstract
{
    public function transform(ForeupOnlineHours $class)
    {
        $formattedObject = [];
        $formattedObject['id']=$class->getId();
        $formattedObject['sunOpen']=$class->getSunOpen();
        $formattedObject['sunClose']=$class->getSunClose();
        $formattedObject['monOpen']=$class->getMonOpen();
        $formattedObject['monClose']=$class->getMonClose();
        $formattedObject['tueOpen']=$class->getTueOpen();
        $formattedObject['tueClose']=$class->getTueClose();
        $formattedObject['wedOpen']=$class->getWedOpen();
        $formattedObject['wedClose']=$class->getWedClose();
        $formattedObject['thuOpen']=$class->getThuOpen();
        $formattedObject['thuClose']=$class->getThuClose();
        $formattedObject['friOpen']=$class->getFriOpen();
        $formattedObject['friClose']=$class->getFriClose();
        $formattedObject['satOpen']=$class->getSatOpen();
        $formattedObject['satClose']=$class->getSatClose();

        return $formattedObject;
    }
}