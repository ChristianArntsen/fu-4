<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\Partial\BookingPlayer;
use League\Fractal;

class booked_player_transformer extends Fractal\TransformerAbstract {

	protected $availableIncludes = [
		'people',"priceClasses"
	];

    public function transform(BookingPlayer $entry)
    {
	    $priceClassName = "";
	    if( $entry->getPriceClass() !=null){
		    $priceClassName = $entry->getPriceClass()->getName();
	    }
        return [
            'id' => $entry->getId(),
            'name' => $entry->getName(),
            'position' => $entry->getPosition(),
            'noShow' => $entry->getNoShow(),
            'paid' => $entry->getPaid(),
	        'priceClassId'=>(string)$entry->getPriceClassId(),
	        'priceClass'=>$priceClassName,
	        'personId'=>(string)$entry->getPersonId()
        ];
    }
	public function includePriceClasses(BookingPlayer $player)
	{
		$class = $player->getPriceClass();
		if(empty($class))
			return null;
		return $this->item($class, new price_class_transformer(), 'priceClass');
	}
	public function includePeople(BookingPlayer $player)
	{
		$person = $player->getPerson();
		if(empty($person))
			return null;
		return $this->item($person, new person_transformer(), 'people');
	}

}