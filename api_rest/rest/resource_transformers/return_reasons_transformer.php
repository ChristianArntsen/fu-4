<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupPrinterGroups;
use foreup\rest\models\entities\ForeupPrinters;
use foreup\rest\models\entities\ForeupRefundReasons;
use League\Fractal;

class return_reasons_transformer extends Fractal\TransformerAbstract
{

	public function transform(ForeupRefundReasons $reason)
    {
	    return [
		    'id'=>$reason->getId(),
		    'label'=>$reason->getLabel(),
	    ];
    }

}