<?php
namespace foreup\rest\resource_transformers;

use League\Fractal;
use foreup\rest\models\entities\ForeupCourses;

class course_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'teesheets',
		'modules',
		'mobile_images'
	];

    public function transform($course)
    {
        if(is_array($course)){
            return $this->transform_array($course);
        }elseif($course instanceof ForeupCourses){
            return $this->transform_ForeupCourses($course);
        }elseif(is_object($course)){
            return $this->transform_object($course);
        }
    }

    private function transform_object($course){
        return [
            'id' => $course->courseId,
            'title' => $course->name,
            'street' => $course->address,
            'city' => $course->city,
            'state' => $course->state,
            'zip' => $course->zip,
            'phone' => $course->phone/*,
            'app_store_description' => $course->mobile_app_summary,
            'app_short_title' => $course->mobile_app_short_title,
            'test_flight_email' => $course->mobile_test_flight_email,
            'course_summary'=>$course->course_summary,
            'base_color' => $course->base_color,
            'app_icon_url' => $course->mobile_app_icon_url,
            'last_updated' => $course->mobile_app_refresh_timestamp,
            'registration_hidden' => $course->hide_registration,
            'published_ios_version' => $course->mobile_app_ios_published_version
*/
        ];
    }

    private function transform_array(array $course){
        return [
            'id' => isset($course['courseId'])?$course['courseId']:null,
            'title' => isset($course['name'])?$course['name']:null,
            'street' => isset($course['address'])?$course['address']:null,
            'city' => isset($course['city'])?$course['city']:null,
            'state' => isset($course['state'])?$course['state']:null,
            'zip' => isset($course['zip'])?$course['zip']:null,
            'phone' => isset($course['phone'])?$course['phone']:null/*,
            'app_store_description' => $course['mobile_app_summary'],
            'app_short_title' => $course['mobile_app_short_title'],
            'test_flight_email' => $course['mobile_test_flight_email'],
            'course_summary'=>$course['course_summary'],
            'base_color' => $course['base_color'],
            'app_icon_url' => $course['mobile_app_icon_url'],
            'last_updated' => $course['mobile_app_refresh_timestamp'],
            'registration_hidden' => $course['hide_registration'],
            'published_ios_version' => $course['mobile_app_ios_published_version']
*/
        ];
    }

    private function transform_ForeupCourses(ForeupCourses $course)
	{
		$app_desc= iconv('UTF-8', 'UTF-8//IGNORE', $course->getMobileAppSummary());
		$summary = iconv('UTF-8', 'UTF-8//IGNORE', $course->getCourseSummary());

		//\Doctrine\Common\Util\Debug::dump($course->getMyTeeSheets());
		return [
			'id' => (int)$course->getCourseId(),
			'title' => $course->getName(),
			'street' => $course->getAddress(),
			'city' => $course->getCity(),
			'state' => $course->getState(),
			'zip' => $course->getZip(),
			'phone' => $course->getPhone(),
			'app_store_description' => $app_desc,
			'app_short_title' => $course->getMobileAppShortTitle(),
			'test_flight_email' => $course->getMobileTestFlightEmail(),
			'course_summary'=>$summary,
			'base_color' => $course->getBaseColor(),
			'app_icon_url' => $course->getMobileAppIconUrl(),
			'last_updated' => $course->getMobileAppRefreshTimestamp(),
			'registration_hidden' => $course->isHideRegistration(),
			'published_ios_version' => $course->getMobileAppIosPublishedVersion(),
			'published_android_version' => $course->getMobileAppAndroidPublishedVersion()
		];
	}

	public function transform_sensitive(ForeupCourses $course)
	{
		$ret = $this->transform_ForeupCourses($course);

		$ret['mercury_id'] = $course->getMercuryId();
		$ret['mercury_password'] = $course->getMercuryPassword();
		$ret['ets_key'] = $course->getEtsKey();
		$ret['mercury_e2e_id'] = $course->getMercuryE2eId();
		$ret['mercury_e2e_password'] = $course->getMercuryE2ePassword();

		return $ret;

	}

	public function includeTeesheets(ForeupCourses $course)
	{
		$teesheets = $course->getTeesheets();

		return $this->collection($teesheets, new teesheet_transformer, 'teesheet');
	}

	public function includeModules(ForeupCourses $course)
	{
		$modules = $course->getMyModules();

		return $this->collection($modules, new module_transformer, 'module');
	}

	public function includeMobileImages(ForeupCourses $course)
	{
		$images = $course->getMobileCourseImages();

		return $this->collection($images, new mobile_image_transformer, 'mobile_images');
	}

}