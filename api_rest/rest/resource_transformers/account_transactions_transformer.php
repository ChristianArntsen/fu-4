<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupAccountTransactions;
use League\Fractal;

class account_transactions_transformer extends Fractal\TransformerAbstract
{
	/**
	 * @var array
	 */
	protected $defaultIncludes = [
		'sale'
	];

	/**
	 * @var array
	 */
	protected $availableIncludes = [
		'customer'
	];

	public function transform(ForeupAccountTransactions $entity)
	{
		return [
			'id' => $entity->getTransId(),
			'courseId' => $entity->getCourseId(),
			'accountType' => $entity->getAccountType(),
			'transDate' => !empty($entity->getTransDate())?$entity->getTransDate()->format(\DateTime::ISO8601):null,
			'transComment' => $entity->getTransComment(),
			'transDescription' => $entity->getTransDescription(),
			'transAmount' =>$entity->getTransAmount(),
			'runningBalance' => $entity->getRunningBalance(),
			'hideOnInvoices' => $entity->getHideOnInvoices()
		];
	}


	/**
	 * includeCustomer
	 *
	 * @param ForeupAccountStatements $statement
	 * @return Fractal\Resource\Item
	 */
	public function includeCustomer(ForeupAccountTransactions $entity)
	{
		$customer = $entity->getCustomer();
		return $this->item($customer, new customer_transformer(), 'customers');
	}

	/**
	 * @param ForeupAccountTransactions|null $entity
	 * @return Fractal\Resource\Item
	 */
	public function includeSale(ForeupAccountTransactions $entity = null)
	{
		$referred = $entity->getSale();
		if(!isset($referred)) return null;

        $salesTransformer = new sales_transformer();
        $salesTransformer->setDefaultIncludes(['items']);

		return $this->item($referred, $salesTransformer, 'sale');
	}
}