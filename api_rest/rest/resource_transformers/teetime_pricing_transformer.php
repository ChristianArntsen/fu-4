<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupTeetime;
use foreup\rest\models\entities\LegacyObjects\PriceClass;
use League\Fractal;
use foreup\rest\models\entities\ForeupTeesheet;

class teetime_pricing_transformer extends Fractal\TransformerAbstract
{
	public function transform(PriceClass $priceClass)
	{
		return [
			'priceClassId' => $priceClass->getClassId(),
			'id' => $priceClass->getClassId(),
			'name' => $priceClass->getName(),
			'isDefault' => $priceClass->getDefault(),
			'9holePrice' => $priceClass->getPricing()->getHole9Price(),
			'9cartPrice' => $priceClass->getPricing()->getHole9Cart(),
			'18holePrice' => $priceClass->getPricing()->getHole18Price(),
			'18cartPrice' => $priceClass->getPricing()->getHole18Cart(),

			'18holeTax' => $priceClass->getPricing()->getHole18Tax(),
			'18cartTax' => $priceClass->getPricing()->getCart18Tax(),
			'9holeTax' => $priceClass->getPricing()->getHole9Tax(),
			'9cartTax' => $priceClass->getPricing()->getCart9Tax(),
			'greenFeeTaxIncluded' => $priceClass->getPricing()->getGreenFeeTaxIncluded(),
			'cartTaxIncluded' => $priceClass->getPricing()->getCartFeeTaxIncluded(),
		];
	}
}