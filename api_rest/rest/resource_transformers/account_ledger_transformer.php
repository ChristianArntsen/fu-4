<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupAccountLedger;
use League\Fractal;
class account_ledger_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
	];

	public function transform(ForeupAccountLedger $ledger)
	{
		//\Doctrine\Common\Util\Debug::dump($course->getMyTeeSheets());
		return [
			'id' => (int)$ledger->getId(),
			'type' => $ledger->getType(),
			'amount' => $ledger->getAmount(),
			'amount_open' => $ledger->getAmountOpen(),
			'amount_paid' => $ledger->getAmountPaid(),
			'date_created' => $ledger->getDateCreated(),
			'start_date' => $ledger->getDateCreated(),
			'due_date' => $ledger->getDateCreated(),
		];
	}

}