<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupItems;
use foreup\rest\models\entities\ForeupItemsTaxes;
use foreup\rest\models\entities\ForeupSalesItems;
use League\Fractal;
use foreup\rest\models\entities\ForeupCourses;

class items_transformer extends Fractal\TransformerAbstract
{
	/**
	 * @var array
	 */
	protected $availableIncludes = [

	];

	/**
	 * @var array
	 */
	protected $defaultIncludes = [
		'taxes'
	];

    public function transform(ForeupItems $item)
    {
	    return [
		    'id'=>(float)$item->getItemId(),
		    'maxDiscount'=>(float)$item->getMaxDiscount(),
		    'addOnPrice'=>(float)$item->getAddOnPrice(),
		    'costPrice'=>(float)$item->getCostPrice(),
		    'courseId'=>(float)$item->getCourseId(),
		    'quantity'=>(float)$item->getQuantity(),
		    'numberOfSides'=>(integer)$item->getNumberOfSides(),
		    'unitPrice'=>(float)$item->getUnitPrice(),
		    'category'=>$item->getCategory(),
		    'itemNumber'=>$item->getItemNumber(),
		    'location'=>$item->getLocation(),
		    'name'=>$item->getName(),
		    'subcategory'=>$item->getSubcategory(),
		    'department'=>$item->getDepartment(),
		    'description'=>$item->getDescription(),
		    'glCode'=>$item->getGlCode(),
		    'isInactive'=>(bool)$item->getInactive(),
		    'doNotPrint'=>(bool)$item->getDoNotPrint(),
		    'deleted'=>(bool)$item->getDeleted(),
		    'isfoodAndBeverage'=>(bool)$item->getFoodAndBeverage(),
		    'isInvisible'=>(bool)$item->getInvisible(),
		    'isFee'=>(bool)$item->getIsFee(),
		    'isGiftcard'=>(bool)$item->getIsGiftcard(),
		    'isPass'=>(bool)$item->getIsPass(),
		    'isSide'=>(bool)$item->getIsSide(),
		    'isUnlimited'=>(bool)$item->getIsUnlimited(),
		    'isSoupOrSalad'=>(bool)$item->getSoupOrSalad(),
		    'unitPriceIncludesTax'=>(bool)$item->getUnitPriceIncludesTax(),
	    ];
    }

    public function snake_case(ForeupItems $item)
    {
    	return [
		'item_id'=>(float)$item->getItemId(),
	    'max_discount'=>(float)$item->getMaxDiscount(),
	    'add_on_price'=>(float)$item->getAddOnPrice(),
	    'cost_price'=>(float)$item->getCostPrice(),
	    'course_id'=>(float)$item->getCourseId(),
	    'quantity'=>(float)$item->getQuantity(),
	    'number_of_sides'=>(integer)$item->getNumberOfSides(),
	    'unit_price'=>(float)$item->getUnitPrice(),
	    'category'=>$item->getCategory(),
	    'item_number'=>$item->getItemNumber(),
	    'location'=>$item->getLocation(),
	    'name'=>$item->getName(),
	    'subcategory'=>$item->getSubcategory(),
	    'department'=>$item->getDepartment(),
	    'description'=>$item->getDescription(),
	    'gl_code'=>$item->getGlCode(),
	    'inactive'=>(bool)$item->getInactive(),
	    'do_not_print'=>(bool)$item->getDoNotPrint(),
	    'deleted'=>(bool)$item->getDeleted(),
	    'food_and_beverage'=>(bool)$item->getFoodAndBeverage(),
	    'invisible'=>(bool)$item->getInvisible(),
	    'is_fee'=>(bool)$item->getIsFee(),
	    'is_giftcard'=>(bool)$item->getIsGiftcard(),
	    'is_pass'=>(bool)$item->getIsPass(),
	    'is_side'=>(bool)$item->getIsSide(),
	    'is_unlimited'=>(bool)$item->getIsUnlimited(),
	    'is_soup_or_salad'=>(bool)$item->getSoupOrSalad(),
	    'unit_price_includes_tax'=>(bool)$item->getUnitPriceIncludesTax(),
    ];
    }

	/**
	 * @param ForeupItems|null $item
	 * @return Fractal\Resource\Collection
	 */
	public function includeTaxes(ForeupItems $item = null)
	{
		$taxes = $item->getTaxes();
		if(!isset($item)) return null;

		return $this->collection($taxes, new items_taxes_transformer(), 'itemTax');
	}

}