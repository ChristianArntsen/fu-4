<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupAccountRecurringChargeGroups;
use League\Fractal;
class account_recurring_charge_groups_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'recurring_charge'
	];

	public function transform(ForeupAccountRecurringChargeGroups $group)
	{
		return [
			'id' => (int)$group->getId(),
			'include' => $group->getInclude(),
			'group_id' => $group->getGroup()->getId(),
			'recurring_charge_id' => $group->getRecurringCharge()->getId()
		];
	}

	/**
	 * includeRecurringCharge
	 *
	 * @param ForeupAccountRecurringChargeGroups|null $group
	 * @return Fractal\Resource\Item|null
	 */
	public function includeRecurringCharge(ForeupAccountRecurringChargeGroups $group = null)
	{
		if(!isset($group))return null;
		$rep = $group->getRecurringCharge();

		return $this->item($rep, new account_recurring_charges_transformer(), 'accountRecurringCharges');
	}
}

?>