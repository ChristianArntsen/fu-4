<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupPriceClass;
use foreup\rest\models\entities\ForeupSeasons;
use League\Fractal;
use foreup\rest\models\entities\ForeupTeesheet;

class teesheet_seasons_classes_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'priceClasses','teesheet','course'
	];

	public function transform(ForeupPriceClass $priceClass)
	{
		return [
			'id' => (int)$priceClass->getId(),
			'color' => $priceClass->getColor(),
			'name' => $priceClass->getName(),
			'dateCreated' => $priceClass->getDateCreated(),
		];
	}

	public function includePriceClasses(ForeupSeasons $seasons)
	{
		return $this->collection($seasons->getClass(), new teesheet_hole_transformer(), 'priceClasses');
	}
	public function includeTeesheet(ForeupSeasons $seasons)
	{
		return $this->collection($seasons->getTeesheet(), new teesheet_hole_transformer(), 'teesheets');
	}
	public function includeCourse(ForeupSeasons $seasons)
	{
		return $this->collection($seasons->getCourse(), new teesheet_hole_transformer(), 'courses');
	}
}