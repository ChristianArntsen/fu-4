<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupBulkEditJobs;
use foreup\rest\models\entities\ForeupCustomerCreditCards;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\models\entities\ForeupSalesPaymentsCreditCards;
use League\Fractal;
class bulk_edit_jobs_transformer extends Fractal\TransformerAbstract
{
	/**
	 * @var array
	 */
	protected $availableIncludes = [
		'employee'
	];

	public $fields = null;

	public function transform(ForeupBulkEditJobs $job)
	{
		$data = [
			'id' => $job->getId(),
			'type' => $job->getType(),
			'courseId' => $job->getCourseId(),
			'employeeId' => $job->getEmployeeId(),
			'status' => $job->getStatus(),
			'totalRecords' => $job->getTotalRecords(),
			'recordsCompleted' => $job->getRecordsCompleted(),
			'recordsFailed' => $job->getRecordsFailed(),
			'percentComplete' => $job->getPercentComplete(),
			'createdAt' => $job->getCreatedAt()->format(DATE_ISO8601),
			'startedAt' => $job->getStartedAt()?$job->getStartedAt()->format(DATE_ISO8601):null,
			'completedAt' => $job->getCompletedAt()?$job->getCompletedAt()->format(DATE_ISO8601):null,
			'totalDuration' => $job->getTotalDuration(),
			'response' => $job->getResponse(),
			'deletedAt' => $job->getDeletedAt()?$job->getDeletedAt()->format(DATE_ISO8601):null,
			'settings' => $job->getSettings(),
			'recordIds' => $job->getRecordIds(),
			'lastProgressUpdate' => $job->getLastProgressUpdate()?$job->getLastProgressUpdate()->format(DATE_ISO8601):null,
		];

		if($this->fields){

			$this->fields[] = 'id';
			$this->fields[] = 'type';

			foreach($this->fields as $field){
				if(isset($data[$field])){
					$response[$field] = $data[$field];
				}
			}
		}else{
			$response = $data;
		}

		return $response;
	}

	/**
	 * @param ForeupBulkEditJobs|null $job
	 * @return Fractal\Resource\Item
	 */
	public function includeEmployee(ForeupBulkEditJobs $job = null)
	{
		$employee = $job->getEmployee();
		if(!isset($employee)) return null;

		return $this->item($employee, new employees_transformer(), 'employee');
	}

	public function setFields($fields){
		$this->fields = $fields;
	}
}
?>