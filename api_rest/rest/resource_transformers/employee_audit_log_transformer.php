<?php
namespace foreup\rest\resource_transformers;

use League\Fractal;
use foreup\rest\models\entities\ForeupEmployeeAuditLog;

class employee_audit_log_transformer extends Fractal\TransformerAbstract {
    public function transform(ForeupEmployeeAuditLog $entry)
    {
        return [
            'id' => (int)$entry->getId(),
            'editor_id'=>(int)$entry->getEditor()->getId(),
            'gmt_logged'=>$entry->getGmtLogged(),
            'person_id'=>$entry->getEditor()->getPerson()->getPersonId(),
            'course_id'=>$entry->getEditor()->getCourseId(),
            'username'=>$entry->getEditor()->getUsername(),
            'action_type_id'=>$entry->getActionType()->getId(),
            'action_type_name'=>$entry->getActionType()->getName(),
            'action_type_desc'=>$entry->getActionType()->getLongDescription()
        ];
    }
}