<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupSalesPayments;
use League\Fractal;

class sales_payments_transformer extends Fractal\TransformerAbstract
{
	/**
	 * @var array
	 */
	protected $availableIncludes = [
		'sale'
	];

	public function transform(ForeupSalesPayments $statement)
	{
		return [
			'paymentType' => $statement->getPaymentType(),
			'type' => $statement->getType(),
			'paymentAmount' => $statement->getPaymentAmount(),
			'tipRecipient' => $statement->getTipRecipient(),
			'invoiceId' => $statement->getInvoiceId(),
			'isTip' => $statement->getIsTip()
		];
	}

}
?>

