<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupSales;
use foreup\rest\models\entities\ForeupTerminals;
use League\Fractal;
use foreup\rest\models\entities\ForeupCourses;

class sales_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'items','terminals','bookings'
	];

    public function transform($course)
    {
        if(is_array($course)){
            return $this->transform_array($course);
        }elseif(is_object($course)){
            return $this->transform_object($course);
        }
    }


    /**
     * @param ForeupSales $sale
     * @return array
     */
    private function transform_object($sale)
    {
        return [
            'id' => $sale->getSaleId(),
            'saleTime'=> $sale->getSaleTime()->setTimezone(new \DateTimeZone("UTC")),
            'comment' => $sale->getComment(),
            'employeeId' => $sale->getEmployeeId(),
            'guestCount' => (int)$sale->getGuestCount(),
            'number' => $sale->getNumber(),
            'paymentType' => $sale->getPaymentType(),
            'refundComment' => $sale->getRefundComment(),
            'refundReason' => $sale->getRefundReason(),
            'total'=>(float)$sale->getTotal(),
            'subtotal'=>(float)$sale->getSubTotal(),
            'tax'=>(float)$sale->getTax(),
            'deleted' => boolval($sale->getDeleted()),
            'deletedAt' => $sale->getDeleted() ? $sale->getDeletedAt() : null,
        ];
    }

    private function transform_array(array $course)
    {
        return [
            'id' => isset($course['courseId'])?$course['courseId']:null
        ];
    }

	public function includeItems(ForeupSales $sale)
	{
		$items = $sale->getItems();

		return $this->collection($items, new sales_items_transformer(), 'sales_items');
	}

	public function includeTerminals(ForeupSales $sale)
	{
		$terminal = $sale->getTerminal();

		return $this->item($terminal, new terminals_transformer(), 'terminals');
	}

	public function includeBookings(ForeupSales $sale)
	{
		$booking = $sale->getTeetime();
		if(empty($booking))
			return null;

		return $this->item($booking, new teetime_transformer(),'bookings');
	}
}