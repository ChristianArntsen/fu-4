<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupCustomerCreditCards;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\models\entities\ForeupSalesPaymentsCreditCards;
use League\Fractal;
class invoices_transformer extends Fractal\TransformerAbstract
{
	/**
	 * @var array
	 */
	protected $availableIncludes = [
		'credit_card', 'customer', 'employee', 'credit_card_payment','sale'
	];

	public function transform(ForeupInvoices $invoice)
	{
		return [
			'id' => (int)$invoice->getId(),
			'statementNumber' => (int) $invoice->getInvoiceNumber(),
			'name' => $invoice->getName(),
			'department' => $invoice->getDepartment(),
			'category' => $invoice->getCategory(),
			'subcategory' => $invoice->getSubcategory(),
			'day' => $invoice->getDay(),
			'courseId' => (int) $invoice->getCourseId(),
			'billingId' => (int) $invoice->getBillingId(),
			'date' => $invoice->getDate()->format(DATE_ISO8601),
			'billStart' => $invoice->getBillStart()->format(DATE_ISO8601),
			'billEnd' => $invoice->getBillEnd()->format(DATE_ISO8601),
			'total' => $invoice->getTotal(),
			'overdueTotal' => $invoice->getOverdueTotal(),
			'previousPayments' => $invoice->getPreviousPayments(),
			'paid' => $invoice->getPaid(),
			'overdue' => $invoice->getOverdue(),
			'deleted' => $invoice->getDeleted(),
			'emailStatement' => $invoice->getEmailInvoice(),
			'lastBillingAttempt' => $invoice->getLastBillingAttempt()->format(DATE_ISO8601),
			'started' => $invoice->getStarted(),
			'chargedNoSale' => $invoice->getChargedNoSale(),
			'charged' => $invoice->getCharged(),
			'emailed' => $invoice->getEmailed(),
			'sendDate' => $invoice->getSendDate()->format(DATE_ISO8601),
			'dueDate' => $invoice->getDueDate()->format(DATE_ISO8601),
			'autoBillDate' => $invoice->getAutoBillDate()->format(DATE_ISO8601),
			'payCustomerAccount' => $invoice->getPayCustomerAccount(),
			'payMemberAccount' => $invoice->getPayMemberAccount(),
			'showAccountTransactions' => $invoice->getShowAccountTransactions(),
			'notes' => $invoice->getNotes(),
			'attemptLimit' => $invoice->getAttemptLimit()
		];
	}

	/**
	 * @param ForeupInvoices|null $invoice
	 * @return Fractal\Resource\Item
	 */
	public function includeCreditCard(ForeupInvoices $invoice = null)
	{
		$card = $invoice->getCreditCard();
		if(!isset($card)) return null;

		return $this->item($card, new customer_credit_card_transformer(), 'credit_card');
	}

	/**
	 * @param ForeupInvoices|null $invoice
	 * @return Fractal\Resource\Item
	 */
	public function includeCustomer(ForeupInvoices $invoice = null)
	{
		$customer = $invoice->getPerson();
		if(!isset($customer)) return null;

		return $this->item($customer, new customer_transformer(), 'customer');
	}

	/**
	 * @param ForeupInvoices|null $invoice
	 * @return Fractal\Resource\Item
	 */
	public function includeEmployee(ForeupInvoices $invoice = null)
	{
		$employee = $invoice->getEmployee();
		if(!isset($employee)) return null;

		return $this->item($employee, new employees_transformer(), 'employee');
	}

	/**
	 * @param ForeupInvoices|null $invoice
	 * @return Fractal\Resource\Item
	 */
	public function includeCreditCardPayment(ForeupInvoices $invoice = null)
	{
		$payment = $invoice->getCreditCardPayment();
		if(!isset($payment)) return null;

		return $this->item($payment, new sales_credit_card_payment_transformer(), 'credit_card_payment');
	}

	/**
	 * @param ForeupInvoices|null $invoice
	 * @return Fractal\Resource\Item
	 */
	public function includeSale(ForeupInvoices $invoice = null)
	{
		$sale = $invoice->getSale();
		if(!isset($sale)) return null;

		return $this->item($sale, new sales_transformer(), 'sale');
	}

}
?>

