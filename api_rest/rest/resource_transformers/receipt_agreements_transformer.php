<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupItemReceiptContent;
use League\Fractal;

class receipt_agreements_transformer extends Fractal\TransformerAbstract
{

	public function transform(ForeupItemReceiptContent $reason)
    {
	    return [
		    'id'=>$reason->getId(),
		    'name'=>$reason->getName(),
		    'content'=>$reason->getContent(),
		    'SeparateReceipt'=>$reason->getSeparateReceipt(),
		    'SignatureLine'=>$reason->getSignatureLine()
	    ];
    }

}