<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupEmployees;
use League\Fractal;

class employees_transformer extends Fractal\TransformerAbstract {

    public function transform(ForeupEmployees $entry)
    {
	    $personTransformer = new person_transformer();
        return [
            'id' => (integer)$entry->getPerson()->getPersonId(),
            'user_level'=>$entry->getUserLevel(),
            'position'=>$entry->getPosition(),
            'username'=>$entry->getUsername(),
            'activated'=>$entry->getActivated(),
	        'contact_info'=>$personTransformer->transform($entry->getPerson())
        ];
    }
}