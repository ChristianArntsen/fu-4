<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\Token;
use League\Fractal;

class token_transformer extends Fractal\TransformerAbstract
{

	public function transform(Token $token)
	{
		return [
			'id' => $token->getId(),
		];
	}

}