<?php
namespace foreup\rest\resource_transformers;

use Doctrine\ORM\EntityNotFoundException;
use foreup\rest\models\entities\ForeupSalesItems;
use League\Fractal;
use foreup\rest\models\entities\ForeupCourses;

class sales_items_transformer extends Fractal\TransformerAbstract
{
    public function transform($saleItems)
    {
        if(is_array($saleItems)){
            return $this->transform_array($saleItems);
        }elseif(is_object($saleItems)){
            return $this->transform_object($saleItems);
        }
    }


    /**
     * @param ForeupSalesItems $saleItem
     * @return array
     */
    private function transform_object($saleItem){
        $return_obj = [
            'id' => $saleItem->getSale()->getSaleId().$saleItem->getLine() ,
            'total'=> (float)$saleItem->getTotal(),
            'subTotal'=> (float)$saleItem->getSubtotal(),
            'tax'=> (float)$saleItem->getTax(),
            'quantity'=> (float)$saleItem->getQuantityPurchased(),
            'unitPrice'=> (float)$saleItem->getItemUnitPrice(),
            'discountPercent'=> (float)$saleItem->getDiscountPercent(),
            'profit'=> (float)$saleItem->getProfit()
        ];

        if($saleItem->getItem() && $saleItem->getItem()->getItemId()){
            $return_obj = array_merge($return_obj,[
                'department'=> $saleItem->getItem()->getDepartment(),
                'category'=> $saleItem->getItem()->getCategory(),
                'subcategory'=> $saleItem->getItem()->getSubcategory(),
                'glCode'=> $saleItem->getItem()->getGlCode(),
                'itemNumber'=> $saleItem->getItem()->getItemNumber(),
                'itemId'=> $saleItem->getItem()->getItemId(),
                'name'=> $saleItem->getItem()->getName(),
                'description'=> $saleItem->getItem()->getDescription()
            ]);
        }

        try {
	        if($saleItem->getPriceClass() && $saleItem->getPriceClass()->getId()){
		        $return_obj = array_merge($return_obj,[
			        'priceClass'=> $saleItem->getPriceClass()->getName(),
			        'priceClassId'=> $saleItem->getPriceClass()->getId()
		        ]);
	        }
        } catch (EntityNotFoundException $e){
	        $return_obj = array_merge($return_obj,[
		        'priceClass'=> "Deleted Price Class",
		        'priceClassId'=> null
	        ]);
        }


        return $return_obj;
    }

    private function transform_array(array $course){
        return [
            'id' => isset($course['courseId'])?$course['courseId']:null
        ];
    }

	public function includeItems(ForeupCourses $sale)
	{
		$images = $sale->getMobileCourseImages();

		return $this->collection($images, new mobile_image_transformer, 'mobile_images');
	}

}