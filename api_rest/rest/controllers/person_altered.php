<?php
namespace foreup\rest\controllers;

use Aws\CloudFront\Exception\Exception;
use Carbon\Carbon;
use foreup\rest\models\entities\ForeupPersonAltered;
use foreup\rest\resource_transformers\person_altered_transformer;
use foreup\rest\controllers\employee_audit_log;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class person_altered extends employee_audit_log
{
    /** @var \foreup\rest\models\repositories\EmployeeAuditLogRepository $repository */
    private $repository;


    public function __construct($db, $current_user)
    {
        parent::__construct($db,$current_user);
        $this->employee_audit_log = new parent($db, $current_user);

        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupPersonAltered');
        $this->transformer = new person_altered_transformer();
        $this->resource_type = "person_altered";
    }

    public function get_course_log_entries(Request $request)
    {
        //* Returns log entries by foreup_employee course_id field
        $uzr = $this->current_user;
        if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin",400);

        $entries = $this->repository->get_course_log_entries($request->attributes->get('id'));
        $content = $this->serializeResource($entries);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
        //*/
    }

    public function get_employee_log_entries(Request $request)
    {
        //* Returns log entries by foreup_employee id field
        $uzr = $this->current_user;
        if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin",400);
        $id = $request->attributes->get('id');
        $entries = $this->repository->get_employee_log_entries($id);
        $content = $this->serializeResource($entries);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
        //*/
    }

    public function save(Request $request)
    {
        $entry = new ForeupPersonAltered();

        $eal = json_decode($this->employee_audit_log->save($request)->getContent());

        $data = $request->request->get('data');
        if (isset($data['attributes'])) {
            $data = $data['attributes'];
        } else {
            return $this->respondWithError("Missing attribute field", 400);
        }

        // person_edited and tables_updated
        if(isset($data['person_edited'])){
            // we are given the unique identifier for a person
            $person = $this->db->find('e:ForeupPeople',(int) $data['person_edited']);
            $entry->setPersonEdited($person);
        }
        else {
            return $this->respondWithError('The person_id of the person acted upon was not included as "person_edited"!',400);
        }

        if(isset($data['tables_updated'])) {
            $entry->setTablesUpdated($data['tables_updated']);
        }
        else {
            return $this->respondWithError('List of subclass tables that were updated in the action must be included as a comma separated string (e.g. "foreup_people,foreup_employees,foreup_users").',400);
        }

        if(isset($eal->data) && is_numeric($eal->data->id)) {
            $entry->setEmployeeAuditLog($this->db->find('e:ForeupEmployeeAuditLog',(int) $eal->data->id));
        }

        $this->db->persist($entry);
        $this->db->flush();

        $content = $this->serializeResource($entry);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }
}

?>