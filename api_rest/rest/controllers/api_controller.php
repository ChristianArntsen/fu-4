<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 12/27/2015
 * Time: 2:45 AM
 */

namespace foreup\rest\controllers;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use foreup\rest\models\entities\ForeupApiUserCourses;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupUsers;
use foreup\rest\models\services\AuthenticatedUser;
use League\Fractal;
use foreup\rest\resource_transformers\course_transformer;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\JsonApiSerializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class api_controller
{
	/**
	 * @var Request
	 */
	protected $request, $transformer, $resource_type,$sortBy,$order,$format; 

    protected $blacklistAttributes = ['id'];

	protected $fieldsToSkip = [];
	protected $fieldsOnlyUpdate = [];
	/**
	 * @var ForeupCourses
	 */
	protected $course;

	protected $byPassAccessCheck  = false;

	/**
	 * @var AuthenticatedUser
	 */
	protected $auth_user;

	/**
	 * @var ForeupUsers
	 */
	protected $current_user;

	public function __construct()
	{
	}

	protected function serializeResource($resource)
	{
		if (!is_subclass_of($this->transformer, "League\\Fractal\\TransformerAbstract")) {
			throw new \ErrorException("Only able to serialize TransformerAbstract classes");
		}

		if (is_array($resource)) {
			$content = $this->serializeCollection($resource);
		} else {
			$content = $this->serializeItem($resource);
		}

		return $content;
	}

	private function serializeItem($item)
	{
		$resource = new Item($item, $this->transformer, $this->resource_type);
		return $this->serializeResponse($resource);
	}

	private function serializeCollection($courses)
	{
		$resource = new Collection($courses, $this->transformer, $this->resource_type);
		return $this->serializeResponse($resource);
	}

	protected function serializeResponse($resource)
	{
		$manager = new Manager();
		if(isset($this->request) && $this->request->query->has("include")){
			$manager->parseIncludes($this->request->query->get('include'));
		}
		if(isset($this->request) && $this->request->query->has("fields")){
			$fields = $this->request->query->get('fields');
			$this->transformer->setFields(preg_split("/,/",$fields));
		}
		$manager->setSerializer(new JsonApiSerializer());
		$data = $manager->createData($resource);
		$content = $data->toJson();
		if(!$content){
			switch (json_last_error()) {
				case JSON_ERROR_NONE:
					echo ' - No errors';
					break;
				case JSON_ERROR_DEPTH:
					echo ' - Maximum stack depth exceeded';
					break;
				case JSON_ERROR_STATE_MISMATCH:
					echo ' - Underflow or the modes mismatch';
					break;
				case JSON_ERROR_CTRL_CHAR:
					echo ' - Unexpected control character found';
					break;
				case JSON_ERROR_SYNTAX:
					echo ' - Syntax error, malformed JSON';
					break;
				case JSON_ERROR_UTF8:
					echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
					break;
				default:
					echo ' - Unknown error';
					break;
			}die;
		}
		return $content;
	}

	protected function respondWithError($message,$code = 403,$detail="")
	{
		$json_response = new JsonResponse();
		$json_response->setData(array(
				'success'=>false,
				'detail'=>$detail,
				'title'=>$message
		));
		$json_response->setStatusCode($code); // 403 Forbidden

		return $json_response;
	}

	protected function saveParametersAndDefaults(Request $request)
	{
		$this->request = $request;
		$this->limit = $request->get("limit",10) < 100 ? $request->get("limit",10): 100;
		$this->start = $request->get("start",0);
		$this->sortBy = $request->get("sort_by");
		$this->order = $request->get("order","asc");
		if(!in_array($this->order,["asc","desc"])){
			$this->order = "asc";
		}
		if(isset($this->request) && $this->request->query->has("format") && $this->request->get("format") == "simple"){
			$this->limit = $request->get("limit",100000);
			$this->start = 0;
			$this->format = "json";
		}
	}

	protected function loadEmployeeInformation()
	{
		$em = $this->db->getRepository('e:ForeupEmployees');
		$result = $em->findBy(["person"=>$this->auth_user->getEmpId(),"courseId"=>$this->auth_user->getCid()]);

		$this->employee = isset($result[0])?$result[0]:false;
	}

	public function byPassAccessCheck()
	{
		$this->byPassAccessCheck = true;
	}

	protected function checkAccess($courseId)
	{
		if($this->byPassAccessCheck)
			return true;

		$course = $this->db->getRepository("e:ForeupCourses")->find($courseId);
		$this->course = $course;
		!empty($course->getTimezone())? date_default_timezone_set($course->getTimezone()) : "";

		if($this->auth_user->getIsEmployee() && ($this->auth_user->getCid() == $courseId || $this->auth_user->getLevel()*1 === 5)){
			return true;
		} else {
			//Check api access
			$permissionsRepository = $this->db->getRepository('e:ForeupApiUserCourses');
			/** @var ForeupApiUserCourses $access */
			$access = $permissionsRepository->findOneBy(["course"=>$courseId,'userId'=>$this->auth_user->getAppId()]);
			if(empty($access)){
				$json_response = new JsonResponse();
				$json_response->setData(array(
					'title'=>"Permission Denied",
					'detail'=>'Your user account doesn\'t have permission to access this course.'
				));
				$json_response->setStatusCode(400);
				$this->response = $json_response;
				return false;
			}

			$this->auth_user->setAppId($access->getUser());
			$this->auth_user->setUid($access->getEmployee()->getPerson()->getPersonId());

		}
		return true;
	}


	/**
	 * @param Request $request
	 * @param $courseId
	 * @throws CoursePermission
	 */
	protected function checkCoursePermission(Request $request, $courseId)
	{
		$this->saveParametersAndDefaults($request);
		if (!$this->checkAccess($courseId)){
			throw new \Exception('Your user account doesn\'t have permission to access this course.');
		}
	}

	/**
	 * @param $results
	 * @return JsonResponse
	 */
	protected  function generateJsonResponse($results): JsonResponse
	{
		$content = $this->serializeResource($results);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	/**
	 * @param Request $request
	 * @param $resource
	 * @throws \Exception
	 * @throws \Exception
	 */
	protected function updateResource(Request $request, $resource,$autoFlush = true)
	{
		$data = $request->request->get("data");
		if (!isset($data['attributes'])) {
			throw new \Exception("Attributes required in request");
		}
		$this->fieldsToSkip[] = "id";
		$transformedObject = $this->transformer->transform($resource);
		foreach ($transformedObject as $field => $value) {
			if(array_search($field,$this->fieldsToSkip) !== false)
				continue;
			if(!empty($this->fieldsOnlyUpdate) && !array_search($field,$this->fieldsOnlyUpdate)){
				continue;
			}

			if (isset($data['attributes'][$field]) && !in_array($field, $this->blacklistAttributes)) {
				$functionName = "set" . $field;
				if (!method_exists($resource, $functionName)) {
					$alternateFunctionName = $functionName;
					$functionName = preg_replace("/\_/","",$functionName);
					if (!method_exists($resource, $functionName)) {
						$functionName = preg_replace("/setis/","set",$functionName);
						if (!method_exists($resource, $functionName)) {
							$functionName = preg_replace("/setis/","",$functionName);
							throw new \Exception("Something seems to have been set up wrong, the following method doesn't exist on our object: " . $functionName." or ".$alternateFunctionName);
						}
					}
				}
				$resource->{$functionName}($data['attributes'][$field]);
			}
		}

		if($autoFlush){
			try{
				$this->db->flush();
			} catch (UniqueConstraintViolationException $e){
				return $this->respondWithError("Validation error, account number not unique.",401);
			}
		}

	}
}