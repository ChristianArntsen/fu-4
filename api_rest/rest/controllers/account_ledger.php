<?php
namespace foreup\rest\controllers;

use Carbon\Carbon;
use foreup\rest\models\entities\ForeupAccountLedger;
use foreup\rest\resource_transformers\account_ledger_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class account_ledger extends api_controller
{
	/** @var \Doctrine\ORM\EntityRepository $repository */
	private $repository;


	/** @var \Doctrine\ORM\EntityManager $db */
	private $db;

	protected $current_user;

	/** @var  \foreup\rest\models\entities\ForeupEmployees */
	private $employee;

	public function __construct($em,$app)
	{
		parent::__construct();

		$this->db = $em;
		$this->repository = $this->db->getRepository('e:ForeupAccountLedger');
		$this->current_user = $app['auth.user'];
		$this->employee = $app['employee'];
		$this->transformer = new account_ledger_transformer();
		$this->resource_type = "account_ledger";
	}

	public function get($id)
	{

	}

	public function save($account_id,Request $request)
	{
		$data = $request->request->get("data");

		//Check that current user is an employee
		//Check that account organization_id belongs to same organization as employee
		if($this->current_user['isEmployee'] === true){
			$this->employee->getCourseId();
			$this->db->getRepository("e:ForeupPeople");

			$account = $this->db->getRepository("e:ForeupAccounts")->findOneBy([
				"id"=>$account_id,
				"organizationId"=>$this->employee->getCourseId()
			]);
			if(!isset($account)){
				return false;
			}
		}



		$ledger_item = new ForeupAccountLedger();
		$ledger_item->setCreatedBy($this->current_user);
		isset($data['type']) ? $ledger_item->setType($data['type']):"";
		isset($data['amount']) ? $ledger_item->setAmount($data['amount']):$ledger_item->setAmount(0);
		isset($data['amount']) ? $ledger_item->setAmountOpen($data['amount']):$ledger_item->setAmountOpen(0);
		isset($data['start_date']) ? $ledger_item->setStartDate($data['start_date']):$ledger_item->setStartDate(Carbon::now());
		isset($data['due_date']) ? $ledger_item->setDueDate($data['due_date']):$ledger_item->setDueDate(null);


		$ledger_item->setOrganizationId($this->employee->getCourseId());
		$ledger_item->setPersonId($this->employee->getPerson()->getPersonId());
		$ledger_item->setAccount($account);


		$this->repository->create($ledger_item);



		$response = new JsonResponse();
		$response->setContent($this->serializeResource($ledger_item));
		return $response;
	}

}