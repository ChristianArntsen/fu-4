<?php
namespace foreup\rest\controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class customer_images extends api_controller
{

	public function get(Request $request,$name)
	{
		if(!preg_match("/(.*)(\.png)?/",$name,$matches)){
			//throw new \Exception("Invalid image type, must be png.");
		}
		$name = $matches[1];
		$names = preg_split("/_/",$name);
		if(count($names)>1 && $names[1] != "")
			$avatar = new \YoHang88\LetterAvatar\LetterAvatar($names[0]." ".$names[1], 'square', 64);
		else {
			if($names == "_"){
				$avatar = new \YoHang88\LetterAvatar\LetterAvatar("", 'square', 64);
			} else {
				$avatar = new \YoHang88\LetterAvatar\LetterAvatar($name, 'square', 64);
			}

		}

		return new Response($avatar->generate()->stream(), 200, array('Content-Type' => 'image/png'));
	}
}