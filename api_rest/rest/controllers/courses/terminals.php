<?php
namespace foreup\rest\controllers\courses;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use foreup\rest\controllers\api_controller;
use foreup\rest\controllers\traits\rollback_trait;
use foreup\rest\models\entities\ForeupTerminals;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\inventory_transformer;
use foreup\rest\resource_transformers\items_transformer;
use foreup\rest\resource_transformers\terminals_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class terminals extends api_controller
{

	use rollback_trait;

    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupTerminals');
        $this->transformer = new terminals_transformer();
        $this->resource_type = "terminals";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId)
    {
		$this->checkCoursePermission($request, $courseId);

		$results = $this->repository->findBy([
			"courseId"=>$courseId
		]);

		return $this->generateJsonResponse($results);
    }

	public function get(Request $request,$courseId,$terminalId)
	{
		$this->checkCoursePermission($request, $courseId);

		$terminal = $this->repository->findOneBy([
			"courseId"=>$courseId,
			"id"=>$terminalId
		]);

		return $this->generateJsonResponse($terminal);
	}

	public function update(Request $request,$courseId,$terminalId)
	{
		$this->checkCoursePermission($request, $courseId);

		$terminal = $this->repository->findOneBy([
			"courseId"=>$courseId,
			"id"=>$terminalId
		]);

		if(!$terminal){
			return $this->respondWithError("Terminal not found or you don't have permission to access this resource. ");
		}

		$this->updateResource($request, $terminal);

		return $this->generateJsonResponse($terminal);
	}

	public function create(Request $request, $courseId){
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupTerminals $terminal */
		$terminal = new ForeupTerminals($this->course);
		$terminal->setCourseId($courseId);

		$this->db->persist($terminal);
		$this->updateResource($request, $terminal);

		return $this->generateJsonResponse($terminal);
	}

	public function delete(Request $request,$courseId,$terminalId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupTerminals $terminal */
		$terminal = $this->repository->find($terminalId);

		$this->db->remove($terminal);
		$this->db->flush();

		return $this->generateJsonResponse($terminal);
	}
}
?>