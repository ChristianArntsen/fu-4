<?php
namespace foreup\rest\controllers\courses\teesheets;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Logging\EchoSQLLogger;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupSpecials;
use foreup\rest\models\entities\ForeupTeesheet;
use foreup\rest\resource_transformers\specials_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class specials extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupSpecials');
        $this->transformer = new specials_transformer();
        $this->resource_type = "special";
        $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId,$teesheetId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;


        $criteria = new Criteria();
        $criteria->andWhere($criteria->expr()->eq('teesheetId',$teesheetId));

        $results = $this->repository->matching($criteria);



        $content = $this->serializeResource($results->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function get(Request $request,$courseId,$teesheetId,$specialId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;


        $season = $this->repository->find($specialId);
        if($season->getTeesheetId() != $teesheetId){
            return $this->respondWithError("No permission. ",401);
        }



        $content = $this->serializeResource($season);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function delete(Request $request,$courseId,$teesheetId,$specialId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupSeasons $season */
        $special = $this->repository->find($specialId);
        if($season->getTeesheetId() != $teesheetId){
            return $this->respondWithError("No permission. ",401);
        }

        $season->setDeleted(1);
        $this->db->flush();


        $content = $this->serializeResource($special);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function create(Request $request,$courseId,$teesheetId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupTeesheet $teesheet */
        $teesheetRepo = $this->db->getRepository('e:ForeupTeesheet');
        $teesheet = $teesheetRepo->find($teesheetId);
        if($teesheet->getId() != $teesheetId){
            return $this->respondWithError("No permission. ",401);
        }



        $data = $request->request->get("data");


        $special = new ForeupSpecials();
        if(!isset($data['attributes'])){
            return $this->respondWithError("Attributes is required. ");
        }


        $special->setCourse($teesheet->getCourse());
        $special->setTeesheetId($teesheetId);
//        if(isset($data['attributes']['name'])){
//            $bookingClass->setSeasonName($data['attributes']['name']);
//        } else {
//            return $this->respondWithError("Season name is required. ");
//        }
//        if(isset($data['attributes']['startDate'])){
//            $date = Carbon::parse($data['attributes']['startDate']);
//            $season->setStartDate($date);
//        } else {
//            return $this->respondWithError("Start date is required. ");
//        }
//        if(isset($data['attributes']['endDate'])){
//            $date = Carbon::parse($data['attributes']['endDate']);
//            $season->setEndDate($date);
//        } else {
//            return $this->respondWithError("End date is required. ");
//        }

//        $bookingClass->setDeleted(0);
//        $bookingClass->setOnlineBooking(0);
//        $bookingClass->setOnlineCloseTime(0);
//        $bookingClass->setOnlineOpenTime(0);

//        /** @var EntityRepository $priceClassRepo */
//        $priceClassRepo = $this->db->getRepository('e:ForeupPriceClass');
//        $pc = $priceClassRepo->findOneBy([
//            "default"=>true,
//            "course"=>$this->db->getReference('e:ForeupCourses',$courseId)
//        ]);
//        if(empty($pc)){
//            $this->respondWithError("Unable to find default price class.",500);
//        }

//        $timeframe = new ForeupSeasonalTimeframes($pc,$season);

//        $season->addClass($pc);


//        if($season->isValid()){
//            $this->db->persist($timeframe);
//            $this->db->persist($season);
//            $this->db->flush();
//        } else {
//            return $this->respondWithError("The season is not valid. ");
//        }




        $content = $this->serializeResource($special);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function update(Request $request,$courseId,$teesheetId,$specialId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupSeasons $season */
        $special = $this->repository->find($specialId);
        if($special->getTeesheetId() != $teesheetId){
            $this->respondWithError("No permission. ",401);
        }


        $data = $request->request->get("data");


//        if(isset($data['attributes'])){
//            if(isset($data['attributes']['name'])){
//                $season->setSeasonName($data['attributes']['name']);
//            }
//            if(isset($data['attributes']['startDate'])){
//                $date = Carbon::parse($data['attributes']['startDate']);
//                $season->setStartDate($date);
//            }
//            if(isset($data['attributes']['endDate'])){
//                $date = Carbon::parse($data['attributes']['endDate']);
//                $season->setEndDate($date);
//            }
//
//            if($season->isValid()){
//                $this->db->flush();
//            } else {
//                $this->respondWithError("The season is not valid. ");
//            }
//
//        }


        $content = $this->serializeResource($special);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }
}
?>