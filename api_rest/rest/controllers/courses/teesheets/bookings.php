<?php
namespace foreup\rest\controllers\courses\teesheets;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupTeesheet;
use foreup\rest\models\entities\ForeupTeetime;
use foreup\rest\models\entities\Partial\BookingPlayer;
use foreup\rest\models\services\AuthenticatedUser;
use foreup\rest\models\services\LegacyTeetimeService;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\teetime_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class bookings extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;


	/**
	 * @var LegacyTeetimeService
	 */
	private $teetimeService;

    public function __construct($db,AuthenticatedUser $auth_user, LegacyTeetimeService $legacyTeetimeService)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupTeetime');
        $this->transformer = new teetime_transformer();
        $this->transformer->setVersion($auth_user->getApiVersion());
        $this->resource_type = "bookings";
	    $this->relativeUrl = "/courses/6270/teesheets/27";
	    $this->teetimeService = $legacyTeetimeService;
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId,$teesheetId)
    {
	    $this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
		    return $this->response;


	    /** @var EntityRepository $teesheets */
	    $teesheets = $this->db->getRepository('e:ForeupTeesheet');
	    /** @var ForeupTeesheet $teesheet */
	    $teesheet = $teesheets->find($teesheetId);
	    if(empty($teesheet) )
		    return $this->respondWithError("Invalid tee sheet. ",401);

	    if($teesheet->getCourseId() != $courseId)
		    return $this->respondWithError("No permission. ",401);


	    $startDate = !empty($request->get("startDate"))?Carbon::parse($request->get("startDate")):Carbon::now()->startOfDay();
	    $endDate = !empty($request->get("endDate"))?Carbon::parse($request->get("endDate")):Carbon::now()->endOfDay();

	    $criteria = new Criteria();
	    $criteria->andWhere($criteria->expr()->gte('startDatetime', $startDate));
	    $criteria->andWhere($criteria->expr()->lte('endDatetime', $endDate));
	    $criteria->andWhere($criteria->expr()->eq('teesheetId', $teesheetId));

	    if($request->get("filter")){
		    $filters = $request->get("filter");
		    if(isset($filters['side'])){
			    $criteria->andWhere($criteria->expr()->eq('side', $filters['side']));
		    }

	    }
	    $criteria->setMaxResults($this->limit);
	    $criteria->setFirstResult($this->start);
	    $criteria->orderBy(["startDatetime"=>Criteria::ASC]);
	    $criteria->andWhere($criteria->expr()->neq("status","deleted"));
	    $criteria->andWhere($criteria->expr()->neq("reround",1));

	    $results = $this->repository->matching($criteria);


	    return $this->generateJsonResponse($results->toArray());
    }

	public function getOne(Request $request,$courseId,$teesheetId,$bookingId)
	{
		$this->checkCoursePermission($request, $courseId);


		$teetime = $this->repository->find($bookingId);
		if(($teetime && $teetime->getTeesheetId() != $teesheetId) || empty($teetime)){
			return $this->respondWithError("No permission. ",401);
		}



		return $this->generateJsonResponse($teetime);
	}

	public function update(Request $request,$courseId,$teesheetId,$bookingId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupTeetime $teetime */
		$teetime = $this->repository->find($bookingId);
		if($teetime && $teetime->getTeesheetId() != $teesheetId){
			return $this->respondWithError("No permission. ",401);
		}

		if(!empty($teetime)){

			$this->fieldsToSkip = ["isReround","type","stats","start","end","title","bookingSource","lastUpdated","dateBooked","dateCancelled"];
			$this->updateResource($request, $teetime);
		}

		return $this->generateJsonResponse($teetime);

	}

	public function create(Request $request,$courseId,$teesheetId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var EntityRepository $teesheets */
		$teesheets = $this->db->getRepository('e:ForeupTeesheet');
		/** @var ForeupTeesheet $teesheet */
		$teesheet = $teesheets->find($teesheetId);
		if(empty($teesheet) )
			return $this->respondWithError("Invalid tee sheet. ",401);

		if($teesheet->getCourseId() != $courseId)
			return $this->respondWithError("No permission. ",401);

		$data = $request->request->get("data");
		if(!isset($data['attributes'])){
			return $this->respondWithError("Attributes is required. ");
		}


		$booking = new ForeupTeetime();
		$booking->setTeesheetId($teesheetId);
		$possibleSets = [
			"start"=>"setStart",
			"side"=>"setSide",
			"details"=>"setDetails",
			"holes"=>"setHoles",
			"players"=>"setPlayerCount",
			"carts"=>"setCarts",
			"title"=>"setTitle"
		];
		array_change_key_case($data['attributes']);
		foreach($possibleSets as $attribute=>$functionName){
			if(isset($data['attributes'][$attribute])){
				$booking->{$functionName}($data['attributes'][$attribute]);
			}
		}
		$booking->setSide("front");
		$booking->setBookingSource("api");

		$this->teetimeService->setToken($this->auth_user->getToken());

		try {
			$bookingId = $this->teetimeService->bookTime($booking);
		} catch(\Exception $e){
			return $this->respondWithError("Error with booking. ".$e->getMessage());
		}

		if(!$bookingId){
			return $this->respondWithError("Error with booking. ");
		}

		$booking = $this->repository->find($bookingId);
		return $this->generateJsonResponse($booking);
	}



	public function delete(Request $request,$courseId,$teesheetId,$bookingId)
	{
		$this->checkCoursePermission($request, $courseId);



		/** @var ForeupTeetime $teetime */
		$teetime = $this->repository->find($bookingId);
		if($teetime && $teetime->getTeesheetId() != $teesheetId){
			return $this->respondWithError("No permission. ",401);
		}

		$teetime->setStatus("deleted");
		$teetime->setCancellerId($this->auth_user->getEmpId());
		$teetime->setDateCancelled(Carbon::now());
		$teetime->setType($teetime->getType()."-emp deleted");

		$teetimeReround = $this->repository->find($bookingId."b");
		if($teetimeReround){
			$teetimeReround->setStatus("deleted");
			$teetimeReround->setCancellerId($this->auth_user->getEmpId());
			$teetimeReround->setDateCancelled(Carbon::now());
			$teetimeReround->setType($teetime->getType()."-emp deleted");
		}

		$this->db->flush();

		return $this->generateJsonResponse($teetime);
	}
}
?>