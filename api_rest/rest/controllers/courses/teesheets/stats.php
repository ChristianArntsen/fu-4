<?php
namespace foreup\rest\controllers\courses\teesheets;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\services\TeesheetStats;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\teesheet_stats_transformer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class stats extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;

	/** @var  TeesheetStats $teesheetStatsServiced */
	private $teesheetStatsService;

    public function __construct($db,$auth_user,$teesheetStatsService)
    {
        parent::__construct();
		$this->teesheetStatsService = $teesheetStatsService;
        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupTeesheet');
        $this->transformer = new teesheet_stats_transformer();
        $this->resource_type = "teesheet_stats";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId,$teesheetId)
    {
	    $this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
		    return $this->response;
	    $date = !empty($request->get("date"))?Carbon::parse($request->get("date")):Carbon::now()->startOfDay();
	    $start = !empty($request->get("start"))?Carbon::parse($request->get("start")):Carbon::now()->startOfDay();
	    $end = !empty($request->get("end"))?Carbon::parse($request->get("end")):$start->copy()->endOfDay();

	    try {
		    if(!empty($request->get("date"))){
			    $statsObject = $this->teesheetStatsService->get($teesheetId,$this->auth_user->getToken(),$date);
		    } else {
			    $statsObject = $this->teesheetStatsService->get($teesheetId,$this->auth_user->getToken(),$start,$end);
		    }
	    } catch(\Exception $e){
		    return $this->respondWithError($e->getMessage());
	    }


	    if(!$statsObject){
	    	return $this->respondWithError("Unable to retrieve data");
	    }

	    $content = $this->serializeResource($statsObject);
	    $response = new JsonResponse();
	    $response->setContent($content);
	    return $response;
    }
}
?>