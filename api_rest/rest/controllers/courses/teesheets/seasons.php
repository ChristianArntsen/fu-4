<?php
namespace foreup\rest\controllers\courses\teesheets;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Logging\EchoSQLLogger;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupPriceClass;
use foreup\rest\models\entities\ForeupSeasonalTimeframes;
use foreup\rest\models\entities\ForeupSeasons;
use foreup\rest\models\entities\ForeupTeesheet;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\teesheet_seasons_transformer;
use foreup\rest\resource_transformers\teetime_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class seasons extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupSeasons');
        $this->transformer = new teesheet_seasons_transformer();
        $this->resource_type = "seasons";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId,$teesheetId)
    {
	    $this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
		    return $this->response;


	    $criteria = new Criteria();
	    $criteria->andWhere($criteria->expr()->eq('deleted',0));
	    $criteria->andWhere($criteria->expr()->eq('teesheet', $this->db->getReference('e:ForeupTeesheet',$teesheetId)));

	    $results = $this->repository->matching($criteria);



	    $content = $this->serializeResource($results->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

	public function get(Request $request,$courseId,$teesheetId,$seasonId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;


		$season = $this->repository->find($seasonId);
		if(empty($season)){
			return $this->respondWithError("The season does not exist. ",404);
		}

		if($season->getTeesheet()->getId() != $teesheetId){
			return $this->respondWithError("No permission. ",401);
		}



		$content = $this->serializeResource($season);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	public function delete(Request $request,$courseId,$teesheetId,$seasonId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		/** @var ForeupSeasons $season */
		$season = $this->repository->find($seasonId);
		if($season->getTeesheet()->getId() != $teesheetId){
			return $this->respondWithError("No permission. ",401);
		}

		$season->setDeleted(1);
		$this->db->flush();


		$content = $this->serializeResource($season);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	public function create(Request $request,$courseId,$teesheetId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		/** @var ForeupTeesheet $teesheet */
		$teesheetRepo = $this->db->getRepository('e:ForeupTeesheet');
		$teesheet = $teesheetRepo->find($teesheetId);
		if($teesheet->getId() != $teesheetId){
			return $this->respondWithError("No permission. ",401);
		}



		$data = $request->request->get("data");


		$season = new ForeupSeasons();
		if(!isset($data['attributes'])){
			return $this->respondWithError("Attributes is required. ");
		}


		$season->setCourse($teesheet->getCourse());
		$season->setTeesheet($teesheet);
		if(isset($data['attributes']['name'])){
			$season->setSeasonName($data['attributes']['name']);
		} else {
			return $this->respondWithError("Season name is required. ");
		}
		if(isset($data['attributes']['startDate'])){
			$date = Carbon::parse($data['attributes']['startDate']);
			$season->setStartDate($date);
		} else {
			return $this->respondWithError("Start date is required. ");
		}
		if(isset($data['attributes']['endDate'])){
			$date = Carbon::parse($data['attributes']['endDate']);
			$season->setEndDate($date);
		} else {
			return $this->respondWithError("End date is required. ");
		}

		$season->setDeleted(0);
		$season->setOnlineBooking(0);
		$season->setOnlineCloseTime(0);
		$season->setOnlineOpenTime(0);

		/** @var EntityRepository $priceClassRepo */
		$priceClassRepo = $this->db->getRepository('e:ForeupPriceClass');
		$pc = $priceClassRepo->findOneBy([
			"default"=>true,
			"course"=>$this->db->getReference('e:ForeupCourses',$courseId)
		]);
		if(empty($pc)){
			$this->respondWithError("Unable to find default price class.",500);
		}

		$timeframe = new ForeupSeasonalTimeframes($pc,$season);

		$season->addClass($pc);


		if($season->isValid()){
			$this->db->persist($timeframe);
			$this->db->persist($season);
			$this->db->flush();
		} else {
			return $this->respondWithError("The season is not valid. ");
		}




		$content = $this->serializeResource($season);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	public function update(Request $request,$courseId,$teesheetId,$seasonId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		/** @var ForeupSeasons $season */
		$season = $this->repository->find($seasonId);
		if($season->getTeesheet()->getId() != $teesheetId){
			$this->respondWithError("No permission. ",401);
		}


		$data = $request->request->get("data");


		if(isset($data['attributes'])){
			if(isset($data['attributes']['name'])){
				$season->setSeasonName($data['attributes']['name']);
			}
			if(isset($data['attributes']['startDate'])){
				$date = Carbon::parse($data['attributes']['startDate']);
				$season->setStartDate($date);
			}
			if(isset($data['attributes']['endDate'])){
				$date = Carbon::parse($data['attributes']['endDate']);
				$season->setEndDate($date);
			}

			if($season->isValid()){
				$this->db->flush();
			} else {
				$this->respondWithError("The season is not valid. ");
			}

		}


		$content = $this->serializeResource($season);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}
}
?>