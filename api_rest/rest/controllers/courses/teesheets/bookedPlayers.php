<?php
namespace foreup\rest\controllers\courses\teesheets;

use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupTeetime;
use foreup\rest\resource_transformers\booked_player_transformer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class bookedPlayers extends api_controller
{

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->transformer = new booked_player_transformer();
        $this->resource_type = "bookedPlayer";
	    $this->loadEmployeeInformation();
    }

    public function update(Request $request,$courseId,$bookingId,$bookedPlayerId)
    {
	    $this->checkCoursePermission($request, $courseId);


	    $data = $request->request->get("data");
	    $teetimeRepo = $this->db->getRepository('e:ForeupTeetime');
	    /** @var ForeupTeetime $teetime */
	    $teetime = $teetimeRepo->find($bookingId);
	    if(empty($teetime)){
	    	return $this->respondWithError("Booking not found.");
	    }
	    $bookedPlayer = $teetime->getBookedPlayerById($bookedPlayerId);

	    $this->fieldsToSkip = ["id","priceClassId","priceClass","personId"];
	    $this->updateResource($request,$bookedPlayer,false);
	    if (isset($data['attributes']['priceClassId'])) {
	    	if(!empty($data['attributes']['priceClassId'])){
			    /** @var EntityRepository $classRepo */
			    $classRepo = $this->db->getRepository('e:ForeupPriceClass');
			    $priceClass = $classRepo->findOneBy([
				    "course"=>$this->course->getGroupMemberIds(),
				    "id"=>$data['attributes']['priceClassId']
			    ]);
			    if(empty($priceClass)){
				    return $this->respondWithError("There doesn't exist a price class for the id: ".$data['attributes']['priceClassId']);
			    }
			    $bookedPlayer->setPriceClass($priceClass);
		    }

	    }


	    if (!empty($data['attributes']['personId'])) {
	    	if(!empty($data['attributes']['name'])){
	    		unset($data['attributes']['name']);
	    		//return $this->respondWithError("Unable to pass in a name and a personId at the same time.");
		    }
		    $customerRepo = $this->db->getRepository('e:ForeupCustomers');
		    /** @var ForeupCustomers $customer */
		    $customer = $customerRepo->findOneBy([
			    "courseId"=>$this->course->getGroupMemberIds(),
			    "person"=>$data['attributes']['personId']
		    ]);
		    if(empty($customer)){
			    return $this->respondWithError("There doesn't exist a customer for the id: ".$data['attributes']['personId']);
		    }
		    $bookedPlayer->setPerson($customer->getPerson());
		    if(!empty($customer->getPriceClass())){
			    $classRepo = $this->db->getRepository('e:ForeupPriceClass');
			    $priceClass = $classRepo->findOneBy([
				    "course"=>$this->course->getGroupMemberIds(),
				    "id"=>$customer->getPriceClass()
			    ]);
			    if(!empty($priceClass)){
				    $bookedPlayer->setPriceClass($priceClass);
			    }
		    }
	    } else if (isset($data['attributes']['personId']) && $data['attributes']['personId'] === ""){
		    $bookedPlayer->setPerson(null);
	    }


	    $this->updateResource($request,$bookedPlayer,false);
	    $this->db->flush();
	    return $this->generateJsonResponse($bookedPlayer);
    }
}
?>