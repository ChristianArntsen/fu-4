<?php
namespace foreup\rest\controllers\courses;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Logging\EchoSQLLogger;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupOnlineHours;
use foreup\rest\models\entities\ForeupTeesheet;
use foreup\rest\resource_transformers\online_hours_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class onlineHours extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupOnlineHours');
        $this->transformer = new online_hours_transformer();
        $this->resource_type = "onlineHours";
        $this->loadEmployeeInformation();
    }

    public function get(Request $request,$courseId,$teesheetId,$specialId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;


        $season = $this->repository->find($specialId);
        if($season->getTeesheetId() != $teesheetId){
            return $this->respondWithError("No permission. ",401);
        }



        $content = $this->serializeResource($season);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function create(Request $request,$courseId,$teesheetId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupTeesheet $teesheet */
        $teesheetRepo = $this->db->getRepository('e:ForeupTeesheet');
        $teesheet = $teesheetRepo->find($teesheetId);
        if($teesheet->getId() != $teesheetId){
            return $this->respondWithError("No permission. ",401);
        }

        $data = $request->request->get("data");

        $onlineHours = new ForeupOnlineHours();
        if(!isset($data['attributes'])){
            return $this->respondWithError("Attributes is required. ");
        }


        $onlineHours->setCourse($teesheet->getCourse());
        $onlineHours->setId($teesheetId);

        $content = $this->serializeResource($onlineHours);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function update(Request $request,$courseId,$teesheetId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupOnlineHours $onlineHours */
        $onlineHours = $this->repository->find($teesheetId);
//        if($onlineHours->getId() != $teesheetId){
//            $this->respondWithError("No permission. ",401);
//        }

        $data = $request->request->get("data");
//        if(isset($data['attributes'])){
//            if(isset($data['attributes']['name'])){
//                $season->setSeasonName($data['attributes']['name']);
//            }
//            if(isset($data['attributes']['startDate'])){
//                $date = Carbon::parse($data['attributes']['startDate']);
//                $season->setStartDate($date);
//            }
//            if(isset($data['attributes']['endDate'])){
//                $date = Carbon::parse($data['attributes']['endDate']);
//                $season->setEndDate($date);
//            }
//
//            if($season->isValid()){
//                $this->db->flush();
//            } else {
//                $this->respondWithError("The season is not valid. ");
//            }
//
//        }

        $content = $this->serializeResource($onlineHours);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }
}
?>