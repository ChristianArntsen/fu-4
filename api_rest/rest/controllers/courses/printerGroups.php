<?php
namespace foreup\rest\controllers\courses;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\controllers\traits\rollback_trait;
use foreup\rest\mock\models\exceptions\CoursePermission;
use foreup\rest\models\entities\ForeupPrinterGroups;
use foreup\rest\models\entities\ForeupPrinters;
use foreup\rest\models\entities\ForeupTerminals;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\inventory_transformer;
use foreup\rest\resource_transformers\items_transformer;
use foreup\rest\resource_transformers\printer_groups_transformer;
use foreup\rest\resource_transformers\printers_transformer;
use foreup\rest\resource_transformers\terminals_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class printerGroups extends api_controller
{
	use rollback_trait;

    /** @var EntityRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupPrinterGroups');
        $this->transformer = new printer_groups_transformer();
        $this->resource_type = "printerGroups";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId)
    {
	    $this->checkCoursePermission($request, $courseId);
	    $results = $this->repository->findBy([
	    	"course"=>$courseId
	    ]);
        return $this->generateJsonResponse($results);
    }


	public function get(Request $request,$courseId,$printerGroupId)
	{
		$this->checkCoursePermission($request, $courseId);

		$printerGroup = $this->repository->findOneBy([
			"course"=>$courseId,
			"id"=>$printerGroupId
		]);

		return $this->generateJsonResponse($printerGroup);
	}

	public function update(Request $request,$courseId,$printerGroupId)
	{
		$this->checkCoursePermission($request, $courseId);

		$printerGroup = $this->repository->findOneBy([
			"course"=>$courseId,
			"id"=>$printerGroupId
		]);

		if(!empty($printerGroup)){
			$this->updateResource($request, $printerGroup);
		}

		return $this->generateJsonResponse($printerGroup);
	}

	public function create(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupPrinters $printer */
		$printerGroup = new ForeupPrinterGroups();
		$printerGroup->setCourse($this->course);

		$this->db->persist($printerGroup);
		$this->updateResource($request, $printerGroup);

		return $this->generateJsonResponse($printerGroup);
	}

    public function delete(Request $request,$courseId,$printerGroupId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupPrinters $printer */
        $printerGroup = $this->repository->find($printerGroupId);

        $this->db->remove($printerGroup);
        $this->db->flush();

        $content = $this->serializeResource($printerGroup);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }


}
?>