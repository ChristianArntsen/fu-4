<?php
namespace foreup\rest\controllers\courses;

use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupItemReceiptContent;
use foreup\rest\models\entities\ForeupPrinters;
use foreup\rest\models\entities\ForeupRefundReasons;
use foreup\rest\resource_transformers\receipt_agreements;
use foreup\rest\resource_transformers\receipt_agreements_transformer;
use Symfony\Component\HttpFoundation\Request;

class receiptAgreements extends api_controller
{
    /** @var EntityRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('e:ForeupItemReceiptContent');
        $this->transformer = new receipt_agreements_transformer();
        $this->resource_type = "receiptContent";
	    $this->loadEmployeeInformation();
    }

	public function getAll(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);
		$results = $this->repository->findBy([
			"courseId"=>$courseId
		]);
		return $this->generateJsonResponse($results);
	}


	public function get(Request $request,$courseId,$agreementId)
	{
		$this->checkCoursePermission($request, $courseId);

		$reason = $this->repository->findOneBy([
			"courseId"=>$courseId,
			"id"=>$agreementId
		]);

		return $this->generateJsonResponse($reason);
	}

	public function update(Request $request,$courseId,$agreementId)
	{
		$this->checkCoursePermission($request, $courseId);

		$reason = $this->repository->findOneBy([
			"courseId"=>$courseId,
			"id"=>$agreementId
		]);

		if(!empty($reason)){
			$this->updateResource($request, $reason);
		}

		return $this->generateJsonResponse($reason);
	}

	public function create(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupPrinters $printer */
		$receipt = new ForeupItemReceiptContent($this->course);
		$receipt->setCourseId($courseId);
		$this->db->persist($receipt);
		$this->updateResource($request, $receipt);
		$receipt->setCourseId($courseId);

		return $this->generateJsonResponse($receipt);
	}



}
?>