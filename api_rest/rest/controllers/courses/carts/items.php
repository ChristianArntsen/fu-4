<?php
namespace foreup\rest\controllers\courses\carts;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Tools\Pagination\Paginator;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupPosCart;
use foreup\rest\models\entities\ForeupPosCartItems;
use foreup\rest\models\entities\LegacyObjects\Payment;
use foreup\rest\models\services\AuthenticatedUser;
use foreup\rest\models\services\FilterQueryParser;
use foreup\rest\models\services\LegacyCartService;
use foreup\rest\resource_transformers\cart_item_transformer;
use foreup\rest\resource_transformers\cart_transformer;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\field_transformer;
use foreup\rest\resource_transformers\sales_transformer;
use League\Csv\Writer;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Symfony\Component\Finder\Expression\Expression;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class items extends api_controller
{
    /** @var EntityRepository $repository */
    private $repository;


	/**
	 * @var EntityManager
	 */
	protected $db;

    public function __construct($db,AuthenticatedUser $auth_user,LegacyCartService $cartService)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->cartService = $cartService;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupPosCart');
        $this->transformer = new cart_item_transformer();
        $this->resource_type = "carts";
	    $this->loadEmployeeInformation();
	    $this->cartService->setToken($auth_user->getToken());
    }

    public function update(Request $request, $courseId, $cartId, $line)
    {
	    $this->checkCoursePermission($request, $courseId);

	    /** @var ForeupPosCart $cart */
	    $cart = $this->repository->findOneBy([
		    "courseId"=>$courseId,
		    "cartId"=>$cartId
	    ]);
	    $criteria = Criteria::create()
		    ->where(Criteria::expr()->eq("line", (int)$line));
	    $filteredItems = $cart->getItems()->matching($criteria);
	    $item = $filteredItems->get(0);

	    if(empty($item)){
	    	return $this->respondWithError("Unable to find that line item.",404);
	    }

	    $this->fieldsOnlyUpdate=["quantity","discountPercent","unitPrice"];
	    $this->updateResource($request, $item);
	    return $this->generateJsonResponse($item);
    }
	public function delete(Request $request, $courseId, $cartId, $line)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupPosCart $cart */
		$cart = $this->repository->findOneBy([
			"courseId"=>$courseId,
			"cartId"=>$cartId
		]);
		$criteria = Criteria::create()
			->where(Criteria::expr()->eq("line", (int)$line));
		$filteredItems = $cart->getItems()->matching($criteria);
		$item = $filteredItems->get(0);

		if(empty($item)){
			return $this->respondWithError("Unable to find that line item.",404);
		}

		$this->db->remove($item);
		$this->db->flush();
		return $this->generateJsonResponse($item);
	}
    public function create(Request $request, $courseId, $cartId)
    {
	    $this->checkCoursePermission($request, $courseId);
	    $data = $request->request->get("data");
	    if (!isset($data['attributes'])) {
		    throw new \Exception("Attributes required in request");
	    }


	    $cart = $this->repository->find($cartId);

	    $this->cartService->setCartId($cartId);
	    /** @var EntityRepository $itemRepo */
	    $itemRepo = $this->db->getRepository('e:ForeupItems');
	    $item = $itemRepo->find($data['attributes']['itemId']);
	    if(empty($item)){
		    throw new \Exception("Invalid item in cart.");
	    }


	    $newItem = new ForeupPosCartItems($cart);
	    $newItem->setItem($item);
	    $this->fieldsToSkip = ["itemId","priceIncludesTax"];
	    $this->updateResource($request, $newItem,false);
//	    $newItem->setItemType();
//	    $newItem->setQuantity();
//	    $newItem->setUnitPrice();
//	    $newItem->setDiscountPercent();
		$line = $this->cartService->addItem($newItem);
		$newItem->setLine($line);

	    return $this->generateJsonResponse($newItem);
    }

}
?>