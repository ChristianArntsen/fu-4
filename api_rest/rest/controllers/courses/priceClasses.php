<?php
namespace foreup\rest\controllers\courses;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupPriceClass;
use foreup\rest\models\entities\ForeupPrinters;
use foreup\rest\models\entities\ForeupRefundReasons;
use foreup\rest\models\entities\ForeupTerminals;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\inventory_transformer;
use foreup\rest\resource_transformers\items_transformer;
use foreup\rest\resource_transformers\price_class_transformer;
use foreup\rest\resource_transformers\printers_transformer;
use foreup\rest\resource_transformers\return_reasons_transformer;
use foreup\rest\resource_transformers\terminals_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class priceClasses extends api_controller
{
    /** @var EntityRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupPriceClass');
        $this->transformer = new price_class_transformer();
        $this->resource_type = "priceClass";
	    $this->loadEmployeeInformation();
	    $this->fieldsToSkip = ["default","dateCreated","course"];
    }

	public function getAll(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);
		$results = $this->repository->findBy([
			"course"=>$courseId
		]);
		return $this->generateJsonResponse($results);
	}


	public function get(Request $request,$courseId,$priceClassId)
	{
		$this->checkCoursePermission($request, $courseId);

		$priceClass = $this->repository->findOneBy([
			"course"=>$courseId,
			"id"=>$priceClassId
		]);

		return $this->generateJsonResponse($priceClass);
	}

	public function update(Request $request,$courseId,$priceClassId)
	{
		$this->checkCoursePermission($request, $courseId);

		$priceClass = $this->repository->findOneBy([
			"course"=>$courseId,
			"id"=>$priceClassId
		]);

		if(!empty($priceClass)){
			$this->updateResource($request, $priceClass);
		}

		return $this->generateJsonResponse($priceClass);
	}

	public function create(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupPriceClass $class*/
		$class = new ForeupPriceClass($this->course);

		$this->db->persist($class);
		$this->updateResource($request, $class);

		return $this->generateJsonResponse($class);
	}

    public function delete(Request $request,$courseId,$priceClassId)
    {
	    $this->checkCoursePermission($request, $courseId);

        /** @var ForeupPriceClass $class*/
        $class = $this->repository->findOneBy([
	        "course"=>$courseId,
	        "id"=>$priceClassId
        ]);

        $this->db->remove($class);

        $this->db->flush();

	    return $this->generateJsonResponse($class);
    }

}
?>