<?php
namespace foreup\rest\controllers\courses;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\controllers\traits\rollback_trait;
use foreup\rest\models\entities\ForeupPrinters;
use foreup\rest\models\entities\ForeupTerminals;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\inventory_transformer;
use foreup\rest\resource_transformers\items_transformer;
use foreup\rest\resource_transformers\printers_transformer;
use foreup\rest\resource_transformers\terminals_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class printers extends api_controller
{
    /** @var EntityRepository $repository */
    private $repository;
	use rollback_trait;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupPrinters');
        $this->transformer = new printers_transformer();
        $this->resource_type = "printers";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId)
    {
	    $this->checkCoursePermission($request, $courseId);


	    $results = $this->repository->findBy([
	    	"course"=>$courseId
	    ]);

	    return $this->generateJsonResponse($results);
    }


	public function get(Request $request,$courseId,$printerId)
	{
		$this->checkCoursePermission($request, $courseId);


		$terminal = $this->repository->find($printerId);

		return $this->generateJsonResponse($terminal);
	}

	public function update(Request $request,$courseId,$printerId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupPrinters $printer */
		$printer = $this->repository->find($printerId);
		if($printer->getCourse()->getCourseId() != $courseId){
			return $this->respondWithError("No permission. ",401);
		}

		$data = $request->request->get("data");
		if(!isset($data['attributes'])){
			return $this->respondWithError("Attributes is required. ");
		}

		$transformedObject = $this->transformer->transform($printer);
		foreach($transformedObject as $field=>$value)
		{
            if(isset($data['attributes'][$field])){
				$functionName = "set".($field=='id'?'PrinterId':$field);
				if(!method_exists($printer,$functionName)){
					throw new \Exception("Something seems to have been set up wrong, the following method doesn't exist on our object: ".$functionName);
				}
				$printer->{$functionName}($data['attributes'][$field]);
			}
		}

		$this->db->flush();

		return $this->generateJsonResponse($printer);
	}

	public function create(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupPrinters $printer */
		$printer = new ForeupPrinters();
		$printer->setCourse($this->course);

		$data = $request->request->get("data");
		if(!isset($data['attributes'])){
			return $this->respondWithError("Attributes is required. ");
		}

		$transformedObject = $this->transformer->transform($printer);
		foreach($transformedObject as $field=>$value)
		{
			if(isset($data['attributes'][$field])){
                $functionName = "set".($field=='id'?'PrinterId':$field);
                if(!method_exists($printer,$functionName)){
					throw new \Exception("Something seems to have been set up wrong, the following method doesn't exist on our object: ".$functionName);
				}
				$printer->{$functionName}($data['attributes'][$field]);
			}
		}
		$this->db->persist($printer);
		$this->db->flush();

		return $this->generateJsonResponse($printer);
	}

    public function delete(Request $request,$courseId,$printerId)
    {
	    $this->checkCoursePermission($request, $courseId);

        /** @var ForeupPrinters $printer */
        $printer = $this->repository->find($printerId);

        $this->db->remove($printer);
        $this->db->flush();

	    return $this->generateJsonResponse($printer);
    }
}
?>