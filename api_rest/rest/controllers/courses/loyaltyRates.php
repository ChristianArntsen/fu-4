<?php
namespace foreup\rest\controllers\courses;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\mock\models\exceptions\CoursePermission;
use foreup\rest\models\entities\ForeupLoyaltyRates;
use foreup\rest\resource_transformers\loyalty_rates_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class loyaltyRates extends api_controller
{
    /** @var EntityRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupLoyaltyRates');
        $this->transformer = new loyalty_rates_transformer();
        $this->resource_type = "loyaltyRates";
        $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId)
    {
        $this->checkCoursePermission($request, $courseId);
        $results = $this->repository->findBy([
            "courseId"=>$courseId
        ]);
        return $this->generateJsonResponse($results);
    }


    public function get(Request $request,$courseId,$loyaltyRateId)
    {
        $this->checkCoursePermission($request, $courseId);

        $loyaltyRate = $this->repository->findOneBy([
            "courseId"=>$courseId,
            "id"=>$loyaltyRateId
        ]);

        return $this->generateJsonResponse($loyaltyRate);
    }

    public function update(Request $request,$courseId,$loyaltyRateId)
    {
        $this->checkCoursePermission($request, $courseId);

        $loyaltyRate = $this->repository->findOneBy([
            "courseId"=>$courseId,
            "id"=>$loyaltyRateId
        ]);

        if(!empty($loyaltyRate)){
            $this->updateResource($request, $loyaltyRate);
        }

        return $this->generateJsonResponse($loyaltyRate);
    }

    public function create(Request $request,$courseId)
    {
        $this->checkCoursePermission($request, $courseId);

        /** @var ForeupLoyaltyRates $loyaltyRate */
        $loyaltyRate = new ForeupLoyaltyRates();
        $loyaltyRate->setCourseId($courseId);

        $this->db->persist($loyaltyRate);
        $this->updateResource($request, $loyaltyRate);

        return $this->generateJsonResponse($loyaltyRate);
    }

    public function delete(Request $request,$courseId,$loyaltyRateId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupLoyaltyRates $loyaltyRate */
        $loyaltyRate = $this->repository->find($loyaltyRateId);

        $this->db->remove($loyaltyRate);
        $this->db->flush();

        $content = $this->serializeResource($loyaltyRate);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }


}
?>