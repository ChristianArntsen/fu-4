<?php
namespace foreup\rest\controllers\courses\customers;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\services\AuthenticatedUser;
use foreup\rest\resource_transformers\customer_group_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class group_members extends api_controller
{
    /** @var EntityManager $repository */
    private $repository;


	/**
	 * sales constructor.
	 * @param $db
	 * @param AuthenticatedUser $auth_user
	 */
	public function __construct($db, $auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupCustomerGroups');
        $this->transformer = new customer_group_transformer();
        $this->resource_type = "customer_groups";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId,$personId)
    {
		$this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
	    	return $this->response;

	    $criteria = new Criteria();
	    $criteria->andWhere($criteria->expr()->eq('courseId', $courseId));
	    $groups = $this->repository->matching($criteria);




        $content = $this->serializeResource($groups->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

}
?>