<?php
namespace foreup\rest\controllers\courses;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Tools\Pagination\Paginator;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\LegacyObjects\Payment;
use foreup\rest\models\services\AuthenticatedUser;
use foreup\rest\models\services\FilterQueryParser;
use foreup\rest\models\services\LegacyCartService;
use foreup\rest\resource_transformers\cart_transformer;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\field_transformer;
use foreup\rest\resource_transformers\sales_transformer;
use League\Csv\Writer;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class carts extends api_controller
{
    /** @var EntityManager $repository */
    private $repository;


	/**
	 * @var EntityManager
	 */
	protected $db;

    public function __construct($db,AuthenticatedUser $auth_user,LegacyCartService $cartService)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->cartService = $cartService;
        //$this->repository = $this->db->getRepository('foreup\rest\models\entities\Foreup');
        $this->transformer = new cart_transformer();
        $this->resource_type = "carts";
	    $this->loadEmployeeInformation();
	    $this->cartService->setToken($auth_user->getToken());
    }

    public function getOne(Request $request,$courseId,$cartId)
    {
	    $this->checkCoursePermission($request, $courseId);

	    $this->cartService->setCartId($cartId);
	    $cart = $this->cartService->getCart();

	    return $this->generateJsonResponse($cart);
    }

    public function addPayment(Request $request,$courseId, $cartId)
    {

	    $this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
		    return $this->response;

	    $data = $request->request->get("data");
	    if(!isset($data['attributes']) || !isset($data['attributes']['amount'])  || !isset($data['attributes']['type'])){
		    return $this->respondWithError("Attributes is required. ");
	    }

		$payment = new Payment();
		$payment->setAmount($data['attributes']['amount']);
		$payment->setType($data['attributes']['type']);

		$this->cartService->setCartId($cartId);
	    try{
		    $this->cartService->addPayment($payment);
	    } catch (\Exception $e){
		    return $this->respondWithError("Error with payment. ".$e->getMessage());
	    }

	    $this->cartService->setCartId($cartId);
	    $cart = $this->cartService->getCart();


	    $response = new JsonResponse();
	    $response->setContent($this->serializeResource($cart));
	    return $response;
    }


    public function update(Request $request, $cartId)
    {
	    $this->cartService->setCartId($cartId);
	    try{
		    $saleId = $this->cartService->completeCart();
	    } catch (\Exception $e){
		    return $this->respondWithError("Error with cart. ".$e->getMessage());
	    }

	    $salesRepo = $this->db->getRepository("e:ForeupSales");
	    $sale = $salesRepo->find($saleId);

	    $this->transformer = new sales_transformer();
	    $this->resource_type = "sales";
	    $response = new JsonResponse();
	    $response->setContent($this->serializeResource($sale));
	    return $response;
    }

}
?>