<?php
namespace foreup\rest\controllers\courses;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupItems;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\items_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class items extends api_controller
{
    /** @var EntityRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupItems');
        $this->transformer = new items_transformer();
        $this->resource_type = "items";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId)
    {
	    $this->checkCoursePermission($request, $courseId);

	    $criteria = new Criteria();
	    $criteria->andWhere($criteria->expr()->eq('courseId', $courseId));
	    $criteria->setMaxResults($this->limit);
	    $criteria->setFirstResult($this->start);

	    $results = $this->repository->matching($criteria);
	    $content = $this->serializeResource($results->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }
	public function get(Request $request,$courseId,$itemId)
	{
		$this->checkCoursePermission($request, $courseId);

		$item = $this->repository->find($itemId);

		$content = $this->serializeResource($item);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}


	/**
	 * This only supports Items,
	 * TODO: Implement Passes, Service Fees, Gift Cards
	 *
	 * @param Request $request
	 * @param $courseId
	 * @return JsonResponse
	 */
	public function create(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);


		$newItem = new ForeupItems();
		$newItem->setCourseId($courseId);

		$this->db->persist($newItem);

		$this->fieldsToSkip = ['id','courseId','deleted'];
		$this->updateResource($request, $newItem,false);

		if($newItem->getItemNumber() != ""){
			$existingItem = $this->repository->findOneBy([
				"courseId"=>$courseId,
				"itemNumber"=>$newItem->getItemNumber()
			]);
			if(!empty($existingItem) && $newItem->getItemId() != $existingItem->getItemId()){
				return $this->respondWithError("Item number must be unique");
			}
		}

		$this->db->flush();


		return $this->generateJsonResponse($newItem);
	}
}
?>