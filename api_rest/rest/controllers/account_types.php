<?php
namespace foreup\rest\controllers;

use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use foreup\rest\models\entities\ForeupAccountLedger;
use foreup\rest\models\entities\ForeupAccountPayments;
use foreup\rest\models\entities\ForeupAccounts;
use foreup\rest\models\entities\ForeupAccountTypes;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\services\AccountLedgerService;
use foreup\rest\resource_transformers\account_ledger_transformer;
use foreup\rest\resource_transformers\account_transformer;
use foreup\rest\resource_transformers\account_types_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use League\Fractal\Resource\Item;

class account_types extends api_controller
{
	private $repository;

	/** @var \Doctrine\ORM\EntityManager $db */
	private $db;


	/** @var  \foreup\rest\models\entities\ForeupEmployees  $current_employee*/
	protected $current_employee;

	/**
	 * accounts constructor.
	 * @param \Doctrine\ORM\EntityManager $em
	 * @param $current_user
     */
	public function __construct($em, $current_employee)
	{
		parent::__construct();
		$this->current_employee = $current_employee;
		$this->db = $em;
		$this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupAccountTypes');
		$this->transformer = new account_types_transformer();
		$this->resource_type = "account_types";
	}


	public function create($account_type_id,Request $request)
	{
		$input = $request->request->get("data");
		if(empty($input) || empty($input['attributes'])){
			return $this->respondWithError("Data empty",400);
		}



		$accountType = new ForeupAccountTypes();
		$accountType->setAllowNegative(isset($input['attributes']['allow_negative'])? $input['attributes']['allow_negative']:'');
		$accountType->setDescription(isset($input['attributes']['description']) ? $input['attributes']['description'] : '');
		$accountType->setLimit(isset($input['attributes']['limit'])?$input['attributes']['limit']:'');
		$accountType->setName(isset($input['attributes']['name'])?$input['attributes']['name']:'');
		$accountType->setOrganizationId($this->current_employee->getCourseId());
		$this->db->persist($accountType);
		$this->db->flush();

		$content = $this->serializeResource($accountType);
		$response = new JsonResponse();
		$response->setContent($content);

		return $response;
	}

}