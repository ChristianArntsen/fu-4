<?php
namespace foreup\rest\controllers;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\traits\rollback_trait;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupSales;
use foreup\rest\models\entities\ForeupSalesItems;
use foreup\rest\models\entities\ForeupSalesPayments;
use foreup\rest\models\services\LegacySaleService;
use foreup\rest\resource_transformers\sales_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class sales extends api_controller
{

	/** @var \foreup\rest\models\repositories\CoursesRepository $repository */
	private $repository;

	/**
	 * sales constructor.
	 * @param $db
	 * @param $current_user
	 * @param $auth_user
	 * @param LegacySaleService $legacySaleService
	 */
	public function __construct($db, $current_user, $auth_user, $legacySaleService)
	{
		parent::__construct();

		$this->db = $db;

        $this->current_user = $current_user;
        $this->auth_user = $auth_user;

		$this->repository = $this->db->getRepository('e:ForeupSales');
		$this->transformer = new sales_transformer();
		$this->resource_type = "sales";
		$this->legacySaleService = $legacySaleService;
	}

	public function getAll($id,Request $request)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($id))
			return $this->response;


		/** @var \foreup\rest\models\entities\ForeupCourses $course **/
		$course = $this->db->getRepository("e:ForeupCourses")->find($id);
		date_default_timezone_set($course->getTimezone());



		$startDate = !empty($request->get("startDate"))?Carbon::parse($request->get("startDate")):Carbon::now()->startOfDay();
		$endDate = !empty($request->get("endDate"))?Carbon::parse($request->get("endDate")):Carbon::now()->endOfDay();

		$criteria = new Criteria();
		$criteria->andWhere($criteria->expr()->gte('saleTime', $startDate));
		$criteria->andWhere($criteria->expr()->lte('saleTime', $endDate));
		$criteria->andWhere($criteria->expr()->eq('courseId', $id));
		$criteria->andWhere($criteria->expr()->eq('deleted', false));
		$criteria->setMaxResults($this->limit);
		$criteria->setFirstResult($this->start);

		$results = $this->repository->matching($criteria);
		$response = new JsonResponse();
		$content = $this->serializeResource($results->toArray());
		$response->setContent($content);
		return $response;
	}

	public function getOne($id,Request $request,$saleId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($id))
			return $this->response;


		/** @var ForeupSales $sale */
		$sale = $this->repository->find($saleId);
		if($sale && $sale->getCourseId() != $id){
			return $this->respondWithError("No permission. ",401);
		}



		$content = $this->serializeResource($sale);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	private function getMissingDataPoints($requiredFields,$passedInData)
	{
		$missing = [];
		foreach($requiredFields as $required)
		{
			if(!isset($passedInData[$required])){
				$missing [] = $required;
			}
		}
		return $missing;
	}

	public function create(Request $request,$courseId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;
		$data = $request->request->get('data');
		if(isset($data['attributes'])){
			$data = $data['attributes'];
		} else {
			return $this->respondWithError("Missing attribute field",400);
		}
		if(empty($this->auth_user->getEmpId())){
			return $this->respondWithError("Your token doesn't have access to this endpoint, contact foreUp if this is a mistake.");
		}
		$sale = new ForeupSales();
		isset($data['saleTime']) ? $sale->setSaleTime($data['saleTime']) : "";
		isset($data['comment']) ? $sale->setSaleTime($data['comment']) : "";
		isset($data['total']) ? $sale->setSaleTime($data['total']) : "";
		isset($data['total']) ? $sale->setSaleTime($data['total']) : "";
		isset($data['subtotal']) ? $sale->setSaleTime($data['subtotal']) : "";
		isset($data['tax']) ? $sale->setSaleTime($data['tax']) : "";
		$sale->setCourseId($courseId);
		$sale->setSaleTime(Carbon::now());


		$missingFields = $this->getMissingDataPoints(["employeeId","terminalId","items","payments"],$data);
		if(!empty($missingFields)){
			return $this->respondWithError("The request is missing the following fields: ".implode(",",$missingFields));
		}



		/** @var EntityRepository $employeeRepo */
		$employeeRepo = $this->db->getRepository('e:ForeupEmployees');
		$employee = $employeeRepo->findBy([
			"person"=>$this->db->getReference('e:ForeupPeople',$data['employeeId']),
			"courseId"=>$courseId
		]);
		;
		if(empty($employee)){
			return $this->respondWithError("Unable to find the specified employee.");
		}
		$sale->setEmployee($employee[0]);




		/** @var EntityRepository $terminalRepo */
		$terminalRepo = $this->db->getRepository('e:ForeupTerminals');
		$terminal = $terminalRepo->findBy([
			"id"=>$data['terminalId'],
			"courseId"=>$courseId
		]);
		if(empty($terminal) ){
			return $this->respondWithError("Unable to find the specified terminal.");
		}
		$sale->setTerminal($terminal[0]);




		foreach($data['items'] as $key=> $item){
			if($item['type'] == "sales_items"){
				$saleItem = new ForeupSalesItems();

				if(empty($item['attributes']['itemId'])){
					return $this->respondWithError("Missing required field: itemId");
				}
				/** @var EntityRepository $itemRepo */
				$itemRepo = $this->db->getRepository('e:ForeupItems');
				$itemRecord = $itemRepo->findBy([
					"itemId"=>$item['attributes']['itemId'],
					"courseId"=>$courseId
				]);
				if(empty($itemRecord)){
					return $this->respondWithError("We can't find the itemId: {$item['attributes']['itemId']}");
				}
				//Check if item exists
				isset($item['attributes']['itemId']) ? $saleItem->setItem($itemRecord[0]) : "";
				isset($item['attributes']['total']) ? $saleItem->setTotal($item['attributes']['total']) : "";
				isset($item['attributes']['subTotal']) ? $saleItem->setSubtotal($item['attributes']['subTotal']) : "";
				isset($item['attributes']['tax']) ? $saleItem->setTax($item['attributes']['tax']) : "";
				isset($item['attributes']['quantity']) ? $saleItem->setQuantityPurchased($item['attributes']['quantity']) : "";
				isset($item['attributes']['unitPrice']) ? $saleItem->setItemUnitPrice($item['attributes']['unitPrice']) : "";
				isset($item['attributes']['discountPercent']) ? $saleItem->setDiscountPercent($item['attributes']['discountPercent']) : "";
				isset($item['attributes']['description']) ? $saleItem->setDescription($item['attributes']['description']) : "";
				$saleItem->setLine($key+1);
				$sale->addItems($saleItem);
			} else {
				$type = isset($item['type']) ? $item['type'] : "empty";
				return $this->respondWithError("Invalid item object, expected 'sales_items' but received ".$type);
			}
		}

		foreach($data['payments'] as $payment){
			if(isset($payment['type']) && $payment['type'] == "sales_payments"){
				$salePayment = new ForeupSalesPayments();
				isset($payment['attributes']['paymentAmount']) ? $salePayment->setPaymentAmount($payment['attributes']['paymentAmount']) : "";
				isset($payment['attributes']['invoiceId']) ? $salePayment->setInvoiceId("app_".$payment['attributes']['invoiceId']) : "";
				//isset($payment['attributes']['paymentType']) ? $salePayment->setPaymentType($payment['attributes']['paymentType']) : "";
				$salePayment->setPaymentType(uniqid("app_"));
				isset($payment['attributes']['type']) ? $salePayment->setType($payment['attributes']['type']) : "";
				$sale->addPayments($salePayment);
			} else {
				$type = isset($payment['type']) ? $payment['type'] : "empty";
				return $this->respondWithError("Invalid payment object, expected 'sales_payments' but received `".$type."`");
			}
		}

		if(!$sale->isValid()){
			return $this->respondWithError($sale->getValidationMessage());
		}

		//$this->db->flush();
		$response = $this->legacySaleService->completeSale($sale,$this->auth_user->getToken());
		if(isset($response->success) && !$response->success){
			return $this->respondWithError($response->msg);
		}

		$result = $this->repository->find($response->sale_id);
		$response = new JsonResponse();
		$content = $this->serializeResource($result);
		$response->setContent($content);
		return $response;
	}
}