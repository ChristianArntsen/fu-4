<?php
namespace foreup\rest\controllers\account_recurring;

use Carbon\Carbon;
use foreup\rest\models\entities\ForeupAccountLedger;
use foreup\rest\models\entities\ForeupAccountRecurringChargeItems;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupRepeatable;
use foreup\rest\models\entities\ForeupRepeatableAccountRecurringChargeItems;
use foreup\rest\resource_transformers\account_ledger_transformer;
use foreup\rest\resource_transformers\account_recurring_charge_items_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use foreup\rest\controllers\api_controller;

class accountRecurringChargeItems extends api_controller
{
	/** @var \Doctrine\ORM\EntityRepository $repository */
	private $repository;


	/** @var \Doctrine\ORM\EntityManager $db */
	protected $db;

	protected $current_user;

	protected $auth_user;

	/** @var  \foreup\rest\models\entities\ForeupEmployees */
	private $employee;

	public function __construct($em,$app,$current_user, $auth_user)
	{
		parent::__construct();

		$this->db = $em;
		$this->repository = $this->db->getRepository('e:ForeupAccountRecurringChargeItems');
		$this->current_user = $current_user;
		$this->employee = $app['employee'];
		$this->auth_user = $auth_user;
		$this->transformer = new account_recurring_charge_items_transformer();
		$this->resource_type = "accountRecurringChargeItems";
	}

	public function get($id){
		$response = new JsonResponse();
		$response->setContent('{}');
		return $response;
	}

	// query to get all active charges for a particular date range
	private function buildChargeQuery($inc){
		$includeDeleted = false;
		if(isset($inc['includeDeleted'])&&$inc['includeDeleted'])$includeDeleted = true;

		$qb = $this->db->createQueryBuilder();
		$qb->select('c')
			->from('e:ForeupAccountRecurringCharges','c');

		// recurring charges active during the specified time period
		// defaults to open ended
		$startDate = !empty($inc['startDate'])?Carbon::parse($inc['startDate']):Carbon::now()->startOfDay();
		$endDate = !empty($inc['endDate'])?Carbon::parse($inc['endDate']):Carbon::now()->endOfDay();

		if (isset($startDate)){
			$qb->where($qb->expr()->orX(
				$qb->expr()->isNull('c.endDate'),
				$qb->expr()->gt('c.endDate', "'$startDate'")
			));
		}
		if (isset($endDate)){
			$qb->andWhere($qb->expr()->orX(
				$qb->expr()->isNull('c.startDate'),
				$qb->expr()->lt('c.startDate', "'$endDate'")
			));
		}


		if($includeDeleted){
			$qb->andWhere($qb->expr()->andX(
				$qb->expr()->isNull('c.deletedBy'),
				$qb->expr()->isNull('c.dateDeleted')
			));
		}

		return $qb;
	}

	// query to get all active repeatables for a particular date range
	private function buildRepeatedQuery($inc){

		$qb = $this->db->createQueryBuilder();
		$qb->select('r')
			->from('e:ForeupRepeatableAccountRecurringChargeItems','r');

		// recurring charges active during the specified time period
		// defaults to open-ended
		$startDate = !empty($inc['startDate'])?Carbon::parse($inc['startDate']):null;//Carbon::now()->startOfDay();
		$endDate = !empty($inc['endDate'])?Carbon::parse($inc['endDate']):null;//Carbon::now()->endOfDay();
		if (isset($startDate)){
			$qb->where($qb->expr()->orX(
				$qb->expr()->isNull('r.until'),
				$qb->expr()->gt('r.until', "'$startDate'")
			));
		}
		if (isset($endDate)){
			$qb->andWhere($qb->expr()->orX(
				$qb->expr()->isNull('r.dtstart'),
				$qb->expr()->lt('r.dtstart', "'$endDate'")
			));
		}

		return $qb;
	}

	// get all recurring charge items for a course
	// optionally limiting to a billing period
	public function getAll($courseId, Request $request){
		$this->saveParametersAndDefaults($request);
		$inc = $request->query->all();

		$limit = $request->query->get('limit');;
		$offset = $request->query->get('offset');

		if(!$this->checkAccess($courseId))
			return $this->response;

		$qb = $this->db->createQueryBuilder();
		$qb2 = $this->buildChargeQuery($inc);
		$qb3 = $this->buildRepeatedQuery($inc);

		$qb->select(array('i'))
			->from('e:ForeupAccountRecurringChargeItems','i')
			->where($qb->expr()->in('i.recurringCharge',$qb2->getDql()))
			->andWhere($qb->expr()->in('i.repeated',$qb3->getDql()));

		if(isset($limit)){
			$qb->setMaxResults($limit);
		}
		if(isset($offset)){
			$qb->setFirstResult($offset);
		}

		$sql = $qb->getDQL();
		$q = $qb->getQuery();

		$res = $q->execute();

		$resource = $this->serializeResource($res);
		$response = new JsonResponse();
		$response->setContent($resource);
		return $response;
	}

	public function save(Request $request)
	{
		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		$validate_only = false;
		$test = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;

		if(isset($data['attributes'])){
			$item = $data['attributes'];
		} else {
			return $this->respondWithError("Missing attribute field",400);
		}
		$this->db->getConnection()->beginTransaction();

		$charge_id = null;
		$charge = null;

		if(isset($item['recurring_charge'])){
			if(is_numeric($item['recurring_charge'])){
				$charge_id = $item['recurring_charge'];
			}elseif(isset($item['recurring_charge']['id'])){
				$charge_id = $item['recurring_charge']['id'];
			}
			try {
				$charge = $this->db->find('e:AccountRecurringCharges', $charge_id);
			}catch (\Exception $e){
				$this->db->getConnection()->rollBack();
				return $this->respondWithError($e->getMessage(),500);
			}
		}

		$rc_item = new ForeupAccountRecurringChargeItems();
		$rc_item->setItemId($item['item_id']);
		isset($item['item_type'])?$rc_item->setItemType($item['item_type']):null;
		isset($item['line_number'])?$rc_item->setLineNumber($item['line_number']):null;
		isset($item['override_price'])?$rc_item->setOverridePrice($item['override_price']):null;
		isset($item['discount_percent'])?$rc_item->setDiscountPercent($item['discount_percent']):null;
		isset($item['quantity'])?$rc_item->setQuantity($item['quantity']):null;
		$rc_item->setRecurringCharge($charge);

		$validation = $rc_item->validate();

		if($validation !== true){
			$this->db->getConnection()->rollBack();
			return $this->respondWithError($validation,500);
		}else{
			// persist the entity
			$this->db->persist($rc_item);
			try {
				if(!$validate_only)$this->db->flush();
			}catch (\Exception $e){
				$this->db->getConnection()->rollBack();
				return $this->respondWithError($e->getMessage(),500);
			}
		}
        $repeated_id = null;
		if(isset($item['repeated'])){
			if(is_numeric($item['repeated'])){
				$repeated = $this->db->find('e:ForeupRepeatable',$item['repeated']);
			}
			else {
				$rep = $item['repeated'];
				$repeated = new ForeupRepeatableAccountRecurringChargeItems();
				$repeated->setType('recurring_charge_item');
				$repeated->setResourceId($validate_only ? 1 : $rc_item->getId());

                isset($rep['freq'])?$repeated->setFreq($rep['freq']):null;
                array_key_exists('until', $rep)?$repeated->setUntil($rep['until']):null;
                array_key_exists('interval', $rep)?$repeated->setInterval($rep['interval']):null;
                array_key_exists('count', $rep)?$repeated->setCount($rep['count']):null;
                array_key_exists('dtstart', $rep)?$repeated->setDtstart($rep['dtstart']):null;
                array_key_exists('bymonth', $rep)?$repeated->setBymonth($rep['bymonth']):null;
                array_key_exists('byweekno', $rep)?$repeated->setByweekno($rep['byweekno']):null;
                array_key_exists('byyearday', $rep)?$repeated->setByyearday($rep['byyearday']):null;
                array_key_exists('bymonthday', $rep)?$repeated->setBymonthday($rep['bymonthday']):null;
                array_key_exists('byday', $rep)?$repeated->setByday($rep['byday']):null;
                array_key_exists('wkst', $rep)?$repeated->setWkst($rep['wkst']):null;
                array_key_exists('byhour', $rep)?$repeated->setByhour($rep['byhour']):null;
                array_key_exists('byminute', $rep)?$repeated->setByminute($rep['byminute']):null;
                array_key_exists('bysecond', $rep)?$repeated->setBysecond($rep['bysecond']):null;
                array_key_exists('bysetpos', $rep)?$repeated->setBysetpos($rep['bysetpos']):null;
			}
			$validation = $repeated->validate();

			if ($validation !== true) {
				$this->db->getConnection()->rollBack();
				return $this->respondWithError($validation, 500);
			} else {

                $today = Carbon::now();
                $next = $repeated->calculateNextOccurence($today);
                $repeated->setNextOccurence($next);

			    // persist the entity
				$this->db->persist($repeated);
				$rc_item->setRepeated($repeated);
				$this->db->persist($rc_item);
				try {
					if (!$validate_only) $this->db->flush();
					$items[] = array(
						'type' => 'accountRecurringChargeItems',
						'id' => $rc_item->getId()
					);
				} catch (\Exception $e) {
					$this->db->getConnection()->rollBack();
					return $this->respondWithError($e->getMessage(), 500);
				}
			}

		}
		if(isset($repeated)){
			$repeated_id = $repeated->getId();
		}

		if(!$test)$this->db->getConnection()->commit();
		$response = new JsonResponse();
		$resource = $this->serializeResource($rc_item);
		$resource = json_decode($resource,true);
		$resource['data']['relationships'] = array(
			'repeated' => array(
				'links' => array(
					'related' => '/repeatable/'.($validate_only?1:$charge->getId()).'/accountRecurringChargeItems/'.($validate_only?1:$rc_item->getId()).'/repeatable',
					'data' => isset($repeated_id)?array(
						'type' => 'repeatable',
						'id' => $repeated_id
					):null
				),
			)
		);
		$resource = json_encode($resource);

		$response->setContent($resource);
		return $response;
	}

}