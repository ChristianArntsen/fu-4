<?php
namespace foreup\rest\controllers\account_recurring;

use Doctrine\Common\Collections\Criteria;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupRepeatableAccountRecurringStatements;
use foreup\rest\resource_transformers\account_recurring_statements_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use foreup\rest\controllers\api_controller;
use \Carbon\Carbon;

class accountRecurringStatements extends api_controller
{
	/** @var \Doctrine\ORM\EntityRepository $repository */
	protected $repository;

	/** @var \Doctrine\ORM\EntityManager $db */
	protected $db;

	protected $current_user;

	protected $auth_user;

	/** @var  \foreup\rest\models\entities\ForeupEmployees */
	protected $employee;

	public function __construct($em,$app,$current_user, $auth_user)
	{
		parent::__construct();

		$this->db = $em;
		$this->repository = $this->db->getRepository('e:ForeupAccountRecurringStatements');
		$this->current_user = $current_user;
		$this->employee = $app['employee'];
		$this->auth_user = $auth_user;
		$this->transformer = new account_recurring_statements_transformer();
		$this->resource_type = "accountRecurringStatements";
	}

	public function startTransaction(){
		$this->db->getConnection()->beginTransaction();
	}

	public function rollback(){
		$this->db->getConnection()->rollBack();
	}

	public function get($courseId,$id,Request $request){
		$this->saveParametersAndDefaults($request);
		$includeDeleted = false;
		$inc = $request->query->all();

        if(isset($inc['includeDeleted'])&&$inc['includeDeleted'])$includeDeleted = true;

        if(!$this->checkAccess($courseId))
			return $this->response;
		$org_id = $this->employee->getCourseId();
		//$statement = $this->db->find('e:ForeupAccountRecurringCharges', $id);

        $criteria = new Criteria();
		$criteria->andWhere($criteria->expr()->eq('organizationId',$courseId));
		$criteria->andWhere($criteria->expr()->eq('id',$id));
		if(!$includeDeleted){
			$criteria->andWhere($criteria->expr()->eq('deletedBy', null));
			$criteria->andWhere($criteria->expr()->eq('dateDeleted', null));
		}

		$statement = $this->repository->matching($criteria)->toArray();

		if(is_array($statement)&&count($statement)===1){
			$statement = $statement[0];
		}

		$resource = $this->serializeResource($statement);

		$response = new JsonResponse();
		$response->setContent($resource);

		return $response;
	}

	public function getAll($courseId,Request $request){
		$this->saveParametersAndDefaults($request);
		$inc = $request->query->all();
		$includeDeleted = false;
		$offset=0;
		$limit=null;
		if(isset($inc['includeDeleted'])&&$inc['includeDeleted'])$includeDeleted = true;
		if(isset($inc['limit'])&&is_numeric($inc['limit'])){
			$limit = $inc['limit'];
		}
		if(isset($inc['offset'])&&is_numeric($inc['offset'])){
			$offset = $inc['offset'];
		}

		if(!$this->checkAccess($courseId))
			return $this->response;

		$criteria = new Criteria();
		$criteria->andWhere($criteria->expr()->eq('organizationId',$courseId));
		if(isset($limit)){
			$criteria->setMaxResults($limit);
		}
		if($offset){
			$criteria->setFirstResult($offset);
		}
		if(!$includeDeleted){
			$criteria->andWhere($criteria->expr()->eq('deletedBy', null));
			$criteria->andWhere($criteria->expr()->eq('dateDeleted', null));
		}

        if(isset($inc['isDefault'])){
            $criteria->andWhere($criteria->expr()->eq('isDefault', (bool) $inc['isDefault']));
        }

		$statement = $this->repository->matching($criteria);
		$resource = $this->serializeResource($statement->toArray());

		$response = new JsonResponse();
		$response->setContent($resource);

		return $response;
	}

	public function create($courseId,Request $request){
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;
		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		if(isset($data['id'])||isset($data['attributes']['id'])){
			return $this->respondWithError("Please use PATCH request to update existing recurring charge",400);
		}

		if(isset($data['attributes'])){
			$id = isset($data['id'])?$data['id']:null;
			$data = $data['attributes'];
			if(isset($id))$data['id']=$id;
			elseif(isset($data['id']))$id = $data['id'];
		} else {
			return $this->respondWithError("Missing attribute field",400);
		}
		$person = $this->db->getRepository('e:ForeupPeople')->find($this->employee->getPerson()->getPersonId());
		$criteria = new Criteria();
		$criteria->andWhere($criteria->expr()->eq('person',$person));
		$criteria->andWhere($criteria->expr()->eq('courseId',$courseId));

		$data['created_by'] = $this->db->getRepository('e:ForeupEmployees')->matching($criteria)->toArray();
		if(is_array($data['created_by']) && count($data['created_by'])===1){
			$data['created_by'] = $data['created_by'][0];
		}
		$data['created_by']->setPerson($person);
		$data['date_created'] = new \DateTime('now');

		$statement =  $this->save($courseId,$data, $meta);
		if(!method_exists($statement,'validate'))return $statement;

		$response = new JsonResponse();
		$resource = $this->serializeResource($statement);

		$response->setContent($resource);
		return $response;
	}

	public function put($courseId, $id = null, Request $request){
		if(!$this->checkAccess($courseId))
			return $this->response;
		$content = json_decode($request->getContent(),true);
		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		if(isset($data['id'])){
			$id = $data['id'];
		}
		if(!isset($id)){
			return $this->create($courseId,$request);
		}
		$courseId = (int) $courseId;
		$this->db->getConnection()->beginTransaction();
		$this->saveParametersAndDefaults($request);

		$validate_only = false;
		$test = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;

		if(isset($data['attributes'])){
			$data = $data['attributes'];
		}

		$link = $this->repository->find($id);
		if(!isset($link)){
			return $this->respondWithError('No statement with that ID found',404);
		}

		// reset (most) defaults
		$link->setPaymentTerms(null);
		$link->setDeletedBy(null);
		$link->setDateDeleted(null);
		$link->setIsActive(1);
		$link->setSendEmptyStatements(1);
		$link->setcustomerMessage(null);
		$link->setName(null);
		$link->setFooterText(null);
		$link->setMessageText(null);
		$link->setTermsAndConditionsText(null);
		$link->setIncludeMemberTransactions(0);
		$link->setIncludeCustomerTransactions(0);
		$link->setSendEmail(0);
		$link->setSendMail(0);
		$link->setIncludePastDue(0);
		$link->setDueAfterDays(0);
		$link->setAutopayEnabled(0);
		$link->setAutopayAfterDays(0);
		$link->setAutopayAttempts(0);
		$link->setAutopayOverdueBalance(0);
		$link->setPayMemberBalance(0);
		$link->setPayCustomerBalance(0);
		$link->setSendZeroChargeStatement(0);
		$link->setIsDefault(0);
		if(!$validate_only)$this->db->flush();

		$data['id'] = $id;
		$statement = $this->save($courseId,$data, $meta);
		if(!method_exists($statement,'validate')){
			return $statement;
		}
		if(!$test)$this->db->getConnection()->commit();
		$response = new JsonResponse();
		$resource = $this->serializeResource($statement);

		$response->setContent($resource);
		return $response;
	}

	public function delete($courseId,$id,Request $request){
		$this->db->getConnection()->beginTransaction();
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		$validate_only = false;
		$test = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;

		$statement = $this->getStatement(array('id'=>$id),$courseId);

		// soft delete
		$statement->setDeletedBy($this->employee->getPerson()->getPersonId());
		$statement->setDateDeleted(new \DateTime());

		if(!$validate_only)$this->db->flush();
		if(!$test)$this->db->getConnection()->commit();

		$response = new JsonResponse();

		$response->setContent('{"data":{"success":true,"content":"accountRecurringStatements instance deleted"}}');
		return $response;
	}

	public function snake_case_begone($data){
		$ret = [];
		foreach ($data as $key=>$value){
			$exp = explode('_',$key);
			if(count($exp)===1){
				$ret[$key]=$value;
				continue;
			}
			$new = '';
			for($i=1;$i<count($exp);$i++){
				$new .= ucfirst($exp[$i]);
			}
			$ret[$new]=$value;
		}

		return $ret;
	}



	public function getStatement($data,$courseId){
		if(is_numeric($data)&&is_int($data*1)){
			$statement = $this->repository->find($data);
			if(!isset($statement)){
				return $this->respondWithError('Statement entity not found with id: '.$data,404);
			}
			return $statement;
		}
		elseif(isset($data['id'])){
			$statement = $this->repository->find($data['id']);
			if(!isset($statement)){
				return $this->respondWithError('Statement entity not found with id: '.$data['id'],404);
			}
		}else {
			$statement = new ForeupAccountRecurringStatements();
		}
		if(isset($data['attributes'])){
			$data = $data['attributes'];
		}
		$terms_id = null;
		if(isset($data['payment_terms'])){
			if(is_numeric($data['payment_terms']) && is_int($data['payment_terms']*1)){
				$terms_id = $data['payment_terms'];
			}elseif(isset($data['payment_terms']['id'])){
				$terms_id = $data['payment_terms']['id'];
			}else{
				$this->db->getConnection()->rollBack();
				return $this->respondWithError('payment_terms must be integer id or payment_terms object',500);
			}

			try {
				$terms = $this->db->find('e:ForeupAccountPaymentTerms', $terms_id);
			}catch (\Exception $e){
				$this->db->getConnection()->rollBack();
				return $this->respondWithError($e->getMessage(),500);
			}
		}
		isset($data['name'])?$statement->setName($data['name']):null;
		isset($data['customerMessage'])?$statement->setCustomerMessage($data['customerMessage']):null;
		//isset($data['date_created'])?$statement->setDateCreated(new \DateTime($data['date_created'])):null;

		$statement->setOrganizationId($courseId);
		if(!isset($data['id'])) {
			$statement->setCreatedBy($data['created_by']);
			$statement->setDateCreated(new \DateTime());
		}

		$statement->setIsDefault(0);
		$statement->setFinanceChargeEnabled(0);
		$statement->setSendZeroChargeStatement(0);
		$statement->setFinanceChargeAmount(0.00);
		$statement->setFinanceChargeType('percent');
		$statement->setFinanceChargeAfterDays(0);

		isset($data['is_active'])?$statement->setIsActive($data['is_active']):null;
		isset($data['send_empty_statements'])?$statement->setSendEmptyStatements($data['send_empty_statements']):null;
		isset($data['include_member_transactions'])?$statement->setIncludeMemberTransactions($data['include_member_transactions']):null;
		isset($data['include_customer_transactions'])?$statement->setIncludeCustomerTransactions($data['include_customer_transactions']):null;
		isset($data['send_email'])?$statement->setSendEmail($data['send_email']):null;
		isset($data['send_mail'])?$statement->setSendMail($data['send_mail']):null;
		isset($data['include_past_due'])?$statement->setIncludePastDue($data['include_past_due']):null;
		isset($data['due_after_days'])?$statement->setDueAfterDays($data['due_after_days']):null;
		isset($data['autopay_enabled'])?$statement->setAutopayEnabled($data['autopay_enabled']):null;
		isset($data['autopay_after_days'])?$statement->setAutopayAfterDays($data['autopay_after_days']):null;
		isset($data['autopay_attempts'])?$statement->setAutopayAttempts($data['autopay_attempts']):null;
		isset($data['autopay_overdue_balance'])?$statement->setAutopayOverdueBalance($data['autopay_overdue_balance']):null;
		isset($data['pay_member_balance'])?$statement->setPayMemberBalance($data['pay_member_balance']):null;
		isset($data['pay_customer_balance'])?$statement->setPayCustomerBalance($data['pay_customer_balance']):null;
		isset($data['footer_text'])?$statement->setFooterText($data['footer_text']):null;
		isset($data['message_text'])?$statement->setMessageText($data['message_text']):null;
		isset($data['terms_and_conditions_text'])?$statement->setTermsAndConditionsText($data['terms_and_conditions_text']):null;

		isset($data['is_default'])?$statement->setIsDefault($data['is_default']): null;
		isset($data['send_zero_charge_statement'])?$statement->setSendZeroChargeStatement($data['send_zero_charge_statement']): null;
		isset($data['finance_charge_enabled'])?$statement->setFinanceChargeEnabled($data['finance_charge_enabled']):null;
		isset($data['finance_charge_amount'])?$statement->setFinanceChargeAmount($data['finance_charge_amount']):null;
		isset($data['finance_charge_type'])?$statement->setFinanceChargeType($data['finance_charge_type']):null;
		isset($data['finance_charge_after_days'])?$statement->setFinanceChargeAfterDays($data['finance_charge_after_days']):null;

		isset($data['isActive'])?$statement->setIsActive($data['isActive']):null;
		isset($data['sendEmptyStatements'])?$statement->setSendEmptyStatements($data['sendEmptyStatements']):null;
		isset($data['includeMemberTransactions'])?$statement->setIncludeMemberTransactions($data['includeMemberTransactions']):null;
		isset($data['includeCustomerTransactions'])?$statement->setIncludeCustomerTransactions($data['includeCustomerTransactions']):null;
		isset($data['sendEmail'])?$statement->setSendEmail($data['sendEmail']):null;
		isset($data['sendMail'])?$statement->setSendMail($data['sendMail']):null;
		isset($data['includePastDue'])?$statement->setIncludePastDue($data['includePastDue']):null;
		isset($data['dueAfterDays'])?$statement->setDueAfterDays($data['dueAfterDays']):null;
		isset($data['autopayEnabled'])?$statement->setAutopayEnabled($data['autopayEnabled']):null;
		isset($data['autopayAfterDays'])?$statement->setAutopayAfterDays($data['autopayAfterDays']):null;
		isset($data['autopayAttempts'])?$statement->setAutopayAttempts($data['autopayAttempts']):null;
		isset($data['autopayOverdueBalance'])?$statement->setAutopayOverdueBalance($data['autopayOverdueBalance']):null;
		isset($data['payMemberBalance'])?$statement->setPayMemberBalance($data['payMemberBalance']):null;
		isset($data['payCustomerBalance'])?$statement->setPayCustomerBalance($data['payCustomerBalance']):null;
		isset($data['footerText'])?$statement->setFooterText($data['footerText']):null;
		isset($data['messageText'])?$statement->setMessageText($data['messageText']):null;
		isset($data['termsAndConditionsText'])?$statement->setTermsAndConditionsText($data['termsAndConditionsText']):null;

		isset($data['isDefault'])?$statement->setIsDefault($data['isDefault']): null;
		isset($data['sendZeroChargeStatement'])&&!isset($data['send_zero_charge_statement'])?$statement->setSendZeroChargeStatement($data['sendZeroChargeStatement']): null;
		isset($data['financeChargeEnabled'])?$statement->setFinanceChargeEnabled($data['financeChargeEnabled']):null;
		isset($data['financeChargeAmount'])?$statement->setFinanceChargeAmount($data['financeChargeAmount']):null;
		isset($data['financeChargeType'])?$statement->setFinanceChargeType($data['financeChargeType']):null;
		isset($data['financeChargeAfterDays'])?$statement->setFinanceChargeAfterDays($data['financeChargeAfterDays']):null;

		return $statement;
	}

	private function getRepeated(ForeupAccountRecurringStatements &$statement,$rep){
		if(isset($rep['id'])){
			$repeated = $this->db->getRepository('e:ForeupRepeatableAccountRecurringStatements')->find($rep['id']);
			if(!isset($repeated)){
				return $this->respondWithError('Repeatable entity not found with id: '.$rep['id'],404);

			}
			if(empty($rep['dtstart'])){
				$rep['dtstart'] = new \DateTime();
			}
		}elseif($statement->getRepeated()){
			$repeated = $statement->getRepeated();
		}else {
			$repeated = new ForeupRepeatableAccountRecurringStatements();
		}
		$repeated->setStatement($statement);

		if(isset($rep['attributes'])){
			$rep = $rep['attributes'];
		}

		isset($rep['freq'])?$repeated->setFreq($rep['freq']):null;
        array_key_exists('until', $rep)?$repeated->setUntil($rep['until']):null;
        array_key_exists('interval', $rep)?$repeated->setInterval($rep['interval']):null;
        array_key_exists('count', $rep)?$repeated->setCount($rep['count']):null;
        array_key_exists('dtstart', $rep)?$repeated->setDtstart($rep['dtstart']):null;
        array_key_exists('bymonth', $rep)?$repeated->setBymonth($rep['bymonth']):null;
        array_key_exists('byweekno', $rep)?$repeated->setByweekno($rep['byweekno']):null;
        array_key_exists('byyearday', $rep)?$repeated->setByyearday($rep['byyearday']):null;
        array_key_exists('bymonthday', $rep)?$repeated->setBymonthday($rep['bymonthday']):null;
        array_key_exists('byday', $rep)?$repeated->setByday($rep['byday']):null;
        array_key_exists('wkst', $rep)?$repeated->setWkst($rep['wkst']):null;
        array_key_exists('byhour', $rep)?$repeated->setByhour($rep['byhour']):null;
        array_key_exists('byminute', $rep)?$repeated->setByminute($rep['byminute']):null;
        array_key_exists('bysecond', $rep)?$repeated->setBysecond($rep['bysecond']):null;
        array_key_exists('bysetpos', $rep)?$repeated->setBysetpos($rep['bysetpos']):null;

		return $repeated;
	}

	public function save($courseId, $data, $meta)
	{
		$validate_only = false;
		$test = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;
		$this->db->getConnection()->beginTransaction();

		// get and save the recurring charge

		$statement = $this->getStatement($data,$courseId);
		if(!method_exists($statement,'getIsDefault'))return $statement;
		if($statement->getIsDefault()){
			// there can be only one...
			$repo = $this->db->getRepository('e:ForeupAccountRecurringStatements');
			$org_rec_statements = $repo->findBy([
				'organizationId'=>$statement->getOrganizationId()
			]);
			foreach ($org_rec_statements as &$org_statement){
				if(empty($data['id']) || $org_statement->getId() != $data['id']){
                    $org_statement->setIsDefault(false);
                }
			}
			if(!$validate_only)$this->db->flush($org_rec_statements);
		}

		if(!method_exists($statement,'validate')){
			return $statement;
		}

		$validation = $statement->validate(false);

		if($validation !== true){
			$this->db->getConnection()->rollBack();
			return $this->respondWithError($validation,500);
		}else{
			//persist the entity
			if(!isset($data['id'])) {
				$this->db->persist($statement);
			}
		}
		if(!$validate_only)$this->db->flush();
		if(isset($data['attributes']))$data=$data['attributes'];

        if(isset($data['repeated'])){
			$repeated = $this->getRepeated($statement,$data['repeated']);
			if(!method_exists($repeated,'validate'))return $repeated;
			if($meta['validate_only'])$repeated->setResourceId(1);
			$validation = $repeated->validate(false);

			if($validation !== true){
				$this->db->getConnection()->rollBack();
				return $this->respondWithError($validation,500);
			}else{
                $today = Carbon::now();
                $next = $repeated->calculateNextOccurence($today);
                $repeated->setNextOccurence($next);
                $this->db->persist($repeated);
				$statement->setRepeated($repeated);
			}
		}

		if(!$validate_only)$this->db->flush();
		if(!$test)$this->db->getConnection()->commit();

		return $statement;
	}
}