<?php
namespace foreup\rest\controllers;

use foreup\rest\models\entities\ForeupApiApplications;
use foreup\rest\models\entities\ForeupApiUserCourses;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupPermissions;
use foreup\rest\models\entities\ForeupUsers;
use foreup\rest\models\entities\Token;
use foreup\rest\models\services\AuthenticatedUser;
use foreup\rest\resource_transformers\token_transformer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class tokens extends api_controller
{
	/** @var \foreup\auth\json_web_token $jwt */
	private $jwt;
	/** @var \foreup\rest\models\repositories\UsersRepository */
	private $repository;

	/** @var  AuthenticatedUser $user */
	protected $auth_user;
	/**
	 * tokens constructor.
	 * @param $db
	 * @param \foreup\auth\json_web_token $jwt
	 */
	public function __construct($db, $jwt, $user = null)
	{
		parent::__construct();
		$this->jwt = $jwt;
		$this->db = $db;
		$this->auth_user = $user;
		$this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupUsers');
		$this->resource_type = "token";
		$this->transformer = new token_transformer();
	}

	public function validate(Request $request,$token)
	{
		//Authenticate username/password
		/** @var ForeupUsers $user */

		$permissionsRepository = $this->db->getRepository('e:ForeupApiUserCourses');

		/** @var ForeupApiUserCourses $permission */
		$permission = $permissionsRepository->findOneBy([
			'userId'=>$this->auth_user->getAppId(),
			'course'=>$this->auth_user->getCid()
		]);

		$valid = false;
		try {
			$this->jwt->setToken($token);
			$this->jwt->setAudience($permission->getModuleUrl());
			$valid = $this->jwt->validate();
		} catch (\Exception $e){
			$valid = false;
		}

		//Get Audience from current user


		$response =  new JsonResponse();
		$response->setData([
			"valid"=>$valid
		]);
		return $response;
	}

	public function save(Request $request)
	{
		//Authenticate username/password
		$user = $this->repository->get_user_from_login(
			$request->request->get('email'),
			$request->request->get('password')
		);
		if(!isset($user)){
			//Check if credentials are an employee
			/** @var \foreup\rest\models\repositories\EmployeesRepository $employeeRepo */
			$employeeRepo = $this->db->getRepository('foreup\rest\models\entities\ForeupEmployees');
			$employee = $employeeRepo->get_employee_from_login(
					$request->request->get('email'),
					$request->request->get('password')
			);
			if(!isset($employee)){
				return \foreup\rest\middleware\authentication::notAuthorized();
			}
		}

		// Set Private Claims
		$this->jwt->addPrivateClaim('level', 0);
		$this->jwt->addPrivateClaim('cid', 0);
		$this->jwt->addPrivateClaim('employee', false);
		if(isset($employee)){
			$this->onfigureEmployeeUser($employee);
		} else {
			$this->configureApiUser($user);
		}
		$this->addLimitationsToToken($user);


		$this->jwt->createToken();
		$token = new Token();
		$token->setToken($this->jwt->getToken());
		$this->jwt->createToken();

		$content = $this->serializeResource($token);
		$response = new JsonResponse();
		$response->setContent($content);

		return $response;

	}

	/**
	 * @param ForeupUsers $user
	 */
	private function addLimitationsToToken($user)
	{
		if(empty($user)){
			return;
		}
		//If user has an associated api_application then we'll add the limitations to the token.
		$applicationRepository = $this->db->getRepository('e:ForeupApiApplications');
		/** @var ForeupApiApplications $applicationPermission */
		$applicationPermission = $applicationRepository->findOneBy(['user' => $user->getId()]);
		if ($applicationPermission) {
			$this->jwt->addPrivateClaim('limitations', $applicationPermission->getLimitationArray());
		}
	}

	/**
	 * @param $access
	 */
	private function generateApiEmployee($access)
	{
		$person = new ForeupPeople();
		$person->setFirstName($access->getActualUser()->getFirstName());
		$person->setLastName($access->getActualUser()->getLastName());
		$person->setEmail($access->getActualUser()->getEmail());
		$this->db->persist($person);
		$this->db->flush();

		$newEmployee = new ForeupEmployees();
		$newEmployee->setCourse($access->getCourse());
		$newEmployee->setUserLevel(1);
		$newEmployee->setPassword(uniqid());
		$newEmployee->setUsername($access->getActualUser()->getEmail() . "+" . $access->getCourse()->getCourseId());
		$newEmployee->setUserLevel(5);


		$newEmployee->setPerson($person);
		$access->setEmployee($newEmployee);
		$this->db->persist($newEmployee);
		$permission = new ForeupPermissions($this->db->getReference('e:ForeupModules','sales'), $person);
		$newEmployee->addPermissions($permission);
		$this->db->persist($permission);
		$permission = new ForeupPermissions($this->db->getReference('e:ForeupModules','sales_v2'), $person);
		$newEmployee->addPermissions($permission);
		$this->db->persist($permission);
		$permission = new ForeupPermissions($this->db->getReference('e:ForeupModules','teesheets'), $person);
		$newEmployee->addPermissions($permission);
		$this->db->persist($permission);


		$this->db->flush();
	}

	/**
	 * @param $user
	 */
	private function configureApiUser($user)
	{
		$this->jwt->addPrivateClaim('uid', $user->getPerson()->getPersonId());
		$this->jwt->addPrivateClaim('apiVersion', $user->getApiVersion());

		$permissionsRepository = $this->db->getRepository('e:ForeupApiUserCourses');
		/** @var ForeupApiUserCourses[] $permission */
		$permission = $permissionsRepository->findBy(['userId' => $user->getPerson()->getPersonId()]);
		foreach ($permission as $access) {
			//All legacy apps should require this
			//Check if an employee is assigned to the connection
			if (!$access->getEmployee()) {
				$this->generateApiEmployee($access);
			}
		}

		if (!empty($permission) && !empty($permission[0]->getEmployee())) {
			$employee = $permission[0]->getEmployee();
			$this->jwt->addPrivateClaim('level', $employee->getUserLevel());
			$this->jwt->addPrivateClaim('uid', $employee->getPerson()->getPersonId());
			$this->jwt->addPrivateClaim('appId', $user->getPerson()->getPersonId());
			$this->jwt->addPrivateClaim('cid', $employee->getCourseId());
			$this->jwt->addPrivateClaim('priceClassId', $permission[0]->getPriceClassId());
		}
	}

	/**
	 * @param ForeupEmployees $employee
	 */
	private function onfigureEmployeeUser($employee)
	{
		$this->jwt->addPrivateClaim('employee', true);
		$this->jwt->addPrivateClaim('level', $employee->getUserLevel());
		$this->jwt->addPrivateClaim('uid', $employee->getPerson()->getPersonId());
		//$this->jwt->addPrivateClaim('teesheet_ids', $employee->getCourse()->getGroupSharedTeesheetIds());
	}

}