<?php
namespace foreup\rest\controllers;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Tools\DisconnectedClassMetadataFactory;
use foreup\rest\controllers\traits\rollback_trait;
use foreup\rest\models\entities\ForeupApiUserCourses;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupMobileTokens;
use foreup\rest\resource_transformers\course_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class courses extends api_controller
{
	use rollback_trait;

    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
	private $repository;


	public function __construct($db,$current_user,$auth_user)
	{
		parent::__construct();

		$this->db = $db;
        $this->current_user = $current_user;
        $this->auth_user = $auth_user;

		$this->repository = $this->db->getRepository('e:ForeupCourses');
		$this->transformer = new course_transformer();
		$this->resource_type = "courses";
	}

	public function get($id, Request $request)
	{
		$this->request = $request;

		/**  @var \foreup\rest\models\entities\ForeupCourses $course */
		$course = $this->repository->find($id);
		$content = $this->serializeResource($course);
		$response = new JsonResponse();
		$response->setContent($content);
		$response->headers->add([
			"last-modified"=>$course->getMobileAppRefreshTimestamp()
		]);
		return $response;
	}

    public function reallyGetAll(Request $request)
    {
        $uzr = $this->auth_user;
        if($uzr->getLevel()*1!==5)return $this->respondWithError("Must be super admin ".$uzr['level'],400);

        $courses = $this->repository->get_all();

        $content = $this->serializeResource($courses);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function getSuggestions(Request $request)
    {
        $uzr = $this->auth_user;
        if($uzr->getLevel()*1!==5)return $this->respondWithError("Must be super admin ",400);
        $search = $request->query->get('search');
        $limit = $request->query->get('limit');
        $limit = $limit ? $limit*1 : 25;
        $search = $search?$search:'';
        $courses = $this->repository->get_suggestions($search, $limit);

        $content = $this->serializeResource($courses);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

	public function getAll(Request $request)
	{
		$this->saveParametersAndDefaults($request);

		$permissionsRepository = $this->db->getRepository('e:ForeupApiUserCourses');
		/** @var ForeupApiUserCourses[] $courseAccess */
		$courseAccess = $permissionsRepository->findBy(['userId'=>$this->auth_user->getAppId()]);
		foreach($courseAccess as $access)
		{
			$courses[]=$access->getCourse();
		}
		if(empty($courses)){
			$courses = [];
		}
		$content = $this->serializeResource($courses);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	public function patch($id,Request $request)
	{
		/** @var \foreup\rest\models\entities\ForeupCourses $data */
		$course_object = $this->repository->find($id);
		$data = $request->request->get("data");
		$response = new JsonResponse();
		if(isset($data['attributes'])){
			if(isset($data['attributes']['published_ios_version'])){
				$course_object->setMobileAppIosPublishedVersion($data['attributes']['published_ios_version']);
			}
            if(isset($data['attributes']['published_android_version'])){
                $course_object->setMobileAppAndroidPublishedVersion($data['attributes']['published_android_version']);
            }
			$this->db->flush();
		}




		$content = $this->serializeResource($course_object);
		$response->setContent($content);
		return $response;
	}


	public function mobileToken($id,Request $request)
	{
		$data = $request->request->get("data");
		if(!isset($data['attributes']) || !isset($data['attributes']['token']) || !isset($data['attributes']['platform'])){
			$json_response = new JsonResponse();
			$json_response->setData(array(
				'title'=>"Missing Fields",
				'detail'=>'Requires both token and platform to save a mobile token.'
			));
			$json_response->setStatusCode(400);
			return $json_response;
		}



		/**  @var \foreup\rest\models\entities\ForeupCourses $course */
		$course = $this->repository->find($id);


		$token = new ForeupMobileTokens();
		$token->setCourse($course);
		$token->setToken($data['attributes']['token']);
		$token->setPlatform($data['attributes']['platform']);


		$course->addMobileTokens($token);

		$this->db->persist($token);
		$this->db->flush();

		$response = new JsonResponse();
		$content = $this->serializeResource($course);
		$response->setContent($content);
		return $response;
	}
}