<?php
namespace foreup\rest\controllers;

use Doctrine\Common\Collections\Criteria;
use foreup\rest\resource_transformers\marketing_campaign_transformer;

use Carbon\Carbon;
use Doctrine\ORM\Tools\DisconnectedClassMetadataFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class marketing_campaigns extends api_controller
{
	/** @var \Doctrine\ORM\EntityRepository $repository */
	private $repository;

	public function __construct($db,$current_user,$auth_user)
	{
		parent::__construct();

        $this->current_user = $current_user;
        $this->auth_user = $auth_user;
		$this->db = $db;
		$this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupMarketingCampaigns');
		$this->transformer = new marketing_campaign_transformer();
		$this->resource_type = "marketing_campaigns";
	}

	public function get($id)
	{
		$course = $this->repository->find($id);
		$content = $this->serializeResource($course);
		return $content;
	}

    public function getAll(Request $request)
    {
		$limit = (int)$request->query->get('limit',25);
		$offset = (int)$request->query->get('offset',0);

        $user = $this->auth_user;
		if($user['level'] == 5) {
			$entries = $this->repository->findBy(array(),null,$limit,$offset);
		} else {
			$entries = $this->repository->findBy([
				"course"=>$user['cid']
			],null,$limit,$offset);
		}


        $content = $this->serializeResource($entries);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

	public function delete()
	{

	}

	public function save()
	{

	}

	public function update($id)
	{

	}

	public function promoteToSent($id)
	{
		$uzr = $this->auth_user;
		if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin",400);
		$ret = $entries = $this->repository->unstick($id,1);

		$response = new JsonResponse();
		$response->setContent(json_encode($ret));
		$this->db->flush();
		return $response;
	}

	public function demoteToDraft($id)
	{
		$uzr = $this->auth_user;
		if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin",400);
		$ret = $entries = $this->repository->unstick($id,0);

		$response = new JsonResponse();
		$response->setContent(json_encode($ret));
		return $response;
	}

}