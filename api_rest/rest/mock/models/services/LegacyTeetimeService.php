<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 9/23/2016
 * Time: 3:36 PM
 */

namespace foreup\rest\mock\models\services;


use Carbon\Carbon;
use foreup\rest\models\entities\ForeupTeetime;
use foreup\rest\models\entities\LegacyObjects\TeetimeSlot;
use GuzzleHttp\Client;

class LegacyTeetimeService extends \foreup\rest\models\services\LegacyTeetimeService
{

	private $timezone,$date,$startTime,$endTime,$holes;

	public function __construct($url)
	{
		$this->timezone = "America/Denver";
		$this->client = new Client(['base_uri' => $url.'/index.php/']);
		$this->headers = $headers = [
			'x-authorization' => 'Bearer ',
			"Api_key"=>"no_limits",
			"Content-Type"=>"application/json"
		];
		$this->date = Carbon::now()->toDateString();
		$this->startTime = 0000;
		$this->endTime = 2359;
		$this->holes = 18;
	}

	public function setToken($token)
	{
		$this->headers['x-authorization'] = "Bearer ".$token;
	}

	/**
	 * @return mixed
	 */
	public function getTimezone()
	{
		return $this->timezone;
	}

	/**
	 * @param mixed $timezone
	 */
	public function setTimezone($timezone)
	{
		$this->timezone = $timezone;
	}

	public function bookTime(ForeupTeetime $booking,$ttid = "")
	{
		$url = '/teesheets/save_simple';
		if(!empty($ttid)){
			$url .= "/".$ttid;
		}
		$headers = $this->headers;
		$headers['Content-Type'] = "application/x-www-form-urlencoded";
		$response = $this->client->request('POST', $url, [
			"headers"=>$headers,
			'form_params' => [
				"start"=>$booking->getStart(),
				"side"=>$booking->getSide(),
				"event_type"=>"teetime",
				"teetime_details"=>$booking->getDetails(),
				"teetime_holes"=>$booking->getHoles(),
				"players"=>$booking->getPlayerCount(),
				"carts"=>$booking->getCarts(),
				"teetime_title"=>$booking->getTitle(),
				"teesheet_id"=>$booking->getTeesheetId(),
				"booking_source"=>"api",
			]
		]);
		$responseBody = json_decode($response->getBody()->getContents(),true);
		if($responseBody['success']){
			return $responseBody['data']['teetime_id'];
		} else {
			if(isset($responseBody['msg'])){
				throw new \Exception($responseBody['msg']);
			} else {
				throw new \Exception("Tee time slot isn't available.");
			}
		}

		return false;
	}

	public function getTimes($token,$scheduleId)
	{
		$timezone =  $this->timezone;
		$this->setToken($token);
			$response = $this->client->request('GET', '/index.php/api/booking/times_advanced', [
				"headers"=>$this->headers,
				"query"=>[
					"date"=>$this->date,
					"start_time"=>$this->startTime,
					"end_time"=>$this->endTime,
					"holes"=>$this->holes,
					"schedule_id"=>$scheduleId,
					"api_key"=>"no_limits"
				]
			]);



		$responseBody = json_decode($response->getBody()->getContents());
		$teetimes = [];
		foreach($responseBody as $teetimeSlot)
		{
			$teetimeSlot->timezone = $timezone;
			$teetimes[] = new TeetimeSlot($teetimeSlot);
		}


		return $teetimes;
	}

	/**
	 * @return string
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param string $date
	 */
	public function setDate($date)
	{
		$date = Carbon::parse($date);
		$this->date = $date->toDateString();
	}

	/**
	 * @return int
	 */
	public function getStartTime()
	{
		return $this->startTime;
	}

	/**
	 * @param int $startTime
	 */
	public function setStartTime($startTime)
	{
		if($startTime < 0 || $startTime > 2359){
			throw new \Exception("Invalid format for startTime, must be between 0000 and 2359");
		}

		$this->startTime = $startTime;
	}

	/**
	 * @return int
	 */
	public function getEndTime()
	{
		return $this->endTime;
	}

	/**
	 * @param int $endTime
	 */
	public function setEndTime($endTime)
	{
		if($endTime < 0 || $endTime > 2359){
			throw new \Exception("Invalid format for endTime, must be between 0000 and 2359");
		}
		$this->endTime = $endTime;
	}

	/**
	 * @return int
	 */
	public function getHoles()
	{
		return $this->holes;
	}

	/**
	 * @param int $holes
	 */
	public function setHoles($holes)
	{
		$this->holes = $holes;
	}


}