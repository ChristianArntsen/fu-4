<?php

namespace foreup\rest\t\Helpers;

use foreup\rest\application\services_loader;

class testing_services_loader extends services_loader {
    // allow non Application to be passed in for testing
    public function __construct(&$app)
    {
        $this->app = $app;
    }
}