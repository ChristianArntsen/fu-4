<?php

namespace foreup\rest\t\Helpers;

use foreup\rest\application\config;

class testing_config extends config {
    // allow non Application to be passed in for testing
    public function __construct(&$app)
    {
        $this->app = $app;
    }
}