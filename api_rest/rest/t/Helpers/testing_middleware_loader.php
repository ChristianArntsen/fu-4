<?php

namespace foreup\rest\t\Helpers;

use foreup\rest\application\middleware_loader;

class testing_middleware_loader extends middleware_loader {
    // allow non Application to be passed in for testing
    public function __construct(&$app)
    {
        $this->app = $app;
    }
}