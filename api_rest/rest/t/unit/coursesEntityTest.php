<?php

namespace foreup\rest\unit;

use Dflydev\Pimple\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\unit\EntityTestingUtility;
use Silex\Application;
use foreup\rest\t\Helpers\testing_uzr;
use foreup\rest\t\Helpers\testing_middleware_loader;
use foreup\rest\t\Helpers\testing_config;
use foreup\rest\t\Helpers\testing_services_loader;
use foreup\rest\t\Helpers\testing_routes_loader;


class testCoursesEntity extends EntityTestingUtility
{
	private $app, $middleware_loader, $configuration, $routes_loader, $services_loader;

	protected function createMockDefaultAppAndDeps()
	{
		$app = new Application();

		$eventManager = $this->getMock('Doctrine\Common\EventManager');

		$connection = $this
			->getMockBuilder('Doctrine\DBAL\Connection')
			->disableOriginalConstructor()
			->getMock();

		$connection
			->expects($this->any())
			->method('getEventManager')
			->will($this->returnValue($eventManager));

		$app['user'] = 12;
		$app['auth.userObj'] = new testing_uzr(5);
		$app['auth.user'] = array(
			'loggedIn' => true,
			'validated' => true,
			'authorized' => true,
			'uid' => 12,
			'cid' => 6270,
			'level' => (int)5,
			'emp_id' => 12,
			'isEmployee' => 1
		);

		return array($app, $connection, $eventManager);
	}

	protected function createMockDefaultApp()
	{
		list ($app, $connection, $eventManager) = $this->createMockDefaultAppAndDeps();

		return $app;
	}

	protected function _before()
	{
		$this->app = $this->createMockDefaultApp();
		$doctrineOrmServiceProvider = new DoctrineOrmServiceProvider();
		$doctrineOrmServiceProvider->register($this->app);
		$this->configuration = new testing_config($this->app);
		$this->configuration->loadConfig();
		$this->middleware_loader = new testing_middleware_loader($this->app);
		$this->middleware_loader->load_middleware();
		$this->services_loader = new testing_services_loader($this->app);
		$this->services_loader->register_services();
		$this->routes_loader = new testing_routes_loader($this->app);
		$this->routes_loader->bindRoutesToControllers();
		$this->course = new ForeupCourses();
	}

	protected function _after()
	{
		$conn = $this->app['orm.em']->getConnection();
		$conn->close();
	}

	public function testInterface(){
		$cou = true;
		$em = $this->app['orm.em'];
		$db = $this->app['courses.controller']->db;
		$repository = $db->getRepository('e:ForeupCourses');
		$course = new ForeupCourses();

		$this->assertTrue(method_exists($course,'__construct'));
		$this->assertTrue(method_exists($course,'getMyTeeSheets'));
		$this->assertTrue(method_exists($course,'getMyModules'));
		$this->assertTrue(method_exists($course,'setActive'));
		$this->assertFalse(method_exists($course,'goPostal'));
		$this->assertTrue(method_exists($course,'getPerson'));
		$this->assertTrue(method_exists($course,'getGroupMemberIds'));

		$verbs = ['get','set'];
		$nouns = ['Address','Modules','CourseId','MobileAppRefreshTimestamp','MobileAppAndroidPublishedVersion',
			'MobileAppIosPublishedVersion','CourseSummary','MobileAppSummary','MobileAppShortTitle',
			'MobileTestFlightEmail','MobileAppIconUrl','BaseColor','AreaId','Cid','CourseAppDataId','Name',
			'Address','City','State','StateName','Postal','Woeid','Zip','Country','AreaCode','Timezone','Dst',
			'LatitudeCentroid','LatitudePoly','LongitudeCentroid','LongitudePoly','Phone','Holes','Type','County',
			'OpenTime','CloseTime','Increment','EarlyBirdHoursBegin','EarlyBirdHoursEnd','MorningHoursBegin',
			'MorningHoursEnd','AfternoonHoursBegin','AfternoonHoursEnd','TwilightHour','SuperTwilightHour',
			'Holidays','OnlineBooking','OnlineBookingProtected','MinRequiredPlayers','MinRequiredHoles',
			'BookingRules','Config','Courses','Customers','Dashboards','Employees', 'Giftcards','ItemKits',
			'Items','MarketingCampaigns','Promotions','Receivings','Reservations','Suppliers','Frontnine',
			'Teesheets','CurrencySymbol','CurrencySymbolPlacement','CurrencyFormat','CompanyLogo',
			'DateFormat','DefaultTax1Name','DefaultTax1Rate','DefaultTax2Cumulative','DefaultTax2Name',
			'DefaultTax2Rate','CustomerCreditNickname','CreditDepartment','CreditDepartmentName',
			'CreditCategory','CreditCategoryName','CreditLimitToSubtotal','MemberBalanceNickname','Email',
			'ReservationEmail','BillingEmail','LastInvoicesSent','Fax','Language','MailchimpApiKey','NumberOfItemsPerPage',
			'WebprntIp','WebprntHotIp','WebprntColdIp','WebprntLabelIp','TeesheetRefreshRate','OnlinePurchaseTerminalId',
			'OnlineInvoiceTerminalId','ReturnPolicy','TimeFormat','Website','AtLogin','AtPassword','AtTest',
			'MercuryId','MercuryPassword','MercuryE2eId','MercuryE2ePassword','EtsKey','E2eAccountId','E2eAccountKey',
			'UseEtsGiftcards','FacebookPageId','FacebookPageName','FacebookExtendedAccessToken','AllowFriendsToInvite',
			'ForeupDiscountPercent','NoShowPolicy','ReservationEmailText','ReservationEmailPhoto','IbeaconMajorId',
			'IbeaconEnabled','AutoGratuity','MinimumFoodSpend','QuickbooksExportSettings','ErangeId','ErangePassword',
			'CreditCardFee','MultiplePrinters','RequireGuestCount','DefaultKitchenPrinter','ElementAccountId',
			'ElementAccountToken','ElementApplicationId','ElementAcceptorId','DisableFacebookWidget',
			'ServiceFeeTax1Name','ServiceFeeTax1Rate','ServiceFeeTax2Name','ServiceFeeTax2Rate','AutoGratuityThreshold',
			'DefaultRegisterLogOpen','CustomerFieldSettings','TermsAndConditions','OnlineBookingWelcomeMessage',
			'Category','Department','Modules','ApiUserCourses','TeedOffColor','ReceiptPrinter'];

		$this->interface_tester($course,$verbs,$nouns);

		$verbs = ['is','set'];
		$nouns = ['ActiveCourse','Demo','MaintenanceMode','AutoMailers','Events','FoodAndBeverage',
			'FoodAndBeverageV2','Passes','Invoices','Quickbooks','Recipients','Sales','Schedules',
			'Tournaments','UnitPriceIncludesTax','DisplayEmailAds','HideBackNine','PrintAfterSale',
			'Webprnt','PrintTwoReceipts','PrintTwoSignatureSlips','PrintTwoReceiptsOther','TipLine',
			'UpdatedPrinting','AfterSaleLoad','FnbLogin','TeesheetUpdatesAutomatically','AutoSplitTeetimes',
			'SendReservationConfirmations','PrintCreditCardReceipt','PrintSalesReceipt',
			'PrintTipLine','CashDrawerOnCash','TrackCash','BlindClose','SeparateCourses','DeductTips',
			'UseTerminals','UseLoyalty','OpenSun','OpenMon','OpenTue','OpenWed','OpenThu','OpenFri','OpenSat',
			'WeekendFri','WeekendSat','WeekendSun','Simulator','TestCourse','PaymentRequired','SendEmailReminder',
			'SendTextReminder','SeasonalPricing','IncludeTaxOnlineBooking','HideEmployeeLastNameReceipt',
			'SalesV2','RequireEmployeePin','CleanOutNightly','UseCourseFiring','CourseFiringIncludeItems',
			'TeeSheetSpeedUp','UseKitchenBuzzers','HideModifierNamesKitchenReceipts','FoodBevSortBySeat',
			'ServiceFeeActive','PrintSuggestedTip','MarketingTexting','StackTeeSheets','AlwaysListAll',
			'FoodBevTipLineFirstReceipt','HideRegistration','MobileAppActive','AutoEmailReceipt','AutoEmailNoPrint',
			'RequireSignatureMemberPayments','HideTaxable','AllowEmployeeRegisterLogBypass','TeetimeDefaultToPlayer1',
			'PrintTeeTimeDetails','ReceiptPrintAccountBalance','PrintMinimumsOnReceipt','LimitFeeDropdownByCustomer',
			'RequireCustomerOnSale','CashDrawerOnSale','UseNewPermissions','SideBySideTeeSheets','CurrentDayCheckinsOnly',
			'OnlineGiftcardPurchases','ShowInvoiceSaleItems'];

		$this->interface_tester($course,$verbs,$nouns);

	}

	public function testGetSet()
	{
		$course = new ForeupCourses();

		foreach($course->boolean_is as $field){
			$course->{'set'.$field[1]}(0);
		}

		foreach($course->mixed_get_set as $field){
			$value = 0;
			if($field[0]==='string')$value = '';
			elseif($field[0]==='datetime')$value = new \DateTime();

			$course->{'set'.$field[1]}($value);
		}

		$this->get_set_tester($course,'ForeupCourses->validate',$course->boolean_is,['is','set']);

		$this->get_set_tester($course,'ForeupCourses->validate',$course->mixed_get_set);
	}
}