<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupPeople;

include_once('EntityTestingUtility.php');

class testAccountStatementsEntity  extends EntityTestingUtility
{
	use \Codeception\Specify;

    protected $statement;

	protected function _before()
	{
		$this->statement = new ForeupAccountStatements();
		$this->statement->setDateCreated(new \DateTime('2016-12-25T12:12:12Z'));
		$this->statement->setStartDate(new \DateTime('2016-12-25T12:12:12Z'));
		$this->statement->setEndDate(new \DateTime('2016-12-25T12:12:12Z'));
		$this->statement->setCreatedBy(1);
		$this->statement->setOrganizationId(1);
		$customer = new ForeupCustomers();
		$person = new ForeupPeople();
		$person->testing_override('personId',1);
		$customer->setPerson($person);
		$this->statement->setCustomer($customer);

	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$statement = $this->statement;

		$this->assertTrue(method_exists($statement,'validate'));
		$this->assertTrue(method_exists($statement,'invalid'));
		$this->assertTrue(method_exists($statement,'getId'));
		$this->assertTrue(method_exists($statement,'getDateCreated'));
		$this->assertTrue(method_exists($statement,'setDateCreated'));
		$this->assertTrue(method_exists($statement,'getStartDate'));
		$this->assertTrue(method_exists($statement,'setStartDate'));
		$this->assertTrue(method_exists($statement,'getEndDate'));
		$this->assertTrue(method_exists($statement,'setEndDate'));
		$this->assertTrue(method_exists($statement,'getCreatedBy'));
		$this->assertTrue(method_exists($statement,'setCreatedBy'));
		$this->assertTrue(method_exists($statement,'getOrganizationId'));
		$this->assertTrue(method_exists($statement,'setOrganizationId'));
		$this->assertTrue(!method_exists($statement,'getPaymentTerms'));
		$this->assertTrue(!method_exists($statement,'setPaymentTerms'));
		$this->assertTrue(method_exists($statement,'getRecurringStatement'));
		$this->assertTrue(method_exists($statement,'setRecurringStatement'));
		$this->assertTrue(!method_exists($statement,'getIncludeBalanceForward'));
		$this->assertTrue(!method_exists($statement,'setIncludeBalanceForward'));
		$this->assertTrue(!method_exists($statement,'getItemizeCharges'));
		$this->assertTrue(!method_exists($statement,'setItemizeCharges'));
		$this->assertTrue(!method_exists($statement,'getItemizeInvoices'));
		$this->assertTrue(!method_exists($statement,'setItemizeInvoices'));
		$this->assertTrue(!method_exists($statement,'getItemizeSales'));
		$this->assertTrue(!method_exists($statement,'setItemizeSales'));
		$this->assertTrue(method_exists($statement,'getNumber'));
		$this->assertTrue(method_exists($statement,'setNumber'));
		$this->assertTrue(method_exists($statement,'getMemo'));
		$this->assertTrue(method_exists($statement,'setMemo'));
		$this->assertTrue(method_exists($statement,'getTotal'));
		$this->assertTrue(method_exists($statement,'setTotal'));
		$this->assertTrue(method_exists($statement,'getTotalPaid'));
		$this->assertTrue(method_exists($statement,'setTotalPaid'));
		$this->assertTrue(method_exists($statement,'getTotalOpen'));
		$this->assertTrue(method_exists($statement,'setTotalOpen'));
		$this->assertTrue(method_exists($statement,'getCustomer'));
		$this->assertTrue(method_exists($statement,'setCustomer'));
		$this->assertTrue(method_exists($statement,'getPersonId'));
		$this->assertTrue(method_exists($statement,'getSendZeroChargeStatement'));
		$this->assertTrue(method_exists($statement,'setSendZeroChargeStatement'));
		$this->assertTrue(method_exists($statement,'getFooterText'));
		$this->assertTrue(method_exists($statement,'setFooterText'));
		$this->assertTrue(method_exists($statement,'getMessageText'));
		$this->assertTrue(method_exists($statement,'setMessageText'));
		$this->assertTrue(method_exists($statement,'getTermsAndConditionsText'));
		$this->assertTrue(method_exists($statement,'setTermsAndConditionsText'));
		$this->assertTrue(method_exists($statement,'getPreviousOpenBalance'));
		$this->assertTrue(method_exists($statement,'setPreviousOpenBalance'));
	}

	public function testGetSet()
	{
		$location = 'ForeupAccountStatements->validate';
		$statement = $this->statement;
		$this->assertTrue($statement->validate());

		$this->string_tester($statement,$location,'memo','getMemo','setMemo',false);

		$this->DateTime_tester($statement,$location,'dateCreated','getDateCreated','setDateCreated',true);

		$this->DateTime_tester($statement,$location,'startDate','getStartDate','setStartDate',true);

		$this->DateTime_tester($statement,$location,'endDate','getEndDate','setEndDate',true);

		$this->DateTime_tester($statement,$location,'dueDate','getDueDate','setDueDate',false);

		$this->integer_tester($statement,$location,'organizationId','getOrganizationId','setOrganizationId',true);


		$this->integer_tester($statement,$location,'createdBy','getCreatedBy','setCreatedBy',true);


		//$this->integer_tester($statement,$location,'personId','getPersonId','setPersonId',true);


		$this->assertEquals($statement->getNumber(),1);
		$this->integer_tester($statement,$location,'number','getNumber','setNumber',true);

		$this->string_tester($statement,$location,'footerText','getFooterText','setFooterText',false);
		$this->string_tester($statement,$location,'messageText','getMessageText','setMessageText',false);
		$this->string_tester($statement,$location,'termsAndConditionsText','getTermsAndConditionsText','setTermsAndConditionsText',false);
		$this->boolean_tester($statement,$location,'sendZeroChargeStatement','getSendZeroChargeStatement','setSendZeroChargeStatement',true);
		$this->numeric_tester($statement,$location,'previousOpenBalance','getPreviousOpenBalance','setPreviousOpenBalance',true);

		$this->numeric_tester($statement,$location,'totalFinanceCharges','getTotalFinanceCharges','setTotalFinanceCharges',true);
		$this->boolean_tester($statement,$location,'financeChargeEnabled','getFinancechargeEnabled','setFinanceChargeEnabled',true);
		$this->numeric_tester($statement,$location,'financeChargeAmount','getFinanceChargeAmount','setFinanceChargeAmount',true);
		$this->enum_tester($statement,$location,'financeChargeType',['percent','fixed'],'getFinanceChargeType','setFinanceChargeType',true);
		$this->integer_tester($statement,$location,'financeChargeAfterDays','getFinanceChargeAfterDays','setFinanceChargeAfterDays',true);
	}
}

?>