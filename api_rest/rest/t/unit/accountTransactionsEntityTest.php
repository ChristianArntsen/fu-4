<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupAccountTransactions;
use foreup\rest\models\entities\ForeupSales;
use foreup\rest\models\entities\ForeupSalesPaymentsCreditCards;

require_once ('EntityTestingUtility.php');

class testAccountTransactionsEntity  extends EntityTestingUtility
{
	use \Codeception\Specify;

	protected $transaction;

	protected function _before()
	{
		$this->transaction = new ForeupAccountTransactions();
		$this->transaction->setAccountType('dunno...');
		$sale = new ForeupSales();
		$sale->testing_override('saleId',12345);
		$this->transaction->setSale($sale);
		$this->transaction->setCourseId(6270);
		$this->transaction->setInvoiceId(678910);
		$this->transaction->setTransHousehold(1112);
		$this->transaction->setTransComment('Why cant this be NULL?');
		$this->transaction->setTransDescription('Why cant this be NULL?');
		$this->transaction->setTransAmount(11.12);
		$this->transaction->setHideOnInvoices(false);
	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$transaction = $this->transaction;

		$this->assertTrue(method_exists($transaction,'validate'));
		$this->assertTrue(method_exists($transaction,'invalid'));
		$this->assertTrue(method_exists($transaction,'getTransId'));
		$this->assertTrue(method_exists($transaction,'getAccountType'));
		$this->assertTrue(method_exists($transaction,'setAccountType'));
		$this->assertTrue(method_exists($transaction,'getSaleId'));
		$this->assertTrue(method_exists($transaction,'getSale'));
		$this->assertTrue(method_exists($transaction,'setSale'));
		$this->assertTrue(method_exists($transaction,'getInvoiceId'));
		$this->assertTrue(method_exists($transaction,'setInvoiceId'));
		$this->assertTrue(method_exists($transaction,'getCourseId'));
		$this->assertTrue(method_exists($transaction,'setCourseId'));
		$this->assertTrue(method_exists($transaction,'getCustomer'));
		$this->assertTrue(method_exists($transaction,'setCustomer'));
		$this->assertTrue(method_exists($transaction,'getTransHousehold'));
		$this->assertTrue(method_exists($transaction,'setTransHousehold'));
		$this->assertTrue(method_exists($transaction,'getTransUser'));
		$this->assertTrue(method_exists($transaction,'setTransUser'));
		$this->assertTrue(method_exists($transaction,'getTransDate'));
		$this->assertTrue(method_exists($transaction,'setTransDate'));
		$this->assertTrue(method_exists($transaction,'getTransComment'));
		$this->assertTrue(method_exists($transaction,'setTransComment'));
		$this->assertTrue(method_exists($transaction,'getTransDescription'));
		$this->assertTrue(method_exists($transaction,'setTransDescription'));
		$this->assertTrue(method_exists($transaction,'getTransAmount'));
		$this->assertTrue(method_exists($transaction,'setTransAmount'));
		$this->assertTrue(method_exists($transaction,'getRunningBalance'));
		$this->assertTrue(method_exists($transaction,'setRunningBalance'));
		$this->assertTrue(method_exists($transaction,'getHideOnInvoices'));
		$this->assertTrue(method_exists($transaction,'setHideOnInvoices'));
		$this->assertTrue(method_exists($transaction,'getMinimumChargeId'));
		$this->assertTrue(method_exists($transaction,'setMinimumChargeId'));
	}

	public function testGetSet()
	{

		$transaction = $this->transaction;
		$this->assertTrue($transaction->validate());
		$location = 'ForeupAccountTransactions->validate';

		$this->string_tester($transaction,$location,'accountType','getAccountType','setAccountType',true);
		//$this->integer_tester($transaction,$location,'saleId','getSaleId','setSaleId',true);
		$this->integer_tester($transaction,$location,'invoiceId','getInvoiceId','setInvoiceId',true);
		$this->integer_tester($transaction,$location,'courseId','getCourseId','setCourseId',true);
		$this->integer_tester($transaction,$location,'transHousehold','getTransHousehold','setTransHousehold',true);
		$this->integer_tester($transaction,$location,'transUser','getTransUser','setTransUser',false);
		$this->DateTime_tester($transaction,$location,'transDate','getTransDate','setTransDate',false);
		$this->string_tester($transaction,$location,'transComment','getTransComment','setTransComment',true);
		$this->string_tester($transaction,$location,'transDescription','getTransDescription','setTransDescription',true);
		$this->numeric_tester($transaction,$location,'transAmount','getTransAmount','setTransAmount',false);
		$this->numeric_tester($transaction,$location,'runningBalance','getRunningBalance','setRunningBalance',false);
		$this->boolean_tester($transaction,$location,'hideOnInvoices','getHideOnInvoices','setHideOnInvoices',true);
		$this->integer_tester($transaction,$location,'minimumChargeId','getMinimumChargeId','setMinimumChargeId',false);
	}
}