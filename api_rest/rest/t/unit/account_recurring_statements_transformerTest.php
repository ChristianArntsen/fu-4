<?php
namespace foreup\rest\unit;
use foreup\rest\controllers\account_recurring\accountRecurringCharges;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupAccountPaymentTerms;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\resource_transformers\account_recurring_statements_transformer;

//use Silex\Application;

class testAccountRecurringStatementsTransformer  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	public function testInterface()
	{
		$transformer = new account_recurring_statements_transformer();
		$this->assertTrue(method_exists($transformer, 'transform'));
		$this->assertTrue(method_exists($transformer, 'includePaymentTerms'));
		$this->assertTrue(method_exists($transformer, 'includeRepeated'));
	}

	public Function testTransform()
	{

		$transformer = new account_recurring_statements_transformer();
		$statement = new ForeupAccountRecurringStatements();
		$statement->setName('valid name');
		$statement->setCustomerMessage('valid description');
		$statement->setFooterText('valid text1');
		$statement->setMessageText('valid text2');
		$statement->setTermsAndConditionsText('valid text3');
		$statement->setIsDefault(1);
		$statement->setSendZeroChargeStatement(1);
		$statement->setDateCreated(new \DateTime('2016-03-03T12:12:12Z'));
		$emp = new ForeupEmployees();
		$per = new ForeupPeople();
		$emp->setPerson($per);
		$statement->setCreatedBy($emp);
		$statement->setOrganizationId(123);
		$statement->setIsActive(1);

		$statement->setIncludeMemberTransactions(1);
		$statement->setIncludeCustomerTransactions(1);
		$statement->setSendEmail(1);
		$statement->setSendMail(1);
		$statement->setIncludePastDue(1);
		$statement->setDueAfterDays(3);
		$statement->setAutopayEnabled(1);
		$statement->setAutopayAfterDays(3);
		$statement->setAutopayAttempts(3);
		$statement->setAutopayOverdueBalance(1);
		$statement->setFinanceChargeEnabled(1);
		$statement->setFinanceChargeAmount(1.23);
		$statement->setFinanceChargeType('fixed');
		$statement->setFinanceChargeAfterDays(15);

		$terms = new ForeupAccountPaymentTerms();
		$terms->setName('valid name');
		$terms->setDateCreated(new \DateTime('2016-03-03T12:12:12Z'));
		$terms->setMemo('valid memo');
		$terms->setOrganizationId(123);

		$returned = $transformer->transform($statement);

		$this->assertEquals($statement->getName(),$returned['name']);
		$this->assertEquals($statement->getCustomerMessage(),$returned['customerMessage']);
		$this->assertEquals($statement->getOrganizationId(),$returned['organizationId']);
		$this->assertEquals($statement->getDateCreated(),new \DateTime($returned['dateCreated']));
		$this->assertEquals($statement->getIsActive(),$returned['isActive']);
		$this->assertEquals($statement->getIncludeMemberTransactions(),$returned['includeMemberTransactions']);
		$this->assertEquals($statement->getIncludeCustomerTransactions(),$returned['includeCustomerTransactions']);
		$this->assertEquals($statement->getSendEmail(),$returned['sendEmail']);
		$this->assertEquals($statement->getSendMail(),$returned['sendMail']);
		$this->assertEquals($statement->getIncludePastDue(),$returned['includePastDue']);
		$this->assertEquals($statement->getDueAfterDays(),$returned['dueAfterDays']);
		$this->assertEquals($statement->getAutopayEnabled(),$returned['autopayEnabled']);
		$this->assertEquals($statement->getAutopayAfterDays(),$returned['autopayAfterDays']);
		$this->assertEquals($statement->getAutopayAttempts(),$returned['autopayAttempts']);
		$this->assertEquals($statement->getAutopayOverdueBalance(),$returned['autopayOverdueBalance']);
		$this->assertEquals($statement->getSendZeroChargeStatement(),$returned['sendZeroChargeStatement']);
		$this->assertEquals($statement->getFooterText(),$returned['footerText']);
		$this->assertEquals($statement->getMessageText(),$returned['messageText']);
		$this->assertEquals($statement->getTermsAndConditionsText(),$returned['termsAndConditionsText']);
		$this->assertEquals($statement->getIsDefault(),$returned['isDefault']);
		$this->assertEquals($statement->getFinanceChargeAfterDays(),$returned['financeChargeAfterDays']);
		$this->assertEquals($statement->getFinanceChargeType(),$returned['financeChargeType']);
		$this->assertEquals($statement->getFinanceChargeAmount(),$returned['financeChargeAmount']);
		$this->assertEquals($statement->getFinanceChargeEnabled(),$returned['financeChargeEnabled']);


		$returned = $transformer->includePaymentTerms($statement);
		$this->assertEmpty($returned);

		$statement->setPaymentTerms($terms);
		$returned = $transformer->includePaymentTerms($statement);
		//$returned = $controller->serializeResource($statement);
		//$this->assertNotEmpty($returned->getData());
		$this->assertEquals($terms,$returned->getData());
	}
}
?>