<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupRepeatable;
use foreup\rest\models\entities\ForeupRepeatableAccountRecurringChargeItems;

class testRepeatableEntity  extends \Codeception\TestCase\Test
{
	private $repeatable;

	protected function _before()
	{
		$this->repeatable = new ForeupRepeatableAccountRecurringChargeItems();
		$this->repeatable->setType('recurring_charge_item');
		$this->repeatable->setResourceId(1);
	}

	protected function _after()
	{
	}

	public function testInterface(){
		$repeatable = $this->repeatable;
		$this->assertTrue(method_exists($repeatable,'getId'));
		$this->assertTrue(method_exists($repeatable,'getType'));
		$this->assertTrue(method_exists($repeatable,'setType'));
		$this->assertTrue(method_exists($repeatable,'getResourceId'));
		$this->assertTrue(method_exists($repeatable,'setResourceId'));
		$this->assertTrue(method_exists($repeatable,'getLastRan'));
		$this->assertTrue(method_exists($repeatable,'setLastRan'));
		$this->assertTrue(method_exists($repeatable,'getNextOccurence'));
		$this->assertTrue(method_exists($repeatable,'setNextOccurence'));
		$this->assertTrue(method_exists($repeatable,'getFreq'));
		$this->assertTrue(method_exists($repeatable,'setFreq'));
		$this->assertTrue(method_exists($repeatable,'getDtstart'));
		$this->assertTrue(method_exists($repeatable,'setDtstart'));
		$this->assertTrue(method_exists($repeatable,'getUntil'));
		$this->assertTrue(method_exists($repeatable,'setUntil'));
		$this->assertTrue(method_exists($repeatable,'getInterval'));
		$this->assertTrue(method_exists($repeatable,'setInterval'));
		$this->assertTrue(method_exists($repeatable,'getCount'));
		$this->assertTrue(method_exists($repeatable,'setCount'));
		$this->assertTrue(method_exists($repeatable,'getBymonth'));
		$this->assertTrue(method_exists($repeatable,'setBymonth'));
		$this->assertTrue(method_exists($repeatable,'getByweekno'));
		$this->assertTrue(method_exists($repeatable,'setByweekno'));
		$this->assertTrue(method_exists($repeatable,'getByyearday'));
		$this->assertTrue(method_exists($repeatable,'setByyearday'));
		$this->assertTrue(method_exists($repeatable,'getBymonthday'));
		$this->assertTrue(method_exists($repeatable,'setBymonthday'));
		$this->assertTrue(method_exists($repeatable,'getByday'));
		$this->assertTrue(method_exists($repeatable,'setByday'));
		$this->assertTrue(method_exists($repeatable,'getWkst'));
		$this->assertTrue(method_exists($repeatable,'setWkst'));
		$this->assertTrue(method_exists($repeatable,'getByhour'));
		$this->assertTrue(method_exists($repeatable,'setByhour'));
		$this->assertTrue(method_exists($repeatable,'getByminute'));
		$this->assertTrue(method_exists($repeatable,'setByminute'));
		$this->assertTrue(method_exists($repeatable,'getBysecond'));
		$this->assertTrue(method_exists($repeatable,'setBysecond'));
		$this->assertTrue(method_exists($repeatable,'getBysetpos'));
		$this->assertTrue(method_exists($repeatable,'setBysetpos'));


	}

	public function testGetSet(){
		$repeatable = $this->repeatable;


		$this->assertEquals($repeatable->getType(),'recurring_charge_item');
		$this->assertTrue($repeatable->validate());
		$repeatable->setType('bob');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: type "bob" not found in enumerated values: ["marketing_campaign","recurring_charge_item","recurring_statement"]');
		$repeatable->setType('marketing_campaign');
		$this->assertEquals($repeatable->getType(),'marketing_campaign');
		$this->assertTrue($repeatable->validate());

		$this->assertEquals($repeatable->getResourceId(),1);
		$this->assertTrue($repeatable->validate());
		$repeatable->setResourceId('bob');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: resourceId must be an integer: bob');
		$repeatable->setResourceId(null);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->isActive Error: resourceId cannot be NULL');
		$repeatable->setResourceId(123);
		$this->assertEquals($repeatable->getResourceId(),123);
		$this->assertTrue($repeatable->validate());
		$repeatable->setResourceId('456');
		$this->assertEquals($repeatable->getResourceId(),'456');
		$this->assertTrue($repeatable->validate());

		$repeatable->setLastRan('bob');
		$this->assertEquals($repeatable->validate(false), 'ForeupRepeatable->validate Error: invalid lastRan: bob');
		$repeatable->setLastRan(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setLastRan(new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertEquals($repeatable->getLastRan(),new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertTrue($repeatable->validate());

		// throws error
		//$repeatable->setNextOccurence('bob');
		//$this->assertEquals($repeatable->validate(false), 'ForeupRepeatable->validate Error: invalid nextOccurence: bob');
		$repeatable->setNextOccurence(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setNextOccurence(new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertEquals($repeatable->getNextOccurence(),new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertTrue($repeatable->validate());

		$repeatable->setFreq('bob');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: freq "bob" not found in enumerated values: ["SECONDLY","MINUTELY","HOURLY","DAILY","WEEKLY","MONTHLY","YEARLY"]');
		$repeatable->setFreq('');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: freq "" not found in enumerated values: ["SECONDLY","MINUTELY","HOURLY","DAILY","WEEKLY","MONTHLY","YEARLY"]');
		$repeatable->setFreq('SECONDLY');
		$this->assertEquals($repeatable->getFreq(),'SECONDLY');
		$this->assertTrue($repeatable->validate());$repeatable->setFreq('MINUTELY');
		$this->assertEquals($repeatable->getFreq(),'MINUTELY');
		$this->assertTrue($repeatable->validate());
		$this->assertTrue($repeatable->validate());$repeatable->setFreq('HOURLY');
		$this->assertTrue($repeatable->validate());
		$this->assertTrue($repeatable->validate());$repeatable->setFreq('DAILY');
		$this->assertTrue($repeatable->validate());
		$this->assertTrue($repeatable->validate());$repeatable->setFreq('WEEKLY');
		$this->assertTrue($repeatable->validate());
		$this->assertTrue($repeatable->validate());$repeatable->setFreq('MONTHLY');
		$this->assertTrue($repeatable->validate());
		$this->assertTrue($repeatable->validate());$repeatable->setFreq('YEARLY');
		$this->assertTrue($repeatable->validate());

		// throws an error
		//$repeatable->setDtstart('bob');
		//$this->assertEquals($repeatable->validate(false), 'ForeupRepeatable->validate Error: invalid dtstart: bob');
		$repeatable->setDtstart(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setDtstart('');
		$this->assertTrue($repeatable->validate());
		$repeatable->setDtstart(new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertEquals($repeatable->getDtstart(),new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertTrue($repeatable->validate());

		//$repeatable->setUntil('bob');
		//$this->assertEquals($repeatable->validate(false), 'ForeupRepeatable->validate Error: invalid until: bob');
		$repeatable->setUntil(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setUntil('');
		$this->assertTrue($repeatable->validate());
		$repeatable->setUntil(new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertEquals($repeatable->getUntil(),new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertTrue($repeatable->validate());

		$repeatable->setInterval('bob');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: interval must be an integer: bob');
		$repeatable->setInterval('1.23');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: interval must be an integer: 1.23');
		$repeatable->setInterval(2.34);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: interval must be an integer: 2.34');
		$repeatable->setInterval(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setInterval('');
		$this->assertTrue($repeatable->validate());
		$repeatable->setInterval(123);
		$this->assertEquals($repeatable->getInterval(),123);
		$this->assertTrue($repeatable->validate());
		$repeatable->setInterval('456');
		$this->assertEquals($repeatable->getInterval(),'456');
		$this->assertTrue($repeatable->validate());

		$repeatable->setCount('bob');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: count must be an integer: bob');
		$repeatable->setCount('1.23');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: count must be an integer: 1.23');
		$repeatable->setCount(2.34);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: count must be an integer: 2.34');
		$repeatable->setCount(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setCount('');
		$this->assertTrue($repeatable->validate());
		$repeatable->setCount(123);
		$this->assertEquals($repeatable->getCount(),123);
		$this->assertTrue($repeatable->validate());
		$repeatable->setCount('456');
		$this->assertEquals($repeatable->getCount(),'456');
		$this->assertTrue($repeatable->validate());

		$repeatable->setBymonth('bob');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): bob');
		$repeatable->setBymonth('1.23');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): 1.23');
		$repeatable->setBymonth(2.34);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): 2.34');
		$repeatable->setBymonth(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setBymonth('');
		$this->assertTrue($repeatable->validate());
		$repeatable->setBymonth(13);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): 13');
		$repeatable->setBymonth(0);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): 0');
		$repeatable->setBymonth([1,2,3,4,5,6,7,8,9,10,11,12,'cow']);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): cow');
		$repeatable->setBymonth([1,2,3,4,5,6,7,8,9,'cow',10,11,12]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): cow');
		$repeatable->setBymonth(['cow',1,2,3,4,5,6,7,8,9,10,11,12]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): cow');
		$repeatable->setBymonth([1,2,3,4,5,6,7,8,9,10,11,12,5.5]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): 5.5');
		$repeatable->setBymonth([1,2,3,4,5,6,7,8,9,4.6,10,11,12]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): 4.6');
		$repeatable->setBymonth([3.7,1,2,3,4,5,6,7,8,9,10,11,12]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonth must be an array of integers (1-12): 3.7');
		$repeatable->setBymonth([1,2,3,4,5,6,7,8,9,10,11,12]);
		$this->assertEquals($repeatable->getBymonth(),[1,2,3,4,5,6,7,8,9,10,11,12]);
		$this->assertTrue($repeatable->validate());
		$repeatable->setBymonth([12]);
		$this->assertEquals($repeatable->getBymonth(),[12]);
		$this->assertTrue($repeatable->validate());
		$repeatable->setBymonth(['1']);
		$this->assertEquals($repeatable->getBymonth(),['1']);
		$this->assertTrue($repeatable->validate());

        $weeknoError = 'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): ';
		$repeatable->setByweekno('bob');
		$this->assertEquals($repeatable->validate(false), $weeknoError.'bob');
		$repeatable->setByweekno('1.23');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): 1.23');
		$repeatable->setByweekno(2.34);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): 2.34');
		$repeatable->setByweekno([1,2,3,4,5,6,7,8,9,10,11,53,'cow']);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): cow');
		$repeatable->setByweekno([1,2,3,4,5,6,7,8,9,'cow',10,11,53]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): cow');
		$repeatable->setByweekno(['cow',1,2,3,4,5,6,7,8,9,10,11,53]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): cow');
		$repeatable->setByweekno([1,2,3,4,5,6,7,8,9,10,11,53,5.5]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): 5.5');
		$repeatable->setByweekno([1,2,3,4,5,6,7,8,9,4.6,10,11,53]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): 4.6');
		$repeatable->setByweekno([3.7,1,2,3,4,5,6,7,8,9,10,11,53]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): 3.7');
		$repeatable->setByweekno([1,2,3,4,5,6,7,8,9,10,11,53]);
		$this->assertEquals($repeatable->getByweekno(),[1,2,3,4,5,6,7,8,9,10,11,53]);
		$repeatable->setByweekno(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setByweekno('');
		$this->assertTrue($repeatable->validate());
		$repeatable->setByweekno([54]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): 54');
		$repeatable->setByweekno([0]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byweekno must be an array of integers (1-53): 0');
		$repeatable->setByweekno([53]);
		$this->assertEquals($repeatable->getByweekno(),[53]);
		$this->assertTrue($repeatable->validate());
		$repeatable->setByweekno(['1']);
		$this->assertEquals($repeatable->getByweekno(),['1']);
		$this->assertTrue($repeatable->validate());

		$repeatable->setByyearday('bob');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): bob');
		$repeatable->setByyearday('1.23');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): 1.23');
		$repeatable->setByyearday(2.34);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): 2.34');
		$repeatable->setByyearday(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setByyearday('');
		$this->assertTrue($repeatable->validate());
		$repeatable->setByyearday(367);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): 367');
		$repeatable->setByyearday(0);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): 0');
		$repeatable->setByyearday([1,2,3,4,5,6,7,8,9,10,11,366,'cow']);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): cow');
		$repeatable->setByyearday([1,2,3,4,5,6,7,8,9,'cow',10,11,366]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): cow');
		$repeatable->setByyearday(['cow',1,2,3,4,5,6,7,8,9,10,11,366]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): cow');
		$repeatable->setByyearday([1,2,3,4,5,6,7,8,9,10,11,366,5.5]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): 5.5');
		$repeatable->setByyearday([1,2,3,4,5,6,7,8,9,4.6,10,11,366]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): 4.6');
		$repeatable->setByyearday([3.7,1,2,3,4,5,6,7,8,9,10,11,366]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: byyearday must be an array of integers (1-366): 3.7');
		$repeatable->setByyearday([1,2,3,4,5,6,7,8,9,10,11,366]);
		$this->assertEquals($repeatable->getByyearday(),[1,2,3,4,5,6,7,8,9,10,11,366]);
		$repeatable->setByyearday([366]);
		$this->assertEquals($repeatable->getByyearday(),[366]);
		$this->assertTrue($repeatable->validate());
		$repeatable->setByyearday(['1']);
		$this->assertEquals($repeatable->getByyearday(),['1']);
		$this->assertTrue($repeatable->validate());

		$repeatable->setBymonthday('bob');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): bob');
		$repeatable->setBymonthday('1.23');
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): 1.23');
		$repeatable->setBymonthday(2.34);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): 2.34');
		$repeatable->setBymonthday(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setBymonthday('');
		$this->assertTrue($repeatable->validate());
		$repeatable->setBymonthday(32);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): 32');
		$repeatable->setBymonthday(0);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): 0');
		$repeatable->setBymonthday([1,2,3,4,5,6,7,8,9,10,11,31,'cow']);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): cow');
		$repeatable->setBymonthday([1,2,3,4,5,6,7,8,9,'cow',10,11,31]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): cow');
		$repeatable->setBymonthday(['cow',1,2,3,4,5,6,7,8,9,10,11,31]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): cow');
		$repeatable->setBymonthday([1,2,3,4,5,6,7,8,9,10,11,31,5.5]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): 5.5');
		$repeatable->setBymonthday([1,2,3,4,5,6,7,8,9,4.6,10,11,31]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): 4.6');
		$repeatable->setBymonthday([3.7,1,2,3,4,5,6,7,8,9,10,11,31]);
		$this->assertEquals($repeatable->validate(false),'ForeupRepeatable->validate Error: bymonthday must be an array of integers (1-31): 3.7');
		$repeatable->setBymonthday([1,2,3,4,5,6,7,8,9,10,11,31]);
		$this->assertEquals($repeatable->getBymonthday(),[1,2,3,4,5,6,7,8,9,10,11,31]);
		$repeatable->setBymonthday([31]);
		$this->assertEquals($repeatable->getBymonthday(),[31]);
		$this->assertTrue($repeatable->validate());
		$repeatable->setBymonthday(['1']);
		$this->assertEquals($repeatable->getBymonthday(),['1']);
		$this->assertTrue($repeatable->validate());

		$bydayErr = 'ForeupRepeatable->validate Error: byday must be an array of integers (0..6, 0 being MO) or the following enumerated values: '.json_encode(array('MO','TU','WE','TH','FR','SA','SU'));
		$repeatable->setByday('bob');
		$this->assertEquals($repeatable->validate(false),$bydayErr.': bob');
		$repeatable->setByday('1.23');
		$this->assertEquals($repeatable->validate(false),$bydayErr.': 1.23');
		$repeatable->setByday(2.34);
		$this->assertEquals($repeatable->validate(false),$bydayErr.': 2.34');
		$repeatable->setByday(null);
		$this->assertTrue($repeatable->validate());
		$repeatable->setByday('');
		$this->assertTrue($repeatable->validate());
		$repeatable->setByday(7);
		$this->assertEquals($repeatable->validate(false),$bydayErr.': 7');
		$repeatable->setByday(-1);
		$this->assertEquals($repeatable->validate(false),$bydayErr.': -1');
		$repeatable->setByday([1,2,3,4,5,6,'cow']);
		$this->assertEquals($repeatable->validate(false),$bydayErr.': cow');
		$repeatable->setByday([1,2,3,'cow',4,5,6]);
		$this->assertEquals($repeatable->validate(false),$bydayErr.': cow');
		$repeatable->setByday(['cow',0,1,2,3,4,5,6]);
		$this->assertEquals($repeatable->validate(false),$bydayErr.': cow');
		$repeatable->setByday([0,1,2,3,4,5,6,5.5]);
		$this->assertEquals($repeatable->validate(false),$bydayErr.': 5.5');
		$repeatable->setByday([0,1,2,3,4.6,4,5,6]);
		$this->assertEquals($repeatable->validate(false),$bydayErr.': 4.6');
		$repeatable->setByday([3.7,0,1,2,3,4,5,6]);
		$this->assertEquals($repeatable->validate(false),$bydayErr.': 3.7');
		$repeatable->setByday(['MO','TU','WE','cow','TH','FR','SA','SU']);
		$this->assertEquals($repeatable->validate(false),$bydayErr.': cow');
		$repeatable->setByday([0,1,2,3,4,5,6]);
		$this->assertEquals($repeatable->getByday(),[0,1,2,3,4,5,6]);
		$repeatable->setByday(['MO','TU','WE','TH','FR','SA','SU']);
		$this->assertEquals($repeatable->getByday(),['MO','TU','WE','TH','FR','SA','SU']);
		$repeatable->setByday([6]);
		$this->assertEquals($repeatable->getByday(),[6]);
		$this->assertTrue($repeatable->validate());
		$repeatable->setByday(['0']);
		$this->assertEquals($repeatable->getByday(),['0']);
		$this->assertTrue($repeatable->validate());
		$repeatable->setByday(['MO']);
		$this->assertEquals($repeatable->getByday(),['MO']);
		$this->assertTrue($repeatable->validate());
		$repeatable->setByday(['SU']);
		$this->assertEquals($repeatable->getByday(),['SU']);
		$this->assertTrue($repeatable->validate());
	}
}

?>