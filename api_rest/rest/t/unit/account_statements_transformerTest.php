<?php
namespace foreup\rest\unit;

use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupAccountTransactions;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\resource_transformers\account_statements_transformer;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\sales_transformer;

class account_statements_transformerTest  extends \Codeception\TestCase\Test
{
	/**
	 * @var ForeupAccountStatements
	 */
	protected $statement;

	/**
	 * @var account_statements_transformer
	 */
	protected $transformer;

	public function _before()
	{
		$this->transformer = new account_statements_transformer();
		$this->statement = new ForeupAccountStatements();
		$this->statement->setStartDate(new \DateTime('1932-04-01'));
		$this->statement->setEndDate(new \DateTime('1932-05-01'));
		$this->statement->setDateCreated(new \DateTime('1932-05-03'));


	}

	public function testInterface()
	{

		$this->assertTrue(method_exists($this->transformer, 'transform'));
	}

	public function testTransform()
	{
		//
	}

	public  function testIncludeCustomerTransactions()
	{
		$customer = new ForeupCustomers();

		$transaction1 = new ForeupAccountTransactions();
		$transaction1->setTransDate(new \DateTime('1932-04-15'));
		$transaction1->setAccountType('customer');
		$transaction2 = new ForeupAccountTransactions();
		$transaction2->setTransDate(new \DateTime('1932-04-16'));
		$transaction2->setAccountType('member');
		$person = new ForeupPeople();
		$person->testing_override('personId',3399);
		$customer->setPerson($person);

		$customer->addAccountTransaction($transaction1);
		$customer->addAccountTransaction($transaction2);
		$this->statement->setCustomer($customer);

		$result = $this->transformer->includeCustomerTransactions($this->statement);

		$data = $result->getData();
		$res_array = $data->toArray();

		$res_array = array_values($res_array);

		$this->assertEquals(1,count($res_array));
		$this->assertTrue(method_exists($res_array[0],'getAccountType'));
		$this->assertEquals('customer',$res_array[0]->getAccountType());
	}

	public  function testIncludeMemberTransactions()
	{
		$customer = new ForeupCustomers();

		$transaction1 = new ForeupAccountTransactions();
		$transaction1->setTransDate(new \DateTime('1932-04-15'));
		$transaction1->setAccountType('customer');
		$transaction2 = new ForeupAccountTransactions();
		$transaction2->setTransDate(new \DateTime('1932-04-16'));
		$transaction2->setAccountType('member');
		$person = new ForeupPeople();
		$person->testing_override('personId',3399);
		$customer->setPerson($person);

		$customer->addAccountTransaction($transaction1);
		$customer->addAccountTransaction($transaction2);
		$this->statement->setCustomer($customer);

		$result = $this->transformer->includeMemberTransactions($this->statement);

		$data = $result->getData();
		$res_array = $data->toArray();

		$res_array = array_values($res_array);

		$this->assertEquals(1,count($res_array));
		$this->assertTrue(method_exists($res_array[0],'getAccountType'));
		$this->assertEquals('member',$res_array[0]->getAccountType());
	}

}

?>