<?php
namespace foreup\rest\unit;

use Dflydev\Pimple\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupUsers;
use foreup\rest\t\Helpers\MockDefaultAppFactory;
use foreup\rest\t\traits\mock_app_trait;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use foreup\rest\t\Helpers\testing_uzr;
use foreup\rest\t\Helpers\testing_middleware_loader;
use foreup\rest\t\Helpers\testing_config;
use foreup\rest\t\Helpers\testing_services_loader;
use foreup\rest\t\Helpers\testing_routes_loader;

class testRecurringChargeController  extends \Codeception\TestCase\Test
{
	use mock_app_trait;

	private $save_for_later, $basic_data,$alt_data,$alt_data2;

	protected function _before()
	{
		$this->setupController('accountRecurringCharges.controller');
		$this->basic_data = json_decode('{
			 "type":"account_recurring_charges",
             "attributes":{
			    "name":"valid name 2",
                "description":"valid description 2",
                "is_active":1,
                "prorate_charges":1,
                "recurringStatement":{
                    "name":"valid name 2",
		            "description":"valid description 2",
		            "is_active":1,
		            "repeated":{
				        "freq": "HOURLY",
				        "dtstart": "2096-12-12T11:11:11-0700",
				        "until": "2116-12-12T11:11:11-0700",
				        "interval": 3,
				        "wkst": "MO",
				        "byday": ["MO",2,"FR"],
				        "bymonth": [3,6,12],
				        "bysetpos": [1,5,11],
				        "bymonthday": [1,15,30],
				        "byyearday": [1,365],
				        "byweekno": [1,25,52],
				        "byhour": [1,12,23],
				        "byminute": [1,30,60],
				        "bysecond": [1,30,60]
				    }
                },
                "items":[
                	{
		                "type":"recurringChargeItem",
                		"attributes":{
		                "item_id":1,
                		"item_type":"item",
                		"override_price":19.99,
                		"quantity":900.00,
                		"is_active":true,
                		"repeated":{
			                "type":"repeated",
                			"attributes":{
				                "freq":"MONTHLY"
                			}
                		}
                		}
                	},
                	{
		                "type":"recurringChargeItem",
                		"attributes":{
		                "item_id":2,
	                		"item_type":"item",
	                		"override_price":18.99,
	                		"quantity":900.00,
	                		"is_active":true,
	                		"repeated":{
			                "type":"repeated",
	                			"attributes":{
				                "freq":"MONTHLY"
	                			}
	                		}
                		}
                	}
                	]
             }
          }',true);
		$this->alt_data = json_decode('{
  	"attributes":{
  		"name":"",
		"description":"",
        "prorate_charges":1,
        "items":[{
	        "attributes": {
				"item_id": "1994",
				"course_id": "6270",
				"name": "SPK Tees - Fattee",
				"department": "Course",
				"category": "PS Tees",
				"subcategory": "",
				"supplier_id": null,
				"item_number": "SPIKFATTEE",
				"description": "",
				"override_price": "6.95",
				"base_price": "6.95",
				"inactive": "0",
				"do_not_print": "0",
				"max_discount": "100.00",
				"inventory_level": 1000,
				"inventory_unlimited": 0,
				"cost_price": "0.00",
				"is_giftcard": "0",
				"is_side": "0",
				"food_and_beverage": "0",
				"number_sides": "0",
				"number_salads": "0",
				"number_soups": "0",
				"print_priority": "0",
				"is_fee": "0",
				"erange_size": "0",
				"meal_course_id": "0",
				"taxes": [],
				"is_serialized": "0",
				"do_not_print_customer_receipt": "0",
				"override_price_includes_tax": 0,
				"is_shared": "0",
				"gl_code": null,
				"allow_alt_description": "0",
				"reorder_level": "0",
				"receipt_content_id": null,
				"receipt_content": false,
				"receipt_content_signature_line": null,
				"receipt_content_separate_receipt": null,
				"force_tax": null,
				"is_service_fee": "0",
				"parent_item_percent": null,
				"whichever_is": null,
				"service_fee_id": null,
				"upcs": [],
				"is_pass": "0",
				"pass_days_to_expiration": null,
				"pass_restrictions": null,
				"pass_price_class_id": null,
				"pass_expiration_date": null,
				"pass_multi_customer": null,
				"pass_enroll_loyalty": null,
				"modifiers": [],
				"sides": "",
				"customer_groups": [],
				"printer_groups": null,
				"item_type": "item",
				"loyalty_points_per_dollar": false,
				"loyalty_dollars_per_point": 0,
				"service_fees": [],
				"quantity": 1,
				"tax": 0,
				"subtotal": 6.95,
				"total": 6.95,
				"discount": 0,
				"repeated": {
					"attributes": {
						"period": "quarterly",
						"freq": "WEEKLY",
						"bymonth": [1, 4, 7, 10],
						"bymonthday": [],
						"byweekday": [],
						"bysetpos": [1]
					}
				},
				"_schedule_completion": "not_started",
				"_schedule_visible": true
			}
		}],
		"is_active": true,
		"recurringStatement": {
			"attributes": {
				"name": "",
				"date_created": null,
				"created_by": null,
				"is_active": true,
				"date_deleted": null,
				"deleted_by": null,
				"deliver_empty_statements": null,
				"include_past_due": "0",
				"include_member_transactions": "0",
				"include_customer_transactions": "0",
				"send_email": "0",
				"send_mail": "0",
				"pay_member_balance":"1",
				"payCustomerBalance":"1",
				"send_zero_charge_statement":"1",
				"footer_text":"valid footer text",
				"messageText":"valid message text",
				"termsAndConditionsText":"valid terms and conditions text",
				"repeated": {
					"attributes": {
						"freq": "MONTHLY",
						"byhour":[-5]
					}
				},
				"undefined": "0",
				"due_after_days": "0",
				"autopay_enabled": "0",
				"autopay_after_days": 0,
				"autopay_attempts": null,
				"autopay_overdue_balance": null
			}
		}
	}
}',true);

		$this->alt_data2 = json_decode('
{  
    "attributes": {
    "name": "New template",
    "description": "",
    "is_active": true,
    "prorate_charges": true,
    "items": [{
      "attributes": {
      "item_id": "1994",
      "item_type": "item",
      "item": {
        "attributes": {
        "itemId": "1994",
        "courseId": "6270",
        "name": "SPK Tees - Fattee",
        "department": "Course",
        "category": "PS Tees",
        "subcategory": "",
        "supplierId": null,
        "itemNumber": "SPIKFATTEE",
        "description": "",
        "unitPrice": "6.95",
        "basePrice": "6.95",
        "inactive": "0",
        "doNotPrint": "0",
        "maxDiscount": "100.00",
        "inventoryLevel": 1000,
        "inventoryUnlimited": 0,
        "costPrice": "0.00",
        "isGiftcard": "0",
        "isSide": "0",
        "foodAndBeverage": "0",
        "numberSides": "0",
        "numberSalads": "0",
        "numberSoups": "0",
        "printPriority": "0",
        "isFee": "0",
        "erangeSize": "0",
        "mealCourseId": "0",
        "taxes": [],
        "isSerialized": "0",
        "doNotPrintCustomerReceipt": "0",
        "unitPriceIncludesTax": 0,
        "isShared": "0",
        "glCode": null,
        "allowAltDescription": "0",
        "reorderLevel": "0",
        "receiptContentId": null,
        "receiptContent": false,
        "receiptContentSignatureLine": null,
        "receiptContentSeparateReceipt": null,
        "forceTax": null,
        "isServiceFee": "0",
        "parentItemPercent": null,
        "whicheverIs": null,
        "serviceFeeId": null,
        "upcs": [],
        "isPass": "0",
        "passDaysToExpiration": null,
        "passRestrictions": null,
        "passPriceClassId": null,
        "passExpirationDate": null,
        "passMultiCustomer": null,
        "passEnrollLoyalty": null,
        "modifiers": [],
        "sides": "",
        "customerGroups": [],
        "printerGroups": null,
        "itemType": "item",
        "loyaltyPointsPerDollar": false,
        "loyaltyDollarsPerPoint": 0,
        "serviceFees": [],
        "quantity": 1,
        "qty": 1,
        "tax": 0,
        "subtotal": 6.95,
        "total": 6.95,
        "discount": 0
      },
    "id": "1994"
  },
  "_active": true,
  "_new": false,
  "name": "",
  "quantity": 1,
  "line_number": 1,
  "repeated": {
    "attributes": {
      "_stepsCompleted": 4,
        "period": "MONTHLY",
        "dtstart": null,
        "_active": true,
        "freq": 1,
        "bymonth": null,
        "bymonthday": [1],
        "byweekday": null,
        "bysetpos": null,
        "interval": 1,
        "byday": null,
        "byhour": [23],
        "byminute": [59],
        "bysecond": [59]
    }
  },
  "_schedule_completion": "complete"
  }
}],
  "recurringStatement": {
    "attributes": {
       "isActive": true,
       "sendEmptyStatements": true,
      "includePastDue": true,
      "includeMemberTransactions": true,
      "includeCustomerTransactions": true,
      "sendEmail": true,
      "sendMail": false,
      "dueAfterDays": 14,
      "autopayAttempts": 2,
      "autopayEnabled": true,
      "autopayAfterDays": 0,
      "autopayOverdueBalance": true,
      "payMemberBalance": "1",
      "payCustomerBalance": "1",
      "sendZeroChargeStatement": true,
      "financeChargeEnabled": true,
      "financeChargeAfterDays": 7,
      "financeChargeType": "percent",
      "financeChargeAmount": "5",
      "termsAndConditionsText": "<p>Terms</p>",
      "footerText": "<p>Footer</p>",
      "messageText": "<p>Message</p>",
      "repeated": {
                        "attributes": {
        "_stepsCompleted": 4,
        "period": "MONTHLY",
        "dtstart": null,
        "freq": 1,
        "bymonth": null,
        "bymonthday": [1],
        "byweekday": null,
        "bysetpos": null,
        "interval": 1,
        "byday": null,
        "byhour": [23],
        "byminute": [59],
        "bysecond": [59]
                                                                                                            }
      },
      "_account_charge_settings_complete": false,
      "undefined": "",
      "interval": "1",
      "dtstart": "",
      "files": ""
                    }
  },
  "totalCharges": 6.95
    }
}',true);

		$this->alt_data3 = json_decode('
{
	"attributes": {
		"name": "Test",
		"description": "",
		"is_active": true,
		"prorate_charges": false,
		"items": [{
			"attributes": {
				"item_id": "1994",
				"item_type": "item",
				"item": {
					"attributes": {
						"itemId": "1994",
						"courseId": "6270",
						"name": "SPK Tees - Fattee",
						"department": "Course",
						"category": "PS Tees",
						"subcategory": "",
						"supplierId": null,
						"itemNumber": "SPIKFATTEE",
						"description": "",
						"unitPrice": "6.95",
						"basePrice": "6.95",
						"inactive": "0",
						"doNotPrint": "0",
						"maxDiscount": "100.00",
						"inventoryLevel": 1000,
						"inventoryUnlimited": 0,
						"costPrice": "0.00",
						"isGiftcard": "0",
						"isSide": "0",
						"foodAndBeverage": "0",
						"numberSides": "0",
						"numberSalads": "0",
						"numberSoups": "0",
						"printPriority": "0",
						"isFee": "0",
						"erangeSize": "0",
						"mealCourseId": "0",
						"taxes": [],
						"isSerialized": "0",
						"doNotPrintCustomerReceipt": "0",
						"unitPriceIncludesTax": 0,
						"isShared": "0",
						"glCode": null,
						"allowAltDescription": "0",
						"reorderLevel": "0",
						"receiptContentId": null,
						"receiptContent": false,
						"receiptContentSignatureLine": null,
						"receiptContentSeparateReceipt": null,
						"forceTax": null,
						"isServiceFee": "0",
						"parentItemPercent": null,
						"whicheverIs": null,
						"serviceFeeId": null,
						"upcs": [],
						"isPass": "0",
						"passDaysToExpiration": null,
						"passRestrictions": null,
						"passPriceClassId": null,
						"passExpirationDate": null,
						"passMultiCustomer": null,
						"passEnrollLoyalty": null,
						"modifiers": [],
						"sides": "",
						"customerGroups": [],
						"printerGroups": null,
						"itemType": "item",
						"loyaltyPointsPerDollar": false,
						"loyaltyDollarsPerPoint": 0,
						"serviceFees": [],
						"quantity": 1,
						"qty": 1,
						"tax": 0,
						"subtotal": 6.95,
						"total": 6.95,
						"discount": 0
					},
					"id": "1994"
				},
				"_active": true,
				"_new": false,
				"name": "",
				"quantity": 1,
				"line_number": 1,
				"override_price": null,
				"repeated": {
					"attributes": {
						"_stepsCompleted": 4,
						"period": "monthly",
						"dtstart": null,
						"_active": true,
						"freq": 1,
						"bymonth": null,
						"bymonthday": [-1],
						"byweekday": null,
						"bysetpos": [1],
						"interval": 1,
						"byhour": [0],
						"bysecond": [0],
						"byminute": [0],
						"byday": null
					}
				},
				"_schedule_completion": "complete",
				"_error": false
			}
		}],
		"recurringStatement": {
			"attributes": {
				"isActive": true,
				"isDefault": false,
				"sendEmptyStatements": true,
				"includePastDue": true,
				"includeMemberTransactions": true,
				"includeCustomerTransactions": true,
				"sendEmail": true,
				"sendMail": false,
				"dueAfterDays": 14,
				"autopayAttempts": 3,
				"autopayEnabled": true,
				"autopayAfterDays": 0,
				"autopayOverdueBalance": true,
				"payMemberBalance": "1",
				"payCustomerBalance": "1",
				"sendZeroChargeStatement": true,
				"financeChargeEnabled": true,
				"financeChargeAfterDays": 1,
				"financeChargeType": "fixed",
				"financeChargeAmount": "20.00",
				"termsAndConditionsText": "Terms and conditions here",
				"footerText": "Footer here",
				"messageText": "Thank you for your business!",
				"repeated": {
					"attributes": {
						"type": "recurring_statement",
						"resource_id": 2,
						"last_ran": null,
						"next_occurence": "2100-04-01T00:00:00-0700",
						"freq": 1,
						"dtstart": null,
						"until": null,
						"interval": null,
						"count": null,
						"bymonth": [1, 4, 7, 10],
						"byweekno": null,
						"byyearday": null,
						"bymonthday": [1],
						"byday": null,
						"wkst": null,
						"byhour": [0],
						"byminute": [0],
						"bysecond": [0],
						"bysetpos": null,
						"_stepsCompleted": 4,
						"period": "quarterly",
						"byweekday": null
					}
				},
				"_account_charge_settings_complete": true,
				"name": null,
				"customerMessage": "",
				"dateCreated": "2017-03-20T16:39:07-0600",
				"createdBy": 12,
				"organizationId": 6270,
				"dateDeleted": null,
				"deletedBy": null,
				"undefined": "",
				"dtstart": "",
				"files": ""
			}
		},
		"totalCharges": 6.95
	}
}
		',true);

		$this->save_for_later = json_decode('{
		"attributes": {
			"name": "Ssss",
			"description": "",
			"is_active": true,
			"prorate_charges": 1,
			"items": [],
			"recurringStatement": {
				"attributes": {
					"isActive": true,
					"sendEmptyStatements": true,
					"includePastDue": false,
					"includeMemberTransactions": false,
					"includeCustomerTransactions": false,
					"sendEmail": false,
					"sendMail": false,
					"dueAfterDays": 0,
					"autopayAttempts": 1,
					"autopayEnabled": false,
					"autopayAfterDays": 0,
					"autopayOverdueBalance": false
				}
			},
			"totalCharges": 0
		}
}',true);
	}

	protected function _after()
	{
		$this->tearDownController();
	}

	public function testCreate(){
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->basic_data);
		//$request->request->set('meta',array('test'=>1,'validate_only'=>1));
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringCharges',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
	}

	public function testSaveForLater(){
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->save_for_later);
		//$request->request->set('meta',array('test'=>1,'validate_only'=>1));
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringCharges',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
	}

	public function testCreateErrors(){
		$request = new Request();

		$data = $this->basic_data;
		$data['id'] = 123;
		$request->request->set('data',$data);
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(400,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertFalse($content['success']);
		$this->assertEquals("accountRecurringCharges->create->Please use PATCH request to update existing recurring charge",$content['title']);

		$data = $this->basic_data;
		//$data['attributes']['items'][0]['id'] = -1;
		$data['attributes']['items'][0]['attributes']['repeated']['id'] = -1;
		$data['attributes']['items'][0]['attributes']['repeated']['resource_id'] = -1;
		$request->request->set('data',$data);
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(404,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertFalse($content['success']);
		$this->assertEquals("Repeatable entity not found with id: -1",$content['title']);

		$data = $this->basic_data;
		//$data['attributes']['items'][0]['id'] = -1;
		$data['attributes']['items'][0]['attributes']['id'] = -1;
		$request->request->set('data',$data);
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(500,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertFalse($content['success']);
		$this->assertEquals("Re-assigning existing recurringChargeItem to new recurringCharge not supported",$content['title']);

		$data = $this->basic_data;
		//$data['attributes']['items'][0]['id'] = -1;
		$data['attributes']['items'][0]['id'] = -1;
		$request->request->set('data',$data);
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(500,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertFalse($content['success']);
		$this->assertEquals("Re-assigning existing recurringChargeItem to new recurringCharge not supported",$content['title']);

		$data = $this->basic_data;
		//$data['attributes']['items'][0]['id'] = -1;
		$data['attributes']['recurringStatement']['repeated']['id'] = -1;
		$request->request->set('data',$data);
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(404,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertFalse($content['success']);
		$this->assertEquals("Repeatable entity not found with id: -1",$content['title']);

		$data = $this->basic_data;
		//$data['attributes']['items'][0]['id'] = -1;
		$data['attributes']['recurringStatement']['id'] = -1;
		$request->request->set('data',$data);
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(404,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertFalse($content['success']);
		$this->assertEquals("Statement entity not found with id: -1",$content['title']);


		unset($data['id']);
		$data['attributes']['id'] = 123;
		$request->request->set('data',$data);
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(400,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertFalse($content['success']);
		$this->assertEquals("accountRecurringCharges->create->Please use PATCH request to update existing recurring charge",$content['title']);


		unset($data['attributes']);
		$request->request->set('data',$data);
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(400,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertFalse($content['success']);
		$this->assertEquals("accountRecurringCharges->create->Missing attributes field",$content['title']);
	}

	public function testGet(){
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->basic_data);
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringCharges',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$old_id = $content['data']['id'];
		$request->query->set('include','items');
		$result = $this->controller->get(6270,$content['data']['id'],$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringCharges',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$this->assertEquals($old_id,$content['data']['id']);
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['relationships']);
		$this->assertNotEmpty($content['data']['relationships']['items']);
		$this->assertNotEmpty($content['data']['relationships']['items']['data']);
		$this->assertEquals(2,count($content['data']['relationships']['items']['data']));
		$this->assertNotEmpty($content['included']);
		$this->assertEquals(6,count($content['included']));
	}

	public function testSave()
	{
		$data = $this->basic_data;
		$data['attributes']['created_by'] = new ForeupEmployees();

		$this->controller->startTransaction();
		$rec_charge = $this->controller->save(6270,$data,['test'=>true,'validate_only'=>true,'return_entity'=>true]);
		$this->assertInstanceOf('\foreup\rest\models\entities\ForeupAccountRecurringCharges',$rec_charge);
		// check that it is cool
		$this->assertEquals($data['attributes']['name'],$rec_charge->getName());
		$this->assertEquals($data['attributes']['description'],$rec_charge->getDescription());
		$this->assertEquals(1,$rec_charge->getIsActive());

		$statement = $rec_charge->getRecurringStatement();
		$this->assertInstanceOf('\foreup\rest\models\entities\ForeupAccountRecurringStatements',$statement);
		// check that it is cool
		$this->assertEquals($data['attributes']['recurringStatement']['name'],$statement->getName());
		$this->assertEquals($data['attributes']['recurringStatement']['customerMessage'],$statement->getCustomerMessage());
		$this->assertEquals($data['attributes']['recurringStatement']['footerText'],$statement->getFooterText());
		$this->assertEquals($data['attributes']['recurringStatement']['messageText'],$statement->getMessageText());
		$this->assertEquals($data['attributes']['recurringStatement']['termsAndConditionsText'],$statement->getTermsAndConditionsText());

		$rep = $statement->getRepeated();
		$this->assertInstanceOf('\foreup\rest\models\entities\ForeupRepeatableAccountRecurringStatements',$rep);
		// check that it is cool
		$this->assertEquals($rep->getType(),'recurring_statement');
		$this->assertEmpty($rep->getLastRan());
		$this->assertEquals($rep->getNextOccurence()->format(DATE_ISO8601),'2112-12-30T23:01:01+0000');
		$this->assertEquals($rep->getFreq(),'HOURLY');
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['bymonth'],$rep->getBymonth());
		$this->assertEquals(['MO',2,'FR'],$rep->getByday());
		$this->assertEquals(['MO','WE','FR'],$rep->getByday(true));
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['bysetpos'],$rep->getBysetpos());
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['bymonthday'],$rep->getBymonthday());
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['byyearday'],$rep->getByyearday());
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['byweekno'],$rep->getByweekno());
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['wkst'],$rep->getWkst());
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['interval'],$rep->getInterval());
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['freq'],$rep->getFreq());
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['byhour'],$rep->getByhour());
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['byminute'],$rep->getByminute());
		$this->assertEquals($data['attributes']['recurringStatement']['repeated']['bysecond'],$rep->getBysecond());
	}

	public function testGetRecStat(){
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->basic_data);
		$request->query->set('include','recurringStatement');
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['relationships']);
		$this->assertNotEmpty($content['data']['relationships']['recurringStatement']);
		$this->assertNotEmpty($content['data']['relationships']['recurringStatement']['data']);
		$this->assertEquals(2,count($content['data']['relationships']['recurringStatement']['data']));
		$this->assertNotEmpty($content['included']);
		$this->assertEquals(2,count($content['included']));
	}

	public function testGetRecStat2(){
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->alt_data);
		$request->query->set('include','recurringStatement');
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['relationships']);
		$this->assertNotEmpty($content['data']['relationships']['recurringStatement']);
		$this->assertNotEmpty($content['data']['relationships']['recurringStatement']['data']);
		$this->assertEquals(2,count($content['data']['relationships']['recurringStatement']['data']));
		$this->assertNotEmpty($content['included']);
		$this->assertEquals(2,count($content['included']));

		foreach($content['included'] as $include){
			if($include['type']!=='accountRecurringStatements')continue;
			$atr = $include['attributes'];
			$dat = $this->alt_data['attributes']['recurringStatement']['attributes'];
			$this->assertEquals($dat['footer_text'],$atr['footerText']);
			$this->assertEquals($dat['messageText'],$atr['messageText']);
			$this->assertEquals($dat['termsAndConditionsText'],$atr['termsAndConditionsText']);
			$this->assertEquals($dat['send_zero_charge_statement'],$atr['sendZeroChargeStatement']);
			$this->assertEquals('percent',$atr['financeChargeType']);
			$this->assertEquals(0.00,$atr['financeChargeAmount']);
		}
	}

	public function testGetRecStat3(){
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->alt_data2);
		$request->query->set('include','recurringStatement');
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['relationships']);
		$this->assertNotEmpty($content['data']['relationships']['recurringStatement']);
		$this->assertNotEmpty($content['data']['relationships']['recurringStatement']['data']);
		$this->assertEquals(2,count($content['data']['relationships']['recurringStatement']['data']));
		$this->assertNotEmpty($content['included']);
		$this->assertEquals(2,count($content['included']));

		foreach($content['included'] as $include){
			if($include['type']!=='accountRecurringStatements')continue;
			$atr = $include['attributes'];
			$dat = $this->alt_data2['attributes']['recurringStatement']['attributes'];
			$this->assertEquals($dat['footerText'],$atr['footerText']);
			$this->assertEquals($dat['messageText'],$atr['messageText']);
			$this->assertEquals($dat['termsAndConditionsText'],$atr['termsAndConditionsText']);
			$this->assertEquals($dat['sendZeroChargeStatement'],$atr['sendZeroChargeStatement']);
			$this->assertEquals($dat['financeChargeAmount'],$atr['financeChargeAmount']);
			$this->assertEquals($dat['financeChargeType'],$atr['financeChargeType']);
			$this->assertEquals($dat['financeChargeAfterDays'],$atr['financeChargeAfterDays']);
			$this->assertEquals($dat['sendZeroChargeStatement'],$atr['financeChargeEnabled']);
			$this->assertEquals($dat['dueAfterDays'],$atr['dueAfterDays']);
			$this->assertEquals($dat['autopayAttempts'],$atr['autopayAttempts']);
			$this->assertEquals($dat['autopayEnabled'],$atr['autopayEnabled']);
			$this->assertEquals($dat['payMemberBalance'],$atr['payMemberBalance']);
			$this->assertEquals($dat['payCustomerBalance'],$atr['payCustomerBalance']);

		}
	}

	public function testGetRecStat4(){
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->alt_data3);
		$request->query->set('include','recurringStatement');
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['relationships']);
		$this->assertNotEmpty($content['data']['relationships']['recurringStatement']);
		$this->assertNotEmpty($content['data']['relationships']['recurringStatement']['data']);
		$this->assertEquals(2,count($content['data']['relationships']['recurringStatement']['data']));
		$this->assertNotEmpty($content['included']);
		$this->assertEquals(2,count($content['included']));

		foreach($content['included'] as $include){
			if($include['type']!=='accountRecurringStatements')continue;
			$atr = $include['attributes'];
			$dat = $this->alt_data3['attributes']['recurringStatement']['attributes'];
			$this->assertEquals($dat['footerText'],$atr['footerText']);
			$this->assertEquals($dat['messageText'],$atr['messageText']);
			$this->assertEquals($dat['termsAndConditionsText'],$atr['termsAndConditionsText']);
			$this->assertEquals($dat['sendZeroChargeStatement'],$atr['sendZeroChargeStatement']);
			$this->assertEquals($dat['financeChargeAmount'],$atr['financeChargeAmount']);
			$this->assertEquals($dat['financeChargeType'],$atr['financeChargeType']);
			$this->assertEquals($dat['financeChargeAfterDays'],$atr['financeChargeAfterDays']);
			$this->assertEquals($dat['sendZeroChargeStatement'],$atr['financeChargeEnabled']);
			$this->assertEquals($dat['dueAfterDays'],$atr['dueAfterDays']);
			$this->assertEquals($dat['autopayAttempts'],$atr['autopayAttempts']);
			$this->assertEquals($dat['autopayEnabled'],$atr['autopayEnabled']);
			$this->assertEquals($dat['payMemberBalance'],$atr['payMemberBalance']);
			$this->assertEquals($dat['payCustomerBalance'],$atr['payCustomerBalance']);

		}
	}

	public function testGetAll(){
		$request = new Request();

		// test basic usage
		//$request->query->set('limit',1);
		//$request->query->set('offset',1);
		//$request->query->set('includeDeleted',1);
		//$request->query->remove('limit');
		// how to test includes in the query?
		$this->controller->startTransaction();
		$result = $this->controller->getAll(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals($result->getStatusCode(),200);
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
	}

	public function testDelete(){
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->basic_data);
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringCharges',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$old_id = $content['data']['id'];
		$result = $this->controller->delete(6270,$old_id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['success']);
		$this->assertTrue(($content['data']['success']));
		$this->assertNotEmpty(($content['data']['content']));
		$this->assertEquals('recurring_charge deleted',$content['data']['content']);
		$request->query->set('includeDeleted',1);
		$result = $this->controller->get(6270,$old_id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);;
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringCharges',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$this->assertEquals($old_id,$content['data']['id']);
	}

	public function testUpdate(){
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->basic_data);
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringCharges',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$old_id = $content['data']['id'];
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['attributes']['name']);
		$this->assertEquals('valid name 2',$content['data']['attributes']['name']);
		$this->assertEquals(1,$content['data']['attributes']['prorate_charges']);

		$new_data = $content['data'];
		$new_data['attributes']['name'] = 'new name!';
		$new_data['attributes']['prorate_charges'] = false;
		$request->request->set('data',$new_data);

		$result = $this->controller->put(6270,$old_id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringCharges',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['attributes']['name']);
		$this->assertEquals('new name!',$content['data']['attributes']['name']);
		$this->assertEquals(0,$content['data']['attributes']['prorate_charges']);
	}

	public function testUpdateNonExist()
	{
		$request = new Request();

		$bogus_id = -1;

		$new_data = $this->basic_data;
		$new_data['attributes']['name'] = 'new name!';
		$new_data['attributes']['prorate_charges'] = false;
		$request->request->set('data',$new_data);
		$this->controller->startTransaction();
		$result = $this->controller->put(6270,$bogus_id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(404,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertEquals('No charge with that ID found',$content['title']);
		$this->assertFalse($content['success']);
	}

}