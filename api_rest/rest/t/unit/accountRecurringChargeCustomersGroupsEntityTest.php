<?php

namespace foreup\rest\unit;
use Aws\CloudFront\Exception\Exception;
use foreup\rest\models\entities\ForeupAccountRecurringChargeCustomers;
use foreup\rest\models\entities\ForeupAccountRecurringChargeGroups;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupGroups;

class testAccountRecurringChargeCustomerGroupsEntity  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	private $cust,$group,$rc;

	protected function _before()
	{
		$cust = new ForeupAccountRecurringChargeCustomers();
		$group = new ForeupAccountRecurringChargeGroups();
		$rc = new ForeupAccountRecurringCharges();
		$this->cust = &$cust;
		$cust->setRecurringCharge($rc);
		//$cust->setPersonId(1);
		$this->group = $group;
		$group->setRecurringCharge($rc);
		$group->setGroupId(1);
		$this->rc = $rc;
	}

	protected function _after()
	{
	}

	public function testCustomerInterface()
	{
		$cust = $this->cust;
		$this->assertTrue(method_exists($cust,'getId'));
		$this->assertTrue(method_exists($cust,'getRecurringCharge'));
		$this->assertTrue(method_exists($cust,'setRecurringCharge'));
		$this->assertTrue(method_exists($cust,'getDateAdded'));
		$this->assertTrue(method_exists($cust,'setDateAdded'));
		$this->assertTrue(method_exists($cust,'getCustomer'));
		$this->assertTrue(method_exists($cust,'setCustomer'));
	}

	public function testGroupInterface()
	{
		$group = $this->group;
		$this->assertTrue(method_exists($group,'getId'));
		$this->assertTrue(method_exists($group,'getRecurringCharge'));
		$this->assertTrue(method_exists($group,'setRecurringCharge'));
		$this->assertTrue(method_exists($group,'getInclude'));
		$this->assertTrue(method_exists($group,'setInclude'));
		$this->assertTrue(method_exists($group,'getGroupId'));
		$this->assertTrue(method_exists($group,'setGroupId'));
	}

	public function testCustomerGetSet()
	{
		$entity = &$this->cust;

		$this->assertEquals($entity->validate(false),'ForeupAccountRecurringChargeCustomers->validate Error: customer not found');
		$entity->setCustomer(new ForeupCustomers());
		$this->assertTrue($entity->validate());
		$this->assertEquals($entity->getRecurringCharge(),$this->rc);

		$entity->setRecurringCharge(new ForeupAccountRecurringCharges());
		$this->assertEquals($entity->getRecurringCharge(),new ForeupAccountRecurringCharges());
		$this->assertTrue($entity->validate());

		$entity->setCustomer(null);
		$this->assertEquals($entity->validate(false),'ForeupAccountRecurringChargeCustomers->validate Error: customer not found');

		$entity->setCustomer(new ForeupCustomers());
		$this->assertEquals($entity->getCustomer(),new ForeupCustomers());
		$this->assertTrue($entity->validate());

	}

	public function testGroupGetSet()
	{
		$entity = $this->group;

		$this->assertEquals($entity->getRecurringCharge(),$this->rc);
		$this->assertEquals($entity->validate(false),'ForeupAccountRecurringChargeGroups->validate Error: group not found');
		$entity->setGroup(new ForeupGroups());
		$this->assertTrue($entity->validate());

		$entity->setRecurringCharge(new ForeupAccountRecurringCharges());
		$this->assertEquals($entity->getRecurringCharge(),new ForeupAccountRecurringCharges());
		$this->assertTrue($entity->validate());

		$entity->setGroup(null);
		$this->assertEquals($entity->validate(false),'ForeupAccountRecurringChargeGroups->validate Error: group not found');
		$entity->setGroup(new ForeupGroups());
		$this->assertEquals($entity->getGroup(),new ForeupGroups());
		$this->assertTrue($entity->validate());

	}
}