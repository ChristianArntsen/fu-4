<?php
namespace foreup\rest\unit;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use foreup\rest\t\Helpers\testing_uzr;
use foreup\rest\t\traits\mock_app_trait;


class testCoursesController  extends \Codeception\TestCase\Test
{
	use mock_app_trait;

	protected function _before()
	{
		$this->setupController('courses.controller');
	}

	protected function _after()
	{
		$this->tearDownController();
	}

	public function testGetAll(){
		$request = new Request();
		$result = $this->controller->getAll($request);

		$this->assertTrue(isset($result));
		$this->assertEquals($result->getStatusCode(),200);
	}

}