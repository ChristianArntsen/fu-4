<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupSalesPaymentsCreditCards;

class testSalesPaymentsCreditCardsEntity  extends EntityTestingUtility
{
	private $payment;

	protected function _before()
	{
		$this->payment = new ForeupSalesPaymentsCreditCards();
		$this->payment->setCourseId(1234);
		$this->payment->setToken('erw34yu89gsrhuiohgsioph3');
		$this->payment->setCardType('VISA');
		$this->payment->setMaskedAccount('xxxxxxxxxxxxxxxxx23425');
		$this->payment->setTranType('Sale');
		$this->payment->setAmount(0.00);
		$this->payment->setAuthAmount(0.00);
		$this->payment->setTaxAmount(0.00);
		$this->payment->setGratuityAmount(0.00);
	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$payment = $this->payment;
		$verbs = array('get','set');

		$this->assertTrue(method_exists($payment,'validate'));
		$this->assertTrue(method_exists($payment,'getInvoice'));
		$this->assertTrue(method_exists($payment,'getId'));
		$nouns = array('Cid','CourseId','MercuryId','MercuryPassword','EtsId','TranType','Amount',
			'AuthAmount','CardType','MaskedAccount','CardholderName','RefNo','OperatorId','TerminalName',
			'AuthCode','VoiceAuthCode','PaymentId','AcqRefData','ProcessData','Token','TokenUsed','AmountRefunded',
			'Frequency','ResponseCode','Status','StatusMessage','AvsResult','CvvResult','TaxAmount',
			'AvsAddress','AvsZip','PaymentIdExpired','CustomerCode','Memo','BatchNo','GratuityAmount',
			'Voided','InitiationTime','PrimaryInvoice','CartId','MobileGuid','SuccessCheckCount','CancelTime',
			'SignatureData','EmvPayment','ToBeVoided');

		$this->interface_tester($payment,$verbs,$nouns);


	}

	function testGetSet()
	{

		$payment = $this->payment;
		$location = 'ForeupSalesPaymentsCreditCards->validate';
		$this->assertTrue($payment->validate());

		$this->integer_tester($payment,$location,'cid','getCid','setCid',true);
		$this->integer_tester($payment,$location,'courseId','getCourseId','setCourseId',true);
		$this->string_tester($payment,$location,'mercuryId','getMercuryId','setMercuryId',true);
		$this->string_tester($payment,$location,'mercuryPassword','getMercuryPassword','setMercuryPassword',true);
		$this->string_tester($payment,$location,'etsId','getEtsId','setEtsId',true);
		$this->string_tester($payment,$location,'tranType','getTranType','setTranType',true);
		$this->numeric_tester($payment,$location,'amount','getAmount','setAmount',true);
		$this->numeric_tester($payment,$location,'authAmount','getAuthAmount','setAuthAmount',true);
		$this->string_tester($payment,$location,'cardType','getCardType','setCardType',true);
		$this->string_tester($payment,$location,'maskedAccount','getMaskedAccount','setMaskedAccount',true);
		$this->string_tester($payment,$location,'cardholderName','getCardholderName','setCardholderName',true);
		$this->string_tester($payment,$location,'refNo','getRefNo','setRefNo',true);
		$this->string_tester($payment,$location,'operatorId','getOperatorId','setOperatorId',true);
		$this->string_tester($payment,$location,'terminalName','getTerminalName','setTerminalName',true);
		$this->datetime_tester($payment,$location,'transPostTime','getTransPostTime','setTransPostTime',true);
		$this->string_tester($payment,$location,'authCode','getAuthCode','setAuthCode',true);
		$this->string_tester($payment,$location,'voiceAuthCode','getVoiceAuthCode','setVoiceAuthCode',true);
		$this->string_tester($payment,$location,'paymentId','getPaymentId','setPaymentId',true);
		$this->string_tester($payment,$location,'acqRefData','getAcqRefData','setAcqRefData',true);
		$this->string_tester($payment,$location,'processData','getProcessData','setProcessData',true);
		$this->string_tester($payment,$location,'token','getToken','setToken',true);
		$this->boolean_tester($payment,$location,'tokenUsed','getTokenUsed','setTokenUsed',true);
		$this->numeric_tester($payment,$location,'amountRefunded','getAmountRefunded','setAmountRefunded',true);
		$this->string_tester($payment,$location,'frequency','getFrequency','setFrequency',true);
		$this->integer_tester($payment,$location,'responseCode','getResponseCode','setResponseCode',true);
		$this->string_tester($payment,$location,'status','getStatus','setStatus',true);
		$this->string_tester($payment,$location,'statusMessage','getStatusMessage','setStatusMessage',true);
		$this->string_tester($payment,$location,'displayMessage','getDisplayMessage','setDisplayMessage',true);
		$this->string_tester($payment,$location,'avsResult','getAvsResult','setAvsResult',true);
		$this->string_tester($payment,$location,'cvvResult','getCvvResult','setCvvResult',true);
		$this->numeric_tester($payment,$location,'taxAmount','getTaxAmount','setTaxAmount',true);
		$this->string_tester($payment,$location,'avsAddress','getAvsAddress','setAvsAddress',true);
		$this->string_tester($payment,$location,'avsZip','getAvsZip','setAvsZip',true);
		$this->string_tester($payment,$location,'paymentIdExpired','getPaymentIdExpired','setPaymentIdExpired',true);
		$this->string_tester($payment,$location,'customerCode','getCustomerCode','setCustomerCode',true);
		$this->string_tester($payment,$location,'memo','getMemo','setMemo',true);
		$this->string_tester($payment,$location,'batchNo','getBatchNo','setBatchNo',true);
		$this->numeric_tester($payment,$location,'gratuityAmount','getGratuityAmount','setGratuityAmount',true);
		$this->boolean_tester($payment,$location,'voided','getVoided','setVoided',true);
		$this->datetime_tester($payment,$location,'initiationTime','getInitiationTime','setInitiationTime',true);
		$this->integer_tester($payment,$location,'primaryInvoice','getPrimaryInvoice','setPrimaryInvoice',true);
		$this->integer_tester($payment,$location,'cartId','getCartId','setCartId',true);
		$this->string_tester($payment,$location,'mobileGuid','getMobileGuid','setMobileGuid',true);
		$this->boolean_tester($payment,$location,'successCheckCount','getSuccessCheckCount','setSuccessCheckCount',true);
		$this->datetime_tester($payment,$location,'cancelTime','getCancelTime','setCancelTime',true);
		$this->string_tester($payment,$location,'signatureData','getSignatureData','setSignatureData',true);
		$this->boolean_tester($payment,$location,'emvPayment','getEmvPayment','setEmvPayment',false);
		$this->boolean_tester($payment,$location,'toBeVoided','getToBeVoided','setToBeVoided',true);

	}

}
?>
