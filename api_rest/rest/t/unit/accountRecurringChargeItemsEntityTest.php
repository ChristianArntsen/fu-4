<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupAccountRecurringChargeItems;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupItems;
use foreup\rest\models\entities\ForeupRepeatable;
use foreup\rest\models\entities\ForeupRepeatableAccountRecurringChargeItems;

class testAccountRecurringChargeItemsEntity  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	private $item;

	protected function _before()
	{
		$item = new ForeupAccountRecurringChargeItems();
		$itm = new ForeupItems();
		$itm->testing_override('itemId',5159);
		$item->setItem($itm);
		$item->setRecurringCharge(new ForeupAccountRecurringCharges());
		$this->item = $item;
	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$item = $this->item;

		$this->assertTrue(method_exists($item,'getId'));
		$this->assertTrue(method_exists($item,'getRepeated'));
		$this->assertTrue(method_exists($item,'setRepeated'));
		$this->assertTrue(method_exists($item,'getLineNumber'));
		$this->assertTrue(method_exists($item,'setLineNumber'));
		$this->assertTrue(method_exists($item,'getItemId'));
		$this->assertTrue(method_exists($item,'setItem'));
		$this->assertTrue(method_exists($item,'getItem'));
		$this->assertTrue(method_exists($item,'getItemType'));
		$this->assertTrue(method_exists($item,'setItemType'));
		$this->assertTrue(method_exists($item,'getOverridePrice'));
		$this->assertTrue(method_exists($item,'setOverridePrice'));
		$this->assertTrue(method_exists($item,'getDiscountPercent'));
		$this->assertTrue(method_exists($item,'setDiscountPercent'));
		$this->assertTrue(method_exists($item,'getQuantity'));
		$this->assertTrue(method_exists($item,'setQuantity'));
		$this->assertTrue(method_exists($item,'getRecurringCharge'));
		$this->assertTrue(method_exists($item,'setRecurringCharge'));
	}

	public function testGetSet()
	{
		$item = $this->item;

		$item->setLineNumber('bob');
		$this->assertEquals($item->validate(false),'ForeupaccountRecurringChargeItems->validate Error: lineNumber must be an integer: bob');
		$item->setLineNumber(null);
		$this->assertEquals($item->validate(false),'ForeupaccountRecurringChargeItems->validate Error: lineNumber cannot be NULL');
		$item->setLineNumber(123);
		$this->assertEquals($item->getLineNumber(),123);
		$this->assertTrue($item->validate());
		$item->setLineNumber('456');
		$this->assertEquals($item->getLineNumber(),'456');
		$this->assertTrue($item->validate());

		$item->setRepeated(null);
		$this->assertTrue($item->validate());
		$item->setRepeated(new ForeupRepeatableAccountRecurringChargeItems());
		$this->assertEquals($item->getRepeated(),new ForeupRepeatableAccountRecurringChargeItems());
		$this->assertTrue($item->validate());

		/*
		$item->setItemId('bob');
		$this->assertEquals($item->validate(false),'ForeupaccountRecurringChargeItems->validate Error: itemId must be an integer: bob');
		$item->setItemId(null);
		$this->assertEquals($item->validate(false),'ForeupaccountRecurringChargeItems->validate Error: itemId cannot be NULL');
		$item->setItemId(123);
		$this->assertEquals($item->getItemId(),123);
		$this->assertTrue($item->validate());
		$item->setItemId('456');
		$this->assertEquals($item->getItemId(),'456');
		$this->assertTrue($item->validate());
		*/
		$item->setItemType('bob');
		$this->assertEquals($item->validate(false),'ForeupaccountRecurringChargeItems->validate Error: itemType "bob" not found in enumerated values: ["item"]');
		$item->setItemType('item');
		$this->assertEquals($item->getItemType(),'item');
		$this->assertTrue($item->validate());

		$this->assertNull($item->getOverridePrice());
		$item->setOverridePrice('bob');
		$this->assertEquals($item->validate(false),'ForeupaccountRecurringChargeItems->validate Error: overridePrice must be numeric: bob');
		$item->setOverridePrice(null);
		$this->assertTrue($item->validate());
		$item->setOverridePrice(123);
		$this->assertEquals($item->getOverridePrice(),123);
		$this->assertTrue($item->validate());
		$item->setOverridePrice('456');
		$this->assertEquals($item->getOverridePrice(),'456');
		$this->assertTrue($item->validate());
		$item->setOverridePrice(123.45);
		$this->assertEquals($item->getOverridePrice(),123.45);
		$this->assertTrue($item->validate());
		$item->setOverridePrice('456.78');
		$this->assertEquals($item->getOverridePrice(),'456.78');
		$this->assertTrue($item->validate());

		$this->assertEquals($item->getDiscountPercent(),'0.00');
		$item->setDiscountPercent('bob');
		$this->assertEquals($item->validate(false),'ForeupaccountRecurringChargeItems->validate Error: discountPercent must be numeric: bob');
		$item->setDiscountPercent(null);
		$this->assertEquals($item->validate(false),'ForeupaccountRecurringChargeItems->validate Error: discountPercent cannot be NULL');
		$item->setDiscountPercent(123);
		$this->assertEquals($item->getDiscountPercent(),123);
		$this->assertTrue($item->validate());
		$item->setDiscountPercent('456');
		$this->assertEquals($item->getDiscountPercent(),'456');
		$this->assertTrue($item->validate());
		$item->setDiscountPercent(123.45);
		$this->assertEquals($item->getDiscountPercent(),123.45);
		$this->assertTrue($item->validate());
		$item->setDiscountPercent('456.78');
		$this->assertEquals($item->getDiscountPercent(),'456.78');
		$this->assertTrue($item->validate());

		$this->assertEquals($item->getQuantity(),'1.00');
		$item->setQuantity('bob');
		$this->assertEquals($item->validate(false),'ForeupaccountRecurringChargeItems->validate Error: quantity must be numeric: bob');
		$item->setQuantity(null);
		$this->assertEquals($item->validate(false),'ForeupaccountRecurringChargeItems->validate Error: quantity cannot be NULL');
		$item->setQuantity(123);
		$this->assertEquals($item->getQuantity(),123);
		$this->assertTrue($item->validate());
		$item->setQuantity('456');
		$this->assertEquals($item->getQuantity(),'456');
		$this->assertTrue($item->validate());
		$item->setQuantity(123.45);
		$this->assertEquals($item->getQuantity(),123.45);
		$this->assertTrue($item->validate());
		$item->setQuantity('456.78');
		$this->assertEquals($item->getQuantity(),'456.78');
		$this->assertTrue($item->validate());

	}
}

?>