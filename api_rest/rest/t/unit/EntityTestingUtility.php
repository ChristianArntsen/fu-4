<?php

namespace foreup\rest\unit;

abstract class EntityTestingUtility  extends \Codeception\TestCase\Test
{
	protected function get_set_tester($object,$location,$fields,$get_set = ['get','set']){
		foreach($fields as $field){
			if($field[0]==='enum')
				$this->{$field[0].'_tester'}($object,$location,lcfirst($field[1]),$field[3],$get_set[0].ucfirst($field[1]),$get_set[1].ucfirst($field[1]),$field[2]);
			else
			    $this->{$field[0].'_tester'}($object,$location,lcfirst($field[1]),$get_set[0].ucfirst($field[1]),$get_set[1].ucfirst($field[1]),$field[2]);
		}
	}

	protected function interface_tester($object,$verb_prefixes,$noun_postfixes) {
		foreach($verb_prefixes as $prefix)
			foreach($noun_postfixes as $postfix)
				$this->assertTrue(method_exists($object,$prefix.$postfix),$prefix.$postfix.' should exist');
	}

	protected function enum_tester($entity,$location,$name,$options,$getter,$setter,$required = false)
	{
		//$this->string_tester($entity,$location,$name,$getter,$setter,$required);
		$entity->{$setter}('_probably_not_valid_value_');
		$this->assertEquals($location.' Error: "'.$name.'" value not found in enum values: _probably_not_valid_value_',$entity->validate(false));
		foreach($options as $option){
			$entity->{$setter}($option);
			$this->assertTrue($entity->validate());
		}
	}

    protected function array_tester($entity,$location,$name,$getter,$setter,$required = false)
    {
        $entity->{$setter}(null);
        if($required)
            $this->assertEquals($entity->validate(false),$location.' Error: "'.$name.'" cannot be NULL');
        else
            $this->assertTrue($entity->validate());

        $entity->{$setter}([1,2]);
        $this->assertTrue( is_array($entity->{$getter}()), $name.' is not a valid array');
    }

	protected function string_tester($entity,$location,$name,$getter,$setter,$required = false)
	{
		$entity->{$setter}(null);
		if($required)
			$this->assertEquals($entity->validate(false),$location.' Error: "'.$name.'" cannot be NULL');
		else
			$this->assertTrue($entity->validate());

		$entity->{$setter}(100);
		$this->assertEquals($location.' Error: "'.$name.'" must be a string: 100',$entity->validate(false));
		$this->assertEquals($entity->{$setter}('valid string'),$entity,$setter);
		$this->assertEquals($entity->{$getter}(),'valid string',$getter);
		$this->assertTrue($entity->validate());
	}

	protected function numeric_tester($entity,$location,$name,$getter,$setter,$required = false)
	{

		$entity->{$setter}('bob');
		$this->assertEquals($entity->validate(false),$location.' Error: "'.$name.'" must be numeric: bob');

		$entity->{$setter}(null);
		if($required)
			$this->assertEquals($entity->validate(false),$location.' Error: "'.$name.'" cannot be NULL');
		else
			$this->assertTrue($entity->validate());

		$entity->{$setter}(123);
		$this->assertEquals($entity->{$getter}(),123);
		$this->assertTrue($entity->validate());
		$entity->{$setter}('456');
		$this->assertEquals($entity->{$getter}(),'456');
		$this->assertTrue($entity->validate());
		$entity->{$setter}(123.45);
		$this->assertEquals($entity->{$getter}(),123.45);
		$this->assertTrue($entity->validate());
		$entity->{$setter}('456.78');
		$this->assertEquals($entity->{$getter}(),'456.78');
		$this->assertTrue($entity->validate());
	}

	protected function integer_tester($entity,$location,$name,$getter,$setter,$required = false)
	{

		$entity->{$setter}('bob');
		$this->assertEquals($entity->validate(false),$location.' Error: "'.$name.'" must be an integer: bob');
		$entity->{$setter}(123.456);
		$this->assertEquals($entity->validate(false),$location.' Error: "'.$name.'" must be an integer: 123.456');

		$entity->{$setter}(null);
		if($required)
		  $this->assertEquals($entity->validate(false),$location.' Error: "'.$name.'" cannot be NULL');
		else
			$this->assertTrue($entity->validate());

		$entity->{$setter}(123);
		$this->assertEquals($entity->{$getter}(),123);
		$this->assertTrue($entity->validate());
		$entity->{$setter}('456');
		$this->assertEquals($entity->{$getter}(),'456');
		$this->assertTrue($entity->validate());
	}

	protected function DateTime_tester($entity,$location,$name,$getter,$setter,$required = false)
	{
		$entity->{$setter}('bob');
		$this->assertEquals($entity->validate(false),$location.' Error: "'.$name.'" must be of type "\DateTime": bob');

		$entity->{$setter}(null);
		if($required)
		    $this->assertEquals($entity->validate(false),$location.' Error: "'.$name.'" cannot be NULL');
		else
			$this->assertTrue($entity->validate());

		$entity->{$setter}(new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertEquals($entity->{$getter}(),new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertTrue($entity->validate());
	}

	protected function boolean_tester($entity,$location,$name,$getter,$setter,$required = false){

		$entity->{$setter}(null);
		if($required)
			$this->assertEquals($location.' Error: "'.$name.'" cannot be NULL',$entity->validate(false));
		else
			$this->assertTrue($entity->validate());

		$entity->{$setter}('bob');
		$this->assertEquals($entity->validate(false),$location.' Error: ambiguous use of string in boolean context: bob');
		$entity->{$setter}(123);
		$this->assertEquals($entity->validate(false),$location.' Error: ambiguous use of numeric in boolean context: 123');
		$entity->{$setter}('456');
		$this->assertEquals($entity->validate(false),$location.' Error: ambiguous use of numeric in boolean context: 456');
		$entity->{$setter}('0');
		$this->assertEquals($entity->{$getter}(),0);
		$this->assertTrue($entity->validate(),$setter);
		$entity->{$setter}(0);
		$this->assertEquals($entity->{$getter}(),0);
		$entity->{$setter}('1');
		$this->assertEquals($entity->{$getter}(),1);
		$this->assertTrue($entity->validate());
		$entity->{$setter}(1);
		$this->assertEquals($entity->{$getter}(),1);
		$this->assertTrue($entity->validate());
		$entity->{$setter}('false');
		$this->assertFalse($entity->{$getter}(),$getter);
		$this->assertTrue($entity->validate(),$getter);
		$entity->{$setter}(false);
		$this->assertFalse($entity->{$getter}());
		$this->assertTrue($entity->validate());
		$entity->{$setter}('true');
		$this->assertTrue($entity->{$getter}());
		$this->assertTrue($entity->validate());
		$entity->{$setter}(true);
		$this->assertTrue($entity->{$getter}());
		$this->assertTrue($entity->validate());
		$entity->{$setter}('FALSE');
		$this->assertFalse($entity->{$getter}());
		$this->assertTrue($entity->validate());
		$entity->{$setter}('TRUE');
		$this->assertTrue($entity->{$getter}());
		$this->assertTrue($entity->validate());
		$entity->{$setter}('False');
		$this->assertFalse($entity->{$getter}());
		$this->assertTrue($entity->validate());
		$entity->{$setter}('True');
		$this->assertTrue($entity->{$getter}());
		$this->assertTrue($entity->validate());
	}
}