<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupCustomerCreditCards;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupInvoiceChargeAttempts;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\models\entities\ForeupSalesPaymentsCreditCards;

require_once ('EntityTestingUtility.php');

class testInvoiceChargeAttemptsEntity  extends EntityTestingUtility
{
	private $charge;

	protected function _before()
	{
		$this->charge = new ForeupInvoiceChargeAttempts();
		$this->charge->setCreditCardCharge(new ForeupSalesPaymentsCreditCards());
		$this->charge->setCreditCard(new ForeupCustomerCreditCards());
		$this->charge->setStatement(new ForeupInvoices());
		$this->charge->setEmployee(new ForeupEmployees());
		$this->charge->setSuccess('');
		$this->charge->setDate(new \DateTime());


	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$charge = $this->charge;

		$this->assertTrue(method_exists($charge,'validate'));
		$this->assertTrue(method_exists($charge,'getChargeAttemptId'));
		$verbs = ['get','set'];
		$nouns = ['CreditCardCharge','Statement','CreditCard','Amount',
			      'Date','Success','StatusMessage','Employee'];

		$this->interface_tester($charge,$verbs,$nouns);
	}

	public function testGetSet()
	{
		$charge = $this->charge;
		$location = 'ForeupStatementChargeAttempts->validate';

		$this->assertTrue($charge->validate());

		$this->numeric_tester($charge,$location,'amount','getAmount','setAmount',true);
		$this->datetime_tester($charge,$location,'date','getDate','setDate',true);
		$this->string_tester($charge,$location,'success','getSuccess','setSuccess',true);
		$this->string_tester($charge,$location,'statusMessage','getStatusMessage','setStatusMessage',true);
	}
}

?>