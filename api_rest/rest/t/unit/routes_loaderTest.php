<?php
namespace foreup\rest\unit;

use Dflydev\Pimple\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Application;
use foreup\rest\t\Helpers\testing_uzr;
use foreup\rest\t\Helpers\testing_middleware_loader;
use foreup\rest\t\Helpers\testing_config;
use foreup\rest\t\Helpers\testing_services_loader;
use foreup\rest\t\Helpers\testing_routes_loader;

class test_routes_loader  extends \Codeception\TestCase\Test
{
	private $app,$middleware_loader,$configuration,$routes_loader,$services_loader;

	protected function createMockDefaultAppAndDeps()
	{
		include '../../config/development/database.php';
		$app = new Application();

		$eventManager = $this->getMock('Doctrine\Common\EventManager');

		$connection = $this
			->getMockBuilder('Doctrine\DBAL\Connection')
			->disableOriginalConstructor()
			->getMock();

		$connection
			->expects($this->any())
			->method('getEventManager')
			->will($this->returnValue($eventManager));

		$app['user'] = 12;
		$app['auth.userObj'] = new testing_uzr(5);
		$app['auth.user'] = array(
			'loggedIn'=>true,
			'validated'=>true,
			'authorized'=>true,
			'uid'=>12,
			'cid'=>6270,
			'level'=>(int)5,
			'emp_id'=>12,
			'isEmployee'=>1
		);

		return array($app, $connection, $eventManager);
	}

	protected function createMockDefaultApp()
	{
		list ($app, $connection, $eventManager) = $this->createMockDefaultAppAndDeps();

		return $app;
	}

	protected function _before()
	{
		$this->app = $this->createMockDefaultApp();
		$doctrineOrmServiceProvider = new DoctrineOrmServiceProvider();
		$doctrineOrmServiceProvider->register($this->app);
		$this->configuration = new testing_config($this->app);
		$this->configuration->loadConfig();
		$this->middleware_loader = new testing_middleware_loader($this->app);
		$this->middleware_loader->load_middleware();
		$this->services_loader = new testing_services_loader($this->app);
		$this->services_loader->register_services();
		$this->routes_loader = new testing_routes_loader($this->app);
		$this->routes_loader->bindRoutesToControllers();
	}

	protected function _after()
	{
		$conn = $this->app['orm.em']->getConnection();
		$conn->close();
	}

	public function testRoutesLoaded(){
		$this->assertTrue(method_exists($this->routes_loader,'__construct'));
		$this->assertTrue(method_exists($this->routes_loader,'instantiateControllers'));
		$this->assertTrue(method_exists($this->routes_loader,'bindRoutesToControllers'));
		$this->assertFalse(isset($this->app['organizations.controller']));
		$this->assertTrue(isset($this->app['courses.controller']));
		$this->assertTrue(isset($this->app['sales.controller']));
		$this->assertTrue(isset($this->app['teesheets.controller']));
		$this->assertTrue(isset($this->app['modules.controller']));
		$this->assertTrue(isset($this->app['marketing_campaigns.controller']));
		$this->assertTrue(isset($this->app['tokens.controller']));
		$this->assertTrue(isset($this->app['scores.controller']));
		$this->assertTrue(isset($this->app['employee_audit_log.controller']));
		$this->assertTrue(isset($this->app['person_altered.controller']));
		$this->assertTrue(isset($this->app['account_ledger.controller']));
		$this->assertTrue(isset($this->app['accounts.controller']));
		$this->assertTrue(isset($this->app['account_types.controller']));
		$this->assertTrue(isset($this->app['account_type_statements.controller']));
		$this->assertTrue(isset($this->app['customers.controller']));
		$this->assertTrue(isset($this->app['customerSales.controller']));
		$this->assertTrue(isset($this->app['items.controller']));
		$this->assertTrue(isset($this->app['inventory.controller']));
		$this->assertTrue(isset($this->app['terminals.controller']));
		$this->assertTrue(isset($this->app['employees.controller']));
		$this->assertTrue(isset($this->app['courseTeesheets.controller']));
		$this->assertTrue(isset($this->app['hooks.controller']));
		$this->assertTrue(isset($this->app['teesheetBookings.controller']));
		$this->assertTrue(isset($this->app['teesheetTeetimes.controller']));
		$this->assertTrue(isset($this->app['teesheetStats.controller']));
		$this->assertTrue(isset($this->app['accountRecurringCharges.controller']));
		$this->assertTrue(isset($this->app['accountRecurringChargeItems.controller']));
	}

	public function testServicesLoaded(){
		$this->assertTrue(method_exists($this->services_loader,'__construct'));
		$this->assertTrue(method_exists($this->services_loader,'register_services'));
		$this->assertTrue(isset($this->app['orm.em']));
		$this->assertTrue(isset($this->app['config.db.options']));
		$this->assertTrue(isset($this->app['config.orm.auto_generate_proxies']));
		$this->assertTrue(isset($this->app['config.jwt_secretKey']));
		$this->assertTrue(isset($this->app['config.jwt_issuer']));
		$this->assertTrue(isset($this->app['config.jwt_audience']));
		$this->assertTrue(isset($this->app['config.jwt_expiresAfter']));
		$this->assertTrue(isset($this->app['config.jwt_cookie_expire']));
		$this->assertTrue(isset($this->app['config.jwt_cookie_path']));
		$this->assertTrue(isset($this->app['config.jwt_cookie_domain']));
		$this->assertTrue(isset($this->app['teesheetStats']));
		$this->assertTrue(isset($this->app['legacySaleService']));
		$this->assertTrue(isset($this->app['legacyTeetimeService']));
	}

	public function testMiddlewareLoaded(){
		$this->assertTrue(method_exists($this->middleware_loader,'__construct'));
		$this->assertTrue(method_exists($this->middleware_loader,'load_middleware'));
	}

	public function testConfigloaded(){
		$this->assertTrue(method_exists($this->configuration,'__construct'));
		$this->assertTrue(method_exists($this->configuration,'loadConfig'));
		$this->assertTrue(method_exists($this->configuration,'loadExternalConfigDir'));
		$this->assertTrue(method_exists($this->configuration,'loadConfigFiles'));
		$this->assertTrue(method_exists($this->configuration,'processBuiltInSettings'));
		$this->assertTrue(method_exists($this->configuration,'configDirectoryNotFound'));
	}

	public function testConstants(){
		$this->assertTrue(defined('ENVIRONMENT'));
		$this->assertEquals(ENVIRONMENT, 'testing');
	}
}
?>