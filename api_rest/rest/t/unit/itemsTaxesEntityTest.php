<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupItemsTaxes;

class testItemsTaxes  extends EntityTestingUtility
{
	private $tax;

	protected function _before()
	{
		$this->tax = new ForeupItemsTaxes();
		$this->tax->setCourseId(123);
		$this->tax->setCid(456);
		$this->tax->setName('bob');
		$this->tax->setPercent(7.89);

	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$tax = $this->tax;

		$this->assertTrue(method_exists($tax,'validate'));

		$verbs = ['get','set'];
		$nouns = ['Item','Cumulative','Cid','CourseId','Percent','Name'];

		$this->interface_tester($tax,$verbs,$nouns);

	}

	public function testGetSet(){
		$tax = $this->tax;
		$location = 'ForeupItemsTaxes->validate';
		$this->assertTrue($tax->validate());

		$fields = [['integer','courseId',true],['integer','cid',true],['string','name',true],
			['numeric','percent',true],['boolean','cumulative',true]];
		$this->assertEquals(5,count($fields));
		$this->get_set_tester($tax,$location,$fields);

	}

}