<?php
namespace foreup\rest\unit;

use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupRepeatableAccountRecurringStatements;
use foreup\rest\models\entities\ForeupUsers;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\services\StatementUtilities;
use foreup\rest\t\traits\mock_app_trait;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class accountStatementsControllerTest extends \Codeception\TestCase\Test
{
	use mock_app_trait;

	protected function _before()
	{
		$this->setupController('accountStatements.controller');
	}

	protected function _after()
	{
		$this->tearDownController();
	}

	private function setup_stuff($controller)
	{
		// get the entity manager
		$db = $controller->db;

		// create recurring statement
		$recStatement = new ForeupAccountRecurringStatements();
		$employee = $db->getRepository('e:ForeupEmployees')->find(12);
		$recStatement->setCreatedBy($employee);
		$recStatement->setOrganizationId(6270);
		$recStatement->setDateCreated(new \DateTime());

		$db->persist($recStatement);
		$db->flush();

		// create recurring
		$repeatable = new ForeupRepeatableAccountRecurringStatements();
		$repeatable->setStatement($recStatement);
		$repeatable->setFreq('MONTHLY');
		$repeatable->setDtstart(new \DateTime('1817-01-01 07:00:00'));
		$repeatable->setBymonthday([1]);

		$db->persist($repeatable);
		$db->flush();

		$recStatement->setRepeated($repeatable);

		// create statement
		$statement = new ForeupAccountStatements();
		$statement->setRecurringStatement($recStatement);
		$statement->setOrganizationId(6270);
		$statement->setNumber(0);
		$statement->setDateCreated(new \DateTime('1817-03-02 18:15:03'));
		$statement->setDueDate(new \DateTime('1817-03-17 18:15:03'));
		$statement->setCreatedBy(12);
		$customer = $db->getRepository('e:ForeupCustomers')->findOneBy(['personId'=>112,'courseId'=>6270]);
		$statement->setCustomer($customer);
		$statement->setStartDate(new \DateTime('1817-02-01 00:00:00'));
		$statement->setEndDate(new \DateTime('1817-03-01 00:00:00'));

		$db->persist($statement);
		$db->flush();

		// return id of statement
		return $statement->getId();
	}

	public function testGet()
	{
		$request = new Request();

		$id = $this->setup_stuff($this->controller);

		$request->query->set('include','accountRecurringStatement');
		$result = $this->controller->get(6270,$id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountStatements',$content['data']['type']);
		//$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['relationships']);
		$this->assertNotEmpty($content['data']['relationships']['accountRecurringStatement']);
		$this->assertNotEmpty($content['data']['relationships']['accountRecurringStatement']['data']);
		$this->assertEquals(2,
			count($content['data']['relationships']['accountRecurringStatement']['data']));
		$this->assertNotEmpty($content['included']);
		$this->assertEquals(2,count($content['included']));
	}

	public function testDelete()
	{
		$request = new Request();

		$id = $this->setup_stuff($this->controller);

		$result = $this->controller->delete(6270,$id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['success']);
		$this->assertTrue(($content['data']['success']));
		$this->assertNotEmpty(($content['data']['content']));
		$this->assertEquals('recurring_charge deleted',$content['data']['content']);
		$request->query->set('include','accountRecurringStatement');
		$result = $this->controller->get(6270,$id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(404,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertEmpty($content['data']);
		$request->query->set('includeDeleted',1);
		$request->query->set('include','accountRecurringStatement');
		$result = $this->controller->get(6270,$id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountStatements',$content['data']['type']);
		//$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['attributes']['deletedBy']);
		$this->assertNotEmpty($content['data']['attributes']['dateDeleted']);
		$this->assertNotEmpty($content['data']['relationships']);
		$this->assertNotEmpty($content['data']['relationships']['accountRecurringStatement']);
		$this->assertNotEmpty($content['data']['relationships']['accountRecurringStatement']['data']);
		$this->assertEquals(2,
			count($content['data']['relationships']['accountRecurringStatement']['data']));
		$this->assertNotEmpty($content['included']);
		$this->assertEquals(2,count($content['included']));
	}

	public function testGetStatementPdf()
	{
		$request = new Request();

		$whazit = $this->app['statementUtilities'];

		$result = $this->controller->getStatementPdf(6270,1);

		$this->assertNotEmpty($result);

		$this->assertEquals(200,$result->getStatusCode());

		$this->assertInstanceOf('Symfony\Component\HttpFoundation\BinaryFileResponse',$result);

		$file = $result->getFile();

		$this->assertEquals('/tmp/processStatementPdfJobs/6270/1/2017-03-03/3399.pdf',$file->getPathname());
	}


}
?>