<?php
namespace foreup\rest\unit;

use Dflydev\Pimple\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupUsers;
use foreup\rest\t\traits\mock_app_trait;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use foreup\rest\t\Helpers\testing_uzr;
use foreup\rest\t\Helpers\MockDefaultAppFactory;
use foreup\rest\t\Helpers\testing_middleware_loader;
use foreup\rest\t\Helpers\testing_config;
use foreup\rest\t\Helpers\testing_services_loader;
use foreup\rest\t\Helpers\testing_routes_loader;

class testRecurringStatementsController  extends \Codeception\TestCase\Test
{
	use mock_app_trait;

	private $basic_data, $case1;

	protected function _before()
	{
		$this->setupController('accountRecurringStatements.controller');

		$this->basic_data = json_decode('{
			"type":"accountRecurringStatements",
            "attributes":{
				"name":"valid name 2",
	            "description":"valid description 2",
	            "isActive":1,
	            "isDefault":1,
	            "repeated":{
					"last_ran": "2016-12-12T11:11:11-0700",
			        "next_occurence": "2016-12-12T11:11:11-0700",
			        "freq": "SECONDLY",
			        "dtstart": "2016-12-12T11:11:11-0700",
			        "until": "2016-12-12T11:11:11-0700",
			        "interval": 2,
			        "wkst": "MO",
                    "byday": ["MO",2,"FR"],
                    "bymonth": [3,6,12],
                    "bysetpos": [1,5,11],
                    "bymonthday": [1,15,30],
                    "byyearday": [1,365],
                    "byweekno": [1,25,52],
                    "byhour": [1,12,23],
                    "byminute": [1,30,60],
                    "bysecond": [1,30,60]
			    }
            }
          }',true);
		
		$this->case1 = json_decode('
{
  "attributes": {
  "isDefault": true,
  "isActive": true,
      "sendEmptyStatements": true,
      "includePastDue": true,
      "includeMemberTransactions": true,
      "includeCustomerTransactions": true,
      "sendEmail": true,
      "sendMail": false,
      "dueAfterDays": 14,
      "autopayAttempts": 3,
      "autopayEnabled": true,
      "autopayAfterDays": 1,
      "autopayOverdueBalance": true,
      "payMemberBalance": "1",
      "payCustomerBalance": "1",
      "sendZeroChargeStatement": true,
      "financeChargeEnabled": true,
      "financeChargeAfterDays": 7,
      "financeChargeType": "percent",
      "financeChargeAmount": "5",
      "termsAndConditionsText": "<p>Terms and conditions here</p>",
      "footerText": "<p>Footer here</p>",
      "messageText": "<p>A message here</p>",
      "repeated": {
      "attributes": {
      "_stepsCompleted": 4,
      "period": "monthly",
      "dtstart": null,
      "freq": 1,
      "bymonth": null,
      "bymonthday": [1],
      "byweekday": null,
      "bysetpos": null,
      "interval": 1,
      "byday": null,
      "byhour": [23],
      "byminute": [59],
      "bysecond": [59]
      }
      },
      "_account_charge_settings_complete": false,
      "undefined": "",
      "interval": "1",
      "dtstart": "",
      "files": ""
      }
      }
      ',true);
	}

	protected function _after()
	{
		$this->tearDownController();
	}

	public function testCreate()
	{
		$request = new Request();

		$data = $this->basic_data;
		$data['attributes']['footerText'] = 'valid footer text';
		$data['attributes']['messageText'] = 'valid message text';
		$data['attributes']['termsAndConditionsText'] = 'valid terms and conditions text';
		$data['attributes']['isDefault'] = true;

		// test basic usage
		$request->request->set('data',$data);
		//$request->request->set('meta',array('test'=>1,'validate_only'=>1));
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals($result->getStatusCode(),200);
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringStatements',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$this->assertEquals('',$content['data']['attributes']['customerMessage']);
		$this->assertEquals(0,$content['data']['attributes']['includeMemberTransactions']);
		$this->assertEquals(0,$content['data']['attributes']['includeCustomerTransactions']);
		$this->assertEquals(0,$content['data']['attributes']['sendEmail']);
		$this->assertEquals(0,$content['data']['attributes']['sendMail']);
		$this->assertEquals(0,$content['data']['attributes']['includePastDue']);
		$this->assertEquals(0,$content['data']['attributes']['dueAfterDays']);
		$this->assertEquals(0,$content['data']['attributes']['autopayEnabled']);
		$this->assertEquals(0,$content['data']['attributes']['autopayAfterDays']);
		$this->assertEquals(0,$content['data']['attributes']['autopayAttempts']);
		$this->assertEquals(0,$content['data']['attributes']['autopayOverdueBalance']);
		$this->assertEquals(0,$content['data']['attributes']['payMemberBalance']);
		$this->assertEquals(0,$content['data']['attributes']['payCustomerBalance']);
		$this->assertEquals($data['attributes']['footerText'],$content['data']['attributes']['footerText']);
		$this->assertEquals($data['attributes']['messageText'],$content['data']['attributes']['messageText']);
		$this->assertEquals($data['attributes']['termsAndConditionsText'],$content['data']['attributes']['termsAndConditionsText']);
	}

	public function testCase1()
	{
		$request = new Request();

		$data = $this->case1;

		// test basic usage
		$request->request->set('data',$data);
		//$request->request->set('meta',array('test'=>1,'validate_only'=>1));
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals($result->getStatusCode(),200);
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringStatements',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));

		$dat = $data['attributes'];
		$atr = $content['data']['attributes'];

		$this->assertEquals('',$atr['customerMessage']);
		$this->assertEquals($dat['isDefault'],$atr['isDefault']);
		$this->assertEquals($dat['isActive'],$atr['isActive']);
		$this->assertEquals($dat['sendEmptyStatement'],$atr['sendEmptyStatement']);
		$this->assertEquals($dat['includePastDue'],$atr['includePastDue']);
		$this->assertEquals($dat['includeMemberTransactions'],$atr['includeMemberTransactions']);
		$this->assertEquals($dat['includeCustomerTransactions'],$atr['includeCustomerTransactions']);
		$this->assertEquals($dat['sendEmail'],$atr['sendEmail']);
		$this->assertEquals($dat['sendMail'],$atr['sendMail']);
		$this->assertEquals($dat['dueAfterDays'],$atr['dueAfterDays']);
		$this->assertEquals($dat['autopayAttempts'],$atr['autopayAttempts']);
		$this->assertEquals($dat['autopayEnabled'],$atr['autopayEnabled']);
		$this->assertEquals($dat['autopayAfterDays'],$atr['autopayAfterDays']);
		$this->assertEquals($dat['autopayOverdueBalance'],$atr['autopayOverdueBalance']);
		$this->assertEquals($dat['payMemberBalance'],$atr['payMemberBalance']);
		$this->assertEquals($dat['payCustomerBalance'],$atr['payCustomerBalance']);
		$this->assertEquals($dat['sendZeroChargeStatement'],$atr['sendZeroChargeStatement']);
		$this->assertEquals($dat['financeChargeEnabled'],$atr['financeChargeEnabled']);
		$this->assertEquals($dat['financeChargeAfterDays'],$atr['financeChargeAfterDays']);
		$this->assertEquals($dat['financeChargeType'],$atr['financeChargeType']);
		$this->assertEquals($dat['financeChargeAmount'],$atr['financeChargeAmount']);
		$this->assertEquals($dat['footerText'],$atr['footerText']);
		$this->assertEquals($dat['messageText'],$atr['messageText']);
		$this->assertEquals($dat['termsAndConditionsText'],$atr['termsAndConditionsText']);
	}

	public function testGet()
	{
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->basic_data);
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringStatements',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$old_id = $content['data']['id'];
		$request->query->set('include','items');
		$result = $this->controller->get(6270,$content['data']['id'],$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringStatements',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$this->assertEquals($old_id,$content['data']['id']);
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['relationships']);
		$this->assertNotEmpty($content['included']);
	}

	public function testGetAll()
	{
		$request = new Request();

		// test basic usage
		$this->controller->startTransaction();
		$result = $this->controller->getAll(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals($result->getStatusCode(),200);
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(is_array($content['data']));
		$this->assertTrue(count($content['data'])>0);
		$this->assertTrue(is_numeric($content['data'][0]['id']));
		$this->assertEquals('accountRecurringStatements',$content['data'][0]['type']);
		$this->assertTrue(is_array($content['data'][0]['attributes']));
	}

	public function testDelete()
	{
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->basic_data);
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringStatements',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$old_id = $content['data']['id'];
		$result = $this->controller->delete(6270,$old_id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['success']);
		$this->assertTrue(($content['data']['success']));
		$this->assertNotEmpty(($content['data']['content']));
		$this->assertEquals('accountRecurringStatements instance deleted',$content['data']['content']);
		$request->query->set('includeDeleted',1);
		$result = $this->controller->get(6270,$old_id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);;
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringStatements',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$this->assertEquals($old_id,$content['data']['id']);
	}

	public function testUpdate()
	{
		$request = new Request();

		// test basic usage
		$request->request->set('data',$this->basic_data);
		$this->controller->startTransaction();
		$result = $this->controller->create(6270,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringStatements',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$old_id = $content['data']['id'];
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['attributes']['name']);
		$this->assertEquals('valid name 2',$content['data']['attributes']['name']);
		$this->assertEquals(1,$content['data']['attributes']['isActive']);
		$this->assertEquals('',$content['data']['attributes']['customerMessage']);
		$this->assertEquals(0,$content['data']['attributes']['includeMemberTransactions']);
		$this->assertEquals(0,$content['data']['attributes']['includeCustomerTransactions']);
		$this->assertEquals(0,$content['data']['attributes']['sendEmail']);
		$this->assertEquals(0,$content['data']['attributes']['sendMail']);
		$this->assertEquals(0,$content['data']['attributes']['includePastDue']);
		$this->assertEquals(0,$content['data']['attributes']['dueAfterDays']);
		$this->assertEquals(0,$content['data']['attributes']['autopayEnabled']);
		$this->assertEquals(0,$content['data']['attributes']['autopayAfterDays']);
		$this->assertEquals(0,$content['data']['attributes']['autopayAttempts']);
		$this->assertEquals(0,$content['data']['attributes']['autopayOverdueBalance']);

		$new_data = $content['data'];
		$new_data['attributes']['isDefault'] = true;
		$new_data['attributes']['name'] = 'new name!';
		$new_data['attributes']['isActive'] = 1;
		$new_data['attributes']['customerMessage'] = 'new name!';
		$new_data['attributes']['includeMemberTransactions'] = 1;
		$new_data['attributes']['includeCustomerTransactions'] = 1;
		$new_data['attributes']['sendEmail'] = 1;
		$new_data['attributes']['sendMail'] = 1;
		$new_data['attributes']['includePastDue'] = 1;
		$new_data['attributes']['dueAfterDays'] = 3;
		$new_data['attributes']['autopayEnabled'] = 1;
		$new_data['attributes']['autopayAfterDays'] = 3;
		$new_data['attributes']['autopayAttempts'] = 3;
		$new_data['attributes']['autopayOverdueBalance'] = 1;
		$new_data['attributes']['payMemberBalance'] = 1;
		$new_data['attributes']['payCustomerBalance'] = 1;
		$new_data['attributes']['footerText'] = 'valid footer text';
		$new_data['attributes']['messageText'] = 'valid message text';
		$new_data['attributes']['termsAndConditionsText'] = 'valid terms and conditions text';



		$request->request->set('data',$new_data);

		$result = $this->controller->put(6270,$old_id,$request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('accountRecurringStatements',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['attributes']['name']);
		$this->assertEquals('new name!',$content['data']['attributes']['name']);
		$this->assertEquals(1,$content['data']['attributes']['isActive']);
		$this->assertEquals($new_data['attributes']['customerMessage'],$content['data']['attributes']['customerMessage']);
		$this->assertEquals($new_data['attributes']['includeMemberTransactions'],$content['data']['attributes']['includeMemberTransactions']);
		$this->assertEquals($new_data['attributes']['includeCustomerTransactions'],$content['data']['attributes']['includeCustomerTransactions']);
		$this->assertEquals($new_data['attributes']['sendEmail'],$content['data']['attributes']['sendEmail']);
		$this->assertEquals($new_data['attributes']['sendMail'],$content['data']['attributes']['sendMail']);
		$this->assertEquals($new_data['attributes']['includePastDue'],$content['data']['attributes']['includePastDue']);
		$this->assertEquals($new_data['attributes']['dueAfterDays'],$content['data']['attributes']['dueAfterDays']);
		$this->assertEquals($new_data['attributes']['autopayEnabled'],$content['data']['attributes']['autopayEnabled']);
		$this->assertEquals($new_data['attributes']['autopayAfterDays'],$content['data']['attributes']['autopayAfterDays']);
		$this->assertEquals($new_data['attributes']['autopayAttempts'],$content['data']['attributes']['autopayAttempts']);
		$this->assertEquals($new_data['attributes']['autopayOverdueBalance'],$content['data']['attributes']['autopayOverdueBalance']);
		$this->assertEquals($new_data['attributes']['payMemberBalance'],$content['data']['attributes']['payMemberBalance']);
		$this->assertEquals($new_data['attributes']['payCustomerBalance'],$content['data']['attributes']['payCustomerBalance']);
		$this->assertEquals($new_data['attributes']['footerText'],$content['data']['attributes']['footerText']);
		$this->assertEquals($new_data['attributes']['messageText'],$content['data']['attributes']['messageText']);
		$this->assertEquals($new_data['attributes']['termsAndConditionsText'],$content['data']['attributes']['termsAndConditionsText']);
	}
}
