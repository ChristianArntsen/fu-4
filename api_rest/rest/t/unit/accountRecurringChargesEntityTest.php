<?php

namespace foreup\rest\unit;
use Aws\CloudFront\Exception\Exception;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use Silex\Application;
use foreup\rest\t\Helpers\testing_uzr;

require_once ('EntityTestingUtility.php');

class testAccountRecurringChargesEntity  extends EntityTestingUtility
{
	use \Codeception\Specify;

	private $charge;

	protected function createMockDefaultAppAndDeps()
	{
		$app = new Application();

		$eventManager = $this->getMock('Doctrine\Common\EventManager');

		$connection = $this
			->getMockBuilder('Doctrine\DBAL\Connection')
			->disableOriginalConstructor()
			->getMock();

		$connection
			->expects($this->any())
			->method('getEventManager')
			->will($this->returnValue($eventManager));

		$app['user'] = 12;
		$app['auth.userObj'] = new testing_uzr(5);
		$app['auth.user'] = array(
			'loggedIn' => true,
			'validated' => true,
			'authorized' => true,
			'uid' => 12,
			'cid' => 6270,
			'level' => (int)5,
			'emp_id' => 12,
			'isEmployee' => 1
		);

		return array($app, $connection, $eventManager);
	}

	protected function createMockDefaultApp()
	{
		list ($app, $connection, $eventManager) = $this->createMockDefaultAppAndDeps();

		return $app;
	}

	protected function _before()
	{
		$this->charge = new ForeupAccountRecurringCharges();
		$this->charge->setDateCreated(new \DateTime('2016-12-25T12:12:12Z'));
		$this->charge->setCreatedBy(1);
		$this->charge->setOrganizationId(1);
		$this->charge->setIsActive(1);
		$this->charge->setRecurringStatement(new ForeupAccountRecurringStatements());
	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$charge = $this->charge;

		$this->assertTrue(method_exists($charge,'validate'));
		$this->assertTrue(method_exists($charge,'invalid'));
		$this->assertTrue(method_exists($charge,'getId'));
		$this->assertTrue(method_exists($charge,'getName'));
		$this->assertTrue(method_exists($charge,'setName'));
		$this->assertTrue(method_exists($charge,'getDescription'));
		$this->assertTrue(method_exists($charge,'setDescription'));
		$this->assertTrue(method_exists($charge,'getDateCreated'));
		$this->assertTrue(method_exists($charge,'setDateCreated'));
		$this->assertFalse(method_exists($charge,'getStartDate'));
		$this->assertFalse(method_exists($charge,'setStartDate'));
		$this->assertFalse(method_exists($charge,'getEndDate'));
		$this->assertFalse(method_exists($charge,'setEndDate'));
		$this->assertTrue(method_exists($charge,'getCreatedBy'));
		$this->assertTrue(method_exists($charge,'setCreatedBy'));
		$this->assertTrue(method_exists($charge,'getOrganizationId'));
		$this->assertTrue(method_exists($charge,'setOrganizationId'));
		$this->assertTrue(method_exists($charge,'getIsActive'));
		$this->assertTrue(method_exists($charge,'setIsActive'));
		$this->assertTrue(method_exists($charge,'getDateDeleted'));
		$this->assertTrue(method_exists($charge,'setDateDeleted'));
		$this->assertTrue(method_exists($charge,'getDeletedBy'));
		$this->assertTrue(method_exists($charge,'setDeletedBy'));
		$this->assertTrue(method_exists($charge,'getPaymentTerms'));
		$this->assertTrue(method_exists($charge,'setPaymentTerms'));
		$this->assertTrue(method_exists($charge,'getProrateCharges'));
		$this->assertTrue(method_exists($charge,'setProrateCharges'));
	}

	public function testGetSet()
	{
		$location = 'ForeupAccountRecurringCharges->validate';
		$charge = $this->charge;
		$this->assertTrue($charge->validate());


		$this->string_tester($charge,$location,'name','getName','setName',false);

		$this->string_tester($charge,$location,'description','getDescription','setDescription',false);


		$charge->setDateCreated(new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertEquals($charge->getDateCreated(),new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertTrue($charge->validate());

		$charge->setCreatedBy('bob');
		$this->assertEquals($charge->validate(false),'ForeupaccountRecurringCharges->validate Error: createdBy must be an integer: bob');
		$charge->setCreatedBy(null);
		$this->assertEquals($charge->validate(false),'ForeupaccountRecurringCharges->validate Error: createdBy cannot be NULL');
		$charge->setCreatedBy(123);
		$this->assertEquals($charge->getCreatedBy(),123);
		$this->assertTrue($charge->validate());
		$charge->setCreatedBy('456');
		$this->assertEquals($charge->getCreatedBy(),'456');
		$this->assertTrue($charge->validate());

		$charge->setOrganizationId('bob');
		$this->assertEquals($charge->validate(false),'ForeupaccountRecurringCharges->validate Error: organizationId must be an integer: bob');
		$charge->setOrganizationId(null);
		$this->assertEquals($charge->validate(false),'ForeupaccountRecurringCharges->validate Error: setOrganizationId cannot be NULL');
		$this->assertNotEquals($charge->getOrganizationId(),'bob');
		$charge->setOrganizationId(123);
		$this->assertEquals($charge->getOrganizationId(),123);
		$this->assertTrue($charge->validate());
		$charge->setOrganizationId('456');
		$this->assertEquals($charge->getOrganizationId(),'456');
		$this->assertTrue($charge->validate());

		$this->boolean_tester($charge,$location,'isActive','getIsActive','setIsActive',true);

		$charge->setDateDeleted(new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertEquals($charge->getDateDeleted(),new \DateTime('2016-12-25T12:12:12Z'));
		$this->assertTrue($charge->validate());
		$charge->setDateDeleted(null);
		$this->assertEquals($charge->getDateDeleted(),null);
		$this->assertTrue($charge->validate());

		$charge->setDeletedBy('bob');
		$this->assertEquals($charge->validate(false),'ForeupaccountRecurringCharges->validate Error: deletedBy must be an integer: bob');
		$charge->setDeletedBy(null);
		$this->assertTrue($charge->validate());
		$charge->setDeletedBy(123);
		$this->assertEquals($charge->getDeletedBy(),123);
		$this->assertTrue($charge->validate());
		$charge->setDeletedBy('456');
		$this->assertEquals($charge->getDeletedBy(),'456');
		$this->assertTrue($charge->validate());

		$this->boolean_tester($charge,$location,'prorateCharges','getProrateCharges','setProrateCharges',true);
	}
}