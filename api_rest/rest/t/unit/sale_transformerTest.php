<?php
namespace foreup\rest\unit;

use foreup\rest\models\entities\ForeupAccountTransactions;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupItems;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupSales;
use foreup\rest\models\entities\ForeupSalesItems;
use foreup\rest\models\entities\ForeupSalesPayments;
use foreup\rest\models\entities\ForeupTeetime;
use foreup\rest\resource_transformers\sales_transformer;

class testSalesTransformer  extends \Codeception\TestCase\Test
{
	public function testInterface()
	{
		$transformer = new sales_transformer();
		$this->assertTrue(method_exists($transformer, 'transform'));
	}

	public function testTransform()
	{
		$transformer = new sales_transformer();
		$sale = new ForeupSales();
		$sale->testing_override('saleId',2);
		$sale->setCid(3);
		$sale->setCourseId(4);
		$item = new ForeupSalesItems();
		$item->setTotal(12.34);
		$item->setSubtotal(12.34);
		$item->setTax(0);
		$sale->addItems($item);
		$item = new ForeupSalesItems();
		$item->setTotal(56.78);
		$item->setSubtotal(56.78);
		$item->setTax(0);
		$sale->addItems($item);

		$payment = new ForeupSalesPayments();
		$payment->setPaymentAmount(9.10);
		$sale->addPayments($payment);

		$transaction = new ForeupAccountTransactions();
		$transaction->setTransAmount(11.12);
		$sale->addAccountTransactions($transaction);

		$customer = new ForeupCustomers();
		$person = new ForeupPeople();
		$person->testing_override('personId',3399);
		$customer->setPerson($person);
		$sale->setCustomer($customer);

		$person = new ForeupPeople();
		$person->testing_override('personId',12);
		$employee = new ForeupEmployees();
		$employee->setPerson($person);
		$sale->setEmployee($employee);

		$sale->setSaleTime(new \DateTime());

		$teetime = new ForeupTeetime();
		$teetime->setPersonId(418);
		$sale->setTeetime($teetime);

		$sale->setComment('valid comment');

		$sale->setTableId('valid tableId');

		$sale->setPaymentType('credit_card');

		$sale->isValid();

		$result = $transformer->transform($sale);

		$this->assertEquals($sale->getSaleId(),$result['id']);
		$this->assertEquals($sale->getSaleTime(),$result['saleTime']);
		$this->assertEquals($sale->getComment(),$result['comment']);
		$this->assertEquals($sale->getGuestCount(),$result['guestCount']);
		$this->assertEquals($sale->getPaymentType(),$result['paymentType']);
		$this->assertEquals($sale->getSubTotal(), $result['subtotal']);
		$this->assertEquals($sale->getTax(),$result['tax']);
		$this->assertEquals($sale->getTotal(),$result['total']);
		//$this->assertEquals(round($result['subtotal']+$result['tax'],2),$result['total']);

		$items = $sale->getItems()->toArray();
		$this->assertEquals(2,count($items));


	}
}

?>
