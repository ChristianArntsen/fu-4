<?php
namespace foreup\rest\unit\models\services;

use Doctrine\ORM\EntityRepository;
use foreup\rest\mock\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupTeetime;
use foreup\rest\models\services\CourseSettingsService;
use foreup\rest\models\services\LegacyTeetimeService;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use foreup\rest\application\config;

class LegacyTeeTimeServiceTest  extends \Codeception\TestCase\Test
{

	public function testGetAll(){
		// CREATE THE SILEX APPLICATION
		$app = new Application();

		// 1) Load the working configuration
		$configLoader = new config($app);
		$configLoader->loadConfig();

		$url = $app['config.legacy_url'];

		$service = new LegacyTeetimeService($url);
		$service->getTimes("hey",17);
	}

}