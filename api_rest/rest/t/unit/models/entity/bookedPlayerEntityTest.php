<?php
namespace foreup\rest\unit\models\entity;

use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupTeetime;

class bookedPlayerEntityTest  extends \Codeception\TestCase\Test
{
	protected function _before()
	{

	}

	protected function _after()
	{

	}

	public function testSomething(){
		$teetime = new ForeupTeetime();
		$bookedPlayer = $teetime->getBookedPlayerByPosition(1);
		$bookedPlayer->setName("test");

		$this->assertTrue($teetime->getTitle() == "test");

		$player = new ForeupPeople();
		$player->setPersonId(1);
		$player->setFirstName("first");
		$player->setLastName("last");
		$bookedPlayer->setPerson($player);

		//Title should take the players last name
		$this->assertTrue($teetime->getTitle() == "last");
		$this->assertTrue($teetime->getPersonName() == "last, first");

		//Set a custom name but keep the person
		$bookedPlayer->setName("custom name");
		$this->assertTrue($teetime->getTitle() == "last");

		//Remove the person but the custom name remains
		$bookedPlayer->setPerson(0);
		$this->assertTrue($teetime->getTitle() == "custom name");
	}
}