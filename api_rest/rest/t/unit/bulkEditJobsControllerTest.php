<?php
namespace foreup\rest\unit;

use Dflydev\Pimple\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupUsers;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use \Codeception\Interfaces;
use foreup\rest\t\Helpers\testing_uzr;
use foreup\rest\t\Helpers\MockDefaultAppFactory;
use foreup\rest\t\Helpers\testing_middleware_loader;
use foreup\rest\t\Helpers\testing_config;
use foreup\rest\t\Helpers\testing_services_loader;
use foreup\rest\t\Helpers\testing_routes_loader;


class testBulkEditJobsController extends \Codeception\TestCase\Test
{
	private $app, $middleware_loader, $configuration, $routes_loader, $services_loader, $basic_data;

    protected function _before()
    {
        $factory = new MockDefaultAppFactory();
        $this->app = $factory->createMockDefaultApp();
        $doctrineOrmServiceProvider = new DoctrineOrmServiceProvider();
        $doctrineOrmServiceProvider->register($this->app);

        $this->configuration = new testing_config($this->app);
        $this->configuration->loadConfig();
        $this->middleware_loader = new testing_middleware_loader($this->app);
        $this->middleware_loader->load_middleware();
        $this->services_loader = new testing_services_loader($this->app);
        $this->services_loader->register_services();
        $this->routes_loader = new testing_routes_loader($this->app);
        $this->routes_loader->bindRoutesToControllers();

        $app =& $this->app;
        $app['user'] = 12;
        $app['auth.userObj'] = new testing_uzr(5);
        $app['auth.user'] = array(
            'loggedIn' => true,
            'validated' => true,
            'authorized' => true,
            'uid' => 12,
            'cid' => 6270,
            'level' => (int)5,
            'emp_id' => 12,
            'isEmployee' => 1
        );
        $person = $this->app['orm.em']->getReference('e:ForeupPeople', 12);
        $person->setPersonId(12);

        $app['employee'] = $this->app['orm.em']->getReference('e:ForeupEmployees', 12);
        $app['employee']->setCourseId(6270);
        $app['employee']->setCid(6270);
        $app['employee']->setUserLevel(5);
        $app['employee']->setPerson( $this->app['orm.em']->getReference('e:ForeupPeople', 12) );

        $this->app['bulkEditJobs.controller']->startTransaction();
    }

    protected function _after()
    {
        $this->app['bulkEditJobs.controller']->rollback();
        $conn = $this->app['orm.em']->getConnection();
        $conn->close();
    }

    private function createTestData(){
        $controller = $this->app['bulkEditJobs.controller'];
        $request = new Request();

        $data = [
            'type' => 'customers',
            'recordIds' => [1,2,3],
            'totalRecords' => 3,
            'settings' => [
                'state' => [
                    'value' => 'ID',
                    'action' => 'set'
                ]
            ],
            'status' => 'pending'
        ];

        $request->request->set('data', ['attributes' => $data]);

        $result = $controller->create(6270, $request);
        $content = json_decode($result->getContent(),true);

        return $content['data']['id'];
    }

    public function testCreate()
    {
        $controller = $this->app['bulkEditJobs.controller'];
        $request = new Request();

        $data = [
            'type' => 'customers',
            'recordIds' => [1,2,3],
            'totalRecords' => 3,
            'settings' => [
                'state' => [
                    'value' => 'ID',
                    'action' => 'set'
                ]
            ],
            'status' => 'pending'
        ];

        // test basic usage
        $request->request->set('data', ['attributes' => $data]);

        //$controller->startTransaction();
        $result = $controller->create(6270, $request);

        $this->assertTrue(isset($result));
        $this->assertEquals(200, $result->getStatusCode());

        $content = $result->getContent();
        $this->assertNotEmpty($content);
        $this->assertTrue(is_string($content));

        $content = json_decode($content,true);

        $this->assertTrue(is_array($content));
        $this->assertNotEmpty($content['data']['attributes']['recordIds']);
        $this->assertEquals(count($content['data']['attributes']['recordIds']), 3);
    }

	public function testGet()
	{
	    $controller = $this->app['bulkEditJobs.controller'];
		$request = new Request();
        $id = $this->createTestData();

		// test basic usage
		$request->request->set('data', $this->basic_data);
		$request->query->set('include',['employee']);

        $result = $controller->get(6270, $id, $request);

        $this->assertTrue(isset($result));
		$this->assertEquals(200,$result->getStatusCode());
		$content = json_decode($result->getContent(), true);

		$this->assertNotEmpty($content);
		$this->assertTrue(is_array($content));
		$this->assertTrue(count($content)>0);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['type']);
		$this->assertEquals('bulkEditJobs',$content['data']['type']);
		$this->assertNotEmpty($content['data']['id']);
		$this->assertTrue(is_numeric($content['data']['id']));
		$this->assertTrue(is_int($content['data']['id']*1));
		$this->assertEquals($id, $content['data']['id']);
		$this->assertNotEmpty($content['data']['attributes']);
		$this->assertNotEmpty($content['data']['relationships']);
		$this->assertNotEmpty($content['included']);
		$this->assertTrue(count($content['included'])===1);
	}

	public function testGetAll()
	{
		$controller = $this->app['bulkEditJobs.controller'];
		$request = new Request();
        $this->createTestData();

		// test basic usage
		$request->query->set('limit',10);
		$result = $controller->getAll(6270,$request);

		$this->assertTrue(isset($result));
		$this->assertEquals($result->getStatusCode(),200);
		$content = $result->getContent();

        $this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));
		$content = json_decode($content,true);
		$this->assertTrue(is_array($content));
		$this->assertTrue(is_array($content['data']));
		$this->assertGreaterThan(0,count($content['data']));
		$this->assertTrue(is_numeric($content['data'][0]['id']));
		$this->assertEquals('bulkEditJobs',$content['data'][0]['type']);
		$this->assertTrue(is_array($content['data'][0]['attributes']));
	}

	public function testDelete()
	{
		$controller = $this->app['bulkEditJobs.controller'];
		$request = new Request();
        $id = $this->createTestData();

		$result = $controller->delete(6270, $id, $request);
		$this->assertTrue(isset($result));
		$this->assertEquals(200, $result->getStatusCode());
		$content = $result->getContent();
		$this->assertNotEmpty($content);
		$this->assertTrue(is_string($content));

        $content = json_decode($content, true);
		$this->assertNotEmpty($content['data']);
		$this->assertNotEmpty($content['data']['success']);
		$this->assertTrue(($content['data']['success']));
		$this->assertNotEmpty(($content['data']['content']));
		$this->assertEquals('bulkEditJobs instance deleted', $content['data']['content']);

        $request->query->set('includeDeleted', 1);
	}
}