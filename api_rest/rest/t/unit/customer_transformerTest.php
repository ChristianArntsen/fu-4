<?php
namespace foreup\rest\unit;

use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\sales_transformer;

class testCustomerTransformer  extends \Codeception\TestCase\Test
{
	public function testInterface()
	{
		$transformer = new sales_transformer();
		$this->assertTrue(method_exists($transformer, 'transform'));
	}

	public function testTransform()
	{
		$transformer = new customer_transformer();

		$customer = new ForeupCustomers();

		$customer->setCid(1);
		$customer->setCourseId(6270);
		$course = new ForeupCourses();
		$course->testing_override('courseId',6270);

		$person = new ForeupPeople();
		$person->testing_override('personId',3399);
		$customer->setPerson($person);

		$customer->setCourse($course);

		$customer->setMember(1);

		$customer->setUsername('validUsername');

		$customer->setPassword('password string');

		$customer->setPriceClass('price_class');

		$customer->setImageId(0);

		$customer->setAccountBalance(12.34);

		$customer->setAccountBalanceAllowNegative(1);

		$customer->setAccountLimit(56.78);

		$customer->setInvoiceBalance(9.10);

		$customer->setInvoiceBalanceAllowNegative(1);

		$customer->setUseLoyalty(1);

		$customer->setLoyaltyPoints(11);

		$customer->setCompanyName('Bob');

		$customer->setTaxable(1);

		$customer->setDiscount(0.00);

		$customer->setDateCreated(new \DateTime('1772-10-22'));

		$result = $transformer->transform($customer);

		$this->assertEquals(3399,$result['id']);

		$this->assertEquals($customer->getUsername(),$result['username']);
		$this->assertEquals($customer->getDiscount(),$result['discount']);
		$this->assertEquals($customer->getAccountBalance(),$result['account_balance']);


	}
}

?>
