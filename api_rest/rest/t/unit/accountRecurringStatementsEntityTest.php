<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupEmployees;
use Silex\Application;

require_once ('EntityTestingUtility.php');

class testAccountRecurringStatementsEntity  extends EntityTestingUtility
{
	use \Codeception\Specify;

	protected $statement;

	protected function _before()
	{
		$this->statement = new ForeupAccountRecurringStatements();
		$this->statement->setDateCreated(new \DateTime('2016-12-25T12:12:12Z'));
		$this->statement->setIsActive(1);
		$this->statement->setCreatedBy(new ForeupEmployees());
		$this->statement->setOrganizationId(1);
	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$statement = $this->statement;

		$this->assertTrue(method_exists($statement,'validate'));
		$this->assertTrue(method_exists($statement,'invalid'));
		$this->assertTrue(method_exists($statement,'getId'));
		$this->assertTrue(method_exists($statement,'getName'));
		$this->assertTrue(method_exists($statement,'setName'));
		$this->assertTrue(method_exists($statement,'getDateCreated'));
		$this->assertTrue(method_exists($statement,'setDateCreated'));
		$this->assertFalse(method_exists($statement,'getStartDate'));
		$this->assertFalse(method_exists($statement,'setStartDate'));
		$this->assertFalse(method_exists($statement,'getEndDate'));
		$this->assertFalse(method_exists($statement,'setEndDate'));
		$this->assertTrue(method_exists($statement,'getCreatedBy'));
		$this->assertTrue(method_exists($statement,'setCreatedBy'));
		$this->assertTrue(method_exists($statement,'getOrganizationId'));
		$this->assertTrue(method_exists($statement,'setOrganizationId'));
		$this->assertTrue(method_exists($statement,'getIsActive'));
		$this->assertTrue(method_exists($statement,'setIsActive'));
		$this->assertTrue(method_exists($statement,'getDateDeleted'));
		$this->assertTrue(method_exists($statement,'setDateDeleted'));
		$this->assertTrue(method_exists($statement,'getDeletedBy'));
		$this->assertTrue(method_exists($statement,'setDeletedBy'));
		$this->assertTrue(method_exists($statement,'getPaymentTerms'));
		$this->assertTrue(method_exists($statement,'setPaymentTerms'));
		$this->assertFalse(method_exists($statement,'getIncludeBalanceForward'));
		$this->assertFalse(method_exists($statement,'setIncludeBalanceForward'));
		$this->assertTrue(method_exists($statement,'getSendEmptyStatements'));
		$this->assertTrue(method_exists($statement,'setSendEmptyStatements'));
		$this->assertFalse(method_exists($statement,'getItemizeSales'));
		$this->assertFalse(method_exists($statement,'setItemizeSales'));
		$this->assertFalse(method_exists($statement,'getItemizeInvoices'));
		$this->assertFalse(method_exists($statement,'setItemizeInvoices'));
		$this->assertFalse(method_exists($statement,'getItemizeCharges'));
		$this->assertFalse(method_exists($statement,'setItemizeCharges'));
		$this->assertFalse(method_exists($statement,'getIncludeSales'));
		$this->assertFalse(method_exists($statement,'setIncludeSales'));
		$this->assertFalse(method_exists($statement,'getIncludeInvoices'));
		$this->assertFalse(method_exists($statement,'setIncludeInvoices'));
		$this->assertFalse(method_exists($statement,'getIncludeCharges'));
		$this->assertFalse(method_exists($statement,'setIncludeCharges'));
		///*
		$this->assertFalse(method_exists($statement,'getAutopayDelayDays'));
		$this->assertFalse(method_exists($statement,'setAutopayDelayDays'));
		$this->assertTrue(method_exists($statement,'getAutopayOverdueBalance'));
		$this->assertTrue(method_exists($statement,'setAutopayOverdueBalance'));
		$this->assertTrue(method_exists($statement,'getAutopayAttempts'));
		$this->assertTrue(method_exists($statement,'setAutopayAttempts'));
		$this->assertTrue(method_exists($statement,'getAutopayAfterDays'));
		$this->assertTrue(method_exists($statement,'setAutopayAfterDays'));
		$this->assertTrue(method_exists($statement,'getAutopayEnabled'));
		$this->assertTrue(method_exists($statement,'setAutopayEnabled'));
		$this->assertTrue(method_exists($statement,'getDueAfterDays'));
		$this->assertTrue(method_exists($statement,'setDueAfterDays'));
		$this->assertTrue(method_exists($statement,'getIncludePastDue'));
		$this->assertTrue(method_exists($statement,'setIncludePastDue'));
		$this->assertTrue(method_exists($statement,'getSendMail'));
		$this->assertTrue(method_exists($statement,'setSendMail'));
		$this->assertTrue(method_exists($statement,'getSendEmail'));
		$this->assertTrue(method_exists($statement,'setSendEmail'));
		$this->assertTrue(method_exists($statement,'getIncludeCustomerTransactions'));
		$this->assertTrue(method_exists($statement,'setIncludeCustomerTransactions'));
		$this->assertTrue(method_exists($statement,'getIncludeMemberTransactions'));
		$this->assertTrue(method_exists($statement,'setIncludeMemberTransactions'));
		$this->assertTrue(method_exists($statement,'getCustomerMessage'));
		$this->assertTrue(method_exists($statement,'setCustomerMessage'));
		$this->assertTrue(method_exists($statement,'getSendZeroChargeStatement'));
		$this->assertTrue(method_exists($statement,'setSendZeroChargeStatement'));
		$this->assertTrue(method_exists($statement,'getFooterText'));
		$this->assertTrue(method_exists($statement,'setFooterText'));
		$this->assertTrue(method_exists($statement,'getMessageText'));
		$this->assertTrue(method_exists($statement,'setMessageText'));
		$this->assertTrue(method_exists($statement,'getTermsAndConditionsText'));
		$this->assertTrue(method_exists($statement,'setTermsAndConditionsText'));
		$this->assertTrue(method_exists($statement,'getIsDefault'));
		$this->assertTrue(method_exists($statement,'setIsDefault'));
		//*/
	}

	public function testGetSet()
	{
		$statement = $this->statement;
		$this->assertTrue($statement->validate());
		$location = 'ForeupAccountRecurringStatements->validate';

		$this->string_tester($statement,$location,'name','getName','setName',false);
		$this->DateTime_tester($statement,$location,'dateCreated','getDateCreated','setDateCreated',true);

		// createdBy

		$this->integer_tester($statement,$location,'organizationId','getOrganizationId','setOrganizationId',true);
		$this->boolean_tester($statement,$location,'isActive','getIsActive','setIsActive',true);
		$this->DateTime_tester($statement,$location,'dateDeleted','getDateDeleted','setDateDeleted',false);
		$this->integer_tester($statement,$location,'deletedBy','getDeletedBy','setDeletedBy',false);
		// sendEmptyStatements
		$this->boolean_tester($statement,$location,'sendEmptyStatements','getSendEmptyStatements','setSendEmptyStatements',true);

		$this->string_tester($statement,$location,'customerMessage','getCustomerMessage','setCustomerMessage',false);
		$this->boolean_tester($statement,$location,'includeMemberTransactions','getIncludeMemberTransactions','setIncludeMemberTransactions',true);
		$this->boolean_tester($statement,$location,'includeCustomerTransactions','getIncludeCustomerTransactions','setIncludeCustomerTransactions',true);
		$this->boolean_tester($statement,$location,'sendEmail','getSendEmail','setSendEmail',true);
		$this->boolean_tester($statement,$location,'sendMail','getSendMail','setSendMail',true);
		$this->boolean_tester($statement,$location,'includePastDue','getIncludePastDue','setIncludePastDue',true);
		$this->integer_tester($statement,$location,'dueAfterDays','getDueAfterDays','setDueAfterDays',true);
		$this->boolean_tester($statement,$location,'autopayEnabled','getAutopayEnabled','setAutopayEnabled',true);
		$this->integer_tester($statement,$location,'autopayAfterDays','getAutopayAfterDays','setAutopayAfterDays',true);
		$this->integer_tester($statement,$location,'autopayAttempts','getAutopayAttempts','setAutopayAttempts',true);
		$this->boolean_tester($statement,$location,'autopayOverdueBalance','getAutopayOverdueBalance','setAutopayOverdueBalance',true);

		$this->boolean_tester($statement,$location,'payMemberBalance','getPayMemberBalance','setPayMemberBalance',true);
		$this->boolean_tester($statement,$location,'payCustomerBalance','getPayCustomerBalance','setPayCustomerBalance',true);

		$this->boolean_tester($statement,$location,'sendZeroChargeStatement','getSendZeroChargeStatement','setSendZeroChargeStatement',true);
		$this->boolean_tester($statement,$location,'isDefault','getIsDefault','setIsDefault',true);

		$this->string_tester($statement,$location,'footerText','getFooterText','setFooterText',false);
		$this->string_tester($statement,$location,'messageText','getMessageText','setMessageText',false);
		$this->string_tester($statement,$location,'termsAndConditionsText','getTermsAndConditionsText','setTermsAndConditionsText',false);


		$this->boolean_tester($statement,$location,'financeChargeEnabled','getFinancechargeEnabled','setFinanceChargeEnabled',true);
		$this->numeric_tester($statement,$location,'financeChargeAmount','getFinanceChargeAmount','setFinanceChargeAmount',true);
		$this->enum_tester($statement,$location,'financeChargeType',['percent','fixed'],'getFinanceChargeType','setFinanceChargeType',true);
		$this->integer_tester($statement,$location,'financeChargeAfterDays','getFinanceChargeAfterDays','setFinanceChargeAfterDays',true);

	}
}

?>