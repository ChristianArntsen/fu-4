<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 6/5/2017
 * Time: 1:37 PM
 */

namespace foreup\rest\unit\controllers\courses;

use foreup\rest\t\traits\mock_app_trait;
use PHPUnit\Framework\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class terminalsControllerTest extends \Codeception\TestCase\Test
{
    use mock_app_trait;

    private $app, $controller, $setup, $basic_data, $request;

    protected function _before()
    {
        $this->setupController('terminals.controller');

        $this->basic_data = [
            "data" => [
                [
                    "type" => "terminals",
                    "id" => "1",
                    "attributes" => [
                        "label" => "Pro Shop",
                        "autoPrintReceipts" => null,
                        "webprnt" => true,
                        "UseRegisterLog" => null,
                        "cashRegister" => false,
                        "printTipLine" => null,
                        "signatureSlipCount" => 2,
                        "creditCardReceiptCount" => 1,
                        "nonCreditCardReceiptCount" => 1,
                        "persistentLogs" => false,
                        "afterSaleLoad" => true,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 2
                    ]
                ],
                [
                    "type" => "terminals",
                    "id" => "7",
                    "attributes" => [
                        "label" => "Pro Shop 2",
                        "autoPrintReceipts" => null,
                        "webprnt" => true,
                        "UseRegisterLog" => null,
                        "cashRegister" => null,
                        "printTipLine" => null,
                        "signatureSlipCount" => 1,
                        "creditCardReceiptCount" => null,
                        "nonCreditCardReceiptCount" => null,
                        "persistentLogs" => false,
                        "afterSaleLoad" => null,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 2
                    ]
                ],
                [
                    "type" => "terminals",
                    "id" => "9",
                    "attributes" => [
                        "label" => "Herezada",
                        "autoPrintReceipts" => null,
                        "webprnt" => true,
                        "UseRegisterLog" => null,
                        "cashRegister" => null,
                        "printTipLine" => null,
                        "signatureSlipCount" => 1,
                        "creditCardReceiptCount" => null,
                        "nonCreditCardReceiptCount" => null,
                        "persistentLogs" => false,
                        "afterSaleLoad" => null,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 3
                    ]
                ],
                [
                    "type" => "terminals",
                    "id" => "14",
                    "attributes" => [
                        "label" => "LIVE EMV TERMINAL",
                        "autoPrintReceipts" => null,
                        "webprnt" => true,
                        "UseRegisterLog" => null,
                        "cashRegister" => null,
                        "printTipLine" => null,
                        "signatureSlipCount" => 1,
                        "creditCardReceiptCount" => null,
                        "nonCreditCardReceiptCount" => null,
                        "persistentLogs" => false,
                        "afterSaleLoad" => true,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 1
                    ]
                ],
                [
                    "type" => "terminals",
                    "id" => "20",
                    "attributes" => [
                        "label" => "TEST EMV TERMINAL",
                        "autoPrintReceipts" => null,
                        "webprnt" => true,
                        "UseRegisterLog" => null,
                        "cashRegister" => null,
                        "printTipLine" => null,
                        "signatureSlipCount" => 1,
                        "creditCardReceiptCount" => null,
                        "nonCreditCardReceiptCount" => null,
                        "persistentLogs" => true,
                        "afterSaleLoad" => null,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 1
                    ]
                ],
                [
                    "type" => "terminals",
                    "id" => "21",
                    "attributes" => [
                        "label" => "Windows (QZ Printer)",
                        "autoPrintReceipts" => null,
                        "webprnt" => false,
                        "UseRegisterLog" => null,
                        "cashRegister" => null,
                        "printTipLine" => true,
                        "signatureSlipCount" => 2,
                        "creditCardReceiptCount" => 2,
                        "nonCreditCardReceiptCount" => 2,
                        "persistentLogs" => true,
                        "afterSaleLoad" => null,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 1
                    ]
                ]
            ]
        ];

        $this->request = new Request();
    }

    protected function _after()
    {
        $this->tearDownController();
    }

    protected function validateError(JsonResponse $response, string $error, int $errorNumber = 403){

        $this->request->request->set('data',$this->basic_data);
        $this->assertEquals($errorNumber, $response->getStatusCode());

        $response = $response->getContent();
        $response = json_decode($response,true);
        $this->assertNotNull($response);
        $this->assertEquals($response['success'], false);
        $this->assertNotEmpty($response['title']);
        $this->assertEquals($error, $response['title']);
    }

    public function testGetAll(){
        $response = $this->controller->getAll($this->request, 6270);
        $response = json_decode($response->getContent(),true);

        $this->assertEquals($this->basic_data, $response);
    }

    public function testGet(){
        $response = $this->controller->get($this->request, 6270, 7);
        $response = json_decode($response->getContent(),true);

        $expectedResponse = null;
        foreach ($this->basic_data['data'] as $terminal) {
            if($terminal['id'] == 7) {
                $expectedResponse = $terminal;
                break;
            }
        }

        $this->assertEquals($expectedResponse, $response['data']);
    }

    public function testUpdateWithNoAttributes(){
        $this->request->request->set('data', "");

        $this->setExpectedException('\Exception',"Attributes required in request");

        $this->controller->update($this->request, 6270, 7);
    }

    public function testUpdateWithInvalidTerminal(){

        $response = $this->controller->update($this->request, 6270, 3);

        $this->validateError($response, "Terminal not found or you don't have permission to access this resource. ");
    }

    public function testUpdateWithNonExistentTerminal(){

        $response = $this->controller->update($this->request, 6270, 10);

        $this->validateError($response, "Terminal not found or you don't have permission to access this resource. ");
    }

    public function testUpdate(){
        $terminal = [
            "type" => "terminals",
            "id" => "1",
            "attributes" => [
                "label" => "Test Shop",
                "autoPrintReceipts" => false,
                "webprnt" => false,
                "UseRegisterLog" => true,
                "cashRegister" => true,
                "printTipLine" => false,
                "signatureSlipCount" => 1,
                "creditCardReceiptCount" => 2,
                "nonCreditCardReceiptCount" => 2,
                "persistentLogs" => true,
                "afterSaleLoad" => false,
                "autoEmailReceipt" => false,
                "autoEmailNoPrint" => false,
                "requireSignatureMemberPayments" => false,
                "multiCashDrawers" => false,
                "cashDrawerNumber" => 2,
                "Https" => true,
                "quickbuttonTab" => 1
            ]
        ];
        $this->request->request->set('data', $terminal);

        $response = $this->controller->update($this->request, 6270, 1);
        $response = json_decode($response->getContent(),true);

        $this->assertEquals($terminal, $response['data']);
    }

    public function testCreate(){
        $terminal = [
            "type" => "terminals",
            "attributes" => [
                "label" => "Test Shop",
                "quickbuttonTab" => 1,
                "autoPrintReceipts" => false,
                "webprnt" => false,
                "UseRegisterLog" => true,
                "cashRegister" => true,
                "printTipLine" => false,
                "signatureSlipCount" => 1,
                "creditCardReceiptCount" => 2,
                "nonCreditCardReceiptCount" => 2,
                "persistentLogs" => true,
                "afterSaleLoad" => false,
                "autoEmailReceipt" => false,
                "autoEmailNoPrint" => false,
                "requireSignatureMemberPayments" => false,
                "multiCashDrawers" => false,
                "cashDrawerNumber" => 2,
                "Https" => true

            ]
        ];
        $this->request->request->set('data', $terminal);

        $response = $this->controller->create($this->request, 6270);
        $response = json_decode($response->getContent(),true);

        $this->assertEquals($terminal['attributes'], $response['data']['attributes']);
    }

    public function testDelete(){
        $terminal = [
            "type" => "terminals",
            "attributes" => [
                "label" => "Test Shop",
                "quickbuttonTab" => 1,
                "autoPrintReceipts" => false,
                "webprnt" => false,
                "UseRegisterLog" => true,
                "cashRegister" => true,
                "printTipLine" => false,
                "signatureSlipCount" => 1,
                "creditCardReceiptCount" => 2,
                "nonCreditCardReceiptCount" => 2,
                "persistentLogs" => true,
                "afterSaleLoad" => false,
                "autoEmailReceipt" => false,
                "autoEmailNoPrint" => false,
                "requireSignatureMemberPayments" => false,
                "multiCashDrawers" => false,
                "cashDrawerNumber" => 2,
                "Https" => true

            ]
        ];
        $this->request->request->set('data', $terminal);

        $response = $this->controller->create($this->request, 6270);
        $response = json_decode($response->getContent(),true);
        $this->request->request->set('data', $response['data']);

        $response = $this->controller->delete($this->request, 6270, $response['data']['id']);
        $response = json_decode($response->getContent(),true);

        $this->assertEquals("", $response['data']['id']);
        $this->assertEquals($terminal['attributes'], $response['data']['attributes']);
    }
}