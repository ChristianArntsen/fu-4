<?php
namespace foreup\rest\unit\controllers\courses\teesheets;

use Doctrine\ORM\EntityRepository;
use foreup\rest\mock\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupTeetime;
use foreup\rest\models\services\CourseSettingsService;
use foreup\rest\models\services\LegacyTeetimeService;
use Symfony\Component\HttpFoundation\Request;

class teetimesControllerTest  extends \Codeception\TestCase\Test
{

	protected function getEmMock()
	{
		$teetime = $this->getMock(ForeupTeetime::class);

		// Now, mock the repository so it returns the mock of the employee
		$employeeRepository = $this
			->getMockBuilder(EntityRepository::class)
			->disableOriginalConstructor()
			->getMock();
		//$employeeRepository->expects($this->once())
		//	->method('find')
		//	->will($this->returnValue($teetime));



		$emMock  = $this->getMock('\Doctrine\ORM\EntityManager',
			array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);
		$emMock->expects($this->any())
			->method('getRepository')
			->will($this->returnValue($employeeRepository));
		$emMock->expects($this->any())
			->method('getClassMetadata')
			->will($this->returnValue((object)array('name' => 'aClass')));
		$emMock->expects($this->any())
			->method('persist')
			->will($this->returnValue(null));
		$emMock->expects($this->any())
			->method('flush')
			->will($this->returnValue(null));
		return $emMock;
	}
	public function GetAll(){
		$db = $this->getEmMock();
		$courseSettings = \Mockery::mock('foreup\rest\models\services\CourseSettingsService',[
			"loadSettings"=>new ForeupCourses()
		]);
		$legacyTeetimeService = new \foreup\rest\mock\models\services\LegacyTeetimeService("http://development.foreupsoftware.com");
		$authUser = \Mockery::mock('foreup\rest\models\services\AuthenticatedUser',[
			"getEmpId"=>1,
			"getCid"=>1,
			"getToken"=>"",
			"getAppId"=>1,
			"getLevel"=>5,
			"getIsEmployee"=>1
		]);
		$controller = new teetimes($db,$authUser,$legacyTeetimeService,$courseSettings);
		$controller->byPassAccessCheck();

		$request = new Request();
		$result = $controller->getAll($request,1,1);

		$this->assertTrue(isset($result));

		return true;
	}

	public function testNothing(){
		//this is just a placeholder until we actually write some tests for this class
		//if you remove this test the class will be marked as failing all tests.
	}

}