<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 6/8/2017
 * Time: 4:44 PM
 */

namespace foreup\rest\unit\controllers\courses;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use foreup\rest\t\traits\mock_app_trait;

class printersControllerTest extends \Codeception\TestCase\Test
{
    use mock_app_trait;
    private $basic_data, $request;

    protected function _before()
    {
        $this->setupController('printers.controller');

        $this->basic_data = [
            "data" => [
                [
                    "type" => "terminals",
                    "id" => "1",
                    "attributes" => [
                        "label" => "Pro Shop",
                        "autoPrintReceipts" => null,
                        "webprnt" => true,
                        "UseRegisterLog" => null,
                        "cashRegister" => false,
                        "printTipLine" => null,
                        "signatureSlipCount" => 2,
                        "creditCardReceiptCount" => 1,
                        "nonCreditCardReceiptCount" => 1,
                        "persistentLogs" => false,
                        "afterSaleLoad" => true,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 2
                    ]
                ],
                [
                    "type" => "terminals",
                    "id" => "7",
                    "attributes" => [
                        "label" => "Pro Shop 2",
                        "autoPrintReceipts" => null,
                        "webprnt" => true,
                        "UseRegisterLog" => null,
                        "cashRegister" => null,
                        "printTipLine" => null,
                        "signatureSlipCount" => 1,
                        "creditCardReceiptCount" => null,
                        "nonCreditCardReceiptCount" => null,
                        "persistentLogs" => false,
                        "afterSaleLoad" => null,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 2
                    ]
                ],
                [
                    "type" => "terminals",
                    "id" => "9",
                    "attributes" => [
                        "label" => "Herezada",
                        "autoPrintReceipts" => null,
                        "webprnt" => true,
                        "UseRegisterLog" => null,
                        "cashRegister" => null,
                        "printTipLine" => null,
                        "signatureSlipCount" => 1,
                        "creditCardReceiptCount" => null,
                        "nonCreditCardReceiptCount" => null,
                        "persistentLogs" => false,
                        "afterSaleLoad" => null,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 3
                    ]
                ],
                [
                    "type" => "terminals",
                    "id" => "14",
                    "attributes" => [
                        "label" => "LIVE EMV TERMINAL",
                        "autoPrintReceipts" => null,
                        "webprnt" => true,
                        "UseRegisterLog" => null,
                        "cashRegister" => null,
                        "printTipLine" => null,
                        "signatureSlipCount" => 1,
                        "creditCardReceiptCount" => null,
                        "nonCreditCardReceiptCount" => null,
                        "persistentLogs" => false,
                        "afterSaleLoad" => true,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 1
                    ]
                ],
                [
                    "type" => "terminals",
                    "id" => "20",
                    "attributes" => [
                        "label" => "TEST EMV TERMINAL",
                        "autoPrintReceipts" => null,
                        "webprnt" => true,
                        "UseRegisterLog" => null,
                        "cashRegister" => null,
                        "printTipLine" => null,
                        "signatureSlipCount" => 1,
                        "creditCardReceiptCount" => null,
                        "nonCreditCardReceiptCount" => null,
                        "persistentLogs" => true,
                        "afterSaleLoad" => null,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 1
                    ]
                ],
                [
                    "type" => "terminals",
                    "id" => "21",
                    "attributes" => [
                        "label" => "Windows (QZ Printer)",
                        "autoPrintReceipts" => null,
                        "webprnt" => false,
                        "UseRegisterLog" => null,
                        "cashRegister" => null,
                        "printTipLine" => true,
                        "signatureSlipCount" => 2,
                        "creditCardReceiptCount" => 2,
                        "nonCreditCardReceiptCount" => 2,
                        "persistentLogs" => true,
                        "afterSaleLoad" => null,
                        "autoEmailReceipt" => null,
                        "autoEmailNoPrint" => null,
                        "requireSignatureMemberPayments" => null,
                        "multiCashDrawers" => null,
                        "cashDrawerNumber" => 1,
                        "Https" => false,
                        "quickbuttonTab" => 1
                    ]
                ]
            ]
        ];

        $this->request = new Request();
    }

    protected function _after()
    {
        $this->tearDownController();
    }

    public function testGetAll(){
        $response = $this->controller->getAll($this->request, 6270);

        $this->assertEquals(200, $response->getStatusCode());

    }
}