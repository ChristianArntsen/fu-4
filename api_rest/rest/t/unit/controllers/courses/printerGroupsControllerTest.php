<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 6/27/2017
 * Time: 10:56 AM
 */

namespace foreup\rest\unit\controllers\courses;

use foreup\rest\t\traits\mock_app_trait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class printerGroupsControllerTest extends \Codeception\TestCase\Test
{
    /*
     * The mock_app_trait pulls in all the functions you need to setup and teardown the foreUP application in your testing environment
     * The trait will add two class level variables once included. They are $app and $controller
     */
    use mock_app_trait;

    private $basic_data, $request;

    protected function _before(){
        /*
         * Controller names can be found by pressing Ctrl+Shift+N then search for the routes_loader.php file.
         * Once in the file look for the string that matches the controller you want to test like the one below
         *
         * setupController is part of the mock_app_trait
        */
        $this->setupController('printerGroups.controller');

        //all controllers require a Request Object to be passed in along with the course ID
        $this->request = new Request();
    }

    protected function _after(){
        //tearDownController is part of mock_app_trait
        $this->tearDownController();
    }


    public function testGetAll(){
        /*
         * If you get the error below then the controller is missing the rollback trait and you will need to add it.
         * "Call to undefined method foreup\rest\controllers\courses\printerGroups::startTransaction()"
         *
         * The standard test course for automated tests is the Cascade Golf Course whose ID is 6270
         */
        $response = $this->controller->getAll($this->request, 6270);
        $response = $response->getContent();
    }
}