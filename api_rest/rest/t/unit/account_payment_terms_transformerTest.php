<?php
namespace foreup\rest\unit;
use foreup\rest\controllers\account_recurring\accountRecurringCharges;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupAccountPaymentTerms;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\resource_transformers\account_payment_terms_transformer;
//use Silex\Application;

class testAccountPaymentTermsTransformer  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	public function testInterface()
	{
		$transformer = new account_payment_terms_transformer();
		$this->assertTrue(method_exists($transformer,'transform'));
	}

	public Function testTransform()
	{
		//$controller = new accountRecurringCharges(null,null);
		$transformer = new account_payment_terms_transformer();

		$terms = new ForeupAccountPaymentTerms();
		$terms->setName('valid name');
		$terms->setDateCreated(new \DateTime('2016-03-03T12:12:12Z'));
		$terms->setMemo('valid memo');
		$terms->setOrganizationId(123);
		$terms->setPastDueDays(12);
		$terms->setPastDueFeePercentage(123);

		$returned = $transformer->transform($terms);

		$this->assertEquals($terms->getName(),$returned['name']);
		$this->assertEquals($terms->getPastDueDays(),$returned['past_due_days']);
		$this->assertEquals($terms->getOrganizationId(),$returned['organization_id']);
		$this->assertEquals($terms->getDateCreated(),new \DateTime($returned['date_created']));
		$this->assertEquals($terms->getPastDueFeePercentage(),$returned['past_due_fee_percentage']);
		$this->assertEquals($terms->getMemo(),$returned['memo']);
	}
}
?>