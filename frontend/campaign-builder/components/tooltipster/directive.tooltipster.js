angular.module('tooltipster', [])

    .directive('tooltip', function() {
        return {
            link: function (scope, element, attrs) {
                element.tooltipster({
                    theme: 'tt-theme'
                });

            }
        }
    });
