'use strict';

cbApp.
    factory('CampaignFactory', function($http,$q,$timeout,$log,dialogs ) {
        var factory = {};
        var dirty = false;
        var throttle = false;
        var campaignSavingPromise = null;

        factory.currentCampaign = {};
        factory.markAsDirty = function(){
            dirty = true;
        }
        factory.getTemplate = function(id){
            var deferred = $q.defer();
            $http.get('/index.php/marketing_campaigns/template?template_id='+id)
                .success(function(data) {
                    deferred.resolve(data.data.template);
                }).error(function(msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }
        factory.saveTemplate = function(template,id){
            if (throttle)
                $timeout.cancel(throttle);
            throttle = $timeout(function() {
                save(template,id);
            }, 1000);
        }
        factory.waitForCampaignSave = function(){
            return  this.campaignSavingPromise;
        }
        function save(template,id){
            var deferred = $q.defer();
            $http.post('/index.php/marketing_campaigns/template?template_id='+id,{"template":template})
                .success(function(data) {
                    deferred.resolve(data.data.template);
                }).error(function(msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }



        factory.deleteCampaign = function(campaign_id){
            var deferred = $q.defer();
            $http.post('/index.php/marketing_campaigns/ajaxDeleteCampaign?campaign_id='+campaign_id)
                .success(function(data) {
                    deferred.resolve(data.data);
                }).error(function(msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }

        factory.copyCampaign = function(campaign){
            var newCampaign = angular.copy(campaign);
            var attributesToClear = ['recipients_pretty','campaign_id','remote_hash','date_created','send_date','send_date_cst','recipient_count','is_sent','attempts','queued'];
            angular.forEach(attributesToClear,function(value,key){
                if(newCampaign[value] != null){
                    delete newCampaign[value];
                }
            })
            newCampaign.status = 'draft';
            return factory.saveCampaign(newCampaign,null);
        }

        factory.saveCampaign = function(campaign,id){
            if(campaign == undefined || campaign == null)
                return;

            if (throttle)
                $timeout.cancel(throttle);

            //The campaign used through out the website has extra properties that don't get saved to the db
            var newCampaign = angular.copy(campaign);
            if(newCampaign.recipients_pretty != null)
                delete(newCampaign.recipients_pretty);
            if(newCampaign.template_details != null)
                delete(newCampaign.template_details);
            if(newCampaign.pausing != null)
                delete(newCampaign.pausing);
            if(newCampaign.hasOwnProperty('rendered_html'))
                delete(newCampaign.rendered_html);
            if(newCampaign.hasOwnProperty('loading'))
                delete(newCampaign.loading);
            if(newCampaign.hasOwnProperty('recipient_reports'))
                delete(newCampaign.recipient_reports);
            var deferred = $q.defer();

            this.campaignSavingPromise = deferred.promise;
            if(id != null){
                newCampaign['campaign_id'] = id;
            }
            throttle = $timeout(function() {
                $http.post('/index.php/marketing_campaigns/ajaxSaveCampaign',{"data":newCampaign,"campaign_id":id})
                    .success(function(data) {
                        deferred.resolve(data.data);
                        if(campaign.finishcampaign != null)
                            delete(campaign.finishcampaign);
                    }).error(function(msg, code) {
                        deferred.reject(msg);
                        $log.error(msg, code);
                    });
            }, 1000);

            return deferred.promise;
        }
        factory.getCurrentCampaign = function(){
            return factory.currentCampaign;
        }

        factory.getCampaign = function(id,params){
            if(params == undefined){
                params = {};
            }

            var deferred = $q.defer();
            if(factory.currentCampaign != undefined && factory.currentCampaign.campaign_id == id && !dirty){
                deferred.resolve(factory.currentCampaign);
                return deferred.promise;
            }
            dirty = false;
            $http({
                "url":'/index.php/marketing_campaigns/ajaxGetCampaign?campaign_id='+id,
                "method":"GET",
                "params":params
            }).success(function(data) {
                    deferred.resolve(data.data);
                    factory.currentCampaign = data.data;
                }).error(function(msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }

        var searchThrottle;
        factory.searchRecipients = function(term){
            if (searchThrottle)
                $timeout.cancel(searchThrottle);

            var deferred = $q.defer();
            searchThrottle = $timeout(function() {
                $http.get('/index.php/marketing_campaigns/recipient_search/ln_and_pn?term='+term)
                    .success(function(data) {
                        deferred.resolve(data);
                    }).error(function(msg, code) {
                        deferred.reject(msg);
                        $log.error(msg, code);
                    });
            }, 400);

            return deferred.promise;
        }
        factory.getRecipients = function(){
            var currentCampaign = this.getCurrentCampaign();

            var deferred = $q.defer();
            $http({
                "url":'/index.php/marketing_campaigns/ajaxGetRecipientList/'+factory.currentCampaign.campaign_id,
                "method":"GET"
            }).success(function(data) {
                deferred.resolve(data.data);
            }).error(function(msg, code) {
                deferred.reject(msg);
                $log.error(msg, code);
            });
            return deferred.promise;
        }


        factory.getAnalytics = function(event){
            var deferred = $q.defer();
            $http.get('/index.php/marketing_campaigns/ajaxGetCampaignAnalytics/'+factory.currentCampaign.campaign_id+"/"+event)
                .success(function(data) {
                    deferred.resolve(data.data);
                    factory.analytics = data.data;
                }).error(function(msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }

        factory.getAllCampaigns = function(){
            var deferred = $q.defer();
            $http.get('/index.php/marketing_campaigns/ajaxGetAllCampaigns')
                .success(function(data) {
                    deferred.resolve(data.data);
                    factory.allCampaigns = data.data;
                    factory.stats = data.stats;
                    factory.reminders = data.reminders;
                 }).error(function(msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }

        factory.saveAsTemplate = function(campaign){

            var deferred = $q.defer();
            var dlg = dialogs.create('/frontend/campaign-builder/app/components/createTemplate/createTemplate.html','customDialogCtrl',{},'md');
            dlg.result.then(function(template){
                $http.post('/index.php/marketing_campaigns/ajaxSaveTemplate',{"campaign_id":campaign.campaign_id,"campaign_name":template.name})
                    .success(function(data) {

                    }).error(function(msg, code) {

                    });
            },function(){
            });

            return deferred;
        }

        factory.deleteTemplate = function(template){

            var deferred = $q.defer();
            $http.get('/index.php/marketing_campaigns/ajaxDeleteTemplate?id='+template.id)
                .success(function(data) {

                }).error(function(msg, code) {

                });

            return deferred;
        }

        factory.getStats = function()
        {

            var deferred = $q.defer();
            if(factory.stats == undefined){
                $http.get('/index.php/marketing_campaigns/ajaxGetAllCampaigns')
                    .success(function(data) {
                        factory.stats = data.stats;
                        deferred.resolve(data.stats);
                    }).error(function(msg, code) {

                    });
            } else {
                deferred.resolve(factory.stats);
            }
            return deferred.promise;
        }
        return factory;
    });