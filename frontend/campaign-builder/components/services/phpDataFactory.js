'use strict';

cbApp.
    factory('PhpDataFactory', function( ) {
        var factory = {};
        factory.campaigns = phpFilledData.campaigns;
        if(phpFilledData.campaigns)
            factory.course_info = phpFilledData.campaigns.course_info;
        factory.prefilledTemplate  = phpFilledData.prefilledTemplate ;
        factory.logo  = phpFilledData.logo ;
        factory.logo_align  = phpFilledData.logo_align ;
        factory.email_hash  = phpFilledData.email_hash ;
        /*
         * Valid status = 'draft,sending,sent'
         *
         */
        factory.getCampaigns= function(){
                return factory.campaigns;
        }


        return factory;
    });