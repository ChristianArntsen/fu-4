var gulp = require('gulp'),
    livereload = require('gulp-livereload'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    connect = require('gulp-connect');


gulp.task('watch', function() {
    gulp.watch([
        'app/index.html',
        'app/main.js',
        'app/**/*.html',
        'app/**/*.js',
        'resources/styles/main.css',
        'components/**/*.js'
    ], ['html'])
        .on('error', swallowError)

});

gulp.task('html', function () {
    gulp.src('app/**/*.html')
        .pipe(connect.reload());
});


gulp.task('connect', function() {
    connect.server({
        root: '',
        port: 5000,
        livereload: true
    });
});

function swallowError (error) {

    //If you want details of the error in the console
    console.log(error.toString());

    this.emit('end');
}

gulp.task('dev', ['connect', 'watch']);