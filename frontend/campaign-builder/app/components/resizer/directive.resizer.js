cbApp.directive('resizer', function($document) {
    return {
        link: function($scope, $element, $attrs) {
            if($attrs.startingHeight){
                $element.css({
                    height: $attrs.startingHeight + 'px'
                });
            }


        },
        scope:{
          "height":"="
        },
        controller: function($scope,$attrs,$element){
            this.resize = function(event) {

                var rawDom = $element[0];
                var _x = 0;
                var _y = 0;
                var body = document.documentElement || document.body;
                var scrollX = window.pageXOffset || body.scrollLeft;
                var scrollY = window.pageYOffset || body.scrollTop;
                _x = rawDom.getBoundingClientRect().left + scrollX;
                _y = rawDom.getBoundingClientRect().top + scrollY;
                var offset = { left: _x, top:_y };;

                // Handle horizontal resizer
                var y = event.pageY;
                var height = y-offset.top;
                if(height<25)
                    return;
                $scope.height = height;
                $scope.$apply();
                $element.css({
                    height: height + 'px'
                });


            }
        }
    };
});