cbApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('app.recipient_viewer', {
            controller: 'recipientsViewerCtrl',
            templateUrl: '/frontend/campaign-builder/app/template_overview/recipients/recipients.html'
        });
}]);

cbApp.controller('recipientsViewerCtrl',
    function($scope,CampaignFactory) {

        $scope.init = function(){
            var promise = CampaignFactory.getRecipients();
            promise.then(function(data){
                $scope.recipients = data;
            })
        }
        $scope.init();
    });
