cbApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('app.dashboard', {
            url: '/dashboard',
            controller: 'dashboardCtrl',
            templateUrl: '/frontend/campaign-builder/app/dashboard/dashboard.html'
        });
}]);

cbApp.controller('dashboardCtrl', function($scope,PhpDataFactory,CampaignFactory,dialogs,$state) {

    $scope.hasRecipients = function(campaign){
        return campaign.recipients != null
            && campaign.recipients.groups != null
            && campaign.recipients.individuals != null
            && (campaign.recipients.groups.length != 0
            || campaign.recipients.individuals.length != 0)
    }

    $scope.loadCampaign = function(campaign){
        if(campaign.type == "text"){
            $state.go('app.template_overview', { 'campaign_id':campaign.campaign_id  });
            return;
        }
        if(!$scope.hasRecipients(campaign)){
            $state.go('app.template_overview', { 'campaign_id':campaign.campaign_id  });
            return;
        }
        if(campaign.json_content == null){
            $state.go('app.template_select', { 'campaign_id':campaign.campaign_id });
            return;
        }
        if(campaign.status == 'draft'){
            $state.go('app.template_builder', { 'campaign_id':campaign.campaign_id  });
            return;
        }
        $state.go('app.template_review', { 'campaign_id':campaign.campaign_id  });
    };

    $scope.deleteCampaign = function(campaign){
        var dlg = dialogs.confirm("You sure?","You're about to wipe out this campaign for good. You will lose it for forever, think about what you're doing.",{size:"md"});
        dlg.result.then(function(){
            campaign.loading.delete = true;
            var promise = CampaignFactory.deleteCampaign(campaign['campaign_id']);
            promise.then(function(data){
                campaign.loading.delete = false;
                var index = $scope.campaigns.drafts.indexOf(campaign);
                $scope.campaigns.drafts.splice(index,1);
            })
        });
    }
    $scope.copyCampaign = function(campaign){
        campaign.loading.copy = true;
        var promise = CampaignFactory.copyCampaign(campaign);
        promise.then(function(){
            campaign.loading.copy = false;
            $scope.init();
        });
    }

    $scope.previewControl = {};
    $scope.launchPreview = function(campaign){
        $scope.previewControl.openPreview(campaign);
    }

    $scope.deleteReminder = function(campaign){
            var dlg = dialogs.confirm("You sure?","You're about to wipe out this campaign for good. You will lose it for forever, think about what you're doing.",{size:"md"});
            dlg.result.then(function(){
                campaign.loading.delete = true;
                var promise = CampaignFactory.deleteCampaign(campaign['campaign_id']);
                promise.then(function(data){
                    campaign.loading.delete = false;
                    var index = $scope.campaigns.reminders.indexOf(campaign);
                    $scope.campaigns.reminders.splice(index,1);
                })
            });
    }
    $scope.pauseCampaign = function(campaign){
        if(campaign.pausing){
            return;
        }
        //If it's being sent out, it can't be canceled
        if(campaign.status == "sending"){
            var dlg = dialogs.notify("Can't do that","This campaign is actually being sent out right now.  We can't cancel in the middle of this process.",{size:"md"});
        } else {
            //If it's scheduled, move it to a draft
            campaign.loading.cancel = true;
            campaign.status = "draft";

            var promise = CampaignFactory.saveCampaign(campaign,campaign.campaign_id);
            promise.then(function(){

                campaign.loading.cancel = false;
                $scope.init();
            })
        }

    }

    $scope.campaigns = {};
    $scope.campaigns = PhpDataFactory.getCampaigns();
    $scope.init = function(){
        var promise = CampaignFactory.getAllCampaigns();
        promise.then(function(data){
            $scope.campaigns = data;
            console.log($scope.campaigns);
            $scope.stats = CampaignFactory.stats;
            $scope.reminders = CampaignFactory.reminders;
        })
    }
    $scope.isLimitWarning = function(usage, limit){
        if((limit - usage)< 200 && (limit - usage)> 0 ){
            return true;
        }
        return false;
    }
    $scope.isLimitDanger = function(usage, limit){
        if((limit - usage)< 0 ){
            return true;
        }
        return false;
    }
    $scope.init();
});