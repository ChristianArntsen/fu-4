var cbApp = angular.module('cbApp', [
    'ui.router',
    'angular-sortable-view',
    'tg.dynamicDirective',
    'angular-medium-editor',
    'dropzone',
    'ui.bootstrap',
    'tooltipster',
    'notify-toast',
    'angular-velocity',
    'localytics.directives',
    'autoFrame',
    'dialogs.main',
    'appHeader',
    'angular-ladda',
    'chart.js',
    'ui.bootstrap',
    'rruleRecurringSelect',
    'ui.bootstrap.datetimepicker'
]);

cbApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/dashboard');
    $stateProvider
        .state('app', {
            url: '',
            views: {
                'header': {
                    templateUrl: '/frontend/campaign-builder/app/header/header.html',
                    controller: 'mainCtrl'
                }
            }
        });
}]);

cbApp.controller('mainCtrl', function($scope,CampaignFactory,$state,dialogs,$http,$animate) {

    $scope.state = {};
    if (/PhantomJS/.test(window.navigator.userAgent)) {
        $animate.enabled(false);
    }
    $scope.createEditCampaign = function(){
        $scope.createLoading = true;
        var promise = CampaignFactory.saveCampaign({"title":"Default Campaign"});
        promise.then(function(data){
            $scope.createLoading = false;
            $state.go('app.template_overview',{"campaign_id":data.campaign_id})
        })
    }
    $scope.$on('$stateChangeSuccess',
        function(event, toState, toParams, fromState, fromParams){
            if (toState.name == "app.dashboard") {
                $scope.state.heading = "Dashboard";
                $scope.state.id = 0;
            }

            if (toState.name=="app.template_overview") {
                $scope.state.heading = "Campaign Overview";
                $scope.state.id = 1;
            }

            if (toState.name=="app.template_select") {
                $scope.state.heading = "Select a Template";
                $scope.state.id = 2;
            }

            if (toState.name=="app.template_builder") {
                $scope.state.heading = "Customize Template";
                $scope.state.id = 3;
            }

            if (toState.name=="app.template_review") {
                $scope.state.heading = "Schedule Campaign";
                $scope.state.id = 4;
            }

        })

    var currentStep = 1;
    $scope.nextStep = function(){
        var id = CampaignFactory.currentCampaign.campaign_id;

        CampaignFactory.saveCampaign();

        var currentState = $state.$current.toString();
        if(currentState == "app.dashboard"){
            $state.go('app.overview',{"campaign_id":id})
        } else if(currentState == "app.template_overview"){
            var promise = CampaignFactory.getStats();
            promise.then(function(data){
                if(CampaignFactory.currentCampaign.type == "email" && data.email.usage > data.email.limit){
                    dialogs.error("Oops","Seems you don't have enough credits to run another campaign.  Contact support get purchase some new credits or to be put on recurring billing.",{size:"md"});
                } else if(CampaignFactory.currentCampaign.type == "text" && data.text.usage > data.text.limit){
                    dialogs.error("Oops","Seems you don't have enough credits to run another campaign.  Contact support get purchase some new credits or to be put on recurring billing.",{size:"md"});
                } else {
                    if(
                        (CampaignFactory.currentCampaign.recipients_pretty == null ||
                        CampaignFactory.currentCampaign.recipients_pretty.length == 0)
                        && CampaignFactory.currentCampaign.recipient_reports.length == 0){
                        dialogs.error("I can't do that yet.","You need to select some recipients first.",{size:"md"});
                    } else {
                        if(CampaignFactory.currentCampaign.type == "text"){
                            $state.go('app.template_review',{"campaign_id":id})
                        } else {
                            $state.go('app.template_select',{"campaign_id":id})
                        }
                    }
                }
            });

        } else if(currentState == "app.template_select"){
            if(CampaignFactory.currentCampaign.json_content == undefined){
                dialogs.error("I can't do that yet.","You need to select a template below.",{size:"md"});
            } else {
                $state.go('app.template_builder',{"campaign_id":id})
            }
        } else if(currentState == "app.template_builder"){
            $state.go('app.template_review',{"campaign_id":id})
        }
    }
    $scope.prevStep = function(){
        var currentState = $state.$current.toString();
        var id = CampaignFactory.currentCampaign.campaign_id;
        if(currentState == "app.template_overview"){
            $state.go('app.dashboard',{"campaign_id":id})
        } else if(currentState == "app.template_select"){
            $state.go('app.template_overview',{"campaign_id":id})
        } else if(currentState == "app.template_builder"){
            $state.go('app.template_select',{"campaign_id":id})
        } else if(currentState == "app.template_review"){
            if(CampaignFactory.currentCampaign.type == "text"){
                $state.go('app.template_overview',{"campaign_id":id})
            } else {
                $state.go('app.template_builder',{"campaign_id":id})
            }
        }
    }

    $scope.loading = false;
    $scope.completeCampaign = function() {
        $scope.$broadcast('completeCampaign', true);
        $scope.loading = true;
    };
    $scope.$on('emailsent',function(){
        $scope.loading=false;
    })
    $scope.$on('emailfailed',function(){
        $scope.loading=false;
    })
    $scope.previewWaiting = false;
    $scope.previewEmail = function() {
        $scope.previewWaiting = true;
        CampaignFactory.waitForCampaignSave().then(function(){
            $scope.previewWaiting = false;
            $scope.$broadcast('previewEmail', {name: 'Campaign Name'});
        })
    };
    $scope.saveAsTemplate = function(){

        $scope.saveasloading = true;
        CampaignFactory.waitForCampaignSave().then(function(){
            var promise = $http.get("/index.php/marketing_campaigns/preview?campaign_id="+CampaignFactory.currentCampaign.campaign_id);
            promise.then(function(data){

                $scope.saveasloading = false;
                CampaignFactory.saveAsTemplate(CampaignFactory.currentCampaign);
            })
        })
    }

    $scope.shouldShowPreview = function(){
        var currentState = $state.$current.toString();
        if(currentState == "app.template_overview" || currentState == "app.template_select" || currentState == 'app.template_review'){
            return false;
        }
        else return true;
    }
    
    $scope.shouldShowComplete = function() {
        var currentState = $state.$current.toString();
        if(currentState == 'app.template_review'){
            return true;
        }
        else return false;
    }

    $scope.$watch(CampaignFactory.getCurrentCampaign,function(newVal,oldVal){
        $scope.currentCampaign = newVal;
    })

    $scope.canPrevious = function(){
        return CampaignFactory.currentCampaign.status == "draft";
    }
    $scope.canNext = function(){
        return CampaignFactory.currentCampaign.status == "draft";
    }

});