cbApp.config(function($stateProvider) {
    $stateProvider
        .state('template_preview', {
            url: '/template-preview/:campaign_id',
            views: {
                'header': {
                    templateUrl: '/frontend/campaign-builder/app/template_preview/template_preview.html',
                    controller: 'templatePreviewCtrl'
                }
            }
        });
});

cbApp.controller('templatePreviewCtrl',
    function($scope,$stateParams,CampaignFactory,$sce) {
        $scope.campaign = {};
        $scope.init = function(){
            if($stateParams.campaign_id != undefined){
                var promise = CampaignFactory.getCampaign($stateParams.campaign_id);
                promise.then(function(data){
                    $scope.campaign = data;
                    //To use a variable within an iframe you must use $sce
                    //See: https://docs.angularjs.org/api/ng/service/$sce
                    $scope.previewUrl = $sce.trustAsResourceUrl("/index.php/marketing_campaigns/preview?campaign_id="+$scope.campaign.campaign_id);
                })
            }

        }
        $scope.init();
        $scope.alert = function(){
            console.log("hey");
        }
    }).directive('iframeOnload', function(){
        return {
            scope: {
                callBack: '&iframeOnload'
            },
            link: function(scope, element, attrs){
                element.on('load', function(){
                    return scope.callBack();
                })
            }
        }});
