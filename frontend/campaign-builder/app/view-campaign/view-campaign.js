cbApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('viewCampaign', {
            url: '/view-campaign/:campaign_id',
            views: {
                'header': {
                    controller: 'viewCampaignCtrl',
                    templateUrl: '/frontend/campaign-builder/app/view-campaign/view-campaign.html'
                }
            }
        });
}]);

cbApp.controller('viewCampaignCtrl', function($scope, CampaignFactory, $state) {

    Date.prototype.addHours= function(h){
        this.setHours(this.getHours()+h);
        return this;
    }



    var processFacetsByHour = function(facets){
        var labels = [],
            opened  = [],
            clicked = [];
        for(i=0;i<23;i++){
            var dayOfWeek = moment().hour(i).format("H");
            labels.push(moment().hour(i).minute(0).format("hh:mm A"));
            if(facets[dayOfWeek] != undefined){
                if(facets[dayOfWeek]['click'] != undefined){
                    clicked.push(facets[dayOfWeek]['click'])
                } else {
                    clicked.push(0);
                }
                if(facets[dayOfWeek]['open'] != undefined){
                    opened.push(facets[dayOfWeek]['open'])
                } else {
                    opened.push(0)
                }
            } else {
                clicked.push(0)
                opened.push(0)
            }
        }
        return {
            "labels":labels,
            "opened":opened,
            "clicked":clicked
        };
    }
    var processFacetsDayofWeek = function(facets){
        var labels = [],
            opened  = [],
            clicked = [];
        for(i=0;i<7;i++){
            var dayOfWeek = moment().day(i).format("dddd");
            labels.push(dayOfWeek);
            if(facets[dayOfWeek] != undefined){
                if(facets[dayOfWeek]['click'] != undefined){
                    clicked.push(facets[dayOfWeek]['click'])
                } else {
                    clicked.push(0);
                }
                if(facets[dayOfWeek]['open'] != undefined){
                    opened.push(facets[dayOfWeek]['open'])
                } else {
                    opened.push(0)
                }
            } else {
                clicked.push(0)
                opened.push(0)
            }
        }
        return {
            "labels":labels,
            "opened":opened,
            "clicked":clicked
        };
    }
    var processFacets = function(facets){


        var labels = [],
            opened  = [],
            clicked = [],
            latestDate;
        for(var key in facets){
            if(labels.length >=48){
                continue;
            }
            var value = facets[key];
            var newLatestDate = new Date(key);
            if(latestDate === undefined){
                latestDate = newLatestDate;
                labels.push(moment(latestDate).format('MMMM Do, h a'));
            }
            while(newLatestDate > latestDate){
                latestDate.addHours(1)
                labels.push(moment(latestDate).format('MMMM Do, h a'));
                if(newLatestDate.getTime() != latestDate.getTime()){
                    opened.push(0);
                    clicked.push(0);
                }
            }

            if(value.open != undefined){
                opened.push(value.open);
            } else {
                opened.push(0);
            }

            if(value.click != undefined){
                clicked.push(value.click);
            } else {
                clicked.push(0);
            }

        }

        while(labels.length < 12){
            latestDate.addHours(1);
            labels.push(moment(latestDate).format('MMMM Do, h a'));
            opened.push(0);
            clicked.push(0);
        }

        return {
            "labels":labels,
            "opened":opened,
            "clicked":clicked
        };
    }
    $scope.loading = true;
    $scope.changeGrouping = function(group){
        if(group == "day,hour"){
            $scope.groupingTitle = "First 48 Hours";
        } else if(group == "dayname"){
            $scope.groupingTitle = "Stats by Day of Week";
        } else if(group == "hour"){
            $scope.groupingTitle = "Stats by Hour";
        }
        grouping = group;
        CampaignFactory.markAsDirty();
        $scope.refreshGraph();
    }
    var grouping = "day,hour";
    $scope.groupingTitle = "First 48 Hours"
    $scope.refreshGraph = function(){
        var promise = CampaignFactory.getCampaign($state.params.campaign_id,{"stats":true,"grouping":grouping}).then(function(data) {
            $scope.campaign = data;
            if(data.facets != undefined && data.facets.length != 0){
                if(grouping == "day,hour"){
                    $scope.facets = processFacets(data.facets);
                } else if(grouping== "dayname") {
                    $scope.facets = processFacetsDayofWeek(data.facets);
                } else if(grouping== "hour") {
                    $scope.facets = processFacetsByHour(data.facets);
                } else {
                    return false;
                }
                $scope.data = [
                    $scope.facets.clicked,
                    $scope.facets.opened
                ];
                $scope.labels = $scope.facets.labels;
            }
            $scope.rates ={};
            $scope.rates.courseClickRate = $scope.campaign.summary.clicked / $scope.campaign.summary.delivered;
            $scope.rates.courseOpenRate = $scope.campaign.summary.opened / $scope.campaign.summary.delivered ;
            $scope.rates.openRate = $scope.campaign.stats.opened / $scope.campaign.stats.delivered;
            $scope.rates.clickRate = $scope.campaign.stats.clicked/ $scope.campaign.stats.delivered;
            var siteOpenAvg = .46;
            var siteClickAvg = .35;


            $scope.loading  = false;


        });

    }
    $scope.refreshGraph();
});