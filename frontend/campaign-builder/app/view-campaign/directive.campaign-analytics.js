cbApp.directive('campaignAnalytics', function(CampaignFactory,$uibModal) {
    return {
        controller: function($scope, $element) {
            $scope.hideDetails = true;
            $scope.contacts = [
            ];
            $scope.getAnalytics = function(event){
                $scope.loadingAnalytics = true
                $scope.hideDetails = false;
                CampaignFactory.getAnalytics(event).then(function(data){
                    $scope.contacts = data;
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: '/frontend/campaign-builder/app/view-campaign/directive.emaillist.html',
                        controller: 'emaillistCtrl',
                        backdrop:true,
                        resolve: {
                            contacts: function () {
                                return $scope.contacts;
                            }
                        }
                    });
                });

                return true;
            }
        },
        scope:{
            "campaign":"="
        },
        templateUrl: '/frontend/campaign-builder/app/view-campaign/directive.campaign-analytics.html'
    }
});