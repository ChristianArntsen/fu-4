cbApp.directive('componentDivider', function() {
    return {
        restrict: 'E',
        scope: {
            componentModel: '='
        },
        link: function(scope, element, attrs, tabsCtrl) {

        },
        templateUrl: '/frontend/campaign-builder/app/template_builder/components/directive.divider.html',
        controller: function($scope){
        }
    };
});