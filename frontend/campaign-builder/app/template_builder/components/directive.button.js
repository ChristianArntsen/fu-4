cbApp.directive('componentButton', function() {
    return {
        restrict: 'E',
        scope: {
            componentModel: '='
        },
        link: function(scope, element, attrs, tabsCtrl) {

        },
        templateUrl: '/frontend/campaign-builder/app/template_builder/components/directive.button.html',
        controller: function($scope){
        }
    };
});