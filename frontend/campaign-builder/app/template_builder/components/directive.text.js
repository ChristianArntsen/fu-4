


cbApp.directive('componentText', function() {
    return {
        restrict: 'E',
        scope: {
            componentModel: '='
        },
        link: function(scope, element, attrs, tabsCtrl) {
        },
        templateUrl: '/frontend/campaign-builder/app/template_builder/components/directive.text.html',
        controller: function($scope,$sce){
            if (/PhantomJS/.test(window.navigator.userAgent)) {
                $scope.isPhantomJS = true;
                $scope.trustedHtml  = $sce.trustAsHtml($scope.componentModel.data);
            }
            $scope.mediumBindOptions = {
                "toolbar":{
                    "buttons": [ "customvariables","bold","underline", "anchor", "h1", "h2", "quote","justifyLeft","justifyCenter","justifyRight","fontname","fontsize"]
                },
                "placeholder": {
                    text: "Enter Text"
                },
                "buttonLabels": "fontawesome",
                extensions: {
                    'customvariables':  new MediumEditor.extensions.customvariables()
                }
            };
        }
    };
});
