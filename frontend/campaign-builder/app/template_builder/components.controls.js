cbApp
    .directive('componentControls', function() {
        return {
            restrict: 'E',
            scope:{"components":"=","key":"="},
            controller:function($scope,ComponentFactory){
                $scope.removeItem = function(key){
                    $scope.components.splice($scope.key,1);
                }
                $scope.editItem = function(){
                    ComponentFactory.activeComponent = $scope.components[$scope.key];
                }

                $scope.component = $scope.components[$scope.key];
            },
            template: '<div class="cb-element-controls">' +
                // only show this one on "button" and "image"
            '<div class="cb-element-controls-item grab" sv-handle tooltip title="Drag to Sort">' +
            '<i class="gi gi-sort"></i>' +
            '</div>' +
            '<div class="cb-element-controls-item" ng-click="removeItem()" tooltip title="Delete">' +
            '<i class="gi gi-trash"></i>' +
            '</div>' +
            '</div>'
        }
    });