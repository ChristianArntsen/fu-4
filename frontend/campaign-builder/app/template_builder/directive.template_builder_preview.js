cbApp.directive('templateBuilderPreview', function() {
   return {
       scope:{
            "control":"="
       },
       link: function(scope,element,attrs){
           scope.internalControl = scope.control || {};
           scope.internalControl.openPreview = function(campaign){
                scope.launchPreview(campaign);
           }
       },
       controller: function($scope, CampaignFactory,$http,$sce) {



           var body = angular.element($('body'));
           
           $scope.pt = {
               container: false,
               web: false,
               mobile: false
           };
           $scope.$on('previewEmail', function(event, data) {
               $scope.launchPreview(data,true);
           });

           $scope.launchPreview = function(campaign,rerender){
               $scope.pt.name = "Preview";
               $scope.pt.container = true;
               $scope.switchWeb();
               body.addClass('hideScroll');

               $scope.pt.loading = true;
               if(campaign == undefined || campaign.campaign_id == undefined){
                   var campaign = CampaignFactory.getCurrentCampaign();
               }
               $scope.campaign = campaign;
               if(rerender){
                   var promise = $http.get("/index.php/marketing_campaigns/preview?campaign_id="+campaign.campaign_id);
                   promise.then(function(data){
                       $scope.pt.loading = false;
                       $scope.previewUrl = $sce.trustAsResourceUrl("/index.php/email/hostedEmail?email="+campaign.remote_hash);
                       $scope.$broadcast('iframe-loadeded');
                   })
               } else {
                   $scope.previewUrl = $sce.trustAsResourceUrl("/index.php/email/hostedEmail?email="+campaign.remote_hash);
                   $scope.$broadcast('iframe-loadeded');
                   $scope.pt.loading = false;
               }

           }


           
           $scope.closeContainer = function() {
               $scope.pt.container = false;
               body.removeClass('hideScroll');
           };
           
           $scope.switchWeb = function() {
               $scope.pt.web = true;
               $scope.pt.mobile = false;
           };
           
           $scope.switchMobile = function() {
               $scope.pt.web = false;
               $scope.pt.mobile = true;
           };
           
       },
       templateUrl: '/frontend/campaign-builder/app/template_builder/template_builder_preview.html'
   } 
});