cbApp.directive('section', function($compile) {
    return {

        restrict: 'EA',
        scope: {
            section: '=',
            sections: '=',
            sectionKey: '='
        },
        templateUrl: "/frontend/campaign-builder/app/template_builder/directive.section.html",
        controller:function($scope){
        }
    };
});