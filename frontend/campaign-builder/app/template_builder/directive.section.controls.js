cbApp
    .directive('sectionControls', function() {
        return {
            restrict: 'E',
            scope:{"sections":"=","key":"="},
            controller:function($scope){
                $scope.deleteSection = function(){
                    $scope.sections.splice($scope.key,1)
                }
            },
            template: '<div class="cb-section-controls" ng-show="sections.length > 1">' +
                // only show this one on "button" and "image"
            '<div class="cb-section-controls-item grab" sv-handle >' +
            '<i class="gi gi-sort"></i>' +
            '</div>' +
            '<div class="cb-section-controls-item" ng-click="deleteSection()" >' +
            '<i class="gi gi-trash"></i>' +
            '</div>' +
            '</div>'
        }
    });