'use strict';

cbApp.
    factory('ComponentFactory', function() {
        var ComponentFactory = {};
        ComponentFactory.activeComponent = {};

        return ComponentFactory;
    });