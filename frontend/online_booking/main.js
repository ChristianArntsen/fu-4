// Override ajax function to always pass API key
$(document).ajaxSend(function(e, xhr, options){
    xhr.setRequestHeader("Api-key", API_KEY);
});


function init_member_search(obj, callback){

	// Typeahead customer suggestion engine
	var customerSearch = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: BASE_API_URL + 'members?q=%QUERY',
			wildcard: '%QUERY',
			cache: false
		},
		limit: 30
	});

	customerSearch.initialize();

	obj.typeahead({
		hint: false,
		highlight: true,
		minLength: 1,
	},
	{
		limit: 101,
		name: 'customers',
		display: function(customer){
			return customer.first_name+' '+customer.last_name;
		},
		source: customerSearch.ttAdapter(),
		async: true,
		templates: {
			suggestion: _.template($('#template_member_search').html())
		}

	}).on('typeahead:selected', function(e, customer, list){
		customerSearch.clearRemoteCache();
		return callback(e, customer, list);
	});

	obj.data('Bloodhound', customerSearch);
}

// Underscore init, custom functions
_.mixin({
	
	round: function(value, places) {	
		if(typeof(value) != 'number'){
			value = parseFloat(value);
		}
		if(isNaN(value)){
			value = 0;
		}
		if(!places){
			places = 2;
		}
		return +(Math.round(value + "e+" + places)  + "e-" + places);
	},

	shorten: function(str, maxLength){
		if(str.length > maxLength + 3){
			return str.substring(0, maxLength) + '...';
		}
		return str;
	},

	formatPhoneNumber: function(phone){
		if(!phone || phone == '' || phone == null){
			return '';
		}
		// First, strip everything but numbers
		phone = phone.replace(/[^0-9]/g, '');
		return '('+phone.substr(0, 3) + ') ' + phone.substr(3, 3) + '-' + phone.substr(6,4);	
	},

	dropdown: function(data, attrs, current_value){
		var container = $('<div />');
		var menu = $('<select />').attr(attrs);
		container.append(menu);

		var options = '';
		_.each(data, function(value, key){
			var selected = '';
			if(key == current_value){
				selected = 'selected';
			}
			options += "<option value='"+key+"' "+selected+">"+value+"</option>";
		});

		menu.html(options);
		return container.html();
	}
});

// Main router to handle page links
var BookingRouter = Backbone.Router.extend({

	routes: {
		"": "index",
		"teetimes":	"teetimes",
		"confirmation/:reservationId": "confirmation",
		"course": "course",
		"account": "account",
		"account/:section": "account",
		"account/giftcards/:giftcard_id": "giftcardDetails",
		"account/rainchecks/:raincheck_id": "raincheckDetails",
		"welcome": "welcome",
		"giftcard": "giftcardPurchase"
	},
	
	index: function(){
		if(App.data.schedules.length == 0){
			this.navigate('welcome', {trigger: true});
		}else{
			this.navigate('teetimes', {trigger: true});
		}
	},
	
	initialize: function(){
		this.listenTo(App.data.user, 'change:logged_in', this.redirectLogout);
		this.on('route', this.trackPageView);
	},

	welcome: function(){
		var page = new OneColLayout();
		App.page.show(page);
		page.content.show( new WelcomeMessageView({model: App.data.course}));
	},
	
	trackPageView: function(){
		var url = Backbone.history.root + Backbone.history.getFragment();
		ga('send', 'pageview', url);	
	},
	
	redirectLogout: function(user){
		if(!user.get('logged_in')){
			if(App.data.schedules.length == 0){
				this.navigate('welcome', {trigger: true});
			}else{
				this.navigate('teetimes', {trigger: true});	
			}
			this.checkLogin();
		}
	},
	
	// If course has online booking password protected, show message 
	// on page for user to log in or register
	checkLogin: function(){
		if((App.settings.online_booking_protected == '1' || 
			App.data.course.get('online_booking_protected') == '1') && 
			!App.data.user.get('logged_in')
		){
			var page = new OneColLayout();
			App.page.show(page);
			page.content.show( new TeeTimeLoginView() );
			return false;		
		}
		return true;
	},
	
	// View teetimes page
	teetimes: function(){

		if(App.data.schedules.length == 0){
			this.navigate('welcome', {trigger: true});
			return true;
		}

		if(!this.checkLogin()){
			return true;
		}
		
		// If no schedule is selected,
		var schedule = App.data.schedules.findWhere({selected: true});
		if(schedule == undefined){
			var page = new TwoColLayout();
			App.page.show(page);			
			page.nav.show( new FilterView({model: App.data.filters, collection: App.data.times}) );
			page.content.show( new TimeListView({collection: App.data.times, template: template, min_players: min_players}) );
			return true;			
		}
		
		// If a booking class is set, but doesn't belong to current schedule, reset it
		var bookingClasses = [];
		var bookingClass = false;
		if(schedule && schedule.has('booking_classes')){
			bookingClasses = schedule.get('booking_classes');
			bookingClass = bookingClasses.get( App.data.filters.get('booking_class') );
		}
		
		if(!bookingClass){
			App.data.filters.set('booking_class', false);
		}
		
		// If booking class is selected, or not required, display teetimes
		if(bookingClasses.length == 0 || (App.data.filters.get('booking_class') && App.data.filters.get('booking_class') != 'false')){
			
			var template = 'public';
			if(bookingClass && bookingClass.get('show_full_details') == 1){
				template = 'private';
			}
			
			var min_players = 1;
			if(bookingClass && bookingClass.get('minimum_players')){
				min_players = parseInt(bookingClass.get('minimum_players'));
			}
			
			var page = new TwoColLayout();
			App.page.show(page);			
			
			page.nav.show( new FilterView({model: App.data.filters, collection: App.data.times}) );
			page.content.show( new TimeListView({collection: App.data.times, template: template, min_players: min_players}) );
			page.booking_class.show( new BookingClassFilterView({model: bookingClass}) );
			
		// If no booking class has been selected, show list of booking classes		
		}else{
			var page = new OneColLayout();
			App.page.show(page);			
			
			page.content.show( new BookingClassListView({collection: schedule.get('booking_classes')}) );
		}
	},
	
	// Reservation confirmation/thank you page
	confirmation: function(reservationId){
		var reservation = App.data.user.get('reservations').get(reservationId);
		$('#modal').modal('hide');
		
		var page = new OneColLayout();
		App.page.show(page);
		page.content.show( new ReservationConfirmationView({model:reservation}) );
	},
	
	course: function(){
		$('#modal').modal('hide');
		
		var page = new OneColLayout();
		App.page.show(page);
		page.content.show( new CourseInformationView({model:App.data.course}) );
	},
	
	account: function(section){
		
		$('#modal').modal('hide');
		
		// If user is not logged in, redirect them to the teetimes page 
		// and show the login window
		if(!App.data.user.get('logged_in')){
			App.router.navigate('teetimes', {trigger:true});
			
			var loginWindow = new LoginView();
			loginWindow.show();
			return false;
		}

		var page = new OneColLayout();
		App.page.show(page);
		page.content.show( new UserProfileView({model: App.data.user, section: section}) );
	},

	giftcardDetails: function(giftcardId){
		var giftcard = App.data.user.get('gift_cards').get(giftcardId);
		var page = new OneColLayout();
		App.page.show(page);
		page.content.show( new GiftCardDetailsView({model: giftcard}) );
	},

	raincheckDetails: function(raincheckId){
		var raincheck = App.data.user.get('rainchecks').get(raincheckId);
		var page = new OneColLayout();
		App.page.show(page);
		page.content.show( new RaincheckDetailsView({model: raincheck}) );
	},

	giftcardPurchase: function(){
		
		if(!App.data.course || App.data.course.get('online_giftcard_purchases') == 0 || !App.data.course.get('credit_card_provider')){
			App.router.navigate('teetimes', {trigger: true});
			return false;
		}

		$('#modal').modal('hide');

		// If user is not logged in, redirect them to the teetimes page 
		// and show the login window
		if(!App.data.user.get('logged_in')){
			App.router.navigate('teetimes', {trigger: true});
			
			var loginWindow = new LoginView({redirect: 'giftcard'});
			loginWindow.show();
			return false;
		}		

		var page = new OneColLayout();
		App.page.show(page);
		page.content.show( new PurchaseGiftcardView({model:App.data.course}) );
	}
});

var App = new Backbone.Marionette.Application();

App.addRegions({
	nav: '#navigation',
	page: '#page'
});

App.addInitializer(function(options){
	this.data = {};
	this.data.status = new Backbone.Model({'loading': false});	
	this.data.course = new Course(COURSE);
	this.data.courses = new CourseCollection(COURSES);
    this.data.schedules = new ScheduleCollection(SCHEDULES);
    this.data.times = new TimeCollection();
	this.data.user = new User(USER);
	this.data.filters = new Filter(DEFAULT_FILTER);

	this.settings = SETTINGS;

	if(SETTINGS.currency_symbol == 0 ) SETTINGS.currency_symbol = "$";
	accounting.settings.currency.symbol  = SETTINGS.currency_symbol;

	this.router = new BookingRouter();
	this.notify = new NotificationView();

	var navView = new NavigationView({userModel: this.data.user, courseModel: this.data.course});
	navView.listenTo(this.router, 'route', navView.highlightLink);
	this.nav.show(navView);
});

App.on('initialize:after', function(){
	Backbone.history.start({pushState: false, root: BASE_URL + COURSE_ID});
	this.data.times.refresh();
});

App.vent.on('reservation', function(reservation){
		
	// If the user is not pre-paying for tee time, show the confirmation page
	if(!reservation.get('is_paying')){
		var reservationId = reservation.get('teetime_id');
		ga('send', 'event', 'Online Booking', 'Tee Time Reservation', App.data.course.getCurrentSchedule());
		App.router.navigate('confirmation/' + reservationId, {trigger: true});		
	}
});

App.vent.on('notification', function (data) {
    var delay = 4000;
    if (!data.type) {
        data.type = 'info';
    } else if (data.type == 'error') {
        data.type = 'danger';
    }

    if (!data.msg) {
        return false;
    }
    if (typeof(data.delay) != 'undefined') {
        delay = data.delay;
    }

    $.bootstrapGrowl(data.msg, {type: data.type, width: 'auto', align: 'center', delay: delay});
});

$(document).ajaxComplete(function(event, response, options){
	
	var code = 0;
	if(response && response.code){
		code = response.code;
	}else if(response && response.status){
		code = response.status;
	}
	
	// If tee time is no longer available
	if(code == 409 && response.responseJSON && !response.responseJSON.success){
        App.vent.trigger('notification', {msg: response.responseJSON.msg, type: 'error', delay:5000});
		
		// Remove time from list
		var tee_time = App.data.times.findWhere({'time': response.responseJSON.time, 'schedule_id': response.responseJSON.schdedule_id});
		if(tee_time){
			App.data.times.remove(tee_time);
		}
		$('#modal').modal('hide');
	}
});

App.start();
