var AddCreditCardView = Backbone.View.extend({
	id: 'add-credit-card',
	className: 'modal-dialog',
	template: _.template( $('#template_add_credit_card').html() ),
	etsForm: _.template( $('#template_ets_credit_card_form').html() ),
	
	events: {
		'click button.add-card': 'addCard',
		'click button.save-address': 'saveBillingAddress'
	},
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
		
		// Once a new credit card is added, finish the reservation
		this.listenTo(App.data.user.get('credit_cards'), 'add', this.finishReservation);
		this.listenTo(App.data.user.get('reservations'), 'error', this.showError);
	},
	
    close: function() {
		this.remove();
    },
	
	showError: function(e, response){
		alert(response.responseJSON.msg);
		$('#modal').modal('hide');
	},

    render: function() {
		var attr = {};
		var course_id = this.model.get('course_id');
		var view = this;
		
		var url = SITE_URL + 'credit_card_window/'+ course_id +'/add_card';
		var data = {
			schedule_id: this.model.get('schedule_id'),
			course_id: course_id
		};

		attr.schedule_id = this.model.get('schedule_id');
		attr.iframe_url = url;
		
		var html = this.template(attr);
		this.$el.html(html);

		var merchant = App.data.course.get('credit_card_provider');
		if(merchant == 'ets'){
			view.$el.find('div.modal-body').html( this.etsForm() );
			$.post(url, data, function(response){
				view.$el.find('div.modal-body').append(response);
			}, 'html');
		
		}else if(merchant == 'element'){
			view.$el.loadMask();
			$.post(url, data, function(response){
				view.$el.find('div.modal-body').html(response);
			}, 'html');

		}else{
			view.$el.find('div.modal-body').css('padding', '0px');
		}
        this.$el.find('#terms_and_conditions').html(TERMS_AND_CONDITIONS);
        this.$el.find('#terms_and_conditions a').attr('target', '_blank');

        $('#modal').html(this.el);
		return this;
    },
    
    show: function(){
		this.render();
		$('#modal').modal();
	},
	
	finishReservation: function(creditCardModel){	
		
		var credit_card_id = creditCardModel.get('credit_card_id');
		
		// If adding card trying to book tee time
		if(this.model instanceof Reservation){
			this.model.set('credit_card_id', credit_card_id);
			
			// Complete reservation
			App.data.user.get('reservations').create(this.model.attributes);
		
		// If adding card trying to pay invoice
		}else if(this.model instanceof Invoice){	
			var creditCardView = new InvoiceSelectCreditCardView({model: this.model, selected_card: credit_card_id });
			creditCardView.show();			
		
		// If just adding card to profile
		}else{
			$('#modal').modal('hide');
		}
	},

	// Book teetime using selected credit card
	addCard: function(event){
		
		var model = this.model;
		var form = this.$el.find('#add_credit_card_form');
		form.bootstrapValidator('validate');
		
		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}	

		// Send credit card details to ETS
		if(App.data.course.get('credit_card_provider') == 'ets'){
			if(model){
				this.$el.find('button.add-card').button('loading');
			}else{
				this.$el.find('button.add-card').data('loading-text', 'Adding card...').button('loading');
			}
			ETSPayment.submitETSPayment();
		}
		return false;
	}
});
