var PurchaseReservationView = Backbone.View.extend({
	
	id: 'purchase_reservation',
	className: 'modal-dialog',
	template: _.template( $('#template_purchase_reservation').html() ),
	
	events: {
		'click button.purchase': 'purchase',
		'click .players': 'setPlayers',
		'click .carts': 'setCarts'			
	},
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});

		this.listenTo(this.model, 'change:pay_total', this.renderPrice);
		App.data.last_reservation = this.model;
	},
	
    close: function() {
		this.remove();
    },

    render: function() {
		
		this.model.calculateTotal();
		this.model.calculatePurchaseTotal();

		var min_required_paying = 2;
		if(App.settings.minimum_players > min_required_paying){
			min_required_paying = App.settings.minimum_players;
		}		

		if(this.model.get('pay_players') < min_required_paying){
			this.model.set('pay_players', min_required_paying);
		}				
		var attributes = this.model.attributes;	
		var schedule = App.data.schedules.findWhere({selected: true});
		
		attributes.reservation_time = moment(attributes.time).format('h:mma');
		attributes.reservation_day = moment(attributes.time).format('MMM D, YYYY');	
		attributes.booking_carts = App.settings.booking_carts;
		attributes.minimum_players = parseInt(App.settings.minimum_players);
		attributes.min_required_paying = min_required_paying;

		var html = this.template(attributes);
		this.$el.html(html);
		$('#modal').html(this.el);
		ga('send', 'event', 'Online Booking', 'View Purchase Form', App.data.course.getCurrentSchedule());

		return this;
    },
    
    renderPrice: function(){
		var total = this.model.get('pay_total');
		var subtotal = this.model.get('pay_subtotal');
		var discount = this.model.get('discount');
		this.$el.find('span.subtotal').text( accounting.formatMoney(subtotal) );
		this.$el.find('span.discount').text( accounting.formatMoney(discount) );
		this.$el.find('span.total').html('<strong>' + accounting.formatMoney(total) + '</strong>');
	},

    show: function(){
		this.render();
		$('#modal').modal();
	},
	
	setPlayers: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('pay_players', value);	
	},
	
	setCarts: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		
		var carts = false;
		if(value == 'yes'){
			carts = true;
		}
		this.model.set('pay_carts', carts);		
	},
	
	// Show credit card form to purchase the reservation
	purchase: function(){
		
		var teetime_id = this.model.get('teetime_id');
		var players = this.model.get('pay_players');
		var course_id = this.model.get('course_id');
		var carts = this.model.get('pay_carts');

		var data = {
			players: players,
			carts: carts,
			time: this.model.get('time'),
			holes: this.model.get('holes'),
			schedule_id: this.model.get('schedule_id'),
			special_id: this.model.get('special_id'),
			course_id: course_id,
			recipient: 'foreup',
			teetime_id: teetime_id,
			booking_class_id: this.model.get('booking_class_id'),
			promo_code: this.model.get('promo_code'),
			player_list: this.model.get('player_list'),
            pending_reservation_id: pending_reservation_obj.get_id()
		};

		var queryStr = $.param(data);
		var url = SITE_URL + 'credit_card_window/'+ course_id +'/purchase?'+queryStr;				
		
		this.$el.find('.modal-body').html('<div id="mercury_loader"><h2>Loading form...</h2>'+
			'<div class="progress progress-striped active">' +
			'<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>' +
			'</div></div><div id="foreign_currency_info"></div>'+
            '<p class="bg-info" style="padding:10px">Do not refresh this page or press the back button while processing a payment.</p>'+
			'<iframe id="mercury_iframe" onLoad="$(\'#mercury_loader\').remove();" ' +
				'src="'+url+'" style="border: none; display: block; width: 100%; height: 540px; padding: 0px; margin: 0px;"></iframe><small>'+PAYMENT_WARNING+'</small><small id="terms_and_conditions">'+TERMS_AND_CONDITIONS+'</small>');
		
		this.$el.find('.modal-footer').hide();
        this.$el.find('#terms_and_conditions a').attr('target', '_blank');
    }
});