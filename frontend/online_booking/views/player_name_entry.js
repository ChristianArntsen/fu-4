var PlayerNameEntryView = Backbone.View.extend({
	
	id: 'player-name-entry',
	className: 'modal-dialog',
	template: _.template( $('#template_player_name_entry').html() ),
	
	events: {
		'click button.guest': 'addGuest',
		'click button.continue': 'continue'
	},
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
	},
	
    close: function() {
		this.remove();
    },

    render: function() {

		var attributes = _.clone(this.model.attributes);
		attributes.user = App.data.user.attributes;

		var html = this.template(attributes);
		this.$el.html(html);
		var view = this;

		init_member_search(this.$el.find('input.member-search'), function(e, customer){
			var input = $(e.currentTarget);
			input.val(customer.first_name+' '+customer.last_name);
			input.data('member-id', customer.person_id);
		});

		$('#modal').html(this.el);
		return this;
    },
    
    show: function(){
		this.render();
		$('#modal').modal();
	},

	addGuest: function(e){
		var button = $(e.currentTarget);
		var row = button.parents('div.row');
		var input = row.find('input.member-search');
		input.val('Guest').data('member-id', null);
		return false;
	},

	continue: function(e){
		
		var continueBtn = this.$el.find('button.continue');
		continueBtn.button('loading');
		
		var players = [];
		$.each(this.$el.find('input.member-search'), function(index, obj){
			players.push({
				position: $(this).data('position'),
				name: $(this).val(),
				person_id: $(this).data('member-id')
			});
		});

		this.model.set('player_list', players);

		// If tee time is available for purchase, show payment
		// method selection window
		if(this.model.canPurchase() && this.model.get('total') > 0){
			var paymentMethod = new PaymentSelectionView({model: this.model});
			paymentMethod.show();
			return false;
		}

		// If a credit card is required to book the time, show credit card selection window
		if(this.model.get('require_credit_card') == 1 && !this.model.get('credit_card_id')){
			var creditCardView = new SelectCreditCardView({model: this.model});
			creditCardView.show();
			return false;
		}

		App.data.user.get('reservations').create(this.model.attributes, {wait: true, error: function(){
			continueBtn.button('reset');
		}});

		return false;
	}
});