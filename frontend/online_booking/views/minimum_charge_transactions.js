var MinimumChargeTransactionView = Backbone.View.extend({
	tagName: 'tr',
	className: '',
	template: _.template( $('#template_minimum_charge_transaction').html() ),
	
	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});

var MinimumChargeTransactionListView = Backbone.View.extend({
	tagName: 'table',
	className: 'table table-striped table-responsive purchases',
	
	render: function(){
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('<thead><tr><th class="sale">Sale #</th><th class="date">Date</th><th style="width: 50%"">Item</th><th>Subtotal</th><th>Tax</th><th class="total">Total</th></tr></thead><tbody></tbody>');
		
		if(this.collection.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderTransaction, this);
		}

		return this;
	},

	renderEmpty: function(){
		this.$el.find('tbody').append('<tr class="purchase empty muted"><td colspan="5">No purchases available</td></tr>');
	},
	
	renderTransaction: function(purchase){
		this.$el.append( new MinimumChargeTransactionView({model: purchase}).render().el );
	}
});