var hasStorage = (function() {
    try {
        localStorage.setItem(mod, mod);
        localStorage.removeItem(mod);
        return true;
    } catch (exception) {
        return false;
    }
}());

var TimeView = Backbone.View.extend({
	tagName: 'li',
	className: function(){
		var classes = 'time col-md-4 col-sm-6 col-xs-12';
		if(IS_GROUP){
			classes += ' aggregate';
		}
		return classes;
	},
	template: _.template( $('#template_time').html() ),
	
	initialize: function(options){
		this.options = options;
		if(options && options.template && options.template == 'private'){
			this.template = _.template( $('#template_time_private').html() );
			this.setElement('<tr class="time"></tr>');
		}
	},
	
	events: {
		'click': 'viewTime'
	},
	
	render: function(){
		var attributes = this.model.attributes;
		
		if(App.settings.booking_carts == "0"){
			attributes.cart_fee = false;
		}
		
		attributes.hide_prices = false;
		if(App.settings.hide_online_prices){
			attributes.hide_prices = true;
		}

		attributes.show_course_name = false;
		if(IS_GROUP){
			attributes.show_course_name = true;
		}
		
		if(this.options && this.options.min_players){
			attributes.min_players = this.options.min_players;
		}

		this.$el.html(this.template(attributes));
		return this;
	},
	
	// Adds the selected teetime to cart
	viewTime: function(){
		var attributes = _.clone(this.model.attributes);
		attributes.players = App.data.filters.get('players');
		attributes.holes = App.data.filters.get('holes');
		
		if(attributes.players == 0){
			attributes.players = 1;
		}
		
		// Create new reservation defaulted to filter settings
		var reservation = new Reservation(attributes);

		// Show modal window with new time
		var bookTimeView = new BookTimeView({model: reservation});
		bookTimeView.show();
	}
});

var TimeListView = Backbone.View.extend({
	tagName: 'ul',
	className: 'col-md-12',
	id: 'times',
	
	initialize: function(options){	
		this.options = options;
		this.listenTo(this.collection, "reset add remove", this.render);
		this.listenTo(App.data.status, "change:loading", this.toggleLoading);
		
		if(this.options && this.options.template == 'private'){
			this.setElement('<div id="#times" class="col-md-12"></div>');
		}
	},
	
	toggleLoading: function(model){
		if(model.get('loading')){
			if(this.options && this.options.template == 'private'){
				this.$el.find('tbody').prepend('<tr class="loading-wrapper"><td class="loading" colspan="3"><h1>Loading Tee times...</h1><img src="'+URL+'/images/loading_golfball2.gif" /></td></tr>');
			}else{
				this.$el.prepend('<li class="loading-wrapper loading"><h1>Loading Tee times...</h1><img src="'+URL+'/images/loading_golfball2.gif" /></li>');
			}
		}else{
			this.$el.find('.loading-wrapper').remove();
		}
	},
	
	render: function() {
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('');
		
		if(this.options.template && this.options.template == 'private'){
			this.$el.html('<table class="table private-times" id="times"><thead><tr>'+
				'<th class="details">Details</th>' +
				'<th class="player hidden-xs" colspan="4">Players</th>'+
				'<th class="player hidden-sm hidden-md hidden-lg">Players</th>'+
				'<th class="reserve hidden-sm hidden-md hidden-lg">&nbsp;</th>'+
			'</tr></thead><tbody></tbody></table>');
		}
		
		if(this.collection.models.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderTime, this);
		}
		this.toggleLoading( App.data.status );
		return this;
	},
	
	renderEmpty: function(){
		
		var html = '';
		var message = '<h1>Use Time/Day filters to find desired teetime</h1>'+
			'<p>To see more times, try adjusting the filters (date, holes, players, or time of day)</p>';

		if(this.collection.message){
			message = '<h1>No tee times available</h1><p>' + this.collection.message +'</p>';
		}

		if(this.options && this.options.template == 'private'){
			html = '<tr><td colspan="5" class="time empty muted">' + message + '</td></tr>';	
		}else{
			html = '<li class="time empty muted">' + message + '</li>';			
		}
		
		this.$el.append(html);
	},
	
	renderTime: function(time){
		var data = {};
		data = this.options;
		data.model = time;
		
		var target = this.$el;
		if(this.options && this.options.template == 'private'){
			target = this.$el.find('tbody');
		}
		
		target.append( new TimeView(data).render().el );
	}
});

var BookTimeView = Backbone.View.extend({
	
	id: 'book_time',
	className: 'modal-dialog',
	template: _.template( $('#template_book_time').html() ),

	events: {
		'click .book': 'completeBooking',
		'click .players .btn': 'setPlayers',
		'click .carts .btn': 'setCarts'
	},

    initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
            pending_reservation_obj.remove();
		});
		
		this.listenTo(this.model, 'change:total', this.renderPrice);
		this.listenTo(this.model, 'invalid', this.validationError);
		this.listenTo(App.vent, 'error', this.showError);			
	},
	
    close: function() {
		this.remove();
    },

    render: function() {
    	view = this;
		this.model.set('players', this.model.get('available_spots'));
		var attributes = this.model.attributes;	
		var schedule = App.data.schedules.findWhere({selected:true});
		
		attributes.reservation_time = moment(attributes.time).format('h:mma');
		attributes.reservation_day = moment(attributes.time).format('MMM D, YYYY');

		attributes.show_course_info = false;
		attributes.course = false;
		if(IS_GROUP){
			App.data.course = App.data.courses.get(attributes.course_id);
			attributes.show_course_info = true;
			attributes.course = App.data.courses.get(attributes.course_id);
		}

		var html = this.template(attributes);
		this.$el.html(html);
		$('#modal').html(this.el);
		ga('send', 'event', 'Online Booking', 'View Tee Time', attributes.course_name +' - '+attributes.schedule_name);

		if(SETTINGS.enable_captcha_online){
            widgetId = grecaptcha.render('recaptcha', {
                'sitekey' : '6LeQqCIUAAAAAI8nAaJahskkE6mNTI9cBBRseaFO',
                'theme' : 'light',
                'size' : 'invisible',
				'badge': 'inline',
                'callback':
                    function(token){
                        view.model.set("captchaid",token);
                        view.book();
                    }
            });
            view.captchaWidgetId = widgetId;
		}

		return this;
    },
    
    renderPrice: function(){
		var total = this.model.get('total');
		this.$el.find('span.total').text( accounting.formatMoney(total) );
	},

    show: function(){
		this.render();
		$('#modal').modal();
	},
	
	showError: function(model, response, options){
		this.$el.find('#booking-error').text(response.responseJSON.msg).show();
	},	
	
	setPlayers: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('players', value);	
	},
	
	setCarts: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		
		var carts = false;
		if(value == 'yes'){
			carts = true;
		}
		
		this.model.set('carts', carts);		
	},
	
	setHoles: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('holes', value);	
	},	
	
	setPromoCode: function(callback){
		
		var field = this.$el.find('input[name="promo_code"]');
		var promo_code = field.val();
		var formgroup = field.parent('.form-group');
		
		var params = _.pick(this.model.attributes, ['time', 'holes', 'players', 
			'carts', 'schedule_id', 'course_id', 'booking_class_id']);
		params.promo_code = promo_code;
		
		var model = this.model;
		var view = this;
		var url = BASE_API_URL + 'courses/' + App.data.course.get('course_id') + '/promo_codes';
		
		// Check if the promo code is valid for this tee time
		$.get(url, params, function(response){
			
			// If valid, apply discount
			if(response.success){
				model.set({
					promo_code: response.promo_code, 
					promo_discount: response.promo_discount
				});
				
				formgroup.find('.glyphicon').remove();
				formgroup.append('<span class="glyphicon glyphicon-ok form-control-feedback" style="top: 22px;"></span>').removeClass('has-error').addClass('has-success');	
				
				if(typeof(callback) == 'function'){
					callback(true);
				}
			}
		
		// If the promo is not valid
		}).error(function(response){
			formgroup.find('.glyphicon').remove();
			formgroup.append('<span class="glyphicon glyphicon-remove form-control-feedback" style="top: 22px;"></span>').removeClass('has-success').addClass('has-error');	
		
			if(typeof(callback) == 'function'){
				callback(false);
			}		
		});
		
		return false;
	},
	
	validationError: function(model){
		// If user is not logged in yet, show log in window
		if(model.validationError == 'log_in_required'){
			App.vent.trigger('unauthenticated', this.model);
		}
	},
	
	// Start booking process
	completeBooking: function(){
        var view = this;
        // Confirm no bookings already exist and if not, create pending reservation
        var url = BASE_API_URL+'pending_reservation';
        var params = _.pick(this.model.attributes, ['time', 'holes', 'players',
            'carts', 'schedule_id', 'course_id', 'booking_class_id']);

        // Check if the promo code is valid for this tee time
        $.post(url, params, function(response){
            // Save tee time id in Local Storage
            params.reservation_id = response.reservation_id;
            pending_reservation_obj.store(params);
            // Start count down timer
            pending_reservation_obj.countdown_interval = window.setInterval(function () {pending_reservation_obj.display_count_down_timer(); }, 1000);

            // If valid, apply discount
            if(response.success){
                view.$el.find('button.book').button('loading');

                // If a promo code was entered, check it first
                if(view.$el.find('input[name="promo_code"]').length > 0 && view.$el.find('input[name="promo_code"]').val() != ''){
                    view.setPromoCode(function(promo_code_valid){

                        // If promo code was valid, continue
                        if(promo_code_valid){
                            view.$el.find('#booking-error').hide();
                            view.book();
                        }else{
                            view.$el.find('#booking-error').text('Promo code is invalid or does not apply').show();
                            view.$el.find('button.book').button('reset');
                        }
                    });

                }else{
                    view.book();
                }
            }

            // If the promo is not valid
        }).error(function(response){
            formgroup.find('.glyphicon').remove();
            formgroup.append('<span class="glyphicon glyphicon-remove form-control-feedback" style="top: 22px;"></span>').removeClass('has-success').addClass('has-error');

            if(typeof(callback) == 'function'){
                callback(false);
            }
        });
    },

	// Attempt to book tee time
	book: function(){
        var view = this;
		var model = this.model;

		// If user is not logged in, show log in window
        if(!App.data.user.get('logged_in')){
			var loginView = new LoginView({reservation: this.model});
			loginView.show();
			return true;
		};
		this.model.attributes.pending_reservation_id = pending_reservation_obj.get_id();
		// Make sure reservation is available for booking
        App.data.user.get('reservations').validate(this.model.attributes, function(success, message){

			if(!success){
				view.$el.find('#booking-error').text(message).show();
				view.$el.find('button.book').button('reset');
				return false;
			}

            // Book tee time
            if(!view.model.get("captchaid") && SETTINGS.enable_captcha_online){
                grecaptcha.execute(view.captchaWidgetId);
                return;
            }

			// If course allows entry of other players, show name entry window
			if(App.settings.allow_name_entry && model.get('players') > 1){
				var nameEntryView = new PlayerNameEntryView({model: model});
				nameEntryView.show();
				return true;
			}

			// If the selected tee time is available for purchase from ForeUp or from the course,
			// show payment method selection
			if((model.canPurchase() && model.get('total') > 0) || model.get('booking_fee_required')){
				var paymentMethod = new PaymentSelectionView({model: model});
				paymentMethod.show();
				return false;
			}

			// If a credit card is required to book the time, show credit card selection window
			if(model.get('require_credit_card') == 1 && !model.get('credit_card_id')){
				var creditCardView = new SelectCreditCardView({model: model});
				creditCardView.show();
				return false;
			}
            // Book tee time
			if(view.model.get("captchaid") || !SETTINGS.enable_captcha_online){
                view.createBooking(model,pending_reservation_obj,view);
            } else {
                grecaptcha.execute(view.captchaWidgetId);
            }
        });
	},
	createBooking: function(model,pending_reservation_obj,view){
        if(model && model.isValid()){
            var button = view.$el.find('button.book');
            App.data.user.get('reservations').create(model.attributes, {wait: true, error: function(){
                button.button('reset');
            }});
        }
        pending_reservation_obj.remove();
	}
});

var pending_reservation_obj = {
    countdown_interval : '',
    time_limit: 120, // Seconds
    store : function(reservation) {
        this.remove();
        var pending_reservation = {'reservation' : reservation, 'date_reserved' : new Date()};
        if(hasStorage){
            localStorage.setItem('pending_reservation', JSON.stringify(pending_reservation));
        }
    },

    get_id : function() {
        if(hasStorage){
            var saved_pending_reservation = localStorage.getItem('pending_reservation');
        }

        if (saved_pending_reservation != null) {
            var response = JSON.parse(saved_pending_reservation);

            if (response != null && typeof response.reservation != 'undefined') {
                return response.reservation.reservation_id;
            }
        }

        return false;
    },

    remove : function() {

        if(hasStorage){
            var saved_pending_reservation = localStorage.getItem('pending_reservation');
        }
        if (saved_pending_reservation != null) {
            // Send cancellation request for last pending reservation
            var response = JSON.parse(saved_pending_reservation);
            if (response !=  null && typeof response.reservation != 'undefined') {
                var url = BASE_API_URL+'pending_reservation/'+response.reservation.reservation_id;

                $.ajax({
                    url:url,
                    type: 'DELETE',
                    data: '',
                    success: function (response) {

                    },
                    error: function (response) {
                        if (typeof(callback) == 'function') {
                            callback(false);
                        }
                    }
                });
            }
        }

        if(hasStorage){
            localStorage.setItem('pending_reservation', null);
        }
        // End count down timer
        this.remove_count_down_timer();
    },

    remove_count_down_timer : function() {
        clearInterval(pending_reservation_obj.countdown_interval);
        this.remove_count_down_timer_html();
    },

    remove_count_down_timer_html : function() {
        $('body').find('#booking_countdown').remove();
    },

    display_count_down_timer : function() {
        return;
        var saved_pending_reservation = localStorage.getItem('pending_reservation');
        if (saved_pending_reservation != null && JSON.parse(saved_pending_reservation) != null) {
            var response = JSON.parse(saved_pending_reservation);
            var saved_date = new Date(response.date_reserved).getTime();
            var now = new Date().getTime();
            var elapsed_time = this.time_limit - Math.floor((now - saved_date) / 1000);
            this.remove_count_down_timer_html();

            if (elapsed_time < 1) {
                App.vent.trigger('notification', {msg: 'Time has run out to complete your previous booking. Please select a new time to continue booking.', type: 'error', delay:600000});
                this.remove();
                $('#modal').modal('hide');
                return;
            }
            var minutes = Math.floor( elapsed_time / 60 );
            var seconds = elapsed_time % 60;
            var reservation_time = moment(response.reservation.time).format('h:mma');
            var message = "Your time, "+reservation_time+", will be held for 2 minutes. You have "+minutes+" min and "+seconds+" sec remaining. Please complete the reservation process.";
            var booking_countdown = "<div id='booking_countdown' class='alert alert-danger' style='z-index:9999; position:fixed; top:20px; left:50%; margin:0px 0px 0px -411px;'>"+message+"</div>";
            $('body').append(booking_countdown);
        }
        else {
            // If there is no valid reservation, then clear the timer.
            this.remove_count_down_timer();
        }
    }
};
