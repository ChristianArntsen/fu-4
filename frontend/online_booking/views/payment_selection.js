var PaymentSelectionView = Backbone.View.extend({
	
	id: 'payment_selection',
	className: 'modal-dialog',
	template: _.template( $('#template_payment_selection').html() ),
	etsForm: _.template( $('#template_ets_credit_card_form').html() ),
	
	events: {
		'click button.continue': 'continueReservation',	
		'click li': 'selectMethod',
		'click button.ets-submit': 'etsSubmit'
	},
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
		App.data.last_reservation = this.model;
	},
	
    close: function() {
		this.remove();
    },
	
	selectMethod: function(e){
		$(e.currentTarget).find('label').addClass('selected');
		$(e.currentTarget).siblings('li').find('label').removeClass('selected');
	},
	
    render: function() {
		this.model.set('discount_percent', App.data.course.get('foreup_discount_percent'));
        this.model.calculateTotal();
		this.model.calculatePurchaseTotal();
				
		var attr = this.model.attributes;
		attr.foreup_discount = this.model.canGetForeUpDiscount();
		attr.booking_fee_terms = false;
		
		if(attr.booking_fee_required){
			var schedule = App.data.schedules.findWhere({selected: true});
			var booking_class = false;

			if(schedule && schedule.get('booking_classes') && App.data.filters.get('booking_class')){
				booking_class = schedule.get('booking_classes').get( App.data.filters.get('booking_class') );
			}
			if(booking_class){
				attr.booking_fee_terms = booking_class.get('booking_fee_terms');
			}
		}

		var html = this.template(attr);

        this.$el.find('#terms_and_conditions').html(TERMS_AND_CONDITIONS);
        this.$el.find('#terms_and_conditions a').attr('target', '_blank');

		this.$el.html(html);
		$('#modal').html(this.el);
		ga('send', 'event', 'Online Booking', 'View Payment Method Form', App.data.course.getCurrentSchedule());

		return this;
    },
    
    show: function(){
		this.render();
		$('#modal').modal();
	},    
    
    continueReservation: function(e){
		var selection = this.$el.find('input[name="payment_method"]:checked').val();
		
		if(selection == 'online-foreup'){
			this.loadPaymentWindow('purchase', 'foreup');
		}else if(selection == 'online'){
			this.loadPaymentWindow('purchase');
		}else if(selection == 'course' && this.model.get('booking_fee_required')){
			this.loadPaymentWindow('purchase-fee');		
		}else{
			this.payAtCourse();
		}
	},

	// Reserve the tee time as normal
	payAtCourse: function(){
		if(!this.model || !this.model.isValid()){
			return false;
		}
		this.$el.find('button.continue').button('loading');
		
		// If a credit card is required to book the time, show credit card selection window
		if(this.model.get('require_credit_card') == 1 && !this.model.get('credit_card_id')){
			var creditCardView = new SelectCreditCardView({model: this.model});
			creditCardView.show();
			return false;
		}		

		App.data.user.get('reservations').create(this.model.attributes, {wait: true});
	},

	etsSubmit: function(e){
		$(e.currentTarget).button('loading');
		ETSPayment.submitETSPayment();
		return false;
	},

	loadPaymentWindow: function(type, recipient){
		
		if(type == undefined){
			type = 'purchase';
		}
		if(recipient == undefined){
			recipient = false;
		}

		var amount = this.model.get('total');
		var booking_fee = this.model.get('booking_fee_price');
		if(this.model.get('booking_fee_per_person')){
			booking_fee = booking_fee * this.model.get('players');
		}

		if(type == 'purchase' && this.model.get('booking_fee_required')){
			amount = booking_fee + this.model.get('total');
		}else if(type == 'purchase-fee'){
			amount = booking_fee;
		}

		var course_id = this.model.get('course_id');
		var url = SITE_URL + 'credit_card_window/'+ course_id +'/'+ type;
		var data = {
			players: this.model.get('players'),
			available_spots: this.model.get('available_spots'),
			carts: this.model.get('carts'),
			time: this.model.get('time'),
			holes: this.model.get('holes'),
			schedule_id: this.model.get('schedule_id'),
			course_id: this.model.get('course_id'),
			special_id: this.model.get('special_id'),
			recipient: recipient,
			booking_class_id: this.model.get('booking_class_id'),
			promo_code: this.model.get('promo_code'),
			player_list: this.model.get('player_list'),
            pending_reservation_id: pending_reservation_obj.get_id()
		};
		var view = this;
		var merchant = App.data.course.get('credit_card_provider');
		
		if(recipient != 'foreup' && (merchant == 'ets' || merchant == 'element' || merchant == 'apriva')){
			var footer = view.$el.find('div.modal-footer');
			if(merchant == 'ets'){
				footer.find('button.ets-submit').show().siblings().hide();	
				view.$el.find('div.modal-body').html( this.etsForm({amount: amount}) );
			
			}else{
				view.$el.loadMask();
				footer.hide();
			}

			$.post(url, data, function(response){
				if(merchant == 'ets'){
					view.$el.find('div.modal-body').append(response);
				}else{
					view.$el.find('div.modal-body').html(response);
				}
			},'html');
		
		}else{
			var queryStr = $.param(data);
			var url = SITE_URL + 'credit_card_window/'+ course_id +'/'+ type +'?'+queryStr;

            view.$el.find('.modal-body').html('<div id="mercury_loader"><h2>Loading form...</h2>'+
				'<div class="progress progress-striped active">' +
				'<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>' +
					'</div></div><div id="foreign_currency_info"></div>'+

                	'<p class="bg-info" style="padding:10px">Do not refresh this page or press the back button while processing a payment.</p>'+
					'<iframe id="mercury_iframe" onLoad="$(\'#mercury_loader\').remove();" ' +
					'src="'+url+'" style="border: none; display: block; width: 100%; height: 540px; padding: 0px; margin: 0px;"></iframe><small>'+PAYMENT_WARNING+'</small><small id="terms_and_conditions">'+TERMS_AND_CONDITIONS+'</small>');

            view.$el.find('.modal-footer').hide();
            view.$el.find('#terms_and_conditions a').attr('target', '_blank');
        }
	}
});