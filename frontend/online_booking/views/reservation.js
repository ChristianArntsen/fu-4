var ReservationView = Backbone.View.extend({
	tagName: 'li',
	className: '',
	template: _.template( $('#template_reservation').html() ),

    initialize: function(){
	    this.listenTo(this.model, 'change', this.render);
    },

	events: {
		'click .cancel': 'cancelReservation',
        'click .update': 'updateReservationWindow'
	},
	
	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	},		
	
	// Cancels the user's reservation
	cancelReservation: function(){
		
		var has_booking_fee = (this.model.get('details').indexOf('PAID BOOKING FEE') >= 0);

		var msg = 'Are you sure you want to cancel this reservation?';
		if(has_booking_fee){
			msg = 'Cancelling this reservation will NOT refund the booking fee, continue?';
		}

		if(confirm(msg)){
			this.$el.find('button').button('loading');
			this.model.destroy({wait: true});
		}
	},

    updateReservationWindow: function(){
        this.model.set('holes', App.data.filters.get('holes'));
        if(App.data.filters.get('players') == 0){
            this.model.set('players', 1);
        } else {
            this.model.set('players', App.data.filters.get('players'));
        }

        // Show modal window with new time
        var editReservationView = new EditReservationView({model: this.model});
        editReservationView.show();
    }
});

var ReservationListView = Backbone.View.extend({
	tagName: 'ul',
	className: 'reservations',

	initialize: function(){
        this.listenTo(this.collection, 'add remove reset sync', this.render);
	},
	
	render: function(){
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('');
		
		if(this.collection.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderReservation, this);
		}

		return this;
	},
	
	renderEmpty: function(){
		this.$el.append('<li class="reservation empty muted"><a class="btn btn-primary" href="#/teetimes">Reserve a tee time now.</a></li>');
	},
	
	renderReservation: function(reservation){
		this.$el.append( new ReservationView({model: reservation}).render().el );
	}
});

var EditReservationView = Backbone.View.extend({
    id: 'book_time',
    className: 'modal-dialog',
    template: _.template( $('#template_update_reservation').html() ),

    events: {
        'click .players .btn': 'setPlayers',
        'click .update-reservation': 'updateReservation'
    },

    initialize: function(){

    },

    close: function() {
        this.remove();
    },

    render: function() {
        // this.model.set('players', this.model.get('available_spots'));
        var attributes = this.model.attributes;
        var tee_sheet = App.data.schedules.get(attributes.teesheet_id);
        if (parseInt(attributes.booking_class_id)) {
            var minimum_players = tee_sheet.get('booking_classes').get(attributes.booking_class_id).get('minimum_players');
        } else {
            var minimum_players = tee_sheet.get('minimum_players');
        }
        attributes.minimum_players = minimum_players;

        var view = this;
        $.ajax({
            type: "POST",
            url: "/index.php/booking/get_available_spots",
            data: {time: attributes.time, tee_sheet_id: attributes.teesheet_id, holes: tee_sheet.get('holes')},
            success: function (response) {

                attributes.available_spots = response.available_spots;
                var html = view.template(attributes);
                view.$el.html(html);
                $('#modal').html(view.el);

                return view;
            },
            dataType: 'json'
        });
    },

    show: function(){
        this.render();
        $('#modal').modal();
    },

    setPlayers: function(event){
        var btn = $(event.target);
        btn.addClass('active').siblings().removeClass('active');
        var value = btn.data('value');
    },

    updateReservation: function(){
        var view = this;
        var players = $('.players a.active').data('value');
        var params = {
            players: players,
            player_count: players,
            update : 1
        };

        this.model.set(params);
        this.model.save(params, {
                wait: true,
                success: function(response){
                    view.$el.find('.update-reservation').button('loading');
                    // Render the update
                    App.vent.trigger('notification', {msg: 'Your reservation has been updated', type: 'success'});
                    view.remove();
                    $('#modal').modal('hide');
                },
                error: function(response){
                    // Show error message
                    App.vent.trigger('notification', {
                        msg: 'We were unable to update your reservation at this time, please call the course',
                        type: 'error',
                        delay: 2000
                    });
                    view.remove();
                }
            }
        );
    }
});


