var PurchaseGiftcardView = Backbone.View.extend({
	tag: 'div',
	template: _.template( $('#template_online_giftcard').html() ),

	events: {
		"click input[name='amount']": "enable_custom_amount",
		"click input[name='send']": "show_recipient",
        "click button.buy": "buy"
	},

    render: function() {
		var html = this.template(this.model.attributes);
		this.$el.html(html);
		return this;
    },

    enable_custom_amount: function(e){
    	var input = $(e.currentTarget);
    	if(input.val() == 'custom'){
    		this.$el.find('input[name="custom_amount"]').prop('disabled', null).focus();
    	}else{
    		this.$el.find('input[name="custom_amount"]').val('').prop('disabled', true);
    	}
    },

    show_recipient: function(e){
    	var input = $(e.currentTarget);
    	if(input.is(':checked')){
    		this.$el.find('div.recipient-info').show().find('input,textarea').prop('disabled', null);
    	}else{
    		this.$el.find('div.recipient-info').hide().find('input,textarea').prop('disabled', true);
    	}
    },

    option_highlight: function(e){
    	var input = $(e.currentTarget);
    	var label = input.parent();
    	label.addClass('highlight');
    	label.siblings().removeClass('highlight');
    },

    buy: function(e){
        
        var amount = this.$('input[name="amount"]:checked').val();
        var custom_amount = this.$('input[name="custom_amount"]').val();
        var form = this.$el.find('form.giftcard');

        form.bootstrapValidator('validate');
        if(!form.data('bootstrapValidator').isValid()){
            return false;
        }

        if(amount == 'custom'){
            amount = custom_amount;
        }

        try {
            amount = new Decimal(amount);

        }catch(error){
            alert('Invalid amount');
            this.$('input[name="custom_amount"]').val('').focus();
            return false;           
        }

        var send = this.$('input[name="send"]').is(':checked');
        var recipient_name = this.$('input[name="recipient_name"]').val();
        var recipient_email = this.$('input[name="recipient_email"]').val();
        var recipient_message = this.$('textarea[name="recipient_message"]').val();

        if(send){
            recipient = {
                name: recipient_name,
                email: recipient_email,
                message: recipient_message
            };

        }else{
            recipient = false;
        }

        var data = {
            total: amount.toDP(2).toNumber(),
            recipient: 'course',
            type: 'purchase_giftcard',
            merchant: App.data.course.get('credit_card_provider'),
            course_id: App.data.course.get('course_id')
        };

        if(send){
            data.send = true;
            data.to = recipient;
        }

        var transaction_data = new Backbone.Model(data);
        
        var credit_card_window = new CreditCardPaymentView({model: transaction_data});
        credit_card_window.show();

        return false;
    }
});