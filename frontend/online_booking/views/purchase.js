var PurchaseView = Backbone.View.extend({
	tagName: 'tr',
	className: '',
	template: _.template( $('#template_purchase').html() ),
	
	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});

var PurchaseListView = Backbone.View.extend({
	tagName: 'table',
	className: 'table table-striped table-responsive purchases',
	
	initialize: function(){
		this.listenTo(this.collection, 'add remove reset', this.render);	
	},
	
	events: {
		'click button.prev': 'prevPage',
		'click button.next': 'nextPage'
	},

	render: function(){
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('<thead><tr><th class="sale">Transaction</th><th class="date">Date</th><th class="items">Items</th><th class="total">Amount</th><th class="total">Balance</th></tr></thead><tbody></tbody>');
		
		if(this.collection.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderPurchase, this);
		}
		this.$el.find('tbody').append('<tr><td colspan="5"> \
			<button class="btn btn-default prev disabled">&lt;</button> \
			<span class="label label-info page-number"><span class="current-page">1</span>/<span class="total-pages">1</span></span> \
			<button class="btn btn-default next disabled">&gt;</button></td></tr>');
		
		this.refreshPageButtons();

		return this;
	},

	nextPage: function(){
		this.collection.getNextPage();
		this.refreshPageButtons();
	},

	prevPage: function(){
		this.collection.getPreviousPage();
		this.refreshPageButtons();
	},
	
	refreshPageButtons: function(){
		if(this.collection.hasNextPage()){
			this.$el.find('button.next').removeClass('disabled');
		}
		if(this.collection.hasPreviousPage()){
			this.$el.find('button.prev').removeClass('disabled');
		}
		this.$el.find('span.current-page').text(this.collection.state.currentPage);
		this.$el.find('span.total-pages').text(this.collection.state.totalPages);
	},

	renderEmpty: function(){
		this.$el.find('tbody').append('<tr class="purchase empty muted"><td colspan="5">No purchases available</td></tr>');
	},
	
	renderPurchase: function(purchase){
		this.$el.append( new PurchaseView({model: purchase}).render().el );
	}
});
