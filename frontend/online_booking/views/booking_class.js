var BookingClassFilterView = Backbone.View.extend({
	tagName: 'div',
	className: 'col-md-12',
	
	events: {
		'click button.change': 'changeBookingClass'
	},
	
	initialize: function(){
		this.listenTo(App.data.filters, 'change:booking_class', this.bookingClassChanged);
	},
	
	bookingClassChanged: function(filters){		
		var schedule = App.data.schedules.get( filters.get('schedule_id') );

		if(filters.get('booking_class') == false){
			var booking_class = new BookingClass();
		}else{
			var booking_class = schedule.get('booking_classes').get( filters.get('booking_class') );
		}
		
		this.model = booking_class;
		this.render();
	},
	
	render: function() {
		var self = this;
		
		if(!this.model){
			this.$el.html('');
		}else{
			this.$el.html('Booking as <span class="label label-primary">' + this.model.get('name') + '</span> <button class="btn btn-sm btn-default change">Change</button>');			
		}

		return this;
	},
	
	changeBookingClass: function(){
		App.data.filters.set('booking_class', false, {silent: true});
		$.post(BASE_API_URL + '/booking_class', {booking_class_id: false}, function(response){
		}, 'json');
		App.router.teetimes();
	}
});

var BookingClassView = Backbone.View.extend({
	tagName: 'button',
	className: 'btn btn-primary col-md-4 col-xs-12 col-md-offset-4',
	
	events: {
		'click': 'setBookingClass'
	},
	
	render: function() {
		var self = this;
		this.$el.html(this.model.get('name'));
		return this;
	},
	
	setBookingClass: function(){
		var booking_class_id = this.model.get('booking_class_id');
		if(
			App.data.user &&
			App.data.user.has('booking_class_ids') &&
			this.model.get('online_booking_protected') == 1 && 
			App.data.user.get('booking_class_ids').indexOf(parseInt(booking_class_id)) < 0
		){
			alert('You do not have permission to access that booking class');
			return false;
		}

		App.data.filters.set('booking_class', booking_class_id);
		App.data.times.refresh();
		App.router.teetimes();
	}
});

var BookingClassListView = Backbone.View.extend({
	className: 'booking-classes',
	
	initialize: function(){
		this.listenTo(this.collection, "reset", this.render);
	},
	
	render: function() {
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('<h3 style="text-align: center;">'+
			(App.data.course.get('online_booking_welcome_message') ?
				App.data.course.get('online_booking_welcome_message') :
				'Select a booking class to continue') +
			'</h3>');
		_.each(this.collection.models, this.renderBookingClass, this);
		
		return this;
	},
	
	renderBookingClass: function(bookingClass){
		this.$el.append( new BookingClassView({model: bookingClass}).render().el );
	}
});
