var Course = Backbone.Model.extend({
	idAttribute: 'course_id',
	defaults: {
		name: '',
		address: ''
	},
	
	getCurrentSchedule: function(){
		var schedule = App.data.schedules.findWhere({selected: true});
		if(!schedule){
			return this.get('name');
		}
		return this.get('name') +':'+ schedule.get('title');
	}
});

var CourseCollection = Backbone.Collection.extend({
	model: Course
})
