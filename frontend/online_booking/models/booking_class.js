var BookingClass = Backbone.Model.extend({
	idAttribute: 'booking_class_id',
	defaults: {
		name: '',
		price_class: '',
		require_credit_card: 0,
		allow_name_entry: 0
	}
});

var BookingClassCollection = Backbone.Collection.extend({
	url: function(){
		return BASE_API_URL + 'courses/' + App.data.filters.get('course_id') + '/schedules/' + App.data.filters.get('schedule_id') + '/booking_classes';
	},
	model: BookingClass,
	
	allPasswordProtected: function(){
		
		if(this.models.length == 0){
			return false;
		}
		
		if(this.findWhere({'online_booking_protected': '0'})){
			allProtected = false;
		}else{
			allProtected = true;
		}
		
		return allProtected;
	}
});
