var Item = Backbone.Model.extend({
	
});

var MenuButton = Backbone.Model.extend({
	defaults: {
		'name': '',
		'category_id': null,
		'subcategory_id': null,
		'item_id': null,
		'order': 0
	}
});

var MenuButtonCollection = Backbone.Collection.extend({
	url: API_URL + '/items/menu_buttons',
	model: MenuButton,
	comparator: 'order'
});