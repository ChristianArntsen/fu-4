var RestaurantReservation = Backbone.Model.extend({
	
	idAttribute: "reservation_id",
	defaults: {
		"name": "",
		"guest_count": null,
		"comments": '',
		"date": null,
		"time": null,
		"date_created": null,
		"checked_in": 0,
		"customer_id": null,
		"employee_id": null,
		"employee_name": null
	},
	
	validate: function(attr){
		if(!attr.time || attr.time == null || attr.time == 'Invalid date'){
			return 'Invalid reservation time';
		}
	}
});

var RestaurantReservationCollection = Backbone.Collection.extend({
	url: FOOD_BEV_URL + '/reservations',
	model: RestaurantReservation,
	
	comparator: function(model){
		return moment(model.attributes.time, 'HH:mm:ss').unix();
	},

	getTotals: function(){
		var total = 0;
		var guests = 0;
		_.each(this.models, function(reservation){
			total++;
			var guest_count = parseInt(reservation.get('guest_count'));
			if(guest_count && guest_count != NaN){
				guests += guest_count;
			}
		});

		return {'total': total, 'guests': guests};		
	}
});
