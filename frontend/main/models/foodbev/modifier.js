var Modifier = Backbone.Model.extend({
	idAttribute: "modifier_id",
	defaults: {
		"modifier_id": null,
		"name": "",
		"selected_price": 0.00,
		"selected_option": "",
		"options": [],
		"price": 0.00,
		"item_id": null,
		"category_id": 0,
		"required": false,
		"default": "",
		"order": 0
	}
});

var ModifierCollection = Backbone.Collection.extend({
	
	comparator: 'order',
	model: function(attrs, options){
		return new Modifier(attrs, options);
	},

	getTotalPrice: function(){
		var total = new Decimal(0);
		if(this.models.length > 0){
			_.each(this.models, function(modifier){
				total = total.plus(modifier.get('selected_price'));
			});
		}
		return total.toDP(2).toNumber();
	},

	// Checks if all required modifiers are set
	isComplete: function(categoryId){
		var complete = true;

		_.each(this.models, function(modifier){
			if(categoryId && modifier.get('category_id') != categoryId){ return true; }
			if(modifier.get('required') && !modifier.get('selected_option')){
				complete = false;
			}
		});

		return complete;
	},
	
	// Returns modifiers that are set to something other than default
	nonDefault: function(){
		return this.filter(function(modifier){
			
			if(modifier.get('required') || String(modifier.get('default')) != String(modifier.get('selected_option'))){
				return true;
			}else{
				return false;
			}
		});
	}
});
