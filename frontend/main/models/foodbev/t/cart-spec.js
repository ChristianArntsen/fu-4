describe("The FbCartItem",function(){
    var cart = new FbCartItem();
    it("should be defined",function(){
        expect(FbCartItem).toBeDefined();
    });
    
    it("should create an instance of itself",function(){
        expect(cart).toBeDefined();
        expect(cart instanceof FbCartItem).toBe(true);
    });


    // there is additional magic happening in the init, copy, and set bits
    // perhaps should be broken up to make it easier to test

    describe("The set_discount method",function(){
        it("should be defined",function(){
            expect(cart.set_discount).toBe(defined);
            expect(typeof cart.set_discount).toBe('function');
        });
    });

    describe("The set_cost_plus_percent_discount method",function(){
        it("should be defined",function(){
            expect(cart.set_cost_plus_percent_discount).toBe(defined);
            expect(typeof cart.set_cost_plus_percent_discount).toBe('function');
        });
    });

    describe("The set_check_all",function(){
        it("should be defined",function(){
            expect(cart.set_check_all).toBe(defined);
            expect(typeof cart.set_check_all).toBe('function');
        });
    });

    describe("The displayError method",function(){
        it("should be defined",function(){
            expect(cart.displayError).toBe(defined);
            expect(typeof cart.displayError).toBe('function');
        });
    });

    describe("The calculatePrice method",function(){
        it("should be defined",function(){
            expect(cart.calculatePrice).toBe(defined);
            expect(typeof cart.calculatePrice).toBe('function');
        });
    });

    describe("The getSubtotal method",function(){
        it("should be defined",function(){
            expect(cart.getSubtotal).toBe(defined);
            expect(typeof cart.getSubtotal).toBe('function');
        });
    });

    describe("The getCompDiscount method",function(){
        it("should be defined",function(){
            expect(cart.getCompDiscount).toBe(defined);
            expect(typeof cart.getCompDiscount).toBe('function');
        });
    });

    describe("The getTax method",function(){
        it("should be defined",function(){
            expect(cart.getTax).toBe(defined);
            expect(typeof cart.getTax).toBe('function');
        });
    });

    describe("The getTotal method",function(){
        it("should be defined",function(){
            expect(cart.getTotal).toBe(defined);
            expect(typeof cart.getTotal).toBe('function');
        });
    });

    describe("The validate method",function(){
        it("should be defined",function(){
            expect(cart.validate).toBe(defined);
            expect(typeof cart.validate).toBe('function');
        });
    });

    describe("The isSoupsComplete method",function(){
        it("should be defined",function(){
            expect(cart.isSoupsComplete).toBe(defined);
            expect(typeof cart.isSoupsComplete).toBe('function');
        });
    });

    describe("The isSidesComplete method",function(){
        it("should be defined",function(){
            expect(cart.isSidesComplete).toBe(defined);
            expect(typeof cart.isSidesComplete).toBe('function');
        });
    });

    describe("The isSaladsComplete method",function(){
        it("should be defined",function(){
            expect(cart.isSaladsComplete).toBe(defined);
            expect(typeof cart.isSaladsComplete).toBe('function');
        });
    });

    describe("The isModifiersComplete method",function(){
        it("should be defined",function(){
            expect(cart.isModifiersComplete).toBe(defined);
            expect(typeof cart.isModifiersComplete).toBe('function');
        });
    });

    describe("The isComplete method",function(){
        it("should be defined",function(){
            expect(cart.isComplete).toBe(defined);
            expect(typeof cart.isComplete).toBe('function');
        });
    });

    describe("The destroy method",function(){
        it("should be defined",function(){
            expect(cart.destroy).toBe(defined);
            expect(typeof cart.destroy).toBe('function');
        });
    });
});

describe("The FbCart",function(){
    it("should be defined",function(){
        expect(FbCart).toBeDefined();
    });

    describe("The undo method",function(){
        it("should be defined");
    });

    describe("The clearNew method",function(){
        it("should be defined");
    });

    describe("The getNextSeat method",function(){
        it("should be defined");
    });

    describe("The getNextLine method",function(){
        it("should be defined");
    });

    describe("The getTotals method",function(){
        it("should be defined");
    });

    describe("The unSelectItems method",function(){
        it("should be defined");
    });

    describe("The selectAllItems method",function(){
        it("should be defined");
    });

    describe("The addItem method",function(){
        // this does view things, so will want to be refactored...
        it("should be defined");
    });
});