describe("FbReceipt",function(){
    it("should be defined",function(){
        expect(FbReceipt).toBeDefined();
    });

    it("should create an instance of itself");

    describe("The clearSelectedItems method",function(){
        it("should be defined");
    });

    describe("The hasCreditCardPayments method",function(){
        it("should be defined");
    });

    describe("The applyCustomerDiscount method",function(){
        it("should be defined");
    });

    describe("The calculateTotals method",function(){
        it("should be defined");
    });

    describe("The getAutoGratuityAmount method",function(){
        it("should be defined");
    });

    describe("The getServiceFee method",function(){
        it("should be defined");
    });

    describe("The mergeTaxes method",function(){
        it("should be defined");
    });

    describe("The getTotalDue method",function(){
        it("should be defined");
    });

    describe("The isPaid method",function(){
        it("should be defined");
    });

    describe("The markPaid method",function(){
        it("should be defined");
    });

    describe("The printReceipt method",function(){
        it("should be defined");
    });

    describe("The compAllItems method",function(){
        it("should be defined");
    });

    describe("The clearComps method",function(){
        it("should be defined");
    });
});

describe("The FbReceiptCart",function(){
    it("should be defined",function(){
        expect(FbReceiptCart).toBeDefined();
    });
});

describe("The FbReceiptCollection",function(){
    it("should be defined",function(){
        expect(FbReceiptCollection).toBeDefined();
    });

    describe("The getSelectedItems method",function(){
        it("should be defined");
    });

    describe("The unSelectItems method",function(){
        it("should be defined");
    });

    describe("The getNextNumber method",function(){
        it("should be defined");
    });

    describe("The getSplitItems method",function(){
        it("should be defined");
    });

    describe("The getCompTotals method",function(){
        it("should be defined");
    });

    describe("The allReceiptsPaid method",function(){
        it("should be defined");
    });

    describe("The paymentsMade method",function(){
        it("should be defined");
    });

    describe("The calculateAllTotals method",function(){
        it("should be defined");
    });
});