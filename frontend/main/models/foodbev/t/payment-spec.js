
describe("FbPayment",function(){
    it("should be defined",function(){
        expect(FbPayment).toBeDefined();
    });
    it("should create an instance of itself");

    describe("The error method",function(){
        it("should have an error method");
    });

    describe("The parse method",function(){
        it("should have a parse method");
    });

    describe("The validate method",function(){
        it("should have a validate method");
    });

    describe("The isCreditCard method",function(){
        it("should have a isCreditCard method");
    })
});

describe("FBPaymentCollection",function(){
    it("should be defined",function(){
        expect(FbPaymentCollection).toBeDefined();
    });
    it("should create an instance of itself");

    describe("The getTotal method",function(){
        it("should have a getTotal method");
    });

    describe("The addPayment method",function(){
        it("should have an addPayment method");
    });
});