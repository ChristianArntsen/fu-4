var FBSalePrintReceipt = Receipt.extend({

	initialize: function(attrs, options){
		Receipt.prototype.initialize.apply(this);
		this.builder = new StarWebPrintBuilder();
		this.auto_gratuity_included = false;

		if(options && options.auto_gratuity_included){
			this.auto_gratuity_included = options.auto_gratuity_included;
		}
	},
	
	get_modifiers: function(modifiers, quantity, splits, spacing){
		
		if(!splits || splits == undefined){
			splits = 1;
		}

		if(!modifiers || modifiers.length == 0){
			return null;
		}
		var data = '';
		var builder = this.builder;

		var space = '';
		if(spacing && spacing > 0){
			for(var i = 0; i < spacing; i++){
				space += ' ';
			}
		}

		if(modifiers instanceof ModifierCollection){
			modifiers = modifiers.toJSON();
		}

		_.each(modifiers, function(modifier){
			
			if(!modifier || !modifier.selected_option || modifier.selected_option == ''){
				return false;
			}

			var modifier_price = new Decimal(0);
			if(modifier.selected_price){
				modifier_price = new Decimal(modifier.selected_price).times(quantity);
			}
			if(splits > 1){
				modifier_price = modifier_price.dividedBy(splits);
			}
			modifier_price = modifier_price.toDP(2).toNumber();
			data += builder.createTextElement({width:1, data: add_white_space(space + '   - ' + modifier.name+ ': '+modifier.selected_option, accounting.formatMoney(modifier_price, ''))+'\n'});
		});		

		return data;
	},

	print_network: function(type, single_receipt){

		var add_white_space = this.add_white_space;
		var receipt_data = '';
		var customer = this.get('customer');
		var receipt = this;
		var table = App.data.food_bev_table;
		var has_credit_card_payments = false;
		var builder = this.builder;
		
		var identifier = 'POS '+this.get('sale_id');	
		if(this.get('sale_number') != null && this.get('sale_number')){
			identifier = 'POS '+this.get('sale_number');
		}else if(this.get('number') != null && this.get('number')){
			identifier = 'POS '+this.get('number');
		}

		if(!single_receipt){
			single_receipt = false;
		}else{
			single_receipt = true;
		}
		
		if(!type){
			type == 'pre-sale';
		}

		var receipt_id_text = '';
		if(this.get('receipt_id')){
			receipt_id_text = 'CHECK #'+ this.get('receipt_id')
		}

		var table_number = '';
		if(this.get('table_number')){
			table_number = this.get('table_number');
		
		}else if(table.get('table_num')){
			table_number = table.get('table_num');
		}

		var receipt_date = moment();
		if(this.get('sale_time')){
			receipt_date = moment(this.get('sale_time'));
		}

		// Course info header
		receipt_data += this.get('receipt_header');
		
		// Add sale information
		receipt_data += builder.createTextElement({width:1,data:'***********************************************\n'});
		receipt_data += builder.createTextElement({width:1,data:add_white_space(receipt_id_text, add_white_space('DATE', receipt_date.format('M/D/YY'), 13))+'\n'});
		receipt_data += builder.createTextElement({width:1,data:add_white_space('TABLE #'+ table_number, add_white_space('TIME', receipt_date.format('h:mma'), 13))+'\n'});
		receipt_data += builder.createTextElement({width:1,data:add_white_space('SERVER:', this.get('employee_name'))+'\n'});
		
		if(receipt.get('customer') && receipt.get('customer').get('person_id')){
			var customer_name = receipt.get('customer').get('last_name') +', '+ receipt.get('customer').get('first_name');
			receipt_data += builder.createTextElement({width:1,data:add_white_space('CUSTOMER:', customer_name)+'\n'});
            if (receipt.get('customer').get('account_number') != null && receipt.get('customer').get('account_number') != '')
            {
                receipt_data += builder.createTextElement({width:1,data:add_white_space('ACCOUNT #: ', receipt.get('customer').get('account_number'))+'\n'});
            }
        }
		receipt_data += builder.createTextElement({width:1,data:'***********************************************\n'});	
		
		// Begin item list
		receipt_data += builder.createTextElement({width:1,data:'\n' +add_white_space('ITEMS ORDERED', 'AMOUNT') + '\n\n'});	
		
		// Loop through items on receipt
		var comp = false;
		var comp_amount = 0;

		_.each(this.get('items').models, function(item, index){
			if(item.get('do_not_print_customer_receipt') == 1){
				return false;
			}
			if(!item.has('split_subtotal_no_discount')){
				item.attributes.split_subtotal_no_discount = item.get('non_discount_subtotal');
			}
			if(!item.has('split_comp_total')){
				item.attributes.split_comp_total = item.get('comp_total');
			}
			if(!item.has('split_modifier_total')){
				item.attributes.split_modifier_total = 0;
			}

			var split = '';
			var split_modifier_total = item.get('split_modifier_total');
			if(split_modifier_total==0 && item.get('modifier_total')>0){
				split_modifier_total = new Decimal(item.get('modifier_total')).toDP(2).toNumber();// Alresdy has splits accounted for//.dividedBy(item.get('number_splits')).toDP(2).toNumber();
			}

			var subtotal = item.get('split_subtotal_no_discount') - split_modifier_total;
			var discount_amount = item.get('split_discount_amount');

			if(item.get('number_splits') > 1 && (item.get('is_side') == 0 || !item.get('is_side'))){
				split = '1/' + item.get('number_splits')+ ' ';
			}
			
			var qtyLine = '('+ accounting.formatNumber(item.get('quantity')) +') ';
			if(item.get('is_side') == 1){
				qtyLine = '    ';
			}

			receipt_data += builder.createTextElement({
				width: 1, data: add_white_space(
					qtyLine + split + item.get('name') +' ', 
					accounting.formatMoney(subtotal, '')
				) +'\n'
			});
			
			if(item.get('modifiers') && item.get('modifiers').length > 0){
				receipt_data += receipt.get_modifiers(item.get('modifiers'), item.get('quantity'), item.get('number_splits'), item.get('is_side'));
			}			

			var side_types = ['sides', 'soups', 'salads'];
			// Loop through sides, soups, and salads (if any of each)
			_.each(side_types, function(side_type){
			
				if(item.get(side_type)){
					var process_me = item.get(side_type);
					if(item.get(side_type).models){
						process_me = item.get(side_type).models;
					}
					_.each(process_me, function(side){
						
						if(side.get('split_discount_amount')){
							discount_amount += side.get('split_discount_amount');
						}	
						var side_subtotal = side.get('split_subtotal_no_discount') - side.get('split_modifier_total');
						receipt_data += builder.createTextElement({width:1,data: add_white_space('    ' +side.get('name')+ ' ', accounting.formatMoney(side_subtotal, ''))+'\n'});
						
						if(side.get('modifiers') && side.get('modifiers').length > 0){
							receipt_data += receipt.get_modifiers(side.get('modifiers'), side.get('quantity'), side.get('number_splits'), 1);
						}
					});
				}
			});

			if(item.get('service_fees')){
				_.each(item.get('service_fees').models, function(service_fee){
					receipt_data += builder.createTextElement({width:1,data: add_white_space('    ' +service_fee.get('name')+ ' ', accounting.formatMoney(service_fee.get('subtotal'), ''))+'\n'});					
				});
			}

			// If item has a discount, display it underneath the item name
			if(item.get('discount') && item.get('discount') > 0){
				receipt_data += builder.createTextElement({width:1,data: add_white_space('    '+ accounting.formatNumber(item.get('discount'), 2) + '% Discount', '-' + accounting.formatMoney(discount_amount, '')) +'\n'});			
			}
			
			if(item.get('split_comp_total') && item.get('split_comp_total') > 0){
				comp = item.get('comp');
				comp_amount = item.get('split_comp_total');
			}

			if(comp && (!receipt.get('items').at(index + 1) || receipt.get('items').at(index + 1).get('is_side') == 0)){
				receipt_data += builder.createTextElement({data: add_white_space('    Comp: ' + comp.description, '-' + accounting.formatMoney(comp_amount, '')) +'\n'});
				comp = false;
				comp_amount = 0;
			}
		});
		
		// Receipt totals
		receipt_data += builder.createTextElement({data:'\n-----------------------------------------------\n'});
		receipt_data += builder.createTextElement({width: 2, data:'\n' + add_white_space('Subtotal:', accounting.formatMoney(receipt.get('subtotal')), 23) });
		
		if(receipt.get('service_fee') && receipt.get('auto_gratuity') && receipt.get('auto_gratuity') * 1 > 0 &&accounting.formatMoney(receipt.get('service_fee').get('subtotal') > 0)){
			receipt_data += builder.createTextElement({width:2, data: '\n' + add_white_space(accounting.formatNumber(receipt.get('auto_gratuity'), 2) + '% Gratuity:', accounting.formatMoney(receipt.get('service_fee').get('subtotal')), 23) });
		}		

		has_auto_gratuity = false;
		if(receipt.get('auto_gratuity') && receipt.get('auto_gratuity') * 1 > 0 && receipt.get('auto_gratuity_amount') > 0){
			has_auto_gratuity = true;
			receipt_data += builder.createTextElement({width:2, data: '\n' + add_white_space(accounting.formatNumber(receipt.get('auto_gratuity'), 2) + '% Gratuity:', accounting.formatMoney(receipt.get('auto_gratuity_amount')), 23) });
		}

		if(receipt.get('taxes')){		
			_.each(receipt.get('taxes'), function(tax){
				console.debug(tax);
				receipt_data += builder.createTextElement({width:2, data:'\n' + add_white_space(accounting.formatNumber(tax.percent, 3) +'% '+ tax.name, accounting.formatMoney(tax.amount), 23) });
			});
		}else{
			receipt_data += builder.createTextElement({width: 2, data:'\n' + add_white_space('Tax:', accounting.formatMoney(0), 23) });
		}

		var total = receipt.get('total');
		if(!this.auto_gratuity_included){
			total = receipt.get('total') + receipt.get('auto_gratuity_amount');
		}
		receipt_data += builder.createTextElement({width: 2, data:'\n' + add_white_space('Total:', accounting.formatMoney(total), 23) });
		receipt_data += builder.createTextElement({width: 1, data:'\n'});
		
		var show_tip_line = false;
		var show_member_balance = false;
		var show_customer_balance = false;

		// Payments list
		if(type == 'sale'){
			receipt_data += builder.createTextElement({width:1, data:'\nPayments:\n'});
			_.each(this.get('payments').models, function(payment, index){
				if(payment.get('is_auto_gratuity') && payment.get('is_auto_gratuity') == 1){
					return false;
				}

				var description = payment.get('type');
				if(payment.has('description')){
					description = payment.get('description');
				}

				if(
					description.indexOf('Credit Card') > -1 ||
					description.indexOf('DCVR') > -1 ||
					description.indexOf('M/C') > -1 ||
					description.indexOf('M-C') > -1 ||
					description.indexOf('AMEX') > -1 ||
					description.indexOf('JCB') > -1 ||
					description.indexOf('DINERS') > -1 ||
					description.indexOf('VISA') > -1
				){
					has_credit_card_payments = true;
				}
				if(has_credit_card_payments ||
					description.indexOf(App.data.course.get('member_balance_nickname')) >= 0 ||
					description.indexOf('Member Balance') >= 0 ||
					description.indexOf(App.data.course.get('customer_credit_nickname')) >= 0 ||
					description.indexOf('Customer Account') >= 0 ||
					(description.indexOf('Gift') >= 0 && App.data.course.get('use_ets_giftcards') == 0)
				){	
					show_tip_line = true;
				}

				if(description.indexOf(App.data.course.get('member_balance_nickname')) >= 0 ||
					description.indexOf('Member Balance') >= 0){
					show_member_balance = true;
				}
				if(description.indexOf(App.data.course.get('customer_credit_nickname')) >= 0 ||
					description.indexOf('Customer Account') >= 0){
					show_customer_balance = true;
				}
				
				var payment_amount = payment.get('amount');
				if(!payment_amount && payment.get('payment_amount')){
					payment_amount = payment.get('payment_amount');
				}
				if(!receipt.auto_gratuity_included && index == 0){
					payment_amount += receipt.get('auto_gratuity_amount');
				}

				receipt_data += builder.createTextElement({width:1,data: add_white_space(description + ': ', accounting.formatMoney(payment_amount)) + '\n'});
			});
		}
		
		if(
			(type == 'pre-sale' && App.data.course.get('food_bev_tip_line_first_receipt') == 1) ||
			(type == 'sale' && show_tip_line && App.data.course.get('print_tip_line') == '1')
		){
			// Add tip line
			receipt_data += builder.createTextElement({width: 1, data: '\n\n' + add_white_space('Tip:',  SETTINGS.currency_symbol+'________.____') + '\n\n'});
			receipt_data += builder.createTextElement({width: 1, data: add_white_space('TOTAL CHARGE:', SETTINGS.currency_symbol+'________.____') });
		
			// Add signature line
			receipt_data += builder.createTextElement({width: 1, data: '\n\n\nX_____________________________________________\n'});
		}


        var payments = this.get('payments');
        var gift_cards = payments.filter(function(gift_card){
			return gift_card.get("type").includes("Gift Card");
		});
		// If member payment was used, show remaining balance
		if (type == 'sale' && 
			App.data.course.get_setting('receipt_print_account_balance') == 1 && 
			show_member_balance && 
			customer
		){
			receipt_data += builder.createTextElement({
				data: '\n' + App.data.course.get('member_balance_nickname') + " ending balance: " +accounting.formatMoney(customer.get('member_account_balance')) + "\n"	
			});
		}

		// If customer account payment was used, show remaining balance
		if (type == 'sale' && 
			App.data.course.get_setting('receipt_print_account_balance') == 1 && 
			show_customer_balance && 
			customer
		){
			receipt_data += builder.createTextElement({
				data: '\n' + App.data.course.get('customer_credit_nickname') + " ending balance: " +accounting.formatMoney(customer.get('account_balance')) + "\n"		
			});
		}


        if(gift_cards.length > 0 ){
            receipt_data += builder.createTextElement({
                data: "\n\n"
            });
        }

        if (gift_cards.length > 0){
            _.each(gift_cards, function(gift_card){
                if(!gift_card.get('gift_card')){
                    return true;
                }
                //Commented out for SUP-666
				//It seems this was copied from POS, f&b however works differently in that it will actually get payment informatino before closing the sale.
               // var ending_balance = _.round(gift_card.get('gift_card').value - gift_card.get('amount'));
				var ending_balance = gift_card.get("gift_card").value;
                receipt_data += builder.createTextElement({
                    data: gift_card.get('type')+ " ending balance " +accounting.formatMoney(ending_balance) + "\n"
                });
            });
        }


		if(
			App.data.course.get('use_loyalty') == '1' &&
			customer && 
			this.get('loyalty') &&
			(this.get('loyalty').earned > 0 || this.get('loyalty').spent > 0)
		){
			receipt_data += builder.createAlignmentElement({position:'left'});
			receipt_data += builder.createTextElement({
				data: "\n\nLoyalty Points\n------------------\n"
			});			
			
			if(this.get('loyalty').earned > 0){
				receipt_data += builder.createTextElement({
					data: "Earned: " + this.get('loyalty').earned + ' points\n'
				});				
			}
			
			if(this.get('loyalty').spent > 0){
				receipt_data += builder.createTextElement({
					data: "Spent: " + this.get('loyalty').spent + ' points\n'
				});				
			}

			receipt_data += builder.createTextElement({
				data: "Current Balance: " + this.get('loyalty').balance + ' points\n'
			});
		}	

		if(type == 'sale' && App.data.course.get_setting('print_suggested_tip') == 1 && !has_auto_gratuity){
			receipt_data += builder.createAlignmentElement({position:'left'});
			receipt_data += builder.createTextElement({
				data: '\n\nSuggested tip amounts\n'
			});
			
			var percentages = [15, 18, 20];
			_.each(percentages, function(percent){
				var tipAmount = (percent / 100) * receipt.get('subtotal');
				receipt_data += builder.createTextElement({
					data: percent +'% - ' + accounting.formatMoney(tipAmount) + '\n'
				});
			});
		}

		if(this.get('customer_note')){
			receipt_data += builder.createTextElement({
				data: '\n\nCUSTOMER NOTE\n' + this.get('customer_note') + '\n'
			});			
		}

		if(App.data.course.get_setting('return_policy') != ''){
			receipt_data += builder.createAlignmentElement({position:'center'});
			receipt_data += builder.createTextElement({
				data: '\n\n' + App.data.course.get_setting('return_policy')
			});
		}
			
		// Add sale ID barcode
		if(type == 'sale'){
			receipt_data += builder.createTextElement({width: 1, data:"\n"});
			receipt_data += builder.createAlignmentElement({position:'center'});
			receipt_data += builder.createTextElement({data:"\n\n"});
			receipt_data += builder.createBarcodeElement({symbology:'Code128', width: 'width2', height:40, hri:false, data: identifier});
			receipt_data += builder.createTextElement({width:1,data:'\nSale ID: '+ identifier + '\n'});					
		}
		
		// Extra spacing at bottom
		receipt_data += builder.createTextElement({
			data: "\n\n\n"
		});
		
		// Cut receipt
		receipt_data += builder.createCutPaperElement({feed: true});
		
		// Double the receipt data if course is set to print two
		if( 
			type == 'sale' && !single_receipt && (
				(!has_credit_card_payments && App.data.course.get_setting('non_credit_card_receipt_count') > 1) ||
				(has_credit_card_payments && App.data.course.get_setting('credit_card_receipt_count') > 1)
			)
		){
			receipt_data += receipt_data;
		}

		// Send receipt to printer
		App.data.print_queue.add_receipt(App.data.course.get_setting('receipt_ip'), receipt_data);
		App.data.print_queue.print();					
	}
});

var FBOrderPrintReceipt = Receipt.extend({
	
	initialize: function(){
		Receipt.prototype.initialize.apply(this);
	},
	
	get_item_printers: function(item){
		
		var printer_ip = item.get('printer_ip');
		
		if(item.get('printers')){
			return item.get('printers');
		
		}else if(printer_ip && printer_ip != ''){
			var printers = [];
			printer_ip = printer_ip.replace('_hot_', '');
			printer_ip = printer_ip.replace('_cold_', '');
			printers.push(printer_ip);
			return printers;
		}
		
		return false;
	},
	
	print_network: function(type){
		
		this.header = '';
		this.body = [];
		this.footer = '';		
		var builder = new StarWebPrintBuilder();
		var ticket_data = {};
		var header_data = '';
		var order = this;
		var get_item_printers = this.get_item_printers;
		var customers = this.get('customers');
		var table_number = this.get('table_number');
		var guest_count = this.get('guest_count');
		var table_name = this.get('table_name');
		
		var is_fired = false;
		var include_fired_course_items = false;
		
		if(this.get('meal_course_id') && this.get('meal_course_id') != 0 && this.get('meal_course_id') != 'NaN'){
			is_fired = true;
		}
		if(App.data.course.get('course_firing_include_items') == 1){
			include_fired_course_items = true
		}

        if(App.data.course.get('use_kitchen_buzzers') == 1){
            // Add buzzer signal (also cash drawer signal)
            header_data += builder.createRawDataElement({data:String.fromCharCode(7)});
        }

        // Add Header... date, time, table, employee, ticket #...
        header_data += builder.createAlignmentElement({position: 'left'});
		if (is_fired){
			header_data += builder.createTextElement({width:2, height: 2, invert: true, data:'******* FIRE ********' + '\n\n'})
		}
		header_data += builder.createTextElement({width:2, height: 1, emphasis: false, invert: false, data:'Prep Ticket #' + this.get('ticket_id') + '\n'});
		
		if(table_number){
			header_data += builder.createTextElement({data:'TABLE #' + table_number + '\n'});
		}
		if(guest_count && guest_count != 0){
			header_data += builder.createTextElement({data:'GUESTS: ' + guest_count + '\n'});
		}
		header_data += builder.createTextElement({width:1, height:1, data:'\n******************************************\n'});	
		
		// Get list of valid meal courses
		var meal_courses = App.data.course.get('meal_courses').toJSON();
		meal_courses = new MealCourseCollection(meal_courses);
		meal_courses.add({'order': '0', 'name': '', 'meal_course_id': '0'});		
		
		var items = new Backbone.Collection( this.get('items') );
		
		// Loop through ticket items, if an item is attached to an old meal course
		// that no longer exists, reset the meal course ID to 0
		_.each(items.models, function(item){
			if(!item.get('meal_course_id') || item.get('meal_course_id') == 0){
				return false;
			}
			
			if(!meal_courses.get(item.get('meal_course_id'))){
				item.set('meal_course_id', '0', {'silent': true});
			}
		});

		// Ticket ID is from server response, it is unique primary key
		// of ticket from database
		var date_time = moment();

		header_data += builder.createTextElement({width:1, height: 1, data:'SERVER: ' + this.get('employee_name') + '\n'});
		
		// If there are any customers attached to table, list them on receipt
		if(customers && customers.length > 0){
			
			var customer_list = '';
			_.each(customers.models, function(customer, i){
				if(i > 0){
					customer_list += '          ';
				}
                customer_list += customer.get('first_name') +' '+ customer.get('last_name') + '\n';
			});
			
			// Trim off last new line character
			customer_list = customer_list.slice(0, -1);
			
			header_data += builder.createTextElement({width:1, height: 1, data:'CUSTOMER: ' + customer_list + '\n'});
		
		// If the table name is filled out, display that as customer
		}else if(table_name && table_name != '' && table_name != null){
			header_data += builder.createTextElement({width:1, height: 1, data:'CUSTOMER: ' + table_name + '\n'});
		}		
		
		header_data += builder.createTextElement({width:1, height: 1, data:'DATE: ' + date_time.format('M/D/YY H:mm') + '\n'});
		header_data += builder.createTextElement({data:'******************************************\n\n'});

		if (this.get('message')){
			header_data += builder.createTextElement({width:2, data:'***** ORDER EDIT ****' + '\n\n'})
			header_data += builder.createTextElement({width:1, emphasis: true, data:'     ' + this.get('message') + '\n\n'})
		}
		header_data += builder.createTextElement({width:2, height: 1, invert: false, emphasis: false, data:'ITEMS' + '\n\n'});
		
		// Loop through meal courses
		_.each(meal_courses.models, function(meal_course){

			// Find items tied to this section, if no items are found, skip the section
			var meal_course_items = items.where({'meal_course_id': meal_course.get('meal_course_id') });
			if(meal_course_items == undefined || meal_course_items.length == 0){
				return false;
			}
			
			// Turn array of course items into a collection
			meal_course_items = new Backbone.Collection(meal_course_items);
			
			if(App.data.course.get('food_bev_sort_by_seat') == 1){
				meal_course_items.comparator = 'seat';
				meal_course_items.sort();	
			}

			var course_separator = '';
			if(meal_course.get('name') != ''){
				course_separator += builder.createTextElement({width:1, height: 1, invert: false, emphasis: true, data: '******************************************\n'});
				course_separator += builder.createTextElement({width:2, height: 2, invert: true, emphasis: true, data: meal_course.get('name') + '\n'});
				course_separator += builder.createTextElement({width:1, height: 1, invert: false, emphasis: true, data: '******************************************\n'});			
			}else{
				course_separator += builder.createTextElement({width:1, height: 1, invert: false, emphasis: true, data: '******************************************\n'});	
			}
			var course_separator_added = false;

			_.each(meal_course_items.models, function(item){
				
				// Add meal course separator to receipt
				if(!course_separator_added && course_separator != ''){
					order.build_receipt_body(
						course_separator,
						get_item_printers(item)
					);	
					course_separator_added = true;
				}
				
				// If golf course does not want to include items in each 
				// meal course section when a meal course is 'fired'
				if(is_fired && !include_fired_course_items){
					return false;
				}				
				
				// Skip printing this item
				var item_receipt_text = '';
				if(item.has('do_not_print') && item.get('do_not_print') == 1){
					return false;
				}
				
				var itemQty = '';
				if(item.get('do_not_print_customer_receipt') == 0){
					item_receipt_text += builder.createTextElement({width:1, height: 1, invert: false, emphasis: true, data:'Seat ' + item.get('seat') + '\n'});	
					itemQty = '(' +parseInt(item.get('quantity')) + ') ';
				}
				
				item_receipt_text += builder.createTextElement({width:2, height: 2, invert: false, emphasis: true, data: itemQty + item.get('name')+'\n'});
				item_receipt_text += builder.createTextElement({width:1, height: 1, emphasis: false, data: ''});

				// Loop through item modifiers
				if(item.get('modifiers').nonDefault().length > 0){
					
					_.each(item.get('modifiers').nonDefault(), function(modifier){
						var selected_option = modifier.get('selected_option')
						if(selected_option == ''){
							selected_option = 'n/a';
						}
						var modifier_name = '';
						if(App.data.course.get('hide_modifier_names_kitchen_receipts') == 0){
							modifier_name = '*'+modifier.get('name')+': ';
						}
						item_receipt_text += builder.createTextElement({width:2, height:1, emphasis:false, invert: true, data:modifier_name+ selected_option +'\n'});
					});
				}
				
				// Add item data to ticket
				order.build_receipt_body(
					item_receipt_text,
					get_item_printers(item)
				);
							
				// Loop through sides, soups, and salads (if any of each)
				// If side item_id is set to 0, it means the guest did not
				// want a side and it can be ignored
				var side_types = ['sides', 'soups', 'salads'];
				
				_.each(side_types, function(side_type){
					if(!item.has(side_type) || !item.get(side_type) || item.get(side_type).length == 0){
						return false
					}
						
					_.each(item.get(side_type).models, function(side){
						
						var side_receipt_text = '';
						
						// Skip printing this item
						if(side.has('do_not_print') && side.get('do_not_print') == 1){
							return false;
						}
						
						side_receipt_text += builder.createTextElement({width:2, height:2, invert: false, emphasis:false, data:'-'+side.get('name')+'\n'});
						
						if(side.get('modifiers').nonDefault().length > 0){
							_.each(side.get('modifiers').nonDefault(), function(modifier){
								var selected_option = modifier.get('selected_option')
								if(selected_option == ''){
									selected_option = 'n/a';
								}
								var modifier_name = '';
								if(App.data.course.get('hide_modifier_names_kitchen_receipts') == 0){
									modifier_name = '*'+modifier.get('name')+': ';
								}
								side_receipt_text += builder.createTextElement({width:2, height:1, emphasis:false, invert: false, data:' '});
								side_receipt_text += builder.createTextElement({width:2, height:1, emphasis:false, invert: true, data:modifier_name+ selected_option +'\n'});
							});	
						}

						// Add item side to ticket
						order.build_receipt_body(
							side_receipt_text,
							get_item_printers(item)
						);									
					});
				});
				
				if(item.get('comments')){
					order.build_receipt_body(
						builder.createTextElement({width:2, height:1,  emphasis:false, invert: false,  data:'COMMENTS: '+item.get('comments') + '\n'}),
                        get_item_printers(item)
					);
				}			
			});
		});
		
		var footer_data = builder.createTextElement({data: '\n\n\n'});
		footer_data += builder.createCutPaperElement({feed: true});
		
		// Loop through raw receipt data to be sent to each web print IP address
		for(var ip in this.body){
			if(ip == null || ip == 0 || ip == undefined){
				return true;
			}
			
			var receipt_body = order.body[ip];
			var receipt_data = header_data + receipt_body + footer_data;
			
			App.data.print_queue.add_receipt(ip, receipt_data);
		}
		
		App.data.print_queue.print();
		return false;			
	},
	
	build_receipt_body: function(data, printers){
		
		var body_data = this.body;
		
		_.each(printers, function(ip_address){
			if(!ip_address || ip_address == ''){
				return false;
			}
			if(!body_data[ip_address] || body_data[ip_address]== undefined){
				body_data[ip_address] = '';
			}
			body_data[ip_address] += data;
		});
		
		this.body = body_data;
	}
});
