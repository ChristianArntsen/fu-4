var FbTable = Backbone.Model.extend({
	
	idAttribute: 'table_num',
	url: '',
	
	defaults: {
		"table_num": null,
		"name": null,
		"sale_time": null,
		"employee_id": null,
		"employee_level": null,
		"date_created": null,
		"cart": null,
		"order": null,
		"receipts": null,
		"customers": null,
		"orders_made": 0,
		"admin_auth_id": false,
		"last_layout": false,
		"guest_count": null,
		"fired_meal_courses": null,
		"loyalty": false
	},
	
	initialize: function(){
		var url = this.generateUrl();
		var cart = new FbCart(this.get('cart'), {url: url + 'cart/', comparator: false, table:this});
		var order = new Order(this.get('order'), {url: url + 'order/', table:this});
		var customers = new CustomerCollection(this.get('customers'), {url: url + 'customers/', table:this});
		var receipts = new FbReceiptCollection(this.get('receipts'), {url: url + 'receipts/', table:this});
		var meal_courses = new MealCourseCollection(this.get('fired_meal_courses'), {url: url + 'meal_courses/', table:this});

		this.set({
			'cart': cart, 
			'receipts': receipts, 
			'customers': customers,
			'fired_meal_courses': meal_courses
		});
		this.on('change:table_num', this.generateUrl);
		this.on('change:name', this.updateLayoutObject);
		this.on('change:guest_count', this.updateAutoGratuity);
		this.on('error', this.showError);
		
		//this.listenTo(customers, 'add', this.applyCustomerDiscount);
		//this.listenTo(customers, 'add', this.addCustomerToReceipt);
		this.listenTo(customers, 'add', this.renderCustomers);
		this.listenTo(customers, 'remove', this.renderCustomers);
		// TODO: Replace calls to global App.*
		this.listenTo(App.data.user, 'change:fnb_logged_in', this.resetTable);
	},
	
	save: function(attrs, options) {
		options || (options = {});
		attrs || (attrs = _.clone(this.attributes));

		// Filter the data to send to the server
		attrs = _.pick(attrs, ['admin_auth_id', 'guest_count', 'last_layout', 'employee_id', 'name']);
		options.attrs = attrs;

		// Proxy the call to the original save function
		return Backbone.Model.prototype.save.call(this, attrs, options);
	},

	parse: function(response){
		var url = this.url;
		
		if(response.cart){
			var cart = this.get('cart');
			cart.url = url + 'cart/';
			cart.reset(response.cart);
			response.cart = cart;
		}
		
		if(response.receipts){
			var receipts = this.get('receipts');
			receipts.url = url + 'receipts/';
			receipts.reset(response.receipts);
			response.receipts = receipts;
		}
		
		if(response.customers){
			var customers = this.get('customers');
			customers.url = url + 'customers/';
			customers.reset(response.customers);			
			response.customers = customers;
		}
		
		if(response.fired_meal_courses){
			var fired_meal_courses = this.get('fired_meal_courses');
			fired_meal_courses.url = url + 'fired_meal_courses/';
			fired_meal_courses.reset(response.fired_meal_courses);			
			response.fired_meal_courses = fired_meal_courses;
		}
		
		return response;
	},

	updateAutoGratuity: function(){

		var headCount = this.get('guest_count');
		// TODO: Replace calls to global App.*
		var threshold = App.data.course.get('auto_gratuity_threshold');
		var auto_gratuity = App.data.course.get('auto_gratuity');
		var receipts = this.get('receipts');

		if(!auto_gratuity || auto_gratuity == 0){
			return false;
		}

		if(receipts && receipts.length > 0 && headCount && threshold /*&& headCount >= threshold*/){
			_.each(receipts.models, function(receipt){
				if(receipt.get('auto_gratuity') == 0 && receipt.get('auto_gratuity_type') == 'percentage' && headCount*1 >= threshold*1){
					receipt.set({
						'auto_gratuity': auto_gratuity
					});
					receipt.save();
				}else if(receipt.get('auto_gratuity') == auto_gratuity && headCount*1 < threshold*1){
					receipt.set({
						'auto_gratuity': 0
					});
					receipt.save();
				}
			});
			return true;
		}
		return null;
	},

	open: function(table_num, callback){
		var table = this;
		if(!table_num || typeof(table_num) == undefined){
			return false;
		}

		this.set({'table_num':table_num}).fetch({success: function(model,resp,options){
			table.renderCustomers();
			if(typeof(callback) == 'function'){
				callback(this);
			}
		},error:function(model,resp,options){
			console.log('fetch error in open!');
			console.log('model: ',model);
			console.log('resp: ',resp);
			console.log('options: ',options);
		}});
		return true;
	},

	merge: function(destinationTable, callback){
		
		var url = this.url + 'merge';
		var model = this;

		$.post(url, {
			destination: destinationTable
		
		}).done(function(response){
			
			if(typeof(callback) == 'function'){
				callback();
			}
			model.open(destinationTable);
		
		}).fail(function(response){
			
			model.trigger('error', model, response);
		});
		
		return true;
	},

	loadRecentTransactions: function(){
		// TODO: Replace calls to global App.*

		var params = {
			food_and_beverage: 1, 
			timeago: '24h',
			limit: App.data.food_bev_recent_transactions.limit
		};

		var terminal = App.data.course.get('terminals').getActiveTerminal();
		if(terminal){
			params.terminal_id = terminal.get('terminal_id');
		}

		// Load latest recent transactions	
		App.data.food_bev_recent_transactions.fetch({ reset: true, data: params });
		App.data.food_bev_recent_transactions.offset = false;
	},

	addCustomer: function(customer){
		// If this is the first customer being attached to table
		// create a new receipt or retrieve an open one to attach
		// the customer to as well
		availableReceipt = this.get('receipts').findWhere({'status':'pending','customer':false});
		
		// If no receipts have been created yet, create a blank one
		if(!availableReceipt){
			availableReceipt = this.get('receipts').add({date_paid: null});
		}
		
		var newCustomer = this.get('customers').create(customer);
		
		if(availableReceipt){
			availableReceipt.set({'customer': newCustomer, 'taxable': newCustomer.get('taxable')});
			availableReceipt.save();
		}
		
		if(!this.get('name') || this.get('name') == ''){
			this.set('name', customer.last_name).save();			
		}

		return true;	
	},
	
	addCustomerToReceipt: function(customer){
		var receipt = this.get('receipts').at(0);
		
		if(receipt != undefined && !receipt.get('customer')){
			receipt.set({'customer': customer});
			receipt.save();
		}
	},
	
	applyCustomerDiscount: function(customer){
		
		var items = this.get('cart');
		
		if(!customer){
			return false;
		}
		var discount = customer.get('discount');
		
		if(!items || items.length == 0){
			return false;
		}

		// Set the customer discount on each item
		if(discount && discount > 0){
			_.each(items.models, function(cart_item){
				cart_item.set_discount(discount);
				cart_item.save();
			});	
		
		// If customer discount is NOT set, but they belong to some groups
		// attempt to apply the group discount
		}else if(customer.get('groups') && customer.get('groups').length > 0){
			_.each(items.models, function(item){
				item.set_cost_plus_percent_discount(customer.get('groups'));
				item.save();
			});
		}
	},
	
	updateLayoutObject: function(){
		var name = this.get('name');
		var table_num = this.get('table_num');

		// TODO: Replace calls to global App.*
		_.each(App.data.food_bev_table_layouts.models, function(layout){
			if(layout.get('objects').findWhere({label: table_num})){
				layout.get('objects').findWhere({label: table_num}).set('table_name', name);
			}
		});
	},
	
	generateUrl: function(){
		this.url = API_URL + '/food_and_beverage/service/' + this.get('table_num') +'/';
		return this.url;
	},
	
	sendOrder: function(){

		var cartItems = this.get('cart').where({'selected': true});
		var copiedItems = [];
		var url = this.url + 'orders';
		var table = this;
		
		_.each(cartItems, function(item){
			// Clone the cart item to place in the order object
			var item_copy = item.toJSON();
			copiedItems.push(new FbCartItem(item_copy, {url: ''}));
		});
		
		var customers = [];
		if(table.get('customers').length > 0){
			customers = table.get('customers').toJSON();
		}
		
		// Create a new order with the list of selected items
		var order = new Order({
			'items': copiedItems,
			'customers': customers,
			'table_name': table.get('name'),
			'table_number': table.get('table_num'),
			'guest_count': table.get('guest_count')
		}, {'url': url});
		order.url = url;
		
		// Save order to back end
		order.save({}, {success: function(){
			table.set('orders_made', parseInt(table.get('orders_made')) + 1);
		}});
		
		return true;
	},
    forceClose:function(callback){
        var table = this;
        // It appears the table has nothing owed, and the user has been prompted
        table.destroy({success: function(model,resp,options) {

            // Refresh recent transactions
            table.loadRecentTransactions();

            // Clear out existing table data
            table.resetTable();

            if(typeof callback === 'function')callback(model,resp,options);
        },error:function(model,resp,options){
            console.log('destroy error in close!');
            console.log('model: ',model);
            console.log('resp: ',resp);
            console.log('options: ',options);
        }});
    },
	cancelTable:function(callback){
		var table = this;
		var failure = false;
		var deferred = [];
        _.each(_.clone(table.get("cart").models), function(model) {
            deferred.push(model.destroy());
        });

        $.when.apply($, deferred).then(function(){
            // It appears the table has nothing owed, and the user has been prompted
            table.destroy({success: function(model,resp,options) {

                // Refresh recent transactions
                table.loadRecentTransactions();

                // Clear out existing table data
                table.resetTable();

                if(typeof callback === 'function')callback(model,resp,options);
            },error:function(model,resp,options){
                console.log('destroy error in close!');
                console.log('model: ',model);
                console.log('resp: ',resp);
                console.log('options: ',options);
            }});
		})

	},
	
	close: function(callback){
		
		var receipts = this.get('receipts');
		var table = this;

		if(!receipts.paymentsMade() || receipts.allReceiptsPaid()){
			
			this.forceClose(callback);
			
			return true;
		}
		
		return false;	
	},

	resetTable: function(){
		var table = this;
		table.get('cart').reset(null);
		table.get('receipts').reset(null);
		table.get('customers').reset(null);

		table.set({
			'suspended_sale_id': null,
			'name': null,
			'sale_time': null,
			'table_num': null
		});		
	},
	
	showError: function(model, response){
		$.loadMask.hide();
		// TODO: Replace calls to global App.*
		App.errorHandler(model, response);
		return false;
	},

	renderCustomers: function(){
		var customers = this.get('customers');
		var count = 0;
		if(customers)count = customers.length;
		$('button.toggle-customers').html('Customers ('+count+')');
	}
});

var FbTableCollection = Backbone.Collection.extend({	
	model: function(attrs, options){
		return new FbTable(attrs, options);
	}
});
