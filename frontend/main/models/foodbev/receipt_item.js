var FbReceiptItem = FbCartItem.extend({
	
	defaults: {
		"split_subtotal": 0,
		"split_total": 0,
		"split_tax": 0,
		"split_comp_total": 0,
		"comp": false,
		"unit_price_includes_tax": 0,
		"service_fees": [],
		"force_tax": null,
		"taxable": 1
	},
	
	initialize: function(attrs, options){

		if(this.collection && this.collection.table){
			this.table = this.collection.table;
		}else if(typeof options !== 'undefined' && typeof options.table !== 'undefined'){
			this.table = options.table;
		}else if(typeof App.data.food_bev_table !== 'undefined') {
			this.table = App.data.food_bev_table;
		}else{
			// Standalone item outside of a real table context
			// Used for Service fees in POS, and
			// Not sure I like this, as it has potential to mask some useful errors.
			this.table = new FbTable();
		}
		
		var line = this.get('line');
		// TODO: Replace calls to global App.*
		var cart = this.table.get('cart');
		var cart_item = cart.get(line);
		if(cart_item){
			this.cart_item = cart_item;
		}

		var modifierCollection = new ModifierCollection(this.get('modifiers'));
		var sideCollection = new FbItemSideCollection(this.get('sides'));
		var soupCollection = new FbItemSideCollection(this.get('soups'));
		var saladCollection = new FbItemSideCollection(this.get('salads'));
		var serviceFeesCollection = new ServiceFeeCollection(this.get('service_fees'));

		// Keep all sides sorted by position
		sideCollection.comparator = 'position';
		soupCollection.comparator = 'position';
		saladCollection.comparator = 'position';

		// Check if the customer attached to this receipt has a default discount
		var receipt = false;
		var customer = false;
		if(this.collection && this.collection.receipt){
			receipt = this.collection.receipt;
			customer = receipt.get('customer');
		}

		// Create url to save sides to database
		// TODO: Replace calls to global App.*
		sideCollection.url = this.table.url + "cart/" + this.get('line') + "/sides";
		soupCollection.url = this.table.url + "cart/" + this.get('line') + "/soups";
		saladCollection.url = this.table.url + "cart/" + this.get('line') + "/salads";

		this.set({
			modifiers: modifierCollection,
			sides: sideCollection,
			soups: soupCollection,
			salads: saladCollection,
			service_fees: serviceFeesCollection
		});

		// Link cart item to receipt item
		// If cart item changes, automatically update receipt item
		if(cart_item){
			this.listenTo(cart_item, "change", this.update, this);
			this.listenTo(cart_item, "remove", this.delete, this);
		}

		// Apply customer discount to item
		if(customer && customer.get('discount') > 0){
			cart_item.set_discount(receipt.get('customer').get('discount'));
			cart_item.save();

			// If customer belongs to any groups, check if there is a group cost + % discount
		}else if(customer && customer.get('groups').length > 0){
			cart_item.set_cost_plus_percent_discount(customer.get('groups'));
			cart_item.save();
		}
	},

	update: function(data){
		this.set(data.attributes);
	},

	splitPaid: function(){
		// returns true if it is split across multiple receipts,
		// and one or more of them is paid.
		if(this.getNumberSplits()<1)return false;
		// TODO: Replace calls to global App.*
		var receipts = this.table.get('receipts').models;
		var line = this.get('line');
		var cart_item = this.table.get('cart').get(line);
		var partialPayments = false;
		var paidSplits=0;
		_.each(receipts, function(receipt){

			if(receipt.get('items').length == 0){
				return;
			}else if(! receipt.getTotalPayments() > 0){
				return;
			}

			// Loop through each completed receipt's items
			_.each(receipt.get('items').models, function(item){
				//if(partialPayments)return;
				if(item.get('line') == line){
					paidSplits++;
					partialPayments = true;
				}
			});
		});
		if(typeof cart_item !== 'undefined')
			cart_item.set('paid_splits',paidSplits);
		else return 0;

		return partialPayments;
	},
	
	// Loops through all receipts and checks if this item is split
	// across multiple receipts
	getNumberSplits: function(){

		var receipts = this.table.get('receipts').models;
		var line = this.get('line');
		var divisor = 1;
		var cart_item = this.table.get('cart').get(line);
		
		// Loop through each receipt
		_.each(receipts, function(receipt){
			
			if(receipt.get('items').length == 0){
				return false;
			}
			
			// Loop through each receipt's items
			_.each(receipt.get('items').models, function(item){
				if(item.get('line') == line){
					divisor++;
					return true;
				}
			});
		});
		if(typeof cart_item !== 'undefined')
			cart_item.set('total_splits',divisor-2);
		else return 0;
		return divisor-2;
	},
	
	// Calculate receipt item price (which may be split)
	calculatePrice: function(divisor, taxable){
		if(typeof(taxable) === 'undefined'){
			taxable = 1;
		}
		
		var receiptItem = this;
		if(!divisor || divisor < 1){
			divisor = 1;
		}

		var cartItem = this.cart_item;
		if(this.cart_item){
			this.cart_item.calculatePrice();
		}

		// Loop through all different sides and get their totals
		var sideTypes = ['soups', 'salads', 'sides'];
		var sidesSplitTotal = new Decimal(0);
		var sidesSplitTax = new Decimal(0);
		var sidesSplitSubtotal = new Decimal(0);

		// Calculate and add up split total, tax, and subtotal of all sides
		_.each(sideTypes, function(sideType){
			if(receiptItem.get(sideType) && receiptItem.get(sideType).length > 0){
				_.each(receiptItem.get(sideType).models, function(side){
					if(!side.get('name'))return;
					receiptItem.calculateSplitPrices(divisor, side, taxable);	
					
					sidesSplitSubtotal = sidesSplitSubtotal.plus(side.get('split_subtotal'));
					sidesSplitTax = sidesSplitTax.plus(side.get('split_tax'));
					sidesSplitTotal = sidesSplitTotal.plus(side.get('split_tax')).plus(side.get('split_subtotal'));				
				});
			}
		});

		this.calculateSplitPrices(divisor, this, taxable);

		var serviceFeesSplitSubtotal = new Decimal(0);
		var serviceFeesSplitTax = new Decimal(0);
		var serviceFeesSplitTotal = new Decimal(0);

		_.each(this.get('service_fees').models, function(service_fee){

			var parent_item_data = {
				"unit_price": receiptItem.get('split_price'),
				"non_discount_subtotal": receiptItem.get('split_subtotal_no_discount'),
				"subtotal": receiptItem.get('split_subtotal'),
				"tax": receiptItem.get('split_tax'),
				"total": receiptItem.get('split_total'),
				"quantity": receiptItem.get('quantity'),
				"comp": cartItem.get('comp')
			};

			service_fee.calculatePrice(taxable, divisor, parent_item_data);

			serviceFeesSplitSubtotal = serviceFeesSplitSubtotal.plus(service_fee.get('subtotal'));
			serviceFeesSplitTax = serviceFeesSplitTax.plus(service_fee.get('tax'));
			serviceFeesSplitTotal = serviceFeesSplitTotal.plus(service_fee.get('tax')).plus(service_fee.get('subtotal'));
		});

		this.set({
			'taxable': taxable,
			'split_service_fees_subtotal': serviceFeesSplitSubtotal.toDP(2).toNumber(),
			'split_service_fees_tax': serviceFeesSplitTax.toDP(2).toNumber(),
			'split_service_fees_total': serviceFeesSplitTotal.toDP(2).toNumber(),			
			'split_sides_subtotal': sidesSplitSubtotal.toDP(2).toNumber(),
			'split_sides_total': sidesSplitTotal.toDP(2).toNumber(),
			'split_sides_tax': sidesSplitTax.toDP(2).toNumber()
		});

		return this;
	},
	
	calculateSplitPrices: function(divisor, item, taxable){
		
		var data = {};
		data.total = new Decimal(item.get('total'));
		data.subtotal = new Decimal(item.get('subtotal'));
		data.subtotal_no_discount =  new Decimal(item.get('subtotal_no_discount'));
		data.tax =  new Decimal(item.get('tax'));
		data.comp_total = new Decimal(item.get('comp_total'));
		data.modifier_total = new Decimal(item.get('modifier_total'));

		// force tax is separate from taxable
		var force_tax = null;
		if(parseInt(this.get('force_tax')) === 0){
			force_tax = 0;
		}else if(parseInt(this.get('force_tax')) === 1){
			force_tax = 1;
		}

		var tax_included = (item.get('unit_price_includes_tax') == 1);
		
		data.price = new Decimal(item.get('price'));
		data.discount_amount = new Decimal(item.get('discount_amount'));
		data.number_splits = parseInt(divisor);
		
		data.split_price = data.price.dividedBy(divisor).toDP(2).toNumber();
		data.split_discount_amount = data.discount_amount.dividedBy(divisor).toDP(2).toNumber();
		data.split_modifier_total = data.modifier_total.dividedBy(divisor).toDP(2).toNumber();

		data.split_comp_total = data.comp_total.dividedBy(divisor).toDP(2);

		// If tax is included, it has already been deducted from the subtotal.
		// We need to add it back in, then split that price.
		if(tax_included){
			data.split_subtotal_no_discount = data.subtotal_no_discount.plus(data.tax).dividedBy(divisor).toDP(2);
			data.split_subtotal = data.subtotal.plus(data.tax).dividedBy(divisor).toDP(2);
		}else{
			data.split_subtotal_no_discount = data.subtotal_no_discount.dividedBy(divisor).toDP(2);
			data.split_subtotal = data.subtotal.dividedBy(divisor).toDP(2);			
		}

		data.split_tax = item.getTax(data.split_subtotal.toDP(2).toNumber(), tax_included);

		// Re-calculate tax with new split subtotal (to get most accurate tax after splitting the item)
		if(tax_included && data.split_subtotal_no_discount>=data.split_price){
			data.split_subtotal_no_discount = data.split_subtotal_no_discount.minus(data.split_tax);
			data.split_subtotal = data.split_subtotal.minus(data.split_tax);
		}
		if((taxable == 1 && parseInt(force_tax) !== 0) || force_tax){
			data.split_total = data.split_subtotal.plus(data.split_tax).toDP(2).toNumber();
		}else{
			data.split_total = data.split_subtotal.toDP(2).toNumber();
			data.split_tax = 0;
			if(force_tax === 0){
				_.forEach(this.get('taxes').models,function(tax){
					tax.amount = 0;
				});
			}
		}

		data.split_subtotal = data.split_subtotal.toDP(2).toNumber();
		data.split_subtotal_no_discount = data.split_subtotal_no_discount.toDP(2).toNumber();
		data.total = data.total.toDP(2).toNumber();
		data.subtotal = data.subtotal.toDP(2).toNumber();
		data.subtotal_no_discount = data.subtotal_no_discount.toDP(2).toNumber();
		data.tax = data.tax.toDP(2).toNumber();
		data.comp_total = data.comp_total.toDP(2).toNumber();
		data.split_comp_total = data.split_comp_total.toDP(2).toNumber();

		item.set(data);
		return true;
	},

	delete: function(data){
		if(this.collection){
			this.collection.remove(this);
		}
	}
});
