var RegisterLog = Backbone.Model.extend({
	
	urlRoot: API_URL + '/register_logs',
	idAttribute: "register_log_id",
	
	defaults: {
		"shift_start": '0000-00-00 00:00:00',
		"shift_end": '0000-00-00 00:00:00',
		"status": "open",
		"open_amount": false,
		"close_amount": 0.00,
		"close_check_amount": 0.00,
		"cash_sales_amount": 0.00,
		"persist": false,
		"employee_id": null,
		"terminal_id": null,
		"drawer_number": '0',
		"counts": {
			"pennies": 0,
			"nickels": 0,
			"dimes": 0,
			"quarters": 0,
			"ones": 0,
			"fives": 0,
			"tens": 0,
			"twenties": 0,
			"fifties": 0,
			"hundreds": 0
		}
	}
});

var RegisterLogCollection = Backbone.Collection.extend({
	
	url: API_URL + '/register_log',
	model: RegisterLog,

	get_open_log: function(terminal_id, match_user){
		
		var terminal = false;
		// TODO: Replace calls to global App.*
		if(terminal_id === undefined){
			terminal = App.data.course.get('terminals').findWhere({'active': true});
		}else{
			terminal = App.data.course.get('terminals').get(terminal_id);
		}

		if(!terminal){
			var terminal_id = '0';
			var multi_cash_drawers = '0';
		}else{
			var terminal_id = String(terminal.get('terminal_id'));
			var multi_cash_drawers = terminal.get('multiCashDrawers');
		}
		
		var register_logs = new Backbone.Collection();
		_.each(this.models, function(log){
			
			// If log has been closed, skip it
			if(log.get('terminal_id') != terminal_id ||
				log.get('shift_end') != '0000-00-00 00:00:00'){
				return false;
			}

			if(multi_cash_drawers == 1 && !match_user){
				register_logs.add(log);
				return false;
			}

			if( 
				log.get('persist') == 1 || 
				log.get('employee_id') == App.data.user.get('person_id')
			){
				register_logs.add(log);
			}
		});

		if(register_logs.at(0)){
			return register_logs.at(0);
		}

		return false;
	},

	prompt_to_open_log: function(force){

		// If user has chosen to skip the register log, or the course does not use register logs, do nothing
		// TODO: Replace calls to global App.*
		if(!force && (App.data.course.get('skipped_register_log') == 1 || 
			App.data.course.get('continued_register_log') == 1 || 
			App.data.course.get_setting('use_register_log') == 0)){
			return false;
		}
		var register_log = this.get_open_log();
		
		// If there is a log open on the current terminal, but it was not opened by
		// the current employee, do nothing.
		if(App.data.course.get_setting('multi_cash_drawers') == 1){
			if(register_log && register_log.get('employee_id') != App.data.user.get('person_id')){
				return false;
			}
		}

		// If there are no open register logs, show the window to open
		// the register log
		if(!register_log || register_log == undefined){
			var register_log_window = new RegisterLogView({
				model: new RegisterLog(),
				collection: this, 
				page: 'sales',
				closable: false
			});
			register_log_window.show();
		
		// If there is an open register log
		// ask user if they would like to continue it or create a new one
		}else if(register_log && App.data.course.get('unfinished_register_log') == 0){
			var register_log_window = new RegisterLogView({
				model: register_log,
				collection: this, 
				page: 'sales',
				closable: false
			});
			register_log_window.show();			
		
		}else{
			register_log.set('active', true);
		}
	},

	set_active: function(register_log_id){
		
		var log = false;
		if(register_log_id){
			log = this.get(register_log_id);
		}

		_.each(this.models, function(register_log){
			register_log.set('active', false);
		});
		
		if(log && log != undefined){
			log.set('active', true);
		}
		
		return true;	
	}
});
