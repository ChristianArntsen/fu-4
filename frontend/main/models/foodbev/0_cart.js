var FbCartItem = Backbone.Model.extend({

	idAttribute: "line",
	defaults: {
		"line": null,
		"name": "",
		"seat": 1,
		"item_id": "",
		"category": "",
		"sub_category": "",
		"department": "",
		"description": "",
		"max_discount": 0,
		"discount":0,
		"discount_amount": 0,
		"discount_percent": 0,
		"price": 0.00,
		"subtotal": 0.00,
		"total": 0.00,
		"tax": 0.00,
		"quantity": 1,
		"is_ordered": 0,
		"is_paid": 0,
		"splits": 0,
		"paid_splits": 0,
		"modifiers": [],
		"sides": [],
		"soups": [],
		"salads": [],
		"customer_groups": [],
		"number_sides": 0,
		"number_soups": 0,
		"number_salads": 0,
		"printer_ip": "",
		"print_priority": 0,
		"comments":"",
		"is_new": false,
		"comp": false,
		"displayed": false,
		"is_side": 0,
		"erange_size": false,
		"service_fees": [],
		"service_fees_subtotal": 0,
		"service_fees_tax": 0,
		"service_fees_total": 0,
		"force_tax": null,
		"taxable": 1
	},

	initialize: function(attrs, options){

		this.cart = false;
		if(this.collection && this.collection.cart){
			this.cart = this.collection.cart;
		}
		if(this.collection && this.collection.table){
			this.table = this.collection.table;

		}else if(typeof options === 'undefined'){
			this.table = App.data.food_bev_table;
		}else if(typeof options.table !== 'undefined'){
			this.table = options.table;
		}else{
			this.table = App.data.food_bev_table;
		}

		var line = this.get('line');
		var modifierCollection = new ModifierCollection(this.get('modifiers'));
		var sideCollection = new FbItemSideCollection(this.get('sides'), {cart: this.cart});
		var soupCollection = new FbItemSideCollection(this.get('soups'), {cart: this.cart});
		var saladCollection = new FbItemSideCollection(this.get('salads'), {cart: this.cart});

		// Keep all sides sorted by position
		sideCollection.comparator = 'position';
		soupCollection.comparator = 'position';
		saladCollection.comparator = 'position';

		// Create url to save sides to database
		sideCollection.url = this.url + this.get('line') + "/sides";
		soupCollection.url = this.url + this.get('line') + "/soups";
		saladCollection.url = this.url + this.get('line') + "/salads";

		if(this.get('customer_groups')){
			this.set('customer_groups', new GroupCollection(this.get('customer_groups')));
		}
		if(this.get('service_fees')){
			this.set('service_fees', new ServiceFeeCollection(this.get('service_fees')));
		}

		var customer = false;

		// If F&B item is in the POS cart
		if(this.cart instanceof Cart){

			this.listenTo(this.cart, 'change:taxable', this.calculatePrice);
			this.on("change:selected", this.set_check_all);

			if(!this.has('unit_price_includes_tax')){
				this.set('unit_price_includes_tax', (App.data.course.get('unit_price_includes_tax') == 1), {'silent': true});
			}
			this.set('unit_price_includes_tax', (this.get('unit_price_includes_tax') == 1), {'silent': true});

			// If a customer with a discount is attached to cart, apply discount to item
			var customer = false;
			if(this.cart && this.cart.get('customers') instanceof CartCustomerCollection && this.cart.get('customers').length > 0){
				var customer = this.cart.get('customers').findWhere({selected: true});
			}

		}else{
			// Customer discount logic is handeld on the ReceiptItem level in F&B
			var customer = null;
			this.on("invalid", this.displayError);
		}

		var customer_discount = new Decimal(0);
		if(customer){
			customer_discount = new Decimal(customer.get('discount'));
		}

		// If customer has discount
		if(customer && customer_discount.greaterThan(0)){
			this.set_discount(customer_discount.toDP(2).toNumber());
		
		// If customer belongs to groups, apply any discounts from them to the item
		}else if(customer && customer.get('groups').length > 0){
			this.set_cost_plus_percent_discount(customer.get('groups'));
		}

		this.set({
			modifiers: modifierCollection,
			sides: sideCollection,
			soups: soupCollection,
			salads: saladCollection
		});

		this.calculatePrice();

		// If any changes made to item, re-calcuate price (total, tax, etc)
		this.on("change:price change:discount change:quantity change:comp", this.calculatePrice);
		this.listenTo(this.get('sides'), "change add remove", this.calculatePrice);
		this.listenTo(this.get('soups'), "change add remove", this.calculatePrice);
		this.listenTo(this.get('salads'), "change add remove", this.calculatePrice);
		this.listenTo(this.get('modifiers'), "change", this.calculatePrice);
	},

	set_discount: function(discount_percent){
		if(!discount_percent){
			return false;
		}
		var discount_percent = new Decimal(discount_percent);

		// If a discount is already set on the item, or it's an item that should
		// not have a discount at all, skip it
		var current_discount = new Decimal(this.get('discount'));
		if(current_discount.greaterThan(0)){
			return true;
		}
		
		var max_discount = new Decimal(this.get('max_discount'));
		var allowed_discount = discount_percent;
		
		// Force discount to item's max discount
		if(max_discount.lessThan(discount_percent)){
			allowed_discount = max_discount;
		}
		
		this.set({
			'discount_percent': allowed_discount.toDP(2).toNumber(),
			'discount': allowed_discount.toDP(2).toNumber()
		});
	},

	set_cost_plus_percent_discount: function(customer_groups){

		// Get best discount rate (cost + lowest percentage)
		if(!this.get('customer_groups')){
			return false;
		}
		var cost_plus_percent = this.get('customer_groups').getBestCostPlusPercent(customer_groups);

		// If no discount was found in the given groups, do nothing
		if(cost_plus_percent === false || cost_plus_percent === undefined || cost_plus_percent === ''){
			return false;
		}

		// Determine effective discount percentage on unit price
		cost_plus_percent = new Decimal(cost_plus_percent).dividedBy(100).toDP(2);
		var price = new Decimal(this.get('base_price'));
		var cost = new Decimal(this.get('cost_price'));
		var discounted_price = cost.times(cost_plus_percent).plus(cost).toDP(2);

		//var effective_discount_percent = new Decimal(1).minus(discounted_price.dividedBy(price).toDP(2));
        var effective_discount_percent = price.minus(discounted_price).dividedBy(price);
		this.set_discount(effective_discount_percent.times(100).toDP(2).toNumber());
	},

	set_check_all: function(){
		if(this.cart && this.get('selected') == false){
			this.cart.set({'check_all': false});
		}
	},

	displayError: function(model, message){
		App.vent.trigger('notification', {msg: message, type: 'error'});
	},

	// Returns a JSON copy of all the item data (including nested collections)
	copy: function(){

		var itemCopy = _.clone(this.attributes);
		itemCopy.modifiers = itemCopy.modifiers.toJSON();

		// Loop through all sides attached to item and copy their modifiers as well
		var sideTypes = ['sides', 'soups', 'salads'];
		_.each(sideTypes, function(type){

			itemCopy[type] = itemCopy[type].toJSON();

			// If sides exist, loop through each side
			if(itemCopy[type].length > 0){

				// Copy side modifiers
				_.each(itemCopy[type], function(side, index){
					itemCopy[type][index].modifiers = side.modifiers.toJSON();
				});
			}
		});

		return itemCopy;
	},

	getNumberSplits: function(){

		var receipts = App.data.food_bev_table.get('receipts').models;
		var line = this.get('line');
		var divisor = 1;
		var cart_item = App.data.food_bev_table.get('cart').get(line);

		// Loop through each receipt
		_.each(receipts, function(receipt){

			if(receipt.get('items').length == 0){
				return false;
			}

			// Loop through each receipt's items
			_.each(receipt.get('items').models, function(item){
				if(item.get('line') == line){
					divisor++;
					return true;
				}
			});
		});
		if(typeof cart_item !== 'undefined')
			cart_item.set('total_splits',divisor-2);
		else return 0;
		return divisor-2;
	},

	splitPaid: function(){
		// returns true if it is split across multiple receipts,
		// and one or more of them is paid.
		if(this.getNumberSplits()<1)return false;
		var receipts = App.data.food_bev_table.get('receipts').models;
		var line = this.get('line');
		var cart_item = App.data.food_bev_table.get('cart').get(line);
		var partialPayments = false;
		var paidSplits=0;
		_.each(receipts, function(receipt){

			if(receipt.get('items').length == 0){
				return;
			}else if(receipt.get('status')!=='complete'){
				return;
			}

			// Loop through each completed receipt's items
			_.each(receipt.get('items').models, function(item){
				//if(partialPayments)return;
				if(item.get('line') == line){
					paidSplits++;
					partialPayments = true;
				}
			});
		});
		if(typeof cart_item !== 'undefined')
			cart_item.set('paid_splits',paidSplits);
		else return 0;

		return partialPayments;
	},

	set: function(attributes, options){

		if(typeof(attributes) == 'string'){
			var key = attributes;
			var value = options;

			attributes = {};
			attributes[key] = value;
		}

		if(!this.attributes.line && this.collection){
			this.attributes.line = this.collection.getNextLine();
			this.id = this.attributes.line;
		}

		// If quantity, discount or comp is changed on cart item,
		// apply same change to item sides (if any)
		var adjustSides = false;
		if(attributes.quantity || attributes.discount || attributes.comp){
			adjustSides = true
		}

		if(adjustSides){
			var cartItem = this;
			var sideTypes = ['sides', 'soups', 'salads'];
			var sideAttributes = _.pick(attributes, 'quantity', 'discount', 'comp');

			if(sideAttributes.discount != undefined){
				sideAttributes.discount_percent = sideAttributes.discount;
			}

			_.each(sideTypes, function(sideType){
				if(cartItem.get(sideType) && cartItem.get(sideType).length > 0){
					_.each(cartItem.get(sideType).models, function(side){

						side.set(sideAttributes, {silent: true});
						side.calculatePrice();
					});
				}
			});
		}

		Backbone.Model.prototype.set.call(this, attributes, options);
		return this;
	},

	calculatePrice: function(taxable){
		var cartItem = this;
		if(typeof(taxable) == 'undefined'){
			taxable = 1;
		}

		// Loop through all different sides and get their totals
		var sideTypes = ['sides', 'soups', 'salads'];
		var sidesTotal = new Decimal(0);
		var sidesTax = new Decimal(0);
		var sidesSubtotal = new Decimal(0);
		var sidesBaseSubtotal = new Decimal(0);
		var modifierTotal = new Decimal(0);
		var sidesCompTotal = new Decimal(0);
		var pos_cart = this.cart;
		
		if(this.cart && this.cart instanceof Cart){	
			taxable = this.cart.get('taxable');
		}

		// force tax != taxable
		var force_tax = null;
		if(parseInt(this.get('force_tax')) === 0){
			force_tax = 0;
		}else if(parseInt(this.get('force_tax')) === 1){
			force_tax = 1;
		}

		if(!this.get('quantity')){
			this.set('quantity',1);
		}

		var tax_included = (this.get('unit_price_includes_tax') == 1);

		_.each(sideTypes, function(sideType){
			if(cartItem.get(sideType) && cartItem.get(sideType).length > 0){
				_.each(cartItem.get(sideType).models, function(side){
					sidesTotal = sidesTotal.plus(side.get('total'));
					sidesTax = sidesTax.plus(side.get('tax'));
					sidesSubtotal = sidesSubtotal.plus(side.get('subtotal'));
					if(side.has('comp_total')){
						sidesCompTotal = sidesCompTotal.plus(side.get('comp_total'));
					}
					if(side.has('base_subtotal')){
						sidesBaseSubtotal = sidesBaseSubtotal.plus(side.get('base_subtotal'));
					}
				});
			}
		});

		var modifierTotal = new Decimal(this.get('modifiers').getTotalPrice());
		var price = new Decimal(this.get('price'));
		var price_with_modifiers = price.plus(modifierTotal).toDP(2).toNumber();

		var no_discount_subtotal = this.getSubtotal(price_with_modifiers, this.get("quantity"), 0);
		var subtotal = this.getSubtotal(price_with_modifiers, this.get("quantity"), this.get('discount'));
		var discount_amount = new Decimal(no_discount_subtotal).minus(subtotal);
		var comp_discount = this.getCompDiscount(subtotal);
		var base_subtotal = subtotal;
		subtotal = new Decimal(subtotal).minus(comp_discount).toDP(2);

		var tax = this.getTax(subtotal.toNumber(), tax_included);
		if(tax_included){
			no_discount_subtotal = new Decimal(no_discount_subtotal).minus(tax).toDP(2).toNumber();
			subtotal = subtotal.minus(tax).toDP(2);
		}
		if((!taxable && force_tax === null) || parseInt(force_tax)===0){
			tax = 0;
			if(force_tax === 0){
				_.forEach(this.get('taxes').models,function(tax){
					tax.amount = 0;
				});
			}
		}

		var total = this.getTotal(subtotal.toNumber(), tax);
		var comp_discount = new Decimal(comp_discount).plus(sidesCompTotal).toDP(2);
		modifierTotal = modifierTotal.times(this.get('quantity'));
		
		var service_fees_total = new Decimal(0);
		var service_fees_subtotal = new Decimal(0);
		var service_fees_tax = new Decimal(0);

		if(this.get('service_fees') && this.get('service_fees').length > 0){
			_.each(this.get('service_fees').models, function(fee){
				
				var parent_item_data = {
					"unit_price": cartItem.get('price'),
					"non_discount_subtotal": no_discount_subtotal,
					"subtotal": subtotal.toDP(2).toNumber(),
					"tax": tax,
					"total": total,
					"quantity": cartItem.get('quantity'),
					"comp": cartItem.get('comp')
				};

				fee.calculatePrice(taxable, 1, parent_item_data);
				
				service_fees_subtotal = service_fees_subtotal.plus(fee.get('subtotal')).toDP(2);
				service_fees_tax = service_fees_tax.plus(fee.get('tax')).toDP(2);
				service_fees_total = service_fees_total.plus(fee.get('total')).toDP(2);
				comp_discount = comp_discount.plus(fee.get('comp_total'));
			});
		}
		
		this.set({
			"taxable": taxable,
			"comp_total": comp_discount.toDP(2).toNumber(),
			"sides_total": sidesTotal.toDP(2).toNumber(),
			"sides_tax": sidesTax.toDP(2).toNumber(),
			"sides_subtotal": sidesSubtotal.toDP(2).toNumber(),
			"sides_base_subtotal": sidesBaseSubtotal.toDP(2).toNumber(),
			"service_fees_subtotal": service_fees_subtotal.toNumber(),
			"service_fees_tax": service_fees_tax.toNumber(),
			"service_fees_total": service_fees_total.toNumber(),
			"modifier_total": modifierTotal.toDP(2).toNumber(),
			"discount_amount": discount_amount.toDP(2).toNumber(),
			"base_subtotal": base_subtotal,
			"subtotal": subtotal.toDP(2).toNumber(),
			"subtotal_no_discount": no_discount_subtotal,
			"non_discount_subtotal": no_discount_subtotal,
			"tax": tax,
			"total": total
		});
	},

	getSubtotal: function(price, qty, discount){
		if(!discount){
			discount = 0;
		}
		if(!qty){
			qty = 1;
		}

		var price = new Decimal(price);
		var discount = new Decimal(discount);
		var qty = new Decimal(qty);

		var percent = new Decimal(100).minus(discount).dividedBy(100);
		var discounted = price.times(percent).toDP(2).times(qty);

		return discounted.toDP(2).toNumber();
	},

	getCompDiscount: function(subtotal){

		var subtotal = new Decimal(subtotal);
		var comp_amount = new Decimal(0);

		if(this.get('comp') && this.get('comp').amount > 0){
			comp_amount = new Decimal(this.get('comp').amount);

			if(this.get('comp').type == 'percentage'){
				var percent = comp_amount.dividedBy(100);
				comp_amount = subtotal.times(percent);
			}
		}

		return comp_amount.toDP(2).toNumber();
	},

	getTax: function(subtotal, tax_included){

		var totalTax = new Decimal(0);
		var subtotal = new Decimal(subtotal);
		var item = this;
		var taxList = this.get('taxes');
		var taxes = [];

		_.each(taxList, function(tax){
			taxes.push(_.clone(tax));
		});		

		if(tax_included === undefined){
			tax_included = false;
		}
		if(!taxes || taxes.length == 0){
			return 0;
		}

		var total_percent = 0.00;
		_.each(taxes,function(tax){
			if(!tax.percent || tax.percent <= 0)return;
            total_percent += tax.percent*1;
		});
        total_percent = new Decimal(total_percent);
		_.each(taxes, function(tax){
			var percentage = new Decimal(tax.percent);
			if(percentage.isNaN() || percentage.isZero()){
				return true;
			}

			var taxAmount = new Decimal(0);
			if(tax_included){
				var actual_subtotal = subtotal.dividedBy( total_percent.dividedBy(100).plus(1) ).toDP(2);
                var percent = percentage.dividedBy(100);
				taxAmount = actual_subtotal.times(percent).toDP(2);

			}else if(tax.cumulative == "1"){
				var percent = percentage.dividedBy(100);
				taxAmount = subtotal.plus(totalTax).times(percent).toDP(2);

			}else{
				var percent = percentage.dividedBy(100);
				taxAmount = subtotal.times(percent).toDP(2);
			}

			tax.amount = taxAmount.toDP(2).toNumber();
			totalTax = totalTax.plus(taxAmount.toNumber());
		});
		
		this.set('taxes', taxes, {silent: true});
		return totalTax.toDP(2).toNumber();
	},

	getTotal: function(subtotal, tax, taxable){
		var subtotal = new Decimal(subtotal);
		var tax = new Decimal(tax);
		
		if(taxable === undefined){
			taxable = true;
		}

		if(taxable){
			var total = subtotal.plus(tax);
		}else{
			var total = subtotal;
		}
		
		return total.toDP(2).toNumber();
	},

	validate: function(attrs, options){

		if(this.cart instanceof Cart){

			if(this instanceof FbSide || this.attributes.quantity < 0){
				return false;
			}

			if(attrs.unit_price !== undefined || attrs.discount_percent !== undefined){
				var minPrice = this.getSubtotal(attrs.base_price, attrs.quantity, attrs.max_discount);
				var setPrice = this.getSubtotal(attrs.unit_price, attrs.quantity, attrs.discount_percent);
				var hasPermission = (App.data.user.is_manager() || App.data.user.is_admin() || App.data.user.is_super_admin());
				var isAuthorized = false;

				if(App.data.cart.get('manager_override_id') != null){
					isAuthorized = true;
					hasPermission = true;
				}

				if(!hasPermission && !isAuthorized && setPrice < minPrice){
					return "Maximum discount is " + accounting.formatMoney(this.get('max_discount'), '') + '% or ' + accounting.formatMoney(minPrice) +
						'<button class="btn btn-sm btn-default pos-manager-override">Manager Override</button>';

				}else if(hasPermission && !isAuthorized && setPrice < minPrice){
					App.vent.trigger('notification', {
						type: 'info',
						msg: "Maximum discount is " + accounting.formatMoney(this.get('max_discount'), '') + '% or <strong>' + accounting.formatMoney(minPrice)+'</strong>'
					});
				}
			}

		}else{
			var maxDiscount = this.get('max_discount');
			var minPrice = this.getSubtotal(this.get('base_price'), attrs.quantity,  maxDiscount);
			var setPrice = _.round( this.getSubtotal(attrs.price, attrs.quantity, attrs.discount), 2);

			if(attrs.quantity < 1){
				return "Quantity must be at least 1";
			}

			if(setPrice < minPrice){
				return "Maximum discount is " + accounting.formatMoney(this.get('max_discount'), '') + '% or ' + accounting.formatMoney(minPrice);
			}
		}
	},

	isSoupsComplete: function(){
		if(this.get('soups').length < this.get('number_soups')){
			return false;
		}
		return true;
	},

	isSidesComplete: function(){
		if(this.get('sides').length < this.get('number_sides')){
			return false;
		}
		return true;
	},

	isSaladsComplete: function(){
		if(this.get('salads').length < this.get('number_salads')){
			return false;
		}
		return true;
	},

	isModifiersComplete: function(categoryId){
		if(!this.get('modifiers').isComplete(categoryId)){
			return false;
		}
		return true;
	},

	// Checks if all required fields, sides, and modifiers are set
	isComplete: function(){

		if(!this.isSoupsComplete()){ return false }
		if(!this.isSidesComplete()){ return false }
		if(!this.isSaladsComplete()){ return false }
		if(!this.isModifiersComplete()){ return false }

		return true;
	},

	// Stop listening to cart tax changes
	destroy: function(options){

		var cartItem = this;
		if(this.cart && this.cart instanceof Cart){

			// Loop through sides and stop listening to cart changes
			var sideTypes = ['sides', 'soups', 'salads'];
			_.each(sideTypes, function(sideType){
				if(cartItem.get(sideType) && cartItem.get(sideType).length > 0){
					_.each(cartItem.get(sideType).models, function(side){
						side.stopListening(side.cart);
					});
				}
			});

			cartItem.stopListening(cartItem.cart);
		}

		return Backbone.Model.prototype.destroy.apply(this, options);
	}
});

var FbCart = Backbone.Collection.extend({

	model: FbCartItem,

	initialize: function(attrs, options){
		this.listenTo(this, 'error', this.undo);
		if(typeof options.table !== 'undefined'){
			this.table = options.table;
		}
	},

	// Undo quantity on item
	undo: function(item, response){

		if(!item.changed.quantity){
			this.remove(item);
		}else{
			item.set('quantity', item._previousAttributes.quantity);
		}

		App.vent.trigger('notification', {msg: response.responseJSON.msg, type: 'error'});
	},

	clearNew: function(){
		_.each(this.models, function(model){
			model.set('is_new', false);
		});
	},

	getNextSeat: function(){
		var seat = 0;
		_.each(this.models, function(cartItem){
			var curSeat = parseInt(cartItem.get('seat'));

			if(curSeat && curSeat > seat){
				seat = curSeat;
			}
		});

		return seat + 1;
	},

	getNextLine: function(){
		var line = 0;
		_.each(this.models, function(cartItem){
			var curLine = parseInt(cartItem.get('line'));

			if(curLine && curLine > line){
				line = curLine;
			}
		});

		return line + 1;
	},

	getTotals: function(){
		var total = new Decimal(0);
		var subtotal =  new Decimal(0);
		var tax =  new Decimal(0);
		var num_items = 0;

		_.each(this.models, function(cartItem){
			
			total = total.plus(cartItem.get('total'))
				.plus(cartItem.get('sides_total'))
				.plus(cartItem.get('service_fees_total'));
			subtotal = subtotal.plus(cartItem.get('subtotal'))
				.plus(cartItem.get('sides_subtotal'))
				.plus(cartItem.get('service_fees_subtotal'));
			tax = tax.plus(cartItem.get('tax'))
				.plus(cartItem.get('sides_tax'))
				.plus(cartItem.get('service_fees_tax'));

			var qty = parseInt(cartItem.get('quantity'));
			if(isNaN(qty) || !qty){
				qty = 0;
			}
			num_items += qty;
		});

		return {
			"total": total.toDP(2).toNumber(),
			"subtotal": subtotal.toDP(2).toNumber(),
			"tax": tax.toDP(2).toNumber(),
			"num_items": num_items
		};
	},

	unSelectItems: function(){
		var selectedItems = this.where({"selected":true});
		_.each(selectedItems, function(item){
			item.unset("selected");
		});
		return this;
	},

	selectAllItems: function(){
		_.each(this.models, function(item){
			if(!item.get('is_ordered')){
				item.set("selected", true);	
			}
		});
		return this;
	},

	addItem: function(item, bypass_meal_course){

		if(typeof bypass_meal_course === 'undefined'){
			bypass_meal_course = false;
		}

		if(
			App.data.course.get('use_course_firing') == 1 &&
			!bypass_meal_course &&
			item.get('prompt_meal_course') == 1 &&
			App.data.course.get('meal_courses').length > 0
		){
			// TODO: Move UI calls to a callback or listener
			var window = new MealCourseSelectWindow({collection: App.data.course.get('meal_courses'), model: item});
			window.show();
			return false;
		}

		var receipts =  App.data.food_bev_table.get('receipts');
		var attributes = item.toJSON();
		attributes.line = null;
		attributes.is_new = true;
		attributes.seat = 1;

		this.clearNew();

		// Get an avaliable receipt
		var availableReceipt = receipts.findWhere({'status':'pending'});
		if(!availableReceipt){
			var availableReceipt = receipts.add({date_paid: null});
		}

		// Add item to cart
		var cart_item = this.create(attributes, {'success': function(){
			var receipt_item = cart_item.toJSON();

			// Add item to first available receipt
			availableReceipt.get('items').add(receipt_item);
		}});

		return true;
	}
});
