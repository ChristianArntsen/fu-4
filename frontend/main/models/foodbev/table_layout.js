var TableLayout = Backbone.Model.extend({
	idAttribute: 'layout_id',
	
	initialize: function(attrs, options){
		var object_collection = new TableLayoutObjectCollection(this.get('objects'), {layout: this});
		object_collection.url = this.url() + '/objects';
		this.set('objects', object_collection);
		var self = this;
		
		this.listenTo(this.collection, 'enable-edit', function(){
			object_collection.trigger('enable-edit');		
		});
		
		this.listenTo(this.collection, 'disable-edit', function(){
			object_collection.trigger('disable-edit');	
		});

		this.listenTo(this.get('objects'), 'select-table', function(model){
			self.collection.trigger('select-table', model);
		})	
	},
	
	parse: function(response){
		
		var url = this.url();
		var objects = this.get('objects');
		
		if(response.layout_id && objects instanceof TableLayoutObjectCollection){
			objects.url = url + '/' + response.layout_id + '/objects';
		}
		
		if(response.objects && this.has('objects')){
			objects.url = url + '/objects';
			objects.reset(response.objects);
			response.objects = objects;
		}
		
		return response;
	}
});

var TableLayoutObject = Backbone.Model.extend({
	idAttribute: 'object_id',

	defaults: {
		label: '',
		employee_id: null,
		employee_first_name: null,
		employee_last_name: null,
		table_name: ''
	},
	
	changeEmployee: function(employee_id, callback){

		if(!employee_id){
			return false;
		}	
		var url = FOOD_BEV_URL + '/service/' + this.get('label');
		var data = {
			employee_id: employee_id
		};
		
		$.ajax(url, {
			type: 'POST',
			data: data,
			success: function(response){
				if(typeof(callback) == 'function'){
					callback(response);
				}
			}
		});
	},

	reset: function(){
		this.set({
			'employee_id': null,
			'employee_first_name': null,
			'employee_last_name': null,
			'last_name': null,
			'table_name': ''
		});
	}
});

var TableLayoutObjectCollection = Backbone.Collection.extend({
	model: TableLayoutObject,
	
	initialize: function(options){
		
		if(options && options.layout){
			this.layout = options.layout;
		}

		var self = this;
		this.on('select-table', function(model){
			if(self.layout){
				self.layout.trigger('select-table', model);
			}
		});
	}
});

var TableLayoutCollection = Backbone.Collection.extend({
	
	url: FOOD_BEV_URL + '/table_layouts',
	model: TableLayout,
	
	initialize: function(){
		this.editing = false;
		var collection = this;
		
		this.listenTo(this, 'enable-edit', function(){
			collection.editing = true;		
		});
		
		this.listenTo(this, 'disable-edit', function(){
			collection.editing = false;	
		});
		
		this.listenTo(this, 'error', function(model, response){
			App.vent.trigger('notification', {msg: response.responseJSON.msg, type: 'error'});
		});				
	},

	getTableObject: function(params){
		
		var tableObj = false;
		if(this.models.length == 0){
			return tableObj;
		}

		_.each(this.models, function(layout){
			if(tableObj && tableObj != undefined){
				return false;
			}
			if(layout && layout.get('objects') && layout.get('objects').length > 0){
				tableObj = layout.get('objects').findWhere(params);
			}
		});

		return tableObj;
	}
});
