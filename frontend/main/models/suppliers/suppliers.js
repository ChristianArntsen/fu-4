var Supplier = Backbone.Model.extend({
	idAttribute: "person_id",
	
	defaults: {
		"person_id": "",
		"name": ""
	}
});

var SupplierCollection = Backbone.Collection.extend({
	
	url: API_URL + '/suppliers',
	model: Supplier,

	get_dropdown_data: function(){
		
		var data = {0: '- None -'};
		if(this.models.length == 0){
			return data;
		}

		_.each(this.models, function(model){
			data[model.get('person_id')] = model.get('company_name');
		});

		return data;
	}
});