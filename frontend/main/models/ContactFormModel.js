// Contact Form Model
// This describes a contact form and which fields are enabled.
// For the user-entered data,see ContactFormEntryModel.
var ContactFormModel = Backbone.Model.extend({
  url: function () {
    return API_URL + '/contact_forms' + (this.isNew() ? '' : '/' + this.id);
  },

  defaults: {
    name: '',

    has_first_name: false,
    has_last_name: false,
    has_email: false,
    has_phone_number: false,
    has_birthday: false,
    has_address: false,
    has_city: false,
    has_state: false,
    has_zip: false,
    has_message: false,

    req_first_name: false,
    req_last_name: false,
    req_email: false,
    req_phone_number: false,
    req_birthday: false,
    req_address: false,
    req_city: false,
    req_state: false,
    req_zip: false,
    req_message: false
  },

  parse: function (response) {
    return response.contact_form || response;
  }
});
