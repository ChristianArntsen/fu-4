var PassItemRuleConditionValue = Backbone.Model.extend({
	idAttribute: 'id',
	defaults: {
		label: '',
		field: false		
	},

	generateLabel: function(){
		if(this.get('field') == 'timeframe'){

			var day_of_week = this.get('day_of_week');
			var time = this.get('time');
			var date = this.get('date');

			var label = '';
			if(day_of_week && day_of_week.join('') != '1234567'){ 
				label += day_of_week.map(function(day){
					return moment(day, 'e').format('ddd');
				}).join(', ');
			}

			if(time && time[0] && time[1] && (time[0] != '0000' || time[1] != '2359')){ 
				label += ' ('+moment(time[0], 'HHmm').format('h:mma') +' to '+ moment(time[1], 'HHmm').format('h:mma') + ')';
			}

			if(date && date[0] && date[1]){ 
				label += ' ' + moment(date[0]).format('M/D/YY') +' to '+ moment(date[1]).format('M/D/YY');
			}

			if(label == ''){
				label = 'Anytime';
			}
			this.set('label', label);			
		}
	}
});

var PassItemRuleConditionValues = Backbone.Collection.extend({
	model: PassItemRuleConditionValue
});