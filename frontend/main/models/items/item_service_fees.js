var ItemServiceFee = Backbone.Model.extend({
	idAttribute: 'service_fee_id'
});

var ItemServiceFees = Backbone.Collection.extend({
	model: ItemServiceFee
});