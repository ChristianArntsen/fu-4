var PassItemRuleCondition = Backbone.Model.extend({
	
	defaults: {
		"field": false,
		"filter": false,
		"value": false,
		"operator": '='
	},

	initialize: function(){
		this.set_parameters();
		this.on('change:field', _.bind(this.set_parameters, this, true));
	},

	set_parameters: function(clear){
		
		if(clear === undefined){
			clear = false;
		}

		var value = this.get('value');
		var filter = this.get('filter');

		if(this.get('field') == 'timeframe' || this.get('field') == 'teesheet'){	
			if(clear){
				value = false;
			}
			this.set({
				'filter': false,
				'value': new PassItemRuleConditionValues(value)
			});

		}else if(this.get('field') == 'days_since_purchase'){
			if(clear){
				value = 0;
			}
			this.set({
				'filter': false,
				'value': value
			});

		}else if(this.get('field')){
			if(clear){
				value = 0;
				filter = false;
			}
			this.set({
				'filter': new PassItemRuleConditionValues(filter),
				'value': value
			});
		}
	}
});

var PassItemRuleConditions = Backbone.Collection.extend({
	model: PassItemRuleCondition
});