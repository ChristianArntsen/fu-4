var ItemCustomerGroup = Backbone.Model.extend({
	idAttribute: "group_id",
	defaults: {
		"label": "",
		"item_cost_plus_percent_discount": null
	}
});

var ItemCustomerGroups = Backbone.Collection.extend({
	model: ItemCustomerGroup
});