var Item = Backbone.Model.extend({
	idAttribute: "item_id",
	urlRoot: API_URL + '/items',

	defaults: {
		"name": "",
		"category": "",
		"subcategory": "",
		"department": "",
		"description": "",
		"item_number": "",
		"unit_price": 0.00,
		"parent_item_percent": 0.00,
		"whichever_is": 'more',
		"cost_price": 0.00,
		"quantity": 1,
		"total": 0.00,
		"subtotal": 0.00,
		"discount_percent": 0,
		"inventory_unlimited": true,
		"inventory_level": 0,
		"is_serialized": 0,
		"item_type": "item",
		"gl_code": "",
		"supplier_id": null,
		"inactive": 0,
		"max_discount": 100,
		"reorder_level": 0,
		"erange_size": 0,
		"taxes": [],
		"pass": false,
		"pass_price_class_id": false,
		"pass_days_to_expiration": 90,
		"pass_expiration_date": false,
		"pass_enroll_loyalty": false,
		"pass_multi_customer": 0,
		"pass_rules": [],
		"pass_restrictions": [],
		"upcs": [],
		"is_shared": 0,
		"unit_price_includes_tax": 0,
		"force_tax": null,
		"service_fee_id": null,
		"receipt_content_id": null
	},

	parse: function(response){
		var data = {};
		if(response && response.rows && response.rows[0]){
			data = response.rows[0];
		}else{
			data = response;
		}
		return data;
	},
	
	set: function(attributes, options){
		if(attributes.pass_restrictions !== undefined && !(attributes.pass_restrictions instanceof PassItemRules)){
			attributes.pass_restrictions = new PassItemRules( attributes.pass_restrictions );
		}
		if(attributes.upcs !== undefined && !(attributes.upcs instanceof ItemUPCCollection)){
			attributes.upcs = new ItemUPCCollection( attributes.upcs );
		}
		if(attributes.customer_groups !== undefined && !(attributes.customer_groups instanceof ItemCustomerGroups)){
			attributes.customer_groups = new ItemCustomerGroups( attributes.customer_groups );
		}
		if(attributes.service_fees !== undefined && !(attributes.service_fees instanceof ItemServiceFees)){
			attributes.service_fees = new ItemServiceFees( attributes.service_fees );
		}
		return Backbone.Model.prototype.set.call(this, attributes, options);
	},

	initialize: function(){
		if(App && App.data && App.data.course.get('default_taxes') && this.isNew()){
			var taxes = _.clone(App.data.course.get('default_taxes'));
			this.set({'taxes': taxes}, {'silent': true});
		}
		if(this.isNew()){
			this.set("customer_groups",new Backbone.Collection());
		}
	}
});

var ItemCollection = Backbone.Collection.extend({
	
	url: API_URL + '/items',
	model: Item,
	
	// Check if all of the items in the cart use loyalty points
	allUsingLoyalty: function(){
		
		var noLoyalty = this.findWhere({'loyalty_cost': false});
		
		if(noLoyalty.length > 0){
			return false;
		}else{ 
			return true;
		}
	},

    parse: function(response){
        if(response && response.rows) {
            return response.rows;
        }
        return response;
    }
});