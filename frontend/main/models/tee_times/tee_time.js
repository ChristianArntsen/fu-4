var Tee_time = Backbone.Model.extend({

	urlRoot: TEE_TIMES_URL,
	idAttribute: "TTID",
	defaults: {
		"TTID": null
	},

	// Over ride backbone save method to only send certain fields to be
	// saved to server (instead of entire object)
	save: function (attrs, options) {
		attrs = attrs || this.toJSON();
		options = options || {};

		// Filter attributes that will be saved to server
		attrs = _.pick(attrs, [			
			'first_name',
			'last_name'
		]);

		// Call super with attrs moved to options
		Backbone.Model.prototype.save.call(this, attrs, options);
	},

	sync: function(method, model, options){
		
		if(!options.attrs && method !== 'read'){
			options.attrs = this.attributes;
		}

		if(options.attrs){
			var attributes = _.clone(options.attrs);

            delete options.attrs;

			options.contentType = 'application/json';
      		options.data = JSON.stringify(attributes);			
		}

		Backbone.Model.prototype.sync.call(this, method, model, options);
	},

	set: function(attributes, options){

		return Backbone.Model.prototype.set.call(this, attributes, options);
	}
});

var TeeTimeCollection = Backbone.Collection.extend({
	url: TEE_TIMES_URL,
	model: Tee_time
});
