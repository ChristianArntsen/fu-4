var Raincheck = Backbone.Model.extend({
	
	idAttribute: "raincheck_id",
	defaults: {
		'expiry_date': false
	},

	isExpired: function(){
		if(!this.hasExpiration()){
			return false;
		}
		var expiration = moment(this.get('expiry_date'));
		
		if(expiration.isBefore()){
			return true;
		}
		return false;
	},

	hasExpiration: function(){
		if(!this.get('expiry_date') || this.get('expiry_date') == '0000-00-00 00:00:00'){
			return false;
		}
		return true;	
	}
});

var Raincheck_collection = Backbone.Collection.extend({
	model: Raincheck
});