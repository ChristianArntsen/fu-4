var TeesheetRecipient = Backbone.Model.extend({
	defaults: {
		label: ''
	}
});


var TeesheetRecipients = Backbone.Collection.extend({
	url: API_URL + '/teesheets/recipients',
	initialize: function(models, options) {
		this.start = options.start;
		this.end = options.end;
	},
	model: TeesheetRecipient
});
