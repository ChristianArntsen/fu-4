var MinimumCharge = Backbone.Model.extend({
	idAttribute: "minimum_charge_id"
});

var MinimumChargeCollection = Backbone.Collection.extend({
	model: MinimumCharge,

	get_dropdown_data: function(){
		var data = {};
		data[''] = '- Select Minimum Charge -';
		
		_.each(this.models, function(model){
			
			var name = model.get('name');
			if(model.get('is_active') == 0){
				name = '(INACTIVE) ' + name;
			}
			data[' ' +model.get('minimum_charge_id')] = name;
		});

		return data;
	}
});