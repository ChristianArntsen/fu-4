// Base receipt model with receipt utility functions
var Receipt = Backbone.Model.extend({
	
	defaults: {
		"receipt_header": '',
		"type": '',
        "receipt_type":'',
		"employee_name": '',
		"auto_gratuity_amount": 0
	},
	
	initialize: function(){
		
		var receipt_header = '';
		var date = this.get('date');
		if(!date){
			date = moment().format('YYYY-MM-DD HH:mm:ss');
		}	
		
		if(!this.has('employee')){
			this.set('employee', new Employee(App.data.user.attributes));
		}
		
		var employee_name = this.get('employee').get('last_name') +', '+ this.get('employee').get('first_name');
		if(App.data.course.get('hide_employee_last_name_receipt') == 1){
			employee_name = this.get('employee').get('first_name');
		}
		this.set('employee_name', employee_name);
		
		this.applet = document.getElementById('qz');
		
		// If course uses Star Web Print printers
		if(App.data.course.get_setting('webprnt') == 1){
			this.set('type', 'network');
			var builder = new StarWebPrintBuilder();
			
			receipt_header += builder.createAlignmentElement({position:'center'});
			receipt_header += builder.createTextElement({width:1, height: 1, data: App.data.course.get('name') + "\n"});
			receipt_header += builder.createTextElement({data: App.data.course.get('address') + "\n"});
			if(App.data.course.get('address_2') != ''){
				receipt_header += builder.createTextElement({data: App.data.course.get('city') + ', ' + App.data.course.get('state') +' '+ App.data.course.get('zip') + "\n"});
			}
			receipt_header += builder.createTextElement({data: App.data.course.get('phone') + "\n\n"});
			receipt_header += builder.createAlignmentElement({position: 'left'});	
		
		// If course uses USB/Serial printers
		}else{
			this.set('type', 'serial');
			// Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
			receipt_header += (chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
			receipt_header += (chr(27) + chr(64));				// Resets the printer
			receipt_header += (chr(29) + chr(33) + chr(16)); 	// Font-size x2
			receipt_header += (chr(27) + "\x61" + "\x31"); 		// Center justify
			
			// Course info header
			receipt_header += (App.data.course.get('name') + "\n");
			receipt_header += (chr(27) + chr(64));				// Resets the printer
			receipt_header += (chr(27) + "\x61" + "\x31"); 		// Center justify
			receipt_header += (chr(27) + chr(77) + chr(48)); 	// Font - Normal font
			receipt_header += (App.data.course.get('address') + "\n");
			receipt_header += (App.data.course.get('city') + ', ' + App.data.course.get('state') +' '+ App.data.course.get('zip') + "\n");
			receipt_header += (App.data.course.get('phone') + "\n\n");
			receipt_header += (chr(27) + chr(97) + chr(48));		// Left justify
			receipt_header += ("Date: " + moment(date).format('M/D/YYYY h:mma') + "\n");		
			receipt_header += ("Employee: " + employee_name + "\n");			
		}
			
		this.set('receipt_header', receipt_header);					
	},
	
	barcode: function(data){	            
		
		var chr = this.chr;
		var barcode = '';
		
		if(!data){
			return barcode;
		}
		
		if(this.get('type') == 'serial'){
			
			barcode += chr(27)+chr(97)+chr(49);
			barcode += "\x1Dh" +chr(80);  // ← Set height
			barcode += "\n\n";// Some spacing at the bottom
			
			var str = String(data);
			var len = str.length;
			
			if(len == 0){
				return barcode;
			}
			
			if (len == 1)
				barcode += "\x1D\x77\x02\x1D\x6B\x49\x08\x7B\x41\x50\x4F\x53\x20"+str;			
			else if (len == 2)
				barcode += "\x1D\x77\x02\x1D\x6B\x49\x09\x7B\x41\x50\x4F\x53\x20"+str;			
			else if (len == 3)
				barcode += "\x1D\x77\x02\x1D\x6B\x49\x0A\x7B\x41\x50\x4F\x53\x20"+str;
			else if (len == 4)
				barcode += "\x1D\x88\x01\x1D\x6B\x49\x0B\x7B\x41\x50\x4F\x53\x20"+str;
			else if (len == 5)
				barcode += "\x1D\x77\x02\x1D\x6B\x49\x0C\x7B\x41\x50\x4F\x53\x20"+str;
			else if (len == 6)
				barcode += "\x1D\x77\x02\x1D\x6B\x49\x0D\x7B\x41\x50\x4F\x53\x20"+str;
			else if (len == 7)
				barcode += "\x1D\x77\x02\x1D\x6B\x49\x0E\x7B\x41\x50\x4F\x53\x20"+str;
			else if (len == 8)
				barcode += "\x1D\x77\x02\x1D\x6B\x49\x0F\x7B\x41\x50\x4F\x53\x20"+str;
			else if (len == 9)
				barcode += "\x1D\x77\x02\x1D\x6B\x49\x10\x7B\x41\x50\x4F\x53\x20"+str;
			else if (len == 10)
				barcode += "\x1D\x77\x02\x1D\x6B\x49\x11\x7B\x41\x50\x4F\x53\x20"+str;
		}
		
		return barcode;
	},

	chr: function(i) {
		return String.fromCharCode(i);
	},
	
	add_white_space: function(str_one, str_two, width){
		
		var width = width == undefined ? 42 : width;
		var strlen_one = str_one.length;
		var strlen_two = str_two.length;
		var white_space = '';
		var white_space_length = 0;
		
		//  Truncate text with ... if longer than available space
		if (strlen_one + strlen_two >= width){
			return (str_one.substr(0, width - strlen_two - 4)+'... '+str_two); 
		}else{
			white_space_length = width - (strlen_one + strlen_two);
		}
			
		for (var i = 0; i < white_space_length; i++){
			white_space += ' ';
		}
		
		return str_one+white_space+str_two;
	},

    add_white_space_serial: function (str_one, str_two, width){
        var width = width == undefined ? 38 : width;
        return this.add_white_space(str_one, str_two, width);
    },

	print: function(receipt_type){
        if (receipt_type == undefined) {
            this.set('receipt_type', '');
        }
        else {
            this.set('receipt_type', receipt_type);
        }
		// If course is using Star web printers
		if(App.data.course.get_setting('webprnt') == 1){
			this.print_network();
		
		// If course is using USB/Serial printers
		}else{
			var receipt = this;	
			try {	
				if(receipt.applet){
					receipt.applet.findPrinter("foreup");

					setTimeout(function(){
						receipt.print_serial(receipt.applet);
					}, 1);
				}
			
			}catch(err){
				App.vent.trigger('notification', {'msg':'Unable to print receipt at this time. Please print manually.', 'type':'error'});
				return false;
			}	
		}
	},
	
	open_cash_drawer: function(employee_id){
		
		var drawer_number = false;
		
		// If an employee_id is passed, check which cash drawer 
		// they are using to open the correct drawer
		if(employee_id != undefined && App.data.course.get_setting('multi_cash_drawers') == 1){
			
			var active_log = App.data.register_logs.get_open_log(undefined, true);
			if(active_log){
				drawer_number = active_log.get('drawer_number');		
			}

			if(!drawer_number){
				App.vent.trigger('notification', {
					'msg': 'Unable to open cash drawer. It is in use by another employee',
					'type': 'error'
				});
				return false;
			}
		}
		
		// If course is using Star web printers
		if(App.data.course.get_setting('webprnt') == 1){
			
			var builder = new StarWebPrintBuilder();
			
			if(!drawer_number){
				var data = builder.createRawDataElement({data:chr(7)});
			}else if(drawer_number == 1){
				var data = builder.createPeripheralElement({channel:1, on:200, off:200});
			}else if(drawer_number == 2){
				var data = builder.createPeripheralElement({channel:2, on:200, off:200});
			}

			App.data.print_queue.add_receipt(App.data.course.get_setting('receipt_ip'), data);
			App.data.print_queue.print();

			console.debug('CASH DRAWER OPEN:', drawer_number);
		
		// If course is using USB/Serial printers
		}else{
			var receipt = this;	
			
			if(receipt.applet){
				try {
					receipt.applet.findPrinter("foreup");
					receipt.applet.append(chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
					
					setTimeout(function(){
						receipt.applet.print()
					}, 1);
					
				}catch(err){
					return false;
				}			
			}
		}		
	}
});

var SalesReceipt = Receipt.extend({
	
	defaults: {
		"sale_id": false,
		"customer": false,
		"items": [],
		"payments": [],
		"total": 0.00,
		"subtotal": 0.00,
		"tax": 0.00,
		"taxes": [],
		"total_due": 0.00,
		"loyalty_points_earned": 0,
		"loyalty_points_spent": 0,
		"auto_gratuity": 0,
		"auto_gratuity_amount": 0,
		"extra_content": {},
		"extra_receipts": {}
	},

	initialize: function(){
		Receipt.prototype.initialize.apply(this);
		this.builder = new StarWebPrintBuilder();
	},
	
	print_serial: function(applet){
		
		var chr = this.chr;
		var add_white_space = this.add_white_space_serial;
		
		var payments = new PaymentCollection(this.get('payments'));
		var items = this.get('items');
		var taxes = this.get('taxes');
		var receipt_data = '';
		var total = this.get('total');
		var subtotal = this.get('subtotal');
		var total_due = this.get('total_due');
		var sale_id = this.get('sale_id');
		var customer = this.get('customer');
		var date = this.get('date');
		var receipt = this;
		var has_credit_card_payments = false;
		var receipt_data = '';
		
		// Course info header
		receipt_data += receipt.get('receipt_header');
		
		if(customer){
			receipt_data += "Customer: " + customer.get('last_name') +', '+ customer.get('first_name');
		}
		receipt_data += "\n\n";

		// Items
		_.each(items, function(item){

			receipt_data += add_white_space(item.get('name') +' ', accounting.formatNumber(item.get('quantity'),2) +' @ '+ accounting.formatMoney(_.round(item.get('unit_price')), '', 2) +'    ' + accounting.formatMoney(item.get('non_discount_subtotal')))+ '\n';
			if(item.get('discount_percent') > 0){
				receipt_data += add_white_space('     ' +accounting.formatNumber(item.get('discount_percent'))+ '% discount', '-' +accounting.formatMoney(item.get('discount_amount'))) + '\n';
			}
		});
		
		// Totals
		receipt_data += '\n\n';
		receipt_data += add_white_space('Subtotal: ', accounting.formatMoney(subtotal)) +'\n';
		
		_.each(taxes, function(tax){
			receipt_data += add_white_space(accounting.formatNumber(tax.percent, 3) +'% '+ tax.name +': ', accounting.formatMoney(tax.amount)) +'\n';
		});
		receipt_data += add_white_space('Total: ', accounting.formatMoney(total)) +'\n';
		
		// Payments
		receipt_data += '\nPayments:\n';
        var receipt_type = this.get('receipt_type');
        show_tip_line = false;
		if(payments.length > 0){
			_.each(payments.models, function(payment){
				
				if(payment.get('type') == 'credit_card'){
					has_credit_card_payments = true;					
				}
				
				if(payment.get('type') == 'credit_card' || payment.get('type') == 'gift_card' || payment.get('type') == 'customer_account' || payment.get('type') == 'member_account'){
					show_tip_line = true;
				}				
				receipt_data += add_white_space(payment.get('description') +': ', accounting.formatMoney(payment.get('amount'))) +'\n';
			});
		}
		
		if(total_due < 0){
			receipt_data += '\n'+ add_white_space('Amount Due: ', accounting.formatMoney(total_due)) +'\n';
		}
		
		// If course wants tip line on receipt
		if((receipt_type == 'suspended_sale' || show_tip_line) && App.data.course.get_setting('print_tip_line') == 1){
			receipt_data += '\n\n'+add_white_space('Tip: ', SETTINGS.currency_symbol+'________.____');
			receipt_data += '\n\n'+add_white_space('TOTAL CHARGE: ', SETTINGS.currency_symbol+'________.____');
			receipt_data += '\n\n\nX_____________________________________________\n';
		}
		//receipt_data += chr(27) + chr(97) + chr(49);
		
		var gift_cards = payments.where({type:'gift_card'});
		var punch_cards = payments.where({type:'punch_card'});

		if(gift_cards.length > 0 || punch_cards.length > 0){
			receipt_data += "\n\n";		
		}
		
		// If a gift card was used, show remaining balance
		if (gift_cards.length > 0){
			_.each(gift_cards, function(gift_card){
				if(!gift_card.get('gift_card')){
					return true;
				}
				var ending_balance = _.round(gift_card.get('gift_card').value - gift_card.get('amount'));
				receipt_data += gift_card.get('description')+ " ending balance " +accounting.formatMoney(ending_balance) + "\n";		
			});
		}
		
		// If a punch card was used, show punches left
		if (punch_cards.length > 0){
			_.each(punch_cards, function(punch_card){
				if(!punch_card.get('punch_card') || !punch_card.get('punch_card').items){
					return true;
				}
				
				receipt_data += punch_card.get('description')+ " - Punches used\n";
				_.each(punch_card.get('punch_card').items, function(item){
					receipt_data += "  " + add_white_space(item.used + '/' + item.punches +' '+item.name, ' ')+ '\n';
				});
				receipt_data += '\n';
			});
		}		
		
		if(App.data.course.get('use_loyalty') == '1' && customer && this.get('loyalty_points') && this.get('loyalty_points') > 0){
			receipt_data += "\n\nYou earned " + this.get('loyalty_points') + ' loyalty points\n';
			receipt_data += "Your loyalty point balance is " + this.get('customer').get('loyalty_points') + '\n';
		}		

		if(App.data.course.get_setting('return_policy') != ''){
			receipt_data += "\n\n";
			receipt_data += App.data.course.get_setting('return_policy');
		}
		receipt_data += "\n\n";			
		
		// If sale number is present, add barcode to receipt
		if(this.has('number')){
			receipt_data += this.barcode('POS '+this.get('number'));
			receipt_data += '\n\nSale ID: POS ' +this.get('number')+'\n';
		}
		
		receipt_data += "\n\n\n\n\n\n";
		receipt_data += chr(27) + chr(105); // Cuts receipt
		
		// Double the receipt data if course is set to print two
		if(
			(!has_credit_card_payments && App.data.course.get_setting('non_credit_card_receipt_count') > 1) ||
			(has_credit_card_payments && App.data.course.get_setting('credit_card_receipt_count') > 1)
		){
			receipt_data += receipt_data;
		}		
		
		// Send characters/raw commands to printer
		applet.append(receipt_data);
		applet.print();
		
		return true;
	},
	
	get_modifiers: function(modifiers, quantity, splits, spacing){
		
		if(!splits || splits == undefined){
			splits = 1;
		}

		if(!modifiers || modifiers.length == 0){
			return null;
		}
		var data = '';
		var builder = this.builder;

		var space = '';
		if(spacing && spacing > 0){
			for(var i = 0; i < spacing; i++){
				space += ' ';
			}
		}

		_.each(modifiers, function(modifier){
			if(!modifier || !modifier.selected_option || modifier.selected_option == ''){
				return false;
			}

			var modifier_price = new Decimal(0);
			if(modifier.selected_price){
				modifier_price = new Decimal(modifier.selected_price).times(quantity);
			}
			if(splits > 1){
				modifier_price = modifier_price.dividedBy(splits);
			}
			modifier_price = modifier_price.toDP(2).toNumber();
			data += builder.createTextElement({width:1, data: add_white_space(space + '   - ' + modifier.name +': '+modifier.selected_option, accounting.formatMoney(modifier_price))+'\n'});
		});		

		return data;
	},	

	add_extra_content: function(receipt_content){

		if(!receipt_content || receipt_content.content == ''){
			return false;
		}
		var data = '';
		if(receipt_content.separate_receipt){
			data += this.get('receipt_header');	
		}

		data += this.builder.createTextElement({
			data: '\n\n' + receipt_content.content
		});
		
		if(receipt_content.signature_line){
			data += this.builder.createTextElement({
				data: '\n\n\n\nX_____________________________________________\n\n'
			});
		}

		data += this.builder.createTextElement({
			data: '\n\n'
		});

		if(receipt_content.separate_receipt){
			data += this.builder.createTextElement({
				data: '\n\n'
			});

			data += this.builder.createCutPaperElement({feed: true});
			var extra_receipts = this.get('extra_receipts');
			extra_receipts[receipt_content.receipt_content_id] = data;
			this.set('extra_receipts', extra_receipts);

		}else{
			var extra_content = this.get('extra_content');
			extra_content[receipt_content.receipt_content_id] = data;
			this.set('extra_content', extra_content);
		}

		return true;
	},

	print_network: function(){
		
		var add_white_space = this.add_white_space;
		var payments = new PaymentCollection(this.get('payments'));
		var items = this.get('items');
		var taxes = this.get('taxes');
		var receipt_data = '';
		var total = this.get('total');
		var subtotal = this.get('subtotal');
		var total_due = this.get('total_due');
		var sale_id = this.get('sale_id');
		var customer = this.get('customer');
		var date = this.get('date');
		var has_credit_card_payments = false;
		var builder = this.builder;
		var receipt_data = '';
		var receipt = this;

		this.set({
			'extra_receipts': {},
			'extra_content': {}
		});
		
		// Course info header
		receipt_data += this.get('receipt_header');	
		receipt_data += builder.createTextElement({data: "Date: "+ moment(date).format('M/D/YYYY h:mma') + "\n"});
		receipt_data += builder.createTextElement({data: "Employee: "+ this.get('employee_name') + "\n"});		
		
		if(customer){
			receipt_data += builder.createTextElement({data: "Customer: " + customer.get('last_name') +', '+ customer.get('first_name')});
            if(customer.get('account_number') != null && customer.get('account_number') != ''){
                receipt_data += builder.createTextElement({data: "\nAccount #: " + customer.get('account_number')});
            }
		}
		receipt_data += builder.createTextElement({data:"\n\n"});
		var extra_content = '';
		
		if(App.data.course.get_setting('print_tee_time_details') == 1 && this.get('teetime')){
			var teetime_date = moment(this.get('teetime').start_date).format('M/D/YYYY h:mma')
			receipt_data += builder.createTextElement({data: "Reservation for "+ this.get('teetime').teesheet_title +'\n' +
				'at '+ teetime_date + '\n\n'});
		}

	
		var show_member_balance = payments.findWhere({type:'member_account'});
		var show_account_balance = payments.findWhere({type:'customer_account'});

		// Items
		if(items.length > 0){
			_.each(items, function(item){
				
				receipt.add_extra_content(item.get('receipt_content'));

				var subtotal = item.get('non_discount_subtotal');
				if(item.get('modifier_total') > 0){
					subtotal -= item.get('modifier_total');
				}

				var name = item.get('name');
				if(item.get('number_splits') > 1){
					name = '1/'+item.get('number_splits') + ' ' + name;
				}

				receipt_data += builder.createTextElement({
                    height: 1,
					data: add_white_space(name +' ', accounting.formatNumber(item.get('quantity'), 2) +' @ '+ item.get('unit_price') +'    ' +accounting.formatMoney(subtotal))+ '\n'
				});
				
				if(item.has('modifiers') && item.get('modifiers') && item.get('modifiers').length > 0){
					receipt_data += receipt.get_modifiers(item.get('modifiers'), item.get('quantity'), item.get('number_splits'));
				}

				if(item.has('items') && item.get('items') && item.get('items').length > 0){
					_.each(item.get('items'), function(item){
						receipt_data += builder.createTextElement({
							data: '  (' + item.quantity + ') ' + item.name+ '\n'
						});
						if(item.service_fees){
							_.each(item.service_fees, function(service_fee){
								receipt_data += builder.createTextElement({width:1,data: add_white_space('    ' +service_fee.name+ ' ', accounting.formatMoney(service_fee.subtotal, ''))+'\n'});
							});
						}
					});
					receipt_data += receipt.get_modifiers(item.get('modifiers'), item.get('quantity'));
				}				

				if(item.get('discount_percent') > 0){
					receipt_data += builder.createTextElement({
						data: add_white_space('     ' +accounting.formatNumber(item.get('discount_percent'))+ '% discount', '-' +accounting.formatMoney(item.get('discount_amount')))+ '\n'
					});
				}

				if(item.has('erange_code') && item.get('erange_code') != 0){
					receipt_data += builder.createTextElement({
                        height: 2,
						data: add_white_space('     E-Range Code:', item.get('erange_code'))+ '\n'
					});
				}

				if(item.get('item_type') == 'member_account'){
					show_member_balance = true;
				}
				if(item.get('item_type') == 'customer_account'){
					show_account_balance = true;
				}
			});
		}

		// Totals
		receipt_data += builder.createTextElement({
            height: 1,
			data: '\n\n' + add_white_space('Subtotal: ', accounting.formatMoney(subtotal)) +'\n'
		});

		if(receipt.get('auto_gratuity') && receipt.get('auto_gratuity') > 0 && receipt.get('auto_gratuity_amount') > 0){
			receipt_data += builder.createTextElement({
				data: add_white_space(accounting.formatNumber(receipt.get('auto_gratuity'), 2) + 
					'% Gratuity:', accounting.formatMoney(receipt.get('auto_gratuity_amount'))) + '\n'
			});
		}

		if(taxes.length > 0){
			_.each(taxes, function(tax){
				receipt_data += builder.createTextElement({
					data: add_white_space(accounting.formatNumber(tax.percent, 3) +'% '+ tax.name +': ', accounting.formatMoney(tax.amount)) +'\n'
				});
			});
		}

		receipt_data += builder.createTextElement({
			data: add_white_space('Total: ', accounting.formatMoney(total + this.get('auto_gratuity_amount'))) +'\n'
		});
		
		// Payments
        var receipt_type = this.get('receipt_type');
        var show_tip_line = false;
		if(payments.length > 0){
			receipt_data += builder.createTextElement({
				data: '\nPayments:\n'
			});
			
			_.each(payments.models, function(payment, index){
				
				if(payment.get('is_auto_gratuity') == 1){
					return false;
				}

				if(payment.get('type') == 'credit_card'){
					has_credit_card_payments = true;
				}
				if(payment.get('type') == 'credit_card' || payment.get('type') == 'gift_card' || payment.get('type') == 'customer_account' || payment.get('type') == 'member_account'){
					show_tip_line = true;
				}
				var payment_amount = payment.get('amount');
				if(index == 0){
					payment_amount += receipt.get('auto_gratuity_amount');
				}
				receipt_data += builder.createTextElement({
					data: add_white_space(payment.get('description') +': ', accounting.formatMoney(payment_amount)) +'\n'
				});
				if(payment.get('invoice_id')){
                    receipt_data += builder.createTextElement({
                        data: add_white_space('Invoice: ', payment.get('invoice_id')) +'\n'
                    });
				}
                if(payment.get('record_id')){
                    receipt_data += builder.createTextElement({
                        data: add_white_space('Record: ', payment.get('record_id')) +'\n'
                    });
                }
			});
		}
		
		if(total_due < 0){
			receipt_data += builder.createTextElement({
				data: '\n'+ add_white_space('Amount Due: ', accounting.formatMoney(total_due)) +'\n'
			});
		}
		
		// If course wants tip line on receipt
		if((receipt_type == 'suspended_sale' || show_tip_line) && App.data.course.get_setting('print_tip_line') == 1){
			receipt_data += builder.createTextElement({
				data: '\n\n' + add_white_space('Tip: ', SETTINGS.currency_symbol+'________.____')
			});
			receipt_data += builder.createTextElement({
				data: '\n\n' + add_white_space('TOTAL CHARGE: ', SETTINGS.currency_symbol+'________.____')
			});
			receipt_data += builder.createTextElement({
				data: '\n\n\nX_____________________________________________\n'
			});
		}
		
		// Add remaining card balances to bottom of receipt
		var gift_cards = payments.where({type:'gift_card'});
		var punch_cards = payments.where({type:'punch_card'});
		
		// If member payment was used, show remaining balance
		if (App.data.course.get_setting('receipt_print_account_balance') == 1 && 
			show_member_balance && 
			customer
		){
			receipt_data += builder.createTextElement({
				data: '\n' + App.data.course.get('member_balance_nickname') + " ending balance: " +accounting.formatMoney(customer.get('member_account_balance')) + "\n"	
			});
		}

		// If customer account payment was used, show remaining balance
		if (App.data.course.get_setting('receipt_print_account_balance') == 1 && 
			show_account_balance && 
			customer
		){
			receipt_data += builder.createTextElement({
				data: '\n' + App.data.course.get('customer_credit_nickname') + " ending balance: " +accounting.formatMoney(customer.get('account_balance')) + "\n"		
			});
		}

		// If customer account payment was used, show remaining balance
		if (App.data.course.get_setting('print_minimums_on_receipt') == 1 &&
			customer &&
			customer.get('minimum_charges') &&
			customer.get('minimum_charges').length > 0
		){

			receipt_data += builder.createTextElement({
				data: "\n\nPrevious F&B Minimum Balance\n------------------\n"
			});
			_.forEach(customer.get('minimum_charges').models,function(minimum){

				receipt_data += builder.createTextElement({
					data: minimum.get('name') +": "+accounting.formatMoney(minimum.get('total'))+"/"+accounting.formatMoney(minimum.get('minimum_amount')) + "\n"
				});
			});
		}

		if(gift_cards.length > 0 || punch_cards.length > 0){
			receipt_data += builder.createTextElement({
				data: "\n\n"
			});			
		}

		if (gift_cards.length > 0){
			_.each(gift_cards, function(gift_card){
				if(!gift_card.get('gift_card')){
					return true;
				}
				var ending_balance = _.round(gift_card.get('gift_card').value - gift_card.get('amount'));
				receipt_data += builder.createTextElement({
					data: gift_card.get('description')+ " ending balance " +accounting.formatMoney(ending_balance) + "\n"
				});				
			});
		}
		
		if (punch_cards.length > 0){
			_.each(punch_cards, function(punch_card){
				if(!punch_card.get('punch_card') || !punch_card.get('punch_card').items){
					return true;
				}
				
				receipt_data += builder.createTextElement({
					data: punch_card.get('description')+ " - Punches used\n"
				});
				
				_.each(punch_card.get('punch_card').items, function(item){
					receipt_data += builder.createTextElement({
						data: "  " + add_white_space(item.used + '/' + item.punches +' '+item.name, ' ')+ '\n'
					});
				});
				
				receipt_data += builder.createTextElement({
					data: "\n"
				});		
			});
		}				
		
		if(
			App.data.course.get('use_loyalty') == '1' &&
			customer && 
			(this.get('loyalty_points_earned') > 0 || this.get('loyalty_points_spent') > 0)
		){
			receipt_data += builder.createTextElement({
				data: "\n\nLoyalty Points\n------------------\n"
			});			
			
			if(this.get('loyalty_points_earned') > 0){
				receipt_data += builder.createTextElement({
					data: "Earned: " + this.get('loyalty_points_earned') + ' points\n'
				});				
			}
			
			if(this.get('loyalty_points_spent') > 0){
				receipt_data += builder.createTextElement({
					data: "Spent: " + this.get('loyalty_points_spent') + ' points\n'
				});				
			}

			receipt_data += builder.createTextElement({
				data: "Current Balance: " + this.get('customer').get('loyalty_points') + ' points\n'
			});
		}
		
		if(this.get('customer_note')){
			receipt_data += builder.createTextElement({
				data: '\n\nCUSTOMER NOTE\n' + this.get('customer_note') + '\n'
			});			
		}

		if(this.get('extra_content') && !_.isEmpty(this.get('extra_content'))){
			_.each(this.get('extra_content'), function(extra_content){
				receipt_data += extra_content;
			});
		}

		if(App.data.course.get_setting('return_policy') != ''){
			receipt_data += builder.createTextElement({
				data: '\n\n' + App.data.course.get_setting('return_policy')
			});
		}		
		
		// Add barcode/sale number to bottom
		if(this.get('number')){
			receipt_data += builder.createAlignmentElement({position:'center'});
			receipt_data += builder.createTextElement({data: "\n\n\n\n"});
			receipt_data += builder.createBarcodeElement({symbology:'Code128', width:'width2', height:80, hri:false, data: 'POS '+this.get('number')});
			receipt_data += builder.createTextElement({data:'\nSale ID: POS '+this.get('number')+ '\n'});
		}		
		
		// Extra spacing at bottom
		receipt_data += builder.createTextElement({
			data: "\n\n"
		});
		
		// Cut receipt
		receipt_data += builder.createCutPaperElement({feed: true});

		// Double the receipt data if course is set to print two
		if(
			(!has_credit_card_payments && App.data.course.get_setting('non_credit_card_receipt_count') > 1) ||
			(has_credit_card_payments && App.data.course.get_setting('credit_card_receipt_count') > 1)
		){
			receipt_data += receipt_data;
		}

		// Attach any extra receipts (content from items in sale, cart agreements, etc)
		if(this.get('extra_receipts') && !_.isEmpty(this.get('extra_receipts'))){
			_.each(this.get('extra_receipts'), function(extra_receipt){
				receipt_data += extra_receipt;
			});
		}
		
		// Send receipt to printer
		App.data.print_queue.add_receipt(App.data.course.get_setting('receipt_ip'), receipt_data);
		App.data.print_queue.print();
	}
});

var RaincheckReceipt = Receipt.extend({
	
	defaults: {
		"raincheck_number": false,
		"customer": false,
		"green_fee_price": 0.00,
		"cart_fee_price": 0.00,
		"subtotal": 0.00,
		"tax": 0.00,
		"total": 0.00,
		"players": 1
	},

	initialize: function(){
		Receipt.prototype.initialize.apply(this);
		if(this.get('customer')){
			this.set('customer', new Customer(this.get('customer')));
		}
	},
	
	print_serial: function(applet){
		
		var chr = this.chr;
		var add_white_space = this.add_white_space;
				
		// Course info header
		applet.append(this.get('receipt_header'));
		
		applet.append('\nRaincheck Credit\n');
		
		if(this.get('customer')){
			applet.append("Customer: " + this.get('customer').get('last_name') +', '+ this.get('customer').get('first_name') + "\n\n");
		}
		applet.append('\n\n' + add_white_space('Green Fee: ', accounting.formatMoney(this.get('green_fee_price'))) +'\n');
		applet.append(add_white_space('Cart Fee: ', accounting.formatMoney(this.get('cart_fee_price'))) +'\n');
		applet.append('\n' + add_white_space('Players: ','x '+ this.get('players')) +'\n');
		applet.append(add_white_space('Subtotal: ', accounting.formatMoney(this.get('subtotal'))) +'\n');
		applet.append('\n' + add_white_space('Tax: ', accounting.formatMoney(this.get('tax'))) +'\n');
		applet.append('\n' + add_white_space('Total Credit: ', accounting.formatMoney(this.get('total'))) +'\n');
		
		var raincheck_id = 'RID ' + this.get('raincheck_number');
		applet.append(this.barcode(raincheck_id));	
		applet.append('\n\nRaincheck ID: '+ raincheck_id +'\n');
		
		applet.append("\n\n\n\n\n\n");			// Some spacing at the bottom
		applet.append(chr(27) + chr(105)); 		// Cuts the receipt		
		
		applet.print();	
	},
	
	print_network: function(){
		
		var add_white_space = this.add_white_space;
		var receipt_data = '';
		var builder = new StarWebPrintBuilder();
		var customer = this.get('customer');
		
		// Course info header
		receipt_data += this.get('receipt_header');
		receipt_data += builder.createTextElement({data: "Date: "+ moment().format('M/D/YYYY h:mma') + "\n"});
		receipt_data += builder.createTextElement({data: "Employee: "+ this.get('employee_name') + "\n"});			
		
		receipt_data += builder.createTextElement({data:'\nRaincheck Credit\n'});
		if(customer){
            receipt_data += builder.createTextElement({data: "Customer: " + customer.get('last_name') +', '+ customer.get('first_name')});
            if (customer.get('account_number') != null && customer.get('account_number') != '')
            {
                receipt_data += builder.createTextElement({data: "\nAccount #: " + customer.get('account_number')});
            }
        }
        receipt_data += builder.createTextElement({data:"\n\n"});

        receipt_data += builder.createTextElement({data: '\n\n' +add_white_space('Green Fee: ', accounting.formatMoney(this.get('green_fee_price')))+'\n'});
		receipt_data += builder.createTextElement({data: add_white_space('Cart Fee: ', accounting.formatMoney(this.get('cart_fee_price')))+'\n'});
		receipt_data += builder.createTextElement({data: '\n' +add_white_space('Players: ','x '+ this.get('players')) +'\n'});
		receipt_data += builder.createTextElement({data: add_white_space('Subtotal: ', accounting.formatMoney(this.get('subtotal')))+'\n'});
		receipt_data += builder.createTextElement({data: '\n' +add_white_space('Tax: ', accounting.formatMoney(this.get('tax')))+'\n'});
		receipt_data += builder.createTextElement({data: '\n' +add_white_space('Total Credit: ', accounting.formatMoney(this.get('total')))+'\n'});
		
		// Add barcode to bottom
		if(this.get('raincheck_number')){
			var raincheck_id = 'RID ' + this.get('raincheck_number');
			receipt_data += builder.createAlignmentElement({position:'center'});
			receipt_data += builder.createTextElement({data: "\n\n"});
			receipt_data += builder.createBarcodeElement({symbology:'Code128', width:'width2', height:80, hri:false, data: raincheck_id});
			receipt_data += builder.createTextElement({data:'\nRaincheck ID: '+ raincheck_id + '\n'});
		}			
		
		// Extra spacing at bottom
		receipt_data += builder.createTextElement({
			data: "\n\n"
		});
		
		// Cut receipt
		receipt_data += builder.createCutPaperElement({feed: true});
		
		// Send receipt to printer
		App.data.print_queue.add_receipt(App.data.course.get_setting('receipt_ip'), receipt_data);
		App.data.print_queue.print();								
	}
});

var CreditCardReceipt = Receipt.extend({
	
	defaults: {
		"card_type": '',
		"card_number": '',
		"auth_code": '',
		"amount": 0.00,
		"cardholder_name": '',
		"transaction_type": 'Sale',
		"input_method": 'Swipe',
		"sale_number": false,
		"check_number": false,
		"table_number": false,
        "merchant_data": ''
	},

	initialize: function(){
		Receipt.prototype.initialize.apply(this);
	},
	
	print_serial: function(applet){
		
		var chr = this.chr;
		var add_white_space = this.add_white_space;
		var receipt_data = ''
		var final_receipt_data = '';
		var merchant_data = this.get('merchant_data');
		// Course info header
		receipt_data += this.get('receipt_header');
		
		receipt_data += 'Date: '+moment().format('M/D/YYYY h:mma')+'\n';
		if(this.get('customer')){
			receipt_data += "Customer: " + this.get('customer').get('last_name') +', '+ this.get('customer').get('first_name') + "\n\n";
		}
		receipt_data += 'Method: '+this.get('input_method')+ '\n';
		receipt_data += 'Type: '+this.get('transaction_type')+ '\n';
		
		if(this.has('invoice_id')){
			receipt_data += 'Ref: ' +this.get('invoice_id')+ '\n';	
		}
		receipt_data += 'Card Type: ' +this.get('card_type')+ '\n';
		receipt_data += 'Card Number: ' +this.get('card_number')+ '\n';
		receipt_data += 'Auth: '+ this.get('auth_code') +'\n\n';
		receipt_data += 'Amount: '+ accounting.formatMoney(this.get('amount')) +'\n\n';
		
		if (App.data.course.get_setting('print_tip_line') == 1) {
			receipt_data += '\n' +add_white_space('Tip: ',SETTINGS.currency_symbol+'________.____');
			receipt_data += '\n\n' +add_white_space('TOTAL CHARGE: ',SETTINGS.currency_symbol+'________.____');
		}
						        
		receipt_data += chr(27) + chr(97) + chr(49);
		receipt_data += '\n\nI agree to pay the above amount according to the card issuer agreement.\n\n\n';
		receipt_data += 'X_____________________________________________\n';
		receipt_data += '   ' +this.get('cardholder_name');
		receipt_data += '\n\n';
		
		final_receipt_data += receipt_data;
		
		if(App.data.course.get_setting('signature_slip_count') == 2){
			final_receipt_data += '\n\n**********************************************\nCustomer Copy\n**********************************************\n';
			
			final_receipt_data += "\n\n\n\n";			// Some spacing at the bottom
			final_receipt_data += chr(27) + chr(105); 		// Cuts the receipt	
			
			final_receipt_data += receipt_data;
			final_receipt_data += '\n\n**********************************************\nMerchant Copy\n**********************************************\n';				
		}
		
		final_receipt_data += "\n\n\n\n";			// Some spacing at the bottom
		final_receipt_data += chr(27) + chr(105); 		// Cuts the receipt						
		
		applet.append(final_receipt_data);
	},
	
	print_network: function(){
		
		var add_white_space = this.add_white_space;
		var receipt_data = '';
		var builder = new StarWebPrintBuilder();
		var final_receipt_data = '';
        var merchant_data = this.get('merchant_data');
        // Course info header
		receipt_data += this.get('receipt_header');
		receipt_data += builder.createTextElement({width:1,data:'Date: '+moment().format('M/D/YYYY h:mma')+'\n'});
		
		if(this.get('sale_number')){
			receipt_data += builder.createTextElement({width:1,data:'Sale ID: '+this.get('sale_number')+'\n'});
		}
		if(this.get('table_number')){
			receipt_data += builder.createTextElement({width:1,data:'Table: '+this.get('table_number')+'\n'});
		}
		if(this.get('check_number')){
			receipt_data += builder.createTextElement({width:1,data:'Check: '+this.get('check_number')+'\n'});
		}			
		if(this.get('customer')){
			receipt_data += builder.createTextElement({width:1,data:"Customer: " + this.get('customer').get('last_name') +', '+ this.get('customer').get('first_name')+ '\n\n'});
        }
        if(typeof merchant_data.EntryMethod != 'undefined' && merchant_data.EntryMethod != '') {
            receipt_data += builder.createTextElement({width: 1, data: 'Method: ' + merchant_data.EntryMethod + '\n'});
        }
        else {
            receipt_data += builder.createTextElement({width: 1, data: 'Method: ' + this.get('input_method') + '\n'});
        }
		receipt_data += builder.createTextElement({width:1,data:'Type: '+ this.get('transaction_type') +'\n'});

        if(this.has('invoice_id')){
            receipt_data += builder.createTextElement({width:1,data:'Ref: '+ this.get('invoice_id') +'\n'});
        }
        if(typeof merchant_data.ProcessorExtraData3 != 'undefined' && merchant_data.ProcessorExtraData3 != ''){
            receipt_data += builder.createTextElement({width:1,data:'VR#: '+ merchant_data.ProcessorExtraData3 +'\n'});
        }
        receipt_data += builder.createTextElement({width:1,data:'Card Type: '+ this.get('card_type') +'\n'});
		receipt_data += builder.createTextElement({width:1,data:'Card Number: '+ this.get('card_number') +'\n'});
        if(typeof merchant_data.EMV_TSI != 'undefined' && merchant_data.EMV_TSI != ''){
            receipt_data += builder.createTextElement({width:1,data:'TSI: '+ merchant_data.EMV_TSI +'\n'});
        }
        if(typeof merchant_data.EMV_TVR != 'undefined' && merchant_data.EMV_TVR != ''){
            receipt_data += builder.createTextElement({width:1,data:'TVR: '+ merchant_data.EMV_TVR +'\n'});
        }
        if(typeof merchant_data.EMV_AID != 'undefined' && merchant_data.EMV_AID != ''){
            receipt_data += builder.createTextElement({width:1,data:'AID: '+ merchant_data.EMV_AID +'\n'});
        }
        receipt_data += builder.createTextElement({width:1,data:'Auth: '+ this.get('auth_code') +'\n\n'});
        receipt_data += builder.createTextElement({width:1,data:'Amount: '+ accounting.formatMoney(this.get('amount')) +'\n\n'});
		
		if (App.data.course.get_setting('print_tip_line') == 1) {
			receipt_data += builder.createTextElement({data:'\n' +add_white_space('Tip: ', SETTINGS.currency_symbol+'________.____')});
			receipt_data += builder.createTextElement({data:'\n\n' +add_white_space('TOTAL CHARGE: ', SETTINGS.currency_symbol+'________.____')});
		}
		
		receipt_data += builder.createTextElement({width:1,data:'\n\nI agree to pay the above amount according to the card issuer agreement.\n\n\n'});
		receipt_data += builder.createTextElement({width:1,data:'X_____________________________________________\n'});
		if(this.has('cardholder_name') && this.get('cardholder_name') != ''){
			receipt_data += builder.createTextElement({data:'  '+this.get('cardholder_name')});
		}
		
		// Extra spacing at bottom
		receipt_data += builder.createTextElement({
			data: "\n\n"
		});
		
		final_receipt_data += receipt_data;
		
		if(App.data.course.get_setting('signature_slip_count') == 2) {
			final_receipt_data += builder.createTextElement({width:1,data:'\n\n**********************************************\nCustomer Copy\n**********************************************\n'});
			final_receipt_data += builder.createCutPaperElement({feed: true});
			final_receipt_data += receipt_data;
			final_receipt_data += builder.createTextElement({width:1,data:'\n\n**********************************************\nMerchant Copy\n**********************************************\n'});
		}
		
		final_receipt_data += builder.createCutPaperElement({feed: true});
		
		// Send receipt to printer
		App.data.print_queue.add_receipt(App.data.course.get_setting('receipt_ip'), final_receipt_data);
		App.data.print_queue.print();					
	}
});
