var Payment = Backbone.Model.extend({

	idAttribute: "payment_id",
	defaults: {
        "amount": 0.00,
		'approved':'false',
        'auth_amount':0,
        'auth_code':'',
        'bypass': 0,
        'card_number':'',
        'card_type':'',
        'cardholder_name':'',
        'description': '',
        "invoice_id": 0,
        'is_auto_gratuity': 0,
		'params': null,
        "record_id": 0,
        'refund_comment':'',
        'refund_reason':'',
        'tip_recipient': 0,
        'tran_type':'',
        'type': '',
        'verified': false
	},

	// Over ride backbone save method to only send certain fields to be
    // saved to server (instead of entire object)
	save: function (attrs, options) {
		attrs = attrs || this.toJSON();
        console.log('-----------------------------attrs-----------------------------');
        console.dir(attrs);

		options = options || {};

		// Filter attributes that will be saved to server
		attrs = _.pick(attrs, ['approved','card_type','card_number',
			'cardholder_name','auth_amount','auth_code','description', 'tran_type', 'type', 'record_id', 'amount',
			'tip_recipient', 'merchant_data', 'number', 'params','bypass','is_auto_gratuity', 'verified', 'refund_reason', 'refund_comment']);

		options.attrs = attrs;

		// Call super with attrs moved to options
		Backbone.Model.prototype.save.call(this, attrs, options);
	}
});

var CartPayment = Payment.extend({

	initialize: function(attrs, options){

		this.cart = false;
		if(options && options.cart){
			this.cart = options.cart;

		}else if(App.data.cart){
			this.cart = App.data.cart;
		}

		if(this.cart && this.get('type') == 'raincheck'){
			this.listenTo(this.cart, 'change:total change:num_items', this.calculateRaincheckTotal);
		}

		if(this.cart && this.get('type') == 'coupon'){
			this.listenTo(this.cart, 'change:total change:num_items', this.calculatePromoTotal);
		}
	},

	// Updates the raincheck payment if an amount is changed in the cart
	calculateRaincheckTotal: function(){
		var valid_green_fees = 0;
		var valid_cart_fees = 0;
		var valid_tax = 0;
		var payment = this;

		if(!this.get('raincheck')){
			return false;
		}
		var raincheck = payment.get('raincheck');
		var raincheck_green_fee = _.round(raincheck.players * raincheck.green_fee.unit_price, 2);
		var raincheck_cart_fee = _.round(raincheck.players * raincheck.cart_fee.unit_price, 2);

		var items = this.cart.get('items').where({selected: true});
		if(!items || items.length == 0){
			this.set('amount', 0);
			return false;
		}

		// Loop through items in cart and add up totals of green fees
		// and cart fees
		_.each(items, function(item){
			if(raincheck.green_fee && item.get('item_type') == 'green_fee'){
				valid_green_fees += item.get('subtotal');
				valid_tax += item.get('tax');
			}

			if(raincheck.cart_fee && item.get('item_type') == 'cart_fee'){
				valid_cart_fees += item.get('subtotal');
				valid_tax += item.get('tax');
			}
		});

		var total_green_fee = Math.min(raincheck_green_fee, valid_green_fees);
		var total_cart_fee = Math.min(raincheck_cart_fee, valid_cart_fees);
		var tax = Math.min(valid_tax, raincheck.tax);

		var amount = _.round(total_green_fee + total_cart_fee + tax);
		amount = _.min([this.get('raincheck').total,amount]);
		this.set('amount', amount);
	},

	calculateGiftCardTotal: function(){

		var valid_cart_total = this.cart.get('total');
		var payment = this;
		var amount = _.round(payment.get('amount'), 2);

		if(!this.get('gift_card')){
			return false;
		}

		var gift_card = payment.get('gift_card');

		// If gift card is limited to a department or category
		if(gift_card.department != '' || gift_card.category != ''){
			var items = [];
			valid_cart_total = 0;

			if(gift_card.department != ''){
				var items = this.cart.get('items').where({
					selected: true,
					department: gift_card.department
				});

			}else if(gift_card.category != ''){
				var items = this.cart.get('items').where({
					selected: true,
					category: gift_card.category
				});
			}

			if(items.length > 0){
				_.each(items, function(item){
					valid_cart_total += _.round(item.get('total'));
				});
			}
		}

		if(this.cart.get('mode') == 'sale'){
			amount = Math.min(valid_cart_total, amount);
		}else{
			amount = Math.max(valid_cart_total, amount);
		}

		var balance_available = _.round(gift_card.value)
		if(amount > balance_available){
			amount = balance_available;
		}

		this.set('amount', amount);
	},

	calculatePromoTotal: function(){

		// Get all items selected in cart
		var items = this.cart.get('items');
		var valid_cart_total = 0;
		var valid_item_quantity = 0;
		var valid_single_item_amount = 0;
		var amount = 0;
		var promotion = this.get('promotion');

		if(!promotion || promotion == undefined){
			return false;
		}

		// Loop through items and add up qualifying item totals
		_.each(items.models, function(item){

			if( (item.get('category') == promotion.category_type && promotion.category_type != '') ||
				(item.get('department') == promotion.department_type && promotion.department_type != '') ||
				(item.get('subcategory') == promotion.subcategory_type && promotion.department_type != '')
			){
				valid_cart_total += item.get('total');
				valid_item_quantity += item.get('quantity');

				// Calculate the total for a single item
				var subtotal = item.getSubtotal(item.get('unit_price'), 1, item.get('discount_percent'));
				var tax = item.getTax(subtotal, item.get('taxes'));
				var item_total = subtotal + tax;
				
				if(item_total < valid_single_item_amount || valid_single_item_amount == 0){
					valid_single_item_amount = item_total;
				}
			}
		});
		
	
		// If promotion is just a discount
		if(promotion.bogo == 'discount'){

			if(promotion.min_purchase <= valid_cart_total){

				var discount_amount = promotion.amount;
				if(promotion.amount_type == '%'){
					var percentage = _.round(promotion.amount / 100, 5);
					discount_amount = _.round(valid_cart_total * percentage, 2);
				}
				amount = discount_amount;
			}

		// If promotion is BOGO (Buy One Get One)
		}else if(promotion.bogo == 'bogo'){

			var discount_amount = 0;
			if((parseInt(promotion.buy_quantity) + parseInt(promotion.get_quantity)) <= valid_item_quantity){

				if(promotion.amount_type == 'FREE'){
					discount_amount = _.round(promotion.get_quantity * valid_single_item_amount, 2);
				}else{
					var percentage = _.round(promotion.amount / 100, 5);
					var discounted_item_amount = _.round(percentage * valid_single_item_amount, 2);
					discount_amount = _.round(promotion.get_quantity * discounted_item_amount, 2);
				}
			}
			amount = discount_amount;
		}

		this.set('amount', amount);
	}
});

var PaymentCollection = Backbone.Collection.extend({
	url: CART_URL + '/payments',
	model: Payment,
	
	addRefund: function(payment, successCallback){
        console.log('++++++++ add refund - payment +++++++++++++++++++');
        console.dir(payment);

		var existing_payment = this.findWhere({type: payment.type, record_id: payment.record_id});
		var collection = this;

		// If a payment already exists with that type, combine the amounts
		if(existing_payment){
			var old_amount = parseFloat(existing_payment.get('amount'));
			var new_amount = _.round(old_amount + parseFloat(payment.amount));
			existing_payment.set({'amount': new_amount});
		}

		// Add new payment to collection
		this.create(payment, {'wait':true, 'success': function(model, response, options){
			collection.trigger('success', model);
		
		}, 'error': function(model, response, options){
			collection.trigger('error', model, response, options);
			App.vent.trigger('notification', {'type':'error', 'msg':response.responseJSON.msg});
		}});
	}
});

var CartPaymentCollection = Backbone.Collection.extend({

	model: CartPayment,

	initialize: function(attrs, options){

		if(options && options.cart){
			this.cart = options.cart;
		}else{
			this.cart = App.data.cart;
		}
		this.url = CART_URL +'/'+ this.cart.get('cart_id') + '/payments';
	},

	remove_saved_credit_card: function(credit_card_id){
		if(!credit_card_id || this.length == 0){
			return false;
		}
		_.each(this.models, function(payment){
			var params = payment.get('params');
			if(params && params.credit_card_id && params.credit_card_id == credit_card_id){
				payment.destroy();
			}
		});
	},

	addPayment: function(payment, successCallback, errorCallback){

		if(String(payment.amount).length > 9){
			App.vent.trigger('notification', {'type':'error', 'msg':'Please select "Credit Card" before swiping card'});
			return false;
		}
		
		if(payment.type == 'raincheck'){
			var existing_payment = this.findWhere({type: payment.type, number: payment.number});
			if(existing_payment != undefined){
				App.vent.trigger('notification', {'type':'error', 'msg':'Raincheck already added as payment'});
				return false;
			}
		}
		var collection = this;

		// If a signature is required for member purchases, show the signature window instead of submitting payment;
		if(App.data.course.get_setting('require_signature_member_payments') == 1 && 
			payment.type == 'member_account' && (!payment.params || !payment.params.signature)){

			var signature_window = new CustomerSignatureView({model: new Backbone.Model(payment)});
			signature_window.show();
			
			// When the signature is finished, apply it to the payment and add the payment again
			this.listenTo(signature_window, 'signature', function(e){
				if(!payment.params){
					payment.params = {};
				}
				payment.params.signature = e.data;
				collection.addPayment(payment, successCallback, errorCallback);
			});

			return false;
		}

		// Add new payment to collection
		this.create(payment, {'wait':true, 'merge':true, 'success': function(model, response, options){
			
			if(response.status == 'warn'){
				App.vent.trigger('notification', {'type':'warning', 'msg':response.msg});
			}

			if(typeof(successCallback) == 'function'){
				successCallback(model, response, options);
			}

			if(App.data.cart.isPaid()){
				App.data.cart.complete();
			
			}else if(payment.type == 'credit_card' && 
				payment.merchant_data && 
				payment.merchant_data.credit_card_id
			){
				collection.remove_saved_credit_card(payment.merchant_data.credit_card_id);
			}
			collection.trigger('success', model, response, options);

		}, 'error': function(model, response, options){
			$('#cancel-credit-card-payment').show();
			App.vent.trigger('notification', {'type':'error', 'msg':response.responseJSON.msg});
			collection.trigger('error', model, response, options);
			
			if(typeof(errorCallback) == 'function'){
				errorCallback(model, response, options);
			}
		}});
	}
});
