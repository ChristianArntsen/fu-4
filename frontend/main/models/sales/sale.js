var Sale = Backbone.Model.extend({

	idAttribute: "sale_id",
	defaults: {
		"number": null,
		"customer": false,
		"employee": {},
		"items": [],
		"payments": [],
		"total": 0.00,
		"comment": '',
        "refund_reason": '',
        "refund_comment": '',
		"sale_time": null,
		"loyalty_points_earned": 0,
		"loyalty_points_spent": 0,
		"teetime_id": null,
		"auto_gratuity_amount": 0,
		"customer_note": null,
		"teetime": false
	},

	// Over ride backbone save method to only send certain fields to be
	// saved to server (instead of entire object)
	save: function (attrs, options) {
		attrs = attrs || this.toJSON();
		options = options || {};

		// Filter attributes that will be saved to server
		attrs = _.pick(attrs, ['comment', 'sale_time', 'customer_id', 'employee_id', 'customer_note']);

		options.attrs = attrs;

		// Call super with attrs moved to options
		Backbone.Model.prototype.save.call(this, attrs, options);
	},

	initialize: function(attrs, options){
		
		if(this.get('customer')){	
			this.set('customer', new Customer(this.get('customer')));
		}
		this.set('employee', new User(this.get('employee')));
		this.set('items', new SaleItemCollection(this.get('items')));
		
		var payments = new PaymentCollection(this.get('payments'));
		payments.url = SALES_URL + '/' + this.id + '/payments';
		this.set('payments', payments);
	},
	
	printReceipt: function(receipt_type){
		var receipt = new SalesReceipt({
			'items': this.get('items').models,
			'payments': this.get('payments').models,
			'subtotal': this.get('subtotal'),
			'total': this.get('total'),
			'tax': this.get('tax'),
			'taxes': this.get('taxes'),
			'total_due': this.get('total_due'),
			'customer': this.get('customer'),
			'employee': this.get('employee'),
			'sale_id': this.get('sale_id'),
			'number': this.get('number'),
			'date': this.get('sale_time'),
			'loyalty_points_earned': this.get('loyalty_points_earned'),
			'loyalty_points_spent': this.get('loyalty_points_spent'),
			'auto_gratuity': this.get('auto_gratuity'),
			'auto_gratuity_amount': this.get('auto_gratuity_amount'),
			'customer_note': this.get('customer_note'),
			'teetime': this.get('teetime')
		});
		
		receipt.print(receipt_type);
	},
	
	email_receipt_to_customer: function(){
		if(!this.get('customer') || 
			!this.get('customer').get('email') || 
			this.get('customer').get('email') == ''
		){
			return false;
		}
		this.emailReceipt(this.get('customer').get('email'));
		return true;
	},

	emailReceipt: function(email){
		
		var sale_id = this.get('sale_number');
		if(!sale_id)sale_id = this.get('number');
		if(!sale_id)sale_id = this.get('sale_id'); // This is not the sale number

		if(!email){
			return false;
		}
		var collection = this.collection;
		
		$.post(SALES_URL +'/'+ sale_id + '/email_receipt', {'email': email}, function(response){
			if(response.success){
				$('#modal-email-receipt').modal('hide');
				if(collection){
					collection.trigger('email-success', response);
				}
				App.vent.trigger('notification', {'type': 'success', 'msg': 'Receipt emailed successfully'});
			}else{
				if(collection){
					collection.trigger('email-error', response);	
				}
				App.vent.trigger('notification', {'type': 'error', 'msg': 'Error emailing receipt'});
			}
		},'json');
		
		return true;
	},
	
	hasCreditCardPayments: function(){
		if(this.get('payments').length == 0){
			return false;
		}
		if(this.get('payments').findWhere({'type': 'credit_card'})){
			return true;
		}
		return false;
	}
});

var SaleItem = Backbone.Model.extend({
	idAttribute: "line",
	
	defaults: {
		"name": "",
		"category": "",
		"sub_category": "",
		"department": "",
		"description": "",
		"unit_price": 0.00,
		"quantity": 1,
		"total": 0.00,
		"subtotal": 0.00,
		"discount_percent": 0,
		"inventory_unlimited": true,
		"inventory_level": 0,
		"food_and_beverage": 0,
		"selected": true
	}
});

var SaleItemCollection = Backbone.Collection.extend({
	model: SaleItem
});

var SaleCollection = Backbone.Collection.extend({
	model: Sale,
	url: SALES_URL,

	limit: 25,

	loadMore: function(filter){
		
		if(!this.offset){
			this.offset = this.length;
		}else{
			this.offset += this.limit;
		}

		var collection = this;
		var params = {
			offset: this.offset,
			date: moment().format('YYYY-MM-DD'),
			limit: collection.limit
		};

		if(App.data.course.get('use_terminals') == 1){
			var terminal = App.data.course.get('terminals').getActiveTerminal();
			params.terminal_id = terminal.get('terminal_id');
		}

		if(filter && filter != undefined){
			_.extend(params, filter);
		}

		$.get(collection.url, params, function(response){
			if(!response || response.length == 0){;
				collection.trigger('more', response);
				return false;
			}
			collection.add(response);
			collection.trigger('more', response);

		}, 'json');

		return false;
	}
});
