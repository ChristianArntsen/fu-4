describe("Customer",function(){
    var cst = new Customer();
    it("should be defined",function(){
        expect(Customer).toBeDefined();
    });

    it("should create an instance of itself",function(){
        expect(cst).toBeDefined();
        expect(cst instanceof Customer).toBe(true);
    });
});

describe("CustomerCollection",function(){
    var cc = new CustomerCollection();
    it("should be defined", function(){
        expect(CustomerCollection).toBeDefined();
    });

    it("should create an instance of itself",function(){
        expect(cc).toBeDefined();
        expect(cc instanceof CustomerCollection).toBe(true);
    });
});