var PersonalMessage = Backbone.Model.extend({
	idAttribute: "id",
	defaults: {
		"messaging": "",
		"incoming": "",
		"timestamp": "",
		"employee_first_name":"",
		"employee_last_name":""
	}
});

var PersonalMessageCollection = Backbone.Collection.extend({
	model: PersonalMessage,
    url: API_URL + '/personal_message',
	getLastDate:function(){
		if(this.length == 0){
			return moment.utc().format("YYYY-MM-DD HH:mm:ss");
		}
		return this.at(this.length-1).get("timestamp");
	},
	sendNewMessage:function(person,msg){
        var data = {
        	"message":msg
		}
		var request = $.ajax({
            type: "POST",
            url: this.url+"/"+person+"/message",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        });
        request.success(function(response){
            if(response.success == false){
            	var msg = 'Problem sending message.';
            	if(response.msg){
            		msg = response.msg;
				}
                App.vent.trigger('notification', {'msg': msg, 'type': 'error'});
			}
        });
        request.error(function(response){
        	var msg = "Problem sending message.";
            App.vent.trigger('notification', {'msg': 'Problem sending message.', 'type': 'error'});
        })
    }
});