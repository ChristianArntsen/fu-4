var ContactFormReplyModel = Backbone.Model.extend({
  url: function () {
    return API_URL + '/contact_form_replies' + (this.isNew() ? '' : '/' + this.id);
  },
  parse: function (response) {
    return response.customer_replies || response;
  }
});
