var CustomerGroupModel = Backbone.JsonRestModel.extend({
    idAttribute: "group_id",
    defaults: {
        label: '',
        itemDiscount:'',
    },
    type: 'customerGroups',
    relations: {
        customer: {
            type: 'customers',
            many: false
        }
    }
});

Backbone.modelFactory.registerModel(CustomerGroupModel, CustomerGroupCollection);
