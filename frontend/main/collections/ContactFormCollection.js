var ContactFormCollection = Backbone.Collection.extend({
  url: API_URL + '/contact_forms',
  model: ContactFormModel,
  parse: function (response) {
    return response.contact_forms;
  }
});
