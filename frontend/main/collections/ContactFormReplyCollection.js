var ContactFormReplyCollection = Backbone.Collection.extend({
  url: API_URL + '/contact_form_replies',
  model: ContactFormReplyModel,
  parse: function (response) {
    return response.customer_replies;
  }
});
