var TerminalSelectEmptyView = Backbone.Marionette.ItemView.extend({
	template: function(){
		return '<h4 class="text-muted text-center">No terminals found</h4>';
	}
});

var TerminalSelectButtonView = Backbone.Marionette.ItemView.extend({
	
	tagName: 'button',
	className: 'btn-lg btn-default btn-block terminal',
	template: JST['terminal_select_button.html'],
	
	events: {
		"click": "select_terminal"
	},
	
	serializeData: function(){
		var data = this.model.toJSON();
		data.active_register_log = false;
		this.active_register_log = false;
		
		if(data.multiCashDrawers == 1 && data.terminal_id != 0){
			data.active_register_log = App.data.register_logs.findWhere({
				'shift_end': '0000-00-00 00:00:00',
				'terminal_id': String(data.terminal_id)
			});
			this.active_register_log = data.active_register_log;
		}

		data.active_register_log_employee = false;
		if(data.active_register_log){
			data.active_register_log_employee = App.data.course.get('employees').get(data.active_register_log.get('employee_id'));
		}
		
		return data;
	},

	select_terminal: function(){
		
		if(this.active_register_log && this.active_register_log.get('employee_id') != App.data.user.get('person_id')){
			alert('This terminal cash drawer is in use by another employee. The cash drawer will not open.');
		}
		App.data.course.get('terminals').setActive(this.model.get('terminal_id'));
		$('#modal-terminal-select').modal('hide');
		return false;
	}
});

var TerminalSelectButtonListView = Backbone.Marionette.CollectionView.extend({
	tagName: 'div',
	childView: TerminalSelectButtonView,
	emptyView: TerminalSelectEmptyView
});

var TerminalSelectWindow = ModalView.extend({
	
	id: 'terminal-select',
	template: JST['terminal_select.html'],

	initialize: function(attrs, options){
		ModalView.prototype.initialize.call(this, options);
	},
	
	render: function() {
		this.$el.html(this.template());
		
		this.collection.add({
			'terminal_id': 0,
			'label': 'Other'
		});
		
		this.buttonsView = new TerminalSelectButtonListView({
			collection: this.collection
		});
		this.$el.find('.modal-body').html(this.buttonsView.render().el);
		return this;
	},
	
	onHide: function(){
		this.buttonsView.destroy();
	}
});
