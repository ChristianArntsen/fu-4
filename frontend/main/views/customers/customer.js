var CustomerGroupView = Backbone.Marionette.ItemView.extend({
	template: JST['customers/customer_group.html'],
	tagName: 'li',
	className: 'group',

	events: {
		"click button.delete": "removeGroup"
	},

	removeGroup: function(e){
        this.model.collection.remove(this.model);
	}
});

var CustomerGroupsView = Backbone.Marionette.CompositeView.extend({
	template: JST['customers/customer_groups.html'],
	childViewContainer: 'ul',
	childView: CustomerGroupView,

	onAddChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	},

	onRemoveChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	}
});

var CustomerPassView = Backbone.Marionette.ItemView.extend({
	template: JST['customers/customer_pass.html'],
	tagName: 'li',
	className: 'pass',

	initialize: function(options){
		this.customer = false;
		if(options && options.customer){
			this.customer = options.customer;
		}
	},

	events: {
		"click button.disconnect": "disconnect",
		"blur input": "saveDates"
	},

	serializeData: function(){
		var data = this.model.toJSON();
		data.customer_profile = this.customer.toJSON(); 
		return data;
	},

	onRender: function(){
		this.$el.find('input').datetimepicker();
	},

	disconnect: function(e){
		this.model.collection.remove(this.model);
		return false;
	},

	saveDates: function(event){
		var data = {};
		data.start_date = this.$el.find('input[name="start_date"]').val();
		data.expiration = this.$el.find('input[name="expiration"]').val();
		this.model.set(data);
	}
});

var CustomerPassesView = Backbone.Marionette.CompositeView.extend({
	template: JST['customers/customer_passes.html'],
	childViewContainer: 'ul',
	childView: CustomerPassView,
	
	childViewOptions: function(){
		return {customer: this.model};
	},

	onRender: function(){
		
		var view = this;
		var search_field = this.$el.find('#customer-pass-search');

		// Typeahead item suggestion engine
		var passSearch = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.whitespace,
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote:  {
				url: API_URL + '/passes?multi_customer=1&search=%QUERY',
				wildcard: '%QUERY',
				transform: function(response){
					return response.rows;
				}
			},
			limit: 100
		});

		passSearch.initialize();

		search_field.typeahead({
			hint: false,
			highlight: true,
			minLength: 1
		},{
			limit: 101,
			name: 'passes',
			displayKey: 'name',
			source: passSearch.ttAdapter(),
			templates: {
				suggestion: JST['pass_search_result.html']
			}

		}).on('typeahead:selected', function(e, pass, list){
			view.collection.add(pass);
			search_field.typeahead('val', '');
		});
	},

	onAddChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	},

	onRemoveChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	}
});

var CustomerFormView = ModalView.extend({

	tagName: 'div',
	template: JST['customers/customer_form.html'],

	events: {
		"click .save": "save",
		"click .edit-photo": "editPhoto",
		"click .take-photo": "takePhoto",
		"click .delete-photo": "deletePhoto",
		"change #customer-new-group": "addGroup",
		"change #customer-new-pass": "addPass",
		"update #customer-birthday": "renderAge",
		"click #send_text_invite": "sendTextInvite",
        'cardswipe #customer-account-number': 'cardSwipe',
        'keyup #customer-zip': "zipLookup",
        'blur #customer-email': 'updatePhoto',
        'keypress #customer-settings-form': 'blockSubmit'
	},

	initialize: function(){
        ModalView.prototype.initialize.call(this, {extraClass: 'modal-lg'});
		
		var view = this;
		this.listenTo(this.model, 'sync', function(model, collection){
			view.household_members = model.get('household_members').length;
		});
		this.listenTo(App.vent, 'customer:photo', this.render_photo);
		this.listenTo(this.model,'change:birthday',this.renderAge);

		this.field_settings = App.data.course.get('customer_field_settings');
		this.household_members = this.model.get('household_members').length;
    },

    blockSubmit: function(e){
        if (e.which == 13) {
            e.preventDefault();
        }
    },

	renderAge: function(e){
		var birthday = this.$el.find('#customer-birthday')[0].value;
		var age = Math.floor((new Date - new Date(birthday))/(60000*60*24*365));
		if(!isNaN(age))
		    this.$el.find('.customer-age').html('('+age+' yrs)');
		else this.$el.find('.customer-age').html('');
	},

    updatePhoto: function(e){
        var email = this.$el.find('#customer-email')[0].value;
        var photoUrl = this.$el.find('.customer-photo').prop('src');

        if (email != '' && photoUrl.indexOf('profile_picture') > -1) {
            var newUrl = "https://www.gravatar.com/avatar/" + md5($.trim(email).toLowerCase()) + '?r=pg&d=' + encodeURIComponent(BASE_URL + "images/profiles/profile_picture.png");
            this.$el.find('.customer-photo').prop('src', newUrl);
        }
    },

    cardSwipe: function(e, card_number){
        this.$el.find('#customer-account-number').val(card_number);
        return false;
    },

	isValidPhone : function(number){
		return !isNaN(number) || _.isNull(number) || _.isUndefined(number) || number =="";
	},

    render: function(){
    	var self = this;
    	var helpers = {
    		get_required_text: this.get_required_text,
    		field_settings: this.field_settings,
    		is_required_field: this.is_required_field,
			member_account_label: App.data.course.get('member_balance_nickname'),
			customer_account_label: App.data.course.get('customer_credit_nickname')
    	};
    	var attr = this.model.attributes;
    	attr = _.extend(attr, helpers);

    	this.$el.html(this.template(attr));
        this.$el.find('#customer-account-number').cardSwipe({
            lengthThreshold:5
        });
        this.$el.find('#customer-birthday').datetimepicker({
        	format: 'MM/DD/YYYY'
        });
		$(this.$el.find('#customer-birthday')).on('dp.change', function(e){
			self.renderAge();
		});
		this.$el.find('#customer-date-created').datetimepicker({
			format: 'MM/DD/YYYY'
		});

		if(this.isValidPhone(this.model.get("cell_phone_number")) && SETTINGS.currency_symbol === "$"){
			this.$el.find('#customer-cell-phone').formatter({
				'pattern': '({{999}}) {{999}}-{{9999}}',
				'persistent': true
			});
		}
		if(this.isValidPhone(this.model.get("phone_number")) && SETTINGS.currency_symbol === "$"){
			this.$el.find('#customer-phone').formatter({
				'pattern': '({{999}}) {{999}}-{{9999}}',
				'persistent': true
			});
		}

		this.renderGroups();
		this.renderPasses();
		this.renderHousehold();
		this.renderMinimumCharges();
        this.renderAge();

		return this;
	},

	render_photo: function(data){
		var url = data.url + '?t=' + moment().unix(); // Force image to refresh
		this.$el.find('img.customer-photo').prop('src', url);
		this.$el.find('#image_id').val(data.image_id);
		this.$el.find('button.delete-photo').show();
		return false;
	},

	is_required_field: function(field){
		if(this.field_settings[field] && 
			this.field_settings[field].required && 
			this.field_settings[field].required == 1
		){
			return true;
		}
		return false;
	},

	get_required_text: function(field){
		if(this.is_required_field(field)){
			return '<span class="text-danger" style="font-size: 1.5em; line-height: 0.25em">*</span>';
		}
		return '';
	},

	renderGroups: function(){
		var groups = this.model.get('groups');
		var groupsView = new CustomerGroupsView({
			collection: groups
		});

		this.$el.find('#customer-groups').html(groupsView.render().el);
	},

	renderHousehold: function(){
		var members = this.model.get('household_members');
		var householdView = new CustomerHouseholdView({
			collection: members
		});

		this.$el.find('#customer-household').html(householdView.render().el);
	},

	renderMinimumCharges: function(){
		var charges = this.model.get('minimum_charges');
		var minimumChargesView = new CustomerMinimumChargesView({
			collection: charges
		});

		this.$el.find('#customer-minimum-charges').html(minimumChargesView.render().el);
	},

	renderPasses: function(){
		var passes = this.model.get('passes');
		var passesView = new CustomerPassesView({
			collection: passes,
			model: this.model
		});

		this.$el.find('#customer-passes').html(passesView.render().el);
	},

	editPhoto: function(){
		var photoUploadWindow = new CustomerPhotoUploadView({model: this.model});
		photoUploadWindow.show();
		return false;
	},

	takePhoto: function(){
		var photoUploadWindow = new CustomerPhotoUploadView({model: this.model, take: true});
		photoUploadWindow.show();
		return false;
	},

	deletePhoto: function(){
		var placeholder = BASE_URL + 'images/profiles/profile_picture.png';
		this.$el.find('img.customer-photo').prop('src', placeholder);
		this.$el.find('#image_id').val(0);
		this.$el.find('button.delete-photo').hide();
		return false;
	},

    zipLookup: function(){
        var zip = this.$el.find('#customer-zip').val();
        if (zip.length == 5 && !isNaN(zip)) {
            // TODO: Add international capabilities
            var client = new XMLHttpRequest();
            client.open("GET", "https://api.zippopotam.us/us/"+zip, true);
            client.onreadystatechange = function() {
                if(client.readyState == 4) {
                    var response = JSON.parse(client.responseText);
                    var city = '';
                    var state = '';
                    if (typeof response.places != 'undefined' && typeof response.places[0] != 'undefined' && typeof response.places[0]['place name'] != 'undefined') {
                        city = response.places[0]['place name'];
                    }
                    if (typeof response.places != 'undefined' && typeof response.places[0] != 'undefined' && typeof response.places[0]['state abbreviation'] != 'undefined') {
                        state = response.places[0]['state abbreviation'];
                    }
                    if ($('#customer-city').val() == city && $('#customer-state').val() == state) {
                        return;
                    }
                    if ($('#customer-city').val() != '' && city != '') {
                        var confirmed = confirm("Would you like to use the following: "+city+", "+state);
                        if (confirmed) {
                            $('#customer-city').val(city);
                            $('#customer-state').val(state);
                        }
                    }
                    else {
                        $('#customer-city').val(city);
                        $('#customer-state').val(state);
                    }
                };
            };

            client.send();
        }
    },

	save: function(){

		var view = this;
		var fields = [
			'first_name',
			'last_name',
			'email',
			'invoice_email',
			'cell_phone_number',
			'phone_number',
			'birthday',
			'date_created',
			'address_1',
			'address_2',
			'city',
			'state',
			'zip',
			'status',
			'comments',
			'status_flag',
			'account_number',
			'price_class',
			'discount',
			'taxable',
			'require_food_minimum',
			'member',
			'account_balance',
			'account_limit',
			'account_balance_allow_negative',
			'member_account_balance',
			'member_account_limit',
			'member_account_balance_allow_negative',
			'invoice_balance',
			'use_loyalty',
			'loyalty_points',
			'username',
			'password',
			'confirm_password',
			'opt_out_email',
			'opt_out_text',
			'image_id'
		];

		if(this.model.get('household_members').length > this.household_members){
			if(!confirm('Account balances from newly added household members will be transferred to this account. Continue?')){
				return false;
			}
		}

		var data = _.getFormData(this.$el);
		data = _.pick(data, fields);
		
		data.groups = this.model.get('groups').toJSON();
		data.account_limit = -Math.abs(data.account_limit);
		data.member_account_limit = -Math.abs(data.member_account_limit);
		data.photo = this.$el.find('img.customer-photo').prop('src');

		if(this.isValidPhone(this.model.get("cell_phone_number"))){
			data.cell_phone_number = data.cell_phone_number.replace(/\D/g,"");
		}
		if(this.isValidPhone(this.model.get("phone_number"))){
			data.phone_number = data.phone_number.replace(/\D/g, "");
		}

		this.$el.loadMask();

		this.model.set(data).save(null, {
			success: function(){
				App.vent.trigger('notification', {'msg': 'Customer saved', 'type': 'success'});
				view.hide();
			},
			error: function(model, response){
				App.vent.trigger('notification', {'msg': response.responseJSON.msg, 'type': 'danger'});
				$.loadMask.hide();
			}
		});
	},

	addGroup: function(event){
		var menu = $(event.currentTarget);
		var group_id = menu.val();
		if(group_id == "0"){
			return false;
		}
		var group = App.data.course.get('groups').get(group_id);
		this.model.get('groups').add(group.attributes);
		menu.val(0);
	},

	addPass: function(event){
		var menu = $(event.currentTarget);
		var pass_id = menu.val();
		if(pass_id == "0"){
			return false;
		}
		var pass = App.data.course.get('passes').get(pass_id);
		pass.set({expiration: "", start_date: ""});

		this.model.get('passes').add(pass.attributes);
		menu.val(0);
	},

	sendTextInvite: function(event){
		this.model.sendTextInvite();
	}
});