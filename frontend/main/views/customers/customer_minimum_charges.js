var CustomerMinimumChargeView = Backbone.Marionette.ItemView.extend({
	template: JST['customers/customer_minimum_charge.html'],
	tagName: 'tr',
	className: 'minimum-charge',

	events: {
		"click button.delete": "removeCharge",
		"blur input.start-date": "saveStartDate"
	},

	removeCharge: function(){
		this.model.collection.remove(this.model);
	},

	saveStartDate: function(){
		
		var start = moment(this.$el.find('input.start-date').val(), 'MM/DD/YYYY');
		if(!start.isValid()){
			this.$el.find('input.start-date').val('');
			return false;
		}

		this.model.set({
			'date_added': start.format('YYYY-MM-DD')
		});
	},

	onRender: function(){
		this.$el.find('input.start-date').datetimepicker({
        	format: 'MM/DD/YYYY'
        });
	}
});

var CustomerMinimumChargesView = Backbone.Marionette.CompositeView.extend({
	template: JST['customers/customer_minimum_charges.html'],
	childViewContainer: 'tbody',
	childView: CustomerMinimumChargeView,
	emptyView: Backbone.Marionette.ItemView.extend({
		tagName: 'tr',
		template: function(){ return '<td colspan="3"><h5 class="text-muted" style="margin: 0px">No charges assigned</h5></td>'; }
	}),

	events: {
		'change #customer-minimum-charge-select': 'add_charge'
	},

	add_charge: function(e){
		
		var dropdown = this.$el.find('#customer-minimum-charge-select');
		if(!dropdown.val() || dropdown.val() == 0){
			return false;
		}
		var charge = App.data.minimum_charges.get( parseInt(dropdown.val()) );
		if(!charge){
			return false;
		}

		var data = charge.toJSON();
		data.date_added = '';
		data.total = 0;

		this.collection.add(data);
		dropdown.val('');
		return false;
	},

	onAddChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	},

	onRemoveChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	}
});