var ContactFormsLayoutView = Backbone.Marionette.LayoutView.extend({
  tagName: 'div',
  className: 'row',

  attributes: {
    id: 'page-contact-forms'
  },

  template: JST['contact_forms/contact_forms.layout.html'],
  
  regions: {
    table: '#contact-form-table'
  },

  events: {
    'click button.add': 'addContactForm',
    'click button.delete': 'deleteContactForms'
  },

  addContactForm: function () {
    (new ContactFormsEditModalView({
      model: new ContactFormModel()
    })).show();
  },

  deleteContactForms: function () {
    var selection = this.$el.find('tbody :checked');
    
    if (selection && selection.length) {
      selection.each(function (i, e) {
        var model = App.data.contact_forms.get(e.getAttribute('data-contact-form-id'));
        model && model.destroy({wait: true});
      });
    } else {
			App.vent.trigger('notification', {'msg': 'First select contact forms to delete.', 'type': 'error'});
    }
  }
});
