var ContactFormsTableRowView = Backbone.Marionette.ItemView.extend({
  tagName: 'tr',
  template: JST['contact_forms/contact_forms_table_row.html'],

  modelEvents: {
    change: 'modelChanged'
  },

  events: {
    'click a.edit': 'onClickEdit',
    'click a.code': 'onClickCode',
    'click a.replies': 'onClickReplies',
    'click a.delete': 'onClickDelete'
  },


  onRender: function () {
    this.el.setAttribute('data-contact-form-id', this.model.get('id'));
  },

  modelChanged: function (model) {
    this.$el.find('.name').text(model.get('name'));
  },

  onClickEdit: function (event) {
    event.preventDefault();
    new ContactFormsEditModalView({
      model: this.model
    }).show();
  },

  onClickCode: function (event) {
    event.preventDefault();
    var model = this.model;
    $.getJSON(API_URL + '/contact_form_keys', function (data) {
      var url = API_URL.replace('http:', 'https:') + '/customer_contact_form?form=' + model.get('id') + '&key=' + data.contact_form_key.key;
      var ifr = '<iframe src="' + url + '"></iframe>';
      window.prompt('Paste this iframe code into your site:', ifr);
    });
  },

  onClickReplies: function (event) {
    event.preventDefault();

    var
      replies = new ContactFormReplyCollection(),
      model = this.model;

    replies.url += '/' + this.model.id;

    replies.fetch({
      success: function () {
        new ContactFormRepliesModalView({
          model: model,
          collection: replies
        }).show();
      }
    });
  },

  onClickDelete: function (event) {
    event.preventDefault();
		if(confirm('Are you sure you want to delete the ' + this.model.get('name') + ' form?')){
			this.model.destroy({wait: true});
		}
  }
});
