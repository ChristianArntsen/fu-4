var ManagerOverrideWindow = ModalView.extend({
	
	tagName: "div",
	id: "authorize_window",
	template: JST['manager_override_window.html'],

	initialize: function(options) {
		if(options && options.closable){
			this.closable = options.closable;
		}
		if(options && options.backdropOpacity){
			this.backdropOpacity = options.backdropOpacity;
		}
		ModalView.prototype.initialize.call(this, options);						
	},

	events: {
		"click button.submit": "submit",
		"keypress": "submitForm"
	},

	render: function() {	
		var attr = {};
		attr.closable = true;
		if(typeof(this.closable) != undefined){
			attr.closable = false;
		}
		attr.employees = App.data.course.get('employees').getDropdownData();
		
		this.$el.html(this.template(attr));
		return this;
	},
	
	submitForm: function(e){
		if(e.keyCode == 13){
			e.preventDefault();
			this.submit();
		}
		return true;
	},
	
	submit: function(){
		var person_id = this.$el.find('select.person_id').val();
		var password = this.$el.find('input.password').val();
		var view = this;
		
		this.$el.loadMask();
		var url = App.data.cart.url + '/override';
		
		$.post(url, {'password': password, 'person_id': person_id}, function(response){				

			view.hide();
			App.data.cart.set({'override_authorization_id': parseInt(person_id)});
			App.data.user.get('acl').set_override(response.person_id, response.permissions);
			App.vent.trigger('notification', {msg: 'Manager override granted', type: 'success'});
		
		},'json').fail(function(){
			App.vent.trigger('notification', {msg: 'Invalid password', type: 'error'});
			$.loadMask.hide();
			view.$el.find('input.password').val('');
		});
	}
});
