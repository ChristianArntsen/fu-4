var EditPassView = ModalLayoutView.extend({

	id: 'issue_pass_window',
	template: JST['passes/edit_pass.html'],

	events: {
		"click button.save": "save",
		"keyup #pass-owner": function(){ this.temp_model.set({customer_id: false, customer: false}); },
		"change #pass-item-id": "change_pass_type"
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
		this.customer = false;

		if(this.model.get('customer')){
			this.customer = this.model.get('customer');
		}
		this.temp_model = new Pass(this.model.toJSON());

		this.listenTo(App.data.passes, 'sync', function(){ this.hide(); });
		this.listenTo(App.data.passes, 'error', function(){ $.loadMask.hide(); });
	},

	regions: {
		'customers': '#pass-customers'
	},

	serializeData: function(){
		var data = this.model.toJSON();
		
		if(this.model.isNew()){
			var pass_item_menu = {'':'- Select Pass Type -'};
			
			if(App.data.pass_items.length > 0){
				_.each(App.data.pass_items.models, function(item){
					pass_item_menu[' '+item.get('item_id')] = item.get('name');
				});				
			}
			data.pass_item_menu = pass_item_menu;
		}

		return data;
	},

    onRender: function(){

		var view = this;
		var model = this.temp_model;

		init_customer_search(this.$el.find('#pass-owner'), function(e, customer, list){
			view.$el.find('#pass-owner').val(customer.first_name +' '+ customer.last_name);
			model.set({
				'customer': _.pick(customer, ['first_name', 'last_name', 'person_id']),
				'customer_id': customer.person_id
			});
			return false;
		});		

		this.$el.find('#pass-start-date').datetimepicker({
			format: 'MM/DD/YYYY'
		});

		this.$el.find('#pass-end-date').datetimepicker({
			format: 'MM/DD/YYYY',
			useCurrent: false
		});

		this.$el.find('#pass-start-date').on('dp.change', function(e){
			view.$el.find('#pass-end-date').data('DateTimePicker').minDate(e.date);
		});

		this.$el.find('#pass-end-date').on('dp.change', function(e){
			view.$el.find('#pass-start-date').data('DateTimePicker').maxDate(e.date);
		});

		if(model.get('multi_customer') == 1){
			var search_field = this.$el.find('#pass-customer-search');
			init_customer_search(search_field, function(e, customer, list){
				model.get('customers').add(customer);
				search_field.typeahead('val', '');
				return false;
			});	
			this.customers.show( new Pass_customer_collection_view({collection: model.get('customers')}) );		
		}

		return this;
    },

    change_pass_type: function(){
    	
    	var pass_item_id = this.$el.find('#pass-item-id').val();
    	
    	console.debug(pass_item_id); 
    	if(!pass_item_id){
    		return false;
    	}

    	var pass_item = App.data.pass_items.get(parseInt(pass_item_id));
    	if(!pass_item){
    		return false;
    	}

    	var start_date = this.$el.find('input[name="start_date"]');
    	var end_date = this.$el.find('input[name="end_date"]');
    	start_date.val( moment().format('MM/DD/YYYY') );

    	if(pass_item.get('pass_expiration_date')){
    		end_date.val( moment(pass_item.get('pass_expiration_date'), 'YYYY-MM-DD').format('MM/DD/YYYY') );
    	}else{
			end_date.val( moment().add(pass_item.get('pass_days_to_expiration'), 'days').format('MM/DD/YYYY') );
    	}

    	return false;
    },
	
	save: function(e){
		
		e.preventDefault();
		var view = this;

		if(!this.temp_model.get('customer_id') || !this.temp_model.get('customer')){
			this.$el.find('#pass-owner').typeahead('val', '');
		}

		// Validate that required fields are filled in
		var form = this.$el.find('form');
		form.bootstrapValidator('validate');

		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}
		this.$el.loadMask();

		var pass_data = {
			"start_date": view.$el.find('input[name="start_date"]').val() + ' 00:00:00',
			"end_date": view.$el.find('input[name="end_date"]').val() + ' 23:59:59',
			"customer": this.temp_model.get('customer'),
			"customer_id": this.temp_model.get('customer_id'),
			"customers": this.temp_model.get('customers').toJSON()
		};

		if(this.model.isNew()){
			pass_data.pass_item_id = view.$el.find('#pass-item-id').val();
			App.data.passes.create(pass_data);

		}else{
			this.model.save(pass_data);			
		}

		return false;
	}
});
