var PassTableView = Backbone.Marionette.ItemView.extend({
	
	tagName: 'div',
	className: 'col-xs-12',
	template: JST['passes/pass_table.html'],

	events: {
		'click .edit': 'edit',
		'click .delete': 'deletePass',
		'check.bs.table': 'toggle_table_actions',
		'uncheck.bs.table': 'toggle_table_actions',
		'check-all.bs.table': 'toggle_table_actions',
		'uncheck-all.bs.table': 'toggle_table_actions'
	},

	// here, we have the link to listening for database changes
	initialize: function(){
		this.listenTo(this.collection, 'sync', this.updateRow);
		this.listenTo(this.collection, 'destroy', this.deleteRow);
	},

	toggle_table_actions: function(){
		
		var selections = this.$el.find('table').bootstrapTable('getSelections');
		if(!selections || selections.length == 0){
			this.$el.find('.table-toolbar button').prop('disabled', true);
		}else{
			this.$el.find('.table-toolbar button').prop('disabled', false);
		}
	},
	
	///////////////////////////////////////////////////////////
	updateRow: function(model){
		//console.log('updateRow model',model,model.id)
		var index = this.collection.indexOf(model);
		var id = model.id;
		var rowData = model.toJSON();

		// not ideal to update the whole table, but...
		this.$el.find('table.passes').bootstrapTable('refresh',{'silent':true});
		/*
		// this does not appear to be present in our version:
		this.$el.find('table.passes').bootstrapTable('updateByUniqueId', {
			id: id,
			row: rowData
		});
		*/
		/*
		// and this does not work with sorting:
		this.$el.find('table.passes').bootstrapTable('updateRow', {
			index: index,
			row: rowData
		});
		*/
	},
    ////////////////////////////////////////////////////////////
	
	
	deleteRow: function(model){
		this.$el.find('table.passes').bootstrapTable('removeByUniqueId', model.get('pass_id'));
	},

	edit: function(e){
		var link = $(e.currentTarget);
		var pass = this.collection.at(link.data('index'));
		
		var passWindow = new EditPassView({model: pass});
		passWindow.show();

		return false;
	},

	//////////////////////////////////////////////////////
	deletePass: function(e){
		var link = $(e.currentTarget);
		var pass = this.collection.at(link.data('index'));		

		if(confirm('Are you sure you want to delete this pass?')){
			pass.destroy({wait: true});
		}
		return false;
	},
	////////////////////////////////////////////////////////////

	onRender: function(){
		
		var url = this.collection.url;
		var view = this;

		this.$el.find('table.passes').bootstrapTable({
			classes: 'table-no-bordered',
			// should perhaps have the collection do the updating? Not sure...
			url: url,
			data: this.collection.toJSON(),
			uniqueId: 'pass_id',
			sidePagination: 'server',
			columns: [
				{
					checkbox: true
				},
				{
					title: 'Pass',
					field: 'name',
					sortName: 'item.name',
					sortable: true
				},
                {
                    title: 'Times Used',
                    field: 'times_used',
                    formatter: function(val, row, index){
                        if(!val){
                            return '0';
                        }
                        return val;
                    }
                },
                {
                    title: 'Restrictions',
                    field: 'restrictions',
                    formatter: function(val, row, index){
                        if(!val || !val.usage){
                            return '-';
                        }
                        plural = '';
                        if(val.usage.count > 1){
                            plural = 's';
                        }
                        return 'Use up to ' + val.usage.count +
                            ' time' + plural + ' per ' + val.usage.period;
                    }
                },
                {
					title: 'Customer',
					field: 'customer',
					sortable: true,
					sortName: 'customer.first_name',
					formatter: function(val, row, index){
						if(!val || !val.person_id){
							return '-';
						}
						return val.first_name+' '+val.last_name;
					}
				},				
				{
					title: 'Start Date',
					field: 'start_date',
					sortable: true,
					formatter: function(val, row, index){
						if(!val || val == '0000-00-00 00:00:00'){
							return '-';
						}
						return moment(val).format('MM/DD/YYYY');
					}
				},
				{
					title: 'End Date',
					field: 'end_date',
					sortable: true,
					formatter: function(val, row, index){
						if(!val || val == '0000-00-00 00:00:00'){
							return '-';
						}						
						return moment(val).format('MM/DD/YYYY');
					}
				},
				{
					title: 'Date Issued',
					field: 'date_created',
					sortable: true,
					formatter: function(val, row, index){
						return moment(val).format('MM/DD/YYYY');
					}
				},
				{
					title: 'Price Class',
					field: "price_class_name",
					sortName: 'price.name',
					sortable: true
				},
				{
					formatter: function(val, row, index){
						return '<div class="btn-group pull-right"><button type="button" data-index="'+index+'" class="btn btn-default btn-xs delete text-danger">Delete</button>' +
							'<button type="button" data-index="'+index+'" class="btn btn-default btn-xs edit">Edit</button></div>';
					}
				}
			],
			page: 1,
			pagination: true,
			pageSize: 50,
			pageNumber: 1,
			pageList: [10,25,50,100],
			search: true,
			toolbar: $('<div class="table-toolbar"><button class="btn btn-default text-danger delete">Delete</button></div>')[0],
			onLoadSuccess: function(data){
                view.collection.reset(data.rows);
			}
		});
		
		this.$el.find('div.search').prepend('<span class="fa fa-search input-icon"></span>');
		this.toggle_table_actions();
	}
});