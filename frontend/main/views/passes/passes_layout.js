var PassLayout = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	className: 'row page background-color-gradient',
	attributes: {
		id: 'page-passes'
	},
	template: JST['passes/passes.layout.html'],

	regions: {
		actions: '#pass-actions',
		table: '#pass-table'
	},

	events: {
		'click button.delete': 'deletePasses',
		'click button.new-pass': 'new_pass'
	},

	deletePasses: function(){
		
		var selection = $('table.passes').bootstrapTable('getSelections');

		if(!selection || selection.length == 0){
			App.vent.trigger('notification', {'msg': 'First select passes to delete', 'type': 'error'});
			return false;
		}

		_.each(selection, function(row){
			var model = App.data.passes.get(row.pass_id);
			if(model){
				model.destroy();
			}
		});

		return false;
	},

	new_pass: function(){
		var pass = new Pass();
		var passWindow = new EditPassView({model: pass});
		passWindow.show();
		return false;		
	}
});