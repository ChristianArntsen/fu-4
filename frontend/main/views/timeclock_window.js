var TimeclockWindowView = ModalView.extend({

	id: 'timeclock_window',
	template: JST['timeclock_window.html'],

	events: {
		"click button.clock-in": "clock_in",
		"click button.clock-out": "clock_out",
		"change select.person_id": "change_employee",
		"click #toggle_timeclock_entries": "toggle_timeclock_entries"
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
	},
	
	toggle_timeclock_entries: function(){
		this.$el.find('#timeclock_entries').toggle();
		return false;
	},
	
    render: function(attrs){
		
		if(attrs == undefined){
			var attrs = {};
			attrs.person_id = null;
			attrs.clocked_in = false;
			attrs.entries = [];					
		}

		attrs.show_terminals = (App.data.course.get('use_terminals') == 1);

		attrs.employees = App.data.course.get('employees').sort().getDropdownData();
		attrs.terminals = App.data.course.get('terminals').getDropdownData();
		this.$el.html(this.template(attrs));		 		
		
		if(attrs.person_id == null){
			this.load_employee(parseInt(App.data.user.get('person_id')));		
		}
		
		return this;
    },
    
    load_employee: function(employee_id){
		
		this.$el.find('div.modal-content').loadMask('show');
		
		var view = this;
		var attrs = {};
		attrs.clocked_in = false;
		attrs.person_id = employee_id;
		var employee = App.data.course.get('employees').get(attrs.person_id);
		
		attrs.entries = employee.get('timeclock_history');
		
		if(attrs.entries.length == 0){
			
			attrs.entries.fetch({reset: true, data: {	
				employee_id: attrs.person_id,
				limit: 10
			}, success: function(){
				
				if(attrs.entries.at(0) && attrs.entries.at(0).get('total') == '0'){
					attrs.clocked_in = true;
					attrs.current_entry = attrs.entries.at(0);
				}
				view.render(attrs);
			}});
		
		}else{
			if(attrs.entries.at(0) && attrs.entries.at(0).get('total') == '0'){
				attrs.clocked_in = true;
				attrs.current_entry = attrs.entries.at(0);
			}
			
			view.render(attrs);			
		}		
	},
    
    change_employee: function(e){
		
		if($(e.currentTarget).val() == ''){
			return false;
		}
		
		this.load_employee( parseInt($(e.currentTarget).val()) );
		return false;
	},
    
    clock_in: function(e){
		
		var view = this;
		if(this.$el.find('input.password').val() == ''){
			App.vent.trigger('notification', {'type': 'error', 'msg': 'Please enter password'});
			this.$el.find('input.password').focus();
			return false;
		}
		this.$el.find('div.modal-content').loadMask('show');
		
		var person_id = parseInt(this.$el.find('select.person_id').val());
		var password = this.$el.find('input.password').val();
		var terminal_id = 0;

		if(this.$el.find('select.terminal').length > 0){
			terminal_id = this.$el.find('select.terminal').val();
		}
		
		var employee = App.data.course.get('employees').get(person_id);	
		employee.get('timeclock_history').clock_in(person_id, password, terminal_id, function(model, response){
			
			attrs = {};
			attrs.clocked_in = true;
			attrs.entries = employee.get('timeclock_history');
			attrs.person_id = person_id;
			attrs.current_entry = model;
			
			view.render(attrs);
		});		

		return false;
	},
	
	clock_out: function(e){
		
		var view = this;
		if(this.$el.find('input.password').val() == ''){
			App.vent.trigger('notification', {'type': 'error', 'msg': 'Please enter password'});
			this.$el.find('input.password').focus();
			return false;
		}
		this.$el.find('div.modal-content').loadMask('show');
		
		var person_id = parseInt(this.$el.find('select.person_id').val());
		var password = this.$el.find('input.password').val();
		
		var employee = App.data.course.get('employees').get(person_id);	
		employee.get('timeclock_history').clock_out(person_id, password, function(model, response){
			
			attrs = {};
			attrs.clocked_in = false;
			attrs.entries = employee.get('timeclock_history');
			attrs.person_id = person_id;
			attrs.current_entry = model;
			
			view.render(attrs);
		});		
		
		return false;
	}
});
