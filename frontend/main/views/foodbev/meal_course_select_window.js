var MealCourseSelectEmptyView = Backbone.Marionette.ItemView.extend({
	template: function(){
		return '<h4 class="text-muted text-center">No meal courses set up yet</h4>';
	}
});

var MealCourseButtonView = Backbone.Marionette.ItemView.extend({
	
	tagName: 'button',
	className: 'btn-lg btn-default btn-block meal-course',
	template: function(attrs){
		
		var course_items = App.data.food_bev_table.get('cart').where({'meal_course_id': attrs.meal_course_id});
		if(course_items == undefined || course_items.length == 0){
			var num_items = 0;
		}else{
			var num_items = course_items.length;
		}
		
		if(num_items > 0){
			return '<span class="label label-info pull-left">'+num_items+'</span>' + attrs.name;
		}else{
			return '<span class="label label-default pull-left">0</span>' + attrs.name;
		}
	},
	
	initialize: function(options){
		this.item = false;
		if(options && options.item){
			this.item = options.item;
		}
	},

	events: {
		"click": "select_course"
	},
	
	select_course: function(){
		
		var item = this.item;
		item.set('meal_course_id', this.model.get('meal_course_id'));
		
		App.data.food_bev_table.get('cart').addItem(item, true);
		$('#modal-meal-course-select').modal('hide');
		
		return false;
	}
});

var MealCourseButtonListView = Backbone.Marionette.CollectionView.extend({
	tagName: 'div',
	childView: MealCourseButtonView,
	emptyView: MealCourseSelectEmptyView
});

var MealCourseSelectWindow = ModalView.extend({
	
	id: 'meal-course-select',
	template: JST['foodbev/meal_course_select.html'],

	initialize: function(attrs, options){
		ModalView.prototype.initialize.call(this, options);
	},
	
	render: function() {
		this.$el.html(this.template());
		this.buttonsView = new MealCourseButtonListView({
			collection: this.collection,
			childViewOptions: {
				'item': this.model
			}
		});
		this.$el.find('.modal-body').html(this.buttonsView.render().el);
		return this;
	},
	
	onHide: function(){
		this.buttonsView.destroy();
	}
});
