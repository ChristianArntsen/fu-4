var CompWindowView = ModalView.extend({
	
	tagName: "div",
	id: "comp-window",
	template: JST['foodbev/comp_window.html'],

	initialize: function(attrs, options) {
		ModalView.prototype.initialize.call(this, options);
	},

	events: {
		"click .save": "save",
		"click .no-comp": "clear",
		"click .type": "setType",
		"click .reason": "setReason",
		"blur input.amount": "calculateAmount"
	},

	render: function() {
		
		var attr = {};	
		attr.reasons = [
			'Did Not Like',
			'Long Wait',
			'Wrong Item',
			'Food Cold',
			'Manager Comp',
			'Employee Discount',
			'Birthday',
			'Anniversary',
			'Promotion',
			'Waste',
			'Other'
		];
		
		if(this.model instanceof FbReceipt){
			this.model.calculateTotals();
		}
		
		attr.subtotal = new Decimal(this.model.get('subtotal')).plus(this.model.get('comp_total')).toDP(2).toNumber();
		
		if(this.model.get('comp')){
			attr.comp = this.model.get('comp');
		}
		
		// Default comp values if no value is set
		if(!attr.comp){
			attr.comp = {
				type: 'percentage',
				amount: 100,
				description: 'Other'
			};
		}
		
		this.$el.html(this.template(attr));
		this.$el.find('input.amount').keypad({position: 'bottom-right'});
		this.calculateAmount();
		return this;
	},
	
	renderTotal: function(total){
		this.$el.find('h1.comp-total').html( accounting.formatMoney(total) );
		return this;
	},
	
	setType: function(event){
		var button = $(event.currentTarget);
		button.addClass('active').siblings().removeClass('active');
		this.calculateAmount(event);
	},
	
	setReason: function(event){
		event.preventDefault();
		this.$el.find('div.reasons').find('button').removeClass('active');
		$(event.currentTarget).addClass('active');
		return false;
	},
	
	calculateAmount: function(event){

		var amount = this.$el.find('input.amount').val();
		if(!amount || amount == ''){
			amount = 0;
		}
		amount = new Decimal(amount);
		var type = 'percentage';

		var total_after_comp = new Decimal(this.model.get('subtotal')).plus(this.model.get('comp_total')).toDP(2);		
		if(type == 'dollar' && amount.greaterThan(total_after_comp)){
			amount = total_after_comp;		
		
		}else if(type == 'percentage' && amount.greaterThan(100)){
			amount = 100;
		}
		
		this.$el.find('input.amount').val( accounting.formatNumber(amount.toDP(2).toNumber(), 2) );
		
		// If comp is being applied to a single item
		if(this.model instanceof FbCartItem){
			var subtotal = new Decimal(this.model.get('subtotal')).plus(this.model.get('comp_total'));
			var percentage = amount.dividedBy(100);
			var comp_amount = subtotal.times(percentage);
			subtotal = subtotal.minus(comp_amount).toDP(2).toNumber();			
		
		// If comp is being applied to an entire receipt
		}else if(this.model instanceof FbReceipt){
			
			var receipt = this.model;
			var subtotal = new Decimal(receipt.get('subtotal')).plus(receipt.get('comp_total'));
			var comp_amount = new Decimal(0);
			
			// Loop through each item and calculate what the new comp total will be
			_.each(receipt.get('items').models, function(item){
				var item_subtotal = new Decimal(item.get('split_subtotal'))
					.plus(item.get('split_service_fees_subtotal'))
					.plus(item.get('split_comp_total'));

				var percentage = amount.dividedBy(100);				
				comp_amount = comp_amount.plus(item_subtotal.times(percentage).toDP(2));
			});
			
			subtotal = subtotal.minus(comp_amount).toDP(2).toNumber();
		}

		this.renderTotal(subtotal);
	},
	
	clear: function(event){
		event.preventDefault();
		
		if(this.model instanceof FbReceipt){
			this.model.clearComps();	
		}else{
			this.model.set({'comp': false});
			this.model.save();				
		}		
			
		this.hide();	
	},
	
	save: function(event){
		event.preventDefault();
		var amount = this.$el.find('input.amount').val();
		var type = 'percentage';
		var reason = this.$el.find('div.reasons').find('button.active').text();

		if(this.model instanceof FbReceipt){
			this.model.compAllItems(type, reason, amount, App.data.food_bev_table.get('admin_auth_id'));
		
		}else if(this.model.getNumberSplits()>0 && this.model.splitPaid()===true) {
			App.vent.trigger('notification', {'type':'error', 'msg':'Cannot comp a partially paid split item.'});
		
		}else{
			this.model.set({'comp': {
				'amount': amount, 
				'type': type, 
				'description': reason, 
				'employee_id': App.data.food_bev_table.get('admin_auth_id')
			}}, {'validate': true});
			this.model.save();		
		}

		this.hide();
	}
});
