var RestaurantReservationListView = Backbone.Marionette.CompositeView.extend({
	
	template: JST['foodbev/restaurant_reservation_list.html'],
	newReservationTemplate: JST['foodbev/restaurant_reservation_edit.html'],
	tagName: 'div',
	childViewContainer: 'tbody',
	childView: RestaurantReservationView,
	
	getEmptyView: function(){
		
		var date = moment(this.collection.date, 'M/D/YY');

		return Backbone.Marionette.ItemView.extend({
			tagName: 'tr',
			template: function(){
				return '<td colspan="7" class="text-muted">'+
					'<h3 class="text-center" style="line-height: 40px;">No reservations made</h3>' + 
				'</td>';
			}
		});
	},
	
	events: {
		'click button.new-reservation': 'addNew'
	},
	
	addNew: function(){
		
		// First check if new reservation form is already open
		if(this.$el.find('input.form-control.time').length > 0){
			return false;
		}
		var reservation = new RestaurantReservation();
		reservation.set({
			'employee_id': App.data.user.get('person_id'),
			'employee_name': App.data.user.get('first_name')+' '+App.data.user.get('last_name')
		});
		
		this.newReservation = new RestaurantReservationView({editing: true, model: reservation});
		this.$el.find('tbody').prepend( this.newReservation.render().el );
				
		return false;
	},
	
	initialize: function(){
		this.listenTo(this.collection, 'request', this.showLoading);
		this.listenTo(this.collection, 'error sync', this.hideLoading);
		this.listenTo(this.collection, 'invalid', this.showError);
	},
	
	showError: function(model, error){
		App.vent.trigger('notification', {'msg': error, 'type':'error'});
	},
	
	showLoading: function(e){	
		if(!e.date || e.date == undefined){
			return false;
		}
		this.$el.loadMask();
	},
	
	hideLoading: function(e){	
		$.loadMask.hide();
	}
});
