var ReceiptItemView = Backbone.View.extend({
	
	tagName: "button",
	className: "btn btn-primary btn-block item",
	template: JST['foodbev/receipt_item.html'],

	events: {
		"click": "selectItem"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		
		if(this.model.attributes.comp && this.model.attributes.comp.amount > 0){
			this.$el.addClass('comped');	
		}
		
		if(this.model.get('selected')){
			this.$el.addClass('selected');
		}else{
			this.$el.removeClass('selected');
		}
		return this;
	},

	selectItem: function(event){
		
		event.preventDefault();
		if( $(event.currentTarget).parents('div.receipt').hasClass('paid') ){
			return false;
		}
		
		if(App.data.food_bev_table.get('admin_auth_id')){
			this.editItem();
			return false;
		}

		if(this.model.get('selected')){
			this.model.set({"selected":false});
			this.$el.removeClass('selected');
		}else{
			this.model.set({"selected":true});
			this.$el.addClass('selected');
		}

		return false;
	},
	
	editItem: function(){
		var mode = $('#split_payments').find('div.btn-group:visible').data('mode');
		var item = App.data.food_bev_table.get('cart').get(this.model.get('line'));
		
		if(mode == 'comp'){
			var compWindow = new CompWindowView({model: item});
			compWindow.show();
			
		}else if(mode == 'edit'){
			var editItemWindow = new FbEditItemView({model: item, source: 'receipts', receipt:this.model.collection.receipt});
			editItemWindow.show();
		}
		
		return false;
	}
});
