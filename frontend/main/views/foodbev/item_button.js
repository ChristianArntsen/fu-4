var CategoryButtonView = Backbone.Marionette.ItemView.extend({
	
	tagName: 'button',
	className: 'btn btn-primary fb-button btn-block category',
	
	template: function(attrs){
		return attrs.name;
	},
	
	events: {
		'click': 'show_items'
	},
	
	onRender: function(){
		this.$el.attr({
			'data-category-id': this.model.get('category_id'),
			'data-subcategory-id': 0,
			'data-item-id': 0
		});
	},
	
	show_items: function(){
		
		this.$el.siblings('button').removeClass('active');
		this.$el.addClass('active');
		
		var category_id = this.model.get('category_id');
		var items = App.data.food_bev_items.where({
			'menu_category_id': category_id, 
			'menu_subcategory_id': null
		});

		// If any items are in this category loop through them
		// and attach an order number to them
		_.each(items, function(item){
			var button = App.data.menu_buttons.findWhere({
				item_id: item.get('item_id'),
				category_id: item.get('menu_category_id'),
				subcategory_id: '0'
			});
			if(button){
				item.set('order', button.get('order'));
			}
		});

		var sub_categories = _.filter(App.data.menu_buttons.models, function(model){
			
			if(model.get('category_id') == category_id &&
				model.get('subcategory_id') != 0 &&
				model.get('item_id') == 0
			){
				// Only display subcategory button if there are items in that subcategory
				if(
					App.data.food_bev_items.findWhere({
						'menu_category_id': category_id, 
						'menu_subcategory_id': model.get('subcategory_id')
					})
				){
					return true;
				}
				return false;
			}
			return false;
		});

		var buttons = items.concat(sub_categories);
		var collection = new MenuButtonCollection(buttons);

		App.page.currentView.item_buttons.show( new ItemButtonListView({collection: collection}) );
		
		return false;
	}
});

var ItemButtonView = Backbone.Marionette.ItemView.extend({
	
	tagName: 'div',
	className: 'col-md-3 col-lg-2 item-button',

	template: function(attrs){
		
		var btn_class = 'btn-info item';
		if(attrs.item_id == 0){
			btn_class = 'btn-warning sub-category';
		}

		if(attrs.menu_category_id){
			var category_id = attrs.menu_category_id;
		}else{
			var category_id = attrs.category_id;
		}

		if(attrs.menu_subcategory_id){
			var subcategory_id = attrs.menu_subcategory_id;
		}else{
			var subcategory_id = attrs.subcategory_id;
		}

		if(!subcategory_id){
			subcategory_id = 0;
		}

		if (attrs.button_color) {
            var color_style = 'background-color:'+attrs.button_color+'; border-color:'+attrs.button_color+';';
        }
        else {
            var color_style = '';
        }
		// Data attributes in button are sent to server when sorting buttons
		return '<button data-item-id="' + attrs.item_id +
			'" data-category-id="' + category_id +
			'" data-subcategory-id="' + subcategory_id +
            '" style="' + color_style +
			'" class="btn fb-button btn-block '+ btn_class +'">'+ attrs.name +'</button>';
	},
	
	events: {
		'click .item': 'add_item',
		'click .sub-category': 'show_sub_category',
	},
	
	add_item: function(){
		var item = this.model;
		App.data.food_bev_table.get('cart').addItem(item);
		return false;
	},
	
	show_sub_category: function(){
		
		var subcategory_id = this.model.get('subcategory_id');
		var category_id = this.model.get('category_id');
		
		var items = App.data.food_bev_items.where({
			'menu_category_id': category_id, 
			'menu_subcategory_id': subcategory_id
		});

		// Loop through items and attach an assign an order number to them
		_.each(items, function(item){
			var button = App.data.menu_buttons.findWhere({
				item_id: String(item.get('item_id')),
				category_id: String(item.get('menu_category_id')),
				subcategory_id: String(item.get('menu_subcategory_id'))
			});
			if(button){
				item.set('order', button.get('order'));
			}
		});

		var collection = new MenuButtonCollection(items);
		collection.sort();

		var sub_items = new SubItemButtonView({collection: collection, model: this.model});
		sub_items.show();
		
		return false;
	}
});

var CategoryButtonListView = Backbone.Marionette.CollectionView.extend({
	
	childView: CategoryButtonView,
	tagName: "div",
	className: "row",
	
	initialize: function(){	
		var view = this;
	},

	// Only display "root" category buttons
	addChild: function(child, childView, index){
		
		if(child.get('subcategory_id') != 0 || child.get('item_id') != 0){
			return false;
		}
		// If no items are found in this category, do not display it
		if(!App.data.food_bev_items.findWhere({'menu_category_id': child.get('category_id')})){
			return false;
		}

		Backbone.Marionette.CollectionView.prototype.addChild.apply(this, arguments);
	},

	onRender: function(){
		this.$el.find('button').first().trigger('click');
	}
});

var ItemButtonListView = Backbone.Marionette.CollectionView.extend({
	
	childView: ItemButtonView,
	tagName: "div",
	className: "row"
});

var SubItemButtonView = ModalView.extend({
	
	tagName: "div",
	id: 'item-sub-category',
	template: JST['foodbev/sub_category_window.html'],
	
	events: {
		'click .edit-order': 'enableSorting',
		'click .save-order': 'saveSort',
		'click .up': 'scrollItemsUp',
		'click .down': 'scrollItemsDown'
	},
	
	initialize: function(){
		ModalView.prototype.initialize.call(this, {extraClass: 'modal-lg'});
		this.listenTo(App.data.food_bev_table.get('cart'), 'add', this.hide);	
	},
	
	render: function() {
		var self = this;
		var attrs = this.model.attributes;
		this.$el.html(this.template(attrs));

		_.each(this.collection.models, this.addItem, this);
		return this;
	},

	addItem: function(item){
		this.$el.find('#sub-category-items').append( new ItemButtonView({model: item}).render().el );
	},
	
	enableSorting: function(e){
		
		this.$el.addClass('editing');
		var list = this.$el.find('#sub-category-items');
		
		list.sortable({
			cancel: ''
		});		
		
		this.$el.find('.edit-order').replaceWith('<a href="#" class="save-order">Save Button Order</a>');	
		
		e.preventDefault();
		return false;
	},
	
	saveSort: function(e){
		e.preventDefault();
		
		this.$el.removeClass('editing');
		var buttons = this.$el.find('#sub-category-items');
		
		App.page.currentView.saveButtonOrder(buttons);
		
		this.$el.find('.ui-sortable').sortable("destroy");
		this.$el.find('a.save-order').replaceWith('<a href="#" class="edit-order">Edit Button Order</a>');	
		return false;	
	},
	
	scrollItemsUp: function(event){
		var list = this.$el.find('#sub-category-items');
		var curPos = list.scrollTop();
		
		list.scrollTop(curPos - 140);
		return false;
	},
	
	scrollItemsDown: function(event){
		var list = this.$el.find('#sub-category-items');
		var curPos = list.scrollTop();
		
		list.scrollTop(curPos + 140);	
		return false;	
	}
});