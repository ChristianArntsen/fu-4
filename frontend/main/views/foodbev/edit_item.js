var FbEditItemView = ModalView.extend({
	tagName: "div",
	attributes: {id: "edit-item"},
	template: JST['foodbev/edit_item.html'],
	options: {active_section: false},
	
	initialize: function(options){
		if(options && options.source){
			this.source = options.source;
		}
		if(options && options.receipt){
			this.receipt = options.receipt;
		}
		this.saved = false;
		ModalView.prototype.initialize.call(this, {extraClass: 'modal-lg'});
	},
	
	events: {
		"click .save-item": "saveItem",
		"click .delete-item": "deleteItem",
		"click li.tab > a": "clickTab",
		"click button.sub": "subField",
		"click button.add": "addField"
	},

	addField: function(event){
		var button = $(event.currentTarget);
		var field = button.parents('div').first().find('input');
		var value = field.val();

		var increment = 1;
		if(field.attr('name') == 'item[discount]'){
			increment = 5;
		}

		field.val( parseFloat(value) + increment );
	},

	subField: function(event){
		var button = $(event.currentTarget);
		var field = button.parents('div').first().find('input');
		var value = field.val();

		if(value <= 1){
			return false;
		}

		var increment = 1;
		if(field.attr('name') == 'item[discount]'){
			increment = 5;
		}

		field.val( value - increment );
	},

	render: function() {
		var data = this.model.attributes;

		if(data.incomplete){
			data.show_incomplete = true;
		}else{
			data.show_incomplete = false;
		}

		// Create list of sections (tabs) to display in item edit window
		data.sections = {
			1: {
				name: "Cook Temp",
				incomplete: !this.model.isModifiersComplete(3)
			},
			2: {
				name: "Customize",
				incomplete: !this.model.isModifiersComplete(1)
			},
			3: {
				name: "Condiments",
				incomplete: !this.model.isModifiersComplete(2)
			},
			4: {
				name: "Soup/Salad",
				incomplete: !(this.model.isSoupsComplete() && this.model.isSaladsComplete())
			},
			5: {
				name: "Sides",
				incomplete: !this.model.isSidesComplete()
			}
		};

		var modifiers = this.model.get('modifiers');

		// Remove any sections that don't have any options
		if(modifiers.length == 0 || !modifiers.findWhere({'category_id':3})){ delete data.sections[1]; } // Cook Temp
		if(modifiers.length == 0 || !modifiers.findWhere({'category_id':1})){ delete data.sections[2]; } // Customize
		if(modifiers.length == 0 || !modifiers.findWhere({'category_id':2})){ delete data.sections[3]; } // Condiments
		if(this.model.get('number_salads') == 0 && this.model.get('number_soups') == 0){ delete data.sections[4]; }
		if(this.model.get('number_sides') == 0){ delete data.sections[5]; }
		
		// Set the first tab to display by default
		data.default_section = false;
		_.every(data.sections, function(section, section_id){
			data.default_section = section_id;
			return false;
		});
		
		var meal_course_dropdown = {" 0":'- No meal courses -'};
		if(App.data.course.get('meal_courses').models.length > 0 && App.data.course.get('use_course_firing') == 1){
			var meal_course_dropdown = {" 0":'- No meal course -'};
			_.each(App.data.course.get('meal_courses').models, function(meal_course){
				meal_course_dropdown[" "+meal_course.get('meal_course_id')] = '('+meal_course.get('order')+') '+meal_course.get('name');
			});
		}
		data.meal_course_dropdown = meal_course_dropdown;
		data.use_course_firing = App.data.course.get('use_course_firing');
		
		this.$el.html(this.template(data));
		this.$el.find('#edit_item_seat').keypad({position: 'right', formatMoney: false});
		this.$el.find('#edit_item_quantity').keypad({position: 'right', formatMoney: false});
		this.$el.find('#edit_item_price, #edit_item_discount').keypad({position: 'right'});

		if(data.default_section){
			this.renderSection(data.default_section);
		}

		if(data.sections[1] || data.sections[2] || data.sections[3] || data.sections[4] || data.sections[5]){
			this.$el.find('.tab-content').css({'min-height': '325px'});
		}
		
		return this;
	},

	clickTab: function(event){
		var button = $(event.currentTarget);
		var section_id = button.data('section-id');
		var tab_pane = $(button.attr('href'));

		// Mark tab as active
		button.parents('li').addClass('active').siblings().removeClass('active');

		this.renderSection(section_id);

		// Show appropriate tab pane
		tab_pane.addClass('active').siblings().removeClass('active');
		event.preventDefault();
	},

	renderSection: function(section_id){
		var viewData = {};
		viewData.section_id = section_id;
		viewData.model = this.model;
		viewData.item_edit_view = this;

		// Modifiers
		var item = this.model;
		if(section_id <= 3){
			var view = new ItemModifiersView(viewData);

		// Soups/salads
		}else if(section_id == 4){
			var view = new ItemSidesView(viewData);

		// Sides
		}else if(section_id == 5){
			var view = new ItemSidesView(viewData);
		}

		// Close last section
		if(this.options.active_section){
			this.options.active_section.remove();
		}

		this.$el.find('#item-edit-' + section_id).html( view.render().el );	
		return true;
	},

	saveItem: function(event){
		if(this.model.getNumberSplits()>0 && this.model.splitPaid()===true) {
			App.vent.trigger('notification', {'type':'error', 'msg':'Cannot edit a partially paid split item.'});
		    return false;
		}

		// Get new data from fields
		var seat = _.getNumber(this.$el.find('#edit_item_seat').val(), this.model.get('seat'));
		var price = _.getNumber(this.$el.find('#edit_item_price').val(), this.model.get('price'));
		var quantity = _.getNumber(this.$el.find('#edit_item_quantity').val(), this.model.get('quantity'));
		var discount = _.getNumber(this.$el.find('#edit_item_discount').val(), this.model.get('discount'));
		var comments = this.$el.find('#edit_item_comments').val();
		
		var meal_course_id = 0;
		if(this.$el.find('#meal_course_id').length > 0){
			meal_course_id = parseInt(this.$el.find('#meal_course_id').val());
		}

		var incomplete = true;
		if(this.model.isComplete()){
			incomplete = false;
		}

		// Update item with new properties
		this.model.set({
			"quantity": quantity, 
			"seat": seat, 
			"price": price, 
			"unit_price": price,
			"discount": discount, 
			"discount_percent": discount,
			"comments": comments,
			"incomplete": incomplete,
			"meal_course_id": String(meal_course_id)
		}, {'validate':true});

		if(!this.model.validationError){
			this.model.save();
			this.saved = true;
			this.hide();
		}
		
		return false;
	},

	deleteItem: function(event){
		this.saved = true;
		if(this.receipt && this.receipt.getTotalPayments && this.receipt.getTotalPayments() > 0) { // receipt
			App.vent.trigger('notification', {'type':'error', 'msg':'Please cancel partial payment(s) before deleting items from receipt.'});
			return false;
		}
		this.model.destroy();
		this.hide();
		return false;
	},

	onHide: function(){
		if(!this.saved){
			this.saveItem();
		}
		return false;
	}
});
