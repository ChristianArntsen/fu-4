var ReceiptView = Backbone.View.extend({
	
	tagName: "div",
	className: "col-md-6 col-lg-4 receipt-container",
	template: JST['foodbev/receipt.html'],

	events: {
		"click .pay": "payReceipt",
		"click .start-tab": "startTab",
		"click .print": "printReceipt",
		"click .delete-receipt": "deleteReceipt",
		"click div.receipt": "receiptClick",
		"click .tax": "changeTaxable",
		"click .up": "scrollItemsUp",
		"click .down": "scrollItemsDown",
		"click .customer": "attachCustomer",
		"click .tip-type button": "changeTipType",
		"keypad_close": "updateGratuity"
	},

	initialize: function() {
		this.model.unstick();
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model.get('items'), "add remove reset", this.render);
		this.listenTo(this.model.get('payments'), "add remove reset", this.render);
	},

	render: function(){
		//this.model.calculateTotals();
		this.$el.html( this.template(this.model.attributes) );
		this.$el.find('div.title').after( new ReceiptItemListView({collection:this.model.get('items')}).render().el );
		this.$el.find('input.receipt-tip').keypad({position: 'bottom-right', closeOnClick: true});
		var receipt = this.$el;
		
		this.$el.find('input.receipt-tip').on('focus', function(){
			receipt.find('.pay, .print').addClass('disabled');
		});

		if(this.model.get('payments').hasTab()){
			this.$el.find('.start-tab').addClass('pay').html('Pay Tab');
		}else{
			this.$el.find('.start-tab').removeClass('pay').html('Start Tab');
		}
		
		this.renderStatus();
		return this;
	},

	renderStatus: function(){
		if(this.model.get('items').length == 0){
			this.$el.find('button.pay, button.print').addClass('disabled');
		}else if(this.model.get('status') != 'complete'){
			this.$el.find('button.pay, button.print').removeClass('disabled');
		}else{
			this.$el.find('button.print').removeClass('disabled');
		}
	},

	startTab: function(event){
		if(
			App.data.course.get_setting('require_customer_on_sale') == 1 &&
			!this.model.get('customer')
		){
			App.vent.trigger('notification', {msg: 'Customer is required to open tab', type: 'error'});
			return false;
		}
		if(this.model.get('payments').hasTab()){
			// user clicked pay tab
			//App.vent.trigger('notification', {'type':'error', 'msg':'Tab already opened.'});
			return false;
		}

		/* CONSIDERATIONS:
		*  No counting these as payments for validation purposes
		*  e.g. empty receipt with an open tab marked as paid upon reload
		 */
		
		// need to bring up credit card payment
		var payments = this.model.get('payments');
		var paymentWindow = new FbPaymentWindowView({collection: payments, model: this.model});
		paymentWindow.save_card = true;
		paymentWindow.show();

		paymentWindow.showCreditCard(event);

		return false;		
	},

	payReceipt: function(event){

		if ($('div#jquery_keypad').is(':visible')){
			alert('Please save gratuity in order to proceed.');
			return false;
		}

		if(this.model.get('items').length == 0 || this.model.get('status') == 'complete'){
			return false;
		}

		if(
			App.data.course.get_setting('require_customer_on_sale') == 1 &&
			!this.model.get('customer')
		){
			App.vent.trigger('notification', {msg: 'Customer is required to complete sale', type: 'error'});
			return false;
		}

		var receiptModel = this.model;
		this.model.calculateTotals();

		var payments = this.model.get('payments');
		var paymentWindow = new FbPaymentWindowView({collection: payments, model: receiptModel});
		paymentWindow.show();

		if(event){
			event.preventDefault();
		}
	},

	changeTipType: function(e){
		var button = $(e.currentTarget);
		var type = button.data('type');
		button.addClass('active').siblings().removeClass('active');

		this.model.set('auto_gratuity_type', type);
		this.model.save();
	},

	
	attachCustomer: function(event){
		event.preventDefault();
		var receiptModel = this.model;
		var customerSearch = new ReceiptCustomerSearchView({receipt: receiptModel});
		
		customerSearch.show();
		
		return false;
	},
	
	printReceipt: function(event){
		if ($('div#jquery_keypad').is(':visible')){
			alert('Please save gratuity in order to proceed.');
			return false;
		}
		this.model.printReceipt('pre-sale');

		if(event){
			event.preventDefault();
		}
	},
	
	changeTaxable: function(event){
		if(event){
			event.preventDefault();
		}
		
		var tax_btn = this.$el.find('button.tax');
		var new_status = 1 - tax_btn.data('tax');
		tax_btn.data('tax', new_status);
		
		if(new_status == 1){
			tax_btn.html('&#10004; Tax');
		}else{
			tax_btn.html('&#x25a2; Tax');
		}
		
		this.model.set('taxable', new_status);
		this.model.save();
		return false;
	},

	cancelPayments: function(event){
		var tot = this.model.getTotalPayments();
		var receipts = this.model.collection;
		if(window.confirm('Do you wish to cancel all payments ('+tot+')? This will include all credit card payments that you have collected for this receipt.')){
			this.model.cancelAllPayments();

			receipts.calculateAllTotals();
			this.model.unstick();
			this.render();
		}else{
			return false;
		}
		return true;
	},

	deleteReceipt: function(event){
		if(!this.model.hasPayments() && this.model.get('items').length === 0)
			return this.model.destroy();

		var prompt = new DeleteReceiptPrompt();
		prompt.model = this.model;
		prompt.parent = this;
		prompt.show();
		return false;
	},

	setCustomer: function(customer) {
		this.model.setCustomer(customer);
	},

	removeCustomer: function(){
		this.model.removeCustomer();
	},
	
	scrollItemsUp: function(event){
		var list = this.$el.find('div.receipt-items');
		var curPos = list.scrollTop();
		
		this.$el.find('div.receipt-items').scrollTop(curPos - 45);
		return false;
	},
	
	scrollItemsDown: function(event){
		var list = this.$el.find('div.receipt-items');
		var curPos = list.scrollTop();
		
		this.$el.find('div.receipt-items').scrollTop(curPos + 45);	
		return false;	
	},
	
	updateGratuity: function(event){
		var auto_gratuity = this.$el.find('input.receipt-tip').val();
		this.model.set({'auto_gratuity': auto_gratuity});
		this.model.save();

		// Enable print/pay buttons
		this.$el.find('button.pay, button.print').removeClass('disabled');
	},
	
	receiptClick: function(event){
		
		// Ignore click when attempting to change gratuity
		if( $(event.target).hasClass('receipt-tip') ){
			return true;
		}
		
		var mode = $('#split_payments').find('div.btn-group:visible').data('mode');		
		if(mode == 'move' || mode == 'split'){
			this.moveItems(event);
		}
		
		if(mode == 'comp'){
			this.compItems(event);
		}

		if(this.model.collection) // if not, then too new or too deleted for collection
			this.model.collection.calculateAllTotals();

	},
	
	compItems: function(event){
		var receipt = this.model;
		var compWindow = new CompWindowView({model: receipt});
		compWindow.show();
		
		return false;
	},
	
	moveItems: function(event){
		var new_r = this.model;
		var receipts = this.model.collection;
		if(!this.model.collection)return;
		var selectedItems = this.model.collection.getSelectedItems();
		var items = [];
		var mode = $('#split_payments').find('div.btn-group:visible').data('mode');		
		
		// If this receipt has already been paid, do nothing
		if(this.model.get('status') == 'complete'){
			receipts.unSelectItems();
			return false;
		}

		if(selectedItems.length == 0){
			return false;
		}
		var targetReceiptItems = this.model.get('items');

		// Add selected items to target receipt.
		_.each(selectedItems, function(selectedItem){

			// Make sure items aren't being moved back onto same receipt
			// and that it is not part of a partially paid receipt
			if((selectedItem.splitPaid() || selectedItem.collection.receipt.getTotalPayments() > 0) && mode !== 'move'){
				App.vent.trigger('notification', {'type':'error', 'msg':'Partially paid item cannot be split. Please cancel payment(s) to split items.'});
				return false;
			} else if(selectedItem.collection.receipt.getTotalPayments() > 0){
				App.vent.trigger('notification', {'type':'error', 'msg':'Items cannot be moved from partially paid receipts. Please cancel payment(s) to move items.'});
				return false;
			}

			if(selectedItem.collection != targetReceiptItems){
				var old_r = selectedItem.collection.receipt;
				
				// If moving item, remove it from the receipt
				// Don't send delete request yet
				var removedSelectedItem = false;
				if(mode == 'move'){
					var removedItemUrl = selectedItem.collection.url + selectedItem.id;
					removedSelectedItem = selectedItem.collection.remove(selectedItem);
					removedSelectedItem.url = removedItemUrl;

					// clean up customer discount nonsense
					if(old_r.get && old_r.get('customer') && old_r.get('customer').get && old_r.get('customer').get('discount')){
						selectedItem.cart_item.set('discount',0);
						selectedItem.cart_item.set('discount_percent',0);
					}

					// apply cusomer discount
					if(new_r.get && new_r.get('customer') && new_r.get('customer').get && new_r.get('customer').get('discount')){
						var customer_discount = new Decimal(new_r.get('customer').get('discount'));
						var appliedDiscount = 0;
						var max_discount = new Decimal(selectedItem.cart_item.get('max_discount'));

						if(max_discount.lessThan(customer_discount)){
							appliedDiscount = max_discount;
						}else{
							appliedDiscount = customer_discount;
						}

						selectedItem.cart_item.set('discount',appliedDiscount);
					}
                    selectedItem.cart_item.save();
				}

				// Copy the item including the nested collections
				var itemCopy = _.clone(selectedItem.toJSON());
				
				targetReceiptItems.create(itemCopy, {complete: function(response){
					// Once item is successfully copied, delete it from other receipt
					if(removedSelectedItem){
						removedSelectedItem.destroy();
					}

					_.each(receipts.models, function(receipt){
						receipt.calculateTotals();
						receipt.unstick();
					});
					
				}, isNew: true});
			}
		});

		this.model.collection.unSelectItems();
	}
});

var DeleteReceiptPrompt = ModalView.extend({
	tagName: "div",
	id: 'delete-receipt-prompt',
	template: JST['foodbev/delete_receipt_window.html'],
	events: {
		'click .delete-sale':'deleteSale'
	},

	initialize: function(){
		//ModalView.prototype.initialize.call(this,{});
	},

	render: function() {
		var attrs = this.model.attributes;
		this.$el.html(this.template(attrs));
	},

	deleteSale:function (e) {
		var tot = this.model.getTotalPayments();
		var receipts = this.model.collection;
		this.model.cancelAllPayments();

		receipts.calculateAllTotals();
		this.model.unstick();

		var splitItems = this.model.collection.getSplitItems();
		var items = this.model.get('items').models;
		var deleteReceipt = true;

		// Make sure items in receipt to be deleted exist on other receipts
		_.each(items, function(item){
			var line = item.get('line');
			if(splitItems[line] <= 1){
				deleteReceipt = false;
				App.vent.trigger('notification', {'type':'error', 'msg':'Cannot delete receipt with unpaid items on it.'});
			}
		});
        var receipt = this.model;
        var callback = function() {
            // we are successful. Sale is gone. Reset the payments
            receipt.get('payments').reset();
			if (deleteReceipt) {
				if (receipt.get('customer')) {
					receipt.removeCustomer();
				}
				receipt.destroy();
			}
			if (App.data.food_bev_table.get('receipts').length === 0) {
				App.data.food_bev_table.close(function () {
					App.page.currentView.show_table_layouts()
				});
				return;
			}
		}
		$.ajax(
			this.model.url()+'/sale',
			{
				success:function(res){
					callback(res);
				},
				error:function(res){
					if(typeof error === 'function')
						error(res);
				},
				data:{
					'reason':$('#delete-receipt-reason').val()
				},
				method:'DELETE'
			}
		);
	}
});