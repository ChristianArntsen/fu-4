var MergeTableWindow = ModalView.extend({
	
	id: 'merge-table',
	template: JST['foodbev/merge_table_window.html'],

	initialize: function(attrs, options){
		if(!options){
			var options = {};
		}
		options.extraClass = 'modal-lg';
		ModalView.prototype.initialize.call(this, options);

		this.listenTo(this.collection, 'select-table', this.selectTable);
		this.listenTo(this.model, 'error', this.showError);
	},
	
	render: function() {
		this.childViews = [];
		this.$el.html(this.template(this.model.toJSON()));
		window.requestAnimationFrame(
			this.renderLayouts.bind(this)
		);
		return this;
	},

	renderLayouts: function(){
		var view = this;
		
		var layout_tabs = new TableLayoutTabsView({collection: this.collection});
		var layouts = new TableLayoutsView({collection: this.collection});
		view.childViews.push(layout_tabs);
		view.childViews.push(layouts);
		
		this.$el.find('div.layout-tabs').html(layout_tabs.render().el);
		this.$el.find('div.layouts').html(layouts.render().el);

		this.$el.find('li.tab > a').first().tab('show');	
		this.$el.find('div.tab-pane.layout').first().addClass('active').siblings().removeClass('active');	

		return this;
	},

	selectTable: function(layoutObject){
		
		if(!layoutObject || !layoutObject.get('label') || layoutObject.get('label') == ''){
			return false;
		}
		this.$el.loadMask();

		var params = {
			'label': App.data.food_bev_table.get('table_num')
		};
		var sourceTableObject = this.collection.getTableObject(params);
		var table_name = this.model.get('name');
		var view = this;

		this.model.merge(layoutObject.get('label'), function(){
			if(sourceTableObject){
				sourceTableObject.reset();	
			}
			if(!layoutObject.get('employee_id')){
				layoutObject.set({
					'employee_first_name': App.data.user.get('first_name'),
					'employee_last_name': App.data.user.get('last_name'),
					'employee_id': App.data.user.get('person_id'),
					'table_name': table_name
				});
			}
			view.hide();
			App.vent.trigger('notification', {msg: 'Table merged successfully', type: 'success'});
		});

		return false;
	},

	onHide: function(){
		_.each(this.childViews, function(view){
			view.destroy();
		});
	}
});
