var TableLayoutObjectView = Backbone.Marionette.ItemView.extend({
	
	tagName: "div",
	className: "object",
	template: JST['foodbev/table_layout_object.html'],
	
	events: {
		"click": "openTable",
		"click button.delete-table": "deleteTable",
		"dblclick": "editName",
		"keypress": "enterSave",
		"click .switch-employee": "switchEmployee"
	},
	
	initialize: function(attrs, options){
		
		this.editing = false;
		if(attrs && attrs.hasOwnProperty('editing') && attrs.editing == true){
			this.editing = true;
		}
		this.listenTo(this.model.collection, 'enable-edit', this.enableEditing);
		this.listenTo(this.model, 'change:label change:employee_id success', this.render);
	},

	getMultiplier: function(){
		// Get multiplier to go from 900 width space to available width space.
		var parent_el = document.querySelector('.layouts');
		if(!parent_el){
			return 1*'bad';
		};
		var css = window.getComputedStyle(parent_el,null);
		var parent_width = css.getPropertyValue('width').replace('px','');
		var parent_height = css.getPropertyValue('height').replace('px','');
		var parent_ratio = null;
		var a_ratio = this.options.layout.get('aspect_ratio');
		if(parent_width>0) parent_ratio = parent_height/parent_width;

		// first we try to fit to our screen.
		// We default to a pixel means a pixel
		var multiplier = 1;

		// can we fit the aspect of the layout?
		if(!isNaN(parent_ratio) && parent_ratio != null &&
			parent_width > 0 && parent_height > 0 &&
		    typeof a_ratio!== 'undefined' && a_ratio &&
			!isNaN(a_ratio.lowest) && !isNaN(a_ratio.rightest) &&
			a_ratio.lowest > 0 && a_ratio.rightest > 0
		){
			// Yes, it looks like we can
			if(parent_ratio > a_ratio.ratio){
				// parent window is taller and narrower in aspect
				// we go by width
				multiplier = parent_width/a_ratio.rightest;
			}
			else if(parent_ratio < a_ratio.ratio){
				// parent window is shorter and wider in aspect
				// we go by height
				multiplier = parent_height/a_ratio.lowest;
			}
			// else, it is already what we need. Leave at 1
		}

		// Now we see if we are zooming in or out
		var zf = 0;
		if(typeof this.options !== 'undefined' &&
			typeof this.options.layout !== 'undefined' &&
			typeof this.options.layout.collection.zoomFactor !== 'undefined' &&
			this.options.layout.collection.zoomFactor)zf = this.options.layout.collection.zoomFactor;
		if(zf>0) {
			for (var i = 0; i < zf; i++){
				multiplier *= 0.85;
			}
		}else{
			for (var i = 0; i > zf; i--){
				multiplier /= 0.85;
			}
		}

		this.multiplier = multiplier;
		return multiplier;
	},
	
	render: function(){
		var multiplier = this.getMultiplier();

		var view = this;
		var style = 'position: absolute;';
		style += 'top:' + (this.model.attributes.pos_y*multiplier) + 'px;';
		style += 'left:' + (this.model.attributes.pos_x*multiplier) + 'px;';
		var wd = 80;
		if(typeof this.model.attributes.width !== 'undefined' && this.model.attributes.width != null && !isNaN(this.model.attributes.width)) {
			wd = this.model.attributes.width;
		}
		style += 'width:' + (wd*multiplier) + 'px;';
		var ht = 80;
		if(typeof this.model.attributes.height !== 'undefined' && this.model.attributes.height != null && !isNaN(this.model.attributes.height)) {
			ht = this.model.attributes.height;
		}
		style += 'height:' + (ht*multiplier) + 'px;';
		
		this.$el.attr('style', style);
		this.$el.attr('data-object-id', this.model.attributes.object_id);
		
		var data = this.model.attributes;

		if(data.employee_id != null && parseInt(data.employee_id) == parseInt(App.data.user.get('person_id'))){
			this.$el.removeClass('other-active').addClass('my-active');
		}else if(data.employee_id != null){
			this.$el.removeClass('my-active').addClass('other-active');
		}

		this.$el.html(this.template(data));
		
		if(this.editing){
			this.enableEditing();
		}
		
		if(this.$el.data('target')){
			this.$el.contextmenu('destroy');	
		}
			
		this.$el.contextmenu({
			target: view.$el.find('.context-menu')
		});
		return this;
	},
	
	switchEmployee: function(e){
		e.preventDefault();
		var select_employee_view = new FbEmployeeSelectWindow({
			collection: App.data.course.get('employees'), 
			model: this.model
		});
		select_employee_view.show();
		return false;
	},
	
	enableEditing: function(){

		var multiplier = this.getMultiplier();
		var model = this.model;
		
		this.$el.draggable({
			stop: function(e, ui){
				var obj = $(ui.helper);
				var pos = obj.position();
				model.set({
					pos_x: Math.round(pos.left/multiplier),
					pos_y: Math.round(pos.top/multiplier)
				}).save();
			}
		});
		
		this.$el.resizable({
			stop: function(e, ui){
				var object = $(ui.element);
				model.set({
					width: Math.round(ui.size.width/multiplier),
					height: Math.round(ui.size.height/multiplier)
				}).save();
			}
		});
		
		return false;
	},
	
	editName: function(){
		
		if(!this.$el.parents('div.modal-body').hasClass('editing')){
			return false;
		}
		
		if(this.$el.hasClass('other-active') || this.$el.hasClass('my-active')){
			App.vent.trigger('notification', {msg: 'Sale must be finished to change table name', type: 'error'});
			return false;
		}
		
		var view = this;
		var label_field = $('<input name="label" class="form-control object-label" type="text" value="" />');
		label_field.val(this.model.get('label'));
		
		this.$el.find('span.label').replaceWith(label_field);
		label_field.focus();
		
		label_field.off('blur').on('blur', function(e){
			view.saveName();
		});
		
		return false;
	},
	
	enterSave: function(e){
		if(e.key == "Enter" || e.keyCode == 13){
			this.saveName();
		}
		return true;
	},
	
	saveName: function(){
		var label = this.$el.find('input.object-label').val();
		var view = this;
		var label_field = this.$el.find('input');
		
		this.model.save({'label': label}, {wait: true, error: function(model, response){
			label_field.off('blur');
			App.vent.trigger('notification', {msg: response.responseJSON.msg, type: 'error'});
			view.render();
		}});
		
		return false;
	},
	
	deleteTable: function(){
		var view = this;
		this.model.destroy({wait: true, error: function(model, response){
			App.vent.trigger('notification', {msg: response.responseJSON.msg, type: 'error'});			
		}});
		
		return false;
	},
	
	openTable: function(e){

		if(this.$el.parents('div.modal-body').hasClass('editing')){
			return true;
		}
		if(this.$el.parents('div.merge-table').length > 0){
			this.model.collection.trigger('select-table', this.model);
			return true;
		}
		
		var table_num = this.model.get('label');
		var layoutObject = this.model;
		App.data.food_bev_table.open(table_num, function(){
			layoutObject.set({
				'employee_first_name': layoutObject.get('employee_first_name')?layoutObject.get('employee_first_name'):App.data.user.get('first_name'),
				'employee_last_name': layoutObject.get('employee_last_name')?layoutObject.get('employee_last_name'):App.data.user.get('last_name'),
				'employee_id': layoutObject.get('employee_id')?layoutObject.get('employee_id'):App.data.user.get('person_id')
			});
			if(App.data.course.get_setting('require_guest_count') == 0 ||
				App.data.food_bev_table.get('guest_count')!== null){
				$('div.modal').modal('hide');
				return false;
			}

			var guest_count_window = new GuestCountWindow();
			guest_count_window.show();
		});
		
		return false;
	}
});

var TableLayoutView = Backbone.Marionette.CompositeView.extend({
	
	tagName: "div",
	className: "tab-pane layout",
	childView: TableLayoutObjectView,
	template: function(){ 
		return '<button class="btn btn-danger btn-sm delete-layout">&times; Delete Layout</button>' +
		'<button class="btn btn-default btn-sm edit-layout"><i class="glyphicon glyphicon-pencil"></i> Edit Layout Name</button>' 
	},
	
	childViewOptions: function(){
		if(App.data.food_bev_table_layouts.editing){
			return {editing: true, layout: this.model}
		}
		return {editing: false, layout: this.model}
	},
	
	initialize: function(){
		this.listenTo(this.model.collection, 'enable-edit', this.enableEditing);
	},
	
	events: {
		'click button.delete-layout': 'deleteLayout',
		'click button.edit-layout': 'editLayout'		
	},
	
	onRender: function(){
		this.$el.attr('id', 'layout_' + this.model.get('layout_id'));
		this.$el.attr('data-layout-id', this.model.get('layout_id'));
		
		if(this.model.collection.editing){
			this.enableEditing();
		}	
	},
	
	enableEditing: function(){
		
		var model = this.model;
		// When new table dropped on to layout, copy it there
		var self = this;

		this.$el.droppable({
			drop: function (ev, ui){
				
				if(!$(ui.draggable).hasClass('drag')){
					return false
				}

				var multiplier = self.getMultiplier();
				var width = multiplier*900; // back to 900px space

				var pos = $(ui.helper).position();
				var type = $(ui.helper).attr('data-type');
				var left = (pos.left + width)/multiplier;
				var top = (pos.top + 16)/multiplier;

				var table = {
					'label': '',
					'employee_id': null,
					'employee_first_name': '',
					'employee_last_name': '',
					'table_name': '',
					'pos_x': left,
					'pos_y': top,
					'width': 75,
					'height': 75,
					'type': type
				};

				model.get('objects').create(table);
			}
		});	
	},
	
	editLayout: function(){
		var edit_window = new TableLayoutEditWindowView({model: this.model});
		edit_window.show();
		return false;
	},
	
	deleteLayout: function(){	
		if(!confirm('Are you sure you want to delete this layout?')){
			return false;
		}
		
		this.model.destroy({wait: true});
		return false;
	},

	getMultiplier: function(){
		// Get multiplier to go from 900 width space to available width space.
		var parent_el = document.querySelector('#modal-table_layout_window .tab-content');
		if(!parent_el){
			return 1*'bad';
		};
		var css = window.getComputedStyle(parent_el,null);
		var parent_width = css.getPropertyValue('width').replace('px','');
		var multiplier = 1;
		if(!isNaN(parent_width) && parent_width != null){
			multiplier = parent_width/900;
		}
		this.multiplier = multiplier;
		return multiplier;
	}
});

var TableLayoutsView = Backbone.Marionette.CollectionView.extend({
	tagName: 'div',
	className: 'tab-content',
	childView: TableLayoutView,
	
	childViewOptions: function(model){
		return { collection: model.attributes.objects }
	},
	
	attachHtml: function(collectionView, childView, index){
		if (collectionView.isBuffering) {
			collectionView.elBuffer.appendChild(childView.el);
		}else{		
			collectionView.$el.append(childView.el);
			childView.$el.addClass('active').siblings().removeClass('active');
		}
	},	
	
	onRemoveChild: function(childView){
		this.$el.find('div.layout:last').addClass('active').siblings().removeClass('active');
	}	
});

var TableLayoutTabView = Backbone.Marionette.ItemView.extend({
	tagName: "li",
	className: 'tab',
	template: function(attrs){
		return '<a role="tab" data-toggle="tab" href="#layout_'+attrs.layout_id+'">'+attrs.name+'</a>';
	},
	
	modelEvents: {
		'change:name': 'render'
	},
	
	events: {
		'click': 'saveLastLayout'
	},
	
	saveLastLayout: function(){
		App.data.food_bev_table_layouts.last_layout = this.model.get('layout_id');
	}
});

var TableLayoutTabsView = Backbone.Marionette.CollectionView.extend({
	tagName: 'ul',
	className: 'nav nav-tabs layout-tabs',
	childView: TableLayoutTabView,
	
	events: {
		"click #new_layout": "newLayout"		
	},
	
	collectionEvents: {
		'enable-edit': 'showNewButton'
	},
	
	onRender: function(){
		if(this.collection.editing){
			this.showNewButton();			
		}
	},
	
	attachHtml: function(collectionView, childView, index){
		if (collectionView.isBuffering) {
			collectionView.elBuffer.appendChild(childView.el);
		}else{		
			if(collectionView.$el.find('li.new').length > 0){
				collectionView.$el.find('li.new').before(childView.el);
				childView.$el.addClass('active').siblings().removeClass('active');
			}else{
				collectionView.$el.append(childView.el);
			}
		}
	},
	
	onRemoveChild: function(childView){
		this.$el.find('li').last().prev().addClass('active');
	},
	
	showNewButton: function(){
		this.$el.find('li.new').remove();
		this.$el.append('<li class="new"><button id="new_layout" class="button btn-success btn-sm new">New</button></li>');
	},
	
	newLayout: function(){
		var edit_window = new TableLayoutEditWindowView();
		edit_window.show();
		return false;
	}
});

var TableLayoutEditWindowView = ModalView.extend({
	
	id: 'edit-layout-window',
	template: JST['foodbev/table_layout_edit_window.html'],

	initialize: function(attrs, options){
		ModalView.prototype.initialize.call(this, options);
	},
	
	events: {
		"click .save": "save"
	},
	
	render: function() {
		var attrs = {
			name: ''
		}	
		if(this.model){
			attrs = this.model.attributes;
		}

		this.$el.html(this.template(attrs));
		return this;
	},
	
	save: function(){
		
		var layout_name = this.$el.find('input').val();
		
		if(!this.model){
			App.data.food_bev_table_layouts.create({name: layout_name}, {wait: true});
			
		}else{
			this.model.set({name: layout_name}).save();
		}
		
		this.hide();
		return false;
	}
});

var TableLayoutWindowView = ModalView.extend({
	
	tagName: "div",
	id: "table_layout_window",
	template: JST['foodbev/table_layout_window.html'],
	
	events: {
		"click ul.nav-tabs a": "setLastLayout",
		"click #edit_layouts": "editLayouts",
		"click #done_editing_layouts": "doneEditing",
		"click #tables_logout": "logout",
		"click #tables_recent_transactions": "recentTransactions",
		"click #tables_close_register": "closeRegister",
		"click #reservations": "reservations",
		//"wheel #table_layout_window .zoom-buttons": "zoom",
		"click button.zoom-in":"zoomIn",
		"click button.zoom-out":"zoomOut"
	},

	initialize: function(attrs, options){
		if(!options){
			options = {};
		}
		this.childViews = [];
		
		this.closable = true;
		if(options && options.closable !== undefined){
			this.closable = options.closable;
		}	
		if(options && options.backdropOpacity){
			this.backdropOpacity = options.backdropOpacity;
		}
		if(options && options.last_layout){
			this.last_layout = options.last_layout;
		}

		options.extraClass = 'modal-lg';
		ModalView.prototype.initialize.call(this, options);
		this.listenTo(this.collection, 'sync reset', this.selectDefaultTab);
	},

	setLastLayout: function(e){
		var self = this;
		var tgt = e.currentTarget.hash;
		var tgt = tgt.replace('#layout_','');
		App.data.food_bev_table_layouts.last_layout;
		this.last_layout = App.data.food_bev_table_layouts.last_layout;
		//$(window).resize(self.renderLayouts);
	},

	render: function() {

		var data = {};
		data.closable = this.closable;
		this.$el.html(this.template(data));
		window.requestAnimationFrame(
		  this.renderLayouts.bind(this)
		);

		return this;
	},
	
	logout: function(){
		App.page.currentView.logout();
		return false;
	},

	onShow: function(){
		var z_index = parseInt($('#modal-table_layout_window').css('z-index'));
		z_index += 2;
		_.defer(function(){ $('nav').css('z-index', z_index) });
	},
	
	recentTransactions: function(){
		App.page.currentView.recentTransactions();
		return false;		
	},
	
	closeRegister: function(){
		App.page.currentView.closeRegister();
		return false;		
	},
	
	reservations: function(){
		var reservations = App.data.restaurant_reservations;
		var reservationWindow = new RestaurantReservationsWindow({collection: reservations});
		reservationWindow.show();
		return false;
	},
	
	selectDefaultTab: function(){
		if(!this.last_layout || this.last_layout == undefined){
			if(this.collection.at(0)){
				this.last_layout = this.collection.at(0).get('layout_id');
			}
		}
		
		if(this.last_layout){
			this.$el.find('a[href="#layout_'+this.last_layout+'"]').tab('show');	
			this.$el.find('#layout_'+this.last_layout).addClass('active').siblings().removeClass('active');	
		}		
	},	
	
	renderLayouts: function(){
		var view = this;

		if(typeof this.zoomFactor !== 'undefined'){
			App.data.food_bev_zoom = this.zoomFactor;
		}
		else if(typeof App.data.food_bev_zoom !== 'undefined'){
			this.zoomFactor = App.data.food_bev_zoom;
		}else{

			this.zoomFactor = App.data.food_bev_zoom = 0;
		}
		this.collection.zoomFactor = this.zoomFactor;

		var layout_tabs = new TableLayoutTabsView({collection: this.collection});
		var layouts = new TableLayoutsView({collection: this.collection});

		view.childViews.push(layout_tabs);
		view.childViews.push(layouts);
		
		this.$el.find('div.layout-tabs').html(layout_tabs.render().el);
		this.$el.find('div.layouts').html(layouts.render().el);
		
		this.selectDefaultTab();
		return this;
	},
	
	doneEditing: function(e){
		$(e.currentTarget).replaceWith('<button id="edit_layouts" class="btn btn-link" style="top: 5px; right: 10px; position: absolute;">Edit Layouts</button>');
		
		this.collection.trigger('disable-edit');
		
		this.$el.find('li.new').remove();
		this.$el.find('.modal-body').removeClass('editing');
		this.$el.find('.ui-droppable').droppable('destroy');
		this.$el.find('.ui-draggable').draggable('destroy');
		this.$el.find('.ui-resizable').resizable('destroy');
		
		return false;
	},
	
	editLayouts: function(e){
		
		var link = $(e.currentTarget);
		link.replaceWith('<button id="done_editing_layouts" class="btn btn-link" style="top: 5px; right: 10px; position: absolute;">Done Editing</button>');
		
		var view = this;
		this.$el.find('.modal-body').addClass('editing');
		
		this.collection.trigger('enable-edit');

		// Allow tables in palette window to be dragged onto layout
		this.$el.find('div.object-palette .drag').draggable({
			helper: 'clone'
		});
		
		return false;	
	},
	
	onHide: function(){
		$('nav').css('z-index', '');
		_.each(this.childViews, function(view){
			view.destroy();
		});
	},

	zoom: function(e){
		console.log('zoom',e);
	},

	zoomIn: function(e){
		this.zoomFactor--;
		this.renderLayouts();
	},

	zoomOut: function(e){
		this.zoomFactor++;
		this.renderLayouts();
	}
});
