var FbReceiptWindowView = ModalView.extend({
	tagName: "div",
	id: "split_payments",
	template: JST['foodbev/receipts.html'],

	initialize: function(){
		ModalView.prototype.initialize.call(this, {extraClass: 'modal-xlg'});
		this.listenTo(this.collection, "add", this.render);
		this.listenTo(this.collection, "remove", this.render);
		this.listenTo(this.collection, "reset", this.render);
		this.listenTo(this.collection, "admin", this.switchAdminMode);
	},

	events: {
		"click .new-receipt": "newReceipt",
		"click .remove-items": "removeItems",
		"click div.mode button": "changeMode",
		"click .admin-override": "adminOverride",
		"click .admin-exit": "adminExit",
		"click button.left": "scrollLeft",
		"click button.right": "scrollRight"
	},

	render: function() {
		
		var table = App.data.food_bev_table;
		table.get('receipts').unSelectItems();
		table.get('cart').unSelectItems();
		
		if(!this.$el.html()){
			this.$el.html('');
		}
		
		var attr = {
			admin_auth: false
		};
		
		if(App.admin_auth_id){
			attr.admin_auth = true;
		}
		
		this.$el.html(this.template(attr));
		_.each(this.collection.models, this.addReceipt, this);

		return this;
	},
	
	scrollRight: function(){
		var receipts = this.$el.find('div.row.receipts');
		var curVal = receipts.position().left;
		var numReceipts = parseInt($('div.row.receipts div.receipt-container').length);
		var receiptWidth = parseInt(this.$el.find('div.receipt-container').first().outerWidth(true)) + 15;
		var totalReceiptWidth = numReceipts * receiptWidth

		if(Math.abs(curVal - receiptWidth) < totalReceiptWidth){
			$('div.row.receipts').css("left", curVal - receiptWidth);
		}		
		
		return false;
	},
	
	scrollLeft: function(){
		var curVal = $('div.row.receipts').position().left;
		var receiptWidth = parseInt(this.$el.find('div.receipt-container').first().outerWidth(true))- 15;

		if(curVal <= -receiptWidth){
			$('div.row.receipts').css("left", curVal + receiptWidth);
		}else{
			$('div.row.receipts').css("left", 0);
		}
		return false;
	},	
	
	switchAdminMode: function(){
		if(App.data.food_bev_table.get('admin_auth_id')){
			this.$el.find('.receipt-nav').addClass('admin');
		}else{
			this.$el.find('.receipt-nav').removeClass('admin');
		}
	},

	addReceipt: function(receipt){
		receipt.calculateTotals();
		this.$el.find('div.row.receipts').append( new ReceiptView({model: receipt}).render().el );
	},

	newReceipt: function(event){
		this.collection.create({"date_paid":null});
		return this;
	},
	
	adminOverride: function(event){
		event.preventDefault();
		var authWindow = new AuthorizeWindowView({action: 'Admin Override'});
		authWindow.show();
		return false;
	},
	
	adminExit: function(event){
		event.preventDefault();
		App.data.food_bev_table.set('admin_auth_id', false);
		this.switchAdminMode();
	},

	removeItems: function(event){
		// Retrieve selected items across all receipts
		var selectedItems = this.collection.getSelectedItems();
		if(!selectedItems){
			return false;
		}
		var receipts = App.data.food_bev_table.get('receipts');
		var cart = App.data.food_bev_table.get('cart');
		var splitItems = receipts.getSplitItems();

		// Loop through selected items and delete them
		_.each(selectedItems, function(item){
			var line = item.get('line');

			// Only allow delete if the item exists on other receipts
			// TODO: do we want to allow removal of unpaid/unordered items from receipt?
			if((splitItems[line] && splitItems[line] > 1) /*|| (!item.get('is_paid') && !item.get('is_ordered'))*/){
				item.destroy();
			}
		});

		receipts.unSelectItems();
		cart.unSelectItems();
		this.render();
		//receipts.calculateAllTotals();
		
		return true;
	},

	changeMode: function(event){
		var btn = $(event.currentTarget);
		btn.parents('.btn-group-justified').find('button').removeClass('active');
		btn.addClass('active');
		btn.parents('.btn-group-justified').data('mode', btn.data('value'));
	},

	close: function() {
		this.$el.unbind();
		this.$el.empty();
	}
});
