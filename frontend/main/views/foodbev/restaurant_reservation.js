var RestaurantReservationView = Backbone.Marionette.ItemView.extend({
	
	tagName: 'tr',
	getTemplate: function(){	
		if(this.editing != undefined && this.editing){
			return JST['foodbev/restaurant_reservation_edit.html'];
		}else{
			return JST['foodbev/restaurant_reservation.html'];
		}
	},
	
	events: {
		"click button.delete": "deleteReservation",
		"click button.edit-reservation": "editReservation",
		"click button.save": "saveReservation",
		"click button.cancel": "cancelEdits",
		"click button.check-in": "checkIn"
	},
	
	initialize: function(options){
		if(options && options.editing !== undefined){
			this.editing = options.editing;
		}
		this.listenTo(this.model, 'change:checked_in', this.render);
	},
	
	onRender: function(){
		
		var view = this;
		if(!this.editing && this.model.isNew()){
			this.$el.addClass('highlight-success');
		}
		
		init_customer_search(this.$el.find('#reservation-name'), function(e, customer, list){
			view.$el.find('#reservation-customer-id').val(customer.person_id);
			view.$el.find('#reservation-name').val(customer.first_name +' '+ customer.last_name).data('customer', customer);
			return false;
		});
		
		_.defer(function(){ view.$el.find('#reservation-time').focus() });
	},

	deleteReservation: function(){
		this.model.destroy();
		return false;
	},
	
	editReservation: function(){
		this.editing = true;
		this.render();
	},
	
	checkIn: function(){
		if(this.model.get('checked_in') == 0){
			this.model.save({'checked_in': 1});
		}else{
			this.model.save({'checked_in': 0});
		}
		return false;
	},
	
	saveReservation: function(){
		
		var date = moment(App.data.restaurant_reservations.date);
		var time = moment(this.$el.find('#reservation-time').val(), 'HH:mma');
		var customer_id = this.$el.find('#reservation-customer-id').val();
		
		if(!customer_id || customer_id == '' || customer_id == undefined){
			customer_id = null;
		}
		
		this.model.set({
			'time': time.format('HH:mm:ss'),
			'name': this.$el.find('#reservation-name').val(),
			'date': date.format('YYYY-MM-DD'),
			'guest_count': this.$el.find('#reservation-guests').val(),
			'comments': this.$el.find('#reservation-comments').val(),
			'customer_id': customer_id
		});
		
		if(this.model.isNew()){
			var reservation = App.data.restaurant_reservations.create(this.model.attributes, {validate: true});

			if(!reservation){
				return false;
			}
			this.remove();
				
		}else{
			this.model.save();
			this.editing = false;
			this.render();
		}
		
		return false;
	},
	
	cancelEdits: function(){
		if(this.model.isNew()){
			this.remove();
		}else{
			this.editing = false;
			this.render();			
		}

		return false;
	}
});
