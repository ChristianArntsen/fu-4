var FbCustomerListItemView = Backbone.Marionette.ItemView.extend({
	tagName: "div",
	className: "row customer",
	template: JST['foodbev/customer.html'],

	events: {
		"click .remove": "removeCustomer"
	},

	initialize: function(){
		this.listenTo(this.model,"change",this.render);
		this.render();
	},

	render:function(){
		if(this.model.get('customer')) {
			this.$el.html(this.template(this.model.get('customer').attributes));
		}
		else {
			this.$el.html('');
		}
	},

	removeCustomer: function(event){
		this.model.removeCustomer();
	}
});

var FbCustomerListEmptyView = Backbone.Marionette.ItemView.extend({
	template: function(attrs){
		return '<div class="row customer">' +
			'<div class="col-xs-12">' +
			'<h4 style="margin: 0px;" class="text-muted">No customers associated</h4>' +
		'</div></div>';
	}
});

var FbCustomerListView = Backbone.Marionette.CollectionView.extend({
	tagName: 'div',
	className: 'col-xs-12',
	childView: FbCustomerListItemView,
	emptyView: FbCustomerListEmptyView
});
