var GuestCountFieldView = Backbone.Marionette.ItemView.extend({
	
	initialize: function(){
		this.listenTo(this.model, 'change:suspended_sale_id change:guest_count', this.render);
		this.listenTo(this.model, 'change:guest_count',this.save_guest_count);
	},
	
	events: {
		'keypad_enter input.guest-count': 'save_guest_count',
		'keypress input.guest-count': 'enter_submit',
		'keypad_close input.guest-count': 'closed'
	},
	closed:function(){
		this.save_guest_count();
	},
	enter_submit: function(e){
		if(e.which == 13){
			this.save_guest_count();
			return false;
		}
	},
	
	template: function(attrs){
		var guest_count = attrs.guest_count;
		if(attrs.guest_count == null){
			guest_count = '';
		}
		return '<input type="text" class="form-control input-sm guest-count pull-right" style="width: 50px; margin: 2px 10px 0px 0px;" value="'+guest_count+'"><label class="pull-right" style="font-weight:normal; line-height: 30px; margin-right: 10px;">Guest Count</label>';
	},
	
	onRender: function(){
		this.$el.find('input.guest-count').keypad(
			{
				position: 'bottom-right',
				formatMoney: false
			});
	},
	
	save_guest_count: function(){
		var guest_count = this.$el.find('input.guest-count').val();
		
		if(guest_count == ''){
			guest_count = null;
		}
		
		if(this.model.get('guest_count') != guest_count){
			this.model.set('guest_count', guest_count);
			this.model.save();
		}
		return false;
	}
});

var GuestCountWindow = ModalView.extend({
	
	id: 'guest-count-window',
	template: JST['foodbev/guest_count_window.html'],

	initialize: function(attrs, options){
		ModalView.prototype.initialize.call(this, options);
	},
	
	events: {
		'click button.save': 'save_guest_count',
		'keypad_enter input.guest-count': 'save_guest_count',
		'keypress input.guest-count': 'enter_save'
	},
	
	render: function() {
		this.$el.html(this.template());
		this.$el.find('input.guest-count').keypad({position: 'bottom-right', formatMoney: false});		
		return this;
	},
	
	enter_save: function(e){
		if(e.which == 13){
			this.save_guest_count();
			return false;
		}
		return true;		
	},
	
	save_guest_count: function(){
		var count = parseInt(this.$el.find('input.guest-count').val());
		if(!count || isNaN(count)){
			count = 0;
		}
		
		App.data.food_bev_table.set('guest_count', count);
		App.data.food_bev_table.save();
		
		$('div.modal').modal('hide');
		return false;
	},
	
	onShow: function(){
		this.$el.find('input').click().focus();
	}
});
