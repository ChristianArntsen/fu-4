var ReceiptCustomerListItemView = Backbone.View.extend({
	
	tagName: "div",
	className: "row customer",
	template: JST['foodbev/receipt_customer.html'],

	events: {
		"click": "attach"
	},

	initialize: function(options) {
		if(options && options.receipt){
			this.receipt = options.receipt;
		}
		this.listenTo(this.model, "change", this.render);
	},

	render: function() {
		var attrs = this.model.attributes;
		this.$el.html(this.template(this.model.attributes));
		return this;
	},
	
	attach: function(event){
		event.preventDefault();
		
		this.receipt.setCustomer(this.model);
		
		$('#modal-customer-search-window').modal('hide');
		return false;
	}
});
