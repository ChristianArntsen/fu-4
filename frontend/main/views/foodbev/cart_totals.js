var FbCartTotalsView = Backbone.View.extend({
	tagName: "div",
	className: "row",
	id: "register_container",
	template: JST['foodbev/cart_totals.html'],

	initialize: function() {
		this.listenTo(this.collection, "change:total change:sides_total change:comp_total add remove reset", this.render);
	},

	render: _.debounce(function(){
		var totals = this.collection.getTotals();
		this.$el.html(this.template(totals));
		return this;
	}, 25)
});
