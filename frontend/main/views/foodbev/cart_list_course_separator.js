var CartListCourseSeparatorView = Backbone.View.extend({
	
	tagName: "tr",
	className: "separator",
	template: JST['foodbev/cart_course_separator.html'],

	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});
