var FireMealCourseEmptyView = Backbone.Marionette.ItemView.extend({
	template: function(){
		return '<h4 class="text-muted text-center">No meal courses set up yet</h4>';
	}
});

var FireMealCourseButtonView = Backbone.Marionette.ItemView.extend({
	
	tagName: 'div',
	className: 'row fire-meal-course',
	template: function(attrs){
		
		var course_items = App.data.food_bev_table.get('cart').where({'meal_course_id': attrs.meal_course_id});
		if(course_items == undefined || course_items.length == 0){
			var num_items = 0;
		}else{
			var num_items = course_items.length;
		}
		attrs.num_items = num_items;
		
		return JST['foodbev/fire_course_button.html'](attrs);
	},

	events: {
		"click button.fire-course": "fire_course"
	},
	
	fire_course: function(){
		
		var meal_course_id = this.model.get('meal_course_id');
		var course_items = App.data.food_bev_table.get('cart').where({'meal_course_id': meal_course_id});
		
		if(course_items == undefined || course_items.length == 0){
			$('#modal-fire-course').modal('hide');
			return false;
		}
		
		var table = App.data.food_bev_table;
		var copiedItems = [];
		var url = table.url + 'orders';
		
		// Clone the cart item to place in the order object
		_.each(course_items, function(item){
			var item_copy = item.toJSON();
			copiedItems.push(new FbCartItem(item_copy, {url: ''}));
		});
		
		// Create a new order with the list of selected items
		var order = new Order({
			'items': copiedItems, 
			'meal_course_id': meal_course_id,
			'table_name': table.get('name'),
			'table_number': table.get('table_num'),
			'guest_count': table.get('guest_count'),
			'customers': table.get('customers').toJSON()
		}, {'url': url});
		order.url = url;
		
		// Save order to back end
		order.save({}, {success: function(){
			table.set('orders_made', parseInt(table.get('orders_made')) + 1);
			
			// Update fired courses
			var fired_course = table.get('fired_meal_courses').get(meal_course_id);
			if(fired_course){
				fired_course.set({
					'date_fired': moment().format('YYYY-MM-DD HH:mm:ss'),
					'fired_employee_name': App.data.user.get('first_name') +' '+App.data.user.get('last_name'),
					'fired_employee_id': App.data.user.get('person_id')
				});
			}
		}});
		
		$('#modal-fire-course').modal('hide');
		return false;
	}
});

var FireMealCourseButtonListView = Backbone.Marionette.CollectionView.extend({
	tagName: 'div',
	childView: FireMealCourseButtonView,
	emptyView: FireMealCourseEmptyView
});

var FireMealCourseWindow = ModalView.extend({
	
	id: 'fire-course',
	template: JST['foodbev/fire_course_window.html'],

	initialize: function(attrs, options){
		ModalView.prototype.initialize.call(this, options);
	},
	
	render: function() {
		this.$el.html(this.template());
		this.buttonsView = new FireMealCourseButtonListView({collection: this.collection});
		this.$el.find('.modal-body').html(this.buttonsView.render().el);
		return this;
	},
	
	onHide: function(){
		this.buttonsView.destroy();
	}
});
