var ItemKitLayout = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	className: 'row page background-color-gradient',
	attributes: {
		id: 'page-item-kits'
	},
	template: JST['item_kits/item_kits.layout.html'],

	regions: {
		actions: '#item-kit-actions',
		table: '#item-kit-table'
	},

	events: {
		'click button.delete': 'deleteItemKits',
		'click button.export': 'export',
        'click button.print-barcode-sheet': 'printBarcodeSheet',
        'click button.print-barcodes': 'printBarcodes',
        'click button.new': 'newItemKit'
	},

	newItemKit: function(){
		var itemKit = new ItemKit();
		var itemKitWindow = new EditItemKitView({model: itemKit, collection: App.data.item_kits});
		itemKitWindow.show();		
	},

	getSelection: function(){
		var selection = $('table.item-kits').bootstrapTable('getSelections');
		if(!selection || selection.length == 0){
			App.vent.trigger('notification', {'msg': 'Items must be selected first', 'type': 'error'});
			return false;
		}
		return selection;
	},

	deleteItemKits: function(){
		
		var selection = this.getSelection();
		if(!selection){ return false; }

		_.each(selection, function(row){
			var model = App.data.item_kits.get(row.item_kit_id);
			if(model){
				model.destroy();
			}
		});

		return false;
	},

	printBarcodeSheet: function(){
		
		var selection = this.getSelection();
		if(!selection){ return false; }

		var itemKits = new Backbone.Collection();

		_.each(selection, function(row){
			var model = App.data.item_kits.get(row.item_kit_id);
			if(model){
				itemKits.add(model);
			}
		});		

		var barcodeWindow = new PrintBarcodesView({collection: itemKits, item_type: 'item_kit'});
		barcodeWindow.show();
		
		return false;
	},

    printBarcodes: function(){
        var selection = this.getSelection();
        if(!selection){ return false; }

        var itemKits = new Backbone.Collection();

        _.each(selection, function(row){
            var model = App.data.item_kits.get(row.item_kit_id);
            if(model){
                itemKits.add(model);
            }
        });

        var individualBarcodeWindow = new PrintIndividualBarcodesView({collection: itemKits, item_type: 'item_kit'});
        individualBarcodeWindow.show();

        return false;
    },

	export: function(){
		window.location = App.data.item_kits.url + '/csv?api_key=' + API_KEY;
		return false;
	}
});