var CardBalanceCheckWindow = ModalView.extend({
	
	tagName: "div",
	id: "card_balance_check",
	template: JST['card_balance_check.html'],
	etsTemplate: JST['ets_card_balance_check.html'],
	detailsTemplate: JST['card_balance_details.html'],

	initialize: function(options) {
		ModalView.prototype.initialize.call(this, options);
		
		this.listenTo(App.vent, 'ets-giftcard-balance', this.showDetails);						
	},

	events: {
		'submit form': 'submit',
		'click button.submit': 'submit'
	},

	render: function() {	
		
		var view = this;

		// If course is using ETS giftcards, show ETS iframe
		if(App.data.course.get('use_ets_giftcards') == 1){
			
			this.$el.html(this.etsTemplate());
			this.$el.loadMask();
			
			// Load iframe
			$.post(SITE_URL + '/v2/home/ets_giftcard_window', null, function(response){
				view.$el.find('div.modal-body').html(response);
				$.loadMask.hide();
			},'html');	
		
		}else{
			this.$el.html(this.template());
			this.$el.find('input.card-number').cardSwipe({
                parseData:!SETTINGS.nonfiltered_giftcard_barcodes
            });
		}
		return this;
	},
	
	onShow: function(){
		this.$el.find('input.card-number').trigger('focus');
	},
	
	submit: function(){
		
		var view = this;
		var number = this.$el.find('input.card-number').val();
		var type = this.$el.find('input[name="type"]:checked').val();
		
		var url = GIFTCARDS_URL;
		var params = {giftcard_number: number};
		if(type == 'punch_card'){
			url = PUNCH_CARDS_URL;
			params = {punch_card_number: number};
		}
		
		this.$el.loadMask();
		
		$.get(url, params, function(response){
			if(!response[0]){
				App.vent.trigger('notification', {msg: 'No card found', type: 'error'});
				view.$el.loadMask.hide();
				return false;
			}
			var attrs = response[0];
			attrs.type = type;
			
			view.showDetails(attrs);
		});
		
		return false;
	},
	
	showDetails: function(data){
		this.$el.html(this.detailsTemplate(data));
	}
});
