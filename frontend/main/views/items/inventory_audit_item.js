var InventoryAuditItemView2 = Backbone.View.extend({
    tagName: "tr",
    template: JST['items/inventory_audit_item.html'],

    events: {
        "click .add-one": "addOne",
        "click .subtract-one": "subtractOne",
        "focus .quantity": "selectAll"
        // "click": "selectItem",
        // "click button.add": "addQuantity",
        // "click button.sub": "subtractQuantity",
        // "click button.modifier": "toggleModifiers"
    },

    initialize: function(){
        // this.listenTo(this.model, "change", this.render);
        // this.listenTo(this.model, "change:incomplete", this.markIncomplete);
        // this.listenTo(this.model, "change:is_new", this.renderQuantityButtons);
        // this.listenTo(this.model.get('modifiers'), "change", this.render);
        // this.listenTo(this.model.get('sides'), "add remove change", this.render);
        // this.listenTo(this.model.get('soups'), "add remove change", this.render);
        // this.listenTo(this.model.get('salads'), "add remove change", this.render);
        // this.modifiersCollapsed = true;
    },

    render: function(){
        // console.log('rendering cart item...')
console.log('rendering item');
console.dir(this.model.attributes);
        this.$el.html(this.template(this.model.attributes));
        this.$el.addClass(this.model.attributes.item_id);
//        this.$el.addClass('not-hidden');
        // if(this.model.get('selected')){
        //     this.$el.addClass('selected');
        // }else{
        //     this.$el.removeClass('selected');
        // }
        //
        // if(this.model.get('is_ordered')){
        //     this.$el.addClass('ordered');
        // }else{
        //     this.$el.removeClass('ordered');
        // }
        // if(this.model.get('is_paid') || (this.model.get('comp_total') && this.model.get('comp_total')>0) ){
        //     this.$el.addClass('paid');
        // }else{
        //     this.$el.removeClass('paid');
        // }
        //
        // // If the item has any required modifiers or sides, then automatically
        // // pop up the edit window
        // if(!this.model.get('showed_edit') && (
        //         this.model.get('modifiers').findWhere({'required': true}) ||
        //         this.model.get('number_soups') > 0 ||
        //         this.model.get('number_salads') > 0 ||
        //         this.model.get('number_sides') > 0
        //     )){
        //     // Set flag in item, so window doesn't keep popping up later
        //     this.model.set({'showed_edit': true});
        //     this.openEditWindow();
        // }
        //
        // if(!this.modifiersCollapsed){
        //     this.expandModifiers();
        // }

        return this;
    },

    addOne: function(event){
//        this.model.set({'quantity': parseInt(this.model.get('quantity')) + 1});
        var currentQuantity = this.$el.find('.quantity').val();
        this.$el.find('.quantity').val(parseInt(currentQuantity) + 1);
        // this.model.save();
        return false;
    },

    subtractOne: function(event){
        // if((this.model.get('quantity') - 1) <= 0){
        //     return false;
        // }
        var currentQuantity = this.$el.find('.quantity').val();
        this.$el.find('.quantity').val(parseInt(currentQuantity) - 1);
        // this.model.set({'quantity': this.model.get('quantity') - 1});
        // this.model.save();
        return false;
    },

    selectAll: function(event){
        this.$el.find('.quantity').select();
    },

    renderQuantityButtons: function(){
        // if(this.model.get('is_new')){
        //     this.$el.find('div.quantity_buttons').show();
        // }else{
        //     this.$el.find('div.quantity_buttons').hide();
        // }
    },

    openEditWindow: function(e){
        // var item = this.model;
        // var editItemWindow = new FbEditItemView({model:item,receipt:item.collection.receipt});
        //
        // var highlightNotCompleted = false;
        // if(item.get('incomplete')){
        //     var highlightNotCompleted = true;
        // }
        //
        // editItemWindow.show();
        // if(e){
        //     e.preventDefault();
        // }
        return false;
    },

    openMessageWindow: function(){
        // var item = this.model;
        // var sendMessageWindow = new FbSendMessageView({model: item});
        // sendMessageWindow.show();

        return false;
    },

    selectItem: function(event){
        // if(this.model.get('is_ordered')){
        //     return false;
        // }
        //
        // if(this.model.get('selected')){
        //     this.model.set({"selected": false});
        //     this.$el.removeClass('selected');
        // }else{
        //     this.model.set({"selected": true});
        //     this.$el.addClass('selected');
        // }
        return false
    },

    markIncomplete: function(event){
        // if(this.model.get('incomplete')){
        //     this.$el.addClass('incomplete');
        // }else{
        //     this.$el.removeClass('incomplete');
        // }
        return false
    },

    toggleModifiers: function(event){
        // if(this.modifiersCollapsed){
        //     // expand the modifiers
        //     this.expandModifiers();
        // }
        // else{
        //     // collapse the modifiers
        //     this.collapseModifiers();
        // }
        //
        // if(event){
        //     event.preventDefault();
        //     return false;
        // }
    },

    expandModifiers: function(){
        // this.modifiersCollapsed = false;
        // this.$el.find('ul.modifiers').addClass('expanded');
    },

    collapseModifiers: function(){
        // this.modifiersCollapsed = true;
        // this.$el.find('ul.modifiers').removeClass('expanded');
    }
});
