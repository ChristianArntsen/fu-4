var PassItemRulesEmptyView = Backbone.Marionette.ItemView.extend({
	tagName: 'div',
	className: 'row',
	template: function(){
		return '<div class="col-md-12"><h3 class="text-muted pass-rule-placeholder">No rules created</h3></div>';
	}
});

var PassItemEditView = Backbone.Marionette.CompositeView.extend({
	
	template: JST['items/edit_pass_item.html'],
	childViewContainer: '#item-pass-rules',
	childView: PassItemRuleView,
	emptyView: PassItemRulesEmptyView,

	events: {
		'click .add-rule': 'add_rule',
		'click .pass-expiration': 'render_expiration'
	},

	add_rule: function(e){
		var new_rule = this.collection.add({});
		var rule_edit = new PassItemRuleEditView({model: new_rule});
		rule_edit.show();		
		return false;
	},

	onRender: function(){
		var view = this;
		this.render_expiration();
		this.$el.find('#pass-expiration-date').datetimepicker({
			format: 'MM/DD/YYYY'
		});

		this.$el.find('#item-pass-rules').sortable({
			placeholder: 'pass-rule-placeholder',
			cancel: 'a,button,.pass-rule-placeholder',
			update: _.bind(this.set_order, this)
		});		
	},

	set_order: function(){
		var order = this.$el.find('#item-pass-rules').sortable('toArray');
		var view = this;

		if(order && order.length){
			_.each(order, function(element_id, position){
				var rule_id = element_id.split('-')[1];
				view.collection.get(rule_id).set('rule_number', position + 1);
			});
		}

		view.collection.sort();
	},

	render_expiration: function(e){
		
		var expiration_type = this.$el.find('.pass-expiration:checked').val();
		if(expiration_type == 'days'){
			this.$el.find('#pass-expiration-date').attr('disabled', 'disabled').addClass('disabled');
			this.$el.find('#pass-expiration-days').attr('disabled', null).removeClass('disabled');
		}else{
			this.$el.find('#pass-expiration-days').attr('disabled', 'disabled').addClass('disabled');
			this.$el.find('#pass-expiration-date').attr('disabled', null).removeClass('disabled');			
		}
	}
});