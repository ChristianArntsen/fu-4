var SearchedForItemsView = EpoxyMarionetteView.extend({
	template: JST['items/searched_for_items_list.html'],
	tagName: 'li',
	className: 'upc',

	events: {
		"click button.delete": "removeItem"
	},
	computeds: {
		currencyFormatUnitPrice: function() {
			return accounting.formatMoney(this.getBinding("unit_price"));
		}
	},
	removeItem: function(){
		this.model.collection.remove(this.model);
	}
});

var SearchForItemsView = Backbone.Marionette.CompositeView.extend({
	template: JST['items/search_for_items.html'],
	childViewContainer: 'ul',
	childView: SearchedForItemsView,

	onRender: function(){
		
		var collection = this.collection;
		var field = this.$el.find('#item-search');

		init_item_search(field, function(e, item){
			collection.add(item);
			field.typeahead('val', '');
			return false;
		});
	},

});