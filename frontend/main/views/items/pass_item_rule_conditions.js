var PassItemRuleConditionView = Backbone.Marionette.LayoutView.extend({
	
	tagName: "div",
	className: "row",
	template: JST['items/pass_item_rule_condition_edit.html'],

	regions: {
		filters: '#condition-filter',
		values: '#condition-value'
	},

	events: {
		'click .add-value': 'add_value',
		'click .add-filter': 'add_filter',
		'click .condition-delete': 'delete_condition',
		'change .condition-field': 'set_field',
		'change .condition-operator': 'set_operator',
		'change input.condition-value': 'set_scalar_value'
	},

	modelEvents: {
		'change:field': 'render'
	},

	init_data: function(){
		
		var view = this;
		
		this.teesheets = [];
		this.scalar_value = false;
		this.can_filter = false;
		this.price_classes = App.data.course.get('price_classes');

		this.valid_operators = {
			'=': '='
		};

 		this.fields = {
			false: '- Select Field -',
			'teesheet': 'Course',
			'timeframe': 'Date/Time',
			'total_used': 'Total Used',
			'total_used_per_day': 'Uses Per Day',
			'total_used_per_week': 'Uses Per Week',
			'total_used_per_month': 'Uses Per Month',
			'total_used_per_year': 'Uses Per Year',
			'days_since_purchase': 'Days Since Purchase'
 		};

		var teesheets = App.data.course.get('teesheets');
		_.each(teesheets.models, function(teesheet){
			view.teesheets.push({
				'field': 'teesheet',
				'label': teesheet.get('course_name') +' - '+ teesheet.get('title'),
				'id': teesheet.get('teesheet_id')
			});
		});

		switch(this.model.get('field')){
			case 'total_used':
			case 'total_used_per_day':
			case 'total_used_per_week':
			case 'total_used_per_month':	
			case 'total_used_per_year':
			case 'days_since_purchase':
				view.can_filter = true;
				view.scalar_value = true;
				view.valid_operators = {
					'<' : '<',
					'<=' : '<=',
					'=' : '=',
					'>=' : '>=',
					'>' : '>'
		 		};
			break;	
		}

		if(this.model.get('field') == 'days_since_purchase'){
			this.can_filter = false;
		}
	},

	serializeData: function(){
		
		var data = this.model.toJSON();
		this.init_data();

		data.teesheets = this.teesheets;
		data.price_classes = this.price_classes;
		data.scalar_value = this.scalar_value;
		data.valid_operators = this.valid_operators;
		data.fields = this.fields;
		data.can_filter = this.can_filter;

		return data;
	},

	onRender: function(){

		if(this.model.get('field') == 'teesheet' || this.model.get('field') == 'timeframe'){
			var values = new PassItemRuleConditionParametersView({
				collection: this.model.get('value'),
				childViewOptions: {
					edit: true
				},
				emptyViewOptions: {
					type: 'value'
				}
			});
			this.values.show(values);
		
		}else if(this.model.get('field') == 'days_since_purchase'){
			var total_field = $('<input type="text" class="form-control condition-value" name="value" value="">');
			total_field.val(this.model.get('value'));
			this.$el.find('#condition-value').html( total_field );

		}else if(this.scalar_value){
			var filters = new PassItemRuleConditionParametersView({
				collection: this.model.get('filter'),
				childViewOptions: {
					edit: true
				},
				emptyViewOptions: {
					type: 'filter'
				}
			});
			this.filters.show(filters);

			var total_field = $('<input type="text" class="form-control condition-value" name="value" value="">');
			total_field.val(this.model.get('value'));
			this.$el.find('#condition-value').html( total_field );
		}
	},

	set_field: function(){
		var field = this.$el.find('select.condition-field').val();
		this.model.set('field', field);
		this.init_data();
	},

	set_operator: function(){
		var operator = this.$el.find('select.condition-operator').val();
		this.model.set('operator', operator);
	},

	set_scalar_value: function(){
		var value = this.$el.find('input.condition-value').val();
		this.model.set('value', value);
	},

	get_parameter: function(field, value_id){
		
		var value = 0;
		switch(field){
			case 'timeframe':
				var value = new PassItemRuleConditionValue({
					label: 'Anytime',
					field: 'timeframe',
					day_of_week: [1,2,3,4,5,6,7],
					date: false,
					time: false
				});
			break;
			case 'teesheet':
				var data = this.teesheets[value_id];
				var value = new PassItemRuleConditionValue(data);				
			break;
			case 'price_class':
				var data = {
					label: this.price_classes[value_id],
					id: value_id,
					field: 'price_class'
				};
				var value = new PassItemRuleConditionValue(data);				
			break;
		}

		return value;
	},

	add_filter: function(e){
		
		var item = $(e.currentTarget);
		var field = item.data('field');
		var value_id = item.data('value-id');
		var filter = this.get_parameter(field, value_id);

		this.model.get('filter').add(filter);
		e.preventDefault();
	},

	add_value: function(e){
		
		var item = $(e.currentTarget);
		var field = item.data('field');
		var value_id = item.data('value-id');	
		var value = this.get_parameter(field, value_id);

		if(value){
			this.model.get('value').add(value);
		}else{
			this.model.set('value', value);
		}
		
		e.preventDefault();
	},

	delete_condition: function(){
		this.model.collection.remove(this.model);
		return false;
	}
}); 

var PassItemRuleConditionsEmptyView = Backbone.Marionette.ItemView.extend({
	tagName: 'div',
	className: 'row',
	template: function(){
		return '<div class="col-md-12"><h3 class="text-muted empty-conditions">No conditions created</h3></div>';
	}
});

var PassItemRuleConditionsView = Backbone.Marionette.CollectionView.extend({
	childView: PassItemRuleConditionView,
	emptyView: PassItemRuleConditionsEmptyView,
	tagName: "div",
	className: "col-md-12"
});