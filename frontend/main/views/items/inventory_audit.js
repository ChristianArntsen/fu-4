var InventoryAuditView2 = ModalLayoutView.extend({

    id: 'inventory_audit_window',
    template: JST['items/inventory_audit.html'],

    events: {
        "change #category-or-department": "update_list",
        "change #inventory-audit-dropdown" : "load_audit",
        "click #new_audit_button" : "open_new_audit",
        "click #past_audits_button" : "open_past_audits",
        "click button.save-and-close": "save_and_close",
        "click button.save-and-finish": "save_and_finish"
    },

    initialize: function(){
        var options = {
            extraClass: 'modal-lg',
            closable: false
        };
        ModalView.prototype.initialize.call(this, options);

        this.temp_model = new InventoryAudit(this.model.toJSON());
        this.startListening();
    },

    startListening: function(){
        // var self = this;
        //
        // this.listenTo(this.temp_model, 'change:unit_price change:max_discount', this.render_minimum_price);
        // this.listenTo(this.temp_model, 'change:inventory_unlimited', this.render_inventory_unlimited);
        // this.listenTo(this.temp_model, 'change:item_type', this.render);
        //
        // this.listenTo(this.model, 'sync', function(model){
        //     self.temp_model.set(model.toJSON());
        // });
        //
        // this.listenTo(this.model, 'error', App.errorHandler);
    },

    update_list: function(){
        var value = this.$el.find('#category-or-department option:selected').text();
        var category_or_department = this.$el.find('#category-or-department option:selected').data('type');
        // Filter list
        if (category_or_department == 'department') {
            var item_list = App.data.items.where({department: value});
        } else {
            var item_list = App.data.items.where({category: value});
        }

        this.$el.find('#inventory-list tr').hide();
        if (value == 'All Departments and Categories') {
            this.$el.find('#inventory-list tr').show();
        }
        else {
            _.each(item_list, function(item){
                $('.'+item.get('item_id')).show();
            });
        }
        this.$el.find('#inventory-list tr').css('backgroundColor', '');
        this.$el.find('#inventory-list tr:visible').each(function (i) {
            if ((i+1) % 2 == 0) $(this).css('backgroundColor', '#f5f5f5');
        });
    },

    load_audit: function(){
        var audit_id = this.$el.find('#inventory-audit-dropdown').val();
        // Fetch audit info
        var that = this;
        // Create inventory audit and get info instead of ajax call
        // $.get(API_URL + '/items/audit_items/', {audit_id: audit_id}, function(response){
        //     // button.button('reset');
        //     // if(response && response[0]){
        //     //     App.vent.trigger('notification', {msg: 'UPC already in use', type: 'error'});
        //     // }else{
        //     //     self.collection.add( new ItemUPC({upc: upc}) );
        //     //     field.val('');
        //     // }
        //     that.$el.find('#audit-inventory-list tbody').html('');
        //     _.each(response, function (audit_item) {
        //         // var department = item.get('department')
        //         // depts[department.toLowerCase()] = department;
        //         // var category = item.get('category');
        //         // categories[category] = category;
        //         // that.addItem(item);
        //         console.log('audit item');
        //         console.dir(audit_item);
        //         that.$el.find('#audit-inventory-list tbody').append("<tr>" +
        //                 "<td>"+(audit_item.item_number == null ? '' : audit_item.item_number)+"</td>"+
        //                 "<td>"+audit_item.name+"</td>"+
        //                 "<td>"+audit_item.current_count+"</td>"+
        //                 "<td>"+audit_item.manual_count+"</td>"+
        //                 "<td>"+audit_item.difference+"</td>"+
        //                 "<td>"+audit_item.rev_diff+"</td>"+
        //                 "<td>"+audit_item.cost_diff+"</td>"+
        //             "</tr>");
        //     });
        //     that.$el.find('#audit-inventory-list tbody tr:visible').each(function (i) {
        //         if ((i+1) % 2 == 0) $(this).css('backgroundColor', '#f5f5f5');
        //     });
        // },'json');
    },

    open_new_audit: function(e){
        e.preventDefault();
        // Highlight button
        // Change dropdown
        // Change data

    },

    open_past_audits: function(audit_id){
        e.preventDefault();
        // Highlight button
        // Change dropdown
        // Change data
        // Load audit
        if (typeof audit_id != 'undefined') {
            this.$el.find('#inventory-audit-dropdown').val(audit_id);
            this.load_audit();
        }
    },

    serializeData: function(){
        //return this.temp_model.toJSON();
    },

    onRender: function(){

        var that = this;
        var depts = {};
        var categories = {};
        this.$el.find('#department_opt_group').html('');
        this.$el.find('#category_opt_group').html('');
        window.parent.App.data.items.fetch({
            data:{limit:10000},
            success: function(model){
//                _.each(that.collection.models, function (item) {
                _.each(window.parent.App.data.items, function (item) {
                    var department = item.get('department')
                    depts[department.toLowerCase()] = department;
                    var category = item.get('category');
                    categories[category] = category;
                    //that.addItem(item);
                    var audit_item = new InventoryAuditItem({
                        item_id: item.get('item_id'),
                        manual_count: 0,
                        current_count: item.get('')
                    });
                    inventory_audit_items.add(audit_item);
                    that.collection.add(inventoryItem);
                    //that.temp_model.addItem(item);
                });
                _.sortKeysBy(depts);
                _.each(depts, function(dept) {
                    console.log(dept);
                    that.$el.find('#department_opt_group').append('<option data-type="department" value="'+dept+'">'+dept+'</option>');
                });
                _.sortKeysBy(categories);
                _.each(categories, function(cat) {
                    console.log(cat);
                    that.$el.find('#category_opt_group').append('<option data-type="category" value="'+cat+'">'+cat+'</option>');
                });
                that.$el.find('#inventory-list tr:visible').each(function (i) {
                    if (i % 2 == 0) $(this).css('backgroundColor', '#f5f5f5');
                });

            }
        });
        $.get(API_URL + '/items/audits/', {}, function(response){
            console.dir(response);
            _.each(response, function(ia) {
                console.log(ia);
                that.$el.find('#inventory-audit-dropdown').append('<option data-type="" value="'+ia.inventory_audit_id+'">'+ia.label+'</option>');
            });
        },'json');


        return this;
    },

    addItem: function(item){
        this.$el.find('table#inventory-list tbody').append( new InventoryAuditItemView({model:item}).render().el );
    },

    save_and_close: function(e){
        this.save(e);
    },

    save_and_finish: function(e){
        this.save(e);
    },

    save: function(e){
        e.preventDefault();
        var view = this;

        // Validate that required fields are filled in
        var form = this.$el.find('#inventory-audit-form');
        console.dir(form);
        form.bootstrapValidator('validate');

        if(!form.data('bootstrapValidator').isValid()){
            return false;
        }
        var inventoryAudit = new window.parent.InventoryAudit({inventory_audit_id:3});
        console.log('inventoryAudit - testing');
        console.dir(inventoryAudit);
        inventoryAudit.save();

        this.$el.loadMask();

        var data = this.getFormData(form);
        //data.cost_price = _.getNumber(data.cost_price?data.cost_price:'0.00');
        //data.unit_price = _.getNumberWNegative(data.unit_price);
        //this.model.set(data);
        // if(this.model.isNew() && this.collection){
        //     this.collection.add(this.model);
        // }

        // this.model.save(null, {wait: true,
        //     success: function(){
        //         view.hide();
        //     },
        //     error: function(){
        //         $.loadMask.hide();
        //     }
        // });
        console.log('----fetching form data ------------');
        console.dir(data);
        $.loadMask.hide();
        return false;
    },

    getFormData: function(form){
        var data = JSON.stringify( $(form).serializeArray() );

        return data;
    }
});