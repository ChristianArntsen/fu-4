var ItemServiceFeeView = Backbone.Marionette.ItemView.extend({
	template: JST['items/item_service_fee.html'],
	tagName: 'li',
	className: 'upc',

	events: {
		"click button.delete": "removeServiceFee"
	},

	removeServiceFee: function(){
		this.model.collection.remove(this.model);
	}
});

var ItemServiceFeesView = Backbone.Marionette.CompositeView.extend({
	template: JST['items/item_service_fees.html'],
	childViewContainer: 'ul',
	childView: ItemServiceFeeView,

	onRender: function(){
		
		var collection = this.collection;
		var self = this;
		var field = this.$el.find('#service-fee-search');

		init_item_search(field, function(e, item){
			collection.add(item);
			field.typeahead('val', '');
			return false;
		}, {
			item_type: 'service_fee'
		});

		this.$el.find('ul').sortable({
			cancel: '',
			distance: 5,
			update: function(){
				self.saveSort();
			}
		});
	},

	saveSort: function(){
		
		var collection = this.collection;

		this.$el.find('ul > li').each(function(){
			var position = $(this).index();
			var id = $(this).find('.fa-sort').data('service-fee-id');
			collection.get(id).set('order', position);	
		});
	},

	onAddChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	},

	onRemoveChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	}
});