var ItemLayout = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	className: 'row page background-color-gradient',
	attributes: {
		id: 'page-items'
	},
	template: JST['items/items.layout.html'],

	regions: {
		actions: '#item-actions',
		table: '#item-table'
	},

	events: {
		'click button.delete': 'deleteItems',
		'click button.export': 'export',
        'click button.print-barcode-sheet': 'printBarcodeSheet',
        'click button.print-barcodes': 'printBarcodes',
        'click button.new': 'newItem',
		'click button.receipt-agreements': 'receiptAgreements'
	},

	newItem: function(){
		var item = new Item();
		item.set('taxes', _.clone(App.data.course.get('default_taxes')));

		var item_window = new EditItemView({model: item, collection: App.data.items});
		item_window.show();		
	},

	receiptAgreements: function(){
		var modal = new window.parent.ItemReceiptContentModalView({
			collection: window.parent.App.data.item_receipt_content
		});
		modal.show();
		return false;
	},

	getSelection: function(){
		var selection = $('table.items').bootstrapTable('getSelections');
		if(!selection || selection.length == 0){
			App.vent.trigger('notification', {'msg': 'Items must be selected first', 'type': 'error'});
			return false;
		}
		return selection;
	},

	deleteItems: function(){
		
		var selection = this.getSelection();
		if(!selection){ return false; }

		_.each(selection, function(row){
			var model = App.data.items.get(row.item_id);
			if(model){
				model.destroy();
			}
		});

		return false;
	},

	printBarcodeSheet: function(){
		
		var selection = this.getSelection();
		if(!selection){ return false; }

		var items = new Backbone.Collection();

		_.each(selection, function(row){
			var model = App.data.items.get(row.item_id);
			if(model){
				items.add(model);
			}
		});		

		var barcodeWindow = new PrintBarcodesView({collection: items});
		barcodeWindow.show();
		
		return false;
	},

    printBarcodes: function(){
        var selection = this.getSelection();
        if(!selection){ return false; }

        var items = new Backbone.Collection();

        _.each(selection, function(row){
            var model = App.data.items.get(row.item_id);
            if(model){
                items.add(model);
            }
        });

        var individualBarcodeWindow = new PrintIndividualBarcodesView({collection: items});
        individualBarcodeWindow.show();

        return false;
    },

	export: function(){
		window.location = App.data.items.url + '/csv?api_key=' + API_KEY;
		return false;
	}
});