/**
 * Created by Nick on 7/10/2017.
 */
var InstallExtensionPromptView = ModalView.extend({

    id: 'install_extension_prompt',
    template: JST['install_extension_prompt.html'],

    events: {
        "click button.add_to_chrome": "addToChrome"
    },

    initialize: function() {
        var options = {
            extraClass: 'modal-lg'
        };
        ModalView.prototype.initialize.call(this, options);
        this.customer = false;
    },

    render: function(){
        var model = this.model;
        var attrs = model.attributes;
        this.$el.html(this.template(attrs));
        return this;
    },

    addToChrome: function(){
        //uses chrome API and will pull a label from the pages header to figure out what extension to install
        chrome.webstore.install();
        this.hide();
    }

});