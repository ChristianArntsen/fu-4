var RefundSalePaymentView = ModalView.extend({

	id: 'refund_sale_payment_window',
	template: JST['sales/refund_sale_payment_window.html'],

	events: {
		"click button.issue-refund": "refund"
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
	},

    render: function(){
		var attrs = this.model.attributes;
        attrs.refund_reasons = App.data.course.get('refund_reasons');
		this.$el.html(this.template(attrs));
		return this;
    },

    refund: function(){
    	
    	var url = API_URL + '/sales/' + this.model.get('sale_id') +'/payments/' + this.model.get('invoice_id');
    	var refund_amount = accounting.unformat(this.$el.find('input.refund').val());
    	var cc_invoice_id = this.model.get('invoice_id');
    	var view = this;
    	var model = this.model;

        if(!refund_amount){
            App.vent.trigger('notification', {msg: 'Refund amount is not valid', type: 'error'});
            return false;
        }

        var refund_amount = new Decimal(refund_amount);
        var max_amount = new Decimal(this.model.get('amount'));

        if(refund_amount.greaterThan(max_amount)){
            this.$el.find('input.refund').val(this.model.get('amount'));
            App.vent.trigger('notification', {msg: 'Refund amount can not be greater than original amount', type: 'error'});
            return false;            
        }

    	this.$el.loadMask({message: 'Refunding payment...'});
    	$.ajax({
    		method: 'PUT',
    		url: url,
    		data: JSON.stringify({refund_amount: refund_amount}),
    		dataType: 'json',
    		contentType: 'application/json',
    		processData: false

    	// If refund is successful
    	}).done(function(response){
    		
    		var payment = {
    			amount: -refund_amount,
    			record_id: cc_invoice_id,
    			description: 'CC Refund',
    			type: 'credit_card_refund',
                refund_comment : view.$el.find('#refund-comment').val(),
                refund_reason : view.$el.find('#refund-reason option:selected').text()
            };

    		// Update the old payment with amount refunded
			if(!model.attributes.params){
				model.attributes.params = {};
			}

    		model.attributes.params.amount_refunded = refund_amount;

    		// Add partial refund payment to cart
    		App.data.cart.get('payments').addPayment(payment, function(){
    			view.hide();
    		});
    	
    	}).fail(function(response){
    		$.loadMask.hide();
    		console.debug(response);
    		App.vent.trigger('notification', {type: 'error', msg: response.responseJSON.msg});
    	});

    	return false;
    }
});
