var EtsGiftCardRefundWindow = ModalView.extend({
	
	tagName: "div",
	id: "ets_gift_card_refund",
	template: JST['sales/ets_gift_card_refund.html'],

	initialize: function(options) {
		ModalView.prototype.initialize.call(this, options);	
		this.listenTo(App.vent, 'ets-giftcard-void', this.processRefund);			
	},

	render: function() {	
		
		var view = this;
		this.$el.html(this.template());
		this.$el.loadMask();
		
		var data = {
			'action': 'refund',
			'amount': this.model.get('amount'),
			'type': 'gift_card',
			'giftcard_void': true
		};
		
		// Load ETS iframe
		$.post(SITE_URL + '/v2/home/credit_card_window', data, function(response){
			view.$el.find('div.modal-body').html(response);
			$.loadMask.hide();
		},'html');
		
		return this;
	},
	
	processRefund: function(card){
		
		// Delete payment from cart
		this.model.destroy();
		App.vent.trigger('notification', {'msg': 'Refund successful', 'type': 'success'});
		this.hide();
		
		return false;
	}
});
