var POSHeaderStatsView = Backbone.Marionette.ItemView.extend({
	tagName: "div",
	className: "header-stats",
	
	initialize: function(){
		this.listenTo(this.model, 'change', this.render);
		this.listenTo(App.data.user, 'change:person_id', this.render);
	},

	getTemplate: function(data){
		if(App.data.user.get('sales_stats') == 1){
			return JST['sales/pos_header_stats.html'];
		}else{
			return function(){ return '' };
		}
	}
});
var TeesheetHeaderStatsView = Backbone.Marionette.ItemView.extend({
    tagName: "div",
    className: "header-stats",

    initialize: function(){
    	var self = this;

        this.listenTo(App.vent, 'teesheet:datechanged',function(data){
            self.model.fetch({
                data:{
                    "start":data.start
                }
            });
        });
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(App.data.user, 'change:person_id', this.render);
    },

    getTemplate: function(data){
        return JST['sales/teesheet_header_stats.html'];
    }
});