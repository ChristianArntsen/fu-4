var IssuePassView = ModalView.extend({

	id: 'issue_pass_window',
	template: JST['sales/issue_pass_window.html'],

	events: {
		"click button.save": "save",
		"keyup #pass-customer": "clearCustomer"
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
		this.customer = false;
	},

    render: function(){
		var attrs = this.model.attributes;
		if(!attrs.params.start_date){
			attrs.params.start_date = moment().format('MM/DD/YYYY');
		}
		
		if(!attrs.params.end_date && attrs.pass_days_to_expiration){
			attrs.params.end_date = moment().add(attrs.pass_days_to_expiration,'days').format('MM/DD/YYYY');
		
		}else if(!attrs.params.end_date && attrs.pass_expiration_date){
			attrs.params.end_date = moment(attrs.pass_expiration_date).format('MM/DD/YYYY');
		}

		this.$el.html(this.template(attrs));

		if(attrs.params && attrs.params.customer){
			this.customer = attrs.params.customer;
		}

		var view = this;
		var model = this.model;
		var form = this.$el.find('form');

		init_customer_search(this.$el.find('#pass-customer'), function(e, customer, list){
			view.$el.find('#pass-customer-id').val(customer.person_id);
			view.$el.find('#pass-customer').val(customer.first_name +' '+ customer.last_name).data('customer', customer);
			view.customer = _.pick(customer, ['first_name', 'last_name', 'person_id']);
			return false;
		});		

		this.$el.find('#pass-start-date').datetimepicker({
			format: 'MM/DD/YYYY'
		});

		this.$el.find('#pass-end-date').datetimepicker({
			format: 'MM/DD/YYYY',
			useCurrent: false
		});

		this.$el.find('#pass-start-date').on('dp.change', function(e){
			form.bootstrapValidator('resetForm');
			view.$el.find('#pass-end-date').data('DateTimePicker').minDate(e.date);
		});

		this.$el.find('#pass-end-date').on('dp.change', function(e){
			form.bootstrapValidator('resetForm');
			view.$el.find('#pass-start-date').data('DateTimePicker').maxDate(e.date);
		});
		
		return this;
    },
	
	save: function(e){
		
		e.preventDefault();
		var view = this;

		var customer_id = false;
		if(this.customer){
			customer_id = this.customer.person_id;
		}

		if(!customer_id){
			view.$el.find('#pass-customer').typeahead('val', '');
			view.$el.find('#pass-customer').val('');
		}

		// Validate that required fields are filled in
		var form = this.$el.find('form');
		form.bootstrapValidator('validate');

		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}

		if(!customer_id){
			App.vent.trigger('notification', {msg: 'Customer is required', 'type': 'danger'});
			return false;
		}

		var pass_data = {
			"start_date": view.$el.find('input[name="start_date"]').val() + '00:00:00',
			"end_date": view.$el.find('input[name="end_date"]').val() + '23:59:59',
			"customer": this.customer,
			"customer_id": customer_id
		};
		
		var item_data = {
			"params": pass_data
		};

		this.model.set(item_data).save();
		this.hide();

		return false;
	},

	clearCustomer: function(e){
		if(e.keyCode == 13){
			return true;
		}
		this.$el.find('#pass-customer-id').val('');
		this.customer = false;
		return true;
	},

	onHide: function(){
		var params = this.model.get('params');
		if(!params || !params.customer){
			this.model.destroy();
		}
	}
});