var SaleLayout = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	className: 'row page',
	attributes: {
		id: 'page-sales'
	},
	template: JST['sales/sale.layout.html'],

	events: {
		"click #issue-raincheck": "handleClickedIssueRaincheck"
	},
	
	initialize: function(){
		var view = this;
		$(document).off('click.manager-override').on('click.manager-override', 'button.pos-manager-override', function(e){
			view.manager_override(e);
		});
		
		this.check_register_log();
	},
	
	check_register_log: function(){
		App.data.register_logs.prompt_to_open_log();		
	},
	
	regions: {
		cart_list: '#cart-list',
		cart_totals: '#cart-totals',
		sale_customers: '#sale-customers',
		sale_payments: '#sale-payments',
		recent_transactions: '#recent-transactions',
		suspended_sales: '#suspended-sales',
		incomplete_sales: '#incomplete-sales',
		quick_buttons: '#sales-quick-buttons'
	},

	handleClickedIssueRaincheck:function(e){
		this.issueRaincheck();
	},
	issueRaincheck: function(raincheck_data){
	   var course = App.data.course;
		var teesheet = course.get('teesheets').at(0);

        if (typeof raincheck_data == 'undefined') {
            raincheck_data = {
                teesheet: teesheet,
                green_fee: null,
                cart_fee: null
            };
        }

		var raincheck = new Raincheck(raincheck_data);
		var view = new IssueRaincheckView({model: raincheck});
		view.show();
	},
	
	manager_override: function(e){
		var window = new ManagerOverrideWindow();
		window.show();
	}
});
