var SuspendedSalesListView = Backbone.View.extend({

	tagName: 'div',
	className: 'panel panel-default',
	template: JST['sales/suspended_sales.html'],

	initialize: function(options){
		if(options && options.incomplete){
			this.incomplete = options.incomplete;
		}
		this.listenTo(this.collection, 'add remove reset', this.render);
		if(this.incomplete){
			this.listenTo(this.incomplete, 'add remove reset', this.render);
		}
	},

	events: {
		"click .panel-heading": "togglePanel"
	},

	render: function(){
		this.$el.html( this.template() );
		var transactions = this.$el.find('ul.suspended');
		var incomplete = this.$el.find('ul.incomplete');
		transactions.html('');
		incomplete.html('');

		if(this.collection.length > 0){
			_.each(this.collection.models, function(sale){
				var suspended_sale = new SuspendedSaleView({model: sale});
				transactions.append( suspended_sale.render().el );
			});
		}

		if(this.incomplete && this.incomplete.length > 0){
			_.each(this.incomplete.models, function(sale){
				var incomplete_sale = new SuspendedSaleView({model: sale});
				incomplete.append( incomplete_sale.render().el );
			});
		}else{
			this.$el.find('div.incomplete-container').hide();
		}

		return this;
	},
	
	togglePanel: function(event){
		if(this.$el.find('.glyphicon-minus-sign').is(':visible')){
			this.hide();
		}else{
			this.show();
		}
	},	
	
	hide: function(event){
		this.$el.find('.panel-body').hide();
		this.$el.find('.glyphicon-minus-sign').removeClass('show').addClass('hidden');
		this.$el.find('.glyphicon-plus-sign').removeClass('hidden').addClass('show');
	},
	
	show: function(event){
		this.$el.find('.panel-body').show();
		this.$el.find('.glyphicon-plus-sign').removeClass('show').addClass('hidden');
		this.$el.find('.glyphicon-minus-sign').removeClass('hidden').addClass('show');
	}
});

var SuspendedSaleView = Backbone.View.extend({

	tagName: 'li',
	className: '',
	attributes: {

	},
	template: JST['sales/suspended_sale.html'],

	initialize: function(){
		this.listenTo(this.model, 'change:total', this.render);
	},

	events: {
		'click a.print': 'printReceipt',
		'click a.delete': 'delete',
		'click a.kitchen': 'printTicket',
		'click': 'resumeSale'
	},

	render: function(){
		var view = this;
		this.$el.html(this.template(this.model.attributes));
		this.$el.contextmenu({
			target: view.$el.find('.context-menu')
		});
		return this;
	},

	resumeSale: function(e){
		
		if(e.target.className == 'print' || e.target.className == 'delete'){
			return true;
		}
		var data = this.model.toJSON();
		var suspended_cart = this.model;
		data.status = 'active';
		
		var customers = _.clone(data.customers);
		var items = _.clone(data.items);
		var payments = _.clone(data.payments);
		var sale_payments = _.clone(data.sale_payments);

		delete data.customers;
		delete data.items;
		delete data.payments;
		delete data.sale_payments;

		var collection = suspended_cart.collection;

		suspended_cart.save({'status':'active'}, {success: function(){
			
			// If the current active cart is a suspended cart, add it back
			// to the suspended list
			if(App.data.cart.get('suspended_name') != '' && App.data.cart.get('suspended_name') != null){
				var current_cart = App.data.cart.toJSON();
				current_cart.status = 'suspended';
				suspended_cart.collection.add(current_cart);
			}
			
			App.data.cart.clear();
			App.data.cart.set(data);
			App.data.cart.get('items').reset(items, {cart: App.data.cart});
			App.data.cart.get('payments').reset(payments, {cart: App.data.cart});
			App.data.cart.get('customers').reset(customers, {cart: App.data.cart});
			App.data.cart.get('sale_payments').reset(sale_payments, {cart: App.data.cart});			
			suspended_cart.collection.remove(suspended_cart);			
		
		}, error: function(model, response){
			collection.remove(model);
			collection.fetch({
				reset: true, 
				data: {status: collection.status}
			});

			App.vent.trigger('notification', {'msg': response.responseJSON.msg, 'type':'error'});
		}});

		return false;
	},
	
	delete: function(e){
		
		if(this.model.get('payments').length > 0){
			if(!confirm('This sale contains partial payments, deleting it will refund all payments.')){
				return false;
			}			
		}

		this.model.destroy();
		return false;
	},

	printTicket: function(e){
		e.preventDefault();
		// print ticket to kitchen printer
		this.model.sendFBOrder();
		$('#page-sales').trigger('click');
		return false;
	},

	printReceipt: function(e){
		
		e.preventDefault();
		
		var receipt_data = this.model.toJSON();
		var customer = this.model.get('customers').findWhere({'selected': true});
		
		if(customer != undefined){
			customer = customer.toJSON();
		}
		
		receipt_data.sale_time = moment().format('YYYY-MM-DD HH:mm:ss');
		receipt_data.employee = App.data.user.toJSON();
		receipt_data.customer = customer;
		
		var sale = new Sale(receipt_data);
		sale.printReceipt('suspended_sale');
	},

	edit: function(e){
		e.preventDefault();
		var window = new EditSaleWindowView({model: this.model});
		window.show();
	}
});