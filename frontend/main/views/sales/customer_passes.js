var CartCustomerPassRuleView = Backbone.Marionette.ItemView.extend({
	template: JST['sales/customer_pass_rule.html'],
	tagName: 'li',
	className: 'pass-rule',

	events: {
		'click button.apply-pass-rule': 'applyPassRule'
	},

	initialize: function(options){
		this.pass = false;
		if(options && options.pass){
			this.pass = options.pass;
		}		
	},

	applyPassRule: function(){
		this.model.applyRule(this.pass);
	}
});

var CartCustomerPassRulesView = Backbone.Marionette.CollectionView.extend({
	tagName: 'ul',
	className: 'pass-restrictions',
	childView: CartCustomerPassRuleView,

	initialize: function(options){
		this.pass = false;
		if(options && options.pass){
			this.pass = options.pass;
		}
	},

	childViewOptions: function(){
		return {pass: this.pass};
	}
});

var CartCustomerPassView = Backbone.Marionette.LayoutView.extend({
	template: JST['sales/customer_pass.html'],
	tagName: 'li',
	className: 'pass',
	childViewContainer: 'div.pass-restrictions',

	regions: {
		'Rules': 'div.pass-restrictions-container'
	},

 	initialize: function(){
 		this.show_rules = false;
 		this.listenTo(this.model.get('rules'), 'change:is_applied', _.throttle(this.render, 10, {leading: false}));
 	},

	onRender: function(){
		this.Rules.show( 
			new CartCustomerPassRulesView({
				collection: this.model.get('rules'),
				pass: this.model
			})
		);
	},

	serializeData: function(){
		var data = this.model.toJSON();
		data.show_rules = this.show_rules;
		return data;
	},

	events: {
		'click .show-pass-details': 'toggleDetails',
		'click button.apply-pass': 'applyPass'
	},

	toggleDetails: function(){
		this.show_rules = !this.show_rules;
		this.$el.find('div.pass-restrictions-container').toggle();
		return false;
	},

	applyPass: function(){

		var first_valid_rule = this.model.get('rules').findWhere({is_valid: true});
		var applied_rule = this.model.get('rules').findWhere({is_applied: true});
		
		if(applied_rule){
			applied_rule.applyRule(this.model);
		}else if(first_valid_rule){
			first_valid_rule.applyRule(this.model);
		}
		
		return false;
	}
});

var CartCustomerPassesView = Backbone.Marionette.CollectionView.extend({
	tagName: 'ul',
	className: 'customer-info',
	childView: CartCustomerPassView
});