var RecentTransactionView = Backbone.Marionette.ItemView.extend({

	tagName: 'li',
	className: 'clearfix',
	template: JST['sales/recent_transaction.html'],

	initialize: function(){
		this.listenTo(this.model, 'change', this.render);
	},

	events: {
		'click .edit': 'edit',
		'click a.print': 'printReceipt',
		'click a.email': 'emailReceipt',
		'click a.tip': 'addTip'
	},

	onRender: function(){
		var view = this;
		if(this.$el.contextmenu){
			//this.$el.contextmenu('destroy');
		}

		this.$el.contextmenu({
			target: view.$el.find('.context-menu')
		});
	},

	printReceipt: function(e){
		e.preventDefault();
		
		if(App.data.course.get_setting('auto_print_receipts') == 1){
			this.model.printReceipt();
		
		}else{
			var receipt_window = new ReceiptWindowView({model: this.model});
			receipt_window.show();
			
			// Delay so barcode image will load before printing
			setTimeout(function(){
				window.print();
			}, 200);
		}			
	},
	
	emailReceipt: function(e){
		e.preventDefault();
		var window = new EmailReceiptWindowView({model: this.model});
		window.show();
	},	

	addTip: function(e){
		e.preventDefault();
		var window = new TipWindowView({model: this.model});
		window.show();
	},

	edit: function(e){
		e.preventDefault();
		var window = new EditSaleWindowView({model: this.model});
		window.show();
	}
});

var RecentTransactionEmpty = Backbone.Marionette.ItemView.extend({
	template: function(){
		return '<h5 class="text text-muted">No transactions today</h5>';
	}
})

var RecentTransactionListView = Backbone.Marionette.CompositeView.extend({

	tagName: 'div',
	className: 'panel panel-default',
	template: JST['sales/recent_transactions.html'],
	childView: RecentTransactionView,
	childViewContainer: 'ul.transactions',
	emptyView: RecentTransactionEmpty,

	events: {
		"click .panel-heading": "togglePanel",
		"click button.more": "loadMore"
	},

	collectionEvents: {
		"more": "hideMoreButton"
	},

	togglePanel: function(event){
		if(this.$el.find('.glyphicon-minus-sign').is(':visible')){
			this.hidePanel();
		}else{
			this.showPanel();
		}
	},
	
	onRender: function(){
		if(this.collection.length < this.collection.limit){
			this.$el.find('button.more').hide();
		}
	},

	loadMore: function(){
		this.$el.find('button.more').button('loading');
		this.collection.loadMore();
		return false;
	},

	hideMoreButton: function(response){
		this.$el.find('button.more').button('reset');
		if(!response || response.length < this.collection.limit){
			this.$el.find('button.more').hide();
		}
	},

	hidePanel: function(event){
		this.$el.find('.panel-body').hide();
		this.$el.find('.glyphicon-minus-sign').removeClass('show').addClass('hidden');
		this.$el.find('.glyphicon-plus-sign').removeClass('hidden').addClass('show');
	},
	
	showPanel: function(event){
		this.$el.find('.panel-body').show();
		this.$el.find('.glyphicon-plus-sign').removeClass('show').addClass('hidden');
		this.$el.find('.glyphicon-minus-sign').removeClass('hidden').addClass('show');
	}
});

var EditSaleWindowView = ModalView.extend({
	
	id: 'edit-sale',
	template: JST['sales/edit_sale_window.html'],

	events: {
		'click button.save': 'save',
		'click button.delete': 'confirm_delete',
        'click button.confirm_delete': 'delete',
		'click a.email-receipt': 'emailReceipt',
		'click a.add-tip': 'addTip',
		'click a.full-return': 'fullReturn',
		'click a.partial-return': 'partialReturn',
		'click a.view-receipt': 'viewReceipt',
		'blur #sale-customer': 'clearCustomer'
	},
	
	initialize: function(){
		this.employee = {};
		this.customer = {};
		ModalView.prototype.initialize.call(this);		
	},

	render: function(){
		
		var attr = this.model.attributes;
		var html = this.template(attr);
		this.$el.html(html);
		var view = this;

		if(attr.employee){
			this.employee = attr.employee.toJSON();
		}
		if(attr.customer){
			this.customer = attr.customer.toJSON();
		}

		init_customer_search(this.$el.find('#sale-customer'), function(e, customer, list){
			view.$el.find('#sale-customer-id').val( customer.person_id );
			view.$el.find('#sale-customer').val( customer.first_name +' '+ customer.last_name );
			view.customer = customer;
			return false;
		});
		
		init_employee_search(this.$el.find('#sale-employee'), function(e, employee, list){
			view.$el.find('#sale-employee-id').val( employee.person_id );
			view.$el.find('#sale-employee').val( employee.first_name +' '+ employee.last_name );
			view.employee = employee;
			return false;
		});
		
		this.$el.find('#sale-date').datetimepicker();	
		return this;
	},
	
	save: function(){
		var view = this;
		var data = {
			'sale_time': view.$el.find('#sale-date').val(),
			'customer_id': view.$el.find('#sale-customer-id').val(),
			'customer': new Customer(view.customer),
			'comment': view.$el.find('#sale-comment').val(),
			'customer_note': view.$el.find('#sale-customer-note').val()
		};

		var employee_id = view.$el.find('#sale-employee-id').val();
		if(employee_id && employee_id != 0){
			data.employee_id = employee_id;
			data.employee = new User(this.employee);
		}

		this.model.set(data);
		this.model.save();
		this.hide();
	},

	clearCustomer: function(e){
		var customer = this.$el.find('#sale-customer').val();
		if(!customer || customer == ''){
			this.$el.find('#sale-customer-id').val('');
			this.customer = {};
		}
	},
	
	viewReceipt: function(e){
		e.preventDefault();
		var window = new ReceiptWindowView({model: this.model});
		window.show();
	},	
	
	emailReceipt: function(e){
		e.preventDefault();
		var window = new EmailReceiptWindowView({model: this.model});
		window.show();
	},
	
	addTip: function(e){
		e.preventDefault();
		var window = new TipWindowView({model: this.model});
		window.show();
	},	
	
	fullReturn: function(e){
		e.preventDefault();	
		this.$el.loadMask();
		App.data.cart.prepareFullReturn(this.model);
		
		var sale_window = this;
		this.listenToOnce(App.data.cart, 'loaded', function(){
			sale_window.hide();
		});
	},
	
	partialReturn: function(e){
		e.preventDefault();
		var partialReturnView = new PartialReturnWindowView({model: this.model});
		partialReturnView.show();
	},

    confirm_delete: function(e){
        e.preventDefault();

        if (!App.data.user.is_admin()) {
            alert('You do not have permissions to delete this sale.');
            return false;
        }

        var window = new ConfirmSaleDeleteView({model: this.model});
        window.show();
    }
});

var EmailReceiptWindowView = ModalView.extend({
	
	id: 'email-receipt',
	template: JST['sales/email_receipt_window.html'],

	events: {
		'click button.send': 'emailReceipt'
	},
	
	initialize: function(){
		ModalView.prototype.initialize.call(this);
		this.listenTo(this.model, 'email-success', this.closeWindow);
		this.listenTo(this.model, 'email-error', this.clearLoading);
	},

	render: function(){
		var data = {
			'sale_id': this.model.attributes.sale_id,
			'number': this.model.attributes.number,
			'email': ''
		}; 
		
		// Pre-populate email field with customer's email
		if(this.model.get('customer') && this.model.get('customer').get('email') != ''){
			data.email = this.model.get('customer').get('email');
		}		
		
		var html = this.template(data);
		this.$el.html(html);	
		
		return this;
	},
	
	emailReceipt: function(e){
		e.preventDefault();
		this.$el.loadMask({message: 'Sending...'});
		var email = this.$el.find('#customer-email').val();
		this.model.emailReceipt(email);
	},
	
	closeWindow: function(model){
		this.hide();
	},
	
	clearLoading: function(model){
		$.loadMask.hide();
	}
});

var ReceiptWindowView = ModalView.extend({
	
	id: 'sale-receipt-window',
	template: JST['sales/receipt_window.html'],

	initialize: function(options){
		ModalView.prototype.initialize.call(this);
		
		if(options && options.new_sale == true && App.data.course.get_setting('after_sale_load') == 0){
			this.on('hide', this.redirect);
		}
	},
	
	render: function(){
		var html = this.template(this.model.attributes);
		this.$el.html(html);	
		return this;
	}, 
	
	redirect: function(){
		if(App.data.cart.get('items').length > 0){
			return false;
		}
				
		window.location = SITE_URL + '/teesheets';
		return true;
	}
});

var PartialReturnWindowView = ModalView.extend({
	
	id: 'email-receipt',
	template: JST['sales/partial_return_window.html'],
	creditCardTemplate: JST['sales/payment_window_credit_card_swipe.html'],

	events: {
		'click button.cash': 'cashReturn',
		'click button.credit-card': 'showCreditCard',
		'click button.member-account': 'memberAccountReturn',
		'click button.customer-account': 'customerAccountReturn',
	},
	
	initialize: function(){
		ModalView.prototype.initialize.call(this);
		this.listenTo(this.model.get('payments'), 'success', this.closeWindow);
		this.listenTo(App.vent, 'credit-card', this.processCreditCard);
	},

	render: function(){
		var data = this.model.attributes;
        data.refund_reasons = App.data.course.get('refund_reasons');
		data.refunded = 0;
		
		_.each(this.model.get('payments').models, function(payment){
			if(payment.get('amount') < 0){
				data.refunded += _.round(payment.get('amount'));
			}
		});	
		data.leftover = _.round(data.total + data.refunded);
		this.leftover = data.leftover;

		var html = this.template(data);
		this.$el.html(html);	
		
		return this;
	},
	
	validate: function(amount){
		var amount = Math.abs(amount);
		if(amount > this.leftover){
			App.vent.trigger('notification', {'type': 'error', 'msg': 'Total refund must be less than sale total'});
			return false;
		}
		return true;
	},
	
	processCreditCard: _.throttle(function(card_data){
		
		this.model.get('payments').addRefund(card_data, function(payment){
			if(card_data.type == 'credit_card' && App.data.course.get_setting('signature_slip_count') > 0){
				var credit_card_receipt = new CreditCardReceipt(payment.attributes);
				credit_card_receipt.print();
			}
		});
	}, 1000, {'leading': true, 'trailing': false}),	
	
	cashReturn: function(e){
		var payment = {};
		payment.amount = _.round(0 - _.round(this.$el.find('input.amount').val(), 2), 2);
        payment.refund_comment = this.$el.find('#refund-comment').val();
        payment.refund_reason = this.$el.find('#refund-reason option:selected').text();
        payment.description = 'Cash Refund';
		payment.type = 'cash_refund';
		payment.record_id = 0;
		payment.invoice_id = 0;
		payment.tip_recipient = 0;
		
		if(!this.validate(payment.amount)){
			return false;
		}
		
		this.model.get('payments').addRefund(payment);
	},
	
	memberAccountReturn: function(e){
		var customer_name = this.model.get('customer').get('last_name')+', '+this.model.get('customer').get('first_name');
		var payment = {};
		payment.amount = _.round(0 - _.round(this.$el.find('input.amount').val(), 2), 2);
        payment.refund_comment = this.$el.find('#refund-comment').val();
        payment.refund_reason = this.$el.find('#refund-reason option:selected').text();
        payment.description = 'Member Balance Refund - '+customer_name;
		payment.type = 'member_account_refund';
		payment.record_id = this.model.get('customer').get('person_id');
		payment.invoice_id = 0;
		payment.tip_recipient = 0;
		
		if(!this.validate(payment.amount)){
			return false;
		}		
		
		this.model.get('payments').addRefund(payment);
	},
	
	customerAccountReturn: function(e){
		var customer_name = this.model.get('customer').get('last_name')+', '+this.model.get('customer').get('first_name');
		var payment = {};
		payment.amount = _.round(0 - _.round(this.$el.find('input.amount').val(), 2), 2);
        payment.refund_comment = this.$el.find('#refund-comment').val();
        payment.refund_reason = this.$el.find('#refund-reason option:selected').text();
        payment.description = 'Customer Credit Refund - '+customer_name;
		payment.type = 'customer_account_refund';
		payment.record_id = this.model.get('customer').get('person_id');
		payment.invoice_id = 0;
		payment.tip_recipient = 0;
		
		if(!this.validate(payment.amount)){
			return false;
		}
					
		this.model.get('payments').addRefund(payment);
	},		
	
	showCreditCardSwipeForm: function(){
		
		var amount = this.$el.find('#payment_amount').val();
		var attrs = {
			amount: amount,
			show_manual_entry: false
		};
		var view = this;
		
		view.$el.find('div.modal-body').html(this.creditCardTemplate(attrs));
		view.$el.find('div.well').hide();	
		var events = _.clone(this.events);	
		
		// Temporarily bind the "submit" button to send the payment
		events['click button.submit-payment'] = 'creditCardReturn';
		
		this.delegateEvents(events);
		this.$el.find('#credit-card-data').focus();
		
		this.$el.find('#credit-card-data').on('keypress', function(e){
			if(e.which == 13){
				view.creditCardReturn();
				view.$el.loadMask();
			}
		});
	
		return false;
	},	
	
	showCreditCard: function(e){
		
		var view = this;
		if(!App.data.course.get('merchant') || e.shiftKey){
			this.creditCardReturn(e);
			return false;
		}
		
		if(App.data.course.get('merchant') == 'element'){
			this.showCreditCardSwipeForm();
			return false;
		}
		
		var data = {};
		data.amount = _.round( Math.abs(this.$el.find('input.amount').val()) );
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.action = 'refund';
		data.sale_id = this.model.get('sale_id');
		data.type = 'credit_card_partial_refund';
		
		// Load merchant card iframe
		view.$el.loadMask();
		$.post(SITE_URL + '/v2/home/credit_card_window', data, function(response){
			view.$el.find('div.modal-body').html(response);
			view.$el.find('div.modal-footer').hide();
			view.$el.find('h4.modal-title').text('Credit Card Payment');
		},'html');
	},	
	
	creditCardReturn: function(e){
		
		var payment = {};
		var sale = this.model;
		
		payment.amount = _.round(0 - _.round(this.$el.find('input.amount').val(), 2), 2);
        payment.refund_comment = this.$el.find('#refund-comment').val();
        payment.refund_reason = this.$el.find('#refund-reason option:selected').text();
        payment.type = 'credit_card_partial_refund';
		payment.record_id = 0;
		payment.invoice_id = 0;	
		payment.sale_id = sale.get('sale_id');
		payment.description = 'CC Partial Refund';

		if(!this.validate(payment.amount)){
			return false;
		}
		
		if(App.data.course.get('merchant') == 'element'){
			payment.merchant_data = {};
			payment.merchant_data.encrypted_track = this.$el.find('#credit-card-data').val();
		}		
		
		sale.get('payments').addRefund(payment);
		return false;		
	},
	
	closeWindow: function(model){
		App.vent.trigger('notification', {'type': 'success', 'msg': 'Refund applied'});
		this.hide();
	}
});

var TipWindowView = ModalView.extend({
	
	id: 'sale-tip',
	template: JST['sales/sale_tip_window.html'],

	events: {
		'click .apply-tip': 'applyTip',
		'change #tip-type': 'setType'
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
		this.listenTo(this.model.get('payments'), 'add change', this.closeWindow);
	},

	render: function(){
		var attributes = this.model.attributes;
        var check_payment = false;
		
		// Default tip types that will always appear
		attributes.tips = new PaymentCollection();
		
		// Loop through payments and add new tip types if member account
		// or credit card charges exist
		_.each(attributes.payments.models, function(payment){
			
			// Don't create menu entries for certain payments
			if(!(payment.get('type') &&
				(payment.get('type') == 'cash' ||
				payment.get('type') == 'check' || 
				payment.get('type') == 'change' || 
				payment.get('type') == 'punch_card' ||
				payment.get('type') == 'loyalty_points' ||
				(payment.get('type') == 'gift_card' && payment.get('invoice_id') != 0) ||
				payment.get('type').indexOf('refund') > -1))
			) {
                // If payment is a tip, don't modify type
                if (payment.get('type') && payment.get('type').indexOf('_tip') > -1) {
                    var payment_id = payment.get('type');
                    var amount = payment.get('amount');
               
                }else{
                    var payment_id = payment.get('type') + '_tip';
                    var amount = 0.00;
                    if (attributes.tips.get(payment_id)) {
                        return false;
                    }
                }
                
                var type = payment_id;
                if (payment.get('invoice_id') > 0) {
                    payment_id += ':' + payment.get('invoice_id');
                } else if (payment.get('record_id') > 0) {
                    payment_id += ':' + payment.get('record_id');
                } else if (payment.get('number') != null && payment.get('number') != '') {
                    payment_id += ':' + payment.get('number');
                }

 				if(payment.get('is_auto_gratuity') == 1){
                	payment_id += ':auto';
                }

                attributes.tips.add({
                    'payment_id': payment_id,
                    'type': type,
                    'amount': amount,
                    'number': payment.get('number'),
                    'description': payment.get('description'),
                    'invoice_id': payment.get('invoice_id'),
                    'record_id': payment.get('record_id'),
                    'tip_recipient': payment.get('tip_recipient'),
                    'tip_recipient_name': payment.get('tip_recipient_name'),
                    'is_auto_gratuity': payment.get('is_auto_gratuity')
                }, {'merge': true});
            }

            if(payment.get('type') == 'check'){
                check_payment = true;
            }
		});
        
        if(check_payment){
            attributes.tips.add({
                'payment_id':'check_tip',
                'description': 'Check Tip',
                'amount': 0.00,
                'type': 'check_tip',
                'record_id': 0,
                'invoice_id': 0
            }, {'merge': true});
            attributes.tips.add({
                'payment_id':'cash_tip',
                'description': 'Cash Tip',
                'amount': 0.00,
                'type': 'cash_tip',
                'record_id': 0,
                'invoice_id': 0
            }, {'merge': true});
        
        }else{
            attributes.tips.add({
                'payment_id':'cash_tip',
                'description': 'Cash Tip',
                'amount': 0.00,
                'type': 'cash_tip',
                'record_id': 0,
                'invoice_id': 0
            }, {'merge': true});
            attributes.tips.add({
                'payment_id':'check_tip',
                'description': 'Check Tip',
                'amount': 0.00,
                'type': 'check_tip',
                'record_id': 0,
                'invoice_id': 0
            }, {'merge': true});
        }

		var html = this.template(attributes);
		this.$el.html(html);
		this.setType( attributes.tips.at(0).get('payment_id') );
		var view = this;
		
		init_employee_search(this.$el.find('#tip-recipient'), function(e, employee, list){
			view.$el.find('#tip-recipient-id').val( employee.person_id );
			view.$el.find('#tip-recipient').val( employee.first_name +' '+ employee.last_name ).data('employee', employee);
			return false;
		});	
		
		this.$el.find('#tip-type').button();
		return this;
	},
	
	setType: function(e){
		
		if(typeof(e) == 'object'){
			var payment_id = $(e.currentTarget).val();
		}else{
			var payment_id = e;
		}

		var	amount = 0.00;
		var	employee_id = 0;
		var	employee_name = '';
		var record_id = 0;
		var payment = this.model.get('payments').get(payment_id);

		console.debug(this.model.get('payments'));

		if(payment && payment.get('amount')){
			amount = payment.get('amount');
			employee_id = payment.get('tip_recipient');
			employee_name = payment.get('tip_recipient_name');
			record_id = payment.get('record_id');
		}
		
		if(!employee_id){
			employee_id = App.data.user.get('person_id');
			employee_name = App.data.user.get('first_name') +' '+ App.data.user.get('last_name');	
		}	
		
		this.$el.find('#tip-amount').val( accounting.formatMoney(amount, '') );
		this.$el.find('#tip-recipient').val(employee_name);
		this.$el.find('#tip-recipient-id').val(employee_id);
	},
	
	applyTip: function(e){
		e.preventDefault();
		var selected_option = this.$el.find('select[name="type"] option:selected');
		
		var data = {};
		data.id = selected_option.data('value');
		data.payment_id = selected_option.data('value');
		data.amount = this.$el.find('#tip-amount').val();
		data.type = selected_option.data('type');
		data.record_id = selected_option.data('record-id');
		data.invoice_id = selected_option.data('invoice-id');
		data.number = selected_option.data('number');
		data.tip_recipient = this.$el.find('#tip-recipient-id').val();
		data.tip_recipient_name = this.$el.find('#tip-recipient').val();
		data.is_auto_gratuity = selected_option.data('is-auto-gratuity');
		
		console.debug(data, selected_option.data('is-auto-gratuity'));

		this.$el.loadMask({message: 'Applying tip...'});
		this.model.get('payments').create(data, {'merge': true, 'wait': true, 'error': function(model, response){
			
			var msg = 'Error adding tip';
			if(response.responseJSON.msg){
				msg = response.responseJSON.msg;
			}
			App.vent.trigger('notification', {'type': 'error', 'msg': msg});
			$.loadMask.hide();		
		}});
	},
	
	closeWindow: function(model){
		App.vent.trigger('notification', {'type': 'success', 'msg': 'Tip added'});
		this.hide();
	}
});

var ConfirmSaleDeleteView = ModalView.extend({

    id: 'sale-delete-confirm',
    template: JST['sales/confirm_sale_delete_window.html'],

    events: {
        'click #confirm-delete': 'delete'
    },

    initialize: function(){
        ModalView.prototype.initialize.call(this);
        //this.listenTo(this.model.get('payments'), 'add change', this.closeWindow);
    },

    render: function(){
        var attr = this.model.attributes;
        attr.refund_reasons = App.data.course.get('refund_reasons');
        var html = this.template(attr);
        this.$el.html(html);
        var view = this;

        return this;
    },

    delete: function(){
        if (!App.data.user.is_admin()) {
            alert('You do not have permissions to delete this sale.');
            return false;
        }
        var view = this;
		var refund_comment = this.$el.find('#deletion-comment').val();
		var refund_reason = this.$el.find('#deletion-reason option:selected').text();
		if(!(!App.data.user.get('acl').can('delete_wo_reason','Sale')) && refund_reason == ""){
			alert('Reason is required to proceed');
			return false;
		}
        view.$el.loadMask();


        this.model.destroy({headers: {'refund_reason':refund_reason, 'refund_comment':refund_comment}, 'wait':true, 'success': function(){
            App.vent.trigger('notification', {'type':'success', 'msg':'Sale deleted'});
            $('#modal-edit-sale').modal('hide');
            view.hide();
        }, 'error': function(){
            App.vent.trigger('notification', {'type':'error', 'msg':'Error deleting sale'});
            $.loadMask.hide();
        }});
    }
});
