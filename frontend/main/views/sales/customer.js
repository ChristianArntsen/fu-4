var CartCustomerDetailsView = Backbone.View.extend({

	tagName: 'div',
	id: function(){
		return 'customer_'+ this.model.get('person_id');
	},
	className: 'tab-pane customer',
	template: JST['sales/customer_details.html'],

	events: {
		'click .edit-customer': 'edit',
		'click #pay-member-account': 'payMemberAccount',
		'click #pay-customer-account': 'payCustomerAccount',
		'click #pay-invoice-account': 'payInvoiceAccount',
		'click .add-photo': 'addPhoto',
		'click li.customer-recent-transaction': 'viewSale',
		'click li.customer-gift-card button.pay': 'payWithGiftCard',
		'click li.customer-raincheck button.pay': 'payWithRaincheck'
	},
	
	initialize: function(){
		this.listenTo(this.model, 'change:first_name change:last_name change:phone_number change:cell_phone_number change:groups change:email change:photo change:image_id', this.render);
		this.listenTo(this.model,'change:birthday',this.render_age);
	},

	get_customer_model: function(){
		// Copy the customer over to the general customer collection
		// so we can easily edit/save their information and have it hit
		// the correct API endpoint. The cart customer collection listens for changes
		// in the general customer collection and updates customers in the cart automatically.
		var cart_customer = this.model;
		var customer = App.data.customers.add(cart_customer.toJSON(), {merge: true});

		customer.once('sync', function(){
			App.data.cart.get('customers').add(customer.toJSON(), {merge: true});
		});

		return customer;
	},

	render: function(){
		var attr = this.model.attributes;
		attr.member_account_label = App.data.course.get('member_balance_nickname');
		attr.customer_account_label = App.data.course.get('customer_credit_nickname');
		attr.price_classes = App.data.course.get('price_classes');
		this.$el.html(this.template(attr));
		
		var passes_view = new CartCustomerPassesView({collection: this.model.get('passes')});
		this.$el.find('div.customer-passes').html(passes_view.render().el);

		return this;
	},

	render_age: function(e){
		var birthday = this.model.get('birthday');
		var age = Math.floor((new Date - new Date(birthday))/(60000*60*24*365));
		console.log('render age',birthday, this.model.id,age, $('#customer_'+this.model.id+' .customer-age'),$('#customer_'+this.model.id+' .customer-age').html())
		if(!isNaN(age))
		    $('#customer_'+this.model.id+' .customer-age').html('('+age+' yrs)');
		else $('#customer_'+this.model.id+' .customer-age').html('');
		console.log($('#customer_'+this.model.id+' .customer-age').html())
	},

	edit: function(){
		
		var customer = this.get_customer_model();
		var form = new CustomerFormView({model: customer});
		form.show();
	},

	addPhoto: function(){
		
		var customer = this.get_customer_model();
		var photoUploadWindow = new CustomerPhotoUploadView({model: customer, saveCustomer: true});
		photoUploadWindow.show();
		return false;
	},

	payMemberAccount: function(e){

		var balance = _.round(this.model.get('member_account_balance'));
		if(balance > 0){
			balance = 0;
		}else{
			balance = Math.abs(balance);
		}

		App.data.cart.addItem({
			'selected': true,
			'name': App.data.course.get('member_balance_nickname'),
			'unit_price': balance,
			'total': balance,
			'tax': 0,
			'discount_percent': 0,
			'subtotal': balance,
			'max_discount': 0,
			'quantity': 1,
			'item_type': 'member_account',
			'params': {
				'customer': this.model.toMinimumJSON()
			}
		});

		return false;
	},

	payWithGiftCard: function(e){
		
		var gift_card_id = $(e.currentTarget).parent().data('gift-card-id');
		var gift_card = this.model.get('gift_cards').get(gift_card_id);

		var amount = App.data.cart.get('total_due');
		if(amount == 0){
			return false;
		}
		var paymentWindow = new SalePaymentWindow({collection: App.data.cart.get('payments')});
		paymentWindow.show();

		var data = {
			amount: amount,
			number: gift_card.get('giftcard_number'),
			type: 'gift_card',
			description: 'Gift Card',
			record_id: 0
		}

		App.data.cart.get('payments').addPayment(data);	
		return false;	
	},

	payWithRaincheck: function(e){
		
		var raincheck_id = $(e.currentTarget).parent().data('raincheck-id');
		var raincheck = this.model.get('rainchecks').get(raincheck_id);

		var amount = App.data.cart.get('total_due');
		if(amount == 0){
			return false;
		}
		var paymentWindow = new SalePaymentWindow({collection: App.data.cart.get('payments')});
		paymentWindow.show();

		var data = {
			amount: amount,
			number: raincheck.get('raincheck_number'),
			type: 'raincheck',
			description: 'Raincheck',
			record_id: 0
		};

		App.data.cart.get('payments').addPayment(data);	
		return false;	
	},

	viewSale: function(e){
		var row = $(e.currentTarget);
		var sale_id = row.data('sale-id');
		var sale = this.model.get('recent_transactions').get(sale_id);

		var receiptWindow = new ReceiptWindowView({model: sale});
		receiptWindow.show();

		return false;
	},

	payCustomerAccount: function(e){

		var balance = _.round(this.model.get('account_balance'));
		if(balance > 0){
			balance = 0;
		}else{
			balance = Math.abs(balance);
		}

		App.data.cart.addItem({
			'selected': true,
			'name': App.data.course.get('customer_credit_nickname'),
			'unit_price': balance,
			'total': balance,
			'tax': 0,
			'discount_percent': 0,
			'subtotal': balance,
			'max_discount': 0,
			'quantity': 1,
			'item_type': 'customer_account',
			'params': {
				'customer': this.model.toMinimumJSON()
			}
		});

		return false;
	},

	payInvoiceAccount: function(e){

		var balance = _.round(this.model.get('invoice_balance'));
		if(balance > 0){
			balance = 0;
		}else{
			balance = Math.abs(balance);
		}

		App.data.cart.addItem({
			'selected': true,
			'name': 'Open Invoices',
			'unit_price': balance,
			'total': balance,
			'tax': 0,
			'discount_percent': 0,
			'subtotal': balance,
			'max_discount': 0,
			'quantity': 1,
			'item_type': 'invoice_account',
			'params': {
				'customer': this.model.toMinimumJSON()
			}
		});

		return false;
	}
});

var CartCustomerTabView = Backbone.View.extend({

	tagName: 'li',
	id: function(){
		return 'customer_tab_' + this.model.get('person_id');
	},
	template: JST['sales/customer_tab.html'],
	
	initialize: function(){
		this.listenTo(this.model, 'change:first_name change:last_name', this.render);
	},
	
	events: {
		'click .delete': 'deleteCustomer',
		'click': 'select'
	},

	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	},

	select: function(e){
		this.model.set({'selected': true}).save();
		return true;
	},

	deleteCustomer: function(e){
		e.preventDefault();
		this.model.destroy();
		return false;
	}
});

var CartCustomersView = Backbone.View.extend({

	tagName: 'div',
	className: 'row',
	template: JST['sales/customers.html'],

	events: {
		'click #add-customer': 'newCustomer',
		'cardswipe input': 'submitSearch',
		'click a.show-pass-details': 'showPassDetails'
	},

	initialize: function(){
		this.listenTo(App.data.cart.get('customers'), 'add remove reset', this.render);
	},

	showPassDetails: function(e){
		var link = $(e.currentTarget);
		var row = link.parents('li').first();
		row.find('ul.pass-restrictions').toggle();
		return false;
	},

	render: function() {

		var self = this;
		this.$el.html(this.template());

		if(!App.data.cart.get('customers') || !App.data.cart.get('customers').models || App.data.cart.get('customers').models.length == 0){
			this.renderEmpty();
		}else{
			_.each(App.data.cart.get('customers').models, this.renderCustomer, this);
		}

		this.$el.find('input.typeahead-customer').cardSwipe({
			setInput: false,
            lengthThreshold:5
		});

		init_customer_search(this.$el.find('input.typeahead-customer'), function(e, customer, list){
			$(e.currentTarget).typeahead('val', '');
			customer.selected = true;
			var customer_model = App.data.cart.get('customers').create(customer);
		}, {
			include_recent_transactions: true,
			include_gift_cards: true,
			include_rainchecks: true
		});

		var selected_customer = App.data.cart.get('customers').findWhere({'selected': true});

		// If a customer is selected, default their tab to open
		if(selected_customer){
			this.$el.find('#customer_tab_' + selected_customer.get('person_id')).addClass('active');
			this.$el.find('#customer_' + selected_customer.get('person_id')).addClass('active');
			var bd = new Date(selected_customer.get('birthday'));
			var now = new Date();
			var age = Math.floor((now-bd)/(1000*60*60*24*365));
			if(!isNaN(age))
				this.$el.find('#customer_'+selected_customer.get('person_id')+' .customer-age').html(' ('+age+' yrs)');
			else this.$el.find('#customer_'+selected_customer.get('person_id')+' .customer-age').html('');
		}else{
			this.$el.find('#customer-tabs li').first().addClass('active');
			this.$el.find('#customer-details div.tab-pane').first().addClass('active');
		}

		return this;
	},

	submitSearch: function(e, data){
		
		this.$el.find('input.typeahead-customer').data('Bloodhound').remote.cancelLastRequest();
		this.$el.find('input.typeahead-customer').typeahead('val', '');
		
		$.get(API_URL + '/customers', {q: data}, function(response){
			if(!response || !response[0]){
				App.vent.trigger('notification', {type: 'error', msg: 'Customer not found'});
				return false;
			}
			var customer = response[0];
			customer.selected = true;
			App.data.cart.get('customers').create(customer);
		});

		return false;
	},

	renderEmpty: function(){
		this.$el.find('#customer-details').append('<div class="tab-pane customer active">Add a customer to sale</div>');
		this.$el.find('#customer-tabs').html('');
	},

	renderCustomer: function(model){
		this.$el.find('#customer-tabs').prepend( new CartCustomerTabView({model: model}).render().el );
		this.$el.find('#customer-details').prepend( new CartCustomerDetailsView({model: model}).render().el );
	},

	newCustomer: function(event){
		event.preventDefault();
		var customer = new Customer({selected: true});
        customer.set("use_loyalty",App.data.course.get('loyalty_auto_enroll'));
		var form = new CustomerFormView({model: customer});

		form.show();

		// When the customer is saved, attach them to the cart automatically
		customer.once('sync', function(model){
			var data = model.toJSON();
			data.selected = true;
			App.data.cart.get('customers').create(data);
		});
	}
});