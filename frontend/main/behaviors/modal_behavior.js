var ModalBehavior = Backbone.Marionette.Behavior.extend({

    defaults:{
        size: "modal-lg",
        title:"Title",
        keyboard: true,
        backdrop: true,
        opacity:.5,
        classes:[]
    },
    ui:function(){
        return{}
    },

    initialize: function(params){
        var self = this;

        var modal_id = '#modalbox';
        $(document).on('hidden.bs.modal', modal_id, {view: self}, function(event){
            self.view.modalWindow.remove();
            self.view.destroy();
        });
        var modal_id = 'modalbox';
        // Check if modal container has been created already, if not, create it
        if($('#' + modal_id).length == 0){
            var container = $(JST['starter_teesheet/behaviors/substate_container.html'](this.options));
            container.find(".modal-body").html(this.el);
            $('body').append(container);
            this.view.modalWindow = container;
        }


    },
    onShow: function(){
        this.view.modalWindow.find(".modal-body").html(this.el);

        if(this.options.closable !== undefined && this.options.closable === false){
            this.options.keyboard = false;
            this.options.backdrop = 'static';
        }
        $('#modalbox').modal({
            keyboard:this.options.keyboard,
            backdrop:this.options.backdrop,
            show:true
        });

        if(this.opacity){
            $('div.modal-backdrop').css('opacity', this.options.opacity);
        }
    },
    onRender: function(){
        this.view.$el.find(".close-modal").click(function(){
            $('#modalbox').modal('hide');
        });
    }
});