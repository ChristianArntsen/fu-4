var support_tools_layout = Backbone.Marionette.LayoutView.extend({

    tagName: 'section',
    attributes: {
        id: 'page-support-tools'
    },
    template: JST['support_tools/templates/support_tools.layout.html'],
    regions: {
        left_nav: '#support-tools-left-nav',
        disp_area: '#support-tools-disp-area'
    },

    events: {
        'click #support-tools-left-nav .btn': 'render_tool'
    },

    render_tool: function(e){
        var row = this.left_nav.$el.find('table.support-tools').bootstrapTable('getRowByUniqueId',e.target.dataset.id);
        this.disp_area.show(new window[row.tool]());
    }
});