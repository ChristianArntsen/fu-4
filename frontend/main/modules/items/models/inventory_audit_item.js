var InventoryAuditItem = Backbone.JsonRestModel.extend({

    defaults: {
        inventory_audit_id: '',
        item_id: '',
        current_count: 0,
        manual_count: 0
    }
});

var InventoryAuditItems = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/inventory_audits/items',
    model: InventoryAuditItem
});