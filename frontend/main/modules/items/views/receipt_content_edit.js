var ReceiptContentEditView = ModalLayoutView.extend({

    template: JST['items/templates/receipt_content_edit_window.html'],

    initialize: function() {
        var options = {
            closable: false,
            extraClass: 'modal-full right'
        };
        ModalView.prototype.initialize.call(this, options);
        this.listenTo(this.model, "invalid", App.errorHandler);
    },

    events: {
        "click button.save": "save_receipt_content",
        "click button.delete": "delete_receipt_content"
    },

    save_receipt_content: function(){

        var view = this;
        var data = _.getFormData(this.$el.find('div.modal-body'));
        data.signature_line = data.signature_line == 1;
        data.separate_receipt = data.separate_receipt == 1;

        var is_valid = this.model.set(data, {validate: true});
        if(!is_valid){
            return false;
        }

        if(this.model.isNew()){
            App.data.item_receipt_content.add(this.model);
        }

        this.$el.loadMask();
        this.model.save(null, {
            'success': function(){
                view.hide();
            }
        });
    },

    delete_receipt_content: function(){

        if(!confirm('Are you sure you want to delete this agreement?')){
            return false;
        }
        this.model.destroy();
        this.hide();
    }
});