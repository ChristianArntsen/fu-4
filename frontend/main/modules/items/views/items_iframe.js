var ItemsIframeLayout = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	className: 'row',
    template: function(){ return ''; },

    onShow: function(){
        
        // Create a new iframe and attach it to the body of the document (if it doesn't exist yet)
        if($('#items_iframe').length == 0) {
            $('<iframe class="page-iframe" src="' +SITE_URL+ '/items?in_iframe=1" id="items_iframe"></iframe>').appendTo('#page-iframe-container');
        }else{
            $('#items_iframe').show();
        }
        
        $('body').addClass('iframe');
    },

    onBeforeDestroy: function () {
        $('#items_iframe').hide();
        $('body').removeClass('iframe');
    }
});