

var TipTransaction = Backbone.Model.extend({

    idAttribute: "sale_id",
    defaults: {
        "transaction_id": null,
        "sale_id": null,
        "employee_id": null,
        "transaction_time": null,
        "notes": null,
        "comment": null,
        "journal_entries": []
    },

    get_url: function(){

    },

    initialize: function(attrs,options){
        var journal_entries = new TipJournalCollection(this.get('journal_entries'));
        console.log('entreez',this.get('journal_entries'),journal_entries,attrs);
        this.set('journal_entries',journal_entries);
    },

    parse: function(response) {
        var url = this.url;

        if (response.journal_entries) {
            var cart = this.get('cart');
            var journal_entries = new TipJournalCollection(response.journal_entries);
            response.journal_entries = journal_entries;
        }
        return response;
    },

    add_employee: function(employee){
        var entries = this.get('journal_entries');
        var entry = new TipJournalEntry({transaction_id:this.get('transaction_id'),employee_id:employee.emp_id,notes:'Tips Payable: '+employee.last_name+', '+employee.first_name});
        var found = false;
        _.each(entries.models,function(existing){
            if(existing.get('notes')===entry.get('notes')&&
            existing.get('transaction_id')==entry.get('transaction_id')&&
            existing.get('employee_id')==entry.get('employee_id')){
                existing.set({'deleted':0,'amount':0.00});
                found = true;
            }
        });
        if(!found)entries.add(entry);
    },

    remove_employee: function(employee_id){
        var entries = this.get('journal_entries');
        var self = this;
        _.each(entries.models,function(existing){
            if(existing.get('transaction_id')==self.get('transaction_id')&&
                existing.get('employee_id')==employee_id){
                existing.set('deleted',1);
                existing.set('amount',0.00);
            }
        });
    },

    set_employee_amount: function(employee_id,amount){
        var entries = this.get('journal_entries');
        var self = this;
        var changed = false;
        _.each(entries.models,function(existing){
            if(existing.get('transaction_id')==self.get('transaction_id')&&
                existing.get('employee_id')==employee_id){

                previous_amount = existing.get('amount');
                existing.set('amount',amount);
                if(!self.is_valid()){
                    existing.set('amount',previous_amount);
                    changed = true;
                }
            }
        });
        return changed;
    },

    set_employee_comment: function(employee_id,comment){
        var entries = this.get('journal_entries');
        var self = this;
        _.each(entries.models,function(existing){
            if(existing.get('transaction_id')==self.get('transaction_id')&&
                existing.get('employee_id')==employee_id){
                existing.set('comment',comment);
            }
        });
    },

    get_balance: function(){
        var entries = this.get('journal_entries');
        var balance = new Decimal(0);
        _.each(entries.models,function(existing){
            if(existing.deleted)return;
            if(existing.get('d_c')==='debit'){
                balance = balance.plus(existing.get('amount'));
            }else{
                balance = balance.minus(existing.get('amount'));
            }
        });
        return balance.toDP(2).toNumber();
    },

    get_total: function(){

        var entries = this.get('journal_entries');
        var total = new Decimal(0);
        _.each(entries.models,function(existing){
            if(existing.deleted)return;
            if(existing.get('d_c')==='debit'){
                total = total.plus(existing.get('amount'));
            }
        });
        return total;
    },

    get_employee_count: function(){
        var count = 0;
        _.each(entries.models,function(existing){
            if(existing.deleted)return;
            if(existing.get('employee_id')){
                count ++;
            }
        });
        return count;
    },

    even_split: function(){
        var total = this.get_total();
        var split = new Decimal(total);
        split = split.dividedBy(this.get_employee_count()).toDP(2).toNumber();
        _.each(entries.models,function(existing){
            if(existing.deleted)return;
            if(existing.get('employee_id')){
                existing.set('amount',split);
            }
        });
    },

    is_valid: function(){
        if(this.get_balance()<0)return false;
        return true;
    }

});

var TipJournalEntry = Backbone.Model.extend({

    idAttribute: "notes",
    defaults: {
        "transaction_id": null,
        "account_id": null,
        "employee_id": null,
        "d_c": "credit",
        "amount": 0.00,
        "notes": null,
        "comment": null,
        "deleted":0
    }

});

var TipJournalCollection = Backbone.Collection.extend({
    model: function (attrs, options) {
        var mdl = new TipJournalEntry(attrs, options);
        if(this.transaction)mdl.set('transaction_id',this.transaction.get('transaction_id'));
        return mdl;
    },

    get_debit: function(){
        for(var i = 0;i<this.models.length;i++){
            var model = this.models[i];
            if(model.get('d_c') === 'debit')return model;
        }
        return false;
    }
});