var ShareTipsWindowView = ModalView.extend({

    id: "tip-sharing",
    className: "tip-sharing modal-dialog",
    template: JST['tip_transactions/TipSharing.html'],

    events: {
        "click button.remove": "removeEmployee",
        "change .tip-amount": "changeAmount",
        "change .tip-percent": "changePercent",
        "keyup .tip-amount": "changeAmount",
        "keyup .tip-percent": "changePercent",
        "keyup .tip-comment": "changeComment",
        "click .submit-tip-transaction": "submitTransaction"
    },

    initialize: function(){
        ModalView.prototype.initialize.call(this);

        this.listenTo(this.model, "change", this.render);
    },

    render: function(){
        var attrs = this.model.attributes;
        attrs.balance = this.model.get_balance();
        if(attrs.total_credits - attrs.total_debits < 0){
            // Zero balance based on percentages
            // scale them to total 100% so they are in a valid state
            this.model.scale_to_100();
        }
        this.$el.html(this.template(attrs));
        var TW = this;
        init_employee_search(this.$el.find('.employee-search'), function(e, employee, list){
            $(e.currentTarget).typeahead('val', '');
            var emp_id = 'tip-tr-emp-'+employee.emp_id;
            if($('#'+emp_id).length){
                // oops, already added!
                return;
            }
            TW.model.add_employee(employee);
            TW.render();
        });
        return this;
    },

    removeEmployee: function(e,f){
        var li = $(e.currentTarget).parents('li')[0];
        var emp_id = $(li).data('employee-id');
        this.model.set_employee_amount(emp_id,0.00);
        this.model.remove_employee(emp_id);
        this.render();
    },

    changeAmount: _.debounce(function(e){
        var amount = e.currentTarget.value;
        var li = $(e.currentTarget).parents('li')[0];
        var emp_id = $(li).data('employee-id');
        this.model.set_employee_amount(emp_id,amount);
        this.render();
    },500),

    changePercent:_.debounce(function(e){
        var percent = new Decimal(e.currentTarget.value).dividedBy(100);
        var amount = new Decimal(this.model.get('total')).times(percent).toDP(2).toNumber();
        var li = $(e.currentTarget).parents('li')[0];
        var emp_id = $(li).data('employee-id');
        this.model.set_employee_amount(emp_id,amount);
        this.render();
    },500),

    changeComment: function(e){
        var comment = e.currentTarget.value;
        var li = $(e.currentTarget).parents('li')[0];
        var emp_id = $(li).data('employee-id');
        this.model.set_employee_comment(emp_id,comment);
    },

    submitTransaction: function(){
        var view = this;
        if(this.model.get('transaction_id')){
            console.log('expecting to patch existing...');
            this.model.url = this.model.base_url+'/'+this.model.get('transaction_id');
            this.model.save(null,{success:function(model,result) {
                console.log(result);
                if (result.success){
                    App.vent.trigger('notification', {msg: 'Tip sharing updated successfully', type: 'success'});
                    view.$el.find('button.close').click();
                }
                else {
                    App.vent.trigger('notification', {msg: 'Failed to update tip sharing. Please try again later.', type: 'error'});
                }
            }});
        }
    }


});