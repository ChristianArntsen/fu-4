var CustomerGroup = Backbone.JsonRestModel.extend({

    urlRoot: CUSTOMERS_URL,
    idAttribute: "group_id",
    defaults: {
        "group_id": null,
        "name": "",
        "percent": "0.00"
    },

    // Over ride backbone save method to only send certain fields to be
    // saved to server (instead of entire object)
    save: function (attrs, options) {
        attrs = attrs || this.toJSON();
        options = options || {};

        // Filter attributes that will be saved to server
        attrs = _.pick(attrs, [
            'group_id',
            'name',
            'percent'
        ]);

        // Call super with attrs moved to options
        Backbone.Model.prototype.save.call(this, attrs, options);
    }

});

var CustomerGroupsCollection = Backbone.JsonRestCollection.extend({
    url: CUSTOMERS_URL,
    model: CustomerGroup
});