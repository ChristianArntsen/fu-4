var CustomerDataTabsView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['customers/templates/tabs.html'],

});

var CustomerInfoSummaryView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['customers/templates/customer_info_summary.html'],

    initialize: function (){
        this.listenTo(this.model, "change", this.render);
    },

    events: {

    },

    save: function(){

        // var name = this.$el.find('#recurring-charge-name').val();
        // var recurringCharge = new RecurringCharge({name: name}, {parse: true});
        // var recurringChargeEdit = new RecurringChargeEditView({model: recurringCharge});
        //
        // recurringChargeEdit.show();
    },

    onRender: function(){
        //this.tabs.show( new CustomerDataTabsView({model: this.model}) );
        //this.summary.show( new CustomerInfoSummaryView({model: this.model}) );
        //this.info.show( new CustomerInfoView({model: this.model}) );
        this.highlight_summary_comments();
    },

    highlight_summary_comments: function() {
        var status_flag = this.model.get('status_flag');

        if (status_flag == 1) {
            this.$el.find('.customer-summary-comments-title .panel-title').css({'color':'white'});
            this.$el.find('.customer-summary-comments-title').css({'backgroundColor':'#C51236'});
            this.$el.find('.customer-summary-status').html('RED');
        }
        else if (status_flag == 2) {
            this.$el.find('.customer-summary-comments-title .panel-title').css({'color':'white'});
            this.$el.find('.customer-summary-comments-title').css({'backgroundColor':'#F9A41A'});
            this.$el.find('.customer-summary-status').html('YELLOW');
        }
        else if (status_flag == 3) {
            this.$el.find('.customer-summary-comments-title .panel-title').css({'color':'white'});
            this.$el.find('.customer-summary-comments-title').css({'backgroundColor':'#6FAC33'});
            this.$el.find('.customer-summary-status').html('GREEN');
        }
        else {
            this.$el.find('.customer-summary-comments-title .panel-title').css({'color':'#333'});
            this.$el.find('.customer-summary-comments-title').css({'backgroundColor':'#f5f5f5'});
            this.$el.find('.customer-summary-status').html('');
        }
    }
});

var CustomerInfoView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    template: JST['customers/templates/customer_info.html'],
    groups:{},

    regions: {
        'customer_groups': '#customer-groups'
    },

    initialize: function (){
        this.groups = this.model.get('groups');
    },

    events: {
        "click .edit-photo": "editPhoto",
        "click .take-photo": "takePhoto",
        "click .delete-photo": "deletePhoto",
        "change #customer-new-group": "addGroup",
        "change input": "optionsChanged",
        "blur #customer-phone": "optionsChanged",
        "blur #customer-cell-phone": "optionsChanged",
        "keyup #customer-comments": "optionsChanged",
        "change select": "optionsChanged",
        'keyup #customer-zip': "zipLookup"
    },

    zipLookup: function(){
        var zip = this.$el.find('#customer-zip').val();
        if (zip.length == 5 && !isNaN(zip)) {
            // TODO: Add international capabilities
            var client = new XMLHttpRequest();
            client.open("GET", "https://api.zippopotam.us/us/"+zip, true);
            client.onreadystatechange = function() {
                if(client.readyState == 4) {
                    var response = JSON.parse(client.responseText);
                    var city = '';
                    var state = '';
                    if (typeof response.places != 'undefined' && typeof response.places[0] != 'undefined' && typeof response.places[0]['place name'] != 'undefined') {
                        city = response.places[0]['place name'];
                    }
                    if (typeof response.places != 'undefined' && typeof response.places[0] != 'undefined' && typeof response.places[0]['state abbreviation'] != 'undefined') {
                        state = response.places[0]['state abbreviation'];
                    }
                    if ($('#customer-city').val() == city && $('#customer-state').val() == state) {
                        return;
                    }
                    if ($('#customer-city').val() != '' && city != '') {
                        var confirmed = confirm("Would you like to use the following: "+city+", "+state);
                        if (confirmed) {
                            $('#customer-city').val(city);
                            $('#customer-state').val(state);
                        }
                    }
                    else {
                        $('#customer-city').val(city);
                        $('#customer-state').val(state);
                    }
                };
            };

            client.send();
        }
    },
    optionsChanged: function() {
        var fields = [
            'first_name',
            'last_name',
            'email',

            'account_number',
            'price_class',
            'discount',
            //'invoice_email',
            'cell_phone_number',
            'phone_number',
            'birthday',
            'date_created',
            'address_1',
            'address_2',
            'city',
            'state',
            'zip',
            'status',
            'comments',
            'status_flag',
            'handicap_score',
            'handicap_account_number',
            'username',
            'password',
            'confirm_password'
            //'image_id'
        ];

        var data = _.getFormData(this.$el);
        data = _.pick(data, fields);

        data.groups = this.groups.toJSON();
        //data.photo = this.$el.find('img.customer-photo').prop('src');

        if(this.isValidPhone(this.model.get("cell_phone_number"))){
            data.cell_phone_number = data.cell_phone_number.replace(/\D/g,"");
        }
        if(this.isValidPhone(this.model.get("phone_number"))){
            data.phone_number = data.phone_number.replace(/\D/g, "");
        }

        if(data.password == "" || data.confirm_password == "" ){
            delete(data.password);
            delete(data.confirm_password);
        }

        this.model.set(data);
    },

    isValidPhone : function(number){
        return !isNaN(number) || _.isNull(number) || _.isUndefined(number) || number =="";
    },

    save: function(){

        // var name = this.$el.find('#recurring-charge-name').val();
        // var recurringCharge = new RecurringCharge({name: name}, {parse: true});
        // var recurringChargeEdit = new RecurringChargeEditView({model: recurringCharge});
        //
        // recurringChargeEdit.show();
    },

    onRender: function(){
        //this.tabs.show( new CustomerDataTabsView({model: this.model}) );
        //this.summary.show( new CustomerInfoSummaryView({model: this.model}) );
        //this.info.show( new CustomerInfoView({model: this.model}) );
        this.$el.find('#customer-account-number').cardSwipe({
            lengthThreshold:5
        });

        var that = this;
        this.$el.find('#customer-birthday').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $(this.$el.find('#customer-birthday')).on('dp.change', function(e){
            that.renderAge();
            that.optionsChanged();
        });
        $(this.$el.find('#customer-date-created')).on('dp.change', function(e){
            that.optionsChanged();
        });

        this.$el.find('#customer-date-created').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        if(this.isValidPhone(this.model.get("cell_phone_number")) && SETTINGS.currency_symbol === "$"){
            this.$el.find('#customer-cell-phone').formatter({
                'pattern': '({{999}}) {{999}}-{{9999}}',
                'persistent': true
            });
        }
        if(this.isValidPhone(this.model.get("phone_number")) && SETTINGS.currency_symbol === "$"){
            this.$el.find('#customer-phone').formatter({
                'pattern': '({{999}}) {{999}}-{{9999}}',
                'persistent': true
            });
        }
        this.renderGroups();
    },

    renderAge: function(e){
        var birthday = this.$el.find('#customer-birthday')[0].value;
        var age = Math.floor((new Date - new Date(birthday))/(60000*60*24*365));
        if(!isNaN(age))
            this.$el.find('.customer-age').html('('+age+' yrs)');
        else this.$el.find('.customer-age').html('');
    },

    renderGroups: function(){
        this.customer_groups.show(new CustomerGroupsView({
            collection: this.groups
        }));
    },

    addGroup: function(event){
        var menu = $(event.currentTarget);
        var group_id = menu.val();
        if(group_id == "0"){
            return false;
        }

        var group = App.data.course.get('groups').get(group_id);
        this.groups.add(group.attributes);
        menu.val(0);
        this.renderGroups();
    },

    editPhoto: function(){
        var photoUploadWindow = new CustomerPhotoUploadView({model: this.model});
        photoUploadWindow.show();
        return false;
    },

    takePhoto: function(){
        var photoUploadWindow = new CustomerPhotoUploadView({model: this.model, take: true});
        photoUploadWindow.show();
        return false;
    },

    deletePhoto: function(){
        var placeholder = BASE_URL + 'images/profiles/profile_picture.png';
        this.$el.find('img.customer-photo').prop('src', placeholder);
        this.$el.find('#image_id').val(0);
        this.$el.find('button.delete-photo').hide();
        return false;
    }

});

var CustomerAccountsSummaryView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['customers/templates/customer_accounts_summary.html'],

    initialize: function (){
        this.listenTo(this.model, "sync", this.render);
    },

    events: {

    },

    save: function(){

        // var name = this.$el.find('#recurring-charge-name').val();
        // var recurringCharge = new RecurringCharge({name: name}, {parse: true});
        // var recurringChargeEdit = new RecurringChargeEditView({model: recurringCharge});
        //
        // recurringChargeEdit.show();
    },

    onRender: function(){
        //this.tabs.show( new CustomerDataTabsView({model: this.model}) );
        //this.summary.show( new CustomerInfoSummaryView({model: this.model}) );
        //this.info.show( new CustomerInfoView({model: this.model}) );
    }
});

var CustomerAccountsView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    template: JST['customers/templates/customer_accounts.html'],

    regions: {
        'customer_household': '#customer-household',
        'customer_passes': '#customer-passes'
    },

    initialize: function (){
        this.original_member_balance = parseFloat(this.model.get("member_account_balance"));
        this.original_account_balance = parseFloat(this.model.get("account_balance"));
    },
    ui:{
        "new_member_balance":"#new_member_balance",
        "new_account_balance":"#new_account_balance",
        "confirm_member_balance_btn":"#confirm_member_balance_btn",
        "confirm_account_balance_btn":"#confirm_account_balance_btn",
    },

    modelEvents:{
        "sync":"handleSave",
    },
    events: {
        "change input": "optionsChanged",
        "change select": "optionsChanged",
        "change textarea": "optionsChanged",
        "click .add-credit-card": "addCreditCard",
        "click @ui.confirm_member_balance_btn": "confirmBalance",
        "click @ui.confirm_account_balance_btn": "confirmBalance",
        "click .remove-credit-card": "removeCreditCard",
        "click .view-member-account-transaction-report": "viewMemberAccountTransactionReport",
        "click .view-account-transaction-report": "viewAccountTransactionReport",
    },
    handleSave:function(){
        this.initialize();
        this.render();
    },
    confirmBalance:function(e){
        e.stopPropagation();
        e.preventDefault();
        var self = this;
        this.model.save(null, {
            success: function(){
                self.model.fetch({
                    data: {
                        include_rainchecks: true,
                        include_credit_cards:true,
                        include_marketing: true,
                        include_account_transactions: true,
                        include_billing: true,
                        include_last_visit: true
                    },
                    reset: true,
                    success: function (collection, response, options) {
                        self.render();
                        self.model.set("account_balance_option","set");
                        self.model.set("member_account_option","set");
                    }});
                App.vent.trigger('notification', {'msg': 'Customer saved', 'type': 'success'});
            },
            error: function(model, response){
                App.vent.trigger('notification', {'msg': response.responseJSON.msg, 'type': 'danger'});
                $.loadMask.hide();
            }
        });
        return false;
    },
    optionsChanged: function() {

        var fields = [
            'taxable',
            'account_balance',
            'account_limit',
            'account_balance_allow_negative',
            'account_balance_option',
            'account_balance_change_reason',
            'new_account_balance',
            'new_member_balance',
            'member_account_balance',
            'member_account_option',
            'member_balance_change_reason',
            'member_account_limit',
            'member_account_balance_allow_negative',
            'member',
            'use_loyalty',
            'loyalty_points'
        ];

        if(this.model.get('household_members').length > this.household_members){
            if(!confirm('Account balances from newly added household members will be transferred to this account. Continue?')){
                return false;
            }
        }

        var data = _.getFormData(this.$el);
        data = _.pick(data, fields);

        if((data.member_account_option == "increase" || data.member_account_option == "decrease") && data.member_account_balance < 0){
            if(!confirm("You're trying to "+data.member_account_option+" a balance by a negative, are you sure you meant to do that?")){
                return;
            }
        }
        if((data.account_balance_option == "increase" || data.account_balance_option == "decrease") && data.account_balance < 0){
            if(!confirm("You're trying to "+data.account_balance_option+" a balance by a negative, are you sure you meant to do that?")){
                return;
            }
        }

        data.account_limit = -Math.abs(data.account_limit);
        data.member_account_limit = -Math.abs(data.member_account_limit);


        var member_account_balance =   parseFloat(data.member_account_balance);
        if(isFinite(member_account_balance)){
            if(data.member_account_option == "set"){
                data.new_member_balance = member_account_balance;
            } else if(data.member_account_option == "increase"){
                data.new_member_balance = this.original_member_balance + member_account_balance;
            } else if(data.member_account_option == "decrease"){
                data.new_member_balance = this.original_member_balance - member_account_balance;
            }
        }
        var account_balance =   parseFloat(data.account_balance);
        if(isFinite(account_balance)){
            if(data.account_balance_option == "set"){
                data.new_account_balance  = account_balance;
            } else if(data.account_balance_option  == "increase"){
                data.new_account_balance  = this.original_account_balance + account_balance;
            } else if(data.account_balance_option  == "decrease"){
                data.new_account_balance = this.original_account_balance - account_balance;
            }
        }

        this.model.set(data);
        if(data.new_account_balance)
            this.ui.new_account_balance.val(accounting.formatMoney(data.new_account_balance));
        if(data.new_member_balance)
            this.ui.new_member_balance.val(accounting.formatMoney(data.new_member_balance));
    },



    renderHousehold: function(){
        var members = this.model.get('household_members');

        this.customer_household.show(new CustomerHouseholdView({
            collection: members
        }));
    },

    renderPasses: function(){
        var passes = this.model.get('passes');

        this.customer_passes.show(new CustomerPassesView({
            collection: passes,
            model: this.model
        }));
    },

    addPass: function(event){
        var menu = $(event.currentTarget);
        var pass_id = menu.val();
        if(pass_id == "0"){
            return false;
        }
        var pass = App.data.course.get('passes').get(pass_id);
        pass.set({expiration: "", start_date: ""});

        this.model.get('passes').add(pass.attributes);
        menu.val(0);
    },

    addCreditCard: function(e){
        e.preventDefault();
        var modal = new AddCustomerCreditCardsView({model:this.model});
        modal.show();
    },

    removeCreditCard: function(e){
        if (confirm('You are about to remove this credit card from this customer account as well as from any associated billing accounts. Are you sure you want to continue?'))
        {
            var tr = $(e.target).closest('tr');
            var credit_card_id = tr.data('credit-card-id');
            var person_id = this.model.get('person_id');
            // REMOVE FROM BILLINGS AND MARK AS DELETED
            $.ajax({
                type: "POST",
                url: "/index.php/customers/delete_credit_card/"+person_id+"/"+credit_card_id,
                data: '',
                success: function(response){
                    // REMOVE FROM HTML
                    if (response.success)
                        tr.remove();
                },
                dataType:'json'
            });
        }
    },


    onRender: function(){
        this.renderHousehold();
        this.renderPasses();
    }
});

var CustomerBillingSummaryView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['customers/templates/customer_billing_summary.html'],

    initialize: function (){

    },

    events: {

    },

    save: function(){

        // var name = this.$el.find('#recurring-charge-name').val();
        // var recurringCharge = new RecurringCharge({name: name}, {parse: true});
        // var recurringChargeEdit = new RecurringChargeEditView({model: recurringCharge});
        //
        // recurringChargeEdit.show();
    },

    onRender: function(){
        //this.tabs.show( new CustomerDataTabsView({model: this.model}) );
        //this.summary.show( new CustomerInfoSummaryView({model: this.model}) );
        //this.info.show( new CustomerInfoView({model: this.model}) );
    }
});

var CustomerBillingView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['customers/templates/customer_billing.html'],

    initialize: function (){

    },

    events: {

    },

    save: function(){

        // var name = this.$el.find('#recurring-charge-name').val();
        // var recurringCharge = new RecurringCharge({name: name}, {parse: true});
        // var recurringChargeEdit = new RecurringChargeEditView({model: recurringCharge});
        //
        // recurringChargeEdit.show();
    },

    onRender: function(){
        //this.tabs.show( new CustomerDataTabsView({model: this.model}) );
        //this.summary.show( new CustomerInfoSummaryView({model: this.model}) );
        //this.info.show( new CustomerInfoView({model: this.model}) );
        this.renderBillings();
        this.renderMinimums();
        this.renderInvoices();
    },

    renderBillings: function(){
        var that = this;
        var billings = this.model.get('billings');
        if (typeof billings != 'undefined' && billings.length > 0) {
            _.each(billings, function (billing) {
                that.$el.find('#billing-profiles-table tbody').append("<tr><td>" + billing.title + "</td><td>" + accounting.formatMoney(billing.total) + "</td></tr>");
            });
        }
        else {
            that.$el.find('#billing-profiles-table tbody').append("<tr><td colspan='2'>No Billing Profiles</td></tr>");
        }
    },

    renderMinimums: function(){
        var that = this;
        var minimum_charge_ids = this.model.get('minimum_charge_ids');
        if (typeof minimum_charge_ids != 'undefined' && minimum_charge_ids != null) {
            _.each(minimum_charge_ids, function (minimum_charge_id) {
                var minimum = App.data.minimum_charges.get(minimum_charge_id);
                that.$el.find('#minimum-charges-table tbody').append("<tr><td>" + minimum.get('name') + "</td><td>" + accounting.formatMoney(minimum.get('minimum_amount')) + "</td><td>Every " + minimum.get('frequency') + " " + minimum.get('frequency_period') + "</td></tr>");
            });
        }
        else {
            that.$el.find('#minimum-charges-table tbody').append("<tr><td colspan='2'>No Minimum Charges</td></tr>");
        }

    },

    renderInvoices: function(){
        var that = this;
        var invoices = this.model.get('invoices');
        if (typeof invoices != 'undefined' && invoices.length > 0) {
            _.each(invoices, function (invoice) {
                if (invoice.paid < invoice.total) {
                    that.$el.find('#unpaid-invoice-table tbody').append("<tr><td>#" + invoice.invoice_number + "</td><td>" + accounting.formatMoney(invoice.total) + "</td></tr>");
                }
                else {
                    that.$el.find('#paid-invoice-table tbody').append("<tr><td>#" + invoice.invoice_number + "</td><td>" + accounting.formatMoney(invoice.total) + "</td></tr>");
                }
            });
        }
        else {
            that.$el.find('#unpaid-invoice-table tbody').append("<tr><td colspan='2'>No Invoices</td></tr>");
            that.$el.find('#paid-invoice-table tbody').append("<tr><td colspan='2'>No Invoices</td></tr>");
        }

    }
});

var CustomerMarketingSummaryView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['customers/templates/customer_marketing_summary.html'],

    initialize: function (){
    },

    events: {
        'click .send-invite-link':"sendTextInvite"
    },

    sendTextInvite: function(event){
        this.model.sendTextInvite();
    },

    save: function(){

        // var name = this.$el.find('#recurring-charge-name').val();
        // var recurringCharge = new RecurringCharge({name: name}, {parse: true});
        // var recurringChargeEdit = new RecurringChargeEditView({model: recurringCharge});
        //
        // recurringChargeEdit.show();
    },

    onRender: function(){
        //this.tabs.show( new CustomerDataTabsView({model: this.model}) );
        //this.summary.show( new CustomerInfoSummaryView({model: this.model}) );
        //this.info.show( new CustomerInfoView({model: this.model}) );
    }
});

var CustomerMarketingView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    template: JST['customers/templates/customer_marketing.html'],
    regions: {
        'messenger': '#messenger'
    },
    initialize: function (){

    },

    events: {

    },

    save: function(){

    },

    onRender: function(){
        this.messenger.show(new CustomerMessages({
            model: this.model
        }));
    }
});
