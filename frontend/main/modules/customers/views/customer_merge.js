var MergeCustomersView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['customers/templates/merge_customer_view.html'],
    id: 'merge',

    regions: {
        customer_columns : '.customer-columns'
    },

    ui: {
        customerSearchBox1: '#name-search-1',
        customerSearchBox2: '#name-search-2',
    },

    initialize: function (options){
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
        this.options = options;
    },

    events: {
        "change .customer-merge-checkbox-radio" : "checkOne",
        "change .select-all" : "handleSelectAll",
        "click #save-changes" : "save"
    },

    checkOne: function(e){
        // Deselect
        var clicked_box = $(e.target);
        var clicked_box_parent = clicked_box.closest('.form-group');
        var checked = clicked_box.prop('checked');
        var box_id = clicked_box.attr('id');
        var other_box = box_id.indexOf('1') == -1 ? $('#'+box_id.replace('2','1')) : $('#'+box_id.replace('1','2'));
        var other_box_parent = other_box.closest('.form-group');
        if (checked) {
            other_box.prop('checked', false);
            other_box_parent.removeClass('selected');
            other_box_parent.addClass('not-selected');
            clicked_box.prop('checked', true);
            clicked_box_parent.addClass('selected');
            clicked_box_parent.removeClass('not-selected');
        } else {
            clicked_box.prop('checked', false);
            clicked_box_parent.removeClass('selected');
            clicked_box_parent.addClass('not-selected');
            other_box.prop('checked', true);
            other_box_parent.addClass('selected');
            other_box_parent.removeClass('not-selected');
        }
    },

    handleSelectAll: function(e){
        var clicked_box = $(e.target);
        var type = clicked_box.data('section');
        var checked = clicked_box.prop('checked');
        var side = (clicked_box.attr('id').indexOf('1') != -1) ? 1 : 2;
        side = checked ? side : (side == 1 ? 2 : 1);
        this.selectAll(type, side);
    },

    selectAll: function(type, side){
        switch(type){
            case 'personal':
                this.$el.find('.customer-merge-personal-'+side+' .customer-merge-checkbox-radio:not(.select-all)').each(function(e){
                    if (!$(this).prop("checked"))
                        $(this).click();
                });
                break;
            case 'accounts':
                this.$el.find('.customer-merge-accounts-'+side+' .customer-merge-checkbox-radio:not(.select-all)').each(function(e){
                    if (!$(this).prop("checked"))
                        $(this).click();
                });
                break;
            case 'groups-and-passes':
                this.$el.find('.customer-merge-groups-and-passes-'+side+' .customer-merge-checkbox:not(.select-all)').each(function(e){
                    if (!$(this).prop("checked"))
                        $(this).click();
                });
                break;
        }
    },

    save: function(){
        var self = this;

        // If two people are not selected, error
        var formData = this.$el.find('form#merge_form').serializeArray();
        //var data = JSON.stringify(formData);
        var validation = this.validate();

        if (validation.success) {
            // Save merge
            var view = this;
            this.$el.loadMask();

            $.post(SITE_URL + '/api/customers/0/merge', formData, function(response){
                $.loadMask.hide();
                App.vent.trigger('notification', {'msg': 'Customer merged', 'type': 'success'});
                view.hide();
                if(view.options.selectedModels[0]){
                    view.options.selectedModels[0].fetch();
                }
                if(view.options.selectedModels[1]){
                    view.options.selectedModels[1].destroy();
                }
            },'html');

        } else {
            alert(validation.msg);
        }
    },

    validate: function(){
        //var person_id_1 = this.$el.find('#person_id_1').val();
        var person_id_2 = this.$el.find('#person_id_2').val();
        if (person_id_2 == '') {
            return {'success':false,'msg':'You must select a second individual to merge.'};
        }

        return {'success':true};
    },

    setCustomer:function(customer, target){
        $.loadMask.hide();
        var MergeCustomer = Backbone.Model.extend({defaults:customer});
        $(target).html(new MergeCustomerColumnView({model:new MergeCustomer()}).render().el);
        this.checkBoxes(customer.column);
        this.initSearch();
    },

    initSearch:function(){
        var self = this;

        init_customer_search(this.$el.find('input#name-search-1, input#name-search-2'), function(e, customer, list){
            var search = $(e.target);
            customer.column = search.attr('id') == 'name-search-1' ? 1 : 2;
            var target = search.closest('.merging-customer');
            target.loadMask();
            var mergeChecked = self.$el.find('#primary-customer-checkbox-'+customer.column).attr('checked');
            self.setCustomer(customer, target);
            if (mergeChecked) {
                $('#primary-customer-checkbox-'+customer.column).click();
            }
        });
    },

    checkBoxes:function(column){

        if (column == 1) {
            $('#select-all-personal-checkbox-1').click();
            $('#select-all-accounts-checkbox-1').click();
            //$('#select-all-groups-and-passes-checkbox-1').click();
            this.selectAll('personal',1);
            this.selectAll('accounts',1);
            //this.selectAll('groups-and-passes',1);
        }
    },

    onRender: function(){
        this.initSearch();
        var selectedModels = this.options.selectedModels;
        var mergingCustomerColumns = this.$el.find('.merging-customer');
        var customer1 = selectedModels[0].attributes;
        customer1.column = 1;
        customer1.person_id = customer1.id;
        //customer1.price_class = 'NOT PULLING YET';
        this.setCustomer(customer1, mergingCustomerColumns[0]);
        if (typeof selectedModels[1] != 'undefined') {
            var customer2 = selectedModels[1].attributes;
            customer2.column = 2;
            customer2.person_id = customer2.id;
            //customer2.price_class = 'NOT PULLING YET';
            this.setCustomer(customer2, mergingCustomerColumns[1]);
        } else {
            var customer2 = {};
            customer2.column = 2;
            customer2.person_id = 0;
            this.setCustomer(customer2, mergingCustomerColumns[1]);
        }
    }

});

var MergeCustomerColumnView = Backbone.Marionette.ItemView.extend({

    template: JST['customers/templates/merge_customer_column_view.html'],

    initialize: function (){
    },

    events: {

    },


    onRender: function(){
    }

});

