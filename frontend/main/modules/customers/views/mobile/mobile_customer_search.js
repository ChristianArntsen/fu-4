
var MobileCustomerSearch = Backbone.Marionette.ItemView.extend({

    template: JST['customers/templates/mobile/basic_search.html'],


    initialize: function(){

    },
    events: {
        'keypress @ui.searchBox': 'setQ',
    },
    ui: {
        searchBox: '.typeahead-customer'
    },
    timeout: false,
    setQ:function(){
        var self = this;
        //if you already have a timout, clear it
        if(this.timeout){ clearTimeout(this.timeout);}

        //start new time, to perform ajax stuff in 500ms
        this.timeout = setTimeout(function() {
            self.model.set("q",self.ui.searchBox.val())
        },500);

    },

    onRender: function(){
        var self = this;

    }
});