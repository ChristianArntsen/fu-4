Backgrid.Customers = {};

Backgrid.Customers.Row = Backgrid.Row.extend({

    events:{
        click:function(e){
            var self = this;
            if (e.target.nodeName != 'INPUT' && (e.target.nodeName != 'BUTTON' && e.target.parentNode.nodeName != 'BUTTON')){
                var selected = this.$el.find(":checkbox").prop("checked");
                this.model.trigger('backgrid:select', this.model, ! selected);


                //TODO: This needs to be refactored, it's specific to customers and clones code below
                var person_id = this.model.get('id');

                var customer = new Customer({
                    person_id: person_id
                });

                var customerEditModal = new EditCustomerView({model: customer});
                customerEditModal.show();
                $('.modal-content').loadMask();

                customer.fetch({
                    silent: true,
                    data: {
                        include_rainchecks: true,
                        include_credit_cards:true,
                        include_marketing: true,
                        include_account_transactions: true,
                        include_billing: true,
                        include_last_visit: true
                    },
                    success: function(model){
                        customerEditModal.render();

                        // Delay the listener because a sync event is triggered
                        // AFTER this success callback (which we don't care about)
                        setTimeout(function(){
                            customer.on('sync', function(){
                                self.model.fetch({
                                    data:{
                                        include:"customerGroups"
                                    }
                                });
                            });
                        }, 10);
                    }
                });
            }
        }
    }
});

Backgrid.Customers.GroupsCell = Backgrid.Cell.extend({
    render: function () {
        this.$el.empty();
        var rawValue = this.model.get(this.column.get("name"));
        var formattedValue = [];
        _.forEach(rawValue.models,function(toRenderObject,key){
            formattedValue.push(toRenderObject.get("label"));
        }),

            this.$el.append($("<span>").text(formattedValue.join(", ")));
        return this;
    }
});

Backgrid.Customers.HistoryCell = Backgrid.Cell.extend({
    template:"<div class='customer-history btn btn-sm btn-primary'>View History</div>",
    events:{
        "click .customer-history":"handleClick"
    },
    handleClick:function(e){
        e.preventDefault();
        e.stopPropagation();
        var personId = this.model.get("id");

        var URL = "/index.php/reports?";
        var reportType = "customers";
        var start = moment().subtract(1,"year").startOf("day").format("MMM D, YYYY hh:mmA");//"Jun 2, 2016 12:00AM";
        var end = moment().endOf("day").format("MMM D, YYYY hh:mmA");//"Jun 2, 2017 11:59PM";
        var jsStart = moment().subtract(1,"year").format("YYYY-MM-DD HH:mm:ss");//"2017-06-02 11:43:24";
        var jsEnd = moment().format("YYYY-MM-DD HH:mm:ss");//"2017-06-02 11:43:24";
        var customerId = this.model.get("id");
        var customerName = this.model.get("last_name")+", "+this.model.get("first_name");
        URL += "report_type=customers";
        URL += "&start="+start;
        URL += "&end="+end;
        URL += "&js_start="+jsStart;
        URL += "&js_end="+jsEnd;
        URL += "&customers_id="+customerId;
        URL += "&customers_name="+customerName;
        URL += "&sale_type=all";
        var win = window.open(URL, '_blank');
        win.focus();
    },
    render: function () {

        this.$el.html(this.template);
        return this;
    }
})

Backgrid.Customers.MenuCell = Backgrid.Cell.extend({

    template:JST['customers/templates/customers_menu.html'],
    events: {
        "click .edit": "editCustomer",
        "click .delete": "deleteCustomer",
        "click .undelete": "undeleteCustomer",
    },
    undeleteCustomer:function(e){
        e.preventDefault();
        this.model.set("deleted",false);
        this.model.save();
        this.model.trigger('destroy', this.model, this.model.collection);
    },
    deleteCustomer : function(e){
        var result = confirm("Are you sure you want to delete this customer?");
        if (result) {
            this.model.destroy();
        }
        e.preventDefault();
        e.stopPropagation();


    },
    editCustomer:function(e){
        var self = this;
        e.preventDefault();
        var person_id = this.model.get('id');

        var customer = new Customer({
            person_id: person_id
        });

        var customerEditModal = new EditCustomerView({model: customer});
        customerEditModal.show();
        $('.modal-content').loadMask();

        customer.fetch({
            silent: true,
            data: {
                include_rainchecks: true,
                include_credit_cards: true,
                include_marketing: true,
                include_account_transactions: true,
                include_billing: true,
                include_last_visit: true
            },
            success: function(model){
                customerEditModal.render();

                // Delay the listener because a sync event is triggered
                // AFTER this success callback (which we don't care about)
                setTimeout(function(){
                    customer.on('sync', function(){
                        self.model.fetch({
                            data:{
                                include:"customerGroups"
                            }
                        });
                    });
                }, 10);
            }
        });
    },

    render: function () {

        this.$el.html(this.template({}));
        return this;
    }
});

var CustomersTableView = Backbone.Marionette.ItemView.extend({
	template: JST['customers/templates/customers_table.html'],
    controls:{
        bulkDelete:function(){
            var result = confirm("Are you sure you want to delete all these records?");
            if (result) {
                _.forEach(this.grid.getSelectedModels(),function(model){
                    model.destroy();
                })
            }

        },
        merge:function(){
            var selectedModels = this.grid.getSelectedModels();
            if (selectedModels.length > 2) {
                alert('Too many customers selected');
                return false;
            }

            var mergeModal = new MergeCustomersView({selectedModels:selectedModels});
            mergeModal.show();
        },
        bulkEdit:function(){

            if(this.grid.collection.selectAll){
                var bulkEditJob = new BulkEditJob({
                    type: 'customers'
                });

                var viewParams = {
                    model: bulkEditJob,
                    recordCollection: this.grid.collection
                };

            }else{
                var ids = [];
                _.each(this.grid.getSelectedModels(), function(model){
                    if(model && typeof model.get == 'function'){
                        ids.push(model.get('id'));
                    }
                });

                var bulkEditJob = new BulkEditJob({
                    type: 'customers',
                    recordIds: ids,
                    totalRecords: ids.length
                });

                var viewParams = {
                    model: bulkEditJob
                };
            }

            window.parent.App.data.bulk_edit_jobs.add(bulkEditJob);

            var bulkEditModal = new BulkEditView(viewParams);
            bulkEditModal.show();

            return false;
        }
    },
    behaviors: function(){
        return {
            TableBehavior: {
                behaviorClass: Marionette.TableBehavior,
                pageSettings:this.options.settings,
                controls:this.controls,
                row: Backgrid.Customers.Row
            }
        }
    }
});
