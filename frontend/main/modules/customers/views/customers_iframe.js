var CustomersIframeLayout = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    className: 'row',
    template: function(){ return ''; },

    onShow: function(){

        // Create a new iframe and attach it to the body of the document (if it doesn't exist yet)
        if($('#customers_iframe').length == 0) {
            $('<iframe class="page-iframe" src="' +SITE_URL+ '/customers?in_iframe=1" id="customers_iframe"></iframe>').appendTo('#page-iframe-container');
        }else{
            $('#customers_iframe').show();
        }

        $('body').addClass('iframe');
    },

    onBeforeDestroy: function () {
        $('#customers_iframe').hide();
        $('body').removeClass('iframe');
    }
});