var GroupsManageView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['customers/templates/groups/manage.html'],

    regions: {
        'group_list': '.group-list'
    },

    initialize: function () {
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
    },

    events: {
        'click #add-group': 'addGroup',
        'click #save-changes': 'save'
    },

    addGroup:function(){
        var new_label = this.$el.find('#new-group-label').val();
        if (new_label == '')
        {
            alert('Please name the new group before adding it.');
            return false;
        }

        console.log('confused about addGroup');
        var new_group = {
            'label': new_label,
            'itemDiscount': this.$el.find('#new-item-discount').val()
        };
        var saved_group = this.collection.create(new CustomerGroupModel(new_group));
        this.$el.find('.group-list').append( new GroupView({model: saved_group}).render().el );
        this.$el.find('#new-group-label').val('');
        this.$el.find('#new-item-discount').val('');

    },

    save: function () {
        _.each(this.collection.models, function(model){model.save()}, this);
        this.hide();
    },

    onRender: function(){
        if (this.collection.length == 0) {
            this.$el.loadMask();
            var that = this;
            this.collection.fetch({
                success: function () {
                    _.each(that.collection.models, that.showGroup, that);
                    $.loadMask.hide();
                }
            });
        }
        else {
            _.each(this.collection.models, this.showGroup, this);
        }
        return this;
    },

    showGroup: function(group){
        console.log('showGroup');
        console.dir(group);
        this.$el.find('.group-list').append( new GroupView({model: group}).render().el );
    },

});