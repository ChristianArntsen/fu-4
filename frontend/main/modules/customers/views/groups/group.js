var GroupView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['customers/templates/groups/group.html'],

    initialize: function () {

    },

    events: {
        "keyup .group-label": "updateLabel",
        "keyup .item-discount": "updateItemDiscount",
        "click .group-delete": "removeGroup"
    },

    updateLabel: function(){
        var groupLabel = this.$el.find('.group-label').val();
        this.model.set('label', groupLabel);
        console.dir(this.model);
    },

    updateItemDiscount: function(){
        var itemDiscount = this.$el.find('.item-discount').val();
        this.model.set('itemDiscount', itemDiscount);
        console.dir(this.model);
    },

    removeGroup: function(){
        this.model.destroy();
        this.remove();
    },

    save: function () {

    },

    onRender: function(){
    }

});