var EditCustomerView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['customers/templates/edit_customer_view.html'],
    defaultTab:"info",
    regions: {
        'summary': '.customer-summary',
        'tabs': '.customer-tabs',
        'info': '.customer-info'
    },

    initialize: function (){
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
        this.listenTo(App.vent, 'customer:photo', this.render_photo);
    },

    events: {
        'click #save-changes': 'save',
        'click .tab-selector-accounts': 'loadAccounts',
        'click .tab-selector-info': 'loadInfo',
        'click .tab-selector-billing': 'loadBilling',
        'click .tab-selector-marketing': 'loadMarketing'
    },
    save: function(){
        var view = this;

        this.$el.loadMask();
        //TODO: Add in support for minimums, this currently doesn't exist because we are redoing minimums
        delete this.model.attributes.minimum_charge_ids;
        delete this.model.attributes.minimum_charges;

        this.model.save(null, {
            success: function(){
                App.vent.trigger('notification', {'msg': 'Customer saved', 'type': 'success'});
                view.hide();
            },
            error: function(model, response){
                App.vent.trigger('notification', {'msg': response.responseJSON.msg, 'type': 'danger'});
                $.loadMask.hide();
            }
        });
    },

    render_photo: function(data){
        var url = data.url + '?t=' + moment().unix(); // Force image to refresh
        this.$el.find('img.customer-photo').prop('src', url);
        this.model.set({image_id:data.image_id});
        return false;
    },

    loadInfo: function(e){
        this.selectTab('.tab-selector-info');
        this.summary.show( new CustomerInfoSummaryView({model: this.model}) );
        this.info.show( new CustomerInfoView({model: this.model}) );
    },

    loadAccounts: function(e){
        this.selectTab('.tab-selector-accounts');
        this.summary.show( new CustomerAccountsSummaryView({model: this.model}));
        this.info.show( new CustomerAccountsView({model: this.model}));
    },

    loadBilling: function(e){
        this.selectTab('.tab-selector-billing');
        this.summary.show( new CustomerBillingSummaryView({model: this.model}));
        this.info.show( new CustomerBillingView({model: this.model}));
    },

    loadMarketing: function(e){
        this.selectTab('.tab-selector-marketing');
        this.summary.show( new CustomerMarketingSummaryView({model: this.model}));
        this.info.show( new CustomerMarketingView({model: this.model}));
    },

    selectTab: function(tab){
        $('.selected-tab').removeClass('selected-tab');
        $(tab).parent('div').addClass('selected-tab');
    },

    onRender: function(){
        var helpers = {
            get_required_text: this.get_required_text,
            field_settings: App.data.course.get('customer_field_settings'),
            is_required_field: this.is_required_field,
            member_account_label: App.data.course.get('member_balance_nickname'),
            customer_account_label: App.data.course.get('customer_credit_nickname')
        };
        var attr = this.model.attributes;
        attr = _.extend(attr, helpers);

        this.tabs.show( new CustomerDataTabsView({model: this.model}) );
        this.loadInfo();
        if(this.defaultTab == "marketing"){

            this.loadMarketing();
        }
    },

    is_required_field: function(field){
        if(this.field_settings[field] &&
            this.field_settings[field].required &&
            this.field_settings[field].required == 1
        ){
            return true;
        }
        return false;
    },

    get_required_text: function(field){
        if(this.is_required_field(field)){
            return '<span class="text-danger" style="font-size: 1.5em; line-height: 0.25em">*</span>';
        }
        return '';
    }
});

var AddCustomerCreditCardsView = ModalLayoutView.extend({

    tagName: 'div',
    id:'manage-credit-cards',
    template: JST['customers/templates/add_customer_credit_cards_view.html'],

    initialize: function () {
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
    },

    onRender: function(){
        var url = "/index.php/customers/open_add_credit_card_window/"+this.model.get('person_id');
        this.$el.find('#payment_iframe').attr('src', url);
    }
});