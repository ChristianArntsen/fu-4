var StatementPayment = Backbone.JsonRestModel.extend({

    type: 'accountStatementPayments',
    defaults: {
        type: '',
        amount: 0
    }
});

Backbone.modelFactory.registerModel(StatementPayment);