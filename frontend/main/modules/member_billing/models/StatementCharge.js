var StatementCharge = Backbone.JsonRestModel.extend({

    type: 'accountStatementCharges',
    defaults: {
        name: ''
    }
});

Backbone.modelFactory.registerModel(StatementCharge);