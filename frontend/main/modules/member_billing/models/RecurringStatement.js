var RecurringStatement = Backbone.JsonRestModel.extend({

    type: 'accountRecurringStatements',
    relations: {
        repeated: {
            type: 'repeated',
            many: false
        }
    },

    urlRoot: REST_API_COURSE_URL + '/accountRecurringStatements',
    defaults: {
        isActive: true,
        isDefault: false,
        sendEmptyStatements: true,
        includePastDue: false,
        includeMemberTransactions: false,
        includeCustomerTransactions: false,
        sendEmail: false,
        sendMail: false,
        dueAfterDays: 0,
        autopayAttempts: 1,
        autopayEnabled: false,
        autopayAfterDays: 0,
        autopayOverdueBalance: false,
        payMemberBalance: false,
        payCustomerBalance: false,
        sendZeroChargeStatement: false,
        financeChargeEnabled: false,
        financeChargeAfterDays: null,
        financeChargeType: 'percent',
        financeChargeAmount: null,
        termsAndConditionsText: null,
        footerText: null,
        messageText: null,
        repeated: {},
        _account_charge_settings_complete: false
    },

    initialize: function(options){

        if(this.get('id')){
            this.set('_account_charge_settings_complete', true);
        }
    }
});

var RecurringStatementCollection = Backbone.JsonRestCollection.extend({
    model: RecurringStatement,
    url: REST_API_COURSE_URL + '/accountRecurringStatements',
    initialize: function(){
        this.fetch({
            data: {
                isDefault: 1
            }
        });
    }
});

Backbone.modelFactory.registerModel(RecurringStatement, RecurringStatementCollection);