var RecurringCharge = Backbone.JsonRestModel.extend({

    type: 'accountRecurringCharges',
    urlRoot: REST_API_COURSE_URL + '/accountRecurringCharges',
    idAttribute: 'id',

    url: function(){
        if(!this.id){
            return REST_API_COURSE_URL + '/accountRecurringCharges?include[]=items&include[]=recurringStatement';
        }else{
            return REST_API_COURSE_URL + '/accountRecurringCharges/' +this.id+'?include[]=items&include[]=recurringStatement'
        }
    },

    relations: {
        recurringStatement: {
            type: 'accountRecurringStatements',
            many: false
        },
        items: {
            type: 'accountRecurringChargeItems',
            many: true
        }
    },

    defaults: {
        name: '',
        description: '',
        is_active: true,
        prorate_charges: 1,
        items: [],
        recurringStatement: {},
        totalCharges: 0
    },

    initialize: function(){
        this.listenTo(this.get('items'), 'add remove reset', this.calculateTotalCharges);
        this.calculateTotalCharges();
    },

    calculateTotalCharges: function(){
        if(!this.get('items') || this.get('items').length == 0){
            return 0;
        }
        var total = 0;

        _.each(this.get('items').models, function(item){
            total += item.get('item').get('total');
        });
        this.set('totalCharges', total);
    }
});

var RecurringChargeCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/accountRecurringCharges',
    model: RecurringCharge,

    dropdownData: function(){

        var options = {0: '- Select Billing Membership -'};
        _.each(this.models, function(model){
            options[model.get('id')] = model.get('name');
        });
        return options;
    },

    fetch: function(options) {
        if(!options)
            options = {};

        if(!options.data){
            options.data = {};
            options.data.include = ['items'];
        }

        if(this.searchParams){
            options.data = this.searchParams;
        }

        return Backbone.JsonRestCollection.prototype.fetch.call(this, options);
    }
});

Backbone.modelFactory.registerModel(RecurringCharge, RecurringChargeCollection);