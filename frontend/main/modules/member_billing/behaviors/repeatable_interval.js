var RepeatableIntervalBehavior = Marionette.Behavior.extend({

    events: {
        "blur #repeat-interval": "setInterval",
        "click #repeat-interval": "resetInterval"
    },

    resetInterval: function(){

        var data = {
            interval: 1
        };

        if(this.view.model.get('period') == 'daily'){
            data.byweekday = null;
        }else if(this.view.model.get('period') == 'monthly'){
            data.bymonth = null;
        }
        this.view.model.set(data);
    },

    setInterval: function(interval){

        if(this.$el.find('#repeat-interval').val() === ''){

            if(
                (this.view.model.get('period') == 'yearly' && !this.view.model.get('bymonth')) ||
                (this.view.model.get('period') == 'monthly' && !this.view.model.get('bymonth')) ||
                (this.view.model.get('period') == 'weekly') ||
                (this.view.model.get('period') == 'daily' && !this.view.model.get('byday'))
            ){
                var data = {interval: 1};
                if(this.view.model.get('period') == 'daily'){
                    data.byweekday = null;
                }else if(this.view.model.get('period') == 'monthly'){
                    data.bymonth = null;
                }
                this.view.model.set(data);
                this.$el.find('#repeat-interval').val(1);
            }
            return false;
        }
        var interval = parseInt(this.$el.find('#repeat-interval').val());

        if(!interval || interval < 0){
            this.$el.find('#repeat-interval').val(this.view.model.get('interval'));
            return false;
        }

        var data = {
            interval: interval
        };

        if(this.view.model.get('period') == 'daily'){
            data.byweekday = null;
        }else if(this.view.model.get('period') == 'monthly'){
            data.bymonth = null;
        }
        this.view.model.set(data);
    }
});