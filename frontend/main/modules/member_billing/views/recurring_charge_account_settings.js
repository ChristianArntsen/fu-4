var RecurringChargeAccountSettingsView = Marionette.ItemView.extend({

    tagName: 'div',
    className: 'recurring-schedule-item-details',
    template: JST['member_billing/templates/recurring_charge_account_settings.html'],

    events: {
        'change #statement-pay-member-balance': 'setData',
        'change #statement-pay-customer-balance': 'setData'
    },

    initialize: function(){
        this.model.set('_account_charge_settings_complete', true);
    },

    setData: function(){
        var payMemberBalance = this.$el.find('#statement-pay-member-balance').is(':checked');
        var payCustomerBalance = this.$el.find('#statement-pay-customer-balance').is(':checked');

        this.model.set({
            'payMemberBalance': payMemberBalance,
            'payCustomerBalance': payCustomerBalance
        });
    }
});