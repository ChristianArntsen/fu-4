var RecurringChargesView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['member_billing/templates/recurring_charges.html'],
    id: 'recurring-charges',

    regions: {
        'charges': '.recurring-charges'
    },

    initialize: function (){
        this.listenTo(this.collection, 'sync', this.hideLoading);
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
    },

    showLoading: function(model, xhr, options){
        this.$el.find('.modal-body').loadMask();
    },

    hideLoading: function(){
        $.loadMask.hide();
    },

    events: {
        'click #new-membership': 'newCharge'
    },

    newCharge: function(){

        var name = this.$el.find('#recurring-charge-name').val();
        this.$el.find('#recurring-charge-name').val('');

        var recurringCharge = new RecurringCharge({name: name});
        var recurringChargeEdit = new RecurringChargeEditView({model: recurringCharge});

        recurringChargeEdit.show();
    },

    onRender: function(){
        this.charges.show( new RecurringChargeListView({collection: this.collection}) );
        this.showLoading();
    }
});