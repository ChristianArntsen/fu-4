var RecurringChargeScheduleListItemView = Marionette.ItemView.extend({

    tagName: 'div',
    className: 'row',
    template: JST['member_billing/templates/recurring_charge_schedule_list_item.html'],

    events: {
        'click': 'select'
    },

    modelEvents: {
        'change:_schedule_visible': 'toggleSelected',
        'change:_schedule_completion': 'render'
    },

    onRender: function(){
        this.toggleSelected();
    },

    select: function(){
        this.model.collection.toggleVisible(this.model.cid);
        return true;
    },

    toggleSelected: function(){
        if(this.model.get('_schedule_visible')){
            this.$el.find('.recurring-schedule-tab').addClass('active');
        }else{
            this.$el.find('.recurring-schedule-tab').removeClass('active');
        }
        return true;
    }
});

var RecurringChargeScheduleListEmptyView = Marionette.ItemView.extend({
    template: function(){
        return '<div class="row line-item-empty text-muted"><div class="col-md-12">Use the search box above to add items or fees</div></div>';
    }
});

var RecurringChargeScheduleItemListView = Marionette.CollectionView.extend({

    tagName: 'div',
    events: {},
    childView: RecurringChargeScheduleListItemView,
    emptyView: RecurringChargeScheduleListEmptyView
});