var RepeatablePeriodSelectorView = Marionette.ItemView.extend({

    tagName: 'div',
    className: 'row',
    template: JST['member_billing/templates/repeatable_period_selector.html'],

    events: {
        'click button.period': 'setPeriod'
    },

    initialize: function(){
        this.listenTo(this.model, 'change:period');
    },

    setPeriod: function (e) {

        var button = $(e.currentTarget);
        button.parents('.btn-group').find('button').removeClass('active');
        button.addClass('active');

        var repeatable = this.model;
        var period = button.data('value');
        var data = {};

        if (period == 'monthly') {
            data.freq = RRule.MONTHLY;
            data.bymonth = null;

        } else if (period == 'quarterly') {
            data.freq = RRule.MONTHLY;
            data.bymonth = null;

        } else if (period == 'yearly') {
            data.freq = RRule.YEARLY;
            data.bymonth = null;
        }
        data.period = period;
        data.interval = null;
        repeatable.set(data);

        return false;
    }
});