var RepeatableYearlyView = Backbone.Marionette.ItemView.extend({

    tagName: "div",
    className: "col-md-12",
    template: JST['member_billing/templates/repeatable/yearly.html'],

    events: {
        'click .repeat-day-select': 'setDay',
        'click .repeat-month': 'setMonth'
    },

    setMonth: function(e){

        var button = $(e.currentTarget);
        var repeatable = this.model;
        var data = {};
        var month = parseInt(button.data('value'));
        var period = repeatable.get('period');

        button.parents('.repeat-month-selector').find('button').removeClass('active');
        button.addClass('active');

        data.bymonth = [month];
        data.interval = null;
        repeatable.set(data);
    },

    setDay: function(e){
        var button = $(e.currentTarget);
        button.siblings().removeClass('active');
        button.addClass('active');
        var day = parseInt(button.data('value'));
        var data = {
            bymonthday: [day],
            byweekday: [],
            bysetpos: [1]
        };
        this.model.set(data);
    },

    behaviors: function() {
        return {
            RepeatableEffectiveDates: {
                behaviorClass: RepeatableEffectiveDatesBehavior
            },
            RepeatableInterval: {
                behaviorClass: RepeatableIntervalBehavior
            }
        }
    }
});