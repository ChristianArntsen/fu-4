var RepeatableVisualizationView = Marionette.ItemView.extend({

    tagName: 'div',
    className: 'repeatable-visualization-container',
    template: JST['member_billing/templates/repeatable/visualization.html'],

    events: {
        'click .scroll-left': 'scrollLeft',
        'click .scroll-right': 'scrollRight'
    },

    initialize: function(){

        var view = this;
        var charges = new Backbone.Collection();
        _.each(this.collection.models, function(model){
            charges.add(model.get('repeated'));
        });
        this.charges = charges;

        if(this.collection) {
            this.listenTo(this.collection, 'add', function (model) {
                if (model.get('repeated')) {
                    model.get('repeated').set({'_active': model.get('_active')}, {silent: true});
                    view.charges.add(model.get('repeated'));
                }
            });
            this.listenTo(this.collection, 'remove', function (model) {
                if (model.get('repeated')) {
                    view.charges.remove(model.get('repeated'));
                }
            });

            this.listenTo(this.collection, 'change:_active', this.highlightCharges);
        }

        if(this.model){
            this.listenTo(this.model, 'change:bymonth change:period change:freq change:interval change:byweekday change:bymonthday change:bysetpos change:until change:dtstart', this.render);
        }
        this.listenTo(this.charges, 'add remove change:bymonth change:period change:freq change:interval change:byweekday change:bymonthday change:bysetpos change:until change:dtstart', this.render)
    },

    highlightCharges: function(model){
        model.get('repeated').set('_active', model.get('_active'));
        if(!model.get('_active')){
            return false;
        }
        this.$el.find('div.billing-day').removeClass('active');
        this.$el.find('div.billing-day.day-' + model.get('line_number')).addClass('active');
    },

    onRender: function(){

    },

    parseRrule: function(model){

        var data = model.toJSON();
        if(typeof(data.freq) === 'undefined' || data.freq === null || data.freq === ''){
            return false;
        }

        // If dtstart or until is set, we need the actual date objects,
        // since the above .toJSON() method makes our date objects into strings
        if(data.dtstart){
            data.dtstart = model.get('dtstart');
        }else{
            data.dtstart = null;
        }
        if(data.until){
            data.until = model.get('until');
        }else{
            data.until = null;
        }

        var rruleParams = _.pick(data, [
            'byhour',
            'byminute',
            'byweekday',
            'bymonth',
            'bymonthday',
            'bysecond',
            'bysetpos',
            'byweekno',
            'byyearday',
            'dtstart',
            'freq',
            'interval',
            'until',
            'wkst'
        ]);

        // Remove empty data
        rruleParams = _.pick(rruleParams, function(value, key){
            if(value === null || (typeof(value.length) != 'undefined' && value.length == 0)){
                return false;
            }
            return true;
        });

        data.rrule = new RRule(rruleParams);
        var in3years = moment().add(3, 'years').toDate();
        data.rules = data.rrule.between(new Date(), in3years);

        return data;
    },

    serializeData: function(){

        var view = this;
        var data = {
            period: false,
            days: []
        };

        if(this.model){
            data.period = this.parseRrule(this.model);
        }

        if(this.charges){

            _.each(this.charges.models, function(model){
                var repeated = model;
                if(model.get('repeated')){
                    repeated = model.get('repeated');
                }
                var rRuleData = view.parseRrule(repeated);

                rRuleData._active = false;
                if(repeated.get('_active')){
                    rRuleData._active = repeated.get('_active');
                }

                if(rRuleData){
                    data.days.push(rRuleData);
                }
            });
        }

        return data;
    },

    scrollLeft: function(){
        var scrollPos = this.$el.find('div.repeatable-visualization').scrollLeft();
        this.$el.find('div.repeatable-visualization').scrollLeft( scrollPos - 300 );
        return false;
    },

    scrollRight: function(){
        var scrollPos = this.$el.find('div.repeatable-visualization').scrollLeft();
        this.$el.find('div.repeatable-visualization').scrollLeft( scrollPos + 300 );
        return false;
    }
});