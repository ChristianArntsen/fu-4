var RepeatableWeeklyView = Backbone.Marionette.ItemView.extend({

    tagName: "div",
    className: "col-md-12",
    template: JST['member_billing/templates/repeatable/weekly.html'],

    events: {
        'click .repeat-day': 'selectDay'
    },

    selectDay: function(e){

        var button = $(e.currentTarget);
        var weekday = parseInt(button.data('value'));

        this.$el.find('button.repeat-day').removeClass('active');
        button.addClass('active');

        var data = {
            freq: RRule.WEEKLY,
            byweekday: [weekday],
            bymonthday: null,
            bymonth: null
        };

        this.model.set(data);
    },

    behaviors: function() {
        return {
            RepeatableEffectiveDates: {
                behaviorClass: RepeatableEffectiveDatesBehavior
            },
            RepeatableInterval: {
                behaviorClass: RepeatableIntervalBehavior
            }
        }
    }
});