var RepeatableDaySelectorView = Marionette.ItemView.extend({

    tagName: 'div',
    className: 'row',
    template: JST['member_billing/templates/repeatable_day_selector.html'],

    events: {
        'click button.period': 'setPeriod',
        'change #repeat-charge-set-position': 'setDynamicDay',
        'change #repeat-charge-weekday': 'setDynamicDay',
        'click .repeat-day-selector': 'toggleChargeDay',
        'click .repeat-dynamic-day': 'toggleDynamicChargeDay',
        'click button.repeat-day-select': 'setDay'
    },

    initialize: function(){
        this.listenTo(this.model, 'change:bymonthday change:byweekday', this.render);
    },

    onRender: function(){
        if(this.model.get('bymonthday') || !this.model.get('byweekday')){
            this.toggleChargeDay();
        }else{
            this.toggleDynamicChargeDay();
        }
    },

    toggleChargeDay: function(){
        this.$el.find('.repeat-day-selector').removeClass('disabled');
        this.$el.find('.repeat-dynamic-day').addClass('disabled');
    },

    toggleDynamicChargeDay: function(){
        this.$el.find('.repeat-day-selector').addClass('disabled');
        this.$el.find('.repeat-dynamic-day').removeClass('disabled');
        this.setDynamicDay();
    },

    setDynamicDay: function(){
        var setPosition = this.$el.find('#repeat-charge-set-position').val();
        var weekday = this.$el.find('#repeat-charge-weekday').val();
        var data = {
            bymonthday: null,
            bysetpos: [parseInt(setPosition)],
            byweekday: [parseInt(weekday)]
        };
        this.model.set(data);
        this.$el.find('button.repeat-day-select').removeClass('active');
    },

    setDay: function(e){
        var button = $(e.currentTarget);
        button.siblings().removeClass('active');
        button.addClass('active');
        var day = parseInt(button.data('value'));
        var data = {
            bymonthday: [day],
            byweekday: [],
            bysetpos: [1]
        };
        this.model.set(data);
    }
});