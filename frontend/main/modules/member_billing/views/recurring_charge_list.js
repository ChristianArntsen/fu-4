var RecurringChargeListItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    className: '',
    template: JST['member_billing/templates/recurring_charge_list_item.html'],

    events:{
        'click a.edit': 'editCharge',
        'click a.delete': 'deleteCharge'
    },

    modelEvents: {
        'change': 'render'
    },

    editCharge: function(){
        var recurringChargeEdit = new RecurringChargeEditView({model: this.model});
        recurringChargeEdit.show();
        return false;
    },

    deleteCharge: function(){
        this.model.destroy();
        return false;
    }
});

var RecurringChargeListView = Marionette.CompositeView.extend({

    tagName: 'table',
    className: 'table table-light',
    template: JST['member_billing/templates/recurring_charge_list.html'],
    childView: RecurringChargeListItemView,
    childViewContainer: 'tbody'
});