var bulk_edit_support_layout = Backbone.Marionette.LayoutView.extend({

    tagName: 'section',
    attributes: {
        id: 'bulk-edit-support'
    },
    template: JST['bulk_edit_support/templates/bulk_edit_support.layout.html'],
    regions: {
        left_nav: '#bulk-edit-support-left-nav',
        disp_area: '#bulk-edit-support-disp-area'
    },

    events: {
        'click button.course': 'load_course_logs'
    },

    onRender: function(){
        this.left_nav.show(new BulkEditSupportLeftNav());
        //this.disp_area.show(new PersonAlteredTable());
    },

    load_course_logs: function(e){
        App.data.course_selected = [e.target.dataset.id,e.target.dataset.name];
        this.disp_area.show(new PersonAlteredTable());
    }
});