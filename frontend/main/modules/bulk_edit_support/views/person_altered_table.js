// Display person altered table
var PersonAlteredTable = Backbone.Marionette.ItemView.extend({
    template: JST['bulk_edit_support/templates/person_altered_table.html'],
    initialize: function() {
        this.collection = App.data.employee_audit_log;
        this.listenTo(this.collection, 'sync', this.updateRow);
        this.handleTagUpdate();
    },
    handleTagUpdate: function(){
        this.render();
    },
    
    updateRow: function(model){
        var index = this.collection.indexOf(model);
        var rowData = model.toJSON();

        this.$el.find('table.person-altered').bootstrapTable('updateRow', {
            index: index,
            row: rowData
        });
    },

    events: {
        'click button.undo': function(e){
            var target = e.target.dataset.id;
            this.collection.undo_action(target,function(res){
                console.log(res);
                var row;
                var disp = 'Table,Affected Rows\n';
                for(var i=0;i<res.data.meta.tables.length;i++){
                    row = res.data.meta.tables[i];
                    disp += row.table+','+row.affected_rows+'\n';
                }
                window.alert(disp);
            });
            return;
        },
        'click button.redo': function(e){
            var target = e.target.dataset.id;
            this.collection.redo_action(target,function(res){
                    console.log(res);
                    var row;
                    var disp = 'Table,Affected Rows\n';
                    for(var i=0;i<res.data.meta.tables.length;i++){
                        row = res.data.meta.tables[i];
                        disp += row.table+','+row.affected_rows+'\n';
                    }
                    window.alert(disp);
            });
        }
    },

    onRender: function(){
        var view = this;
        var id = App.data.course_selected[0];
        var nm = App.data.course_selected[1];
        this.$el.find('.table-title')[0].innerHTML = nm;
        if(!id)return;
        console.log(this);
        this.table = this.$el.find('table.person-altered').bootstrapTable({
            classes: 'table-no-bordered table-no-background col-xs-12',
            data: this.collection.rawToJSON(),

            url:'/api_rest/index.php/courses/'+id+'/employee_audit_log',
            dataField:'data',
            uniqueId: 'id',
            responseHandler: function(res){
                console.log('res',res);
                for(var i=0;i<res.data.length;i++){
                    res.data[i].attributes.id = res.data[i].id;
                    res.data[i] = res.data[i].attributes;
                }
                return res;
            },
            sidePagination: 'client',
            //sortName:'gmt_logged',
            //sortOrder:'desc',
            columns: [
                {
                    title: 'Action',
                    field: 'action_type_name',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            val +
                            '</div>'
                    }
                },
                {
                    title: 'Description',
                    field: 'action_type_desc',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            val +
                            '</div>'
                    }
                },
                {
                    title: 'Employee',
                    field: 'username',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            val +
                            '</div>'
                    }
                },
                {
                    title: 'Date Time',
                    field: 'gmt_logged',
                    formatter: function(val, row, idx){
                        //return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            return val.date// +
                            //'</div>'
                    },
                    search:true,
                    sortable:true,
                    halign:'center',
                    sorter:function(a,b){
                        console.log('sorting?')
                        return new Date(a.date) - new Date(b.date);
                    }
                },
                {
                    field: 'id',
                    formatter: function(val, row, idx){
                        if(row.action_type_id !== 11)return '';
                        return '<div class="col-xs-6" style="padding-bottom: 15px;">' +
                            '<button class="btn btn-primary btn-block new undo" data-id="' + val + '">Undo</button>' +
                            '</div>'+
                            '<div class="col-xs-6" style="padding-bottom: 15px;">' +
                            '<button class="btn btn-primary btn-block new redo" data-id="' + val + '">Redo</button>' +
                            '</div>'
                    }
                }
            ],
            page: 1,
            pagination: true,
            pageSize: 50,
            pageNumber: 1,
            pageList: [10,25,50,100],
            search:true,
            onLoadSuccess: function(data){
                // seems a bit backward to have the table
                // control the collection.
                // might make sense for this use-case?
                var dt = {data:[]};
                for(var i = 0;i<data.data.length;i++){
                    dt.data.push({id:data.data[i].id,type:'employee_audit_log',attributes:data});
                }
                view.collection.reset(dt,{parse:true});
            }
        });
    }
});