var BulkEditView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['bulk_edit/templates/bulk_edit.html'],
    type: false,

    events: {
        "click button.next-step": "next_step",
        "change input.enable-change": "toggleField"
    },

    regions: {
        wizard_steps: 'div.steps',
        footer: 'div.modal-footer',
        body: 'div.modal-body'
    },

    initialize: function(options){

        this.existingJobsChecked = false;

        if(this.model.get('recordIds') && this.model.get('recordIds').length > 0){
            this.recordsLoaded = true;
        }else{
            this.recordsLoaded = false;
        }

        if(options && options.recordCollection){
            this.recordCollection = options.recordCollection;
        }

        this.steps = new Wizard({
            steps: {
                1: 'Changes',
                2: 'Confirm',
                3: 'Start Edit'
            },
            currentStep: 1,
            totalSteps: 3
        });

        this.listenTo(this.steps, 'change:currentStep', this.render_body);

        if(options && options.type){
            this.type = options.type;
        }

        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full', closable: false});
    },

    checkExistingJobs: function(){

        var view = this;
        if(this.existingJobsChecked){
            return true;
        }
        view.showLoading();

        App.data.bulk_edit_jobs.fetch({
            data: {
                'status': 'ready|in-progress|completed'
            },
            success: function(collection){
                if(collection.at(0)){
                    view.steps.set({'currentStep': 3}, {silent: true});
                    view.model = collection.at(0);
                    view.recordsLoaded = true;
                    view.render();
                    return true;
                }

                if(!view.loadRecords()){
                    view.hideLoading();
                }
            },
            error: function(){
                view.hideLoading();
            }
        });

        this.existingJobsChecked = true;
    },

    loadRecords: function(){

        if(!this.recordCollection){
            return false;
        }
        if(this.recordsLoaded){
            return false;
        }
        var view = this;

        var params = _.clone(this.recordCollection.getFilterParams());
        params.format = 'simple';

        $.get(this.recordCollection.url, params, function(response){
            view.recordsLoaded = true;
            view.model.set({
                'recordIds': response,
                'totalRecords': response.length
            });
            view.hideLoading();
        });

        return true;
    },

    toggleField: function(e){
        var checkbox = $(e.currentTarget);
        var row = checkbox.parents('div.row').first();

        if(checkbox.is(':checked')){
            row.find('input, select, textarea').not('input.enable-change').attr('disabled', null);
        }else{
            row.find('input, select, textarea').not('input.enable-change').attr('disabled', 'disabled');
        }
    },

    showLoading: function(){
        this.$el.loadMask();
    },

    hideLoading: function(){
        this.$el.loadMask.hide();
    },

    onRender: function(){

        this.wizard_steps.show( new WizardStepsView({model: this.steps}) );

        var footer =  new WizardFooterView({model: this.steps});
        footer.template = JST['bulk_edit/templates/bulk_edit_footer.html'];

        this.footer.show(footer);
        this.render_body();
        this.checkExistingJobs();
    },

    render_body: function(){

        var body_view = ({
            1: BulkEditCustomerChangesView,
            2: BulkEditConfirmView,
            3: BulkEditStatusView
        })[this.steps.get('currentStep')];

        this.body.show( new body_view({model: this.model}) );
    },

    getChanges: function(el){

        var changing = [];
        var actions = {};
        var values = {};
        var data = {};

        var rawData = _.getFormData(el);

        // Separate field values and meta data
        _.each(rawData, function(value, field){
            var fieldParts = field.split(':');
            if(fieldParts[1] && fieldParts[1] == 'edit' && value){
                changing.push(fieldParts[0]);
            }else if(fieldParts[1] && fieldParts[1] == 'action'){
                actions[fieldParts[0]] = value;
            }else{
                values[field] = value;
            }
        });

        // Loop through fields that we are actually changing
        _.each(changing, function(field){
            var change = {
                value: values[field],
                action: 'set'
            };

            if(actions[field]){
                change.action = actions[field];
            }
            data[field] = change;
        });

        return data;
    },

    next_step: function(){
        if(typeof(this.body.currentView.validate) == 'function' && !this.body.currentView.validate()){
            return false;
        }
        this.steps.next();
        return false;
    },

    save: function(){
        this.body.currentView.save();
        return false;
    },

    onHide: function(){
        if(this.model.get('status') == 'completed' || this.model.get('status') == 'cancelled'){
            this.model.destroy();
        }
    }
});