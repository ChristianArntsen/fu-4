var BulkEditCustomerGroupView = Backbone.Marionette.ItemView.extend({
    template: JST['bulk_edit/templates/customer_group.html'],
    tagName: 'div',
    className: 'row bulk-edit-field-record',

    events: {
        "click button.delete": "removeGroup"
    },

    removeGroup: function(){
        this.model.collection.remove(this.model);
    }
});

var BulkEditCustomerGroupsView = Backbone.Marionette.CollectionView.extend({
    childView: BulkEditCustomerGroupView,
    tagName: 'div',
    className: 'col-md-8'
});