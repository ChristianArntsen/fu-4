var BulkEditStatusView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['bulk_edit/templates/bulk_edit_status.html'],

    events: {
        'click .cancel': 'cancelJob'
    },

    initialize: function(){
        this.listenTo(this.model, 'change', this.render);
        this.status_checker = false;
    },

    stopStatusCheck: function(){
        if(this.status_checker){
            App.data.v2.customers.fetch({
                data:{
                    include:"customerGroups"
                }
            });
            clearInterval(this.status_checker);
        }
    },

    onRender: function(){

        var view = this;
        var status = this.model.get('status');

        if((status == 'ready' || status == 'in-progress') && !this.status_checker){
            this.status_checker = setInterval(function(){
                if(view.model.get('id')){
                    view.model.fetch({
                        data: {fields: 'status,totalRecords,recordsCompleted,recordsFailed,percentComplete,completedAt,totalDuration,response'}
                    });
                }
            }, 2500);
        }

        if(status == 'completed' || status == 'cancelled'){
            this.stopStatusCheck();
        }
    },

    cancelJob: function(){
        this.stopStatusCheck();
        this.model.set('status', 'cancelled');
        this.model.save();
        return false;
    },

    onBeforeDestroy: function(){
        this.stopStatusCheck();
    }
});