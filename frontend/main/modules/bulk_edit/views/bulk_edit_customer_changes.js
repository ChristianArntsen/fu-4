var BulkEditCustomerChangesView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    template: JST['bulk_edit/templates/bulk_edit_customer_changes.html'],

    regions: {
        'groups': '#customer-groups',
        'minimumCharges': '#customer-minimum-charges',
        'passes': '#customer-passes',
        'householdMembers': '#customer-household-members',
        'recurringCharges': '#customer-recurring-charges'
    },

    events: {
        'change #customer-add-group': 'addGroup',
        'change #customer-add-minimum-charge': 'addMinimumCharge',
        'change #customer-add-recurring-charge': 'addRecurringCharge',
        'change .action-menu': 'clearRecords'
    },

    initialize: function(){
        this.data = {};
        this.data.groups = new GroupCollection();
        this.data.passes = new PassCollection();
        this.data.household_members = new HouseholdMemberCollection();
        this.data.minimum_charges = new MinimumChargeCollection();
        this.data.recurring_charges = new RecurringChargeCollection();

        if(App.data.recurring_charges.length == 0){
            App.data.recurring_charges.fetch();
        }

        this.listenTo(App.data.recurring_charges, 'sync', this.renderRecurringChargeMenu);
    },

    clearRecords: function(e) {
        var menu = $(e.currentTarget);
        var field = menu.attr('name').split(':');
        field = field[0];

        if (menu.val() == 'clear' && this.data[field]) {
            this.data[field].reset();
            menu.parent().find('.add-record').hide();
        } else {
            menu.parent().find('.add-record').show();
        }
    },

    renderRecurringChargeMenu: function(){
        this.$el.find('#customer-add-recurring-charge').replaceWith(
            _.dropdown(App.data.recurring_charges.dropdownData(), {id: 'customer-add-recurring-charge', class: 'form-control form-group add-record', disabled: 'disabled'})
        )
    },

    addGroup: function(){

        var groupId = this.$el.find('#customer-add-group').val();
        if(!groupId){
            return false;
        }
        var group = App.data.course.get('groups').get(groupId);
        var groupData = _.pick(group.toJSON(), ['group_id', 'label']);

        this.data.groups.add(groupData);
        this.$el.find('#customer-add-group').val(0);
    },

    addMinimumCharge: function(){

        var chargeId = this.$el.find('#customer-add-minimum-charge').val();
        if(!chargeId){
            return false;
        }
        var charge = App.data.minimum_charges.get(parseInt(chargeId));
        var chargeData = _.pick(charge.toJSON(), ['minimum_charge_id', 'name', 'minimum_amount']);
        chargeData.date_added = '';

        this.data.minimum_charges.add(chargeData);
        this.$el.find('#customer-add-minimum-charge').val(0);
    },

    addRecurringCharge: function(){

        var chargeId = this.$el.find('#customer-add-recurring-charge').val();
        if(!chargeId){
            return false;
        }
        var charge = App.data.recurring_charges.get(parseInt(chargeId));
        var chargeData = _.pick(charge.toJSON(), ['id', 'name', 'items']);
        chargeData.date_added = '';

        this.data.recurring_charges.add(chargeData);
        this.$el.find('#customer-add-recurring-charge').val(0);
    },

    fillData: function(){

        var view = this;
        var data = this.model.get('settings');

        _.each(data, function(fieldData, field){

            var inputName = '[name=' + field + ']';
            var input = view.$el.find(inputName);
            var actionMenu = view.$el.find('[name="' + field + ':action"]');

            view.$el.find('#editing-customer-' + field).prop('checked', 'checked');

            if(actionMenu.length > 0){
                actionMenu.attr('disabled', null).val(fieldData.action);
            }
            input.val(fieldData.value);
            input.attr('disabled', null);
        });
    },

    onRender: function(){

        this.$el.find('input.calendar').datetimepicker({
            defaultDate: false,
            useCurrent: false,
            format: 'MM/DD/YYYY'
        });

        var view = this;
        var search_field = this.$el.find('#customer-pass-search');

        var passSearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote:  {
                url: API_URL + '/passes?multi_customer=1&search=%QUERY',
                wildcard: '%QUERY',
                transform: function(response){
                    return response.rows;
                }
            },
            limit: 100
        });

        passSearch.initialize();

        search_field.typeahead({
            hint: false,
            highlight: true,
            minLength: 1
        },{
            limit: 101,
            name: 'passes',
            displayKey: 'name',
            source: passSearch.ttAdapter(),
            templates: {
                suggestion: JST['pass_search_result.html']
            }

        }).on('typeahead:selected', function(e, pass, list){
            view.data.passes.add( _.pick(pass, ['pass_id', 'name', 'is_valid', 'start_date', 'end_date']) );
            search_field.typeahead('val', '');
        });

        this.groups.show( new BulkEditCustomerGroupsView({collection: this.data.groups}) );
        this.minimumCharges.show( new BulkEditCustomerMinimumChargesView({collection: this.data.minimum_charges}) );
        this.passes.show( new BulkEditCustomerPassesView({collection: this.data.passes}) );
        this.recurringCharges.show( new BulkEditCustomerRecurringChargesView({collection: this.data.recurring_charges}) );

        //this.fillData();
    },

    getData: function(){

        var data = BulkEditView.prototype.getChanges(this.$el);

        if(data.groups){
            data.groups.value = this.data.groups.toJSON();
        }
        if(data.passes){
            data.passes.value = this.data.passes.toJSON();
        }
        if(data.minimum_charges){
            data.minimum_charges.value = this.data.minimum_charges.toJSON();
        }
        if(data.recurring_charges){
            data.recurring_charges.value = this.data.recurring_charges.toJSON();
        }

        if(data.member_account_balance){
            data.member_balance_change_reason = {
                action: 'set',
                value: this.$el.find('input[name="member_balance_change_reason"]').val()
            };
        }
        if(data.account_balance){
            data.account_balance_change_reason = {
                action: 'set',
                value: this.$el.find('input[name="account_balance_change_reason"]').val()
            };
        }
        if(data.loyalty_points){
            data.loyalty_points_change_reason = {
                action: 'set',
                value: this.$el.find('input[name="loyalty_points_change_reason"]').val()
            };
        }

        // Make account limits negative
        if(data.member_account_limit){
            data.member_account_limit.value = 0 - _.getNumber(data.member_account_limit.value);
        }
        if(data.account_limit){
            data.account_limit.value = 0 - _.getNumber(data.account_limit.value);
        }

        return data;
    },

    validate: function(){
        var data = this.getData();

        if(!data || _.isEmpty(data)){
            App.vent.trigger('notification', {type: 'error', msg: 'At least one change must be made'});
            return false;
        }

        var requiredFields = App.data.course.get('customer_field_settings');

        requiredFields.member_account_balance = {
            required: true,
            name: 'Member Account Balance'
        };
        requiredFields.account_balance = {
            required: true,
            name: 'Account Balance'
        };
        requiredFields.member_account_limit = {
            required: true,
            name: 'Member Account Limit'
        };
        requiredFields.account_limit = {
            required: true,
            name: 'Account Limit'
        };
        requiredFields.loyalty_points = {
            required: true,
            name: 'Loyalty Points'
        };

        var oneRequired = {
            'groups': 'group',
            'minimum_charges': 'minimum charge',
            'recurring_charges': 'billing membership',
            'passes': 'pass'
        };

        var fieldError = _.find(data, function(fieldData, field){

            if(oneRequired[field] && fieldData.action != 'clear' && _.isEmpty(fieldData.value)){
                App.vent.trigger('notification', {type: 'error', msg: 'At least one <em>'+ oneRequired[field] + '</em> is required'});
                return true;
            }

            if(requiredFields[field] && requiredFields[field].required == 1 && fieldData.value === ''){
                App.vent.trigger('notification', {type: 'error', msg: requiredFields[field].name + ' can not be blank'});
                return true;
            }

            return false;
        });

        if(fieldError){
            return false;
        }

        this.model.set('settings', data);
        return true;
    }
});