var BulkEditConfirmView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    template: JST['bulk_edit/templates/bulk_edit_confirm.html'],

    regions: {
        'recordPreview': '#record-preview'
    },

    events: {

    },

    onRender: function(){

    },

    validate: function(){

        this.model.set('status', 'ready');
        this.model.save();

        return true;
    }
});