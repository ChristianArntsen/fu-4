
var PlayersEditItemView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['starter_teesheet/templates/starter_booking_edit_player.html'],
    events: {
        "click .player":"togglePaid",
        "click #teedoff":"teeOff",
    },
    togglePaid: function(){
        this.model.set("paid",!this.model.get("paid"));
        this.model.save();
        this.render();
    }

});



var PlayersEditCollectionView = Backbone.Marionette.CollectionView.extend({
    tagName: 'div',
    childView: PlayersEditItemView,
    addChild: function(child, ChildView, index){
        if (child.get("position") <= this.options.playerCount) {
            Backbone.Marionette.CollectionView.prototype.addChild.apply(this, arguments);
        }
    }
    // emptyView: TerminalSelectEmptyView
});
