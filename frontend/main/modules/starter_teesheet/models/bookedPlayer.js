var BookedPlayer = Backbone.JsonRestModel.extend({
    type:"bookedPlayer",
    url: function () {
        return REST_API_COURSE_URL+"/teesheets/"+this.collection.teesheetId+"/bookings/"+this.collection.bookingId+"/bookedPlayers" + (this.isNew() ? '' : '/' + this.id);
    },
    idAttribute: "id",
    defaults: {
        "name": "",
        "position": 1,
        "cartPaid": false,
        "noShow": false,
        "paid": false,
        "priceClassId": "",
        "priceClass": "",
        "personId": null,
        "relationshipType":"bookedPlayer"
    },
    relations: {
        booking: {
            type: 'bookings',
            many: false
        },
        people: {
            type: 'people',
            many: false
        }
    }
});

var BookedPlayerCollection = Backbone.JsonRestCollection.extend({
    url: function(){
        return  API_URL + '/bookedPlayers';
    },
    model: BookedPlayer,
    setCollectionUrl:function(url){
        this.collectionUrl = url;
    }
});

Backbone.modelFactory.registerModel(BookedPlayer,BookedPlayerCollection);