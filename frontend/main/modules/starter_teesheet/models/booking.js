/*!
 (c) 2012 Uzi Kilon, Splunk Inc.
 Backbone Poller 1.1.3
 https://github.com/uzikilon/backbone-poller
 Backbone Poller may be freely distributed under the MIT license.
 */
(function (root, factory) {
    'use strict';
    if (typeof define == 'function' && define.amd) {
        define(['underscore', 'backbone'], factory);
    }
    else if (typeof require === 'function' && typeof exports === 'object') {
        module.exports = factory(require('underscore'), require('backbone'));
    }
    else {
        root.Backbone.Poller = factory(root._, root.Backbone);
    }
}(this, function (_, Backbone) {
    'use strict';

    // Default settings
    var defaults = {
        delay: 1000,
        condition: function () {
            return true;
        }
    };

    // Available events
    var events = ['start', 'stop', 'fetch', 'success', 'error', 'complete'];

    var pollers = [];
    function findPoller(model) {
        return _.find(pollers, function (poller) {
            return poller.model === model;
        });
    }

    var PollingManager = {

        // **Backbone.Poller.get(model[, options])**
        // <pre>
        // Returns a singleton instance of a poller for a model
        // Stops it if running
        // If options.autostart is true, will start it
        // Returns a poller instance
        // </pre>
        get: function (model, options) {
            var poller = findPoller(model);
            if (!poller) {
                poller = new Poller(model, options);
                pollers.push(poller);
            }
            else {
                poller.set(options);
            }
            if (options && options.autostart === true) {
                poller.start({silent: true});
            }
            return poller;
        },

        // **Backbone.Poller.size()**
        // <pre>
        // Returns the number of instantiated pollers
        // </pre>
        size: function () {
            return pollers.length;
        },

        // **Backbone.Poller.reset()**
        // <pre>
        // Stops all pollers and removes from the pollers pool
        // </pre>
        reset: function () {
            while (pollers.length) {
                pollers[0].destroy();
            }
        }
    };

    function Poller(model, options) {
        this.model = model;
        this.cid = _.uniqueId('poller');
        this.set(options);

        if (this.model instanceof Backbone.Model) {
            this.listenTo(this.model, 'destroy', this.destroy);
        }
    }

    _.extend(Poller.prototype, Backbone.Events, {

        // **poller.set([options])**
        // <pre>
        // Reset poller options and stops the poller
        // </pre>
        set: function (options) {
            this.options = _.extend({}, defaults, options || {});
            if (this.options.flush) {
                this.off();
            }
            _.each(events, function (name) {
                var callback = this.options[name];
                if (_.isFunction(callback)) {
                    this.off(name, callback, this);
                    this.on(name, callback, this);
                }
            }, this);

            return this.stop({silent: true});
        },
        //
        // **poller.start([options])**
        // <pre>
        // Start the poller
        // Returns a poller instance
        // Triggers a 'start' events unless options.silent is set to true
        // </pre>
        start: function (options) {
            if (!this.active()) {
                options && options.silent || this.trigger('start', this.model);
                this.options.active = true;
                if (this.options.delayed) {
                    delayedRun(this, _.isNumber(this.options.delayed) && this.options.delayed);
                }
                else {
                    run(this);
                }
            }
            return this;
        },
        // **poller.stop([options])**
        // <pre>
        // Stops the poller
        // Aborts any running XHR call
        // Returns a poller instance
        // Triggers a 'stop' events unless options.silent is set to true
        // </pre>
        stop: function (options) {
            options && options.silent || this.trigger('stop', this.model);
            this.options.active = false;
            this.xhr && this.xhr.abort && this.xhr.abort();
            this.xhr = null;
            clearTimeout(this.timeoutId);
            this.timeoutId = null;
            return this;
        },
        // **poller.active()**
        // <pre>
        // Returns a boolean for poller status
        // </pre>
        active: function () {
            return this.options.active === true;
        },

        destroy: function () {
            var index = _.indexOf(pollers, this);
            if (index > -1) {
                this.stop().stopListening().off();
                pollers.splice(index, 1);
            }
        }
    });

    function run(poller) {
        if (validate(poller)) {
            var options = _.extend({}, poller.options, {
                success: function (model, resp) {
                    poller.trigger('success', model, resp);
                    delayedRun(poller);
                },
                error: function (model, resp) {
                    if (poller.options.continueOnError) {
                        poller.trigger('error', model, resp);
                        delayedRun(poller);
                    }
                    else {
                        poller.stop({silent: true});
                        poller.trigger('error', model, resp);
                    }
                }
            });
            poller.trigger('fetch', poller.model);
            poller.xhr = poller.model.fetch(options);
        }
    }

    var backoff = {};
    function getDelay(poller) {
        if (_.isNumber(poller.options.delay)) {
            return poller.options.delay;
        }

        var min = poller.options.delay[0],
            max = poller.options.delay[1],
            interval = poller.options.delay[2] || 2;

        if (backoff[poller.cid]) {
            if (_.isFunction(interval)) {
                backoff[poller.cid] = interval(backoff[poller.cid]);
            }
            else {
                backoff[poller.cid] *= interval;
            }
        }
        else {
            backoff[poller.cid] = 1;
        }

        var res = Math.round(min * backoff[poller.cid]);
        if (max && max > 0) {
            res = Math.min(res, max);
        }
        return res;
    }

    function delayedRun(poller, delay) {
        if (validate(poller)) {
            poller.timeoutId = _.delay(run, delay || getDelay(poller), poller);
        }
    }

    function validate(poller) {
        if (!poller.options.active) {
            return false;
        }
        if (poller.options.condition(poller.model) !== true) {
            poller.stop({silent: true});
            poller.trigger('complete', poller.model);
            return false;
        }
        return true;
    }

    /* Test hooks */
    PollingManager.getDelay = getDelay;
    PollingManager.prototype = Poller.prototype;

    return PollingManager;
}));


(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['backbone', 'underscore'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('backbone'), require('underscore'));
    } else {
        root.VirtualCollection = factory(root.Backbone, root._);
    }
}(this, function(Backbone, _) {

// Available under the MIT License (MIT);

    var VirtualCollection = Backbone.VirtualCollection = Backbone.Collection.extend({

        constructor: function (collection, options) {
            options = options || {};
            this.collection = collection;

            if (options.comparator !== undefined) this.comparator = options.comparator;
            if (options.close_with) this.bindLifecycle(options.close_with, 'close'); // Marionette 1.*
            if (options.destroy_with) this.bindLifecycle(options.destroy_with, 'destroy'); // Marionette 2.*
            if (collection.model) this.model = collection.model;
            this._clearChangesCache();

            this.accepts = VirtualCollection.buildFilter(options.filter);
            this._rebuildIndex();
            this.listenTo(this.collection, 'add', this._onAdd);
            this.listenTo(this.collection, 'remove', this._onRemove);
            this.listenTo(this.collection, 'change', this._onChange);
            this.listenTo(this.collection, 'reset',  this._onReset);
            this.listenTo(this.collection, 'filter',  this._onFilter);
            this.listenTo(this.collection, 'sort',  this._onSort);
            this.listenTo(this.collection, 'update',  this._onUpdate);
            this._proxyParentEvents(['sync', 'request', 'error']);

            this.initialize.apply(this, arguments);
        },

        bindLifecycle: function (view, method_name) {
            this.listenTo(view, method_name, this.stopListening);
        },

        updateFilter: function (filter) {
            this.accepts = VirtualCollection.buildFilter(filter);
            this._rebuildIndex();
            this.trigger('filter', this, filter);
            this.trigger('reset', this, filter);
            return this;
        },

        _rebuildIndex: function () {
            _.invoke(this.models, 'off', 'all', this._onAllEvent, this);
            this._reset();
            this.collection.each(_.bind(function (model, i) {
                if (this.accepts(model, i)) {
                    this.listenTo(model, 'all', this._onAllEvent);
                    this.models.push(model);
                    this._byId[model.cid] = model;
                    if (model.id) this._byId[model.id] = model;
                }
            }, this));
            this.length = this.models.length;

            if (this.comparator) this.sort({silent: true});
        },

        orderViaParent: function (options) {
            this.models = this.collection.filter(function (model) {
                return (this._byId[model.cid] !== undefined);
            }, this);
            if (!options.silent) this.trigger('sort', this, options);
        },

        _onSort: function (collection, options) {
            if (this.comparator !== undefined) return;
            this.orderViaParent(options);
        },

        _proxyParentEvents: function (events) {
            _.each(events, _.bind(function (eventName) {
                this.listenTo(this.collection, eventName, _.partial(this.trigger, eventName));
            }, this));
        },

        _clearChangesCache: function(){
            this._changeCache = {
                added: [],
                removed: [],
                merged: []
            };
        },

        _onUpdate: function (collection, options) {
            var newOptions = _.extend({}, options, {changes: this._changeCache});
            this.trigger('update', this, newOptions);
            this._clearChangesCache();
        },

        _onAdd: function (model, collection, options) {
            if (this.get(model) || !this.accepts(model, options.index)) return;
            this._changeCache.added.push(model);
            this._indexAdd(model);
            this.listenTo(model, 'all', this._onAllEvent);
            this.trigger('add', model, this, options);
        },

        _onRemove: function (model, collection, options) {
            if (!this.get(model)) return;
            this._changeCache.removed.push(model);
            var i = this._indexRemove(model)
                , options_clone = _.clone(options);
            options_clone.index = i;
            model.off('all', this._onAllEvent, this);
            this.trigger('remove', model, this, options_clone);
        },

        _onChange: function (model, options) {
            if (!model || !options) return; // ignore malformed arguments coming from custom events
            var already_here = this.get(model);

            if (this.accepts(model, options.index)) {
                if (already_here) {
                    if (!this._byId[model.id] && model.id) {
                        this._byId[model.id] = model;
                    }
                    this.trigger('change', model, this, options);
                } else {
                    this._onAdd(model, this.collection, options);
                }
            } else {
                if (already_here) {
                    var i = this._indexRemove(model)
                        , options_clone = _.clone(options);
                    options_clone.index = i;
                    this.trigger('remove', model, this, options_clone);
                }
            }
        },

        _onReset: function (collection, options) {
            this._rebuildIndex();
            this.trigger('reset', this, options);
        },

        _onFilter: function (collection, options) {
            this._rebuildIndex();
            this.trigger('filter', this, options);
        },

        sortedIndex: function (model, value, context) {
            var iterator = _.isFunction(value) ? value : function(target) {
                return target.get(value);
            };

            if (iterator.length == 1) {
                return _.sortedIndex(this.models, model, iterator, context);
            } else {
                return sortedIndexTwo(this.models, model, iterator, context);
            }
        },

        _indexAdd: function (model) {
            if (this.get(model)) return;
            var i;
            // uses a binsearch to find the right index
            if (this.comparator) {
                i = this.sortedIndex(model, this.comparator, this);
            } else if (this.comparator === undefined) {
                i = this.sortedIndex(model, function (target) {
                    //TODO: indexOf traverses the array every time the iterator is called
                    return this.collection.indexOf(target);
                }, this);
            } else {
                i = this.length;
            }
            this.models.splice(i, 0, model);
            this._byId[model.cid] = model;
            if (model.id) this._byId[model.id] = model;
            this.length += 1;
        },

        _indexRemove: function (model) {
            model.off('all', this._onAllEvent, this);
            var i = this.indexOf(model);
            if (i === -1) return i;
            this.models.splice(i, 1);
            delete this._byId[model.cid];
            if (model.id) delete this._byId[model.id];
            this.length -= 1;
            return i;
        },

        _onAllEvent: function (eventName) {
            var explicitlyHandledEvents = ['add', 'remove', 'change', 'reset', 'sort'];
            if (!_.contains(explicitlyHandledEvents, eventName)) {
                this.trigger.apply(this, arguments);
            }
        },

        clone: function () {
            return new this.collection.constructor(this.models);
        }

    }, { // static props

        buildFilter: function (options) {
            if (!options) {
                return function () {
                    return true;
                };
            } else if (_.isFunction(options)) {
                return options;
            } else if (options.constructor === Object) {
                return function (model) {
                    return _.every(_.keys(options), function (key) {
                        return model.get(key) === options[key];
                    });
                };
            }
        }
    });

// methods that alter data should proxy to the parent collection
    _.each(['add', 'remove', 'set', 'reset', 'push', 'pop', 'unshift', 'shift', 'slice', 'sync', 'fetch', 'url'], function (method_name) {
        VirtualCollection.prototype[method_name] = function () {
            if (_.isFunction(this.collection[method_name])){
                return this.collection[method_name].apply(this.collection, arguments);
            } else {
                return this.collection[method_name];
            }
        };
    });

    /**

     Equivalent to _.sortedIndex, but for comparators with two arguments

     **/
    function sortedIndexTwo (array, obj, iterator, context) {
        var low = 0, high = array.length;
        while (low < high) {
            var mid = (low + high) >>> 1;
            iterator.call(context, obj, array[mid]) > 0 ? low = mid + 1 : high = mid;
        }
        return low;
    }

    _.extend(VirtualCollection.prototype, Backbone.Events);

    return VirtualCollection;
}));


/*jshint indent:2 */
(function (global) {

    var Backbone = global.Backbone,
        Lib = {},
        _ = global._;

    if ((!_  || !Backbone) && (typeof require !== 'undefined')) {
        _ = require('underscore');
        Backbone = require('backbone');
    }

    /**
     * Checks a parameter from the obj
     *
     * @param {Object} obj         parameters
     * @param {String} name        of the parameter
     * @param {String} explanation used when throwing an error
     */
    function needs(obj, name, explanation) {
        if (!obj[name]) {
            throw new Error('Missing parameter ' + name + '. ' + explanation);
        }
    }

    Lib.GroupModel = Backbone.Model;
    Lib.GroupCollection = Backbone.Collection.extend({
        closeWith: function (event_emitter) {
            event_emitter.on('close', this.stopListening);
        }
    });

    /**
     * Function that returns a collection of sorting groups
     *
     * @param {Object} options
     *  - {Collection} collection (base collection)
     *  - {Function} groupby (function that returns a model's group id)
     *
     *  - {[Function]} comparator
     *  - {[Function]} GroupModel the group model
     *  - {[Function]} GroupCollection the groups collection
     *
     * @return {Collection}
     */
    Lib.buildGroupedCollection = function (options) {
        var Constructor = options.GroupCollection || Lib.GroupCollection;

        needs(options, 'collection', 'The base collection to group');
        needs(options, 'groupBy', 'The function that returns a model\'s group id');

        options.group_collection = new Constructor(null, {
            comparator: options.comparator
        });

        Lib._onReset(options);
        options.group_collection.listenTo(options.collection, 'add', _.partial(Lib._onAdd, options));
        options.group_collection.listenTo(options.collection, 'change', _.partial(Lib._onAdd, options));
        options.group_collection.listenTo(options.collection, 'remove', _.partial(Lib._onRemove, options));
        options.group_collection.listenTo(options.collection, 'reset', _.partial(Lib._onReset, options));


        if (!options.close_with) {
            console.warn("You should provide an event emitter via `close_with`," +
                " or else the listeners will never be unbound!");
        } else {
            options.group_collection.listenToOnce(options.close_with,
                'close', options.group_collection.stopListening);
            options.group_collection.listenToOnce(options.close_with,
                'destroy', options.group_collection.stopListening);
        }

        return options.group_collection;
    };

    /**
     * Creates a Group model for a given id.
     *
     * @param {Object} options
     * @param {String} group_id
     * @return {Group}
     */
    Lib._createGroup = function (options, group_id) {
        var Constructor = options.GroupModel || Lib.GroupModel,
            vc, group, vc_options;

        vc_options = _.extend(options.vc_options || {}, {
            filter: function (model) {
                return options.groupBy(model) === group_id;
            },
            close_with: options.close_with
        });

        vc = new Backbone.VirtualCollection(options.collection, vc_options);
        group = new Constructor({id: group_id, vc: vc});
        group.vc = vc;
        vc.listenTo(vc, 'remove', _.partial(Lib._onVcRemove, options.group_collection, group));

        return group;
    };

    /**
     * Handles the add event on the base collection
     *
     * @param {Object} options
     * @param {Model} model
     */
    Lib._onAdd = function (options, model) {
        var id = options.groupBy(model);

        if (!options.group_collection.get(id)) {
            options.group_collection.add(Lib._createGroup(options, id));
        }
    };

    /**
     * Handles the remove event on the base collection
     *
     * @param {Object} options
     * @param  {Model} model
     */
    Lib._onRemove = function (options, model) {
        var id = options.groupBy(model),
            group = options.group_collection.get(id);

        if (group && !group.vc.length) {
            options.group_collection.remove(group);
        }
    };

    /**
     * Handles the reset event on the base collection
     *
     * @param {Object} options
     */
    Lib._onReset = function (options) {
        var group_ids = _.uniq(options.collection.map(options.groupBy));
        options.group_collection.reset(_.map(group_ids, _.partial(Lib._createGroup, options)));
    };

    /**
     * Handles vc removal
     *
     * @param {VirtualCollection} group_collection
     * @param {?} group
     */
    Lib._onVcRemove = function (group_collection, group) {
        if (!group.vc.length) {
            group_collection.remove(group);
        }
    };

    Backbone.buildGroupedCollection = Lib.buildGroupedCollection;

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = Lib;
    }

}(this));

var BookingModel = Backbone.JsonRestModel.extend({


    type: 'bookings',
    relations: {
        bookedPlayers: {
            type: 'bookedPlayer',
            many: true
        }
    },
    idAttribute: "id",
    defaults: {
        "isReround": "",
        "type": "",
        "status": "",
        "duration": "",
        "playerCount": "",
        "holes": "",
        "carts": "",
        "paidPlayerCount": "",
        "noShowCount": "",
        "title": "",
        "details": "",
        "side": "",
        "bookingSource": "",
        "start": "",
        "end": "",
        "lastUpdated": "",
        "dateBooked": "",
        "cart1": "",
        "cart2": "",
        "teedOffTime": null,
        "bookedPlayers": [],
        "relationshipType":"booking"
    },
    initialize: function(attributes,options){
        var self = this;
        this.get("bookedPlayers").teesheetId = this.collection.teesheetId;
        this.get("bookedPlayers").bookingId = this.get("id");
        this.on('change:bookedPlayers', this.update);
        this.get("bookedPlayers").on("change:paid",function(model,newValue){
            if(newValue)
                self.set("paidPlayerCount",self.get("paidPlayerCount") + 1)
            else
                self.set("paidPlayerCount",self.get("paidPlayerCount") - 1)
        });
    },
    update:function(){
        this.get("bookedPlayers").teesheetId = this.collection.teesheetId;
        this.get("bookedPlayers").bookingId = this.get("id");
    }
});

var BookingCollection = Backbone.JsonRestCollection.extend({
    model: BookingModel,
    url: function() {
        return REST_API_COURSE_URL + this.collectionUrl;
    },
    initialize: function(attributes,options){
        if(options && options.url){
            this.collectionUrl = options.url;
            this.teesheetId = options.teesheetId;
        }
    }
});

Backbone.modelFactory.registerModel(BookingModel,BookingCollection);