var Wizard = Backbone.Model.extend({

    defaults: {
        steps: {},
        currentStep: 0,
        totalSteps: 0,
        maxStep: 0
    },

    initialize: function(){
        this.on('change:currentStep', this.setMax);
        this.setMax();
    },

    setMax: function(){
        if(this.get('currentStep') > this.get('maxStep')){
            this.set('maxStep', this.get('currentStep'));
        }
    },

    next: function(){
        this.set('currentStep', this.get('currentStep') + 1);
    },

    previous: function(){
        this.set('currentStep', this.get('currentStep') - 1);
    }
});