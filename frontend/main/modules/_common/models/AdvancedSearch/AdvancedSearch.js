var AdvancedSearchModel = Backbone.Model.extend({
    defaults: {
        module: '',
        q: '',
        applied_filters:[],
        allowed_fields:[]
    },
    initialize:function(){

        var allowedFields = new AllowedFieldsCollection();
        allowedFields.module = this.get('module');
        this.set("allowed_fields", allowedFields);
        this.get("allowed_fields").fetch();

        var appliedFilters = new AppliedFilterCollection();
        appliedFilters.setAllowedFields(this.get("allowed_fields"));

        this.set("applied_filters",appliedFilters);
    },

    getFilterParams: function(){

        var searchParams = {
            "q": this.get("q")
        };
        this.get("applied_filters").forEach(function(field){
            if(field.get("field") && field.get("value")){
                if(searchParams[field.get("field")]){
                    searchParams[field.get("field")] += ","+field.get("comparison")+":"+field.get("value");
                } else {
                    searchParams[field.get("field")] = field.get("comparison")+":"+field.get("value");
                }
            }
        });

        return searchParams;
    }
});

var AppliedFilterModel = Backbone.Model.extend({
    defaults: {
        allowed_fields:[],
        field:null,
        comparison:null,
        value: null,
        type:"text",
        possibleValues:[]
    }
});

var AppliedFilterCollection = Backbone.Collection.extend({
    setAllowedFields:function(fields){
        this.allowedFields = fields;
        this.models.forEach(function(value,key){
            value.set("allowed_fields",fields);
        })
    },
    addNewFilter:function(){
        var newModel = new AppliedFilterModel();
        newModel.set("allowed_fields",this.allowedFields);
        this.add(newModel);
    }
});

var AllowedFieldModel = Backbone.JsonRestModel.extend({
    defaults:{
        id:"",
        name:"",
        label:"",
        type:"",
        possibleValues:[]
    }
});

var AllowedFieldsCollection = Backbone.JsonRestCollection.extend({
    module: '',
    url: function(){
        return REST_API_COURSE_URL + '/' + this.module + '/fields';
    },
    model: AllowedFieldModel
});

Backbone.modelFactory.registerModel(AllowedFieldModel, AllowedFieldsCollection);