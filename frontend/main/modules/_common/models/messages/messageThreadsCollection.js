var MessageThread = Backbone.Model.extend({
	idAttribute: "person_id",
	defaults: {
		"first_name": "",
		"last_name": "",
		"cell_phone_number": "",
		"phone_number": "",
		"id": "",
		"course_id": "",
		"message": "",
		"incoming": "",
		"timestamp": "",
        "last_updated":"",
		"read": 0,
        "ignore":0
	}
});

var MessageThreadCollection = Backbone.Collection.extend({
    initialize:function(){
        this.on('change:timestamp',this.sort,this);
    },
	model: MessageThread,
    comparator : function(message1,message2){
        if(moment(message1.get("last_updated")).isAfter(moment(message2.get("last_updated")))){
            return -1;
        } else {
            return 1;
        }

    }
    ,
    url: API_URL + '/personal_message/threads',
    getLastDate: function(){
        var latest = ""
        this.each(function(thread){
            if(latest == ""){
                latest = moment(thread.get("last_updated"));
            } else if(moment(thread.get("last_updated")).isAfter(latest)){
                latest = moment(thread.get("last_updated"));
            }
        });
        if(latest == ""){
            return moment.utc().format("YYYY-MM-DD HH:mm:ss");
        }
        return latest.format();
	},
    getUnreadCount:function(){
        var unreadCount = 0;
        this.each(function(message){
            if(message.get("read") == 0 && message.get("incoming") == 1){
                unreadCount++;
            }
        })
        return unreadCount;
    },
});