// validation.js
// tool for validating data from JSON API endpoint

JsonRestValidator = function(){
    this.reset();
};

JsonRestValidator.prototype = {
    // validation is broken up into discrete sections of the response
    // the deep argument is intended for simple validation of the entire message,
    // defaults to false

    //testing: stub, accept valid, reject invalid
    reset:function() {
        this._last_error = '';
        this._warnings = [];
        /*
        Node format:
        {
          type,
          pointer,
          value,
          error,
          warnings
        }
         */
        this._node_hash = {}; // keys are JSON pointer strings
        this._node_list = [];
    },

    //testing: stub
    validate: function(res,deep){
        if(!this.validate_result(res,deep)){

            //console.log('Invalid response from JSON API endpoint: ' + this.url,res);
            //console.log(this._last_error);
            return false;
        }
        return true;
    },

    //testing: stub
    validate_result:function(res,deep){
        // one of the following:
        // (data | errors) meta
        if(!res || !(typeof res.data !== 'undefined' ||
                     typeof res.errors !== 'undefined' ||
                     typeof res.meta !== 'undefined')){
            this._last_error = 'Server response must contain one or both of the following: (data | errors) meta: ' +
            JSON.stringify(res);
            return false;
        }

        if(typeof res.data !== 'undefined' && typeof res.error !== 'undefined'){
            this._last_error = 'Server response cannot have both data and errors set';
            return false;
        }

        if(typeof res.data !== 'undefined' && !this.validate_primary_data(res.data,deep))return false;

        if(typeof res.errors !== 'undefined' && !this.validate_primary_errors(res.errors,deep))return false;

        if(typeof res.meta !== 'undefined' && !this.validate_primary_meta(res.meta,deep))return false;

        if(deep){
            if(!this.validate_primary_lazy(res,deep))return false;
        }

        // made it through the gamut.
        return true;
    },

    //testing: stub
    validate_primary_lazy: function(res,deep){
        // jsonapi links included
        // if no data, then no included!
        if(typeof res.data === 'undefined' && typeof res.included !== 'undefined'){
            this._last_error = 'included parameter cannot be included if no data parameter is present';
            return false;
        }

        if(typeof res.links !== 'undefined'){
            if(!this.validate_links(res.links,deep))return false;
        }

        if(typeof res.jsonapi !== 'undefined'){
            if(!this.validate_json_api(res.jsonapi,deep))return false;
        }

        if(typeof res.included !== 'undefined'){
            if(!this.validate_primary_included(res.included,deep))return false;
        }

        return true;
    },

    //testing: stub, accept valid, reject invalid
    validate_json_api: function(api,deep){
        //
        if(typeof api !== 'object' || api === null || api.push){
            this._last_error = '"jsonapi" must be an object';
            return false;
        }
        if(typeof api.version !== 'undefined' && typeof api.version !== 'string'){
            this._last_error = '"jsonapi" version must be a string: ' + JSON.stringify(api.version);
            return false;
        }
        if(typeof api.meta !== 'undefined'){
            if(!this.validate_primary_meta(api.meta, deep))return false;
        }
        return true;
    },

    //testing: stub, accept valid
    validate_links:function(links,deep){
        //

        if(typeof links !== 'object' || links === null || links.push){
            this._last_error = '"links" must be an object';
            return false;
        }
        for(var p in links){
            if(links.hasOwnProperty(p)){
                // self data meta related pagination about
                if(!this.validate_link_member(links[p])){
                    this._last_error += ' at link index: ' + p;
                    return false;
                }
            }
        }
        return true;
    },

    //testing: stub, accept valid, reject invalid
    validate_link_member: function(link,deep){
        //
        if(typeof link === 'string')return true; // this is good

        if(typeof link.href !== 'undefined' && typeof link.href !== 'string'){
                this._last_error = 'Invalid link href found: ' + JSON.stringify(link.href);
                return false;
        }

        if(typeof link.meta !== 'undefined' && !this.validate_primary_meta(link.meta))return false;

        return true;
    },

    //testing: stub
    validate_primary_errors:function(errors,deep){
        //

        if(!errors.push){
           this._last_error = 'errors must be presented as an array of error objects';
        }

        if(deep){
            this.validate_errors_lazy(errors,deep);
        }

        return true;
    },

    //testing: stub
    validate_errors_lazy: function(errors,deep){
        //
        if(!errors.length){
            this.add_warning('Warning: errors array present, but empty');
        }
        var err;
        for(var i=0;i<errors.length;i++){
            err = errors[i];
            if(typeof err.id !== 'undefined'){
                // no guidance in spec other than unique identifier of occurance of problem. Probably a string...
            }
            if(typeof err.links !== 'undefined'){
                if(typeof err.links.about === 'undefined'){
                    this._last_error = 'links on errror objects must have an "about" member';
                    return false;
                }
                if(!this.validate_link_member(err.links.about))return false;
            }
            if(typeof err.status !== 'undefined'){
                if(typeof err.status !== 'string'){
                    this._last_error = '"status" on error objects must be expressed as a string';
                    return false;
                }
                if(isNaN(err.status*1)){
                    this._last_error = '"status" on error objects must be the HTTP status code pertinent to this problem';
                    return false;
                }
            }
            if(typeof err.code !== 'undefined'){
                if(typeof err.code !== 'string'){
                    this._last_error = '"code" on error objects must be expressed as a string';
                    return false;
                }
            }
            if(typeof err.title !== 'undefined'){
                if(typeof err.title !== 'string'){
                    this._last_error = '"title" on error objects must be expressed as a string';
                    return false;
                }
            }
            if(typeof err.detail !== 'undefined'){
                if(typeof err.detail !== 'string'){
                    this._last_error = '"detail" on error objects must be expressed as a string';
                    return false;
                }
            }
            if(typeof err.source !== 'undefined'){
                // should be an object referencing the source of the error.
                // may have a pointer: (JSON pointer) and a parameter: (URI query parameter that caused the problem)
                if(!this.validate_error_source(err.source))return false;
            }
            if(typeof err.meta !== 'undefined'){
                if(!this.validate_primary_meta(err.meta,deep))return false;
            }
        }

        return true;
    },

    //testing: stub
    validate_error_source:function(source,deep){
        //
        if(typeof source !== 'object' || source === null || source.push){
            this._last_error = '"source" must be an object';
            return false;
        }
        
        if(typeof source.pointer !== 'undefined' && typeof source.pointer !== 'string'){
            this._last_error = 'Error pointers should be strings conforming to RFC6901';
            return false;
        }

        if(typeof source.parameter !== 'undefined' && typeof source.parameter !== 'string'){
            this._last_error = 'Error parameter should be a string indicating which URI query parameter caused the error';
            return false;
        }
        return true;
    },

    //testing: stub, accept valid, reject invalid
    validate_primary_meta:function(meta,deep){
        //
        if(typeof meta === 'undefined'){
            this._last_error = 'Empty "meta" was passed';
            return false;
        }

        if(typeof meta !== 'object' || meta === null || meta.push){
            this._last_error = '"meta" must be an object';
            return false;
        }
        return true;
    },

    //testing: stub
    validate_primary_included: function(inc,deep){

        if(!inc.push){

            this._last_error = 'Primary included must be an array';
            return false;
        }
        if(deep) {
            for (var i = 0; i < inc.length; i++) {
                if (inc.type && !this.validate_resource(inc,deep)) {
                    this._last_error += ' at primary included index ' + i;
                    return false;
                }
            }
        }

        // TODO: also need to ensure that it is referenced from primary data or links within primary or included resources
        this.add_warning('Warning: validate_primary included is not yet fully implemented');
        return true;
    },

    //testing: stub, accept valid, reject invalid
    validate_resource_linkage:function(inc,deep){
        //
        if(typeof inc === 'undefined' || (inc !== null && !inc.push && !this.validate_resource(inc))){

            this._last_error = 'resource linkage must be null, an empty array, ' +
                'a single resource identifier object, ' +
                'or an array of resource identifier objects';
            return false;
        }

        // null and singleton
        if(!inc)return true;
        if(!inc.push)return true;

        // arrays
        if(inc.push && deep) {
            for (var i = 0; i < inc.length; i++) {
                if (!this.validate_resource(inc[i],false)) {
                    this._last_error += ' at resource linkage index ' + i;
                    return false;
                }
                if(typeof inc[i].meta !== 'undefined' ){
                    this.validate_primary_meta(inc[i].meta,deep);
                }
            }
        }
        return true;
    },

    //testing: stub
    validate_primary_data:function(data,deep){
        //
        if(typeof data === 'undefined' ||
           !(data === null ||
             data.push ||
               this.validate_resource(data,deep)
           )){

            this._last_error = 'Primary data must be a null, array, or resource object';
            return false;
        }

        // null and singleton
        if(!data)return true;
        if(!data.push)return true;

        if(deep) {
            if (data.push) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].type && !this.validate_resource(data[i],deep)) {
                        this._last_error += ' at primary data index ' + i;
                        return false;
                    }
                }
            }
        }
        return true;
    },

    //testing: stub
    validate_resource: function(resource,deep){
        //
        if(typeof resource !== 'object' || resource === null || resource.push){
            this._last_error = '"resource" must be an object: ' + JSON.stringify(resource);
            return false;
        }

        if(typeof resource.id === 'undefined' || typeof resource.type === 'undefined'){
            this._last_error = 'Resource must contain both "id" and "type" parameters';
            return false;
        }

        if(typeof resource.id !== 'string'){
            this._last_error = 'Resource "id" must be a string';
            return false;
        }

        if(typeof resource.type !== 'string'){
            this._last_error = 'Resource "type" must be a string';
            return false;
        }

        if(!this.validate_member_name(resource.type)){
            this._last_error += ' on resource with id: '+resource.id;
            return false;
        }

        if(deep){
            if(!this.validate_resource_lazy(resource,deep))return false;
        }
        return true;
    },

    //testing: stub
    validate_resource_lazy: function(resource,deep) {
        // attributes relationships links meta
        if(typeof resource.links !== 'undefined') {
            if (!this.validate_links(resource.links))return false;
        }

        if(typeof resource.meta !== 'undefined') {
            if (!this.validate_primary_meta(resource.meta))return false;
        }

        if(typeof resource.attributes !== 'undefined'){
            if(!this.validate_attributes(resource.attributes,deep))return false;
        }

        if(typeof resource.relationships !== 'undefined'){
            if(!this.validate_relationships(resource.relationships,deep))return false;
        }

        return true;
    },

    //testing: stub
    validate_relationships: function(relationships,deep){

        if(typeof relationships !== 'object' || relationships === null || relationships.push){
            this._last_error = '"relationships" must be an object';
            return false;
        }

        for(var p in relationships){
            if(relationships.hasOwnProperty(p)){
                // self data meta related pagination about
                if(!this.validate_relationship(relationships[p])){
                    this._last_error += ' at relationships index: ' + p;
                    return false;
                }
            }
        }
        return true;
    },


    //testing: stub, acceptance
    validate_relationship: function(relation,deep){
        //
        if(typeof relation !== 'object' || relation === null || relation.push){
            this._last_error = '"relationships" member must be an object';
            return false;
        }

        if(typeof relation.links === 'undefined' &&
           typeof relation.data === 'undefined' &&
           typeof relation.meta === 'undefined'){
            this._last_error = 'One of the following members required on relationships: links | data | meta';
            return false;
        }

        if(typeof relation.links !== 'undefined'){
            if(!this.validate_links(relation.links,deep))return false;
            if(typeof relation.links.self === 'undefined' &&
               typeof relation.links.data === 'undefined' &&
               typeof relation.links.meta === 'undefined'){
                this._last_error = 'one of the following members required on relationships.links: ';
                this._last_error += 'self | data | meta';
            }
        }

        if(typeof relation.data !== 'undefined'){
            if(!this.validate_resource_linkage(relation.data,deep))return false;
        }

        if(typeof relation.meta !== 'undefined'){
            if(!this.validate_primary_meta(relation.meta,deep))return false;
        }
        return true;
    },

    //testing: stub, acceptance, reject bad
    validate_attributes: function(attributes,deep){
        //
        if(typeof attributes !== 'object' || attributes === null || attributes.push){
            this._last_error = '"attributes" must be an object';
            return false;
        }
        if(typeof attributes.links !== 'undefined') {
            this._last_error = 'Cannot use reserved "links" member in attributes';
            return false;
        }

        if(typeof attributes.relationships !== 'undefined'){
            this._last_error = 'Cannot use reserved "relationships" member in attributes';
            return false
        }
        return true;
    },

    //testing: stub, acceptance, reject bad
    validate_member_name: function(mn){
        var res = mn.match(this.member_name_reserved);
        if(res){
            var idx = res[0].search(res[1])
            this._last_error = 'illegal character "' + res[1] + '" at position ' + idx + ' of string: ' + mn;
            return false;
        }
        if(this.member_name_not_first.test(mn)){
            this._last_error = 'member names must not begin with " ", "-", or "_"';
            return false;
        }

        if(this.member_name_not_last.test(mn)){
            this._last_error = 'member names must not end with " ", "-", or "_"';
            return false;
        }

        if(this.member_name_space.test(mn)){
            this.add_warning('Use of " " in member names allowed but not recommended: ' + mn);
        }

        if(!this.member_name_recommended.test(mn)){
            this.add_warning('Use of Unicode characters U+0080 and above in member names allowed but not recommended: ' + mn);
        }

        return true;
    },
    member_name_space:/(?:^.*)\u0020(?:.*$)/, // not recommended
    member_name_reserved:/(?:^.*?)([\u002B\u002c\u002e\u005b\u005d\u0021\u0022\u0023\u0024\u0025\u0026\u0027\u0028\u0029\u002a\u002f\u003a\u003b\u003c\u003d\u003e\u003f\u0040\u005c\u005e\u0060\u007b\u007c\u007d\u007e\u007f\u0000\u0001\u0002\u0003\u0004\u0005\u0006\u0007\u0008\u0009\u000a\u000b\u000c\u000d\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f])(?:.*$)/,
    member_name_not_first:/^[\u002d\u005f\u0020](?:.*$)/,
    member_name_not_last:/(?:^.*)[\u002d\u005f\u0020]$/,
    member_name_recommended:/^[0-9a-zA-Z\u002d\u005f\u0020]+$/,

    get_last_error: function() {
        if(!this._last_error)return false;
        return this._last_error;
    },
    get_last_warning: function() {
        if(!this._warnings.length)return false;
        return this._warnings[this._warnings.length-1];
    },
    get_warnings: function() {
        return JSON.parse(JSON.stringify(this._warnings));
    },
    set_last_error: function(err) {
        this._last_error = err;
    },
    add_warning: function(warn) {
        this._warnings.push(warn);
    }
};

// So it works as a Node.js module too.
if(typeof exports !== 'undefined')exports.validator = new JsonRestValidator();