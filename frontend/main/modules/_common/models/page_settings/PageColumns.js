var PageSettingsPageColumn = Backgrid.Column.extend({

    defaults: {
        name: "default",
        label: "Default",
        editable: false,
        cell:"string",
        displayed: true
    }
});

var PageSettingsPageColumnCollection = Backgrid.Columns.extend({
    model: PageSettingsPageColumn
});