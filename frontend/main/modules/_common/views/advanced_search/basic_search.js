var BasicSearchView = Backbone.Marionette.ItemView.extend({

	template: JST['_common/templates/advanced_search/basic_search.html'],
    placeholder: 'Search...',

	initialize: function(options){
        if(options && options.placeholder){
            this.placeholder = options.placeholder;
        }
	},

    serializeData: function(){
	    return {placeholder: this.placeholder};
    },

    events: {
        'blur @ui.searchBox': 'setQ',
        'keydown @ui.searchBox': 'handleEnterPress'
    },

    ui: {
        searchBox: 'input.search'
    },

    setQ:function(){
	    this.model.set("q", this.ui.searchBox.val());
    },

    handleEnterPress:function(event){
        if (event.which == 13) {
            this.setQ();
        }
    },

	onRender: function(){
        var self = this;
        this.$el.find('input.search').cardSwipe({
            setInput: false,
            lengthThreshold:5
        });

        init_customer_search(this.$el.find('input.search'), function(e, customer, list){
            self.setQ();
        });
	}
});