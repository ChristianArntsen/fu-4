var ConfirmView = ModalLayoutView.extend({
    template: JST['_common/templates/confirm/confirm.html'],

    events:{
        'click .yes':'yes',
        'click .no':'no'
    },


    initialize: function(options){
    },

    yes:function(){
        this.close();
    },


    no:function(e){
        this.close();
    }


});