Backgrid.BooleanCell = Backgrid.Cell.extend({
    render: function () {
        var booleanValue = this.model.get(this.column.get("name"));
        if(booleanValue) {
            this.$el.append($("<i class='fa fa-check green-check-mark' aria-hidden='true' color='green'>"));
        }

        return this;
    }
});