//Source: https://github.com/orangain/marionette-sortable
Marionette.TableBehavior = Marionette.Behavior.extend({
    controlsEnabled:false,
    ui:{
        "gridControls":".controls",
        "grid":".grid",
        "stateDisplay":"#state-display"
    },
    events: {
        "click @ui.gridControls": "processControlAction"
    },
    initialize:function(){
        if(!this.options.pageSettings){
            console.error("The table requires a pageSettings opject, pass one in through the behavior options.")
            return false;
        }
        if(!this.options.pageSettings.possibleColumns){
            console.error("The pageSettings object must contain a list of possible columns.");
            return false;
        }
        var collection = this.view.options.collection;
        collection.on('backgrid:selected', function(model, selected) {
            //do what i need here
            var selectedModels = this.grid.getSelectedModels();
            if(selectedModels.length == 1 && model == selectedModels[0]){
                this.enableControls(selected);
            } else if (selectedModels.length == 0){
                this.enableControls(selected);
            } else {
                this.enableControls(true);
            }
        },this);

        this.listenTo(this.options.pageSettings,"sync",function(){
            this.updateGridColumns();
        },this);
        this.listenTo(this.options.pageSettings,"change:page_size",function(){
            this.view.collection.state.pageSize = this.view.options.settings.get("page_size");
            this.view.collection.fetch({include:'customerGroups'});
        });


        this.listenTo(collection,"sync",function(){

            var start = collection.state.currentPage*collection.state.pageSize + 1;
            var end = start + collection.state.pageSize -1;
            if(end > collection.state.totalRecords)
                end = collection.state.totalRecords;
            this.ui.stateDisplay.html("Displaying <span class='start'>" + start + "</span> to <span class='end'>"+ end + "</span> of <span class='total'>"+collection.state.totalRecords+"</span>");
        });
        this.listenTo(collection, 'backgrid.selectnone', function(){
            this.grid.clearSelectedModels();
        });
    },
    updateGridColumns:function(){
        this.grid.columns.reset(this.options.pageSettings.getColumnsForGrid());
    },
    renderGrid:function(){

        var row = Backgrid.Row;
        if(this.options && this.options.row){
            row = this.options.row;
        }

        this.grid = new Backgrid.Grid({
            row: row,
            columns: this.options.pageSettings.getColumnsForGrid(),
            collection: this.view.options.collection,
            emptyText: "No Data"
        });

        this.$el.find('table.items').append(this.grid.render().el);
    },
    onRender: function(){
        this.enableControls(this.controlsEnabled);

        this.renderGrid();
        var collection = this.view.options.collection;
        var paginator = new Backgrid.Extension.Paginator({
            windowSize: 5,
            collection:  this.view.options.collection
        });
        this.$el.find('#paginator').append(paginator.render().el);
    },


    processControlAction:function(e){
        var action = $(e.currentTarget).attr("action");
        if(!this.controlsEnabled)
            return false;
        if(!this.options.controls){
            console.error("There are no controls setup for this view.  Add a 'controls' array to this view.")
            return false;
        }
        if(!this.options.controls[action]){
            console.error("There doesn't appear to be a handler for the action: "+action)
            return false;
        }

        this.options.controls[action].apply(this);
    },
    enableControls:function(enable){
        this.controlsEnabled = enable;
        if(enable){
            this.ui.gridControls.prop("disabled",false);
        } else {
            this.ui.gridControls.prop("disabled",true);
        }
    }

});