// Receipt printer groups
var PriceClassModel = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {
        "name": '',
        'color':'',
        'dateCreated':'',
        'default':0,
        'cart':1,
        'cartGlCode':null,
        'greenGlCode':null,
        'isShared':0
    }
});


var PriceClassCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/priceClasses',
    model: PriceClassModel,

    initialize: function () {
        this.fetch()
    }
});