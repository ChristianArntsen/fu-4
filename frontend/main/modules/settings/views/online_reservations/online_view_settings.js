var OnlineViewSettingsView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/online_reservations/online_view_settings.html'],
    events: {
        'change #tee_sheet_id':'loadTeeSheet',
        'click .add_online_reservation_group':'viewOnlineReservationGroup',
        'click .add_online_special':'viewOnlineSpecial'
    },
    initialize:function(){
        this.listenTo(App.vent, 'save-settings', this.save);
        this.tee_sheets = App.data.v2.teesheets;
    },
    onRender:function(){
        this.$el.find('#tee_sheet_id').replaceWith(_.dropdown(this.tee_sheets.getMenuData(), {"name":"tee_sheet_id", "id":"tee_sheet_id", "class":"form-control"}, 0));
    },
    loadTeeSheet:function(e){
        var tee_sheet_id = this.$el.find('#tee_sheet_id').val();
        this.model = this.tee_sheets.findWhere({'id':String(tee_sheet_id)});
        this.renderTeeSheetSettings();
    },
    renderTeeSheetSettings:function(){
        this.$el.find('#online-view-settings-holder').html(new OnlineViewTeeSheetSettingsView({model:this.model}).render().el);
    }
});

var OnlineViewTeeSheetSettingsView = Backbone.Marionette.LayoutView.extend({
    tagName:'div',
    template: JST['settings/templates/online_reservations/online_view_tee_sheet_settings.html']
});
