var GeneralView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/online_reservations/general.html'],
    events: {
        'change #tee_sheet_id':'loadTeeSheet',
        'click .add_online_reservation_group':'viewOnlineReservationGroup',
        'click .add_online_special':'viewOnlineSpecial'
    },
    initialize:function(){
        this.listenTo(App.vent, 'save-settings', this.save);
        this.tee_sheets = App.data.v2.teesheets;
    },
    onRender:function(){
        this.$el.find('#tee_sheet_id').replaceWith(_.dropdown(this.tee_sheets.getMenuData(), {"name":"tee_sheet_id", "id":"tee_sheet_id", "class":"form-control"}, 0));
    },
    loadTeeSheet:function(e){
        debugger;
        var tee_sheet_id = this.$el.find('#tee_sheet_id').val();
        this.model = this.tee_sheets.findWhere({'id':String(tee_sheet_id)});
        this.renderTeeSheetGeneralSettings(tee_sheet_id);
        this.loadOnlineReservationGroups(tee_sheet_id);
        this.renderOnlineReservationGroups();
        this.loadOnlineSpecials(tee_sheet_id);
        this.renderOnlineSpecials();
    },
    renderTeeSheetGeneralSettings:function(tee_sheet_id){
        this.$el.find('.tee-sheet-online-general-settings').html(new OnlineGeneralSettingsView({model:this.tee_sheets.findWhere({'id':String(tee_sheet_id)})}).render().el);
    },
    loadOnlineReservationGroups:function(tee_sheet_id){
        App.data.course.get('booking_classes').tee_sheet_id = tee_sheet_id;
        App.data.course.get('booking_classes').fetch({async:false});
        this.reservationGroups = App.data.course.get('booking_classes');
    },
    renderOnlineReservationGroups:function(){
        var view = this;
        this.$el.find('.online-reservation-group-list').html('');
        if (this.reservationGroups.length > 0) {
            _.each(this.reservationGroups.models, function(reservationGroup){
                view.addOnlineReservationGroup(reservationGroup);
            });
        } else {
            var html = '<tr id="no-payment-types"><td colspan="2">No Online Reservation Groups</td></tr>';
            this.$el.find('.online-reservation-group-list').append(html);
        }
    },
    addOnlineReservationGroup:function(reservationGroup){
        this.$el.find('.online-reservation-group-list').append(new OnlineReservationGroupListItemView({model: reservationGroup}).render().el );
    },
    loadOnlineSpecials:function(tee_sheet_id){
        App.data.course.get('specials').tee_sheet_id = tee_sheet_id;
        App.data.course.get('specials').fetch({async:false});
        this.specials = App.data.course.get('specials');
    },
    renderOnlineSpecials:function(){
        var view = this;
        this.$el.find('.online-specials-list').html('');
        if (this.specials.length > 0) {
            _.each(this.specials.models, function(special){
                view.addOnlineSpecial(special);
            });
        } else {
            var html = '<tr id="no-payment-types"><td colspan="2">No Online Specials</td></tr>';
            this.$el.find('.online-specials-list').append(html);
        }
    },
    addOnlineSpecial:function(special){
        this.$el.find('.online-specials-list').append(new OnlineSpecialListItemView({model: special}).render().el );
    },
    save:function(){
        var data = _.getFormData(this.$el);
        // Filter down to the only options that are savable
        var tee_sheet_fields = [
            'online_close_time',
            'online_open_time',
            'pay_online',
            'online_booking',
            'days_in_booing_window',
            'limit_holes',
            'minimum_players',
            'require_credit_card',
            'notify_all_participants'
        ];
        data = _.pick(data, tee_sheet_fields);
        console.log('------------------------------------- saveSettings --------------------------------------------');
        console.dir(data);

        //        return;
        //data.groups = this.groups.toJSON();
        //data.photo = this.$el.find('img.customer-photo').prop('src');
        if (Object.keys(data).length > 0) {
            this.model.set(data);
            this.model.save();
        }
    },
    viewOnlineReservationGroup:function(){
        var onlineReservationGroupModal = new EditOnlineReservationGroupView();
        onlineReservationGroupModal.show();
    },
    viewOnlineSpecial:function(){
        var onlineSpecialModal = new EditOnlineSpecialView();
        onlineSpecialModal.show();

    }
});

var OnlineGeneralSettingsView = Backbone.Marionette.ItemView.extend({
    tagName: 'div',
    template: JST['settings/templates/online_reservations/online_general_settings.html']
});

var OnlineReservationGroupListItemView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/online_reservations/online_reservation_group_list_item.html'],

    initialize: function () {

    },

    events: {
        // "blur .printer": "updateLabel",
        // "blur .ip_address": "updateIpAddress",
        // "click .removePrinter": "removePrinter"
    },

    updateLabel: function(){
        // var label = this.$el.find('.printer').val();
        // this.model.set('label', label);
    },

    updateIpAddress: function(){
        // var ipAddresss = this.$el.find('.ip_address').val();
        // this.model.set('ipAddress', ipAddresss);
    },

    removePrinter: function(){
        // this.model.destroy();
        // this.remove();
    },

    save: function () {

    },

    onRender: function(){
    }

});

var OnlineSpecialListItemView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/online_reservations/online_special_list_item.html'],

    initialize: function () {

    },

    events: {
        // "blur .printer": "updateLabel",
        // "blur .ip_address": "updateIpAddress",
        // "click .removePrinter": "removePrinter"
    },

    updateLabel: function(){
        // var label = this.$el.find('.printer').val();
        // this.model.set('label', label);
    },

    updateIpAddress: function(){
        // var ipAddresss = this.$el.find('.ip_address').val();
        // this.model.set('ipAddress', ipAddresss);
    },

    removePrinter: function(){
        // this.model.destroy();
        // this.remove();
    },

    save: function () {

    },

    onRender: function(){
    }

});

