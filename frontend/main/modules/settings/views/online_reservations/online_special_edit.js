var EditOnlineSpecialView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['settings/templates/online_reservations/edit_online_special_view.html'],

    regions: {

    },

    initialize: function () {
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
    }
});
