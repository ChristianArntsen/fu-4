var EmployeePageSettingsView = PageSettingsView.extend({

    intitialize: function(options){
        PageSettingsView.prototype.apply(this, options);
    },

    onRender: function(){
        this.renderColumns();
        this.renderPageSize();
        this.generalSettings.show( new EmployeePageSettingsRequiredFieldsView() );

        if (!this.options.modal) {
            this.$el.find('#modal').prevObject[0].className = '';
            this.$el.find('.modal-content').removeClass('modal-content');
            this.$el.find('.modal-footer').hide();
        }

    },

    save: function(){

        var view = this;
        this.columns.currentView.setColumns();
        this.pageSize.currentView.setPageSize();

        view.$el.loadMask();

        this.model.save(null, {
            success: function(){
                App.vent.trigger('notification', {'msg': 'Settings saved', 'type': 'success'});
                view.hide();
                $.loadMask.hide();
            },
            error: function(model, response){
                App.vent.trigger('notification', {'msg': response.responseJSON.msg, 'type': 'danger'});
                $.loadMask.hide();
            }
        });
    }
});

var EmployeePageSettingsRequiredFieldsView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['settings/templates/employees/employee_settings_required_fields.html'],
    ui:{
    },
    events:{
    },
    initialize:function(){

    },
    onShow:function(){

    },
    onRender:function(){

    }
});