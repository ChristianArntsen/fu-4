var EditPriceClassView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['settings/templates/tee_sheets/edit_price_class_view.html'],

    regions: {},
    events: {
        'click #save-changes':'save'
    },
    initialize:function(){
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
    },
    onRender:function(){
        this.$el.find('#colorPicker').colorpicker();
    },
    save:function(){
        var name = this.$el.find('#name');
        var color = this.$el.find('#color');
        var cart = this.$el.find('#cart');
        var greenGlCode = this.$el.find('#greenGlCode');
        var cartGlCode = this.$el.find('#cartGlCode');
        this.model.set('name', name.val());
        this.model.set('color', color.val());
        this.model.set('cart', cart.is(':checked') ? 1 : 0);
        this.model.set('greenGlCode', greenGlCode.val());
        this.model.set('cartGlCode', cartGlCode.val());
        this.model.save();
        this.hide();
    }
});