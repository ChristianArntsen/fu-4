var TeeSheetBasicSettingsView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    template: JST['settings/templates/tee_sheets/basic_settings.html'],
    groups:{},

    regions: {
        'seasons':'.seasons',
        'season_pricing':'.season-pricing'
    },

    initialize: function (){

    },

    events: {

    },

    optionsChanged: function() {
        var fields = [
            'first_name',
            'last_name',
            'email',

            'account_number',
            'price_class',
            'discount',
            //'invoice_email',
            'cell_phone_number',
            'phone_number',
            'birthday',
            'date_created',
            'address_1',
            'address_2',
            'city',
            'state',
            'zip',
            'status',
            'comments',
            'status_flag',
            'handicap_score',
            'handicap_account_number',
            'username',
            'password',
            'confirm_password'
            //'image_id'
        ];

        var data = _.getFormData(this.$el);
        data = _.pick(data, fields);

        data.groups = this.groups.toJSON();
        //data.photo = this.$el.find('img.customer-photo').prop('src');

        if(this.isValidPhone(this.model.get("cell_phone_number"))){
            data.cell_phone_number = data.cell_phone_number.replace(/\D/g,"");
        }
        if(this.isValidPhone(this.model.get("phone_number"))){
            data.phone_number = data.phone_number.replace(/\D/g, "");
        }

        if(data.password == "" || data.confirm_password == "" ){
            delete(data.password);
            delete(data.confirm_password);
        }

        this.model.set(data);
    },

    onRender: function(){
        App.data.course.get('seasons').tee_sheet_id = this.model.get('teesheet_id');
        this.seasons.show( new ManageSeasonsView({collection:App.data.course.get('seasons')}));
        this.$el.find('.colorPicker').colorpicker();
    }
});

