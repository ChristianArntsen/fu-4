var TeeSheetTaxesAndReceiptsView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/tee_sheets/taxes_and_receipts.html'],
    events:{
        'click .manage_receipt_agreements':'receiptAgreements'
    },
    receiptAgreements: function(){
        var modal = new window.parent.ItemReceiptContentModalView({
            collection: window.parent.App.data.item_receipt_content
        });
        modal.show();
        return false;
    },
});

