var PlayerTypesView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/tee_sheets/player_types.html'],
    events:{
        'click .add-price-class':'newPriceClass'
    },
    initialize:function(){
        this.listenTo(this.collection,'sync',this.renderPriceClasses);
    },
    onRender:function(){
        this.renderPriceClasses();
    },
    newPriceClass:function(){
        var price_class = new PriceClassModel();
        this.collection.add(price_class);
        this.editPriceClass(price_class);
    },
    addPriceClass:function(price_class){
        this.$el.find('.player-type-list').append(new PriceClassRowView({model: price_class}).render().el );
    },
    editPriceClass:function(price_class){
        var editPriceClassView = new EditPriceClassView({model:price_class});
        editPriceClassView.show();
    },
    renderPriceClasses:function(){
        var view = this;
        this.$el.find('.player-type-list').html('');
        if (this.collection.length > 0) {
            _.each(this.collection.models, function(price_class){
                view.addPriceClass(price_class);
            });
        } else {
            var html = '<tr><td><div class="col-md-3">'+
                '<img src="/images/sales/terminals-icon.png"/>'+
                '</div>'+
                '<div class="col-md-7 no-printers-box">'+
                '<h4>You currently do not have any terminals</h4>'+
                '<div class="form-inline marg-top-3">'+
                '<div class="btn-group form-group">'+
                '<button type="button" class="btn add-terminal btn-primary new settings-corner-button">Add Terminal</button>'+
                '</div>'+
                '</div>'+
                '</div></td></tr>';
            this.$el.find('.player-type-list').append(html);
        }
    }
});

var PriceClassRowView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/tee_sheets/price_class_row.html'],

    initialize: function () {

    },

    events: {
        'click .edit-price-class': 'edit',
        'click .delete-price-class':'delete'
    },
    edit:function(e){
        e.preventDefault();
        var editPriceClassView = new EditPriceClassView({model:this.model});
        editPriceClassView.show();
    },
    delete:function(){
        this.model.destroy();
        this.remove();
    }
});