var ManageSeasonsView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    template: JST['settings/templates/tee_sheets/manage_seasons.html'],
    initialize: function (){
        this.collection.fetch({'async':false});
    },

    events: {
        'click .seasonListItem':'selectSeason'
    },

    onRender: function(){
        this.loadSeasonList();
    },

    loadSeasonList: function(){
        this.$el.find('.season-list').html('');
        var view = this;
        _.each(this.collection.models, function(season){
            view.addSeason(season);
        });
    },
    addSeason: function(season){
        this.$el.find('.season-list').append(new SeasonListItemView({model:season}).render().el);
    },
    selectSeason: function(e){
        var season_id  = $(e.target).data('season-id');
        this.highlightSeason(e);
        this.loadSeasonSettings(season_id);
        this.loadSeasonPriceClasses(season_id);
    },
    highlightSeason:function(e){
        $('.selected-season').removeClass('selected-season');
        $(e.target).addClass('selected-season');
    },
    loadSeasonSettings:function(season_id){
        this.$el.find('.season-settings').html(new SeasonSettingsListView().render().el);
    },
    loadSeasonPriceClasses:function(season_id){
        App.data.course.get('price_classes').season_id = season_id;
    }
});

var SeasonListItemView = Backbone.Marionette.ItemView.extend({
    tagName: 'div',
    template: JST['settings/templates/tee_sheets/season_item_view.html']
});

var SeasonSettingsListView = Backbone.Marionette.ItemView.extend({
    tagName: 'div',
    template: JST['settings/templates/tee_sheets/season_settings_list_view.html']
});
