var TaxesAndReturnsView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/sales/taxes_and_returns.html'],
    events:{
        'click .add_return_reason':'newReturnReason'
    },
    initialize:function(){
        this.listenTo(App.vent, 'save-settings', this.save);
    },
    save:function(){
        this.saveReturnReasons();
    },
    onRender:function(){
        this.renderReturnReasons();
    },
    renderReturnReasons:function(){
        var view = this;
        this.$el.find('.return-reason-list').html('');
        var returnReasons = App.data.course.get('refund_reasons');
        if (returnReasons.length > 0) {
            _.each(returnReasons.models, function(returnReason){
                view.addReturnReasonRow(returnReason);
            });
        } else {
            var html = '<tr id="no-return-reasons"><td colspan="2">No Return Reasons</td></tr>';
            this.$el.find('.return-reason-list').append(html);
        }
    },
    addReturnReasonRow:function(returnReason){
        this.$el.find('.return-reason-list').append(new ReturnReasonView({model: returnReason}).render().el );
    },
    newReturnReason:function(){
        var view = this;
        var returnReasons = App.data.course.get('refund_reasons');
        var newReturnReason = new RefundReason();
        returnReasons.add(newReturnReason);
        newReturnReason.save({}, {success:function(){
            view.renderReturnReasons();
        }});

    },
    saveReturnReasons:function(){
        var returnReasons = App.data.course.get('refund_reasons');
        _.each(returnReasons.models, function(returnReason){
            returnReason.save();
        });
    },
});

var ReturnReasonView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/sales/return_reason.html'],

    initialize: function () {

    },

    events: {
        "keyup .returnReason": "updateLabel",
        "click .removeReturnReason": "removeReturnReason"
    },

    updateLabel: function(){
        var returnReason = this.$el.find('.returnReason').val();
        this.model.set('label', returnReason);
    },

    removeReturnReason: function(){
        this.model.destroy();
        this.remove();
    },

    save: function () {

    },

    onRender: function(){
    }

});