var PrintingAndTerminalsView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/sales/printing_and_terminals.html'],
    events:{
        'click .manage-printers':'managePrinters',
        'click .add-terminal':'openTerminalView',
        'click .manage-receipt-agreements':'manageReceiptAgreements'
    },
    initialize:function(){
        this.terminals = App.data.course.get('terminals');
        this.listenTo(this.terminals,'add change remove',this.renderTerminals);
        this.listenTo(this.terminals,'add change remove',this.renderTerminalDropdowns);
        //this.listenTo(App.vent, 'save-settings', this.save);
    },
    save:function(){

    },
    onRender:function(){
        this.renderTerminals();
    },
    managePrinters:function(){
        var managePrintersView = new ManagePrintersView();
        managePrintersView.show();
    },
    renderTerminals:function(){
        var view = this;
        this.$el.find('.terminal-list').html('');
        if (this.terminals.length > 0) {
            _.each(this.terminals.models, function(terminal){
                view.addTerminal(terminal);
            });
        } else {
            var html = '<tr><td><div class="col-md-3">'+
                    '<img src="/images/sales/terminals-icon.png"/>'+
                '</div>'+
                '<div class="col-md-7 no-printers-box">'+
                    '<h4>You currently do not have any terminals</h4>'+
                    '<div class="form-inline marg-top-3">'+
                        '<div class="btn-group form-group">'+
                            '<button type="button" class="btn add-terminal btn-primary new settings-corner-button">Add Terminal</button>'+
                        '</div>'+
                    '</div>'+
                '</div></td></tr>';
            this.$el.find('.terminal-list').append(html);
        }
    },
    renderTerminalDropdowns:function(){
        var dropdowns = this.$el.find('.terminal-dropdown');
        _.each(dropdowns, function(dropdown){
            var selected = $(dropdown).val();
            var name = $(dropdown).attr('name');
            $(dropdown).replaceWith(_.dropdown(App.data.course.get('terminals').getDropdownData(true), {"name":name, "class":"form-control terminal-dropdown"}, selected));
        });
    },
    addTerminal: function (terminal) {
        this.$el.find('.terminal-list').append(new TerminalManageView({model: terminal}).render().el );
    },
    openTerminalView:function(){
        var newTerminal = new Terminal();
        var editTerminalView = new EditTerminalView({model:newTerminal});
        editTerminalView.show();
    },
    manageReceiptAgreements:function(){
        // var editReceiptAgreementView = new editReceiptAgreementView();
        // editReceiptAgreementView.show();
        var modal = new ItemReceiptContentModalView({collection: App.data.item_receipt_content});
        modal.show();
    }
});

var TerminalManageView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/sales/manage_terminal.html'],

    initialize: function () {

    },

    events: {
        'click .edit-terminal': 'editTerminal'
    },
    editTerminal:function(e){
        e.preventDefault();
        var editTerminalView = new EditTerminalView({model:this.model});
        editTerminalView.show();
    },

    updateLabel: function(){
    },

    removePrinterGroup: function(){
    },

    save: function () {

    },

    onRender: function(){
    }

});

