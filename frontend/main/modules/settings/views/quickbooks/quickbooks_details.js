var QuickbooksView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/quickbooks/quickbooks_view.html']
});
