var LoyaltyView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/loyalty/loyalty_view.html'],
    events:{
        'click .add-loyalty-rate':'newLoyaltyRate'
    },
    initialize:function(){
        this.listenTo(App.vent, 'save-settings', this.save);
        this.loyalty_rates = App.data.course.get('loyalty_rates');
    },
    onRender:function(){
        this.renderRates();
    },
    renderRates:function(){
        var view = this;
        this.$el.find('.loyalty-rate-list').html('');
        if (this.loyalty_rates.length > 0) {
            _.each(this.loyalty_rates.models, function(loyalty_rate){
                view.addRate(loyalty_rate);
            });
        } else {
            var html = '<tr id="no-loyalty-rates"><td colspan="6">No Loyalty Rates</td></tr>';
            this.$el.find('.loyalty-rate-list').append(html);
        }
    },
    newLoyaltyRate:function(){
        var newLoyalty = new LoyaltyRate();
        this.addRate(newLoyalty);
        this.loyalty_rates.add(newLoyalty);
    },
    addRate:function(rate){
        this.$el.find('.loyalty-rate-list').append(new LoyaltyRateManageView({model: rate}).render().el );
    },
    save:function(){
        var view = this;
        _.each(view.loyalty_rates.models, function(loyalty_rate){
            loyalty_rate.save();
        });
    }
});

var LoyaltyRateManageView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/loyalty/manage_rate.html'],

    initialize: function () {

    },

    events: {
        "click .removeRate": "removeRate",
        "change .dollarsPerPoint":"updateDollarsPerPoint",
        "change .pointsPerDollar":"updatePointsPerDollar",
        "change .rateLabel":"updateLabel",
        "change .valueLabel":"updateValueLabel",
    },

    updateDollarsPerPoint: function(){
        var dollarsPerPoint = this.$el.find('.dollarsPerPoint').val();
        this.model.set('dollarsPerPoint', dollarsPerPoint);
        this.updateEffectiveRate();
    },

    updatePointsPerDollar: function(){
        var pointsPerDollar = this.$el.find('.pointsPerDollar').val();
        this.model.set('pointsPerDollar', pointsPerDollar);
        this.updateEffectiveRate();
    },

    updateEffectiveRate:function(){
        var dpp = this.model.get('dollarsPerPoint');
        var ppd = this.model.get('pointsPerDollar');
        this.$el.find('.effectiveRate').html(dpp*ppd+'%');
    },

    updateLabel: function(){
        var label = this.$el.find('.rateLabel').val();
        this.model.set('label', label);
    },

    updateValueLabel: function(){
        var valueLabel = this.$el.find('.valueLabel').val();
        this.model.set('valueLabel', valueLabel);
    },

    removeRate: function(){
        this.model.destroy();
        this.remove();
    }

});
