var BusinessDetailsView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/business_details/business_details_view.html']
});
