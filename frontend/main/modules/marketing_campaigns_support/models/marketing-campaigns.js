if(typeof BASE_URL === 'undefined'){
    BASE_URL = '/';
}

var MarketingCampaigns = Backbone.JsonRestModel.extend({
    url : function() {
        var base = BASE_URL + 'api_rest/index.php/marketing_campaigns';
        if (this.isNew()) return base;
        return base + (base.charAt(base.length - 1) == '/' ? '' : '/') + this.id;
    },
    demote_to_draft: function(callback){
        $.ajax(BASE_URL + 'api_rest/index.php/marketing_campaigns/'+this.id +'/demote_to_draft', {
            method:"POST",
            contentType:'application/json',
            success: function (res) {
                if(typeof callback === 'function')callback(res);
            },
            dataType: 'json'
        });
    },
    promote_to_sent: function(callback){
        $.ajax(BASE_URL + 'api_rest/index.php/marketing_campaigns/'+this.id +'/promote_to_sent', {
            method:"POST",
            contentType:'application/json',
            success: function (res) {
                if(typeof callback === 'function')callback(res);
            },
            dataType: 'json'
        });
    },
    _super: Backbone.JsonRestModel.prototype
});

var MarketingCampaignsCollection = Backbone.JsonRestCollection.extend({
    model: MarketingCampaigns,
    url: BASE_URL + 'api_rest/index.php/marketing_campaigns',
    initialize: function(){
        this.fetch();
    },
    demote_to_draft: function(id,callback){
        return this.get(id).demote_to_draft(callback);
    },
    promote_to_sent: function(id,callback){
        return this.get(id).promote_to_sent(callback);
    },
    _super: Backbone.JsonRestCollection.prototype
});