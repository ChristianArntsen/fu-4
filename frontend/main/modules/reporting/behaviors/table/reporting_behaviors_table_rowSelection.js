var reporting_behaviors_table_rowSelection = Backbone.Marionette.Behavior.extend({
    defaults: {
        table:".table_id"
    },
    ui: function(){
        return {
            table: this.options.table
        }
    },
    onRender:function(){
        if(this.$el.find(this.ui.table).length){
            this.$el.find(this.ui.table).selectable({
                distance: 30,
                filter: 'tr',
                selecting: function(e, ui) {
                    var curr = $(ui.selecting.tagName, e.target).index(ui.selecting);
                    if(e.shiftKey && prev > -1) {
                        $(ui.selecting.tagName, e.target).slice(Math.min(prev, curr), 1 + Math.max(prev, curr)).addClass('ui-selected');
                        prev = -1;
                    } else {
                        prev = curr;
                    }
                }
            });
        }
    }
});