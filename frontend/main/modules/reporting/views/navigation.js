var reporting_views_navigation = Backbone.Marionette.ItemView.extend({
	template: JST['reporting/templates/navigation.html'],
	initialize:function(){
		this.listenTo(this.options.tags,"change reset add remove",this.handleTagUpdate);
		this.listenTo(this.collection,"change reset add remove",this.render);
		this.handleTagUpdate();

	},
	onRender:function(){
		var self = this;
		this.$el.find('li.nav-tab-vertical').first().addClass('active');
		this.$el.find('div.tab-pane').first().addClass('active');
	},
	handleTagUpdate:function(){
		this.tags = this.options.tags.pluck("tag");
		this.render();
	},
	events:{
		"click .report-template":"selectReport",
		"click label.tree-toggler":"toggleTree",
		"keyup #report_search":"searchHandler",
		"change #report_search":"searchHandler",
		"click #report_search":"searchHandler"
	},
	resetSearchResults:function(){
		this.$el.find(".search-number").hide();
		this.$el.find(".report-template").show();
	},
	searchHandler:function(e){
		var self = this;
		this.searchTerm = this.$el.find('#report_search').val()
		var tagTotals = [];
		this.resetSearchResults();
		if(self.searchTerm == ""){
			return;
		}

		this.collection.forEach(function(item){


			if( item.get("description").toLowerCase().includes(self.searchTerm.toLowerCase()) ||
				item.get("title").toLowerCase().includes(self.searchTerm.toLowerCase())
			){
				self.$el.find("[data-value='"+item.get("id")+"']").show();
				_.forEach(item.get("tags"),function(tag){
					if(tagTotals[tag] == undefined){
						tagTotals[tag] = 0;
					}
					tagTotals[tag]++;
				})
			} else {
				self.$el.find("[data-value='"+item.get("id")+"']").hide();
			}
		});
		for (var tag in tagTotals){
			var badge = self.$el.find("#report-"+tag+" .badge");
			badge.show();
			badge.html(tagTotals[tag]);
		}


	},
	selectReport:function(e){
		// Create a new instance of ladda for the specified button
		if($(e.currentTarget).hasClass("ladda-button") ){
			var l = $(e.currentTarget).ladda();
		} else {
			var l = $(e.currentTarget).find('.ladda-button').ladda();
		}

		// Start loading
		l.ladda( 'start' );

		var report_id = $(e.currentTarget).attr("data-value")
		this.options.parent.selectReportById(report_id);
	},
	serializeData: function(){
		var context = {};
		context.items =  this.collection.toJSON();
		context.tags =  this.tags;
		return context;
	}
});