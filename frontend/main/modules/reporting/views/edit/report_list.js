reporting_views_edit_reportList = Backbone.Marionette.CompositeView.extend({
    getChildView: function(item){
        return reporting_views_edit_reportListItem;
    },
    template: JST['reporting/templates/edit/ReportList.html'],
    childViewContainer: "#reports",
	initialize:function(){
		var self = this;
		this.collection.fetch({
			"success": function (model) {
				self.render();
			},
            "error":function(collection,response,options){
                App.vent.trigger('notification', {'msg': "Unable to retrieve report list.  Contact support if this continues to happen.", 'type': 'danger'});
            }
		});
	},
    events:{
        "click #createNewReport":"createNewReport"
    },
    createNewReport:function(e){
        var report = new reporting_models_report();
        report.save().done(function(data){
                Backbone.history.navigate('report_edit/'+report.get("id"), true);
            }
        );
    }
});