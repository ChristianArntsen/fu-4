var reporting_views_edit_tags = Backbone.Marionette.ItemView.extend({
    template: JST['reporting/templates/edit/Tags.html'],
    tagName:"div",
    onRender:function(){
        this.$el.find("#tags").select2({
            tags: true,
            multiple:true
        })
    },
    initialize:function(){
        var self = this;
        var tags = new reporting_models_report_tags();
        tags.fetch().success(function(response){
            var data = [];
            _.forEach(response,function(value){
                data.push({
                    id:value.tag,
                    text:value.tag
                });
            })
            self.$el.find("#tags").select2({
                data: data
            })
        });
    },
    events: {
        "change #tags":"handleTagChange"
    },
    handleTagChange:function(){
        var tags = this.model.get("tags");
        tags.splice(0,tags.length);
        _.forEach(this.$el.find("#tags").val(),function(newTag){
            tags.push(newTag);
        })
    }

});