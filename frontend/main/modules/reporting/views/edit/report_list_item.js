reporting_views_edit_reportListItem = Backbone.Marionette.ItemView.extend({
    template: JST['reporting/templates/edit/ReportListItem.html'],
    tagName:"tr",
    initialize:function(){
    },
    events:{
        "click #deleteReport":"deleteReport"
    },
    deleteReport:function(event){
        this.model.destroy();
    }
});