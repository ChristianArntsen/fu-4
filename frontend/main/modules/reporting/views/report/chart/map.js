var reporting_views_report_chart_map = reporting_views_report_chart.extend({
    _renderChart:function(){
        if(!this.$el.find("#myChart")){
            return;
        }
        var data = this._convertCSVToDatasets(this.model.get("chart_data"));
        console.log(data);
        this.$el.find("#myChart").highcharts('Map', {
                plotOptions:{turboThreshold: 100000},
                title: {
                    text: 'Highmaps basic lat/lon demo'
                },

                mapNavigation: {
                    enabled: true
                },

                tooltip: {
                    headerFormat: '',
                    pointFormat: '<b>{point.name}</b><br>Lat: {point.lat}, Lon: {point.lon}'
                },

                series: [{
                    // Use the gb-all map with no data as a basemap
                    mapData: Highcharts.maps['custom/usa-and-canada'],
                    name: 'Basemap',
                    borderColor: '#A0A0A0',
                    nullColor: 'rgba(200, 200, 200, 0.3)',
                    showInLegend: false
                }, {
                    // Specify points using lat/lon
                    type: 'mappoint',
                    name: 'Cities',
                    color: Highcharts.getOptions().colors[1],
                    data:data
                }]
            }
        );
    },
    _convertCSVToDatasets:function(data){
        var datasets = [];
        _.each(data,function(row,rowNumber){
            if(rowNumber > 0){
                datasets.push({
                    lat:row[0] * 1,
                    lon:row[1] * 1
                })
            };
        })

        return datasets;
    },
    updateChart:function(){


        this.categoryColumn = this.model.get("columns").findWhere({"completeName":"latitude_centroid"});
        this.valueColumn = this.model.get("columns").findWhere({"completeName":"longitude_centroid"});
        this.categoryLabel = (this.categoryColumn.get("label") || this.categoryColumn.get("completeName"));
        if(this.model.chartParameters.get("visualGrouping")){
            this.categoryLabel += " by "+this.model.chartParameters.get("visualGrouping")
        }
        this.valueLabel = this.valueColumn.get("label") || this.valueColumn.get("completeName");
        if(this.model.get("chart_data") != ""){
            this._renderChart();
        }
    }
});

