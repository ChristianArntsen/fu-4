var reporting_views_report_tableMini = Backbone.Marionette.ItemView.extend({
		template: JST['reporting/templates/report/table_mini.html'],

		initialize: function () {

		},
		loadDataTable:function(){
			var table = this.$el.find('#mini_table').DataTable();
		},
		refreshTable:function(){
			this.model.set("visibleColumns",this.model.getVisibleColumnObjects());
			this.render();
		},
		handleLoading:function(){
			this.render();
		},
		behaviors: {
			LoadingBehavior: {
				behaviorClass: reporting_behaviors_loading
			},
			SummaryBehavior: {
				behaviorClass: reporting_behaviors_table_rowsSummary
			},
			"ModalDialogBehavior":{
				behaviorClass: reporting_behaviors_modal_dialog
			}
		}
	}
);
