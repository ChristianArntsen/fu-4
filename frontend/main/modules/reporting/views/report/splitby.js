var reporting_views_report_splitByView = EpoxyMarionetteView.extend((function(){

	return {
		template: JST['reporting/templates/report/splitby.html'],
		childViewContainer: "select",
		modelEvents:{
			"change visibleColumns":"handleColumnsChanged"
		},
		handleColumnsChanged:function(){
			this.render();
		}
	}
})());

