var reporting_views_report_filters_filter = EpoxyMarionetteView.extend({

    events:{
        "click .dropdown-toggle":"dropDownToggler",
        "click .dropdown-menu li":"dropDownSelector"
    },
    dropDownToggler:function(e){
        this.$el.find(".dropdown-menu").toggle();
    },
    dropDownSelector:function(e){
        this.$el.find(".dropdown-menu").toggle();

        var options = {
            custom:"fa-crosshairs",
            is_null:"fa-star-o",
            is_not_null:"fa-star",
            not_equal:"fa-exclamation",
            terminal:"fa-desktop",
            user:"fa-user"
        };
        //Get id of element that was clicked
        //Set
        var selectedElement = this.$el.find(".dropdown-toggle").find("i");
        var selectedOption = $(e.currentTarget).data("value");
        selectedElement.removeClass();
        selectedElement.addClass("fa");
        selectedElement.addClass(options[selectedOption]);

        this.model.set("value_type",selectedOption);
        if(selectedOption != "custom" && selectedOption != "not_equal"){
            this.$el.find("input").prop("disabled",true);
            this.$el.find("select").prop("disabled",true);
            this.model.set("value",selectedOption);
        } else {
            this.$el.find("input").prop("disabled",false);
            this.$el.find("select").prop("disabled",false);
            this.model.set("value","");
        }


        e.preventDefault();
        e.stopPropagation();
    }

});
