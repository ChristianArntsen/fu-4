var reporting_views_report_reportStaticLayout = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	template: JST['reporting/templates/report/report.static.layout.html'],

	initialize:function(){

		this.listenTo(App.vent, 'refresh-report', this.handleRefreshReport);
	},
	modelEvents:{
	},
	onRender:function(){
        var report = this.model;
		if(report.get("data") == undefined){
			report.getData();
		}

		if(report.get("date_range").get("column") != "" && report.get("date_range").get("column") != undefined){
			report.get('date_range').loadDefaultRange();
			this.getRegion('dateFilter').show(new reporting_views_report_filters_dateRange({model:report.get("date_range")}) );
		}
        this.getRegion('filters').show(new reporting_views_appliedFilters_layout({collection:report.get('filters'),model:report}) );
		this.getRegion('table').show(new reporting_views_report_static({model:this.model}));
	},

	handleRefreshReport:function(){
		var report = this.model;
		report.getData();
	},
	onShow:function(){
	},
	regions: {
		tabs: '#r-tab',
		table: '#r-table',
		filters: '#r-filters',
		dateFilter: '#r-default-date-filter'
	},
	downloadExcel: function(e){
		e.preventDefault();
		this.model.getFile("excel");
	},

	downloadPdf: function(e){
		e.preventDefault();
		this.model.getFile("pdf");
	},
	downloadCsv: function(e){
		e.preventDefault();
		this.model.getFile("csv");
	},
	events: {
		"click #data-refresh":"sendAjax",
		"click #download-excel":"downloadExcel",
		"click #download-pdf":"downloadPdf",
		"click #download-csv":"downloadCsv",
		"click #save":"saveModal"
	}

});
