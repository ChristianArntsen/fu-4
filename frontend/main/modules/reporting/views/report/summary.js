var reporting_views_report_summary = Backbone.Marionette.ItemView.extend({
		template: JST['reporting/templates/report/summary.html'],
		initialize: function () {
			this.listenTo(this.model, 'change:summary_data', this.refreshTable);
			this.listenTo(this.model, 'change:loading', this.handleLoading);
		},
		onRender:function(){
		},
		refreshTable:function(){
			this.render()
		},
		handleLoading:function(){
			this.render();
		}
	}
);
