var reporting_views_appliedFilters_layout = Backbone.Marionette.LayoutView.extend({
	template: JST['reporting/templates/report/applied_filters/layout.html'],

    onRender:function(){
        var possibleFilters = this.model.getFilterableColumns();
        possibleFilters = possibleFilters.sort();
        var activeFilters = new reporting_views_report_appliedFilters_collection({collection:this.collection,model:this.model});
        var filterableList = new reporting_views_report_filterSelection_filterableList({collection:possibleFilters,model:this.model});
        this.getRegion('filters').show(activeFilters);
        this.getRegion('newFilter').show(filterableList);

    },
	events:{
		"click #new-filter":"addFilter"
	},
	addFilter: function(event){
        event.stopPropagation();
	},
    regions: {
        newFilter: '#new-filter',
        filters: '#filters'
    }
});
