var reporting_views_report_tableBasic = Backbone.Marionette.ItemView.extend({
		template: JST['reporting/templates/report/table_mini.html'],

		initialize: function () {
			this.totalNumericColumns();
		},
		totalNumericColumns:function(){
			var totals = [];
			var self = this;
			this.model.set("visibleColumns",this.model.getVisibleColumnObjects());
			_.each(self.model.get("data"),function(row,key){
				_.each(self.model.get("visibleColumns"),function(column,key){
					var value = row[column.get('completeName')];
					if(!totals[key]){
						if($.isNumeric(value)){
							totals[key] = parseFloat(value)
						} else {
							totals[key] = "";
						}
					} else {
						if($.isNumeric(value) && $.isNumeric(totals[key])){
							totals[key] += parseFloat(value)
						}
					}
				})
			})
			_.each(totals,function(total,key){
				if($.isNumeric(total)){
					totals[key] = parseFloat(total).toFixed(2)
				}
			})
			this.model.set("totals",totals);
		},
		onRender:function(){
			var totals = [];
			this.$el.find( "tr").each(function() {

				$(this).children('td').each (function(key) {
					var value = $(this).attr("data-value");
					if(!totals[key]){
						if($.isNumeric(value)){
							totals[key] = parseFloat(value)
						} else {
							totals[key] = "";
						}
					} else {
						if($.isNumeric(value) && $.isNumeric(totals[key])){
							totals[key] += parseFloat(value)
						}
					}
				});
			});
			this.model.set("totals",totals);
		},
		onShow:function(){
			this.loadDataTable();
		},
		loadDataTable:function(){
			if(this.model.get("data") != null){

				var oTable = this.$el.find('#table_id').DataTable({
					"scrollY":        "400px",
					"scrollCollapse": true,
					"dom": "rtiS",
					"deferRender": true
				});

			}
		},
		behaviors: {
			SummaryBehavior: {
				behaviorClass: reporting_behaviors_table_rowsSummary
			}
		}
	}
);
