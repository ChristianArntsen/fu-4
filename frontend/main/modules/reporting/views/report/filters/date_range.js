var reporting_views_report_filters_dateRange = reporting_views_report_filters_filter.extend({
    template: JST['reporting/templates/report/filter/date_range.html'],
    className: 'nr-filter-container form-inline',
    onRender: function(){
        var singleDate = this.model.get("start_locked");
        var self = this;


        var start = self.model.get("start_value");
        var end = self.model.get("end_value");
        if(singleDate){
            start = self.model.get("end_value");
        }
        this.$el.find('.input-daterange').daterangepicker(
            {
                format: 'MMMM DD, YYYY")',
                startDate: start,
                endDate: end,
                showDropdowns: true,
                showWeekNumbers: true,
                singleDatePicker:singleDate,
                opens: 'right',
                drops: 'down',
                rangeSelectedCallback:function(e) {

                    if(e == "custom"){
                        self.model.setToStaticMode();
                        return
                    }
                    self.model.setToDynamicMode();
                    var dynamicRange = e.target.innerHTML;
                    //self.model.set("type", "moving");
                    if(dynamicRange == "Today"){
                        self.model.set("movingRange", "today");
                        self.model.set("typeOfMovingRange", "label");
                    } else if(dynamicRange == "Yesterday"){
                        self.model.set("movingRange", "yesterday");
                        self.model.set("typeOfMovingRange", "label");
                    } else if(dynamicRange == "Last 7 Days"){
                        self.model.set("movingRange", "7");
                        self.model.set("typeOfMovingRange", "days");
                    } else if(dynamicRange == "Last 30 Days"){
                        self.model.set("movingRange", "30");
                        self.model.set("typeOfMovingRange", "days");
                    } else if(dynamicRange == "This Month"){
                        self.model.set("movingRange", "thismonth");
                        self.model.set("typeOfMovingRange", "label");
                    } else if(dynamicRange == "This Week"){
                        self.model.set("movingRange", "thisweek");
                        self.model.set("typeOfMovingRange", "label");
                    } else if(dynamicRange == "Last 2 Weeks"){
                        self.model.set("movingRange", "lasttwoweeks");
                        self.model.set("typeOfMovingRange", "label");
                    } else if(dynamicRange == "Last Month"){
                        self.model.set("movingRange", "lastmonth");
                        self.model.set("typeOfMovingRange", "label");
                    } else if(dynamicRange == "Last Year"){
                        self.model.set("movingRange", "lastyear");
                        self.model.set("typeOfMovingRange", "label");
                    } else {
                        //self.model.set("type", "static");
                        self.model.set("movingRange", "");
                        self.model.set("typeOfMovingRange", "");
                    }
                },
                ranges: self.rangeArray
            },
            function(start, end, label) {
                if(!singleDate){
                    self.model.set("start_value",start.format('MMMM DD, YYYY'));
                }
                self.model.set("end_value",end.format('MMMM DD, YYYY'));

                App.vent.trigger('refresh-report');
            });


        var $popup = $('.daterangepicker');
        $popup.click(function () { return false; });
    },

    rangeArray:{
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
        'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
        'Last 2 Weeks': [moment().subtract(14, 'days').startOf('week'), moment().subtract(7, 'days').endOf('week')],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'This Week': [moment().startOf('week'), moment().endOf('week')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
    },
    onInitialize:function(){
        //If in dynamic mode, set datepicker accordingly
        var mapping = {
            "today":"Today",
            "yesterday":"Yesterday",
            "lasttwoweeks":"Last 2 Weeks",
            "thismonth":"This Month",
            "thisweek":"This Week",
            "lastmonth":"Last Month",
            "lastyear":"Last Year",
            "7":"Last 7 Days",
            "30":"Last 30 Days"
        };
        if(mapping[this.model.get("movingRange")]){
            var mappedOut = mapping[this.model.get("movingRange")];
            var startValue = this.rangeArray[mappedOut][0];
            var endValue = this.rangeArray[mappedOut][1];
            this.model.set("start_value",startValue.format('MMMM DD, YYYY'));
            this.model.set("end_value",endValue.format('MMMM DD, YYYY'));
        }

    }

});
