var reporting_views_report_BlankReport = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	className: 'col-md-12',
	template: JST['reporting/templates/report/blank_report.html'],

	behaviors: {
		LoadingBehavior: {
			behaviorClass: reporting_behaviors_loading
		}
	},
	onRender:function(){
		this.getRegion('navigation').show(this.options.navigation);
	},
	regions: {
		navigation: '#r-navigation'
	}
});
