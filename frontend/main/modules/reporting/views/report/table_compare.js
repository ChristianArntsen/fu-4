var reporting_views_report_tableCompare = Backbone.Marionette.LayoutView.extend({
		template: JST['reporting/templates/report/table_compare.html'],
		ui:{
			"currentDateHeader":"#current_daterange",
			"previousDateHeader":"#previous_daterange"
		},
		modelEvents: {
			"change:date_range.start_value":"handleNewDateRange"
		},
		initialize:function(){
			this.listenTo(this.model.get("date_range"), 'change:start_value', this.handleNewDateRange);
			this.listenTo(this.model.get("date_range"), 'change:end_value', this.handleNewDateRange);
		},
		handleNewDateRange : function(){
			var dateRange = this.model.get("date_range");
			this.ui.currentDateHeader.text(this.generateHeader(dateRange));
		},
		handleNewPrevDateRange : function (){
			dateRange = this.model.get("comparativeReports")[0].get("date_range");
			this.ui.previousDateHeader.text(this.generateHeader(dateRange));
		},
		generateHeader:function(dateRange){
			return 	dateRange.get("start_value") + " - " + dateRange.get("end_value");
		},
		onRender: function () {
			if(this.getRegion("previousTable")){
				if(typeof(this.previous_table_view) == "undefined"){
					if(this.model.get("comparativeReports").length == 0){
						var clonedModel = this.model.deepClone();
						var currentDateRange = clonedModel.get("date_range").clone();
						clonedModel.set("date_range",currentDateRange);
						clonedModel.set("loading",0);
						this.model.set("comparativeReports",[clonedModel]);

						this.model.setDateInCompReports(clonedModel);
						this.model.getData();
					} else {

						this.model.setDateInCompReports(this.model.get("comparativeReports")[0]);
						this.model.getData();
					}

					this.previous_table_view = new reporting_views_report_table({model:this.model.get("comparativeReports")[0]});
					this.previous_table_view.comparative = true;

				}
				if(typeof(this.current_table_view) == "undefined"){
					this.current_table_view = new reporting_views_report_table({model:this.model});
				}
				this.getRegion('currentTable').show(this.current_table_view);
				this.getRegion('previousTable').show(this.previous_table_view);


				this.listenTo(this.model.get("comparativeReports")[0].get("date_range"), 'change:start_value', this.handleNewPrevDateRange);
				this.listenTo(this.model.get("comparativeReports")[0].get("date_range"), 'change:end_value', this.handleNewPrevDateRange);
				this.handleNewDateRange();
				this.handleNewPrevDateRange();
			}

		},
		searchTable:function(query){
			this.getRegion('previousTable').currentView.searchTable(query);
			this.getRegion('currentTable').currentView.searchTable(query);
		},
		regions: {
			previousTable: '#r-previous_table',
			currentTable: '#r-current_table'
		}
	}
);
