var reporting_views_report_filterSelection_filterableList = Backbone.Marionette.CompositeView.extend({
    template: JST['reporting/templates/report/filter_selection/filterable_list.html'],
    childViewContainer:"#filter_list",
    childViewOptions  : function () { return { parentModel: this.model }; },
    getChildView: function(item){
        return reporting_views_report_filterSelection_singleFilter;
    },
    events:{
        "keyup #search_for_filter":"searchForFilters"
    },
    searchForFilters:function(){
        var search = $('#search_for_filter').val() || '';
        this.collection.invoke('set', 'filter', search);
    }
});
