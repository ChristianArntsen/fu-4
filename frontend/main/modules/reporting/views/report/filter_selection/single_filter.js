var reporting_views_report_filterSelection_singleFilter = EpoxyMarionetteView.extend({
    tagName:"li",
    className: "filter-item",
    template: JST['reporting/templates/report/filter_selection/single_filter.html'],
    modelEvents: {
        "change filter": "filter"
    },
    filter: function(model) {
        if (model.shouldBeShown(model.get("filter"))) {
            this.$el.show();
        } else {
            this.$el.hide();
        }
    },
    events:{
        "click":"addFilter"
    },
    addFilter:function(){
        var filter_column = this.model.get("completeName");
        var parentModel = this.options.parentModel;
        var selectedColumn = parentModel.get("columns").findWhere({"completeName":filter_column});
        parentModel.get("filters").addFilter({
            column: filter_column,
            selectedColumn: selectedColumn,
            type: selectedColumn.get("type")
        });
        this.$el.parents('.page-filter').removeClass('open');
    }
});
