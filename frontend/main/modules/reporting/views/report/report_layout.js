var reporting_views_report_reportLayout = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	className: 'col-md-12',
	template: JST['reporting/templates/report/report.layout.html'],

	initialize:function(){
		if(this.model.chartParameters == undefined){
			this.model.chartParameters = new reporting_models_chartParameters({
				columns:this.model.get("columns")
			});
		}

        this.listenTo(this.model.chartParameters,"change:xAxis",function(){
            var chart_parameters = this.model.chartParameters;
            //If it's a date, set the dategrouping column to that date and set xAxis to "date"
            var possibleColumns =  chart_parameters.get("possibleXColumns");
            var column = possibleColumns.findWhere({completeName:chart_parameters.get("xAxis")});
            if(column.get("type") == "date_range"){
                this.model.get("date_range").set("column",column.get("completeName"));
            }
        });

		this.listenTo(this.model.chartParameters,"change",function(){
			this.refreshChart();
		});
		this.listenTo(App.vent, 'refresh-report', this.handleRefreshChart);
		this.listenTo(App.vent, 'columns-changed', this.handleColumnsChanged);

	},
	modelEvents:{
		"change:chart_showing":"handleChartShowing"
	},
	onRender:function(){

		if(this.model.get("data") == undefined){
			this.sendAjax();
		}

        var report = this.model;


		if(report.get("date_range").get("column") != "" && report.get("date_range").get("column") != undefined){
			report.get('date_range').loadDefaultRange();
			this.getRegion('dateFilter').show(new reporting_views_report_filters_dateRange({model:report.get("date_range")}) );
		}
        this.getRegion('filters').show(new reporting_views_appliedFilters_layout({collection:report.get('filters'),model:report}) );
        this.getRegion('columns').show(new reporting_views_columnSelection_collection({collection:report.getSelectableColumns(),model:report}) );
        this.getRegion('summary').show(new reporting_views_report_summary({model:report}) );


	},
	onShow:function(){
		this.handleChartShowing();
		if(this.model.get("user_field") == null )
		{
			$('#send-to-marketing').hide();
		}
	},

	regions: {
		tabs: '#r-tab',
		widget: '#r-widget',
		summary: '#r-summary',
		table: '#r-table',
		chart: '#r-chart',
		filters: '#r-filters',
		dateGrouping: '#r-date-grouping',
		dateFilter: '#r-default-date-filter',
		columns: '#r-columns',
		chartParameters: '#r-chart-parameters'
	},
	handleColumnsChanged:function(){
		this.model.set("columns_dirty",true);
		_.forEach(this.model.get("comparativeReports"),function(value,key){
			value.set("columns_dirty",true);
		});

		this.model.isDirty = true;
		this.sendAjax();
	},
	handleRefreshChart:function(){
		this.model.isDirty = true;
		this.sendAjax();
	},
	refreshChart:function(){
		this._renderChart();
		this.model.getChartData()
	},
	forceRefreshChart:function(){
		this._renderChart();
		this.model.forceGetChartData();
	},
	handleChartShowing:function(){

		if(this.model.get("chart_showing")){
			this._showChart();
		} else {
			this._hideChart();

			this.showTable();
		}

		this.$el.find('#toggle-chart').toggleClass("active",this.model.get("chart_showing"));
	},
	_renderChart:function(){
		if(this.model.chartParameters.get("selectedGraph") == "line" ){
			this.currentChartType = "line";
			this.getRegion('chart').show(new reporting_views_report_chart_line({model:this.model}) );
		} else if(this.model.chartParameters.get("selectedGraph") == "pie" ){
			this.currentChartType = "pie";
			this.getRegion('chart').show(new reporting_views_report_chart_pi({model:this.model}) );
		} else if(this.model.chartParameters.get("selectedGraph") == "bar" ){
			this.currentChartType = "bar";
			this.getRegion('chart').show(new reporting_views_report_bar({model:this.model}) );
		}

	},
	sendAjax: function(){
		this.showTable();
		if(this.getRegion("table")){
			this.model.getData();
			this.model.getSummary();
			this.model.forceGetChartData();
		}
	},
	downloadExcel: function(e){
		e.preventDefault();
		this.model.getFile("excel");
	},

	downloadPdf: function(e){
		e.preventDefault();
		this.model.getFile("pdf");
	},
	downloadCsv: function(e){
		e.preventDefault();
		this.model.getFile("csv");
	},
	showTable:function(){
		if(!this.model.get("chart_showing")){
			if(this.getRegion("table")){
				if(typeof(this.table_view) == "undefined"){
					if(this.model.get("comparative")){
						this.table_view = new reporting_views_report_tableCompare({model:this.model});
					} else {
						this.table_view = new reporting_views_report_table({model:this.model});
					}
					window.table_view = this.table_view;
				}

				this.getRegion('table').show(this.table_view,{ preventDestroy: true });
			}
		}
	},
	onDestroy:function(){
		if(typeof(this.table_view) != "undefined"){
			this.table_view.destroy();
		}
	},
	openChartOptions:function(){
		this.getRegion('chartParameters').show(new reporting_views_report_chart_editParameters({model:this.model.chartParameters }));
	},
	saveModal:function(){
		var form = new reporting_views_report_modalSave({model: this.model});
		form.show();
	},
	searchTable: function(){
		var query = this.$('input.search').val();
		var table = this.getRegion('table').currentView.searchTable(query);
	},
	toggleChart:function(){
		if(this.model.get("chart_showing") == undefined) {
			this.model.set("chart_showing",false);
		}
		this.model.set("chart_showing",!this.model.get("chart_showing"));

		//this.model.forceGetChartData();
	},
	_showChart:function(){
		this.$el.find('.page-subnav').hide();
		this.$el.find('#r-summary').hide();
		this.$el.find('#r-table').hide();

		this._renderChart();
		this.getRegion('chart').show(new reporting_views_report_bar({model:this.model}));
	},
	_hideChart:function(){
		this.$el.find('.page-subnav').show();
		this.$el.find('#r-summary').show();
		this.$el.find('#r-table').show();

		this.getRegion("chart").empty();
	},
	handleSendToMarketing:function(){
		var self = this;
		if(this.model.isDirty){
			this.model.set("saving_for_report",true);
			this.saveModal();
			this.model.once('report-saved', function(){

				self.model.isDirty = false;
				self._sendToMarketing();
			});
		} else {
			this._sendToMarketing();
		}

	},
	handleCompare:function(e){
		e.preventDefault();
		var comparison = $(e.currentTarget).find(":selected").attr("id");
		if(typeof comparison == "undefined"){
			this.model.set("comparative",false);
			this.model.set("comparativeType","");
		} else {
			this.model.set("comparative",true);
			this.model.set("comparativeType",comparison);
		}

		this.table_view.destroy();
		this.table_view = undefined;
		this.showTable();
	},
	_sendToMarketing:function(){
		//Ask to save report with new name
		//Call server after save to create new campaign and get link to new campaign
		this.model.createLinkedCampaign();

	},
	events: {
		"click #data-refresh":"sendAjax",
		"click #download-excel":"downloadExcel",
		"click #download-pdf":"downloadPdf",
		"click #download-csv":"downloadCsv",
		"click #save":"saveModal",
		"click #refreshChart":"refreshChart",
		"click #chart-options":"openChartOptions",
		"click #toggle-chart":"toggleChart",
		"click #send-to-marketing":"handleSendToMarketing",
		"change .comparisons":"handleCompare",
		"keyup input.search": "searchTable"
	}

});
