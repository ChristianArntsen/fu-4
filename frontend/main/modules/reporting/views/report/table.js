var reporting_views_report_table = Backbone.Marionette.LayoutView.extend({
		template: JST['reporting/templates/report/table.html'],
		events: {
			//"click td.clickable":"handleTDClick",
			"click .linkout":"launchLinkout"
		},
        ui :{
		    table:".data_table"
        },
		handleTDClick:function(e){
			this.model.addFilter({
				"column":$(e.target).attr("data-column"),
				"operator":"=",
				"value":$(e.target).attr("data-value")
			});

			this.model.getData()
			this.model.forceGetChartData()
			this.model.getSummary()
		},
		initialize: function () {
			this.listenTo(this.model, 'change:data', this.refreshTable);
			this.listenTo(this.model, 'change:loading', this.handleLoading);
			if(this.model.get("data")){
				this.model.set("visibleColumns",this.model.getVisibleColumnObjects());
			}


			_.bind(this.removeDataTableIfNeeded,this);
		},
		onShow: function(){
			this.refreshTable();
		},
        getTableElement:function(){
            return this.ui.table.DataTable();
        },
        searchTable:function(query){
            var table = this.getTableElement();
            table.search(query).draw();
        },
		renderTable:function(){
			var self = this;
			if(this.model.get("data") != null){
				var fixedOrdering = this.getFixedOrderColumns();
				var sortableColumns = this.getUnSortableColumns();
				var order = this.getOrderArray();
				var data = this.prepareData();
				var visibleColumns = self.model.get("visibleColumns");

				self.removeDataTableIfNeeded();
				_.delay(function(){
					if(typeof self.table != "undefined"){
						self.table.clear();
						self.table.rows.add(data);
						self.table.draw();
					} else {
						self.table = self.ui.table.DataTable({
							fixedHeader: {
								header: true,
								footer: true
							},
							orderFixed:fixedOrdering,
							order:order,
							columns:sortableColumns,
							data:data,
							autoWidth: false,
							dom: '<"top"i>Zrtp ',
							pageLength: 100,
							bDeferRender: true,
							fnHeaderCallback: function( nHead, aData, iStart, iEnd, aiDisplay ) {
								var count = nHead.getElementsByTagName('th').length;
								if($(nHead).next("tr").length){
									$(nHead).next("tr").remove();
								}
								$(nHead).after("<tr></tr>");
								var sub_header =$(nHead).next("tr");

								for(var i = 0;i<visibleColumns.length;i++){

									var total = 0;
									var unique_names = {};
									var type = visibleColumns[i].get("type");
									for(var j = 0;j<aData.length;j++){
										if(type=="number"){
											var column = aData[j][i];
											if(column == null || column == "&nbsp;")
												column = 0;
											total+=parseFloat(column);
										} else if(type=="date_range"){
											unique_names[aData[j][i].substring(0,10)] = 1;
										} else {
											unique_names[aData[j][i]] = 1;
										}
									}

									if(visibleColumns[i].get("completeName").indexOf("_id") === -1 || visibleColumns[i].get("completeName").indexOf("count") >= 0 || visibleColumns[i].get("filterable") == false ){
										if(type == "number"){
											sub_header.append("<td class = 'text-right'>"+total.toFixed(2)+"</td>");
										} else {
											sub_header.append("<td>"+Object.keys(unique_names).length+"</td>");
										}
									} else {
										sub_header.append("<td>&nbsp;</td>");
									}

								}
							},
							rowCallback: function( row, data, index ) {
								$(row).find("td").each(function(key,cell){
									$(cell).attr("data-value",$(cell).html());
									if(visibleColumns[key] !== undefined){
										$(cell).attr("data-column",visibleColumns[key].get("completeName").substring(0,255));
									}
									$(cell).addClass("clickable");
									return cell;
								})
								return row;
							}
						});
                        new $.fn.dataTable.ColReorder( self.table);
						self.table.on( 'column-reorder', function ( e, settings, details ) {
							var visibleColumns = self.model.get("columns");
							visibleColumns.swapItems(details['from'],details['to']);
						} );
						self.table.on( 'order.dt', function ( e, settings, details ) {
							if(typeof details[0] != "undefined"){
								self.model.set("ordering",[
									{
										col:visibleColumns[details[0]['col']].get("completeName"),
										dir:details[0].dir
									}
								]);
							}

						} );
					}

				},1)
			}
		},
		removeDataTableIfNeeded:function(){
			if(this.model.get("columns_dirty") && typeof this.table != "undefined"){
				this.model.set("columns_dirty",false)
				this.table.destroy();
                this.ui.table.html("");
				delete(this.table);
			}
		},
		prepareData:function(){

			var self = this;
			var data = [];
			_.each(self.model.get("data"),function(row,row_key){
				var newRow = [];
				_.each(self.model.get("visibleColumns"),function(column,key){
					newRow.push(row[column.get("completeName").substring(0,256)] || "&nbsp;");
				 })
                
                self.model.get("linkouts").each(function(link,key){
                    newRow.push('<button class="btn btn-default btn-xs linkout"  data-row="'+row_key+'" data-linkout="'+key+'">'+link.get("label")+' </button>');
                });

				data.push(newRow);
			})

			return data;
		},
		refreshTable:function(){
			this.model.set("visibleColumns",this.model.getVisibleColumnObjects());
			this.renderTable();
		},
		getFixedOrderColumns: function () {
			var self = this;
			var fixedOrdering = [];
			_.each(self.model.get("grouping"), function (group) {
				self.model.get("visibleColumns").forEach(function(column,index){
					if (column.get("column") == group && column.get("locked")) {
						fixedOrdering.push([index, 'asc']);
						return true;
					};
				});
			});
			return fixedOrdering;
		},
		getOrderArray:function(){
			var self = this;
			var order = [];
			this.model.get("ordering").forEach(function(value){
				var index = self.getColIndex(value.col);
				if(index === false){
					return;
				}
				var direction = value.dir || "asc";
				order.push([
					index,direction
				])
			});
			return order;
		},
		getColIndex : function(colName){
			var foundColumn = this.model.get("visibleColumns").find(function(column,index){
				return column.get("completeName") == colName
			});
			if(foundColumn)
				return this.model.get("visibleColumns").indexOf(foundColumn);
			else
				return false;
		},
		getUnSortableColumns: function () {
			var sortableColumns = [];
			this.model.get("visibleColumns").forEach(function(value,key){
                var column = {
                    title:value.get("label") || value.get("column")
                };
				if (!value.get("sortable")) {
					column.orderable = false;
				} else {
                    column.orderable = true;
				}

                if(value.get("type") == "number"){
                    column.className = "dt-body-right";
                }
				if(value.get("type") == "date_range"){
					column.type = "date";
					column.render = function (data) {
						var date = new moment(data);
						if(!date.isValid()){
							return "-";
						}
						if(date.hour() == 0 && date.minute() == 0)
							return date.format("MMM DD, YYYY");
						else
							return date.format("MMM DD, YYYY h:mm a");
					}
				}

                sortableColumns.push(column);
			})
			if(!this.model.get("linkouts").isEmpty()){
				sortableColumns.push({orderable: false,title:"Link Outs"})
			}
			return sortableColumns;
		},
		handleLoading:function(){
			//this.render();
		},
		behaviors: {
			LoadingBehavior: {
				behaviorClass: reporting_behaviors_loading
			}
			//SelectingBehavior: {
			//	behaviorClass: reporting_behaviors_table_rowsSummary
			//}
		},
		launchLinkout: function(e){
			var self = this;
			var row_key = $(e.target).attr("data-row");
			var row_linkout = $(e.target).attr("data-linkout");
			var data = this.model.get("data")[row_key];
			var linkout = this.model.get("linkouts").at(row_linkout);

			var filterCollection = new FilterCollection();

			linkout.get("columns").each(function(column){
				filterCollection.add({
					column:column.get("completeName"),
					operator:"=",
					value:data[column.get("completeName").substring(0,256)]
				});
			})
			
			var report = new reporting_models_report({
				id:linkout.get("linked_report_id"),
				columns:null,
				group:null,
				date_range:null,
				order:null,
				comparativeReports:[]
			});

			if(self.openingReport){
				return;
			}

			self.openingReport = true;
			report.fetch().success(function(response){
				if(linkout.get("type") == "popup"){
					var view = new reporting_views_report_tableMini({"model":report});
					self.addRegion("popup","#subview");
					report.initialize();

					report.set("filters",filterCollection);
					if(report.get("base") === self.model.get("base")){
						report.set("date_range",self.model.get("date_range"));
					} else {
						var column = report.get("date_range").get("column");
						var dateRange = self.model.get("date_range").clone();
						dateRange.set("column",column);
						report.set("date_range",dateRange);
					}
					report.getData().success(function(response){
						view.refreshTable();
						view.loadDataTable();
						self.openingReport = false;
					})
					self.getRegion("popup").show(view);
				} else if(linkout.get("type")=="new_tab") {
					report.set("date_range",self.model.get("date_range"));
					report.set("filters",filterCollection);
					self.openingReport = false;
					App.vent.trigger("new-tab",report);
				}

			});




		}
	}
);
