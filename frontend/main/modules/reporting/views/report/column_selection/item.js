var reporting_views_columnSelection_item = EpoxyMarionetteView.extend((function(){
	return {
		tagName:"option",
		attributes: function(){
			var attributes = {};
			if(this.model.get("visible") === true){
				attributes.selected = "selected";
			}
			attributes.value = this.model.get("completeName");
			return attributes;
		},
		template: JST['reporting/templates/report/column_selection/item.html'],
		initialize: function(){
		}
	}
})());

