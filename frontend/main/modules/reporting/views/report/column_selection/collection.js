var reporting_views_columnSelection_collection = Backbone.Marionette.CompositeView.extend((function(){

	return {
		template: JST['reporting/templates/report/column_selection/collection.html'],
		childViewContainer: "select",
        onRender: function(){
			var self = this;
			updateColumnModel = _.bind(this.updateColumnModel, this);
			this.$el.find('.selectpicker').multiselect({
				onChange:updateColumnModel,
				buttonText: function(options, select){
					return 'Showing '+ options.length + ' of ' + select.find('option').length + ' columns'
				},
				inheritClass: true,
				onDropdownHide:function(){
					App.vent.trigger('columns-changed');
				}
			});

		},
		updateColumnModel: function (option,checked) {
			var column = this.collection.find(function (item) {
				return item.get("completeName") == $(option).val()
			})
			if (column) {
				column.set("visible", checked);
			}
		},
		getChildView: function(item){
			return reporting_views_columnSelection_item;
		}
	}
})());

