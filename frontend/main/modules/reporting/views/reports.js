var reporting_views_reports = Backbone.Marionette.LayoutView.extend({

	tagName: 'div',
	className: 'row page background-color-gradient',
	template: JST['reporting/templates/reports.layout.html'],
	attributes: {
		id: 'page-reports'
	},

	onBeforeShow: function(){
		var self = this;
		//var navigationView = new reporting_views_navigation({parent:this,collection:this.reports});
		//this.getRegion('navigation').show(navigationView);

		var tabView = new reporting_views_tabs({report_layout:this,collection:this.tabs});
		this.getRegion('tabs').show(tabView);

		if(this.options.id){
			var report = new reporting_models_report({id: this.options.id});
			report.fetch({
				"success":function(model){
					report.initialize();
					self._showReport(report);
				},
				"error":function(collection,response,options){
					App.vent.trigger('notification', {'msg': "Unable fetch report.", 'type': 'danger'});
				}
			});
		} else {
			self.getRegion('report').show(self.createBlankView());
		}


	},
	createBlankView:function(){
		return new reporting_views_report_BlankReport({
			navigation:new reporting_views_navigation({parent:this,collection:this.reports,tags:this.tags})
		});
	},
	initialize:function(){
        var self = this;
		this.tabs = new TabsCollection();
        this.reports = new ReportCollection()
        this.reports.on('request', function() {
            self.$el.find("#loading").show();
            self.$el.find("#content").hide();
        })
        this.reports.on('sync', function() {
            self.$el.find("#loading").hide();
            self.$el.find("#content").show();
        })

		this.tags = new reporting_models_report_tags();
		this.tags.fetch();
		this.reports.fetch({
			"error":function(collection,response,options){
				App.vent.trigger('notification', {'msg': "Unable to retrieve report list.  Contact support if this continues to happen.", 'type': 'danger'});
			}
		});



		this.listenTo(App.vent, 'new-tab', this.handleNewTab);
	},

	regions: {
		tabs: '#r-tabs',
		report: '#r-single-report'
	},
	handleNewTab:function(report){
		var self = this;
		var filters = report.get("filters");

		report.fetch({
			"success":function(model){
				report.initialize();
				report.set("filters",filters);
				currentTab = self.tabs.addTab();
				self.tabs.selectTab(currentTab);
				currentTab.loadReportIntoTab(report);
				if(self.tabs.getSelectedTab() == currentTab){
					self._showReport(report);
				}
			},
			"error":function(collection,response,options){
				App.vent.trigger('notification', {'msg': "Unable fetch report.", 'type': 'danger'});
				self.getRegion('report').hide();
			}
		})
	},
	selectReport: function(report){
		if(report){
			this._showReport(report);
			this.tabs.loadReportIntoTab(report);
		} else {
			this.getRegion('report').show(this.createBlankView());
		}
	},
	preLoadedFilters: [],
    addPreloadFilter: function(filter){
		this.preLoadedFilters.push(filter);
	},
	selectReportById: function(report_id){
		var self= this;
        var report = new reporting_models_report({id: report_id});
		var loadingView = this.createBlankView();
		var currentTab = self.tabs.getSelectedTab();
		var preLoadedFilters = self.preLoadedFilters.slice();
		self.preLoadedFilters = [];
		return report.fetch({
			"success":function(model){
                report.initialize();
                _.each(preLoadedFilters,function(filter){
                    report.addFilter(filter);
				});
                if(self.tabs.length == 0){
					currentTab = self.tabs.addTab();
					self.tabs.selectTab(currentTab);
				}
				currentTab.loadReportIntoTab(report);
				if(self.tabs.getSelectedTab() == currentTab){
					self._showReport(report);
				}
			},
            "error":function(collection,response,options){
                App.vent.trigger('notification', {'msg': "Unable fetch report.", 'type': 'danger'});
                self.getRegion('report').hide();
            }
		});
	},
	clearReportWindow: function(){
		self.getRegion('report').hide();
	},
	_showReport:function(report){
		if(report.get("type") == "static"){
			this.getRegion('report').show(new reporting_views_report_reportStaticLayout({model:report}));
		} else {
			this.getRegion('report').show(new reporting_views_report_reportLayout({model:report}));
		}
	}
});