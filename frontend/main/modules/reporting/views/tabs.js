var reporting_views_tab = Backbone.Marionette.ItemView.extend({
    tagName: "span",
    template:  JST['reporting/templates/tab.html'],
    events:{
        "click #tab":"selectTab",
        "click #closeTab":"closeTab"
    },
    closeTab:function(event){
        this.options.parent.removeTab(this.model);
        event.stopPropagation();
    },
    selectTab:function(){
        this.options.parent.selectTab(this.model);
    }
});

var reporting_views_tabs = Backbone.Marionette.CompositeView.extend({
	template: JST['reporting/templates/tabs.html'],
    childViewContainer: "#tabs",
    childView: reporting_views_tab,
    childViewOptions: function() {
        return { parent: this,parentCollection:this.collection };
    },
    initialize:function(){
        this.listenTo(this.collection, 'change', this.render);
        this.addtab();
    },
    events:{
        "click #addtab":"addtab"
    },
    addtab:function(){
        var newTab = this.collection.addTab();
        this.selectTab(newTab);
    },
    selectTab:function(model){
        if(model != undefined){
            this.collection.selectTab(model);

            //We want the saved view to load and not
            this.options.report_layout.selectReport(model.get("report"));
        } else {
            this.options.report_layout.selectReport(undefined);
        }
    },
    removeTab:function(model){
        this.collection.closeTab(model);
        this.selectTab(this.collection.first())
    }
});