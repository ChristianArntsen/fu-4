var reporting_models_report = Backbone.Model.extend({
	defaults: {
		possibleColumns: [],
		data: [],
		course_id: "",
		ordering: [],
		grouping: [],
		date_range: "",
		filters: "",
		description: "",
		title: "",
		tags: "",
		base: "",
		summary_data: [],
		loading: 0,
		linkouts: [],
		visibleColumns:[],
		split_by:"",
		chart_data:"",
		comparative: false,
		comparativeType:"",
		comparativeReports: [],
		chart_showing:false
	},
	urlRoot: REPORT_TEMPLATES_URL,
	initialize: function(attrs, options){
		this.updateAttributes();

		_.bindAll(this, "syncComparativeReports");
		_.bindAll(this, "handleNewComparativeReport");
		this.on('change:filters', this.syncComparativeReports);
		this.on('change:visibleColumns', this.syncComparativeReports);
		//this.on('change:columns_dirty', this.syncComparativeReports);
		//this.on('change:date_range', this.handleNewComparativeReport);
		this.on('change:comparativeType', this.handleNewComparativeReport);
	},
	handleNewComparativeReport: function(){
		if(this.get("comparativeReports").length > 0){
			var comparativeReport = this.get("comparativeReports")[0];
			this.setDateInCompReports(comparativeReport)
		}

	},
	setDateInCompReports : function(comparativeReport){
		comparativeReport.get("date_range").setToStaticMode();
		if(this.get("comparativeType") == "last_week"){
			//Same range but subtract a week
			comparativeReport.get("date_range").set("start_value",moment(this.get("date_range").get("start_value")).subtract(1,"week").format("MMMM DD, YYYY"));
			comparativeReport.get("date_range").set("end_value",moment(this.get("date_range").get("end_value")).subtract(1,"week").format("MMMM DD, YYYY"));
		} else if(this.get("comparativeType") == "last_month"){
			//Same range but subtract a month
			comparativeReport.get("date_range").set("start_value",moment(this.get("date_range").get("start_value")).subtract(1,"month").format("MMMM DD, YYYY"));
			comparativeReport.get("date_range").set("end_value",moment(this.get("date_range").get("end_value")).subtract(1,"month").format("MMMM DD, YYYY"));
		} else if(this.get("comparativeType") == "last_year"){
			//Same range but subtract a year
			comparativeReport.get("date_range").set("start_value",moment(this.get("date_range").get("start_value")).subtract(1,"year").format("MMMM DD, YYYY"));
			comparativeReport.get("date_range").set("end_value",moment(this.get("date_range").get("end_value")).subtract(1,"year").format("MMMM DD, YYYY"));
		}
	},
	syncComparativeReports: function(){
		if(this.get("comparativeReports").length == 0)
			return;
		var self = this;
		_.forEach(this.get("comparativeReports"),function(value,key){
			value.set("filters",self.get("filters"));
			self.setDateInCompReports(value);
			value.set("visibleColumns",self.get("visibleColumns"));
			if(self.get("columns_dirty"))
				value.set("columns_dirty",self.get("columns_dirty"));
		});
	},
	updateAttributes: function(){
		var self = this;
		var filters = new FilterCollection();


		var dateGrouping = new DateGrouping(this.get('date_range'));
		var columnCollection = new ColumnCollection(this.get('columns'));
		var summaryColumnCollection = new ColumnCollection(this.get('summary_columns'));
		var linkoutCollection = new LinkoutCollection(this.get('linkouts'));

		dateGrouping.set({"report":this});
		this.set({'date_range': dateGrouping});
		this.set({'columns': columnCollection});
		this.set({'summary_columns': summaryColumnCollection});
		this.set({'linkouts': linkoutCollection});

		if(this.get('filters')){
			_.forEach(this.get('filters'),function(filter){
				if(typeof filter == "undefined"){
					return;
				}
				var selectedColumn = self.get("columns").findWhere({completeName:filter.column});
				filter.selectedColumn = selectedColumn;
				filters.addFilter(filter);
			});
			filters.each(function(filter){
				filter.collection = filters;
			})
		}
		filters.report = this;

		this.set({'filters': filters});
	},
	deepClone:function(){
		var clonedModel = new reporting_models_report(this.attributes);
		clonedModel.set("filters",this.get("filters"))
		clonedModel.set("date_range",this.get("date_range"))
		clonedModel.set("columns",this.get("columns"))
		clonedModel.set("summary_columns",this.get("summary_columns"))
		clonedModel.set("linkouts",this.get("linkouts"))

		return clonedModel;
	},
	forceGetChartData: function(){
		return this._chartDataCall();
	},
	getChartData:function(){
        self = this;
		var attributesForcingRefresh = ["xAxis","yAxis","visualWidth","visualGrouping","splitBy"]
		var attributesChanges = Object.keys(this.chartParameters.changedAttributes())
        if(_.intersection(attributesForcingRefresh,attributesChanges).length > 0 || this.get("chart_data") == ""){
            return this._chartDataCall();
        } else {
            var d = $.Deferred();
            d.resolve();
            return d.promise();
        }
	},
	_chartDataCall:function(){

		if(this.chartParameters.get("yAxis") === ""){
			return ;
		}
		var self = this;
		if(this.chart_data_request){
			this.chart_data_request.abort();
		}
		self.set({"chart_data":[]});

		self.set("loading",self.get("loading") + 1);
		var request = $.ajax({
			type: "POST",
			url: self.urlRoot+"/"+self.id+"/chart_data_request",
			data: JSON.stringify(self._prepareChartDataRequest()),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			processData: false
		});
		this.chart_data_request = request;
		request.done(function(response){
			self.set({"chart_data":response});

			self.set("loading",self.get("loading") - 1);
		});
		request.fail(function(response,text_status ){
			if(text_status != "abort"){
				//App.vent.trigger('notification', {'msg': "There was a problem getting the chart data.", 'type': 'danger'});
			}

			self.set("loading",self.get("loading") - 1);
		})
		return request;
	},
	getData:function(){
            self = this;
            self.set("loading",self.get("loading") + 1);
            var request = $.ajax({
                type: "POST",
                url: self.urlRoot+"/"+self.id+"/data_request",
                data: JSON.stringify(self._prepareDataRequest()),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                processData: false
            });
            request.success(function(response){
                self.set({"data":response});
                self.set("loading",self.get("loading") - 1);
            });
            request.error(function(response){
                self.set("loading",self.get("loading") - 1);
                App.vent.trigger('notification', {'msg': "There was a problem with the request.", 'type': 'danger'});
            })


			_.forEach(this.get("comparativeReports"),function(value,key){
				value.set("loading",value.get("loading") + 1);
				self.setDateInCompReports(value);
				var request = $.ajax({
					type: "POST",
					url: value.urlRoot+"/"+value.get("id")+"/data_request",
					data: JSON.stringify(value._prepareDataRequest()),
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					processData: false
				});
				request.success(function(response){
					value.set({"data":response});
					value.set("loading",value.get("loading") - 1);
				});
				request.error(function(response){
					value.set("loading",value.get("loading") - 1);
					App.vent.trigger('notification', {'msg': "There was a problem with the request.", 'type': 'danger'});
				})
			})
            return request;

	},
	downloadCount: 0,
	downloadURL : function downloadURL(url){
		var hiddenIFrameID = 'hiddenDownloader' + this.downloadCount++;
		var iframe = document.createElement('iframe');
		iframe.id = hiddenIFrameID;
		iframe.style.display = 'none';
		document.body.appendChild(iframe);
		iframe.src = url;
	},
	getFile:function(type){
		self = this;
		self.set("loading",self.get("loading") + 1);
		var request = $.ajax({
			type: "POST",
			url: self.urlRoot+"/"+self.id+"/data_request?format="+type,
			data: JSON.stringify(self._prepareDataRequest()),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			processData: false
		});
		request.success(function(response){
			if(!_.isEmpty(response.error)){
				App.vent.trigger('notification', {'msg': "This report doesn't currently support that file type.", 'type': 'danger'});
			} else {
				self.downloadURL("/index.php/api/reports/"+response.destination);
			}
			self.set("loading",self.get("loading") - 1);
			if(self.get("comparative")){
				_.forEach(self.get("comparativeReports"),function(value,key){
					value.getFile(type);
				});
			}

		});
		request.error(function(response){
			self.set("loading",self.get("loading") - 1);
			App.vent.trigger('notification', {'msg': "There was a problem with the request.", 'type': 'danger'});
		});




		return request;
	},
	getSummary:function(){

		self.set("loading",self.get("loading") + 1);
		var summaryRequest = $.ajax({
			type: "POST",
			url: self.urlRoot+"/"+self.id+"/data_request?summary=true",
			data: JSON.stringify(self._prepareDataRequest()),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			processData: false
		});
		summaryRequest.success(function(response){
			self.set({"summary_data":response});
			self.set("loading",self.get("loading") - 1);
		});
		summaryRequest.error(function(response){
			self.set("loading",self.get("loading") - 1);
			App.vent.trigger('notification', {'msg': "There was a problem with the summary request.", 'type': 'danger'});
		})
		return summaryRequest;
	},
    _getPreparedFilters: function () {
        var filters = [];
        this.get("filters").forEach(function (filter) {
            if (filter.get("value") == "" && filter.get("start_value") == "") {
                return;
            }
            var newFilter = {};
            for (var attr in filter.attributes) {
                if ((_.isArray(filter.attributes[attr]) || _.isObject(filter.attributes[attr])) && attr != "value") {
                    continue;
                }
                newFilter[attr] = filter.attributes[attr];
            }
            filters.push(newFilter);
        });
        return filters;
    }, _getPreparedDateRange: function () {
        var dateRange = {};
        for (var attr in this.get("date_range").attributes) {
            if (_.isArray(this.get("date_range").attributes[attr]) || _.isObject(this.get("date_range").attributes[attr])) {
                continue;
            }
            dateRange[attr] = this.get("date_range").attributes[attr];
        }
        return dateRange;
    },
	_prepareChartDataRequest:function(){
        var chart_parameters = this.chartParameters;
        //If it's a date, set the dategrouping column to that date and set xAxis to "date"
        var possibleXColumns =  chart_parameters.get("possibleXColumns");
        var column = possibleXColumns.findWhere({completeName:this.chartParameters.get("xAxis")});
        if(_.isUndefined(column))
        	return [];
        if(column.get("type")=="date_range"){
            var category = "date"
        } else {
            var category = this.chartParameters.get("xAxis")
        }
        var filters = this._getPreparedFilters();
        var dateRange = this._getPreparedDateRange();
        var dataRequest = {
			"category":category,
			"range":this.chartParameters.get("yAxis"),
			"split_by":this.chartParameters.get("splitBy")!=""?this.chartParameters.get("splitBy"):undefined,
			"filters": filters,
			"date_range":dateRange,
			"visual_width":this.chartParameters.get("visualWidth"),
			"visual_grouping":this.chartParameters.get("visualGrouping")
		};
		return dataRequest;
	},
	_prepareDataRequest:function(){

        var filters = this._getPreparedFilters();
        var dateRange = this._getPreparedDateRange();
		var dataRequest = {
			"split_by":undefined,
			"comparative":this.get("comparative"),
			"comparativeType":this.get("comparativeType"),
            "filters": filters,
            "date_range":dateRange,
			"group":this.get("grouping"),
			"columns": this._getColumnsForRequest(),
			"order":this.get("ordering")
		};
		return dataRequest;
	},
	_getColumnsForRequest:function(){
		var columns = [];
		this.get("columns").forEach(function(column,key){
			if(column.get("visible")){
				if(column.get("completeName") == null){
					columns.push(column.get("custom_column"))
				} else {
					columns.push(column.get("completeName"));
				}
			}
		})
		if(!columns.length){
			columns = null;
		}
		return columns;
	},
    getColumnsPossibleValues: function(column){
        var dataRequest = this._prepareDataRequest();
        dataRequest.columns = [column];
        dataRequest.group = [column];
        dataRequest.filters = [];
        dataRequest.order = [];
		dataRequest.split_by = undefined;
        return $.ajax({
            type: "POST",
            url: this.urlRoot+"/"+this.id+"/data_request",
            data: JSON.stringify(dataRequest),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false
        })
    },
	getVisibleColumns:function(){
		return _(
			this.get("columns")
				.where({visible : true})
		)
			.chain()
			.pluck("attributes")
			.pluck("column")
			.value()
	},
	getFilterableColumns:function(){
        return new FilterCollection(this.get("columns").where({filterable : true}))
	},
	getVisibleColumnObjects:function(){
		return this.get("columns").where({visible : true});

	},
	getSelectableColumns:function(){
		var columns = _.map(
			this.get("columns").where({selectable : true}),
			function(column) {
				return column;
			}
		);

		return new ColumnCollection(columns);
	},
	"addFilter":function(newFilter){
		var selectedColumn = this.get("columns").findWhere({completeName:newFilter.column});
		if(!selectedColumn){
			return [];
		}
		newFilter.type = selectedColumn.get("type");
		newFilter.selectedColumn = selectedColumn;
		if(selectedColumn.get("type")=="date_range"){
			var dateTime = moment(newFilter.value);
			newFilter.start_value = dateTime.format("MMMM DD, YYYY");
			newFilter.end_value = dateTime.format("MMMM DD, YYYY");
		}
		this.get("filters").addFilter(newFilter);

	},
	"createLinkedCampaign":function(){
		$.ajax({
			type: "POST",
			url: "/index.php/marketing_campaigns/ajaxSaveCampaign",
			data: JSON.stringify({data:{
				title:"Campaign for `"+this.get("title")+"` report",
				report_id:this.get("id")
			}}),
			success: function(response){
				App.vent.trigger('notification', {'msg':'Campaign started. View <a href="/marketing_campaigns/campaign_builder#/template-overview/'+response.data.campaign_id+'">campaign now.</a>'});
			},
			dataType:'json'
		});
	}
});