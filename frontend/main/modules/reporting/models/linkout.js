var Linkout = Backbone.Model.extend({
	defaults: {
		report_id:"",
		linked_report_id:"",
		label:"",
		columns:[],
		type:"popup"
	},
	initialize: function(attrs, options){
		this.set("columns",new ColumnCollection(this.get('columns')));
	}

});