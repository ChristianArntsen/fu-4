var Column = Backbone.Model.extend({
	defaults: {
		id:"",
		report_id:"",
		label:"",
		column_id:"",
		aggregate:"",
		filterable:true,
		visible:true,
		selectable:true,
		sortable:true,
		locked:false
	},
	initialize: function(attrs, options){
		this.set("completeName",this._getCompleteName());
	},
	_getCompleteName: function(){
		var completeName = this.get("column");

		if(this.get("table")){
			completeName = this.get("table")+"."+this.get("column");
		} else {
			completeName = this.get("column");
		}
		if(this.get("aggregate")){
			completeName = this.get("aggregate")+"("+completeName+")";
		}
		if(this.get("custom_column")){
			completeName = this.get("custom_column");
		}
		return completeName;
	} ,
    generateLabel:function(){
        if(this.get("label") ){
            return this.get("label");
        } else {
            return this.get("completeName");
        }
    },
    shouldBeShown:function(search){
		var label = this.generateLabel().toLowerCase();
        return typeof(search) != "undefined" && label.indexOf(search.toLowerCase()) > -1;
    }
});