var reporting_models_report_filter_text = reporting_models_report_filter.extend({
    defaults: {
        "default_operators":[">=",">","=","<","<="],
        "type": "text"
    }
});
_.extend(reporting_models_report_filter_text.prototype.defaults, reporting_models_report_filter.prototype.defaults);