var reporting_models_report_filter = Backbone.Model.extend({
    defaults: {
        "default_operators": [">=", ">", "=", "<", "<="],
        "column": undefined,
        "operator": "",
        "value": "",
        "loading": "",
        "aggregate": "",
        "filterLabel": "Select a Column",
        "boolean": "AND",
        "post_filter": false,
        "dropdownStatus": "closed"
    },
    initialize: function(arguments) {
        if(arguments.selectedColumn){
            if(arguments.selectedColumn.get("post_filter") == "Y"){
                this.set("post_filter",true);
            }
        }
    }
});