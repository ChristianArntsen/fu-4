var TabsCollection = Backbone.Collection.extend({
    model: function(attrs, options){
        return new Tab(attrs, options);
    },
    getInstance: function () {
        if (this._instance === undefined) {
            this._instance = new this();
        }
        return this._instance;
    },
    addTab: function(){
        return this.add(new Tab());
    },
    currentTab: undefined,
    selectTab:function(tab){
        if(tab != undefined){

            this.currentTab = tab;
            this.each(function(value){
                value.set("active",false);
            });
            tab.set("active",true);
        }
    },
    getSelectedTab:function(){
        return this.currentTab;
    },
    closeTab:function(model){
        model.destroy();
        this.selectTab(this.first())
    },
    loadReportIntoTab:function(report){
        var selectedTab = this.getSelectedTab();
        if(selectedTab == undefined){
            selectedTab = this.addTab();
            this.selectTab(selectedTab);
        }
        selectedTab.set("report",report);
        selectedTab.set("title",report.get("title"))
    },
    initialize:function(){
    }

});
