var reporting_models_chartParameters = Backbone.Model.extend({
	defaults: function(){
        return {
            columns: [],
            xAxis: "",
            yAxis: "",
            possibleXColumns: new Backbone.Collection(),
            possibleYColumns: new Backbone.Collection(),
            possibleCharts: ["Pi Chart","Line Graph","Bar Chart"],
            selectedGraph:"bar",
            visualWidth:"day",
            visualGrouping:"hour",
			splitBy:"",
            possibleGroupings:[],
            refreshRequired:true
        }
	},
	urlRoot: REPORT_TEMPLATES_URL,
	initialize:function(attr){
		var self = this;
		if(attr.columns){
			this.columns = attr.columns;
			attr.columns.each(function(column){
				if(column.get("aggregate") != "" && column.get("aggregate") != "max" && column.get("filterable")){
					self.get("possibleYColumns").add(column);
				}
                if(column.get("type") !== "number" && column.get("filterable") ){
					self.get("possibleXColumns").add(column);
				}
			})
		}
        if(this.get("possibleXColumns").first())
		    this.set("xAxis",this.get("possibleXColumns").first().get("completeName"));
        if(this.get("possibleYColumns").first())
            this.set("yAxis",this.get("possibleYColumns").first().get("completeName"));
		this.initializePossibleGroupings();
		this.on('change:visualWidth',this.initializePossibleGroupings,this);
	},
	initializePossibleGroupings:function(){
		var possibleGroupings = [];
		if(this.get("visualWidth") == "day"){
			possibleGroupings.push({label:"Hour",value:"hour"});
		} else if(this.get("visualWidth") == "week"){
			possibleGroupings.push({label:"Day of Week",value:"dayofweek"});
		} else if(this.get("visualWidth") == "month"){
			possibleGroupings.push({label:"Day",value:"day"});
		} else if(this.get("visualWidth") == "year"){
			possibleGroupings.push({label:"Week",value:"week"});
			possibleGroupings.push({label:"Month",value:"month"});
		} else {
			possibleGroupings.push({label:"Hour",value:"hour"});
			possibleGroupings.push({label:"Day",value:"day"});
			possibleGroupings.push({label:"Week",value:"week"});
			possibleGroupings.push({label:"Month",value:"month"});
			possibleGroupings.push({label:"Year",value:"year"});
		}
		this.set("possibleGroupings",possibleGroupings);

		this.set("visualGrouping",possibleGroupings[possibleGroupings.length - 1].value);
	}

});