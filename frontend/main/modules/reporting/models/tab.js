var Tab = Backbone.Model.extend({
    defaults:{
        report:null,
        active:false,
        title:"No Report Selected"
    },
    loadReportIntoTab:function(report){
        this.set("report",report);
        this.set("title",report.get("title"));
    }
});