var DataImportUploadView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    className: 'col-md-12',
    upload_field: false,
    upload_data: false,
    template: JST['data_import/templates/data_import_upload.html'],

    events: {
        "click .continue": function(){
            this.trigger('next', this, this.model);
        },
        "click .back": function(){
            this.model.set('data', false);
            this.render();
        },
        "click .select-file": "select_file",
        "dragover .data-import-upload": "drag_highlight",
        "dragend .data-import-upload": "drag_clear",
        "dragleave .data-import-upload": "drag_clear",
        "dragexit .data-import-upload": "drag_clear",
        "drop .data-import-upload": "drag_clear"
    },

    drag_highlight: function(){
        this.$el.find('div.data-import-upload').addClass('drag');
    },

    drag_clear: function(){
        this.$el.find('div.data-import-upload').removeClass('drag');
    },

    onRender: function(){

        var view = this;
        this.upload_field = this.$el.find('#fileupload');

        this.upload_field.fileupload({
            url: API_URL + '/data_import/upload?api_key=no_limits',
            dataType: 'json',
            add: function (e, data) {
                if(!data.files[0]){
                    return false;
                }
                view.upload_data = data;

                // Parse the file before uploading so we can catch any file errors
                view.parse_file(data.files[0]);
            },

            done: function (e, data){
                if(!data || !data.files) {
                    return false;
                }

                if(data.jqXHR && data.jqXHR.responseJSON && data.jqXHR.responseJSON.id){
                    view.model.set('id', data.jqXHR.responseJSON.id);
                }
                App.data.data_import_jobs.add(view.model);
                view.show_success();
            },

            progressall: function(e, data){
                var percentage = parseInt(data.loaded / data.total * 100, 10);
                view.$el.find('span.percentage').html(percentage);
                view.$el.find('#upload-progress-bar').css('width', percentage + '%');
            }
        });
    },

    parse_file: function(file){

        var view = this;
        this.model.set('file', file);
        this.show_uploading();

        var first_row_headers = this.$el.find('#first-row-headers').is(':checked');
        this.model.set_setting('first_row_headers', first_row_headers);
        this.$el.find('div.data-import-settings').hide();

        var file_name = file.name.split('.');
        var ext = file_name[file_name.length - 1].toLowerCase();
        if(ext != 'csv' && ext != 'txt'){
            this.model.set('error', 'File must be in a valid CSV format');
            this.show_error();
            return false;
        }

        // Parse the file
        this.model.parse_file(function(){
            var data = view.model.get('data');
            if(data.errors.length > 0){
                view.model.set('error', 'There was a problem reading the file. Make sure it is a valid CSV file.');
                view.show_error();
                return false;
            }
            view.start_upload();
        });
    },

    start_upload: function(){

        var view = this;
        this.model.set({
            type: 'customers',
            source_type: 'csv'
        });

        view.upload_data.submit()
            .error(function(xhr){
                if(xhr && xhr.responseJSON && xhr.responseJSON.msg){
                    view.model.set('error', xhr.responseJSON.msg);
                }
                view.show_error();
            });
    },

    select_file: function(){
        this.upload_field.trigger('click');
        return false;
    },

    show_error: function(){
        var template = JST['data_import/templates/data_import_upload_error.html'];
        this.$el.find('div.data-import-upload').html(template(this.model.toJSON()));
    },

    show_success: function(){
        var template = JST['data_import/templates/data_import_upload_success.html'];
        this.$el.find('div.data-import-upload').html(template(this.model.toJSON()));
    },

    show_uploading: function(){
        var template = JST['data_import/templates/data_import_upload_loading.html'];
        this.$el.find('div.data-import-settings').show();
        this.$el.find('div.data-import-upload').html(template(this.model.toJSON()));
    }
});