var DataImportFinishView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    className: 'col-md-12',
    template: JST['data_import/templates/data_import_finish.html'],
    status_checker: false,

    events: {
        "click button.start-import": "finish",
        "click button.back": function(){ this.trigger('previous', this); },
        "click button.cancel-in-progress": "cancel_job",
        "blur #data-import-notify-email": "save_email"
    },

    initialize: function(){
        this.listenTo(this.model, 'change:status change:percent_complete change:response', this.render);
    },

    onRender: function(){
        var view = this;
        var status = this.model.get('status');

        if((status == 'ready' || status == 'in-progress') && !this.status_checker){
            this.status_checker = setInterval(function(){
                view.model.fetch();
            }, 2500);

            $('button.cancel-import').html('Close');
        }

        if((status == 'completed' || status == 'cancelled') && this.status_checker){
            clearInterval(this.status_checker);
            view.model.fetch();
        }
    },

    save_email: function(){
        var email = this.$el.find('#data-import-notify-email').val();
        this.model.set_setting('notify_email', email);
    },

    finish: function(){
        this.model.save({status: 'ready'});
        return false;
    },

    cancel_job: function(){
        if(!confirm('Cancelling now may result in a partial import, are you sure?')){
            return false;
        }
        this.model.set({'status':'cancelled'}, {silent: true});
        this.$el.find('button.cancel-in-progress').button('loading');
        this.model.save();
    },

    onBeforeDestroy: function(){
        if(this.status_checker){
            clearInterval(this.status_checker);
        }
    }
});