var DataImportMatchView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    className: 'col-md-12',
    template: JST['data_import/templates/data_import_match.html'],

    events: {
        "click button.continue": 'save_match',
        "click button.back": function(){ this.trigger('previous', this); }
    },

    save_match: function(){

        var view = this;
        var columns = {};
        this.$el.find('select.column-select').each(function(){

            var csv_column_index = $(this).val().replace('col_', '');
            if(!csv_column_index){
                return true;
            }
            csv_column_index = parseInt(csv_column_index);

            var destination_key = $(this).data('destination-col');
            columns[destination_key] = csv_column_index;
        });
        var unique_map = _.uniq(Object.values(columns));
        if(unique_map.length != _.size(columns)){
            App.vent.trigger('notification', {msg: 'Error, a single source column is being matched to multiple destination columns', type: 'error'});
            return false;
        }

        var fields = this.model.get_fields();
        var required_field_error = false;
        _.each(fields, function(field, key){
            if(field.required && typeof(columns[key]) == 'undefined'){
                required_field_error = true;
            }
        });

        if(required_field_error){
            App.vent.trigger('notification', {msg: 'Error, required fields must be matched', type: 'error'});
            return false;
        }

        this.model.set_setting('column_map', columns);
        this.model.save();

        this.trigger('next', this);
    }
});