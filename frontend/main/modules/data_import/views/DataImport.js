var DataImportView = ModalLayoutView.extend({

    step: 1,
    tagName: 'div',
    template: JST['data_import/templates/data_import.html'],

    events: {
        "click .close-job": "close_job"
    },

    initialize: function(){
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-lg', closable: false});

        if(this.model.get('status') != 'pending'){
            this.step = 4;
        }
    },

    regions: {
        'ImportSteps': '#import-steps'
    },

    onRender: function(){

        this.$el.find('.data-import-step').removeClass('active');
        for(var x = 1; x < this.step; x++){
            this.$el.find('#data-import-step-'+x).addClass('complete');
        }
        this.$el.find('#data-import-step-'+this.step).addClass('active');

        if(this.step == 1) {
            var child_view = new DataImportUploadView({model: this.model});
        }else if(this.step == 2){
            var child_view = new DataImportMatchView({model: this.model});
        }else if(this.step == 3){
            var child_view = new DataImportPreviewView({model: this.model});
        }else if(this.step == 4){
            var child_view = new DataImportFinishView({model: this.model});
        }

        this.ImportSteps.show(child_view);

        this.listenTo(child_view, 'next', this.next_step);
        this.listenTo(child_view, 'previous', this.previous_step);
    },

    next_step: function(){
        this.step++;
        this.render();
    },

    previous_step: function(){
        this.step--;
        this.render();
    },

    close_job: function(){

        if(this.model.get('status') != 'completed' && this.model.get('status') != 'cancelled'){
            if(!confirm('By cancelling the import you will lose all progress, continue?')){
                return false;
            }
        }

        this.model.destroy();
        this.hide();
    }
});