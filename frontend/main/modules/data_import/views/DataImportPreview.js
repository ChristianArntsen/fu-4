var DataImportPreviewView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    className: 'col-md-12',
    template: JST['data_import/templates/data_import_preview.html'],
    offset: 0,
    limit: 10,
    total: 0,

    events: {
        "click .prev-results": "prev_results",
        "click .next-results": "next_results",
        "click .continue": function(){ this.trigger('next', this); },
        "click .back": function(){ this.trigger('previous', this); }
    },

    initialize: function(){

    },

    prev_results: function(){

        if(this.offset <= 1){ return false; }
        this.offset -= this.limit;
        this.render();
        return false;
    },

    next_results: function(){

        if((this.offset + this.limit) >= this.model.get('total_records')) return false;

        this.offset += this.limit;
        this.render();
        return false;
    },

    serializeData: function(){

        var data = this.model.toJSON();
        data.offset = this.offset;
        data.limit = this.limit;
        return data;
    }
});