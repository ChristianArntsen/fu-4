<?php

header('Strict-Transport-Security: max-age=31536000');

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>HTTPS enabled</title>
</head>
<body>
<main>
<h1>HTTPS has been permanently enabled for this browser</h1>
    <p>The Strict Transport Security header has been sent to your browser.
    This means that the browser will automatically ensure that it is connected to foreUP's software securely via HTTPS.</p>
    <p>Users should be aware that <b>this will interfere with receipt printing</b> if HTTPS has not been enabled for network printers.
        Problems will also arise if users attempt to access both HTTPS and non-HTTPS enabled terminals from the same browser.</p>
    <p>If this action was taken in error, take the following steps:
    <ol>
        <li>Do not close this tab</li>
        <li>Navigate to your browser's HSTS settings by entering "chrome://net-internals/#hsts" in the url bar</li>
        <li>Delete this domain</li>
        <li>Return to the foreUP home screen and navigate to "Settings" (or <a href="/config">follow this link</a>)</li>
        <li>Under the "Terminals" tab, find this terminal and click the edit button</li>
        <li>Unset the "Force HTTPS" selector</li>
        <li>Log out of the software</li>
        <li>Close this tab</li>
    </ol></p>
    <a href="/index.php">Click here to return to the Home Page</a>
</main>
</body>
</html>
